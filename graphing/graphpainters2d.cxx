/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <vector>
#include <QApplication>

#include "graphing/graphpainters2d.hxx"
#include "guicore/guiformat.hxx"

using namespace CPD3::GUI;
using namespace CPD3::Algorithms;

namespace CPD3 {
namespace Graphing {

/** @file graphing/graphpainters2d.hxx
 * Painters for two dimensional graphs.
 */

static inline bool pointsContiguous(double endA, double startB, double continuity)
{
    if (!FP::defined(continuity))
        return true;
    if (!FP::defined(endA) || !FP::defined(startB))
        return true;
    return (startB - endA) <= continuity;
}

static QRectF transformersToClippingRect(const AxisTransformer &x, const AxisTransformer &y)
{
    double px;
    double w;
    if (x.getScreenMin() < x.getScreenMax()) {
        px = x.getScreenMin();
        w = x.getScreenMax() - x.getScreenMin() + 1;
    } else {
        px = x.getScreenMax();
        w = x.getScreenMin() - x.getScreenMax() + 1;
    }

    double py;
    double h;
    if (y.getScreenMin() < y.getScreenMax()) {
        py = y.getScreenMin();
        h = y.getScreenMax() - y.getScreenMin() + 1;
    } else {
        py = y.getScreenMax();
        h = y.getScreenMin() - y.getScreenMax() + 1;
    }

    return QRectF(px, py, w, h);
}

static const double maximumPointNearDistance = 10.0;

static void findNearestPoint(const QPointF &target,
                             const std::vector<TracePoint<3>> &points,
                             AxisTransformer &tX,
                             AxisTransformer &tY,
                             TracePoint<3> &result,
                             double &distance)
{
    distance = FP::undefined();
    for (const auto &it : points) {
        double y = tY.toScreen(it.d[1], false);
        if (!FP::defined(y))
            continue;
        if (tY.screenClipped(y))
            continue;
        double x = tX.toScreen(it.d[0], false);
        if (!FP::defined(x))
            continue;
        if (tX.screenClipped(x))
            continue;
        double d = x - target.x();
        double targetDistance = d * d;
        d = y - target.y();
        targetDistance += d * d;
        if (FP::defined(distance) && distance < targetDistance)
            continue;
        distance = targetDistance;
        result = it;
    }
    if (FP::defined(distance)) {
        distance = sqrt(distance);
        if (distance > maximumPointNearDistance)
            distance = FP::undefined();
    }
}

/**
 * Create a new line painter.
 * 
 * @param setPoints         the points to paint (in X,Y,Z(color, ignored) order)
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setLineWidth      the line width
 * @param setStyle          the line style
 * @param setColor          the color to paint with
 * @param setIsolatedPointSymbol the symbol to paint when points are isolated (or unselected to disable)
 * @param setContinuity     the maximum continuity gap time
 * @param setSkipUndefined  if set then don't consider undefined points
 * @param setBaselineX      the X axis baseline
 * @param setBaselineY      the Y axis baseline
 * @param priority          the draw sort priority
 */
GraphPainter2DLines::GraphPainter2DLines(std::vector<TracePoint<3>> setPoints,
                                         const AxisTransformer &setX,
                                         const AxisTransformer &setY,
                                         qreal setLineWidth,
                                         Qt::PenStyle setStyle,
                                         const QColor &setColor,
                                         const TraceSymbol &setIsolatedPointSymbol,
                                         double setContinuity,
                                         bool setSkipUndefined,
                                         quintptr setTracePtr,
                                         int priority) : Graph2DDrawPrimitive(priority),
                                                         points(std::move(setPoints)),
                                                         tX(setX),
                                                         tY(setY),
                                                         lineWidth(setLineWidth),
                                                         style(setStyle),
                                                         color(setColor),
                                                         symbol(setIsolatedPointSymbol),
                                                         continuity(setContinuity),
                                                         skipUndefined(setSkipUndefined),
                                                         tracePtr(setTracePtr)
{ }

GraphPainter2DLines::~GraphPainter2DLines() = default;

void GraphPainter2DLines::paint(QPainter *painter)
{
    if (points.empty())
        return;

    if (points.size() == 1) {
        if (symbol.getType() == TraceSymbol::Unselected)
            return;
        double y = tY.toScreen(points.front().d[1], false);
        if (!FP::defined(y))
            return;
        double x = tX.toScreen(points.front().d[0], false);
        if (!FP::defined(x))
            return;

        painter->save();
        painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
        QBrush brush(color);
        painter->setBrush(brush);
        painter->setPen(QPen(brush, 1));
        symbol.paint(painter, x, y);
        painter->restore();
        return;
    }

    std::vector<Graph2DSimpleDrawPoint<2> > paintIsolated;
    QPainterPath linePath;
    bool priorFirst = true;
    auto priorPoint = points.cbegin();
    for (auto it = priorPoint + 1, end = points.cend(); it != end; ++it) {
        /* Not priorPoint, since the continuity check should still be able
         * to cause breaks between undefined points */
        if (!pointsContiguous((it - 1)->end, it->start, continuity)) {
            if (symbol.getType() != TraceSymbol::Unselected) {
                if (priorFirst)
                    paintIsolated.emplace_back(*(it - 1));
                if (it == end - 1)
                    paintIsolated.emplace_back(*it);
            }
            priorFirst = true;
            priorPoint = it;
            continue;
        }

        double x = tX.toScreen(it->d[0], false);
        double y = tY.toScreen(it->d[1], false);
        if (!FP::defined(y) || !FP::defined(x)) {
            if (skipUndefined)
                continue;
            if (priorFirst && symbol.getType() != TraceSymbol::Unselected) {
                paintIsolated.emplace_back(*priorPoint);
            }
            priorFirst = true;
            priorPoint = it;
            continue;
        }

        if (priorFirst) {
            double px = tX.toScreen(priorPoint->d[0], false);
            double py = tY.toScreen(priorPoint->d[1], false);
            if (!FP::defined(py) || !FP::defined(px)) {
                priorFirst = true;
                priorPoint = it;
                continue;
            }
            linePath.moveTo(px, py);
            priorFirst = false;
        }
        linePath.lineTo(x, y);
    }

    if (linePath.isEmpty() && paintIsolated.empty())
        return;

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    QBrush brush(color);
    painter->setBrush(brush);

    if (!linePath.isEmpty()) {
        painter->strokePath(linePath, QPen(brush, lineWidth, style));
    }

    if (!paintIsolated.empty()) {
        painter->setPen(QPen(brush, 1));
        for (const auto &it : paintIsolated) {
            double y = tY.toScreen(it.d[1], false);
            if (!FP::defined(y))
                continue;
            double x = tX.toScreen(it.d[0], false);
            if (!FP::defined(x))
                continue;

            symbol.paint(painter, x, y);
        }
    }

    painter->restore();
}

void GraphPainter2DLines::updateMouseover(const QPointF &point,
                                          std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    if (points.empty())
        return;
    TracePoint<3> hit;
    double distance;
    findNearestPoint(point, points, tX, tY, hit, distance);
    if (!FP::defined(distance))
        return;
    if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(output.get())) {
        QPointF otherDelta(trace->getCenter() - point);
        if (sqrt(otherDelta.x() * otherDelta.x() + otherDelta.y() * otherDelta.y()) < distance)
            return;
    }

    output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverComponentTraceStandard(
            QPointF(tX.toScreen(hit.d[0], true), tY.toScreen(hit.d[1], true)), tracePtr, hit));
}

void GraphPainter2DLines::updateMouseClick(const QPointF &,
                                           std::shared_ptr<Graph2DMouseoverComponent> &)
{ }

enum {
    Gradient_InvalidX = 0x1,
    Gradient_InvalidY = 0x2,
    Gradient_XDirectionMask = 0xF0,
    Gradient_XDirectionAscending = 0x10,
    Gradient_XDirectionDescending = 0x20,
    Gradient_YDirectionMask = 0xF00,
    Gradient_YDirectionAscending = 0x100,
    Gradient_YDirectionDescending = 0x200,
};

template<typename PointType, std::size_t idxX, std::size_t idxY, std::size_t idxZ>
void paintGradientLines(QPainter *painter,
                        qreal lineWidth,
                        Qt::PenStyle style,
                        TraceGradient &inputGradient,
                        typename std::vector<PointType>::const_iterator start,
                        typename std::vector<PointType>::const_iterator end,
                        int bits,
                        qreal &dashOffset,
                        bool skipUndefined = false)
{
    QPainterPath path;
    QLinearGradient gradient;

    if (!(bits & Gradient_InvalidX)) {
        bool prior = false;
        qreal offset;
        qreal scale;

        if (bits & Gradient_XDirectionAscending) {
            offset = start->d[idxX];
            scale = 1.0 / (end->d[idxX] - start->d[idxX]);
            gradient.setStart(start->d[idxX], start->d[idxY]);
            gradient.setFinalStop(end->d[idxX], start->d[idxY]);
        } else {
            offset = end->d[idxX];
            if (start->d[idxX] != end->d[idxX])
                scale = 1.0 / (start->d[idxX] - end->d[idxX]);
            else
                scale = 0;
            gradient.setStart(end->d[idxX], start->d[idxY]);
            gradient.setFinalStop(start->d[idxX], start->d[idxY]);
        }

        for (auto it = start; it != end + 1; ++it) {
            if (!FP::defined(it->d[idxY]) ||
                    !FP::defined(it->d[idxZ]) ||
                    !FP::defined(it->d[idxX])) {
                if (!skipUndefined)
                    prior = false;
                continue;
            }
            if (!prior) {
                path.moveTo(it->d[idxX], it->d[idxY]);
                prior = true;
            } else {
                path.lineTo(it->d[idxX], it->d[idxY]);
            }
            gradient.setColorAt((it->d[idxX] - offset) * scale,
                                inputGradient.evaluate(it->d[idxZ]));
        }
    } else {
        bool prior = false;
        qreal offset;
        qreal scale;

        if (bits & Gradient_YDirectionAscending) {
            offset = start->d[idxY];
            scale = 1.0 / (end->d[idxY] - start->d[idxY]);
            gradient.setStart(start->d[idxX], start->d[idxY]);
            gradient.setFinalStop(start->d[idxX], end->d[idxY]);
        } else {
            offset = end->d[idxY];
            if (start->d[idxY] != end->d[idxY])
                scale = 1.0 / (start->d[idxY] - end->d[idxY]);
            else
                scale = 0;
            gradient.setStart(end->d[idxX], end->d[idxY]);
            gradient.setFinalStop(end->d[idxX], start->d[idxY]);
        }

        for (auto it = start; it != end + 1; ++it) {
            if (!FP::defined(it->d[idxY]) ||
                    !FP::defined(it->d[idxZ]) ||
                    !FP::defined(it->d[idxX])) {
                if (!skipUndefined)
                    prior = false;
                continue;
            }
            if (!prior) {
                path.moveTo(it->d[idxX], it->d[idxY]);
                prior = true;
            } else {
                path.lineTo(it->d[idxX], it->d[idxY]);
            }
            gradient.setColorAt((it->d[idxY] - offset) * scale,
                                inputGradient.evaluate(it->d[idxZ]));
        }
    }

    QPen pen(QBrush(gradient), lineWidth, style);
    pen.setDashOffset(dashOffset);
    painter->setPen(pen);
    painter->setBrush(QBrush(Qt::NoBrush));
    painter->drawPath(path);
    dashOffset = painter->pen().dashOffset();
}

template<typename PointType, std::size_t idxX, std::size_t idxY, std::size_t idxZ>
void handleGradientBreakdown(QPainter *painter,
                             qreal lineWidth,
                             Qt::PenStyle style,
                             TraceGradient &gradient,
                             typename std::vector<PointType>::const_iterator &start,
                             typename std::vector<PointType>::const_iterator it,
                             typename std::vector<PointType>::const_iterator prior,
                             int &bits,
                             qreal &dashOffset,
                             bool skipUndefined = false)
{
    double x = it->d[idxX];
    double y = it->d[idxY];
    double z = it->d[idxZ];
    if (!FP::defined(y) || !FP::defined(z) || !FP::defined(x)) {
        if (start != prior) {
            paintGradientLines<PointType, idxX, idxY, idxZ>(painter, lineWidth, style, gradient,
                                                            start, it - 1, bits, dashOffset,
                                                            skipUndefined);
        }
        start = it;
        bits = 0;
        return;
    }

    double priorX = prior->d[idxX];
    double priorY = prior->d[idxY];
    double priorZ = prior->d[idxZ];
    if (!FP::defined(priorY) || !FP::defined(priorZ) || !FP::defined(priorX)) {
        start = it;
        bits = 0;
        return;
    }

    if (!(bits & Gradient_InvalidX)) {
        if (!(bits & Gradient_XDirectionMask)) {
            if (priorX < x) {
                bits |= Gradient_XDirectionAscending;
            } else if (priorX > x) {
                bits |= Gradient_XDirectionDescending;
            } else if (priorZ != z) {
                if (bits & Gradient_InvalidY) {
                    paintGradientLines<PointType, idxX, idxY, idxZ>(painter, lineWidth, style,
                                                                    gradient, start, it - 1, bits,
                                                                    dashOffset, skipUndefined);
                    start = it - 1;
                    bits = 0;
                    handleGradientBreakdown<PointType, idxX, idxY, idxZ>(painter, lineWidth, style,
                                                                         gradient, start, it, prior,
                                                                         bits, dashOffset,
                                                                         skipUndefined);
                    return;
                } else {
                    bits |= Gradient_InvalidX;
                }
            }
        } else {
            if (priorX == x && priorZ != z) {
                if (bits & Gradient_InvalidY) {
                    paintGradientLines<PointType, idxX, idxY, idxZ>(painter, lineWidth, style,
                                                                    gradient, start, it - 1, bits,
                                                                    dashOffset, skipUndefined);
                    start = it - 1;
                    bits = 0;
                    handleGradientBreakdown<PointType, idxX, idxY, idxZ>(painter, lineWidth, style,
                                                                         gradient, start, it, prior,
                                                                         bits, dashOffset,
                                                                         skipUndefined);
                    return;
                } else {
                    bits |= Gradient_InvalidX;
                }
            }
            if (bits & Gradient_XDirectionAscending) {
                if (priorX > x) {
                    if (bits & Gradient_InvalidY) {
                        paintGradientLines<PointType, idxX, idxY, idxZ>(painter, lineWidth, style,
                                                                        gradient, start, it - 1,
                                                                        bits, dashOffset,
                                                                        skipUndefined);
                        start = it - 1;
                        bits = 0;
                        handleGradientBreakdown<PointType, idxX, idxY, idxZ>(painter, lineWidth,
                                                                             style, gradient, start,
                                                                             it, prior, bits,
                                                                             dashOffset,
                                                                             skipUndefined);
                        return;
                    } else {
                        bits |= Gradient_InvalidX;
                    }
                }
            } else {
                if (priorX < x) {
                    if (bits & Gradient_InvalidY) {
                        paintGradientLines<PointType, idxX, idxY, idxZ>(painter, lineWidth, style,
                                                                        gradient, start, it - 1,
                                                                        bits, dashOffset,
                                                                        skipUndefined);
                        start = it - 1;
                        bits = 0;
                        handleGradientBreakdown<PointType, idxX, idxY, idxZ>(painter, lineWidth,
                                                                             style, gradient, start,
                                                                             it, prior, bits,
                                                                             dashOffset,
                                                                             skipUndefined);
                        return;
                    } else {
                        bits |= Gradient_InvalidX;
                    }
                }
            }
        }
    }

    if (!(bits & Gradient_InvalidY)) {
        if (!(bits & Gradient_YDirectionMask)) {
            if (priorY < y) {
                bits |= Gradient_YDirectionAscending;
            } else if (priorY > y) {
                bits |= Gradient_YDirectionDescending;
            } else if (priorZ != z) {
                if (bits & Gradient_InvalidX) {
                    paintGradientLines<PointType, idxX, idxY, idxZ>(painter, lineWidth, style,
                                                                    gradient, start, it - 1, bits,
                                                                    dashOffset, skipUndefined);
                    start = it - 1;
                    bits = 0;
                    handleGradientBreakdown<PointType, idxX, idxY, idxZ>(painter, lineWidth, style,
                                                                         gradient, start, it, prior,
                                                                         bits, dashOffset,
                                                                         skipUndefined);
                    return;
                } else {
                    bits |= Gradient_InvalidY;
                }
            }
        } else {
            if (priorY == y && priorZ != z) {
                if (bits & Gradient_InvalidX) {
                    paintGradientLines<PointType, idxX, idxY, idxZ>(painter, lineWidth, style,
                                                                    gradient, start, it - 1, bits,
                                                                    dashOffset, skipUndefined);
                    start = it - 1;
                    bits = 0;
                    handleGradientBreakdown<PointType, idxX, idxY, idxZ>(painter, lineWidth, style,
                                                                         gradient, start, it, prior,
                                                                         bits, dashOffset,
                                                                         skipUndefined);
                    return;
                } else {
                    bits |= Gradient_InvalidY;
                }
            }
            if (bits & Gradient_YDirectionAscending) {
                if (priorY > y) {
                    if (bits & Gradient_InvalidX) {
                        paintGradientLines<PointType, idxX, idxY, idxZ>(painter, lineWidth, style,
                                                                        gradient, start, it - 1,
                                                                        bits, dashOffset,
                                                                        skipUndefined);
                        start = it - 1;
                        bits = 0;
                        handleGradientBreakdown<PointType, idxX, idxY, idxZ>(painter, lineWidth,
                                                                             style, gradient, start,
                                                                             it, prior, bits,
                                                                             dashOffset,
                                                                             skipUndefined);
                        return;
                    } else {
                        bits |= Gradient_InvalidY;
                    }
                }
            } else {
                if (priorY < y) {
                    if (bits & Gradient_InvalidX) {
                        paintGradientLines<PointType, idxX, idxY, idxZ>(painter, lineWidth, style,
                                                                        gradient, start, it - 1,
                                                                        bits, dashOffset,
                                                                        skipUndefined);
                        start = it - 1;
                        bits = 0;
                        handleGradientBreakdown<PointType, idxX, idxY, idxZ>(painter, lineWidth,
                                                                             style, gradient, start,
                                                                             it, prior, bits,
                                                                             dashOffset,
                                                                             skipUndefined);
                        return;
                    } else {
                        bits |= Gradient_InvalidY;
                    }
                }
            }
        }
    }
}

/**
 * Create a new line painter.
 * 
 * @param setPoints         the points to paint (in X,Y,Z(color) order)
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setZ              the Z (color) transformer (0-1)
 * @param setLineWidth      the line width
 * @param setStyle          the line style
 * @param setGradient       the color gradient paint with
 * @param setIsolatedPointSymbol the symbol to paint when points are isolated (or unselected to disable)
 * @param setContinuity     the maximum continuity gap time
 * @param setSkipUndefined  if set then don't consider undefined points
 * @param priority          the draw sort priority
 */
GraphPainter2DLinesGradient::GraphPainter2DLinesGradient(std::vector<TracePoint<3> > setPoints,
                                                         const AxisTransformer &setX,
                                                         const AxisTransformer &setY,
                                                         const AxisTransformer &setZ,
                                                         qreal setLineWidth,
                                                         Qt::PenStyle setStyle,
                                                         const TraceGradient &setGradient,
                                                         const TraceSymbol &setIsolatedPointSymbol,
                                                         double setContinuity,
                                                         bool setSkipUndefined,
                                                         quintptr setTracePtr,
                                                         int priority) : Graph2DDrawPrimitive(
        priority),
                                                                         points(std::move(
                                                                                 setPoints)),
                                                                         tX(setX),
                                                                         tY(setY),
                                                                         tZ(setZ),
                                                                         lineWidth(setLineWidth),
                                                                         style(setStyle),
                                                                         gradient(setGradient),
                                                                         symbol(setIsolatedPointSymbol),
                                                                         continuity(setContinuity),
                                                                         skipUndefined(
                                                                                 setSkipUndefined),
                                                                         tracePtr(setTracePtr)
{ }

GraphPainter2DLinesGradient::~GraphPainter2DLinesGradient() = default;

void GraphPainter2DLinesGradient::paint(QPainter *painter)
{
    if (points.empty())
        return;

    if (points.size() == 1) {
        if (symbol.getType() == TraceSymbol::Unselected)
            return;
        double y = tY.toScreen(points.front().d[1], false);
        if (!FP::defined(y))
            return;
        double z = tZ.toScreen(points.front().d[2], true);
        if (!FP::defined(z))
            return;
        double x = tX.toScreen(points.front().d[0], false);
        if (!FP::defined(x))
            return;

        painter->save();
        painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
        QBrush brush(gradient.evaluate(z));
        painter->setBrush(brush);
        painter->setPen(QPen(brush, 1));
        symbol.paint(painter, x, y);
        painter->restore();
        return;
    }

    std::vector<Graph2DSimpleDrawPoint<3> > translated;
    translated.reserve(points.size());
    for (const auto &it : points) {
        Graph2DSimpleDrawPoint<3> p;
        p.d[0] = tX.toScreen(it.d[0], false);
        p.d[1] = tY.toScreen(it.d[1], false);
        p.d[2] = tZ.toScreen(it.d[2], true);
        translated.emplace_back(std::move(p));
    }

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);

    int gradientBits = 0;
    auto start = translated.cbegin();
    auto prior = start;
    qreal dashOffset = 0;
    std::vector<Graph2DSimpleDrawPoint<3> > paintIsolated;
    auto pointItter = points.cbegin() + 1;
    for (auto it = start + 1, end = translated.cend(); it != end; ++it, ++pointItter) {
        if (!pointsContiguous((pointItter - 1)->end, pointItter->start, continuity)) {
            if (start != prior) {
                paintGradientLines<Graph2DSimpleDrawPoint<3>, 0, 1, 2>(painter, lineWidth, style,
                                                                       gradient, start, prior,
                                                                       gradientBits, dashOffset,
                                                                       skipUndefined);
            } else if (symbol.getType() != TraceSymbol::Unselected) {
                paintIsolated.push_back(*prior);
            }
            start = it;
            prior = it;
            gradientBits = 0;
            continue;
        }

        if (!FP::defined(it->d[1]) || !FP::defined(it->d[2]) || !FP::defined(it->d[2])) {
            if (skipUndefined)
                continue;
            if (start == prior && symbol.getType() != TraceSymbol::Unselected) {
                paintIsolated.push_back(*prior);
            }
        }

        handleGradientBreakdown<Graph2DSimpleDrawPoint<3>, 0, 1, 2>(painter, lineWidth, style,
                                                                    gradient, start, it, prior,
                                                                    gradientBits, dashOffset);
        prior = it;
    }
    if (start != prior) {
        paintGradientLines<Graph2DSimpleDrawPoint<3>, 0, 1, 2>(painter, lineWidth, style, gradient,
                                                               start, prior, gradientBits,
                                                               dashOffset, skipUndefined);
    } else if (symbol.getType() != TraceSymbol::Unselected) {
        paintIsolated.emplace_back(translated.back());
    }

    if (!paintIsolated.empty()) {
        for (const auto &it : paintIsolated) {
            double y = it.d[1];
            if (!FP::defined(y))
                continue;
            double z = it.d[2];
            if (!FP::defined(z))
                continue;
            double x = it.d[0];
            if (!FP::defined(x))
                continue;

            QBrush brush(gradient.evaluate(z));
            painter->setBrush(brush);
            painter->setPen(QPen(brush, 1));
            symbol.paint(painter, x, y);
        }
    }

    painter->restore();
}

void GraphPainter2DLinesGradient::updateMouseover(const QPointF &point,
                                                  std::shared_ptr<
                                                          Graph2DMouseoverComponent> &output)
{
    if (points.empty())
        return;
    TracePoint<3> hit;
    double distance;
    findNearestPoint(point, points, tX, tY, hit, distance);
    if (!FP::defined(distance))
        return;
    if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(output.get())) {
        QPointF otherDelta(trace->getCenter() - point);
        if (sqrt(otherDelta.x() * otherDelta.x() + otherDelta.y() * otherDelta.y()) < distance)
            return;
    }

    output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverComponentTraceStandard(
            QPointF(tX.toScreen(hit.d[0], true), tY.toScreen(hit.d[1], true)), tracePtr, hit));
}

void GraphPainter2DLinesGradient::updateMouseClick(const QPointF &,
                                                   std::shared_ptr<Graph2DMouseoverComponent> &)
{ }

/**
 * Create a new point painter.
 * 
 * @param setPoints         the points to paint (in X,Y,Z(color, ignored) order)
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param symbol            the symbol to paint
 * @param setColor          the color to paint with
 * @param setBaselineX      the X axis baseline
 * @param setBaselineY      the Y axis baseline
 * @param priority          the draw sort priority
 */
GraphPainter2DPoints::GraphPainter2DPoints(std::vector<TracePoint<3>> setPoints,
                                           const AxisTransformer &setX,
                                           const AxisTransformer &setY,
                                           const TraceSymbol &setSymbol,
                                           const QColor &setColor,
                                           quintptr setTracePtr,
                                           int priority) : Graph2DDrawPrimitive(priority),
                                                           points(std::move(setPoints)),
                                                           tX(setX),
                                                           tY(setY),
                                                           symbol(setSymbol),
                                                           color(setColor),
                                                           tracePtr(setTracePtr)
{ }

GraphPainter2DPoints::~GraphPainter2DPoints() = default;

void GraphPainter2DPoints::paint(QPainter *painter)
{
    if (points.empty())
        return;
    if (symbol.getType() == TraceSymbol::Unselected)
        return;

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);

    QBrush brush(color);
    painter->setBrush(brush);
    painter->setPen(QPen(brush, 1));
    for (const auto &it : points) {
        double y = tY.toScreen(it.d[1], false);
        if (!FP::defined(y))
            continue;
        double x = tX.toScreen(it.d[0], false);
        if (!FP::defined(x))
            continue;
        symbol.paint(painter, x, y);
    }

    painter->restore();
}

void GraphPainter2DPoints::updateMouseover(const QPointF &point,
                                           std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    if (points.empty())
        return;
    if (symbol.getType() == TraceSymbol::Unselected)
        return;
    TracePoint<3> hit;
    double distance;
    findNearestPoint(point, points, tX, tY, hit, distance);
    if (!FP::defined(distance))
        return;
    if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(output.get())) {
        QPointF otherDelta(trace->getCenter() - point);
        if (sqrt(otherDelta.x() * otherDelta.x() + otherDelta.y() * otherDelta.y()) < distance)
            return;
    }

    output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverComponentTraceStandard(
            QPointF(tX.toScreen(hit.d[0], true), tY.toScreen(hit.d[1], true)), tracePtr, hit));
}

void GraphPainter2DPoints::updateMouseClick(const QPointF &,
                                            std::shared_ptr<Graph2DMouseoverComponent> &)
{ }

/**
 * Create a new point painter.
 * 
 * @param setPoints         the points to paint (in X,Y,Z(color) order)
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setZ              the Z (color) transformer (0-1)
 * @param symbol            the symbol to paint
 * @param setGradient       the color gradient paint with
 * @param priority          the draw sort priority
 */
GraphPainter2DPointsGradient::GraphPainter2DPointsGradient(std::vector<TracePoint<3> > setPoints,
                                                           const AxisTransformer &setX,
                                                           const AxisTransformer &setY,
                                                           const AxisTransformer &setZ,
                                                           const TraceSymbol &setSymbol,
                                                           const TraceGradient &setGradient,
                                                           quintptr setTracePtr,
                                                           int priority) : Graph2DDrawPrimitive(
        priority),
                                                                           points(std::move(
                                                                                   setPoints)),
                                                                           tX(setX),
                                                                           tY(setY),
                                                                           tZ(setZ),
                                                                           symbol(setSymbol),
                                                                           gradient(setGradient),
                                                                           tracePtr(setTracePtr)
{ }

GraphPainter2DPointsGradient::~GraphPainter2DPointsGradient()
{ }

void GraphPainter2DPointsGradient::paint(QPainter *painter)
{
    if (points.empty())
        return;
    if (symbol.getType() == TraceSymbol::Unselected)
        return;

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);

    for (const auto &it : points) {
        double y = tY.toScreen(it.d[1], false);
        if (!FP::defined(y))
            continue;
        double z = tZ.toScreen(it.d[2], true);
        if (!FP::defined(z))
            continue;
        double x = tX.toScreen(it.d[0], false);
        if (!FP::defined(x))
            continue;

        QBrush brush(gradient.evaluate(z));
        painter->setBrush(brush);
        painter->setPen(QPen(brush, 1));
        symbol.paint(painter, x, y);
    }

    painter->restore();
}

void GraphPainter2DPointsGradient::updateMouseover(const QPointF &point,
                                                   std::shared_ptr<
                                                           Graph2DMouseoverComponent> &output)
{
    if (points.empty())
        return;
    if (symbol.getType() == TraceSymbol::Unselected)
        return;
    TracePoint<3> hit;
    double distance;
    findNearestPoint(point, points, tX, tY, hit, distance);
    if (!FP::defined(distance))
        return;
    if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(output.get())) {
        QPointF otherDelta(trace->getCenter() - point);
        if (sqrt(otherDelta.x() * otherDelta.x() + otherDelta.y() * otherDelta.y()) < distance)
            return;
    }

    output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverComponentTraceStandard(
            QPointF(tX.toScreen(hit.d[0], true), tY.toScreen(hit.d[1], true)), tracePtr, hit));
}

void GraphPainter2DPointsGradient::updateMouseClick(const QPointF &,
                                                    std::shared_ptr<Graph2DMouseoverComponent> &)
{ }

#define MAXIMUM_FIT_DEPTH   13
#define MINIMUM_FIT_DEPTH   9

template<std::size_t N>
static std::size_t evaluateFitPoints(std::vector<Graph2DSimpleDrawPoint<N>> &target,
                                     std::size_t index,
                                     int usedDepth,
                                     double scale,
                                     Model *model,
                                     AxisTransformer **tr,
                                     bool clipLast = false)
{
    if (usedDepth >= MAXIMUM_FIT_DEPTH)
        return 0;

    Graph2DSimpleDrawPoint<N> add;
    add.d[0] = (target[index].d[0] + target[index + 1].d[0]) * 0.5;
    if (N == 4) {
        add.d[1] = model->oneToOne(add.d[0] * scale);
        add.d[2] = tr[0]->toScreen(add.d[0], false);
        add.d[3] = tr[1]->toScreen(add.d[1], clipLast);
    } else if (N == 5) {
        add.d[2] = add.d[1] = add.d[0] * scale;
        model->applyIO(add.d[1], add.d[2]);
        add.d[3] = tr[0]->toScreen(add.d[1], false);
        add.d[4] = tr[1]->toScreen(add.d[2], clipLast);
    } else if (N == 6) {
        add.d[2] = add.d[1] = add.d[0] * scale;
        model->applyIO(add.d[1], add.d[2]);
        add.d[3] = tr[0]->toScreen(add.d[0], false);
        add.d[4] = tr[1]->toScreen(add.d[1], false);
        add.d[5] = tr[2]->toScreen(add.d[2], clipLast);
    } else if (N == 7) {
        add.d[3] = add.d[2] = add.d[1] = add.d[0] * scale;
        model->applyIO(add.d[1], add.d[2], add.d[3]);
        add.d[4] = tr[0]->toScreen(add.d[1], false);
        add.d[5] = tr[1]->toScreen(add.d[2], false);
        add.d[6] = tr[2]->toScreen(add.d[3], clipLast);
    } else if (N == 8) {
        add.d[3] = add.d[2] = add.d[1] = add.d[0] * scale;
        model->applyIO(add.d[1], add.d[2], add.d[3]);
        add.d[4] = tr[0]->toScreen(add.d[0], false);
        add.d[5] = tr[1]->toScreen(add.d[1], false);
        add.d[6] = tr[2]->toScreen(add.d[2], false);
        add.d[7] = tr[3]->toScreen(add.d[3], clipLast);
    } else if ((N & 1) == 0) {
        Q_ASSERT(N > 2);
        QVector<double> values;
        values.reserve(N / 2);
        for (int i = 1; i < N / 2; i++) {
            values.push_back(add.d[0] * scale);
        }
        model->applyIO(values);
        add.d[N / 2] = tr[0]->toScreen(add.d[0], false);
        for (int i = 1; i < N / 2; i++) {
            if (i > values.size()) {
                add.d[i] = FP::undefined();
            } else {
                add.d[i] = values[i - 1];
                add.d[i + N / 2] = tr[i]->toScreen(add.d[i], clipLast && (i == N / 2 - 1));
            }
        }
    } else {
        QVector<double> values;
        values.reserve(N / 2);
        for (int i = 1; i <= N / 2; i++) {
            values.push_back(add.d[0] * scale);
        }
        model->applyIO(values);
        for (int i = 1; i <= N / 2; i++) {
            if (i > values.size()) {
                add.d[i] = FP::undefined();
            } else {
                add.d[i] = values[i - 1];
                add.d[i + N / 2 + 1] = tr[i]->toScreen(add.d[i], clipLast && (i == N / 2));
            }
        }
    }

    if (usedDepth >= MINIMUM_FIT_DEPTH) {
        AxisTransformer **trClip = tr;
        bool isSmallDelta = fabs(target[index].d[0] - target[index + 1].d[0]) <
                (fabs(trClip[0]->getRealMax() - trClip[0]->getRealMin()) / 256.0);
        double distance1 = 0.0;
        double distance2 = 0.0;
        double distance3 = 0.0;
        for (std::size_t i = (N / 2) + (N & 1); i < N; i++, trClip++) {
            double a = target[index].d[i];
            if (!FP::defined(a))
                continue;
            double b = target[index + 1].d[i];
            if (!FP::defined(b))
                continue;
            double c = add.d[i];
            if (!FP::defined(c))
                continue;
            if (isSmallDelta) {
                if ((*trClip)->screenClipped(a) &&
                        (*trClip)->screenClipped(b) &&
                        (*trClip)->screenClipped(c)) {
                    return 0;
                }
            }
            double d = a - b;
            distance1 += d * d;
            d = a - c;
            distance2 += d * d;
            d = b - c;
            distance3 += d * d;
        }
        if (distance1 <= 1.0 && distance2 <= 1.0 && distance3 <= 1.0)
            return 0;
    }

    target.insert(target.begin() + index + 1, add);
    std::size_t nAdd1 =
            evaluateFitPoints < N > (target, index, usedDepth + 1, scale, model, tr, clipLast);
    index += nAdd1 + 1;
    std::size_t nAdd2 =
            evaluateFitPoints < N > (target, index, usedDepth + 1, scale, model, tr, clipLast);
    return 1 + nAdd1 + nAdd2;
}

template<std::size_t N, std::size_t idxX, std::size_t idxY>
static void drawLineFit(QPainter *painter,
                        const std::vector<Graph2DSimpleDrawPoint<N> > &points,
                        qreal lineWidth,
                        Qt::PenStyle style,
                        const QColor &color)
{
    QPainterPath path;
    std::size_t nValid = 0;
    for (auto it = points.cbegin(), end = points.cend(); it != end; ++it) {
        double y = it->d[idxY];
        if (!FP::defined(y)) {
            if (nValid == 1) {
                path.lineTo((it - 1)->d[idxX], (it - 1)->d[idxY]);
            }
            nValid = 0;
            continue;
        }
        double x = it->d[idxX];
        if (!FP::defined(x)) {
            if (nValid == 1) {
                path.lineTo((it - 1)->d[idxX], (it - 1)->d[idxY]);
            }
            nValid = 0;
            continue;
        }

        if (nValid == 0) {
            path.moveTo(x, y);
        } else {
            path.lineTo(x, y);
        }
        nValid++;
    }
    if (nValid == 1) {
        path.lineTo(points.back().d[idxX], points.back().d[idxY]);
    }

    if (path.isEmpty())
        return;

    QBrush brush(color);
    painter->setBrush(brush);
    painter->strokePath(path, QPen(brush, lineWidth, style));
}

template<std::size_t N, std::size_t idxX, std::size_t idxY>
static void findNearestTransformed(const QPointF &target,
                                   const std::vector<Graph2DSimpleDrawPoint<N> > &points,
                                   AxisTransformer &tX,
                                   AxisTransformer &tY,
                                   Graph2DSimpleDrawPoint<N> &result,
                                   double &distance)
{
    distance = FP::undefined();
    result.clear();
    for (const auto &it : points) {
        if (!FP::defined(it.d[idxY]) || !FP::defined(it.d[idxX]))
            continue;
        if (tY.screenClipped(it.d[idxY]) || tX.screenClipped(it.d[idxX]))
            continue;
        double d = it.d[idxX] - target.x();
        double targetDistance = d * d;
        d = it.d[idxY] - target.y();
        targetDistance += d * d;
        if (FP::defined(distance) && distance < targetDistance)
            continue;
        distance = targetDistance;
        result = it;
    }
    if (FP::defined(distance)) {
        distance = sqrt(distance);
        if (distance > maximumPointNearDistance)
            distance = FP::undefined();
    }
}

/**
 * Create a new fit drawer.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setLineWidth      the line width
 * @param setStyle          the line style
 * @param setColor          the color to paint with
 * @param setScale          the scale factor applied to the independent variable before the model
 * @param priority          the draw sort priority
 */
GraphPainter2DFitX::GraphPainter2DFitX(std::shared_ptr<Model> setModel,
                                       const AxisTransformer &setX,
                                       const AxisTransformer &setY,
                                       qreal setLineWidth,
                                       Qt::PenStyle setStyle,
                                       const QColor &setColor,
                                       double setScale,
                                       quintptr setTracePtr,
                                       int priority) : Graph2DDrawPrimitive(priority),
                                                       model(std::move(setModel)),
                                                       tX(setX),
                                                       tY(setY),
                                                       lineWidth(setLineWidth),
                                                       style(setStyle),
                                                       color(setColor),
                                                       scale(setScale),
                                                       tracePtr(setTracePtr)
{
    Q_ASSERT(model);
    Q_ASSERT(scale != 0.0);
}

GraphPainter2DFitX::~GraphPainter2DFitX() = default;

std::vector<Graph2DSimpleDrawPoint<4> > GraphPainter2DFitX::generatePoints()
{
    std::vector<Graph2DSimpleDrawPoint<4> > points;

    Graph2DSimpleDrawPoint<4> add;
    add.d[0] = tX.getRealMin();
    add.d[1] = model->oneToOne(add.d[0] * scale);
    add.d[2] = tX.toScreen(add.d[0], false);
    add.d[3] = tY.toScreen(add.d[1], false);
    points.emplace_back(add);

    add.d[0] = tX.getRealMax();
    add.d[1] = model->oneToOne(add.d[0] * scale);
    add.d[2] = tX.toScreen(add.d[0], false);
    add.d[3] = tY.toScreen(add.d[1], false);
    points.emplace_back(add);

    AxisTransformer *tr[2];
    tr[0] = &tX;
    tr[1] = &tY;
    evaluateFitPoints<4>(points, 0, 0, scale, model.get(), tr);

    return points;
}

void GraphPainter2DFitX::paint(QPainter *painter)
{
    auto points = generatePoints();

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    drawLineFit<4, 2, 3>(painter, points, lineWidth, style, color);
    painter->restore();
}

void GraphPainter2DFitX::updateMouseover(const QPointF &point,
                                         std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    auto points = generatePoints();

    Graph2DSimpleDrawPoint<4> hit;
    double distance;
    findNearestTransformed<4, 2, 3>(point, points, tX, tY, hit, distance);
    if (!FP::defined(distance))
        return;
    if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(output.get())) {
        QPointF otherDelta(trace->getCenter() - point);
        if (sqrt(otherDelta.x() * otherDelta.x() + otherDelta.y() * otherDelta.y()) < distance)
            return;
    }

    output = std::shared_ptr<Graph2DMouseoverComponent>(
            new Graph2DMouseoverComponentTraceFit(QPointF(hit.d[2], hit.d[3]), tracePtr, hit.d[0],
                                                  hit.d[1]));
}

void GraphPainter2DFitX::updateMouseClick(const QPointF &,
                                          std::shared_ptr<Graph2DMouseoverComponent> &)
{ }

/**
 * Create a new fit drawer.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setLineWidth      the line width
 * @param setStyle          the line style
 * @param setColor          the color to paint with
 * @param setScale          the scale factor applied to the independent variable before the model
 * @param priority          the draw sort priority
 */
GraphPainter2DFitY::GraphPainter2DFitY(std::shared_ptr<Model> setModel,
                                       const AxisTransformer &setX,
                                       const AxisTransformer &setY,
                                       qreal setLineWidth,
                                       Qt::PenStyle setStyle,
                                       const QColor &setColor,
                                       double setscale,
                                       quintptr setTracePtr,
                                       int priority) : Graph2DDrawPrimitive(priority),
                                                       model(std::move(setModel)),
                                                       tX(setX),
                                                       tY(setY),
                                                       lineWidth(setLineWidth),
                                                       style(setStyle),
                                                       color(setColor),
                                                       scale(setscale),
                                                       tracePtr(setTracePtr)
{
    Q_ASSERT(model);
    Q_ASSERT(scale != 0.0);
}

GraphPainter2DFitY::~GraphPainter2DFitY() = default;

std::vector<Graph2DSimpleDrawPoint<4> > GraphPainter2DFitY::generatePoints()
{
    std::vector<Graph2DSimpleDrawPoint<4> > points;

    Graph2DSimpleDrawPoint<4> add;
    add.d[0] = tY.getRealMin();
    add.d[1] = model->oneToOne(add.d[0] * scale);
    add.d[2] = tY.toScreen(add.d[0], false);
    add.d[3] = tX.toScreen(add.d[1], false);
    points.emplace_back(add);

    add.d[0] = tY.getRealMax();
    add.d[1] = model->oneToOne(add.d[0] * scale);
    add.d[2] = tY.toScreen(add.d[0], false);
    add.d[3] = tX.toScreen(add.d[1], false);
    points.emplace_back(add);

    AxisTransformer *tr[2];
    tr[0] = &tY;
    tr[1] = &tX;
    evaluateFitPoints<4>(points, 0, 0, scale, model.get(), tr);

    return points;
}

void GraphPainter2DFitY::paint(QPainter *painter)
{
    auto points = generatePoints();

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    drawLineFit<4, 3, 2>(painter, points, lineWidth, style, color);
    painter->restore();
}

void GraphPainter2DFitY::updateMouseover(const QPointF &point,
                                         std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    auto points = generatePoints();

    Graph2DSimpleDrawPoint<4> hit;
    double distance;
    findNearestTransformed<4, 3, 2>(point, points, tX, tY, hit, distance);
    if (!FP::defined(distance))
        return;
    if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(output.get())) {
        QPointF otherDelta(trace->getCenter() - point);
        if (sqrt(otherDelta.x() * otherDelta.x() + otherDelta.y() * otherDelta.y()) < distance)
            return;
    }

    output = std::shared_ptr<Graph2DMouseoverComponent>(
            new Graph2DMouseoverComponentTraceFit(QPointF(hit.d[3], hit.d[2]), tracePtr, hit.d[1],
                                                  hit.d[0]));
}

void GraphPainter2DFitY::updateMouseClick(const QPointF &,
                                          std::shared_ptr<Graph2DMouseoverComponent> &)
{ }

/**
 * Create a new fit drawer.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setLineWidth      the line width
 * @param setStyle          the line style
 * @param setColor          the color to paint with
 * @param setMinI           the minimum I value
 * @param setMaxI           the maximum I value
 * @param priority          the draw sort priority
 */
GraphPainter2DFitI::GraphPainter2DFitI(std::shared_ptr<Model> setModel,
                                       const AxisTransformer &setX,
                                       const AxisTransformer &setY,
                                       qreal setLineWidth,
                                       Qt::PenStyle setStyle,
                                       const QColor &setColor,
                                       double setMinI,
                                       double setMaxI,
                                       quintptr setTracePtr,
                                       int priority) : Graph2DDrawPrimitive(priority),
                                                       model(std::move(setModel)),
                                                       tX(setX),
                                                       tY(setY),
                                                       lineWidth(setLineWidth),
                                                       style(setStyle),
                                                       color(setColor),
                                                       minI(setMinI),
                                                       maxI(setMaxI),
                                                       tracePtr(setTracePtr)
{
    Q_ASSERT(model);
}

GraphPainter2DFitI::~GraphPainter2DFitI() = default;

std::vector<Graph2DSimpleDrawPoint<5> > GraphPainter2DFitI::generatePoints()
{
    std::vector<Graph2DSimpleDrawPoint<5> > points;

    if (FP::defined(minI)) {
        Graph2DSimpleDrawPoint<5> add;
        add.d[0] = minI;
        add.d[1] = minI;
        add.d[2] = minI;
        model->applyIO(add.d[1], add.d[2]);
        add.d[3] = tX.toScreen(add.d[1], false);
        add.d[4] = tY.toScreen(add.d[2], false);
        points.push_back(add);
    }

    if (FP::defined(maxI) && (!FP::defined(minI) || minI != maxI)) {
        Graph2DSimpleDrawPoint<5> add;
        add.d[0] = maxI;
        add.d[1] = maxI;
        add.d[2] = maxI;
        model->applyIO(add.d[1], add.d[2]);
        add.d[3] = tX.toScreen(add.d[1], false);
        add.d[4] = tY.toScreen(add.d[2], false);
        points.push_back(add);
    }

    if (points.size() == 2) {
        AxisTransformer *tr[2];
        tr[0] = &tX;
        tr[1] = &tY;
        evaluateFitPoints<5>(points, 0, 0, 1.0, model.get(), tr);
    }

    return points;
}

void GraphPainter2DFitI::paint(QPainter *painter)
{
    auto points = generatePoints();
    if (points.empty())
        return;

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    drawLineFit<5, 3, 4>(painter, points, lineWidth, style, color);
    painter->restore();
}

void GraphPainter2DFitI::updateMouseover(const QPointF &point,
                                         std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    auto points = generatePoints();
    if (points.empty())
        return;

    double distance;
    Graph2DSimpleDrawPoint<5> hit;
    findNearestTransformed<5, 3, 4>(point, points, tX, tY, hit, distance);
    if (!FP::defined(distance))
        return;
    if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(output.get())) {
        QPointF otherDelta(trace->getCenter() - point);
        if (sqrt(otherDelta.x() * otherDelta.x() + otherDelta.y() * otherDelta.y()) < distance)
            return;
    }

    output = std::shared_ptr<Graph2DMouseoverComponent>(
            new Graph2DMouseoverComponentTraceFit(QPointF(hit.d[3], hit.d[4]), tracePtr, hit.d[1],
                                                  hit.d[2], FP::undefined(), hit.d[0]));
}

void GraphPainter2DFitI::updateMouseClick(const QPointF &,
                                          std::shared_ptr<Graph2DMouseoverComponent> &)
{ }

template<std::size_t N, std::size_t idxX, std::size_t idxY, std::size_t idxZ>
static void drawLineFitGradient(QPainter *painter,
                                const std::vector<Graph2DSimpleDrawPoint<N> > &points,
                                qreal lineWidth,
                                Qt::PenStyle style,
                                TraceGradient &gradient)
{
    int gradientBits = 0;
    auto start = points.cbegin();
    qreal dashOffset = 0;
    for (auto it = start + 1, end = points.cend(); it != end; ++it) {
        if (!FP::defined(it->d[idxY]) || !FP::defined(it->d[idxZ]) || !FP::defined(it->d[idxX])) {
            if (start == it - 1 &&
                    FP::defined(start->d[idxY]) &&
                    FP::defined(start->d[idxZ]) &&
                    FP::defined(start->d[idxX])) {
                QBrush brush(gradient.evaluate(it->d[idxZ]));
                painter->setBrush(brush);
                painter->setPen(QPen(brush, 1));
                QPointF p(start->d[idxX], start->d[idxY]);
                painter->drawLine(p, p);
            }
        }

        handleGradientBreakdown<Graph2DSimpleDrawPoint<N>, idxX, idxY, idxZ>(painter, lineWidth,
                                                                             style, gradient, start,
                                                                             it, it - 1,
                                                                             gradientBits,
                                                                             dashOffset);
    }
    if (start != points.cend() - 1) {
        paintGradientLines<Graph2DSimpleDrawPoint<N>, idxX, idxY, idxZ>(painter, lineWidth, style,
                                                                        gradient, start,
                                                                        points.cend() - 1,
                                                                        gradientBits, dashOffset);
    } else if (FP::defined(start->d[idxY]) &&
            FP::defined(start->d[idxZ]) &&
            FP::defined(start->d[idxX])) {
        QBrush brush(gradient.evaluate(start->d[idxZ]));
        painter->setBrush(brush);
        painter->setPen(QPen(brush, 1));
        QPointF p(start->d[idxX], start->d[idxY]);
        painter->drawLine(p, p);
    }
}


/**
 * Create a new fit drawer.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setZ              the Z (color, 0-1) transformer
 * @param setLineWidth      the line width
 * @param setStyle          the line style
 * @param setGradient       the gradient to paint with
 * @param setScale          the scale factor applied to the independent variable before the model
 * @param priority          the draw sort priority
 */
GraphPainter2DFitXGradient::GraphPainter2DFitXGradient(std::shared_ptr<Model> setModel,
                                                       const AxisTransformer &setX,
                                                       const AxisTransformer &setY,
                                                       const AxisTransformer &setZ,
                                                       qreal setLineWidth,
                                                       Qt::PenStyle setStyle,
                                                       const TraceGradient &setGradient,
                                                       double setScale,
                                                       quintptr setTracePtr,
                                                       int priority) : Graph2DDrawPrimitive(
        priority),
                                                                       model(std::move(setModel)),
                                                                       tX(setX),
                                                                       tY(setY),
                                                                       tZ(setZ),
                                                                       lineWidth(setLineWidth),
                                                                       style(setStyle),
                                                                       gradient(setGradient),
                                                                       scale(setScale),
                                                                       tracePtr(setTracePtr)
{
    Q_ASSERT(model);
    Q_ASSERT(scale != 0.0);
}

GraphPainter2DFitXGradient::~GraphPainter2DFitXGradient() = default;

std::vector<Graph2DSimpleDrawPoint<6> > GraphPainter2DFitXGradient::generatePoints()
{
    std::vector<Graph2DSimpleDrawPoint<6> > points;

    Graph2DSimpleDrawPoint<6> add;
    add.d[2] = add.d[1] = add.d[0] = tX.getRealMin();
    model->applyIO(add.d[1], add.d[2]);
    add.d[3] = tX.toScreen(add.d[0], false);
    add.d[4] = tY.toScreen(add.d[1], false);
    add.d[5] = tZ.toScreen(add.d[2], true);
    points.push_back(add);

    add.d[2] = add.d[1] = add.d[0] = tX.getRealMax();
    model->applyIO(add.d[1], add.d[2]);
    add.d[3] = tX.toScreen(add.d[0], false);
    add.d[4] = tY.toScreen(add.d[1], false);
    add.d[5] = tZ.toScreen(add.d[2], true);
    points.push_back(add);

    AxisTransformer *tr[3];
    tr[0] = &tX;
    tr[1] = &tY;
    tr[2] = &tZ;
    evaluateFitPoints<6>(points, 0, 0, scale, model.get(), tr, true);

    return points;
}

void GraphPainter2DFitXGradient::paint(QPainter *painter)
{
    auto points = generatePoints();

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    drawLineFitGradient<6, 3, 4, 5>(painter, points, lineWidth, style, gradient);
    painter->restore();
}

void GraphPainter2DFitXGradient::updateMouseover(const QPointF &point,
                                                 std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    auto points = generatePoints();

    Graph2DSimpleDrawPoint<6> hit;
    double distance;
    findNearestTransformed<6, 3, 4>(point, points, tX, tY, hit, distance);
    if (!FP::defined(distance))
        return;
    if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(output.get())) {
        QPointF otherDelta(trace->getCenter() - point);
        if (sqrt(otherDelta.x() * otherDelta.x() + otherDelta.y() * otherDelta.y()) < distance)
            return;
    }

    output = std::shared_ptr<Graph2DMouseoverComponent>(
            new Graph2DMouseoverComponentTraceFit(QPointF(hit.d[3], hit.d[4]), tracePtr, hit.d[0],
                                                  hit.d[1], hit.d[2]));
}

void GraphPainter2DFitXGradient::updateMouseClick(const QPointF &,
                                                  std::shared_ptr<Graph2DMouseoverComponent> &)
{ }

/**
 * Create a new fit drawer.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setZ              the Z (color, 0-1) transformer
 * @param setLineWidth      the line width
 * @param setStyle          the line style
 * @param setGradient       the gradient to paint with
 * @param setScale          the scale factor applied to the independent variable before the model
 * @param priority          the draw sort priority
 */
GraphPainter2DFitYGradient::GraphPainter2DFitYGradient(std::shared_ptr<Model> setModel,
                                                       const AxisTransformer &setX,
                                                       const AxisTransformer &setY,
                                                       const AxisTransformer &setZ,
                                                       qreal setLineWidth,
                                                       Qt::PenStyle setStyle,
                                                       const TraceGradient &setGradient,
                                                       double setScale,
                                                       quintptr setTracePtr,
                                                       int priority) : Graph2DDrawPrimitive(
        priority),
                                                                       model(std::move(setModel)),
                                                                       tX(setX),
                                                                       tY(setY),
                                                                       tZ(setZ),
                                                                       lineWidth(setLineWidth),
                                                                       style(setStyle),
                                                                       gradient(setGradient),
                                                                       scale(setScale),
                                                                       tracePtr(setTracePtr)
{
    Q_ASSERT(model);
    Q_ASSERT(scale != 0.0);
}

GraphPainter2DFitYGradient::~GraphPainter2DFitYGradient() = default;

std::vector<Graph2DSimpleDrawPoint<6> > GraphPainter2DFitYGradient::generatePoints()
{
    std::vector<Graph2DSimpleDrawPoint<6> > points;

    Graph2DSimpleDrawPoint<6> add;
    add.d[2] = add.d[1] = add.d[0] = tY.getRealMin();
    model->applyIO(add.d[1], add.d[2]);
    add.d[3] = tY.toScreen(add.d[0], false);
    add.d[4] = tX.toScreen(add.d[1], false);
    add.d[5] = tZ.toScreen(add.d[2], true);
    points.push_back(add);

    add.d[2] = add.d[1] = add.d[0] = tY.getRealMax();
    model->applyIO(add.d[1], add.d[2]);
    add.d[3] = tY.toScreen(add.d[0], false);
    add.d[4] = tX.toScreen(add.d[1], false);
    add.d[5] = tZ.toScreen(add.d[2], true);
    points.push_back(add);

    AxisTransformer *tr[3];
    tr[0] = &tY;
    tr[1] = &tX;
    tr[2] = &tZ;
    evaluateFitPoints<6>(points, 0, 0, scale, model.get(), tr, true);

    return points;
}

void GraphPainter2DFitYGradient::paint(QPainter *painter)
{
    auto points = generatePoints();

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    drawLineFitGradient<6, 4, 3, 5>(painter, points, lineWidth, style, gradient);
    painter->restore();
}

void GraphPainter2DFitYGradient::updateMouseover(const QPointF &point,
                                                 std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    auto points = generatePoints();

    Graph2DSimpleDrawPoint<6> hit;
    double distance;
    findNearestTransformed<6, 4, 3>(point, points, tX, tY, hit, distance);
    if (!FP::defined(distance))
        return;
    if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(output.get())) {
        QPointF otherDelta(trace->getCenter() - point);
        if (sqrt(otherDelta.x() * otherDelta.x() + otherDelta.y() * otherDelta.y()) < distance)
            return;
    }

    output = std::shared_ptr<Graph2DMouseoverComponent>(
            new Graph2DMouseoverComponentTraceFit(QPointF(hit.d[4], hit.d[3]), tracePtr, hit.d[1],
                                                  hit.d[0], hit.d[2]));
}

void GraphPainter2DFitYGradient::updateMouseClick(const QPointF &,
                                                  std::shared_ptr<Graph2DMouseoverComponent> &)
{ }

/**
 * Create a new fit drawer.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setZ              the Z (color, 0-1) transformer
 * @param setLineWidth      the line width
 * @param setStyle          the line style
 * @param setGradient       the gradient to paint with
 * @param setMinI           the minimum I value
 * @param setMaxI           the maximum I value
 * @param priority          the draw sort priority
 */
GraphPainter2DFitIGradient::GraphPainter2DFitIGradient(std::shared_ptr<Model> setModel,
                                                       const AxisTransformer &setX,
                                                       const AxisTransformer &setY,
                                                       const AxisTransformer &setZ,
                                                       qreal setLineWidth,
                                                       Qt::PenStyle setStyle,
                                                       const TraceGradient &setGradient,
                                                       double setMinI,
                                                       double setMaxI,
                                                       quintptr setTracePtr,
                                                       int priority) : Graph2DDrawPrimitive(
        priority),
                                                                       model(std::move(setModel)),
                                                                       tX(setX),
                                                                       tY(setY),
                                                                       tZ(setZ),
                                                                       lineWidth(setLineWidth),
                                                                       style(setStyle),
                                                                       gradient(setGradient),
                                                                       minI(setMinI),
                                                                       maxI(setMaxI),
                                                                       tracePtr(setTracePtr)
{
    Q_ASSERT(model);
}

GraphPainter2DFitIGradient::~GraphPainter2DFitIGradient() = default;

std::vector<Graph2DSimpleDrawPoint<7> > GraphPainter2DFitIGradient::generatePoints()
{
    std::vector<Graph2DSimpleDrawPoint<7> > points;

    if (FP::defined(minI)) {
        Graph2DSimpleDrawPoint<7> add;
        add.d[0] = minI;
        add.d[1] = minI;
        add.d[2] = minI;
        add.d[3] = minI;
        model->applyIO(add.d[1], add.d[2], add.d[3]);
        add.d[4] = tX.toScreen(add.d[1], false);
        add.d[5] = tY.toScreen(add.d[2], false);
        add.d[6] = tZ.toScreen(add.d[3], true);
        points.push_back(add);
    }

    if (FP::defined(maxI) && (!FP::defined(minI) || minI != maxI)) {
        Graph2DSimpleDrawPoint<7> add;
        add.d[0] = maxI;
        add.d[1] = maxI;
        add.d[2] = maxI;
        add.d[3] = maxI;
        model->applyIO(add.d[1], add.d[2], add.d[3]);
        add.d[4] = tX.toScreen(add.d[1], false);
        add.d[5] = tY.toScreen(add.d[2], false);
        add.d[6] = tZ.toScreen(add.d[3], true);
        points.push_back(add);
    }

    if (points.size() == 2) {
        AxisTransformer *tr[3];
        tr[0] = &tX;
        tr[1] = &tY;
        tr[2] = &tZ;
        evaluateFitPoints<7>(points, 0, 0, 1.0, model.get(), tr, true);
    }

    return points;
}

void GraphPainter2DFitIGradient::paint(QPainter *painter)
{
    auto points = generatePoints();
    if (points.empty())
        return;

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    drawLineFitGradient<7, 4, 5, 6>(painter, points, lineWidth, style, gradient);
    painter->restore();
}

void GraphPainter2DFitIGradient::updateMouseover(const QPointF &point,
                                                 std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    auto points = generatePoints();
    if (points.empty())
        return;

    double distance;
    Graph2DSimpleDrawPoint<7> hit;
    findNearestTransformed<7, 4, 5>(point, points, tX, tY, hit, distance);
    if (!FP::defined(distance))
        return;
    if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(output.get())) {
        QPointF otherDelta(trace->getCenter() - point);
        if (sqrt(otherDelta.x() * otherDelta.x() + otherDelta.y() * otherDelta.y()) < distance)
            return;
    }

    output = std::shared_ptr<Graph2DMouseoverComponent>(
            new Graph2DMouseoverComponentTraceFit(QPointF(hit.d[3], hit.d[4]), tracePtr, hit.d[1],
                                                  hit.d[2], hit.d[3], hit.d[0]));
}

void GraphPainter2DFitIGradient::updateMouseClick(const QPointF &,
                                                  std::shared_ptr<Graph2DMouseoverComponent> &)
{ }

/**
 * Create a new fill drawer.  This takes a color (Z axis) transformer that is
 * already set to the correct input/output ranges, otherwise they will be
 * calculated from the scan.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setZ              the Z (color, 0-1) transformer
 * @param setGradient       the gradient to paint with
 * @param priority          the draw sort priority
 */
GraphPainter2DFillModelScan::GraphPainter2DFillModelScan(std::shared_ptr<Model> setModel,
                                                         const AxisTransformer &setX,
                                                         const AxisTransformer &setY,
                                                         const AxisTransformer &setZ,
                                                         const TraceGradient &setGradient,
                                                         quintptr setTracePtr,
                                                         int priority) : Graph2DDrawPrimitive(
        priority),
                                                                         model(std::move(setModel)),
                                                                         tX(setX),
                                                                         tY(setY),
                                                                         tZ(setZ),
                                                                         gradient(setGradient),
                                                                         tracePtr(setTracePtr),
                                                                         hadZRange(true)
{
    if (!FP::defined(tZ.getRealMin()) || !FP::defined(tZ.getRealMax()))
        hadZRange = false;
}

/**
 * Create a new fill drawer.  This will generate a color transformer that
 * takes the minimum and maximum values found in the scan and generates a
 * gradient value of 0 to 1.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setGradient       the gradient to paint with
 * @param priority          the draw sort priority
 */
GraphPainter2DFillModelScan::GraphPainter2DFillModelScan(std::shared_ptr<Model> setModel,
                                                         const AxisTransformer &setX,
                                                         const AxisTransformer &setY,
                                                         const TraceGradient &setGradient,
                                                         quintptr setTracePtr,
                                                         int priority) : Graph2DDrawPrimitive(
        priority),
                                                                         model(std::move(setModel)),
                                                                         tX(setX),
                                                                         tY(setY),
                                                                         tZ(),
                                                                         gradient(setGradient),
                                                                         tracePtr(setTracePtr),
                                                                         hadZRange(false)
{
    Q_ASSERT(model);
    tZ.setMinExtendAbsolute(0);
    tZ.setMaxExtendAbsolute(0);
}

GraphPainter2DFillModelScan::~GraphPainter2DFillModelScan()
{ }

void GraphPainter2DFillModelScan::paint(QPainter *painter)
{
    int startX = (int) floor(qMin(tX.getScreenMin(), tX.getScreenMax()));
    int endX = (int) ceil(qMax(tX.getScreenMin(), tX.getScreenMax()));
    int width = endX - startX + 1;
    int startY = (int) floor(qMin(tY.getScreenMin(), tY.getScreenMax()));
    int endY = (int) ceil(qMax(tY.getScreenMin(), tY.getScreenMax()));
    int height = endY - startY + 1;

    if (width <= 0 || height <= 0)
        return;

    if (!hadZRange || !FP::defined(tZ.getRealMin()) || !FP::defined(tZ.getRealMax())) {
        std::vector<double> values;
        values.reserve(width * height);

        double min = FP::undefined();
        double max = 0;
        for (int y = startY; y <= endY; y++) {
            double yc = tY.toReal(y);
            if (!FP::defined(yc)) {
                values.insert(values.end(), width, FP::undefined());
                continue;
            }
            for (int x = startX; x <= endX; x++) {
                double xc = tX.toReal(x);
                if (!FP::defined(xc)) {
                    values.emplace_back(FP::undefined());
                    continue;
                }
                double v = model->twoToOne(xc, yc);
                values.emplace_back(v);
                if (!FP::defined(min)) {
                    min = v;
                    max = v;
                } else {
                    if (!FP::defined(v))
                        continue;
                    if (min > v) min = v;
                    if (max < v) max = v;
                }
            }
        }
        if (!FP::defined(min))
            return;
        tZ.setMinExtendAbsolute(0);
        tZ.setMaxExtendAbsolute(0);
        tZ.setReal(min, max);

        if (!FP::defined(tZ.getScreenMin()) || !FP::defined(tZ.getScreenMax())) {
            tZ.setScreen(0, 1);
        }

        QImage img(width, height, QImage::Format_ARGB32_Premultiplied);
        img.fill(0);
        for (int y = 0; y < height; y++) {
            QRgb *data = (QRgb *) img.scanLine(y);
            for (int x = 0; x < width; x++, data++) {
                double v = tZ.toScreen(values[y * width + x], true);
                if (!FP::defined(v))
                    continue;

                QRgb put = gradient.evaluate(v).rgba();
                quint8 alpha = put >> 24;
                if (alpha == 0)
                    continue;
                if (alpha != 0xFF) {
                    quint8 r = (((put >> 16) & 0xFF) * alpha) / 0xFF;
                    quint8 g = (((put >> 8) & 0xFF) * alpha) / 0xFF;
                    quint8 b = ((put & 0xFF) * alpha) / 0xFF;
                    put = (alpha << 24) | (r << 16) | (g << 8) | b;
                }
                *data = put;
            }
        }

        painter->save();
        painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
        painter->drawImage(startX, startY, img);
        painter->restore();

        return;
    }

    if (!FP::defined(tZ.getScreenMin()) || !FP::defined(tZ.getScreenMax())) {
        tZ.setScreen(0, 1);
    }

    QImage img(width, height, QImage::Format_ARGB32_Premultiplied);
    img.fill(0);
    for (int y = 0; y < height; y++) {
        double yc = tY.toReal(y + startY);
        if (!FP::defined(yc))
            continue;
        QRgb *data = (QRgb *) img.scanLine(y);
        for (int x = 0; x < width; x++, data++) {
            double xc = tX.toReal(x + startX);
            if (!FP::defined(xc))
                continue;

            double v = tZ.toScreen(model->twoToOne(xc, yc), true);
            if (!FP::defined(v))
                continue;

            QRgb put = gradient.evaluate(v).rgba();
            quint8 alpha = put >> 24;
            if (alpha == 0)
                continue;
            if (alpha != 0xFF) {
                quint8 r = (((put >> 16) & 0xFF) * alpha) / 0xFF;
                quint8 g = (((put >> 8) & 0xFF) * alpha) / 0xFF;
                quint8 b = ((put & 0xFF) * alpha) / 0xFF;
                put = (alpha << 24) | (r << 16) | (g << 8) | b;
            }
            *data = put;
        }
    }

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    painter->drawImage(startX, startY, img);
    painter->restore();
}

void GraphPainter2DFillModelScan::updateMouseover(const QPointF &point,
                                                  std::shared_ptr<
                                                          Graph2DMouseoverComponent> &output)
{
    if (!hadZRange || !FP::defined(tZ.getRealMin()) || !FP::defined(tZ.getRealMax()))
        return;

    double yc = tY.toReal(point.y());
    if (!FP::defined(yc))
        return;
    double xc = tX.toReal(point.x());
    if (!FP::defined(xc))
        return;
    double z = model->twoToOne(xc, yc);
    if (!FP::defined(z))
        return;

    output = std::shared_ptr<Graph2DMouseoverComponent>(
            new Graph2DMouseoverComponentTraceFillModelScan(point, tracePtr, xc, yc, z));
}

void GraphPainter2DFillModelScan::updateMouseClick(const QPointF &,
                                                   std::shared_ptr<Graph2DMouseoverComponent> &)
{ }


/**
 * Create a new bilinear gridding interpolator.
 * 
 * @param points            the points to draw
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setZ              the Z (color, 0-1) transformer
 * @param setGradient       the gradient to paint with
 * @param priority          the draw sort priority
 */
GraphPainter2DFillBilinearGrid::GraphPainter2DFillBilinearGrid(std::vector<
        TracePoint<3> > setPoints,
                                                               const AxisTransformer &setX,
                                                               const AxisTransformer &setY,
                                                               const AxisTransformer &setZ,
                                                               const TraceGradient &setGradient,
                                                               quintptr setTracePtr,
                                                               int priority) : Graph2DDrawPrimitive(
        priority),
                                                                               points(std::move(
                                                                                       setPoints)),
                                                                               tX(setX),
                                                                               tY(setY),
                                                                               tZ(setZ),
                                                                               gradient(
                                                                                       setGradient),
                                                                               tracePtr(setTracePtr)
{ }

GraphPainter2DFillBilinearGrid::~GraphPainter2DFillBilinearGrid() = default;

std::vector<Graph2DSimpleDrawPoint<3>> GraphPainter2DFillBilinearGrid::gridPoints(int &pitch)
{
    QVector<double> xPoints;
    QVector<double> yPoints;
    xPoints.reserve(points.size());
    yPoints.reserve(points.size());
    for (const auto &add : points) {
        xPoints.push_back(tX.toScreen(add.d[0], false));
        yPoints.push_back(tY.toScreen(add.d[1], false));
    }
    auto xTargets = LinearCluster::cluster(xPoints, nullptr, 1.5, 3.0);
    auto yTargets = LinearCluster::cluster(yPoints, nullptr, 1.5, 3.0);

    std::vector<double> columnPosition;
    std::vector<int> columnCount;
    std::vector<double> rowPosition;
    std::vector<int> rowCount;
    std::vector<std::vector<double> > gridData;
    std::vector<std::vector<int> > gridCount;

    for (int pointIdx = 0, totalPoints = points.size(); pointIdx < totalPoints; pointIdx++) {
        int row = yTargets[pointIdx];
        int column = xTargets[pointIdx];
        if (row == -1 || column == -1)
            continue;
        double z = points[pointIdx].d[2];
        if (!FP::defined(z))
            continue;

        rowCount.reserve(row + 1);
        rowPosition.reserve(row + 1);
        while (row >= (int) rowCount.size()) {
            rowCount.push_back(0);
            rowPosition.push_back(0.0);
        }

        columnCount.reserve(column + 1);
        columnPosition.reserve(column + 1);
        while (column >= (int) columnCount.size()) {
            columnCount.push_back(0);
            columnPosition.push_back(0.0);
        }

        rowCount[row]++;
        rowPosition[row] += yPoints[pointIdx];
        columnCount[column]++;
        columnPosition[column] += xPoints[pointIdx];

        gridCount.reserve(row + 1);
        gridData.reserve(row + 1);
        while (row >= (int) gridCount.size()) {
            gridCount.push_back(std::vector<int>(columnCount.size(), 0));
            gridData.push_back(std::vector<double>(columnCount.size(), 0.0));
        }
        gridCount[row].reserve(column + 1);
        gridData[row].reserve(column + 1);
        while (column >= (int) gridCount[row].size()) {
            gridCount[row].push_back(0);
            gridData[row].push_back(0.0);
        }

        gridCount[row][column]++;
        gridData[row][column] += z;
    }

    {
        std::vector<double>::iterator updateColumnPosition = columnPosition.begin();
        pitch = 0;
        for (std::vector<int>::const_iterator addColumnCount = columnCount.begin(),
                endColumns = columnCount.end();
                addColumnCount != endColumns;
                ++addColumnCount, ++updateColumnPosition) {
            if (*addColumnCount == 0) {
                *updateColumnPosition = FP::undefined();
                continue;
            }
            *updateColumnPosition /= (double) (*addColumnCount);
            ++pitch;
        }
    }
    if (pitch == 0)
        return {};

    auto addRowPosition = rowPosition.cbegin();
    auto addRowCount = rowCount.cbegin();
    auto addGridCountRow = gridCount.cbegin();
    std::vector<Graph2DSimpleDrawPoint<3>> result;
    result.reserve(pitch * rowCount.size());
    for (std::vector<std::vector<double> >::const_iterator addGridRow = gridData.begin(),
            endGridRows = gridData.end();
            addGridRow != endGridRows;
            ++addGridRow, ++addRowPosition, ++addRowCount, ++addGridCountRow) {
        if (*addRowCount == 0)
            continue;
        double yPosition = *addRowPosition / (double) (*addRowCount);

        std::vector<double>::const_iterator addColumnPosition = columnPosition.begin();
        std::vector<int>::const_iterator addGridCount = addGridCountRow->begin();
        for (std::vector<double>::const_iterator addGridData = addGridRow->begin(),
                endGridData = addGridRow->end();
                addGridData != endGridData;
                ++addGridData, ++addGridCount, ++addColumnPosition) {
            double xPosition = *addColumnPosition;
            if (!FP::defined(xPosition))
                continue;

            Graph2DSimpleDrawPoint<3> add;
            add.d[0] = xPosition;
            add.d[1] = yPosition;
            if (*addGridCount == 0) {
                add.d[2] = FP::undefined();
            } else {
                add.d[2] = *addGridData / (double) (*addGridCount);
            }
            result.emplace_back(std::move(add));
        }
        for (std::vector<double>::const_iterator endColumnPosition = columnPosition.end();
                addColumnPosition != endColumnPosition;
                ++addColumnPosition) {
            double xPosition = *addColumnPosition;
            if (!FP::defined(xPosition))
                continue;

            Graph2DSimpleDrawPoint<3> add;
            add.d[0] = xPosition;
            add.d[1] = yPosition;
            add.d[2] = FP::undefined();
            result.emplace_back(std::move(add));
        }
    }

    Q_ASSERT(result.size() % pitch == 0);

    return result;
}

static int setBilinearScanPositionsRow(std::vector<
        Graph2DSimpleDrawPoint<3>>::const_iterator &primary,
                                       std::vector<Graph2DSimpleDrawPoint<
                                               3>>::const_iterator &secondary,
                                       std::vector<Graph2DSimpleDrawPoint<3>>::const_iterator end,
                                       double target,
                                       int pitch)
{
    for (;;) {
        if (primary->d[1] == target) {
            secondary = primary;
            return true;
        }
        secondary = primary + pitch;
        if (secondary == end)
            return false;
        if (secondary->d[1] > target)
            return true;
        primary = secondary;
    }
}

static int setBilinearScanPositionsColumn(std::vector<
        Graph2DSimpleDrawPoint<3>>::const_iterator &primary0,
                                          std::vector<Graph2DSimpleDrawPoint<
                                                  3>>::const_iterator &secondary0,
                                          std::vector<Graph2DSimpleDrawPoint<
                                                  3>>::const_iterator &primary1,
                                          std::vector<Graph2DSimpleDrawPoint<
                                                  3>>::const_iterator &secondary1,
                                          std::vector<
                                                  Graph2DSimpleDrawPoint<3>>::const_iterator end0,
                                          double target)
{
    for (;;) {
        if (primary0->d[0] == target) {
            secondary0 = primary0;
            secondary1 = primary1;
            return true;
        }
        secondary0 = primary0 + 1;
        secondary1 = primary1 + 1;
        if (secondary0 == end0)
            return false;
        if (secondary0->d[0] > target)
            return true;
        primary0 = secondary0;
        primary1 = secondary1;
    }
}

void GraphPainter2DFillBilinearGrid::paint(QPainter *painter)
{
    int startX = (int) std::floor(std::min(tX.getScreenMin(), tX.getScreenMax()));
    int endX = (int) std::ceil(std::max(tX.getScreenMin(), tX.getScreenMax()));
    int width = endX - startX + 1;
    int startY = (int) std::floor(std::min(tY.getScreenMin(), tY.getScreenMax()));
    int endY = (int) std::ceil(std::max(tY.getScreenMin(), tY.getScreenMax()));
    int height = endY - startY + 1;

    if (width <= 0 || height <= 0)
        return;

    if (!FP::defined(tZ.getScreenMin()) || !FP::defined(tZ.getScreenMax())) {
        tZ.setScreen(0, 1);
    }

    int pitch = -1;
    auto gridded = gridPoints(pitch);
    if (gridded.empty() || pitch <= 0)
        return;

    if (static_cast<int>(gridded.size()) == pitch || pitch == 1) {
        /* Just allocate a new one, since this is a degenerate case anyway */
        GraphPainter2DLinesGradient temp
                (points, tX, tY, tZ, 0, Qt::SolidLine, gradient, TraceSymbol(), FP::undefined(),
                 true, tracePtr);
        temp.paint(painter);
        return;
    }

    QImage img(width, height, QImage::Format_ARGB32_Premultiplied);
    img.fill(0);

    auto gridScanline = gridded.cbegin();
    auto gridScanEnd = gridded.cend();
    auto secondaryLine = gridScanEnd;

    for (int y = 0; y < height; y++) {
        int yc = y + startY;

        if (!setBilinearScanPositionsRow(gridScanline, secondaryLine, gridScanEnd, (double) yc,
                                         pitch))
            break;
        if (secondaryLine == gridScanEnd)
            continue;
        if (gridScanline->d[1] > (double) yc)
            continue;
        auto primaryLine = gridScanline;

        auto primaryEnd = primaryLine + pitch;
        auto primaryNext = gridScanEnd;
        auto secondaryNext = gridScanEnd;
        QRgb *data = (QRgb *) img.scanLine(y);
        for (int x = 0; x < width; x++, data++) {
            int xc = x + startX;

            if (!setBilinearScanPositionsColumn(primaryLine, primaryNext, secondaryLine,
                                                secondaryNext, primaryEnd, (double) xc))
                continue;
            if (primaryNext == gridScanEnd)
                continue;
            if (primaryLine->d[0] > (double) xc)
                continue;
            Q_ASSERT(secondaryNext != gridScanEnd);

            double dy = secondaryLine->d[1] - primaryLine->d[1];
            double dx = primaryNext->d[0] - primaryLine->d[0];
            double v;
            if (dy <= 0.0) {
                if (dx <= 0.0) {
                    v = primaryLine->d[2];
                } else if (FP::defined(primaryLine->d[2]) && FP::defined(primaryNext->d[2])) {
                    v = primaryLine->d[2];
                    v += ((double) xc - primaryLine->d[0]) * ((primaryNext->d[2] - v) / dx);
                } else {
                    v = FP::undefined();
                }
            } else if (dx <= 0.0) {
                if (FP::defined(primaryLine->d[2]) && FP::defined(secondaryLine->d[2])) {
                    v = primaryLine->d[2];
                    v += ((double) yc - primaryLine->d[1]) * ((secondaryLine->d[2] - v) / dy);
                } else {
                    v = FP::undefined();
                }
            } else if (FP::defined(primaryLine->d[2]) &&
                    FP::defined(primaryNext->d[2]) &&
                    FP::defined(secondaryLine->d[2]) &&
                    FP::defined(secondaryNext->d[2])) {
                dx *= dy;
                v = (primaryLine->d[2] / dx) *
                        (primaryNext->d[0] - (double) xc) *
                        (secondaryLine->d[1] - (double) yc);
                v += (primaryNext->d[2] / dx) *
                        ((double) xc - primaryLine->d[0]) *
                        (secondaryLine->d[1] - (double) yc);
                v += (secondaryLine->d[2] / dx) *
                        (primaryNext->d[0] - (double) xc) *
                        ((double) yc - primaryLine->d[1]);
                v += (secondaryNext->d[2] / dx) *
                        ((double) xc - primaryLine->d[0]) *
                        ((double) yc - primaryLine->d[1]);
            } else {
                v = FP::undefined();
            }
            v = tZ.toScreen(v, true);
            if (!FP::defined(v))
                continue;

            QRgb put = gradient.evaluate(v).rgba();
            quint8 alpha = put >> 24;
            if (alpha == 0)
                continue;
            if (alpha != 0xFF) {
                quint8 r = (((put >> 16) & 0xFF) * alpha) / 0xFF;
                quint8 g = (((put >> 8) & 0xFF) * alpha) / 0xFF;
                quint8 b = ((put & 0xFF) * alpha) / 0xFF;
                put = (alpha << 24) | (r << 16) | (g << 8) | b;
            }
            *data = put;
        }
    }

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    painter->drawImage(startX, startY, img);
    painter->restore();
}

void GraphPainter2DFillBilinearGrid::updateMouseover(const QPointF &point,
                                                     std::shared_ptr<
                                                             Graph2DMouseoverComponent> &output)
{
    if (points.empty())
        return;
    TracePoint<3> hit;
    double distance;
    findNearestPoint(point, points, tX, tY, hit, distance);
    if (!FP::defined(distance))
        return;
    if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(output.get())) {
        QPointF otherDelta(trace->getCenter() - point);
        if (sqrt(otherDelta.x() * otherDelta.x() + otherDelta.y() * otherDelta.y()) < distance)
            return;
    }

    output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverComponentTraceStandard(
            QPointF(tX.toScreen(hit.d[0], true), tY.toScreen(hit.d[1], true)), tracePtr, hit));
}

void GraphPainter2DFillBilinearGrid::updateMouseClick(const QPointF &,
                                                      std::shared_ptr<Graph2DMouseoverComponent> &)
{ }

static QPointF shadeToSide(AxisTransformer &tX,
                           AxisTransformer &tY,
                           Axis2DSide side,
                           double x,
                           double y)
{
    switch (side) {
    case Axis2DBottom:
        return QPointF(x, qMax(tY.getScreenMin(), tY.getScreenMax()));
    case Axis2DTop:
        return QPointF(x, qMin(tY.getScreenMin(), tY.getScreenMax()));
    case Axis2DLeft:
        return QPointF(qMin(tX.getScreenMin(), tX.getScreenMax()), y);
    case Axis2DRight:
        return QPointF(qMax(tX.getScreenMin(), tX.getScreenMax()), y);
    }
    Q_ASSERT(false);
    return QPointF();
}


/**
 * Create a new line shading fill painter.
 * 
 * @param setPoints         the points to paint (in X,Y,Z(color, ignored) order)
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setSide           the side to fill to
 * @param setColor          the color to paint with
 * @param setContinuity     the maximum continuity gap time
 * @param setSkipUndefined  if set then don't consider undefined points
 * @param priority          the draw sort priority
 */
GraphPainter2DFillShade::GraphPainter2DFillShade(std::vector<TracePoint<3> > setPoints,
                                                 const AxisTransformer &setX,
                                                 const AxisTransformer &setY,
                                                 Axis2DSide setSide,
                                                 const QColor &setColor,
                                                 double setContinuity,
                                                 bool setSkipUndefined,
                                                 int priority) : Graph2DDrawPrimitive(priority),
                                                                 points(std::move(setPoints)),
                                                                 tX(setX),
                                                                 tY(setY),
                                                                 side(setSide),
                                                                 color(setColor),
                                                                 continuity(setContinuity),
                                                                 skipUndefined(setSkipUndefined)
{ }

GraphPainter2DFillShade::~GraphPainter2DFillShade() = default;

void GraphPainter2DFillShade::paint(QPainter *painter)
{
    if (points.empty())
        return;
    if (points.size() == 1) {
        double y = tY.toScreen(points.front().d[1], false);
        if (!FP::defined(y))
            return;
        double x = tX.toScreen(points.front().d[0], false);
        if (!FP::defined(x))
            return;

        painter->save();
        painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
        QBrush brush(color);
        painter->setBrush(brush);
        painter->setPen(QPen(brush, 1));
        painter->drawLine(QPointF(x, y), shadeToSide(tX, tY, side, x, y));
        painter->restore();
        return;
    }

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    QBrush brush(color);
    painter->setBrush(brush);
    painter->setPen(QPen(brush, 1));

    double startX = tX.toScreen(points.front().d[0], false);
    double startY = tY.toScreen(points.front().d[1], false);
    double lastX = FP::undefined();
    double lastY = FP::undefined();
    bool increasing = false;
    QPainterPath path;
    for (auto it = points.cbegin() + 1, end = points.cend(); it != end; ++it) {
        /* Not priorPoint, since the continuity check should still be able
         * to cause breaks between undefined points */
        if (!pointsContiguous((it - 1)->end, it->start, continuity)) {
            if (!FP::defined(lastY) || !FP::defined(lastX)) {
                if (FP::defined(startY) && FP::defined(startX)) {
                    painter->drawLine(QPointF(startX, startY),
                                      shadeToSide(tX, tY, side, startX, startY));
                }
            } else {
                path.lineTo(shadeToSide(tX, tY, side, lastX, lastY));
                path.lineTo(shadeToSide(tX, tY, side, startX, startY));
                path.closeSubpath();
                painter->drawPath(path);
                path = QPainterPath();
            }
            startX = tX.toScreen(it->d[0], false);
            startY = tY.toScreen(it->d[1], false);
            lastX = FP::undefined();
            lastY = FP::undefined();
            continue;
        }

        double x = tX.toScreen(it->d[0], false);
        double y = tY.toScreen(it->d[1], false);
        if (!FP::defined(y) || !FP::defined(x)) {
            if (skipUndefined)
                continue;
            if (!FP::defined(lastY) || !FP::defined(lastX)) {
                if (FP::defined(startY) && FP::defined(startX)) {
                    painter->drawLine(QPointF(startX, startY),
                                      shadeToSide(tX, tY, side, startX, startY));
                }
            } else {
                path.lineTo(shadeToSide(tX, tY, side, lastX, lastY));
                path.lineTo(shadeToSide(tX, tY, side, startX, startY));
                path.closeSubpath();
                painter->drawPath(path);
                path = QPainterPath();
            }
            startX = FP::undefined();
            startY = FP::undefined();
            lastX = FP::undefined();
            lastY = FP::undefined();
            continue;
        }

        if (!FP::defined(startY) || !FP::defined(startX)) {
            startX = x;
            startY = y;
            continue;
        }
        if (!FP::defined(lastY) || !FP::defined(lastX)) {
            path.moveTo(startX, startY);
            if (side == Axis2DBottom || side == Axis2DTop) {
                increasing = x > startX;
            } else {
                increasing = y > startY;
            }
        } else {
            bool delta;
            if (side == Axis2DBottom || side == Axis2DTop) {
                delta = x > lastX;
            } else {
                delta = y > lastY;
            }
            if (delta != increasing) {
                path.lineTo(shadeToSide(tX, tY, side, lastX, lastY));
                path.lineTo(shadeToSide(tX, tY, side, startX, startY));
                path.closeSubpath();
                painter->drawPath(path);
                path = QPainterPath();

                startX = lastX;
                startY = lastY;
                increasing = delta;
                path.moveTo(startX, startY);
            }
        }
        path.lineTo(x, y);
        lastX = x;
        lastY = y;
    }
    if (!FP::defined(lastY) || !FP::defined(lastX)) {
        if (FP::defined(startY) && FP::defined(startX)) {
            painter->drawLine(QPointF(startX, startY), shadeToSide(tX, tY, side, startX, startY));
        }
    } else {
        path.lineTo(shadeToSide(tX, tY, side, lastX, lastY));
        path.lineTo(shadeToSide(tX, tY, side, startX, startY));
        path.closeSubpath();
        painter->drawPath(path);
    }

    painter->restore();
}


namespace {
template<typename PointType>
struct Graph2DPainterShadeState {
    double start;
    double prior;
    bool increasing;
    typename std::vector<PointType>::const_iterator first;
    typename std::vector<PointType>::const_iterator last;

    void reset(double v, typename std::vector<PointType>::const_iterator it)
    {
        start = v;
        prior = FP::undefined();
        first = it;
        last = it;
    }

    template<std::size_t idxX, std::size_t idxY, std::size_t idxZ>
    void reset(typename std::vector<PointType>::const_iterator it, Axis2DSide side)
    {
        if (!FP::defined(it->d[idxY]) ||
                !FP::defined(it->d[idxX]) ||
                (idxZ != static_cast<std::size_t>(-1) && !FP::defined(it->d[idxZ]))) {
            start = FP::undefined();
            prior = FP::undefined();
            first = it;
            last = it;
            return;
        }
        if (side == Axis2DTop || side == Axis2DBottom)
            start = it->d[idxX];
        else
            start = it->d[idxY];
        prior = FP::undefined();
        first = it;
        last = it;
    }
};
}

template<std::size_t idxX, std::size_t idxY, typename PointType>
static void drawFixedShadeArea(QPainter *painter,
                               AxisTransformer &tX,
                               AxisTransformer &tY,
                               Axis2DSide side,
                               typename std::vector<PointType>::const_iterator first,
                               typename std::vector<PointType>::const_iterator last)
{
    if (first == last) {
        painter->drawLine(QPointF(first->d[idxX], first->d[idxY]),
                          shadeToSide(tX, tY, side, first->d[idxX], first->d[idxY]));
        return;
    }

    QPainterPath path;
    path.moveTo(first->d[idxX], first->d[idxY]);
    for (auto it = first + 1, end = last + 1; it != end; ++it) {
        if (!FP::defined(it->d[idxY]) || !FP::defined(it->d[idxX]))
            continue;
        path.lineTo(it->d[idxX], it->d[idxY]);
    }
    path.lineTo(shadeToSide(tX, tY, side, last->d[idxX], last->d[idxY]));
    path.lineTo(shadeToSide(tX, tY, side, first->d[idxX], first->d[idxY]));
    path.closeSubpath();
    painter->drawPath(path);
}

namespace {
template<std::size_t idxX, std::size_t idxY, std::size_t idxZ, typename PointType>
class Graph2DPainterGradientShader {
    TraceGradient *gradient;
public:
    explicit Graph2DPainterGradientShader(TraceGradient *g) : gradient(g)
    { }

    void operator()(QPainter *painter,
                    AxisTransformer &tX,
                    AxisTransformer &tY,
                    Axis2DSide side,
                    typename std::vector<PointType>::const_iterator first,
                    typename std::vector<PointType>::const_iterator last)
    {
        if (first == last) {
            if (!FP::defined(first->d[idxY]) ||
                    !FP::defined(first->d[idxZ]) ||
                    !FP::defined(first->d[idxX]))
                return;
            QBrush brush(gradient->evaluate(first->d[idxZ]));
            painter->setBrush(brush);
            painter->setPen(QPen(brush, 1));
            painter->drawLine(QPointF(first->d[idxX], first->d[idxY]),
                              shadeToSide(tX, tY, side, first->d[idxX], first->d[idxY]));
            return;
        }

        qreal offset;
        qreal scale;
        if (side == Axis2DTop || side == Axis2DBottom) {
            offset = first->d[idxX];
            if (last->d[idxX] != first->d[idxX])
                scale = 1.0 / (last->d[idxX] - first->d[idxX]);
            else
                scale = 0;
        } else {
            offset = first->d[idxY];
            if (last->d[idxY] != first->d[idxY])
                scale = 1.0 / (last->d[idxY] - first->d[idxY]);
            else
                scale = 0;
        }

        QPainterPath path;
        QLinearGradient g;
        path.moveTo(first->d[idxX], first->d[idxY]);
        g.setStart(first->d[idxX], first->d[idxY]);;
        g.setColorAt(0, gradient->evaluate(first->d[idxZ]));
        for (auto it = first + 1, end = last + 1; it != end; ++it) {
            if (!FP::defined(it->d[idxY]) || !FP::defined(it->d[idxX]) || !FP::defined(it->d[idxZ]))
                continue;
            path.lineTo(it->d[idxX], it->d[idxY]);
            if (side == Axis2DTop || side == Axis2DBottom) {
                g.setColorAt((it->d[idxX] - offset) * scale, gradient->evaluate(it->d[idxZ]));
            } else {
                g.setColorAt((it->d[idxY] - offset) * scale, gradient->evaluate(it->d[idxZ]));
            }
        }
        if (side == Axis2DTop || side == Axis2DBottom)
            g.setFinalStop(last->d[idxX], first->d[idxY]);
        else
            g.setFinalStop(first->d[idxX], last->d[idxY]);
        path.lineTo(shadeToSide(tX, tY, side, last->d[idxX], last->d[idxY]));
        path.lineTo(shadeToSide(tX, tY, side, first->d[idxX], first->d[idxY]));
        path.closeSubpath();

        QBrush brush(g);
        painter->setBrush(brush);
        painter->setPen(QPen(brush, 1));
        painter->drawPath(path);
    }
};
}


template<std::size_t idxX, std::size_t idxY, std::size_t idxZ, typename PointType,
                                                               typename PainterType>
static void handleShadePoint(QPainter *painter,
                             Graph2DPainterShadeState<PointType> &state,
                             typename std::vector<PointType>::const_iterator it,
                             AxisTransformer &tX,
                             AxisTransformer &tY,
                             Axis2DSide side,
                             bool skipUndefined,
                             PainterType effectivePainter)
{
    double x = it->d[idxX];
    double y = it->d[idxY];
    double z;
    if (idxZ != static_cast<std::size_t>(-1))
        z = it->d[idxZ];
    else
        z = 0;
    if (!FP::defined(y) || !FP::defined(x) || !FP::defined(z)) {
        if (skipUndefined)
            return;
        if (!FP::defined(state.prior)) {
            if (FP::defined(state.start)) {
                effectivePainter(painter, tX, tY, side, state.first, state.first);
            }
        } else {
            effectivePainter(painter, tX, tY, side, state.first, state.last);
        }
        state.reset(FP::undefined(), it);
        return;
    }

    double keyValue;
    if (side == Axis2DBottom || side == Axis2DTop) {
        keyValue = x;
    } else {
        keyValue = y;
    }

    if (!FP::defined(state.start)) {
        state.start = keyValue;
        state.first = it;
        state.last = it;
        return;
    }

    if (!FP::defined(state.prior)) {
        state.increasing = keyValue > state.start;
    } else {
        bool delta = keyValue > state.prior;
        if (delta != state.increasing) {
            effectivePainter(painter, tX, tY, side, state.first, state.last);

            state.start = state.prior;
            state.first = state.last;
            state.increasing = delta;
        }
    }
    state.prior = keyValue;
    state.last = it;
}

/**
 * Create a new line shading fill painter.
 * 
 * @param setPoints         the points to paint (in X,Y,Z(color) order)
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setZ              the Z (color) transformer
 * @param setSide           the side to fill to
 * @param setGradient       the gradient to paint with
 * @param setContinuity     the maximum continuity gap time
 * @param setSkipUndefined  if set then don't consider undefined points
 * @param priority          the draw sort priority
 */
GraphPainter2DFillShadeGradient::GraphPainter2DFillShadeGradient(std::vector<
        TracePoint<3> > setPoints,
                                                                 const AxisTransformer &setX,
                                                                 const AxisTransformer &setY,
                                                                 const AxisTransformer &setZ,
                                                                 Axis2DSide setSide,
                                                                 const TraceGradient &setGradient,
                                                                 double setContinuity,
                                                                 bool setSkipUndefined,
                                                                 int priority)
        : Graph2DDrawPrimitive(priority),
          points(std::move(setPoints)),
          tX(setX),
          tY(setY),
          tZ(setZ),
          side(setSide),
          gradient(setGradient),
          continuity(setContinuity),
          skipUndefined(setSkipUndefined)
{ }

GraphPainter2DFillShadeGradient::~GraphPainter2DFillShadeGradient() = default;

void GraphPainter2DFillShadeGradient::paint(QPainter *painter)
{
    if (points.empty())
        return;
    if (points.size() == 1) {
        double y = tY.toScreen(points.front().d[1], false);
        if (!FP::defined(y))
            return;
        double z = tY.toScreen(points.front().d[2], true);
        if (!FP::defined(z))
            return;
        double x = tX.toScreen(points.front().d[0], false);
        if (!FP::defined(x))
            return;

        painter->save();
        painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
        QBrush brush(gradient.evaluate(z));
        painter->setBrush(brush);
        painter->setPen(QPen(brush, 1));
        painter->drawLine(QPointF(x, y), shadeToSide(tX, tY, side, x, y));
        painter->restore();
        return;
    }

    for (int i = 0, max = points.size(); i < max; i++) {
        points[i].d[0] = tX.toScreen(points[i].d[0], false);
        points[i].d[1] = tY.toScreen(points[i].d[1], false);
        points[i].d[2] = tZ.toScreen(points[i].d[2], true);
    }

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);

    Graph2DPainterGradientShader<0, 1, 2, TracePoint<3> > shader(&gradient);
    Graph2DPainterShadeState <TracePoint<3>> state;
    state.increasing = false;
    state.reset<0, 1, 2>(points.cbegin(), side);
    for (auto it = points.cbegin() + 1, end = points.cend(); it != end; ++it) {
        /* Not the prior, since the continuity check should still be able
         * to cause breaks between undefined points */
        if (!pointsContiguous((it - 1)->end, it->start, continuity)) {
            shader(painter, tX, tY, side, state.first, state.last);
            state.reset<0, 1, 2>(it, side);
            continue;
        }

        handleShadePoint<0, 1, 2, TracePoint<3> >(painter, state, it, tX, tY, side, skipUndefined,
                                                  shader);
    }
    shader(painter, tX, tY, side, state.first, state.last);

    painter->restore();
}


template<std::size_t N, std::size_t idxX, std::size_t idxY>
static void drawShadeFit(QPainter *painter,
                         const std::vector<Graph2DSimpleDrawPoint<N> > &points,
                         Axis2DSide side,
                         AxisTransformer &tX,
                         AxisTransformer &tY)
{
    Graph2DPainterShadeState <Graph2DSimpleDrawPoint<N>> state;
    state.template reset<idxX, idxY, static_cast<std::size_t>(-1)>(points.cbegin(), side);
    state.increasing = false;
    for (auto it = points.cbegin() + 1, end = points.cend(); it != end; ++it) {
        handleShadePoint<idxX, idxY, static_cast<std::size_t>(-1), Graph2DSimpleDrawPoint<N>>(
                painter, state, it, tX, tY, side, false, drawFixedShadeArea < idxX, idxY,
                Graph2DSimpleDrawPoint<N> > );
    }
    drawFixedShadeArea<idxX, idxY, Graph2DSimpleDrawPoint<N>>(painter, tX, tY, side, state.first,
                                                              state.last);
}

/**
 * Create a new fit shade side drawer.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setSide           the side to fill to
 * @param setColor          the color to paint with
 * @param setScale          the scale factor applied to the independent variable before the model
 * @param priority          the draw sort priority
 */
GraphPainter2DShadeFitX::GraphPainter2DShadeFitX(std::shared_ptr<Model> setModel,
                                                 const AxisTransformer &setX,
                                                 const AxisTransformer &setY,
                                                 Axis2DSide setSide,
                                                 const QColor &setColor,
                                                 double setScale,
                                                 int priority) : Graph2DDrawPrimitive(priority),
                                                                 model(std::move(setModel)),
                                                                 tX(setX),
                                                                 tY(setY),
                                                                 side(setSide),
                                                                 color(setColor),
                                                                 scale(setScale)
{
    Q_ASSERT(model);
    Q_ASSERT(scale != 0.0);
}

GraphPainter2DShadeFitX::~GraphPainter2DShadeFitX() = default;

void GraphPainter2DShadeFitX::paint(QPainter *painter)
{
    std::vector<Graph2DSimpleDrawPoint<4> > points;

    Graph2DSimpleDrawPoint<4> add;
    add.d[0] = tX.getRealMin();
    add.d[1] = model->oneToOne(add.d[0] * scale);
    add.d[2] = tX.toScreen(add.d[0], false);
    add.d[3] = tY.toScreen(add.d[1], false);
    points.push_back(add);

    add.d[0] = tX.getRealMax();
    add.d[1] = model->oneToOne(add.d[0] * scale);
    add.d[2] = tX.toScreen(add.d[0], false);
    add.d[3] = tY.toScreen(add.d[1], false);
    points.push_back(add);

    AxisTransformer *tr[2];
    tr[0] = &tX;
    tr[1] = &tY;
    evaluateFitPoints<4>(points, 0, 0, scale, model.get(), tr);

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    QBrush brush(color);
    painter->setBrush(brush);
    painter->setPen(QPen(brush, 1));
    drawShadeFit<4, 2, 3>(painter, points, side, tX, tY);
    painter->restore();
}


/**
 * Create a new fit side shade drawer.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setSide           the side to fill to
 * @param setColor          the color to paint with
 * @param setScale          the scale factor applied to the independent variable before the model
 * @param priority          the draw sort priority
 */
GraphPainter2DShadeFitY::GraphPainter2DShadeFitY(std::shared_ptr<Model> setModel,
                                                 const AxisTransformer &setX,
                                                 const AxisTransformer &setY,
                                                 Axis2DSide setSide,
                                                 const QColor &setColor,
                                                 double setscale,
                                                 int priority) : Graph2DDrawPrimitive(priority),
                                                                 model(std::move(setModel)),
                                                                 tX(setX),
                                                                 tY(setY),
                                                                 side(setSide),
                                                                 color(setColor),
                                                                 scale(setscale)
{
    Q_ASSERT(model);
    Q_ASSERT(scale != 0.0);
}

GraphPainter2DShadeFitY::~GraphPainter2DShadeFitY() = default;

void GraphPainter2DShadeFitY::paint(QPainter *painter)
{
    std::vector<Graph2DSimpleDrawPoint<4> > points;

    Graph2DSimpleDrawPoint<4> add;
    add.d[0] = tY.getRealMin();
    add.d[1] = model->oneToOne(add.d[0] * scale);
    add.d[2] = tY.toScreen(add.d[0], false);
    add.d[3] = tX.toScreen(add.d[1], false);
    points.push_back(add);

    add.d[0] = tY.getRealMax();
    add.d[1] = model->oneToOne(add.d[0] * scale);
    add.d[2] = tY.toScreen(add.d[0], false);
    add.d[3] = tX.toScreen(add.d[1], false);
    points.push_back(add);

    AxisTransformer *tr[2];
    tr[0] = &tY;
    tr[1] = &tX;
    evaluateFitPoints<4>(points, 0, 0, scale, model.get(), tr);

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    QBrush brush(color);
    painter->setBrush(brush);
    painter->setPen(QPen(brush, 1));
    drawShadeFit<4, 3, 2>(painter, points, side, tX, tY);
    painter->restore();
}


/**
 * Create a new fit side fill drawer.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setSide           the side to fill to
 * @param setColor          the color to paint with
 * @param setMinI           the minimum I value
 * @param setMaxI           the maximum I value
 * @param priority          the draw sort priority
 */
GraphPainter2DShadeFitI::GraphPainter2DShadeFitI(std::shared_ptr<Model> setModel,
                                                 const AxisTransformer &setX,
                                                 const AxisTransformer &setY,
                                                 Axis2DSide setSide,
                                                 const QColor &setColor,
                                                 double setMinI,
                                                 double setMaxI,
                                                 int priority) : Graph2DDrawPrimitive(priority),
                                                                 model(std::move(setModel)),
                                                                 tX(setX),
                                                                 tY(setY),
                                                                 side(setSide),
                                                                 color(setColor),
                                                                 minI(setMinI),
                                                                 maxI(setMaxI)
{
    Q_ASSERT(model);
}

GraphPainter2DShadeFitI::~GraphPainter2DShadeFitI() = default;

void GraphPainter2DShadeFitI::paint(QPainter *painter)
{
    std::vector<Graph2DSimpleDrawPoint<5> > points;

    if (FP::defined(minI)) {
        Graph2DSimpleDrawPoint<5> add;
        add.d[0] = minI;
        add.d[1] = minI;
        add.d[2] = minI;
        model->applyIO(add.d[1], add.d[2]);
        add.d[3] = tX.toScreen(add.d[1], false);
        add.d[4] = tY.toScreen(add.d[2], false);
        points.push_back(add);
    }

    if (FP::defined(maxI) && (!FP::defined(minI) || minI != maxI)) {
        Graph2DSimpleDrawPoint<5> add;
        add.d[0] = maxI;
        add.d[1] = maxI;
        add.d[2] = maxI;
        model->applyIO(add.d[1], add.d[2]);
        add.d[3] = tX.toScreen(add.d[1], false);
        add.d[4] = tY.toScreen(add.d[2], false);
        points.push_back(add);
    }

    if (points.empty())
        return;

    if (points.size() == 2) {
        AxisTransformer *tr[2];
        tr[0] = &tX;
        tr[1] = &tY;
        evaluateFitPoints<5>(points, 0, 0, 1.0, model.get(), tr);
    }

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    QBrush brush(color);
    painter->setBrush(brush);
    painter->setPen(QPen(brush, 1));
    drawShadeFit<5, 3, 4>(painter, points, side, tX, tY);
    painter->restore();
}


template<std::size_t N, std::size_t idxX, std::size_t idxY, std::size_t idxZ>
static void drawShadeFitGradient(QPainter *painter,
                                 const std::vector<Graph2DSimpleDrawPoint<N> > &points,
                                 Axis2DSide side,
                                 AxisTransformer &tX,
                                 AxisTransformer &tY,
                                 TraceGradient &gradient)
{
    Graph2DPainterGradientShader <idxX, idxY, idxZ, Graph2DSimpleDrawPoint<N>> shader(&gradient);
    Graph2DPainterShadeState <Graph2DSimpleDrawPoint<N>> state;
    state.increasing = false;
    state.template reset<idxX, idxY, idxZ>(points.cbegin(), side);
    for (auto it = points.cbegin() + 1, end = points.cend(); it != end; ++it) {
        handleShadePoint<idxX, idxY, idxZ, Graph2DSimpleDrawPoint<N> >(painter, state, it, tX, tY,
                                                                       side, false, shader);
    }
    shader(painter, tX, tY, side, state.first, state.last);
}

/**
 * Create a new fit shade side drawer.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setZ              the Z (color) transformer
 * @param setSide           the side to fill to
 * @param setGradient       the gradient to paint with
 * @param setScale          the scale factor applied to the independent variable before the model
 * @param priority          the draw sort priority
 */
GraphPainter2DShadeFitXGradient::GraphPainter2DShadeFitXGradient(std::shared_ptr<Model> setModel,
                                                                 const AxisTransformer &setX,
                                                                 const AxisTransformer &setY,
                                                                 const AxisTransformer &setZ,
                                                                 Axis2DSide setSide,
                                                                 const TraceGradient &setGradient,
                                                                 double setScale,
                                                                 int priority)
        : Graph2DDrawPrimitive(priority),
          model(std::move(setModel)),
          tX(setX),
          tY(setY),
          tZ(setZ),
          side(setSide),
          gradient(setGradient),
          scale(setScale)
{
    Q_ASSERT(model);
    Q_ASSERT(scale != 0.0);
}

GraphPainter2DShadeFitXGradient::~GraphPainter2DShadeFitXGradient() = default;

void GraphPainter2DShadeFitXGradient::paint(QPainter *painter)
{
    std::vector<Graph2DSimpleDrawPoint<6> > points;

    Graph2DSimpleDrawPoint<6> add;
    add.d[2] = add.d[1] = add.d[0] = tX.getRealMin();
    model->applyIO(add.d[1], add.d[2]);
    add.d[3] = tX.toScreen(add.d[0], false);
    add.d[4] = tY.toScreen(add.d[1], false);
    add.d[5] = tZ.toScreen(add.d[2], true);
    points.push_back(add);

    add.d[2] = add.d[1] = add.d[0] = tX.getRealMax();
    model->applyIO(add.d[1], add.d[2]);
    add.d[3] = tX.toScreen(add.d[0], false);
    add.d[4] = tY.toScreen(add.d[1], false);
    add.d[5] = tZ.toScreen(add.d[2], true);
    points.push_back(add);

    AxisTransformer *tr[3];
    tr[0] = &tX;
    tr[1] = &tY;
    tr[2] = &tZ;
    evaluateFitPoints<6>(points, 0, 0, scale, model.get(), tr, true);

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    drawShadeFitGradient<6, 3, 4, 5>(painter, points, side, tX, tY, gradient);
    painter->restore();
}


/**
 * Create a new fit side shade drawer.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setZ              the Z (color) transformer
 * @param setSide           the side to fill to
 * @param setGradient       the gradient to paint with
 * @param setScale          the scale factor applied to the independent variable before the model
 * @param priority          the draw sort priority
 */
GraphPainter2DShadeFitYGradient::GraphPainter2DShadeFitYGradient(std::shared_ptr<Model> setModel,
                                                                 const AxisTransformer &setX,
                                                                 const AxisTransformer &setY,
                                                                 const AxisTransformer &setZ,
                                                                 Axis2DSide setSide,
                                                                 const TraceGradient &setGradient,
                                                                 double setscale,
                                                                 int priority)
        : Graph2DDrawPrimitive(priority),
          model(std::move(setModel)),
          tX(setX),
          tY(setY),
          tZ(setZ),
          side(setSide),
          gradient(setGradient),
          scale(setscale)
{
    Q_ASSERT(model);
    Q_ASSERT(scale != 0.0);
}

GraphPainter2DShadeFitYGradient::~GraphPainter2DShadeFitYGradient() = default;

void GraphPainter2DShadeFitYGradient::paint(QPainter *painter)
{
    std::vector<Graph2DSimpleDrawPoint<6> > points;

    Graph2DSimpleDrawPoint<6> add;
    add.d[2] = add.d[1] = add.d[0] = tY.getRealMin();
    model->applyIO(add.d[1], add.d[2]);
    add.d[3] = tY.toScreen(add.d[0], false);
    add.d[4] = tX.toScreen(add.d[1], false);
    add.d[5] = tZ.toScreen(add.d[2], true);
    points.push_back(add);

    add.d[2] = add.d[1] = add.d[0] = tY.getRealMax();
    model->applyIO(add.d[1], add.d[2]);
    add.d[3] = tY.toScreen(add.d[0], false);
    add.d[4] = tX.toScreen(add.d[1], false);
    add.d[5] = tZ.toScreen(add.d[2], true);
    points.push_back(add);

    AxisTransformer *tr[3];
    tr[0] = &tY;
    tr[1] = &tX;
    tr[2] = &tZ;
    evaluateFitPoints<6>(points, 0, 0, scale, model.get(), tr, true);

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    drawShadeFitGradient<6, 4, 3, 5>(painter, points, side, tX, tY, gradient);
    painter->restore();
}


/**
 * Create a new fit side fill drawer.
 * 
 * @param setModel          the model to use
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setZ              the Z (color) transformer
 * @param setSide           the side to fill to
 * @param setGradient       the gradient to paint with
 * @param setMinI           the minimum I value
 * @param setMaxI           the maximum I value
 * @param priority          the draw sort priority
 */
GraphPainter2DShadeFitIGradient::GraphPainter2DShadeFitIGradient(std::shared_ptr<Model> setModel,
                                                                 const AxisTransformer &setX,
                                                                 const AxisTransformer &setY,
                                                                 const AxisTransformer &setZ,
                                                                 Axis2DSide setSide,
                                                                 const TraceGradient &setGradient,
                                                                 double setMinI,
                                                                 double setMaxI,
                                                                 int priority)
        : Graph2DDrawPrimitive(priority),
          model(std::move(setModel)),
          tX(setX),
          tY(setY),
          tZ(setZ),
          side(setSide),
          gradient(setGradient),
          minI(setMinI),
          maxI(setMaxI)
{
    Q_ASSERT(model);
}

GraphPainter2DShadeFitIGradient::~GraphPainter2DShadeFitIGradient() = default;

void GraphPainter2DShadeFitIGradient::paint(QPainter *painter)
{
    std::vector<Graph2DSimpleDrawPoint<7> > points;

    if (FP::defined(minI)) {
        Graph2DSimpleDrawPoint<7> add;
        add.d[0] = minI;
        add.d[1] = minI;
        add.d[2] = minI;
        add.d[3] = minI;
        model->applyIO(add.d[1], add.d[2], add.d[3]);
        add.d[4] = tX.toScreen(add.d[1], false);
        add.d[5] = tY.toScreen(add.d[2], false);
        add.d[6] = tZ.toScreen(add.d[3], true);
        points.push_back(add);
    }

    if (FP::defined(maxI) && (!FP::defined(minI) || minI != maxI)) {
        Graph2DSimpleDrawPoint<7> add;
        add.d[0] = maxI;
        add.d[1] = maxI;
        add.d[2] = maxI;
        add.d[3] = maxI;
        model->applyIO(add.d[1], add.d[2], add.d[3]);
        add.d[4] = tX.toScreen(add.d[1], false);
        add.d[5] = tY.toScreen(add.d[2], false);
        add.d[6] = tZ.toScreen(add.d[3], true);
        points.push_back(add);
    }

    if (points.empty())
        return;

    if (points.size() == 2) {
        AxisTransformer *tr[3];
        tr[0] = &tX;
        tr[1] = &tY;
        tr[2] = &tZ;
        evaluateFitPoints<7>(points, 0, 0, 1.0, model.get(), tr, true);
    }

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);
    drawShadeFitGradient<7, 4, 5, 6>(painter, points, side, tX, tY, gradient);
    painter->restore();
}


/**
 * Create a new box and whisker painter.
 * 
 * @param setPoints         the points to paint
 * @param setX              the X transformer
 * @param setY              the Y transformer
 * @param setFill           the interior box fill color
 * @param setWhiskerLineWidth the line width of the whiskers
 * @param setWhiskerStyle   the line style of the whiskers
 * @param setWhiskerColor   the color of the whiskers
 * @param setWhiskerCapWidth the cap width (as a fraction of the box width) on the whiskers
 * @param setBoxLineWidth   the box outline style
 * @param setBoxStyle       the box outline style
 * @param setBoxColor       the box outline color
 * @param setMiddleLineWidth the median/middle line width
 * @param setMiddleStyle    the median/middle line style
 * @param setMiddleColor    the median/middle line color
 * @param setVertical       if true then the bins are drawn vertically
 * @param priority          the draw sort priority
 */
GraphPainter2DBin::GraphPainter2DBin(std::vector<Bin2DPaintPoint> setPoints,
                                     const AxisTransformer &setX,
                                     const AxisTransformer &setY,
                                     const QColor &setFill,
                                     qreal setWhiskerLineWidth,
                                     Qt::PenStyle setWhiskerStyle,
                                     const QColor &setWhiskerColor,
                                     qreal setWhiskerCapWidth,
                                     qreal setBoxLineWidth,
                                     Qt::PenStyle setBoxStyle,
                                     const QColor &setBoxColor,
                                     qreal setMiddleLineWidth,
                                     Qt::PenStyle setMiddleStyle,
                                     const QColor &setMiddleColor,
                                     bool setVertical,
                                     quintptr setTracePtr,
                                     int priority) : Graph2DDrawPrimitive(priority),
                                                     points(std::move(setPoints)),
                                                     tX(setX),
                                                     tY(setY),
                                                     fill(setFill),
                                                     whiskerLineWidth(setWhiskerLineWidth),
                                                     whiskerStyle(setWhiskerStyle),
                                                     whiskerColor(setWhiskerColor),
                                                     whiskerCapWidth(setWhiskerCapWidth),
                                                     boxLineWidth(setBoxLineWidth),
                                                     boxStyle(setBoxStyle),
                                                     boxColor(setBoxColor),
                                                     middleLineWidth(setMiddleLineWidth),
                                                     middleStyle(setMiddleStyle),
                                                     middleColor(setMiddleColor),
                                                     vertical(setVertical),
                                                     tracePtr(setTracePtr)
{ }

GraphPainter2DBin::~GraphPainter2DBin() = default;

void GraphPainter2DBin::paint(QPainter *painter)
{
    if (points.empty())
        return;

    painter->save();
    painter->setClipRect(transformersToClippingRect(tX, tY), Qt::IntersectClip);

    for (const auto &it : points) {
        double whiskerLower;
        double whiskerUpper;
        double boxFirstEdge;
        double boxSecondEdge;
        double boxLower;
        double boxUpper;
        double middle;
        double center;

        if (vertical) {
            center = tX.toScreen(it.paintCenter, false);
            if (FP::defined(it.paintWidth) && FP::defined(it.paintCenter) && it.paintWidth >= 0.0) {
                boxFirstEdge = tX.toScreen(it.paintCenter - it.paintWidth * 0.5, false);
                boxSecondEdge = tX.toScreen(it.paintCenter + it.paintWidth * 0.5, false);
            } else {
                boxFirstEdge = FP::undefined();
                boxSecondEdge = FP::undefined();
            }

            whiskerLower = tY.toScreen(it.lowest[0], false);
            whiskerUpper = tY.toScreen(it.uppermost[0], false);
            boxLower = tY.toScreen(it.lower[0], false);
            boxUpper = tY.toScreen(it.upper[0], false);
            middle = tY.toScreen(it.middle[0], false);
        } else {
            center = tY.toScreen(it.paintCenter, false);
            if (FP::defined(it.paintWidth) && FP::defined(it.paintCenter)) {
                boxFirstEdge = tY.toScreen(it.paintCenter - it.paintWidth * 0.5, false);
                boxSecondEdge = tY.toScreen(it.paintCenter + it.paintWidth * 0.5, false);
            } else {
                boxFirstEdge = FP::undefined();
                boxSecondEdge = FP::undefined();
            }

            whiskerLower = tX.toScreen(it.lowest[0], false);
            whiskerUpper = tX.toScreen(it.uppermost[0], false);
            boxLower = tX.toScreen(it.lower[0], false);
            boxUpper = tX.toScreen(it.upper[0], false);
            middle = tX.toScreen(it.middle[0], false);
        }

        if (FP::defined(it.paintWidth) && it.paintWidth < 0.0) {
            if (FP::defined(center)) {
                boxFirstEdge = center + it.paintWidth * 0.5;
                boxSecondEdge = center - it.paintWidth * 0.5;
            } else {
                boxFirstEdge = FP::undefined();
                boxSecondEdge = FP::undefined();
            }
        }

        if (!FP::defined(boxFirstEdge))
            boxFirstEdge = center;
        if (!FP::defined(boxSecondEdge))
            boxSecondEdge = center;
        if (!FP::defined(boxLower))
            boxLower = middle;
        if (!FP::defined(boxUpper))
            boxUpper = middle;

        double whiskerLowerStop = FP::undefined();
        double whiskerUpperStop = FP::undefined();
        if (FP::defined(center)) {
            if (FP::defined(whiskerLower)) {
                if (FP::defined(boxLower)) {
                    whiskerLowerStop = boxLower;
                } else if (FP::defined(middle)) {
                    whiskerLowerStop = middle;
                } else if (FP::defined(boxUpper)) {
                    whiskerLowerStop = center;
                } else {
                    whiskerLowerStop = whiskerUpper;
                    whiskerUpperStop = whiskerLowerStop;
                }
            }
            if (FP::defined(whiskerUpper) && !FP::defined(whiskerUpperStop)) {
                if (FP::defined(boxUpper)) {
                    whiskerUpperStop = boxUpper;
                } else if (FP::defined(middle)) {
                    whiskerUpperStop = middle;
                } else {
                    whiskerUpperStop = center;
                }
            }
        }

        double whiskerCapStart = FP::undefined();
        double whiskerCapEnd = FP::undefined();
        if (whiskerCapWidth > 0.0 &&
                FP::defined(boxFirstEdge) &&
                FP::defined(boxSecondEdge) &&
                boxFirstEdge != boxSecondEdge) {
            double width = (boxSecondEdge - boxFirstEdge) * (whiskerCapWidth - 1.0) * 0.5;
            whiskerCapStart = boxFirstEdge - width;
            whiskerCapEnd = boxSecondEdge + width;
        }

        QPainterPath whiskerPath;
        QPainterPath boxPath;
        QPainterPath middlePath;

        if (vertical) {
            if (FP::defined(center) &&
                    FP::defined(whiskerLower) &&
                    FP::defined(whiskerLowerStop) &&
                    whiskerLower != whiskerLowerStop) {
                whiskerPath.moveTo(center, whiskerLowerStop);
                whiskerPath.lineTo(center, whiskerLower);
            }
            if (FP::defined(whiskerCapStart) &&
                    FP::defined(whiskerCapEnd) &&
                    FP::defined(whiskerLower)) {
                if (FP::defined(center)) {
                    whiskerPath.moveTo(center, whiskerLower);
                    whiskerPath.lineTo(whiskerCapStart, whiskerLower);
                    whiskerPath.moveTo(center, whiskerLower);
                    whiskerPath.lineTo(whiskerCapEnd, whiskerLower);
                } else {
                    whiskerPath.moveTo(whiskerCapStart, whiskerLower);
                    whiskerPath.lineTo(whiskerCapEnd, whiskerLower);
                }
            }
            if (FP::defined(center) &&
                    FP::defined(whiskerUpper) &&
                    FP::defined(whiskerUpperStop) &&
                    whiskerUpper != whiskerUpperStop) {
                whiskerPath.moveTo(center, whiskerUpperStop);
                whiskerPath.lineTo(center, whiskerUpper);
            }
            if (FP::defined(whiskerCapStart) &&
                    FP::defined(whiskerCapEnd) &&
                    FP::defined(whiskerUpper)) {
                if (FP::defined(center)) {
                    whiskerPath.moveTo(center, whiskerUpper);
                    whiskerPath.lineTo(whiskerCapStart, whiskerUpper);
                    whiskerPath.moveTo(center, whiskerUpper);
                    whiskerPath.lineTo(whiskerCapEnd, whiskerUpper);
                } else {
                    whiskerPath.moveTo(whiskerCapStart, whiskerUpper);
                    whiskerPath.lineTo(whiskerCapEnd, whiskerUpper);
                }
            }

            if (FP::defined(boxFirstEdge) && FP::defined(boxSecondEdge)) {
                if (FP::defined(boxLower) && FP::defined(boxUpper)) {
                    boxPath.moveTo(boxFirstEdge, boxLower);
                    boxPath.lineTo(boxSecondEdge, boxLower);
                    boxPath.lineTo(boxSecondEdge, boxUpper);
                    boxPath.lineTo(boxFirstEdge, boxUpper);
                    boxPath.closeSubpath();
                }
                if (FP::defined(middle)) {
                    middlePath.moveTo(boxFirstEdge, middle);
                    middlePath.lineTo(boxSecondEdge, middle);
                }
            } else if (FP::defined(center)) {
                if (FP::defined(boxLower) && FP::defined(boxUpper)) {
                    boxPath.moveTo(center, boxLower);
                    boxPath.lineTo(center, boxUpper);
                }
                if (FP::defined(middle)) {
                    middlePath.moveTo(center, middle);
                    middlePath.lineTo(center, middle);
                }
            }
        } else {
            if (FP::defined(center) &&
                    FP::defined(whiskerLower) &&
                    FP::defined(whiskerLowerStop) &&
                    whiskerLower != whiskerLowerStop) {
                whiskerPath.moveTo(whiskerLowerStop, center);
                whiskerPath.lineTo(whiskerLower, center);
            }
            if (FP::defined(whiskerCapStart) &&
                    FP::defined(whiskerCapEnd) &&
                    FP::defined(whiskerLower)) {
                if (FP::defined(center)) {
                    whiskerPath.moveTo(whiskerLower, center);
                    whiskerPath.lineTo(whiskerLower, whiskerCapStart);
                    whiskerPath.moveTo(whiskerLower, center);
                    whiskerPath.lineTo(whiskerLower, whiskerCapEnd);
                } else {
                    whiskerPath.moveTo(whiskerLower, whiskerCapStart);
                    whiskerPath.lineTo(whiskerLower, whiskerCapEnd);
                }
            }
            if (FP::defined(center) &&
                    FP::defined(whiskerUpper) &&
                    FP::defined(whiskerUpperStop) &&
                    whiskerUpper != whiskerUpperStop) {
                whiskerPath.moveTo(whiskerUpperStop, center);
                whiskerPath.lineTo(whiskerUpper, center);
            }
            if (FP::defined(whiskerCapStart) &&
                    FP::defined(whiskerCapEnd) &&
                    FP::defined(whiskerUpper)) {
                if (FP::defined(center)) {
                    whiskerPath.moveTo(whiskerUpper, center);
                    whiskerPath.lineTo(whiskerUpper, whiskerCapStart);
                    whiskerPath.moveTo(whiskerUpper, center);
                    whiskerPath.lineTo(whiskerUpper, whiskerCapEnd);
                } else {
                    whiskerPath.moveTo(whiskerUpper, whiskerCapStart);
                    whiskerPath.lineTo(whiskerUpper, whiskerCapEnd);
                }
            }

            if (FP::defined(boxFirstEdge) && FP::defined(boxSecondEdge)) {
                if (FP::defined(boxLower) && FP::defined(boxUpper)) {
                    boxPath.moveTo(boxLower, boxFirstEdge);
                    boxPath.lineTo(boxLower, boxSecondEdge);
                    boxPath.lineTo(boxUpper, boxSecondEdge);
                    boxPath.lineTo(boxUpper, boxFirstEdge);
                    boxPath.closeSubpath();
                }
                if (FP::defined(middle)) {
                    middlePath.moveTo(middle, boxFirstEdge);
                    middlePath.lineTo(middle, boxSecondEdge);
                }
            } else if (FP::defined(center)) {
                if (FP::defined(boxLower) && FP::defined(boxUpper)) {
                    boxPath.moveTo(boxLower, center);
                    boxPath.lineTo(boxUpper, center);
                }
                if (FP::defined(middle)) {
                    middlePath.moveTo(middle, center);
                    middlePath.lineTo(middle, center);
                }
            }
        }

        if (fill.isValid()) {
            painter->fillPath(boxPath, fill);
        }

        if (whiskerStyle != Qt::NoPen) {
            painter->strokePath(whiskerPath,
                                QPen(QBrush(whiskerColor), whiskerLineWidth, whiskerStyle,
                                     Qt::FlatCap));
        }

        if (boxStyle != Qt::NoPen) {
            painter->strokePath(boxPath, QPen(QBrush(boxColor), boxLineWidth, boxStyle));
        }

        if (middleStyle != Qt::NoPen) {
            painter->strokePath(middlePath, QPen(QBrush(middleColor), middleLineWidth, middleStyle,
                                                 Qt::FlatCap));
        }
    }

    painter->restore();
}

double GraphPainter2DBin::pointDistance(double x1,
                                        double x2,
                                        double y1,
                                        double y2,
                                        const QPointF &point,
                                        double &cx,
                                        double &cy)
{
    if (!FP::defined(y1))
        y1 = y2;
    if (!FP::defined(y1))
        return FP::undefined();
    if (!FP::defined(x1))
        x1 = x2;
    if (!FP::defined(x1))
        return FP::undefined();
    if (FP::defined(x2) && x1 == x2)
        x2 = FP::undefined();
    if (FP::defined(y2) && y1 == y2)
        y2 = FP::undefined();

    if (tX.getScreenMin() < tX.getScreenMax()) {
        if (x1 < tX.getScreenMin()) {
            if (!FP::defined(x2) || x2 < tX.getScreenMin())
                return FP::undefined();
            x1 = tX.getScreenMin();
        } else if (x1 > tX.getScreenMax()) {
            if (!FP::defined(x2) || x2 > tX.getScreenMax())
                return FP::undefined();
            x1 = tX.getScreenMax();
        }
        if (FP::defined(x2)) {
            if (x2 < tX.getScreenMin())
                x2 = tX.getScreenMin();
            else if (x2 > tX.getScreenMax())
                x2 = tX.getScreenMax();
        }
    } else {
        if (x1 < tX.getScreenMax()) {
            if (!FP::defined(x2) || x2 < tX.getScreenMax())
                return FP::undefined();
            x1 = tX.getScreenMax();
        } else if (x1 > tX.getScreenMin()) {
            if (!FP::defined(x2) || x2 > tX.getScreenMin())
                return FP::undefined();
            x1 = tX.getScreenMin();
        }
        if (FP::defined(x2)) {
            if (x2 < tX.getScreenMax())
                x2 = tX.getScreenMax();
            else if (x2 > tX.getScreenMin())
                x2 = tX.getScreenMin();
        }
    }

    if (tY.getScreenMin() < tY.getScreenMax()) {
        if (y1 < tY.getScreenMin()) {
            if (!FP::defined(y2) || y2 < tY.getScreenMin())
                return FP::undefined();
            y1 = tY.getScreenMin();
        } else if (y1 > tY.getScreenMax()) {
            if (!FP::defined(y2) || y2 > tY.getScreenMax())
                return FP::undefined();
            y1 = tY.getScreenMax();
        }
        if (FP::defined(y2)) {
            if (y2 < tY.getScreenMin())
                y2 = tY.getScreenMin();
            else if (y2 > tY.getScreenMax())
                y2 = tY.getScreenMax();
        }
    } else {
        if (y1 < tY.getScreenMax()) {
            if (!FP::defined(y2) || y2 < tY.getScreenMax())
                return FP::undefined();
            y1 = tY.getScreenMax();
        } else if (y1 > tY.getScreenMin()) {
            if (!FP::defined(y2) || y2 > tY.getScreenMin())
                return FP::undefined();
            y1 = tY.getScreenMin();
        }
        if (FP::defined(y2)) {
            if (y2 < tY.getScreenMax())
                y2 = tY.getScreenMax();
            else if (y2 > tY.getScreenMin())
                y2 = tY.getScreenMin();
        }
    }

    if (FP::defined(x2)) {
        if (x2 < x1)
            qSwap(x1, x2);
    } else {
        x2 = x1;
    }
    if (FP::defined(y2)) {
        if (y2 < y1)
            qSwap(y1, y2);
    } else {
        y2 = y1;
    }

    cx = x1;
    if (point.x() > cx) {
        if (point.x() < x2)
            cx = point.x();
        else
            cx = x2;
    }
    cy = y1;
    if (point.y() > cy) {
        if (point.y() < y2)
            cy = point.y();
        else
            cy = y2;
    }

    double d = cx - point.x();
    double distance = d * d;
    d = cy - point.y();
    distance += d * d;
    distance = sqrt(distance);
    if (distance > maximumPointNearDistance)
        return FP::undefined();
    return distance;
}

void GraphPainter2DBin::checkMouseoverPoint(double x1,
                                            double x2,
                                            double y1,
                                            double y2,
                                            const Bin2DPaintPoint &binPoint,
                                            Graph2DMouseoverComponentTraceBox::Location location,
                                            const QPointF &point,
                                            std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    double cx;
    double cy;
    double distance = pointDistance(x1, x2, y1, y2, point, cx, cy);
    if (!FP::defined(distance))
        return;

    if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(output.get())) {
        QPointF otherDelta(trace->getCenter() - point);
        if (sqrt(otherDelta.x() * otherDelta.x() + otherDelta.y() * otherDelta.y()) < distance)
            return;
    }

    output = std::shared_ptr<Graph2DMouseoverComponent>(
            new Graph2DMouseoverComponentTraceBox(QPointF(cx, cy), tracePtr, binPoint, location));
}

void GraphPainter2DBin::updateMouseover(const QPointF &point,
                                        std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    if (points.empty())
        return;

    for (const auto &it : points) {
        double center;
        double boxFirstEdge;
        double boxSecondEdge;
        if (vertical) {
            center = tX.toScreen(it.paintCenter, false);
            if (FP::defined(it.paintWidth) && FP::defined(it.paintCenter) && it.paintWidth >= 0.0) {
                boxFirstEdge = tX.toScreen(it.paintCenter - it.paintWidth * 0.5, false);
                boxSecondEdge = tX.toScreen(it.paintCenter + it.paintWidth * 0.5, false);
            } else {
                boxFirstEdge = FP::undefined();
                boxSecondEdge = FP::undefined();
            }
        } else {
            center = tY.toScreen(it.paintCenter, false);
            if (FP::defined(it.paintWidth) && FP::defined(it.paintCenter)) {
                boxFirstEdge = tY.toScreen(it.paintCenter - it.paintWidth * 0.5, false);
                boxSecondEdge = tY.toScreen(it.paintCenter + it.paintWidth * 0.5, false);
            } else {
                boxFirstEdge = FP::undefined();
                boxSecondEdge = FP::undefined();
            }
        }

        if (FP::defined(it.paintWidth) && it.paintWidth < 0.0) {
            if (FP::defined(center)) {
                boxFirstEdge = center + it.paintWidth * 0.5;
                boxSecondEdge = center - it.paintWidth * 0.5;
            } else {
                boxFirstEdge = FP::undefined();
                boxSecondEdge = FP::undefined();
            }
        }

        if (!FP::defined(boxFirstEdge))
            boxFirstEdge = center;
        if (!FP::defined(boxSecondEdge))
            boxSecondEdge = center;

        if (vertical) {
            checkMouseoverPoint(center, FP::undefined(), tY.toScreen(it.lowest[0], false),
                                FP::undefined(), it, Graph2DMouseoverComponentTraceBox::Lowest,
                                point, output);
            checkMouseoverPoint(boxFirstEdge, boxSecondEdge, tY.toScreen(it.lower[0], false),
                                FP::undefined(), it, Graph2DMouseoverComponentTraceBox::Lower,
                                point, output);
            checkMouseoverPoint(boxFirstEdge, boxSecondEdge, tY.toScreen(it.middle[0], false),
                                FP::undefined(), it, Graph2DMouseoverComponentTraceBox::Middle,
                                point, output);
            checkMouseoverPoint(boxFirstEdge, boxSecondEdge, tY.toScreen(it.upper[0], false),
                                FP::undefined(), it, Graph2DMouseoverComponentTraceBox::Upper,
                                point, output);
            checkMouseoverPoint(center, FP::undefined(), tY.toScreen(it.uppermost[0], false),
                                FP::undefined(), it, Graph2DMouseoverComponentTraceBox::Uppermost,
                                point, output);
        } else {
            checkMouseoverPoint(tX.toScreen(it.lowest[0], false), FP::undefined(), center,
                                FP::undefined(), it, Graph2DMouseoverComponentTraceBox::Lowest,
                                point, output);
            checkMouseoverPoint(tY.toScreen(it.lower[0], false), FP::undefined(), boxFirstEdge,
                                boxSecondEdge, it, Graph2DMouseoverComponentTraceBox::Lower, point,
                                output);
            checkMouseoverPoint(tX.toScreen(it.middle[0], false), FP::undefined(), boxFirstEdge,
                                boxSecondEdge, it, Graph2DMouseoverComponentTraceBox::Middle, point,
                                output);
            checkMouseoverPoint(tX.toScreen(it.upper[0], false), FP::undefined(), boxFirstEdge,
                                boxSecondEdge, it, Graph2DMouseoverComponentTraceBox::Upper, point,
                                output);
            checkMouseoverPoint(tX.toScreen(it.uppermost[0], false), FP::undefined(), center,
                                FP::undefined(), it, Graph2DMouseoverComponentTraceBox::Uppermost,
                                point, output);
        }
    }
}

void GraphPainter2DBin::updateMouseClick(const QPointF &,
                                         std::shared_ptr<Graph2DMouseoverComponent> &)
{ }

/**
 * Create a new indicator painter.
 * 
 * @param setPoints         the true points to paint
 * @param setTransformer    the position transformer
 * @param setBaseline       the axis baseline
 * @param setTotalDepth     the total depth of the area
 * @param setColor          the color to paint with
 * @param setContinuity     the maximum continuity gap time
 * @param priority          the draw sort priority
 */
GraphPainter2DIndicator::GraphPainter2DIndicator(std::vector<IndicatorPoint<1> > setPoints,
                                                 const AxisTransformer &setTransformer,
                                                 qreal setBaseline,
                                                 qreal setTotalDepth,
                                                 const AxisIndicator2D &setIndicator,
                                                 const QColor &setColor,
                                                 double setContinuity,
                                                 quintptr setTracePtr,
                                                 int priority) : Graph2DDrawPrimitive(priority),
                                                                 points(std::move(setPoints)),
                                                                 transformer(setTransformer),
                                                                 baseline(setBaseline),
                                                                 totalDepth(setTotalDepth),
                                                                 indicator(setIndicator),
                                                                 color(setColor),
                                                                 continuity(setContinuity),
                                                                 tracePtr(setTracePtr)
{ }

GraphPainter2DIndicator::~GraphPainter2DIndicator() = default;

void GraphPainter2DIndicator::paint(QPainter *painter)
{
    if (points.empty())
        return;
    if (indicator.getType() == AxisIndicator2D::Unselected)
        return;

    painter->save();

    qreal screenMin = transformer.getScreenMin();
    qreal screenMax = transformer.getScreenMax();
    if (screenMax < screenMin)
        qSwap(screenMin, screenMax);

    switch (indicator.getPosition()) {
    case Axis2DTop:
        painter->setClipRect(QRectF(screenMin, baseline, screenMax - screenMin, totalDepth),
                             Qt::IntersectClip);
        break;
    case Axis2DBottom:
        painter->setClipRect(
                QRectF(screenMin, baseline - totalDepth, screenMax - screenMin, totalDepth),
                Qt::IntersectClip);
        break;
    case Axis2DLeft:
        painter->setClipRect(QRectF(baseline, screenMin, totalDepth, screenMax - screenMin),
                             Qt::IntersectClip);
        break;
    case Axis2DRight:
        painter->setClipRect(
                QRectF(baseline - totalDepth, screenMin, totalDepth, screenMax - screenMin),
                Qt::IntersectClip);
        break;
    }

    QBrush brush(color);
    painter->setBrush(brush);
    painter->setPen(QPen(brush, 1));

    double min = FP::undefined();
    double max = 0;
    for (auto it = points.cbegin(), end = points.cend(); it != end; ++it) {
        if (FP::defined(min) && !pointsContiguous((it - 1)->end, it->start, continuity)) {
            indicator.paint(min, max, painter, baseline, totalDepth);
            min = FP::undefined();
        }

        double v = transformer.toScreen(it->d[0], false);
        if (!FP::defined(min)) {
            min = v;
            max = v;
            continue;
        }
        if (!FP::defined(v)) {
            indicator.paint(min, max, painter, baseline, totalDepth);
            min = FP::undefined();
            continue;
        }

        if (min > v) min = v;
        if (max < v) max = v;
    }
    if (FP::defined(min)) {
        indicator.paint(min, max, painter, baseline, totalDepth);
    }

    painter->restore();
}

void GraphPainter2DIndicator::updateMouseover(const QPointF &point,
                                              std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    if (points.empty())
        return;
    if (indicator.getType() == AxisIndicator2D::Unselected)
        return;

    double distance = maximumPointNearDistance * maximumPointNearDistance;
    auto found = points.cend();
    QPointF selectedPoint;
    qreal insetBegin;
    qreal insetEnd;
    indicator.getInsets(baseline, totalDepth, insetBegin, insetEnd);
    for (auto it = points.cbegin(), end = points.cend(); it != end; ++it) {

        double v = transformer.toScreen(it->d[0], false);
        if (!FP::defined(v))
            continue;
        if (transformer.screenClipped(v))
            continue;

        double x = 0;
        double y = 0;
        switch (indicator.getPosition()) {
        case Axis2DTop:
            y = point.y();
            if (y < insetBegin) {
                y = insetBegin;
            } else if (y > insetEnd) {
                y = insetEnd;
            }
            x = v;
            break;
        case Axis2DBottom:
            y = point.y();
            if (y > insetBegin) {
                y = insetBegin;
            } else if (y < insetEnd) {
                y = insetEnd;
            }
            x = v;
            break;
        case Axis2DLeft:
            x = point.x();
            if (x < insetBegin) {
                x = insetBegin;
            } else if (x > insetEnd) {
                x = insetEnd;
            }
            y = v;
            break;
        case Axis2DRight:
            x = point.x();
            if (x > insetBegin) {
                x = insetBegin;
            } else if (x < insetEnd) {
                x = insetEnd;
            }
            y = v;
            break;
        }

        double d = x - point.x();
        double targetDistance = d * d;
        d = y - point.y();
        targetDistance += d * d;
        if (distance < targetDistance)
            continue;
        distance = targetDistance;
        found = it;
        selectedPoint = QPointF((qreal) x, (qreal) y);
    }
    if (found == points.cend())
        return;
    distance = sqrt(distance);

    if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(output.get())) {
        QPointF otherDelta(trace->getCenter() - point);
        if (sqrt(otherDelta.x() * otherDelta.x() + otherDelta.y() * otherDelta.y()) < distance)
            return;
    }

    output = std::shared_ptr<Graph2DMouseoverComponent>(
            new Graph2DMouseoverComponentIndicator(selectedPoint, tracePtr, *found,
                                                   indicator.getPosition()));
}

/**
 * Create a new line painter.
 * 
 * @param s1            the first point
 * @param s2            the second point
 * @param setLineWidth  the line width
 * @param setLineStyle  the line style
 * @param setColor      the line color
 * @param priority      the draw sort priority
 */
GraphPainter2DSimpleLine::GraphPainter2DSimpleLine(const QPointF &s1,
                                                   const QPointF &s2,
                                                   qreal setLineWidth,
                                                   Qt::PenStyle setLineStyle,
                                                   const QColor &setColor,
                                                   int priority) : Graph2DDrawPrimitive(priority),
                                                                   p1(s1),
                                                                   p2(s2),
                                                                   lineWidth(setLineWidth),
                                                                   lineStyle(setLineStyle),
                                                                   color(setColor)
{ }

GraphPainter2DSimpleLine::~GraphPainter2DSimpleLine() = default;

void GraphPainter2DSimpleLine::paint(QPainter *painter)
{
    painter->save();
    QBrush brush(color);
    painter->setBrush(brush);
    painter->setPen(QPen(brush, lineWidth, lineStyle));
    painter->drawLine(p1, p2);
    painter->restore();
}


/**
 * Create a new title painter.
 * 
 * @param setText       the text to draw
 * @param setTop        the top center of the text
 * @param setFont       the font
 * @param setColor      the color
 * @param priority      the draw sort priority
 */
GraphPainter2DTitle::GraphPainter2DTitle(const QString &setText,
                                         const QPointF &setTop,
                                         const QFont &setFont,
                                         const QColor &setColor,
                                         int priority) : Graph2DDrawPrimitive(priority),
                                                         text(setText),
                                                         top(setTop),
                                                         font(setFont),
                                                         color(setColor)
{ }

GraphPainter2DTitle::~GraphPainter2DTitle()
{ }

void GraphPainter2DTitle::paint(QPainter *painter)
{
    painter->save();
    QBrush brush(color);
    painter->setBrush(brush);
    painter->setPen(QPen(brush, 1));
    painter->setFont(font);
    auto textPos = GUIStringLayout::alignTextTop(painter, {top, QSizeF(0, 0)},
                                                 Qt::TextDontClip | Qt::AlignHCenter, text);
    painter->drawText(textPos, Qt::TextDontClip, text);
    painter->restore();
}

void GraphPainter2DTitle::updateMouseClick(const QPointF &point,
                                           std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    QFontMetrics fm(font);
    qreal height = GUIStringLayout::maximumCharacterDimensions(text, fm).height();
    if (point.y() > height)
        return;
    qreal width = fm.boundingRect(text).width();
    if (qAbs(point.x() - top.x()) > width / 2)
        return;
    output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverTitle());
}


/**
 * Create a new legend painter.
 * 
 * @param setLegend         the legend to paint
 * @param setLegendOrigin   the origin point of the legend
 * @param setBackground     the background rectangle to paint
 * @param setBackgroundLineWidth the line width of the background outline
 * @param setBackgroundLineStyle the line style of the background outline
 * @param setBackgroundFill the fill color of the background
 * @param setBackgroundOutline the outline color of the background
 * @param priority          the draw sort priority
 */
GraphPainter2DLegend::GraphPainter2DLegend(const Legend &setLegend,
                                           const QPointF &setLegendOrigin,
                                           const QRectF &setBackground,
                                           qreal setBackgroundLineWidth,
                                           Qt::PenStyle setBackgroundLineStyle,
                                           const QColor &setBackgroundFill,
                                           const QColor &setBackgroundOutline,
                                           int priority) : Graph2DDrawPrimitive(priority),
                                                           legend(setLegend),
                                                           legendOrigin(setLegendOrigin),
                                                           background(setBackground),
                                                           backgroundLineWidth(
                                                                   setBackgroundLineWidth),
                                                           backgroundLineStyle(
                                                                   setBackgroundLineStyle),
                                                           backgroundFill(setBackgroundFill),
                                                           backgroundOutline(setBackgroundOutline)
{ }

GraphPainter2DLegend::~GraphPainter2DLegend()
{ }

void GraphPainter2DLegend::paint(QPainter *painter)
{
    painter->save();

    if (background.width() > 0 && background.height() > 0) {
        painter->setBrush(QBrush(backgroundFill));
        painter->setPen(QPen(QBrush(backgroundOutline), backgroundLineWidth, backgroundLineStyle));
        painter->drawRect(background);
    }

    painter->translate(legendOrigin);
    legend.paint(painter);

    painter->restore();
}

void GraphPainter2DLegend::updateMouseover(const QPointF &point,
                                           std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    QSizeF legendSize(legend.getSize());
    if (point.x() >= legendOrigin.x() &&
            point.x() <= legendOrigin.x() + legendSize.width() &&
            point.y() >= legendOrigin.y() &&
            point.y() <= legendOrigin.y() + legendSize.height()) {
        if (auto item = legend.itemAt(point.x() - legendOrigin.x(), point.y() - legendOrigin.y())) {
            output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverLegendItem(
                    std::static_pointer_cast<GraphLegendItem2D>(item)));
            return;
        }
    }

    if (background.width() > 0 && background.height() > 0) {
        if (background.contains(point)) {
            output.reset();
            return;
        }
    }
}

void GraphPainter2DLegend::updateMouseClick(const QPointF &point,
                                            std::shared_ptr<Graph2DMouseoverComponent> &output)
{
    QSizeF legendSize(legend.getSize());
    if (point.x() >= legendOrigin.x() &&
            point.x() <= legendOrigin.x() + legendSize.width() &&
            point.y() >= legendOrigin.y() &&
            point.y() <= legendOrigin.y() + legendSize.height()) {
        if (auto item = legend.itemAt(point.x() - legendOrigin.x(), point.y() - legendOrigin.y())) {
            output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverLegendItem(
                    std::static_pointer_cast<GraphLegendItem2D>(item)));
            return;
        }
    }

    if (background.width() > 0 && background.height() > 0) {
        if (background.contains(point)) {
            output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverLegend());
            return;
        }
    }
}


GraphPainter2DHighlight::Region::Region()
        : tX(),
          tY(),
          minX(FP::undefined()),
          maxX(FP::undefined()),
          minY(FP::undefined()),
          maxY(FP::undefined()),
          color()
{ }

GraphPainter2DHighlight::Region::Region(const Region &) = default;

GraphPainter2DHighlight::Region &GraphPainter2DHighlight::Region::operator=(const Region &) = default;

GraphPainter2DHighlight::Region::Region(Region &&) = default;

GraphPainter2DHighlight::Region &GraphPainter2DHighlight::Region::operator=(Region &&) = default;

/**
 * Create a new highlight region.
 * 
 * @param x         the x transformer
 * @param y         the y transformer
 * @param setMinX   the the minimum X coordinate
 * @param setMaxX   the the maximum X coordinate
 * @param setMinY   the the minimum Y coordinate
 * @param setMaxY   the the maximum Y coordinate
 * @param setColor  the color to draw with
 */
GraphPainter2DHighlight::Region::Region(const AxisTransformer &x,
                                        const AxisTransformer &y,
                                        double setMinX,
                                        double setMaxX,
                                        double setMinY,
                                        double setMaxY,
                                        const QColor &setColor) : tX(x),
                                                                  tY(y),
                                                                  minX(setMinX),
                                                                  maxX(setMaxX),
                                                                  minY(setMinY),
                                                                  maxY(setMaxY),
                                                                  color(setColor)
{ }

/**
 * Create a new legend painter.
 * 
 * @param setRegions    the regions to draw
 * @param priority      the draw sort priority
 */
GraphPainter2DHighlight::GraphPainter2DHighlight(std::vector<Region> setRegions, int priority)
        : Graph2DDrawPrimitive(priority), regions(std::move(setRegions))
{ }

GraphPainter2DHighlight::~GraphPainter2DHighlight() = default;

void GraphPainter2DHighlight::paint(QPainter *painter)
{
    if (regions.empty())
        return;

    painter->save();

    std::vector<std::uint8_t> hitRegions(regions.size(), false);
    for (;;) {
        std::ptrdiff_t first = -1;
        QPainterPath path;
        /* So intersecting regions add rather than subtract */
        path.setFillRule(Qt::WindingFill);

        for (auto region = regions.begin(); region != regions.end(); ++region) {
            auto idx = region - regions.begin();
            if (hitRegions[idx])
                continue;
            if (first == -1) {
                first = idx;
            } else {
                if (region->color != regions[first].color)
                    continue;
            }
            hitRegions[idx] = true;

            QRectF add;

            if (!FP::defined(region->minY) && !FP::defined(region->maxY)) {
                if (region->tY.getScreenMin() < region->tY.getScreenMax()) {
                    add.setTop(region->tY.getScreenMin());
                    add.setBottom(region->tY.getScreenMax());
                } else {
                    add.setTop(region->tY.getScreenMax());
                    add.setBottom(region->tY.getScreenMin());
                }
            } else if (!FP::defined(region->minY)) {
                double x = region->tY.toScreen(region->maxY, false);
                if (!FP::defined(x))
                    continue;
                if (x < region->tY.getScreenMin()) {
                    add.setTop(x);
                    add.setBottom(region->tY.getScreenMin());
                } else {
                    add.setBottom(x);
                    add.setTop(region->tY.getScreenMin());
                }
            } else if (!FP::defined(region->maxY)) {
                double x = region->tY.toScreen(region->minY, false);
                if (!FP::defined(x))
                    continue;
                if (x < region->tY.getScreenMax()) {
                    add.setTop(x);
                    add.setBottom(region->tY.getScreenMax());
                } else {
                    add.setBottom(x);
                    add.setTop(region->tY.getScreenMax());
                }
            } else {
                double x1 = region->tY.toScreen(region->minY, false);
                if (!FP::defined(x1))
                    continue;
                double x2 = region->tY.toScreen(region->maxY, false);
                if (!FP::defined(x2))
                    continue;
                if (x1 < x2) {
                    add.setTop(x1);
                    add.setBottom(x2);
                } else if (x1 == x2) {
                    add.setTop(x1);
                    add.setHeight(1E-6);
                } else {
                    add.setBottom(x1);
                    add.setTop(x2);
                }
            }

            if (!FP::defined(region->minX) && !FP::defined(region->maxX)) {
                if (region->tX.getScreenMin() < region->tX.getScreenMax()) {
                    add.setLeft(region->tX.getScreenMin());
                    add.setRight(region->tX.getScreenMax());
                } else {
                    add.setLeft(region->tX.getScreenMax());
                    add.setRight(region->tX.getScreenMin());
                }
            } else if (!FP::defined(region->minX)) {
                double x = region->tX.toScreen(region->maxX, false);
                if (!FP::defined(x))
                    continue;
                if (x < region->tX.getScreenMin()) {
                    add.setLeft(x);
                    add.setRight(region->tX.getScreenMin());
                } else {
                    add.setRight(x);
                    add.setLeft(region->tX.getScreenMin());
                }
            } else if (!FP::defined(region->maxX)) {
                double x = region->tX.toScreen(region->minX, false);
                if (!FP::defined(x))
                    continue;
                if (x < region->tX.getScreenMax()) {
                    add.setLeft(x);
                    add.setRight(region->tX.getScreenMax());
                } else {
                    add.setRight(x);
                    add.setLeft(region->tX.getScreenMax());
                }
            } else {
                double x1 = region->tX.toScreen(region->minX, false);
                if (!FP::defined(x1))
                    continue;
                double x2 = region->tX.toScreen(region->maxX, false);
                if (!FP::defined(x2))
                    continue;
                if (x1 < x2) {
                    add.setLeft(x1);
                    add.setRight(x2);
                } else if (x1 == x2) {
                    add.setLeft(x1);
                    add.setWidth(1E-6);
                } else {
                    add.setRight(x1);
                    add.setLeft(x2);
                }
            }

            path.addRect(add);
            painter->setClipRect(transformersToClippingRect(region->tX, region->tY),
                                 Qt::IntersectClip);
        }

        if (first == -1)
            break;

        if (!path.isEmpty()) {
            QBrush brush(regions[first].color);
            painter->setBrush(brush);
            painter->setPen(QPen(brush, 0));
            painter->drawPath(path.simplified());
        }
    }

    painter->restore();
}


}
}
