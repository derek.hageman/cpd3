/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>
#include <QToolTip>
#include <QMenu>
#include <QDialog>
#include <QGridLayout>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QLabel>
#include <QImageWriter>
#include <QSvgGenerator>
#include <QMessageBox>
#include <QShortcut>
#include <QPushButton>
#include <QColorDialog>

#ifdef HAVE_PRINTING

#include <QPrinter>
#include <QPrintDialog>
#include <io/drivers/file.hxx>

#endif

#include "graphing/displaywidget.hxx"
#include "graphing/displaylayout.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/sequencematch.hxx"
#include "displaywidget.hxx"


using namespace CPD3::Data;
using namespace CPD3::GUI;
using namespace CPD3::Graphing::Internal;

namespace CPD3 {
namespace Graphing {

/** @file graphing/displaywidget.hxx
 * The base widget for drawing displays.
 */

namespace Internal {

class DisplayWidgetPrivate {
public:
    DisplayWidget *parent;

    std::unique_ptr<Display> display;
    DisplayDynamicContext context;
    QSize minimumSize;

    QTimer timeoutTimer;
    QTimer toolTipTimer;
    QTimer interactiveThrottle;
    QTimer interactiveQueued;

    bool resetSelectsAll;
    DisplayWidget::SmoothingType smoothingType;
    bool contaminationRemoved;


    DisplayWidgetPrivate(DisplayWidget *setParent) : parent(setParent),
                                                     resetSelectsAll(false),
                                                     smoothingType(
                                                             DisplayWidget::Smoothing_Disabled),
                                                     contaminationRemoved(false)
    {

        context.setInteractive(true);
        parent->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        timeoutTimer.setSingleShot(true);
        timeoutTimer.setInterval(10000);
        QObject::connect(&timeoutTimer, SIGNAL(timeout()), parent, SLOT(interactionTimeout()));

        toolTipTimer.setSingleShot(true);
        toolTipTimer.setInterval(10000);

        interactiveThrottle.setSingleShot(true);
        interactiveThrottle.setInterval(500);
        interactiveQueued.setSingleShot(true);
        interactiveQueued.setInterval(500);
        QObject::connect(&interactiveQueued, SIGNAL(timeout()), parent, SLOT(update()));

        parent->setFocusPolicy(Qt::NoFocus);
        parent->setContextMenuPolicy(Qt::NoContextMenu);
        parent->setMouseTracking(false);
    }

    virtual ~DisplayWidgetPrivate() = default;

    void displayUpdated()
    {
        toolTipTimer.disconnect();

        if (!display) {
            minimumSize = QSize();
            parent->setFocusPolicy(Qt::NoFocus);
            parent->setContextMenuPolicy(Qt::NoContextMenu);
            parent->setMouseTracking(false);
            return;
        }

        minimumSize = display->getMinimumSize(NULL, context).toSize();

        if (display->acceptsKeyboardInput()) {
            parent->setFocusPolicy(Qt::ClickFocus);
        } else {
            parent->setFocusPolicy(Qt::NoFocus);
        }
        parent->setContextMenuPolicy(Qt::CustomContextMenu);

        if (display->acceptsMouseover())
            parent->setMouseTracking(true);
        else
            parent->setMouseTracking(false);

        QObject::connect(display.get(), &Display::interactiveRepaint, parent,
                         &DisplayWidget::interactiveRepaint, Qt::QueuedConnection);
        QObject::connect(display.get(), &Display::timeRangeSelected, parent,
                         &DisplayWidget::timeRangeSelected);
        QObject::connect(display.get(), &Display::setMouseTracking, parent,
                         &DisplayWidget::displayMouseTracking);
        QObject::connect(display.get(), &Display::setMouseGrab, parent,
                         &DisplayWidget::setMouseGrab);
        QObject::connect(display.get(), &Display::setKeyboardGrab, parent,
                         &DisplayWidget::setKeyboardGrab);
        QObject::connect(display.get(), &Display::setToolTip, parent,
                         &DisplayWidget::changeToolTip);
        QObject::connect(display.get(), &Display::internalZoom, parent,
                         &DisplayWidget::internalZoom);
        {
            auto target = parent;
            QObject::connect(display.get(), &Display::readyForFinalPaint, target,
                             [target]() { target->update(); }, Qt::QueuedConnection);
        }
    }

    virtual void setDisplay(std::unique_ptr<Display> &&set)
    {
        display = std::move(set);
        displayUpdated();
        parent->update();
    }

    virtual void registerChain(DisplayWidgetTapChain *chain)
    {
        if (!display)
            return;
        chain->setSmoothing(smoothingType, contaminationRemoved);
        display->registerChain(chain);
        chain->clearSmoothing();
        parent->update();
    }

    void setContext(const DisplayDynamicContext &context)
    {
        this->context = context;
        this->context.setInteractive(true);
    }

    virtual std::vector<std::shared_ptr<DisplayHighlightController>> getHighlightControllers()
    {
        if (!display)
            return {};
        auto add = display->highlightController();
        if (!add)
            return {};
        return {std::move(add)};
    }

    std::vector<std::shared_ptr<DisplayOutput> > getOutputs()
    {
        if (!display)
            return {};
        return display->getOutputs(context);
    }

    virtual void paint(QPainter *painter)
    {
        painter->fillRect(QRectF(0, 0, parent->width(), parent->height()), QBrush(Qt::white));
        display->paint(painter, QRectF(1, 1, parent->width() - 2, parent->height() - 2), context);
    }

    void releaseFocus()
    {
        parent->releaseKeyboard();
        parent->releaseMouse();
        display->interactionStopped();
    }

    virtual bool keyPressEvent(QKeyEvent *event)
    {
        timeoutTimer.start();
        if (display) {
            if (event->key() == Qt::Key_Escape) {
                releaseFocus();
            } else if (display->eventKeyPress(event)) {
                return true;
            }
        }
        return false;
    }

    virtual bool keyReleaseEvent(QKeyEvent *event)
    {
        timeoutTimer.start();
        if (display) {
            if (display->eventKeyRelease(event))
                return true;
        }
        return false;
    }

    virtual bool mouseMoveEvent(QMouseEvent *event)
    {
        timeoutTimer.start();
        if (display) {
            QRect area(1, 1, parent->width() - 2, parent->height() - 2);
            if (area.contains(event->pos())) {
                if (display->acceptsMouseover()) {
                    toolTipTimer.start();
                    display->setMouseover(event->pos());
                }
                if (display->eventMouseMove(event))
                    return true;
            } else {
                toolTipTimer.disconnect();
                toolTipTimer.stop();
                display->clearMouseover();
            }
        }
        return false;
    }

    virtual bool mousePressEvent(QMouseEvent *event)
    {
        timeoutTimer.start();
        if (display) {
            if (display->eventMousePress(event))
                return true;
        }
        return false;
    }

    virtual bool mouseReleaseEvent(QMouseEvent *event)
    {
        timeoutTimer.start();
        if (display) {
            if (display->eventMouseRelease(event))
                return true;
        }
        return false;
    }

    virtual void interactionTimeout()
    {
        if (!display)
            return;
        parent->releaseKeyboard();
        parent->releaseMouse();
        display->interactionStopped();
    }

    virtual void displayMouseTracking(bool enable)
    {
        if (display && display->acceptsMouseover())
            return;
        parent->setMouseTracking(enable);
    }

    virtual void leaveEvent(QEvent *)
    {
        if (display) {
            if (display->acceptsMouseover()) {
                toolTipTimer.disconnect();
                toolTipTimer.stop();
                display->clearMouseover();
            }
        }
    }

    virtual void setMouseGrab(bool enable)
    {
        if (enable) {
            timeoutTimer.start();
            parent->grabMouse();
        } else {
            parent->releaseMouse();
        }
    }

    virtual void setKeyboardGrab(bool enable)
    {
        if (enable) {
            timeoutTimer.start();
            parent->grabKeyboard();
        } else {
            parent->releaseKeyboard();
        }
    }

    void setVisibleTimeRange(double start, double end)
    {
        if (!display)
            return;
        display->setVisibleTimeRange(start, end);
        parent->update();
    }

    virtual void changeToolTip(const QPoint &point, const QString &text)
    {
        QToolTip::showText(QPoint(), QString());
        if (display && !text.isEmpty()) {
            QPoint translated(parent->mapToGlobal(point));
            QToolTip::showText(translated, text, parent,
                               QRect(translated.x() - 10, translated.y() - 10, 20, 20));

            /* Logic from Qt core */
            int time = 10000 + 40 * qMax(0, text.length() - 100);
            toolTipTimer.disconnect();
            QObject::connect(&toolTipTimer, SIGNAL(timeout()), display.get(),
                             SLOT(clearMouseover()));
            toolTipTimer.start(time);
        }
    }

    bool getResetSelectsAll() const
    { return resetSelectsAll; }

    virtual void setResetSelectsAll(bool set)
    {
        resetSelectsAll = set;
    }

    DisplayWidget::SmoothingType getSmoothingType() const
    { return smoothingType; }

    virtual void setSmoothingType(DisplayWidget::SmoothingType set)
    {
        smoothingType = set;
        parent->emitChainReload();
    }

    bool getContaminationRemoved() const
    { return contaminationRemoved; }

    virtual void setContaminationRemoved(bool set)
    {
        contaminationRemoved = set;
        parent->emitChainReload();
    }

    virtual void setSmoothing(DisplayWidget::SmoothingType type, bool contam)
    {
        smoothingType = type;
        contaminationRemoved = contam;
        parent->emitChainReload();
    }

    virtual void setDisplaySmoothing(DisplaySaveContext &save)
    {
        Variant::Write smoothing = Variant::Write::empty();
        DisplayWidget::modifyConfigForSmoother(smoothingType, contaminationRemoved, smoothing);
        save.setTraceDispatchSmoothing(display.get(), smoothing);
    }

    ValueSegment::Transfer exportChanges()
    {
        DisplaySaveContext save;
        setDisplaySmoothing(save);
        display->saveOverrides(save);
        return save.result();
    }

    virtual void contextMenu(const QPoint &point)
    {
        if (!display)
            return;

        QMenu menu(parent);
        QObject::connect(display.get(), SIGNAL(outputsChanged()), &menu, SLOT(close()));

        DisplayWidgetContextDisplay
                *displayContext = new DisplayWidgetContextDisplay(parent, &menu);
        QAction *act;

        bool isChildDisplay = (qobject_cast<DisplayWidget *>(parent->parent()) != 0);

        auto outputs = display->getOutputs(context);
        auto mouseModifier = display->getMouseModification(point, QRectF(0, 0, parent->width(),
                                                                         parent->height()),
                                                           context);

        bool anyChangable = false;

        if (mouseModifier) {
            QList<QAction *> actions(mouseModifier->getMenuActions(&menu));
            if (!actions.isEmpty()) {
                QMenu *outputMenu = new QMenu(mouseModifier->getTitle(), &menu);
                outputMenu->setToolTip(
                        parent->tr("Change settings for the part of the display under the mouse."));
                outputMenu->setStatusTip(parent->tr("Display component settings"));
                outputMenu->setWhatsThis(parent->tr(
                        "This menu provides configuration options for the part of the display that was clicked on."));
                menu.addMenu(outputMenu);
                anyChangable = true;

                for (QList<QAction *>::const_iterator action = actions.constBegin(),
                        end = actions.constEnd(); action != end; ++action) {
                    (*action)->setParent(outputMenu);
                    outputMenu->addAction(*action);
                }
            }
        }

        for (const auto &output : outputs) {
            auto actions = output->getMenuActions(&menu);
            if (actions.isEmpty())
                continue;

            QMenu *outputMenu = new QMenu(output->getTitle(), &menu);
            outputMenu->setToolTip(parent->tr("Change settings for part of the display."));
            outputMenu->setStatusTip(parent->tr("Display component settings"));
            outputMenu->setWhatsThis(parent->tr(
                    "This menu provides configuration options for a component of the main display."));
            menu.addMenu(outputMenu);
            anyChangable = true;

            for (auto action : actions) {
                action->setParent(outputMenu);
                outputMenu->addAction(action);
            }
        }

        /* Make this conditional? (On what?) */
        {
            QMenu *smoothingMenu = new QMenu(parent->tr("S&moothing", "Context|Smoothing"), &menu);
            smoothingMenu->setToolTip(parent->tr("Change display smoothing settings."));
            smoothingMenu->setStatusTip(parent->tr("Display smoothing settings"));
            smoothingMenu->setWhatsThis(parent->tr(
                    "This menu provides allows additional smoothing to be used on the display."));
            menu.addMenu(smoothingMenu);
            anyChangable = true;
            parent->buildSmoothingMenu(smoothingMenu, displayContext,
                                       SLOT(changeSmoothing(DisplayWidget::SmoothingType, bool)),
                                       parent->getSmoothingType(),
                                       parent->getContaminationRemoved());
        }

        if (isChildDisplay) {
            QMenu *smoothingMenu =
                    new QMenu(parent->tr("All Sm&oothing", "Context|AllSmoothing"), &menu);
            smoothingMenu->setToolTip(
                    parent->tr("Change smoothing settings for all visible displays."));
            smoothingMenu->setStatusTip(parent->tr("Display smoothing settings"));
            smoothingMenu->setWhatsThis(parent->tr(
                    "This menu provides allows additional smoothing to be applied to all visible displays."));
            menu.addMenu(smoothingMenu);
            anyChangable = true;

            DisplayWidget::SmoothingType type = parent->getSmoothingType();
            bool contam = parent->getContaminationRemoved();
            DisplayWidget *widget = qobject_cast<DisplayWidget *>(parent->parent());
            if (widget != 0) {
                if (type != widget->getSmoothingType()) {
                    type = (DisplayWidget::SmoothingType) (-1);
                    contam = false;
                }
                if (contam != widget->getContaminationRemoved())
                    contam = false;
            }

            parent->buildSmoothingMenu(smoothingMenu, displayContext,
                                       SLOT(changeAllSmoothing(DisplayWidget::SmoothingType, bool)),
                                       type, contam);
        }

        if (anyChangable) {
            act = new QAction(parent->tr("&Revert Changes", "Context|RevertChanges"), &menu);
            act->setToolTip(parent->tr("Revert any changes made to the display."));
            act->setStatusTip(parent->tr("Revert display changes"));
            act->setWhatsThis(parent->tr("This reverts any changes made to the display."));
            QObject::connect(act, SIGNAL(triggered()), display.get(), SLOT(resetOverrides()));
            menu.addAction(act);

            act = new QAction(parent->tr("&Save Changes", "Context|SaveChanges"), &menu);
            act->setToolTip(parent->tr("Save any changes made to the display."));
            act->setStatusTip(parent->tr("Save display changes"));
            act->setWhatsThis(parent->tr("This saves any changes made to the display."));
            QObject::connect(act, SIGNAL(triggered()), parent, SLOT(menuSaveChanges()));
            menu.addAction(act);
        }

        if (displayContext->hasZoom()) {
            if (!menu.isEmpty())
                menu.addSeparator();

            act = new QAction(parent->tr("Manual &Zoom", "Context|ManualZoom"), &menu);
            act->setToolTip(parent->tr("Manually specify the display zooming."));
            act->setStatusTip(parent->tr("Manually specify zoom"));
            act->setWhatsThis(parent->tr(
                    "This allows manual entry of the display zooming.  It also allows the displayed area to be propagated to other parts of the system."));
            QObject::connect(act, SIGNAL(triggered()), displayContext, SLOT(manualZoom()));
            menu.addAction(act);

            act = new QAction(parent->tr("&Reset Zoom", "Context|ResetZoom"), &menu);
            act->setToolTip(parent->tr("Reset all zooming applied to."));
            act->setStatusTip(parent->tr("Enable a part of the display"));
            act->setWhatsThis(parent->tr(
                    "This sets the enabled status of the output of the display.  A disabled output is not shown."));
            QObject::connect(act, SIGNAL(triggered()), displayContext, SLOT(resetZoom()));
            menu.addAction(act);
        }

        if (!menu.isEmpty())
            menu.addSeparator();

        act = new QAction(parent->tr("&Export Image", "Context|ExportImage"), &menu);
        act->setToolTip(parent->tr("Create an image file with the contents of the display."));
        act->setStatusTip(parent->tr("Export to an image file"));
        act->setWhatsThis(parent->tr("This creates an image file of the contents of the display."));
        QObject::connect(act, SIGNAL(triggered()), displayContext, SLOT(exportImage()));
        menu.addAction(act);

        if (isChildDisplay) {
            act = new QAction(parent->tr("Export &All", "Context|ExportAll"), &menu);
            act->setToolTip(parent->tr(
                    "Create an image file with the contents of all the visible displays."));
            act->setStatusTip(parent->tr("Export to an image file"));
            act->setWhatsThis(parent->tr(
                    "This creates an image file of the contents of all visible displays."));
            QObject::connect(act, SIGNAL(triggered()), displayContext, SLOT(exportImageAll()));
            menu.addAction(act);
        }

        act = new QAction(parent->tr("&Print", "Context|Print"), &menu);
        act->setToolTip(parent->tr("Print the contents of the display."));
        act->setStatusTip(parent->tr("Print"));
        act->setWhatsThis(parent->tr("This will open a printing dialog for the display."));
        QObject::connect(act, SIGNAL(triggered()), displayContext, SLOT(print()));
        menu.addAction(act);

        if (isChildDisplay) {
            act = new QAction(parent->tr("P&rint All", "Context|PrintAll"), &menu);
            act->setToolTip(parent->tr("Print the contents of all visible displays."));
            act->setStatusTip(parent->tr("Print"));
            act->setWhatsThis(
                    parent->tr("This will open a printing dialog for all visible displays."));
            QObject::connect(act, SIGNAL(triggered()), displayContext, SLOT(printAll()));
            menu.addAction(act);
        }

        act = new QAction(parent->tr("&Display in New Window", "Context|DisplayPopup"), &menu);
        act->setToolTip(parent->tr("Create a new window with a copy of the display."));
        act->setStatusTip(parent->tr("Display a copy of the display"));
        act->setWhatsThis(parent->tr(
                "This creates a copy of the display in a new pop up window.  The copied window is not connected to the original display and so does not share new data or zooms with it."));
        QObject::connect(act, SIGNAL(triggered()), displayContext, SLOT(displayPopup()));
        menu.addAction(act);

        if (isChildDisplay) {
            act = new QAction(parent->tr("Display All in &New Window", "Context|DisplayPopupAll"),
                              &menu);
            act->setToolTip(parent->tr("Create a new window with a copy of all visible displays."));
            act->setStatusTip(parent->tr("Display a copy of all displays"));
            act->setWhatsThis(parent->tr(
                    "This creates a copy of all visible displays in a new pop up window.  The copied window is not connected to the original displays and so does not share new data or zooms with it."));
            QObject::connect(act, SIGNAL(triggered()), displayContext, SLOT(displayPopupAll()));
            menu.addAction(act);
        }

        act = new QAction(parent->tr("Display &Full Screen", "Context|DisplayFullScreen"), &menu);
        act->setToolTip(parent->tr("Show this display in full screen mode."));
        act->setStatusTip(parent->tr("Display in full screen mode"));
        act->setWhatsThis(parent->tr("This shows the display in full screen mode."));
        QObject::connect(act, SIGNAL(triggered()), displayContext, SLOT(displayFullScreen()));
        menu.addAction(act);

        if (isChildDisplay) {
            act = new QAction(
                    parent->tr("Display All in Full &Screen", "Context|DisplayFullScreenAll"),
                    &menu);
            act->setToolTip(parent->tr("Show all visible displays in full screen mode."));
            act->setStatusTip(parent->tr("Show all displays in full screen mode"));
            act->setWhatsThis(parent->tr("This shows all visible displays in full screen mode."));
            QObject::connect(act, SIGNAL(triggered()), displayContext,
                             SLOT(displayFullScreenAll()));
            menu.addAction(act);
        }


        parent->releaseMouse();
        parent->releaseKeyboard();
        if (display->acceptsMouseover()) {
            toolTipTimer.disconnect();
            toolTipTimer.stop();
            display->clearMouseover();
        }
        display->interactionStopped();

        menu.exec(parent->mapToGlobal(point));

        if (display->acceptsMouseover()) {
            QRect area(1, 1, parent->width() - 2, parent->height() - 2);
            if (area.contains(point))
                display->setMouseover(point);
        }
    }

    virtual bool resizeEvent(QResizeEvent *event)
    {
        Q_UNUSED(event);
        return false;
    }

    virtual bool event(QEvent *event)
    {
        Q_UNUSED(event);
        return false;
    }
};

class DisplayWidgetPrivateLayout : public DisplayWidgetPrivate {
public:
    DisplayLayout *layout;
    std::vector<std::unique_ptr<DisplayWidget>> children;

    void releaseChildren()
    {
        /* Actual ownership is retained by the layout, so fudge this and make sure the
         * unique_ptrs are released */
        for (const auto &c : children) {
            c->p->display.release();
        }
    }

    void updateLayout()
    {
        if (!layout->haveEverPrepared) {
            layout->prepareLayout(QRectF(QPoint(0, 0), parent->size()), nullptr, context);
        }
        if (!layout->havePredicted) {
            layout->predictLayout(QRectF(QPoint(0, 0), parent->size()), nullptr, context);
        }
        layout->setPositions(QRectF(QPoint(0, 0), QSizeF(parent->size())));

        for (int i = 0, max = children.size(); i < max; i++) {
            QRect ng(layout->elements[i]->position.toRect());
            if (ng != children[i]->geometry()) {
                children[i]->setGeometry(ng);
                children[i]->update();
            }
        }
    }

    void setChildren()
    {
        releaseChildren();
        children.clear();

        if (!layout)
            return;

        for (const auto &it : layout->elements) {
            /* Ownership fudged here */
            children.emplace_back(
                    new DisplayWidget(std::unique_ptr<Display>(it->display.get()), parent));
            children.back()->setResetSelectsAll(parent->getResetSelectsAll());

            QObject::connect(children.back().get(), &DisplayWidget::requestChainReload, parent,
                             &DisplayWidget::requestChainReload);
            QObject::connect(children.back().get(), &DisplayWidget::timeRangeSelected, parent,
                             &DisplayWidget::timeRangeSelected);
            QObject::connect(children.back().get(), &DisplayWidget::internalZoom, parent,
                             &DisplayWidget::internalZoom);
            QObject::connect(children.back().get(), &DisplayWidget::visibleZoom, parent,
                             &DisplayWidget::combineVisibleZoom);
            QObject::connect(children.back().get(), &DisplayWidget::globalZoom, parent,
                             &DisplayWidget::combineGlobalZoom);
            QObject::connect(children.back().get(), &DisplayWidget::saveChanges, parent,
                             &DisplayWidget::combineSaveChanges);
            QObject::connect(it->display.get(), &Display::interactiveRepaint, parent,
                             &DisplayWidget::interactiveRepaint, Qt::QueuedConnection);
            {
                auto target = parent;
                QObject::connect(it->display.get(), &Display::readyForFinalPaint, target,
                                 [target]() { target->update(); }, Qt::QueuedConnection);
            }
        }
        updateLayout();
    }

    DisplayWidgetPrivateLayout(DisplayWidget *setParent) : DisplayWidgetPrivate(setParent)
    { }

    virtual ~DisplayWidgetPrivateLayout()
    { releaseChildren(); }

    void setDisplay(std::unique_ptr<Display> &&set) override
    {
        layout = qobject_cast<DisplayLayout *>(set.get());
        DisplayWidgetPrivate::setDisplay(std::move(set));
        setChildren();

        if (!display)
            return;
        display->disconnect();
    }

    void registerChain(DisplayWidgetTapChain *chain) override
    {
        if (!layout) {
            DisplayWidgetPrivate::registerChain(chain);
            return;
        }
        if (layout->elements.empty()) {
            layout->registerChain(chain);
            return;
        }
        layout->elementsReady = 0;
        for (const auto &it : children) {
            it->registerChain(chain);
        }
        parent->update();
    }

    void setResetSelectsAll(bool set) override
    {
        DisplayWidgetPrivate::setResetSelectsAll(set);
        for (const auto &it : children) {
            it->setResetSelectsAll(set);
        }
    }

    void setSmoothingType(DisplayWidget::SmoothingType set) override
    {
        for (const auto &it : children) {
            it->setSmoothingType(set);
        }
        DisplayWidgetPrivate::setSmoothingType(set);
    }

    void setContaminationRemoved(bool set) override
    {
        for (const auto &it : children) {
            it->setContaminationRemoved(set);
        }
        DisplayWidgetPrivate::setContaminationRemoved(set);
    }

    void setSmoothing(DisplayWidget::SmoothingType type, bool contam) override
    {
        for (const auto &it : children) {
            it->setSmoothing(type, contam);
        }
        DisplayWidgetPrivate::setSmoothing(type, contam);
    }

    void setDisplaySmoothing(DisplaySaveContext &save) override
    {
        for (const auto &it : children) {
            it->p->setDisplaySmoothing(save);
        }
        DisplayWidgetPrivate::setDisplaySmoothing(save);
    }

    void paint(QPainter *painter) override
    {
        QRectF dimensions(QRectF(QPoint(0, 0), parent->size()));
        painter->fillRect(dimensions, QBrush(Qt::white));
        layout->prepareLayout(dimensions, painter->device(), context);
        layout->predictLayout(dimensions, painter->device(), context);
        layout->setPositions(dimensions);
        for (std::size_t i = 0, max = children.size(); i < max; i++) {
            QRect ng(layout->elements[i]->position.toRect());
            if (ng != children[i]->geometry()) {
                children[i]->setGeometry(ng);
            }
        }
    }

    bool keyPressEvent(QKeyEvent *) override
    { return false; }

    bool keyReleaseEvent(QKeyEvent *) override
    { return false; }

    bool mouseMoveEvent(QMouseEvent *) override
    { return false; }

    bool mousePressEvent(QMouseEvent *) override
    { return false; }

    bool mouseReleaseEvent(QMouseEvent *) override
    { return false; }

    void interactionTimeout() override
    { }

    void displayMouseTracking(bool) override
    { }

    void leaveEvent(QEvent *) override
    { }

    void setMouseGrab(bool) override
    { }

    void setKeyboardGrab(bool) override
    { }

    void changeToolTip(const QPoint &, const QString &) override
    { }

    void contextMenu(const QPoint &) override
    { }

    std::vector<std::shared_ptr<DisplayHighlightController>> getHighlightControllers() override
    {
        if (!layout)
            return {};
        std::vector<std::shared_ptr<DisplayHighlightController>> result;
        for (const auto &it : layout->elements) {
            auto add = it->display->highlightController();
            if (!add)
                continue;
            result.emplace_back(std::move(add));
        }
        return result;
    }

    bool resizeEvent(QResizeEvent *) override
    {
        if (!layout)
            return false;
        updateLayout();
        return true;
    }

    bool event(QEvent *event) override
    {
        if (event->type() == QEvent::LayoutRequest) {
            updateLayout();
            event->accept();
            return true;
        }
        return false;
    }
};

}

/**
 * Create a new display widget with no Display element.
 * 
 * @param parent    the parent widget
 */
DisplayWidget::DisplayWidget(QWidget *parent) : QWidget(parent)
{
    p.reset(new DisplayWidgetPrivate(this));
    isLayout = false;
    connect(this, &DisplayWidget::customContextMenuRequested, this,
            &DisplayWidget::widgetContextMenu);
}

/**
 * Create a new display widget with the given Display element and parent.
 * <br>
 * This takes ownership of the display, if any.
 * 
 * @param setDisplay        the display to use or NULL for none
 * @param parent            the parent widget
 */
DisplayWidget::DisplayWidget(std::unique_ptr<Display> &&setDisplay, QWidget *parent) : QWidget(
        parent)
{
    DisplayLayout *layout = qobject_cast<DisplayLayout *>(setDisplay.get());
    if (layout) {
        p.reset(new DisplayWidgetPrivateLayout(this));
        isLayout = true;
    } else {
        p.reset(new DisplayWidgetPrivate(this));
        isLayout = false;
    }
    p->setDisplay(std::move(setDisplay));
    connect(this, &DisplayWidget::customContextMenuRequested, this,
            &DisplayWidget::widgetContextMenu);
}

DisplayWidget::~DisplayWidget() = default;

/**
 * Change the display to the given one.  This will delete the old display
 * if any.
 * 
 * @param set       the display to change to
 */
void DisplayWidget::setDisplay(std::unique_ptr<Display> &&set)
{
    DisplayLayout *layout = qobject_cast<DisplayLayout *>(set.get());
    if (layout) {
        if (!isLayout) {
            p->setDisplay({});
            p.reset(new DisplayWidgetPrivateLayout(this));
            isLayout = true;
        }
        p->setDisplay(std::move(set));
    } else {
        if (isLayout) {
            p->setDisplay({});
            p.reset(new DisplayWidgetPrivate(this));
            isLayout = false;
        }
        p->setDisplay(std::move(set));
    }
}

/**
 * Get he display used by the widget, if any.
 *
 * @return the currently used display
 */
Display *DisplayWidget::getDisplay() const
{ return p->display.get(); }

/**
 * Register any inputs needed for this display in the given filter chain.
 * The specialized subclass of the chain is used by the widget to provide
 * interactive smoothing control.
 * 
 * @param chain         the chain to get data from
 */
void DisplayWidget::registerChain(DisplayWidgetTapChain *chain)
{ p->registerChain(chain); }

/**
 * Set the display context to the given one.  This automatically switches
 * is to interactive.
 * 
 * @param context   the new context
 */
void DisplayWidget::setContext(const DisplayDynamicContext &context)
{ p->setContext(context); }

/**
 * Get the display context.
 * 
 * @return the context
 */
const DisplayDynamicContext &DisplayWidget::getContext() const
{ return p->context; }

QSize DisplayWidget::sizeHint() const
{ return p->minimumSize; }

void DisplayWidget::applyZoom(DisplayZoomCommand command)
{ command.redo(); }

void DisplayWidget::setVisibleTimeRange(double start, double end)
{ p->setVisibleTimeRange(start, end); }

void DisplayWidget::displayFullScreen()
{ DisplayWidgetContextDisplay::displayFullScreen(this); }

void DisplayWidget::releaseFocus()
{ p->releaseFocus(); }

/**
 * Get the highlight controllers for the display.
 * 
 * @return the highlight controllers
 */
std::vector<std::shared_ptr<DisplayHighlightController>> DisplayWidget::getHighlightControllers()
{ return p->getHighlightControllers(); }

/**
 * Get the outputs for the display.
 * 
 * @return the outputs
 */
std::vector<std::shared_ptr<DisplayOutput> > DisplayWidget::getOutputs()
{ return p->getOutputs(); }

void DisplayWidget::interactiveRepaint(bool userInitiated)
{
    if (!userInitiated && p->interactiveThrottle.isActive()) {
        if (!p->interactiveQueued.isActive())
            p->interactiveQueued.start();
        return;
    }
    p->interactiveThrottle.start();
    update();
}

void DisplayWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    p->interactiveQueued.stop();
    if (p->display == NULL)
        return;
    QPainter painter(this);
    p->paint(&painter);
}

void DisplayWidget::keyPressEvent(QKeyEvent *event)
{
    if (p->keyPressEvent(event))
        return;
    QWidget::keyPressEvent(event);
}

void DisplayWidget::keyReleaseEvent(QKeyEvent *event)
{
    if (p->keyReleaseEvent(event))
        return;
    QWidget::keyReleaseEvent(event);
}

void DisplayWidget::enterEvent(QEvent *event)
{
    if (focusPolicy() == Qt::NoFocus) {
        QWidget::enterEvent(event);
        return;
    }
    setFocus(Qt::MouseFocusReason);
    event->accept();
}

void DisplayWidget::leaveEvent(QEvent *event)
{
    p->leaveEvent(event);
}

void DisplayWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (p->mouseMoveEvent(event))
        return;
    QWidget::mouseMoveEvent(event);
}

void DisplayWidget::mousePressEvent(QMouseEvent *event)
{
    if (p->mousePressEvent(event))
        return;
    QWidget::mousePressEvent(event);
}

void DisplayWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (p->mouseReleaseEvent(event))
        return;
    QWidget::mouseReleaseEvent(event);
}

void DisplayWidget::resizeEvent(QResizeEvent *event)
{
    if (p->resizeEvent(event))
        return;
    QWidget::resizeEvent(event);
}

bool DisplayWidget::event(QEvent *event)
{
    if (p->event(event))
        return true;
    return QWidget::event(event);
}

void DisplayWidget::interactionTimeout()
{ p->interactionTimeout(); }

void DisplayWidget::displayMouseTracking(bool enable)
{ p->displayMouseTracking(enable); }

void DisplayWidget::setMouseGrab(bool enable)
{ p->setMouseGrab(enable); }

void DisplayWidget::setKeyboardGrab(bool enable)
{ p->setKeyboardGrab(enable); }

void DisplayWidget::widgetContextMenu(const QPoint &point)
{ p->contextMenu(point); }

void DisplayWidget::changeToolTip(const QPoint &point, const QString &text)
{ p->changeToolTip(point, text); }

void DisplayWidget::menuSaveChanges()
{
    DisplayWidget *transverse = this;
    for (;;) {
        DisplayWidget *next = qobject_cast<DisplayWidget *>(transverse->parent());
        if (!next)
            break;
        transverse = next;
    }
    if (transverse != this) {
        transverse->menuSaveChanges();
    } else {
        ValueSegment::Transfer target(p->exportChanges());
        emit saveChanges(target);
    }
}

bool DisplayWidget::getResetSelectsAll() const
{ return p->getResetSelectsAll(); }

void DisplayWidget::setResetSelectsAll(bool set)
{ p->setResetSelectsAll(set); }

DisplayWidget::SmoothingType DisplayWidget::getSmoothingType() const
{ return p->getSmoothingType(); }

void DisplayWidget::setSmoothingType(SmoothingType set)
{ return p->setSmoothingType(set); }

bool DisplayWidget::getContaminationRemoved() const
{ return p->getContaminationRemoved(); }

void DisplayWidget::setContaminationRemoved(bool set)
{ return p->setContaminationRemoved(set); }

void DisplayWidget::setSmoothing(SmoothingType type, bool contam)
{ return p->setSmoothing(type, contam); }

void DisplayWidget::emitZoom(DisplayZoomCommand command)
{ emit internalZoom(command); }

void DisplayWidget::emitVisibleZoom(DisplayZoomCommand command)
{ emit visibleZoom(command); }

void DisplayWidget::emitGlobalZoom(DisplayZoomCommand command)
{ emit globalZoom(command); }

void DisplayWidget::emitResetTimes()
{ emit timeRangeSelected(FP::undefined(), FP::undefined()); }

void DisplayWidget::emitChainReload()
{ emit requestChainReload(); }

void DisplayWidget::combineVisibleZoom(DisplayZoomCommand command)
{
    Q_ASSERT(isLayout);
    DisplayWidgetPrivateLayout *lp = static_cast<DisplayWidgetPrivateLayout *>(p.get());
    Display *originator = nullptr;
    DisplayWidget *widgetOriginator = qobject_cast<DisplayWidget *>(sender());
    if (widgetOriginator)
        originator = widgetOriginator->p->display.get();
    for (const auto &element : lp->layout->elements) {
        if (element->display.get() == originator)
            continue;
        for (const auto &it : element->display->zoomAxes()) {
            command.addCombining(it);
        }
    }
    emit visibleZoom(command);
}

void DisplayWidget::combineGlobalZoom(DisplayZoomCommand command)
{
    Q_ASSERT(isLayout);
    DisplayWidgetPrivateLayout *lp = static_cast<DisplayWidgetPrivateLayout *>(p.get());
    Display *originator = nullptr;
    DisplayWidget *widgetOriginator = qobject_cast<DisplayWidget *>(sender());
    if (widgetOriginator)
        originator = widgetOriginator->p->display.get();
    for (const auto &element : lp->layout->elements) {
        if (element->display.get() == originator)
            continue;
        for (const auto &it : element->display->zoomAxes()) {
            command.addCombining(it);
        }
    }
    emit globalZoom(command);
}

void DisplayWidget::combineSaveChanges(ValueSegment::Transfer overlay)
{
    Q_ASSERT(isLayout);
    DisplayWidgetPrivateLayout *lp = static_cast<DisplayWidgetPrivateLayout *>(p.get());
    Display *originator = nullptr;
    DisplayWidget *widgetOriginator = qobject_cast<DisplayWidget *>(sender());
    if (!widgetOriginator)
        return;
    for (const auto &element : lp->layout->elements) {
        if (element->display.get() == originator)
            continue;
        for (auto &it : overlay) {
            Variant::Root sub;
            sub[element->name].set(it.read());
            it.root() = std::move(sub);
        }
        emit saveChanges(overlay);
        return;
    }
}

namespace Internal {

DisplayWidgetSmoothingForwarder::DisplayWidgetSmoothingForwarder(QObject *target,
                                                                 const char *slot,
                                                                 DisplayWidget::SmoothingType st,
                                                                 bool contam,
                                                                 QObject *parent) : QObject(parent),
                                                                                    type(st),
                                                                                    contamination(
                                                                                            contam)
{
    connect(this, SIGNAL(smoothingChanged(DisplayWidget::SmoothingType, bool)), target, slot);
}

void DisplayWidgetSmoothingForwarder::smoothingDisabled()
{
    type = DisplayWidget::Smoothing_Disabled;
    contamination = false;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing1M()
{
    type = DisplayWidget::Smoothing_1M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing6M()
{
    type = DisplayWidget::Smoothing_6M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing10M()
{
    type = DisplayWidget::Smoothing_10M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing15M()
{
    type = DisplayWidget::Smoothing_15M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing1H()
{
    type = DisplayWidget::Smoothing_1H;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing6H()
{
    type = DisplayWidget::Smoothing_6H;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing1D()
{
    type = DisplayWidget::Smoothing_1D;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing1W()
{
    type = DisplayWidget::Smoothing_1W;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing1Mon()
{
    type = DisplayWidget::Smoothing_1MON;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing1PLP1M()
{
    type = DisplayWidget::Smoothing_1PLP_1M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing1PLP3M()
{
    type = DisplayWidget::Smoothing_1PLP_3M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing1PLP10M()
{
    type = DisplayWidget::Smoothing_1PLP_10M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing1PLP15M()
{
    type = DisplayWidget::Smoothing_1PLP_15M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing1PLP1H()
{
    type = DisplayWidget::Smoothing_1PLP_1H;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing1PLP1D()
{
    type = DisplayWidget::Smoothing_1PLP_1D;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing4PLP1M()
{
    type = DisplayWidget::Smoothing_4PLP_1M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing4PLP3M()
{
    type = DisplayWidget::Smoothing_4PLP_3M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing4PLP10M()
{
    type = DisplayWidget::Smoothing_4PLP_10M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing4PLP15M()
{
    type = DisplayWidget::Smoothing_4PLP_15M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing4PLP1H()
{
    type = DisplayWidget::Smoothing_4PLP_1H;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing4PLP1D()
{
    type = DisplayWidget::Smoothing_4PLP_1D;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothingFourier1M()
{
    type = DisplayWidget::Smoothing_Fourier_1M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothingFourier3M()
{
    type = DisplayWidget::Smoothing_Fourier_3M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothingFourier10M()
{
    type = DisplayWidget::Smoothing_Fourier_10M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothingFourier15M()
{
    type = DisplayWidget::Smoothing_Fourier_15M;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothingFourier1H()
{
    type = DisplayWidget::Smoothing_Fourier_1H;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothingFourier1D()
{
    type = DisplayWidget::Smoothing_Fourier_1D;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothing3RSSH()
{
    type = DisplayWidget::Smoothing_3RSSH;
    emit smoothingChanged(type, contamination);
}

void DisplayWidgetSmoothingForwarder::smoothingContamination(bool on)
{
    contamination = on;
    emit smoothingChanged(type, contamination);
}

}

/**
 * Build a smoothing menu.  This will call the given slot on the
 * target (like slot(SmoothingType, bool) ) when a menu option is selected.
 * 
 * @param menu      the menu to add the smoothing options to
 * @param target    the target of the slot
 * @param slot      the target slot to call
 * @param currentSmoothingType the current smoothing type
 * @param currentContaminationRemoved the current contamination removal state
 */
void DisplayWidget::buildSmoothingMenu(QMenu *menu,
                                       QObject *target,
                                       const char *slot,
                                       SmoothingType currentSmoothingType,
                                       bool currentContaminationRemoved)
{
    QAction *act;

    DisplayWidgetSmoothingForwarder *forward =
            new DisplayWidgetSmoothingForwarder(target, slot, currentSmoothingType,
                                                currentContaminationRemoved, menu);

    QActionGroup *group = new QActionGroup(menu);

    act = new QAction(tr("&Disable", "Context|Smoothing|Disable"), menu);
    act->setToolTip(tr("Disable all additional smoothing."));
    act->setStatusTip(tr("Disable smoothing"));
    act->setWhatsThis(tr("This disables all additional smoothing that is currently active."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_Disabled && !currentContaminationRemoved);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothingDisabled()));
    menu->addAction(act);

    act = new QAction(tr("Hide &Contamination", "Context|Smoothing|RemoveContamination"), menu);
    act->setToolTip(tr("Hide contaminated data."));
    act->setStatusTip(tr("Hide contamination"));
    act->setWhatsThis(tr("This remove all contaminated data from being shown."));
    act->setCheckable(true);
    act->setChecked(currentContaminationRemoved);
    connect(act, SIGNAL(triggered(bool)), forward, SLOT(smoothingContamination(bool)));
    menu->addAction(act);

    QMenu *submenu;

    submenu = new QMenu(tr("&Average", "Context|Smoothing|AverageMenu"), menu);
    menu->addMenu(submenu);

    act = new QAction(tr("&1 Minute", "Context|Smoothing|Average|1Minute"), submenu);
    act->setToolTip(tr("Average data to one minute intervals."));
    act->setStatusTip(tr("One minute averages"));
    act->setWhatsThis(
            tr("This applies conventional averaging with an interval of one minute to data."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_1M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing1M()));
    submenu->addAction(act);

    act = new QAction(tr("&6 Minutes", "Context|Smoothing|Average|6Minutes"), submenu);
    act->setToolTip(tr("Average data to six minute intervals."));
    act->setStatusTip(tr("Six minute averages"));
    act->setWhatsThis(
            tr("This applies conventional averaging with an interval of six minutes to data."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_6M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing6M()));
    submenu->addAction(act);

    act = new QAction(tr("1&0 Minutes", "Context|Smoothing|Average|10Minutes"), submenu);
    act->setToolTip(tr("Average data to ten minute intervals."));
    act->setStatusTip(tr("Ten minute averages"));
    act->setWhatsThis(
            tr("This applies conventional averaging with an interval of ten minutes to data."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_10M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing10M()));
    submenu->addAction(act);

    act = new QAction(tr("1&5 Minutes", "Context|Smoothing|Average|15Minutes"), submenu);
    act->setToolTip(tr("Average data to 15 minute intervals."));
    act->setStatusTip(tr("15 minute averages"));
    act->setWhatsThis(
            tr("This applies conventional averaging with an interval of 15 minutes to data."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_15M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing15M()));
    submenu->addAction(act);

    act = new QAction(tr("1 &Hour", "Context|Smoothing|Average|1Hour"), submenu);
    act->setToolTip(tr("Average data to one hour intervals."));
    act->setStatusTip(tr("Hourly averages"));
    act->setWhatsThis(
            tr("This applies conventional averaging with an interval of one hour to data."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_1H);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing1H()));
    submenu->addAction(act);

    act = new QAction(tr("6 H&ours", "Context|Smoothing|Average|6Hours"), submenu);
    act->setToolTip(tr("Average data to six hour intervals."));
    act->setStatusTip(tr("Six hour averages"));
    act->setWhatsThis(
            tr("This applies conventional averaging with an interval of six hours to data."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_6H);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing6H()));
    submenu->addAction(act);

    act = new QAction(tr("1 &Day", "Context|Smoothing|Average|1Day"), submenu);
    act->setToolTip(tr("Average data to one day intervals."));
    act->setStatusTip(tr("Daily averages"));
    act->setWhatsThis(
            tr("This applies conventional averaging with an interval of one day to data."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_1D);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing1D()));
    submenu->addAction(act);

    act = new QAction(tr("1 &Week", "Context|Smoothing|Average|1Week"), submenu);
    act->setToolTip(tr("Average data to one week intervals."));
    act->setStatusTip(tr("Weekly averages"));
    act->setWhatsThis(
            tr("This applies conventional averaging with an interval of one week to data."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_1W);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing1W()));
    submenu->addAction(act);

    act = new QAction(tr("1 &Month", "Context|Smoothing|Average|1Month"), submenu);
    act->setToolTip(tr("Average data to one month intervals."));
    act->setStatusTip(tr("Monthly averages"));
    act->setWhatsThis(
            tr("This applies conventional averaging with an interval of one month to data."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_1MON);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing1Mon()));
    submenu->addAction(act);


    submenu = new QMenu(tr("&Single Pole Low Pass", "Context|Smoothing|1PLP"), menu);
    menu->addMenu(submenu);

    act = new QAction(tr("\xCF\x84 = &1 Minute", "Context|Smoothing|1PLP|1Minute"), submenu);
    act->setToolTip(tr("Apply a single pole low pass filter with a time constant of one minute."));
    act->setStatusTip(tr("One minute single pole low pass"));
    act->setWhatsThis(
            tr("This applies a single pole low pass digital filter to data with a time constant of one minute.  The time constant is the amount of time the filter takes to reach 63.2% of the final value after a step change."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_1PLP_1M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing1PLP1M()));
    submenu->addAction(act);

    act = new QAction(tr("\xCF\x84 = &3 Minutes", "Context|Smoothing|1PLP|3Minutes"), submenu);
    act->setToolTip(
            tr("Apply a single pole low pass filter with a time constant of three minutes."));
    act->setStatusTip(tr("Three minute single pole low pass"));
    act->setWhatsThis(
            tr("This applies a single pole low pass digital filter to data with a time constant of three minutes.  The time constant is the amount of time the filter takes to reach 63.2% of the final value after a step change."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_1PLP_3M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing1PLP3M()));
    submenu->addAction(act);

    act = new QAction(tr("\xCF\x84 = 1&0 Minutes", "Context|Smoothing|1PLP|10Minutes"), submenu);
    act->setToolTip(tr("Apply a single pole low pass filter with a time constant of ten minutes."));
    act->setStatusTip(tr("Ten minute single pole low pass"));
    act->setWhatsThis(
            tr("This applies a single pole low pass digital filter to data with a time constant of ten minutes.  The time constant is the amount of time the filter takes to reach 63.2% of the final value after a step change."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_1PLP_10M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing1PLP10M()));
    submenu->addAction(act);

    act = new QAction(tr("\xCF\x84 = 1&5 Minutes", "Context|Smoothing|1PLP|15Minutes"), submenu);
    act->setToolTip(tr("Apply a single pole low pass filter with a time constant of 15 minutes."));
    act->setStatusTip(tr("15 minute single pole low pass"));
    act->setWhatsThis(
            tr("This applies a single pole low pass digital filter to data with a time constant of 15 minutes.  The time constant is the amount of time the filter takes to reach 63.2% of the final value after a step change."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_1PLP_15M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing1PLP15M()));
    submenu->addAction(act);

    act = new QAction(tr("\xCF\x84 = 1 &Hour", "Context|Smoothing|1PLP|1Hour"), submenu);
    act->setToolTip(tr("Apply a single pole low pass filter with a time constant of one hour."));
    act->setStatusTip(tr("One hour single pole low pass"));
    act->setWhatsThis(
            tr("This applies a single pole low pass digital filter to data with a time constant of one hour.  The time constant is the amount of time the filter takes to reach 63.2% of the final value after a step change."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_1PLP_1H);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing1PLP1H()));
    submenu->addAction(act);

    act = new QAction(tr("\xCF\x84 = 1 &Day", "Context|Smoothing|1PLP|1Day"), submenu);
    act->setToolTip(tr("Apply a single pole low pass filter with a time constant of one day."));
    act->setStatusTip(tr("One day single pole low pass"));
    act->setWhatsThis(
            tr("This applies a single pole low pass digital filter to data with a time constant of one day.  The time constant is the amount of time the filter takes to reach 63.2% of the final value after a step change."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_1PLP_1D);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing1PLP1D()));
    submenu->addAction(act);


    submenu = new QMenu(tr("&Four Pole Low Pass", "Context|Smoothing|4PLP"), menu);
    menu->addMenu(submenu);

    act = new QAction(tr("\xCF\x84 = &1 Minute", "Context|Smoothing|4PLP|1Minute"), submenu);
    act->setToolTip(tr("Apply a four pole low pass filter with a time constant of one minute."));
    act->setStatusTip(tr("One minute four pole low pass"));
    act->setWhatsThis(
            tr("This applies a four pole low pass digital filter to data with a time constant of one minute.  The time constant is the amount of time the filter takes to reach 63.2% of the final value after a step change."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_4PLP_1M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing4PLP1M()));
    submenu->addAction(act);

    act = new QAction(tr("\xCF\x84 = &3 Minutes", "Context|Smoothing|4PLP|3Minutes"), submenu);
    act->setToolTip(tr("Apply a four pole low pass filter with a time constant of three minutes."));
    act->setStatusTip(tr("Three minute four pole low pass"));
    act->setWhatsThis(
            tr("This applies a four pole low pass digital filter to data with a time constant of three minutes.  The time constant is the amount of time the filter takes to reach 63.2% of the final value after a step change."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_4PLP_3M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing4PLP3M()));
    submenu->addAction(act);

    act = new QAction(tr("\xCF\x84 = 1&0 Minutes", "Context|Smoothing|4PLP|10Minutes"), submenu);
    act->setToolTip(tr("Apply a four pole low pass filter with a time constant of ten minutes."));
    act->setStatusTip(tr("Ten minute four pole low pass"));
    act->setWhatsThis(
            tr("This applies a four pole low pass digital filter to data with a time constant of ten minutes.  The time constant is the amount of time the filter takes to reach 63.2% of the final value after a step change."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_4PLP_10M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing4PLP10M()));
    submenu->addAction(act);

    act = new QAction(tr("\xCF\x84 = 1&5 Minutes", "Context|Smoothing|4PLP|15Minutes"), submenu);
    act->setToolTip(tr("Apply a four pole low pass filter with a time constant of 15 minutes."));
    act->setStatusTip(tr("15 minute four pole low pass"));
    act->setWhatsThis(
            tr("This applies a four pole low pass digital filter to data with a time constant of 15 minutes.  The time constant is the amount of time the filter takes to reach 63.2% of the final value after a step change."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_4PLP_15M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing4PLP15M()));
    submenu->addAction(act);

    act = new QAction(tr("\xCF\x84 = 1 &Hour", "Context|Smoothing|4PLP|1Hour"), submenu);
    act->setToolTip(tr("Apply a four pole low pass filter with a time constant of one hour."));
    act->setStatusTip(tr("One hour four pole low pass"));
    act->setWhatsThis(
            tr("This applies a four pole low pass digital filter to data with a time constant of one hour.  The time constant is the amount of time the filter takes to reach 63.2% of the final value after a step change."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_4PLP_1H);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing4PLP1H()));
    submenu->addAction(act);

    act = new QAction(tr("\xCF\x84 = 1 &Day", "Context|Smoothing|4PLP|1Day"), submenu);
    act->setToolTip(tr("Apply a four pole low pass filter with a time constant of one day."));
    act->setStatusTip(tr("One day four pole low pass"));
    act->setWhatsThis(
            tr("This applies a four pole low pass digital filter to data with a time constant of one day.  The time constant is the amount of time the filter takes to reach 63.2% of the final value after a step change."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_4PLP_1D);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing4PLP1D()));
    submenu->addAction(act);


    submenu = new QMenu(tr("F&ourier Transform Low Pass", "Context|Smoothing|Fourier"), menu);
    menu->addMenu(submenu);

    act = new QAction(tr("&1 Minute Cutoff", "Context|Smoothing|Fourier|1Minute"), submenu);
    act->setToolTip(
            tr("Apply a Fourier transform based low pass filter with a cutoff at one minute."));
    act->setStatusTip(tr("One minute FFT low pass"));
    act->setWhatsThis(
            tr("This applies applies a cutoff removing all frequencies with periods shorter than one minute."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_Fourier_1M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothingFourier1M()));
    submenu->addAction(act);

    act = new QAction(tr("&3 Minute Cutoff", "Context|Smoothing|Fourier|3Minutes"), submenu);
    act->setToolTip(
            tr("Apply a Fourier transform based low pass filter with a cutoff at three minutes."));
    act->setStatusTip(tr("Three minute FFT low pass"));
    act->setWhatsThis(
            tr("This applies applies a cutoff removing all frequencies with periods shorter than three minutes."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_Fourier_3M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothingFourier3M()));
    submenu->addAction(act);

    act = new QAction(tr("1&0 Minute Cutoff", "Context|Smoothing|Fourier|10Minutes"), submenu);
    act->setToolTip(
            tr("Apply a Fourier transform based low pass filter with a cutoff at ten minutes."));
    act->setStatusTip(tr("Ten minute FFT low pass"));
    act->setWhatsThis(
            tr("This applies applies a cutoff removing all frequencies with periods shorter than ten minutes."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_Fourier_10M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothingFourier10M()));
    submenu->addAction(act);

    act = new QAction(tr("1&5 Minute Cutoff", "Context|Smoothing|Fourier|15Minutes"), submenu);
    act->setToolTip(
            tr("Apply a Fourier transform based low pass filter with a cutoff at 15 minutes."));
    act->setStatusTip(tr("15 minute FFT low pass"));
    act->setWhatsThis(
            tr("This applies applies a cutoff removing all frequencies with periods shorter than 15 minutes."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_Fourier_15M);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothingFourier15M()));
    submenu->addAction(act);

    act = new QAction(tr("1 &Hour Cutoff", "Context|Smoothing|Fourier|1Hour"), submenu);
    act->setToolTip(
            tr("Apply a Fourier transform based low pass filter with a cutoff at one hour."));
    act->setStatusTip(tr("One hour FFT low pass"));
    act->setWhatsThis(
            tr("This applies applies a cutoff removing all frequencies with periods shorter than one hour."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_Fourier_1H);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothingFourier1H()));
    submenu->addAction(act);

    act = new QAction(tr("1 &Day Cutoff", "Context|Smoothing|Fourier|1Day"), submenu);
    act->setToolTip(
            tr("Apply a Fourier transform based low pass filter with a cutoff at one day."));
    act->setStatusTip(tr("One day FFT low pass"));
    act->setWhatsThis(
            tr("This applies applies a cutoff removing all frequencies with periods shorter than one day."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_Fourier_1D);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothingFourier1D()));
    submenu->addAction(act);


    act = new QAction(tr("&Tukey 3RSSH", "Context|Smoothing|3RSSH"), menu);
    act->setToolTip(tr("Apply a Tukey 3RSSH smoother."));
    act->setStatusTip(tr("Tukey 3RSSH"));
    act->setWhatsThis(tr("This applies a Tukey style 3RSSH smoother to data."));
    act->setCheckable(true);
    act->setChecked(currentSmoothingType == Smoothing_3RSSH);
    act->setActionGroup(group);
    connect(act, SIGNAL(triggered()), forward, SLOT(smoothing3RSSH()));
    menu->addAction(act);
}


/**
 * Save the configuration to a user selected file.  This is used for
 * displays that are not attached to a station when the GUI widget
 * requests a save.
 * 
 * @param baseline  the baseline configuration
 * @param overlay   the updated overlay
 * @param parent    the parent widget
 */
void DisplayWidget::saveConfiguration(const Variant::Read &baseline,
                                      ValueSegment::Transfer overlay,
                                      QWidget *parent)
{
    overlay.insert(overlay.begin(),
                   ValueSegment(FP::undefined(), FP::undefined(), Variant::Root(baseline)));

    QStringList filters;
    filters << tr("CPD3 data (*.c3d)");
    filters << tr("CPD3 binary data (*.c3r)");
    filters << tr("CPD3 XML data (*.xml)");
    filters << tr("CPD3 JSON data (*.json)");
    filters << tr("Any files (*)");

    QString selectedFilter;
    QString fileName(QFileDialog::getSaveFileName(parent, tr("Save Configuration",
                                                             "save configuration title"), QString(),
                                                  filters.join(";;"), &selectedFilter));
    if (fileName.isEmpty())
        return;

    StandardDataOutput::OutputType type = StandardDataOutput::OutputType::Direct;
    IO::File::Mode mode = IO::File::Mode::writeOnly().textMode().bufferedMode();
    QFileInfo info(fileName);
    if (info.completeSuffix().isEmpty() && !info.exists()) {
        if (selectedFilter == tr("CPD3 data (*.c3d)")) {
            type = StandardDataOutput::OutputType::Direct;
            fileName.append(tr(".c3d", "save extension"));
        } else if (selectedFilter == tr("CPD3 binary data (*.c3r)")) {
            type = StandardDataOutput::OutputType::Raw;
            fileName.append(tr(".c3r", "save extension"));
            mode.textMode(false);
        } else if (selectedFilter == tr("CPD3 XML data (*.xml)")) {
            type = StandardDataOutput::OutputType::XML;
            fileName.append(tr(".xml", "save extension"));
        } else if (selectedFilter == tr("CPD3 JSON data (*.json)")) {
            type = StandardDataOutput::OutputType::JSON;
            fileName.append(tr(".json", "save extension"));
        }
    } else {
        if (selectedFilter == tr("CPD3 data (*.c3d)")) {
            type = StandardDataOutput::OutputType::Direct;
        } else if (selectedFilter == tr("CPD3 binary data (*.c3r)")) {
            type = StandardDataOutput::OutputType::Raw;
            mode.textMode(false);
        } else if (selectedFilter == tr("CPD3 XML data (*.xml)")) {
            type = StandardDataOutput::OutputType::XML;
        } else if (selectedFilter == tr("CPD3 JSON data (*.json)")) {
            type = StandardDataOutput::OutputType::JSON;
        }
    }

    auto target = IO::Access::file(fileName, mode);
    if (!target) {
        QMessageBox::information(parent, tr("Error Opening File"),
                                 tr("Can't open file %1 for export").arg(fileName));
        return;
    }
    auto stream = target->stream();
    if (!stream) {
        QMessageBox::information(parent, tr("Error Opening File"),
                                 tr("Can't open file %1 for export").arg(fileName));
        return;
    }

    StandardDataOutput output(std::move(stream), type);
    output.start();
    target.reset();
    overlay = ValueSegment::merge(overlay);
    SequenceValue::Transfer combined;
    for (auto &it : overlay) {
        output.emplaceData(
                SequenceIdentity({"nil", "config", "displayexport"}, it.getStart(), it.getEnd()),
                std::move(it.root()));
    }
    output.endData();
    output.wait();
}

void DisplayWidget::modifyConfigForSmoother(SmoothingType smoothingType,
                                            bool removeContamination,
                                            Variant::Write &config)
{
    if (removeContamination) {
        /* Insert the removal before any processing, so intensives, etc
         * take it into account */
        if (config["Components"].getType() == Variant::Type::Array) {
            for (std::size_t i = config["Components"].toArray().size(); i > 0; --i) {
                config["Components"].array(i).set(config["Components"].array(i - 1));
            }
            config["Components"].array(0).setEmpty();
        }

        auto comp = config["Components"].array(0);
        comp["Name"].setString("corr_removecontam");
        comp["Options/variables/Variable"].setString("");

        /* Pretty dumb way of doing this, but need the flags unconditionally */
        SequenceMatch::Composite::addToConfiguration(config["Additional"], QString(), QString(),
                                                     "F1_.*");
    }

    switch (smoothingType) {
    case DisplayWidget::Smoothing_Disabled:
        break;

    case DisplayWidget::Smoothing_1M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("avg");
        comp["Options/interval/Units"].setString("Minutes");
        comp["Options/interval/Count"].setInt64(1);
        comp["Options/interval/Aligned"].setBool(true);
        comp["Options/contamination"].setString("none");
        break;
    }
    case DisplayWidget::Smoothing_6M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("avg");
        comp["Options/interval/Units"].setString("Minutes");
        comp["Options/interval/Count"].setInt64(6);
        comp["Options/interval/Aligned"].setBool(true);
        comp["Options/contamination"].setString("none");
        break;
    }
    case DisplayWidget::Smoothing_10M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("avg");
        comp["Options/interval/Units"].setString("Minutes");
        comp["Options/interval/Count"].setInt64(10);
        comp["Options/interval/Aligned"].setBool(true);
        comp["Options/contamination"].setString("none");
        break;
    }
    case DisplayWidget::Smoothing_15M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("avg");
        comp["Options/interval/Units"].setString("Minutes");
        comp["Options/interval/Count"].setInt64(15);
        comp["Options/interval/Aligned"].setBool(true);
        comp["Options/contamination"].setString("none");
        break;
    }
    case DisplayWidget::Smoothing_1H: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("avg");
        comp["Options/interval/Units"].setString("Hours");
        comp["Options/interval/Count"].setInt64(1);
        comp["Options/interval/Aligned"].setBool(true);
        comp["Options/contamination"].setString("none");
        break;
    }
    case DisplayWidget::Smoothing_6H: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("avg");
        comp["Options/interval/Units"].setString("Hours");
        comp["Options/interval/Count"].setInt64(6);
        comp["Options/interval/Aligned"].setBool(true);
        comp["Options/contamination"].setString("none");
        break;
    }
    case DisplayWidget::Smoothing_1D: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("avg");
        comp["Options/interval/Units"].setString("Days");
        comp["Options/interval/Count"].setInt64(1);
        comp["Options/interval/Aligned"].setBool(true);
        comp["Options/contamination"].setString("none");
        break;
    }
    case DisplayWidget::Smoothing_1W: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("avg");
        comp["Options/interval/Units"].setString("Weeks");
        comp["Options/interval/Count"].setInt64(1);
        comp["Options/interval/Aligned"].setBool(true);
        comp["Options/contamination"].setString("none");
        break;
    }
    case DisplayWidget::Smoothing_1MON: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("avg");
        comp["Options/interval/Units"].setString("Months");
        comp["Options/interval/Count"].setInt64(1);
        comp["Options/interval/Aligned"].setBool(true);
        comp["Options/contamination"].setString("none");
        break;
    }

    case DisplayWidget::Smoothing_1PLP_1M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_1plp");
        comp["Options/tc/Units"].setString("Minutes");
        comp["Options/tc/Count"].setInt64(1);
        comp["Options/tc/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_1PLP_3M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_1plp");
        comp["Options/tc/Units"].setString("Minutes");
        comp["Options/tc/Count"].setInt64(3);
        comp["Options/tc/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_1PLP_10M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_1plp");
        comp["Options/tc/Units"].setString("Minutes");
        comp["Options/tc/Count"].setInt64(10);
        comp["Options/tc/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_1PLP_15M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_1plp");
        comp["Options/tc/Units"].setString("Minutes");
        comp["Options/tc/Count"].setInt64(15);
        comp["Options/tc/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_1PLP_1H: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_1plp");
        comp["Options/tc/Units"].setString("Hours");
        comp["Options/tc/Count"].setInt64(1);
        comp["Options/tc/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_1PLP_1D: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_1plp");
        comp["Options/tc/Units"].setString("Days");
        comp["Options/tc/Count"].setInt64(1);
        comp["Options/tc/Align"].setBool(false);
        break;
    }

    case DisplayWidget::Smoothing_4PLP_1M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_4plp");
        comp["Options/tc/Units"].setString("Minutes");
        comp["Options/tc/Count"].setInt64(1);
        comp["Options/tc/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_4PLP_3M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_4plp");
        comp["Options/tc/Units"].setString("Minutes");
        comp["Options/tc/Count"].setInt64(3);
        comp["Options/tc/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_4PLP_10M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_4plp");
        comp["Options/tc/Units"].setString("Minutes");
        comp["Options/tc/Count"].setInt64(10);
        comp["Options/tc/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_4PLP_15M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_4plp");
        comp["Options/tc/Units"].setString("Minutes");
        comp["Options/tc/Count"].setInt64(15);
        comp["Options/tc/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_4PLP_1H: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_4plp");
        comp["Options/tc/Units"].setString("Hours");
        comp["Options/tc/Count"].setInt64(1);
        comp["Options/tc/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_4PLP_1D: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_4plp");
        comp["Options/tc/Units"].setString("Days");
        comp["Options/tc/Count"].setInt64(1);
        comp["Options/tc/Align"].setBool(false);
        break;
    }

    case DisplayWidget::Smoothing_Fourier_1M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_fourier");
        comp["Options/low/Units"].setString("Minutes");
        comp["Options/low/Count"].setInt64(1);
        comp["Options/low/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_Fourier_3M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_fourier");
        comp["Options/low/Units"].setString("Minutes");
        comp["Options/low/Count"].setInt64(3);
        comp["Options/low/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_Fourier_10M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_fourier");
        comp["Options/low/Units"].setString("Minutes");
        comp["Options/low/Count"].setInt64(10);
        comp["Options/low/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_Fourier_15M: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_fourier");
        comp["Options/low/Units"].setString("Minutes");
        comp["Options/low/Count"].setInt64(15);
        comp["Options/low/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_Fourier_1H: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_fourier");
        comp["Options/low/Units"].setString("Hours");
        comp["Options/low/Count"].setInt64(1);
        comp["Options/low/Align"].setBool(false);
        break;
    }
    case DisplayWidget::Smoothing_Fourier_1D: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_fourier");
        comp["Options/low/Units"].setString("Days");
        comp["Options/low/Count"].setInt64(1);
        comp["Options/low/Align"].setBool(false);
        break;
    }

    case DisplayWidget::Smoothing_3RSSH: {
        auto comp = config["Components"].toArray().after_back();
        comp["Name"].setString("smooth_3rssh");
        break;
    }

    }
}

static void doExportImage(QWidget *widget,
                          Display *display,
                          const DisplayDynamicContext &baseContext)
{
    if (!display)
        return;

    auto rawExtensions = QImageWriter::supportedImageFormats();
    rawExtensions.append(QByteArray("svg"));
    QString filter;
    QString allSuffixes;
    QString selectedFilter;
    for (const auto &add : rawExtensions) {
        QString str(add);
        if (str.isEmpty() || str.contains('%'))
            continue;
        str = str.toLower();

        if (!allSuffixes.isEmpty())
            allSuffixes.append(' ');
        allSuffixes.append("*.");
        allSuffixes.append(str);

        QString localFilter(DisplayWidget::tr("%1 Images (*.%2)").arg(str.toUpper(), str));

        if (str == "png") {
            if (!filter.isEmpty())
                filter.prepend(";;");
            filter.prepend(localFilter);
            selectedFilter = localFilter;
            continue;
        }

        if (!filter.isEmpty())
            filter.append(";;");
        filter.append(localFilter);
        if (selectedFilter.isEmpty() || str == "png")
            selectedFilter = localFilter;
    }
    if (allSuffixes.contains(' ')) {
        allSuffixes = DisplayWidget::tr(";;All Images (%1)").arg(allSuffixes);
        filter.append(allSuffixes);
        if (selectedFilter.isEmpty())
            selectedFilter = filter;
    }

    auto fileName =
            QFileDialog::getSaveFileName(widget, DisplayWidget::tr("Export Image"), QString(),
                                         filter, &selectedFilter);
    if (fileName.isEmpty())
        return;

    DisplayWidgetContextSizeDialog
            sizeDialog(!fileName.endsWith(".svg", Qt::CaseInsensitive), widget);
    sizeDialog.setWindowTitle(DisplayWidget::tr("Set Image Size"));
    if (sizeDialog.exec() != QDialog::Accepted)
        return;
    auto size = sizeDialog.getSelectedSize();
    if (size.width() < 1 || size.height() < 1)
        return;

    DisplayDynamicContext context = baseContext;
    context.setInteractive(false);
    if (fileName.endsWith(".svg", Qt::CaseInsensitive)) {
        QSvgGenerator svg;
        svg.setFileName(fileName);
        svg.setSize(size);
        svg.setViewBox(QRect(0, 0, size.width(), size.height()));
        QPainter painter;
        painter.begin(&svg);
        QRectF dimensions(0, 0, size.width(), size.height());
        display->prepareLayout(dimensions, painter.device(), context);
        display->predictLayout(dimensions, painter.device(), context);
        display->paint(&painter, dimensions, context);
        painter.end();
    } else {
        bool hit = false;
        for (QList<QByteArray>::const_iterator add = rawExtensions.constBegin(),
                end = rawExtensions.constEnd(); add != end; ++add) {
            if (fileName.endsWith(QString::fromLatin1(".%1").arg(QString(*add)))) {
                hit = true;
                break;
            }
        }
        if (!hit) {
            if (selectedFilter == allSuffixes) {
                fileName.append(".png");
            } else {
                QString first(selectedFilter.section(' ', 0, 0));
                if (first.isEmpty()) {
                    fileName.append(".png");
                } else {
                    fileName.append('.');
                    fileName.append(first.toLower());
                }
            }
        }

        QImage img(size, QImage::Format_ARGB32);
        img.fill(sizeDialog.getSelectedColor());
        QPainter painter;
        painter.begin(&img);
        QRectF dimensions(0, 0, size.width(), size.height());
        display->prepareLayout(dimensions, painter.device(), context);
        display->predictLayout(dimensions, painter.device(), context);
        display->paint(&painter, dimensions, context);
        painter.end();
        img.save(fileName);
    }
}

void DisplayWidget::promptExportImage()
{ doExportImage(this, p->display.get(), p->context); }

static void doPrint(QWidget *widget, Display *display, const DisplayDynamicContext &baseContext)
{
#if !defined(HAVE_PRINTING)
    Q_UNUSED(widget);
    Q_UNUSED(display);
    Q_UNUSED(baseContext);
#else
    if (!display)
        return;

    QPrinter printer;
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
    printer.setOrientation(QPrinter::Landscape);
#else
    printer.setPageOrientation(QPageLayout::Landscape);
#endif
    QPrintDialog dialog(&printer, widget);
    dialog.setWindowTitle(DisplayWidget::tr("Print Display"));
    dialog.setPrintRange(QAbstractPrintDialog::AllPages);
    dialog.setOption(QAbstractPrintDialog::PrintToFile, true);
    dialog.setOption(QAbstractPrintDialog::PrintPageRange, false);
    dialog.setOption(QAbstractPrintDialog::PrintCollateCopies, false);
    dialog.setOption(QAbstractPrintDialog::PrintCurrentPage, false);
    if (dialog.exec() != QDialog::Accepted)
        return;

    DisplayDynamicContext context = baseContext;
    context.setInteractive(false);

    QPainter painter;
    painter.begin(&printer);
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
    QRectF dimensions(printer.paperRect().x(), printer.paperRect().y(), printer.pageRect().width(),
                      printer.pageRect().height());
#else
    QRectF dimensions(printer.pageLayout().fullRectPixels(printer.resolution()));
#endif
    display->prepareLayout(dimensions, painter.device(), context);
    display->predictLayout(dimensions, painter.device(), context);
    display->paint(&painter, dimensions, context);
    painter.end();
#endif
}

void DisplayWidget::promptPrint()
{ doPrint(this, p->display.get(), p->context); }

namespace Internal {

DisplayWidgetContextDisplay::DisplayWidgetContextDisplay(DisplayWidget *set, QObject *parent)
        : QObject(parent), widget(set)
{
    Q_ASSERT(widget->p->display != NULL);
    zoom = widget->p->display->zoomAxes();
}


DisplayWidgetContextZoomDialog::DisplayWidgetContextZoomDialog(const QPointer<
        DisplayWidget> &parent, const std::vector<std::shared_ptr<DisplayZoomAxis> > &zoom)
        : QDialog(parent.data()), widget(parent), axes()
{
    setWindowTitle(tr("Zoom"));

    static const double exponentThresholdMax = 1E5;
    static const double exponentThresholdMin = 1E-2;

    QGridLayout *layout = new QGridLayout;
    layout->setContentsMargins(5, 5, 5, 5);

    std::deque<std::shared_ptr<DisplayZoomAxis>> remaining(zoom.begin(), zoom.end());
    int row = 0;
    bool anyCoupled = false;
    while (!remaining.empty()) {
        auto first = std::move(remaining.front());
        remaining.pop_front();
        auto add = first->getCoupledAxes();
        for (const auto &remove : add) {
            for (auto check = remaining.begin(); check != remaining.end();) {
                if (*check == remove) {
                    check = remaining.erase(check);
                    continue;
                }
                ++check;
            }
        }

        add.insert(add.begin(), first);

        QCheckBox *couple;
        int startRow = row;
        int startIdx = axes.size();
        if (add.size() > 1) {
            couple = new QCheckBox(tr("Lock", "axis range couple"), this);
            couple->setToolTip(tr("Lock the relative ranges of the axes together."));
            couple->setStatusTip(tr("Axis zoom range lock"));
            couple->setWhatsThis(
                    tr("When selected this keeps the ranges of the axes locked together."));
            couple->setCheckState(Qt::Checked);
        } else {
            couple = nullptr;
        }

        for (const auto &a : add) {
            double min = a->getMin();
            double max = a->getMax();

            Axis axis;
            axis.axis = a;
            axis.couple = nullptr;
            axis.min = min;
            axis.max = max;

            auto mode = a->getDisplayMode();
            if (mode == DisplayZoomAxis::Display_Time) {
                QString title(a->getTitle());
                if (!title.isEmpty()) {
                    QLabel *label = new QLabel(title);
                    layout->addWidget(label, row, 0, 1, 1, Qt::AlignRight);
                }
                axis.time = new TimeBoundSelection(this);
                axis.time->setToolTip(tr("Enter the time range to zoom to."));
                axis.time->setStatusTip(tr("Time range zoom"));
                axis.time->setWhatsThis(tr("This is the range of times to zoom to."));
                layout->addWidget(axis.time, row, 1, 1, 5, Qt::AlignLeft);
                axis.time->setAllowUndefinedStart(!FP::defined(min));
                axis.time->setAllowUndefinedEnd(!FP::defined(max));
                axis.time->setBounds(min, max);

                axis.simpleMin = nullptr;
                axis.exponentMin = nullptr;
                axis.simpleMax = nullptr;
                axis.exponentMax = nullptr;
            } else {
                if (!FP::defined(min) && !FP::defined(max))
                    continue;
                axis.time = NULL;

                QString title(a->getTitle());
                if (!title.isEmpty()) {
                    QLabel *label = new QLabel(title);
                    layout->addWidget(label, row, 0, 1, 1, Qt::AlignRight);
                }

                if (FP::defined(min)) {
                    QLabel *label = new QLabel(tr(" Min:", "axis min label"));
                    layout->addWidget(label, row, 1, 1, 1, Qt::AlignRight);
                    if (mode == DisplayZoomAxis::Display_Decimal ||
                            (mode == DisplayZoomAxis::Display_Automatic &&
                                    fabs(min) < exponentThresholdMax &&
                                    fabs(min) >= exponentThresholdMin &&
                                    (!FP::defined(max) ||
                                            (fabs(max) < exponentThresholdMax &&
                                                    fabs(max) >= exponentThresholdMin)))) {
                        axis.exponentMin = nullptr;
                        axis.simpleMin = new QDoubleSpinBox(this);
                        axis.simpleMin
                            ->setRange(-exponentThresholdMax * 10, exponentThresholdMax * 10);
                        axis.simpleMin->setValue(min);
                        layout->addWidget(axis.simpleMin, row, 2, 1, 1, Qt::AlignLeft);

                        axis.simpleMin->setToolTip(tr("Enter the minimum value to display."));
                        axis.simpleMin->setStatusTip(tr("Minimum value"));
                        axis.simpleMin->setWhatsThis(tr("This is the minimum value to display."));
                    } else {
                        axis.simpleMin = nullptr;
                        axis.exponentMin = new ExponentEdit(this);
                        axis.exponentMin->setValue(min);
                        layout->addWidget(axis.exponentMin, row, 2, 1, 1, Qt::AlignLeft);

                        axis.exponentMin->setToolTip(tr("Enter the minimum value to display."));
                        axis.exponentMin->setStatusTip(tr("Minimum value"));
                        axis.exponentMin->setWhatsThis(tr("This is the minimum value to display."));
                    }
                } else {
                    axis.simpleMin = nullptr;
                    axis.exponentMin = nullptr;
                }


                if (FP::defined(max)) {
                    QLabel *label = new QLabel(tr(" Max:", "axis max label"));
                    layout->addWidget(label, row, 3, 1, 1, Qt::AlignRight);
                    if (mode == DisplayZoomAxis::Display_Decimal ||
                            (mode == DisplayZoomAxis::Display_Automatic &&
                                    fabs(max) < exponentThresholdMax &&
                                    fabs(max) >= exponentThresholdMin &&
                                    (!FP::defined(min) ||
                                            (fabs(min) < exponentThresholdMax &&
                                                    fabs(min) >= exponentThresholdMin)))) {
                        axis.exponentMax = nullptr;
                        axis.simpleMax = new QDoubleSpinBox(this);
                        axis.simpleMax
                            ->setRange(-exponentThresholdMax * 10, exponentThresholdMax * 10);
                        axis.simpleMax->setValue(max);
                        layout->addWidget(axis.simpleMax, row, 4, 1, 1, Qt::AlignLeft);

                        axis.simpleMax->setToolTip(tr("Enter the maximum value to display."));
                        axis.simpleMax->setStatusTip(tr("Maximum value"));
                        axis.simpleMax->setWhatsThis(tr("This is the maximum value to display."));
                    } else {
                        axis.simpleMax = nullptr;
                        axis.exponentMax = new ExponentEdit(this);
                        axis.exponentMax->setValue(max);
                        layout->addWidget(axis.exponentMax, row, 4, 1, 1, Qt::AlignLeft);

                        axis.exponentMax->setToolTip(tr("Enter the maximum value to display."));
                        axis.exponentMax->setStatusTip(tr("Maximum value"));
                        axis.exponentMax->setWhatsThis(tr("This is the maximum value to display."));
                    }
                } else {
                    axis.simpleMax = nullptr;
                    axis.exponentMax = nullptr;
                }
            }

            if (axis.time) {
                axis.time->setProperty("axisIndex", static_cast<int>(axes.size()));
            }
            if (axis.simpleMin) {
                axis.simpleMin->setProperty("axisIndex", static_cast<int>(axes.size()));
                axis.simpleMin->setProperty("axisMin", true);
            }
            if (axis.simpleMax) {
                axis.simpleMax->setProperty("axisIndex", static_cast<int>(axes.size()));
                axis.simpleMax->setProperty("axisMin", false);
            }
            if (axis.exponentMin) {
                axis.exponentMin->setProperty("axisIndex", static_cast<int>(axes.size()));
                axis.exponentMin->setProperty("axisMin", true);
            }
            if (axis.exponentMax) {
                axis.exponentMax->setProperty("axisIndex", static_cast<int>(axes.size()));
                axis.exponentMax->setProperty("axisMin", false);
            }

            connectAxis(axis);
            axes.emplace_back(std::move(axis));

            row++;
        }

        if (couple && row != startRow) {
            layout->addWidget(couple, startRow, 5, row - startRow, 1, Qt::AlignLeft);

            for (std::size_t i = startIdx; i < axes.size(); i++) {
                axes[i].couple = couple;
            }
            anyCoupled = true;
        } else if (couple) {
            delete couple;
        }
    }

    if (!anyCoupled) {
        layout->addWidget(new QWidget(this), 0, 5, 1, 1);
    }

    layout->setColumnStretch(5, 1);

    QDialogButtonBox *buttonBox = new QDialogButtonBox(Qt::Horizontal, this);
    layout->addWidget(buttonBox, row, 0, 1, -1);

    QPushButton *button;

    button = buttonBox->addButton(QDialogButtonBox::Cancel);
    connect(button, SIGNAL(clicked()), this, SLOT(reject()));

    button = new QPushButton(tr("&Apply"), this);
    button->setToolTip(tr("Apply the zoom to all relevant axes on the currently visible display."));
    button->setWhatsThis(
            tr("This applies the given zoom to all relevant axes on the currently visible display."));
    buttonBox->addButton(button, QDialogButtonBox::ApplyRole);
    connect(button, SIGNAL(clicked()), this, SLOT(applyLocal()));

    button = new QPushButton(tr("Apply to &Visible"), this);
    button->setToolTip(tr("Apply the zoom to all relevant axes on the currently visible display."));
    button->setWhatsThis(
            tr("This applies the given zoom to all relevant axes on the currently visible display."));
    buttonBox->addButton(button, QDialogButtonBox::ApplyRole);
    connect(button, SIGNAL(clicked()), this, SLOT(applyVisible()));

    button = new QPushButton(tr("Apply &Globally"), this);
    button->setToolTip(tr("Apply the zoom to all relevant axes on all displays."));
    button->setWhatsThis(tr("This applies the given zoom to all relevant axes on all displays."));
    buttonBox->addButton(button, QDialogButtonBox::ApplyRole);
    connect(button, SIGNAL(clicked()), this, SLOT(applyGlobal()));

    setLayout(layout);
}

void DisplayWidgetContextZoomDialog::connectAxis(const Axis &axis)
{
    if (axis.time) {
        connect(axis.time, SIGNAL(boundsEdited(double, double)), this, SLOT(axisLimitsChanged()));
    }
    if (axis.simpleMin) {
        connect(axis.simpleMin, SIGNAL(valueChanged(double)), this, SLOT(axisLimitsChanged()));
    }
    if (axis.simpleMax) {
        connect(axis.simpleMax, SIGNAL(valueChanged(double)), this, SLOT(axisLimitsChanged()));
    }
    if (axis.exponentMin) {
        connect(axis.exponentMin, SIGNAL(valueChanged(double)), this, SLOT(axisLimitsChanged()));
    }
    if (axis.exponentMax) {
        connect(axis.exponentMax, SIGNAL(valueChanged(double)), this, SLOT(axisLimitsChanged()));
    }
}

void DisplayWidgetContextZoomDialog::changeAxisBounds(const Axis &axis, double min, double max)
{
    if (axis.time) {
        axis.time->disconnect();
        axis.time->setBounds(min, max);
    }
    if (axis.simpleMin) {
        axis.simpleMin->disconnect();
        axis.simpleMin->setValue(min);
    }
    if (axis.simpleMax) {
        axis.simpleMax->disconnect();
        axis.simpleMax->setValue(max);
    }
    if (axis.exponentMin) {
        axis.exponentMin->disconnect();
        axis.exponentMin->setValue(min);
    }
    if (axis.exponentMax) {
        axis.exponentMax->disconnect();
        axis.exponentMax->setValue(max);
    }
    connectAxis(axis);
}

void DisplayWidgetContextZoomDialog::axisLimitsChanged()
{
    QWidget *source = qobject_cast<QWidget *>(sender());
    if (!source)
        return;
    QVariant v(source->property("axisIndex"));
    if (!v.isValid())
        return;
    int idx = v.toInt();
    if (idx < 0 || idx >= static_cast<int>(axes.size()))
        return;
    const auto &axis = axes[idx];

    double oldMin = axis.min;
    double oldMax = axis.max;
    double min = oldMin;
    double max = oldMax;
    if (axis.time) {
        min = axis.time->getStart();
        max = axis.time->getEnd();
    } else {
        v = source->property("axisMin");
        if (!v.isValid()) {
            if (axis.simpleMin)
                min = axis.simpleMin->value();
            else if (axis.exponentMin)
                min = axis.exponentMin->getValue();

            if (axis.simpleMax)
                max = axis.simpleMax->value();
            else if (axis.exponentMax)
                max = axis.exponentMax->getValue();
        } else if (v.toBool()) {
            if (axis.simpleMin)
                min = axis.simpleMin->value();
            else if (axis.exponentMin)
                min = axis.exponentMin->getValue();
        } else {
            if (axis.simpleMax)
                max = axis.simpleMax->value();
            else if (axis.exponentMax)
                max = axis.exponentMax->getValue();
        }
    }

    if (!FP::defined(min) || !FP::defined(max)) {
        axes[idx].min = min;
        axes[idx].max = max;
        return;
    }

    if (min >= max) {
        if (min != oldMin) {
            int pwr;
            if (fabs(max) < 1E-8) {
                pwr = -10;
            } else {
                pwr = (int) floor(log10(fabs(max))) - 2;
            }
            max = min + pow(10.0, pwr);
        } else {
            int pwr;
            if (fabs(min) < 1E-8) {
                pwr = -10;
            } else {
                pwr = (int) floor(log10(fabs(min))) - 2;
            }
            min = max - pow(10.0, pwr);
        }
        changeAxisBounds(axis, min, max);
    }

    if (!axis.couple || axis.couple->checkState() != Qt::Checked) {
        axes[idx].min = min;
        axes[idx].max = max;
        return;
    }

    if (!FP::defined(oldMin) || !FP::defined(oldMax) || oldMin == oldMax) {
        axes[idx].min = min;
        axes[idx].max = max;
        return;
    }

    for (std::size_t i = 0, size = axes.size(); i < size; i++) {
        auto &target = axes[i];
        if (static_cast<int>(i) == idx || target.couple != axis.couple)
            continue;
        double otherMin = target.min;
        double otherMax = target.max;

        if (!FP::defined(otherMin) || !FP::defined(otherMax))
            continue;

        double scale = (otherMax - otherMin) / (oldMax - oldMin);
        double newMin = otherMin + scale * (min - oldMin);
        double newMax = otherMax + scale * (max - oldMax);
        if (otherMin == newMin && otherMax == newMax)
            continue;
        changeAxisBounds(target, newMin, newMax);
        target.min = newMin;
        target.max = newMax;
    }

    axes[idx].min = min;
    axes[idx].max = max;
}

DisplayZoomCommand DisplayWidgetContextZoomDialog::constructCommand() const
{
    DisplayZoomCommand cmd;
    for (const auto &it : axes) {
        cmd.add(it.axis, it.min, it.max);
    }
    return cmd;
}

void DisplayWidgetContextZoomDialog::applyLocal()
{
    if (widget.isNull())
        return;
    widget->emitZoom(constructCommand());
    accept();
}

void DisplayWidgetContextZoomDialog::applyVisible()
{
    if (widget.isNull())
        return;
    widget->emitVisibleZoom(constructCommand());
    accept();
}

void DisplayWidgetContextZoomDialog::applyGlobal()
{
    if (widget.isNull())
        return;
    widget->emitGlobalZoom(constructCommand());
    accept();
}

void DisplayWidgetContextDisplay::manualZoom()
{
    if (widget.isNull())
        return;
    if (zoom.empty())
        return;
    DisplayWidgetContextZoomDialog dialog(widget, zoom);
    dialog.setModal(true);
    dialog.exec();
}

void DisplayWidgetContextDisplay::resetZoom()
{
    if (widget.isNull())
        return;
    if (widget->getResetSelectsAll())
        widget->emitResetTimes();
    if (zoom.empty())
        return;
    DisplayZoomCommand command;
    for (const auto &axis : zoom) {
        command.add(axis, FP::undefined(), FP::undefined());
    }
    widget->emitZoom(command);
}

DisplayWidget *DisplayWidgetContextDisplay::outermostWidget()
{
    DisplayWidget *transverse = widget.data();
    for (;;) {
        DisplayWidget *next = qobject_cast<DisplayWidget *>(transverse->parent());
        if (!next)
            break;
        transverse = next;
    }
    return transverse;
}

DisplayWidgetContextSizeDialog::DisplayWidgetContextSizeDialog(bool enableColor, QWidget *parent)
        : QDialog(parent)
{
    setModal(true);
    QGridLayout *layout = new QGridLayout;
    layout->setContentsMargins(5, 5, 5, 5);

    QLabel *label = new QLabel(tr("Width:"), this);
    layout->addWidget(label, 0, 0, Qt::AlignRight);
    width = new QSpinBox(this);
    width->setToolTip(tr("The width of the output in pixels."));
    width->setStatusTip(tr("Output image width"));
    width->setWhatsThis(tr("This is the width of the resulting image in pixels."));
    width->setMinimum(5);
    width->setMaximum(66535);
    width->setSingleStep(50);
    if (parent != 0)
        width->setValue(parent->width());
    else
        width->setValue(1024);
    layout->addWidget(width, 0, 1, Qt::AlignLeft);

    label = new QLabel(tr("Height:"), this);
    layout->addWidget(label, 0, 2, Qt::AlignRight);
    height = new QSpinBox(this);
    height->setToolTip(tr("The height of the output in pixels."));
    height->setStatusTip(tr("Output image height"));
    height->setWhatsThis(tr("This is the height of the resulting image in pixels."));
    height->setMinimum(5);
    height->setMaximum(66535);
    height->setSingleStep(50);
    if (parent != 0)
        height->setValue(parent->height());
    else
        height->setValue(768);
    layout->addWidget(height, 0, 3, Qt::AlignLeft);

    preserveAspectRatio = new QCheckBox(tr("Preserve Aspect Ratio"), this);
    preserveAspectRatio->setToolTip(tr("Lock the ratio of the width to height."));
    preserveAspectRatio->setStatusTip(tr("Keep the aspect ratio fixed"));
    preserveAspectRatio->setWhatsThis(
            tr("When checked the width or height will adjust when the other is edited to preserve the ratio between the two."));
    preserveAspectRatio->setCheckState(Qt::Checked);
    layout->addWidget(preserveAspectRatio, 0, 4, Qt::AlignRight);

    QDialogButtonBox *buttonBox =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 this);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    QPushButton *button = new QPushButton(tr("1024x768"), this);
    button->setToolTip(tr("Set the current dimensions to 1024x768."));
    button->setStatusTip(tr("Set to 1024x768"));
    button->setWhatsThis(
            tr("This changes the size to 1024x768 and sets the aspect ration accordingly."));
    connect(button, SIGNAL(clicked()), this, SLOT(set1024x768()));
    buttonBox->addButton(button, QDialogButtonBox::ActionRole);

    if (enableColor) {
        selectedColor = QColor(Qt::white);

        button = new QPushButton(tr("Transparent"), this);
        button->setToolTip(tr("Generate the image with a transparent background."));
        button->setStatusTip(tr("Transparent background"));
        button->setWhatsThis(
                tr("This accepts the image export and uses a transparent background."));

        if (auto okButton = buttonBox->button(QDialogButtonBox::Ok)) {
            button->setIcon(okButton->icon());
        }

        connect(button, &QAbstractButton::clicked, this, [this] {
            selectedColor = QColor(255, 255, 255, 0);
            accept();
        });
        buttonBox->addButton(button, QDialogButtonBox::AcceptRole);
    }

    layout->addWidget(buttonBox, 1, 0, 1, -1);

    setLayout(layout);

    aspectRatio = (qreal) width->value() / (qreal) height->value();
    connect(width, SIGNAL(valueChanged(int)), this, SLOT(widthChanged(int)));
    connect(height, SIGNAL(valueChanged(int)), this, SLOT(heightChanged(int)));
}

void DisplayWidgetContextSizeDialog::widthChanged(int w)
{
    if (preserveAspectRatio->checkState() != Qt::Checked) {
        if (height->value() > 0)
            aspectRatio = (qreal) w / (qreal) height->value();
        return;
    }
    height->disconnect();
    if (aspectRatio > 0.0)
        height->setValue((int) qRound((qreal) w / aspectRatio));
    connect(height, SIGNAL(valueChanged(int)), this, SLOT(heightChanged(int)));
}

void DisplayWidgetContextSizeDialog::heightChanged(int h)
{
    if (preserveAspectRatio->checkState() != Qt::Checked) {
        if (h > 0)
            aspectRatio = (qreal) width->value() / h;
        return;
    }
    width->disconnect();
    width->setValue((int) qRound((qreal) h * aspectRatio));
    connect(width, SIGNAL(valueChanged(int)), this, SLOT(widthChanged(int)));
}

void DisplayWidgetContextSizeDialog::set1024x768()
{
    width->disconnect();
    height->disconnect();
    width->setValue(1024);
    height->setValue(768);
    aspectRatio = (qreal) width->value() / (qreal) height->value();
    connect(width, SIGNAL(valueChanged(int)), this, SLOT(widthChanged(int)));
    connect(height, SIGNAL(valueChanged(int)), this, SLOT(heightChanged(int)));
}

QSize DisplayWidgetContextSizeDialog::getSelectedSize() const
{ return QSize(width->value(), height->value()); }

QColor DisplayWidgetContextSizeDialog::getSelectedColor() const
{ return selectedColor; }

void DisplayWidgetContextDisplay::exportImage(DisplayWidget *widget)
{ doExportImage(widget, widget->p->display.get(), widget->p->context); }

void DisplayWidgetContextDisplay::exportImage()
{
    if (widget.isNull())
        return;
    exportImage(widget.data());
}

void DisplayWidgetContextDisplay::exportImageAll()
{
    if (widget.isNull())
        return;
    exportImage(outermostWidget());
}

void DisplayWidgetContextDisplay::print(DisplayWidget *widget)
{ doPrint(widget, widget->p->display.get(), widget->p->context); }

void DisplayWidgetContextDisplay::print()
{
    if (widget.isNull())
        return;
    print(widget.data());
}

void DisplayWidgetContextDisplay::printAll()
{
    if (widget.isNull())
        return;
    print(outermostWidget());
}


void DisplayWidgetContextDisplay::displayPopup(DisplayWidget *widget)
{
    auto base = widget->p->display.get();
    if (!base)
        return;

    QDialog *dialog = new QDialog(widget);
    dialog->setAttribute(Qt::WA_DeleteOnClose, true);
    dialog->setWindowTitle("CPD3 Display");
    dialog->setStyleSheet("QDialog { background-color: white }");

    QGridLayout *layout = new QGridLayout;
    layout->setContentsMargins(1, 1, 1, 1);

    DisplayWidget *displayWidget = new DisplayWidget(base->clone(), dialog);
    displayWidget->setResetSelectsAll(true);
    connect(displayWidget, SIGNAL(internalZoom(CPD3::Graphing::DisplayZoomCommand)), displayWidget,
            SLOT(applyZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(displayWidget, SIGNAL(visibleZoom(CPD3::Graphing::DisplayZoomCommand)), displayWidget,
            SLOT(applyZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(displayWidget, SIGNAL(globalZoom(CPD3::Graphing::DisplayZoomCommand)), displayWidget,
            SLOT(applyZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(displayWidget, SIGNAL(timeRangeSelected(double, double)), displayWidget,
            SLOT(setVisibleTimeRange(double, double)));
    layout->addWidget(displayWidget, 0, 0);
    dialog->setLayout(layout);
    dialog->resize(widget->size());

    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

void DisplayWidgetContextDisplay::displayPopup()
{
    if (widget.isNull())
        return;
    displayPopup(widget.data());
}

void DisplayWidgetContextDisplay::displayPopupAll()
{
    if (widget.isNull())
        return;
    displayPopup(outermostWidget());
}

void DisplayWidgetContextDisplay::displayFullScreen(DisplayWidget *widget)
{
    auto base = widget->p->display.get();
    if (!base)
        return;

    static bool haveShownWarning = false;
    if (!haveShownWarning) {
        haveShownWarning = true;
        QMessageBox::information(widget, tr("Entering Full Screen Mode"),
                                 tr("Press ESCAPE or SPACE BAR to exit full screen mode."),
                                 QMessageBox::Ok, QMessageBox::Ok);
    }

    QDialog *dialog = new QDialog(widget);
    dialog->setAttribute(Qt::WA_DeleteOnClose, true);
    dialog->setWindowTitle("CPD3 Full Screen");
    dialog->setStyleSheet("QDialog { background-color: white }");

    new QShortcut(Qt::Key_Escape, dialog, SLOT(close()));
    new QShortcut(Qt::Key_Space, dialog, SLOT(close()));

    QGridLayout *layout = new QGridLayout;
    layout->setContentsMargins(10, 10, 10, 10);

    DisplayWidget *displayWidget = new DisplayWidget(base->clone(), dialog);
    displayWidget->setResetSelectsAll(true);
    connect(displayWidget, SIGNAL(internalZoom(CPD3::Graphing::DisplayZoomCommand)), displayWidget,
            SLOT(applyZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(displayWidget, SIGNAL(visibleZoom(CPD3::Graphing::DisplayZoomCommand)), displayWidget,
            SLOT(applyZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(displayWidget, SIGNAL(globalZoom(CPD3::Graphing::DisplayZoomCommand)), displayWidget,
            SLOT(applyZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(displayWidget, SIGNAL(timeRangeSelected(double, double)), displayWidget,
            SLOT(setVisibleTimeRange(double, double)), Qt::QueuedConnection);
    layout->addWidget(displayWidget, 0, 0);
    dialog->setLayout(layout);
    dialog->resize(widget->size());

    dialog->showFullScreen();
    dialog->raise();
    dialog->activateWindow();
}

void DisplayWidgetContextDisplay::displayFullScreen()
{
    if (widget.isNull())
        return;
    displayFullScreen(widget.data());
}

void DisplayWidgetContextDisplay::displayFullScreenAll()
{
    if (widget.isNull())
        return;
    displayFullScreen(outermostWidget());
}

void DisplayWidgetContextDisplay::changeSmoothing(DisplayWidget *widget,
                                                  DisplayWidget::SmoothingType type,
                                                  bool contam)
{
    widget->setSmoothing(type, contam);
}

void DisplayWidgetContextDisplay::changeSmoothing(DisplayWidget::SmoothingType type, bool contam)
{
    if (widget.isNull())
        return;
    changeSmoothing(widget.data(), type, contam);
}

void DisplayWidgetContextDisplay::changeAllSmoothing(DisplayWidget::SmoothingType type, bool contam)
{
    if (widget.isNull())
        return;
    changeSmoothing(outermostWidget(), type, contam);
}

}

DisplayWidgetTapChain::DisplayWidgetTapChain()
        : ProcessingTapChain(),
          smoothingType(DisplayWidget::Smoothing_Disabled),
          removeContamination(false)
{ }

DisplayWidgetTapChain::~DisplayWidgetTapChain()
{ }

void DisplayWidgetTapChain::setSmoothing(DisplayWidget::SmoothingType type, bool contam)
{
    smoothingType = type;
    removeContamination = contam;
}

void DisplayWidgetTapChain::clearSmoothing()
{
    smoothingType = DisplayWidget::Smoothing_Disabled;
    removeContamination = false;
}

void DisplayWidgetTapChain::add(StreamSink *target,
                                const Variant::Read &config,
                                const SequenceMatch::Composite &selection)
{
    Variant::Root local(config);
    DisplayWidget::modifyConfigForSmoother(smoothingType, removeContamination, local.write());
    ProcessingTapChain::add(target, local, selection);
}

void DisplayWidgetTapChain::add(QList<QPair<SequenceMatch::Composite, StreamSink *> > &targets,
                                const Variant::Read &config)
{
    Variant::Root local(config);
    DisplayWidget::modifyConfigForSmoother(smoothingType, removeContamination, local.write());
    ProcessingTapChain::add(targets, local);
}

}
}
