/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGAXISTRANSFORMER_H
#define CPD3GRAPHINGAXISTRANSFORMER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "graphing/graphing.hxx"
#include "datacore/variant/root.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Graphing {

class AxisTransformerBoundDialog;

/**
 * Provides the interface to transform points in real space to screen space
 * for an axis.
 */
class CPD3GRAPHING_EXPORT AxisTransformer {
    double realMin;
    double realMax;
    double screenMin;
    double screenMax;

    bool isLog;
    bool reverse;
    double scale;
    double realOffset;

    enum BoundMode {
        Fixed, ExtendFraction, ExtendAbsolute, ExtendPower,
    };

    BoundMode minMode;
    double boundMin;
    double boundMinFractionAtLeast;
    double boundMinFractionAtMost;

    BoundMode maxMode;
    double boundMax;
    double boundMaxFractionAtLeast;
    double boundMaxFractionAtMost;

    double absoluteMin;
    double absoluteMax;

    double requiredMin;
    double requiredMax;

    friend class AxisTransformerBoundDialog;

public:
    AxisTransformer();

    AxisTransformer(const AxisTransformer &other);

    AxisTransformer &operator=(const AxisTransformer &other);

    AxisTransformer(AxisTransformer &&other);

    AxisTransformer &operator=(AxisTransformer &&other);

    AxisTransformer(const Data::Variant::Read &configuration);

    Data::Variant::Root toConfiguration() const;

    void setReal(double &min,
                 double &max,
                 double zoomMin = FP::undefined(),
                 double zoomMax = FP::undefined());

    void setReal(const double &min,
                 const double &max,
                 double zoomMin = FP::undefined(),
                 double zoomMax = FP::undefined());

    void setScreen(double min, double max);

    double toScreen(double value, bool clip = true) const;

    double toReal(double value, bool clip = true) const;

    bool screenClipped(double value) const;

    double getScreenMin() const;

    double getScreenMax() const;

    double getRealMin() const;

    double getRealMax() const;

    bool isLogarithmic() const;

    void setLogarithmic(bool l);

    bool isReversed() const;

    void setReversed(bool reversed);

    void setMinFixed(double value);

    void setMinExtendFraction(double fraction,
                              double atLeast = FP::undefined(),
                              double atMost = FP::undefined());

    void setMinExtendAbsolute(double amount);

    void setMinExtendPowerRound(double base = 10.0);

    void setAbsoluteMin(double min);

    void setRequiredMin(double min);

    void setMaxFixed(double value);

    void setMaxExtendFraction(double fraction,
                              double atLeast = FP::undefined(),
                              double atMost = FP::undefined());

    void setMaxExtendAbsolute(double amount);

    void setMaxExtendPowerRound(double base = 10.0);

    void setAbsoluteMax(double max);

    void setRequiredMax(double max);
};

}
}


#endif
