/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGBIN2D_H
#define CPD3GRAPHINGBIN2D_H

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QList>

#include "graphing/graphing.hxx"
#include "algorithms/model.hxx"
#include "graphing/tracedispatch.hxx"
#include "graphing/tracecommon.hxx"
#include "graphing/graphcommon2d.hxx"
#include "graphing/axistransformer.hxx"
#include "graphing/axisdimension.hxx"
#include "graphing/binset.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * A specialization of the general bin type that caries additional information
 * needed for painting.
 */
struct CPD3GRAPHING_EXPORT Bin2DPaintPoint : public BinPoint<1, 1> {
    /** The paint center of the bin */
    double paintCenter;
    /** The paint width, in pixels if < 0 */
    double paintWidth;

    inline Bin2DPaintPoint()
    { }

    inline Bin2DPaintPoint(const BinPoint<1, 1> &point) : BinPoint<1, 1>(point),
                                                          paintCenter(point.center[0]),
                                                          paintWidth(point.width[0])
    { }
};

class BinOutput2D;

/**
 * The parameters for two dimensional box whisker plots.
 */
class CPD3GRAPHING_EXPORT BinParameters2D : public BinParameters {
public:
    /** How bins are drawn. */
    enum DrawMode {
        /** Use the median width times a constant scale. */
        Draw_WidthConstantScale, /** Use the individual width times a constant scale. */
        Draw_WidthScale, /** Use a fixed width in real space. */
        Draw_WidthFixedReal, /** Use a fixed width in screen space. */
        Draw_WidthFixedScreen,
    };
    /** The possible origins for fits. */
    enum FitOrigin {
        /** On the lowest coordinate of the bin. */
        Fit_Lowest = 0, /** On the lower coordinate of the bin. */
        Fit_Lower, /** On the middle coordinate of the bin. */
        Fit_Middle, /** On the upper coordinate of the bin. */
        Fit_Upper, /** On the uppermost coordinate of the bin. */
        Fit_Uppermost,
    };

    /* C++ < 14 does not provide specialization for enums */
    struct FitOriginHash {
        inline std::size_t operator()(FitOrigin t) const
        { return std::hash<std::size_t>()(static_cast<std::size_t>(t)); }
    };

private:
    enum {
        Component_DrawPriority = 0x00000001,
        Component_FillEnable = 0x00000002,
        Component_FillColor = 0x00000004,
        Component_OutlineEnable = 0x00000008,
        Component_OutlineWidth = 0x00000010,
        Component_OutlineStyle = 0x00000020,
        Component_OutlineColor = 0x00000040,
        Component_WhiskerEnable = 0x00000080,
        Component_WhiskerLineWidth = 0x00000100,
        Component_WhiskerLineStyle = 0x00000200,
        Component_WhiskerColor = 0x00000400,
        Component_WhiskerCapWidth = 0x00000800,
        Component_MiddleEnable = 0x00001000,
        Component_MiddleLineWidth = 0x00002000,
        Component_MiddleLineStyle = 0x00004000,
        Component_MiddleColor = 0x00008000,
        Component_MiddleExtendEnable = 0x00010000,
        Component_MiddleExtendLineWidth = 0x00020000,
        Component_MiddleExtendLineStyle = 0x00040000,
        Component_MiddleExtendColor = 0x00080000,
        Component_MiddleExtendDrawPriority = 0x00100000,
        Component_LegendEntry = 0x00200000,
        Component_LegendText = 0x00400000,
        Component_LegendFont = 0x00800000,
        Component_LegendSortPriority = 0x01000000,
        Component_IBinding = 0x02000000,
        Component_DBinding = 0x04000000,
        Component_DrawMode = 0x08000000,
        Component_DrawBoxes = 0x10000000,
        Component_RelocationEnable = 0x20000000,
    };
    int components;

    int drawPriority;
    bool fillEnable;
    QColor fillColor;
    bool outlineEnable;
    qreal outlineWidth;
    Qt::PenStyle outlineStyle;
    QColor outlineColor;
    bool whiskerEnable;
    qreal whiskerLineWidth;
    Qt::PenStyle whiskerLineStyle;
    QColor whiskerColor;
    qreal whiskerCapWidth;
    bool middleEnable;
    qreal middleLineWidth;
    Qt::PenStyle middleLineStyle;
    QColor middleColor;
    bool middleExtendEnable;
    qreal middleExtendLineWidth;
    Qt::PenStyle middleExtendLineStyle;
    QColor middleExtendColor;
    int middleExtendDrawPriority;
    bool legendEntry;
    QString legendText;
    QFont legendFont;
    int legendSortPriority;
    QString iBinding;
    QString dBinding;
    DrawMode drawMode;
    double drawConstant;
    bool drawBoxes;
    bool relocationEnable;

    struct Fit {
        enum {
            Component_Model = 0x00000001,
            Component_LogIBase = 0x00000002,
            Component_LogDBase = 0x00000004,
            Component_LineWidth = 0x00000008,
            Component_LineStyle = 0x00000010,
            Component_DrawPriority = 0x00000020,
            Component_Color = 0x00000040,
            Component_LegendText = 0x00000080,
            Component_LegendFont = 0x00000100,
        };
        int components;
        Data::Variant::Root model;
        double logIBase;
        double logDBase;
        qreal lineWidth;
        Qt::PenStyle lineStyle;
        int drawPriority;
        QColor color;
        QString legendText;
        QFont legendFont;

        Fit();

        Fit(const Fit &other);

        Fit &operator=(const Fit &other);

        Fit(Fit &&other);

        Fit &operator=(Fit &&other);

        Fit(const Data::Variant::Read &value);

        void save(Data::Variant::Write &value) const;

        void overlay(const Fit &top);

        void clear();
    };

    std::unordered_map<FitOrigin, Fit, FitOriginHash> fits;
public:
    BinParameters2D();

    virtual ~BinParameters2D();

    BinParameters2D(const BinParameters2D &other);

    BinParameters2D &operator=(const BinParameters2D &other);

    BinParameters2D(BinParameters2D &&other);

    BinParameters2D &operator=(BinParameters2D &&other);

    BinParameters2D(const Data::Variant::Read &value);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<TraceParameters> clone() const override;

    void overlay(const TraceParameters *over) override;

    void copy(const TraceParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify the box draw priority.
     * @return true if the parameters specify the box draw priority
     */
    inline bool hasDrawPriority() const
    { return components & Component_DrawPriority; }

    /**
     * Get the box draw priority.
     * @return the box draw priority
     */
    inline int getDrawPriority() const
    { return drawPriority; }

    /**
     * Test if the parameters specify the fill enable state.
     * @return true if the parameters specify the fill enable state
     */
    inline bool hasFillEnable() const
    { return components & Component_FillEnable; }

    /**
     * Get the fill enable state.
     * @return true to draw the box fill
     */
    inline bool getFillEnable() const
    { return fillEnable; }

    /**
     * Change the fill enable state.  This sets the defined flag.
     * @param enable    enable the fill
     */
    inline void overrideFillEnable(bool enable)
    {
        fillEnable = enable;
        components |= Component_FillEnable;
    }

    /**
     * Test if the parameters specify the box fill color.
     * @return true if the parameters specify the box fill color
     */
    inline bool hasFillColor() const
    { return components & Component_FillColor; }

    /**
     * Get the box fill color.
     * @return the box fill color
     */
    inline QColor getFillColor() const
    { return fillColor; }

    /**
     * Change the fill color.  This does not set the "color defined" flag.
     * @param set the new color
     */
    inline void setFillColor(const QColor &set)
    { fillColor = set; }

    /**
     * Change the outline color.  This sets the defined flag.
     * @param color the new color
     */
    inline void overrideFillColor(const QColor &color)
    {
        fillColor = color;
        components |= Component_FillColor;
    }

    /**
     * Test if the parameters specify the outline enable state.
     * @return true if the parameters specify the outline enable state
     */
    inline bool hasOutlineEnable() const
    { return components & Component_OutlineEnable; }

    /**
     * Get the outline enable state.
     * @return true to draw the box outline
     */
    inline bool getOutlineEnable() const
    { return outlineEnable; }

    /**
     * Test if the parameters specify the outline width.
     * @return true if the parameters specify the outline width
     */
    inline bool hasOutlineWidth() const
    { return components & Component_OutlineWidth; }

    /**
     * Get the outline width.
     * @return the outline width
     */
    inline qreal getOutlineWidth() const
    { return outlineWidth; }

    /**
     * Test if the parameters specify the outline style.
     * @return true if the parameters specify the outline style
     */
    inline bool hasOutlineStyle() const
    { return components & Component_OutlineStyle; }

    /**
     * Get the outline style.
     * @return the outline style
     */
    inline Qt::PenStyle getOutlineStyle() const
    { return outlineStyle; }

    /**
     * Test if the parameters specify the outline color.
     * @return true if the parameters specify the outline color
     */
    inline bool hasOutlineColor() const
    { return components & Component_OutlineColor; }

    /**
     * Get the outline color.
     * @return the outline color
     */
    inline QColor getOutlineColor() const
    { return outlineColor; }

    /**
     * Change the outline color.  This does not set the "color defined" flag.
     * @param set the new color
     */
    inline void setOutlineColor(const QColor &set)
    { outlineColor = set; }

    /**
     * Change the outline color.  This sets the defined flag.
     * @param color the new color
     */
    inline void overrideOutlineColor(const QColor &color)
    {
        outlineColor = color;
        components |= Component_OutlineColor;
    }

    /**
     * Test if the parameters specify the whisker enable state.
     * @return true if the parameters specify the whisker enable state
     */
    inline bool hasWhiskerEnable() const
    { return components & Component_WhiskerEnable; }

    /**
     * Get the whisker enable state.
     * @return true to draw the box whisker
     */
    inline bool getWhiskerEnable() const
    { return whiskerEnable; }

    /**
     * Override the whisker enable state to the given value.  This sets the
     * defined flag.
     * @param s the new whisker enable state
     */
    inline void overrideWhiskerEnable(bool s)
    {
        whiskerEnable = s;
        components |= Component_WhiskerEnable;
    }

    /**
     * Test if the parameters specify the whisker width.
     * @return true if the parameters specify the whisker width
     */
    inline bool hasWhiskerLineWidth() const
    { return components & Component_WhiskerLineWidth; }

    /**
     * Get the whisker width.
     * @return the whisker width
     */
    inline qreal getWhiskerLineWidth() const
    { return whiskerLineWidth; }

    /**
     * Test if the parameters specify the whisker style.
     * @return true if the parameters specify the whisker style
     */
    inline bool hasWhiskerLineStyle() const
    { return components & Component_WhiskerLineStyle; }

    /**
     * Get the whisker style.
     * @return the whisker style
     */
    inline Qt::PenStyle getWhiskerLineStyle() const
    { return whiskerLineStyle; }

    /**
     * Test if the parameters specify the whisker color.
     * @return true if the parameters specify the whisker color
     */
    inline bool hasWhiskerColor() const
    { return components & Component_WhiskerColor; }

    /**
     * Get the whisker color.
     * @return the whisker color
     */
    inline QColor getWhiskerColor() const
    { return whiskerColor; }

    /**
     * Change the whisker color.  This does not set the "color defined" flag.
     * @param set the new color
     */
    inline void setWhiskerColor(const QColor &set)
    { whiskerColor = set; }

    /**
     * Test if the parameters specify the whisker cap width.
     * @return true if the parameters specify the whisker cap width
     */
    inline bool hasWhiskerCapWidth() const
    { return components & Component_WhiskerCapWidth; }

    /**
     * Get the whisker cap width.
     * @return the whisker cap width
     */
    inline qreal getWhiskerCapWidth() const
    { return whiskerCapWidth; }

    /**
     * Test if the parameters specify the middle enable state.
     * @return true if the parameters specify the middle enable state
     */
    inline bool hasMiddleEnable() const
    { return components & Component_MiddleEnable; }

    /**
     * Get the middle enable state.
     * @return true to draw the box middle
     */
    inline bool getMiddleEnable() const
    { return middleEnable; }

    /**
     * Test if the parameters specify the middle width.
     * @return true if the parameters specify the middle width
     */
    inline bool hasMiddleLineWidth() const
    { return components & Component_MiddleLineWidth; }

    /**
     * Get the middle width.
     * @return the middle width
     */
    inline qreal getMiddleLineWidth() const
    { return middleLineWidth; }

    /**
     * Test if the parameters specify the middle style.
     * @return true if the parameters specify the middle style
     */
    inline bool hasMiddleLineStyle() const
    { return components & Component_MiddleLineStyle; }

    /**
     * Get the middle style.
     * @return the middle style
     */
    inline Qt::PenStyle getMiddleLineStyle() const
    { return middleLineStyle; }

    /**
     * Test if the parameters specify the middle color.
     * @return true if the parameters specify the middle color
     */
    inline bool hasMiddleColor() const
    { return components & Component_MiddleColor; }

    /**
     * Get the middle color.
     * @return the middle color
     */
    inline QColor getMiddleColor() const
    { return middleColor; }

    /**
     * Change the middle color.  This does not set the "color defined" flag.
     * @param set the new color
     */
    inline void setMiddleColor(const QColor &set)
    { middleColor = set; }


    /**
     * Test if the parameters specify the extended middle line enable state.
     * @return true if the parameters specify the extended middle line enable state
     */
    inline bool hasMiddleExtendEnable() const
    { return components & Component_MiddleExtendEnable; }

    /**
     * Get the extended middle line enable state.
     * @return true to draw the box extended middle line
     */
    inline bool getMiddleExtendEnable() const
    { return middleExtendEnable; }

    /**
     * Test if the parameters specify the extended middle line width.
     * @return true if the parameters specify the extended middle line width
     */
    inline bool hasMiddleExtendLineWidth() const
    { return components & Component_MiddleExtendLineWidth; }

    /**
     * Get the extended middle line width.
     * @return the extended middle line width
     */
    inline qreal getMiddleExtendLineWidth() const
    { return middleExtendLineWidth; }

    /**
     * Test if the parameters specify the extended middle line style.
     * @return true if the parameters specify the extended middle line style
     */
    inline bool hasMiddleExtendLineStyle() const
    { return components & Component_MiddleExtendLineStyle; }

    /**
     * Get the extended middle line style.
     * @return the extended middle line style
     */
    inline Qt::PenStyle getMiddleExtendLineStyle() const
    { return middleExtendLineStyle; }

    /**
     * Test if the parameters specify the extended middle line color.
     * @return true if the parameters specify the middleExtend color
     */
    inline bool hasMiddleExtendColor() const
    { return components & Component_MiddleExtendColor; }

    /**
     * Get the extended middle line color.
     * @return the extended middle line color
     */
    inline QColor getMiddleExtendColor() const
    { return middleExtendColor; }

    /**
     * Change the extended middle line color.  This does not set the 
     * "color defined" flag.
     * @param set the new color
     */
    inline void setMiddleExtendColor(const QColor &set)
    { middleExtendColor = set; }

    /**
     * Test if the parameters specify the extended middle line priority.
     * @return true if the parameters specify the extended middle line priority
     */
    inline bool hasMiddleExtendDrawPriority() const
    { return components & Component_MiddleExtendDrawPriority; }

    /**
     * Get the extended middle line draw priority.
     * @return the extended middle line draw priority
     */
    inline int getMiddleExtendDrawPriority() const
    { return middleExtendDrawPriority; }

    /**
     * Test if the parameters specify the legend entry show status.
     * @return true if the parameters specify if the legend entry shows
     */
    inline bool hasLegendEntry() const
    { return components & Component_LegendEntry; }

    /**
     * Get the legend entry status.
     * @return the legend entry status
     */
    inline bool getLegendEntry() const
    { return legendEntry; }

    /**
     * Override the legend entry to the given status.  This sets the
     * defined flag.
     * @param text  the new legend text
     */
    inline void overrideLegendEntry(bool enable)
    {
        legendEntry = enable;
        components |= Component_LegendEntry;
    }

    /**
     * Test if the parameters specify the legend text.
     * @return true if the parameters specify the legend text
     */
    inline bool hasLegendText() const
    { return components & Component_LegendText; }

    /**
     * Get the legend text.
     * @return the legend text
     */
    inline QString getLegendText() const
    { return legendText; }

    /**
     * Override the legend text to the given value.  This sets the
     * defined flag.
     * @param text  the new legend text
     */
    inline void overrideLegendText(const QString &text)
    {
        legendText = text;
        components |= Component_LegendText;
    }

    /**
     * Test if the parameters specify a legend font.
     * @return true if the parameters specify a legend font
     */
    inline bool hasLegendFont() const
    { return components & Component_LegendFont; }

    /**
     * Get the legend font.
     * @return the legend font.
     */
    inline QFont getLegendFont() const
    { return legendFont; }

    /**
     * Change the legend font.  This sets the defined flag.
     * @param font the new font
     */
    inline void overrideLegendFont(const QFont &font)
    {
        legendFont = font;
        components |= Component_LegendFont;
    }

    /**
     * Test if the parameters specify a legend sort priority.
     * @return true if the parameters specify a legend sort priority
     */
    inline bool hasLegendSortPriority() const
    { return components & Component_LegendSortPriority; }

    /**
     * Get the legend sort priority.
     * @return the legend sort priority
     */
    inline int getLegendSortPriority() const
    { return legendSortPriority; }

    /**
     * Test if the parameters specify the independent axis binding.
     * @return true if the parameters specify the independent axis binding
     */
    inline bool hasIBinding() const
    { return components & Component_IBinding; }

    /**
     * Get the independent axis binding.
     * @return the independent axis binding
     */
    inline QString getIBinding() const
    { return iBinding; }

    /**
     * Test if the parameters specify the dependent axis binding.
     * @return true if the parameters specify the dependent axis binding
     */
    inline bool hasDBinding() const
    { return components & Component_DBinding; }

    /**
     * Get the dependent axis binding.
     * @return the dependent axis binding
     */
    inline QString getDBinding() const
    { return dBinding; }

    /**
     * Test if the parameters specify the drawing mode.
     * @return true if the parameters specify the drawing mode
     */
    inline bool hasDrawMode() const
    { return components & Component_DrawMode; }

    /**
     * Get the draw mode.
     * @return the draw mode
     */
    inline DrawMode getDrawMode() const
    { return drawMode; }

    /**
     * Get the draw constant.
     * @return the draw constant
     */
    inline double getDrawConstant() const
    { return drawConstant; }

    /**
     * Test if the parameters specify the draw boxes enable state.
     * @return true if the parameters specify the draw boxes enable state
     */
    inline bool hasDrawBoxes() const
    { return components & Component_DrawBoxes; }

    /**
     * Test if the boxes should be drawn.
     * @return true to draw the boxes
     */
    inline bool getDrawBoxes() const
    { return drawBoxes; }

    /**
     * Test if the parameters specify the relocation enable state.
     * @return true if the parameters specify the relocation enable state
     */
    inline bool hasRelocationEnable() const
    { return components & Component_RelocationEnable; }

    /**
     * Get the relocation enable state.
     * @return true if boxes can be relocated
     */
    inline bool getRelocationEnable() const
    { return relocationEnable; }


    /**
     * Test if the parameters specify a fit model.
     * @param origin    the origin of the fit
     * @return true if the parameters specify a fit model
     */
    inline bool hasFitModel(FitOrigin origin) const
    { return fits.at(origin).components & Fit::Component_Model; }

    /**
     * Get the fit model parameters.
     * @param origin    the origin of the fit
     * @return the fit model parameters
     */
    inline Data::Variant::Read getFitModel(FitOrigin origin) const
    { return fits.at(origin).model; }

    /**
     * Test if the parameters specify a fit I log base.
     * @param origin    the origin of the fit
     * @return true if the parameters specify a fit I log base
     */
    inline bool hasFitLogIBase(FitOrigin origin) const
    { return fits.at(origin).components & Fit::Component_LogIBase; }

    /**
     * Get the fit I log base.
     * @param origin    the origin of the fit
     * @return the log base for the I dimension
     */
    inline double getFitLogIBase(FitOrigin origin) const
    { return fits.at(origin).logIBase; }

    /**
     * Set the fit I log base.  This does not set the defined flag.
     * @param origin    the origin of the fit
     * @param b         the new base
     */
    inline void setFitLogIBase(FitOrigin origin, double b)
    { fits[origin].logIBase = b; }

    /**
     * Test if the parameters specify a fit I log base.
     * @param origin    the origin of the fit
     * @return true if the parameters specify a fit I log base
     */
    inline bool hasFitLogDBase(FitOrigin origin) const
    { return fits.at(origin).components & Fit::Component_LogDBase; }

    /**
     * Get the fit D log base.
     * @param origin    the origin of the fit
     * @return the log base for the D dimension
     */
    inline double getFitLogDBase(FitOrigin origin) const
    { return fits.at(origin).logDBase; }

    /**
     * Set the fit D log base.  This does not set the defined flag.
     * @param origin    the origin of the fit
     * @param b         the new base
     */
    inline void setFitLogDBase(FitOrigin origin, double b)
    { fits[origin].logDBase = b; }

    /**
     * Test if the parameters specify a fit line width.
     * @param origin    the origin of the fit
     * @return true if the parameters specify a fit line width
     */
    inline bool hasFitLineWidth(FitOrigin origin) const
    { return fits.at(origin).components & Fit::Component_LineWidth; }

    /**
     * Get the fit line width.
     * @param origin    the origin of the fit
     * @return the fit line width
     */
    inline qreal getFitLineWidth(FitOrigin origin) const
    { return fits.at(origin).lineWidth; }

    /**
     * Override the line width of a fit to the given value.  This sets the
     * defined flag.
     * @param origin    the fit origin
     * @param width     the new width
     */
    inline void overrideFitLineWidth(FitOrigin origin, qreal width)
    {
        fits[origin].lineWidth = width;
        fits[origin].components |= Fit::Component_LineWidth;
    }

    /**
     * Test if the parameters specify a fit line style.
     * @param origin    the origin of the fit
     * @return true if the parameters specify a fit line style
     */
    inline bool hasFitLineStyle(FitOrigin origin) const
    { return fits.at(origin).components & Fit::Component_LineStyle; }

    /**
     * Get the fit line style.
     * @param origin    the origin of the fit
     * @return the fit line style
     */
    inline Qt::PenStyle getFitLineStyle(FitOrigin origin) const
    { return fits.at(origin).lineStyle; }

    /**
     * Override the line style of a fit to the given value.  This sets the
     * defined flag.
     * @param origin    the fit origin
     * @param style     the new style
     */
    inline void overrideFitLineStyle(FitOrigin origin, Qt::PenStyle style)
    {
        fits[origin].lineStyle = style;
        fits[origin].components |= Fit::Component_LineStyle;
    }

    /**
     * Test if the parameters specify a fit draw priority.
     * @param origin    the origin of the fit
     * @return true if the parameters specify a fit draw priority.
     */
    inline bool hasFitDrawPriority(FitOrigin origin) const
    { return fits.at(origin).components & Fit::Component_DrawPriority; }

    /**
     * Get the fit draw priority.
     * @param origin    the origin of the fit
     * @return the fit draw priority.
     */
    inline int getFitDrawPriority(FitOrigin origin) const
    { return fits.at(origin).drawPriority; }

    /**
     * Test if the parameters specify a fit color.
     * @param origin    the origin of the fit
     * @return true if the parameters specify a fit color
     */
    inline bool hasFitColor(FitOrigin origin) const
    { return fits.at(origin).components & Fit::Component_Color; }

    /**
     * Get the fit color.
     * @param origin    the origin of the fit
     * @return the fit color
     */
    inline QColor getFitColor(FitOrigin origin) const
    { return fits.at(origin).color; }

    /**
     * Change the fit color.  This does not set the "color defined" flag.
     * @param origin    the origin of the fit
     * @param set   the new color
     */
    inline void setFitColor(FitOrigin origin, const QColor &set)
    { fits[origin].color = set; }

    /**
     * Override the color of a fit to the given value.  This sets the
     * defined flag.
     * @param origin    the fit origin
     * @param color     the new color
     */
    inline void overrideFitColor(FitOrigin origin, const QColor &color)
    {
        fits[origin].color = color;
        fits[origin].components |= Fit::Component_Color;
    }

    /**
     * Test if the parameters specify the fit legend text.
     * @param origin    the origin of the fit
     * @return true if the parameters specify the legend text
     */
    inline bool hasFitLegendText(FitOrigin origin) const
    { return fits.at(origin).components & Fit::Component_LegendText; }

    /**
     * Get the fit legend text.
     * @param origin    the origin of the fit
     * @return the legend text
     */
    inline QString getFitLegendText(FitOrigin origin) const
    { return fits.at(origin).legendText; }

    /**
     * Override the legend text of a fit to the given value.  This sets the
     * defined flag.
     * @param origin    the fit origin
     * @param text      the new legend text
     */
    inline void overrideFitLegendText(FitOrigin origin, const QString &text)
    {
        fits[origin].legendText = text;
        fits[origin].components |= Fit::Component_LegendText;
    }

    /**
     * Test if the parameters specify a fit legend font.
     * @param origin    the origin of the fit
     * @return true if the parameters specify a legend font
     */
    inline bool hasFitLegendFont(FitOrigin origin) const
    { return fits.at(origin).components & Fit::Component_LegendFont; }

    /**
     * Override the legend font of a fit to the given value.  This sets the
     * defined flag.
     * @param origin    the fit origin
     * @param font      the new legend font
     */
    inline void overrideFitLegendFont(FitOrigin origin, const QFont &font)
    {
        fits[origin].legendFont = font;
        fits[origin].components |= Fit::Component_LegendFont;
    }

    /**
     * Get the fit legend font.
     * @param origin    the origin of the fit
     * @return the legend font.
     */
    inline QFont getFitLegendFont(FitOrigin origin) const
    { return fits.at(origin).legendFont; }
};

class BinHandler2D;

class BinSet2D;

namespace Internal {
class Graph2DLegendModificationBins;

class Graph2DLegendModificationBinsFit;
}

/**
 * A holder that accumulates data for final combination into drawers.  This
 * allows the system to correctly handle overlapping boxes.
 */
class CPD3GRAPHING_EXPORT BinDrawPrecursor {
    std::vector<Bin2DPaintPoint> points;
    AxisTransformer tX;
    AxisTransformer tY;
    BinParameters2D *parameters;
    quintptr tracePtr;

    friend class BinHandler2D;

    friend class BinSet2D;

    BinDrawPrecursor(std::vector<Bin2DPaintPoint> setPoints,
                     const AxisTransformer &setX,
                     const AxisTransformer &setY,
                     BinParameters2D *params,
                     quintptr setTracePtr);

public:
    BinDrawPrecursor();

    BinDrawPrecursor(const BinDrawPrecursor &other);

    BinDrawPrecursor &operator=(const BinDrawPrecursor &other);

    BinDrawPrecursor(BinDrawPrecursor &&other);

    BinDrawPrecursor &operator=(BinDrawPrecursor &&other);
};

/**
 * The base class for two dimensional bin traces.
 */
class CPD3GRAPHING_EXPORT BinHandler2D : public BinValueHandler<1, 1> {
    friend class BinOutput2D;

    friend class Internal::Graph2DLegendModificationBins;

    friend class Internal::Graph2DLegendModificationBinsFit;

    BinParameters2D *parameters;
    bool anyValidPoints;

    bool alwaysHideLegend;

    struct Fit {
        Algorithms::DeferredModelWrapper *running;
        std::shared_ptr<Algorithms::Model> fit;
        bool needRerun;

        Fit();

        ~Fit();
    };

    std::unordered_map<BinParameters2D::FitOrigin, std::unique_ptr<Fit>,
                       BinParameters2D::FitOriginHash> fits;

    Algorithms::Model *wrapFit(BinParameters2D::FitOrigin origin, Algorithms::Model *input);

    bool startFit(BinParameters2D::FitOrigin origin, Fit *fit, bool deferrable);

    std::vector<Bin2DPaintPoint> getSizedBins(AxisTransformer &x,
                                              AxisTransformer &y,
                                              bool vertical) const;

    QString getLegendText(const std::vector<Data::SequenceName::Set> &inputUnits,
                          bool showStation,
                          bool showArchive,
                          const DisplayDynamicContext &context,
                          bool *changed) const;

public:
    BinHandler2D(std::unique_ptr<BinParameters2D> &&params);

    virtual ~BinHandler2D();

    using Cloned = typename BinValueHandler<1, 1>::Cloned;

    Cloned clone() const override;

    bool process(const std::vector<TraceDispatch::OutputValue> &incoming,
                 bool deferrable = false) override;

    void trimToLast() override;

    void trimData(double start, double end) override;

    void setVisibleTimeRange(double start, double end) override;

    bool axesBound(AxisDimension *x,
                   AxisDimension *y,
                   bool vertical = true,
                   bool deferrable = false);

    QString getDisplayTitle(const std::vector<Data::SequenceName::Set> &inputUnits = {},
                            bool showStation = false,
                            bool showArchive = false,
                            const DisplayDynamicContext &context = {}) const;

    std::vector<std::shared_ptr<LegendItem> > getLegend(const std::vector<
            Data::SequenceName::Set> &inputUnits = {},
                                                        bool showStation = false,
                                                        bool showArchive = false,
                                                        const DisplayDynamicContext &context = {},
                                                        bool *changed = nullptr);

    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> createDraw(AxisTransformer &x,
                                                                  AxisTransformer &y,
                                                                  std::vector<
                                                                          BinDrawPrecursor> &precursors,
                                                                  bool vertical = true,
                                                                  double xBaseline = FP::undefined(),
                                                                  double yBaseline = FP::undefined()) const;

    void getColors(const std::vector<Data::SequenceName::Set> &inputUnits,
                   std::vector<TraceUtilities::ColorRequest> &requestColors,
                   std::deque<QColor> &claimedColors) const;

    void claimColors(std::deque<QColor> &colors) const;

    std::vector<std::shared_ptr<DisplayOutput> > getOutputs(const std::vector<
            Data::SequenceName::Set> &inputUnits = {},
                                                            bool showStation = false,
                                                            bool showArchive = false,
                                                            const DisplayDynamicContext &context = {});

    /**
     * Test if the handler specifies an independent axis binding.
     * @return true if the handler specifies an independent axis binding
     */
    inline bool hasIBinding() const
    { return parameters->hasIBinding(); }

    /**
     * Get the independent axis binding.
     * @return the independent axis binding
     */
    inline QString getIBinding() const
    { return parameters->getIBinding(); }

    /**
     * Test if the handler specifies an dependent axis binding.
     * @return true if the handler specifies an dependent axis binding
     */
    inline bool hasDBinding() const
    { return parameters->hasDBinding(); }

    /**
     * Get the dependent axis binding.
     * @return the dependent axis binding
     */
    inline QString getDBinding() const
    { return parameters->getDBinding(); }

    QColor getColor() const;

    /**
     * Get the fit color.
     * @param origin    the origin of the fit
     * @return the fit color
     */
    inline QColor getFitColor(BinParameters2D::FitOrigin origin) const
    { return parameters->getFitColor(origin); }

    /**
     * Set the override status for the legend hide.  If enabled then the
     * handler will not produce legend entries.
     * 
     * @param en        the enable state
     * @return          true if the state changed
     */
    inline bool setLegendAlwaysHide(bool en)
    {
        if (en == alwaysHideLegend)
            return false;
        alwaysHideLegend = en;
        return true;
    }

protected:
    BinHandler2D(const BinHandler2D &other, std::unique_ptr<TraceParameters> &&params);

    void convertValue(const TraceDispatch::OutputValue &value,
                      std::vector<TracePoint<2>> &points) override;

    virtual std::vector<BinPoint<1, 1>> getMiddleExtendPoints(const std::vector<
            BinPoint<1, 1> > &input) const;
};


/**
 * The base class for two dimensional box whisker plot sets.
 */
class CPD3GRAPHING_EXPORT BinSet2D : public BinDispatchSet<1, 1> {
    int countOverlapping(std::vector<BinDrawPrecursor> &precursors,
                         bool vertical,
                         const std::vector<std::vector<Bin2DPaintPoint>> &points) const;

    void layoutPrecursors(std::vector<BinDrawPrecursor> &precursors,
                          bool vertical,
                          std::vector<std::vector<Bin2DPaintPoint>> &points) const;

public:
    BinSet2D();

    virtual ~BinSet2D();

    using Cloned = typename BinDispatchSet<1, 1>::Cloned;

    Cloned clone() const override;

    std::vector<BinHandler2D *> getSortedHandlers() const;

    bool addLegend(Legend &legend,
                   const DisplayDynamicContext &context = DisplayDynamicContext()) const;

    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> combinePrecursors(std::vector<
            BinDrawPrecursor> &precursors, bool vertical = true) const;

    void getColors(std::vector<TraceUtilities::ColorRequest> &requestColors,
                   std::deque<QColor> &claimedColors) const;

    void claimColors(std::deque<QColor> &colors) const;

    QString handlerTitle(BinHandler2D *bins, const DisplayDynamicContext &context) const;

    QString handlerOrigin(BinHandler2D *bins, const DisplayDynamicContext &context) const;

    std::vector<std::shared_ptr<
            DisplayOutput> > getOutputs(const DisplayDynamicContext &context = DisplayDynamicContext());

    using CreatedHandler = typename BinDispatchSet<1, 1>::CreatedHandler;

protected:
    BinSet2D(const BinSet2D &other);

    std::unique_ptr<
            TraceParameters> parseParameters(const Data::Variant::Read &value) const override;

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override;
};

/**
 * The output type for 2-D bin traces.
 */
class CPD3GRAPHING_EXPORT BinOutput2D : public DisplayOutput {
Q_OBJECT
    QPointer<BinHandler2D> handler;
    QString title;
public:
    BinOutput2D(BinHandler2D *handler, const QString &title);

    virtual ~BinOutput2D();

    QString getTitle() const override;

    QList<QAction *> getMenuActions(QWidget *parent = nullptr) override;

public slots:

    /**
     * Set if the trace should hide legend entries.
     * 
     * @param enable    true to disable the legend entry
     */
    void setLegendAlwaysHide(bool enable);
};

namespace Internal {


/**
 * The legend modification item for a 2-D bins display.
 */
class Graph2DLegendModificationBins : public Graph2DLegendModification {
Q_OBJECT

    QPointer<BinHandler2D> handler;
    QString legendText;
public:
    Graph2DLegendModificationBins(Graph2D *graph,
                                  const QPointer<BinHandler2D> &setHandler,
                                  const QString &setLegendText);

    virtual ~Graph2DLegendModificationBins();

    QList<QAction *> getMenuActions(QWidget *parent = nullptr) override;
};

/**
 * The legend modification item for a 2-D bins fit.
 */
class Graph2DLegendModificationBinsFit : public Graph2DLegendModification {
Q_OBJECT

    QPointer<BinHandler2D> handler;
    BinParameters2D::FitOrigin origin;
    QString legendText;
public:
    Graph2DLegendModificationBinsFit(Graph2D *graph,
                                     const QPointer<BinHandler2D> &setHandler,
                                     BinParameters2D::FitOrigin setOrigin,
                                     const QString &setLegendText);

    virtual ~Graph2DLegendModificationBinsFit();

    QList<QAction *> getMenuActions(QWidget *parent = nullptr) override;
};

}

}
}

#endif
