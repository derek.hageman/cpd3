/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGDISPLAYENABLE_H
#define CPD3GRAPHINGDISPLAYENABLE_H

#include "core/first.hxx"

#include <memory>
#include <unordered_map>
#include <QtGlobal>
#include <QObject>

#include "graphing/graphing.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * A class of that handles display enable evaluation conditions.
 */
class CPD3GRAPHING_EXPORT DisplayEnable : public QObject {
Q_OBJECT

    Data::Archive::Access *reader;
    std::unique_ptr<Data::Archive::Access> localArchive;
    std::unique_ptr<Data::Archive::Access::ReadLock> localLock;

    double start;
    double end;
    Data::SequenceName::Component station;
    Data::SequenceName::Component archive;
    Data::SequenceName::Component variable;

    Data::SequenceName::Set knownToExist;

    class EnableResult;

    friend class EnableResult;

    void integrateSelection(const Data::SequenceMatch::Composite &selection);

    Data::SequenceMatch::Composite toSelection(const Data::Variant::Read &configuration) const;

    std::unique_ptr<EnableResult> constructEnable(const Data::Variant::Read &configuration);

    enum class EnableType {
        Constant = 0,

        AtLeast, AtMost, AtLeastInstrument, AtMostInstrument,

        And, Or, Not,
    };

    class EnableResult {
        DisplayEnable &enable;
    public:
        EnableResult(DisplayEnable &enable);

        virtual ~EnableResult();

        inline const Data::SequenceName::Set &existingNames() const
        { return enable.knownToExist; }

        inline void integrateSelection(const Data::SequenceMatch::Composite &selection)
        { return enable.integrateSelection(selection); }

        inline Data::SequenceMatch::Composite toSelection(const Data::Variant::Read &configuration) const
        { return enable.toSelection(configuration); }

        bool evaluate(const std::unique_ptr<EnableResult> &other);

        virtual bool evaluate() = 0;

        virtual std::size_t hash() const = 0;

        virtual bool equalTo(const EnableResult &other) = 0;

        virtual EnableType type() const = 0;

        virtual std::unique_ptr<EnableResult> clone() const = 0;

    protected:
        EnableResult(const EnableResult &);
    };

    class Enable_Constant;

    class Enable_AtLeast;

    class Enable_AtMost;

    class Enable_AtLeastInstrument;

    class Enable_AtMostInstrument;

    class Enable_And;

    class Enable_Or;

    class Enable_Not;

    struct EnablePointerHash {
        inline std::size_t operator()(const std::unique_ptr<EnableResult> &result) const
        { return result->hash(); }
    };

    struct EnablePointerEqual {
        inline bool operator()(const std::unique_ptr<EnableResult> &a,
                               const std::unique_ptr<EnableResult> &b) const
        { return a->equalTo(*b); }
    };

    std::unordered_map<std::unique_ptr<EnableResult>, bool, EnablePointerHash, EnablePointerEqual>
            resultCache;

public:
    /**
     * Create the enable handler.
     * 
     * @param start     the start time of evaluation
     * @param end       the end time of evaluation
     * @param station   the default station
     * @param archive   the default archive
     * @param variable  the default variable
     * @param access    the archive reader to query with
     */
    DisplayEnable(double start = FP::undefined(),
                  double end = FP::undefined(),
                  const Data::SequenceName::Component &station = {},
                  const Data::SequenceName::Component &archive = {},
                  const Data::SequenceName::Component &variable = {},
                  Data::Archive::Access *access = nullptr);

    virtual ~DisplayEnable();

    /**
     * Check if the given configuration is enabled.
     *
     * @param value     the configuration to check
     * @return true     if the configuration is enabled
     */
    bool enabled(const Data::Variant::Read &value);

protected:

    /**
     * Evaluate a selection for comparison against the backing data.
     *
     * @param selection the selection to load
     * @return          the matched sequence names
     */
    virtual Data::SequenceName::Set evaluateSelection(const Data::SequenceMatch::Composite &selection);
};

}
}

#endif
