/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>
#include <QSettings>
#include <QLoggingCategory>

#include "datacore/variant/root.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/archive/access.hxx"
#include "graphing/displaycomponent.hxx"
#include "graphing/graph2d.hxx"
#include "graphing/timeseries2d.hxx"
#include "graphing/cycleplot2d.hxx"
#include "graphing/displaylayout.hxx"
#include "io/drivers/file.hxx"


Q_LOGGING_CATEGORY(log_graphing_displaycomponent, "cpd3.graphing.displaycomponent", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace Graphing {

/** @file graphing/displaycomponent.hxx
 * Various utilities to deal with display as components of the system.
 */

enum {
    Grid_Disabled, Grid_Both, Grid_X, Grid_Y
};

enum {
    Legend_Disabled,
    Legend_Inside_TopLeft,
    Legend_Inside_TopRight,
    Legend_Inside_BottomLeft,
    Legend_Inside_BottomRight,
    Legend_Outside_Left,
    Legend_Outside_Right,
};

enum {
    Fit_Disabled,
    Fit_SimpleLeastSquares,
    Fit_ZeroLeastSquares,
    Fit_SecondOrderPolynomial,
    Fit_ZeroSecondOrderPolynomial,
    Fit_ThirdOrderPolynomial,
    Fit_ZeroThirdOrderPolynomial,
};

enum {
    PDFFit_Disabled, PDFFit_CSpline,
};

static void insertFitCommon(ComponentOptions &options)
{
    ComponentOptionEnum *fit = new ComponentOptionEnum(DisplayComponent::tr("fit", "name"),
                                                       DisplayComponent::tr("Fit type"),
                                                       DisplayComponent::tr(
                                                               "This is the type of fit displayed on the graph."),
                                                       DisplayComponent::tr("Disabled",
                                                                            "grid default"));

    fit->add(Fit_Disabled, "disabled", DisplayComponent::tr("disabled", "mode name"),
             DisplayComponent::tr("No fit is displayed."));
    fit->alias(Fit_Disabled, DisplayComponent::tr("none", "mode name"));
    fit->alias(Fit_Disabled, DisplayComponent::tr("off", "mode name"));

    fit->add(Fit_SimpleLeastSquares, "linear", DisplayComponent::tr("linear", "mode name"),
             DisplayComponent::tr("Linear least squares."));
    fit->alias(Fit_SimpleLeastSquares, DisplayComponent::tr("lsq", "mode name"));
    fit->alias(Fit_SimpleLeastSquares, DisplayComponent::tr("leastsquares", "mode name"));

    fit->add(Fit_ZeroLeastSquares, "linearzero", DisplayComponent::tr("linearzero", "mode name"),
             DisplayComponent::tr("Linear least squares forced through zero."));
    fit->alias(Fit_ZeroLeastSquares, DisplayComponent::tr("zerolinear", "mode name"));

    fit->add(Fit_SecondOrderPolynomial, "secondorder",
             DisplayComponent::tr("secondorder", "mode name"),
             DisplayComponent::tr("Second order polynomial."));
    fit->alias(Fit_SecondOrderPolynomial, DisplayComponent::tr("2poly", "mode name"));

    fit->add(Fit_ZeroSecondOrderPolynomial, "secondorderzero",
             DisplayComponent::tr("secondorderzero", "mode name"),
             DisplayComponent::tr("Second order polynomial forced through zero."));
    fit->alias(Fit_ZeroSecondOrderPolynomial, DisplayComponent::tr("zerosecondorder", "mode name"));

    fit->add(Fit_ThirdOrderPolynomial, "thirdorder",
             DisplayComponent::tr("thirdorder", "mode name"),
             DisplayComponent::tr("Third order polynomial."));
    fit->alias(Fit_ThirdOrderPolynomial, DisplayComponent::tr("3poly", "mode name"));

    fit->add(Fit_ZeroThirdOrderPolynomial, "thirdorderzero",
             DisplayComponent::tr("thirdorderzero", "mode name"),
             DisplayComponent::tr("Third order polynomial forced through zero."));
    fit->alias(Fit_ZeroThirdOrderPolynomial, DisplayComponent::tr("zerothirdorder", "mode name"));

    options.add("fit", fit);
}

static void setFitCommon(const ComponentOptions &options, Variant::Root &config)
{
    if (options.isSet("fit")) {
        switch (qobject_cast<ComponentOptionEnum *>(options.get("fit"))->get().getID()) {
        case Fit_Disabled:
            break;
        case Fit_SimpleLeastSquares:
            config["Traces/All/Settings/Fit/Model/Type"].setString("leastsquares");
            break;
        case Fit_ZeroLeastSquares:
            config["Traces/All/Settings/Fit/Model/Type"].setString("zeroleastsquares");
            break;
        case Fit_SecondOrderPolynomial:
            config["Traces/All/Settings/Fit/Model/Type"].setString("npolynomial");
            config["Traces/All/Settings/Fit/Model/Order"].setInt64(2);
            break;
        case Fit_ZeroSecondOrderPolynomial:
            config["Traces/All/Settings/Fit/Model/Type"].setString("zeronpolynomial");
            config["Traces/All/Settings/Fit/Model/Order"].setInt64(2);
            break;
        case Fit_ThirdOrderPolynomial:
            config["Traces/All/Settings/Fit/Model/Type"].setString("npolynomial");
            config["Traces/All/Settings/Fit/Model/Order"].setInt64(3);
            break;
        case Fit_ZeroThirdOrderPolynomial:
            config["Traces/All/Settings/Fit/Model/Type"].setString("zeronpolynomial");
            config["Traces/All/Settings/Fit/Model/Order"].setInt64(3);
            break;
        }
    }
}

static void insertLogarithmicOptions(ComponentOptions &options)
{
    options.add("log", new ComponentOptionBoolean(DisplayComponent::tr("log", "name"),
                                                  DisplayComponent::tr("Use a logarithmic axes"),
                                                  DisplayComponent::tr(
                                                          "This changes the variable axes to be logarithmic."),
                                                  QString()));
}

static void setLogarithmicFilter(const ComponentOptions &options,
                                 Variant::Root &config,
                                 int dimension,
                                 const QString &path = "Traces/All",
                                 bool remove = true)
{
    if (options.isSet("log") && qobject_cast<ComponentOptionBoolean *>(options.get("log"))->get()) {
        auto filter = config[path]["Settings/Transformer"].toArray().after_back();
        filter["Type"].setString("RejectRange");
        filter["Remove"].setBool(remove);
        filter["Maximum"].setDouble(0.0);
        filter["Dimension"].setInt64(dimension);
    }
}

static void insertQuantileOptions(ComponentOptions &options)
{
    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(DisplayComponent::tr("quantile", "name"),
                                            DisplayComponent::tr("Filter to the Nth quantile"),
                                            DisplayComponent::tr(
                                                    "This causes the plot to only include the inner "
                                                    "percentage of data set.  For example, setting this to 99, causes "
                                                    "the graph to exclude the top and bottom 0.5% of data."),
                                            QString());
    d->setMaximum(100.0, true);
    d->setMinimum(0.0, false);
    d->setAllowUndefined(true);
    options.add("quantile", d);
}

static void setQuantileFilter(const ComponentOptions &options,
                              Variant::Root &config,
                              int dimension,
                              const QString &path = "Traces/All",
                              bool remove = true)
{
    if (options.isSet("quantile")) {
        double q = qobject_cast<ComponentOptionSingleDouble *>(options.get("quantile"))->get();
        if (FP::defined(q) && q > 0.0 && q < 100.0) {
            q = ((100.0 - q) * 0.5) / 100.0;
            auto filter = config[path]["Settings/Transformer"].toArray().after_back();
            filter["Type"].setString("AcceptQuantile");
            filter["Remove"].setBool(remove);
            filter["Minimum"].setDouble(q);
            filter["Maximum"].setDouble(1.0 - q);
            filter["Dimension"].setInt64(dimension);
        }
    }
}

static void insertGraph2DCommon(ComponentOptions &options, bool allanLegend = false)
{
    options.add("title", new ComponentOptionSingleString(DisplayComponent::tr("title", "name"),
                                                         DisplayComponent::tr("Title"),
                                                         DisplayComponent::tr(
                                                                 "This is the main graph title."),
                                                         QString()));

    options.add("x-title", new ComponentOptionSingleString(DisplayComponent::tr("x-title", "name"),
                                                           DisplayComponent::tr("X axis title"),
                                                           DisplayComponent::tr(
                                                                   "This is the title output along the X axis."),
                                                           DisplayComponent::tr(
                                                                   "Generated from the axis units.")));

    options.add("y-title", new ComponentOptionSingleString(DisplayComponent::tr("y-title", "name"),
                                                           DisplayComponent::tr("Y axis title"),
                                                           DisplayComponent::tr(
                                                                   "This is the title output along the Y axis."),
                                                           DisplayComponent::tr(
                                                                   "Generated from the axis units.")));

    ComponentOptionEnum *grid = new ComponentOptionEnum(DisplayComponent::tr("grid", "name"),
                                                        DisplayComponent::tr("Grid display"),
                                                        DisplayComponent::tr(
                                                                "This is the grid mode to display.  This enables "
                                                                "or disables the possible grids that are shown."),
                                                        DisplayComponent::tr("Disabled",
                                                                             "grid default"));
    grid->add(Grid_Disabled, "disabled", DisplayComponent::tr("disabled", "mode name"),
              DisplayComponent::tr("No grid is displayed."));
    grid->alias(Grid_Disabled, DisplayComponent::tr("none", "mode name"));
    grid->alias(Grid_Disabled, DisplayComponent::tr("off", "mode name"));
    grid->add(Grid_Both, "both", DisplayComponent::tr("both", "mode name"),
              DisplayComponent::tr("Both X and Y grid lines are displayed."));
    grid->alias(Grid_Both, DisplayComponent::tr("xy", "mode name"));
    grid->alias(Grid_Both, DisplayComponent::tr("x-y", "mode name"));
    grid->alias(Grid_Both, DisplayComponent::tr("enabled", "mode name"));
    grid->alias(Grid_Both, DisplayComponent::tr("enable", "mode name"));
    grid->add(Grid_X, "x", DisplayComponent::tr("x", "mode name"),
              DisplayComponent::tr("Only grid lines extending from the X axis are "
                                   "displayed."));
    grid->add(Grid_Y, "y", DisplayComponent::tr("y", "mode name"),
              DisplayComponent::tr("Only grid lines extending from the Y axis are "
                                   "displayed."));
    options.add("grid", grid);

    ComponentOptionEnum *legendPosition =
            new ComponentOptionEnum(DisplayComponent::tr("legend", "name"),
                                    DisplayComponent::tr("Legend display"), DisplayComponent::tr(
                            "This is the legend mode to use.  This sets the "
                            "position of the legend for the graph."),
                                    allanLegend ? DisplayComponent::tr("Inside top right corner",
                                                                       "legend default")
                                                : DisplayComponent::tr("Inside top left corner",
                                                                       "legend default"));
    legendPosition->add(Legend_Disabled, "disabled", DisplayComponent::tr("disabled", "mode name"),
                        DisplayComponent::tr("No legend is displayed."));
    legendPosition->alias(Legend_Disabled, DisplayComponent::tr("none", "mode name"));
    legendPosition->alias(Legend_Disabled, DisplayComponent::tr("off", "mode name"));
    legendPosition->add(Legend_Inside_TopLeft, "topleft",
                        DisplayComponent::tr("topleft", "mode name"),
                        DisplayComponent::tr("The legend is located inside the trace area at "
                                             "the top left corner."));
    legendPosition->alias(Legend_Inside_TopLeft, DisplayComponent::tr("inside", "mode name"));
    legendPosition->add(Legend_Inside_TopRight, "topright",
                        DisplayComponent::tr("topright", "mode name"),
                        DisplayComponent::tr("The legend is located inside the trace area at "
                                             "the top right corner."));
    legendPosition->add(Legend_Inside_BottomLeft, "bottomleft",
                        DisplayComponent::tr("bottomleft", "mode name"),
                        DisplayComponent::tr("The legend is located inside the trace area at "
                                             "the bottom left corner."));
    legendPosition->add(Legend_Inside_BottomRight, "bottomright",
                        DisplayComponent::tr("bottomright", "mode name"),
                        DisplayComponent::tr("The legend is located inside the trace area at "
                                             "the bottom right corner."));
    legendPosition->add(Legend_Outside_Left, "left", DisplayComponent::tr("left", "mode name"),
                        DisplayComponent::tr("The legend is located outside and to the left "
                                             "of the graph."));
    legendPosition->add(Legend_Outside_Right, "right", DisplayComponent::tr("right", "mode name"),
                        DisplayComponent::tr("The legend is located outside and to the right "
                                             "of the graph."));
    legendPosition->alias(Legend_Outside_Right, DisplayComponent::tr("outside", "mode name"));
    options.add("legend", legendPosition);
}

static void setGraph2DCommon(const ComponentOptions &options, Variant::Root &config)
{
    if (options.isSet("title")) {
        config["Title/Text"].setString(
                qobject_cast<ComponentOptionSingleString *>(options.get("title"))->get());
    }

    if (options.isSet("x-title")) {
        config["XAxes/Defaults/Title/Text"].setString(
                qobject_cast<ComponentOptionSingleString *>(options.get("x-title"))->get());
    }

    if (options.isSet("y-title")) {
        config["YAxes/Defaults/Title/Text"].setString(
                qobject_cast<ComponentOptionSingleString *>(options.get("y-title"))->get());
    }

    if (options.isSet("grid")) {
        switch (qobject_cast<ComponentOptionEnum *>(options.get("grid"))->get().getID()) {
        case Grid_Disabled:
            break;
        case Grid_Both:
            config["XAxes/Defaults/GridEnable"].setBool(true);
            config["YAxes/Defaults/GridEnable"].setBool(true);
            break;
        case Grid_X:
            config["XAxes/Defaults/GridEnable"].setBool(true);
            break;
        case Grid_Y:
            config["YAxes/Defaults/GridEnable"].setBool(true);
            break;
        }
    }

    if (options.isSet("legend")) {
        switch (qobject_cast<ComponentOptionEnum *>(options.get("legend"))->get().getID()) {
        case Legend_Disabled:
            config["Legend/Enable"].setBool(false);
            break;
        case Legend_Inside_TopLeft:
            config["Legend/Position"].setString("topleft");
            break;
        case Legend_Inside_TopRight:
            config["Legend/Position"].setString("topright");
            break;
        case Legend_Inside_BottomLeft:
            config["Legend/Position"].setString("bottomleft");
            break;
        case Legend_Inside_BottomRight:
            config["Legend/Position"].setString("bottomright");
            break;
        case Legend_Outside_Left:
            config["Legend/Position"].setString("left");
            break;
        case Legend_Outside_Right:
            config["Legend/Position"].setString("right");
            break;
        }
    }
}

static void insertGraph2DVarOptions(ComponentOptions &options)
{
    options.add("x", new ComponentOptionSequenceMatch(DisplayComponent::tr("x", "name"),
                                                      DisplayComponent::tr("X Variable"),
                                                      DisplayComponent::tr(
                                                              "This is the variable or variables to plot on "
                                                              "the X axis."), QString()));

    options.add("y", new ComponentOptionSequenceMatch(DisplayComponent::tr("y", "name"),
                                                      DisplayComponent::tr("Y Variable"),
                                                      DisplayComponent::tr(
                                                              "This is the variable or variables to plot on "
                                                              "the Y axis."), QString()));

    options.add("z", new ComponentOptionSequenceMatch(DisplayComponent::tr("z", "name"),
                                                      DisplayComponent::tr("Z  Variable"),
                                                      DisplayComponent::tr(
                                                              "This is the variable or variables that determine "
                                                              "the trace color.  This generates a continuum of colors between "
                                                              "minimum and maximum values for the variable.  For example, "
                                                              "it could be used to display the humidity for points comparing "
                                                              "the scattering between two nephelometers."),
                                                      QString()));
}

static void setGraph2DVarOptions(const ComponentOptions &options, Variant::Root &config)
{
    if (options.isSet("x")) {
        config["Traces/All/Data/Dimensions/#0/Input"].setString(
                qobject_cast<ComponentOptionSequenceMatch *>(options.get("x"))->get());

        setLogarithmicFilter(options, config, 0);
        setQuantileFilter(options, config, 0);
    }

    if (options.isSet("y")) {
        config["Traces/All/Data/Dimensions/#1/Input"].setString(
                qobject_cast<ComponentOptionSequenceMatch *>(options.get("y"))->get());

        setLogarithmicFilter(options, config, 1);
        setQuantileFilter(options, config, 1);
    }

    if (options.isSet("z")) {
        config["Traces/All/Data/Dimensions/#2/Input"].setString(
                qobject_cast<ComponentOptionSequenceMatch *>(options.get("z"))->get());
        setQuantileFilter(options, config, 2);
    }
}

static void insertTimeSeries2DVarOptions(ComponentOptions &options)
{
    options.add("y", new ComponentOptionSequenceMatch(DisplayComponent::tr("y", "name"),
                                                      DisplayComponent::tr("Y Variable"),
                                                      DisplayComponent::tr(
                                                              "This is the variable or variables to plot on the Y axis."),
                                                      QString()));

    options.add("z", new ComponentOptionSequenceMatch(DisplayComponent::tr("z", "name"),
                                                      DisplayComponent::tr("Z  Variable"),
                                                      DisplayComponent::tr(
                                                              "This is the variable or variables that determine "
                                                              "the trace color.  This generates a continuum of colors between "
                                                              "minimum and maximum values for the variable."),
                                                      QString()));
}

static void setTimeSeries2DVarOptions(const ComponentOptions &options, Variant::Root &config)
{
    if (options.isSet("y")) {
        config["Traces/All/Data/Dimensions/#0/Input"].setString(
                qobject_cast<ComponentOptionSequenceMatch *>(options.get("y"))->get());

        setLogarithmicFilter(options, config, 0);
        setQuantileFilter(options, config, 0);
    }

    if (options.isSet("z")) {
        config["Traces/All/Data/Dimensions/#1/Input"].setString(
                qobject_cast<ComponentOptionSequenceMatch *>(options.get("z"))->get());
        setQuantileFilter(options, config, 1);
    }

    config["Needles"].setType(Variant::Type::Hash);
}

static void insertCyclePlot2DVarOptions(ComponentOptions &options)
{
    options.add("y", new ComponentOptionSequenceMatch(DisplayComponent::tr("y", "name"),
                                                      DisplayComponent::tr("Y Variable"),
                                                      DisplayComponent::tr(
                                                              "This is the variable or variables to plot on the Y axis."),
                                                      QString()));

    options.add("total", new ComponentOptionBoolean(DisplayComponent::tr("total", "name"),
                                                    DisplayComponent::tr("Display a total bin"),
                                                    DisplayComponent::tr(
                                                            "This enables the display of a bin that contains all data."),
                                                    QString()));

    options.add("extend", new ComponentOptionBoolean(DisplayComponent::tr("extend", "name"),
                                                     DisplayComponent::tr(
                                                             "Draw extended median lines"),
                                                     DisplayComponent::tr(
                                                             "If set then the median lines of the bins are "
                                                             "extended to cover the entire graph.  If totals are enabled "
                                                             "only the median of the totals is extended."),
                                                     DisplayComponent::tr(
                                                             "Enabled with a total bin")));
}

static void setCyclePlot2DVarOptions(const ComponentOptions &options, Variant::Root &config)
{
    if (options.isSet("y")) {
        config["XBins/All/Data/Dimensions/#0/Input"].setString(
                qobject_cast<ComponentOptionSequenceMatch *>(options.get("y"))->get());

        setLogarithmicFilter(options, config, 0, "XBins/All", false);
        setQuantileFilter(options, config, 0, "XBins/All", false);

        if (options.isSet("total")) {
            if (qobject_cast<ComponentOptionBoolean *>(options.get("total"))->get()) {
                config["XBins/All/Settings/Binning/TotalAfter"].setBool(true);
                if (!options.isSet("extend")) {
                    config["XBins/All/Settings/MiddleExtend/Enable"].setBool(true);
                }
            }
        }

        if (options.isSet("extend")) {
            if (qobject_cast<ComponentOptionBoolean *>(options.get("extend"))->get()) {
                config["XBins/All/Settings/MiddleExtend/Enable"].setBool(true);
            }
        }
    }
}

static void insertCDFVarOptions(ComponentOptions &options)
{
    options.add("x", new ComponentOptionSequenceMatch(DisplayComponent::tr("x", "name"),
                                                      DisplayComponent::tr("X Variable"),
                                                      DisplayComponent::tr(
                                                              "This is the variable or variables to plot on the X axis."),
                                                      QString()));

    options.add("z", new ComponentOptionSequenceMatch(DisplayComponent::tr("z", "name"),
                                                      DisplayComponent::tr("Z  Variable"),
                                                      DisplayComponent::tr(
                                                              "This is the variable or variables that determine "
                                                              "the trace color.  This generates a continuum of colors between "
                                                              "minimum and maximum values for the variable."),
                                                      QString()));
}

static void setCDFVarOptions(const ComponentOptions &options, Variant::Root &config)
{
    if (options.isSet("x")) {
        config["Traces/All/Data/Dimensions/#0/Input"].setString(
                qobject_cast<ComponentOptionSequenceMatch *>(options.get("x"))->get());

        setLogarithmicFilter(options, config, 0);
        setQuantileFilter(options, config, 0);
    }

    if (options.isSet("z")) {
        config["Traces/All/Data/Dimensions/#2/Input"].setString(
                qobject_cast<ComponentOptionSequenceMatch *>(options.get("z"))->get());
        setQuantileFilter(options, config, 2);
    }
}

static void insertPDFVarOptions(ComponentOptions &options)
{
    options.add("x", new ComponentOptionSequenceMatch(DisplayComponent::tr("x", "name"),
                                                      DisplayComponent::tr("X Variable"),
                                                      DisplayComponent::tr(
                                                              "This is the variable or variables to plot on the X axis."),
                                                      QString()));
}

static void setPDFVarOptions(const ComponentOptions &options, Variant::Root &config)
{
    if (options.isSet("x")) {
        config["XBins/All/Data/Dimensions/#0/Input"].setString(
                qobject_cast<ComponentOptionSequenceMatch *>(options.get("x"))->get());

        setLogarithmicFilter(options, config, 0, "XBins/All");
        setQuantileFilter(options, config, 0, "XBins/All");
    }
}

static void insertAllanVarOptions(ComponentOptions &options)
{
    options.add("y", new ComponentOptionSequenceMatch(DisplayComponent::tr("y", "name"),
                                                      DisplayComponent::tr("Y Variable"),
                                                      DisplayComponent::tr(
                                                              "This is the variable or variables to the "
                                                              "standard deviation of on the Y axis."),
                                                      QString()));

    options.add("z", new ComponentOptionSequenceMatch(DisplayComponent::tr("z", "name"),
                                                      DisplayComponent::tr("Z  Variable"),
                                                      DisplayComponent::tr(
                                                              "This is the variable or variables that determine "
                                                              "the trace color.  This generates a continuum of colors between "
                                                              "minimum and maximum values for the variable."),
                                                      QString()));
}

static void setAllanVarOptions(const ComponentOptions &options, Variant::Root &config)
{
    if (options.isSet("y")) {
        config["Traces/All/Data/Dimensions/#0/Input"].setString(
                qobject_cast<ComponentOptionSequenceMatch *>(options.get("y"))->get());
        setQuantileFilter(options, config, 1);
    }

    if (options.isSet("z")) {
        config["Traces/All/Data/Dimensions/#1/Input"].setString(
                qobject_cast<ComponentOptionSequenceMatch *>(options.get("z"))->get());
        setQuantileFilter(options, config, 1);
    }
}

static ComponentOptions getScatter2DOptions()
{
    ComponentOptions options;

    insertGraph2DCommon(options);
    insertFitCommon(options);
    insertGraph2DVarOptions(options);
    insertQuantileOptions(options);
    insertLogarithmicOptions(options);

    return options;
}

static QList<ComponentExample> getScatter2DExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getScatter2DOptions();

    (qobject_cast<ComponentOptionSequenceMatch *>(options.get("x")))->set("BaG_A11");
    (qobject_cast<ComponentOptionSequenceMatch *>(options.get("y")))->set("BaG_A12");
    examples.append(ComponentExample(options, DisplayComponent::tr("Single trace"),
                                     DisplayComponent::tr(
                                             "This will create a simple scatter plot of "
                                             "BaG_A12 vs BaG_A11 for all cut sizes.")));
    examples.last().setInputTypes(0);
    examples.last().setInputAuxiliary(QStringList() << "BaG_A11" << "BaG_A12");

    return examples;
}

static Variant::Root configurationScatter2D(const ComponentOptions &options)
{
    Variant::Root config;
    config["Type"].setString("Graph");

    setGraph2DCommon(options, config);
    setGraph2DVarOptions(options, config);
    setFitCommon(options, config);

    if (options.isSet("log")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("log"))->get()) {
            config["XAxes/Defaults/Range/Log"].setBool(true);
            config["YAxes/Defaults/Range/Log"].setBool(true);
        }
    }
    return config;

}

static Display *createScatter2D(const ComponentOptions &options, const DisplayDefaults &defaults)
{
    Graph2D *result = new Graph2D(defaults);
    result->initialize(ValueSegment::Transfer{
                               ValueSegment(FP::undefined(), FP::undefined(), configurationScatter2D(options))},
                       defaults);
    return result;
}


static ComponentOptions getTimeSeries2DOptions()
{
    ComponentOptions options;

    insertGraph2DCommon(options);
    insertFitCommon(options);
    insertTimeSeries2DVarOptions(options);
    insertLogarithmicOptions(options);
    insertQuantileOptions(options);

    return options;
}

static QList<ComponentExample> getTimeSeries2DExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getTimeSeries2DOptions();

    (qobject_cast<ComponentOptionSequenceMatch *>(options.get("y")))->set("BaG_A11");
    examples.append(ComponentExample(options, DisplayComponent::tr("Single trace"),
                                     DisplayComponent::tr(
                                             "This will create a simple time series plot of "
                                             "BaG_A11 for all cut sizes.")));
    examples.last().setInputTypes(ComponentExample::Input_Absorption);

    return examples;
}

static Variant::Root configurationTimeSeries2D(const ComponentOptions &options)
{
    Variant::Root config;
    config["Type"].setString("Timeseries");

    setGraph2DCommon(options, config);
    setTimeSeries2DVarOptions(options, config);
    setFitCommon(options, config);

    if (options.isSet("log")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("log"))->get()) {
            config["YAxes/Defaults/Range/Log"].setBool(true);
        }
    }
    return config;

}

static Display *createTimeSeries2D(const ComponentOptions &options, const DisplayDefaults &defaults)
{
    TimeSeries2D *result = new TimeSeries2D(defaults);
    result->initialize(ValueSegment::Transfer{
                               ValueSegment(FP::undefined(), FP::undefined(), configurationTimeSeries2D(options))},
                       defaults);
    return result;
}


static ComponentOptions getCyclePlot2DOptions()
{
    ComponentOptions options;

    insertGraph2DCommon(options);
    insertFitCommon(options);
    insertCyclePlot2DVarOptions(options);
    insertLogarithmicOptions(options);
    insertQuantileOptions(options);

    options.add("box", new ComponentOptionBoolean(DisplayComponent::tr("box", "name"),
                                                  DisplayComponent::tr("Enable box display"),
                                                  DisplayComponent::tr(
                                                          "When disabled this hides the box and displays only the whiskers and median."),
                                                  DisplayComponent::tr("Enabled")));

    ComponentOptionTimeBlock *block =
            new ComponentOptionTimeBlock(DisplayComponent::tr("period", "name"),
                                         DisplayComponent::tr("The total display period"),
                                         DisplayComponent::tr(
                                                 "This is the total period of the display.  Data "
                                                 "is plotted such that the X axis repeats on this period."),
                                         DisplayComponent::tr("One year"));
    block->setDefault(Time::Year, 1, true);
    options.add("interval", block);

    block = new ComponentOptionTimeBlock(DisplayComponent::tr("division", "name"),
                                         DisplayComponent::tr("The division time interval"),
                                         DisplayComponent::tr(
                                                 "The X axis is divided based on this interval.  "
                                                 "For example, if this is set to one month then a division and bin "
                                                 "appear for every month in the period.  This cannot be greater "
                                                 "than the total period.  Alignment is ignored (implicitly aligned "
                                                 "with the period)."),
                                         DisplayComponent::tr("One month"));
    block->setDefault(Time::Month, 1, false);
    options.add("division", block);

    return options;
}

static QList<ComponentExample> getCyclePlot2DExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getCyclePlot2DOptions();

    (qobject_cast<ComponentOptionSequenceMatch *>(options.get("y")))->set("BaG_A11");
    (qobject_cast<ComponentOptionTimeBlock *>(options.get("interval")))->set(Time::Day, 1, true);
    (qobject_cast<ComponentOptionTimeBlock *>(options.get("division")))->set(Time::Hour, 1, false);
    examples.append(ComponentExample(options, DisplayComponent::tr("Hourly"), DisplayComponent::tr(
            "This will create a daily cycle plot with bins "
            "for each hour of the day.")));
    examples.last().setInputTypes(ComponentExample::Input_Absorption);

    return examples;
}

static Variant::Root configurationCyclePlot2D(const ComponentOptions &options)
{
    Variant::Root config;
    config["Type"].setString("Cycle");

    setGraph2DCommon(options, config);
    setCyclePlot2DVarOptions(options, config);
    setFitCommon(options, config);

    if (options.isSet("log")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("log"))->get()) {
            config["YAxes/Defaults/Range/Log"].setBool(true);
        }
    }

    if (options.isSet("interval")) {
        ComponentOptionTimeBlock
                *interval = qobject_cast<ComponentOptionTimeBlock *>(options.get("interval"));
        config["Interval"].set(TimeCycleInterval(interval->getUnit(), interval->getCount(),
                                                 interval->getAlign()).save());
    }

    if (options.isSet("division")) {
        ComponentOptionTimeBlock
                *division = qobject_cast<ComponentOptionTimeBlock *>(options.get("division"));
        config["Division"].set(TimeCycleDivision(division->getUnit(), division->getCount()).save());
    }


    if (options.isSet("box") &&
            !qobject_cast<ComponentOptionBoolean *>(options.get("box"))->get()) {
        config["XBins/All/Settings/Box/Enable"].setBool(false);
        config["XBins/All/Settings/Fill/Enable"].setBool(false);
        config["XBins/All/Settings/Whisker/Width"].setDouble(2.0);
        config["XBins/All/Settings/Middle/Width"].setDouble(5.0);
    }

    return config;

}

static Display *createCyclePlot2D(const ComponentOptions &options, const DisplayDefaults &defaults)
{
    CyclePlot2D *result = new CyclePlot2D(defaults);
    result->initialize(ValueSegment::Transfer{
                               ValueSegment(FP::undefined(), FP::undefined(), configurationCyclePlot2D(options))},
                       defaults);
    return result;
}


static ComponentOptions getCDFOptions()
{
    ComponentOptions options;

    insertGraph2DCommon(options);
    insertCDFVarOptions(options);
    insertLogarithmicOptions(options);
    insertQuantileOptions(options);

    options.add("fit", new ComponentOptionBoolean(DisplayComponent::tr("fit", "name"),
                                                  DisplayComponent::tr(
                                                          "Enable the display of a fit"),
                                                  DisplayComponent::tr(
                                                          "This sets the display of the distribution "
                                                          "fits."),
                                                  DisplayComponent::tr("Enabled")));

    return options;
}

static QList<ComponentExample> getCDFExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getCDFOptions();

    (qobject_cast<ComponentOptionSequenceMatch *>(options.get("x")))->set("BaG_A11");
    (qobject_cast<ComponentOptionBoolean *>(options.get("log")))->set(true);
    examples.append(ComponentExample(options, DisplayComponent::tr("Simple"),
                                     DisplayComponent::tr("This will create a basic CDF plot.")));
    examples.last().setInputTypes(ComponentExample::Input_Absorption);

    return examples;
}

static Variant::Root configurationCDF(const ComponentOptions &options)
{
    Variant::Root config;
    config["Type"].setString("Graph");

    setGraph2DCommon(options, config);
    setCDFVarOptions(options, config);

    if (options.isSet("log")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("log"))->get()) {
            config["XAxes/Defaults/Range/Log"].setBool(true);
        }
    }

    config["Traces/All/Settings/Transformer"].toArray().after_back()["Type"].setString("cdf");
    config["Traces/All/Settings/Line/Enable"].setBool(true);
    config["Traces/All/Settings/Symbol/Enable"].setBool(false);
    config["Traces/All/Settings/Continuity"].setDouble(FP::undefined());
    config["YAxes/Defaults/Fixed"].setType(Variant::Type::Array);
    config["YAxes/Defaults/Title/Text"].setString(DisplayComponent::tr("Percentile"));
    config["YAxes/Defaults/ToolTipUnits"].setString("\xCF\x83");
    config["YAxes/Defaults/Ticks/Mode"].setString("cdf");
    config["YAxes/Defaults/Range/Minimum/Type"].setString("ExtendFraction");
    config["YAxes/Defaults/Range/Minimum/Fraction"].setDouble(0.025);
    config["YAxes/Defaults/Range/Maximum/Type"].setString("ExtendFraction");
    config["YAxes/Defaults/Range/Maximum/Fraction"].setDouble(0.025);
    config["XAxes/Defaults/Range/Minimum/Type"].setString("ExtendFraction");
    config["XAxes/Defaults/Range/Minimum/Fraction"].setDouble(0.05);
    config["XAxes/Defaults/Range/Maximum/Type"].setString("ExtendFraction");
    config["XAxes/Defaults/Range/Maximum/Fraction"].setDouble(0.05);

    if (!options.isSet("fit") ||
            qobject_cast<ComponentOptionBoolean *>(options.get("fit"))->get()) {
        config["Traces/All/Settings/Fit/Model/Type"].setString("cdf");
        config["Traces/All/Settings/Fit/DisableZ"].setBool(true);
    }

    return config;

}

static Display *createCDF(const ComponentOptions &options, const DisplayDefaults &defaults)
{
    Graph2D *result = new Graph2D(defaults);
    result->initialize(ValueSegment::Transfer{
            ValueSegment(FP::undefined(), FP::undefined(), configurationCDF(options))}, defaults);
    return result;
}


static ComponentOptions getPDFOptions()
{
    ComponentOptions options;

    insertGraph2DCommon(options);
    insertPDFVarOptions(options);
    insertLogarithmicOptions(options);
    insertQuantileOptions(options);

    options.add("show-bins", new ComponentOptionBoolean(DisplayComponent::tr("show-bins", "name"),
                                                        DisplayComponent::tr(
                                                                "Enable display of bins"),
                                                        DisplayComponent::tr(
                                                                "This enables the display of the histogram bins "
                                                                "used in the PDF."), QString()));

    ComponentOptionSingleInteger *bins =
            new ComponentOptionSingleInteger(DisplayComponent::tr("bins", "name"),
                                             DisplayComponent::tr("The number of bins used"),
                                             DisplayComponent::tr(
                                                     "This is the number of bins spread uniformly "
                                                     "across the data range used in the histogram."),
                                             DisplayComponent::tr("10", "bin count"));
    bins->setMinimum(2);
    bins->setMaximum(10000);
    bins->setAllowUndefined(false);
    options.add("bins", bins);

    ComponentOptionEnum *fit = new ComponentOptionEnum(DisplayComponent::tr("fit", "name"),
                                                       DisplayComponent::tr("Fit type"),
                                                       DisplayComponent::tr(
                                                               "This is the type of fit used on the histogram."),
                                                       DisplayComponent::tr("Cublic spline",
                                                                            "PDF fit default"));
    fit->add(PDFFit_Disabled, "disabled", DisplayComponent::tr("disabled", "mode name"),
             DisplayComponent::tr("No fit is displayed."));
    fit->alias(PDFFit_Disabled, DisplayComponent::tr("none", "mode name"));
    fit->alias(PDFFit_Disabled, DisplayComponent::tr("off", "mode name"));
    fit->add(PDFFit_CSpline, "cspline", DisplayComponent::tr("cspline", "mode name"),
             DisplayComponent::tr("Cubic spline interpolation."));
    fit->alias(PDFFit_CSpline, DisplayComponent::tr("cubicspline", "mode name"));
    options.add("fit", fit);

    return options;
}

static QList<ComponentExample> getPDFExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getCDFOptions();

    (qobject_cast<ComponentOptionSequenceMatch *>(options.get("x")))->set("BaG_A11");
    (qobject_cast<ComponentOptionBoolean *>(options.get("log")))->set(true);
    examples.append(ComponentExample(options, DisplayComponent::tr("Simple"),
                                     DisplayComponent::tr("This will create a basic PDF plot.")));
    examples.last().setInputTypes(ComponentExample::Input_Absorption);

    return examples;
}

static Variant::Root configurationPDF(const ComponentOptions &options)
{
    Variant::Root config;
    config["Type"].setString("Graph");

    setGraph2DCommon(options, config);
    setPDFVarOptions(options, config);

    int nBins = 10;
    if (options.isSet("bins")) {
        nBins = (int) qobject_cast<ComponentOptionSingleInteger *>(options.get("bins"))->get();
    }

    config["XBins/All/Settings/Box/Histogram"].setBool(true);
    config["XBins/All/Settings/Binning/Type"].setString("Uniform");
    config["XBins/All/Settings/Binning/Count"].setInt64(nBins);
    config["YAxes/Defaults/Fixed"].setType(Variant::Type::Array);
    config["YAxes/Defaults/Range/Minimum/Type"].setString("Fixed");
    config["YAxes/Defaults/Range/Minimum/Value"].setDouble(0.0);
    config["YAxes/Defaults/Range/Maximum/Absolute"].setDouble(100.0);
    config["YAxes/Defaults/ToolTipUnits"].setString("%");

    if (options.isSet("log")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("log"))->get()) {
            config["XAxes/Defaults/Range/Log"].setBool(true);
            config["XBins/All/Settings/Binning/Log"].setBool(true);
        }
    }

    config["XBins/All/Settings/DrawBoxes"].setBool(false);
    if (options.isSet("show-bins")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("show-bins"))->get()) {
            config["XBins/All/Settings/DrawBoxes"].setBool(true);
        }
    }

    if (options.isSet("fit")) {
        switch (qobject_cast<ComponentOptionEnum *>(options.get("fit"))->get().getID()) {
        case PDFFit_Disabled:
            break;
        case PDFFit_CSpline:
            config["XBins/All/Settings/Fit/Upper/Model/Type"].setString("cspline");
            break;
        }
    } else {
        config["XBins/All/Settings/Fit/Upper/Model/Type"].setString("cspline");
    }

    return config;

}

static Display *createPDF(const ComponentOptions &options, const DisplayDefaults &defaults)
{
    Graph2D *result = new Graph2D(defaults);
    result->initialize(ValueSegment::Transfer{
            ValueSegment(FP::undefined(), FP::undefined(), configurationPDF(options))}, defaults);
    return result;
}


static ComponentOptions getDensity2DOptions()
{
    ComponentOptions options;

    insertGraph2DCommon(options);
    insertFitCommon(options);
    insertGraph2DVarOptions(options);
    insertLogarithmicOptions(options);
    insertQuantileOptions(options);

    options.add("points", new ComponentOptionBoolean(DisplayComponent::tr("points", "name"),
                                                     DisplayComponent::tr("Show discrete points"),
                                                     DisplayComponent::tr(
                                                             "This enables the display of the discrete points."),
                                                     DisplayComponent::tr("Disabled")));

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(DisplayComponent::tr("density-limit", "name"),
                                            DisplayComponent::tr("Density limit threshold"),
                                            DisplayComponent::tr(
                                                    "The limit of the contribution of any point to "
                                                    "the total density as a percentage of the total number of points."),
                                            DisplayComponent::tr("0.5% of data"));
    d->setMinimum(0.0, false);
    d->setMaximum(100.0, true);
    d->setAllowUndefined(true);
    options.add("density-limit", d);

    return options;
}

static QList<ComponentExample> getDensity2DExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getDensity2DOptions();

    (qobject_cast<ComponentOptionSequenceMatch *>(options.get("x")))->set("::BaG_A11:pm10");
    (qobject_cast<ComponentOptionSequenceMatch *>(options.get("y")))->set("::BaG_A12:pm10");
    examples.append(ComponentExample(options, DisplayComponent::tr("Single trace"),
                                     DisplayComponent::tr(
                                             "This will create a simple density plot of "
                                             "BaG_A12 vs BaG_A11 for PM10 data.")));
    examples.last().setInputTypes(0);
    examples.last().setInputAuxiliary(QStringList() << "BaG_A11" << "BaG_A12");

    return examples;
}

static Variant::Root configurationDensity2D(const ComponentOptions &options)
{
    Variant::Root config;
    config["Type"].setString("Graph");

    setGraph2DCommon(options, config);
    setGraph2DVarOptions(options, config);
    setFitCommon(options, config);

    if (!options.isSet("points") ||
            !qobject_cast<ComponentOptionBoolean *>(options.get("points"))->get()) {
        config["Traces/All/Settings/Symbol/Enable"].setBool(false);
    }
    config["Traces/All/Settings/Fill/Model/Type"].setString("Density");

    if (options.isSet("density-limit")) {
        double v = qobject_cast<ComponentOptionSingleDouble *>(options.get("density-limit"))->get();
        if (FP::defined(v)) {
            v /= 100.0;
            config["Traces/All/Settings/Fill/Model/Limit"].setDouble(v);
        }
    } else {
        config["Traces/All/Settings/Fill/Model/Limit"].setDouble(0.005);
    }

    if (options.isSet("log")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("log"))->get()) {
            config["XAxes/Defaults/Range/Log"].setBool(true);
            config["YAxes/Defaults/Range/Log"].setBool(true);
        }
    }
    return config;

}

static Display *createDensity2D(const ComponentOptions &options, const DisplayDefaults &defaults)
{
    Graph2D *result = new Graph2D(defaults);
    result->initialize(ValueSegment::Transfer{
                               ValueSegment(FP::undefined(), FP::undefined(), configurationDensity2D(options))},
                       defaults);
    return result;
}


static ComponentOptions getAllanOptions()
{
    ComponentOptions options;

    insertGraph2DCommon(options, true);
    insertAllanVarOptions(options);
    insertQuantileOptions(options);

    options.add("points", new ComponentOptionBoolean(DisplayComponent::tr("points", "name"),
                                                     DisplayComponent::tr("Show discrete points"),
                                                     DisplayComponent::tr(
                                                             "This enables the display of the discrete points."),
                                                     DisplayComponent::tr("Disabled")));

    options.add("fit", new ComponentOptionBoolean(DisplayComponent::tr("fit", "name"),
                                                  DisplayComponent::tr("Display the fit line"),
                                                  DisplayComponent::tr(
                                                          "This controls if the fit line is enabled."),
                                                  DisplayComponent::tr("Enabled")));

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(DisplayComponent::tr("fit-start", "name"),
                                            DisplayComponent::tr("Start of the fit data"),
                                            DisplayComponent::tr(
                                                    "The start bound of the data that contributes to "
                                                    "the fit.  Any data before this will be ignored in the fit."),
                                            DisplayComponent::tr("10 seconds"));
    d->setMinimum(0.0, false);
    d->setAllowUndefined(true);
    options.add("fit-start", d);

    d = new ComponentOptionSingleDouble(DisplayComponent::tr("fit-end", "name"),
                                        DisplayComponent::tr("End of the fit data"),
                                        DisplayComponent::tr(
                                                "The end bound of the data that contributes to "
                                                "the fit.  Any data after this will be ignored in the fit."),
                                        DisplayComponent::tr("100 seconds"));
    d->setMinimum(0.0, false);
    d->setAllowUndefined(true);
    options.add("fit-end", d);

    return options;
}

static QList<ComponentExample> getAllanExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getAllanOptions();

    (qobject_cast<ComponentOptionSequenceMatch *>(options.get("y")))->set("::BaG_A11:pm10");
    examples.append(ComponentExample(options, DisplayComponent::tr("Single trace"),
                                     DisplayComponent::tr(
                                             "This will create a simple Allan plot of PM10 "
                                             "data for BaG_A11.")));
    examples.last().setInputTypes(ComponentExample::Input_Absorption);
    examples.last().setInputStation("sfa");
    examples.last().setInputRange(Time::Bounds(1354500824, 1354536000));

    return examples;
}

static Variant::Root configurationAllan(const ComponentOptions &options)
{
    Variant::Root config;
    config["Type"].setString("Graph");

    config["Legend/Position"].setString("TopRight");

    setGraph2DCommon(options, config);
    setAllanVarOptions(options, config);

    config["XAxes/Defaults/Range/Log"].setBool(true);
    config["YAxes/Defaults/Range/Log"].setBool(true);
    config["ZAxes/Defaults/Range/Log"].setBool(true);

    config["Traces/All/Settings/Transformer"].toArray().after_back()["Type"].setString("allan");
    config["Traces/All/Settings/TransformerMode"].setString("WithStart");
    config["Traces/All/Settings/Continuity"].setDouble(FP::undefined());

    if (!options.isSet("points") ||
            !qobject_cast<ComponentOptionBoolean *>(options.get("points"))->get()) {
        config["Traces/All/Settings/Symbol/Enable"].setBool(false);
        config["Traces/All/Settings/Line/Enable"].setBool(true);
    } else {
        config["Traces/All/Settings/Symbol/Enable"].setBool(true);
        config["Traces/All/Settings/Line/Enable"].setBool(false);
    }

    if (!options.isSet("fit") ||
            qobject_cast<ComponentOptionBoolean *>(options.get("fit"))->get()) {
        config["Traces/All/Settings/Fit/Model/Type"].setString("allan");
        config["Traces/All/Settings/Fit/Log/X"].setDouble(FP::undefined());
        config["Traces/All/Settings/Fit/Log/Y"].setDouble(FP::undefined());
        config["Traces/All/Settings/Fit/Log/Z"].setDouble(FP::undefined());

        if (options.isSet("fit-start")) {
            config["Traces/All/Settings/Fit/Model/Start"].setDouble(
                    qobject_cast<ComponentOptionSingleDouble *>(options.get("fit-start"))->get());
        } else {
            config["Traces/All/Settings/Fit/Model/Start"].setDouble(10.0);
        }

        if (options.isSet("fit-end")) {
            config["Traces/All/Settings/Fit/Model/End"].setDouble(
                    qobject_cast<ComponentOptionSingleDouble *>(options.get("fit-end"))->get());
        } else {
            config["Traces/All/Settings/Fit/Model/End"].setDouble(100.0);
        }
    }

    config["XAxes/Defaults/Fixed"].setType(Variant::Type::Array);
    config["XAxes/Defaults/Title/Text"].setString(DisplayComponent::tr("Averaging Time"));
    config["XAxes/Defaults/ToolTipUnits"].setString("seconds");
    config["XAxes/Defaults/Ticks/Mode"].setString("allan");

    return config;

}

static Display *createAllan(const ComponentOptions &options, const DisplayDefaults &defaults)
{
    Graph2D *result = new Graph2D(defaults);
    result->initialize(ValueSegment::Transfer{
            ValueSegment(FP::undefined(), FP::undefined(), configurationAllan(options))}, defaults);
    return result;
}


static ComponentOptions getLayoutOptions()
{
    ComponentOptions options;

    ComponentOptionFile *file = new ComponentOptionFile(DisplayComponent::tr("input", "name"),
                                                        DisplayComponent::tr(
                                                                "The file that contains the layout"),
                                                        DisplayComponent::tr(
                                                                "If set then the layout is loaded from this file."),
                                                        QString());
    file->setMode(ComponentOptionFile::Read);
    file->setExtensions(QSet<QString>() << "xml" << "cpd3");
    file->setTypeDescription(DisplayComponent::tr("CPD3 data file"));
    options.add("input", file);

    options.add("configuration",
                new DynamicSequenceSelectionOption(DisplayComponent::tr("configuration", "name"),
                                                   DisplayComponent::tr(
                                                           "The configuration that contains the layout"),
                                                   DisplayComponent::tr(
                                                           "The variable in the archive used to load the layout."),
                                                   QString()));

    options.exclude("input", "configuration");
    options.exclude("configuration", "input");

    options.add("path", new ComponentOptionSingleString(DisplayComponent::tr("path", "name"),
                                                        DisplayComponent::tr("Configuration path"),
                                                        DisplayComponent::tr(
                                                                "The path within the configuration to use."),
                                                        QString()));

    return options;
}

static QList<ComponentExample> getLayoutExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getLayoutOptions();
    (qobject_cast<ComponentOptionFile *>(options.get("input")))->set("layout.xml");
    examples.append(ComponentExample(options, DisplayComponent::tr("Static file"),
                                     DisplayComponent::tr(
                                             "This will load the layout contained in the file "
                                             "\"layout.xml\".")));

    options = getLayoutOptions();
    /*(qobject_cast<ComponentOptionSequenceMatch *>
        (options.get("configuration")))->set("bnd:config:displays");*/
    (qobject_cast<ComponentOptionSingleString *>(options.get("path")))->set(
            "Templates/Graphs/Scattering");
    examples.append(ComponentExample(options, DisplayComponent::tr("Archive display"),
                                     DisplayComponent::tr(
                                             "This a display from the main system configuration.")));

    return examples;
}

namespace {
class LayoutSegmentReader : public StreamSink {
    SequenceSegment::Stream reader;
public:
    SequenceSegment::Transfer result;

    LayoutSegmentReader() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    { Util::append(reader.add(values), result); }

    void incomingData(SequenceValue::Transfer &&values) override
    { Util::append(reader.add(std::move(values)), result); }

    void incomingData(const SequenceValue &value) override
    { Util::append(reader.add(value), result); }

    void incomingData(SequenceValue &&value) override
    { Util::append(reader.add(std::move(value)), result); }

    virtual void endData()
    { Util::append(reader.finish(), result); }
};
}

static ValueSegment::Transfer getLayoutConfiguration(const ComponentOptions &options)
{
    ValueSegment::Transfer result;

    if (options.isSet("input")) {
        auto fileName = qobject_cast<ComponentOptionFile *>(options.get("input"))->get();
        auto file = IO::Access::file(fileName, IO::File::Mode::readOnly())->stream();
        if (!file) {
            qCWarning(log_graphing_displaycomponent) << "Error opening layout file" << fileName;
            return {};
        }

        StandardDataInput dataInput;
        LayoutSegmentReader reader;
        dataInput.start();
        dataInput.setEgress(&reader);

        IO::Generic::Stream::Iterator it(*file);
        while (auto contents = it.next()) {
            dataInput.incomingData(contents);
        }
        dataInput.endData();
        dataInput.wait();
        result = SequenceSegment::reduce(reader.result);
    } else {
        Archive::Access archive;
        Archive::Access::ReadLock lock(archive);
        Archive::Selection::Match stations;
        Util::append(SequenceName::impliedStations(&archive), stations);

        std::unique_ptr<DynamicSequenceSelection> match;
        if (options.isSet("configuration")) {
            match.reset((qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("configuration")))->getOperator());
        } else {
            match.reset(new DynamicSequenceSelection::Match(std::vector<SequenceMatch::Element>{
                    SequenceMatch::Element(QStringList(), {"configuration"}, {"displays"})}));
        }

        auto selections = match->toArchiveSelections(stations, {"configuration"}, {"displays"});
        double now = Time::time();
        for (auto sel = selections.begin(); sel != selections.end();) {
            sel->includeMetaArchive = false;
            if (Range::compareStart(now, sel->getStart()) > 0) {
                sel->setStart(now);
                if (Range::compareStartEnd(sel->getStart(), sel->getEnd()) >= 0) {
                    sel = selections.erase(sel);
                    continue;
                }
            }
            ++sel;
        }

        if (!selections.empty()) {
            result.clear();

            StreamSink::Iterator incoming;
            archive.readStream(selections, &incoming)->detach();

            ValueSegment::Stream reader;
            while (incoming.hasNext()) {
                auto add = incoming.next();
                if (!match->matches(add, add.getName()))
                    continue;
                Util::append(reader.add(std::move(add)), result);
            }
            Util::append(reader.finish(), result);
        }

        if (options.isSet("path")) {
            QString path(qobject_cast<ComponentOptionSingleString *>(options.get("path"))->get());
            result = ValueSegment::withPath(std::move(result), path);
        }

        if (result.empty()) {
            qCWarning(log_graphing_displaycomponent) << "Layout contains no configuration";
        } else {
            bool anyDefined = false;
            for (const auto &it : result) {
                if (it.getValue().exists()) {
                    anyDefined = true;
                    break;
                }
            }
            if (!anyDefined) {
                qCWarning(log_graphing_displaycomponent) << "Layout has no valid configuration";
            }
        }
    }

    return result;
}

static Variant::Root configurationLayout(const ComponentOptions &options)
{
    std::vector<Variant::Root> toMerge;
    for (auto &add : getLayoutConfiguration(options)) {
        toMerge.emplace_back(std::move(add.root()));
    }
    return Variant::Root::overlay(toMerge.begin(), toMerge.end());
}

static Display *createLayout(const ComponentOptions &options, const DisplayDefaults &defaults)
{
    DisplayLayout *result = new DisplayLayout(defaults);
    result->initialize(getLayoutConfiguration(options), defaults);
    return result;
}


/**
 * Get the options for the given display type.
 * 
 * @param type  the type
 * @return      a set of options
 */
ComponentOptions DisplayComponent::options(DisplayType type)
{
    switch (type) {
    case Display_Scatter2D:
        return getScatter2DOptions();
    case Display_TimeSeries2D:
        return getTimeSeries2DOptions();
    case Display_Cycle2D:
        return getCyclePlot2DOptions();
    case Display_CDF:
        return getCDFOptions();
    case Display_PDF:
        return getPDFOptions();
    case Display_Density2D:
        return getDensity2DOptions();
    case Display_Allan:
        return getAllanOptions();
    case Display_Layout:
        return getLayoutOptions();
    }
    Q_ASSERT(false);
    return ComponentOptions();
}

/**
 * Get the list of examples for the display given type.
 * 
 * @param type  the type
 * @return      a list of examples
 */
QList<ComponentExample> DisplayComponent::examples(DisplayType type)
{
    switch (type) {
    case Display_Scatter2D:
        return getScatter2DExamples();
    case Display_TimeSeries2D:
        return getTimeSeries2DExamples();
    case Display_Cycle2D:
        return getCyclePlot2DExamples();
    case Display_CDF:
        return getCDFExamples();
    case Display_PDF:
        return getPDFExamples();
    case Display_Density2D:
        return getDensity2DExamples();
    case Display_Allan:
        return getAllanExamples();
    case Display_Layout:
        return getLayoutExamples();
    }
    Q_ASSERT(false);
    return QList<ComponentExample>();
}

/**
 * Create an instance of the given type with the given options.
 * 
 * @param type      the type to create
 * @param options   the options to use
 * @param defaults  the defaults to use
 * @return          a newly allocated display
 */
Display *DisplayComponent::create(DisplayType type,
                                  const ComponentOptions &options,
                                  const DisplayDefaults &defaults)
{
    switch (type) {
    case Display_Scatter2D:
        return createScatter2D(options, defaults);
    case Display_TimeSeries2D:
        return createTimeSeries2D(options, defaults);
    case Display_Cycle2D:
        return createCyclePlot2D(options, defaults);
    case Display_CDF:
        return createCDF(options, defaults);
    case Display_PDF:
        return createPDF(options, defaults);
    case Display_Density2D:
        return createDensity2D(options, defaults);
    case Display_Allan:
        return createAllan(options, defaults);
    case Display_Layout:
        return createLayout(options, defaults);
    }
    Q_ASSERT(false);
    return NULL;
}

/**
 * Get the baseline configuration for the given type and options
 * 
 * @param type      the type to create
 * @param options   the options to use
 * @return          the baseline configuration
 */
Variant::Root DisplayComponent::baselineConfiguration(DisplayType type,
                                                      const ComponentOptions &options)
{
    switch (type) {
    case Display_Scatter2D:
        return configurationScatter2D(options);
    case Display_TimeSeries2D:
        return configurationTimeSeries2D(options);
    case Display_Cycle2D:
        return configurationCyclePlot2D(options);
    case Display_CDF:
        return configurationCDF(options);
    case Display_PDF:
        return configurationPDF(options);
    case Display_Density2D:
        return configurationDensity2D(options);
    case Display_Allan:
        return configurationAllan(options);
    case Display_Layout:
        return configurationLayout(options);
    }
    Q_ASSERT(false);
    return Variant::Root();
}

}
}
