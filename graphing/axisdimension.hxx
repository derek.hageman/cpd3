/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGAXISDIMENSION_H
#define CPD3GRAPHINGAXISDIMENSION_H

#include "core/first.hxx"

#include <memory>
#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QPointer>
#include <QAtomicInt>

#include "graphing/graphing.hxx"
#include "graphing/axistransformer.hxx"
#include "graphing/axistickgenerator.hxx"
#include "graphing/display.hxx"

namespace CPD3 {
namespace Graphing {

class AxisDimensionSet;

/**
 * The base class for all axis parameters.
 */
class CPD3GRAPHING_EXPORT AxisParameters {
    enum {
        Component_Transformer = 0x00000001,
        Component_AlwaysShow = 0x00000002,
        Component_Units = 0x00000004,
        Component_GroupUnits = 0x00000008,
        Component_TickGenerator = 0x00000010,
        Component_TickLevels = 0x00000020,
        Component_SortPriority = 0x00000040,
    };
    int components;
    AxisTransformer transformer;
    bool alwaysShow;
    std::unordered_set<QString> units;
    std::unordered_set<QString> groupUnits;
    std::shared_ptr<AxisTickGenerator> tickGenerator;
    double tickLogBase;
    std::size_t tickLevels;
    int sortPriority;

    friend class AxisDimensionSet;

public:
    AxisParameters();

    virtual ~AxisParameters();

    AxisParameters(const AxisParameters &);

    AxisParameters &operator=(const AxisParameters &);

    AxisParameters(AxisParameters &&);

    AxisParameters &operator=(AxisParameters &&);

    AxisParameters(const Data::Variant::Read &value, QSettings *settings = nullptr);

    virtual void save(Data::Variant::Write &value) const;

    virtual std::unique_ptr<AxisParameters> clone() const;

    virtual void overlay(const AxisParameters *over);

    virtual void copy(const AxisParameters *from);

    virtual void clear();

    /**
     * Test if the parameters specify a transformer.
     * @return true if the parameters specify a transformer
     */
    inline bool hasTransformer() const
    { return components & Component_Transformer; }

    /**
     * Get the axis transformer.
     * @return the axis transformer
     */
    inline AxisTransformer getTransformer() const
    { return transformer; }

    /**
     * Override the axis transformer.  This sets the defined flag.
     * @param transformer   the new transformer
     */
    inline void overrideTransformer(const AxisTransformer &transformer)
    {
        this->transformer = transformer;
        components |= Component_Transformer;
    }

    /**
     * Test if the parameters specify the always show state.
     * @return true if the parameters specify the always show state
     */
    inline bool hasAlwaysShow() const
    { return components & Component_AlwaysShow; }

    /**
     * Test if the axis should always be shown, even if unused.
     * @return the axis always show state
     */
    inline bool getAlwaysShow() const
    { return alwaysShow; }

    /**
     * Test if the parameters specify the axis units.
     * @return true if the parameters specify the axis units
     */
    inline bool hasUnits() const
    { return components & Component_Units; }

    /**
     * Get the axis units.
     * @return the axis units
     */
    inline const std::unordered_set<QString> &getUnits() const
    { return units; }

    /**
     * Test if the parameters specify the axis group units.
     * @return true if the parameters specify the axis units
     */
    inline bool hasGroupUnits() const
    { return components & Component_GroupUnits; }

    /**
     * Get the axis gruop units.
     * @return the axis units
     */
    inline const std::unordered_set<QString> &getGroupUnits() const
    { return groupUnits; }

    /**
     * Test if the parameters specify a tick generator.
     * @return true if the parameters specify a tick generator
     */
    inline bool hasTickGenerator() const
    { return components & Component_TickGenerator; }

    /**
     * Get the axis tick generator.
     * @return the tick generator
     */
    inline std::shared_ptr<AxisTickGenerator> getTickGenerator() const
    { return tickGenerator; }

    /**
     * Set the axis tick generator.  This does not change the defined flag.
     * @return the tick generator
     */
    inline void setTickGenerator(const std::shared_ptr<AxisTickGenerator> &set)
    { tickGenerator = set; }

    /**
     * Get the tick log base.
     * @return the tick log base
     */
    inline double getTickLogBase() const
    { return tickLogBase; }

    /**
     * Test if the parameters specify the number of ticks levels.
     * @return true if the parameters specify the number of tick levels
     */
    inline bool hasTickLevels() const
    { return components & Component_TickLevels; }

    /**
     * Get the total number of tick levels.
     * @return the number of tick levels
     */
    inline std::size_t getTickLevels() const
    { return tickLevels; }

    /**
     * Set the number of tick levels.  This does not set the defined flag.
     * @param n the new number of tick levels
     */
    inline void setTickLevels(std::size_t n)
    { tickLevels = n; }

    /**
     * Test if the parameters specify the sort priority.
     * @return true if the parameters specify the sort priority
     */
    inline bool hasSortPriority() const
    { return components & Component_SortPriority; }

    /**
     * Get the axis sort priority.
     * @return the axis sort priority
     */
    inline int getSortPriority() const
    { return sortPriority; }

protected:
    /**
     * Force the axis transformer to the given value.
     * 
     * @param setTransformer    the new transformer
     * @param defined           the new defined mode
     */
    void forceAxisTransformer(const AxisTransformer &setTransformer, bool defined = false);

    /**
     * Force the axis tick generator to the given value.
     * 
     * @param setTickGenerator  the new generator
     * @param setTickLogBase    the new tick log base
     * @param defined           the new defined mode
     */
    void forceTickGenerator(const std::shared_ptr<AxisTickGenerator> &setTickGenerator,
                            double setTickLogBase = FP::undefined(),
                            bool defined = false);
};

class AxisDimensionSet;

class AxisDimensionZoom;

/**
 * A single dimension of values for an axis.
 */
class CPD3GRAPHING_EXPORT AxisDimension : public QObject {
Q_OBJECT

    friend class AxisDimensionSet;

    friend class AxisDimensionZoom;

    std::unique_ptr<AxisParameters> parameters;
    std::unique_ptr<AxisParameters> originalParameters;
    std::unique_ptr<AxisParameters> overridenParameters;

    AxisTransformer transformer;

    double rawMin;
    double rawMax;
    double dataMin;
    double dataMax;
    double zoomMin;
    double zoomMax;

    std::unordered_set<QString> units;
    std::unordered_set<QString> groupUnits;
    bool dynamic;
    int attachedReferenceCount;

    QAtomicInt externalReferenceCount;

    inline bool hasExternalReferences() const
    {
#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
        return externalReferenceCount.load() != 0;
#else
        return externalReferenceCount.loadRelaxed() != 0;
#endif
    }

public:
    AxisDimension(std::unique_ptr<AxisParameters> &&params);

    virtual ~AxisDimension();

    /**
     * Create a polymorphic copy of the dimension.  This copy does NOT
     * inherit any outstanding bindings or couplings.
     * 
     * @return a copy
     */
    virtual std::unique_ptr<AxisDimension> clone() const;

    /**
     * Get the axis sort priority.
     * @return the axis sort priority
     */
    inline int getSortPriority() const
    { return parameters->getSortPriority(); }

    /**
     * Get a reference to the axis transformer.
     * @return a reference to the axis transformer
     */
    inline AxisTransformer &getTransformer()
    { return transformer; }

    /**
     * Get a copy of the axis transformer.
     * @return a copy of the axis transformer
     */
    inline AxisTransformer getTransformer() const
    { return transformer; }

    /**
     * Get the logarithm base for the ticks
     * @return the log base for the ticks
     */
    inline double getTickLogBase() const
    { return parameters->getTickLogBase(); }

    /**
     * Get the minimum bound of data values seen.
     * @return the minimum real value
     */
    inline double getRawMin() const
    { return rawMin; }

    /**
     * Get the maximum bound of data values seen.
     * @return the maximum real value
     */
    inline double getRawMax() const
    { return rawMax; }

    /**
     * Force the minimum bound of data to the given value.
     * @param m the new minimum bound
     */
    inline void setRawMin(double m)
    { rawMin = m; }

    /**
     * Force the maximum bound of data to the given value.
     * @param m the new maximum bound
     */
    inline void setRawMax(double m)
    { rawMax = m; }

    /**
     * Get the minimum bound of data on the axis.
     * @return the minimum real value
     */
    inline double getMin() const
    { return dataMin; }

    /**
     * Get the maximum bound of data on the axis.
     * @return the maximum real value
     */
    inline double getMax() const
    { return dataMax; }

    /**
     * Get the minimum bound of the axis zoom (if any).
     * @return the minimum real value
     */
    inline double getZoomMin() const
    { return zoomMin; }

    /**
     * Get the maximum bound of the axis zoom (if any).
     * @return the maximum real value
     */
    inline double getZoomMax() const
    { return zoomMax; }

    /**
     * Get the axis units.
     * @return the axis units
     */
    inline const std::unordered_set<QString> &getUnits() const
    { return units; }

    /**
     * Get the axis grouping units.
     * @return the axis grouping units
     */
    inline const std::unordered_set<QString> &getGroupUnits() const
    { return groupUnits; }

    /**
     * Get the untransformed ticks
     * @return a copy of the untransformed ticks
     */
    virtual std::vector<std::vector<AxisTickGenerator::Tick>> getTicks() const;

    /**
     * Register data limits.
     * 
     * @param min   the minimum observed value
     * @param max   the maximum observed value
     */
    void registerDataLimits(double min, double max);

    /**
     * Called to indicate that all data has been registered and the
     * transformer should be notified.
     */
    void allDataRegistered();

    /**
     * Set the screen space limits.
     * 
     * @param min   the screen coordinate of the minimum value
     * @param max   the screen coordinate of the maximum value
     */
    void setScreen(double min, double max);

    /**
     * Set the zoom override.
     * 
     * @param min   the real space zoom minimum
     * @param max   the real space zoom maximum
     */
    void setZoom(double min, double max);

    /**
     * Called if a coupling is in effect.
     * 
     * @param min       the required minimum
     * @param max       the required maximum
     * @param position  the position of this axis with respect to the coupling
     */
    virtual void couplingUpdated(double min,
                                 double max,
                                 DisplayCoupling::CouplingPosition position);

    /**
     * Create range couplings (if any) for this axis.
     * 
     * @param mode      the suggested mode to create the coupling with
     * @return          a list of range couplings
     */
    virtual std::vector<std::shared_ptr<
            DisplayCoupling>> createRangeCoupling(DisplayCoupling::CoupleMode mode = DisplayCoupling::Always);

    /**
     * Called to mark the beginning of an update.
     */
    virtual void beginUpdate();

    /**
     * Test if the dimension has any overrides set.
     * 
     * @return true if there are any overrides set
     */
    inline bool hasOverrideParameters() const
    { return overridenParameters != nullptr; }

    AxisParameters *overrideParameters();

    void mergeOverrideParameters();

    void revertOverrideParameters();

    void saveOverrideParameters(Data::Variant::Write &target) const;

protected:
    /**
     * Copy the dimension with the given parameters
     * 
     * @param other         the dimension to copy
     * @param parameters    the parameters to use
     */
    AxisDimension(const AxisDimension &other, std::unique_ptr<AxisParameters> &&params);

    /**
     * Called to update the axis transformer.
     * 
     * @param min       the input and output axis minimum
     * @param max       the input and output axis maximum
     * @param zoomMin   the zoom minimum
     * @param zoomMax   the zoom maximum
     */
    virtual void updateTransformer(double &min, double &max, double zoomMin, double zoomMax);

    /**
     * Get the current parameters.
     *
     * @return  the parameters
     */
    inline AxisParameters *getParameters() const
    { return parameters.get(); }
};

namespace Internal {

/**
 * The general class used to apply unit based couplings.
 */
class AxisUnitCoupling : public DisplayRangeCoupling {
Q_OBJECT

    QPointer<AxisDimension> parent;
public:
    AxisUnitCoupling();

    AxisUnitCoupling(DisplayCoupling::CoupleMode mode, AxisDimension *setParent);

    virtual ~AxisUnitCoupling();

    double getMin() const override;

    double getMax() const override;

    bool canCouple(const std::shared_ptr<DisplayCoupling> &other) const override;

    void applyCoupling(const QVariant &data, DisplayCoupling::CouplingPosition position) override;
};

}

class AxisDimensionSet;

/**
 * A general zoom controller for an axis.
 */
class CPD3GRAPHING_EXPORT AxisDimensionZoom : public DisplayZoomAxis {
Q_OBJECT

    QPointer<AxisDimension> dimension;
    QPointer<AxisDimensionSet> parent;
    std::vector<std::weak_ptr<DisplayZoomAxis> > siblings;

    friend class AxisDimensionSet;

public:
    AxisDimensionZoom();

    AxisDimensionZoom(AxisDimension *setDimension, AxisDimensionSet *setParent);

    virtual ~AxisDimensionZoom();

    QString getTitle() const override;

    double getMin() const override;

    double getMax() const override;

    double getZoomMin() const override;

    double getZoomMax() const override;

    std::vector<std::shared_ptr<DisplayZoomAxis>> getCoupledAxes() const override;

    void setZoom(double min, double max) override;

    bool sharedZoom(const std::shared_ptr<DisplayZoomAxis> &other) const override;
};

/**
 * A set of axes for a single dimension of data.
 */
class CPD3GRAPHING_EXPORT AxisDimensionSet : public QObject {
Q_OBJECT

    friend class AxisDimensionZoom;

    std::unique_ptr<AxisParameters> defaults;

    std::unordered_map<std::string, std::unique_ptr<AxisDimension>> bound;
    std::vector<std::unique_ptr<AxisDimension>> unbound;

    std::unordered_map<std::string, std::unique_ptr<AxisDimension>> unreferencedBound;
    std::vector<std::unique_ptr<AxisDimension>> unreferencedUnbound;

    struct OverrideBound {
        ~OverrideBound();

        QRegularExpression matcher;
        std::unique_ptr<AxisParameters> parameters;

        void apply(AxisParameters *target, const QString &name) const;
    };

    std::vector<std::unique_ptr<OverrideBound>> overrideBound;

    struct OverrideUnbound {
        ~OverrideUnbound();

        std::vector<QRegularExpression> matchers;
        bool useGroup;
        std::unique_ptr<AxisParameters> parameters;

        void apply(AxisParameters *target,
                   const std::unordered_set<QString> &units,
                   const std::unordered_set<QString> &groupUnits) const;
    };

    std::vector<std::unique_ptr<OverrideUnbound>> overrideUnbound;

    void externalZoomNotify();

public:
    AxisDimensionSet();

    virtual ~AxisDimensionSet();

    /**
     * Create a polymorphic copy of the set of axes.
     * 
     * @return a copy
     */
    virtual std::unique_ptr<AxisDimensionSet> clone() const;

    /**
     * Test if two values representing configurations for the axis set
     * set could be merged together into a single set.
     * 
     * @param a the first configuration
     * @param b the second configuration
     */
    static bool mergable(const Data::Variant::Read &a, const Data::Variant::Read &b);

    /**
     * Initialize the axis dimension.
     * 
     * @param config        the configuration
     * @param defaults      the defaults to use
     */
    virtual void initialize(const Data::ValueSegment::Transfer &config,
                            const DisplayDefaults &defaults = DisplayDefaults());

    /**
     * Called to mark the beginning of an update.  During an update
     * axes are expected to be referenced and the data range set.  If
     * an axis is not referenced before the call to finishUpdate() to may
     * be removed or hidden.
     */
    virtual void beginUpdate();

    /**
     * Called after beginUpdate() to complete the update.
     */
    virtual void finishUpdate();

    /**
     * Get an axis dimension associated with the given units.  This may select
     * a static axis of one matches the given units.
     * 
     * @param units         the displayable units
     * @param groupUnits    the units to group by or empty to use the display units
     * @return              an axis pointer
     */
    virtual AxisDimension *get(const std::unordered_set<QString> &units,
                               const std::unordered_set<QString> &groupUnits);

    /**
     * Get an axis dimension associated with the given binding.  This may select
     * a static axis of one matches the binding but it may also create a
     * dynamic one if no static axis of the given binding exists.
     * 
     * @param units         the displayable units
     * @param groupUnits    the units to group by or empty to use the display units
     * @param binding       the axis binding
     * @return              an axis pointer
     */
    virtual AxisDimension *get(const std::unordered_set<QString> &units,
                               const std::unordered_set<QString> &groupUnits,
                               const QString &binding);

    /**
     * Get all axis dimensions that should be displayed.
     * 
     * @return a list all axis dimensions.
     */
    virtual std::vector<AxisDimension *> getDisplayed() const;

    /**
     * Count the number of axes that should be displayed.
     * 
     * @return the number of displayed axes
     */
    std::size_t countDisplayed() const;

    /**
     * Get all axis dimensions that are currently referenced.
     * 
     * @return a list all axis dimensions.
     */
    virtual std::vector<AxisDimension *> getAllDimensions() const;

    /**
     * Get the zoom controllers for the axis set.
     * 
     * @return a list of zoom controllers
     */
    virtual std::vector<std::shared_ptr<DisplayZoomAxis>> getZoom();

    /**
     * Create a zoom command for the given mouse zoom points.  The
     * axes must have had their transformers bound correctly, since this
     * will use them to back out the transform on the given points.
     * 
     * @param min   the coordinate of the new minimum bound
     * @param max   the coordinate of the new maximum bound
     * @return      a zoom command
     */
    DisplayZoomCommand convertMouseZoom(qreal min, qreal max);

    /**
     * Create range couplings (if any) for the axis set.
     * 
     * @param mode      the suggested mode to create the coupling with
     * @return          a list of range couplings
     */
    virtual std::vector<std::shared_ptr<
            DisplayCoupling>> createRangeCoupling(DisplayCoupling::CoupleMode mode = DisplayCoupling::Always);

    /**
     * Get the default axis parameters;
     * 
     * @return the default axis parameters
     */
    inline AxisParameters *getDefaultParameters() const
    { return defaults.get(); }

    Data::ValueSegment::Transfer getManualOverrides(const QString &overrideName = QString::fromLatin1(
            "Saved_%1"));

    void resetManualOverrides();

protected:
    AxisDimensionSet(const AxisDimensionSet &other);

    /**
     * Parse the given value into a new parameters object.
     * 
     * @param value     the value to parse
     * @param settings  the default settings object
     * @return          a newly allocated parameters object
     */
    virtual std::unique_ptr<AxisParameters> parseParameters(const Data::Variant::Read &value,
                                                            QSettings *settings = nullptr) const;

    /**
     * Create a new axis dimension object, given a set of parameters.
     * 
     * @param parameters    the parameters to create the dimension with
     * @return              a newly allocated handler
     */
    virtual std::unique_ptr<AxisDimension> createDimension(std::unique_ptr<
            AxisParameters> &&parameters) const;

    /**
     * Create the zoom object for a given dimension, or return NULL for
     * none.
     * 
     * @param dimension the dimension to zoom for
     * @return          the zoom or NULL for none
     */
    virtual std::shared_ptr<AxisDimensionZoom> createZoom(AxisDimension *dimension);

signals:

    /**
     * Emitted whenever the zoom is updated on any component dimension.
     */
    void zoomUpdated();
};

}
}

#endif
