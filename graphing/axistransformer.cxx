/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "graphing/axistransformer.hxx"
#include "core/util.hxx"

using namespace CPD3::Data;


namespace CPD3 {
namespace Graphing {

/** @file graphing/axistransformer.hxx
 * Provides the interface to transform points from real space to screen space.
 */

AxisTransformer::AxisTransformer() : realMin(FP::undefined()),
                                     realMax(FP::undefined()),
                                     screenMin(FP::undefined()),
                                     screenMax(FP::undefined()),
                                     isLog(false),
                                     reverse(false),
                                     scale(FP::undefined()),
                                     realOffset(FP::undefined()),
                                     minMode(ExtendFraction),
                                     boundMin(0.1),
                                     boundMinFractionAtLeast(FP::undefined()),
                                     boundMinFractionAtMost(FP::undefined()),
                                     maxMode(ExtendFraction),
                                     boundMax(0.1),
                                     boundMaxFractionAtLeast(FP::undefined()),
                                     boundMaxFractionAtMost(FP::undefined()),
                                     absoluteMin(FP::undefined()),
                                     absoluteMax(FP::undefined()),
                                     requiredMin(FP::undefined()),
                                     requiredMax(FP::undefined())
{ }

AxisTransformer::AxisTransformer(const AxisTransformer &other) = default;

AxisTransformer &AxisTransformer::operator=(const AxisTransformer &other) = default;

AxisTransformer::AxisTransformer(AxisTransformer &&other) = default;

AxisTransformer &AxisTransformer::operator=(AxisTransformer &&other) = default;

/**
 * Construct a transformer from the given configuration.
 * 
 * @param configuration     the configuration to use
 * @param extend            true if the configuration should extend by default
 */
AxisTransformer::AxisTransformer(const Variant::Read &configuration) : realMin(FP::undefined()),
                                                                       realMax(FP::undefined()),
                                                                       screenMin(FP::undefined()),
                                                                       screenMax(FP::undefined()),
                                                                       isLog(false),
                                                                       reverse(false),
                                                                       scale(FP::undefined()),
                                                                       realOffset(FP::undefined()),
                                                                       minMode(ExtendFraction),
                                                                       boundMin(0.1),
                                                                       boundMinFractionAtLeast(
                                                                               FP::undefined()),
                                                                       boundMinFractionAtMost(
                                                                               FP::undefined()),
                                                                       maxMode(ExtendFraction),
                                                                       boundMax(0.1),
                                                                       boundMaxFractionAtLeast(
                                                                               FP::undefined()),
                                                                       boundMaxFractionAtMost(
                                                                               FP::undefined()),
                                                                       absoluteMin(FP::undefined()),
                                                                       absoluteMax(FP::undefined()),
                                                                       requiredMin(FP::undefined()),
                                                                       requiredMax(FP::undefined())
{
    if (configuration["Log"].exists())
        isLog = configuration["Log"].toBool();

    if (configuration["Reverse"].exists())
        reverse = configuration["Reverse"].toBool();
    {
        const auto &type = configuration["Minimum/Type"].toString();
        if (Util::equal_insensitive(type, "extendfraction")) {
            minMode = ExtendFraction;
            if (configuration["Minimum/Fraction"].exists())
                boundMin = configuration["Minimum/Fraction"].toDouble();
            if (configuration["Minimum/AtLeast"].exists())
                boundMinFractionAtLeast = configuration["Minimum/AtLeast"].toDouble();
            if (configuration["Minimum/AtMost"].exists())
                boundMinFractionAtMost = configuration["Minimum/AtMost"].toDouble();
        } else if (Util::equal_insensitive(type, "fixed") &&
                configuration["Minimum/Value"].exists()) {
            minMode = Fixed;
            boundMin = configuration["Minimum/Value"].toDouble();
            realMin = boundMin;
        } else if (Util::equal_insensitive(type, "extendabsolute")) {
            minMode = ExtendAbsolute;
            if (configuration["Minimum/Amount"].exists())
                boundMin = configuration["Minimum/Amount"].toDouble();
            else
                boundMin = 0.0;
        } else if (Util::equal_insensitive(type, "powerround")) {
            minMode = ExtendPower;
            if (configuration["Minimum/Base"].exists()) {
                boundMin = configuration["Minimum/Base"].toDouble();
                if (boundMin <= 1.0)
                    boundMin = 10.0;
            } else {
                boundMin = 10.0;
            }
        }
        if (configuration["Minimum/Absolute"].exists())
            absoluteMin = configuration["Minimum/Absolute"].toDouble();
        if (configuration["Minimum/Required"].exists())
            requiredMin = configuration["Minimum/Required"].toDouble();
    }

    {
        const auto &type = configuration["Maximum/Type"].toString();
        if (Util::equal_insensitive(type, "extendfraction")) {
            maxMode = ExtendFraction;
            if (configuration["Maximum/Fraction"].exists())
                boundMax = configuration["Maximum/Fraction"].toDouble();
            if (configuration["Maximum/AtLeast"].exists())
                boundMaxFractionAtLeast = configuration["Maximum/AtLeast"].toDouble();
            if (configuration["Maximum/AtMost"].exists())
                boundMaxFractionAtMost = configuration["Maximum/AtMost"].toDouble();
        } else if (Util::equal_insensitive(type, "fixed") &&
                configuration["Maximum/Value"].exists()) {
            maxMode = Fixed;
            boundMax = configuration["Maximum/Value"].toDouble();
            realMax = boundMax;
        } else if (Util::equal_insensitive(type, "extendabsolute")) {
            maxMode = ExtendAbsolute;
            if (configuration["Maximum/Amount"].exists())
                boundMax = configuration["Maximum/Amount"].toDouble();
            else
                boundMax = 0.0;
        } else if (Util::equal_insensitive(type, "powerround")) {
            maxMode = ExtendPower;
            if (configuration["Maximum/Base"].exists()) {
                boundMax = configuration["Maximum/Base"].toDouble();
                if (boundMax <= 1.0)
                    boundMax = 10.0;
            } else {
                boundMax = 10.0;
            }
        }
        if (configuration["Maximum/Absolute"].exists())
            absoluteMax = configuration["Maximum/Absolute"].toDouble();
        if (configuration["Maximum/Required"].exists())
            requiredMax = configuration["Maximum/Required"].toDouble();
    }
}

/**
 * Save the transformer to a configuration value.
 * 
 * @return a configuration value
 */
Variant::Root AxisTransformer::toConfiguration() const
{
    Variant::Root configuration;

    configuration["Log"].setBool(isLog);
    configuration["Reverse"].setBool(reverse);
    if (FP::defined(absoluteMin))
        configuration["Minimum/Absolute"].setDouble(absoluteMin);
    if (FP::defined(absoluteMax))
        configuration["Maximum/Absolute"].setDouble(absoluteMax);
    if (FP::defined(requiredMin))
        configuration["Minimum/Required"].setDouble(requiredMin);
    if (FP::defined(requiredMax))
        configuration["Maximum/Required"].setDouble(requiredMax);

    switch (minMode) {
    case Fixed:
        configuration["Minimum/Type"].setString("Fixed");
        configuration["Minimum/Value"].setDouble(boundMin);
        break;
    case ExtendFraction:
        configuration["Minimum/Type"].setString("ExtendFraction");
        configuration["Minimum/Fraction"].setDouble(boundMin);
        if (FP::defined(boundMinFractionAtLeast))
            configuration["Minimum/AtLeast"].setDouble(boundMinFractionAtLeast);
        if (FP::defined(boundMinFractionAtMost))
            configuration["Minimum/AtMost"].setDouble(boundMinFractionAtMost);
        break;
    case ExtendAbsolute:
        configuration["Minimum/Type"].setString("ExtendAbsolute");
        configuration["Minimum/Amount"].setDouble(boundMin);
        break;
    case ExtendPower:
        configuration["Minimum/Type"].setString("PowerRound");
        configuration["Minimum/Base"].setDouble(boundMin);
        break;
    }

    switch (maxMode) {
    case Fixed:
        configuration["Maximum/Type"].setString("Fixed");
        configuration["Maximum/Value"].setDouble(boundMax);
        break;
    case ExtendFraction:
        configuration["Maximum/Type"].setString("ExtendFraction");
        configuration["Maximum/Fraction"].setDouble(boundMax);
        if (FP::defined(boundMaxFractionAtLeast))
            configuration["Maximum/AtLeast"].setDouble(boundMaxFractionAtLeast);
        if (FP::defined(boundMaxFractionAtMost))
            configuration["Maximum/AtMost"].setDouble(boundMaxFractionAtMost);
        break;
    case ExtendAbsolute:
        configuration["Maximum/Type"].setString("ExtendAbsolute");
        configuration["Maximum/Amount"].setDouble(boundMax);
        break;
    case ExtendPower:
        configuration["Maximum/Type"].setString("PowerRound");
        configuration["Maximum/Base"].setDouble(boundMax);
        break;
    }

    return configuration;
}

/**
 * Set the axis real value limits.  This also updates the min and max with and
 * adjustments that needed to be made.  Any screen coordinates are invalidated
 * by this, and need to be set again with setScreen(double, double).
 * 
 * @param min       the axis minimum (input and output)
 * @param min       the axis maximum (input and output)
 * @param zoomMin   the zoomed axis minimum or FP::undefined() for none
 * @param zoomMax   the zoomed axis maximum or FP::undefined() for none
 */
void AxisTransformer::setReal(double &min, double &max, double zoomMin, double zoomMax)
{
    if (isLog && FP::defined(min) && min < 0.0) {
        if (FP::defined(max) && max > 0.0)
            min = max * 1E-5;
        else
            min = 1E-6;
    }
    if (isLog && FP::defined(max) && max < 0.0) {
        if (FP::defined(min) && min > 0.0)
            max = min * 1E5;
        else
            max = 1E-6;
    }

    if (minMode == Fixed) {
        min = boundMin;
    } else if (minMode == ExtendAbsolute && FP::defined(min)) {
        min -= boundMin;
    } else if (minMode == ExtendPower && FP::defined(min)) {
        if (min <= 0.0) {
            if (FP::defined(max) && max > 0.0)
                min = max * pow(boundMin, -3);
            else
                min = pow(boundMin, -3);
        }
        min = pow(boundMin, floor(log(min) / log(boundMin)));
    }
    if (maxMode == Fixed) {
        max = boundMax;
    } else if (maxMode == ExtendAbsolute && FP::defined(max)) {
        max += boundMax;
    } else if (maxMode == ExtendPower && FP::defined(max)) {
        if (max <= 0.0) {
            if (FP::defined(min) && min > 0.0)
                max = min * pow(boundMax, 3);
            else
                max = pow(boundMax, 3);
        }
        max = pow(boundMax, ceil(log(max) / log(boundMax)));
    }

    if (FP::defined(zoomMin))
        min = zoomMin;
    if (FP::defined(zoomMax))
        max = zoomMax;

    if (FP::defined(min) &&
            FP::defined(max) &&
            (minMode == ExtendFraction || maxMode == ExtendFraction)) {

        if (FP::defined(absoluteMin) &&
                !FP::defined(zoomMin) &&
                FP::defined(min) &&
                min < absoluteMin)
            min = absoluteMin;
        if (FP::defined(absoluteMax) &&
                !FP::defined(zoomMax) &&
                FP::defined(max) &&
                max > absoluteMax)
            max = absoluteMax;

        double distance = max - min;
        double zeroCheck = fabs(max);
        double zeroTemp = fabs(min);
        if (zeroCheck == 0.0 || (zeroTemp != 0.0 && zeroTemp < zeroCheck))
            zeroCheck = zeroTemp;
        if (distance == 0.0 || distance < zeroCheck * 1E-6) {
            if (zeroCheck == 0.0)
                distance = 0.01;
            else
                distance = zeroCheck * 0.01;
        }

        if (minMode == ExtendFraction && !FP::defined(zoomMin)) {
            double extend;
            if (!isLog || min <= 0.0)
                extend = distance * boundMin;
            else
                extend = min * boundMin;

            if (FP::defined(boundMinFractionAtLeast) && extend < boundMinFractionAtLeast)
                extend = boundMinFractionAtLeast;
            if (FP::defined(boundMinFractionAtMost) && extend > boundMinFractionAtMost)
                extend = boundMinFractionAtMost;
            min -= extend;
        }
        if (maxMode == ExtendFraction && !FP::defined(zoomMax)) {
            double extend;
            if (!isLog || max <= 0.0)
                extend = distance * boundMax;
            else
                extend = max * boundMax;

            if (FP::defined(boundMaxFractionAtLeast) && extend < boundMaxFractionAtLeast)
                extend = boundMaxFractionAtLeast;
            if (FP::defined(boundMaxFractionAtMost) && extend > boundMaxFractionAtMost)
                extend = boundMaxFractionAtMost;
            max += extend;
        }
    }

    if (FP::defined(requiredMin) && !FP::defined(zoomMin) && FP::defined(min) && min > requiredMin)
        min = requiredMin;
    if (FP::defined(requiredMax) && !FP::defined(zoomMax) && FP::defined(max) && max < requiredMax)
        max = requiredMax;
    if (FP::defined(absoluteMin) && !FP::defined(zoomMin) && FP::defined(min) && min < absoluteMin)
        min = absoluteMin;
    if (FP::defined(absoluteMax) && !FP::defined(zoomMax) && FP::defined(max) && max > absoluteMax)
        max = absoluteMax;

    if (isLog && FP::defined(min) && min <= 0.0) {
        if (FP::defined(max) && max > 0.0)
            min = max * 1E-5;
        else
            min = 1E-6;
    }
    if (isLog && FP::defined(max) && max <= 0.0) {
        if (FP::defined(min) && min > 0.0)
            max = min * 1E5;
        else
            max = 1E-6;
    }

    if (FP::defined(min) && FP::defined(max) && min >= max) {
        min = FP::undefined();
        max = FP::undefined();
    }
    realMin = min;
    realMax = max;
    scale = FP::undefined();
}

/** 
 * Same as setReal( double &, double &, double, double ) except that the
 * bounds are not output.
 * 
 * @param min       the axis minimum
 * @param min       the axis maximum
 * @param zoomMin   the zoomed axis minimum or FP::undefined() for none
 * @param zoomMax   the zoomed axis maximum or FP::undefined() for none
 * @see setReal( double &, double &, double, double )
 */
void AxisTransformer::setReal(const double &min, const double &max, double zoomMin, double zoomMax)
{
    double tmin = min;
    double tmax = max;
    setReal(tmin, tmax, zoomMin, zoomMax);
}

/**
 * Set the screen dimensions.  This must be called after setReal(double &,
 * double &, double, double).
 * 
 * @param min   the screen coordinate of the minimum of the axis
 * @param max   the screen coordinate of the maximum of the axis
 */
void AxisTransformer::setScreen(double min, double max)
{
    Q_ASSERT(FP::defined(min) && FP::defined(max));
    screenMin = min;
    screenMax = max;

    if (!FP::defined(realMin) || !FP::defined(realMax) || realMin == realMax) {
        scale = FP::undefined();
        return;
    }

    if (!isLog || realMin <= 0.0 || realMax <= 0.0) {
        if (reverse) {
            realOffset = -realMax;
            scale = (max - min) / (realMin - realMax);
        } else {
            realOffset = -realMin;
            scale = (max - min) / (realMax - realMin);
        }
    } else {
        double lmn = log(realMin);
        double lmx = log(realMax);
        if (reverse) {
            scale = (max - min) / (lmn - lmx);
            realOffset = -lmx;
        } else {
            scale = (max - min) / (lmx - lmn);
            realOffset = -lmn;
        }
    }
}

/**
 * Transform a point from real space into screen space.
 * 
 * @param value     the real position to transform
 * @param clip      if set then the output is clipped to screen space bounds
 * @return          a screen space position
 */
double AxisTransformer::toScreen(double value, bool clip) const
{
    if (!FP::defined(value) || !FP::defined(scale))
        return FP::undefined();

    if (isLog) {
        if (value <= 0.0) {
            if (clip)
                return screenMin;
            return FP::undefined();
        }
        value = log(value);
    }
    value = (value + realOffset) * scale + screenMin;
    if (clip) {
        if (screenMin < screenMax) {
            if (value < screenMin)
                return screenMin;
            if (value > screenMax)
                return screenMax;
        } else {
            if (value > screenMin)
                return screenMin;
            if (value < screenMax)
                return screenMax;
        }
    }
    return value;
}

/**
 * Test if a point was clipped to the screen.
 * 
 * @param value     the screen value
 * @return          true if the point is outside the screen space
 */
bool AxisTransformer::screenClipped(double value) const
{
    Q_ASSERT(FP::defined(value));
    Q_ASSERT(FP::defined(screenMin) && FP::defined(screenMax));
    if (screenMin < screenMax) {
        if (value < screenMin)
            return true;
        if (value > screenMax)
            return true;
    } else {
        if (value > screenMin)
            return true;
        if (value < screenMax)
            return true;
    }
    return false;
}

/**
 * Get the minimum screen value.
 * 
 * @return the minimum screen value
 */
double AxisTransformer::getScreenMin() const
{ return screenMin; }

/**
 * Get the maximum screen value.
 * 
 * @return the maximum screen value
 */
double AxisTransformer::getScreenMax() const
{ return screenMax; }

/**
 * Get the minimum real value.
 * 
 * @return the minimum real value
 */
double AxisTransformer::getRealMin() const
{ return realMin; }

/**
 * Get the maximum real value.
 * 
 * @return the maximum real value
 */
double AxisTransformer::getRealMax() const
{ return realMax; }

/**
 * Transform a point from screen space into real space.
 * 
 * @param value     the screen position to transform
 * @param clip      if set then the output is clipped to real space bounds
 * @return          a real space position
 */
double AxisTransformer::toReal(double value, bool clip) const
{
    if (!FP::defined(value) || !FP::defined(scale) || scale == 0.0)
        return FP::undefined();

    value = (value - screenMin) / scale - realOffset;
    if (isLog)
        value = exp(value);
    if (clip) {
        if (value < realMin)
            return realMin;
        if (value > realMax)
            return realMax;
    }
    return value;
}

/**
 * Test if the transformer is logarithmic.
 * 
 * @return  true if the transform is logarithmic
 */
bool AxisTransformer::isLogarithmic() const
{ return isLog; }

/**
 * Set the transformer to be logarithmic or linear.  Logarithmic transformers
 * select their base from the range of the axis.
 * 
 * @param l true for logarithmic
 */
void AxisTransformer::setLogarithmic(bool l)
{ isLog = l; }

/**
 * Test if the transformer is logarithmic.
 *
 * @return  true if the transform is logarithmic
 */
bool AxisTransformer::isReversed() const
{ return reverse; }

/**
 * Set the transformer to be reversed or normal.  Reversed transformers invert
 * the direction the axis shows points.
 *
 * @param reversed  true for reversed
 */
void AxisTransformer::setReversed(bool reversed)
{ this->reverse = reversed; }


/**
 * Set the axis to a fixed minimum value.
 * 
 * @param value the minimum value
 */
void AxisTransformer::setMinFixed(double value)
{
    minMode = Fixed;
    boundMin = value;
}

/**
 * Set the axis to extend the minimum value by a fraction of the total range.
 * 
 * @param fraction  the fraction of the range to extend by
 * @param atLeast   if set then the extension is always at least this
 * @param atMost    if set then the extension is never more than this
 */
void AxisTransformer::setMinExtendFraction(double fraction, double atLeast, double atMost)
{
    Q_ASSERT(FP::defined(fraction));
    minMode = ExtendFraction;
    boundMin = fraction;
    boundMinFractionAtLeast = atLeast;
    boundMinFractionAtMost = atMost;
}

/**
 * Set the axis to extend the minimum value by a fixed absolute amount.
 * 
 * @param amount    the amount to extend the minimum by
 */
void AxisTransformer::setMinExtendAbsolute(double amount)
{
    Q_ASSERT(FP::defined(amount));
    minMode = ExtendAbsolute;
    boundMin = amount;
}

/**
 * Set the axis to extend the minimum by rounding to a power.  For example
 * setting this to 10 would cause 0.5 to be rounded to 0.1.
 * 
 * @param base  the base to use
 */
void AxisTransformer::setMinExtendPowerRound(double base)
{
    Q_ASSERT(FP::defined(base) && base > 1.0);
    minMode = ExtendPower;
    boundMin = base;
}

/**
 * Set the absolute minimum the axis is allowed to take.  This clips extensions
 * but not zooms.
 * 
 * @param min   the minimum value
 */
void AxisTransformer::setAbsoluteMin(double min)
{
    absoluteMin = min;
}

/**
 * Set the required minimum the axis must always show.  This forces non-zoomed
 * axis to always have a minimum of at least the specified value
 *
 * @param min   the minimum value
 */
void AxisTransformer::setRequiredMin(double min)
{
    requiredMin = min;
}


/**
 * Set the axis to a fixed maximum value.
 * 
 * @param value the maximum value
 */
void AxisTransformer::setMaxFixed(double value)
{
    maxMode = Fixed;
    boundMax = value;
}

/**
 * Set the axis to extend the maximum value by a fraction of the total range.
 * 
 * @param fraction  the fraction of the range to extend by
 * @param atLeast   if set then the extension is always at least this
 * @param atMost    if set then the extension is never more than this
 */
void AxisTransformer::setMaxExtendFraction(double fraction, double atLeast, double atMost)
{
    Q_ASSERT(FP::defined(fraction));
    maxMode = ExtendFraction;
    boundMax = fraction;
    boundMaxFractionAtLeast = atLeast;
    boundMaxFractionAtMost = atMost;
}

/**
 * Set the axis to extend the maximum value by a fixed absolute amount.
 * 
 * @param amount    the amount to extend the maximum by
 */
void AxisTransformer::setMaxExtendAbsolute(double amount)
{
    Q_ASSERT(FP::defined(amount));
    maxMode = ExtendAbsolute;
    boundMax = amount;
}

/**
 * Set the axis to extend the maximum by rounding to a power.  For example
 * setting this to 10 would cause 5 to be rounded to 10.
 * 
 * @param base  the base to use
 */
void AxisTransformer::setMaxExtendPowerRound(double base)
{
    Q_ASSERT(FP::defined(base) && base > 1.0);
    maxMode = ExtendPower;
    boundMax = base;
}

/**
 * Set the absolute maximum the axis is allowed to take.  This clips extensions
 * but not zooms.
 * 
 * @param max   the maximum value
 */
void AxisTransformer::setAbsoluteMax(double max)
{
    absoluteMax = max;
}

/**
 * Set the required maximum the axis must always show.  This forces non-zoomed
 * axis to always have a maximum of at least the specified value
 *
 * @param max   the maximum value
 */
void AxisTransformer::setRequiredMax(double max)
{
    requiredMax = max;
}

}
}
