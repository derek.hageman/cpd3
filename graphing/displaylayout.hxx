/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGDISPLAYLAYOUT_H
#define CPD3GRAPHINGDISPLAYLAYOUT_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QObject>

#include "graphing/display.hxx"

namespace CPD3 {
namespace Graphing {

class DisplayWidget;
namespace Internal {
class DisplayWidgetPrivate;

class DisplayWidgetPrivateLayout;
}

/**
 * A layout of nested displays.
 */
class CPD3GRAPHING_EXPORT DisplayLayout : public Display {
Q_OBJECT

    struct Element {
        int x;
        int y;
        int width;
        int height;
        QSizeF minimumSize;
        QSizeF maximumSize;
        std::unique_ptr<Display> display;

        QString name;
        std::vector<int> rowStretch;
        std::vector<int> columnStretch;

        std::vector<std::shared_ptr<DisplayCoupling> > dataCoupling;
        std::vector<std::shared_ptr<DisplayCoupling> > layoutCoupling;

        QRectF position;

        Element();

        ~Element();

        Element(const Element &other);
    };

    std::vector<std::unique_ptr<Element>> elements;
    std::vector<std::vector<Element *>> rows;
    std::vector<std::vector<Element *>> columns;
    std::vector<int> rowStretch;
    std::vector<int> columnStretch;

    bool havePrepared;
    bool havePredicted;
    bool haveEverPrepared;
    bool haveEverPredicted;
    Element *priorMouseover;
    std::size_t elementsReady;

    void setBreakdowns();

    void setPositions(const QRectF &dimensions);

    friend class DisplayWidget;

    friend class Internal::DisplayWidgetPrivate;

    friend class Internal::DisplayWidgetPrivateLayout;

    friend bool displayElementRowSort(const Element *a, const Element *b);

    friend bool displayElementColumnSort(const Element *a, const Element *b);

public:
    virtual ~DisplayLayout();

    DisplayLayout(const DisplayDefaults &defaults = DisplayDefaults(), QObject *parent = 0);

    DisplayLayout(const DisplayLayout &other);

    std::unique_ptr<Display> clone() const override;

    virtual void initialize(const Data::ValueSegment::Transfer &config,
                            const DisplayDefaults &defaults = {});

    /**
     * Test if two values representing configurations for the displays
     * could be merged together into a single display.
     * 
     * @param a the first configuration
     * @param b the second configuration
     */
    static bool mergable(const Data::Variant::Read &a, const Data::Variant::Read &b);

    void registerChain(Data::ProcessingTapChain *chain) override;

    std::vector<std::shared_ptr<
            DisplayOutput>> getOutputs(const DisplayDynamicContext &context = {}) override;

    QSizeF getMinimumSize(QPaintDevice *paintdevice = nullptr,
                          const DisplayDynamicContext &context = {}) override;

    void prepareLayout(const QRectF &maximumDimensions,
                       QPaintDevice *paintdevice = nullptr,
                       const DisplayDynamicContext &context = {}) override;

    void predictLayout(const QRectF &maximumDimensions,
                       QPaintDevice *paintdevice = nullptr,
                       const DisplayDynamicContext &context = {}) override;

    void paint(QPainter *painter,
               const QRectF &dimensions,
               const DisplayDynamicContext &context = {}) override;

    bool eventMousePress(QMouseEvent *event) override;

    bool eventMouseRelease(QMouseEvent *event) override;

    bool eventMouseMove(QMouseEvent *event) override;

    bool eventKeyPress(QKeyEvent *event) override;

    bool eventKeyRelease(QKeyEvent *event) override;

    std::shared_ptr<DisplayModificationComponent> getMouseModification(const QPoint &point,
                                                                       const QRectF &dimensions = {},
                                                                       const DisplayDynamicContext &context = {}) override;

    void setMouseover(const QPoint &point) override;

    std::vector<std::shared_ptr<DisplayZoomAxis>> zoomAxes() override;

    std::shared_ptr<DisplayHighlightController> highlightController() override;

    void saveOverrides(DisplaySaveContext &target) override;

    bool acceptsKeyboardInput() const override;

    bool acceptsMouseover() const override;

public slots:

    void resetOverrides() override;

    void trimData(double start, double end) override;

    void setVisibleTimeRange(double start, double end) override;

    void interactionStopped() override;

    void clearMouseover() override;

private slots:

    void elementReadyForFinalPaint();
};

}
}


#endif
