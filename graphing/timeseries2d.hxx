/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGTIMESERIES2D_H
#define CPD3GRAPHINGTIMESERIES2D_H

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QList>

#include "graphing/graphing.hxx"
#include "graphing/graph2d.hxx"
#include "core/timeutils.hxx"


namespace CPD3 {
namespace Graphing {

/**
 * The parameters for a 2D time series graph.
 */
class CPD3GRAPHING_EXPORT TimeSeries2DParameters : public Graph2DParameters {
    enum {
        Component_NeedleDrawPriority = 0x0001,
    };
    int components;
    int needleDrawPriority;
public:
    TimeSeries2DParameters();

    virtual ~TimeSeries2DParameters();

    TimeSeries2DParameters(const TimeSeries2DParameters &other);

    TimeSeries2DParameters &operator=(const TimeSeries2DParameters &other);

    TimeSeries2DParameters(const Data::Variant::Read &value, const DisplayDefaults &defaults = {});

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<Graph2DParameters> clone() const override;

    void overlay(const Graph2DParameters *over) override;

    void initializeOverlay(const Graph2DParameters *over) override;

    void copy(const Graph2DParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify a needle draw priority.
     * @return true if the parameters specify a needle draw priority
     */
    inline bool hasNeedleDrawPriority() const
    { return components & Component_NeedleDrawPriority; }

    /**
     * Get the needle drawing priority.
     * @return the needle drawing priority
     */
    inline int getNeedleDrawPriority() const
    { return needleDrawPriority; }
};

/**
 * The parameters for a 2D time series trace
 */
class CPD3GRAPHING_EXPORT TimeSeriesTraceParameters2D : public TraceParameters2D {
public:
    enum PointOrigin {
        Origin_Start, Origin_End, Origin_Center, Origin_Both,
    };
private:
    enum {
        Component_Origin = 0x0001,
    };
    int components;
    PointOrigin origin;
public:
    TimeSeriesTraceParameters2D();

    virtual ~TimeSeriesTraceParameters2D();

    TimeSeriesTraceParameters2D(const TimeSeriesTraceParameters2D &other);

    TimeSeriesTraceParameters2D &operator=(const TimeSeriesTraceParameters2D &other);

    TimeSeriesTraceParameters2D(const Data::Variant::Read &value);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<TraceParameters> clone() const override;

    void overlay(const TraceParameters *over) override;

    void copy(const TraceParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify the point origin.
     * @return true if the parameters specify the point origin
     */
    inline bool hasOrigin() const
    { return components & Component_Origin; }

    /**
     * Get the point origin.
     * @return the point origin
     */
    inline PointOrigin getOrigin() const
    { return origin; };
};

/**
 * The base class for a time series trace.
 */
class CPD3GRAPHING_EXPORT TimeSeriesTraceHandler2D : public TraceHandler2D {
    TimeSeriesTraceParameters2D *parameters;

    double getFitInterval() const;

public:
    TimeSeriesTraceHandler2D(std::unique_ptr<TimeSeriesTraceParameters2D> &&params);

    virtual ~TimeSeriesTraceHandler2D();

    using Cloned = typename TraceHandler2D::Cloned;

    Cloned clone() const override;

protected:
    TimeSeriesTraceHandler2D(const TimeSeriesTraceHandler2D &other,
                             std::unique_ptr<TraceParameters> &&params);

    double getFitScale() const override;

    QString getFitLegend(const std::shared_ptr<Algorithms::Model> &fit) const override;

    void convertValue(const TraceDispatch::OutputValue &value,
                      std::vector<TracePoint<3>> &points) override;

    QVector<Algorithms::TransformerPoint *> prepareTransformer(const std::vector<
            TraceDispatch::OutputValue> &points,
                                                               Algorithms::TransformerParameters &parameters) override;

    std::vector<TracePoint<3>> convertTransformerResult(const QVector<
            Algorithms::TransformerPoint *> &points) override;

    Algorithms::TransformerAllocator *getTransformerAllocator() override;
};

/**
 * The base class for two dimensional time series trace sets.
 */
class CPD3GRAPHING_EXPORT TimeSeriesTraceSet2D : public TraceSet2D {
public:
    TimeSeriesTraceSet2D();

    virtual ~TimeSeriesTraceSet2D();

    using Cloned = typename TraceSet2D::Cloned;

    Cloned clone() const override;

protected:
    TimeSeriesTraceSet2D(const TimeSeriesTraceSet2D &other);

    std::unique_ptr<
            TraceParameters> parseParameters(const Data::Variant::Read &value) const override;

    using CreatedHandler = TraceSet2D::CreatedHandler;

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override;
};

/**
 * The parameters for a 2D time needle trace realtime display.
 */
class CPD3GRAPHING_EXPORT TimeSeriesNeedleParameters2D : public TimeSeriesTraceParameters2D {
    enum {
        Component_NeedleEnable = 0x0001,
        Component_NeedleFont = 0x0002,
        Component_NeedleLabelColor = 0x0004,
        Component_NeedleLabelFormat = 0x0008,
    };
    int components;
    bool needleEnable;
    QFont needleFont;
    QColor needleLabelColor;
    QString needleLabelFormat;
public:
    TimeSeriesNeedleParameters2D();

    virtual ~TimeSeriesNeedleParameters2D();

    TimeSeriesNeedleParameters2D(const TimeSeriesNeedleParameters2D &other);

    TimeSeriesNeedleParameters2D &operator=(const TimeSeriesNeedleParameters2D &other);

    TimeSeriesNeedleParameters2D(const Data::Variant::Read &value);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<TraceParameters> clone() const override;

    void overlay(const TraceParameters *over) override;

    void copy(const TraceParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify a needle enable state.
     * @return true if a needle font is set
     */
    inline bool hasNeedleEnable() const
    { return components & Component_NeedleEnable; }

    /**
     * Get the needle enable state.
     * @return true if needles should be drawn
     */
    inline bool getNeedleEnable() const
    { return needleEnable; }

    /**
     * Test if the parameters specify a needle font.
     * @return true if a needle font is set
     */
    inline bool hasNeedleFont() const
    { return components & Component_NeedleFont; }

    /**
     * Get the needle label font.
     * @return the needle font
     */
    inline QFont getNeedleFont() const
    { return needleFont; }

    /**
     * Test if the parameters specify a needle label color.
     * @return true if a needle color is set
     */
    inline bool hasNeedleLabelColor() const
    { return components & Component_NeedleLabelColor; }

    /**
     * Get the needle label font.
     * @return the needle font
     */
    inline QColor getNeedleLabelColor() const
    { return needleLabelColor; }

    /**
     * Test if the parameters specify a needle label format.
     * @return true if the parameters specify a needle label format.
     */
    inline bool hasNeedleLabelFormat() const
    { return components & Component_NeedleLabelFormat; }

    /**
     * Get the needle label format.
     * @return the needle label format
     */
    inline QString getNeedleLabelFormat() const
    { return needleLabelFormat; }
};

/**
 * The base class for a time series realtime needle.
 */
class CPD3GRAPHING_EXPORT TimeSeriesNeedleHandler2D : public TimeSeriesTraceHandler2D {
    TimeSeriesNeedleParameters2D *parameters;

public:
    TimeSeriesNeedleHandler2D(std::unique_ptr<TimeSeriesNeedleParameters2D> &&params);

    virtual ~TimeSeriesNeedleHandler2D();

    using Cloned = TimeSeriesTraceHandler2D::Cloned;

    Cloned clone() const override;

    void getColors(const std::vector<Data::SequenceName::Set> &latestUnits,
                   const std::vector<Data::SequenceName::Set> &inputUnits,
                   std::vector<TraceUtilities::ColorRequest> &requestColors,
                   std::deque<QColor> &claimedColors,
                   TraceSet2D *traces) const;

    void claimColors(std::deque<QColor> &colors,
                     const std::vector<Data::SequenceName::Set> &latestUnits,
                     const std::vector<Data::SequenceName::Set> &inputUnits,
                     TraceSet2D *traces) const;

protected:
    TimeSeriesNeedleHandler2D(const TimeSeriesNeedleHandler2D &other,
                              std::unique_ptr<TraceParameters> &&params);
};

/**
 * The base class for two dimensional time series realtime needles.
 */
class CPD3GRAPHING_EXPORT TimeSeriesNeedleSet2D : public TimeSeriesTraceSet2D {
public:
    TimeSeriesNeedleSet2D();

    virtual ~TimeSeriesNeedleSet2D();

    void getColors(std::vector<TraceUtilities::ColorRequest> &requestColors,
                   std::deque<QColor> &claimedColors,
                   TraceSet2D *traces) const;

    void claimColors(std::deque<QColor> &colors, TraceSet2D *traces) const;

    bool addLegend(Legend &legend, const DisplayDynamicContext &context, TraceSet2D *traces) const;

    using Cloned = TimeSeriesTraceSet2D::Cloned;

    Cloned clone() const override;

protected:
    TimeSeriesNeedleSet2D(const TimeSeriesNeedleSet2D &other);

    std::unique_ptr<
            TraceParameters> parseParameters(const Data::Variant::Read &value) const override;

    using CreatedHandler = TimeSeriesTraceSet2D::CreatedHandler;

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override;
};


/**
 * The parameters for bins on a time series graph.
 */
class CPD3GRAPHING_EXPORT TimeSeriesBinParameters2D : public BinParameters2D {
public:
    /**
     * The type of binning used.
     */
    enum BinningType {
        /**
         * A fixed number of total bins, uniformly distributed on the total
         * range. */
        Binning_FixedUniform, /**
         * A fixed bin size, specified in logical time units.
         */
        Binning_FixedSize
    };
private:
    enum {
        Component_Binning = 0x0001,
    };
    int components;

    BinningType binningType;
    int binningCount;
    Time::LogicalTimeUnit binningUnits;
    bool binningAlign;
public:
    TimeSeriesBinParameters2D();

    virtual ~TimeSeriesBinParameters2D();

    TimeSeriesBinParameters2D(const TimeSeriesBinParameters2D &other);

    TimeSeriesBinParameters2D &operator=(const TimeSeriesBinParameters2D &other);

    TimeSeriesBinParameters2D(const Data::Variant::Read &value);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<TraceParameters> clone() const override;

    void overlay(const TraceParameters *over) override;

    void copy(const TraceParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify binning.
     * @return true if the parameters specify binning
     */
    inline bool hasBinning() const
    { return components & Component_Binning; }

    /**
     * Get the binning type.
     * @return      the binning type
     */
    inline BinningType getBinningType() const
    { return binningType; }

    /**
     * Get the binning count.
     * @return      the number of bins
     */
    inline int getBinningCount() const
    { return binningCount; }

    /**
     * Get the binning units.
     * @return      the time units to bin on
     */
    inline Time::LogicalTimeUnit getBinningUnits() const
    { return binningUnits; }

    /**
     * Get the binning alignment state.
     * @return      true to bin to aligned times
     */
    inline bool getBinningAlign() const
    { return binningAlign; }

};

/**
 * The base class for two dimensional time series bins
 */
class CPD3GRAPHING_EXPORT TimeSeriesBinHandler2D : public BinHandler2D {
    TimeSeriesBinParameters2D *parameters;
public:
    TimeSeriesBinHandler2D(std::unique_ptr<TimeSeriesBinParameters2D> &&params);

    virtual ~TimeSeriesBinHandler2D();

    using Cloned = BinHandler2D::Cloned;

    Cloned clone() const override;

protected:
    TimeSeriesBinHandler2D(const TimeSeriesBinHandler2D &other,
                           std::unique_ptr<TraceParameters> &&params);

    void convertValue(const TraceDispatch::OutputValue &value,
                      std::vector<TracePoint<2>> &points) override;

    QVector<Algorithms::TransformerPoint *> prepareTransformer(const std::vector<
            TraceDispatch::OutputValue> &points,
                                                               Algorithms::TransformerParameters &parameters) override;

    std::vector<TracePoint<2> > convertTransformerResult(const QVector<
            Algorithms::TransformerPoint *> &points) override;

    Algorithms::TransformerAllocator *getTransformerAllocator() override;

    void buildBins(std::size_t dim,
                   std::vector<double> &starts,
                   std::vector<double> &ends,
                   std::vector<double> &centers) override;

    std::vector<TracePoint<2>> applyFilter(std::vector<TracePoint<2>> input,
                                           double start,
                                           double end,
                                           std::size_t dim) override;
};

/**
 * The base class for two dimensional time series box whisker plot sets.
 */
class CPD3GRAPHING_EXPORT TimeSeriesBinSet2D : public BinSet2D {
public:
    TimeSeriesBinSet2D();

    virtual ~TimeSeriesBinSet2D();

    using Cloned = BinSet2D::Cloned;

    Cloned clone() const override;

protected:
    TimeSeriesBinSet2D(const TimeSeriesBinSet2D &other);

    std::unique_ptr<
            TraceParameters> parseParameters(const Data::Variant::Read &value) const override;

    using CreatedHandler = BinSet2D::CreatedHandler;

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override;
};

/**
 * Parameters for an indicator on a two dimensional time series graph.
 */
class CPD3GRAPHING_EXPORT TimeSeriesIndicatorParameters2D : public IndicatorParameters2D {
public:
    enum PointOrigin {
        Origin_Start, Origin_End, Origin_Center
    };
private:
    enum {
        Component_Origin = 0x0001,
    };
    int components;
    PointOrigin origin;
public:
    TimeSeriesIndicatorParameters2D();

    virtual ~TimeSeriesIndicatorParameters2D();

    TimeSeriesIndicatorParameters2D(const TimeSeriesIndicatorParameters2D &other);

    TimeSeriesIndicatorParameters2D &operator=(const TimeSeriesIndicatorParameters2D &other);

    TimeSeriesIndicatorParameters2D(const Data::Variant::Read &value);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<TraceParameters> clone() const override;

    void overlay(const TraceParameters *over) override;

    void copy(const TraceParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify the point origin.
     * @return true if the parameters specify the point origin
     */
    inline bool hasOrigin() const
    { return components & Component_Origin; }

    /**
     * Get the point origin.
     * @return the point origin
     */
    inline PointOrigin getOrigin() const
    { return origin; };
};

/**
 * A handler for a single indicator on a two dimensional time series graph.
 */
class CPD3GRAPHING_EXPORT TimeSeriesIndicatorHandler2D : public IndicatorHandler2D {
    TimeSeriesIndicatorParameters2D *parameters;
public:
    TimeSeriesIndicatorHandler2D(std::unique_ptr<TimeSeriesIndicatorParameters2D> &&params);

    virtual ~TimeSeriesIndicatorHandler2D();

    using Cloned = IndicatorHandler2D::Cloned;

    Cloned clone() const override;

protected:
    void convertIndicatorValue(const TraceDispatch::OutputValue &value,
                               IndicatorPoint<1> &add) override;

    TimeSeriesIndicatorHandler2D(const TimeSeriesIndicatorHandler2D &other,
                                 std::unique_ptr<TraceParameters> &&params);
};

/**
 * A handler for a set of indicators on a two dimensional time series graph.
 */
class CPD3GRAPHING_EXPORT TimeSeriesIndicatorSet2D : public IndicatorSet2D {
public:
    TimeSeriesIndicatorSet2D();

    virtual ~TimeSeriesIndicatorSet2D();

    using Cloned = IndicatorSet2D::Cloned;

    Cloned clone() const override;

protected:
    TimeSeriesIndicatorSet2D(const TimeSeriesIndicatorSet2D &other);

    std::unique_ptr<
            TraceParameters> parseParameters(const Data::Variant::Read &value) const override;

    using CreatedHandler = IndicatorSet2D::CreatedHandler;

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override;
};

/**
 * The common parameters for the time axis on a time series graph.
 */
class CPD3GRAPHING_EXPORT AxisParametersTimeSeries2D : public AxisParameters2DSide {
    enum {
        Component_TimeFormat = 0x0001,
    };
    int components;
    QString timeFormat;
public:
    AxisParametersTimeSeries2D();

    virtual ~AxisParametersTimeSeries2D();

    AxisParametersTimeSeries2D(const AxisParametersTimeSeries2D &other);

    AxisParametersTimeSeries2D &operator=(const AxisParametersTimeSeries2D &other);

    AxisParametersTimeSeries2D(const Data::Variant::Read &value, QSettings *settings = 0);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<AxisParameters> clone() const override;

    void overlay(const AxisParameters *over) override;

    void copy(const AxisParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify a time format override.
     * @return true if the parameters specify a time format override
     */
    inline bool hasTimeFormat() const
    { return components & Component_TimeFormat; }

    /**
     * Get the time format override.
     * @return the time format override string
     */
    inline QString getTimeFormat() const
    { return timeFormat; }
};

/**
 * The set of axis representing the time on a time series 2D graph.
 */
class CPD3GRAPHING_EXPORT AxisDimensionTimeSeries2D : public AxisDimension2DSide {
    AxisParametersTimeSeries2D *parameters;
public:
    AxisDimensionTimeSeries2D(std::unique_ptr<AxisParametersTimeSeries2D> &&params);

    virtual ~AxisDimensionTimeSeries2D();

    std::unique_ptr<AxisDimension> clone() const override;

    std::vector<std::shared_ptr<
            DisplayCoupling>> createRangeCoupling(DisplayCoupling::CoupleMode mode = DisplayCoupling::Always) override;

    bool enableBoundsModification() const override;

protected:
    AxisDimensionTimeSeries2D(const AxisDimensionTimeSeries2D &other,
                              std::unique_ptr<AxisParameters> &&params);
};

namespace Internal {

/**
 * A coupling for a time series axis
 */
class TimeSeriesAxisCoupling : public DisplayRangeCoupling {
Q_OBJECT

    QPointer<AxisDimensionTimeSeries2D> parent;
public:
    TimeSeriesAxisCoupling();

    TimeSeriesAxisCoupling(DisplayCoupling::CoupleMode mode, AxisDimensionTimeSeries2D *setParent);

    virtual ~TimeSeriesAxisCoupling();

    double getMin() const override;

    double getMax() const override;

    bool canCouple(const std::shared_ptr<DisplayCoupling> &other) const override;

    void applyCoupling(const QVariant &data, DisplayCoupling::CouplingPosition position) override;
};

}

class AxisDimensionSetTimeSeries2D;

namespace Internal {

/**
 * The zoom for a time axis.
 */
class TimeSeriesAxisZoom : public AxisDimensionZoom {
Q_OBJECT

    QPointer<AxisDimensionTimeSeries2D> dimension;
    QPointer<AxisDimensionSetTimeSeries2D> parent;
public:
    TimeSeriesAxisZoom();

    TimeSeriesAxisZoom(AxisDimensionTimeSeries2D *setDimension,
                       AxisDimensionSetTimeSeries2D *setParent);

    virtual QString getTitle() const;

    virtual bool sharedZoom(const std::shared_ptr<DisplayZoomAxis> &other) const;

    virtual DisplayZoomAxis::DisplayMode getDisplayMode() const;
};

}

/**
 * A set of side for the time axis on a 2D time series graph.
 */
class CPD3GRAPHING_EXPORT AxisDimensionSetTimeSeries2D : public AxisDimensionSet2DSide {
public:
    AxisDimensionSetTimeSeries2D();

    virtual ~AxisDimensionSetTimeSeries2D();

    std::unique_ptr<AxisDimensionSet> clone() const override;

    AxisDimension *get(const std::unordered_set<QString> &units,
                       const std::unordered_set<QString> &groupUnits) override;

    AxisDimension *get(const std::unordered_set<QString> &units,
                       const std::unordered_set<QString> &groupUnits,
                       const QString &binding) override;

protected:
    AxisDimensionSetTimeSeries2D(const AxisDimensionSetTimeSeries2D &other);

    std::unique_ptr<AxisParameters> parseParameters(const Data::Variant::Read &value,
                                                    QSettings *settings = nullptr) const override;

    std::unique_ptr<AxisDimension> createDimension(std::unique_ptr<
            AxisParameters> &&parameters) const override;

    std::shared_ptr<AxisDimensionZoom> createZoom(AxisDimension *dimension) override;
};

/**
 * A time series / strip chart style two dimensional graph.  Time is on the
 * X axis and values are on the Y axis.
 */
class CPD3GRAPHING_EXPORT TimeSeries2D : public Graph2D {
Q_OBJECT

    QSettings *settings;
    std::unique_ptr<TimeSeriesNeedleSet2D> needles;
    double start;
    double end;
    bool timeBindingUpdated;

    enum {
        Ready_Needles = 0x0001, Ready_ALL = Ready_Needles,
    };
    int ready;

    struct NeedleBinding {
        TimeSeriesNeedleHandler2D *needle;
        AxisDimension2DSide *y;
        AxisDimension2DColor *z;
        QString text;
    };
    QVector<NeedleBinding> needleBindings;

public:
    virtual ~TimeSeries2D();

    TimeSeries2D(const DisplayDefaults &defaults = {}, QObject *parent = nullptr);

    TimeSeries2D(const TimeSeries2D &other);

    std::unique_ptr<Display> clone() const override;

    void registerChain(Data::ProcessingTapChain *chain) override;

    void initialize(const Data::ValueSegment::Transfer &config,
                    const DisplayDefaults &defaults = {}) override;

public slots:

    void setVisibleTimeRange(double start, double end) override;

protected:
    std::unique_ptr<TraceSet2D> createTraces() override;

    std::unique_ptr<BinSet2D> createXBins() override;

    std::unique_ptr<BinSet2D> createYBins() override;

    std::unique_ptr<IndicatorSet2D> createXIndicators() override;

    std::unique_ptr<AxisDimensionSet2DSide> createXAxes() override;

    std::unique_ptr<Graph2DParameters> parseParameters(const Data::Variant::Read &config,
                                                       const DisplayDefaults &defaults = {}) override;

    void checkFinalReady() override;

    std::vector<
            GraphPainter2DHighlight::Region> translateHighlightTrace(DisplayHighlightController::HighlightType type,
                                                                     double start,
                                                                     double end,
                                                                     double min,
                                                                     double max,
                                                                     TraceHandler2D *trace,
                                                                     AxisDimension2DSide *x,
                                                                     AxisDimension2DSide *y,
                                                                     AxisDimension2DColor *z,
                                                                     const std::unordered_set<
                                                                             size_t> &affectedDimensions) override;

    std::vector<
            GraphPainter2DHighlight::Region> translateHighlightXBin(DisplayHighlightController::HighlightType type,
                                                                    double start,
                                                                    double end,
                                                                    double min,
                                                                    double max,
                                                                    BinHandler2D *bins,
                                                                    AxisDimension2DSide *x,
                                                                    AxisDimension2DSide *y,
                                                                    const std::unordered_set<
                                                                            size_t> &affectedDimensions) override;

    bool auxiliaryProcess(const DisplayDynamicContext &context) override;

    bool auxiliaryBind(const DisplayDynamicContext &context,
                       AxisDimensionSet2DSide *xAxes,
                       AxisDimensionSet2DSide *yAxes,
                       AxisDimensionSet2DColor *zAxes,
                       std::vector<TraceUtilities::ColorRequest> &findColors,
                       std::deque<QColor> &claimedColors,
                       TraceSet2D *traces,
                       BinSet2D *xBins,
                       BinSet2D *yBins,
                       IndicatorSet2D *xIndicators,
                       IndicatorSet2D *yIndicators) override;

    bool auxiliaryClaim(const DisplayDynamicContext &context,
                        Legend &legend,
                        std::deque<QColor> &dynamicColors,
                        TraceSet2D *traces,
                        BinSet2D *xBins,
                        BinSet2D *yBins,
                        IndicatorSet2D *xIndicators,
                        IndicatorSet2D *yIndicators) override;

    bool getAuxiliaryInsets(const DisplayDynamicContext &context,
                            QPaintDevice *paintdevice,
                            qreal &top,
                            qreal &bottom,
                            qreal &left,
                            qreal &right) override;

    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> getAuxiliaryDraw(const QRectF &traceArea,
                                                                        const QRectF &sideAxisOutline,
                                                                        const QSizeF &totalSize) override;

    QString convertMouseoverToolTipTrace(TraceHandler2D *trace,
                                         TraceSet2D *traces,
                                         AxisDimension2DSide *x,
                                         AxisDimension2DSide *y,
                                         AxisDimension2DColor *z,
                                         const TracePoint<3> &point,
                                         const DisplayDynamicContext &context) override;

    QString convertMouseoverToolTipBins(BinHandler2D *bins,
                                        BinSet2D *binSet,
                                        bool vertical,
                                        AxisDimension2DSide *x,
                                        AxisDimension2DSide *y,
                                        const BinPoint<1, 1> &point,
                                        Graph2DMouseoverComponentTraceBox::Location location,
                                        const DisplayDynamicContext &context) override;

    QString convertMouseoverToolTipIndicator(IndicatorHandler2D *indicator,
                                             IndicatorSet2D *indicatorSet,
                                             AxisDimension2DSide *axis,
                                             const IndicatorPoint<1> &point,
                                             Axis2DSide side,
                                             const DisplayDynamicContext &context) override;

    QString convertMouseoverToolTipFit(TraceHandler2D *trace,
                                       TraceSet2D *traces,
                                       AxisDimension2DSide *x,
                                       AxisDimension2DSide *y,
                                       AxisDimension2DColor *z,
                                       double fx,
                                       double fy,
                                       double fz,
                                       double fi,
                                       const DisplayDynamicContext &context) override;

    QString convertMouseoverToolTipModelScan(TraceHandler2D *trace,
                                             TraceSet2D *traces,
                                             AxisDimension2DSide *x,
                                             AxisDimension2DSide *y,
                                             AxisDimension2DColor *z,
                                             double mx,
                                             double my,
                                             double mz,
                                             const DisplayDynamicContext &context) override;

    QString convertMouseoverToolTipFit(BinHandler2D *bins,
                                       BinSet2D *binSet,
                                       bool vertical,
                                       AxisDimension2DSide *x,
                                       AxisDimension2DSide *y,
                                       BinParameters2D::FitOrigin origin,
                                       double fx,
                                       double fy,
                                       const DisplayDynamicContext &context) override;

    void zoomMouseX(AxisDimensionSet2DSide *xAxes, qreal min, qreal max) override;

private slots:

    void needlesReady();
};

}
}

#endif
