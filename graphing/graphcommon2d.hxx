/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGGRAPHCOMMON2D_H
#define CPD3GRAPHINGGRAPHCOMMON2D_H

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QDialog>
#include <QCheckBox>
#include <QLineEdit>

#include "graphing/graphing.hxx"
#include "graphing/display.hxx"
#include "graphing/legend.hxx"
#include "core/sharedwork.hxx"
#include "graphing/traceset.hxx"
#include "graphing/binset.hxx"
#include "graphing/indicator.hxx"
#include "graphing/axiscommon.hxx"

namespace CPD3 {
namespace Graphing {

class Graph2D;

/**
 * The base class the mouseoever components for 2D graphs are derived from.
 */
class CPD3GRAPHING_EXPORT Graph2DMouseoverComponent : public QObject {
Q_OBJECT

public:
    Graph2DMouseoverComponent();

    virtual ~Graph2DMouseoverComponent();
};

/**
 * The base class for 2D graph draw elements.
 */
class CPD3GRAPHING_EXPORT Graph2DDrawPrimitive {
public:
    enum {
        Priority_Default = 0,
        Priority_Fill = -1,
        Priority_Highlight = 1,
        Priority_Grid = 2,
        Priority_AxisFixedLine = 3,
        Priority_BinMiddleLines = 4,
        Priority_Bins = 5,
        Priority_TraceLines = 6,
        Priority_TracePoints = 7,
        Priority_TraceFit = 8,
        Priority_Indicator = 9,
        Priority_AxisGradientBar = 10,
        Priority_Axes = 11,
        Priority_Legend = 12,
    };
private:
    int sortPriority;
public:
    Graph2DDrawPrimitive(int setSortPriority = Priority_Default);

    virtual ~Graph2DDrawPrimitive();

    virtual bool drawLessThan(const Graph2DDrawPrimitive *other) const;

    /**
     * Paint the component to the given painter.
     * 
     * @param painter   the target to paint to
     */
    virtual void paint(QPainter *painter) = 0;

    /**
     * Update the mouseover for this primitive, if necessary.
     * <br>
     * The default implementation does nothing.
     * 
     * @param point     the mouse point
     * @param output    the input and output point
     */
    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    /**
     * Update the mouse click for this primitive, if necessary.
     * <br>
     * The default implementation calls updateMouseover( const QPointF &,
     * std::shared_ptr<Graph2DMouseoverComponent> &)
     * 
     * @param point     the mouse point
     * @param output    the input and output point
     */
    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * The specialization of a legend item for a two dimensional trace.
 */
class CPD3GRAPHING_EXPORT GraphLegendItem2D : public LegendItem {
public:
    GraphLegendItem2D(const QString &setText,
                      const QColor &setColor,
                      const QFont &setFont,
                      qreal setLineWidth,
                      Qt::PenStyle setStyle,
                      const TraceSymbol &setSymbol,
                      bool setDrawSymbol,
                      bool setDrawSwatch = false);

    virtual DisplayModificationComponent *createModificationComponent(Graph2D *graph);

    virtual QString getLegendTooltip() const;
};

/**
 * A mouseover component on a two dimension trace-space point.
 */
class CPD3GRAPHING_EXPORT Graph2DMouseoverComponentTrace : public Graph2DMouseoverComponent {
Q_OBJECT

    QPointF center;
    quintptr tracePtr;
public:
    Graph2DMouseoverComponentTrace(const QPointF &setCenter, quintptr setTracePtr);

    virtual ~Graph2DMouseoverComponentTrace();

    /**
     * Get the center point.
     * @return the center point
     */
    inline QPointF getCenter() const
    { return center; }

    /**
     * Get the trace point ID.
     * @return the trace pointer ID.
     */
    inline quintptr getTracePtr() const
    { return tracePtr; }
};

/**
 * A simple standard point on a normal trace.
 */
class CPD3GRAPHING_EXPORT Graph2DMouseoverComponentTraceStandard
        : public Graph2DMouseoverComponentTrace {
Q_OBJECT

    TracePoint<3> point;
public:
    Graph2DMouseoverComponentTraceStandard(const QPointF &setCenter,
                                           quintptr setTracePtr,
                                           const TracePoint<3> &setPoint);

    virtual ~Graph2DMouseoverComponentTraceStandard();

    /**
     * Get the trace point.
     * @return the trace point
     */
    inline TracePoint<3> getPoint() const
    { return point; }
};

/**
 * A simple standard point on a trace fit.
 */
class CPD3GRAPHING_EXPORT Graph2DMouseoverComponentTraceFit
        : public Graph2DMouseoverComponentTrace {
Q_OBJECT

    double x;
    double y;
    double z;
    double i;
public:
    Graph2DMouseoverComponentTraceFit(const QPointF &setCenter,
                                      quintptr setTracePtr,
                                      double sX,
                                      double sY,
                                      double sZ = FP::undefined(),
                                      double sI = FP::undefined());

    virtual ~Graph2DMouseoverComponentTraceFit();

    /**
     * Get the real space X coordinate.
     * @return the X coordinate
     */
    inline double getX() const
    { return x; }

    /**
     * Get the real space Y coordinate.
     * @return the Y coordinate
     */
    inline double getY() const
    { return y; }

    /**
     * Get the real space Z coordinate.
     * @return the Z coordinate
     */
    inline double getZ() const
    { return z; }

    /**
     * Get the real space I coordinate.
     * @return the I coordinate
     */
    inline double getI() const
    { return i; }
};


/**
 * A simple standard point on a trace fill model scan.
 */
class CPD3GRAPHING_EXPORT Graph2DMouseoverComponentTraceFillModelScan
        : public Graph2DMouseoverComponentTrace {
Q_OBJECT

    double x;
    double y;
    double z;
public:
    Graph2DMouseoverComponentTraceFillModelScan(const QPointF &setCenter,
                                                quintptr setTracePtr,
                                                double sX,
                                                double sY,
                                                double sZ);

    virtual ~Graph2DMouseoverComponentTraceFillModelScan();

    /**
     * Get the real space X coordinate.
     * @return the X coordinate
     */
    inline double getX() const
    { return x; }

    /**
     * Get the real space Y coordinate.
     * @return the Y coordinate
     */
    inline double getY() const
    { return y; }

    /**
     * Get the real space Z coordinate.
     * @return the Z coordinate
     */
    inline double getZ() const
    { return z; }
};

/**
 * A point on a box whisker trace.
 */
class CPD3GRAPHING_EXPORT Graph2DMouseoverComponentTraceBox
        : public Graph2DMouseoverComponentTrace {
Q_OBJECT
public:
    enum Location {
        Lowest, Lower, Middle, Upper, Uppermost,
    };
private:
    BinPoint<1, 1> point;
    Location location;
public:
    Graph2DMouseoverComponentTraceBox(const QPointF &setCenter,
                                      quintptr setTracePtr,
                                      const BinPoint<1, 1> &setPoint,
                                      Location setLocation);

    virtual ~Graph2DMouseoverComponentTraceBox();

    /**
     * Get the bin point.
     * @return the bin point
     */
    inline BinPoint<1, 1> getPoint() const
    { return point; }

    /**
     * Get the real space location hit.
     * @return the location
     */
    inline Location getLocation() const
    { return location; }
};

/**
 * A point on an indicator
 */
class CPD3GRAPHING_EXPORT Graph2DMouseoverComponentIndicator
        : public Graph2DMouseoverComponentTrace {
Q_OBJECT
private:
    IndicatorPoint<1> point;
    Axis2DSide side;
public:
    Graph2DMouseoverComponentIndicator(const QPointF &setCenter,
                                       quintptr setTracePtr,
                                       const IndicatorPoint<1> &setPoint,
                                       Axis2DSide setSide);

    virtual ~Graph2DMouseoverComponentIndicator();

    /**
     * Get the indicator point.
     * @return the bin point
     */
    inline IndicatorPoint<1> getPoint() const
    { return point; }

    /**
     * Get the axis side the indicator was on.
     * @return the axis side
     */
    inline Axis2DSide getSide() const
    { return side; }
};


/**
 * The main graph title.
 */
class CPD3GRAPHING_EXPORT Graph2DMouseoverTitle : public Graph2DMouseoverComponent {
Q_OBJECT
public:
    Graph2DMouseoverTitle();

    virtual ~Graph2DMouseoverTitle();
};

/**
 * Part of the legend.
 */
class CPD3GRAPHING_EXPORT Graph2DMouseoverLegend : public Graph2DMouseoverComponent {
Q_OBJECT
public:
    Graph2DMouseoverLegend();

    virtual ~Graph2DMouseoverLegend();
};

/**
 * A specific item in the legend.
 */
class CPD3GRAPHING_EXPORT Graph2DMouseoverLegendItem : public Graph2DMouseoverLegend {
Q_OBJECT

    std::shared_ptr<GraphLegendItem2D> item;
public:
    Graph2DMouseoverLegendItem(const std::shared_ptr<GraphLegendItem2D> &setItem);

    virtual ~Graph2DMouseoverLegendItem();

    /**
     * Get the legend item that the mouseover point hit.
     * 
     * @return the legend item
     */
    inline std::shared_ptr<GraphLegendItem2D> getItem() const
    { return item; }
};

/**
 * One of the axes of the graph.
 */
class CPD3GRAPHING_EXPORT Graph2DMouseoverAxis : public Graph2DMouseoverComponent {
Q_OBJECT

    quintptr axisPtr;
public:
    Graph2DMouseoverAxis(quintptr setAxisPtr);

    virtual ~Graph2DMouseoverAxis();

    /**
     * Get the axis ID of the mouseover.
     * @return the axis ID
     */
    inline quintptr getAxisPtr() const
    { return axisPtr; }
};


/**
 * The basic global parameters of a graph.
 */
class CPD3GRAPHING_EXPORT Graph2DParameters {
public:
    enum LegendPosition {
        /** Inside the trace area, attached to the top left corner. */
        Inside_TopLeft, /** Inside the trace area, attached to the top right corner. */
        Inside_TopRight, /** Inside the trace area, attached to the bottom left corner. */
        Inside_BottomLeft, /** Inside the trace area, attached to the bottom right corner. */
        Inside_BottomRight, /** Outside of the graph, on the left margin. */
        Outside_Left, /** Outside of the graph, on the right margin. */
        Outside_Right,
    };
private:
    enum {
        Component_TitleText = 0x00000001,
        Component_TitleFont = 0x00000002,
        Component_TitleColor = 0x00000004,
        Component_LegendEnable = 0x00000008,
        Component_LegendPosition = 0x00000010,
        Component_LegendBoxXInset = 0x00000040,
        Component_LegendBoxYInset = 0x00000080,
        Component_LegendBoxSize = 0x00000100,
        Component_LegendBoxOutlineWidth = 0x00000200,
        Component_LegendBoxOutlineStyle = 0x00000400,
        Component_LegendBoxOutlineColor = 0x00000800,
        Component_LegendBoxFillColor = 0x00001000,
        Component_LegendDrawPriority = 0x00002000,
        Component_HighlightColor = 0x00004000,
        Component_HighlightDrawPriority = 0x00008000,
        Component_InsetTop = 0x00010000,
        Component_InsetBottom = 0x00020000,
        Component_InsetLeft = 0x00040000,
        Component_InsetRight = 0x00080000,
    };
    int components;

    QString titleText;
    QFont titleFont;
    QColor titleColor;
    bool legendEnable;
    LegendPosition legendPosition;
    qreal legendBoxXInset;
    qreal legendBoxYInset;
    qreal legendBoxSize;
    qreal legendBoxOutlineWidth;
    Qt::PenStyle legendBoxOutlineStyle;
    QColor legendBoxOutlineColor;
    QColor legendBoxFillColor;
    int legendDrawPriority;
    QColor highlightColor;
    int highlightDrawPriority;
    qreal insetTop;
    qreal insetBottom;
    qreal insetLeft;
    qreal insetRight;
public:
    Graph2DParameters();

    virtual ~Graph2DParameters();

    Graph2DParameters(const Graph2DParameters &other);

    Graph2DParameters &operator=(const Graph2DParameters &other);

    Graph2DParameters(Graph2DParameters &&other);

    Graph2DParameters &operator=(Graph2DParameters &&other);

    Graph2DParameters(const Data::Variant::Read &value,
                      const DisplayDefaults &defaults = DisplayDefaults());

    /**
     * Save the parameters to the given value.  Derived classes must call
     * the base implementation.
     * 
     * @param value the target value
     */
    virtual void save(Data::Variant::Write &value) const;

    /**
     * Create a polymorphic copy of the parameters.
     * 
     * @return a copy of the parameters
     */
    virtual std::unique_ptr<Graph2DParameters> clone() const;

    /**
     * Overlay the given parameters on top of these.  Derived implementations 
     * must call the base one.
     * 
     * @param over  the parameters to overlay
     */
    virtual void overlay(const Graph2DParameters *over);

    /**
     * Overlay the given parameters on top of these during initialization.  
     * Derived implementations must call the base one.
     * 
     * @param over  the parameters to overlay
     */
    virtual void initializeOverlay(const Graph2DParameters *over);

    /**
     * Copy the contents if the other parameters into these.
     * <br>
     * This is equivalent to: *this = *(static_cast<const CLASS *>(from))
     * 
     * @param from the source parameters to copy from
     */
    virtual void copy(const Graph2DParameters *from);

    /**
     * Clear all defined flags in the parameters.  Derived implementations 
     * must call the base one.
     */
    virtual void clear();

    /**
     * Test if the parameters specify the title text.
     * @return true if the parameters specify the title text
     */
    inline bool hasTitleText() const
    { return components & Component_TitleText; }

    /**
     * Get the title text.
     * @return the title text
     */
    inline QString getTitleText() const
    { return titleText; }

    /**
     * Set the title text of the graph.  This does not set the text defined
     * flag.
     * @param text the text to set
     */
    inline void setTitleText(const QString &text)
    { titleText = text; }

    /**
     * Override the main graph title.  This sets the defined flag.
     * @param text  the new title text
     */
    inline void overrideTitleText(const QString &text)
    {
        titleText = text;
        components |= Component_TitleText;
    }

    /**
     * Test if the parameters specify the title font.
     * @return true if the parameters specify the title font
     */
    inline bool hasTitleFont() const
    { return components & Component_TitleFont; }

    /**
     * Get the title text.
     * @return the title text
     */
    inline QFont getTitleFont() const
    { return titleFont; }

    /**
     * Override the main graph title font.  This sets the defined flag.
     * @param font  the new title font
     */
    inline void overrideTitleFont(const QFont &font)
    {
        titleFont = font;
        components |= Component_TitleFont;
    }

    /**
     * Test if the parameters specify the title color.
     * @return true if the parameters specify the title color
     */
    inline bool hasTitleColor() const
    { return components & Component_TitleColor; }

    /**
     * Get the title color.
     * @return the title color
     */
    inline QColor getTitleColor() const
    { return titleColor; }

    /**
     * Override the main graph title color.  This sets the defined flag.
     * @param color the new title color
     */
    inline void overrideTitleColor(const QColor &color)
    {
        titleColor = color;
        components |= Component_TitleColor;
    }

    /**
     * Test if the parameters specify the legend enable state.
     * @return true if the parameters specify the legend enable state
     */
    inline bool hasLegendEnable() const
    { return components & Component_LegendEnable; }

    /**
     * Test if the legend should be drawn.
     * @return true if the legend is enabled
     */
    inline bool getLegendEnable() const
    { return legendEnable; }

    /**
     * Test if the parameters specify the legend position.
     * @return true if the parameters specify the legend position
     */
    inline bool hasLegendPosition() const
    { return components & Component_LegendPosition; }

    /**
     * Get the legend position.
     * @return the legend position
     */
    inline LegendPosition getLegendPosition() const
    { return legendPosition; }

    /**
     * Override the legend position.  This sets the defined flag.
     * @param pos   the new legend position
     */
    inline void overrideLegendPosition(LegendPosition pos)
    {
        legendPosition = pos;
        components |= Component_LegendPosition;
    }

    /**
     * Test if the parameters specify the legend box X inset.
     * @return true if the parameters specify the box X inset
     */
    inline bool hasLegendBoxXInset() const
    { return components & Component_LegendBoxXInset; }

    /**
     * Get the legend box X inset.
     * @return the legend box X inset
     */
    inline qreal getLegendBoxXInset() const
    { return legendBoxXInset; }

    /**
     * Test if the parameters specify the legend box Y inset.
     * @return true if the parameters specify the box Y inset
     */
    inline bool hasLegendBoxYInset() const
    { return components & Component_LegendBoxYInset; }

    /**
     * Get the legend box Y inset.
     * @return the legend box Y inset
     */
    inline qreal getLegendBoxYInset() const
    { return legendBoxYInset; }

    /**
     * Test if the parameters specify the legend box size.
     * @return true if the parameters specify the box size
     */
    inline bool hasLegendBoxSize() const
    { return components & Component_LegendBoxSize; }

    /**
     * Get the legend box size.
     * @return the legend box size
     */
    inline qreal getLegendBoxSize() const
    { return legendBoxSize; }

    /**
     * Test if the parameters specify the legend box outline width.
     * @return true if the parameters specify the box outline width
     */
    inline bool hasLegendBoxOutlineWidth() const
    { return components & Component_LegendBoxOutlineWidth; }

    /**
     * Get the legend box outline width.
     * @return the legend box outline width
     */
    inline qreal getLegendBoxOutlineWidth() const
    { return legendBoxOutlineWidth; }

    /**
     * Test if the parameters specify the legend box outline style.
     * @return true if the parameters specify the box outline style
     */
    inline bool hasLegendBoxOutlineStyle() const
    { return components & Component_LegendBoxOutlineStyle; }

    /**
     * Get the legend box outline style.
     * @return the legend box outline style
     */
    inline Qt::PenStyle getLegendBoxOutlineStyle() const
    { return legendBoxOutlineStyle; }

    /**
     * Test if the parameters specify the legend box outline color.
     * @return true if the parameters specify the box outline color
     */
    inline bool hasLegendBoxOutlineColor() const
    { return components & Component_LegendBoxOutlineColor; }

    /**
     * Get the legend box outline color.
     * @return the legend box outline color
     */
    inline QColor getLegendBoxOutlineColor() const
    { return legendBoxOutlineColor; }

    /**
     * Test if the parameters specify the legend box fill color.
     * @return true if the parameters specify the box fill color
     */
    inline bool hasLegendBoxFillColor() const
    { return components & Component_LegendBoxFillColor; }

    /**
     * Get the legend box fill color.
     * @return the legend box fill color
     */
    inline QColor getLegendBoxFillColor() const
    { return legendBoxFillColor; }

    /**
     * Test if the parameters specify the legend draw sort priority.
     * @return true if the parameters specify legend draw sort priority
     */
    inline bool hasLegendDrawPriority() const
    { return components & Component_LegendDrawPriority; }

    /**
     * Get the legend draw sort priority.
     * @return the legend draw sort priority
     */
    inline int getLegendDrawPriority() const
    { return legendDrawPriority; }

    /**
     * Test if the parameters specify the the highlight area color.
     * @param type  the type of the highlight
     * @return true if the parameters specify the highlight area color
     */
    inline bool hasHighlightColor(DisplayHighlightController::HighlightType type) const
    {
        Q_UNUSED(type);
        return components & Component_HighlightColor;
    }

    /**
     * Get the highlight area color color.
     * @param type  the type of the highlight
     * @return the highlight area color color
     */
    inline QColor getHighlightColor(DisplayHighlightController::HighlightType type) const
    {
        Q_UNUSED(type);
        return highlightColor;
    }

    /**
     * Test if the parameters specify the highlight draw sort priority.
     * @return true if the parameters specify highlight draw sort priority
     */
    inline bool hasHighlightDrawPriority() const
    { return components & Component_HighlightDrawPriority; }

    /**
     * Get the highlight draw sort priority.
     * @return the highlight draw sort priority
     */
    inline int getHighlightDrawPriority() const
    { return highlightDrawPriority; }

    /**
     * Test if the parameters specify a top side inset.
     * @return true if the parameters specify a top side inset
     */
    inline bool hasInsetTop() const
    { return components & Component_InsetTop; }

    /**
     * Get the top side inset.
     * @return the top side inset
     */
    inline qreal getInsetTop() const
    { return insetTop; }

    /**
     * Test if the parameters specify a bottom side inset.
     * @return true if the parameters specify a bottom side inset
     */
    inline bool hasInsetBottom() const
    { return components & Component_InsetBottom; }

    /**
     * Get the bottom side inset.
     * @return the bottom side inset
     */
    inline qreal getInsetBottom() const
    { return insetBottom; }

    /**
     * Test if the parameters specify a left side inset.
     * @return true if the parameters specify a left side inset
     */
    inline bool hasInsetLeft() const
    { return components & Component_InsetLeft; }

    /**
     * Get the left side inset.
     * @return the left side inset
     */
    inline qreal getInsetLeft() const
    { return insetLeft; }

    /**
     * Test if the parameters specify a right side inset.
     * @return true if the parameters specify a right side inset
     */
    inline bool hasInsetRight() const
    { return components & Component_InsetRight; }

    /**
     * Get the right side inset.
     * @return the right side inset
     */
    inline qreal getInsetRight() const
    { return insetRight; }
};

namespace Internal {

/**
 * The base class for legend modification on a 2-D graph.
 */
class Graph2DLegendModification : public DisplayModificationComponent {
Q_OBJECT

    QPointer<Graph2D> graph;

    void setPosition(Graph2DParameters::LegendPosition pos);

public:
    explicit Graph2DLegendModification(Graph2D *graph);

    virtual ~Graph2DLegendModification();

    QString getTitle() const override;

    QList<QAction *> getMenuActions(QWidget *parent = nullptr) override;
};

}

}
}


#endif
