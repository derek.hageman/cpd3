/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>
#include <QPen>
#include <QPainter>

#include "algorithms/model.hxx"
#include "core/range.hxx"
#include "graphing/legend.hxx"
#include "guicore/guiformat.hxx"

using namespace CPD3::Data;
using namespace CPD3::Algorithms;
using namespace CPD3::GUI;

namespace CPD3 {
namespace Graphing {

/** @file graphing/legend.hxx
 * Defines utilities to draw a simple graph legend.
 */

LegendItem::LegendItem() = default;

LegendItem::~LegendItem() = default;

LegendItem::LegendItem(const QString &setText,
                       const QColor &setColor,
                       const QFont &setFont,
                       double setLineWidth,
                       Qt::PenStyle setStyle,
                       const TraceSymbol &setSymbol,
                       bool setDrawSymbol,
                       bool setDrawSwatch) : lineWidth(setLineWidth),
                                             lineStyle(setStyle),
                                             drawSwatch(setDrawSwatch),
                                             symbol(setSymbol),
                                             drawSymbol(setDrawSymbol),
                                             text(setText),
                                             color(setColor),
                                             font(setFont),
                                             nGroup(1)
{ }

Legend::Legend() : items(), columns(), predictedSize(0, 0)
{ }

Legend::Legend(const Legend &) = default;

Legend &Legend::operator=(const Legend &) = default;

Legend::Legend(Legend &&) = default;

Legend &Legend::operator=(Legend &&) = default;

/**
 * Clear the legend.
 */
void Legend::clear()
{
    items.clear();
    columns.clear();
    predictedSize = QSizeF(0, 0);
    rowHeights.clear();
    columnWidths.clear();
    columnGraphicWidths.clear();
}

/**
 * Add a single stand alone item to the legend.
 * 
 * @param add   the item to add
 */
void Legend::append(const std::shared_ptr<LegendItem> &add)
{ items.emplace_back(add); }

void Legend::append(std::shared_ptr<LegendItem> &&add)
{ items.emplace_back(std::move(add)); }

/**
 * A multiple grouped items to the legend.
 * 
 * @param add   the items to add as a group
 */
void Legend::append(const std::vector<std::shared_ptr<LegendItem>> &add)
{
    if (add.empty()) return;
    auto idxFirst = items.size();
    Util::append(add, items);
    items[idxFirst]->nGroup = add.size();
}

void Legend::append(std::vector<std::shared_ptr<LegendItem>> &&add)
{
    if (add.empty()) return;
    auto idxFirst = items.size();
    auto nAdd = add.size();
    Util::append(std::move(add), items);
    items[idxFirst]->nGroup = nAdd;
}

static const double lineSampleWidth = 2.0;
static const double swatchSize = 1.0;
static const double spacingBeforeText = 0.5;
static const double spacingBetweenRows = 0.15;
static const double spacingBetweenColumns = 1.0;

/**
 * Layout the legend according the constraints given.
 * 
 * @param maximumWidth  if positive this is the maximum width to try for
 * @param maximumHeight if positive this is the maximum height to try for
 * @param paintdevice   the paint device to use for sizing or NULL for screen space
 */
void Legend::layout(double maximumWidth, double maximumHeight, QPaintDevice *paintdevice)
{
    if (items.empty()) {
        columns.clear();
        predictedSize = QSizeF(0, 0);
        return;
    }

    QFontMetrics fm(QApplication::font());
    if (paintdevice)
        fm = QFontMetrics(QApplication::font(), paintdevice);
    qreal baseSize = fm.height();

    for (std::size_t layoutColumns = 1, maxCols = items.size() + 1;
            layoutColumns <= maxCols;
            layoutColumns++) {
        std::size_t divRow;
        if (layoutColumns != maxCols)
            divRow = static_cast<std::size_t>(std::ceil(
                    (double) items.size() / (double) layoutColumns));
        else
            divRow = items.size();

        columns.clear();
        columns.emplace_back();

        bool valid = true;
        bool firstOverflowed = false;
        for (std::size_t i = 0, max = items.size(); i < max; i++) {
            if (columns.back().size() + items[i]->nGroup > divRow) {
                if (columns.size() >= layoutColumns) {
                    valid = false;
                    break;
                }
                if (columns.size() == 1 && items[i]->nGroup != 1 && !firstOverflowed) {
                    firstOverflowed = true;
                    divRow = columns.front().size() + items[i]->nGroup;
                } else {
                    columns.emplace_back();
                }
            }
            columns.back().push_back(items[i]);
        }
        if (!valid && layoutColumns != maxCols)
            continue;

        rowHeights.clear();
        columnWidths.clear();
        columnGraphicWidths.clear();
        for (std::size_t col = 0, nCols = columns.size(); col < nCols; col++) {
            columnWidths.emplace_back(0);

            const auto &rows = columns[col];

            double graphicWidth = 0;
            for (std::size_t row = 0, nRows = rows.size(); row < nRows; row++) {
                qreal width = 0;

                if (rows[row]->drawSwatch) {
                    width = qMax(width, swatchSize * baseSize);
                } else {
                    if ((rows[row]->lineWidth >= 0.0 && rows[row]->lineStyle != Qt::NoPen) ||
                            (rows[row]->drawSymbol &&
                                    rows[row]->symbol.getType() != TraceSymbol::Unselected)) {
                        if (rows[row]->lineWidth >= 0 && rows[row]->lineStyle != Qt::NoPen) {
                            width = qMax(width, lineSampleWidth * baseSize);
                        }
                        if (rows[row]->drawSymbol &&
                                rows[row]->symbol.getType() != TraceSymbol::Unselected) {
                            QSizeF symbolSize(rows[row]->symbol.predictSize(paintdevice));
                            width = qMax(width, symbolSize.width());
                        }
                    }
                }

                if (width > graphicWidth)
                    graphicWidth = width;
            }

            columnGraphicWidths.emplace_back(graphicWidth);

            for (std::size_t row = 0, nRows = rows.size(); row < nRows; row++) {
                if (row >= rowHeights.size())
                    rowHeights.emplace_back(0);

                qreal height = 0;

                if (rows[row]->drawSwatch) {
                    height = qMax(height, swatchSize * baseSize);
                } else {
                    if ((rows[row]->lineWidth >= 0.0 && rows[row]->lineStyle != Qt::NoPen) ||
                            (rows[row]->drawSymbol &&
                                    rows[row]->symbol.getType() != TraceSymbol::Unselected)) {
                        if (rows[row]->lineWidth >= 0 && rows[row]->lineStyle != Qt::NoPen) {
                            if (rows[row]->lineWidth == 0)
                                height = qMax(height, (qreal) 1);
                            else
                                height = qMax(height, rows[row]->lineWidth);
                        }
                        if (rows[row]->drawSymbol &&
                                rows[row]->symbol.getType() != TraceSymbol::Unselected) {
                            QSizeF symbolSize(rows[row]->symbol.predictSize(paintdevice));
                            height = qMax(height, symbolSize.height() + 2);
                        }
                    }
                }

                qreal width = graphicWidth;

                if (!rows[row]->text.isEmpty()) {
                    if (!paintdevice)
                        fm = QFontMetrics(rows[row]->font);
                    else
                        fm = QFontMetrics(rows[row]->font, paintdevice);
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
                    qreal textWidth = fm.width(rows[row]->text);
#else
                    qreal textWidth = fm.horizontalAdvance(rows[row]->text);
#endif
                    if (textWidth > 0) {
                        if (width > 0) {
                            width += spacingBeforeText * baseSize;
                        }
                        width += textWidth;
                    }
                    height = qMax(height, (qreal) GUIStringLayout::maximumCharacterDimensions(
                            rows[row]->text, fm).height());
                }

                if (height > rowHeights[row])
                    rowHeights[row] = height;
                if (width > columnWidths[col])
                    columnWidths[col] = width;
            }
        }

        qreal totalWidth = 0;
        for (std::size_t col = 0, nCols = columnWidths.size(); col < nCols; col++) {
            if (col != nCols - 1)
                columnWidths[col] += spacingBetweenColumns * baseSize;
            totalWidth += columnWidths[col];
        }

        qreal totalHeight = 0;
        for (std::size_t row = 0, nRows = rowHeights.size(); row < nRows; row++) {
            if (row != nRows - 1)
                totalHeight += spacingBetweenRows * baseSize + 1;
            totalHeight += rowHeights[row];
        }

        if (layoutColumns != maxCols &&
                ((maximumWidth > 0.0 && totalWidth > maximumWidth) ||
                        (maximumHeight > 0.0 && totalHeight > maximumHeight))) {
            continue;
        }

        predictedSize = QSizeF(totalWidth, totalHeight);
        return;
    }
}

/**
 * Get the size as defined by the latest layout call.  This does not include
 * any border spacing.
 * 
 * @return the legend size
 */
QSizeF Legend::getSize() const
{ return predictedSize; }

/**
 * Paint the legend.  This does not include any border or background.  The 
 * painter should be translated to the upper left corner of where the legend
 * should be drawn.
 * 
 * @param painter   the painter target
 */
void Legend::paint(QPainter *painter) const
{
    if (items.empty())
        return;

    QFontMetrics fm(QApplication::font(), painter->device());
    qreal baseSize = fm.height();

    painter->save();

    qreal x = 0;
    for (std::size_t col = 0, nCols = columns.size(); col < nCols; col++) {
        const auto &rows = columns[col];
        qreal y = 0;
        for (std::size_t row = 0, nRows = rows.size(); row < nRows; row++) {
            qreal startX = x;
            qreal midY = y + rowHeights[row] * 0.5;
            qreal graphicWidth = columnGraphicWidths[col];

            painter->setBrush(QBrush(rows[row]->color));

            if (rows[row]->drawSwatch) {
                qreal effectiveSwatchSize = baseSize * swatchSize;

                if (effectiveSwatchSize > 2) {
                    painter->setPen(QPen(QBrush(Qt::black), 1));
                    painter->drawRect(
                            QRectF(startX + graphicWidth * 0.5 - effectiveSwatchSize * 0.5 + 1,
                                   midY - effectiveSwatchSize * 0.5 + 1, effectiveSwatchSize - 2,
                                   effectiveSwatchSize - 2));
                }
            } else {
                if ((rows[row]->lineWidth >= 0 && rows[row]->lineStyle != Qt::NoPen) ||
                        (rows[row]->drawSymbol &&
                                rows[row]->symbol.getType() != TraceSymbol::Unselected)) {
                    if (rows[row]->drawSymbol &&
                            rows[row]->symbol.getType() != TraceSymbol::Unselected) {
                        painter->setPen(QPen(painter->brush(), 1));
                        rows[row]->symbol.paint(painter, startX + graphicWidth * 0.5, midY);
                    }

                    if (rows[row]->lineWidth >= 0 && rows[row]->lineStyle != Qt::NoPen) {
                        painter->setPen(
                                QPen(painter->brush(), rows[row]->lineWidth, rows[row]->lineStyle));
                        painter->drawLine(QPointF(startX, midY),
                                          QPointF(startX + graphicWidth, midY));
                    }
                }
            }
            if (graphicWidth > 0)
                startX += graphicWidth;
            if (startX > x)
                startX += spacingBeforeText * baseSize;

            if (!rows[row]->text.isEmpty()) {
                painter->setFont(rows[row]->font);
                painter->setPen(QPen(painter->brush(), 1));
                painter->drawText(QRectF(startX, midY, 0, 0),
                                  Qt::TextDontClip | Qt::AlignVCenter | Qt::AlignLeft,
                                  rows[row]->text);
            }

            y += rowHeights[row] + spacingBetweenRows * baseSize + 1;
        }

        x += columnWidths[col];
    }

    painter->restore();
}

/**
 * Paint the legend.  This does not include any border or background.  
 * 
 * @param painter   the painter target
 * @param x         the left coordinate of the legend
 * @param y         the top coordinate of the legend
 */
void Legend::paint(QPainter *painter, qreal x, qreal y) const
{
    painter->save();
    painter->translate(x, y);
    paint(painter);
    painter->restore();
};

/**
 * Find the item at the given point (relative to the top left of the legend).
 * 
 * @param x             the x coordinate
 * @param y             the y coordinate
 * @param paintdevice   the paint device to use for sizing or NULL for screen space
 * @return              the item at the given point, or NULL is none
 */
std::shared_ptr<LegendItem> Legend::itemAt(qreal x, qreal y, QPaintDevice *paintdevice) const
{
    if (items.empty() || x < 0 || y < 0)
        return {};

    QFontMetrics fm(QApplication::font());
    if (paintdevice)
        fm = QFontMetrics(QApplication::font(), paintdevice);
    qreal baseSize = fm.height();

    std::size_t col = 0;
    for (std::size_t nCols = columns.size(); col < nCols; col++) {
        if (x < 0)
            return std::shared_ptr<LegendItem>();
        if (col == nCols - 1) {
            if (x <= columnWidths[col])
                break;
        } else {
            if (x <= columnWidths[col] - spacingBetweenColumns * baseSize)
                break;
        }
        x -= columnWidths[col];
    }
    if (col >= columns.size())
        return {};

    std::size_t row = 0;
    for (std::size_t nRows = columns[col].size(); row < nRows; row++) {
        if (y < 0)
            return std::shared_ptr<LegendItem>();
        if (y <= rowHeights[row])
            break;
        y -= rowHeights[row] + spacingBetweenRows * baseSize + 1;
    }
    if (row >= rowHeights.size() || row >= columns[col].size())
        return {};
    return columns[col][row];
}

}
}
