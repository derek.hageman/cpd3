/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>
#include <QInputDialog>
#include <QColorDialog>
#include <QFontDialog>
#include <QMenu>
#include <QActionGroup>

#include "datacore/variant/root.hxx"
#include "algorithms/model.hxx"
#include "algorithms/logwrapper.hxx"
#include "graphing/graphcommon2d.hxx"
#include "graphing/trace2d.hxx"
#include "graphing/graphpainters2d.hxx"

using namespace CPD3::Data;
using namespace CPD3::Algorithms;
using namespace CPD3::Graphing::Internal;

namespace CPD3 {
namespace Graphing {

/** @file graphing/trace2d.hxx
 * The basis for two dimensional graph traces.
 */


TraceParameters2D::~TraceParameters2D()
{ }

TraceParameters2D::TraceParameters2D()
        : TraceParameters(),
          components(Component_LAST, false),
          lineEnable(false),
          lineWidth(0.0),
          lineStyle(Qt::SolidLine),
          lineDrawPriority(Graph2DDrawPrimitive::Priority_TraceLines),
          symbolEnable(true),
          symbolDrawPriority(Graph2DDrawPrimitive::Priority_TracePoints),
          color(0, 0, 0),
          legendEntry(true),
          legendSortPriority(0),
          fitLogXBase(FP::undefined()),
          fitLogYBase(FP::undefined()),
          fitLogZBase(FP::undefined()),
          fitMode(Fit_IndependentX),
          fitMinI(0.0),
          fitMaxI(1.0),
          fitDisableZ(false),
          fitLineWidth(1.0),
          fitLineStyle(Qt::SolidLine),
          fitDrawPriority(Graph2DDrawPrimitive::Priority_TraceFit),
          fitColor(0, 0, 0),
          fillLogXBase(FP::undefined()),
          fillLogYBase(FP::undefined()),
          fillLogZBase(FP::undefined()),
          fillMode(Fill_ModelScan),
          fillDrawPriority(Graph2DDrawPrimitive::Priority_Fill),
          continuity(1.0),
          skipUndefined(false)
{
    fitLegendFont = legendFont;
    components.assign(Component_LAST, false);
}

TraceParameters2D::TraceParameters2D(const TraceParameters2D &) = default;

TraceParameters2D &TraceParameters2D::operator=(const TraceParameters2D &) = default;

TraceParameters2D::TraceParameters2D(TraceParameters2D &&) = default;

TraceParameters2D &TraceParameters2D::operator=(TraceParameters2D &&) = default;

void TraceParameters2D::copy(const TraceParameters *from)
{ *this = *(static_cast<const TraceParameters2D *>(from)); }

void TraceParameters2D::clear()
{ components.assign(Component_LAST, false); }

std::unique_ptr<TraceParameters> TraceParameters2D::clone() const
{ return std::unique_ptr<TraceParameters>(new TraceParameters2D(*this)); }

/**
 * Construct trace parameters from the given configuration value.
 * 
 * @param value the value to configure from
 */
TraceParameters2D::TraceParameters2D(const Variant::Read &value) : TraceParameters(value),
                                                                   components(Component_LAST,
                                                                              false),
                                                                   lineEnable(false),
                                                                   lineWidth(0.0),
                                                                   lineStyle(Qt::SolidLine),
                                                                   lineDrawPriority(
                                                                           Graph2DDrawPrimitive::Priority_TraceLines),
                                                                   symbolEnable(true),
                                                                   symbolDrawPriority(
                                                                           Graph2DDrawPrimitive::Priority_TracePoints),
                                                                   color(0, 0, 0),
                                                                   legendEntry(true),
                                                                   legendSortPriority(0),
                                                                   fitLogXBase(FP::undefined()),
                                                                   fitLogYBase(FP::undefined()),
                                                                   fitLogZBase(FP::undefined()),
                                                                   fitMode(Fit_IndependentX),
                                                                   fitMinI(0.0),
                                                                   fitMaxI(1.0),
                                                                   fitDisableZ(false),
                                                                   fitLineWidth(1.0),
                                                                   fitLineStyle(Qt::SolidLine),
                                                                   fitDrawPriority(
                                                                           Graph2DDrawPrimitive::Priority_TraceFit),
                                                                   fitColor(0, 0, 0),
                                                                   fillLogXBase(FP::undefined()),
                                                                   fillLogYBase(FP::undefined()),
                                                                   fillLogZBase(FP::undefined()),
                                                                   fillMode(Fill_ModelScan),
                                                                   fillDrawPriority(
                                                                           Graph2DDrawPrimitive::Priority_Fill),
                                                                   continuity(1.0),
                                                                   skipUndefined(false)
{
    fitLegendFont = legendFont;

    if (value["Line/Enable"].exists()) {
        lineEnable = value["Line/Enable"].toBool();
        components[Component_LineEnable] = true;
    }

    if (value["Line/Width"].exists()) {
        double checkLineWidth = value["Line/Width"].toDouble();
        if (!FP::defined(checkLineWidth) || checkLineWidth < 0.0)
            lineWidth = 0.0;
        else
            lineWidth = checkLineWidth;
        components[Component_LineWidth] = true;
    }

    if (value["Line/Style"].exists()) {
        lineStyle = Display::parsePenStyle(value["Line/Style"], lineStyle);
        fitLineStyle = lineStyle;
        components[Component_LineStyle] = true;
    }

    if (INTEGER::defined(value["Line/DrawPriority"].toInt64())) {
        lineDrawPriority = value["Line/DrawPriority"].toInt64();
        components[Component_LineDrawPriority] = true;
    }

    if (value["Symbol/Enable"].exists()) {
        symbolEnable = value["Symbol/Enable"].toBool();
        components[Component_SymbolEnable] = true;
    }

    if (value["Symbol"].exists()) {
        symbol = TraceSymbol(value["Symbol"]);
        components[Component_Symbol] = true;
    }

    if (INTEGER::defined(value["Symbol/DrawPriority"].toInt64())) {
        symbolDrawPriority = value["Symbol/DrawPriority"].toInt64();
        components[Component_SymbolDrawPriority] = true;
    }

    if (value["Color"].exists()) {
        color = Display::parseColor(value["Color"], color);
        fitColor = color;
        components[Component_Color] = true;
    }

    if (value["Legend/Enable"].exists()) {
        legendEntry = value["Legend/Enable"].toBool();
        components[Component_LegendEntry] = true;
    }

    if (value["Legend/Text"].exists()) {
        legendText = value["Legend/Text"].toDisplayString();
        components[Component_LegendText] = true;
    }

    if (value["Legend/Font"].exists()) {
        legendFont = Display::parseFont(value["Legend/Font"], legendFont);
        fitLegendFont = legendFont;
        components[Component_LegendFont] = true;
    }

    if (value["Legend/SortPriority"].exists()) {
        legendSortPriority = (int) value["Legend/SortPriority"].toInt64();
        components[Component_LegendSortPriority] = true;
    }

    if (value["Fit/Model"].exists()) {
        fitModel = Variant::Root(value["Fit/Model"]);
        components[Component_FitModel] = true;
    }

    if (value["Fit/Log/X"].exists()) {
        fitLogXBase = value["Fit/Log/X"].toDouble();
        components[Component_FitLogXBase] = true;
    }

    if (value["Fit/Log/Y"].exists()) {
        fitLogYBase = value["Fit/Log/Y"].toDouble();
        components[Component_FitLogYBase] = true;
    }

    if (value["Fit/Log/Z"].exists()) {
        fitLogZBase = value["Fit/Log/Z"].toDouble();
        components[Component_FitLogZBase] = true;
    }

    if (value["Fit/Mode"].exists()) {
        const auto &mode = value["Fit/Mode"].toString();
        if (Util::equal_insensitive(mode, "x", "independentx", "xindependent")) {
            fitMode = Fit_IndependentX;
            components[Component_FitMode] = true;
        } else if (Util::equal_insensitive(mode, "y", "independenty", "yindependent")) {
            fitMode = Fit_IndependentY;
            components[Component_FitMode] = true;
        } else if (Util::equal_insensitive(mode, "i", "independenti", "iindependent")) {
            fitMode = Fit_IndependentI;
            components[Component_FitMode] = true;
            if (value["Fit/MinI"].exists()) {
                fitMinI = value["Fit/MinI"].toDouble();
            }
            if (value["Fit/MaxI"].exists()) {
                fitMaxI = value["Fit/MaxI"].toDouble();
            }
        }
    }

    if (value["Fit/DisableZ"].exists()) {
        fitDisableZ = value["Fit/DisableZ"].toBool();
        components[Component_FitDisableZ] = true;
    }

    if (value["Fit/Line/Width"].exists()) {
        fitLineWidth = value["Fit/Line/Width"].toDouble();
        if (!FP::defined(fitLineWidth) || fitLineWidth < 0.0)
            fitLineWidth = 2.0;
        components[Component_FitLineWidth] = true;
    }

    if (value["Fit/Line/Style"].exists()) {
        fitLineStyle = Display::parsePenStyle(value["Fit/Line/Style"], fitLineStyle);
        components[Component_FitLineStyle] = true;
    }

    if (INTEGER::defined(value["Fit/DrawPriority"].toInt64())) {
        fitDrawPriority = value["Fit/DrawPriority"].toInt64();
        components[Component_FitDrawPriority] = true;
    }

    if (value["Fit/Color"].exists()) {
        fitColor = Display::parseColor(value["Fit/Color"], fitColor);
        components[Component_FitColor] = true;
    }

    if (value["Fit/Legend/Text"].exists()) {
        fitLegendText = value["Fit/Legend/Text"].toDisplayString();
        components[Component_FitLegendText] = true;
    }

    if (value["Fit/Legend/Font"].exists()) {
        fitLegendFont = Display::parseFont(value["Fit/Legend/Font"], fitLegendFont);
        components[Component_FitLegendFont] = true;
    }

    if (value["Fill/Model"].exists()) {
        fillModel = Variant::Root(value["Fill/Model"]);
        components[Component_FillModel] = true;
    }

    if (value["Fill/Log/X"].exists()) {
        fillLogXBase = value["Fill/Log/X"].toDouble();
        components[Component_FillLogXBase] = true;
    }

    if (value["Fill/Log/Y"].exists()) {
        fillLogYBase = value["Fill/Log/Y"].toDouble();
        components[Component_FillLogYBase] = true;
    }

    if (value["Fill/Log/Z"].exists()) {
        fillLogZBase = value["Fill/Log/Z"].toDouble();
        components[Component_FillLogZBase] = true;
    }

    if (value["Fill/Mode"].exists()) {
        const auto &mode = value["Fill/Mode"].toString();
        if (Util::equal_insensitive(mode, "scan", "modelscan")) {
            fillMode = Fill_ModelScan;
            components[Component_FillMode] = true;
        } else if (Util::equal_insensitive(mode, "grid", "bilinear", "bilineargrid")) {
            fillMode = Fill_BilinearGrid;
            components[Component_FillMode] = true;
        } else if (Util::equal_insensitive(mode, "shadebottom", "shadetobottom", "bottom")) {
            fillMode = Fill_ShadeToBottom;
            components[Component_FillMode] = true;
        } else if (Util::equal_insensitive(mode, "shadetop", "shadetotop", "top")) {
            fillMode = Fill_ShadeToTop;
            components[Component_FillMode] = true;
        } else if (Util::equal_insensitive(mode, "shadeleft", "shadetoleft", "left")) {
            fillMode = Fill_ShadeToLeft;
            components[Component_FillMode] = true;
        } else if (Util::equal_insensitive(mode, "shaderight", "shadetoright", "right")) {
            fillMode = Fill_ShadeToRight;
            components[Component_FillMode] = true;
        } else if (Util::equal_insensitive(mode, "fitshadebottom", "fitshadetobottom",
                                           "fitbottom")) {
            fillMode = Fill_FitShadeToBottom;
            components[Component_FillMode] = true;
        } else if (Util::equal_insensitive(mode, "fitshadetop", "fitshadetotop", "fittop")) {
            fillMode = Fill_FitShadeToTop;
            components[Component_FillMode] = true;
        } else if (Util::equal_insensitive(mode, "fitshadeleft", "fitshadetoleft", "fitleft")) {
            fillMode = Fill_FitShadeToLeft;
            components[Component_FillMode] = true;
        } else if (Util::equal_insensitive(mode, "fitshaderight", "fitshadetoright", "fitright")) {
            fillMode = Fill_FitShadeToRight;
            components[Component_FillMode] = true;
        }
    }

    if (INTEGER::defined(value["Fill/DrawPriority"].toInt64())) {
        fillDrawPriority = value["Fill/DrawPriority"].toInt64();
        components[Component_FillDrawPriority] = true;
    }

    if (value["Axes/X"].exists()) {
        xBinding = value["Axes/X"].toQString();
        components[Component_XBinding] = true;
    }

    if (value["Axes/Y"].exists()) {
        yBinding = value["Axes/Y"].toQString();
        components[Component_YBinding] = true;
    }

    if (value["Axes/Z"].exists()) {
        zBinding = value["Axes/Z"].toQString();
        components[Component_ZBinding] = true;
    }

    if (value["Continuity"].exists()) {
        continuity = value["Continuity"].toDouble();
        if (FP::defined(continuity) && continuity < 0.0)
            continuity = 0.0;
        components[Component_Continuity] = true;
    }

    if (value["SkipUndefined"].exists()) {
        skipUndefined = value["SkipUndefined"].toBool();
        components[Component_SkipUndefined] = true;
    }
}

void TraceParameters2D::save(Variant::Write &value) const
{
    TraceParameters::save(value);

    if (components[Component_LineEnable]) {
        value["Line/Enable"].setBool(lineEnable);
    }

    if (components[Component_LineWidth]) {
        value["Line/Width"].setDouble(lineWidth);
    }

    if (components[Component_LineStyle]) {
        value["Line/Style"].set(Display::formatPenStyle(lineStyle));
    }

    if (components[Component_LineDrawPriority]) {
        value["Line/DrawPriority"].setInt64(lineDrawPriority);
    }

    if (components[Component_Symbol]) {
        value["Symbol"].set(symbol.toConfiguration());
    }

    if (components[Component_SymbolEnable]) {
        value["Symbol/Enable"].setBool(symbolEnable);
    }

    if (components[Component_SymbolDrawPriority]) {
        value["Symbol/DrawPriority"].setInt64(symbolDrawPriority);
    }

    if (components[Component_Color]) {
        value["Color"].set(Display::formatColor(color));
    }

    if (components[Component_LegendEntry]) {
        value["Legend/Enable"].setBool(legendEntry);
    }

    if (components[Component_LegendText]) {
        value["Legend/Text"].setString(legendText);
    }

    if (components[Component_LegendFont]) {
        value["Legend/Font"].set(Display::formatFont(legendFont));
    }

    if (components[Component_LegendSortPriority]) {
        value["Legend/SortPriority"].setInt64(legendSortPriority);
    }

    if (components[Component_FitModel]) {
        value["Fit/Model"].set(fitModel);
    }

    if (components[Component_FitLogXBase]) {
        value["Fit/Log/X"].setDouble(fitLogXBase);
    }

    if (components[Component_FitLogYBase]) {
        value["Fit/Log/Y"].setDouble(fitLogYBase);
    }

    if (components[Component_FitLogZBase]) {
        value["Fit/Log/Z"].setDouble(fitLogZBase);
    }

    if (components[Component_FitMode]) {
        switch (fitMode) {
        case Fit_IndependentX:
            value["Fit/Mode"].setString("X");
            break;
        case Fit_IndependentY:
            value["Fit/Mode"].setString("Y");
            break;
        case Fit_IndependentI:
            value["Fit/Mode"].setString("I");
            value["Fit/MinI"].setDouble(fitMinI);
            value["Fit/MaxI"].setDouble(fitMaxI);
            break;
        }
    }

    if (components[Component_FitDisableZ]) {
        value["Fit/DisableZ"].setBool(fitDisableZ);
    }

    if (components[Component_FitLineWidth]) {
        value["Fit/Line/Width"].setDouble(fitLineWidth);
    }

    if (components[Component_FitLineStyle]) {
        value["Fit/Line/Style"].set(Display::formatPenStyle(fitLineStyle));
    }

    if (components[Component_FitDrawPriority]) {
        value["Fit/DrawPriority"].setInt64(fitDrawPriority);
    }

    if (components[Component_FitColor]) {
        value["Fit/Color"].set(Display::formatColor(fitColor));
    }

    if (components[Component_FitLegendText]) {
        value["Fit/Legend/Text"].setString(fitLegendText);
    }

    if (components[Component_FitLegendFont]) {
        value["Fit/Legend/Font"].set(Display::formatFont(fitLegendFont));
    }

    if (components[Component_FillModel]) {
        value["Fill/Model"].set(fillModel);
    }

    if (components[Component_FillLogXBase]) {
        value["Fill/Log/X"].setDouble(fillLogXBase);
    }

    if (components[Component_FillLogYBase]) {
        value["Fill/Log/Y"].setDouble(fillLogYBase);
    }

    if (components[Component_FillLogZBase]) {
        value["Fill/Log/Z"].setDouble(fillLogZBase);
    }

    if (components[Component_FillDrawPriority]) {
        value["Fill/DrawPriority"].setInt64(fillDrawPriority);
    }

    if (components[Component_FillMode]) {
        switch (fillMode) {
        case Fill_ModelScan:
            value["Fill/Mode"].setString("Scan");
            break;
        case Fill_BilinearGrid:
            value["Fill/Mode"].setString("Grid");
            break;
        case Fill_ShadeToTop:
            value["Fill/Mode"].setString("Top");
            break;
        case Fill_ShadeToBottom:
            value["Fill/Mode"].setString("Bottom");
            break;
        case Fill_ShadeToLeft:
            value["Fill/Mode"].setString("Left");
            break;
        case Fill_ShadeToRight:
            value["Fill/Mode"].setString("Right");
            break;
        case Fill_FitShadeToTop:
            value["Fill/Mode"].setString("FitTop");
            break;
        case Fill_FitShadeToBottom:
            value["Fill/Mode"].setString("FitBottom");
            break;
        case Fill_FitShadeToLeft:
            value["Fill/Mode"].setString("FitLeft");
            break;
        case Fill_FitShadeToRight:
            value["Fill/Mode"].setString("FitRight");
            break;
        }
    }

    if (components[Component_XBinding]) {
        value["Axes/X"].setString(xBinding);
    }

    if (components[Component_YBinding]) {
        value["Axes/Y"].setString(yBinding);
    }

    if (components[Component_ZBinding]) {
        value["Axes/Z"].setString(zBinding);
    }

    if (components[Component_Continuity]) {
        value["Continuity"].setDouble(continuity);
    }

    if (components[Component_SkipUndefined]) {
        value["SkipUndefined"].setBool(skipUndefined);
    }
}

void TraceParameters2D::overlay(const TraceParameters *over)
{
    TraceParameters::overlay(over);

    const TraceParameters2D *top = static_cast<const TraceParameters2D *>(over);

    if (top->components[Component_LineEnable]) {
        lineEnable = top->lineEnable;
        components[Component_LineEnable] = true;
    }

    if (top->components[Component_LineWidth]) {
        lineWidth = top->lineWidth;
        components[Component_LineWidth] = true;
    }

    if (top->components[Component_LineStyle]) {
        lineStyle = top->lineStyle;
        components[Component_LineStyle] = true;
    }

    if (top->components[Component_LineDrawPriority]) {
        lineDrawPriority = top->lineDrawPriority;
        components[Component_LineDrawPriority] = true;
    }

    if (top->components[Component_SymbolEnable]) {
        symbolEnable = top->symbolEnable;
        components[Component_SymbolEnable] = true;
    }

    if (top->components[Component_Symbol]) {
        symbol = top->symbol;
        components[Component_Symbol] = true;
    }

    if (top->components[Component_SymbolDrawPriority]) {
        symbolDrawPriority = top->symbolDrawPriority;
        components[Component_SymbolDrawPriority] = true;
    }

    if (top->components[Component_Color]) {
        color = top->color;
        components[Component_Color] = true;
    }

    if (top->components[Component_LegendEntry]) {
        legendEntry = top->legendEntry;
        components[Component_LegendEntry] = true;
    }

    if (top->components[Component_LegendText]) {
        legendText = top->legendText;
        components[Component_LegendText] = true;
    }

    if (top->components[Component_LegendFont]) {
        legendFont = top->legendFont;
        components[Component_LegendFont] = true;
    }

    if (top->components[Component_LegendSortPriority]) {
        legendSortPriority = top->legendSortPriority;
        components[Component_LegendSortPriority] = true;
    }

    if (top->components[Component_FitModel]) {
        fitModel = top->fitModel;
        components[Component_FitModel] = true;
    }

    if (top->components[Component_FitLogXBase]) {
        fitLogXBase = top->fitLogXBase;
        components[Component_FitLogXBase] = true;
    }

    if (top->components[Component_FitLogYBase]) {
        fitLogYBase = top->fitLogYBase;
        components[Component_FitLogYBase] = true;
    }

    if (top->components[Component_FitLogZBase]) {
        fitLogZBase = top->fitLogZBase;
        components[Component_FitLogZBase] = true;
    }

    if (top->components[Component_FitMode]) {
        fitMode = top->fitMode;
        fitMinI = top->fitMinI;
        fitMaxI = top->fitMaxI;
        components[Component_FitMode] = true;
    }

    if (top->components[Component_FitDisableZ]) {
        fitDisableZ = top->fitDisableZ;
        components[Component_FitDisableZ] = true;
    }

    if (top->components[Component_FitLineWidth]) {
        fitLineWidth = top->fitLineWidth;
        components[Component_FitLineWidth] = true;
    }

    if (top->components[Component_FitLineStyle]) {
        fitLineStyle = top->fitLineStyle;
        components[Component_FitLineStyle] = true;
    }

    if (top->components[Component_FitDrawPriority]) {
        fitDrawPriority = top->fitDrawPriority;
        components[Component_FitDrawPriority] = true;
    }

    if (top->components[Component_FitColor]) {
        fitColor = top->fitColor;
        components[Component_FitColor] = true;
    }

    if (top->components[Component_FitLegendText]) {
        fitLegendText = top->fitLegendText;
        components[Component_FitLegendText] = true;
    }

    if (top->components[Component_FitLegendFont]) {
        fitLegendFont = top->fitLegendFont;
        components[Component_FitLegendFont] = true;
    }

    if (top->components[Component_FillModel]) {
        fillModel = top->fillModel;
        components[Component_FillModel] = true;
    }

    if (top->components[Component_FillLogXBase]) {
        fillLogXBase = top->fillLogXBase;
        components[Component_FillLogXBase] = true;
    }

    if (top->components[Component_FillLogYBase]) {
        fillLogYBase = top->fillLogYBase;
        components[Component_FillLogYBase] = true;
    }

    if (top->components[Component_FillLogZBase]) {
        fillLogZBase = top->fillLogZBase;
        components[Component_FillLogZBase] = true;
    }

    if (top->components[Component_FillDrawPriority]) {
        fillDrawPriority = top->fillDrawPriority;
        components[Component_FillDrawPriority] = true;
    }

    if (top->components[Component_FillMode]) {
        fillMode = top->fillMode;
        components[Component_FillMode] = true;
    }

    if (top->components[Component_XBinding]) {
        xBinding = top->xBinding;
        components[Component_XBinding] = true;
    }

    if (top->components[Component_YBinding]) {
        yBinding = top->yBinding;
        components[Component_YBinding] = true;
    }

    if (top->components[Component_ZBinding]) {
        zBinding = top->zBinding;
        components[Component_ZBinding] = true;
    }

    if (top->components[Component_Continuity]) {
        continuity = top->continuity;
        components[Component_Continuity] = true;
    }

    if (top->components[Component_SkipUndefined]) {
        skipUndefined = top->skipUndefined;
        components[Component_SkipUndefined] = true;
    }
}


TraceHandler2D::TraceHandler2D(std::unique_ptr<TraceParameters2D> &&params) : TraceValueHandler<3>(
        std::move(params)),
                                                                              parameters(
                                                                                      static_cast<TraceParameters2D *>(getParameters())),
                                                                              anyValidPoints(false),
                                                                              runningFit(NULL),
                                                                              fit(),
                                                                              needFitRerun(false),
                                                                              runningFill(NULL),
                                                                              fill(),
                                                                              needFillRerun(false),
                                                                              alwaysHideLegend(
                                                                                      false)
{ }

TraceHandler2D::~TraceHandler2D()
{
    if (runningFit != NULL)
        runningFit->deleteWhenFinished();
    if (runningFill != NULL)
        runningFill->deleteWhenFinished();
}

TraceHandler2D::TraceHandler2D(const TraceHandler2D &other,
                               std::unique_ptr<TraceParameters> &&params) : TraceValueHandler<3>(
        other, std::move(params)),
                                                                            parameters(
                                                                                    static_cast<TraceParameters2D *>(getParameters())),
                                                                            anyValidPoints(
                                                                                    other.anyValidPoints),
                                                                            runningFit(NULL),
                                                                            fit(),
                                                                            needFitRerun(
                                                                                    (other.runningFit ||
                                                                                            other.fit)
                                                                                    ? true
                                                                                    : other.needFitRerun),
                                                                            runningFill(NULL),
                                                                            fill(),
                                                                            needFillRerun(
                                                                                    (other.runningFill ||
                                                                                            other.fill)
                                                                                    ? true
                                                                                    : other.needFillRerun),
                                                                            alwaysHideLegend(
                                                                                    other.alwaysHideLegend)
{ }

TraceHandler2D::Cloned TraceHandler2D::clone() const
{ return Cloned(new TraceHandler2D(*this, parameters->clone())); }

static double applyInputLog(double in, double logBase)
{
    if (!FP::defined(logBase))
        return in;
    if (!FP::defined(in))
        return in;
    if (in <= 0.0)
        return FP::undefined();
    return log(in) / logBase;
}

static double applyInputLogDefined(double in, double logBase)
{
    if (!FP::defined(logBase))
        return in;
    if (in <= 0.0)
        return FP::undefined();
    return log(in) / logBase;
}

static QVector<double> translateDimension(const std::vector<TracePoint<3>> &points,
                                          std::size_t N,
                                          double logBase,
                                          double scale = 1.0)
{
    QVector<double> result;
    result.reserve(points.size());
    for (const auto &it : points) {
        if (FP::defined(it.d[N]))
            result.append(applyInputLogDefined(it.d[N] * scale, logBase));
        else
            result.append(FP::undefined());
    }
    return result;
}

Model *TraceHandler2D::wrapFit(Model *input)
{
    if (input == NULL)
        return NULL;
    QVector<double> inputBases;
    QVector<double> outputBases;
    switch (parameters->getFitMode()) {
    case TraceParameters2D::Fit_IndependentX:
        inputBases << parameters->getFitLogXBase();
        outputBases << parameters->getFitLogYBase();
        break;
    case TraceParameters2D::Fit_IndependentY:
        inputBases << parameters->getFitLogYBase();
        outputBases << parameters->getFitLogXBase();
        break;
    case TraceParameters2D::Fit_IndependentI:
        outputBases << parameters->getFitLogXBase() << parameters->getFitLogYBase();
        break;
    }
    if (FP::defined(getLimits().min[2]) && !parameters->getFitDisableZ())
        outputBases << parameters->getFitLogZBase();
    return LogWrapperMultiple::create(input, inputBases, outputBases);
}

bool TraceHandler2D::startFit(bool deferrable)
{
    if (deferrable && runningFit != NULL) {
        needFitRerun = true;
        return false;
    }
    if (!anyValidPoints) {
        if (runningFit != NULL) {
            runningFit->deleteWhenFinished();
            runningFit = NULL;
        }
        fit.reset();
        return false;
    }
    needFitRerun = false;

    QList<ModelInputConstraints> inputs;
    QList<ModelOutputConstraints> outputs;
    ModelParameters modelParams;

    double lXBase = parameters->getFitLogXBase();
    if (FP::defined(lXBase) && lXBase > 0.0)
        lXBase = log(lXBase);
    else
        lXBase = FP::undefined();
    double lYBase = parameters->getFitLogYBase();
    if (FP::defined(lYBase) && lYBase > 0.0)
        lYBase = log(lYBase);
    else
        lYBase = FP::undefined();
    double lZBase = parameters->getFitLogZBase();
    if (FP::defined(lZBase) && lZBase > 0.0)
        lZBase = log(lZBase);
    else
        lZBase = FP::undefined();

    switch (parameters->getFitMode()) {
    case TraceParameters2D::Fit_IndependentX: {
        double scale = getFitScale();
        if (FP::defined(getLimits().min[0])) {
            inputs.append(
                    ModelInputConstraints(applyInputLogDefined(getLimits().min[0] * scale, lXBase),
                                          applyInputLogDefined(getLimits().max[0] * scale,
                                                               lXBase)));
        } else {
            inputs.append(ModelInputConstraints());
        }
        outputs.append(ModelOutputConstraints());
        modelParams.setDimension(0, translateDimension(getPoints(), 0, lXBase, scale));
        modelParams.setDimension(1, translateDimension(getPoints(), 1, lYBase));
        break;
    }
    case TraceParameters2D::Fit_IndependentY: {
        double scale = getFitScale();
        if (FP::defined(getLimits().min[1])) {
            inputs.append(
                    ModelInputConstraints(applyInputLogDefined(getLimits().min[1] * scale, lYBase),
                                          applyInputLogDefined(getLimits().max[1] * scale,
                                                               lYBase)));
        } else {
            inputs.append(ModelInputConstraints());
        }
        outputs.append(ModelOutputConstraints());
        modelParams.setDimension(0, translateDimension(getPoints(), 1, lYBase, scale));
        modelParams.setDimension(1, translateDimension(getPoints(), 0, lXBase));
        break;
    }
    case TraceParameters2D::Fit_IndependentI:
        outputs.append(ModelOutputConstraints());
        outputs.append(ModelOutputConstraints());
        modelParams.setDimension(0, translateDimension(getPoints(), 0, lXBase));
        modelParams.setDimension(1, translateDimension(getPoints(), 1, lYBase));
        break;
    }
    if (FP::defined(getLimits().min[2]) && !parameters->getFitDisableZ()) {
        outputs.append(ModelOutputConstraints());
        modelParams.setDimension(modelParams.totalDimensions(),
                                 translateDimension(getPoints(), 2, lZBase));
    }

    if (deferrable) {
        runningFit =
                new DeferredModelWrapper(parameters->getFitModel(), inputs, outputs, modelParams);
        QObject::connect(runningFit, &DeferredModelWrapper::modelReady, this,
                         &TraceValueHandlerBase::deferredUpdate, Qt::QueuedConnection);
        runningFit->waitForModelReady(5);
    } else {
        if (runningFit != NULL) {
            runningFit->deleteWhenFinished();
            runningFit = NULL;
        }
        fit = std::shared_ptr<Model>(
                wrapFit(Model::fromConfiguration(parameters->getFitModel(), inputs, outputs,
                                                 modelParams)));
        return true;
    }

    return false;
}

Model *TraceHandler2D::wrapFill(Model *input)
{
    if (input == NULL)
        return NULL;
    QVector<double> inputBases;
    QVector<double> outputBases;
    switch (parameters->getFillMode()) {
    case TraceParameters2D::Fill_ModelScan:
        inputBases << parameters->getFillLogXBase() << parameters->getFillLogYBase();
        outputBases << parameters->getFillLogZBase();
        break;
    case TraceParameters2D::Fill_BilinearGrid:
    case TraceParameters2D::Fill_ShadeToTop:
    case TraceParameters2D::Fill_ShadeToBottom:
    case TraceParameters2D::Fill_ShadeToLeft:
    case TraceParameters2D::Fill_ShadeToRight:
    case TraceParameters2D::Fill_FitShadeToTop:
    case TraceParameters2D::Fill_FitShadeToBottom:
    case TraceParameters2D::Fill_FitShadeToLeft:
    case TraceParameters2D::Fill_FitShadeToRight:
        break;
    }
    return LogWrapperMultiple::create(input, inputBases, outputBases);
}

bool TraceHandler2D::startFill(bool deferrable)
{
    if (deferrable && runningFill != NULL) {
        needFillRerun = true;
        return false;
    }
    if (!anyValidPoints) {
        if (runningFill != NULL) {
            runningFill->deleteWhenFinished();
            runningFill = NULL;
        }
        fill.reset();
        return false;
    }
    needFillRerun = false;

    QList<ModelInputConstraints> inputs;
    QList<ModelOutputConstraints> outputs;
    ModelParameters modelParams;

    double lXBase = parameters->getFillLogXBase();
    if (FP::defined(lXBase) && lXBase > 0.0)
        lXBase = log(lXBase);
    else
        lXBase = FP::undefined();
    double lYBase = parameters->getFillLogYBase();
    if (FP::defined(lYBase) && lYBase > 0.0)
        lYBase = log(lYBase);
    else
        lYBase = FP::undefined();
    double lZBase = parameters->getFillLogZBase();
    if (FP::defined(lZBase) && lZBase > 0.0)
        lZBase = log(lZBase);
    else
        lZBase = FP::undefined();

    switch (parameters->getFillMode()) {
    case TraceParameters2D::Fill_ModelScan:
        inputs.append(ModelInputConstraints(applyInputLog(getLimits().min[0], lXBase),
                                            applyInputLog(getLimits().max[0], lXBase)));
        inputs.append(ModelInputConstraints(applyInputLog(getLimits().min[1], lYBase),
                                            applyInputLog(getLimits().max[1], lYBase)));
        outputs.append(ModelOutputConstraints());
        modelParams.setDimension(0, translateDimension(getPoints(), 0, lXBase));
        modelParams.setDimension(1, translateDimension(getPoints(), 1, lYBase));
        if (FP::defined(getLimits().min[2])) {
            modelParams.setDimension(2, translateDimension(getPoints(), 2, lZBase));
        }
        break;

    case TraceParameters2D::Fill_BilinearGrid:
    case TraceParameters2D::Fill_ShadeToTop:
    case TraceParameters2D::Fill_ShadeToBottom:
    case TraceParameters2D::Fill_ShadeToLeft:
    case TraceParameters2D::Fill_ShadeToRight:
    case TraceParameters2D::Fill_FitShadeToTop:
    case TraceParameters2D::Fill_FitShadeToBottom:
    case TraceParameters2D::Fill_FitShadeToLeft:
    case TraceParameters2D::Fill_FitShadeToRight:
        break;
    }

    if (deferrable) {
        runningFill =
                new DeferredModelWrapper(parameters->getFillModel(), inputs, outputs, modelParams);
        QObject::connect(runningFill, &DeferredModelWrapper::modelReady, this,
                         &TraceValueHandlerBase::deferredUpdate, Qt::QueuedConnection);
        runningFill->waitForModelReady(5);
    } else {
        if (runningFill != NULL) {
            runningFill->deleteWhenFinished();
            runningFill = NULL;
        }
        fill = std::shared_ptr<Model>(wrapFill(
                Model::fromConfiguration(parameters->getFillModel(), inputs, outputs,
                                         modelParams)));
        return true;
    }

    return false;
}

static inline bool pointsContiguous(double endA, double startB, double continuity)
{
    if (!FP::defined(continuity))
        return true;
    if (!FP::defined(endA) || !FP::defined(startB))
        return true;
    return (startB - endA) <= continuity;
}

bool TraceHandler2D::process(const std::vector<TraceDispatch::OutputValue> &incoming,
                             bool deferrable)
{
    bool changed = TraceValueHandler<3>::process(incoming, deferrable);
    if (changed) {
        bool hadValidPoints = anyValidPoints;

        anyValidPoints = false;
        anyIsolatedPoints = false;
        int contiguousPointCount = 0;
        const auto &points = getPoints();
        for (auto it = points.begin(), end = points.end(); it != end; ++it) {
            if (!FP::defined(it->d[1]) || !FP::defined(it->d[0])) {
                if (contiguousPointCount == 1)
                    anyIsolatedPoints = true;
                contiguousPointCount = 0;
                continue;
            }
            anyValidPoints = true;

            if (anyIsolatedPoints)
                break;

            if (contiguousPointCount == 0) {
                contiguousPointCount = 1;
                continue;
            }
            if (!pointsContiguous((it - 1)->end, it->start, parameters->getContinuity())) {
                if (contiguousPointCount == 1) {
                    anyIsolatedPoints = true;
                    break;
                }
                contiguousPointCount = 1;
                continue;
            }
            contiguousPointCount++;
        }

        if (anyValidPoints || hadValidPoints) {
            needFitRerun = true;
            needFillRerun = true;
        }
    }

    return changed;
}

void TraceHandler2D::convertValue(const TraceDispatch::OutputValue &value,
                                  std::vector<TracePoint<3>> &points)
{
    auto priorSize = points.size();
    TraceValueHandler<3>::convertValue(value, points);
    if (priorSize == points.size())
        return;
    for (auto it = points.begin() + priorSize; it != points.end();) {
        if (!FP::defined(it->d[1]) || !FP::defined(it->d[0])) {
            it = points.erase(it);
            continue;
        }
        ++it;
    }
}

/**
 * Inform the trace that the axes have been bound and pass it parameters
 * about them.  This must be called before anything that uses the fits
 * is invoked.
 * 
 * @param x             the X axis or NULL
 * @param y             the Y axis or NULL
 * @param z             the Z axis or NULL
 * @param deferrable    true if the update can be deferred
 * @return              true if the trace has changed
 */
bool TraceHandler2D::axesBound(AxisDimension *x,
                               AxisDimension *y,
                               AxisDimension *z,
                               bool deferrable)
{
    bool changed = false;

    if (!parameters->hasFitLogXBase()) {
        if (x != NULL && !FP::equal(x->getTickLogBase(), parameters->getFitLogXBase())) {
            parameters->setFitLogXBase(x->getTickLogBase());
            needFitRerun = true;
        }
    }
    if (!parameters->hasFitLogYBase()) {
        if (y != NULL && !FP::equal(y->getTickLogBase(), parameters->getFitLogYBase())) {
            parameters->setFitLogYBase(y->getTickLogBase());
            needFitRerun = true;
        }
    }
    if (!parameters->hasFitLogZBase()) {
        if (FP::defined(getLimits().min[2]) &&
                z != NULL &&
                !FP::equal(z->getTickLogBase(), parameters->getFitLogZBase())) {
            parameters->setFitLogZBase(z->getTickLogBase());
            needFitRerun = true;
        }
    }

    if (parameters->hasFitModel()) {
        if (needFitRerun || (!fit && (!deferrable || !runningFit))) {
            if (startFit(deferrable))
                changed = true;
        }
        if (runningFit != NULL) {
            Model *check = runningFit->takeModel();
            if (check != NULL) {
                delete runningFit;
                runningFit = NULL;
                fit = std::shared_ptr<Model>(wrapFit(check));
                changed = true;
            }
        }
    }


    if (!parameters->hasFillLogXBase()) {
        if (x != NULL && !FP::equal(x->getTickLogBase(), parameters->getFillLogXBase())) {
            parameters->setFillLogXBase(x->getTickLogBase());
            needFillRerun = true;
        }
    }
    if (!parameters->hasFillLogYBase()) {
        if (y != NULL && !FP::equal(y->getTickLogBase(), parameters->getFillLogYBase())) {
            parameters->setFillLogYBase(y->getTickLogBase());
            needFillRerun = true;
        }
    }
    if (!parameters->hasFillLogZBase()) {
        if (FP::defined(getLimits().min[2]) &&
                z != NULL &&
                !FP::equal(z->getTickLogBase(), parameters->getFillLogZBase())) {
            parameters->setFillLogZBase(z->getTickLogBase());
            needFillRerun = true;
        }
    }

    if (parameters->hasFillModel()) {
        switch (parameters->getFillMode()) {
        case TraceParameters2D::Fill_ModelScan:
            if (needFillRerun || (!fill && (!deferrable || !runningFill))) {
                if (startFill(deferrable))
                    changed = true;
            }
            break;
        case TraceParameters2D::Fill_BilinearGrid:
        case TraceParameters2D::Fill_ShadeToTop:
        case TraceParameters2D::Fill_ShadeToBottom:
        case TraceParameters2D::Fill_ShadeToLeft:
        case TraceParameters2D::Fill_ShadeToRight:
        case TraceParameters2D::Fill_FitShadeToTop:
        case TraceParameters2D::Fill_FitShadeToBottom:
        case TraceParameters2D::Fill_FitShadeToLeft:
        case TraceParameters2D::Fill_FitShadeToRight:
            break;
        }
        if (runningFill != NULL) {
            Model *check = runningFill->takeModel();
            if (check != NULL) {
                delete runningFill;
                runningFill = NULL;
                fill = std::shared_ptr<Model>(wrapFill(check));
                changed = true;
            }
        }
    }

    return changed;
}

void TraceHandler2D::trimToLast()
{
    TraceValueHandler<3>::trimToLast();
    needFitRerun = true;
    needFillRerun = true;
}

void TraceHandler2D::trimData(double start, double end)
{
    TraceValueHandler<3>::trimData(start, end);
    needFitRerun = true;
    needFillRerun = true;
}

void TraceHandler2D::setVisibleTimeRange(double start, double end)
{
    TraceValueHandler<3>::setVisibleTimeRange(start, end);
    needFitRerun = true;
    needFillRerun = true;
}

QString TraceHandler2D::getLegendText(const std::vector<SequenceName::Set> &inputUnits,
                                      bool showStation,
                                      bool showArchive,
                                      const DisplayDynamicContext &context,
                                      bool *changed) const
{
    if (parameters->hasLegendText()) {
        QString text(parameters->getLegendText());
        context.handleString(text, DisplayDynamicContext::String_GraphLegend, changed);
        return text;
    }
    return TraceValueHandler<3>::getLegendText(inputUnits, showStation, showArchive);
}

namespace {

class GraphLegendItemTrace2D : public GraphLegendItem2D {
    QPointer<TraceHandler2D> handler;
public:
    GraphLegendItemTrace2D(TraceHandler2D *setHandler,
                           const QString &setText,
                           const QColor &setColor,
                           const QFont &setFont,
                           qreal setLineWidth,
                           Qt::PenStyle setStyle,
                           const TraceSymbol &setSymbol,
                           bool setDrawSymbol,
                           bool setDrawSwatch = false) : GraphLegendItem2D(setText, setColor,
                                                                           setFont, setLineWidth,
                                                                           setStyle, setSymbol,
                                                                           setDrawSymbol,
                                                                           setDrawSwatch),
                                                         handler(setHandler)
    { }

    virtual DisplayModificationComponent *createModificationComponent(Graph2D *graph)
    {
        return new Graph2DLegendModificationTrace(graph, handler, getText());
    }

    virtual QString getLegendTooltip() const
    {
        if (handler.isNull())
            return QString();
        return handler->getLegendTooltip();
    }
};

class GraphLegendItemTraceFit2D : public GraphLegendItem2D {
    QPointer<TraceHandler2D> handler;
public:
    GraphLegendItemTraceFit2D(TraceHandler2D *setHandler,
                              const QString &setText,
                              const QColor &setColor,
                              const QFont &setFont,
                              qreal setLineWidth,
                              Qt::PenStyle setStyle,
                              const TraceSymbol &setSymbol,
                              bool setDrawSymbol,
                              bool setDrawSwatch = false) : GraphLegendItem2D(setText, setColor,
                                                                              setFont, setLineWidth,
                                                                              setStyle, setSymbol,
                                                                              setDrawSymbol,
                                                                              setDrawSwatch),
                                                            handler(setHandler)
    { }

    virtual DisplayModificationComponent *createModificationComponent(Graph2D *graph)
    {
        return new Graph2DLegendModificationTraceFit(graph, handler, getText());
    }

    virtual QString getLegendTooltip() const
    {
        if (handler.isNull())
            return QString();
        return handler->getLegendTooltip();
    }
};

}

/**
 * Get the legend items (in order) associated with this trace.
 * 
 * @param inputUnits    the current set of units that went into this trace
 * @param showStation   if set then show the station in the legend
 * @param showArchive   if set then show the archive in the legend
 * @param context       the dynamic context
 * @param changed       the changed value passed to the dynamic context
 * @return              the in order legend items
 */
std::vector<std::shared_ptr<LegendItem>> TraceHandler2D::getLegend(const std::vector<
        SequenceName::Set> &inputUnits,
                                                                   bool showStation,
                                                                   bool showArchive,
                                                                   const DisplayDynamicContext &context,
                                                                   bool *changed)
{
    if (!parameters->getLegendEntry() || alwaysHideLegend)
        return {};

    QString legendText(getLegendText(inputUnits, showStation, showArchive, context, changed));
    /* No "normal" points, but maybe a fit */
    if (legendText.isEmpty() ||
            FP::defined(getLimits().min[2]) ||
            !anyValidPoints ||
            (!parameters->getLineEnable() &&
                    !parameters->getSymbolEnable() &&
                    (!parameters->hasFillMode() ||
                            (parameters->getFillMode() != TraceParameters2D::Fill_ModelScan &&
                                    parameters->getFillMode() !=
                                            TraceParameters2D::Fill_BilinearGrid)))) {
        if (!anyValidPoints || !parameters->hasFitModel())
            return {};
        if (parameters->hasFitLegendText()) {
            legendText = parameters->getFitLegendText();
            context.handleString(legendText, DisplayDynamicContext::String_GraphLegend, changed);
        } else {
            if (fit) {
                legendText = tr("%1: %2", "fit label format").arg(legendText, getFitLegend(fit));
            } else if (FP::defined(getLimits().min[2])) {
                /* No fit (yet) so don't add a legend line if this is
                 * a gradient based trace. */
                return {};
            }
        }
        if (legendText.isEmpty())
            return {};
        return {std::make_shared<GraphLegendItemTraceFit2D>(this, legendText,
                                                            parameters->getFitColor(),
                                                            parameters->getFitLegendFont(),
                                                            parameters->getFitLineWidth(),
                                                            parameters->getFitLineStyle(),
                                                            TraceSymbol(), false)};
    }

    std::vector<std::shared_ptr<LegendItem> > result;

    if (!legendText.isEmpty()) {
        result.emplace_back(
                std::make_shared<GraphLegendItemTrace2D>(this, legendText, parameters->getColor(),
                                                         parameters->getLegendFont(),
                                                         parameters->getLineEnable()
                                                         ? parameters->getLineWidth() : 0.0,
                                                         parameters->getLineEnable()
                                                         ? parameters->getLineStyle() : Qt::NoPen,
                                                         parameters->getSymbol(),
                                                         parameters->getSymbolEnable()));
    }

    if (parameters->hasFitModel()) {
        QString text;
        if (parameters->hasFitLegendText()) {
            text = parameters->getFitLegendText();
            context.handleString(text, DisplayDynamicContext::String_GraphLegend, changed);
        } else {
            if (fit) {
                text = tr(" %1", "fit sub label format").arg(getFitLegend(fit));
            } else {
                text = tr(" Fit", "fit not done yet label");
            }
        }
        if (!text.isEmpty()) {
            result.emplace_back(std::make_shared<GraphLegendItemTraceFit2D>(this, text,
                                                                            parameters->getFitColor(),
                                                                            parameters->getFitLegendFont(),
                                                                            parameters->getFitLineWidth(),
                                                                            parameters->getFitLineStyle(),
                                                                            TraceSymbol(), false));
        }
    }

    return result;
}

/**
 * Get the display title of this trace.
 * 
 * @param inputUnits    the current set of units that went into this trace
 * @param showStation   if set then show the station in the legend
 * @param showArchive   if set then show the archive in the legend
 * @param context       the dynamic context
 * @return              the display title
 */
QString TraceHandler2D::getDisplayTitle(const std::vector<SequenceName::Set> &inputUnits,
                                        bool showStation,
                                        bool showArchive,
                                        const DisplayDynamicContext &context) const
{
    QString title(getLegendText(inputUnits, showStation, showArchive, context, NULL));
    if (!title.isEmpty())
        return title;

    if (parameters->hasFitModel()) {
        if (parameters->hasFitLegendText()) {
            return context.handleString(parameters->getFitLegendText(),
                                        DisplayDynamicContext::String_GraphLegend, NULL);
        } else if (fit) {
            return getFitLegend(fit);
        }
    }

    return TraceValueHandler<3>::getLegendText(inputUnits, showStation, showArchive);
}

static Axis2DSide fillToAxisSide(TraceParameters2D::FillMode mode)
{
    switch (mode) {
    case TraceParameters2D::Fill_ShadeToTop:
        return Axis2DTop;
    case TraceParameters2D::Fill_ShadeToBottom:
        return Axis2DBottom;
    case TraceParameters2D::Fill_ShadeToLeft:
        return Axis2DLeft;
    case TraceParameters2D::Fill_ShadeToRight:
        return Axis2DRight;
    case TraceParameters2D::Fill_FitShadeToTop:
        return Axis2DTop;
    case TraceParameters2D::Fill_FitShadeToBottom:
        return Axis2DBottom;
    case TraceParameters2D::Fill_FitShadeToLeft:
        return Axis2DLeft;
    case TraceParameters2D::Fill_FitShadeToRight:
        return Axis2DRight;
    default:
        break;
    }
    return Axis2DBottom;
}

/**
 * Create draw items for the trace.
 * 
 * @param x         the final X transformer
 * @param y         the final Y transformer
 * @param z         the final Z transformer (may be undefined if the trace has no Z limits)
 * @param gradient  the color (Z) gradient
 */
std::vector<
        std::unique_ptr<Graph2DDrawPrimitive>> TraceHandler2D::createDraw(const AxisTransformer &x,
                                                                          const AxisTransformer &y,
                                                                          const AxisTransformer &z,
                                                                          const TraceGradient &gradient) const
{
    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> result;

    if (parameters->hasFillMode() || fill) {
        TraceGradient fillGradient(gradient);
        if (!FP::defined(z.getScreenMin())) {
            if (parameters->getColor() == QColor(0, 0, 0)) {
                fillGradient = TraceGradient();
            } else {
                fillGradient = TraceGradient(parameters->getColor());
            }
        }
        switch (parameters->getFillMode()) {
        case TraceParameters2D::Fill_ModelScan:
            if (fill) {
                result.emplace_back(new GraphPainter2DFillModelScan(fill, x, y, z, fillGradient,
                                                                    (quintptr) this,
                                                                    parameters->getFillDrawPriority()));
            }
            break;
        case TraceParameters2D::Fill_BilinearGrid:
            if (anyValidPoints && FP::defined(z.getRealMin()) && FP::defined(getLimits().min[2])) {
                result.emplace_back(
                        new GraphPainter2DFillBilinearGrid(getPoints(), x, y, z, gradient,
                                                           (quintptr) this,
                                                           parameters->getFillDrawPriority()));
            }
            break;
        case TraceParameters2D::Fill_ShadeToTop:
        case TraceParameters2D::Fill_ShadeToBottom:
        case TraceParameters2D::Fill_ShadeToLeft:
        case TraceParameters2D::Fill_ShadeToRight:
            if (anyValidPoints) {
                if (FP::defined(z.getRealMin()) && FP::defined(getLimits().min[2])) {
                    result.emplace_back(new GraphPainter2DFillShadeGradient(getPoints(), x, y, z,
                                                                            fillToAxisSide(
                                                                                    parameters->getFillMode()),
                                                                            gradient,
                                                                            parameters->getContinuity(),
                                                                            parameters->getSkipUndefined(),
                                                                            parameters->getFillDrawPriority()));
                } else {
                    QColor color(parameters->getColor());
                    if (!parameters->hasColor() &&
                            (parameters->getLineEnable() || parameters->getSymbolEnable()))
                        color = color.lighter(125);
                    result.emplace_back(new GraphPainter2DFillShade(getPoints(), x, y,
                                                                    fillToAxisSide(
                                                                            parameters->getFillMode()),
                                                                    color,
                                                                    parameters->getContinuity(),
                                                                    parameters->getSkipUndefined(),
                                                                    parameters->getFillDrawPriority()));
                }
            }
            break;
        case TraceParameters2D::Fill_FitShadeToTop:
        case TraceParameters2D::Fill_FitShadeToBottom:
        case TraceParameters2D::Fill_FitShadeToLeft:
        case TraceParameters2D::Fill_FitShadeToRight:
            if (fit) {
                std::unique_ptr<Graph2DDrawPrimitive> add;
                if (FP::defined(z.getRealMin()) && FP::defined(getLimits().min[2])) {
                    switch (parameters->getFitMode()) {
                    case TraceParameters2D::Fit_IndependentX:
                        add.reset(new GraphPainter2DShadeFitXGradient(fit, x, y, z, fillToAxisSide(
                                parameters->getFillMode()), gradient, getFitScale(),
                                                                      parameters->getFillDrawPriority()));
                        break;
                    case TraceParameters2D::Fit_IndependentY:
                        add.reset(new GraphPainter2DShadeFitXGradient(fit, x, y, z, fillToAxisSide(
                                parameters->getFillMode()), gradient, getFitScale(),
                                                                      parameters->getFillDrawPriority()));
                        break;
                    case TraceParameters2D::Fit_IndependentI:
                        add.reset(new GraphPainter2DShadeFitIGradient(fit, x, y, z, fillToAxisSide(
                                parameters->getFillMode()), gradient, parameters->getFitMaxI(),
                                                                      parameters->getFitMaxI(),
                                                                      parameters->getFillDrawPriority()));
                        break;
                    }
                } else {
                    QColor color(parameters->getFitColor());
                    if (!parameters->hasFitColor() &&
                            (parameters->getLineEnable() || parameters->getSymbolEnable()))
                        color = color.lighter(125);
                    switch (parameters->getFitMode()) {
                    case TraceParameters2D::Fit_IndependentX:
                        add.reset(new GraphPainter2DShadeFitX(fit, x, y, fillToAxisSide(
                                parameters->getFillMode()), color, getFitScale(),
                                                              parameters->getFillDrawPriority()));
                        break;
                    case TraceParameters2D::Fit_IndependentY:
                        add.reset(new GraphPainter2DShadeFitY(fit, x, y, fillToAxisSide(
                                parameters->getFillMode()), color, getFitScale(),
                                                              parameters->getFillDrawPriority()));
                        break;
                    case TraceParameters2D::Fit_IndependentI:
                        add.reset(new GraphPainter2DShadeFitI(fit, x, y, fillToAxisSide(
                                parameters->getFillMode()), color, parameters->getFitMaxI(),
                                                              parameters->getFitMaxI(),
                                                              parameters->getFillDrawPriority()));
                        break;
                    }
                }
                if (add)
                    result.emplace_back(std::move(add));
            }
            break;
        }
    }

    if (anyValidPoints) {
        if (parameters->getLineEnable()) {
            if (FP::defined(z.getRealMin()) && FP::defined(getLimits().min[2])) {
                result.emplace_back(new GraphPainter2DLinesGradient(getPoints(), x, y, z,
                                                                    parameters->getLineWidth(),
                                                                    parameters->getLineStyle(),
                                                                    gradient,
                                                                    (parameters->getSymbolEnable()
                                                                     ? TraceSymbol()
                                                                     : parameters->getSymbol()),
                                                                    parameters->getContinuity(),
                                                                    parameters->getSkipUndefined(),
                                                                    (quintptr) this,
                                                                    parameters->getLineDrawPriority()));
            } else {
                result.emplace_back(
                        new GraphPainter2DLines(getPoints(), x, y, parameters->getLineWidth(),
                                                parameters->getLineStyle(), parameters->getColor(),
                                                (parameters->getSymbolEnable() ? TraceSymbol()
                                                                               : parameters->getSymbol()),
                                                parameters->getContinuity(),
                                                parameters->getSkipUndefined(), (quintptr) this,
                                                parameters->getLineDrawPriority()));
            }
        }

        if (parameters->getSymbolEnable()) {
            if (FP::defined(z.getRealMin()) && FP::defined(getLimits().min[2])) {
                result.emplace_back(new GraphPainter2DPointsGradient(getPoints(), x, y, z,
                                                                     parameters->getSymbol(),
                                                                     gradient, (quintptr) this,
                                                                     parameters->getSymbolDrawPriority()));
            } else {
                result.emplace_back(
                        new GraphPainter2DPoints(getPoints(), x, y, parameters->getSymbol(),
                                                 parameters->getColor(), (quintptr) this,
                                                 parameters->getSymbolDrawPriority()));
            }
        }
    }

    if (fit) {
        if (FP::defined(z.getRealMin()) &&
                FP::defined(getLimits().min[2]) &&
                !parameters->getFitDisableZ()) {
            switch (parameters->getFitMode()) {
            case TraceParameters2D::Fit_IndependentX:
                result.emplace_back(
                        new GraphPainter2DFitXGradient(fit, x, y, z, parameters->getFitLineWidth(),
                                                       parameters->getFitLineStyle(), gradient,
                                                       getFitScale(), (quintptr) this,
                                                       parameters->getFitDrawPriority()));
                break;
            case TraceParameters2D::Fit_IndependentY:
                result.emplace_back(
                        new GraphPainter2DFitXGradient(fit, x, y, z, parameters->getFitLineWidth(),
                                                       parameters->getFitLineStyle(), gradient,
                                                       getFitScale(), (quintptr) this,
                                                       parameters->getFitDrawPriority()));
                break;
            case TraceParameters2D::Fit_IndependentI:
                result.emplace_back(
                        new GraphPainter2DFitIGradient(fit, x, y, z, parameters->getFitLineWidth(),
                                                       parameters->getFitLineStyle(), gradient,
                                                       parameters->getFitMaxI(),
                                                       parameters->getFitMaxI(), (quintptr) this,
                                                       parameters->getFitDrawPriority()));
                break;
            }
        } else {
            switch (parameters->getFitMode()) {
            case TraceParameters2D::Fit_IndependentX:
                result.emplace_back(new GraphPainter2DFitX(fit, x, y, parameters->getFitLineWidth(),
                                                           parameters->getFitLineStyle(),
                                                           parameters->getFitColor(), getFitScale(),
                                                           (quintptr) this,
                                                           parameters->getFitDrawPriority()));
                break;
            case TraceParameters2D::Fit_IndependentY:
                result.emplace_back(new GraphPainter2DFitY(fit, x, y, parameters->getFitLineWidth(),
                                                           parameters->getFitLineStyle(),
                                                           parameters->getFitColor(), getFitScale(),
                                                           (quintptr) this,
                                                           parameters->getFitDrawPriority()));
                break;
            case TraceParameters2D::Fit_IndependentI:
                result.emplace_back(new GraphPainter2DFitI(fit, x, y, parameters->getFitLineWidth(),
                                                           parameters->getFitLineStyle(),
                                                           parameters->getFitColor(),
                                                           parameters->getFitMaxI(),
                                                           parameters->getFitMaxI(),
                                                           (quintptr) this,
                                                           parameters->getFitDrawPriority()));
                break;
            }
        }
    }

    return std::move(result);
}

/**
 * Get the scale to multiply the (X or Y) independent variable by before
 * passing to the fit model.
 * 
 * @return the fit scale
 */
double TraceHandler2D::getFitScale() const
{ return 1.0; }

/**
 * Get the base legend line text for a fit entry.
 * 
 * @param fit   the fit
 * @return      the legend text
 */
QString TraceHandler2D::getFitLegend(const std::shared_ptr<Algorithms::Model> &fit) const
{
    Q_ASSERT(fit);
    ModelLegendParameters lp;
    lp.setConfidence(0.95);
    ModelLegendEntry legend(fit->legend(lp));
    return legend.getLine();
}


/**
 * Get an colors in use and the units for any that are requested to be
 * assigned.
 * 
 * @param inputUnits        the inputs that went into this trace
 * @param requestColors     the output list of units to request colors for
 * @param claimedColors     the output list of already claimed colors
 */
void TraceHandler2D::getColors(const std::vector<SequenceName::Set> &inputUnits,
                               std::vector<TraceUtilities::ColorRequest> &requestColors,
                               std::deque<QColor> &claimedColors) const
{
    if (!anyValidPoints)
        return;
    if (FP::defined(getLimits().min[2]))
        return;
    std::size_t wantColors = 0;
    if (parameters->getLineEnable() || parameters->getSymbolEnable() || fill) {
        if (parameters->hasColor())
            claimedColors.emplace_back(parameters->getColor());
        else
            wantColors++;
    }
    if (fit) {
        if (parameters->hasFitColor())
            claimedColors.emplace_back(parameters->getFitColor());
        else
            wantColors++;
    }
    if (wantColors == 0)
        return;

    for (std::ptrdiff_t i = inputUnits.size() - 1; i >= 0; i--) {
        if (inputUnits[i].empty())
            continue;
        TraceUtilities::ColorRequest req;
        const auto &check = inputUnits[i];
        if (check.size() == 1) {
            req.unit = *check.begin();
        } else {
            std::vector<SequenceName> sorted(check.begin(), check.end());
            std::sort(sorted.begin(), sorted.end(), SequenceName::OrderDisplay());
            req.unit = std::move(sorted.front());
        }
        for (int j = 0; j < 2; j++) {
            req.wavelength = getWavelength(j);
            if (FP::defined(req.wavelength))
                break;
        }
        requestColors.emplace_back(std::move(req));
        wantColors--;
        break;
    }
    for (std::size_t i = 0; i < wantColors; i++) {
        requestColors.emplace_back();
    }
}

/**
 * Claim the colors assigned to the units returned from 
 * getColors( const QVector<QSet<SequenceName> > &,
 * QList<TraceUtilities::ColorRequest> &, QList<QColor> & ).  This should 
 * remove the colors used from the front of the list.
 * 
 * @param colors        the input and output list of assigned colors
 */
void TraceHandler2D::claimColors(std::deque<QColor> &colors) const
{
    if (!anyValidPoints)
        return;
    if (FP::defined(getLimits().min[2]))
        return;
    if (!colors.empty() &&
            anyValidPoints &&
            (parameters->getLineEnable() || parameters->getSymbolEnable() || fill) &&
            !parameters->hasColor()) {
        parameters->setColor(std::move(colors.front()));
        colors.pop_front();
    }
    if (!colors.empty() && fit && !parameters->hasFitColor()) {
        parameters->setFitColor(std::move(colors.front()));
        colors.pop_front();
    }
}

/**
 * Insert all used trace symbols into the given set of types.
 * 
 * @param types the used symbol types
 */
void TraceHandler2D::insertUsedSymbols(std::unordered_set<TraceSymbol::Type,
                                                          TraceSymbol::TypeHash> &types) const
{
    if (!anyValidPoints)
        return;
    if (!parameters->getSymbolEnable())
        return;
    TraceSymbol::Type t = parameters->getSymbol().getType();
    if (t == TraceSymbol::Unselected)
        return;
    if (!parameters->hasSymbol())
        return;
    types.insert(t);
}

/**
 * Bind a symbol for the trace, if needed.
 * 
 * @param types the types of symbols in use
 */
void TraceHandler2D::bindSymbol(std::unordered_set<TraceSymbol::Type, TraceSymbol::TypeHash> &types)
{
    if (!anyValidPoints)
        return;
    if (!parameters->getSymbolEnable() && !anyIsolatedPoints)
        return;
    TraceSymbol::Type t = parameters->getSymbolReference().getType();
    if (t == TraceSymbol::Unselected) {
        parameters->getSymbolReference().selectTypeIfNeeded(types);
    } else if (!parameters->hasSymbol()) {
        if (!types.count(t)) {
            types.insert(t);
            return;
        }
        parameters->getSymbolReference().setType(TraceSymbol::Unselected);
        parameters->getSymbolReference().selectTypeIfNeeded(types);
    }
}

TraceOutput2D::TraceOutput2D(TraceHandler2D *setHandler, const QString &setTitle) : handler(
        setHandler), title(setTitle)
{ }

TraceOutput2D::~TraceOutput2D() = default;

QString TraceOutput2D::getTitle() const
{ return title; }

QList<QAction *> TraceOutput2D::getMenuActions(QWidget *parent)
{
    if (handler.isNull())
        return {};
    QList<QAction *> result;

    QAction *act;

    act = new QAction(tr("Enable &Line", "Context|EnableLine"), this);
    act->setToolTip(tr("Enable or disable connecting lines between points."));
    act->setStatusTip(tr("Enable trace lines"));
    act->setWhatsThis(
            tr("This enables or disables the connecting lines between the points of this trace."));
    act->setCheckable(true);
    act->setChecked(handler->parameters->getLineEnable());
    QObject::connect(act, &QAction::toggled, this, [this](bool enable) {
        if (handler.isNull())
            return;
        static_cast<TraceParameters2D *>(handler->overrideParameters())->overrideLineEnable(enable);
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("Enable &Points", "Context|EnablePoints"), this);
    act->setToolTip(tr("Enable or disable the discrete point symbols."));
    act->setStatusTip(tr("Enable trace points"));
    act->setWhatsThis(tr("This enables or disables the the symbols drawn at each discrete point."));
    act->setCheckable(true);
    act->setChecked(handler->parameters->getSymbolEnable());
    QObject::connect(act, &QAction::toggled, this, [this](bool enable) {
        if (handler.isNull())
            return;
        static_cast<TraceParameters2D *>(handler->overrideParameters())->overrideSymbolEnable(
                enable);
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("&Color", "Context|SetTraceColor"), this);
    act->setToolTip(tr("Change the color of the trace."));
    act->setStatusTip(tr("Set the trace color"));
    act->setWhatsThis(tr("Set the color of the trace."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        QColorDialog dialog(handler->parameters->getColor(), parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Set Trace Color"));
        dialog.setOption(QColorDialog::ShowAlphaChannel);
        if (dialog.exec() != QDialog::Accepted)
            return;
        static_cast<TraceParameters2D *>(handler->overrideParameters())->overrideColor(
                dialog.currentColor());
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("Line &width", "Context|SetTraceWidth"), this);
    act->setToolTip(tr("Change the width of the line in use."));
    act->setStatusTip(tr("Set the trace width"));
    act->setWhatsThis(tr("Set the width of the connecting line of the trace."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        bool ok = false;
        auto width = QInputDialog::getDouble(parent, tr("Set Trace Line Width"),
                                             tr("Width (0 = hairline)"),
                                             handler->parameters->getLineWidth(), 0, 1000, 0, &ok);
        if (!ok || !FP::defined(width))
            return;
        static_cast<TraceParameters2D *>(handler->overrideParameters())->overrideLineWidth(width);
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    result.append(Display::selectPenStyle(handler->parameters->getLineStyle(),
                                          [this](Qt::PenStyle style) {
                                              if (handler.isNull())
                                                  return;
                                              static_cast<TraceParameters2D *>(handler->overrideParameters())
                                                      ->overrideLineStyle(style);
                                              handler->mergeOverrideParameters();
                                              emit changed();
                                          }, this)->menuAction());

    result.append(handler->parameters->getSymbol().customizeMenu([this](const TraceSymbol &symbol) {
        if (handler.isNull())
            return;
        static_cast<TraceParameters2D *>(handler->overrideParameters())->overrideTraceSymbol(
                symbol);
        handler->mergeOverrideParameters();
        emit changed();
    }, this)->menuAction());

    return result;
}

void TraceOutput2D::setLegendAlwaysHide(bool enable)
{
    if (handler.isNull())
        return;
    if (!handler->setLegendAlwaysHide(enable))
        return;
    emit changed();
}

/**
 * The output controllers (if any) for the handler
 * 
 * @param inputUnits    the current set of units that went into this trace
 * @param showStation   if set then show the station in the legend
 * @param showArchive   if set then show the archive in the legend
 * @param context       the dynamic context
 * @return a            list of output controllers
 */
std::vector<std::shared_ptr<DisplayOutput>> TraceHandler2D::getOutputs(const std::vector<
        SequenceName::Set> &inputUnits,
                                                                       bool showStation,
                                                                       bool showArchive,
                                                                       const DisplayDynamicContext &context)
{
    if (!anyValidPoints)
        return {};
    return {std::make_shared<TraceOutput2D>(this,
                                            getDisplayTitle(inputUnits, showStation, showArchive,
                                                            context))};
}

TraceSet2D::TraceSet2D() = default;

TraceSet2D::~TraceSet2D() = default;

TraceSet2D::TraceSet2D(const TraceSet2D &other) = default;

TraceSet2D::Cloned TraceSet2D::clone() const
{ return Cloned(new TraceSet2D(*this)); }

namespace {
struct TraceSet2DSortItem {
    TraceHandler2D *handler;
    std::vector<SequenceName::Set> units;

    bool operator<(const TraceSet2DSortItem &other) const
    {
        if (handler->getLegendSortPriority() != other.handler->getLegendSortPriority()) {
            return handler->getLegendSortPriority() < other.handler->getLegendSortPriority();
        }

        std::size_t max = std::min<std::size_t>(units.size(), other.units.size());
        if (!max) return false;
        for (std::ptrdiff_t i = max - 1; i >= 0; i--) {
            const auto &a = units[i];
            const auto &b = other.units[i];
            if (a.size() == 1 && b.size() == 1) {
                const auto &ua = *a.begin();
                const auto &ub = *b.begin();
                if (ua == ub)
                    continue;
                return SequenceName::OrderDisplay()(ua, ub);
            } else if (!a.empty() && !b.empty()) {
                std::vector<SequenceName> sa(a.begin(), a.end());
                std::sort(sa.begin(), sa.end(), SequenceName::OrderLogical());
                std::vector<SequenceName> sb(b.begin(), b.end());
                std::sort(sb.begin(), sb.end(), SequenceName::OrderLogical());
                for (std::size_t j = 0, nUnits = std::min<std::size_t>(sa.size(), sb.size());
                        j < nUnits;
                        ++j) {
                    const auto &ua = sa[j];
                    const auto &ub = sb[j];
                    if (ua == ub)
                        continue;
                    return SequenceName::OrderDisplay()(ua, ub);
                }
            }
        }
        return false;
    }
};
}

/**
 * Get the handlers in a legend suitable sort order.
 * 
 * @return a list of handlers
 */
std::vector<TraceHandler2D *> TraceSet2D::getSortedHandlers() const
{
    std::vector<TraceSet2DSortItem> sort;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            TraceSet2DSortItem add;
            add.handler = static_cast<TraceHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);
            sort.emplace_back(std::move(add));
        }
    }
    std::stable_sort(sort.begin(), sort.end());
    std::vector<TraceHandler2D *> result;
    result.reserve(sort.size());
    for (const auto &it : sort) {
        result.emplace_back(it.handler);
    }
    return result;
}

/**
 * Add all trace items to the legend.
 * 
 * @param legend    the target legend
 * @param context   the context to use
 * @return          true if the legend has dynamically changed (as reported by the context, not necessarily if other parts have changed)
 */
bool TraceSet2D::addLegend(Legend &legend, const DisplayDynamicContext &context) const
{
    std::vector<TraceSet2DSortItem> sort;
    bool showStation = false;
    bool showArchive = false;
    bool changed = false;
    SequenceName::Component priorStation;
    SequenceName::Component priorArchive;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            TraceSet2DSortItem add;
            add.handler = static_cast<TraceHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);

            for (const auto &dim : add.units) {
                for (const auto &unit : dim) {
                    if (!showStation) {
                        if (priorStation.empty()) {
                            priorStation = unit.getStation();
                        } else if (priorStation != unit.getStation()) {
                            showStation = true;
                        }
                    }
                    if (!showArchive) {
                        if (priorArchive.empty()) {
                            priorArchive = unit.getArchive();
                        } else if (priorArchive != unit.getArchive()) {
                            showArchive = true;
                        }
                    }
                }
            }

            sort.emplace_back(std::move(add));
        }
    }
    std::stable_sort(sort.begin(), sort.end());
    for (const auto &it : sort) {
        legend.append(it.handler->getLegend(it.units, showStation, showArchive, context, &changed));
    }
    return changed;
}

/**
 * Get the title of a given handler.  This is used to generate strings
 * to refer to the trace for user interaction.
 * 
 * @param trace     the trace to get the title for
 * @param context   the context to use
 */
QString TraceSet2D::handlerTitle(TraceHandler2D *trace, const DisplayDynamicContext &context) const
{
    bool showStation = false;
    bool showArchive = false;
    SequenceName::Component priorStation;
    SequenceName::Component priorArchive;
    std::vector<SequenceName::Set> handlerUnits;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            const auto &units = dispatch->getUnits(j);
            if (static_cast<TraceHandler2D *>(handlers[j].get()) == trace)
                handlerUnits = units;

            for (const auto &dim : units) {
                for (const auto &unit : dim) {
                    if (!showStation) {
                        if (priorStation.empty()) {
                            priorStation = unit.getStation();
                        } else if (priorStation != unit.getStation()) {
                            showStation = true;
                        }
                    }
                    if (!showArchive) {
                        if (priorArchive.empty()) {
                            priorArchive = unit.getArchive();
                        } else if (priorArchive != unit.getArchive()) {
                            showArchive = true;
                        }
                    }
                }
            }
        }
    }

    return trace->getDisplayTitle(handlerUnits, showStation, showArchive, context);
}

/**
 * Get the origin for a given handler.  This is used to generate additional
 * disambiguation information.
 * 
 * @param trace     the trace to get the origin for
 * @param context   the context to use
 */
QString TraceSet2D::handlerOrigin(TraceHandler2D *trace, const DisplayDynamicContext &) const
{
    return trace->getOriginText();
}

/**
 * Get an colors in use and the units for any that are requested to be
 * assigned.
 * 
 * @param requestColors     the output list of units to request colors for
 * @param claimedColors     the output list of already claimed colors
 */
void TraceSet2D::getColors(std::vector<TraceUtilities::ColorRequest> &requestColors,
                           std::deque<QColor> &claimedColors) const
{
    std::vector<TraceSet2DSortItem> sort;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            TraceSet2DSortItem add;
            add.handler = static_cast<TraceHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);
            sort.emplace_back(std::move(add));
        }
    }
    std::stable_sort(sort.begin(), sort.end());
    for (const auto &it : sort) {
        it.handler->getColors(it.units, requestColors, claimedColors);
    }
}

/**
 * Claim the colors assigned to the units returned from 
 * getColors( QList<TraceUtilities::ColorRequest> &, QList<QColor> & ).  This 
 * should remove the colors used from the front of the list.
 * 
 * @param colors        the input and output list of assigned colors
 */
void TraceSet2D::claimColors(std::deque<QColor> &colors) const
{
    std::vector<TraceSet2DSortItem> sort;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            TraceSet2DSortItem add;
            add.handler = static_cast<TraceHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);
            sort.emplace_back(std::move(add));
        }
    }
    std::stable_sort(sort.begin(), sort.end());
    for (const auto &it : sort) {
        it.handler->claimColors(colors);
    }
}

/**
 * The output controllers (if any) for the set of traces.
 * 
 * @param context   the context to use
 * @return          the list ouf output handlers in order
 */
std::vector<
        std::shared_ptr<DisplayOutput>> TraceSet2D::getOutputs(const DisplayDynamicContext &context)
{
    std::vector<TraceSet2DSortItem> sort;
    bool showStation = false;
    bool showArchive = false;
    SequenceName::Component priorStation;
    SequenceName::Component priorArchive;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            TraceSet2DSortItem add;
            add.handler = static_cast<TraceHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);

            for (const auto &dim : add.units) {
                for (const auto &unit : dim) {
                    if (!showStation) {
                        if (priorStation.empty()) {
                            priorStation = unit.getStation();
                        } else if (priorStation != unit.getStation()) {
                            showStation = true;
                        }
                    }
                    if (!showArchive) {
                        if (priorArchive.empty()) {
                            priorArchive = unit.getArchive();
                        } else if (priorArchive != unit.getArchive()) {
                            showArchive = true;
                        }
                    }
                }
            }

            sort.emplace_back(std::move(add));
        }
    }
    std::stable_sort(sort.begin(), sort.end());

    std::vector<std::shared_ptr<DisplayOutput>> result;
    for (const auto &it : sort) {
        Util::append(it.handler->getOutputs(it.units, showStation, showArchive, context), result);
    }
    return result;
}

std::unique_ptr<TraceParameters> TraceSet2D::parseParameters(const Variant::Read &value) const
{ return std::unique_ptr<TraceParameters>(new TraceParameters2D(value)); }

TraceSet2D::CreatedHandler TraceSet2D::createHandler(std::unique_ptr<
        TraceParameters> &&parameters) const
{
    return CreatedHandler(new TraceHandler2D(std::unique_ptr<TraceParameters2D>(
            static_cast<TraceParameters2D *>(parameters.release()))));
}

namespace Internal {

Graph2DLegendModificationTrace::Graph2DLegendModificationTrace(Graph2D *graph,
                                                               const QPointer<
                                                                       TraceHandler2D> &setHandler,
                                                               const QString &setLegendText)
        : Graph2DLegendModification(graph), handler(setHandler), legendText(setLegendText)
{ }

Graph2DLegendModificationTrace::~Graph2DLegendModificationTrace() = default;

QList<QAction *> Graph2DLegendModificationTrace::getMenuActions(QWidget *parent)
{
    QList<QAction *> result(Graph2DLegendModification::getMenuActions(parent));
    if (handler.isNull())
        return result;

    QAction *act;

    act = new QAction(tr("Trace &Title", "Context|SetTraceTitle"), this);
    act->setToolTip(tr("Change the trace title."));
    act->setStatusTip(tr("Set the trace title"));
    act->setWhatsThis(tr("Change or disable the trace title in the legend."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        bool ok = false;
        auto title =
                QInputDialog::getText(parent, tr("Set Trace Title", "Context|TraceTitleDialog"),
                                      tr("Title:", "Context|TraceTitleLabel"), QLineEdit::Normal,
                                      legendText, &ok);
        if (!ok)
            return;
        static_cast<TraceParameters2D *>(handler->overrideParameters())->overrideLegendText(title);
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("Trace &Color", "Context|SetTraceColor"), this);
    act->setToolTip(tr("Change the color of the trace."));
    act->setStatusTip(tr("Set the trace color"));
    act->setWhatsThis(tr("Set the color of the trace."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        QColorDialog dialog(handler->parameters->getColor(), parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Set Trace Color"));
        dialog.setOption(QColorDialog::ShowAlphaChannel);
        if (dialog.exec() != QDialog::Accepted)
            return;
        static_cast<TraceParameters2D *>(handler->overrideParameters())->overrideColor(
                dialog.currentColor());
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("Legend Entry &Font", "Context|SetTraceFont"), this);
    act->setToolTip(tr("Change the trace legend entry font."));
    act->setStatusTip(tr("Set the trace font"));
    act->setWhatsThis(tr("Set the font used for the selected legend entry text."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        QFontDialog dialog(handler->parameters->getLegendFont(), parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Set Trace Legend Font"));
        if (dialog.exec() != QDialog::Accepted)
            return;
        static_cast<TraceParameters2D *>(handler->overrideParameters())->overrideLegendFont(
                dialog.selectedFont());
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    return result;
}

Graph2DLegendModificationTraceFit::Graph2DLegendModificationTraceFit(Graph2D *graph,
                                                                     const QPointer<
                                                                             TraceHandler2D> &setHandler,
                                                                     const QString &setLegendText)
        : Graph2DLegendModification(graph), handler(setHandler), legendText(setLegendText)
{ }

Graph2DLegendModificationTraceFit::~Graph2DLegendModificationTraceFit() = default;

QList<QAction *> Graph2DLegendModificationTraceFit::getMenuActions(QWidget *parent)
{
    QList<QAction *> result(Graph2DLegendModification::getMenuActions(parent));
    if (handler.isNull())
        return result;

    QAction *act;

    act = new QAction(tr("&Fit Title", "Context|SetTraceFitTitle"), this);
    act->setToolTip(tr("Change the trace fit title."));
    act->setStatusTip(tr("Set the trace fit title"));
    act->setWhatsThis(tr("Change or disable the trace fit title in the legend."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        bool ok = false;
        auto title =
                QInputDialog::getText(parent, tr("Set Fit Title", "Context|TraceFitTitleDialog"),
                                      tr("Title:", "Context|TraceFitTitleLabel"), QLineEdit::Normal,
                                      legendText, &ok);
        if (!ok)
            return;
        static_cast<TraceParameters2D *>(handler->overrideParameters())->overrideFitLegendText(
                title);
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("Fit Legend Fo&nt", "Context|SetTraceFitFont"), this);
    act->setToolTip(tr("Change the fit legend entry font."));
    act->setStatusTip(tr("Set the fit font"));
    act->setWhatsThis(tr("Set the font used for the selected legend entry fit text."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        QFontDialog dialog(handler->parameters->getFitLegendFont(), parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Set Fit Legend Font"));
        if (dialog.exec() != QDialog::Accepted)
            return;
        static_cast<TraceParameters2D *>(handler->overrideParameters())->overrideFitLegendFont(
                dialog.selectedFont());
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("Fit C&olor", "Context|SetTraceFitColor"), this);
    act->setToolTip(tr("Change the color of the fit."));
    act->setStatusTip(tr("Set the fit color"));
    act->setWhatsThis(tr("Set the color of the fit."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        QColorDialog dialog(handler->parameters->getFitColor(), parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Set Fit Color"));
        dialog.setOption(QColorDialog::ShowAlphaChannel);
        if (dialog.exec() != QDialog::Accepted)
            return;
        static_cast<TraceParameters2D *>(handler->overrideParameters())->overrideFitColor(
                dialog.currentColor());
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("Fit Line w&idth", "Context|SetFitWidth"), this);
    act->setToolTip(tr("Change the width of the fit line."));
    act->setStatusTip(tr("Set the fit width"));
    act->setWhatsThis(tr("Set the width of the fit line of the trace."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        bool ok = false;
        auto width = QInputDialog::getDouble(parent, tr("Set Fit Line Width"),
                                             tr("Width (0 = hairline)"),
                                             handler->parameters->getFitLineWidth(), 0, 1000, 0,
                                             &ok);
        if (!ok || !FP::defined(width))
            return;
        static_cast<TraceParameters2D *>(handler->overrideParameters())->overrideFitLineWidth(
                width);
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    {
        auto menu = Display::selectPenStyle(handler->parameters->getFitLineStyle(),
                                            [this](Qt::PenStyle style) {
                                                static_cast<TraceParameters2D *>(handler->overrideParameters())
                                                        ->overrideFitLineStyle(style);
                                                handler->mergeOverrideParameters();
                                                emit changed();
                                            }, this);
        menu->setTitle(tr("Fit Sty&le", "Context|SetFitPenStyle"));
        result.append(menu->menuAction());
    }

    return result;
}

}

}
}
