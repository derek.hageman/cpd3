/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QDateTime>
#include <QSettings>

#include "core/timeutils.hxx"
#include "core/number.hxx"
#include "graphing/axistickgenerator.hxx"
#include "guicore/guiformat.hxx"
#include "algorithms/statistics.hxx"

using namespace CPD3::GUI;
using namespace CPD3::Algorithms;

namespace CPD3 {
namespace Graphing {

/** @file graphing/axistickgenerator.hxx
 * Provides the general interface for axis division control algorithms.
 */

AxisTickGenerator::Tick::Tick() : point(FP::undefined())
{ }

AxisTickGenerator::Tick::Tick(double setPoint,
                              const QString &setLabel,
                              const QString &setSecondaryLabel,
                              const QString &setBlockLabel) : point(setPoint),
                                                              label(setLabel),
                                                              secondaryLabel(setSecondaryLabel),
                                                              blockLabel(setBlockLabel)
{ }

AxisTickGenerator::Tick::Tick(const Tick &) = default;

AxisTickGenerator::Tick &AxisTickGenerator::Tick::operator=(const Tick &) = default;

AxisTickGenerator::Tick::Tick(Tick &&) = default;

AxisTickGenerator::Tick &AxisTickGenerator::Tick::operator=(Tick &&) = default;


AxisTickGenerator::AxisTickGenerator(QObject *parent) : QObject(parent)
{ }

AxisTickGenerator::~AxisTickGenerator()
{ }

void AxisTickGenerator::generateLabels(std::vector<Tick> &ticks)
{
    static const double stopEpsilon = 1E-5;
    static const int maximumDisplayDigits = 5;

    double min = FP::undefined();
    double max = FP::undefined();

    int maxSignificantDigits = 0;
    int maximumTrailingZeros = 0;
    int maximumLeadingZeros = 0;
    int maximumFractionalDigits = 0;
    int exponentDigits = 1;
    bool hadNegativeExponent = false;
    for (const auto &it : ticks) {
        if (!FP::defined(it.point))
            continue;
        if (!FP::defined(min)) {
            min = it.point;
            max = it.point;
        } else {
            if (min > it.point) min = it.point;
            if (max < it.point) max = it.point;
        }

        int digits;
        int base;
        if (it.point == 0.0) {
            digits = 1;
            base = 1;
        } else {
            double value = fabs(it.point);
            double lbase = log10(value);
            if (lbase < 0) {
                base = (int) floor(lbase);
            } else {
                base = (int) ceil(lbase);
                /* Handle exactly 1.0 */
                if (base == 0)
                    base = 1;
            }

            for (digits = 1; digits < 5; digits++) {
                int baseDigits = -base + digits;
                if (base >= 0)
                    baseDigits++;
                double power = pow(10.0, baseDigits);
                double normalized = value * power;
                normalized -= floor(value * power / 10.0 + stopEpsilon) * 10.0;
                if (fabs(normalized) < stopEpsilon)
                    break;
            }

            exponentDigits = qMax(exponentDigits, digits);
            if (base < 0)
                hadNegativeExponent = true;
        }

        maxSignificantDigits = qMax(maxSignificantDigits, digits);
        if (base < 0) {
            maximumLeadingZeros = qMax(maximumLeadingZeros, -(base + 1));
            maximumFractionalDigits = qMax(maximumFractionalDigits, digits);
        } else {
            maximumTrailingZeros = qMax(maximumTrailingZeros, base - digits);
            maximumFractionalDigits = qMax(maximumFractionalDigits, digits - base);
        }
    }

    if (!FP::defined(min))
        return;

    if (maximumTrailingZeros + maxSignificantDigits > maximumDisplayDigits ||
            (max - min) <= pow(10.0, -maxSignificantDigits - 1)) {
        int decimalDigits = maxSignificantDigits - 1;
        QString positiveExponent;
        if (hadNegativeExponent)
            positiveExponent = QString('+');
        for (auto &it : ticks) {
            if (!FP::defined(it.point))
                continue;
            it.label = FP::scientificFormat(it.point, decimalDigits, exponentDigits, QString('E'),
                                            QString(), positiveExponent);
        }
        return;
    }

    for (auto &it : ticks) {
        if (!FP::defined(it.point))
            continue;
        it.label = NumberFormat(1, maximumLeadingZeros + maximumFractionalDigits).apply(it.point,
                                                                                        QChar());
    }
}

/**
 * Create a new tick generator using the CPD3 GUI application settings to
 * determine the displayed formats of the times.
 * 
 * @param parent    the object parent
 */
AxisTickGeneratorTime::AxisTickGeneratorTime(QObject *parent) : AxisTickGenerator(parent),
                                                                settings(NULL),
                                                                timeFormat()
{
    settings = new QSettings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION, this);
}

/**
 * Create a new tick generator using the given application settings.  May
 * be NULL, in which case defaults are used.
 * 
 * @param useSettings   the settings to use or NULL
 * @param parent        the object parent
 */
AxisTickGeneratorTime::AxisTickGeneratorTime(QSettings *useSettings, QObject *parent)
        : AxisTickGenerator(parent), settings(useSettings), timeFormat()
{ }

/**
 * Create a new tick generator using the CPD3 GUI application settings to
 * determine the displayed formats of the times.
 * 
 * @param overrideFormat    the time format override
 * @param parent    the object parent
 */
AxisTickGeneratorTime::AxisTickGeneratorTime(const QString &overrideFormat, QObject *parent)
        : AxisTickGenerator(parent), settings(NULL), timeFormat(overrideFormat.toLower())
{
    settings = new QSettings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION, this);
}

/**
 * Create a new tick generator using the given application settings.  May
 * be NULL, in which case defaults are used.
 * 
 * @param overrideFormat    the time format override
 * @param useSettings   the settings to use or NULL
 * @param parent        the object parent
 */
AxisTickGeneratorTime::AxisTickGeneratorTime(const QString &overrideFormat,
                                             QSettings *useSettings,
                                             QObject *parent) : AxisTickGenerator(parent),
                                                                settings(useSettings),
                                                                timeFormat(overrideFormat.toLower())
{ }

AxisTickGeneratorTime::~AxisTickGeneratorTime()
{ }


const double AxisTickGeneratorTime::compareEpsilon = 0.00001;

AxisTickGenerator::Tick AxisTickGeneratorTime::makeTick(double t, Time::LogicalTimeUnit unit) const
{
    AxisTickGenerator::Tick tick;
    tick.point = t;

    QString format(timeFormat);
    if (format.isEmpty() && settings != NULL)
        format = settings->value("time/displayformat").toString().toLower();
    if (format == "datetime") {
        QDateTime dt(Time::toDateTime(t));
        switch (unit) {
        case Time::Year:
            tick.label = dt.toString("yyyy");
            break;
        case Time::Quarter:
            /* Fudge this for leap years, where these aren't aligned */
            if (QDate::isLeapYear(dt.date().year()) && dt.date().month() != 1) {
                tick.label = dt.toString("MM-dd");
                tick.blockLabel = dt.toString("yyyy");
                break;
            }
        case Time::Month:
            tick.label = dt.toString("M");
            tick.blockLabel = dt.toString("yyyy");
            break;
        case Time::Week:
            tick.label = dt.toString("MM-dd");
            tick.blockLabel = dt.toString("yyyy");
            break;
        case Time::Day:
            tick.label = dt.toString("d");
            tick.blockLabel = dt.toString("yyyy-MM");
            break;
        case Time::Hour:
            tick.label = dt.toString("h");
            tick.blockLabel = dt.toString("yyyy-MM-dd");
            break;
        case Time::Minute:
            tick.label = dt.toString("m");
            tick.blockLabel = dt.toString("yyyy-MM-ddThh");
            break;
        case Time::Second:
            tick.label = dt.toString("s");
            tick.blockLabel = dt.toString("yyyy-MM-ddThh:mm");
            break;
        case Time::Millisecond:
            tick.label = dt.toString("z");
            tick.blockLabel = dt.toString("yyyy-MM-ddThh:mm:ss");
            break;
        }
    } else {
        QDateTime dt(Time::toDateTime(t));
        double yearStart = Time::fromDateTime(
                QDateTime(QDate(dt.date().year(), 1, 1), QTime(0, 0, 0), Qt::UTC));
        double doy = (t - yearStart) / 86400.0 + 1.0;

        switch (unit) {
        case Time::Year:
            tick.label = dt.toString("yyyy");
            break;
        case Time::Quarter: {
            int year;
            int qtr;
            Time::containingQuarter(t, &year, &qtr);
            tick.label = QString::number(qRound(doy));
            tick.secondaryLabel = tr("Q%1", "quarter format").arg(qtr);
            tick.blockLabel = QString::number(year).rightJustified(4, '0');
            break;
        }
        case Time::Month:
            tick.label = QString::number(qRound(doy));
            tick.secondaryLabel = dt.toString("MMM");
            tick.blockLabel = dt.toString("yyyy");
            break;
        case Time::Week: {
            tick.label = QString::number(qRound(doy));
            tick.secondaryLabel = tr("W%1", "week format").arg(dt.date().weekNumber());
            tick.blockLabel = dt.toString("yyyy");
            break;
        }
        case Time::Day:
            tick.label = QString::number(qRound(doy));
            tick.blockLabel = dt.toString("yyyy");
            break;
        case Time::Hour:
            tick.label = Time::toYearDOY(t, QString(), unit, QChar(), Time::DOYOnly);
            tick.secondaryLabel = dt.toString("hh:mm");
            tick.blockLabel = dt.toString("yyyy");
            break;
        case Time::Minute:
            tick.label = Time::toYearDOY(t, QString(), unit, QChar(), Time::DOYOnly);
            tick.secondaryLabel = dt.toString("hh:mm");
            tick.blockLabel = dt.toString("yyyy");
            break;
        case Time::Second:
            tick.label = Time::toYearDOY(t, QString(), unit, QChar(), Time::DOYOnly);
            tick.secondaryLabel = dt.toString("hh:mm:ss");
            tick.blockLabel = dt.toString("yyyy");
            break;
        case Time::Millisecond:
            tick.label = NumberFormat(1, 7).apply(doy, QChar());
            tick.secondaryLabel = dt.toString("s.zzz");
            tick.blockLabel = dt.toString("yyyy");
            break;
        }
    }
    return tick;
}

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorTime::runLogical(double min,
                                                                       double max,
                                                                       int flags,
                                                                       double start,
                                                                       Time::LogicalTimeUnit unit,
                                                                       int count) const
{

    start = Time::floor(start, unit, count);
    if (start + compareEpsilon < min) {
        if (!(flags & AxisTickGenerator::IncludeBeforeMin))
            start = Time::logical(start, unit, count, true, false);
    } else if (start - compareEpsilon > min) {
        if ((flags & AxisTickGenerator::IncludeBeforeMin))
            start = Time::logical(start, unit, -count, true, true);
    } else {
        if (!(flags & AxisTickGenerator::IncludeMin)) {
            start = Time::logical(start, unit, count, true, false);
        }
    }

    std::vector<AxisTickGenerator::Tick> result;
    while (start - compareEpsilon < max) {
        /* Insert the min and max as exact values, so comparisons are easy
         * elsewhere */
        if (fabs(start - min) < compareEpsilon) {
            if (flags & AxisTickGenerator::IncludeMin)
                result.emplace_back(makeTick(min, unit));
        } else if (fabs(start - max) < compareEpsilon) {
            if (flags & AxisTickGenerator::IncludeMax)
                result.emplace_back(makeTick(max, unit));
            /* If we hit the max exactly, then we're done regardless since
             * we wouldn't need to insert anything after it either way */
            return result;
        } else {
            result.emplace_back(makeTick(start, unit));
        }
        start = Time::logical(start, unit, count, true, false);
    }

    if ((flags & AxisTickGenerator::IncludeAfterMax))
        result.emplace_back(makeTick(start, unit));

    return result;
}

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorTime::generate(double min,
                                                                     double max,
                                                                     int flags) const
{
    if (max == min) return {};
    Q_ASSERT(FP::defined(max));
    Q_ASSERT(FP::defined(min));

    QDateTime tMin(Time::toDateTime(min)); /* Implicit floor() */
    QDateTime tMax(Time::toDateTime(ceil(max)));

    int yearMin = tMin.date().year();
    int yearMax = tMax.date().year();

    if (floor((double) yearMax / 100.0) - floor((double) yearMin / 100.0) > 1.0) {
        /* Century */
        return runLogical(min, max, flags, Time::fromDateTime(
                QDateTime(QDate((int) floor((double) yearMin / 100.0) * 100, 1, 1), QTime(0, 0, 0),
                          Qt::UTC)), Time::Year, 100);
    }
    if (floor((double) yearMax / 10.0) - floor((double) yearMin / 10.0) > 1.0) {
        /* Decade */
        return runLogical(min, max, flags, Time::fromDateTime(
                QDateTime(QDate((int) floor((double) yearMin / 10.0) * 10, 1, 1), QTime(0, 0, 0),
                          Qt::UTC)), Time::Year, 10);
    }
    if (floor((double) yearMax / 4.0) - floor((double) yearMin / 4.0) > 1.0) {
        /* 2-years */
        return runLogical(min, max, flags, Time::fromDateTime(
                QDateTime(QDate((int) floor((double) yearMin / 2.0) * 2, 1, 1), QTime(0, 0, 0),
                          Qt::UTC)), Time::Year, 2);
    }

    double dT = max - min;

    if (yearMax - yearMin > 1 && dT >= 63072000) {
        return runLogical(min, max, flags, min, Time::Year, 1);
    }
    if (dT >= 31449600) {
        return runLogical(min, max, flags, min, Time::Quarter, 1);
    }

    int monMin = tMin.date().month();
    int monMax = tMax.date().month();

    double yearStartMin =
            Time::fromDateTime(QDateTime(QDate(yearMin, 1, 1), QTime(0, 0, 0), Qt::UTC));
    double doyMin = (min - yearStartMin) / 86400.0 + 1.0;
    int idoyMin = (int) floor(doyMin);
    double yearStartMax =
            Time::fromDateTime(QDateTime(QDate(yearMax, 1, 1), QTime(0, 0, 0), Qt::UTC));
    double doyMax = (max - yearStartMax) / 86400.0 + 1.0;
    int idoyMax = (int) floor(doyMax);


    if ((yearMax != yearMin && ((monMax + 12) - monMin) > 2) ||
            monMax - monMin > 2 ||
            (idoyMin == 1 && idoyMax == 91 && QDate::isLeapYear(yearMin))) {
        /* Above is a special case for quarter subdivisions on leap years */
        return runLogical(min, max, flags, min, Time::Month, 1);
    }
    if (dT >= 1209600) {
        return runLogical(min, max, flags, min, Time::Week, 1);
    }

    if (dT >= 259200) {
        return runLogical(min, max, flags, min, Time::Day, 1);
    }
    if (dT >= 64800) {
        return runLogical(min, max, flags, floor(min / 21600.0) * 21600.0, Time::Hour, 6);
    }
    if (dT >= 10800) {
        return runLogical(min, max, flags, min, Time::Hour, 1);
    }
    if (dT >= 2700) {
        return runLogical(min, max, flags, floor(min / 900.0) * 900.0, Time::Minute, 15);
    }
    if (dT >= 1800) {
        return runLogical(min, max, flags, floor(min / 600.0) * 600.0, Time::Minute, 10);
    }
    if (dT >= 900) {
        return runLogical(min, max, flags, floor(min / 300.0) * 300.0, Time::Minute, 5);
    }
    if (dT >= 180) {
        return runLogical(min, max, flags, min, Time::Minute, 1);
    }
    if (dT >= 45) {
        return runLogical(min, max, flags, floor(min / 15.0) * 15.0, Time::Second, 15);
    }
    if (dT >= 30) {
        return runLogical(min, max, flags, floor(min / 10.0) * 10.0, Time::Second, 10);
    }
    if (dT >= 15) {
        return runLogical(min, max, flags, floor(min / 5.0) * 5.0, Time::Second, 5);
    }
    if (dT > 2) {
        return runLogical(min, max, flags, min, Time::Second, 1);
    }

    /* Handle sub-second divisions as numeric values */
    double secondStart = (double) qRound((max + min) * 0.5);
    auto ticks = AxisTickGeneratorDecimal().generate((min - secondStart) * 1000.0,
                                                     (max - secondStart) * 1000.0, flags);
    QString base(tr("Milliseconds from %1", "sub second block label").arg(
            GUITime::formatTime(secondStart, settings, timeFormat, true)));
    for (auto &tick : ticks) {
        tick.point = tick.point / 1000.0 + secondStart;
        tick.blockLabel = base;
    }
    return ticks;
}

/**
 * Create a new tick generator with a maximum of five significant digits.
 * 
 * @param parent    the object parent
 */
AxisTickGeneratorDecimal::AxisTickGeneratorDecimal(QObject *parent) : AxisTickGenerator(parent),
                                                                      maxSignificantDigits(5)
{ }

/**
 * Create a new tick generator with the given maximum number of significant
 * digits.
 * 
 * @param setMaxSigDigits   the number of digits, must be at least 1
 * @param parent            the object parent
 */
AxisTickGeneratorDecimal::AxisTickGeneratorDecimal(int setMaxSigDigits, QObject *parent)
        : AxisTickGenerator(parent), maxSignificantDigits(setMaxSigDigits)
{
    Q_ASSERT(setMaxSigDigits > 0);
}

AxisTickGeneratorDecimal::~AxisTickGeneratorDecimal()
{ }


const int AxisTickGeneratorDecimal::minTotalTicks = 3;
const int AxisTickGeneratorDecimal::maxTotalTicks = 7;
const double AxisTickGeneratorDecimal::hitZeroWeight = 2.0;
const double AxisTickGeneratorDecimal::epsilonNormalized = 1E-10;
const double AxisTickGeneratorDecimal::stepSizes[3] = {1.0, 5.0, 2.0};
const double AxisTickGeneratorDecimal::stepSizeWeights[3] = {1.0, 0.9, 0.8};
const double
        AxisTickGeneratorDecimal::totalTickWeights[8] = {0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.5};

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorDecimal::generateAligned(double min,
                                                                               double max,
                                                                               int flags) const
{
    if (max == min) return {};

    int baseDigits;
    if (max == 0.0) {
        baseDigits = (int) floor(log10(fabs(min)));
    } else if (min == 0.0) {
        baseDigits = (int) floor(log10(fabs(max)));
    } else {
        baseDigits = (int) floor(qMax(log10(fabs(max)), log10(fabs(min))));
    }

    double baseScalar = pow(10.0, (double) baseDigits);
    double epsilon = baseScalar * epsilonNormalized;

    double normalizedMin = min / baseScalar;
    double normalizedMax = max / baseScalar;

    double bestWeight = 0.0;
    double bestAbsoluteMin = 0.0;
    double bestStep = 0.0;
    int bestDigDigits = 0;
    for (int sigDigits = 0; sigDigits < maxSignificantDigits; sigDigits++) {
        double shift = pow(10.0, (double) sigDigits);
        double effectiveMin = normalizedMin * shift;
        double effectiveMax = normalizedMax * shift;

        for (int stepIdx = 0;
                stepIdx < (int) (sizeof(stepSizes) / sizeof(stepSizes[0]));
                stepIdx++) {
            double stepSize = stepSizes[stepIdx];

            double tickMin = ceil(effectiveMin / stepSize) * stepSize;
            double tickMax = floor(effectiveMax / stepSize) * stepSize;

            double deltaTicks = tickMax - tickMin;
            if (deltaTicks < epsilon)
                continue;

            int nSteps = (int) floor(deltaTicks / stepSize) + 1;
            if (nSteps < minTotalTicks || nSteps > maxTotalTicks)
                continue;

            double weight = stepSizeWeights[stepIdx] + totalTickWeights[nSteps];
            if (tickMax >= -epsilon && tickMin <= epsilon) {
                double stepsToZero = -tickMin / stepSize;
                if (floor(stepsToZero) == ceil(stepsToZero)) {
                    weight += hitZeroWeight;
                }
            }

            if (weight <= bestWeight)
                continue;

            bestWeight = weight;
            bestStep = (stepSize / shift) * baseScalar;
            bestDigDigits = sigDigits;

            /* Don't extend this if it's already at the min */
            double reversedTickMin = (tickMin / shift) * baseScalar;
            if (fabs(reversedTickMin - min) < epsilon) {
                bestAbsoluteMin = reversedTickMin;
            } else {
                bestAbsoluteMin = (floor(tickMin - stepSize) / shift) * baseScalar;
            }
        }
    }

    if (bestWeight <= 0.0) {
        bestAbsoluteMin = min;
        bestStep = (max - min) / 4.0;
    }

    std::vector<AxisTickGenerator::Tick> ticks;
    for (int tickIdx = 0;; tickIdx++) {
        double pos = bestAbsoluteMin + tickIdx * bestStep;

        if (pos < (min - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeBeforeMin))
                continue;
        } else if (pos > (max + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeAfterMax)) {
                if (!(flags & AxisTickGenerator::IncludeMax))
                    break;
                continue;
            }
        } else if (fabs(pos - min) < epsilon) {
            if (!(flags & AxisTickGenerator::IncludeMin))
                continue;
            pos = min;
        } else if (fabs(pos - max) < epsilon) {
            if (!(flags & AxisTickGenerator::IncludeMax)) {
                if (!(flags & AxisTickGenerator::IncludeAfterMax))
                    break;
                continue;
            }
            pos = max;
        }

        AxisTickGenerator::Tick t;
        t.point = pos;
        ticks.emplace_back(std::move(t));

        if (pos >= max)
            break;
    }

    /* Takes more significant digits than we allowed, so don't bother 
     * cleaning it up */
    if (bestWeight <= 0.0) {
        int nExp;
        if (baseDigits == 0)
            nExp = 1;
        else
            nExp = (int) ceil(log10((double) abs(baseDigits)));
        if (nExp <= 0) nExp = 1;

        if (bestAbsoluteMin < 0.0) {
            for (auto &tick : ticks) {
                tick.label =
                        FP::scientificFormat(tick.point, maxSignificantDigits, nExp, QString('E'),
                                             QString(' '),
                                             baseDigits < 0 ? QString('+') : QString());
            }
        } else {
            for (auto &tick : ticks) {
                tick.label =
                        FP::scientificFormat(tick.point, maxSignificantDigits, nExp, QString('E'),
                                             QString(), baseDigits < 0 ? QString('+') : QString());
            }
        }
    } else {
        int integerDigits = 0;
        int decimalDigits = 0;
        if (baseDigits < maxSignificantDigits) {
            if (baseDigits >= 0) {
                integerDigits = baseDigits + 1;
                decimalDigits = bestDigDigits - baseDigits;
            } else if (baseDigits > -maxSignificantDigits) {
                integerDigits = 1;
                decimalDigits = -baseDigits + bestDigDigits;
                if (decimalDigits > maxSignificantDigits) {
                    decimalDigits = 0;
                    integerDigits = 0;
                }
            }
        }

        bool useExponential = false;
        if (integerDigits == 0 && decimalDigits == 0) {
            useExponential = true;
            if (baseDigits == 0)
                integerDigits = 1;
            else
                integerDigits = (int) ceil(log10((double) abs(baseDigits)));
            if (integerDigits <= 0) integerDigits = 1;
            decimalDigits = bestDigDigits;
        }
        if (decimalDigits < 0)
            decimalDigits = 0;

        QString positivePad;
        /*if (bestAbsoluteMin < 0.0)
            positivePad = QString(' ');*/

        for (auto &tick : ticks) {
            if (useExponential) {
                tick.label =
                        FP::scientificFormat(tick.point, decimalDigits, integerDigits, QString('E'),
                                             positivePad,
                                             baseDigits < 0 ? QString('+') : QString());
            } else {
                tick.label = NumberFormat(1, decimalDigits).apply(tick.point, QChar());
            }
        }
    }
    return ticks;
}

const double AxisTickGeneratorDecimal::minor4Divisible[4] = {1, 2, 4, 8};

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorDecimal::generateFixed(double min,
                                                                             double max,
                                                                             int flags) const
{
    double delta = max - min;
    if (delta <= 0.0) return {};

    int baseDigits = (int) floor(log10(delta));
    double normalizedDelta = delta / pow(10.0, (double) baseDigits);

    int nDivisions = 5;
    for (int i = 0; i < (int) (sizeof(minor4Divisible) / sizeof(minor4Divisible[0])); i++) {
        if (fabs(normalizedDelta - minor4Divisible[i]) <= epsilonNormalized) {
            nDivisions = 4;
            break;
        }
    }

    std::vector<AxisTickGenerator::Tick> ticks;
    double step = delta / nDivisions;
    for (int i = 0; i <= nDivisions; i++) {
        double pos = min + i * step;
        if (i == 0) {
            if (!(flags & AxisTickGenerator::IncludeMin))
                continue;
            pos = min;
        } else if (i == nDivisions) {
            if (!(flags & AxisTickGenerator::IncludeMax))
                continue;
            pos = max;
        }

        AxisTickGenerator::Tick t;
        t.point = pos;
        ticks.emplace_back(std::move(t));
    }

    generateLabels(ticks);

    return ticks;
}

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorDecimal::generate(double min,
                                                                        double max,
                                                                        int flags) const
{
    Q_ASSERT(FP::defined(max));
    Q_ASSERT(FP::defined(min));

    if (flags & AxisTickGenerator::FirstLevelHandling) {
        return generateAligned(min, max, flags);
    } else {
        return generateFixed(min, max, flags);
    }
}

AxisTickGeneratorLog::AxisTickGeneratorLog(QObject *parent) : AxisTickGenerator(parent), base(10.0)
{ lbase = log((double) base); }

AxisTickGeneratorLog::AxisTickGeneratorLog(double setBase, QObject *parent) : AxisTickGenerator(
        parent), base(setBase)
{
    Q_ASSERT(FP::defined(base) && base > 1.0);
    lbase = log(base);
}

AxisTickGeneratorLog::~AxisTickGeneratorLog()
{ }

const double AxisTickGeneratorLog::epsilonNormalized = 1E-10;

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorLog::generateAligned(double min,
                                                                           double max,
                                                                           int flags) const
{
    if (min <= 0.0 && max <= 0.0)
        return {};

    if (min <= 0.0)
        min = max * pow(base, -5.0);

    int lMin = (int) floor(log(min) / lbase);
    int lMax = (int) ceil(log(max) / lbase);

    std::vector<AxisTickGenerator::Tick> ticks;

    /* If the major ticks are entirely outside the axis, try
     * promoting the minor. */
    if (lMin + 1 >= lMax) {
        double posMin = pow(base, (double) lMin);
        double epsilonMin = epsilonNormalized * posMin;
        double posMax = pow(base, (double) lMax);
        double epsilonMax = epsilonNormalized * posMax;
        if (posMin < min - epsilonMin && posMax > max + epsilonMax) {
            double step = 0;
            double epsilon = epsilonNormalized * fabs(max);
            for (int effectiveLog = lMin; effectiveLog <= lMin; effectiveLog--) {
                step = pow(base, effectiveLog);
                if (min + step < (max - epsilon))
                    break;
            }

            double roundedMin = floor(min / step) * step;
            for (int tick = 0, nTicks = (int) ceil((max - roundedMin) / step);
                    tick <= nTicks;
                    tick++) {
                double pos = roundedMin + step * tick;
                epsilon = epsilonNormalized * fabs(pos);

                if (pos < (min - epsilon)) {
                    if (!(flags & AxisTickGenerator::IncludeBeforeMin))
                        continue;
                } else if (pos > (max + epsilon)) {
                    if (!(flags & AxisTickGenerator::IncludeAfterMax)) {
                        if (!(flags & AxisTickGenerator::IncludeMax))
                            break;
                        continue;
                    }
                } else if (fabs(pos - min) < epsilon) {
                    if (!(flags & AxisTickGenerator::IncludeMin))
                        continue;
                    pos = min;
                } else if (fabs(pos - max) < epsilon) {
                    if (!(flags & AxisTickGenerator::IncludeMax)) {
                        if (!(flags & AxisTickGenerator::IncludeAfterMax))
                            break;
                        continue;
                    }
                    pos = max;
                }

                AxisTickGenerator::Tick t;
                t.point = pos;
                ticks.emplace_back(std::move(t));
            }
            generateLabels(ticks);
            return ticks;
        }
    }

    bool exponential = (base != 10);
    if (lMin < -4 || lMax > 4)
        exponential = true;

    QString exponentSign;
    int exponentDigits = 2;
    if (exponential && base == 10) {
        if (lMin <= -1000 || lMax >= 1000)
            exponentDigits = 4;
        else if (lMin <= -100 || lMax >= 100)
            exponentDigits = 3;
        else if (lMin <= -10 || lMax >= 10)
            exponentDigits = 2;
        else
            exponentDigits = 1;
    }
    if (exponential && lMin < 0)
        exponentSign = QString('+');

    int totalDecimalWidth = 0;
    if (!exponential) {
        if (lMin < 0)
            totalDecimalWidth = -lMin + 2;
        else
            totalDecimalWidth = lMin;
        if (lMax > totalDecimalWidth)
            totalDecimalWidth = lMax;
    }

    for (int tick = lMin; tick <= lMax; tick++) {
        double pos = pow(base, (double) tick);
        double epsilon = epsilonNormalized * pos;

        if (pos < (min - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeBeforeMin))
                continue;
        } else if (pos > (max + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeAfterMax)) {
                if (!(flags & AxisTickGenerator::IncludeMax))
                    break;
                continue;
            }
        } else if (fabs(pos - min) < epsilon) {
            if (!(flags & AxisTickGenerator::IncludeMin))
                continue;
            pos = min;
        } else if (fabs(pos - max) < epsilon) {
            if (!(flags & AxisTickGenerator::IncludeMax)) {
                if (!(flags & AxisTickGenerator::IncludeAfterMax))
                    break;
                continue;
            }
            pos = max;
        }

        AxisTickGenerator::Tick t;
        t.point = pos;

        if (exponential) {
            if (base == 10) {
                t.label = QString::fromLatin1("1E");
                if (tick >= 0)
                    t.label.append(exponentSign);
                else
                    t.label.append('-');
                if (tick < 0) {
                    t.label.append(QString::number(-tick, 10).rightJustified(exponentDigits));
                } else {
                    t.label.append(QString::number(tick, 10).rightJustified(exponentDigits));
                }
            } else {
                t.label = FP::scientificFormat(pos, 2, exponentDigits, QString('E'), QString(),
                                               exponentSign);
            }
        } else {
            if (base == 10) {
                if (tick < 0) {
                    t.label = QString::fromLatin1("0.");
                    if (tick < -1)
                        t.label.append(QString('0').repeated(-tick - 1));
                    t.label.append('1');
                } else {
                    t.label = QString('1');
                    if (tick > 0)
                        t.label.append(QString('0').repeated(tick));
                }
            } else {
                t.label = QString::number(pos, 'f', 3);
            }
        }

        ticks.emplace_back(std::move(t));
    }

    return ticks;
}

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorLog::generateFixed(double min,
                                                                         double max,
                                                                         int flags) const
{
    double step = 0;
    double epsilon = epsilonNormalized * fabs(max);
    for (int deltaBase = (int) floor(log((max - min)) / lbase);; deltaBase--) {
        step = pow(base, deltaBase);
        if (min + step < (max - epsilon))
            break;
    }
    std::vector<AxisTickGenerator::Tick> ticks;
    for (int i = 0, total = (int) ceil(base); i <= total; i++) {
        double pos = min + i * step;
        epsilon = epsilonNormalized * fabs(pos);

        if (pos < (min - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeBeforeMin))
                continue;
        } else if (pos > (max + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeAfterMax)) {
                if (!(flags & AxisTickGenerator::IncludeMax))
                    break;
                continue;
            }
        } else if (fabs(pos - min) < epsilon) {
            if (!(flags & AxisTickGenerator::IncludeMin))
                continue;
            pos = min;
        } else if (fabs(pos - max) < epsilon) {
            if (!(flags & AxisTickGenerator::IncludeMax)) {
                if (!(flags & AxisTickGenerator::IncludeAfterMax))
                    break;
                continue;
            }
            pos = max;
        }

        AxisTickGenerator::Tick t;
        t.point = pos;
        ticks.emplace_back(std::move(t));
    }
    generateLabels(ticks);
    return ticks;
}

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorLog::generate(double min,
                                                                    double max,
                                                                    int flags) const
{
    Q_ASSERT(FP::defined(max));
    Q_ASSERT(FP::defined(min));
    if (min >= max)
        return {};

    if (flags & AxisTickGenerator::FirstLevelHandling) {
        return generateAligned(min, max, flags);
    } else {
        return generateFixed(min, max, flags);
    }
}


AxisTickGeneratorConstant::AxisTickGeneratorConstant(QObject *parent) : AxisTickGenerator(parent),
                                                                        interval(1.0),
                                                                        origin(FP::undefined())
{ }

AxisTickGeneratorConstant::AxisTickGeneratorConstant(double setInterval,
                                                     double setOrigin,
                                                     QObject *parent) : AxisTickGenerator(parent),
                                                                        interval(setInterval),
                                                                        origin(setOrigin)
{
    Q_ASSERT(FP::defined(interval) && interval > 0.0);
}

AxisTickGeneratorConstant::~AxisTickGeneratorConstant()
{ }


const double AxisTickGeneratorConstant::epsilonNormalized = 1E-10;
const int AxisTickGeneratorConstant::maximumDivisions = 65536;

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorConstant::generateAligned(double min,
                                                                                double max,
                                                                                int flags) const
{
    double epsilon = qMax(fabs(min), fabs(max)) * epsilonNormalized;

    double base;
    if (FP::defined(origin)) {
        base = floor((min - origin) / interval) * interval + origin;
    } else {
        base = min;
    }

    std::vector<AxisTickGenerator::Tick> ticks;
    int totalDivisions = (int) ceil((max - base) / interval);
    totalDivisions = qMin(totalDivisions, maximumDivisions);
    for (int i = 0; i <= totalDivisions; i++) {
        double pos = base + interval * (double) i;

        if (pos < (min - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeBeforeMin))
                continue;
        } else if (pos > (max + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeAfterMax)) {
                if (!(flags & AxisTickGenerator::IncludeMax))
                    break;
                continue;
            }
        } else if (fabs(pos - min) < epsilon) {
            if (!(flags & AxisTickGenerator::IncludeMin))
                continue;
            pos = min;
        } else if (fabs(pos - max) < epsilon) {
            if (!(flags & AxisTickGenerator::IncludeMax)) {
                if (!(flags & AxisTickGenerator::IncludeAfterMax))
                    break;
                continue;
            }
            pos = max;
        }

        AxisTickGenerator::Tick t;
        t.point = pos;
        ticks.emplace_back(std::move(t));
    }

    generateLabels(ticks);
    return ticks;
}

const double AxisTickGeneratorConstant::minor4Divisible[4] = {1, 2, 4, 8};

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorConstant::generateFixed(double min,
                                                                              double max,
                                                                              int flags) const
{
    double delta = max - min;
    if (delta <= 0.0) return {};

    int baseDigits = (int) floor(log10(delta));
    double normalizedDelta = delta / pow(10.0, (double) baseDigits);

    int nDivisions = 5;
    for (int i = 0; i < (int) (sizeof(minor4Divisible) / sizeof(minor4Divisible[0])); i++) {
        if (fabs(normalizedDelta - minor4Divisible[i]) <= epsilonNormalized) {
            nDivisions = 4;
            break;
        }
    }

    std::vector<AxisTickGenerator::Tick> ticks;
    double step = delta / nDivisions;
    for (int i = 0; i <= nDivisions; i++) {
        double pos = min + i * step;
        if (i == 0) {
            if (!(flags & AxisTickGenerator::IncludeMin))
                continue;
            pos = min;
        } else if (i == nDivisions) {
            if (!(flags & AxisTickGenerator::IncludeMax))
                continue;
            pos = max;
        }

        AxisTickGenerator::Tick t;
        t.point = pos;
        ticks.emplace_back(std::move(t));
    }

    generateLabels(ticks);

    return ticks;
}

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorConstant::generate(double min,
                                                                         double max,
                                                                         int flags) const
{
    Q_ASSERT(FP::defined(max));
    Q_ASSERT(FP::defined(min));
    if (min >= max)
        return {};

    if (flags & AxisTickGenerator::FirstLevelHandling) {
        return generateAligned(min, max, flags);
    } else {
        return generateFixed(min, max, flags);
    }
}


AxisTickGeneratorNormalQuantiles::AxisTickGeneratorNormalQuantiles(QObject *parent)
        : AxisTickGenerator(parent)
{ }

AxisTickGeneratorNormalQuantiles::~AxisTickGeneratorNormalQuantiles()
{ }

const double AxisTickGeneratorNormalQuantiles::epsilonNormalized = 1E-6;
const double AxisTickGeneratorNormalQuantiles::tryQuantiles[3] = {0.5, 0.75, 0.25};
const int AxisTickGeneratorNormalQuantiles::tryDecimals[3] = {0, 0, 0};

static bool normalTickCompare(const AxisTickGenerator::Tick &a, const AxisTickGenerator::Tick &b)
{ return a.point < b.point; }

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorNormalQuantiles::generate(double min,
                                                                                double max,
                                                                                int flags) const
{
    Q_ASSERT(FP::defined(max));
    Q_ASSERT(FP::defined(min));
    std::vector<AxisTickGenerator::Tick> result;
    if (min >= max)
        return {};

    double epsilon = qMax(fabs(min), fabs(max)) * epsilonNormalized;
    if (!(flags & AxisTickGenerator::FirstLevelHandling)) {
        /*double step = (max - min) / 4.0;

        NumberFormat sigmaFmt(1,2);
        for (int i=0; i<=4; i++) {
            double position = min + (double)i * step;
        
            if (position < (min - epsilon)) {
                if (!(flags & AxisTickGenerator::IncludeBeforeMin))
                    continue;
            } else if (position <= (min + epsilon)) {
                if (!(flags & AxisTickGenerator::IncludeMin))
                    continue;
                position = min;
            }
            
            if (position > (max + epsilon)) {
                if (!(flags & AxisTickGenerator::IncludeAfterMax))
                    break;
            } else if (position >= (max - epsilon)) {
                if (!(flags & AxisTickGenerator::IncludeMax))
                    continue;
                position = max;
            }
            
            AxisTickGenerator::Tick add;
            add.point = Statistics::normalCDF(position) * 100.0;
            add.secondaryLabel = QString::fromUtf8("%1\xCF\x83").arg(
                sigmaFmt.apply(position, QChar()));
            result.emplace_back(std::move(add));
        }
        generateLabels(result);
        
        for (auto it = result.begin(); it != result.end(); ) {
            if (!FP::defined(it->point) || it->point <= 0.0 || 
                    it->point >= 100.0) {
                it = result.erase(it);
                continue;
            }
            it->point = Statistics::normalQuantile(it->point / 100.0);
            ++it;
        }*/
        return result;
    }

    int nInRange = 0;
    int nDecimals = 0;
    for (std::size_t idx = 0; idx < sizeof(tryQuantiles) / sizeof(tryQuantiles[0]); idx++) {
        double quantile = tryQuantiles[idx];
        double position = Statistics::normalQuantile(quantile);

        bool inRange = true;
        if (position < (min - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeBeforeMin))
                continue;
            inRange = false;
        } else if (position <= (min + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeMin))
                continue;
            position = min;
        }

        if (position > (max + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeAfterMax))
                continue;
            inRange = false;
        } else if (position >= (max - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeMax))
                continue;
            position = max;
        }

        AxisTickGenerator::Tick add;
        add.point = position;
        result.emplace_back(std::move(add));
        if (inRange) {
            nInRange++;
            nDecimals = qMax(nDecimals, tryDecimals[idx]);
        }
    }

    bool exceeded = false;
    for (int base = 1; base < 20; base++) {
        double quantile = pow(10.0, -base);
        double position = Statistics::normalQuantile(quantile);

        bool inRange = true;
        if (position < (min - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeBeforeMin))
                break;
            if (exceeded)
                break;
            exceeded = true;
            inRange = false;
        } else if (position <= (min + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeMin))
                continue;
            position = min;
            exceeded = true;
        }

        if (position > (max + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeAfterMax))
                continue;
        } else if (position >= (max - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeMax))
                continue;
            position = max;
        }

        AxisTickGenerator::Tick add;
        add.point = position;
        result.emplace_back(std::move(add));
        if (inRange) {
            nDecimals = qMax(nDecimals, base - 2);
            nInRange++;
        }
    }

    exceeded = false;
    for (int base = 1; base < 20; base++) {
        double quantile = 1.0 - pow(10.0, -base);
        double position = Statistics::normalQuantile(quantile);

        bool inRange = true;
        if (position < (min - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeBeforeMin))
                continue;
            inRange = false;
        } else if (position <= (min + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeMin))
                continue;
            position = min;
        }

        if (position > (max + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeAfterMax))
                break;
            if (exceeded)
                break;
            exceeded = true;
            inRange = false;
        } else if (position >= (max - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeMax))
                continue;
            exceeded = true;
            position = max;
        }

        AxisTickGenerator::Tick add;
        add.point = position;
        result.emplace_back(std::move(add));
        if (inRange) {
            nDecimals = qMax(nDecimals, base - 2);
            nInRange++;
        }
    }

    if (nInRange < 3) {
        result = AxisTickGeneratorDecimal().generate(Statistics::normalCDF(min) * 100.0,
                                                     Statistics::normalCDF(max) * 100.0, flags);

        NumberFormat sigmaFmt(1, 2);
        for (auto it = result.begin(); it != result.end();) {
            if (!FP::defined(it->point) || it->point <= 0.0 || it->point >= 100.0) {
                it = result.erase(it);
                continue;
            }

            double position = Statistics::normalQuantile(it->point / 100.0);
            if (fabs(position - min) < epsilon) {
                if (!(flags & AxisTickGenerator::IncludeMin)) {
                    it = result.erase(it);
                    continue;
                }
                position = min;
            } else if (position < (min - epsilon)) {
                if (!(flags & AxisTickGenerator::IncludeBeforeMin)) {
                    it = result.erase(it);
                    continue;
                }
            }
            if (fabs(position - max) < epsilon) {
                if (!(flags & AxisTickGenerator::IncludeMax)) {
                    it = result.erase(it);
                    continue;
                }
                position = max;
            } else if (position > (max + epsilon)) {
                if (!(flags & AxisTickGenerator::IncludeAfterMax)) {
                    it = result.erase(it);
                    continue;
                }
            }

            it->secondaryLabel = sigmaFmt.apply(position);
            it->point = position;
            ++it;
        }
        return result;
    }

    std::stable_sort(result.begin(), result.end(), normalTickCompare);
    double criticalValue = min - epsilon;
    while (result.size() > 1 &&
            result.front().point < criticalValue &&
            result[1].point < criticalValue) {
        result.erase(result.begin());
    }
    criticalValue = max + epsilon;
    while (result.size() > 1 &&
            result.back().point > criticalValue &&
            result[result.size() - 2].point > criticalValue) {
        result.pop_back();
    }

    NumberFormat labelFmt(2, nDecimals);
    NumberFormat sigmaFmt(1, 2);
    for (auto &it : result) {
        it.label = labelFmt.apply(Statistics::normalCDF(it.point) * 100.0, QChar());
        it.secondaryLabel = QString::fromUtf8("%1\xCF\x83").arg(sigmaFmt.apply(it.point, QChar()));
    }
    return result;
}


AxisTickGeneratorTimeInterval::AxisTickGeneratorTimeInterval(QObject *parent) : AxisTickGenerator(
        parent)
{ }

AxisTickGeneratorTimeInterval::~AxisTickGeneratorTimeInterval()
{ }

const double AxisTickGeneratorTimeInterval::epsilonNormalized = 1E-6;

/* @formatter:off
 * CLion bug: this line makes it swallow the space after the "double". */
const double AxisTickGeneratorTimeInterval::breakIntervals[8] =
        {1.0, 60.0, 3600.0, 86400.0, 604800.0, 2419200.0, 31536000.0, 315360000.0};
/* @formatter:on */

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorTimeInterval::generateTicks(double min,
                                                                                  double max,
                                                                                  int flags,
                                                                                  double step,
                                                                                  double divisor,
                                                                                  const QString &unit) const
{
    std::vector<AxisTickGenerator::Tick> result;

    double epsilon = qMax(fabs(min), fabs(max)) * epsilonNormalized;
    double startPosition = floor(min / step) * step;
    for (int i = 0; i < 500; i++) {
        double position = startPosition + (double) i * step;

        if (position < (min - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeBeforeMin))
                continue;
        } else if (position <= (min + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeMin))
                continue;
            position = min;
        }

        if (position > (max + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeAfterMax))
                break;
            AxisTickGenerator::Tick add;
            add.point = position;
            add.label = QString("%1%2").arg(floor(position / divisor + 0.5)).arg(unit);
            result.emplace_back(std::move(add));
            break;
        } else if (position >= (max - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeMax))
                continue;
            AxisTickGenerator::Tick add;
            add.point = max;
            add.label = QString("%1%2").arg(floor(position / divisor + 0.5)).arg(unit);
            result.emplace_back(std::move(add));
            break;
        }

        AxisTickGenerator::Tick add;
        add.point = position;
        add.label = QString("%1%2").arg(floor(position / divisor + 0.5)).arg(unit);
        result.emplace_back(std::move(add));
    }
    return result;
}

void AxisTickGeneratorTimeInterval::setMajorLabel(AxisTickGenerator::Tick &tick) const
{
    double epsilon = fabs(tick.point) * epsilonNormalized;
    if (tick.point + epsilon < 1.0) {
        tick.label = QString("%1msec").arg(floor(tick.point * 1000.0 + 0.5));
    } else if (tick.point + epsilon < 60.0) {
        tick.label = QString("%1S").arg(floor(tick.point + 0.5));
    } else if (tick.point + epsilon < 3600.0) {
        tick.label = QString("%1M").arg(floor(tick.point / 60.0 + 0.5));
    } else if (tick.point + epsilon < 86400.0) {
        tick.label = QString("%1H").arg(floor(tick.point / 3600.0 + 0.5));
    } else if (tick.point + epsilon < 604800.0) {
        tick.label = QString("%1D").arg(floor(tick.point / 86400.0 + 0.5));
    } else if (tick.point + epsilon < 31536000.0) {
        tick.label = QString("%1W").arg(floor(tick.point / 604800.0 + 0.5));
    } else {
        tick.label = QString("%1Y").arg(floor(tick.point / 31536000.0 + 0.5));
    }
}

std::vector<AxisTickGenerator::Tick> AxisTickGeneratorTimeInterval::generate(double min,
                                                                             double max,
                                                                             int flags) const
{
    Q_ASSERT(FP::defined(max));
    Q_ASSERT(FP::defined(min));
    if (min >= max || max <= 0.0)
        return {};

    double epsilon = qMax(fabs(min), fabs(max)) * epsilonNormalized;
    if (!(flags & AxisTickGenerator::FirstLevelHandling)) {
        if (max - epsilon < 1.0) {
            return generateTicks(min, max, flags, 0.250, 0.001, "msec");
        } else if (max - epsilon < 60.0) {
            return generateTicks(min, max, flags, 15.0, 1.0, "S");
        } else if (max - epsilon < 3600.0) {
            return generateTicks(min, max, flags, 900.0, 60.0, "M");
        } else if (max - epsilon < 86400.0) {
            return generateTicks(min, max, flags, 14400.0, 3600.0, "H");
        } else if (max - epsilon < 604800.0) {
            return generateTicks(min, max, flags, 86400.0, 86400.0, "D");
        } else if (max - epsilon < 2419200.0) {
            return generateTicks(min, max, flags, 604800.0, 604800.0, "W");
        } else if (max - epsilon < 31536000.0) {
            return generateTicks(min, max, flags, 2419200.0, 604800.0, "W");
        } else {
            return generateTicks(min, max, flags, 315360000.0, 31536000.0, "Y");
        }
    }

    int nIntervals = sizeof(breakIntervals) / sizeof(breakIntervals[0]);

    if (breakIntervals[nIntervals - 1] <= min - epsilon) {
        return generateTicks(min, max, flags, breakIntervals[nIntervals - 1], 31536000.0, "Y");
    }
    if (breakIntervals[0] >= max + epsilon) {
        return generateTicks(min, max, flags, 0.250, 0.001, "msec");
    }

    int startIntervalIndex = 0;
    epsilon = fabs(min) * epsilonNormalized;
    for (; startIntervalIndex < nIntervals - 1; startIntervalIndex++) {
        if (breakIntervals[startIntervalIndex + 1] + epsilon >= min)
            break;
    }

    int endIntervalIndex = startIntervalIndex + 1;
    epsilon = fabs(max) * epsilonNormalized;
    for (; endIntervalIndex <= nIntervals; endIntervalIndex++) {
        if (breakIntervals[endIntervalIndex - 1] + epsilon >= max)
            break;
    }

    if (startIntervalIndex + 1 == endIntervalIndex - 1 &&
            breakIntervals[startIntervalIndex] <= min - epsilon &&
            breakIntervals[endIntervalIndex - 1] >= max - epsilon) {
        if (max - epsilon < 1.0) {
            return generateTicks(min, max, flags, 0.250, 0.001, "msec");
        } else if (max - epsilon < 60.0) {
            return generateTicks(min, max, flags, 15.0, 1.0, "S");
        } else if (max - epsilon < 3600.0) {
            return generateTicks(min, max, flags, 900.0, 60.0, "M");
        } else if (max - epsilon < 86400.0) {
            return generateTicks(min, max, flags, 14400.0, 3600.0, "H");
        } else if (max - epsilon < 604800.0) {
            return generateTicks(min, max, flags, 86400.0, 86400.0, "D");
        } else if (max - epsilon < 2419200.0) {
            return generateTicks(min, max, flags, 604800.0, 604800.0, "W");
        } else if (max - epsilon < 31536000.0) {
            return generateTicks(min, max, flags, 2419200.0, 604800.0, "W");
        } else {
            return generateTicks(min, max, flags, 315360000.0, 31536000.0, "Y");
        }
    }

    std::vector<AxisTickGenerator::Tick> result;
    for (; startIntervalIndex < endIntervalIndex; ++startIntervalIndex) {
        double position = breakIntervals[startIntervalIndex];
        epsilon = fabs(position) * epsilonNormalized;

        if (position < (min - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeBeforeMin))
                continue;
        } else if (position <= (min + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeMin))
                continue;
            position = min;
        }

        if (position > (max + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeAfterMax))
                break;
        } else if (position >= (max - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeMax))
                continue;
            position = max;
        }

        AxisTickGenerator::Tick add;
        add.point = position;
        setMajorLabel(add);
        result.emplace_back(std::move(add));
    }

    if (startIntervalIndex == nIntervals && breakIntervals[nIntervals - 1] <= max - epsilon) {
        for (int i = 1; i < 500; i++) {
            double position = breakIntervals[nIntervals - 1] + 315360000.0 * (double) i;
            epsilon = fabs(position) * epsilonNormalized;

            if (position < (min - epsilon)) {
                if (!(flags & AxisTickGenerator::IncludeBeforeMin))
                    continue;
            } else if (position <= (min + epsilon)) {
                if (!(flags & AxisTickGenerator::IncludeMin))
                    continue;
                position = min;
            }

            if (position > (max + epsilon)) {
                if (!(flags & AxisTickGenerator::IncludeAfterMax))
                    break;
                AxisTickGenerator::Tick add;
                add.point = position;
                setMajorLabel(add);
                result.emplace_back(std::move(add));
                break;
            } else if (position >= (max - epsilon)) {
                if (!(flags & AxisTickGenerator::IncludeMax))
                    continue;
                AxisTickGenerator::Tick add;
                add.point = max;
                setMajorLabel(add);
                result.emplace_back(std::move(add));
                break;
            }

            AxisTickGenerator::Tick add;
            add.point = position;
            setMajorLabel(add);
            result.emplace_back(std::move(add));
        }
    }

    return result;
}


}
}

