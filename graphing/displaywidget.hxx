/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGDISPLAYWIDGET_H
#define CPD3GRAPHINGDISPLAYWIDGET_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QTimer>
#include <QSpinBox>
#include <QCheckBox>
#include <QDialog>

#include "graphing/graphing.hxx"
#include "graphing/display.hxx"
#include "datacore/processingtapchain.hxx"
#include "datacore/segment.hxx"
#include "guicore/exponentedit.hxx"
#include "guicore/timeboundselection.hxx"

namespace CPD3 {
namespace Graphing {

class DisplayWidgetTapChain;

namespace Internal {
class DisplayWidgetContextDisplay;

class DisplayWidgetContextZoomDialog;

class DisplayWidgetPrivate;

class DisplayWidgetPrivateLayout;
}

/**
 * The base widget for drawing displays.
 */
class CPD3GRAPHING_EXPORT DisplayWidget : public QWidget {
Q_OBJECT
public:
    enum SmoothingType {
        Smoothing_Disabled = 0,
        Smoothing_1M,
        Smoothing_6M,
        Smoothing_10M,
        Smoothing_15M,
        Smoothing_1H,
        Smoothing_6H,
        Smoothing_1D,
        Smoothing_1W,
        Smoothing_1MON,
        Smoothing_1PLP_1M,
        Smoothing_1PLP_3M,
        Smoothing_1PLP_10M,
        Smoothing_1PLP_15M,
        Smoothing_1PLP_1H,
        Smoothing_1PLP_1D,
        Smoothing_4PLP_1M,
        Smoothing_4PLP_3M,
        Smoothing_4PLP_10M,
        Smoothing_4PLP_15M,
        Smoothing_4PLP_1H,
        Smoothing_4PLP_1D,
        Smoothing_Fourier_1M,
        Smoothing_Fourier_3M,
        Smoothing_Fourier_10M,
        Smoothing_Fourier_15M,
        Smoothing_Fourier_1H,
        Smoothing_Fourier_1D,
        Smoothing_3RSSH,
    };

private:

    std::unique_ptr<Internal::DisplayWidgetPrivate> p;
    bool isLayout;

    friend class Internal::DisplayWidgetPrivate;

    friend class Internal::DisplayWidgetPrivateLayout;

    friend class Internal::DisplayWidgetContextDisplay;

    friend class Internal::DisplayWidgetContextZoomDialog;

    friend class DisplayWidgetTapChain;

    /**
     * This property determines if the "reset zoom" option also
     * emits an undefined time range select.  This can be used by environments
     * without a time selection control to ensure consistent behavior.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setResetSelectsAll(bool)
     *  <li> bool getResetSelectsAll() const
     * </ul>
     */
    Q_PROPERTY(bool resetSelectsAll
                       READ getResetSelectsAll
                       WRITE setResetSelectsAll
                       DESIGNABLE
                       true
                       USER
                       true)

    /**
     * This property determines sets the smoothing override behavior used in
     * the display.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setSmoothingType(SmoothingType)
     *  <li> SmoothingType getSmoothingType() const
     * </ul>
     */
    Q_PROPERTY(SmoothingType smoothingType
                       READ getSmoothingType
                       WRITE setSmoothingType
                       DESIGNABLE
                       false
                       USER
                       true)

    /**
     * This property determines if contaminated data is always removed before
     * data output.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setContaminationRemoved(bool)
     *  <li> bool getContaminationRemoved() const
     * </ul>
     */
    Q_PROPERTY(bool contaminationRemoved
                       READ getContaminationRemoved
                       WRITE setContaminationRemoved
                       DESIGNABLE
                       false
                       USER
                       true)

    void emitZoom(DisplayZoomCommand command);

    void emitVisibleZoom(DisplayZoomCommand command);

    void emitGlobalZoom(DisplayZoomCommand command);

    void emitResetTimes();

    void emitChainReload();

    static void modifyConfigForSmoother(SmoothingType type,
                                        bool removeContamination,
                                        Data::Variant::Write &config);

    static inline void modifyConfigForSmoother(SmoothingType type,
                                               bool removeContamination,
                                               Data::Variant::Write &&config)
    { return modifyConfigForSmoother(type, removeContamination, config); }

public:
    DisplayWidget(QWidget *parent);

    DisplayWidget(std::unique_ptr<Display> &&setDisplay = {}, QWidget *parent = 0);

    virtual ~DisplayWidget();

    void setDisplay(std::unique_ptr<Display> &&set);

    Display *getDisplay() const;

    void setContext(const DisplayDynamicContext &context);

    const DisplayDynamicContext &getContext() const;

    void registerChain(DisplayWidgetTapChain *chain);

    QSize sizeHint() const override;

    std::vector<std::shared_ptr<DisplayHighlightController>> getHighlightControllers();

    std::vector<std::shared_ptr<DisplayOutput>> getOutputs();

    bool getResetSelectsAll() const;

    void setResetSelectsAll(bool set);

    SmoothingType getSmoothingType() const;

    void setSmoothingType(SmoothingType set);

    bool getContaminationRemoved() const;

    void setContaminationRemoved(bool set);

    void setSmoothing(SmoothingType type, bool contam);

    static void buildSmoothingMenu(QMenu *menu,
                                   QObject *target,
                                   const char *slot,
                                   SmoothingType currentSmoothingType = Smoothing_Disabled,
                                   bool currentContaminationRemoved = false);

    static void saveConfiguration(const Data::Variant::Read &baseline,
                                  Data::ValueSegment::Transfer overlay,
                                  QWidget *parent = 0);

signals:

    /**
     * Emitted to request a chain reload (parent calling 
     * registerChain(DisplayWidgetFilterChain *) again).  This is used to
     * implement smoothing control, which requires reloading the chain.
     */
    void requestChainReload();

    /**
     * Emitted when a time range is selected by the display.
     * 
     * @param start     the selected start time
     * @param end       the selected end time
     */
    void timeRangeSelected(double start, double end);

    /**
     * Emitted when a zoom is generated internally.  For example, this is
     * used by graphs when an area is selected by the mouse.  This provided
     * so the parent can implement an undo stack that handles zooming.  If 
     * unused this can be connected to applyZoom( DisplayZoomCommand )
     * which just calls the command's redo() function.
     * 
     * @param command   the zoom command
     */
    void internalZoom(CPD3::Graphing::DisplayZoomCommand command);

    /**
     * Emitted when a zoom is generated that should apply to the visible 
     * displays.  For example, this is used when the user selects "Apply to 
     * Visible" from the manual zoom dialog.  This provided
     * so the parent can implement an undo stack that handles zooming.  If 
     * unused this can be connected to applyZoom( DisplayZoomCommand )
     * which just calls the command's redo() function.
     * 
     * @param command   the zoom command
     */
    void visibleZoom(CPD3::Graphing::DisplayZoomCommand command);

    /**
     * Emitted when a zoom is generated that should apply to all displays.  
     * For example, this is used when the user selects "Apply to All"" from 
     * the manual zoom dialog.  This provided
     * so the parent can implement an undo stack that handles zooming.  If 
     * unused this can be connected to applyZoom( DisplayZoomCommand )
     * which just calls the command's redo() function.
     * 
     * @param command   the zoom command
     */
    void globalZoom(CPD3::Graphing::DisplayZoomCommand command);

    /**
     * Emitted when a save changes request is made.  The value is the
     * changes made from the baseline the display was configured with.
     * 
     * @param overlay   the changed overlay
     */
    void saveChanges(CPD3::Data::ValueSegment::Transfer overlay);

public slots:

    /**
     * Call the given command's redo() function.  This is provided for
     * convenience in environments that do not implement an undo stack. 
     */
    void applyZoom(CPD3::Graphing::DisplayZoomCommand command);

    /**
     * Sets the display's time range to the given bounds.  This calls
     * setVisibleTimeRange( double, double ) on the display.  If the
     * environment does not implement a time range selector, this
     * should be connected directly to timeRangeSelected( double, double ).
     * 
     * @param start     the start time of undefined for none
     * @param end       the end time or undefined for none
     */
    void setVisibleTimeRange(double start, double end);

    /**
     * Create a fullscreen display of the widget.
     */
    void displayFullScreen();

    /**
     * Release all focus on the display.
     */
    void releaseFocus();

    /**
     * Display the export image prompt.
     */
    void promptExportImage();

    /**
     * Display the print prompt.
     */
    void promptPrint();

private slots:

    void combineVisibleZoom(CPD3::Graphing::DisplayZoomCommand command);

    void combineGlobalZoom(CPD3::Graphing::DisplayZoomCommand command);

    void combineSaveChanges(CPD3::Data::ValueSegment::Transfer overlay);

protected:
    void paintEvent(QPaintEvent *event) override;

    void keyPressEvent(QKeyEvent *event) override;

    void keyReleaseEvent(QKeyEvent *event) override;

    void enterEvent(QEvent *event) override;

    void leaveEvent(QEvent *event) override;

    void mouseMoveEvent(QMouseEvent *event) override;

    void mousePressEvent(QMouseEvent *event) override;

    void mouseReleaseEvent(QMouseEvent *event) override;

    void resizeEvent(QResizeEvent *event) override;

    bool event(QEvent *event) override;

private slots:

    void interactiveRepaint(bool userInitiated);

    void interactionTimeout();

    void displayMouseTracking(bool enable);

    void setMouseGrab(bool enable);

    void setKeyboardGrab(bool enable);

    void widgetContextMenu(const QPoint &point);

    void changeToolTip(const QPoint &point, const QString &text);

    void menuSaveChanges();
};

/**
 * A specialized filter chain used by the display widget to provide interactive
 * smoothing options.
 */
class CPD3GRAPHING_EXPORT DisplayWidgetTapChain : public Data::ProcessingTapChain {
    friend class Internal::DisplayWidgetPrivate;

    DisplayWidget::SmoothingType smoothingType;
    bool removeContamination;

    void setSmoothing(DisplayWidget::SmoothingType type, bool contam);

    void clearSmoothing();

public:
    DisplayWidgetTapChain();

    virtual ~DisplayWidgetTapChain();

    void add(Data::StreamSink *target,
             const Data::Variant::Read &config,
             const Data::SequenceMatch::Composite &selection = Data::SequenceMatch::Composite(
                     Data::SequenceMatch::Element::SpecialMatch::All)) override;

    void add(QList<QPair<Data::SequenceMatch::Composite, Data::StreamSink *> > &targets,
             const Data::Variant::Read &config) override;
};

namespace Internal {

class DisplayWidgetSmoothingForwarder : public QObject {
Q_OBJECT

    DisplayWidget::SmoothingType type;
    bool contamination;

    friend class DisplayWidget;

public:
    DisplayWidgetSmoothingForwarder(QObject *target,
                                    const char *slot,
                                    DisplayWidget::SmoothingType st,
                                    bool contam,
                                    QObject *parent = 0);

signals:

    void smoothingChanged(DisplayWidget::SmoothingType smoothingType, bool contaminationRemoved);

private slots:

    void smoothingDisabled();

    void smoothing1M();

    void smoothing6M();

    void smoothing10M();

    void smoothing15M();

    void smoothing1H();

    void smoothing6H();

    void smoothing1D();

    void smoothing1W();

    void smoothing1Mon();

    void smoothing1PLP1M();

    void smoothing1PLP3M();

    void smoothing1PLP10M();

    void smoothing1PLP15M();

    void smoothing1PLP1H();

    void smoothing1PLP1D();

    void smoothing4PLP1M();

    void smoothing4PLP3M();

    void smoothing4PLP10M();

    void smoothing4PLP15M();

    void smoothing4PLP1H();

    void smoothing4PLP1D();

    void smoothingFourier1M();

    void smoothingFourier3M();

    void smoothingFourier10M();

    void smoothingFourier15M();

    void smoothingFourier1H();

    void smoothingFourier1D();

    void smoothing3RSSH();

    void smoothingContamination(bool on);
};

class DisplayWidgetContextZoomDialog : public QDialog {
Q_OBJECT

    QPointer<DisplayWidget> widget;
    struct Axis {
        std::shared_ptr<DisplayZoomAxis> axis;
        QDoubleSpinBox *simpleMin;
        GUI::ExponentEdit *exponentMin;
        QDoubleSpinBox *simpleMax;
        GUI::ExponentEdit *exponentMax;
        GUI::TimeBoundSelection *time;
        QCheckBox *couple;
        double min;
        double max;
    };
    std::vector<Axis> axes;

    void connectAxis(const Axis &axis);

    void changeAxisBounds(const Axis &axis, double min, double max);

    DisplayZoomCommand constructCommand() const;

public:
    DisplayWidgetContextZoomDialog(const QPointer<DisplayWidget> &parent,
                                   const std::vector<std::shared_ptr<DisplayZoomAxis> > &zoom);

private slots:

    void applyLocal();

    void applyVisible();

    void applyGlobal();

    void axisLimitsChanged();
};

class DisplayWidgetContextSizeDialog : public QDialog {
Q_OBJECT

    QSpinBox *width;
    QSpinBox *height;
    QCheckBox *preserveAspectRatio;
    qreal aspectRatio;
    QColor selectedColor;
public:
    DisplayWidgetContextSizeDialog(bool enableColor = false, QWidget *parent = 0);

    QSize getSelectedSize() const;

    QColor getSelectedColor() const;

private slots:

    void widthChanged(int w);

    void heightChanged(int h);

    void set1024x768();
};

class DisplayWidgetContextDisplay : public QObject {
Q_OBJECT

    QPointer<DisplayWidget> widget;
    std::vector<std::shared_ptr<DisplayZoomAxis> > zoom;

    DisplayWidget *outermostWidget();

    void exportImage(DisplayWidget *widget);

    void print(DisplayWidget *widget);

    void displayPopup(DisplayWidget *widget);

    void changeSmoothing(DisplayWidget *widget, DisplayWidget::SmoothingType type, bool contam);

public:
    DisplayWidgetContextDisplay(DisplayWidget *set, QObject *parent = 0);

    inline bool hasZoom() const
    { return !zoom.empty(); }

    static void displayFullScreen(DisplayWidget *widget);

public slots:

    void manualZoom();

    void resetZoom();

    void exportImage();

    void exportImageAll();

    void print();

    void printAll();

    void displayPopup();

    void displayPopupAll();

    void displayFullScreen();

    void displayFullScreenAll();

    void changeSmoothing(DisplayWidget::SmoothingType type, bool contam);

    void changeAllSmoothing(DisplayWidget::SmoothingType type, bool contam);
};

}

}
}

Q_DECLARE_TYPEINFO(CPD3::Graphing::DisplayWidget::SmoothingType, Q_PRIMITIVE_TYPE);

Q_DECLARE_METATYPE(CPD3::Graphing::DisplayWidget::SmoothingType);

#endif
