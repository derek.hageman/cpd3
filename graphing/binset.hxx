/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGBINSET_H
#define CPD3GRAPHINGBINSET_H

#include "core/first.hxx"

#include <math.h>
#include <algorithm>
#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QVector>
#include <QString>
#include <QSet>
#include <QDebugStateSaver>
#include <QDebug>

#include "graphing/graphing.hxx"
#include "graphing/traceset.hxx"
#include "algorithms/statistics.hxx"
#include "algorithms/linearcluster.hxx"
#include "datacore/variant/composite.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * A "point" representing a bin.
 * 
 * @param I the number of independent dimensions
 * @param D the number of dependent dimensions
 */
template<std::size_t I, std::size_t D>
struct BinPoint {
    /** The center values. */
    double center[I];
    /** The widths. */
    double width[I];
    /** The lowest (whisker) values. */
    double lowest[D];
    /** The lower (box) values. */
    double lower[D];
    /** The middle (median) values. */
    double middle[D];
    /** The upper (box) values. */
    double upper[D];
    /** The uppermost (whisker) values. */
    double uppermost[D];
    /** The total number of points in the bin or the total time covered. */
    double total[D];

    bool operator==(const BinPoint<I, D> &other) const
    {
        for (std::size_t i = 0; i < I; i++) {
            if (!FP::equal(center[i], other.center[i]))
                return false;
            if (!FP::equal(width[i], other.width[i]))
                return false;
        }
        for (std::size_t i = 0; i < D; i++) {
            if (!FP::equal(lowest[i], other.lowest[i]))
                return false;
            if (!FP::equal(lower[i], other.lower[i]))
                return false;
            if (!FP::equal(middle[i], other.middle[i]))
                return false;
            if (!FP::equal(upper[i], other.upper[i]))
                return false;
            if (!FP::equal(uppermost[i], other.uppermost[i]))
                return false;
            if (!FP::equal(total[i], other.total[i]))
                return false;
        }
        return true;
    }

    bool operator!=(const BinPoint<I, D> &other) const
    {
        return !(*this == other);
    }
};

template<int I, int D>
QDebug operator<<(QDebug stream, const BinPoint<I, D> &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "BinPoint<" << I << ',' << D << ">(";
    for (std::size_t i = 0; i < I; i++) {
        if (i != 0) stream << ',';
        stream << "I[" << i << "]={center=" << v.center[i] << ",width=" << v.width[i] << '}';
    }
    for (std::size_t i = 0; i < D; i++) {
        stream << ",D[" << i << "]={" << v.lowest[i] << ',' << v.lower[i] << ',' << v.middle[i]
               << ',' << v.upper[i] << ',' << v.uppermost[i] << v.total[i] << '}';
    }
    stream << ')';
    return stream;
}

}
}

namespace CPD3 {
namespace Graphing {

/**
 * The base class for all bin parameters.
 */
class CPD3GRAPHING_EXPORT BinParameters : public TraceParameters {
public:
    /**
     * The the type of binning used.
     */
    enum BinningType {
        /** 
         * A fixed number of total bins, uniformly distributed on the total 
         * range. */
        Binning_FixedUniform, /**
         * A fixed bin size, uniformly distributed across the range of
         * data.
         */
        Binning_FixedSize, /**
         * Automatic point clustering.  This is generally only usable on
         * discrete data sampling with noise. 
         */
        Binning_Clustering, /**
         * A static set of bins.
         */
        Binning_Static,
    };

    /**
     * How the quantile numbers are interpreted.
     */
    enum QuantileMode {
        /**
         * Standard percentiles: 0.5 is the median, 0.95 is the 95th percentile,
         * etc.
         */
        Standard, /**
         * As multipliers of the calculated normal distribution.  That is the
         * mean and standard deviation are calculated and the quantiles are
         * the quantiles of that normal distribution.
         */
        Normal,
    };

    /**
     * A static sized bin.
     */
    struct CPD3GRAPHING_EXPORT StaticBin {
        /** The start value. */
        double start;
        /** The end value. */
        double end;
        /** The drawing center value. */
        double center;
    };

private:
    enum {
        Component_Binning = 0x0001,
        Component_QuantileMode = 0x0002,
        Component_WhiskerQuantile = 0x0004,
        Component_BoxQuantile = 0x0008,
        Component_AsHistogram = 0x0010,
    };
    int components;

    struct BinningDefinition {
        BinningType binning;
        int count;
        double size;
        bool logarithmic;
        std::vector<StaticBin> bins;
    };
    std::vector<BinningDefinition> bins;
    QuantileMode quantileMode;
    double whiskerQuantile;
    double boxQuantile;
    bool asHistogram;
public:
    BinParameters();

    virtual ~BinParameters();

    BinParameters(const BinParameters &other);

    BinParameters &operator=(const BinParameters &other);

    BinParameters(BinParameters &&other);

    BinParameters &operator=(BinParameters &&other);

    BinParameters(const Data::Variant::Read &value);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<TraceParameters> clone() const override;

    void overlay(const TraceParameters *over) override;

    void copy(const TraceParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify binning.
     * @return true if the parameters specify binning
     */
    inline bool hasBinning() const
    { return components & Component_Binning; }

    /**
     * Get the binning type.
     * @param dim   the independent dimension number
     * @return      the binning type
     */
    inline BinningType getBinningType(std::size_t dim) const
    {
        if (dim >= bins.size()) return Binning_FixedUniform;
        return bins[dim].binning;
    }

    /**
     * Get the binning count.
     * @param dim   the independent dimension number
     * @return      the number of bins
     */
    inline int getBinningCount(std::size_t dim) const
    {
        if (dim >= bins.size()) return 5;
        return bins[dim].count;
    }

    /**
     * Get the binning size.
     * @param dim   the independent dimension number
     * @return      the bin size
     */
    inline double getBinningSize(std::size_t dim) const
    {
        if (dim >= bins.size()) return 1.0;
        return bins[dim].size;
    }

    /**
     * Get the binning logarithmic setting.
     * @param dim   the independent dimension number
     * @return      true if the dimension should be logarithmic
     */
    inline double getBinningLogarithmic(std::size_t dim) const
    {
        if (dim >= bins.size()) return false;
        return bins[dim].logarithmic;
    }

    /**
     * Get the binning static bins.
     * @param dim   the independent dimension number
     * @return      the static bins
     */
    inline const std::vector<StaticBin> &getBinningStatic(std::size_t dim) const
    {
        if (dim >= bins.size()) {
            static const std::vector<StaticBin> empty;
            return empty;
        }
        return bins[dim].bins;
    }

    /**
     * Test if the parameters specify the quantile interpretation mode.
     * @return true if the parameters specify a quantile mode
     */
    inline bool hasQuantileMode() const
    { return components & Component_QuantileMode; }

    /**
     * Get the quantile interpretation mode.
     * @return the quantile mode
     */
    inline QuantileMode getQuantileMode() const
    { return quantileMode; }


    /**
     * Test if the parameters specify a whisker quantile.
     * @return true if the parameters specify a whisker quantile
     */
    inline bool hasWhiskerQuantile() const
    { return components & Component_WhiskerQuantile; }

    /**
     * Get the whisker quantile.
     * @return  the whisker quantile.
     */
    inline double getWhiskerQuantile() const
    { return whiskerQuantile; }

    /**
     * Test if the parameters specify a box quantile.
     * @return true if the parameters specify a box quantile
     */
    inline bool hasBoxQuantile() const
    { return components & Component_BoxQuantile; }

    /**
     * Get the box quantile.
     * @return  the box quantile.
     */
    inline double getBoxQuantile() const
    { return boxQuantile; }

    /**
     * Test if the parameters specify the output being a histogram.
     * @return true if the parameters specify the output being a histogram
     */
    inline bool hasAsHistogram() const
    { return components & Component_AsHistogram; }

    /**
     * Get the histogram state.
     * @return  the histogram state
     */
    inline bool getAsHistogram() const
    { return asHistogram; }
};

#define FIXEDBINSIZE_NUMBER_LIMIT  65536

/**
 * A handler for a single final output binner.  The independent dimensions
 * are first in the trace dimension order.
 * 
 * @param I the number of independent dimensions
 * @param D the number of dependent dimensions
 */
template<std::size_t I, std::size_t D>
class BinValueHandler : public TraceValueHandler<I + D> {
    BinParameters *parameters;

    TraceLimits<I + D> limits;

    std::vector<BinPoint<I, D>> points;

    void insertToLimits(std::size_t dim, double value)
    {
        if (!FP::defined(limits.min[dim])) {
            limits.min[dim] = value;
            limits.max[dim] = value;
            return;
        }
        if (!FP::defined(value))
            return;
        if (limits.min[dim] > value) limits.min[dim] = value;
        if (limits.max[dim] < value) limits.max[dim] = value;
    }

    void updateBins()
    {
        std::vector<double> starts[I];
        std::vector<double> ends[I];
        std::vector<double> centers[I];
        for (std::size_t dim = 0; dim < I; dim++) {
            buildBins(dim, starts[dim], ends[dim], centers[dim]);
            Q_ASSERT(!starts[dim].empty());
            Q_ASSERT(starts[dim].size() == ends[dim].size());
            Q_ASSERT(starts[dim].size() == centers[dim].size());
        }

        std::size_t pitch[I + 1];
        pitch[0] = 1;
        for (std::size_t dim = 1; dim <= I; dim++) {
            pitch[dim] = pitch[dim - 1] * starts[dim - 1].size();
        }

        const auto &inputPoints = TraceValueHandler<I + D>::getPoints();
        double effectiveStart = 0;
        double effectiveEnd = 0;
        double totalTime[D];
        if (parameters->getAsHistogram()) {
            int nUndefined = 0;
            effectiveStart = FP::undefined();
            effectiveEnd = FP::undefined();
            for (std::size_t i = 0; i < D; i++) {
                totalTime[i] = 0.0;
            }
            for (const auto &it : inputPoints) {
                if (!FP::defined(effectiveStart)) {
                    effectiveStart = it.start;
                } else if (FP::defined(it.start) && effectiveStart > it.start) {
                    effectiveStart = it.start;
                }
                if (!FP::defined(effectiveEnd)) {
                    effectiveEnd = it.end;
                } else if (FP::defined(it.end) && effectiveEnd < it.end) {
                    effectiveEnd = it.end;
                }

                double start = it.start;
                if (Range::compareStart(start, TraceValueHandler<I + D>::getStart()) < 0)
                    start = TraceValueHandler<I + D>::getStart();
                double end = it.end;
                if (Range::compareEnd(end, TraceValueHandler<I + D>::getEnd()) > 0)
                    end = TraceValueHandler<I + D>::getEnd();

                if (FP::defined(start) && FP::defined(end) && end >= start) {
                    for (std::size_t i = 0; i < D; i++) {
                        if (!FP::defined(it.d[I + i]))
                            continue;
                        totalTime[i] += end - start;
                    }
                } else {
                    nUndefined++;
                }
            }

            if (nUndefined != 0) {
                for (std::size_t i = 0; i < D; i++) {
                    totalTime[i] = 0.0;
                }
                for (const auto &it : inputPoints) {
                    double start = it.start;
                    if (!FP::defined(start))
                        start = effectiveStart;
                    if (Range::compareStart(start, TraceValueHandler<I + D>::getStart()) < 0)
                        start = TraceValueHandler<I + D>::getStart();
                    double end = it.end;
                    if (!FP::defined(end))
                        end = effectiveEnd;
                    if (Range::compareEnd(end, TraceValueHandler<I + D>::getEnd()) > 0)
                        end = TraceValueHandler<I + D>::getEnd();

                    if (!FP::defined(start) || !FP::defined(end) || end < start) {
                        for (std::size_t i = 0; i < D; i++) {
                            if (!FP::defined(it.d[I + i]))
                                continue;
                            totalTime[i] += 1.0;
                        }
                    } else {
                        for (std::size_t i = 0; i < D; i++) {
                            if (!FP::defined(it.d[I + i]))
                                continue;
                            totalTime[i] += end - start;
                        }
                    }
                }
            }
        }

        std::vector<std::vector<TracePoint<I + D>>> binnedPoints;
        binnedPoints.reserve(pitch[I]);
        for (std::size_t i = 0; i < pitch[1]; i++) {
            binnedPoints.emplace_back(
                    applyFilter({inputPoints.begin(), inputPoints.end()}, starts[0][i], ends[0][i],
                                0));
        }
        if (I > 1) {
            for (std::size_t i = pitch[1]; i < pitch[I]; i++) {
                binnedPoints.emplace_back(binnedPoints[i - pitch[1]]);
            }
            for (std::size_t dim = 1; dim < I; dim++) {
                for (std::size_t i = 0; i < pitch[I]; i++) {
                    std::size_t idx = (i / pitch[dim]) % pitch[dim + 1];
                    binnedPoints[i] = applyFilter(std::move(binnedPoints[i]), starts[dim][idx],
                                                  ends[dim][idx], dim);
                }
            }
        }

        points.clear();
        limits.reset();
        for (std::size_t i = 0; i < pitch[I]; i++) {
            if (binnedPoints[i].empty())
                continue;
            BinPoint<I, D> point;
            for (std::size_t j = 0; j < I; j++) {
                std::size_t idx = (i / pitch[j]) % pitch[j + 1];
                point.center[j] = centers[j][idx];
                double start = starts[j][idx];
                double end = ends[j][idx];
                if (FP::defined(start) && FP::defined(end) && end > start)
                    point.width[j] = end - start;
                else
                    point.width[j] = FP::undefined();
            }
            if (parameters->getAsHistogram()) {
                convertHistogram(binnedPoints[i], totalTime, effectiveStart, effectiveEnd, point);
            } else {
                convertBin(binnedPoints[i], point);
            }
            points.emplace_back(point);

            for (std::size_t j = 0; j < D; j++) {
                insertToLimits(j + I, point.lowest[j]);
                insertToLimits(j + I, point.lower[j]);
                insertToLimits(j + I, point.middle[j]);
                insertToLimits(j + I, point.upper[j]);
                insertToLimits(j + I, point.uppermost[j]);
            }
            for (std::size_t j = 0; j < I; j++) {
                insertToLimits(j, point.center[j]);
                if (FP::defined(point.width[j]) && FP::defined(point.center[j])) {
                    insertToLimits(j, point.center[j] - point.width[j] * 0.5);
                    insertToLimits(j, point.center[j] + point.width[j] * 0.5);
                }
            }
        }
    }

public:
    BinValueHandler(std::unique_ptr<BinParameters> &&params) : TraceValueHandler<I + D>(
            std::move(params)),
                                                               parameters(
                                                                       static_cast<BinParameters *>(TraceValueHandler<
                                                                               I +
                                                                                       D>::getParameters()))
    { }

    using Cloned = typename TraceValueHandler<I + D>::Cloned;

    Cloned clone() const override
    { return Cloned(new BinValueHandler<I, D>(*this, parameters->clone())); }

    virtual ~BinValueHandler()
    { }

    bool process(const std::vector<TraceDispatch::OutputValue> &incoming,
                 bool deferrable = false) override
    {
        if (!TraceValueHandler<I + D>::process(incoming, deferrable))
            return false;
        updateBins();
        return true;
    }

    void trimToLast() override
    {
        TraceValueHandler<I + D>::trimToLast();
        updateBins();
    }

    void trimData(double start, double end) override
    {
        TraceValueHandler<I + D>::trimData(start, end);
        updateBins();
    }

    void setVisibleTimeRange(double start, double end) override
    {
        TraceValueHandler<I + D>::setVisibleTimeRange(start, end);
        updateBins();
    }

    inline const std::vector<BinPoint<I, D>> &getPoints() const
    { return points; }

    inline const TraceLimits<I + D> &getLimits() const
    { return limits; }

    /**
     * Test if the handler represents a histogram of values.
     * @return true if the handler is a histogram
     */
    inline bool isHistogram() const
    { return parameters->getAsHistogram(); }

protected:
    BinValueHandler(const BinValueHandler<I, D> &other, std::unique_ptr<TraceParameters> &&params)
            : TraceValueHandler<I + D>(other, std::move(params)),
              parameters(static_cast<BinParameters *>(TraceValueHandler<I + D>::getParameters())),
              limits(other.limits),
              points(other.points)
    { }

    void convertValue(const TraceDispatch::OutputValue &value,
                      std::vector<TracePoint<I + D>> &points) override
    {
        if (!parameters->getAsHistogram()) {
            TraceValueHandler<I + D>::convertValue(value, points);
            return;
        }
        if (value.isMetadata()) {
            for (std::size_t i = 0; i < I + D; i++) {
                TraceValueHandler<I + D>::updateMetadata(i, value.get(i));
            }
            return;
        }

        TracePoint<I + D> add;
        add.start = value.getStart();
        add.end = value.getEnd();
        for (std::size_t i = I; i < I + D; i++) {
            add.d[i] = 0.0;
        }
        for (std::size_t i = 0; i < I; i++) {
            switch (value.getType(i)) {
            case Data::Variant::Type::Array:
            case Data::Variant::Type::Matrix: {
                int idxStart = points.size();
                for (std::size_t j = i; j < I; j++) {
                    add.d[j] = FP::undefined();
                }
                for (; i < I; i++) {
                    this->handleValueFanout(points, add, idxStart, value.get(i), i);
                }
                return;
            }
            default:
                add.d[i] = Data::Variant::Composite::toNumber(value.get(i));
                break;
            }
        }
        points.emplace_back(std::move(add));
    }

    std::vector<TracePoint<I + D>> convertTransformerResult(const QVector<
            Algorithms::TransformerPoint *> &points) override
    {
        if (!parameters->getAsHistogram())
            return TraceValueHandler<I + D>::convertTransformerResult(points);

        std::vector<TracePoint<I + D> > result;
        result.reserve(points.size());
        for (auto it : points) {
            const typename TraceValueHandlerComplex<I + D,
                                                    TracePoint<I + D> >::template TransformValue<
                    I + D> *point = static_cast<const typename TraceValueHandlerComplex<I + D,
                                                                                        TracePoint<
                                                                                                I +
                                                                                                        D> >::template TransformValue<
                    I + D> *>(it);

            TracePoint<I + D> add;
            add.start = point->getValue().getStart();
            add.end = point->getValue().getEnd();
            for (std::size_t i = I; i < I + D; i++) {
                add.d[i] = 0.0;
            }
            for (std::size_t i = 0; i < I; i++) {
                add.d[i] = point->get(i);
            }
            result.emplace_back(std::move(add));
        }
        return result;
    }

    /**
     * Build the list of bins for the given dimension.  The three output
     * values must be the same size and must be non empty.
     * 
     * @param dim       the dimension number
     * @param starts    the output list of starts
     * @param ends      the output list of ends
     * @param centers   the output list of centers
     */
    virtual void buildBins(size_t dim,
                           std::vector<double> &starts,
                           std::vector<double> &ends,
                           std::vector<double> &centers)
    {
        double min = TraceValueHandler<I + D>::getLimits().min[dim];
        double max = TraceValueHandler<I + D>::getLimits().max[dim];
        if (!FP::defined(min)) {
            starts.emplace_back(min);
            ends.emplace_back(min);
            centers.emplace_back(min);
            return;
        }
        switch (parameters->getBinningType(dim)) {
        case BinParameters::Binning_FixedUniform: {
            int nBins = parameters->getBinningCount(dim);
            if (parameters->getBinningLogarithmic(dim)) {
                if (max <= 0.0 || max == min || nBins <= 0) {
                    starts.emplace_back(min);
                    ends.emplace_back(max);
                    centers.emplace_back((min + max) * 0.5);
                } else {
                    if (min <= 0.0)
                        min = max * 1E-6;
                    min = log(min);
                    max = log(max);
                    double width = (max - min) / (double) nBins;
                    for (int bin = 0; bin < nBins; bin++) {
                        double start = min + width * (double) bin;
                        double end = min + width * (double) (bin + 1);
                        starts.emplace_back(exp(start));
                        ends.emplace_back(exp(end));
                        centers.emplace_back(exp((start + end) * 0.5));
                    }
                }
            } else {
                double totalWidth = max - min;
                if (totalWidth == 0.0 || nBins <= 0) {
                    starts.emplace_back(min);
                    ends.emplace_back(min);
                    centers.emplace_back(min);
                } else {
                    double width = totalWidth / (double) nBins;
                    for (int bin = 0; bin < nBins; bin++) {
                        double start = min + width * (double) bin;
                        double end = min + width * (double) (bin + 1);
                        starts.emplace_back(start);
                        ends.emplace_back(end);
                        centers.emplace_back((start + end) * 0.5);
                    }
                }
            }
            break;
        }

        case BinParameters::Binning_FixedSize: {
            double width = parameters->getBinningSize(dim);
            if (width <= 0.0) {
                starts.emplace_back(min);
                ends.emplace_back(max);
                centers.emplace_back((min + max) * 0.5);
            } else {
                if (parameters->getBinningLogarithmic(dim)) {
                    min = log10(min);
                    max = log10(max);
                    double baseStart = floor(min / width) * width;
                    for (int n = 0; n < FIXEDBINSIZE_NUMBER_LIMIT; n++) {
                        double start = baseStart + width * (double) n;
                        if (n > 0 && start >= max)
                            break;
                        double end = baseStart + width * (double) (n + 1);
                        starts.emplace_back(pow(10.0, start));
                        ends.emplace_back(pow(10.0, end));
                        centers.emplace_back(pow(10.0, (start + end) * 0.5));
                    }
                } else {
                    double baseStart = floor(min / width) * width;
                    for (int n = 0; n < FIXEDBINSIZE_NUMBER_LIMIT; n++) {
                        double start = baseStart + width * (double) n;
                        if (n > 0 && start >= max)
                            break;
                        double end = baseStart + width * (double) (n + 1);
                        starts.emplace_back(start);
                        ends.emplace_back(end);
                        centers.emplace_back((start + end) * 0.5);
                    }
                }
            }
            break;
        }

        case BinParameters::Binning_Clustering: {
            const auto &inputPoints = TraceValueHandler<I + D>::getPoints();
            QVector<double> binPoints;
            binPoints.reserve(inputPoints.size());
            if (parameters->getBinningLogarithmic(dim)) {
                if (max <= 0.0) {
                    starts.emplace_back(min);
                    ends.emplace_back(max);
                    centers.emplace_back((min + max) * 0.5);
                    break;
                }
                if (min <= 0.0)
                    min = max * 1E-6;
                min = log10(min);
                max = log10(max);
                for (const auto &add : inputPoints) {
                    if (!FP::defined(add.d[dim]) || add.d[dim] <= 0.0) {
                        binPoints.push_back(FP::undefined());
                    } else {
                        binPoints.push_back(log10(add.d[dim]));
                    }
                }
            } else {
                for (const auto &add : inputPoints) {
                    binPoints.push_back(add.d[dim]);
                }
            }

            QVector<double> breaks;
            auto bins = Algorithms::LinearCluster::cluster(binPoints, &breaks,
                                                           parameters->getBinningSize(dim));
            if (breaks.empty()) {
                if (parameters->getBinningLogarithmic(dim)) {
                    centers.emplace_back(pow(10.0, (max + min) * 0.5));
                    starts.emplace_back(pow(10.0, min));
                    ends.emplace_back(pow(10.0, max));
                } else {
                    centers.emplace_back((max + min) * 0.5);
                    starts.emplace_back(min);
                    ends.emplace_back(max);
                }
                break;
            }

            auto pointOrigin = binPoints.begin();
            auto binTarget = bins.begin();
            std::vector<std::size_t> binCounts;
            for (auto endBin = bins.end(); binTarget != endBin; ++binTarget, ++pointOrigin) {
                auto binID = *binTarget;
                if (binID > FIXEDBINSIZE_NUMBER_LIMIT)
                    continue;
                if (binID < 0)
                    continue;
                while (static_cast<std::size_t>(binID) >= starts.size()) {
                    if (starts.empty()) {
                        starts.emplace_back(min);
                    } else {
                        starts.emplace_back(breaks[starts.size() - 1]);
                    }
                    if (ends.size() >= static_cast<std::size_t>(breaks.size())) {
                        ends.emplace_back(max);
                    } else {
                        ends.emplace_back(breaks.at(ends.size()));
                    }
                    centers.emplace_back(FP::undefined());
                    binCounts.emplace_back(0);
                }
                if (!FP::defined(centers[binID])) {
                    centers[binID] = *pointOrigin;
                    binCounts[binID] = 1;
                } else {
                    centers[binID] += *pointOrigin;
                    binCounts[binID]++;
                }
            }

            auto binModify = centers.begin();
            for (auto countTarget = binCounts.begin(), endTarget = binCounts.end();
                    countTarget != endTarget;
                    ++countTarget, ++binModify) {
                if (*countTarget > 0) {
                    *binModify /= (double) (*countTarget);
                }
            }

            if (parameters->getBinningLogarithmic(dim)) {
                for (auto &m : centers) {
                    if (!FP::defined(m))
                        continue;
                    m = pow(10.0, m);
                }
                for (auto &m : starts) {
                    if (!FP::defined(m))
                        continue;
                    m = pow(10.0, m);
                }
                for (auto &m : ends) {
                    if (!FP::defined(m))
                        continue;
                    m = pow(10.0, m);
                }
            }

            break;
        }

        case BinParameters::Binning_Static: {
            const auto &stat = parameters->getBinningStatic(dim);
            if (stat.empty()) {
                starts.emplace_back(min);
                ends.emplace_back(max);
                centers.emplace_back((min + max) * 0.5);
            } else {
                for (const auto &it : stat) {
                    starts.emplace_back(it.start);
                    ends.emplace_back(it.end);
                    centers.emplace_back(it.center);
                }
            }
            break;
        }

        }
    }

    /**
     * Apply the bin filter to the given points.
     * 
     * @param input     the input point list
     * @param start     the bin start
     * @param end       the bin end
     * @param dim       the dimension number
     * @return          the list of points that belong to the bin
     */
    virtual std::vector<TracePoint<I + D>> applyFilter(std::vector<TracePoint<I + D>> input,
                                                       double start,
                                                       double end,
                                                       std::size_t dim)
    {
        std::vector<TracePoint<I + D>> result;
        if (FP::equal(end, TraceValueHandler<I + D>::getLimits().max[dim])) {
            for (const auto &it : input) {
                if (!FP::defined(it.d[dim])) {
                    if (!FP::defined(start) || FP::defined(end))
                        continue;
                } else {
                    if (FP::defined(start) && it.d[dim] < start)
                        continue;
                    if (FP::defined(end) && it.d[dim] > end)
                        continue;
                }
                result.emplace_back(it);
            }
        } else {
            for (const auto &it : input) {
                if (!FP::defined(it.d[dim])) {
                    if (!FP::defined(start) || FP::defined(end))
                        continue;
                } else {
                    if (FP::defined(start) && it.d[dim] < start)
                        continue;
                    if (FP::defined(end) && it.d[dim] >= end)
                        continue;
                }
                result.emplace_back(it);
            }
        }
        return result;
    }

    /**
     * Convert the points in a bin into a single bin point.  The caller will
     * set the width and center of the bin.  This implementation calls
     * setQuantiles( std::vector<TracePoint<I+D> > &points, double &, double &
     * double &, double &, double & ) to set the values.
     * 
     * @param points    the points to process
     * @param output    the output point
     */
    virtual void convertBin(const std::vector<TracePoint<I + D>> &points, BinPoint<I, D> &output)
    {

        std::vector<double> sort;
        sort.reserve(points.size());
        for (std::size_t i = 0; i < D; i++) {
            sort.clear();
            for (const auto &it : points) {
                if (!FP::defined(it.d[I + i]))
                    continue;
                sort.push_back(it.d[I + i]);
            }
            std::sort(sort.begin(), sort.end());
            setQuantiles(sort, output.lowest[i], output.lower[i], output.middle[i], output.upper[i],
                         output.uppermost[i]);
            output.total[i] = sort.size();
        }
    }

    /**
     * Convert the points in a bin into a single bin point.  The caller will
     * set the width and center of the bin.  This implementation uses
     * the time covered by the points to calculate the fraction of the total
     * time the point represents.  The lower bound is set to zero and the 
     * upper one is set to that fraction.
     * 
     * @param points            the points to process
     * @param totalTime         the total time in seconds covered by all data
     * @param effectiveStart    the effective start time of data
     * @param effectiveEnd      the effective end time of data
     * @param output            the output point
     */
    virtual void convertHistogram(const std::vector<TracePoint<I + D>> &points,
                                  const double totalTime[D],
                                  double effectiveStart,
                                  double effectiveEnd,
                                  BinPoint<I, D> &output)
    {
        for (std::size_t i = 0; i < D; i++) {
            double coveredTime = 0.0;
            for (const auto &it : points) {
                if (!FP::defined(it.d[I + i]))
                    continue;
                double start = it.start;
                if (!FP::defined(start))
                    start = effectiveStart;
                if (Range::compareStart(start, TraceValueHandler<I + D>::getStart()) < 0)
                    start = TraceValueHandler<I + D>::getStart();
                double end = it.end;
                if (!FP::defined(end))
                    end = effectiveEnd;
                if (Range::compareEnd(end, TraceValueHandler<I + D>::getEnd()) > 0)
                    end = TraceValueHandler<I + D>::getEnd();
                if (!FP::defined(start) || !FP::defined(end)) {
                    coveredTime += 1.0;
                    continue;
                }
                if (end <= start)
                    continue;
                coveredTime += end - start;
            }
            output.lowest[i] = FP::undefined();
            output.lower[i] = 0.0;
            output.middle[i] = FP::undefined();
            if (totalTime[i] > 0.0)
                output.upper[i] = (coveredTime / totalTime[i]) * 100.0;
            else
                output.upper[i] = 0.0;
            output.uppermost[i] = FP::undefined();
            output.total[i] = coveredTime;
        }
    }

    /**
     * Set the bin limits based on the parameter quantiles.
     * 
     * @param sorted    the input sorted points
     * @param lowest    the output lowest value
     * @param lower     the output lower value
     * @param middle    the output middle value
     * @param upper     the output upper value
     * @param uppermost the output uppermost value
     */
    virtual void setQuantiles(const std::vector<double> &sorted,
                              double &lowest,
                              double &lower,
                              double &middle,
                              double &upper,
                              double &uppermost)
    {

        switch (parameters->getQuantileMode()) {
        case BinParameters::Standard:
            if (!FP::defined(parameters->getWhiskerQuantile())) {
                lowest = FP::undefined();
                uppermost = FP::undefined();
            } else {
                lowest = Algorithms::Statistics::quantile(sorted, parameters->getWhiskerQuantile());
                uppermost = Algorithms::Statistics::quantile(sorted, 1.0 -
                        parameters->getWhiskerQuantile());
            }

            if (!FP::defined(parameters->getBoxQuantile())) {
                lower = FP::undefined();
                upper = FP::undefined();
            } else {
                lower = Algorithms::Statistics::quantile(sorted, parameters->getBoxQuantile());
                upper = Algorithms::Statistics::quantile(sorted,
                                                         1.0 - parameters->getBoxQuantile());
            }

            middle = Algorithms::Statistics::quantile(sorted, 0.5);
            break;

        case BinParameters::Normal: {
            double mean, sd;
            Algorithms::Statistics::meanSD(sorted, mean, sd);

            middle = mean;
            if (!FP::defined(sd) || !FP::defined(mean) || sd <= 0.0) {
                lower = FP::undefined();
                upper = FP::undefined();
                lowest = FP::undefined();
                uppermost = FP::undefined();
            } else {
                if (!FP::defined(parameters->getWhiskerQuantile())) {
                    lowest = FP::undefined();
                    uppermost = FP::undefined();
                } else {
                    double sds = Algorithms::Statistics::normalQuantile(
                            parameters->getWhiskerQuantile());
                    lowest = mean + sds * sd;
                    uppermost = mean - sds * sd;
                }

                if (!FP::defined(parameters->getBoxQuantile())) {
                    lower = FP::undefined();
                    upper = FP::undefined();
                } else {
                    double sds =
                            Algorithms::Statistics::normalQuantile(parameters->getBoxQuantile());
                    lower = mean + sds * sd;
                    upper = mean - sds * sd;
                }
            }

            break;
        }

        }
    }
};

/**
 * A handler for a set of multiple binning dispatches.  This produces a 
 * number of bin value handlers that represent the actual bin data.
 */
template<std::size_t I, std::size_t D>
class BinDispatchSet : public TraceDispatchSet<I + D> {
public:
    BinDispatchSet() = default;

    virtual ~BinDispatchSet() = default;

    using Cloned = typename TraceDispatchSet<I + D>::Cloned;

    using CreatedHandler = typename TraceDispatchSet<I + D>::CreatedHandler;

    Cloned clone() const override
    { return Cloned(new BinDispatchSet<I, D>(*this)); }

protected:
    BinDispatchSet(const BinDispatchSet<I, D> &other) : TraceDispatchSet<I + D>(other)
    { }

    std::unique_ptr<
            TraceParameters> parseParameters(const Data::Variant::Read &value) const override
    { return std::unique_ptr<TraceParameters>(new BinParameters(value)); }

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override
    {
        return CreatedHandler(new BinValueHandler<I, D>(std::unique_ptr<BinParameters>(
                static_cast<BinParameters *>(parameters.release()))));
    }
};

}
}

#endif
