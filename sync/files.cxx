/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QEventLoop>
#include <QLoggingCategory>

#include "sync/files.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"
#include "io/drivers/file.hxx"


Q_LOGGING_CATEGORY(log_sync_upload, "cpd3.sync.upload", QtWarningMsg)

Q_LOGGING_CATEGORY(log_sync_download, "cpd3.sync.download", QtWarningMsg)


/** @file sync/files.hxx
 * Synchronization base on file exchange.
 */

using namespace CPD3::Data;
using namespace CPD3::Transfer;

namespace CPD3 {
namespace Sync {

SyncUpload::SyncUpload(Filter *local,
                       const Variant::Read &encryption,
                       const Variant::Read &signature,
                       const Variant::Read &upload,
                       double modifiedTime,
                       const std::shared_ptr<Archive::Access> &writeA,
                       const std::shared_ptr<Archive::Access> &readA) : localFilter(local),
                                                                        encryption(encryption),
                                                                        signature(signature),
                                                                        upload(upload),
                                                                        readAccess(readA),
                                                                        writeAccess(writeA),
                                                                        modifiedTime(modifiedTime),
                                                                        ownWriter(false),
                                                                        mutex(),
                                                                        terminated(false),
                                                                        success(false),
                                                                        readSink(*this),
                                                                        resultInformation()
{
    this->encryption.detachFromRoot();
    this->signature.detachFromRoot();
    this->upload.detachFromRoot();
    if (!readAccess)
        readAccess = std::make_shared<Archive::Access>();
    if (!writeAccess)
        writeAccess = std::make_shared<Archive::Access>();
}

SyncUpload::~SyncUpload()
{
    if (referenceOperation) {
        referenceOperation->signalTerminate();
        referenceOperation->wait();
        referenceOperation.reset();
    }

    if (readOperation) {
        readOperation->signalTerminate();
        readOperation->wait();
        readOperation.reset();
    }
}

void SyncUpload::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    terminateRequested();
}

bool SyncUpload::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

bool SyncUpload::succeeded()
{
    std::lock_guard<std::mutex> lock(mutex);
    return success;
}

std::unique_ptr<
        CPD3::Transfer::FileUploader> SyncUpload::createUploader(const CPD3::Data::Variant::Read &configuration,
                                                                 IO::Access::Handle inputFile)
{
    std::unique_ptr<CPD3::Transfer::FileUploader> result(new FileUploader(configuration));
    result->addUpload(std::move(inputFile));
    return std::move(result);
}

void SyncUpload::run()
{
    double startTime = Time::time();

    qCDebug(log_sync_upload) << "Starting file upload";

    feedback.emitStage(tr("Exporting data"),
                       tr("Data are being extracted and placed into a precursor file."), false);

    auto dataFile = IO::Access::temporaryFile(true);
    if (!dataFile) {
        feedback.emitFailure(tr("Error opening temporary file"));
        return;
    }

    {
        auto targetStream = dataFile->stream();
        if (!targetStream)
            return;
        targetStream->start();
        if (!exportData(std::move(targetStream), [&dataFile]() -> std::uint_fast64_t {
            return QFileInfo(QString::fromStdString(dataFile->filename())).size();
        }))
            return;
    }

    double exportEndTime = Time::time();

    if (testTerminated())
        return;

    auto totalSize = dataFile->size();
    if (totalSize == 0) {
        dataFile.reset();
        qCDebug(log_sync_upload) << "Output is zero size, no new values available";
        std::lock_guard<std::mutex> lock(mutex);
        success = true;
        return;
    }

    qCDebug(log_sync_upload) << "Precursor of size" << totalSize << "byte(s) created in"
                             << Logging::elapsed(exportEndTime - startTime);

    resultInformation.write().hash("DataSize").setInt64(totalSize);
    resultInformation.write().hash("ExportTime").setDouble(exportEndTime - startTime);

    auto uploadFile = IO::Access::temporaryFile(true);
    if (!uploadFile) {
        feedback.emitFailure(tr("Error opening temporary file"));
        dataFile.reset();
        return;
    }

    if (!packageOutput(dataFile, uploadFile)) {
        return;
    }
    dataFile.reset();

    double packageEndTime = Time::time();
    totalSize = uploadFile->size();

    qCDebug(log_sync_upload) << "Upload file of" << totalSize << "byte(s) created in"
                             << Logging::elapsed(packageEndTime - exportEndTime);

    resultInformation.write().hash("UploadSize").setInt64(totalSize);
    resultInformation.write().hash("PackageTime").setDouble(packageEndTime - exportEndTime);

    if (!uploadPackage(std::move(uploadFile)))
        return;
    uploadFile.reset();

    double endTime = Time::time();

    resultInformation.write().hash("UploadTime").setDouble(endTime - packageEndTime);
    resultInformation.write().hash("TotalTime").setDouble(endTime - startTime);

    qCDebug(log_sync_upload) << "File upload completed after"
                             << Logging::elapsed(endTime - startTime);

    std::lock_guard<std::mutex> lock(mutex);
    success = true;
}

QString SyncUpload::toByteUnits(qint64 n)
{
    if (n > 1000 * 1000 * 1000) {
        return tr("%1 GiB").arg((double) n / (double) (1024 * 1024 * 1024), 0, 'f', 2);
    } else if (n > 1000 * 1000) {
        return tr("%1 MiB").arg((double) n / (double) (1024 * 1024), 0, 'f', 2);
    } else if (n > 1000) {
        return tr("%1 KiB").arg((double) n / (double) (1024), 0, 'f', 2);
    } else {
        return tr("%n bytes", "", (int) n);
    }
}

bool SyncUpload::exportData(std::unique_ptr<IO::Generic::Stream> &&target,
                            const std::function<std::uint_fast64_t()> &totalSize)
{
    DataPack packer(std::move(target), true, true);

    Q_ASSERT(readAccess);
    Q_ASSERT(writeAccess);

    if (testTerminated())
        return false;

    {
        auto selections = localFilter->toArchiveSelection(modifiedTime);
        if (selections.empty()) {
            qCDebug(log_sync_upload) << "No data selections available";
            return true;
        }

        referenceOperation = writeAccess->writeRemoteReferenced();

        readSink.target = &packer;
        readOperation = readAccess->readErasure(selections, &readSink);
    }

    packer.start();
    for (;;) {
        QEventLoop loop;
        terminateRequested.connect(&loop, std::bind(&QEventLoop::quit, &loop), true);
        packer.finished.connect(&loop, std::bind(&QEventLoop::quit, &loop), true);
        if (totalSize) {
            QTimer::singleShot(500, &loop, std::bind(&QEventLoop::quit, &loop));
        }

        if (testTerminated()) {
            packer.signalTerminate();
            readOperation->signalTerminate();
            readOperation->wait();
            referenceOperation->signalTerminate();
            packer.wait();
            return false;
        }
        if (packer.isFinished())
            break;

        if (totalSize) {
            feedback.emitState(tr("%1 written").arg(toByteUnits(totalSize())));
        }
        loop.exec();
    }

    packer.wait();
    if (totalSize()) {
        feedback.emitState(tr("%1 total").arg(toByteUnits(totalSize())));
    }

    readOperation->wait();
    readOperation.reset();

    referenceOperation->wait();
    referenceOperation.reset();

    readSink.target = nullptr;

    readAccess.reset();
    writeAccess.reset();

    return true;
}

bool SyncUpload::packageOutput(const IO::Access::Handle &input, const IO::Access::Handle &output)
{
    TransferPack packer;
    Threading::Receiver rx;
    terminateRequested.connect(rx, std::bind(&TransferPack::signalTerminate, &packer));
    packer.feedback.forward(feedback);

    packer.compressStage();
    packer.encryptStage(encryption);
    packer.signStage(signature);
    packer.checksumStage();

    if (testTerminated())
        return false;
    return packer.exec(input, output);
}

bool SyncUpload::uploadPackage(IO::Access::Handle file)
{
    auto uploader = createUploader(upload, std::move(file));
    if (!uploader)
        return false;

    Threading::Receiver rx;
    terminateRequested.connect(rx, std::bind(&FileUploader::signalTerminate, uploader.get()));
    uploader->feedback.forward(feedback);
    if (testTerminated())
        return false;

    return uploader->exec();
}

QByteArray SyncUpload::getFilterState() const
{
    Q_ASSERT(localFilter);
    return localFilter->state();
}

SyncUpload::ReadSink::ReadSink(SyncUpload &upload) : upload(upload), target(nullptr)
{ }

SyncUpload::ReadSink::~ReadSink() = default;

void SyncUpload::ReadSink::incomingValue(const ArchiveValue &value)
{
    Q_ASSERT(upload.localFilter);
    upload.localFilter->advance(value.getStart());
    if (!upload.localFilter->accept(value))
        return;

    if (!value.isRemoteReferenced() && upload.referenceOperation)
        upload.referenceOperation->incomingValue(value);
    Q_ASSERT(target);
    target->incomingValue(value);
}

void SyncUpload::ReadSink::incomingValue(ArchiveValue &&value)
{
    Q_ASSERT(upload.localFilter);
    upload.localFilter->advance(value.getStart());
    if (!upload.localFilter->accept(value))
        return;

    if (!value.isRemoteReferenced() && upload.referenceOperation)
        upload.referenceOperation->incomingValue(value);
    Q_ASSERT(target);
    target->incomingValue(std::move(value));
}

void SyncUpload::ReadSink::incomingErasure(const ArchiveErasure &erasure)
{
    Q_ASSERT(upload.localFilter);
    upload.localFilter->advance(erasure.getStart());
    if (!upload.localFilter->accept(erasure))
        return;

    Q_ASSERT(target);
    target->incomingErasure(erasure);
}

void SyncUpload::ReadSink::incomingErasure(ArchiveErasure &&erasure)
{
    Q_ASSERT(upload.localFilter);
    upload.localFilter->advance(erasure.getStart());
    if (!upload.localFilter->accept(erasure))
        return;

    Q_ASSERT(target);
    target->incomingErasure(std::move(erasure));
}

void SyncUpload::ReadSink::endStream()
{
    if (upload.referenceOperation)
        upload.referenceOperation->endData();
    Q_ASSERT(target);
    target->endStream();
}


SyncDownload::SyncDownload(const Variant::Read &download,
                           const std::shared_ptr<Archive::Access> &writeA) : download(download),
                                                                             writeAccess(writeA),
                                                                             ownWriter(false),
                                                                             mutex(),
                                                                             terminated(false),
                                                                             success(false)
{
    if (!writeAccess)
        writeAccess = std::make_shared<Archive::Access>();
}

SyncDownload::~SyncDownload()
{
    if (synchronizeOperation) {
        synchronizeOperation->signalTerminate();
        synchronizeOperation->wait();
        synchronizeOperation.reset();
    }
}

void SyncDownload::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    emit terminateRequested();
}

bool SyncDownload::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

bool SyncDownload::succeeded()
{
    std::lock_guard<std::mutex> lock(mutex);
    return success;
}

void SyncDownload::completeDownloader(FileDownloader *downloader)
{
    Q_UNUSED(downloader);
}

bool SyncDownload::signatureAuthorized(const QString &authorization)
{
    Q_UNUSED(authorization);
    return true;
}

QString SyncDownload::applySubstitutions(const QString &input) const
{ return input; }

void SyncDownload::pushInvokeStage(const QString &input, const QString &output)
{ }

void SyncDownload::popInvokeStage()
{ }

static bool fileSortCompare(const std::unique_ptr<DownloadFile> &a, const std::unique_ptr<DownloadFile> &b)
{
    double ftA = a->getFileTime();
    double ftB = b->getFileTime();
    if (FP::defined(ftA)) {
        if (FP::defined(ftB)) {
            if (ftA != ftB)
                return ftA < ftB;
        } else {
            return false;
        }
    } else if (FP::defined(ftB)) {
        return true;
    }

    double mtA = a->getModifiedTime();
    double mtB = b->getModifiedTime();
    if (FP::defined(mtA)) {
        if (FP::defined(mtB)) {
            if (mtA != mtB)
                return mtA < mtB;
        } else {
            return false;
        }
    } else if (FP::defined(mtB)) {
        return true;
    }

    return a->fileName() < b->fileName();
}

void SyncDownload::run()
{
    double startTime = Time::time();

    qCDebug(log_sync_download) << "Starting file download";

    std::unique_ptr<FileDownloader> downloader(createDownloader(download));
    Threading::Receiver rx;
    terminateRequested.connect(rx, std::bind(&FileDownloader::signalTerminate, downloader.get()));
    downloader->feedback.forward(feedback);
    if (testTerminated())
        return;

    if (!downloader->exec(download))
        return;

    auto files = downloader->takeFiles();
    if (files.empty()) {
        qCDebug(log_sync_download) << "No new files to process";
    } else {
        std::stable_sort(files.begin(), files.end(), fileSortCompare);

        bool first = true;
        bool ok = true;
        for (auto file = files.begin(), endFiles = files.end();
                file != endFiles;
                ++file, first = false) {

            double fileStart = Time::time();

            Variant::Root fileInformation;
            QString name((*file)->fileName());
            auto fileInformationOutput = fileInformation.write();
            if (!processFile(std::move(*file), fileInformationOutput, first)) {
                fileInformation["Error"].setBool(true);
                ok = false;
            }
            file->reset();

            double fileEnd = Time::time();
            qCDebug(log_sync_download) << "Completed file" << name << "after"
                                       << Logging::elapsed(fileEnd - fileStart);
            fileInformation["ProcessingTime"].setDouble(fileEnd - fileStart);

            results.push_back(std::move(fileInformation));

            if (!ok) {
                if (first) {
                    break;
                } else {
                    continue;
                }
            }
        }

        if (!ok) {
            if (!first) {
                qCWarning(log_sync_download)
                    << "Processing failure after the first file; this may result in incomplete or unpredictable synchronized data";
            }
            qCDebug(log_sync_download) << "Aborted processing due to an error";
            if (synchronizeOperation) {
                synchronizeOperation->signalTerminate();
                synchronizeOperation->wait();
                synchronizeOperation.reset();
            }
            return;
        }
    }

    if (testTerminated()) {
        if (synchronizeOperation) {
            synchronizeOperation->signalTerminate();
            synchronizeOperation->wait();
            synchronizeOperation.reset();
        }
        return;
    }

    if (synchronizeOperation)
        synchronizeOperation->endStream();

    completeDownloader(downloader.get());
    downloader.reset();

    if (synchronizeOperation) {
        synchronizeOperation->wait();
        synchronizeOperation.reset();
    }

    double endTime = Time::time();

    qCDebug(log_sync_download) << "File download completed after "
                               << Logging::elapsed(endTime - startTime);

    std::lock_guard<std::mutex> lock(mutex);
    success = true;
}

bool SyncDownload::processFile(std::unique_ptr<Transfer::DownloadFile> &&file,
                               CPD3::Data::Variant::Write &fileInformation,
                               bool canTerminate)
{
    QFileInfo local(file->fileInfo());
    qCDebug(log_sync_download) << "Processing file" << file->fileName() << "from local file"
                               << local.absoluteFilePath();

    feedback.emitStage(tr("Process %1").arg(file->fileName()),
                       tr("The system is processing %1.").arg(file->fileName()), true);

    fileInformation.hash("Local").setString(local.absoluteFilePath());
    fileInformation.hash("BaseName").setString(file->fileName());
    fileInformation.hash("Matched").setString(file->getPattern());
    fileInformation.hash("Writable").setBool(file->writable());
    fileInformation.hash("ModifiedTime").setDouble(file->getModifiedTime());
    fileInformation.hash("FileTime").setDouble(file->getFileTime());

    auto inputFile = IO::Access::file(local, IO::File::Mode::readOnly());
    if (!inputFile) {
        qCDebug(log_sync_download) << "Failed to open local file";
        return false;
    }

    auto dataFile = IO::Access::temporaryFile(true);
    if (!dataFile) {
        qCDebug(log_sync_download) << "Failed to open temporary file";
        return false;
    }

    if (testTerminated())
        return false;

    auto authorization = unpackFile(*file, inputFile, dataFile, fileInformation, canTerminate);
    inputFile.reset();

    bool doRemove = file->writable();
    if (authorization.isEmpty()) {
        if (doRemove && (!canTerminate || !testTerminated()))
            QFile::remove(local.absoluteFilePath());
        return true;
    }

    if (dataFile->size() == 0) {
        qCDebug(log_sync_download) << "Unpacked file is empty, no action taken";
        if (doRemove)
            QFile::remove(local.absoluteFilePath());
        return true;
    }

    if (!unpackData(dataFile, authorization, fileInformation, canTerminate))
        return false;

    if (doRemove)
        QFile::remove(local.absoluteFilePath());
    return true;
}

QString SyncDownload::unpackFile(Transfer::DownloadFile &file,
                                 const IO::Access::Handle &input,
                                 const IO::Access::Handle &output,
                                 CPD3::Data::Variant::Write &fileInformation,
                                 bool canTerminate)
{
    FileUnpacker unpacker(*this, file);
    Threading::Receiver rx;
    unpacker.feedback.forward(feedback);
    if (canTerminate) {
        terminateRequested.connect(rx, std::bind(&FileUnpacker::signalTerminate, &unpacker));
        if (testTerminated())
            return QString();
    }

    unpacker.requireChecksum();
    unpacker.requireSignature(
            TransferUnpack::Signature_SingleOnly | TransferUnpack::Signature_IncludeCertificate);
    unpacker.requireEncryption();
    unpacker.requireCompression();
    unpacker.requireEnd();

    if (!unpacker.exec(input, output)) {
        if (unpacker.isUnauthorized()) {
            qCDebug(log_sync_download) << "File authorization" << unpacker.getSigningCertificateID()
                                       << "denied";

            fileInformation["Unauthorized"].setBool(true);
            fileInformation["UnauthorizedDigest"].setString(unpacker.getSigningCertificateID());
            return QString();
        }

        qCDebug(log_sync_download) << "File unpacking failed:" << unpacker.errorString();

        fileInformation["Corrupt"].setBool(true);
        fileInformation["CorruptReason"].setString(unpacker.errorString());
        return QString();
    }

    QString authorizationDigest(unpacker.getSigningCertificateID());
    if (authorizationDigest.isEmpty()) {
        qCDebug(log_sync_download) << "File has no signature";

        fileInformation["Corrupt"].setBool(true);
        fileInformation["CorruptReason"].setString(tr("File has no signature"));
        return QString();
    }

    fileInformation["AuthorizationDigest"].setString(authorizationDigest);

    qCDebug(log_sync_download) << "Accepted authorization" << authorizationDigest;
    return authorizationDigest;
}

SyncDownload::FileUnpacker::FileUnpacker(SyncDownload &p, DownloadFile &f) : parent(p),
                                                                             file(f),
                                                                             signingCertificate(),
                                                                             signatureRejected(
                                                                                     false)
{ }

SyncDownload::FileUnpacker::~FileUnpacker() = default;

QString SyncDownload::FileUnpacker::getSigningCertificateID() const
{ return signingCertificate; }

bool SyncDownload::FileUnpacker::isUnauthorized() const
{ return signatureRejected; }

QString SyncDownload::FileUnpacker::applySubstitutions(const QString &input) const
{ return parent.applySubstitutions(input); }

CPD3::Data::Variant::Read SyncDownload::FileUnpacker::getKeyForCertificate(const QByteArray &id,
                                                                           int depth)
{
    if (depth != 2)
        return Variant::Read::empty();
    QString name(QString::fromLatin1(id.toHex().toLower()));
    return parent.decryptionInformation(signingCertificate, name, &file);
}

bool SyncDownload::FileUnpacker::signatureAuthorized(const QList<QByteArray> &ids, int depth)
{
    if (depth != 1)
        return false;
    if (ids.size() != 1)
        return false;

    QString name(QString::fromLatin1(ids.first().toHex().toLower()));
    signingCertificate = name;
    if (!parent.signatureAuthorized(name)) {
        signatureRejected = true;
        return false;
    }
    return true;
}


bool SyncDownload::unpackData(const IO::Access::Handle &input,
                              const QString &authorization,
                              CPD3::Data::Variant::Write &fileInformation,
                              bool canTerminate)
{
    Filter *filter = createFilter(authorization);
    if (!filter) {
        fileInformation["Corrupt"].setBool(true);
        fileInformation["CorruptReason"].setString(tr("No data filter available"));
        return false;
    }
    UnpackSink sink(*this, filter);

    /* End the one from a prior file, if we have it */
    if (synchronizeOperation) {
        synchronizeOperation->endStream();
        synchronizeOperation->wait();
        synchronizeOperation.reset();
    }

    Q_ASSERT(writeAccess);
    Q_ASSERT(!synchronizeOperation);
    synchronizeOperation = writeAccess->writeSynchronize();

    DataUnpack unpacker;
    Threading::Receiver rx;
    unpacker.feedback.forward(feedback);
    if (canTerminate) {
        terminateRequested.connect(rx, std::bind(&DataUnpack::signalTerminate, &unpacker));
        if (testTerminated()) {
            synchronizeOperation->signalTerminate();
            synchronizeOperation->endStream();
            synchronizeOperation->wait();
            synchronizeOperation.reset();
            return false;
        }
    }

    if (!unpacker.exec(input, &sink)) {
        sink.injectInformation(fileInformation);

        synchronizeOperation->signalTerminate();
        synchronizeOperation->endStream();
        synchronizeOperation->wait();
        synchronizeOperation.reset();

        fileInformation["Corrupt"].setBool(true);
        fileInformation["CorruptReason"].setString(unpacker.errorString());
        return true;
    }

    sink.injectInformation(fileInformation);
    return true;
}


SyncDownload::UnpackSink::UnpackSink(SyncDownload &p, Filter *f) : parent(p),
                                                                   filter(f),
                                                                   acceptedValuesCount(0),
                                                                   rejectedValuesCount(0),
                                                                   acceptedErasureCount(0),
                                                                   rejectedErasureCount(0)
{ }

SyncDownload::UnpackSink::~UnpackSink() = default;

void SyncDownload::UnpackSink::incomingValue(const ArchiveValue &value)
{
    filter->advance(value.getStart());
    if (!filter->accept(value)) {
        ++rejectedValuesCount;
        return;
    }

    ++acceptedValuesCount;

    Q_ASSERT(parent.synchronizeOperation);
    parent.synchronizeOperation->incomingValue(value);
}

void SyncDownload::UnpackSink::incomingValue(ArchiveValue &&value)
{
    filter->advance(value.getStart());
    if (!filter->accept(value)) {
        ++rejectedValuesCount;
        return;
    }

    ++acceptedValuesCount;

    Q_ASSERT(parent.synchronizeOperation);
    parent.synchronizeOperation->incomingValue(std::move(value));
}

void SyncDownload::UnpackSink::incomingErasure(const ArchiveErasure &value)
{
    filter->advance(value.getStart());
    if (!filter->accept(value)) {
        ++rejectedErasureCount;
        return;
    }

    ++acceptedErasureCount;

    Q_ASSERT(parent.synchronizeOperation);
    parent.synchronizeOperation->incomingErasure(value);
}

void SyncDownload::UnpackSink::incomingErasure(ArchiveErasure &&value)
{
    filter->advance(value.getStart());
    if (!filter->accept(value)) {
        ++rejectedErasureCount;
        return;
    }

    ++acceptedErasureCount;

    Q_ASSERT(parent.synchronizeOperation);
    parent.synchronizeOperation->incomingErasure(std::move(value));
}

void SyncDownload::UnpackSink::endStream()
{ }

void SyncDownload::UnpackSink::injectInformation(Variant::Write &fileInformation) const
{
    fileInformation["AcceptedValues"].setInt64(acceptedValuesCount);
    fileInformation["RejectedValues"].setInt64(rejectedValuesCount);
    fileInformation["AcceptedDeleted"].setInt64(acceptedErasureCount);
    fileInformation["RejectedDeleted"].setInt64(rejectedErasureCount);
}


}
}


