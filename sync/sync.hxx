/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SYNC_H
#define CPD3SYNC_H

#include <QtCore/QtGlobal>

#if defined(cpd3sync_EXPORTS)
#   define CPD3SYNC_EXPORT Q_DECL_EXPORT
#else
#   define CPD3SYNC_EXPORT Q_DECL_IMPORT
#endif

#endif
