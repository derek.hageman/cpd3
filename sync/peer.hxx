/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SYNCPEER_H
#define CPD3SYNCPEER_H

#include "core/first.hxx"

#include <vector>
#include <deque>
#include <memory>
#include <mutex>
#include <QtGlobal>
#include <QWaitCondition>
#include <QVector>
#include <QHash>
#include <QList>
#include <QTimer>
#include <QByteArray>

#include "sync/sync.hxx"
#include "sync/filter.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/stream.hxx"
#include "core/actioncomponent.hxx"
#include "core/threading.hxx"

namespace CPD3 {
namespace Sync {

/**
 * A synchronizing connection to a peer.
 */
class CPD3SYNC_EXPORT Peer : public QObject {
Q_OBJECT

public:
    /**
     * The possible states of error for the peer.
     */
    enum ErrorState {
        /** No Error */
                Error_None, /** Fatal error, no recovery possible */
                Error_Fatal, /** Recoverable error, rollback possible */
                Error_Recoverable,
    };
private:

    std::unique_ptr<Filter> localFilter;
    std::unique_ptr<Filter> remoteFilter;
    double modifiedAfter;

    std::unique_ptr<CPD3::Data::Archive::Access> readAccess;
    CPD3::Data::Archive::Access::ErasureHandle readOperation;

    std::unique_ptr<CPD3::Data::Archive::Access> writeAccess;
    CPD3::Data::Archive::Access::SynchronizationHandle synchronizeOperation;
    CPD3::Data::Archive::Access::RemoteReferencedHandle referenceOperation;

    class ReadSink : public CPD3::Data::ErasureSink {
        Peer &peer;

        std::mutex mutex;

        std::deque<CPD3::Data::ArchiveValue> values;
        std::deque<CPD3::Data::ArchiveErasure> erasures;
        bool isEnded;
    public:
        ReadSink(Peer &peer);

        virtual ~ReadSink();

        virtual void incomingValue(const CPD3::Data::ArchiveValue &value);

        virtual void incomingValue(CPD3::Data::ArchiveValue &&value);

        virtual void incomingErasure(const CPD3::Data::ArchiveErasure &erasure);

        virtual void incomingErasure(CPD3::Data::ArchiveErasure &&erasure);

        virtual void endStream();

        bool take(std::deque<CPD3::Data::ArchiveValue> &v,
                  std::deque<CPD3::Data::ArchiveErasure> &e);


        Threading::Signal<> updated;
    };

    ReadSink readSink;

    enum {
        /* Awaiting startup to send initialization data */
                Output_Initialize,

        /* Sending the remote request data */
                Output_RequestFlush,

        /* Awaiting a remote request before sending data */
                Output_RequestWait,

        /* Sending data to the remote end */
                Output_Sending,

        /* Sending data from old checkpoints to the remote end */
                Output_Resuming,

        /* Data send complete, but still acknowledging checkpoints */
                Output_Acknowledging,

        /* Local upload complete */
                Output_LocalSent,

        /* Peer has finished all processing, output complete */
                Output_Completed,
    } outputState;
    QByteArray outputBuffer;

    enum {
        /* Awaiting version number */
                Input_Version,

        /* Awaiting archive read request */
                Input_Request,

        /* Awaiting a response from the remote */
                Input_Processing,

        /* Awaiting a response from the remote, but discarding everything
         * until we see a checkpoint equal to the current one */
                Input_Resuming,

        /* Remote sync completed, no more data accepted */
                Input_Completed,
    } inputState;
    QByteArray inputBuffer;

    ErrorState inError;

    struct PeerCheckpoint {
        quint64 id;

        double latestTime;

        quint16 nextUnit;
        std::vector<CPD3::Data::SequenceName> unitLookup;

        std::deque<CPD3::Data::ArchiveValue> values;
        std::deque<CPD3::Data::ArchiveErasure> erasures;

        inline PeerCheckpoint() : id(0), latestTime(FP::undefined()), nextUnit(0)
        { }
    };

    PeerCheckpoint incomingPriorCheckpoint;
    PeerCheckpoint incomingActiveCheckpoint;

    std::deque<CPD3::Data::ArchiveValue> outgoingPendingValues;
    std::deque<CPD3::Data::ArchiveErasure> outgoingPendingErasures;
    CPD3::Data::SequenceName::Map<quint16> outgoingUnitLookup;
    std::deque<PeerCheckpoint> unacknowledgedCheckpoints;
    PeerCheckpoint outgoingBeginCheckpoint;
    PeerCheckpoint outgoingCheckpoint;
    quint64 outgoingCheckpointSize;
    std::size_t outgoingResumeCheckpointIndex;

    bool singleEnded;

    QTimer heartbeatTimer;

    QTimer byteTrackingTimer;
    double byteTrackingTime;
    quint64 bytesSinceLastTime;

    void inputFault();

    void inputCompleted();

    void beginLocalRead(const CPD3::Data::Archive::Selection::List &selections);

    void outputCompleted();

    bool processIncoming(CPD3::Data::ArchiveValue &&value);

    bool processIncoming(CPD3::Data::ArchiveErasure &&value);

    bool processRead(int &offset);

    void emptyRead();

    void writeCheckpoint(const PeerCheckpoint &cp);

    void addUnitDefine(QByteArray &buffer, const CPD3::Data::SequenceName &unit);

    void requestRemoteData();

    void readAllLocalData();

    void fillWriteBuffer();

    void rebuildSendLookup(const PeerCheckpoint &cp);

    void reapWriter();

    void reapReader();

    static QString formatRate(double rate, const QString &units);

public:
    /**
     * The possible modes the peer can operate in. 
     */
    enum PeerMode {
        /** Bidirectional communications with another Peer instance. */
                Mode_Bidirectional,
        /** Writing data to a static output for later usage by another Peer. */
                Mode_Write,
        /** Reading data written out beforehand by another Peer. */
                Mode_Read,
    };

    /**
     * Create a peer connection.  This takes ownership of all parameters.
     *
     * @param local         the local read access allowed to the remote peer
     * @param remote        the data to request from the remote peer
     * @param modifiedTime  the modification time to request data after
     * @param readA    the archive to read from
     * @param writeA   the archive to write to
     * @param parent        the parent object
     */
    Peer(Filter *local,
         Filter *remote,
         PeerMode mode = Mode_Bidirectional,
         double modifiedTime = FP::undefined(),
         std::unique_ptr<CPD3::Data::Archive::Access> &&readA = std::unique_ptr<CPD3::Data::Archive::Access>(),
         std::unique_ptr<CPD3::Data::Archive::Access> &&writeA = std::unique_ptr<CPD3::Data::Archive::Access>(),
         QObject *parent = 0);

    virtual ~Peer();

    /**
     * Add incoming data from the remote end of the peer.
     * 
     * @param in    the incoming data
     */
    void incomingData(const QByteArray &in);

    /**
     * Test if the hosting end should stall reading if possible.  This can
     * be used with the incomingStallCleared() signal to limit the amount
     * of buffering being done.
     * 
     * @return true if the incoming data buffer is large enough to stall data
     */
    bool shouldStallIncoming();

    /**
     * The possible results of writing data.
     */
    enum WriteResult {
        /** Wait for another request signal before continuing. */
                WriteResult_Wait,
        /** Continue writing if possible (i.e. there is more data pending). */
                WriteResult_Continue,
        /** Writing completed, no more writes required. */
                WriteResult_Completed,
        /** An error occurred during writing. */
                WriteResult_Error,
    };

    /**
     * Attempt to all available data to the given target device.  This can
     * safely be called even if there is no pending data.
     * 
     * @param target    the device to write to
     * @return          the write status
     */
    WriteResult writePending(QIODevice *target);

    /**
     * Test if the peer has completed synchronization.
     * <br>
     * Note that the connection being in an error states counts as completed.
     * 
     * @return true if both ends have completed synchronization
     */
    bool isFinished() const;

    /**
     * Test if the peer has finished transferring data with the remote end.
     * This is distinct from being finished in that the archive write
     * may still be in progress
     * <br>
     * Note that the connection being in an error states counts as completed.
     * 
     * @return true if both ends have transferred all data they need to
     */
    bool transferCompleted() const;

    /**
     * Get the error state of the peer.
     * 
     * @return the current error state
     */
    ErrorState errorState() const;

    /**
     * Get the state for the remote filter.
     *
     * @return  the remote filter state
     */
    QByteArray getRemoteFilterState() const;

    ActionFeedback::Source feedback;

public slots:

    /**
     * Start negotiation with the remote peer.
     */
    void start();

    /**
     * Roll the connection back to the last checkpoint.  This is used
     * when re-establishing connections after an interruption.
     * 
     * @return true if the rollback succeeded
     */
    bool rollback();

    /**
     * Request termination of the synchronization.
     */
    void signalTerminate();

private slots:

    void sendHeartbeat();

    void updateByteRate();

    void checkFinished();

signals:

    /**
     * Emitted when both peers have finished synchronization.
     */
    void finished();

    /**
     * Emitted when an error is detected, usually this results in
     * connection termination.
     */
    void errorDetected();

    /**
     * Emitted when a read stall may have been cleared.
     */
    void incomingStallCleared();

    /**
     * Emitted to request a flush of data to the remote end.
     */
    void flushRequested();

    /**
     * Emitted whenever data to write is available.  That is, this is emitted
     * to request a call to writePending(QIODevice *).
     */
    void writeAvailable();
};

}
}

#endif
