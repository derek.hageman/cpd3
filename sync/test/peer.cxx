/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>
#include <QSignalSpy>
#include <QTemporaryFile>
#include <QBuffer>

#include "core/qtcompat.hxx"
#include "core/waitutils.hxx"
#include "sync/peer.hxx"

using namespace CPD3;
using namespace CPD3::Sync;
using namespace CPD3::Data;

class TestFilter : public Filter {
    QList<SequenceName> ac;
public:
    TestFilter(const QList<SequenceName> &a) : ac(a)
    { }

    virtual ~TestFilter()
    { }

    virtual bool accept(const ArchiveValue &value)
    {
        return ac.contains(value.getUnit());
    }

    virtual bool accept(const ArchiveErasure &value)
    {
        return ac.contains(value.getUnit());
    }

    virtual void advance(double)
    { }

    virtual Archive::Selection::List toArchiveSelection(double modified) const
    {
        Archive::Selection sel(FP::undefined(), FP::undefined());
        sel.modifiedAfter = modified;
        return Archive::Selection::List{sel};
    }
};

namespace CPD3 {
namespace Data {
bool operator==(const ArchiveValue &a, const ArchiveValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestPeer : public QObject {
Q_OBJECT

    static bool exchange(Peer &from, Peer &to, QEventLoop &el)
    {
        QByteArray data;
        {
            QBuffer buffer(&data);
            if (!buffer.open(QIODevice::WriteOnly))
                return false;
            switch (from.writePending(&buffer)) {
            case Peer::WriteResult_Wait:
                break;
            case Peer::WriteResult_Continue:
                QTimer::singleShot(0, &el, SLOT(quit()));
                break;
            case Peer::WriteResult_Completed:
                break;
            case Peer::WriteResult_Error:
                return false;
            }
        }
        if (data.isEmpty())
            return true;
        to.incomingData(data);
        return true;
    }

    static void connectEventLoop(QEventLoop &el, Peer &p)
    {
        connect(&p, SIGNAL(finished()), &el, SLOT(quit()));
        connect(&p, SIGNAL(errorDetected()), &el, SLOT(quit()));
        connect(&p, SIGNAL(writeAvailable()), &el, SLOT(quit()));
    }

    static bool archiveContains(const QFile &db, const ArchiveValue &lookingFor)
    {
        Archive::Access access(db);
        ArchiveSink::Iterator data;
        access.readArchive(Archive::Selection(lookingFor.getUnit(), lookingFor.getStart(),
                                              lookingFor.getEnd()), &data)->detach();

        ArchiveValue check;
        while ((check = data.next()).isValid()) {
            if (!ArchiveValue::ExactlyEqual()(check, lookingFor))
                continue;
            data.abort();
            data.wait();
            return true;
        }
        return false;
    }

    static bool archiveContains(const QFile &db, const ArchiveErasure &lookingFor)
    {
        Archive::Access access(db);
        ErasureSink::Iterator data;
        access.readErasure(Archive::Selection(lookingFor.getUnit(), lookingFor.getStart(),
                                              lookingFor.getEnd()), &data)->detach();

        ArchiveErasure check;
        while ((check = data.erasureNext()).isValid()) {
            if (!ArchiveErasure::ExactlyEqual()(check, lookingFor))
                continue;
            data.abort();
            data.wait();
            return true;
        }
        return false;
    }

    static ValueSegment::Transfer cfg(const Variant::Read &value)
    {
        return ValueSegment::Transfer{
                ValueSegment(FP::undefined(), FP::undefined(), Variant::Root(value))};
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void basic()
    {
        QTemporaryFile db1;
        QVERIFY(db1.open());

        QTemporaryFile db2;
        QVERIFY(db2.open());

        SequenceName u1("s1", "a1", "u1");
        SequenceName u2("s1", "a1", "u2");
        SequenceName u3("s1", "a1", "u3");
        SequenceName u4("s1", "a1", "u4");

        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u3, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(0.1), 40.0, 50.0, 1, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0),});

        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u4, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, false),
                                                      ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u1, 50.0, 60.0, 0, 1000.0),});


        TestFilter *fl1 = new TestFilter(QList<SequenceName>() << u1 << u2);
        TestFilter *fr1 = new TestFilter(QList<SequenceName>() << u3 << u4);

        Peer p1(fl1, fr1, Peer::Mode_Bidirectional, 1001.0,
                std::unique_ptr<Archive::Access>(new Archive::Access(db1)),
                std::unique_ptr<Archive::Access>(new Archive::Access(db1)));


        TestFilter *fl2 = new TestFilter(QList<SequenceName>() << u3 << u4);
        TestFilter *fr2 = new TestFilter(QList<SequenceName>() << u1 << u2);

        Peer p2(fl2, fr2, Peer::Mode_Bidirectional, 1002.0,
                std::unique_ptr<Archive::Access>(new Archive::Access(db2)),
                std::unique_ptr<Archive::Access>(new Archive::Access(db2)));


        QSignalSpy errors1(&p1, SIGNAL(errorDetected()));
        QSignalSpy errors2(&p2, SIGNAL(errorDetected()));
        QSignalSpy finished1(&p1, SIGNAL(finished()));
        QSignalSpy finished2(&p2, SIGNAL(finished()));

        p1.start();
        p2.start();

        ElapsedTimer to;
        to.start();
        while (to.elapsed() < 30000 && (!p1.isFinished() || !p2.isFinished())) {
            QVERIFY(errors1.isEmpty());
            QVERIFY(errors2.isEmpty());

            QEventLoop el;

            QTimer::singleShot(1000, &el, SLOT(quit()));

            connectEventLoop(el, p1);
            connectEventLoop(el, p2);

            QVERIFY(exchange(p1, p2, el));
            QVERIFY(exchange(p2, p1, el));

            el.exec();
        }
        QVERIFY(p1.isFinished());
        QVERIFY(p2.isFinished());

        while (to.elapsed() < 30000) {
            QEventLoop el;
            connect(&p1, SIGNAL(finished()), &el, SLOT(quit()));
            connect(&p2, SIGNAL(finished()), &el, SLOT(quit()));
            QTimer::singleShot(1000, &el, SLOT(quit()));
            if (finished1.count() > 0 && finished2.count() > 0)
                break;
            el.exec();
        }

        QVERIFY(finished1.count() > 0);
        QVERIFY(finished2.count() > 0);
        QVERIFY(errors1.isEmpty());
        QVERIFY(errors2.isEmpty());

        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));

        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
    }

    void checkpointFlush()
    {
        QTemporaryFile db1;
        QVERIFY(db1.open());

        QTemporaryFile db2;
        QVERIFY(db2.open());

        SequenceName u1("s1", "a1", "u1");
        SequenceName u2("s1", "a1", "u2");

        Variant::Root bigValue(Variant::Bytes(262144, 'A'));
        ArchiveValue::Transfer values1;
        ArchiveValue::Transfer values2;
        for (int i = 0; i < 256; i++) {
            values1.push_back(ArchiveValue(u1, bigValue, 10.0, 20.0, i, 1000.0, false));
            values2.push_back(ArchiveValue(u2, bigValue, 10.0, 20.0, i, 1000.0, false));
        }
        Archive::Access(db1).writeSynchronous(values1);
        Archive::Access(db2).writeSynchronous(values2);

        TestFilter *fl1 = new TestFilter(QList<SequenceName>() << u1);
        TestFilter *fr1 = new TestFilter(QList<SequenceName>() << u2);

        Peer p1(fl1, fr1, Peer::Mode_Bidirectional, FP::undefined(),
                std::unique_ptr<Archive::Access>(new Archive::Access(db1)),
                std::unique_ptr<Archive::Access>(new Archive::Access(db1)));


        TestFilter *fl2 = new TestFilter(QList<SequenceName>() << u2);
        TestFilter *fr2 = new TestFilter(QList<SequenceName>() << u1);

        Peer p2(fl2, fr2, Peer::Mode_Bidirectional, FP::undefined(),
                std::unique_ptr<Archive::Access>(new Archive::Access(db2)),
                std::unique_ptr<Archive::Access>(new Archive::Access(db2)));


        QSignalSpy errors1(&p1, SIGNAL(errorDetected()));
        QSignalSpy errors2(&p2, SIGNAL(errorDetected()));
        QSignalSpy finished1(&p1, SIGNAL(finished()));
        QSignalSpy finished2(&p2, SIGNAL(finished()));

        p1.start();
        p2.start();

        ElapsedTimer to;
        to.start();
        while (to.elapsed() < 30000 && (!p1.isFinished() || !p2.isFinished())) {
            QVERIFY(errors1.isEmpty());
            QVERIFY(errors2.isEmpty());

            QEventLoop el;

            QTimer::singleShot(1000, &el, SLOT(quit()));

            connectEventLoop(el, p1);
            connectEventLoop(el, p2);

            QVERIFY(exchange(p1, p2, el));
            QVERIFY(exchange(p2, p1, el));

            el.exec();
        }
        QVERIFY(p1.isFinished());
        QVERIFY(p2.isFinished());

        while (to.elapsed() < 30000) {
            QEventLoop el;
            connect(&p1, SIGNAL(finished()), &el, SLOT(quit()));
            connect(&p2, SIGNAL(finished()), &el, SLOT(quit()));
            QTimer::singleShot(1000, &el, SLOT(quit()));
            if (finished1.count() > 0 && finished2.count() > 0)
                break;
            el.exec();
        }

        QVERIFY(finished1.count() > 0);
        QVERIFY(finished2.count() > 0);
        QVERIFY(errors1.isEmpty());
        QVERIFY(errors2.isEmpty());

        {
            Archive::Access access(db1);
            Archive::Access::ReadLock lock(access, true);
            ArchiveSink::Iterator data;
            access.readArchive(Archive::Selection(), &data)->detach();
            while (data.hasNext()) {
                auto rem = std::find(values2.begin(), values2.end(), data.next());
                if (rem != values2.end())
                    values2.erase(rem);
            }
        }
        {
            Archive::Access access(db2);
            Archive::Access::ReadLock lock(access, true);
            ArchiveSink::Iterator data;
            access.readArchive(Archive::Selection(), &data)->detach();
            while (data.hasNext()) {
                auto rem = std::find(values1.begin(), values1.end(), data.next());
                if (rem != values1.end())
                    values1.erase(rem);
            }
        }
        QVERIFY(values1.empty());
        QVERIFY(values2.empty());
    }

    void singleEnded()
    {
        QTemporaryFile db1;
        QVERIFY(db1.open());

        QTemporaryFile db2;
        QVERIFY(db2.open());

        SequenceName u1("s1", "a1", "u1");
        SequenceName u2("s1", "a1", "u2");
        SequenceName u3("s1", "a1", "u3");
        SequenceName u4("s1", "a1", "u4");

        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u3, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(0.1), 40.0, 50.0, 1, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0),});

        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u4, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, false),
                                                      ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u1, 50.0, 60.0, 0, 1000.0),});


        QByteArray data;

        {
            Peer p1(new TestFilter(QList<SequenceName>() << u1 << u2),
                    new TestFilter(QList<SequenceName>() << u3 << u4), Peer::Mode_Write, 1001.0,
                    std::unique_ptr<Archive::Access>(new Archive::Access(db1)),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db1)));

            QBuffer buffer(&data);
            QVERIFY(buffer.open(QIODevice::WriteOnly));

            QSignalSpy errors(&p1, SIGNAL(errorDetected()));
            QSignalSpy finished(&p1, SIGNAL(finished()));

            p1.start();

            ElapsedTimer to;
            to.start();
            while (to.elapsed() < 30000 && !p1.isFinished()) {
                QVERIFY(errors.isEmpty());

                QEventLoop el;

                QTimer::singleShot(1000, &el, SLOT(quit()));

                connectEventLoop(el, p1);

                switch (p1.writePending(&buffer)) {
                case Peer::WriteResult_Wait:
                    break;
                case Peer::WriteResult_Continue:
                    QTimer::singleShot(0, &el, SLOT(quit()));
                    break;
                case Peer::WriteResult_Completed:
                    break;
                case Peer::WriteResult_Error:
                    QFAIL("Write error detected");
                    break;
                }

                el.exec();
            }
            QVERIFY(p1.isFinished());

            while (to.elapsed() < 30000) {
                QEventLoop el;
                connect(&p1, SIGNAL(finished()), &el, SLOT(quit()));
                QTimer::singleShot(1000, &el, SLOT(quit()));
                if (finished.count())
                    break;
                el.exec();
            }

            QVERIFY(finished.count() > 0);
            QVERIFY(errors.isEmpty());
        }
        QVERIFY(data.size() > 0);

        {
            Peer p2(new TestFilter(QList<SequenceName>() << u3 << u4),
                    new TestFilter(QList<SequenceName>() << u1 << u2), Peer::Mode_Read, 1002.0,
                    std::unique_ptr<Archive::Access>(new Archive::Access(db2)),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db2)));

            QSignalSpy errors(&p2, SIGNAL(errorDetected()));
            QSignalSpy finished(&p2, SIGNAL(finished()));

            p2.start();
            p2.incomingData(data);

            ElapsedTimer to;
            to.start();
            while (to.elapsed() < 30000 && !p2.isFinished()) {
                QVERIFY(errors.isEmpty());

                QEventLoop el;

                QTimer::singleShot(1000, &el, SLOT(quit()));

                connectEventLoop(el, p2);

                el.exec();
            }
            QVERIFY(p2.isFinished());

            while (to.elapsed() < 30000) {
                QEventLoop el;
                connect(&p2, SIGNAL(finished()), &el, SLOT(quit()));
                QTimer::singleShot(1000, &el, SLOT(quit()));
                if (finished.count())
                    break;
                el.exec();
            }

            QVERIFY(finished.count() > 0);
            QVERIFY(errors.isEmpty());
        }
        data.clear();

        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2, ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0,
                                                  false)));
        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0,
                                                  false)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));

        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1, ArchiveValue(u3, Variant::Root(0.1), 40.0, 50.0, 1, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, true)));

        {
            Peer p2(new TestFilter(QList<SequenceName>() << u3 << u4),
                    new TestFilter(QList<SequenceName>() << u1 << u2), Peer::Mode_Write, 1002.0,
                    std::unique_ptr<Archive::Access>(new Archive::Access(db2)),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db2)));

            QBuffer buffer(&data);
            QVERIFY(buffer.open(QIODevice::WriteOnly));

            QSignalSpy errors(&p2, SIGNAL(errorDetected()));
            QSignalSpy finished(&p2, SIGNAL(finished()));

            p2.start();

            ElapsedTimer to;
            to.start();
            while (to.elapsed() < 30000 && !p2.isFinished()) {
                QVERIFY(errors.isEmpty());

                QEventLoop el;

                QTimer::singleShot(1000, &el, SLOT(quit()));

                connectEventLoop(el, p2);

                switch (p2.writePending(&buffer)) {
                case Peer::WriteResult_Wait:
                    break;
                case Peer::WriteResult_Continue:
                    QTimer::singleShot(0, &el, SLOT(quit()));
                    break;
                case Peer::WriteResult_Completed:
                    break;
                case Peer::WriteResult_Error:
                    QFAIL("Write error detected");
                    break;
                }

                el.exec();
            }
            QVERIFY(p2.isFinished());

            while (to.elapsed() < 30000) {
                QEventLoop el;
                connect(&p2, SIGNAL(finished()), &el, SLOT(quit()));
                QTimer::singleShot(1000, &el, SLOT(quit()));
                if (finished.count())
                    break;
                el.exec();
            }

            QVERIFY(finished.count() > 0);
            QVERIFY(errors.isEmpty());
        }
        QVERIFY(data.size() > 0);

        {
            Peer p1(new TestFilter(QList<SequenceName>() << u1 << u2),
                    new TestFilter(QList<SequenceName>() << u3 << u4), Peer::Mode_Read, 1001.0,
                    std::unique_ptr<Archive::Access>(new Archive::Access(db1)),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db1)));

            QSignalSpy errors(&p1, SIGNAL(errorDetected()));
            QSignalSpy finished(&p1, SIGNAL(finished()));

            p1.start();
            p1.incomingData(data);

            ElapsedTimer to;
            to.start();
            while (to.elapsed() < 30000 && !p1.isFinished()) {
                QVERIFY(errors.isEmpty());

                QEventLoop el;

                QTimer::singleShot(1000, &el, SLOT(quit()));

                connectEventLoop(el, p1);

                el.exec();
            }
            QVERIFY(p1.isFinished());

            while (to.elapsed() < 30000) {
                QEventLoop el;
                connect(&p1, SIGNAL(finished()), &el, SLOT(quit()));
                QTimer::singleShot(1000, &el, SLOT(quit()));
                if (finished.count())
                    break;
                el.exec();
            }

            QVERIFY(finished.count() > 0);
            QVERIFY(errors.isEmpty());
        }
        data.clear();

        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));

        QVERIFY(archiveContains(db2,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));
    }

    void threeWayBidirectional()
    {
        QTemporaryFile db1;
        QVERIFY(db1.open());

        QTemporaryFile db2;
        QVERIFY(db2.open());

        QTemporaryFile db3;
        QVERIFY(db3.open());

        SequenceName u1("s1", "a1", "u1");
        SequenceName u2("s1", "a1", "u2");

        Variant::Write both = Variant::Write::empty();
        both["#0/Variable"] = "u1";
        both["#0/Archive"] = "a1";
        both["#0/Accept"] = true;
        both["#0/Bidirectional"] = true;
        both["#1/Variable"] = "u2";
        both["#1/Archive"] = "a1";
        both["#1/Accept"] = true;

        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u1, Variant::Root(1.0), 50.0, 60.0, 0, 1000.0, true),
                ArchiveValue(u2, Variant::Root(2.0), 50.0, 60.0, 0, 2000.0, true),});

        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u1, Variant::Root(3.0), 50.0, 60.0, 0, 1100.0, true),
                ArchiveValue(u2, Variant::Root(4.0), 50.0, 60.0, 0, 1900.0, true),});

        QByteArray state1_3;
        QByteArray state1_2;
        QByteArray state2;
        QByteArray state3;

        {
            Peer p1(new BasicFilter(cfg(both), "s1"), new TrackingFilter(cfg(both), "s1"),
                    Peer::Mode_Bidirectional, FP::undefined(),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db1)),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db1)));

            Peer p2(new BasicFilter(cfg(both), "s1"), new TrackingFilter(cfg(both), "s1"),
                    Peer::Mode_Bidirectional, FP::undefined(),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db3)),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db3)));

            p1.start();
            p2.start();

            ElapsedTimer to;
            to.start();
            while (to.elapsed() < 30000 && (!p1.isFinished() || !p2.isFinished())) {
                QEventLoop el;

                QTimer::singleShot(1000, &el, SLOT(quit()));

                connectEventLoop(el, p1);
                connectEventLoop(el, p2);

                QVERIFY(exchange(p1, p2, el));
                QVERIFY(exchange(p2, p1, el));

                el.exec();
            }
            QVERIFY(p1.isFinished());
            QVERIFY(p2.isFinished());

            state1_3 = p1.getRemoteFilterState();
            state3 = p2.getRemoteFilterState();
        }

        QVERIFY(archiveContains(db3,
                                ArchiveValue(u1, Variant::Root(1.0), 50.0, 60.0, 0, 1000.0, true)));
        QVERIFY(archiveContains(db3,
                                ArchiveValue(u2, Variant::Root(2.0), 50.0, 60.0, 0, 2000.0, true)));

        {
            Peer p1(new BasicFilter(cfg(both), "s1"), new TrackingFilter(cfg(both), "s1"),
                    Peer::Mode_Bidirectional, FP::undefined(),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db1)),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db1)));

            Peer p2(new BasicFilter(cfg(both), "s1"), new TrackingFilter(cfg(both), "s1"),
                    Peer::Mode_Bidirectional, FP::undefined(),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db2)),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db2)));

            p1.start();
            p2.start();

            ElapsedTimer to;
            to.start();
            while (to.elapsed() < 30000 && (!p1.isFinished() || !p2.isFinished())) {
                QEventLoop el;

                QTimer::singleShot(1000, &el, SLOT(quit()));

                connectEventLoop(el, p1);
                connectEventLoop(el, p2);

                QVERIFY(exchange(p1, p2, el));
                QVERIFY(exchange(p2, p1, el));

                el.exec();
            }
            QVERIFY(p1.isFinished());
            QVERIFY(p2.isFinished());

            state1_2 = p1.getRemoteFilterState();
            state2 = p2.getRemoteFilterState();
        }

        QVERIFY(archiveContains(db1,
                                ArchiveValue(u1, Variant::Root(3.0), 50.0, 60.0, 0, 1100.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u2, Variant::Root(2.0), 50.0, 60.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u1, Variant::Root(3.0), 50.0, 60.0, 0, 1100.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u2, Variant::Root(2.0), 50.0, 60.0, 0, 2000.0, true)));

        {
            Peer p1(new BasicFilter(cfg(both), "s1"), new TrackingFilter(cfg(both), "s1", state1_3),
                    Peer::Mode_Bidirectional, 2000.0,
                    std::unique_ptr<Archive::Access>(new Archive::Access(db1)),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db1)));

            Peer p2(new BasicFilter(cfg(both), "s1"), new TrackingFilter(cfg(both), "s1", state3),
                    Peer::Mode_Bidirectional, 2000.0,
                    std::unique_ptr<Archive::Access>(new Archive::Access(db3)),
                    std::unique_ptr<Archive::Access>(new Archive::Access(db3)));

            p1.start();
            p2.start();

            ElapsedTimer to;
            to.start();
            while (to.elapsed() < 30000 && (!p1.isFinished() || !p2.isFinished())) {
                QEventLoop el;

                QTimer::singleShot(1000, &el, SLOT(quit()));

                connectEventLoop(el, p1);
                connectEventLoop(el, p2);

                QVERIFY(exchange(p1, p2, el));
                QVERIFY(exchange(p2, p1, el));

                el.exec();
            }
            QVERIFY(p1.isFinished());
            QVERIFY(p2.isFinished());

            state1_3 = p1.getRemoteFilterState();
            state3 = p2.getRemoteFilterState();
        }

        QVERIFY(archiveContains(db3,
                                ArchiveValue(u1, Variant::Root(3.0), 50.0, 60.0, 0, 1100.0, true)));
        QVERIFY(archiveContains(db3,
                                ArchiveValue(u2, Variant::Root(2.0), 50.0, 60.0, 0, 2000.0, true)));
    }
};

QTEST_MAIN(TestPeer)

#include "peer.moc"
