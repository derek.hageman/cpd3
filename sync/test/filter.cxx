/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>
#include <QSignalSpy>

#include "sync/filter.hxx"
#include "datacore/segment.hxx"

using namespace CPD3;
using namespace CPD3::Sync;
using namespace CPD3::Data;

class TestFilter : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        Variant::Root config;
        config["#0/Variable"] = "T_S11";
        config["#0/Archive"] = "raw";
        config["#0/Accept"] = true;
        BasicFilter f(ValueSegment::Transfer
        { ValueSegment(FP::undefined(), FP::undefined(), std::move(config)) },
                      "bnd");

        QVERIFY(f.accept(ArchiveValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.0),
                                      FP::undefined(),
                                      FP::undefined(), 0, FP::undefined(), false)));
        QVERIFY(!f.accept(ArchiveValue(SequenceName("thd", "raw", "T_S11"), Variant::Root(1.0),
                                       FP::undefined(),
                                       FP::undefined(), 0, FP::undefined(), false)));

        f.advance(500.0);

        Archive::Selection::List selections = f.toArchiveSelection(500.0);
        QVERIFY(!selections.empty());
        for (const auto &sel : selections) {
            QCOMPARE(sel.stations, (Archive::Selection::Match{"bnd"}));
        }
    }

    void tracking()
    {
        Variant::Root config;
        config["#0/Variable"] = "T_S11";
        config["#0/Archive"] = "raw";
        config["#0/Accept"] = true;
        config["#0/Bidirectional"] = true;
        TrackingFilter f(ValueSegment::Transfer
        { ValueSegment(FP::undefined(), FP::undefined(), config) },
                 "bnd");

        QVERIFY(f.accept(ArchiveValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.0),
                                      FP::undefined(),
                                      FP::undefined(), 0, 1000.0, false)));

        Archive::Selection::List selections = f.toArchiveSelection(1500.0);
        QVERIFY(!selections.empty());
        for (const auto &sel : selections) {
            QCOMPARE(sel.stations, (Archive::Selection::Match{"bnd"}));
            QCOMPARE(sel.modifiedAfter, 1000.0);
        }

        QByteArray state = f.state();
        TrackingFilter f2(ValueSegment::Transfer
        { ValueSegment(FP::undefined(), FP::undefined(), config) },
                 "bnd", state);
        selections = f.toArchiveSelection(1500.0);
        QVERIFY(!selections.empty());
        for (const auto &sel : selections) {
            QCOMPARE(sel.stations, (Archive::Selection::Match{"bnd"}));
            QCOMPARE(sel.modifiedAfter, 1000.0);
        }
    }
};

QTEST_MAIN(TestFilter)

#include "filter.moc"
