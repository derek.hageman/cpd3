/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>
#include <QSignalSpy>
#include <QTcpServer>
#include <QSslCertificate>
#include <QTcpSocket>
#include <QTemporaryFile>
#include <QBuffer>

#include "sync/files.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/stream.hxx"
#include "core/qtcompat.hxx"
#include "core/waitutils.hxx"
#include "transfer/package.hxx"
#include "transfer/datafile.hxx"
#include "algorithms/cryptography.hxx"

using namespace CPD3;
using namespace CPD3::Sync;
using namespace CPD3::Data;
using namespace CPD3::Algorithms;
using namespace CPD3::Transfer;

static const char *cert1Data = "-----BEGIN CERTIFICATE-----\n"
        "MIICbTCCAdYCCQDZ6mj3BI+kCTANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
        "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
        "DU5PQUEvR01EL0VTUkwxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
        "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1MzkwNloXDTM5MTIxMjE1MzkwNlow\n"
        "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
        "ZGVyMRYwFAYDVQQKDA1OT0FBL0dNRC9FU1JMMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
        "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
        "jQAwgYkCgYEAn3nIKzjLLEY8KG6+hUvrkjDkl9VU8HFKnp8wA2RsMta/UvKG4ZIY\n"
        "bNVsKSiAGm1G0i6Mtftw8quyihoxuy/hElH/XJChU5lersdrhalocHXoipNx8BeW\n"
        "T8sXuJOrh+IoDKqVI4I6Sz42qAlqBU7bNm40TE8sBPAOefyTGavC3e0CAwEAATAN\n"
        "BgkqhkiG9w0BAQUFAAOBgQBU2hljZZpJJ+zK6eePUpdELQGPGHJEbzfReeSBhRau\n"
        "iDj4jEa7IE3yrMv8NqC3Q+6lEVuJg0NInvDYsNYLPjMSPS+jKldrmrJvg+TFrp1w\n"
        "Qr3JoznNxwcwuDyrKjOVux6jo2aype3Zayo5VUizJX9Zo+xLe2MBVR1D0MXmoq8L\n"
        "zA==\n"
        "-----END CERTIFICATE-----";
static const char *key1Data = "-----BEGIN RSA PRIVATE KEY-----\n"
        "MIICXAIBAAKBgQCfecgrOMssRjwobr6FS+uSMOSX1VTwcUqenzADZGwy1r9S8obh\n"
        "khhs1WwpKIAabUbSLoy1+3Dyq7KKGjG7L+ESUf9ckKFTmV6ux2uFqWhwdeiKk3Hw\n"
        "F5ZPyxe4k6uH4igMqpUjgjpLPjaoCWoFTts2bjRMTywE8A55/JMZq8Ld7QIDAQAB\n"
        "AoGBAIQOd0f7PpsKCfS9R7zfklG7dP+Z4z07wzu4vCyC8uniVAoe1LxjmyA8VtV6\n"
        "OSIpDTUs4M4tSWlZ7n1XlYjY6/lNt3xsy5BBQMlWVI8BV/yXnWRCxkQGBvXYTUKe\n"
        "7LRsUcqyGpAPeuStO/V8GEM5H272yv5S4413D09C9cAhROMtAkEA07j2lnnSl/WR\n"
        "0NFEOT9+nbh6Z+yRTzybC86TBhLCN3bY4VvqSWi1PdcU3oiz81dcaAPTksdx18/Y\n"
        "cSfz295ewwJBAMDTq22Dt2HMPQymxNUn9/IdIPU34/fEqSHS3e9hAgrp2gDVeNHR\n"
        "ZEzMkW00rLgdukkW51PV9hVIhYXdxYOjZY8CQFsf/cnwLurGf/b/SrzVDjr1/oEi\n"
        "Obx/2j+vrmnrwvm6Rkhglir4TSGLo+jPr5vpmtUN6I8BFoeLZp31UyjrwZ8CQC3W\n"
        "Y2ruI7qozV5jimjNToCMchg4yAVPB5GVydIsskqb2onWNRlTeE9VVcCrA9/kmTLk\n"
        "serY8t2OVsdCt8AaKHsCQFMEhJCpXaeLnrzToSv0uqRYjcT+r3ZjSghhSepN0ZA+\n"
        "PPj1CMmq9JrQaQAv3rT3hCbDF38TPhKX64OYzbuvdxA=\n"
        "-----END RSA PRIVATE KEY-----\n";
static const char *cert2Data = "-----BEGIN CERTIFICATE-----\n"
        "MIICbTCCAdYCCQCr9EVbER/M4TANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
        "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
        "DU5PQUEvRVNSTC9HTUQxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
        "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1NDIxNloXDTM5MTIxMjE1NDIxNlow\n"
        "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
        "ZGVyMRYwFAYDVQQKDA1OT0FBL0VTUkwvR01EMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
        "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
        "jQAwgYkCgYEAqa4sVcN1M4mWVqTqZuG2VikkOTpYrHYgnD4jFKDfi5FTFE9OQ5oW\n"
        "xhrBdoEtiWLEPmPSQCtJWCbD0d/YQ1ITxOF0OSaAa4c6PkdeiDmfull8EGmuaTFo\n"
        "ikcFS0r2SVtvIy3eFEAg9GBF4iCMp2AvUo2eqNCLt4CdRi6LQs2xkmUCAwEAATAN\n"
        "BgkqhkiG9w0BAQUFAAOBgQCEvlrqC1zChgkFMq8Oi7YRQOdx7ikzDodey/uu0r/W\n"
        "DxCnpA+F2f3okHiR5+LfYqQ5fvCYnmZd7w69tvdpui8swa15aXWaxLVD9/woyM2y\n"
        "Ofce/9pw6UgoYLkh9KsW5+2EP2Sn44lWcU5Jf8U/p8cDdjNIDzDG4vw7UFWiRakm\n"
        "Tw==\n"
        "-----END CERTIFICATE-----\n";
static const char *key2Data = "-----BEGIN RSA PRIVATE KEY-----\n"
        "MIICWwIBAAKBgQCprixVw3UziZZWpOpm4bZWKSQ5OlisdiCcPiMUoN+LkVMUT05D\n"
        "mhbGGsF2gS2JYsQ+Y9JAK0lYJsPR39hDUhPE4XQ5JoBrhzo+R16IOZ+6WXwQaa5p\n"
        "MWiKRwVLSvZJW28jLd4UQCD0YEXiIIynYC9SjZ6o0Iu3gJ1GLotCzbGSZQIDAQAB\n"
        "AoGAUzE2Q4ZlfDNFJo4M7wxTXcMmI3jb6RKxwmkkwgRuFfvWg+quMK7n45FSsUt8\n"
        "jBOErCI8/4E5oKLA97GMUtV3IxAXT/hxQRe6qZS7PEUW4k6Op7evX1hSCfI/IAB/\n"
        "AS6OqUR1U4+AFArhPS3CA+vuY4I01mpWpvI3dfvq29oUW4ECQQDQ4ItViA/CjWiq\n"
        "peW2+cptQ8QqkNGscjsUTZyp3aNtunC1mP/HRKlKLnuhY6G+LQWbzDv7BSJ9UNO4\n"
        "Ro1y90jJAkEAz/Xdjqx/xZntCleuxvMACWdhpMBqfdgs7+maLDXbtc9G43ygbqIz\n"
        "Wj99k9lu0L3iHdRlYicwdfFFBbvd4jsmvQJATOF5J4AvHNLjpXvuc0y5n0IEIA6x\n"
        "viFFcZGnijZUAv1OouivrG6vSOiXBK4hSFhV6iRgJ2KacTmg1ADT627tUQJAK4hl\n"
        "W9OCX8QMGekm/iCqNk284/cfk75oEcTN8ElJ9/Iu/bn9/4rWwyKdUBDpIKtPJT1s\n"
        "B7L6cwYRk9Sy6wPE5QJAFmjRUr3GypvuC4haazaOMyIt/FEbC+kUs9kq6JZsiBuM\n"
        "LbZ02ko9IkhfOiUIewV6+Xb2j+1u/Itc737M6RZ+3Q==\n"
        "-----END RSA PRIVATE KEY-----";

class TestFilter : public Filter {
    QList<SequenceName> ac;
public:
    TestFilter(const QList<SequenceName> &a) : ac(a)
    { }

    virtual ~TestFilter()
    { }

    virtual bool accept(const ArchiveValue &value)
    {
        return ac.contains(value.getUnit());
    }

    virtual bool accept(const ArchiveErasure &value)
    {
        return ac.contains(value.getUnit());
    }

    virtual void advance(double)
    { }

    virtual Archive::Selection::List toArchiveSelection(double modified) const
    {
        Archive::Selection sel(FP::undefined(), FP::undefined());
        sel.modifiedAfter = modified;
        return Archive::Selection::List{sel};
    }
};

class TestFileUnpacker : public TransferUnpack {
public:
    QSet<QByteArray> acceptedSignatures;
    QHash<QByteArray, Variant::Read> certificateLookup;
    QHash<QByteArray, Variant::Read> keyLookup;

    TestFileUnpacker()
    { }

    virtual ~TestFileUnpacker()
    { }

protected:
    virtual Variant::Read getCertificate(const QByteArray &id, bool decryption, int depth)
    {
        Q_UNUSED(depth);
        Q_UNUSED(decryption);
        return certificateLookup.value(id, Variant::Read::empty());
    }

    virtual Variant::Read getKeyForCertificate(const QByteArray &id, int depth)
    {
        Q_UNUSED(depth);
        return keyLookup.value(id, Variant::Read::empty());
    }

    virtual bool signatureAuthorized(const QList<QByteArray> &ids, int depth)
    {
        Q_UNUSED(depth);
        for (QList<QByteArray>::const_iterator check = ids.constBegin(), end = ids.constEnd();
                check != end;
                ++check) {
            if (!acceptedSignatures.contains(*check))
                return false;
        }
        return true;
    }
};

static bool contains(const ArchiveValue::Transfer &values, const ArchiveValue &lookingFor)
{
    for (const auto &check : values) {
        if (!SequenceValue::ValueEqual()(check, lookingFor))
            continue;
        if (!FP::equal(check.getModified(), lookingFor.getModified()))
            continue;
        return true;
    }
    return false;
}

static bool contains(const ArchiveErasure::Transfer &erasures, const ArchiveErasure &lookingFor)
{
    for (const auto &check : erasures) {
        if (!ArchiveErasure::ExactlyEqual()(check, lookingFor))
            continue;
        return true;
    }
    return false;
}

class TestDownloader : public SyncDownload {
public:
    QSet<QString> authorized;
    QHash<QString, Variant::Read> decryption;
    QHash<QString, QList<SequenceName> > filters;
    QByteArray state;

    TestDownloader(const Variant::Read &download, const QFile &file) : SyncDownload(download,
                                                                                    std::make_shared<
                                                                                    Archive::Access>(
                                                                                    file))
    { }

    virtual ~TestDownloader()
    { }

protected:

    virtual FileDownloader *createDownloader(const Variant::Read &configuration)
    {
        FileDownloader *dl = new FileDownloader;
        dl->restoreState(state);
        return dl;
    }

    virtual void completeDownloader(Transfer::FileDownloader *downloader)
    {
        state = downloader->saveState();
    }

    virtual bool signatureAuthorized(const QString &authorization)
    {
        return authorized.contains(authorization);
    }

    virtual Variant::Read decryptionInformation(const QString &authorization,
                                                const QString &encryptingCertificate,
                                                Transfer::DownloadFile *file)
    {
        Q_UNUSED(authorization);
        Q_UNUSED(file);
        return decryption.value(encryptingCertificate, Variant::Read::empty());
    }

    virtual Filter *createFilter(const QString &authorization)
    {
        return new TestFilter(filters.value(authorization));
    }
};

class TestSyncFiles : public QObject {
Q_OBJECT

    static bool archiveContains(const QFile &db, const ArchiveValue &lookingFor)
    {
        Archive::Access access(db);
        ArchiveSink::Iterator data;
        access.readArchive(Archive::Selection(lookingFor.getUnit(), lookingFor.getStart(),
                                              lookingFor.getEnd()), &data)->detach();

        ArchiveValue check;
        while ((check = data.next()).isValid()) {
            if (!ArchiveValue::ExactlyEqual()(check, lookingFor))
                continue;
            data.abort();
            data.wait();
            return true;
        }
        return false;
    }

    static bool archiveContains(const QFile &db, const ArchiveErasure &lookingFor)
    {
        Archive::Access access(db);
        ErasureSink::Iterator data;
        access.readErasure(Archive::Selection(lookingFor.getUnit(), lookingFor.getStart(),
                                              lookingFor.getEnd()), &data)->detach();

        ArchiveErasure check;
        while ((check = data.erasureNext()).isValid()) {
            if (!ArchiveErasure::ExactlyEqual()(check, lookingFor))
                continue;
            data.abort();
            data.wait();
            return true;
        }
        return false;
    }

    static void recursiveRemove(const QDir &base)
    {
        if (base.path().isEmpty() || base == QDir::current())
            return;
        QFileInfoList list(base.entryInfoList(QDir::Files | QDir::NoDotAndDotDot));
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            if (!QFile::remove(rm->absoluteFilePath())) {
                qDebug() << "Failed to remove file " << rm->absoluteFilePath();
                continue;
            }
        }
        list = base.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            QDir subDir(base);
            if (!subDir.cd(rm->fileName())) {
                qDebug() << "Failed to change directory " << rm->absoluteFilePath();
                continue;
            }
            recursiveRemove(subDir);
            base.rmdir(rm->fileName());
        }
    }

    static bool directoryHasContents(const QDir &dir,
                                     const QStringList &contents = QStringList(),
                                     bool retry = true)
    {
        dir.refresh();
        QFileInfoList fi(dir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot));
        if (contents.size() != fi.size()) {
            for (QFileInfoList::const_iterator add = fi.constBegin(), end = fi.constEnd();
                    add != end;
                    ++add) {
                qDebug() << "Unexpected file list:" << add->fileName();
            }
            return false;
        }
        for (QStringList::const_iterator check = contents.constBegin(),
                endCheck = contents.constEnd(); check != endCheck; ++check) {
            bool hit = false;
            for (QFileInfoList::const_iterator info = fi.constBegin(), endInfo = fi.constEnd();
                    info != endInfo;
                    ++info) {
                if (info->fileName() != *check)
                    continue;
                hit = true;
                break;
            }
            if (!hit) {
                qDebug() << "Unmatched file:" << *check;
                return false;
            }
        }
        return true;
    }

    static void readEverything(const QFile &file,
                               ArchiveValue::Transfer &values,
                               ArchiveErasure::Transfer &deleted)
    {
        ErasureSink::Buffer buffer;
        Archive::Access access(file);
        access.readErasure(Archive::Selection(), &buffer)->wait();
        values = buffer.archiveTake();
        deleted = buffer.erasureTake();
    }

    QString tmpPath;
    QDir tmpDir;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        tmpDir = QDir(QDir::tempPath());
        tmpPath = "CPD3SyncFiles-";
        tmpPath.append(Random::string());
        tmpDir.mkdir(tmpPath);
        QVERIFY(tmpDir.cd(tmpPath));
    }

    void cleanupTestCase()
    {
        recursiveRemove(tmpDir);
        tmpDir.cdUp();
        tmpDir.rmdir(tmpPath);
    }

    void init()
    {
        recursiveRemove(tmpDir);
    }

    void cleanup()
    {
        recursiveRemove(tmpDir);
    }

    void uploadFile()
    {
        QTemporaryFile db1;
        QVERIFY(db1.open());

        SequenceName u1("s1", "a1", "u1");
        SequenceName u2("s1", "a1", "u2");
        SequenceName u3("s1", "a1", "u3");

        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u3, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(0.1), 40.0, 50.0, 1, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0),});

        Variant::Write config = Variant::Write::empty();
        config["Sign/Key"].setString(key1Data);
        config["Sign/Certificate"].setString(cert1Data);
        config["Encrypt/Key"].setString(key2Data);
        config["Encrypt/Certificate"].setString(cert2Data);
        config["Transfer/Type"] = "Copy";

        QTemporaryFile tempFile;
        QVERIFY(tempFile.open());
        QString targetName(tempFile.fileName());
        config["Transfer/Local/Target"] = targetName;

        {

            TestFilter *fl = new TestFilter(QList<SequenceName>() << u1 << u2);
            SyncUpload upload(fl, config["Encrypt"], config["Sign"], config["Transfer"], 1001.0,
                              std::unique_ptr<Archive::Access>(new Archive::Access(db1)),
                              std::unique_ptr<Archive::Access>(new Archive::Access(db1)));

            upload.start();
            QVERIFY(upload.wait(30000));
            QVERIFY(upload.succeeded());

            QByteArray cert1Sig(Cryptography::sha512(QSslCertificate(cert1Data)));
            QByteArray cert2Sig(Cryptography::sha512(QSslCertificate(cert2Data)));

            TestFileUnpacker fileUnpack;
            fileUnpack.keyLookup.insert(cert2Sig, Variant::Root(key2Data));
            fileUnpack.acceptedSignatures.insert(cert1Sig);
            fileUnpack.requireChecksum();
            fileUnpack.requireSignature(TransferUnpack::Signature_SingleOnly |
                                                TransferUnpack::Signature_RequireIdentification);
            fileUnpack.requireEncryption();
            fileUnpack.requireCompression();
            fileUnpack.requireEnd();

            auto outputFile = IO::Access::file(targetName, IO::File::Mode::readOnly());
            QVERIFY(outputFile.get() != nullptr);

            auto intermediate = IO::Access::buffer();
            QVERIFY(fileUnpack.exec(outputFile, intermediate));

            DataUnpack dataUnpack;
            ErasureSink::Buffer output;
            QVERIFY(dataUnpack.exec(intermediate, &output));

            auto values = output.archiveTake();
            auto erasures = output.erasureTake();

            QCOMPARE((int) values.size(), 2);
            QCOMPARE((int) erasures.size(), 1);
            QVERIFY(contains(values,
                             ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, false)));
            QVERIFY(contains(values,
                             ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, false)));
            QVERIFY(contains(erasures, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
        }
        QFile::resize(targetName, 0);

        {
            TestFilter *fl = new TestFilter(QList<SequenceName>() << u1);
            SyncUpload upload(fl, config["Encrypt"], config["Sign"], config["Transfer"], 999.0,
                              std::unique_ptr<Archive::Access>(new Archive::Access(db1)),
                              std::unique_ptr<Archive::Access>(new Archive::Access(db1)));

            upload.start();
            QVERIFY(upload.wait(30000));
            QVERIFY(upload.succeeded());

            QByteArray cert1Sig(Cryptography::sha512(QSslCertificate(cert1Data)));
            QByteArray cert2Sig(Cryptography::sha512(QSslCertificate(cert2Data)));

            TestFileUnpacker fileUnpack;
            fileUnpack.keyLookup.insert(cert2Sig, Variant::Root(key2Data));
            fileUnpack.acceptedSignatures.insert(cert1Sig);
            fileUnpack.requireChecksum();
            fileUnpack.requireSignature(TransferUnpack::Signature_SingleOnly |
                                                TransferUnpack::Signature_RequireIdentification);
            fileUnpack.requireEncryption();
            fileUnpack.requireCompression();
            fileUnpack.requireEnd();

            auto outputFile = IO::Access::file(targetName, IO::File::Mode::readOnly());
            QVERIFY(outputFile.get() != nullptr);

            auto intermediate = IO::Access::buffer();
            QVERIFY(fileUnpack.exec(outputFile, intermediate));

            DataUnpack dataUnpack;
            ErasureSink::Buffer output;
            QVERIFY(dataUnpack.exec(intermediate, &output));

            auto values = output.archiveTake();
            auto erasures = output.erasureTake();

            QCOMPARE((int) values.size(), 3);
            QCOMPARE((int) erasures.size(), 1);
            QVERIFY(contains(values,
                             ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0, false)));
            QVERIFY(contains(values,
                             ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0, false)));
            QVERIFY(contains(values,
                             ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, false)));
            QVERIFY(contains(erasures, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
        }
        QFile::resize(targetName, 0);

        {
            TestFilter *fl = new TestFilter(QList<SequenceName>() << u1 << u2 << u3);
            SyncUpload upload(fl, config["Encrypt"], config["Sign"], config["Transfer"], 2001.0,
                              std::unique_ptr<Archive::Access>(new Archive::Access(db1)),
                              std::unique_ptr<Archive::Access>(new Archive::Access(db1)));

            upload.start();
            QVERIFY(upload.wait(30000));
            QVERIFY(upload.succeeded());

            QFile outputFile(targetName);
            QVERIFY(outputFile.open(QIODevice::ReadOnly));
            QCOMPARE(outputFile.size(), Q_INT64_C(0));
        }
    }

    void downloadFile()
    {
        QTemporaryFile db1;
        QVERIFY(db1.open());

        SequenceName u1("s1", "a1", "u1");
        SequenceName u2("s1", "a1", "u2");
        SequenceName u3("s1", "a1", "u3");

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "Local";
        config["Writable"] = true;
        config["Path"] = tmpDir.absolutePath();
        config["Match"] = "input.dat";
        config["MinimumAge"].setInt64(0);

        QString cert1Sig(Cryptography::sha512(QSslCertificate(cert1Data)).toHex().toLower());
        QString cert2Sig(Cryptography::sha512(QSslCertificate(cert2Data)).toHex().toLower());
        ArchiveValue::Transfer values;
        ArchiveErasure::Transfer erasures;

        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u1, Variant::Root(0.0), 10.0, 20.0, 0, 500.0, true),
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u2, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),
                ArchiveValue(u3, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true)});

        {
            auto dataFile = IO::Access::buffer();
            DataPack data(dataFile->stream());
            data.start();
            data.incomingValue(ArchiveValue::Transfer{
                    ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0, true),
                    ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0, true),
                    ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true),
                    ArchiveValue(u3, Variant::Root(0.1), 40.0, 50.0, 1, 1000.0, true)});
            data.incomingErasure(ArchiveErasure::Transfer{ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0),
                                                          ArchiveErasure(u2, 50.0, 60.0, 0, 2000.0),
                                                          ArchiveErasure(u3, 50.0, 60.0, 0,
                                                                         1000.0)});
            data.endData();
            Threading::pollInEventLoop([&] { return !data.isFinished(); });
            data.wait();

            auto outputFile =
                    IO::Access::file(tmpDir.filePath("input.dat"), IO::File::Mode::writeOnly());
            QVERIFY(outputFile.get() != nullptr);

            TransferPack packer;
            packer.compressStage();
            Variant::Write config1 = Variant::Write::empty();
            config1["Certificate"].setString(cert1Data);
            config1["Key"].setString(key1Data);
            packer.encryptStage(config1);
            Variant::Write config2 = Variant::Write::empty();
            config2["Certificate"].setString(cert2Data);
            config2["Key"].setString(key2Data);
            packer.signStage(config2);
            packer.checksumStage();

            QVERIFY(packer.exec(dataFile, outputFile));
        }

        QVERIFY(directoryHasContents(tmpDir, QStringList("input.dat")));

        QByteArray state;
        {
            TestDownloader download(config, db1);

            download.authorized.insert(cert2Sig);
            download.decryption.insert(cert1Sig, Variant::Root(key1Data));
            download.filters.insert(cert2Sig, QList<SequenceName>() << u1 << u2);
            download.state = state;

            download.start();
            QVERIFY(download.wait(30000));
            QVERIFY(download.succeeded());
        }

        QVERIFY(directoryHasContents(tmpDir, QStringList()));


        readEverything(db1, values, erasures);

        QCOMPARE((int) values.size(), 5);
        QVERIFY(contains(values,
                         ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0, true)));
        QVERIFY(contains(values,
                         ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0, true)));
        QVERIFY(contains(values,
                         ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(contains(values,
                         ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true)));
        QVERIFY(contains(values,
                         ArchiveValue(u3, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true)));

        QCOMPARE((int) erasures.size(), 1);
        QVERIFY(contains(erasures, ArchiveErasure(u2, 50.0, 60.0, 0, 2000.0)));

        {
            TestDownloader download(config, db1);

            download.authorized.insert(cert2Sig);
            download.decryption.insert(cert1Sig, Variant::Root(key1Data));
            download.filters.insert(cert2Sig, QList<SequenceName>() << u1 << u2);
            download.state = state;

            download.start();
            QVERIFY(download.wait(30000));
            QVERIFY(download.succeeded());
        }

        QVERIFY(directoryHasContents(tmpDir, QStringList()));
    }

    void exchange()
    {
        QTemporaryFile db1;
        QVERIFY(db1.open());

        QTemporaryFile db2;
        QVERIFY(db2.open());

        SequenceName u1("s1", "a1", "u1");
        SequenceName u2("s1", "a1", "u2");
        SequenceName u3("s1", "a1", "u3");
        SequenceName u4("s1", "a1", "u4");

        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u3, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(0.1), 40.0, 50.0, 1, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0),});

        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u4, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, false),
                                                      ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u1, 50.0, 60.0, 0, 1000.0),});

        Variant::Write config1 = Variant::Write::empty();
        config1["Sign/Key"].setString(key1Data);
        config1["Sign/Certificate"].setString(cert1Data);
        config1["Encrypt/Key"].setString(key2Data);
        config1["Encrypt/Certificate"].setString(cert2Data);

        Variant::Write config2 = Variant::Write::empty();
        config2["Sign/Key"].setString(key2Data);
        config2["Sign/Certificate"].setString(cert2Data);
        config2["Encrypt/Key"].setString(key1Data);
        config2["Encrypt/Certificate"].setString(cert1Data);

        QList<SequenceName> fl1;
        fl1 << u1 << u2;
        QList<SequenceName> fr1;
        fr1 << u3 << u4;

        QList<SequenceName> fl2;
        fl2 << u3 << u4;
        QList<SequenceName> fr2;
        fr2 << u1 << u2;

        QString cert1Sig(Cryptography::sha512(QSslCertificate(cert1Data)).toHex().toLower());
        QString cert2Sig(Cryptography::sha512(QSslCertificate(cert2Data)).toHex().toLower());

        QByteArray st1;
        QByteArray st2;

        tmpDir.mkdir("dir1");
        QDir dir1(tmpDir);
        QVERIFY(dir1.cd("dir1"));
        tmpDir.mkdir("dir2");
        QDir dir2(tmpDir);
        QVERIFY(dir2.cd("dir2"));

        config1["Upload/Type"] = "Copy";
        config1["Upload/Local/Target"] = dir1.absoluteFilePath("file.dat");
        config1["Download/Type"] = "Local";
        config1["Download/Writable"] = true;
        config1["Download/Path"] = dir2.absolutePath();
        config1["Download/Match"] = "file.dat";
        config1["Download/MinimumAge"].setInt64(0);

        config2["Upload/Type"] = "Copy";
        config2["Upload/Local/Target"] = dir2.absoluteFilePath("file.dat");
        config2["Download/Type"] = "Local";
        config2["Download/Writable"] = true;
        config2["Download/Path"] = dir1.absolutePath();
        config2["Download/Match"] = "file.dat";
        config2["Download/MinimumAge"].setInt64(0);

        {
            SyncUpload upload
                    (new TestFilter(fl1), config1["Encrypt"], config1["Sign"], config1["Upload"],
                     1001.0, std::make_shared<Archive::Access>(db1),
                     std::make_shared<Archive::Access>(db1));

            upload.start();
            QVERIFY(upload.wait(30000));
            QVERIFY(upload.succeeded());
        }
        QVERIFY(directoryHasContents(dir1, QStringList("file.dat")));

        {
            SyncUpload upload
                    (new TestFilter(fl2), config2["Encrypt"], config2["Sign"], config2["Upload"],
                     1002.0, std::make_shared<Archive::Access>(db2),
                     std::make_shared<Archive::Access>(db2));

            upload.start();
            QVERIFY(upload.wait(30000));
            QVERIFY(upload.succeeded());
        }
        QVERIFY(directoryHasContents(dir2, QStringList("file.dat")));

        {
            TestDownloader download(config1["Download"], db1);

            download.authorized.insert(cert2Sig);
            download.decryption.insert(cert1Sig, Variant::Root(key1Data));
            download.filters.insert(cert2Sig, fr1);
            download.state = st1;

            download.start();
            QVERIFY(download.wait(30000));
            QVERIFY(download.succeeded());
            st1 = download.state;
        }
        QVERIFY(directoryHasContents(dir2, QStringList()));

        {
            TestDownloader download(config2["Download"], db2);

            download.authorized.insert(cert1Sig);
            download.decryption.insert(cert2Sig, Variant::Root(key2Data));
            download.filters.insert(cert1Sig, fr2);
            download.state = st2;

            download.start();
            QVERIFY(download.wait(30000));
            QVERIFY(download.succeeded());
            st2 = download.state;
        }
        QVERIFY(directoryHasContents(dir1, QStringList()));

        {
            TestDownloader download(config1["Download"], db1);

            download.authorized.insert(cert2Sig);
            download.decryption.insert(cert1Sig, Variant::Root(key1Data));
            download.filters.insert(cert2Sig, fr1);
            download.state = st1;

            download.start();
            QVERIFY(download.wait(30000));
            QVERIFY(download.succeeded());
            st1 = download.state;
        }
        QVERIFY(directoryHasContents(dir2, QStringList()));

        {
            TestDownloader download(config2["Download"], db2);

            download.authorized.insert(cert1Sig);
            download.decryption.insert(cert2Sig, Variant::Root(key2Data));
            download.filters.insert(cert1Sig, fr2);
            download.state = st2;

            download.start();
            QVERIFY(download.wait(30000));
            QVERIFY(download.succeeded());
            st2 = download.state;
        }
        QVERIFY(directoryHasContents(dir1, QStringList()));


        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));

        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
    }
};

QTEST_MAIN(TestSyncFiles)

#include "files.moc"
