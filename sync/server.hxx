/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SYNCSERVER_H
#define CPD3SYNCSERVER_H

#include "core/first.hxx"

#include <memory>
#include <mutex>
#include <QtGlobal>
#include <QTcpServer>
#include <QSslSocket>
#include <QTimer>
#include <QProcess>
#include <QLocalServer>
#include <QLocalSocket>

#include "sync/sync.hxx"
#include "core/compression.hxx"
#include "core/threading.hxx"
#include "datacore/stream.hxx"
#include "database/runlock.hxx"
#include "datacore/segment.hxx"
#include "sync/peer.hxx"


namespace CPD3 {
namespace Sync {

class Server;

namespace Internal {

class SSLServer : public QTcpServer {
Q_OBJECT

    CPD3::Data::Variant::Read configuration;
    QList<QSslSocket *> pending;
public:
    SSLServer(const CPD3::Data::Variant::Read &config, QObject *parent = 0);

    virtual ~SSLServer();

    virtual bool hasPendingConnections() const;

    virtual QTcpSocket *nextPendingConnection();

protected:
    virtual void incomingConnection(qintptr socketDescriptor);

};

struct ServerConnectionID {
    QByteArray digest;
    QByteArray id;
    CPD3::Data::SequenceName::Component station;
};

class ServerConnection : public QObject {
Q_OBJECT

    ServerConnectionID connectionID;
public:
    ServerConnection(const ServerConnectionID &id);

    virtual ~ServerConnection();

    virtual void attach(QSslSocket *socket) = 0;

    virtual void reattach(QSslSocket *socket) = 0;

    virtual bool isCompleted() const = 0;

    virtual void signalTerminate() = 0;

    inline const ServerConnectionID &id() const
    { return connectionID; }

signals:

    void completed();
};

class ServerConnectionInternal : public ServerConnection {
Q_OBJECT

    enum {
        /* Initialization, no device attached yet */
                State_Initialize,

        /* Configuring and locking the connection */
                State_Configuring,

        /* Normal processing, peer handling all data */
                State_Connected,

        /* Connection interrupted, awaiting reattachment */
                State_Resume,

        /* Connection closing, flushing all remaining data */
                State_Closing,

        /* Connection completed */
                State_Completed,
    } state;
    QTimer timeoutTimer;

    QSslSocket *socket;
    CompressedStream *compressor;
    std::unique_ptr<Database::RunLock> lock;
    QString lockKey;
    Peer *peer;

    class ConfigurationInitializer {
    public:
        struct Result {
            CPD3::Data::ValueSegment::Transfer config;
            std::unique_ptr<Database::RunLock> lock;
            QString lockKey;
            double modifiedAfter;

            Result() = default;

            Result(const Result &) = delete;

            Result &operator=(const Result &) = delete;

            Result(Result &&) = default;

            Result &operator=(Result &&) = default;
        };

    private:
        ServerConnectionID id;

        std::mutex mutex;
        Result result;
        enum {
            Active, Terminating, Succeeded, Failed,
        } state;

        std::thread thread;

        bool maybeTerminate();
    public:
        ConfigurationInitializer(const ServerConnectionID &id);

        virtual ~ConfigurationInitializer();

        bool wasSuccessful();

        Result take();

        void signalTerminate();

        void wait();

        bool isFinished();

        Threading::Signal<> complete;

    protected:
        virtual void run();
    };

    std::unique_ptr<ConfigurationInitializer> configurationInitializer;

    void fatalShutdown();

public:
    ServerConnectionInternal(const ServerConnectionID &id);

    virtual ~ServerConnectionInternal();

    virtual void attach(QSslSocket *socket);

    virtual void reattach(QSslSocket *socket);

    virtual bool isCompleted() const;

    virtual void signalTerminate();

private slots:

    void updateSocket();

    void peerWrite();

    void configurationReady();

    void flushCompressor();

    void disconnectResume();

    void connectionTimeout();
};

#ifdef Q_OS_UNIX

class ServerConnectionDetached : public ServerConnection {
Q_OBJECT

    enum {
        /* Initialization, no external process started yet */
                State_Initialize,
        /* Waiting for the external process to be ready */
                State_WaitForStarted,
        /* Sending the initialization information to the external */
                State_SendInitialization,
        /* Waiting for the authorization token from the external */
                State_RecieveExternalAuthorization,
        /* Waiting for a connection from the external */
                State_WaitForConnected,
        /* Sending authorization to the external */
                State_SendExternalAuthorization,
        /* Waiting for the external to provide its authorization */
                State_AuthorizeExternal,

        /* Connected and synchronizing */
                State_Connected,

        /* Connection interrupted, awaiting reattachment */
                State_Resume,
        /* Connection closing, flushing all remaining data */
                State_Closing,
        /* Connection completed */
                State_Completed,
    } state;
    QTimer timeoutTimer;

    QSslSocket *socket;
    QProcess *external;

    QLocalSocket *link;
    QLocalServer *server;

    QByteArray localAuthorization;
    QByteArray remoteAuthorization;

    QByteArray socketWriteBuffer;
    QByteArray externalWriteBuffer;
    QByteArray linkWriteBuffer;
    QByteArray externalReadBuffer;
    QByteArray linkInitialReadBuffer;

    void fatalShutdown(bool kill = true);

    void checkCompleted();

    void disconnectResume();

    void processExternal();

public:
    ServerConnectionDetached(const ServerConnectionID &id);

    virtual ~ServerConnectionDetached();

    virtual void attach(QSslSocket *socket);

    virtual void reattach(QSslSocket *socket);

    virtual bool isCompleted() const;

    virtual void signalTerminate();


    enum ExternalMessage {
        External_Completed = 0, External_ResumableError, External_FatalError,
    };
    enum InternalMessage {
        Internal_Reconnected = 0, Internal_Aborted,
    };

private slots:

    void updateSocket();

    void updateExternal();

    void updateLink();

    void writeToSocket();

    void writeToExternal();

    void writeToLink();

    void incomingConnection();

    void connectionTimeout();
};

#endif


class PreliminaryServerConnection : public QObject {
Q_OBJECT

    QSslSocket *socket;
    Server *server;

    enum {
        /* Waiting for the TLS connection handshake to complete */
                State_Connecting,
        /* Waiting for the client to request a type */
                State_Receive_Type,
        /* Waiting for the client to send the resume ID code */
                State_Receive_ResumeID,
        /* Waiting for all data to be written so the handoff can completed */
                State_Handoff_New,
        /* Waiting for all data to be written so the handoff can completed */
                State_Handoff_Resume,
        /* Ready for handoff to a peer */
                State_Ready_New,
        /* Ready for handoff to resume */
                State_Ready_Resume,
        /* Connection closed */
                State_Closed,
    } state;
    QTimer timeoutTimer;

    QByteArray readBuffer;
    QByteArray writeBuffer;

    ServerConnectionID assembledID;
public:
    PreliminaryServerConnection(QSslSocket *socket, Server *server);

    virtual ~PreliminaryServerConnection();

    void signalTerminate();

    bool isAborted() const;

    bool isReady() const;

    ServerConnectionID id() const;

    ServerConnection *createConnection() const;

    QSslSocket *takeSocket();

signals:

    void ready();

    void aborted();

private slots:

    void updateSocket();

    void connectionTimeout();

    void processWrite();
};

}


/**
 * The main synchronization server implementation.
 */
class CPD3SYNC_EXPORT Server : public QObject {
Q_OBJECT

    enum {
        /* Initialization, socket not yet open */
                State_Initialize,

        /* Normal operation, accepting connections */
                State_Run,

        /* Shutting down, no new connections accepted */
                State_Shutdown,

        /* Finished, all sockets shut down */
                State_Finished,
    } state;

    Internal::SSLServer *server;
    CPD3::Data::Variant::Read configuration;

    QList<Internal::PreliminaryServerConnection *> incomingConnections;
    QHash<QByteArray, Internal::ServerConnection *> connections;
    QList<Internal::ServerConnection *> reapingConnections;

    double possibleDigestValid;
    std::unordered_set<std::string> possibleDigests;

    CPD3::Data::SequenceName::Component loadAuthorized(const QByteArray &digest);

    friend class Internal::PreliminaryServerConnection;

    void checkFinished();

public:
    Server(QObject *parent = 0);

    virtual ~Server();

public slots:

    /**
     * Start execution of the server.
     * 
     * @return true on success
     */
    bool start();

    /**
     * End the server.
     * 
     * @param closeConnections  if set then existing connections are also closed
     */
    void signalTerminate(bool closeConnections = true);

signals:

    /**
     * Emitted when the server has finished operation.
     */
    void finished();

private slots:

    void reapIncoming();

    void reapConnections();

    void promoteIncoming();

    void incomingConnection();
};

}
}

#endif
