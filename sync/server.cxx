/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtEndian>
#include <QTcpServer>
#include <QCoreApplication>
#include <QHostInfo>
#include <QDir>
#include <QLoggingCategory>
#include <QDebugStateSaver>

#include "sync/server.hxx"
#include "algorithms/cryptography.hxx"
#include "core/process.hxx"
#include "core/threading.hxx"
#include "core/memory.hxx"


Q_LOGGING_CATEGORY(log_sync_server, "cpd3.sync.server", QtWarningMsg)

using namespace CPD3::Data;
using namespace CPD3::Algorithms;
using namespace CPD3::Sync::Internal;

namespace CPD3 {
namespace Sync {

/** @file sync/server.hxx
 * Synchronization server implementation.
 */

namespace {
class ServerSSLSocket : public QSslSocket {
public:
    ServerSSLSocket(QTcpServer *parent) : QSslSocket(parent)
    { }

    virtual ~ServerSSLSocket()
    { }

    virtual bool atEnd() const
    {
        QAbstractSocket::SocketState s = state();
        return s == QAbstractSocket::UnconnectedState || s == QAbstractSocket::ClosingState;
    }
};
}

namespace Internal {

static QDebug operator<<(QDebug stream, const ServerConnectionID &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << v.digest.toHex().constData() << "-" << v.id.toHex().constData() << "-"
           << QString::fromStdString(v.station).toUpper().toUtf8().constData();
    return stream;
}

static bool readExactly(QIODevice *device, QByteArray &buffer, qint64 required)
{
    if (required <= 0)
        return false;
    qint64 n = device->bytesAvailable();
    if (n > 0) {
        if (buffer.isEmpty()) {
            buffer = device->read((int) qMin(required, n));
        } else {
            qint64 max = required - buffer.size();
            buffer.append(device->read((int) qMin(max, n)));
        }
        if (buffer.size() >= required)
            return false;
        if (device->bytesAvailable() > 0)
            return true;
    }
    return false;
}

SSLServer::SSLServer(const CPD3::Data::Variant::Read &config, QObject *parent) : QTcpServer(parent),
                                                                                 configuration(
                                                                                         config),
                                                                                 pending()
{ configuration.detachFromRoot(); }

SSLServer::~SSLServer()
{
    qDeleteAll(pending);
}

bool SSLServer::hasPendingConnections() const
{ return !pending.isEmpty(); }

QTcpSocket *SSLServer::nextPendingConnection()
{
    if (pending.isEmpty())
        return 0;
    return pending.takeFirst();
}

void SSLServer::incomingConnection(qintptr socketDescriptor)
{
    ServerSSLSocket *socket = new ServerSSLSocket(this);

    if (!Cryptography::setupSocket(socket, configuration.hash("SSL"))) {
        qCDebug(log_sync_server) << "Failed to setup SSL on socket";
    }

    if (!socket->setSocketDescriptor(socketDescriptor, QAbstractSocket::ConnectedState,
                                     QIODevice::ReadWrite)) {
        socket->close();
        delete socket;
        return;
    }
    socket->setReadBufferSize(8388608);

    socket->startServerEncryption();

    pending.append(socket);
    emit newConnection();
}


static QString findHandlerProgram()
{
    QDir programDir(QCoreApplication::applicationDirPath());
    if (!programDir.exists())
        return {};
    {
        QFileInfo file(programDir.absoluteFilePath("cpd3_sync_serverhandler"));
        if (file.exists())
            return file.absoluteFilePath();
    }
    {
        QFileInfo file(programDir.absoluteFilePath("cpd3_sync_serverhandler.exe"));
        if (file.exists())
            return file.absoluteFilePath();
    }
    return {};
}

PreliminaryServerConnection::PreliminaryServerConnection(QSslSocket *sock, Server *srv) : QObject(
        srv),
                                                                                          socket(sock),
                                                                                          server(srv),
                                                                                          state(State_Connecting),
                                                                                          timeoutTimer(
                                                                                                  this),
                                                                                          readBuffer(),
                                                                                          writeBuffer(),
                                                                                          assembledID()
{
    timeoutTimer.setSingleShot(true);
    connect(&timeoutTimer, SIGNAL(timeout()), this, SLOT(connectionTimeout()));

    timeoutTimer.start(30000);

    connect(socket, SIGNAL(encrypted()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(socket, SIGNAL(modeChanged(QSslSocket::SslMode)), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(socket, SIGNAL(readyRead()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(socket, SIGNAL(readChannelFinished()), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(processWrite()), Qt::QueuedConnection);

    QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
}

PreliminaryServerConnection::~PreliminaryServerConnection()
{
    if (socket)
        delete socket;
}

void PreliminaryServerConnection::signalTerminate()
{
    if (!socket)
        return;
    switch (state) {
    case State_Connecting:
    case State_Receive_Type:
    case State_Receive_ResumeID:
    case State_Handoff_New:
    case State_Handoff_Resume:
    case State_Ready_New:
    case State_Ready_Resume:
        socket->abort();
        state = State_Closed;
        timeoutTimer.stop();
        emit aborted();
        break;
    case State_Closed:
        return;
    }

    QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
}

bool PreliminaryServerConnection::isAborted() const
{
    if (!socket)
        return true;
    switch (state) {
    case State_Connecting:
    case State_Receive_Type:
    case State_Receive_ResumeID:
    case State_Handoff_New:
    case State_Handoff_Resume:
    case State_Ready_New:
    case State_Ready_Resume:
        return false;
    case State_Closed:
        return true;
    }
    Q_ASSERT(false);
    return true;
}

bool PreliminaryServerConnection::isReady() const
{
    if (!socket)
        return false;
    switch (state) {
    case State_Connecting:
    case State_Receive_Type:
    case State_Receive_ResumeID:
    case State_Handoff_New:
    case State_Handoff_Resume:
    case State_Closed:
        return false;
    case State_Ready_New:
    case State_Ready_Resume:
        return true;
    }
    Q_ASSERT(false);
    return false;
}

ServerConnectionID PreliminaryServerConnection::id() const
{ return assembledID; }

ServerConnection *PreliminaryServerConnection::createConnection() const
{
    switch (state) {
    case State_Connecting:
    case State_Receive_Type:
    case State_Receive_ResumeID:
    case State_Handoff_New:
    case State_Handoff_Resume:
    case State_Closed:
    case State_Ready_Resume:
        return NULL;
    case State_Ready_New:
        break;
    }

#ifdef Q_OS_UNIX
    if (qgetenv("CPD3_SYNCSERVER_ONE_PROCESS").isEmpty()) {
        if (!findHandlerProgram().isEmpty()) {
            return new ServerConnectionDetached(assembledID);
        }
    }
#endif

    return new ServerConnectionInternal(assembledID);
}

QSslSocket *PreliminaryServerConnection::takeSocket()
{
    QSslSocket *result = socket;
    socket = nullptr;
    state = State_Closed;
    return result;
}

void PreliminaryServerConnection::updateSocket()
{
    if (!socket)
        return;

    if (socket->state() == QAbstractSocket::UnconnectedState) {
        qCDebug(log_sync_server) << "Socket" << assembledID << "closed during initial handshake:"
                                 << socket->errorString();
        socket->deleteLater();
        socket = nullptr;
        timeoutTimer.stop();
        state = State_Closed;
        emit aborted();
        return;
    }

    switch (state) {
    case State_Connecting:
        if (!socket->isEncrypted())
            return;

        {
            QSslCertificate cert(socket->peerCertificate());
            if (cert.isNull()) {
                qCInfo(log_sync_server) << "Received null certificate from"
                                        << socket->peerAddress().toString();
                signalTerminate();
                return;
            }
            assembledID.digest = Cryptography::sha512(cert).leftJustified(64, 0, true);
        }

        assembledID.station = server->loadAuthorized(assembledID.digest);
        if (assembledID.station.empty()) {
            qCInfo(log_sync_server) << "Unauthorized certificate"
                                    << assembledID.digest.toHex().constData() << "received from"
                                    << socket->peerAddress().toString();
            signalTerminate();
            return;
        }

        qCInfo(log_sync_server) << "Incoming connection from" << socket->peerAddress().toString()
                                << "with certificate" << assembledID.digest.toHex().constData()
                                << "bound to"
                                << QString::fromStdString(assembledID.station).toUpper();

        state = State_Receive_Type;
        timeoutTimer.start(30000);

        QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        break;
    case State_Receive_Type:
        if (readExactly(socket, readBuffer, 1)) {
            QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        }
        if (readBuffer.size() < 1)
            break;
        switch (readBuffer.at(0)) {
        case 0:
            assembledID.id = Random::data(16);
            writeBuffer.append(assembledID.id);

            qCInfo(log_sync_server) << "New synchronization request from"
                                    << socket->peerAddress().toString();

            readBuffer.clear();
            state = State_Handoff_New;
            QMetaObject::invokeMethod(this, "processWrite", Qt::QueuedConnection);
            break;
        case 1:
            qCInfo(log_sync_server) << "Synchronization resume request from"
                                    << socket->peerAddress().toString();

            readBuffer.clear();
            state = State_Receive_ResumeID;
            QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
            break;
        default:
            qCDebug(log_sync_server) << "Invalid initialization mode received from"
                                     << socket->peerAddress().toString();
            signalTerminate();
            break;
        }
        break;
    case State_Receive_ResumeID:
        if (readExactly(socket, readBuffer, 16)) {
            QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        }
        if (readBuffer.size() < 16)
            break;

        assembledID.id = readBuffer.leftJustified(16, 0, true);

        readBuffer.clear();
        state = State_Handoff_Resume;
        QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        break;
    case State_Handoff_New:
        if (!writeBuffer.isEmpty())
            return;
        socket->flush();
        state = State_Ready_New;
        timeoutTimer.stop();
        emit ready();
        break;
    case State_Handoff_Resume:
        if (!writeBuffer.isEmpty())
            return;
        socket->flush();
        state = State_Ready_Resume;
        timeoutTimer.stop();
        emit ready();
        break;
    case State_Ready_New:
    case State_Ready_Resume:
        break;
    case State_Closed:
        break;
    }
}

void PreliminaryServerConnection::connectionTimeout()
{
    if (!socket)
        return;
    switch (state) {
    case State_Connecting:
    case State_Receive_Type:
    case State_Receive_ResumeID:
    case State_Handoff_New:
    case State_Handoff_Resume:
        qCDebug(log_sync_server) << "Timeout in initial connection state" << state << "from"
                                 << socket->peerAddress().toString();
        socket->abort();
        socket->deleteLater();
        socket = nullptr;
        timeoutTimer.stop();
        state = State_Closed;
        emit aborted();
        break;
    case State_Ready_New:
    case State_Ready_Resume:
        break;
    case State_Closed:
        break;
    }
}

void PreliminaryServerConnection::processWrite()
{
    if (!socket)
        return;

    QByteArray toWrite;
    qSwap(toWrite, writeBuffer);
    if (toWrite.isEmpty())
        return;

    qint64 n = socket->write(toWrite);
    if (n == -1) {
        qCDebug(log_sync_server) << "Socket " << assembledID
                                 << " disconnected during write operation: "
                                 << socket->errorString();
        socket->deleteLater();
        socket = nullptr;
        timeoutTimer.stop();
        state = State_Closed;
        emit aborted();
        return;
    }

    if ((int) n >= toWrite.size()) {
        QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        return;
    }

    Q_ASSERT(writeBuffer.isEmpty());

    int remaining = toWrite.size() - (int) n;
    writeBuffer.resize(remaining);
    memcpy(writeBuffer.data(), toWrite.constData() + n, remaining);
}


ServerConnection::ServerConnection(const ServerConnectionID &id) : connectionID(id)
{ }

ServerConnection::~ServerConnection()
{ }


ServerConnectionInternal::ServerConnectionInternal(const ServerConnectionID &id) : ServerConnection(
        id),
                                                                                   state(State_Initialize),
                                                                                   socket(nullptr),
                                                                                   compressor(
                                                                                           nullptr),
                                                                                   lock(),
                                                                                   lockKey(),
                                                                                   peer(nullptr),
                                                                                   configurationInitializer()
{
    timeoutTimer.setSingleShot(true);
    connect(&timeoutTimer, SIGNAL(timeout()), this, SLOT(connectionTimeout()));
}

ServerConnectionInternal::~ServerConnectionInternal()
{
    lock.reset();
    if (compressor) {
        compressor->abort();
        delete compressor;
    }
    if (socket) {
        socket->abort();
        delete socket;
    }
    if (peer)
        delete peer;
    if (configurationInitializer) {
        configurationInitializer->signalTerminate();
        configurationInitializer->wait();
        configurationInitializer.reset();
    }

    Memory::release_unused();
}

void ServerConnectionInternal::attach(QSslSocket *sock)
{
    switch (state) {
    case State_Initialize:
        break;
    case State_Configuring:
    case State_Connected:
    case State_Resume:
    case State_Closing:
    case State_Completed:
        qCDebug(log_sync_server) << "Can't begin in state" << state;
        socket->deleteLater();
        return;
    }

    qCDebug(log_sync_server) << "Attaching internal connection" << id() << "to"
                             << sock->peerAddress().toString();

    if (compressor) {
        compressor->abort();
        compressor->deleteLater();
        compressor = nullptr;
    }
    if (socket) {
        socket->abort();
        socket->deleteLater();
        socket = nullptr;
    }
    socket = sock;

    connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    compressor = new CompressedStream(socket, this);
    Q_ASSERT(compressor);
    compressor->setMaximumBufferSize(8388608);

    connect(compressor, SIGNAL(readyRead()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(compressor, SIGNAL(readChannelFinished()), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(compressor, SIGNAL(bytesWritten(qint64)), this, SLOT(peerWrite()),
            Qt::QueuedConnection);

    if (configurationInitializer) {
        configurationInitializer->signalTerminate();
        configurationInitializer->wait();
    }
    state = State_Configuring;
    configurationInitializer.reset(new ConfigurationInitializer(id()));
    configurationInitializer->complete.connect(this, "configurationReady");

    timeoutTimer.stop();
    configurationReady();
}

static const quint8 rcSynchronizeStateVersion = 1;

void ServerConnectionInternal::configurationReady()
{
    if (!configurationInitializer)
        return;
    if (!configurationInitializer->isFinished())
        return;
    configurationInitializer->wait();

    switch (state) {
    case State_Configuring:
        break;
    case State_Initialize:
    case State_Connected:
    case State_Resume:
    case State_Closing:
    case State_Completed:
        configurationInitializer.reset();
        return;
    }

    if (!configurationInitializer->wasSuccessful()) {
        qCDebug(log_sync_server) << "Configuration failed for" << id()
                                 << ", synchronization aborted";
        configurationInitializer.reset();

        fatalShutdown();
        state = State_Completed;
        emit completed();
        return;
    }

    qCDebug(log_sync_server) << "Configuration suceeded for" << id()
                             << ", starting peer synchronization";

    auto result = configurationInitializer->take();
    configurationInitializer.reset();
    lock = std::move(result.lock);
    lockKey = std::move(result.lockKey);

    timeoutTimer.start(120 * 1000);
    state = State_Connected;

    QByteArray remoteState;
    {
        QByteArray data(lock->get(lockKey));
        if (!data.isEmpty()) {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 version = 0;
            stream >> version;

            if (stream.status() != QDataStream::Ok || version != rcSynchronizeStateVersion) {
                qCDebug(log_sync_server) << "Invalid state data";
            } else {
                stream >> remoteState;
            }
        }
    }

    peer = new Peer(new BasicFilter(ValueSegment::withPath(result.config, "Local/Synchronize"),
                                    id().station),
                    new TrackingFilter(ValueSegment::withPath(result.config, "Remote/Synchronize"),
                                       id().station, remoteState), Peer::Mode_Bidirectional,
                    result.modifiedAfter);
    Q_ASSERT(peer);
    connect(peer, SIGNAL(incomingStallCleared()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(peer, SIGNAL(writeAvailable()), this, SLOT(peerWrite()), Qt::QueuedConnection);
    connect(peer, SIGNAL(errorDetected()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(peer, SIGNAL(finished()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(peer, SIGNAL(flushRequested()), this, SLOT(flushCompressor()), Qt::QueuedConnection);

    QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
    QMetaObject::invokeMethod(this, "peerWrite", Qt::QueuedConnection);

    peer->start();
}

void ServerConnectionInternal::reattach(QSslSocket *sock)
{
    switch (state) {
    case State_Resume:
        break;
    case State_Configuring:
    case State_Initialize:
    case State_Closing:
    case State_Completed:
        qCDebug(log_sync_server) << "Can't reattach in state" << state;
        socket->deleteLater();
        return;
    case State_Connected:
        qCDebug(log_sync_server) << "Resuming " << id() << "on reconnection from"
                                 << socket->peerAddress().toString();
        break;
    }

    qCDebug(log_sync_server) << "Reattaching internal connection" << id() << "to"
                             << sock->peerAddress().toString();

    if (compressor) {
        compressor->abort();
        compressor->deleteLater();
        compressor = nullptr;
    }
    if (socket) {
        socket->abort();
        socket->deleteLater();
        socket = nullptr;
    }
    socket = sock;

    connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    compressor = new CompressedStream(socket, this);
    Q_ASSERT(compressor);
    compressor->setMaximumBufferSize(8388608);

    connect(compressor, SIGNAL(readyRead()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(compressor, SIGNAL(readChannelFinished()), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(compressor, SIGNAL(bytesWritten(qint64)), this, SLOT(peerWrite()),
            Qt::QueuedConnection);

    Q_ASSERT(peer);

    timeoutTimer.start(120 * 1000);
    state = State_Connected;

    if (!peer->rollback()) {
        qCDebug(log_sync_server) << "Rollback failed for" << id() << ", synchronization aborted";
        fatalShutdown();
        state = State_Completed;
        emit completed();
        return;
    }

    QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
    QMetaObject::invokeMethod(this, "peerWrite", Qt::QueuedConnection);
}

void ServerConnectionInternal::flushCompressor()
{
    if (!compressor)
        return;
    compressor->flush(0);
}

void ServerConnectionInternal::disconnectResume()
{
    switch (state) {
    case State_Connected:
        break;
    case State_Configuring:
    case State_Resume:
    case State_Initialize:
    case State_Closing:
    case State_Completed:
        return;
    }

    Q_ASSERT(peer);

    qCInfo(log_sync_server) << "Recoverable error from" << id() << ", awaiting resume";

    if (compressor) {
        compressor->abort();
        compressor->deleteLater();
        compressor = nullptr;
    }
    if (socket) {
        socket->abort();
        socket->deleteLater();
        socket = nullptr;
    }

    state = State_Resume;

    timeoutTimer.start(3600 * 1000);
}

bool ServerConnectionInternal::isCompleted() const
{
    switch (state) {
    case State_Completed:
        break;
    case State_Configuring:
    case State_Resume:
    case State_Initialize:
    case State_Closing:
    case State_Connected:
        return false;
    }
    return true;
}

void ServerConnectionInternal::connectionTimeout()
{
    switch (state) {
    case State_Initialize:
    case State_Configuring:
    case State_Completed:
        break;
    case State_Connected:
        qCInfo(log_sync_server) << "Timeout expired while connected to" << id();
        disconnectResume();
        return;
    case State_Resume:
        qCInfo(log_sync_server) << "No reconnection received from" << id() << ", giving up";
        if (lock) {
            lock->fail();
            lock.reset();
        }
        fatalShutdown();
        state = State_Completed;
        emit completed();
        return;
    case State_Closing:
        qCDebug(log_sync_server) << "Took too long to close " << id();
        fatalShutdown();
        state = State_Completed;
        emit completed();
        return;
    }
}

void ServerConnectionInternal::fatalShutdown()
{
    if (compressor) {
        compressor->abort();
        compressor->deleteLater();
        compressor = nullptr;
    }
    if (socket) {
        socket->abort();
        socket->deleteLater();
        socket = nullptr;
    }
    if (peer) {
        peer->signalTerminate();
    }
    if (lock) {
        lock->fail();
        lock.reset();
    }
    if (configurationInitializer) {
        configurationInitializer->signalTerminate();
    }
}

void ServerConnectionInternal::signalTerminate()
{
    switch (state) {
    case State_Completed:
        return;
    case State_Configuring:
    case State_Resume:
    case State_Initialize:
    case State_Closing:
    case State_Connected:
        break;
    }

    fatalShutdown();
    state = State_Completed;
    emit completed();
}

void ServerConnectionInternal::peerWrite()
{
    if (!peer || !compressor)
        return;
    switch (peer->writePending(compressor)) {
    case Peer::WriteResult_Wait:
    case Peer::WriteResult_Continue:
        break;
    case Peer::WriteResult_Completed:
        compressor->disconnect(this, SLOT(peerWrite()));
        compressor->flush(0);
        QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        break;
    case Peer::WriteResult_Error:
        qCDebug(log_sync_server) << "Write error encountered on socket " << id();
        QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        return;
    }

    switch (state) {
    case State_Initialize:
    case State_Completed:
    case State_Resume:
    case State_Configuring:
    case State_Connected:
        break;
    case State_Closing:
        updateSocket();
        break;
    }
}

void ServerConnectionInternal::updateSocket()
{
    switch (state) {
    case State_Initialize:
    case State_Completed:
    case State_Resume:
        return;
    case State_Configuring:
        break;
    case State_Connected:
        Q_ASSERT(compressor);
        Q_ASSERT(socket);
        Q_ASSERT(peer);

        if (socket->state() == QAbstractSocket::UnconnectedState) {
            compressor->drainBackendRead();
            while (compressor->bytesAvailable() > 0) {
                peer->incomingData(compressor->read(65536));
            }
        } else {
            if (!peer->shouldStallIncoming() && compressor->bytesAvailable() > 0) {
                timeoutTimer.start(120 * 1000);
                peer->incomingData(compressor->read(65536));
                if (compressor->bytesAvailable() > 0) {
                    QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
                }
            }
        }

        switch (peer->errorState()) {
        case Peer::Error_None:
            break;
        case Peer::Error_Fatal:
            qCDebug(log_sync_server) << "Fatal error in connection " << id()
                                     << ", aborting synchronization";
            fatalShutdown();
            state = State_Completed;
            emit completed();
            return;
        case Peer::Error_Recoverable:
            disconnectResume();
            return;
        }

        if (peer->isFinished()) {
            compressor->flush(0);

            if (lock) {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << rcSynchronizeStateVersion;
                    stream << peer->getRemoteFilterState();
                }
                lock->set(lockKey, data);

                lock->release();
                lock.reset();
            }

            peer->deleteLater();
            peer = nullptr;
            state = State_Closing;
            QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);

            timeoutTimer.start(120 * 1000);

            qCInfo(log_sync_server) << "Synchronization completed with" << id()
                                    << ", shutting down connection";
            return;
        }

        /* Transfer completed, so don't check the network status */
        if (peer->transferCompleted())
            return;

        if (socket->state() == QAbstractSocket::UnconnectedState) {
            qCDebug(log_sync_server) << "Premature disconnection from" << id();
            disconnectResume();
            return;
        }
        break;
    case State_Closing:
        if (!compressor)
            break;

        Q_ASSERT(socket);
        if (compressor->bytesToWrite() > 0) {
            compressor->flush(0);
            socket->flush();
        } else {
            if (socket->state() == QAbstractSocket::ConnectedState) {
                socket->disconnectFromHost();
            }
        }
        if (socket->state() != QAbstractSocket::UnconnectedState)
            return;

        qCDebug(log_sync_server) << "Connection shutdown complete for " << id();
        compressor->close();
        compressor->deleteLater();
        compressor = nullptr;
        socket->deleteLater();
        socket = nullptr;
        state = State_Completed;
        emit completed();
        return;
    }

    if (compressor) {
        Q_ASSERT(socket);
        if (socket->state() != QAbstractSocket::UnconnectedState)
            return;
    }

    qCDebug(log_sync_server) << "Disconnected in state" << state;

    fatalShutdown();
    state = State_Completed;
    emit completed();
}

ServerConnectionInternal::ConfigurationInitializer::ConfigurationInitializer(const ServerConnectionID &i)
        : id(i), mutex(), result(), state(Active), thread()
{
    thread = std::thread(&ConfigurationInitializer::run, this);
}

ServerConnectionInternal::ConfigurationInitializer::~ConfigurationInitializer()
{
    wait();
}

bool ServerConnectionInternal::ConfigurationInitializer::wasSuccessful()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == Succeeded;
}

ServerConnectionInternal::ConfigurationInitializer::Result ServerConnectionInternal::ConfigurationInitializer::take()
{
    Q_ASSERT(state == Succeeded);
    return std::move(result);
}

void ServerConnectionInternal::ConfigurationInitializer::signalTerminate()
{
    std::lock_guard<std::mutex> lock(mutex);
    switch (state) {
    case Active:
        state = Terminating;
        break;
    case Terminating:
    case Succeeded:
    case Failed:
        break;
    }
}

bool ServerConnectionInternal::ConfigurationInitializer::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    switch (state) {
    case Active:
    case Terminating:
        return false;
    case Succeeded:
    case Failed:
        return true;
    }
    return false;
}

bool ServerConnectionInternal::ConfigurationInitializer::maybeTerminate()
{
    std::lock_guard<std::mutex> lock(mutex);
    switch (state) {
    case Active:
        return false;
    case Terminating:
        state = Failed;
        return true;
    case Succeeded:
    case Failed:
        Q_ASSERT(false);
        break;
    }
    return false;
}

void ServerConnectionInternal::ConfigurationInitializer::run()
{
    auto cSig = complete.defer();

    if (maybeTerminate())
        return;

    result.config = ValueSegment::Stream::read(
            Archive::Selection(FP::undefined(), FP::undefined(), {id.station}, {"configuration"},
                               {"synchronize"}));

    if (maybeTerminate())
        return;

    QString digestKey(id.digest.toHex().toLower());
    result.config =
            ValueSegment::withPath(result.config, QString("Authorization/%1").arg(digestKey));

    if (maybeTerminate())
        return;

    result.lock.reset(new Database::RunLock);

    bool ok = false;
    result.lockKey =
            QString("synchronize %1 %2").arg(QString::fromStdString(id.station), digestKey);
    result.modifiedAfter = result.lock->acquire(result.lockKey, 30.0, &ok);
    if (!ok) {
        qCDebug(log_sync_server) << "Failed to acquire lock for" << id;
        return;
    }

    std::lock_guard<std::mutex> lock(mutex);
    state = Succeeded;
}

void ServerConnectionInternal::ConfigurationInitializer::wait()
{
    if (thread.joinable())
        thread.join();
}

#ifdef Q_OS_UNIX

ServerConnectionDetached::ServerConnectionDetached(const ServerConnectionID &id) : ServerConnection(
        id),
                                                                                   state(State_Initialize),
                                                                                   timeoutTimer(
                                                                                           this),
                                                                                   socket(nullptr),
                                                                                   external(
                                                                                           nullptr),
                                                                                   link(nullptr),
                                                                                   server(nullptr),
                                                                                   localAuthorization(),
                                                                                   remoteAuthorization(),
                                                                                   socketWriteBuffer(),
                                                                                   externalWriteBuffer(),
                                                                                   linkWriteBuffer(),
                                                                                   externalReadBuffer(),
                                                                                   linkInitialReadBuffer()
{
    timeoutTimer.setSingleShot(true);
    connect(&timeoutTimer, SIGNAL(timeout()), this, SLOT(connectionTimeout()));
}

ServerConnectionDetached::~ServerConnectionDetached()
{
    if (socket)
        delete socket;
    if (external) {
        external->kill();
        external->waitForFinished();
        delete external;

        qCDebug(log_sync_server) << "External process killed for" << id();
    }
    if (link)
        delete link;
    if (server) {
        server->close();
        delete server;
    }

    Memory::release_unused();
}

void ServerConnectionDetached::attach(QSslSocket *sock)
{
    switch (state) {
    case State_Initialize:
        break;
    case State_Resume:
    case State_WaitForStarted:
    case State_SendInitialization:
    case State_RecieveExternalAuthorization:
    case State_Completed:
    case State_Connected:
    case State_WaitForConnected:
    case State_SendExternalAuthorization:
    case State_AuthorizeExternal:
    case State_Closing:
        qCDebug(log_sync_server) << "Can't begin in state" << state;
        socket->deleteLater();
        return;
    }

    if (external) {
        external->kill();
        external->waitForFinished();
        external->deleteLater();
        external = nullptr;
    }
    if (link) {
        link->close();
        link->deleteLater();
        link = nullptr;
    }
    if (server) {
        server->close();
        server->deleteLater();
        server = nullptr;
    }
    if (socket) {
        socket->abort();
        socket->deleteLater();
        socket = nullptr;
    }
    socket = sock;

    connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(socket, SIGNAL(readyRead()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(socket, SIGNAL(readChannelFinished()), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(writeToSocket()),
            Qt::QueuedConnection);

    localAuthorization = Random::data(16);
    QString socketName(QString("CPD3SYNC-%1").arg(Random::string()));

    server = new QLocalServer(this);
    Q_ASSERT(server);
    server->setSocketOptions(QLocalServer::UserAccessOption);
    connect(server, SIGNAL(newConnection()), this, SLOT(incomingConnection()),
            Qt::QueuedConnection);

    QLocalServer::removeServer(socketName);
    if (!server->listen(socketName)) {
        qCDebug(log_sync_server) << "Failed to listen to" << socketName << "for" << id() << ":"
                                 << server->errorString();
        fatalShutdown();
        state = State_Completed;
        emit completed();
        return;
    }

    external = new QProcess(this);
    Q_ASSERT(external);
    Process::forwardError(external);

    connect(external, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(updateExternal()),
            Qt::QueuedConnection);
    connect(external, SIGNAL(stateChanged(QProcess::ProcessState)), this, SLOT(updateExternal()),
            Qt::QueuedConnection);
    connect(external, SIGNAL(readyRead()), this, SLOT(updateExternal()), Qt::QueuedConnection);
    connect(external, SIGNAL(readChannelFinished()), this, SLOT(updateExternal()),
            Qt::QueuedConnection);
    connect(external, SIGNAL(bytesWritten(qint64)), this, SLOT(writeToExternal()),
            Qt::QueuedConnection);

    {
        QString program(findHandlerProgram());
        if (program.isEmpty()) {
            qCDebug(log_sync_server) << "Failed to locate external handler program";
            fatalShutdown();
            state = State_Completed;
            emit completed();
            return;
        }
        external->start(program);
    }

    externalWriteBuffer.append(localAuthorization);
    QByteArray data;
    {
        QDataStream stream(&data, QIODevice::WriteOnly);
        stream.setVersion(QDataStream::Qt_4_5);
        stream.setByteOrder(QDataStream::LittleEndian);

        stream << socketName;
        stream << id().station;
        stream << QString::fromLatin1(id().digest.toHex().toLower());
    }
    {
        int n = externalWriteBuffer.size();
        externalWriteBuffer.resize(n + 4);
        qToLittleEndian<quint32>(data.size(), (uchar *) externalWriteBuffer.data() + n);
    }
    externalWriteBuffer.append(data);

    qCDebug(log_sync_server) << "Detached handler for" << id() << "started listening on"
                             << socketName;

    timeoutTimer.start(120 * 1000);

    state = State_WaitForStarted;
    externalReadBuffer.clear();
    QMetaObject::invokeMethod(this, "updateExternal", Qt::QueuedConnection);

}

void ServerConnectionDetached::reattach(QSslSocket *sock)
{
    switch (state) {
    case State_Resume:
        break;
    case State_Initialize:
    case State_WaitForStarted:
    case State_SendInitialization:
    case State_RecieveExternalAuthorization:
    case State_Completed:
    case State_Closing:
        qCDebug(log_sync_server) << "Can't reattach in state" << state;
        socket->deleteLater();
        return;
    case State_Connected:
    case State_WaitForConnected:
    case State_SendExternalAuthorization:
    case State_AuthorizeExternal:
        qCDebug(log_sync_server) << "Resuming" << id() << "on reconnection from"
                                 << socket->peerAddress().toString();
        break;
    }

    if (!external) {
        socket->deleteLater();
        return;
    }

    if (link) {
        link->abort();
        link->deleteLater();
        link = nullptr;
    }
    if (socket) {
        socket->abort();
        socket->deleteLater();
        socket = nullptr;
    }
    socket = sock;
    linkWriteBuffer.clear();
    socketWriteBuffer.clear();
    linkInitialReadBuffer.clear();

    connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(socket, SIGNAL(readyRead()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(socket, SIGNAL(readChannelFinished()), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(writeToSocket()),
            Qt::QueuedConnection);

    state = State_WaitForConnected;
    timeoutTimer.start(120 * 1000);

    Q_ASSERT(external);
    externalWriteBuffer.append((char) Internal_Reconnected);
    QMetaObject::invokeMethod(this, "writeToExternal", Qt::QueuedConnection);
}

bool ServerConnectionDetached::isCompleted() const
{
    switch (state) {
    case State_Initialize:
    case State_WaitForStarted:
    case State_SendInitialization:
    case State_RecieveExternalAuthorization:
    case State_WaitForConnected:
    case State_SendExternalAuthorization:
    case State_AuthorizeExternal:
    case State_Connected:
    case State_Resume:
    case State_Closing:
        return false;
    case State_Completed:
        break;
    }
    return true;
}

void ServerConnectionDetached::fatalShutdown(bool kill)
{
    if (socket) {
        socket->abort();
        socket->deleteLater();
        socket = nullptr;
    }
    if (link) {
        link->abort();
        link->deleteLater();
        link = nullptr;
    }
    if (server) {
        server->close();
        server->deleteLater();
        server = nullptr;
    }

    if (kill) {
        if (external) {
            external->kill();
            external->waitForFinished(1000);
            external->deleteLater();
            external = nullptr;

            qCDebug(log_sync_server) << "External process killed for" << id();
        }

        state = State_Completed;
        emit completed();
        return;
    } else {
        if (external) {
            external->terminate();
        }
    }

    if (external) {
        externalWriteBuffer.clear();
        externalWriteBuffer.append((char) Internal_Aborted);
        QMetaObject::invokeMethod(this, "writeToExternal", Qt::QueuedConnection);
    }

    state = State_Closing;
    checkCompleted();
}

void ServerConnectionDetached::signalTerminate()
{
    switch (state) {
    case State_Initialize:
    case State_WaitForStarted:
    case State_SendInitialization:
    case State_RecieveExternalAuthorization:
    case State_WaitForConnected:
    case State_SendExternalAuthorization:
    case State_AuthorizeExternal:
    case State_Connected:
    case State_Resume:
        break;
    case State_Closing:
    case State_Completed:
        return;
    }

    fatalShutdown();
}

void ServerConnectionDetached::connectionTimeout()
{
    switch (state) {
    case State_Initialize:
        break;
    case State_WaitForStarted:
        qCInfo(log_sync_server) << "Timeout waiting for external handler for" << id() << "to start";
        fatalShutdown(true);
        break;
    case State_SendInitialization:
        qCInfo(log_sync_server) << "Timeout sending initialization for for" << id() << "to start";
        fatalShutdown(true);
        break;
    case State_RecieveExternalAuthorization:
        qCInfo(log_sync_server) << "Timeout waiting for authorization from" << id() << "to start";
        fatalShutdown(true);
        break;

    case State_WaitForConnected:
        qCInfo(log_sync_server) << "Timeout waiting for data connection from" << id();
        disconnectResume();
        break;
    case State_SendExternalAuthorization:
        qCInfo(log_sync_server) << "Timeout sending data authorization to" << id();
        disconnectResume();
        break;
    case State_AuthorizeExternal:
        qCInfo(log_sync_server) << "Timeout waiting for data authorization from" << id();
        disconnectResume();
        break;
    case State_Connected:
        qCInfo(log_sync_server) << "Timeout during synchronization from" << id();
        disconnectResume();
        break;
    case State_Resume:
        qCInfo(log_sync_server) << "No reconnection received from" << id() << ", giving up";
        fatalShutdown();
        break;
    case State_Closing:
        qCInfo(log_sync_server) << "Took too long to close" << id();
        fatalShutdown(true);
        break;
    case State_Completed:
        break;
    }

}

void ServerConnectionDetached::disconnectResume()
{
    /* Don't close the sockets and link right away, since this might
     * actually be the final chunk, which means we need to flush them.
     * Closing is handled by the write routines. */

    switch (state) {
    case State_Initialize:
    case State_WaitForStarted:
    case State_SendInitialization:
    case State_RecieveExternalAuthorization:
        fatalShutdown();
        return;
    case State_WaitForConnected:
    case State_SendExternalAuthorization:
    case State_AuthorizeExternal:
    case State_Connected:
        state = State_Resume;
        break;
    case State_Resume:
    case State_Closing:
    case State_Completed:
        return;
    }

    timeoutTimer.start(3600 * 1000);

    qCDebug(log_sync_server) << "Entering resume wait for" << id();

    checkCompleted();
}

void ServerConnectionDetached::checkCompleted()
{
    switch (state) {
    case State_Closing:
        if (socket)
            return;
        if (link)
            return;
        if (external)
            return;
        break;
    case State_Completed:
        return;

    case State_Initialize:
        return;
    case State_WaitForStarted:
    case State_SendInitialization:
    case State_RecieveExternalAuthorization:
    case State_WaitForConnected:
    case State_SendExternalAuthorization:
    case State_AuthorizeExternal:
    case State_Connected:
    case State_Resume:
        if (!socket && !server && !external && !link)
            break;
        return;
    }

    qCInfo(log_sync_server) << "Detatched connection ended for " << id();

    state = State_Completed;
    emit completed();
}

void ServerConnectionDetached::incomingConnection()
{
    if (link)
        return;

    switch (state) {
    case State_WaitForConnected:
        break;
    case State_Initialize:
    case State_WaitForStarted:
    case State_SendInitialization:
    case State_RecieveExternalAuthorization:
    case State_SendExternalAuthorization:
    case State_AuthorizeExternal:
    case State_Connected:
    case State_Resume:
    case State_Closing:
    case State_Completed:
        return;
    }

    link = server->nextPendingConnection();
    if (!link) {
        link = nullptr;
        return;
    }

    connect(link, SIGNAL(stateChanged(QLocalSocket::LocalSocketState)), this, SLOT(updateLink()),
            Qt::QueuedConnection);
    connect(link, SIGNAL(readyRead()), this, SLOT(updateLink()), Qt::QueuedConnection);
    connect(link, SIGNAL(readChannelFinished()), this, SLOT(updateLink()), Qt::QueuedConnection);
    connect(link, SIGNAL(bytesWritten(qint64)), this, SLOT(writeToLink()), Qt::QueuedConnection);

    linkInitialReadBuffer.clear();
    QMetaObject::invokeMethod(this, "updateLink", Qt::QueuedConnection);

    qCDebug(log_sync_server) << "Received external handler connection for" << id();
}

static bool forwardChunk(QIODevice *device,
                         QByteArray &target,
                         QObject *writeObject = 0,
                         const char *writeMethod = 0)
{
    qint64 n = device->bytesAvailable();
    if (n <= 0)
        return false;

    QByteArray bufferRead(device->read((int) qMin(Q_INT64_C(65536), n)));
    if (target.isEmpty()) {
        target = bufferRead;
        if (writeObject && writeMethod) {
            QMetaObject::invokeMethod(writeObject, writeMethod, Qt::QueuedConnection);
        }
    } else {
        target.append(bufferRead);
    }

    return device->bytesAvailable() > 0;
}

static void consumeBytes(QByteArray &data, int n)
{
    if (n >= data.size()) {
        data.clear();
        return;
    }

    int remaining = data.size() - n;
    memmove(data.data(), data.constData() + n, remaining);
    data.resize(remaining);
}

void ServerConnectionDetached::updateSocket()
{
    if (!socket) {
        checkCompleted();
        return;
    }

    switch (state) {
    case State_Connected:
        if (!link || link->bytesToWrite() + linkWriteBuffer.size() > 8388608)
            break;
        if (socket->bytesAvailable() > 0) {
            timeoutTimer.start(120 * 1000);
        }
        if (forwardChunk(socket, linkWriteBuffer, this, "writeToLink")) {
            QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        }
        break;
    case State_Closing:
    case State_Resume:
        if (link && forwardChunk(socket, linkWriteBuffer, this, "writeToLink")) {
            QMetaObject::invokeMethod(this, "updateLink", Qt::QueuedConnection);
        }
        break;
    case State_WaitForConnected:
    case State_Initialize:
    case State_WaitForStarted:
    case State_SendInitialization:
    case State_RecieveExternalAuthorization:
    case State_SendExternalAuthorization:
    case State_AuthorizeExternal:
    case State_Completed:
        break;
    }

    if (socket->state() != QAbstractSocket::UnconnectedState)
        return;

    qCDebug(log_sync_server) << "Socket for" << id() << "disconnected";

    switch (state) {
    case State_Connected:
        /* This may be the data before we receive the end from
         * the external, so read it all now */
        if (link) {
            while (forwardChunk(socket, linkWriteBuffer, this, "writeToLink")) { }
        }
        disconnectResume();
        break;
    case State_Closing:
    case State_Resume:
        /* This may be the data before we receive the end from
         * the external, so read it all now */
        if (link) {
            while (forwardChunk(socket, linkWriteBuffer, this, "writeToLink")) { }
        }
        break;
    case State_Completed:
    case State_WaitForConnected:
    case State_Initialize:
    case State_WaitForStarted:
    case State_SendInitialization:
    case State_RecieveExternalAuthorization:
    case State_SendExternalAuthorization:
    case State_AuthorizeExternal:
        break;
    }

    socketWriteBuffer.clear();
    socket->close();
    socket->deleteLater();
    socket = nullptr;

    checkCompleted();
}

void ServerConnectionDetached::writeToSocket()
{
    if (!socket)
        return;
    if (!socketWriteBuffer.isEmpty()) {
        qint64 n = socket->write(socketWriteBuffer);
        if (n == -1) {
            qCDebug(log_sync_server) << "Error writing to link for" << id() << ":"
                                     << socket->errorString();
            socket->deleteLater();
            socket = nullptr;
            switch (state) {
            case State_Closing:
            case State_Completed:
                checkCompleted();
                return;
            case State_Initialize:
                fatalShutdown();
                return;
            case State_WaitForStarted:
            case State_SendInitialization:
            case State_RecieveExternalAuthorization:
                /* Ignore this, the transition to connected will
                 * check for an invalid and enter resume if required */
                return;
            case State_WaitForConnected:
            case State_SendExternalAuthorization:
            case State_AuthorizeExternal:
            case State_Connected:
            case State_Resume:
                break;
            }
            disconnectResume();
            return;
        }
        consumeBytes(socketWriteBuffer, (int) n);
        if (socketWriteBuffer.isEmpty()) {
            QMetaObject::invokeMethod(this, "updateLink", Qt::QueuedConnection);
        }
    }

    switch (state) {
    case State_Resume:
        /* Don't close the socket until we've drained the pending data,
         * so if this is the final operation everything is sent */
        if (!socketWriteBuffer.isEmpty())
            return;
        if (socket->bytesToWrite() > 0)
            return;
        socket->close();
        socket->deleteLater();
        socket = nullptr;
        checkCompleted();
        break;
    case State_Closing:
        /* Dont close the socket until we've drained the link */
        if (link)
            return;
        if (!socketWriteBuffer.isEmpty())
            return;
        if (socket->bytesToWrite() > 0)
            return;
        socket->close();
        socket->deleteLater();
        socket = nullptr;
        checkCompleted();
        break;
    case State_Initialize:
    case State_WaitForStarted:
    case State_SendInitialization:
    case State_RecieveExternalAuthorization:
    case State_WaitForConnected:
    case State_SendExternalAuthorization:
    case State_AuthorizeExternal:
    case State_Connected:
    case State_Completed:
        break;
    }
}

void ServerConnectionDetached::updateLink()
{
    if (!link) {
        checkCompleted();
        return;
    }

    switch (state) {
    case State_WaitForConnected:
        if (link->state() != QLocalSocket::ConnectedState)
            break;

        state = State_SendExternalAuthorization;
        linkWriteBuffer.append(remoteAuthorization);
        QMetaObject::invokeMethod(this, "writeToLink", Qt::QueuedConnection);
        break;
    case State_SendExternalAuthorization:
        if (readExactly(link, linkInitialReadBuffer, 16)) {
            QMetaObject::invokeMethod(this, "updateLink", Qt::QueuedConnection);
        }
        break;
    case State_AuthorizeExternal:
        if (readExactly(link, linkInitialReadBuffer, 16)) {
            QMetaObject::invokeMethod(this, "updateLink", Qt::QueuedConnection);
        }
        if (linkInitialReadBuffer.size() < 16)
            break;

        if (linkInitialReadBuffer != localAuthorization) {
            qCDebug(log_sync_server) << "Received mismatched authorization for" << id();
            link->abort();
            link->deleteLater();
            link = nullptr;
            state = State_WaitForConnected;
            QMetaObject::invokeMethod(this, "incomingConnection", Qt::QueuedConnection);

            checkCompleted();
            break;
        }

        linkInitialReadBuffer.clear();

        if (!socket) {
            qCDebug(log_sync_server)
                << "Remote connection closed during data link initialization for" << id();
            disconnectResume();
            return;
        }

        qCDebug(log_sync_server) << "Remote handler for" << id()
                                 << " accepted, synchronization proceeding";

        timeoutTimer.start(120 * 1000);

        state = State_Connected;
        QMetaObject::invokeMethod(this, "updateLink", Qt::QueuedConnection);
        QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        QMetaObject::invokeMethod(this, "updateExternal", Qt::QueuedConnection);
        break;

    case State_Connected:
        if (!socket || socket->bytesToWrite() + socketWriteBuffer.size() > 8388608)
            break;
        if (forwardChunk(link, socketWriteBuffer, this, "writeToSocket")) {
            QMetaObject::invokeMethod(this, "updateLink", Qt::QueuedConnection);
        }
        break;
    case State_Closing:
    case State_Resume:
        if (socket && forwardChunk(link, socketWriteBuffer, this, "writeToSocket")) {
            QMetaObject::invokeMethod(this, "updateLink", Qt::QueuedConnection);
        }
        break;
    case State_Completed:
    case State_Initialize:
    case State_WaitForStarted:
    case State_SendInitialization:
    case State_RecieveExternalAuthorization:
        return;
    }

    if (link->state() != QLocalSocket::UnconnectedState)
        return;

    qCDebug(log_sync_server) << "Data link to" << id() << "disconnected";

    switch (state) {
    case State_Connected:
        /* This may be the data before we receive the end from
         * the external, so read it all now */
        if (socket) {
            while (forwardChunk(link, socketWriteBuffer, this, "writeToSocket")) { }
        }
        disconnectResume();
        break;
    case State_Closing:
    case State_Resume:
        /* This may be the data before we receive the end from
         * the external, so read it all now */
        if (socket) {
            while (forwardChunk(link, socketWriteBuffer, this, "writeToSocket")) { }
        }
        break;
    case State_Completed:
    case State_Initialize:
    case State_WaitForStarted:
    case State_SendInitialization:
    case State_RecieveExternalAuthorization:
        break;
    case State_WaitForConnected:
    case State_SendExternalAuthorization:
    case State_AuthorizeExternal:
        disconnectResume();
        break;
    }

    linkWriteBuffer.clear();
    link->close();
    link->deleteLater();
    link = nullptr;

    checkCompleted();
}

void ServerConnectionDetached::writeToLink()
{
    if (!link)
        return;

    if (!linkWriteBuffer.isEmpty()) {
        qint64 n = link->write(linkWriteBuffer);
        if (n == -1) {
            qCDebug(log_sync_server) << "Error writing to link for" << id() << ":"
                                     << link->errorString();
            link->deleteLater();
            link = nullptr;
            switch (state) {
            case State_Closing:
            case State_Completed:
                checkCompleted();
                return;
            case State_Initialize:
                fatalShutdown();
                return;
            case State_WaitForStarted:
            case State_SendInitialization:
            case State_RecieveExternalAuthorization:
                /* Shouldn't happen */
                return;
            case State_WaitForConnected:
            case State_SendExternalAuthorization:
            case State_AuthorizeExternal:
            case State_Connected:
            case State_Resume:
                break;
            }
            disconnectResume();
            return;
        }
        consumeBytes(linkWriteBuffer, (int) n);
    }

    switch (state) {
    case State_SendExternalAuthorization:
        if (!linkWriteBuffer.isEmpty())
            break;
        state = State_AuthorizeExternal;
        QMetaObject::invokeMethod(this, "updateLink", Qt::QueuedConnection);
        break;
    case State_Connected:
        if (!linkWriteBuffer.isEmpty())
            break;
        QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        break;
    case State_Resume:
        /* Don't close the link until we drain the data we had, so if
         * this was the final one everything gets into it */
        if (!linkWriteBuffer.isEmpty())
            return;
        if (link->bytesToWrite() > 0)
            return;
        link->close();
        link->deleteLater();
        link = nullptr;
        checkCompleted();
        break;

    case State_Closing:
        /* Don't close the link until we've drained the socket */
        if (socket)
            return;
        if (!linkWriteBuffer.isEmpty())
            return;
        if (link->bytesToWrite() > 0)
            return;
        link->close();
        link->deleteLater();
        link = nullptr;
        checkCompleted();
        break;
    case State_Initialize:
    case State_WaitForStarted:
    case State_SendInitialization:
    case State_RecieveExternalAuthorization:
    case State_WaitForConnected:
    case State_AuthorizeExternal:
    case State_Completed:
        break;
    }
}

void ServerConnectionDetached::processExternal()
{
    while (!externalReadBuffer.isEmpty()) {
        ExternalMessage msg = (ExternalMessage) externalReadBuffer.at(0);
        consumeBytes(externalReadBuffer, 1);

        switch (msg) {
        case External_Completed:
            qCDebug(log_sync_server) << "Completion received from" << id();
            switch (state) {
            case State_Connected:
            case State_Resume:
                if (server)
                    server->close();

                if (external) {
                    external->closeWriteChannel();
                }
                externalWriteBuffer.clear();

                state = State_Closing;
                QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
                QMetaObject::invokeMethod(this, "updateLink", Qt::QueuedConnection);
                QMetaObject::invokeMethod(this, "writeToSocket", Qt::QueuedConnection);
                QMetaObject::invokeMethod(this, "writeToLink", Qt::QueuedConnection);

                checkCompleted();
                return;

            case State_WaitForConnected:
            case State_SendExternalAuthorization:
            case State_AuthorizeExternal:
                if (link) {
                    link->abort();
                    link->close();
                    link->deleteLater();
                }
                if (server) {
                    server->close();
                    server->deleteLater();
                    server = nullptr;
                }

                if (external) {
                    external->closeWriteChannel();
                }
                externalWriteBuffer.clear();

                state = State_Closing;
                QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
                QMetaObject::invokeMethod(this, "writeToSocket", Qt::QueuedConnection);

                checkCompleted();
                return;

            case State_Initialize:
            case State_WaitForStarted:
            case State_SendInitialization:
            case State_RecieveExternalAuthorization:
            case State_Completed:
            case State_Closing:
                break;
            }
            break;
        case External_ResumableError:
            qCDebug(log_sync_server) << "Recoverable error received from" << id();
            switch (state) {
            case State_WaitForConnected:
            case State_SendExternalAuthorization:
            case State_AuthorizeExternal:
            case State_Connected:
            case State_Resume:
                disconnectResume();
                break;

            case State_Initialize:
            case State_WaitForStarted:
            case State_SendInitialization:
            case State_RecieveExternalAuthorization:
            case State_Completed:
            case State_Closing:
                break;
            }
            break;
        case External_FatalError:
            qCDebug(log_sync_server) << "Fatal error received from" << id();
            switch (state) {
            case State_Initialize:
            case State_WaitForStarted:
            case State_SendInitialization:
            case State_RecieveExternalAuthorization:
            case State_Completed:
            case State_Closing:
                break;
            case State_WaitForConnected:
            case State_SendExternalAuthorization:
            case State_AuthorizeExternal:
            case State_Connected:
            case State_Resume:
                fatalShutdown();
                break;
            }
            break;
        }
    }
}

void ServerConnectionDetached::updateExternal()
{
    if (!external) {
        checkCompleted();
        return;
    }

    switch (state) {
    case State_Initialize:
    case State_Completed:
        return;
    case State_WaitForStarted:
        if (external->state() != QProcess::Running)
            break;

        state = State_SendInitialization;
        QMetaObject::invokeMethod(this, "writeToExternal", Qt::QueuedConnection);
        break;

    case State_SendInitialization:
        if (readExactly(external, externalReadBuffer, 16)) {
            QMetaObject::invokeMethod(this, "updateExternal", Qt::QueuedConnection);
        }
        break;
    case State_RecieveExternalAuthorization:
        if (readExactly(external, externalReadBuffer, 16)) {
            QMetaObject::invokeMethod(this, "updateExternal", Qt::QueuedConnection);
        }
        if (externalReadBuffer.size() < 16)
            break;
        remoteAuthorization = externalReadBuffer;
        externalReadBuffer.clear();

        state = State_WaitForConnected;
        QMetaObject::invokeMethod(this, "incomingConnection", Qt::QueuedConnection);
        break;
    case State_WaitForConnected:
    case State_SendExternalAuthorization:
    case State_AuthorizeExternal:
    case State_Connected:
    case State_Resume:
        forwardChunk(external, externalReadBuffer);
        processExternal();
        break;
    case State_Closing:
        break;
    }

    if (external->state() != QProcess::NotRunning)
        return;

    qCDebug(log_sync_server) << "External process ended for" << id() << "with exit code"
                             << external->exitCode();

    externalWriteBuffer.clear();
    external->close();
    external->deleteLater();
    external = nullptr;

    if (link) {
        link->abort();
        link->close();
        link->deleteLater();
        link = nullptr;
    }
    if (server) {
        server->close();
        server->deleteLater();
        server = nullptr;
    }

    state = State_Closing;
    checkCompleted();

    QMetaObject::invokeMethod(this, "writeToSocket", Qt::QueuedConnection);
}

void ServerConnectionDetached::writeToExternal()
{
    if (!external)
        return;

    if (!externalWriteBuffer.isEmpty()) {
        qint64 n = external->write(externalWriteBuffer);
        if (n == -1) {
            qCDebug(log_sync_server) << "Error writing to external for" << id() << ":"
                                     << external->errorString();

            switch (state) {
            case State_Closing:
            case State_Completed:
                if (!external->waitForFinished(0)) {
                    external->terminate();
                    if (!external->waitForFinished(1000)) {
                        external->kill();
                        external->waitForFinished(1000);
                    }
                }
                external->close();
                external->deleteLater();
                external = nullptr;

                checkCompleted();
                return;

            case State_Initialize:
            case State_WaitForStarted:
                return;
            case State_SendInitialization:
            case State_RecieveExternalAuthorization:
            case State_WaitForConnected:
            case State_SendExternalAuthorization:
            case State_AuthorizeExternal:
            case State_Connected:
            case State_Resume:
                break;
            }

            if (!external->waitForFinished(0)) {
                external->kill();
                external->waitForFinished(1000);
            }
            external->close();
            external->deleteLater();
            external = nullptr;
            fatalShutdown();
            return;
        }
        consumeBytes(externalWriteBuffer, (int) n);
    }

    switch (state) {
    case State_SendInitialization:
        if (!externalWriteBuffer.isEmpty())
            break;
        state = State_RecieveExternalAuthorization;
        QMetaObject::invokeMethod(this, "updateExternal", Qt::QueuedConnection);
        break;
    case State_Closing:
        if (!externalWriteBuffer.isEmpty())
            return;
        if (external->bytesToWrite() > 0)
            return;
        QMetaObject::invokeMethod(this, "updateExternal", Qt::QueuedConnection);
        break;
    case State_Initialize:
    case State_WaitForStarted:
    case State_Completed:
    case State_RecieveExternalAuthorization:
    case State_WaitForConnected:
    case State_SendExternalAuthorization:
    case State_AuthorizeExternal:
    case State_Connected:
    case State_Resume:
        break;
    }
}

#endif

}


Server::Server(QObject *parent) : QObject(parent),
                                  state(State_Initialize),
                                  server(NULL),
                                  configuration(),
                                  incomingConnections(),
                                  connections(),
                                  reapingConnections(),
                                  possibleDigestValid(0),
                                  possibleDigests()
{ }

Server::~Server()
{
    qDeleteAll(incomingConnections);
    qDeleteAll(connections);
    if (server)
        delete server;
}

bool Server::start()
{
    switch (state) {
    case State_Initialize:
        break;
    case State_Shutdown:
    case State_Finished:
    case State_Run:
        return false;
    }

    qRegisterMetaType<QAbstractSocket::SocketState>("QAbstractSocket::SocketState");
    qRegisterMetaType<QLocalSocket::LocalSocketState>("QLocalSocket::LocalSocketState");
    qRegisterMetaType<QSslSocket::SslMode>("QSslSocket::SslMode");
    qRegisterMetaType<QProcess::ExitStatus>("QProcess::ExitStatus");
    qRegisterMetaType<QProcess::ProcessState>("QProcess::ProcessState");

    {
        double now = Time::time();
        auto confList = ValueSegment::Stream::read(
                Archive::Selection(now, now + 1.0, {}, {"configuration"}, {"synchronize"}));
        auto f = Range::findIntersecting(confList, now);
        if (f != confList.cend())
            configuration = f->getValue().hash("Server");
    }
    configuration.detachFromRoot();

    server = new SSLServer(configuration, this);
    Q_ASSERT(server);
    connect(server, SIGNAL(newConnection()), this, SLOT(incomingConnection()),
            Qt::QueuedConnection);

    qint64 port = configuration.hash("Port").toInt64();
    if (!INTEGER::defined(port) || port < 0)
        port = 14231;

    bool ok = false;
    if (configuration.hash("ListenAddress").exists()) {
        ok = server->listen(QHostAddress(configuration.hash("ListenAddress").toQString()),
                            (quint16) port);
    } else {
        ok = server->listen(QHostAddress::Any, (quint16) port);
    }

    if (!ok) {
        qCDebug(log_sync_server) << "Failed to listen for connections:" << server->errorString();
        delete server;
        server = nullptr;
        state = State_Finished;
        emit finished();
        return false;
    }

    qCInfo(log_sync_server) << "Listening for connections using"
                            << server->serverAddress().toString() << "port" << server->serverPort();
    QMetaObject::invokeMethod(this, "incomingConnection", Qt::QueuedConnection);

    state = State_Run;
    return true;
}

void Server::incomingConnection()
{
    switch (state) {
    case State_Initialize:
    case State_Shutdown:
    case State_Finished:
        return;
    case State_Run:
        break;
    }

    for (;;) {
        ServerSSLSocket *socket = static_cast<ServerSSLSocket *>(
                server->nextPendingConnection());
        if (socket == 0)
            break;

        qCDebug(log_sync_server) << "Starting connection with" << socket->peerAddress().toString();

        PreliminaryServerConnection *connection = new PreliminaryServerConnection(socket, this);
        connect(connection, SIGNAL(ready()), this, SLOT(promoteIncoming()), Qt::QueuedConnection);
        connect(connection, SIGNAL(aborted()), this, SLOT(reapIncoming()), Qt::QueuedConnection);
        incomingConnections.append(connection);
    }

    QMetaObject::invokeMethod(this, "promoteIncoming", Qt::QueuedConnection);
    QMetaObject::invokeMethod(this, "reapIncoming", Qt::QueuedConnection);
}

void Server::signalTerminate(bool closeConnections)
{
    switch (state) {
    case State_Initialize:
        state = State_Finished;
        emit finished();
        return;
    case State_Shutdown:
    case State_Finished:
        return;
    case State_Run:
        state = State_Shutdown;
        break;
    }

    qCDebug(log_sync_server) << "Starting shutdown";

    if (closeConnections) {
        for (QList<PreliminaryServerConnection *>::const_iterator
                c = incomingConnections.constBegin(), end = incomingConnections.constEnd();
                c != end;
                ++c) {
            (*c)->signalTerminate();
        }
        for (QHash<QByteArray, ServerConnection *>::iterator c = connections.begin(),
                endC = connections.end(); c != endC; ++c) {
            c.value()->signalTerminate();
        }
    }

    checkFinished();
}

SequenceName::Component Server::loadAuthorized(const QByteArray &digest)
{
    std::string digestString = digest.toHex().toLower().toStdString();
    if (digestString.empty())
        return {};

    if (Time::time() - possibleDigestValid < 30.0) {
        /* Reject it right now without an archive read if we can (no
         * specific digest for it, no unauthorized and no
         * default) */
        if (possibleDigests.count(digestString) == 0 && possibleDigests.count(std::string()) == 0) {
            return {};
        }
    }

    SequenceName::ComponentSet allStations;
    SequenceSegment::Transfer confList;
    {
        Archive::Access access;
        Archive::Access::ReadLock lock(access);
        allStations = access.availableStations();
        confList = SequenceSegment::Stream::read(
                Archive::Selection(FP::undefined(), FP::undefined(), {}, {"configuration"},
                                   {"synchronize"}), &access);
    }

    SequenceName::Component station;
    possibleDigests.clear();
    for (auto seg = confList.rbegin(), endSeg = confList.rend(); seg != endSeg; ++seg) {
        for (const auto &check : allStations) {
            if (check.empty())
                continue;
            if (check == "_")
                continue;
            SequenceName configUnit(check, "configuration", "synchronize");
            auto stationDigests = seg->takeValue(configUnit).read().hash("Authorization").toHash();

            Util::merge(stationDigests.keys(), possibleDigests);
            if (stationDigests[std::string()].exists())
                possibleDigests.insert(std::string());

            if (station.empty() && stationDigests[digestString].exists()) {
                station = check;
            }
        }
    }

    possibleDigestValid = Time::time();
    return station;
}


void Server::reapIncoming()
{
    for (QList<PreliminaryServerConnection *>::iterator check = incomingConnections.begin();
            check != incomingConnections.end();) {
        if (!(*check)->isAborted()) {
            ++check;
            continue;
        }

        delete *check;
        check = incomingConnections.erase(check);
    }
    checkFinished();
}

void Server::reapConnections()
{
    for (QHash<QByteArray, ServerConnection *>::iterator check = connections.begin();
            check != connections.end();) {
        if (!check.value()->isCompleted()) {
            ++check;
            continue;
        }

        delete check.value();
        check = connections.erase(check);
    }
    for (QList<Internal::ServerConnection *>::iterator check = reapingConnections.begin();
            check != reapingConnections.end();) {
        if (!(*check)->isCompleted()) {
            ++check;
            continue;
        }

        delete *check;
        check = reapingConnections.erase(check);
    }
    checkFinished();
}

void Server::checkFinished()
{
    switch (state) {
    case State_Initialize:
    case State_Run:
    case State_Finished:
        return;
    case State_Shutdown:
        break;
    }

    if (!incomingConnections.isEmpty())
        return;
    if (!connections.isEmpty())
        return;
    if (!reapingConnections.isEmpty())
        return;

    state = State_Finished;
    emit finished();
}

void Server::promoteIncoming()
{
    for (QList<PreliminaryServerConnection *>::iterator check = incomingConnections.begin();
            check != incomingConnections.end();) {
        if (!(*check)->isReady()) {
            ++check;
            continue;
        }

        ServerConnectionID id((*check)->id());
        ServerConnection *connection = (*check)->createConnection();
        if (!connection) {
            QHash<QByteArray, ServerConnection *>::iterator target = connections.find(id.digest);
            if (target == connections.end()) {
                qCDebug(log_sync_server) << "Can't find connection" << id << "for resume";
                delete *check;
                check = incomingConnections.erase(check);
                continue;
            }
            QSslSocket *socket = (*check)->takeSocket();
            target.value()->reattach(socket);
        } else {
            QHash<QByteArray, ServerConnection *>::iterator target = connections.find(id.digest);
            if (target != connections.end()) {
                qCDebug(log_sync_server) << "Removing existing connection for" << id;
                target.value()->signalTerminate();
                /* This may have a long wait if it's blocked on archive 
                 * access, so accept the connection but reap the old
                 * one later */
                reapingConnections.append(target.value());
                target.value() = connection;
            } else {
                target = connections.insert(id.digest, connection);
            }

            QSslSocket *socket = (*check)->takeSocket();
            target.value()->attach(socket);

            connect(connection, SIGNAL(completed()), this, SLOT(reapConnections()),
                    Qt::QueuedConnection);
        }

        delete *check;
        check = incomingConnections.erase(check);
    }

    QMetaObject::invokeMethod(this, "reapConnections", Qt::QueuedConnection);
}


}
}
