/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cstring>
#include <QtEndian>
#include <QLoggingCategory>

#include "sync/peer.hxx"
#include "core/waitutils.hxx"


Q_LOGGING_CATEGORY(log_sync_peer, "cpd3.sync.peer", QtWarningMsg)

Q_LOGGING_CATEGORY(log_sync_peer_checkpoint, "cpd3.sync.peer.checkpoint", QtWarningMsg)


using namespace CPD3::Data;

static const int serializeChunkLimit = 16777216;
static const int pendingOutputSizeLimit = 16777216;
static const quint64 checkpointSizeLimit = 262144;
static const int checkpointOutstandingLimit = 64;

namespace CPD3 {
namespace Sync {

/** @file sync/peer.hxx
 * Synchronization peer interfaces.
 */

enum {
    /* A heartbeat sent periodically for connection keep alive. */
            Code_Heartbeat = 0,

    /* Define the next unit in the sequence.  The data that follows is
     * the serialization of the unit. */
            Code_DefineUnit,

    /* Indicate the start of a checkpoint.  This is followed by a uint64
     * ID. */
            Code_Checkpoint_Begin,

    /* Indicate that the remote writer has completed the checkpoint and
     * all information about it can be dropped by the reader.  This is 
     * followed by a uint64 ID. */
            Code_Checkpoint_Acknowledge,

    /* One or more archive values of the same time range.  The data that
     * follows is the start (double), end (double), priority (int32),
     * [ name (uint16), modified (double), value serialization ] * N */
            Code_ArchiveValue,

    /* One or more archive erasures of the same time range, The data that follows
     * is the start (double), end (double), priority (int32), 
     * [ name (uint16), modified (double) ] * N */
            Code_ArchiveErasure,

    /* End of stream.  No data. */
            Code_Completed,

    /* Peer has finished processing. */
            Code_PeerCompleted,
};

enum {
    Version_V1 = 0x512CFF03,
};

static uchar *prepareTargetBuffer(QByteArray &target, int toAdd)
{
    int offset = target.size();
    target.resize(offset + toAdd);
    return (uchar *) target.data() + offset;
}

static void queueSizedData(QByteArray &target, const QByteArray &data)
{
    quint32 sz = (quint32) data.size();
    if (sz < 0xFFFF) {
        uchar *p = prepareTargetBuffer(target, 2 + (int) sz);
        qToLittleEndian<quint16>((quint16) sz, p);
        p += 2;
        memcpy(p, data.data(), sz);
        return;
    }

    uchar *p = prepareTargetBuffer(target, 6 + (int) sz);
    *p = 0xFF;
    ++p;
    *p = 0xFF;
    ++p;
    qToLittleEndian<quint32>(sz, p);
    p += 4;
    memcpy(p, data.data(), sz);
}

static bool sizedDataReady(const QByteArray &source, QByteArray &target, int &offset, int add = 0)
{
    int available = source.size() - (offset + add);
    if (available < 2)
        return false;
    const uchar *p = (const uchar *) source.data() + (offset + add);
    quint16 sizeA = qFromLittleEndian<quint16>(p);
    available -= 2;
    if (sizeA != 0xFFFF) {
        if (available < (int) sizeA)
            return false;
        offset += 2 + add;
        target = QByteArray::fromRawData(source.data() + offset, sizeA);
        offset += sizeA;
        return true;
    }
    if (available < 4)
        return false;
    p += 2;
    quint32 sizeB = qFromLittleEndian<quint32>(p);
    available -= 4;
    if (available < (int) sizeB)
        return false;
    offset += 6 + add;
    target = QByteArray::fromRawData(source.data() + offset, sizeB);
    offset += sizeB;
    return true;
}

static bool canRead(const QByteArray &target, int size, int offset)
{
    int available = target.size() - offset;
    return available >= size;
}

static void prepareStream(QDataStream &stream)
{
    stream.setVersion(QDataStream::Qt_4_5);
    stream.setByteOrder(QDataStream::LittleEndian);
}

Peer::Peer(Filter *local,
           Filter *remote,
           PeerMode mode,
           double modifiedTime,
           std::unique_ptr<Archive::Access> &&readA,
           std::unique_ptr<Archive::Access> &&writeA,
           QObject *parent) : QObject(parent),
                              localFilter(local),
                              remoteFilter(remote),
                              modifiedAfter(modifiedTime),
                              readAccess(std::move(readA)),
                              readOperation(),
                              writeAccess(std::move(writeA)),
                              synchronizeOperation(),
                              referenceOperation(),
                              readSink(*this),
                              outputState(Output_Initialize),
                              outputBuffer(),
                              inputState(Input_Version),
                              inputBuffer(),
                              inError(Error_None),
                              incomingPriorCheckpoint(),
                              incomingActiveCheckpoint(),
                              outgoingPendingValues(),
                              outgoingPendingErasures(),
                              outgoingUnitLookup(),
                              unacknowledgedCheckpoints(),
                              outgoingBeginCheckpoint(),
                              outgoingCheckpoint(),
                              outgoingCheckpointSize(0),
                              outgoingResumeCheckpointIndex(0),
                              singleEnded(false),
                              heartbeatTimer(this),
                              byteTrackingTimer(this),
                              byteTrackingTime(Time::time()),
                              bytesSinceLastTime(0)
{

    outgoingCheckpoint.id = 1;

    switch (mode) {
    case Mode_Bidirectional:
        if (!readAccess)
            readAccess.reset(new Archive::Access);
        if (!writeAccess)
            writeAccess.reset(new Archive::Access);
        break;
    case Mode_Write:
        singleEnded = true;

        if (!readAccess)
            readAccess.reset(new Archive::Access);

        /* Need the writer for network flagging */
        if (!writeAccess)
            writeAccess.reset(new Archive::Access);
        inputState = Input_Completed;
        break;
    case Mode_Read:
        singleEnded = true;

        if (!writeAccess)
            writeAccess.reset(new Archive::Access);

        readAccess.reset();

        localFilter.reset();
        outputState = Output_Completed;
        break;
    }


    readSink.updated.connect(this, "writeAvailable");

    heartbeatTimer.setSingleShot(false);
    heartbeatTimer.setInterval(10000);
    connect(&heartbeatTimer, SIGNAL(timeout()), this, SLOT(sendHeartbeat()));

    byteTrackingTimer.setSingleShot(false);
    byteTrackingTimer.setInterval(1000);
    connect(&byteTrackingTimer, SIGNAL(timeout()), this, SLOT(updateByteRate()));
}

Peer::~Peer()
{
    if (synchronizeOperation) {
        synchronizeOperation->signalTerminate();
        synchronizeOperation->wait();
        synchronizeOperation.reset();
    }
    if (referenceOperation) {
        referenceOperation->signalTerminate();
        referenceOperation->wait();
        referenceOperation.reset();
    }

    if (readOperation) {
        readOperation->signalTerminate();
        readOperation->wait();
        readOperation.reset();
    }

    if (writeAccess) {
        writeAccess->signalTerminate();
        writeAccess->waitForLocks();
        writeAccess.reset();
    }
    if (readAccess) {
        readAccess->signalTerminate();
        readAccess->waitForLocks();
        readAccess.reset();
    }
}

void Peer::inputFault()
{
    switch (inputState) {
    case Input_Version:
    case Input_Request:
        qCDebug(log_sync_peer)
            << "Unable to recover from read errors before handshake completion, aborting connection";

        feedback.emitFailure(tr("Protocol error"));

        inputState = Input_Completed;
        inError = Error_Fatal;
        break;
    case Input_Processing:
    case Input_Resuming:
        if (singleEnded) {
            qCDebug(log_sync_peer)
                << "Unable to recover from read errors without a bidirectional connection, ending processing";

            feedback.emitFailure(tr("Invalid data"));

            inputState = Input_Completed;
            inError = Error_Fatal;
        } else {
            inError = Error_Recoverable;
        }
        break;
    case Input_Completed:
        break;
    }

    emit errorDetected();
    checkFinished();
}

void Peer::inputCompleted()
{
    Q_ASSERT(inputState != Input_Completed);

    qCDebug(log_sync_peer) << "Received incoming data completion";

    Q_ASSERT(incomingActiveCheckpoint.values.empty());
    Q_ASSERT(incomingActiveCheckpoint.erasures.empty());
    if (synchronizeOperation)
        synchronizeOperation->endStream();
    inputState = Input_Completed;

    switch (outputState) {
    case Output_Initialize:
    case Output_RequestFlush:
    case Output_RequestWait:
    case Output_Sending:
    case Output_Resuming:
        feedback.emitStage(tr("Sending data"),
                           tr("Synchronization data are being sent to the remote end."), true);
        break;
    case Output_LocalSent:
    case Output_Completed:
        break;
    case Output_Acknowledging:
        outputState = Output_LocalSent;
        break;
    }

    checkFinished();
}

void Peer::beginLocalRead(const Archive::Selection::List &selections)
{
    Q_ASSERT(!singleEnded);

    if (outputState != Output_RequestWait) {
        qCDebug(log_sync_peer)
            << "Remote end requested a local read while the writer can't accept it";
        inputFault();
        return;
    }

    if (selections.empty()) {
        qCDebug(log_sync_peer) << "No selections to read, ending writer";
        outputCompleted();
        return;
    }

    if (!readAccess) {
        qCDebug(log_sync_peer) << "No local archive, ending writer";
        outputCompleted();
        return;
    }
    Q_ASSERT(writeAccess);
    Q_ASSERT(!readOperation);
    Q_ASSERT(!referenceOperation);

    outputState = Output_Sending;

    referenceOperation = writeAccess->writeRemoteReferenced();;
    referenceOperation->complete.connect(this, "checkFinished");

    readOperation = readAccess->readErasure(selections, &readSink);
    readOperation->complete.connect(this, "checkFinished");

    checkFinished();
}

void Peer::outputCompleted()
{
    qCDebug(log_sync_peer) << "Output completed";

    if (referenceOperation)
        referenceOperation->endData();

    *(prepareTargetBuffer(outputBuffer, 1)) = Code_Completed;

    switch (inputState) {
    case Input_Version:
    case Input_Request:
    case Input_Processing:
    case Input_Resuming:
        outputState = Output_Acknowledging;
        break;
    case Input_Completed:
        if (outputState != Output_Completed)
            outputState = Output_LocalSent;
        break;
    }

    emit writeAvailable();
    emit flushRequested();

    checkFinished();
}

bool Peer::processIncoming(ArchiveValue &&value)
{
    if (inputState == Input_Resuming)
        return true;

    if (remoteFilter) {
        /* Can only advance when single ended (no rollback possible) */
        if (singleEnded && FP::defined(value.getStart()))
            remoteFilter->advance(value.getStart());
        if (!remoteFilter->accept(value))
            return true;
    }

    /* Safe for resume because the prior checkpoint will have been restored */
    if (Range::compareStart(incomingActiveCheckpoint.latestTime, value.getStart()) > 0) {
        qCDebug(log_sync_peer) << "Value stream start time is not ascending";
        inputFault();
        return false;
    }
    incomingActiveCheckpoint.latestTime = value.getStart();

    /* It's ok to write even if we're not single ended, since all it does
     * it make us repeat work on a checkpoint rollback and we allow unsorted in
     * bidirectional mode. */
    Q_ASSERT(synchronizeOperation);
    synchronizeOperation->incomingValue(std::move(value));
    return true;
}

bool Peer::processIncoming(ArchiveErasure &&value)
{
    if (inputState == Input_Resuming)
        return true;

    if (remoteFilter) {
        /* Can only advance when single ended (no rollback possible) */
        if (singleEnded && FP::defined(value.getStart()))
            remoteFilter->advance(value.getStart());
        if (!remoteFilter->accept(value))
            return true;
    }

    /* Safe for resume because the prior checkpoint will have been restored */
    if (Range::compareStart(incomingActiveCheckpoint.latestTime, value.getStart()) > 0) {
        qCDebug(log_sync_peer) << "Deleted stream start time is not ascending";
        inputFault();
        return false;
    }
    incomingActiveCheckpoint.latestTime = value.getStart();

    /* It's ok to write even if we're not single ended, since all it does
     * it make us repeat work on a checkpoint rollback and we allow unsorted in
     * bidirectional mode. */
    Q_ASSERT(synchronizeOperation);
    synchronizeOperation->incomingErasure(std::move(value));
    return true;
}

bool Peer::processRead(int &offset)
{
    switch (inputState) {
    case Input_Version: {
        if (!canRead(inputBuffer, 4, offset))
            return false;
        quint32 check = qFromLittleEndian<quint32>((const uchar *) inputBuffer.data() + offset);
        offset += 4;
        switch (check) {
        case Version_V1:
            break;
        default:
            qCDebug(log_sync_peer) << "Unsupported remote version " << check;
            inputFault();
            return false;
        }
        if (!singleEnded) {
            inputState = Input_Request;

            feedback.emitStage(tr("Handshaking"),
                               tr("The system is waiting for the remote end to request data."),
                               false);
        } else {
            feedback.emitStage(tr("Reading data"), tr("Data are being read."), true);
            inputState = Input_Processing;
        }
        return true;
    }
    case Input_Request: {
        /* Don't process this until the output has flushed */
        if (outputState != Output_RequestWait)
            return false;

        QByteArray buffer;
        if (!sizedDataReady(inputBuffer, buffer, offset))
            return false;
        QDataStream stream(&buffer, QIODevice::ReadOnly);
        prepareStream(stream);

        quint32 n = 0;
        stream >> n;
        Archive::Selection::List selections;
        for (quint32 i = 0; i < n; i++) {
            Archive::Selection sel;
            sel.includeDefaultStation = false;
            sel.includeMetaArchive = false;

            stream >> sel.modifiedAfter >> sel.start >> sel.end >> sel.stations >> sel.archives
                   >> sel.variables >> sel.hasFlavors >> sel.lacksFlavors >> sel.exactFlavors;

            selections.emplace_back(std::move(sel));
        }

        if (stream.status() != QDataStream::Ok) {
            qCDebug(log_sync_peer) << "Selection stream read error";
            inputFault();
            return false;
        }

        qCDebug(log_sync_peer) << "Starting requested read of " << selections.size()
                               << " selection(s)";
        beginLocalRead(selections);

        inputState = Input_Processing;
        feedback.emitStage(tr("Receiving data"), tr("Data are being received from the remote end."),
                           true);
        return true;
    }
    case Input_Completed:
    case Input_Processing:
    case Input_Resuming:
        break;
    }

    switch (inputBuffer.at(offset)) {
    case Code_Completed:
        if (inputState == Input_Completed) {
            qCDebug(log_sync_peer) << "Received duplicate input completion";
            inputFault();
            return false;
        }
        offset++;
        inputCompleted();
        return true;
    case Code_PeerCompleted:
        if (outputState == Output_LocalSent) {
            qCDebug(log_sync_peer) << "Received peer work done completion";
            outputState = Output_Completed;
            checkFinished();
        }
        offset++;
        return true;
    case Code_Heartbeat:
        offset++;
        return true;
    case Code_Checkpoint_Begin: {
        if (inputState == Input_Completed) {
            qCDebug(log_sync_peer) << "Received checkpoint begin after input completion";
            inputFault();
            return false;
        }
        if (!canRead(inputBuffer, 8, offset + 1))
            return false;
        quint64 id = qFromLittleEndian<quint64>((const uchar *) inputBuffer.data() + (offset + 1));
        offset += 9;

        if (singleEnded) {
            qCDebug(log_sync_peer_checkpoint) << "Disregarding checkpoint" << id
                                              << "in single ended mode";
            return true;
        }

        if (id < incomingActiveCheckpoint.id) {
            qCDebug(log_sync_peer_checkpoint) << "Received outdated checkpoint" << id
                                              << ", current data cleared";

            Q_ASSERT(incomingActiveCheckpoint.values.empty());
            Q_ASSERT(incomingActiveCheckpoint.erasures.empty());
            incomingActiveCheckpoint.unitLookup.clear();
            inputState = Input_Resuming;
        } else if (id == incomingActiveCheckpoint.id) {
            qCDebug(log_sync_peer) << "Received identical checkpoint " << id
                                   << ", setting lookups and resuming";

            incomingActiveCheckpoint = incomingPriorCheckpoint;
            incomingActiveCheckpoint.id = id;
            Q_ASSERT(incomingActiveCheckpoint.values.empty());
            Q_ASSERT(incomingActiveCheckpoint.erasures.empty());
            inputState = Input_Processing;
        } else {
            qCDebug(log_sync_peer_checkpoint) << "Completing checkpoint" << id;

            if (remoteFilter && FP::defined(incomingActiveCheckpoint.latestTime)) {
                remoteFilter->advance(incomingActiveCheckpoint.latestTime);
            }

            Q_ASSERT(!singleEnded);
            Q_ASSERT(outputState != Output_Initialize);
            {
                uchar *target = prepareTargetBuffer(outputBuffer, 9);
                *target = Code_Checkpoint_Acknowledge;
                ++target;
                qToLittleEndian<quint64>(id, target);
                emit writeAvailable();
            }

            incomingPriorCheckpoint = incomingActiveCheckpoint;
            incomingActiveCheckpoint.id = id;
        }
        return true;
    }
    case Code_Checkpoint_Acknowledge: {
        if (!canRead(inputBuffer, 8, offset + 1))
            return false;
        quint64 id = qFromLittleEndian<quint64>((const uchar *) inputBuffer.data() + (offset + 1));
        offset += 9;

        qCDebug(log_sync_peer_checkpoint) << "Checkpoint" << id << "acknowledged";

        while (!unacknowledgedCheckpoints.empty() && unacknowledgedCheckpoints.front().id <= id) {
            unacknowledgedCheckpoints.pop_front();
        }

        emit writeAvailable();
        return true;
    }
    case Code_DefineUnit: {
        if (inputState == Input_Completed) {
            qCDebug(log_sync_peer) << "Received define unit after input completion";
            inputFault();
            return false;
        }

        QByteArray buffer;
        if (!sizedDataReady(inputBuffer, buffer, offset, 1))
            return false;
        QDataStream stream(&buffer, QIODevice::ReadOnly);
        prepareStream(stream);

        std::size_t index = static_cast<std::size_t>(incomingActiveCheckpoint.nextUnit);
        if (index >= incomingActiveCheckpoint.unitLookup.size())
            incomingActiveCheckpoint.unitLookup.push_back(SequenceName());
        Q_ASSERT(incomingActiveCheckpoint.unitLookup.size() > index);

        stream >> incomingActiveCheckpoint.unitLookup[index];
        if (stream.status() != QDataStream::Ok) {
            qCDebug(log_sync_peer) << "Unit stream read error";
            inputFault();
            return false;
        }

        incomingActiveCheckpoint.nextUnit++;
        return true;
    }
    case Code_ArchiveValue: {
        if (inputState == Input_Completed) {
            qCDebug(log_sync_peer) << "Received value after input completion";
            inputFault();
            return false;
        }

        QByteArray buffer;
        if (!sizedDataReady(inputBuffer, buffer, offset, 1))
            return false;
        QDataStream stream(&buffer, QIODevice::ReadOnly);
        prepareStream(stream);

        double start = FP::undefined();
        stream >> start;
        double end = FP::undefined();
        stream >> end;
        qint32 priority = 0;
        stream >> priority;

        while (!stream.atEnd()) {
            quint16 unitId = 0;
            stream >> unitId;
            if (static_cast<std::size_t>(unitId) >= incomingActiveCheckpoint.unitLookup.size()) {
                qCDebug(log_sync_peer) << "Value stream unit read error:" << unitId << "vs"
                                       << incomingActiveCheckpoint.unitLookup.size();
                inputFault();
                return false;
            }

            double modified = FP::undefined();
            stream >> modified;

            Variant::Root v = Variant::Root::deserialize(stream);

            if (!processIncoming(
                    ArchiveValue(incomingActiveCheckpoint.unitLookup[unitId], std::move(v), start,
                                 end, static_cast<int>(priority), modified, true)))
                return false;
        }

        if (stream.status() != QDataStream::Ok) {
            qCDebug(log_sync_peer) << "Value stream read error";
            inputFault();
            return false;
        }
        return true;
    }
    case Code_ArchiveErasure: {
        if (inputState == Input_Completed) {
            qCDebug(log_sync_peer) << "Received erasure after input completion";
            inputFault();
            return false;
        }

        QByteArray buffer;
        if (!sizedDataReady(inputBuffer, buffer, offset, 1))
            return false;
        QDataStream stream(&buffer, QIODevice::ReadOnly);
        prepareStream(stream);

        double start = FP::undefined();
        stream >> start;
        double end = FP::undefined();
        stream >> end;
        qint32 priority = 0;
        stream >> priority;

        while (!stream.atEnd()) {
            quint16 unitId = 0;
            stream >> unitId;
            if (static_cast<std::size_t>(unitId) >= incomingActiveCheckpoint.unitLookup.size()) {
                qCDebug(log_sync_peer) << "Erasure stream unit read error:" << unitId << "vs"
                                       << incomingActiveCheckpoint.unitLookup.size();
                inputFault();
                return false;
            }

            double modified = FP::undefined();
            stream >> modified;

            if (!processIncoming(
                    ArchiveErasure(incomingActiveCheckpoint.unitLookup[unitId], start, end,
                                   static_cast<int>(priority), modified)))
                return false;
        }

        if (stream.status() != QDataStream::Ok) {
            qCDebug(log_sync_peer) << "Erasure stream read error";
            inputFault();
            return false;
        }
        return true;
    }
    default:
        qCDebug(log_sync_peer) << "Unrecognized remote code: " << inputBuffer.at(offset);
        inputFault();
        return false;
    }

    return false;
}

void Peer::emptyRead()
{
    if (inputBuffer.isEmpty())
        return;
    if (inError != Error_None) {
        inputBuffer.clear();
        return;
    }
    int beginOffset = 0;

    for (;;) {
        if (beginOffset >= inputBuffer.size())
            break;
        if (!processRead(beginOffset))
            break;
    }

    if (beginOffset > 0) {
        int remaining = inputBuffer.size() - beginOffset;
        if (remaining <= 0) {
            inputBuffer.clear();
        } else {
            std::memmove(inputBuffer.data(), inputBuffer.data() + beginOffset, remaining);
            inputBuffer.resize(remaining);
        }
    }
}

void Peer::incomingData(const QByteArray &in)
{
    if (inputBuffer.isEmpty())
        inputBuffer = in;
    else
        inputBuffer.append(in);

    if (inputState != Input_Completed) {
        bytesSinceLastTime += in.size();
    }

    emptyRead();
}

void Peer::writeCheckpoint(const PeerCheckpoint &cp)
{
    uchar *target = prepareTargetBuffer(outputBuffer, 9);
    *target = Code_Checkpoint_Begin;
    ++target;
    qToLittleEndian<quint64>(cp.id, target);

    qCDebug(log_sync_peer_checkpoint) << "Finished sending checkpoint" << cp.id;

    emit writeAvailable();
    emit flushRequested();
}

template<typename PendingType, typename Serializer, typename CompletedType = PendingType>
static void serializeAvailable(QByteArray &buffer,
                               PendingType &pending,
                               Serializer serializer,
                               const SequenceName::Map<quint16> &lookup,
                               CompletedType *completed)
{
    QDataStream stream(&buffer, QIODevice::WriteOnly);
    prepareStream(stream);

    auto ul = lookup.find(pending.front().getUnit());
    if (ul == lookup.end())
        return;

    double start = pending.front().getStart();
    double end = pending.front().getEnd();
    qint32 priority = pending.front().getPriority();

    stream << start;
    stream << end;
    stream << priority;

    stream << ul->second;
    serializer(stream, pending.front());

    if (completed != NULL)
        completed->emplace_back(pending.front());
    pending.pop_front();

    for (auto add = pending.begin(); add != pending.end() && buffer.size() < serializeChunkLimit;) {
        if (Range::compareStart(start, add->getStart()) < 0)
            break;
        Q_ASSERT(FP::equal(start, add->getStart()));
        if (add->getPriority() != (int) priority) {
            ++add;
            continue;
        }
        if (Range::compareEnd(end, add->getEnd()) != 0) {
            ++add;
            continue;
        }
        ul = lookup.find(add->getUnit());
        if (ul == lookup.end()) {
            ++add;
            continue;
        }

        stream << ul->second;
        serializer(stream, *add);

        if (completed != NULL)
            completed->emplace_back(*add);
        add = pending.erase(add);
    }
}

static void serializeArchiveValue(QDataStream &stream, const ArchiveValue &value)
{
    stream << value.getModified();
    stream << value.root();
}

static void serializeArchiveErasure(QDataStream &stream, const ArchiveErasure &value)
{
    stream << value.getModified();
}


void Peer::addUnitDefine(QByteArray &buffer, const SequenceName &unit)
{
    QDataStream stream(&buffer, QIODevice::WriteOnly);
    prepareStream(stream);

    quint16 id = outgoingCheckpoint.nextUnit;
    outgoingCheckpoint.nextUnit++;

    if (static_cast<std::size_t>(id) >= outgoingCheckpoint.unitLookup.size()) {
        outgoingCheckpoint.unitLookup.push_back(unit);
        Q_ASSERT(id < outgoingCheckpoint.unitLookup.size());
        outgoingUnitLookup.emplace(unit, id);
    } else {
        outgoingUnitLookup.erase(outgoingCheckpoint.unitLookup[static_cast<std::size_t>(id)]);
        outgoingCheckpoint.unitLookup[static_cast<std::size_t>(id)] = unit;
        outgoingUnitLookup.emplace(unit, id);
    }

    stream << unit;
}

void Peer::requestRemoteData()
{
    Archive::Selection::List selections;
    if (remoteFilter)
        selections = remoteFilter->toArchiveSelection(modifiedAfter);

    QByteArray buffer;
    QDataStream stream(&buffer, QIODevice::WriteOnly);
    prepareStream(stream);

    stream << static_cast<quint32>(selections.size());
    for (const auto &s : selections) {
        stream << s.modifiedAfter << s.start << s.end << s.stations << s.archives << s.variables
               << s.hasFlavors << s.lacksFlavors << s.exactFlavors;
    }

    queueSizedData(outputBuffer, buffer);

    outputState = Output_RequestFlush;
}

void Peer::readAllLocalData()
{
    Archive::Selection::List selections;
    if (localFilter)
        selections = localFilter->toArchiveSelection(modifiedAfter);

    for (auto &sel : selections) {
        sel.includeDefaultStation = false;
        sel.includeMetaArchive = false;
    }

    if (!readAccess) {
        qCDebug(log_sync_peer) << "No local archive, ending writer";
        outputCompleted();
        return;
    }
    if (selections.empty()) {
        qCDebug(log_sync_peer) << "No local data, ending writer";
        outputCompleted();
        return;
    }
    Q_ASSERT(writeAccess);
    Q_ASSERT(!readOperation);
    Q_ASSERT(!referenceOperation);

    qCDebug(log_sync_peer) << "Starting local read of" << selections.size() << "selection(s)";

    outputState = Output_Sending;

    referenceOperation = writeAccess->writeRemoteReferenced();
    referenceOperation->complete.connect(this, "checkFinished");

    readOperation = readAccess->readErasure(selections, &readSink);
    readOperation->complete.connect(this, "checkFinished");

    checkFinished();
}

void Peer::fillWriteBuffer()
{
    for (;;) {
        switch (outputState) {
        case Output_Initialize: {
            {
                uchar *p = prepareTargetBuffer(outputBuffer, 4);
                qToLittleEndian<quint32>((quint32) Version_V1, p);
            }

            if (!singleEnded) {
                requestRemoteData();
            } else {
                feedback.emitStage(tr("Writing data"), tr("Synchronization data are being output."),
                                   true);
                readAllLocalData();
            }

            emit writeAvailable();
            emit flushRequested();
            continue;
        }
        case Output_RequestWait:
        case Output_RequestFlush:
        case Output_LocalSent:
        case Output_Completed:
        case Output_Acknowledging:
            return;
        case Output_Sending:
        case Output_Resuming:
            break;
        }

        if (outputState != Output_Resuming &&
                !singleEnded &&
                outgoingCheckpointSize > checkpointSizeLimit) {
            writeCheckpoint(outgoingCheckpoint);
            unacknowledgedCheckpoints.push_back(outgoingCheckpoint);
            outgoingCheckpoint.id++;
            outgoingCheckpoint.values.clear();
            outgoingCheckpoint.erasures.clear();
            outgoingCheckpointSize = 0;
            outgoingBeginCheckpoint = outgoingCheckpoint;
        }

        if (outputBuffer.size() > pendingOutputSizeLimit)
            return;

        if (!outgoingPendingValues.empty() || !outgoingPendingErasures.empty()) {
            if (!outgoingPendingValues.empty() &&
                    (outgoingPendingErasures.empty() ||
                            Range::compareStart(outgoingPendingValues.front().getStart(),
                                                outgoingPendingErasures.front().getStart()) <= 0)) {
                QByteArray buffer;
                serializeAvailable(buffer, outgoingPendingValues, serializeArchiveValue,
                                   outgoingUnitLookup,
                                   outputState != Output_Resuming ? &outgoingCheckpoint.values
                                                                  : NULL);

                if (buffer.isEmpty()) {
                    /* No data written, so we know it's a unit add
                     * request */
                    addUnitDefine(buffer, outgoingPendingValues.front().getUnit());
                    *(prepareTargetBuffer(outputBuffer, 1)) = Code_DefineUnit;
                } else {
                    *(prepareTargetBuffer(outputBuffer, 1)) = Code_ArchiveValue;
                }

                queueSizedData(outputBuffer, buffer);
                outgoingCheckpointSize += buffer.size();
            } else {
                Q_ASSERT(!outgoingPendingErasures.empty());

                QByteArray buffer;
                serializeAvailable(buffer, outgoingPendingErasures, serializeArchiveErasure,
                                   outgoingUnitLookup,
                                   outputState != Output_Resuming ? &outgoingCheckpoint.erasures
                                                                  : NULL);

                if (buffer.isEmpty()) {
                    /* No data written, so we know it's a unit add
                     * request */
                    addUnitDefine(buffer, outgoingPendingErasures.front().getUnit());
                    *(prepareTargetBuffer(outputBuffer, 1)) = Code_DefineUnit;
                } else {
                    *(prepareTargetBuffer(outputBuffer, 1)) = Code_ArchiveErasure;
                }

                queueSizedData(outputBuffer, buffer);
                outgoingCheckpointSize += buffer.size();
            }

            continue;
        }

        if (outputState == Output_Resuming) {
            Q_ASSERT(outgoingResumeCheckpointIndex < unacknowledgedCheckpoints.size());

            if (outgoingResumeCheckpointIndex > 0) {
                writeCheckpoint(unacknowledgedCheckpoints[outgoingResumeCheckpointIndex]);
            }
            outgoingResumeCheckpointIndex++;
            if (outgoingResumeCheckpointIndex >= unacknowledgedCheckpoints.size()) {
                outputState = Output_Sending;
                outgoingPendingValues = outgoingCheckpoint.values;
                outgoingPendingErasures = outgoingCheckpoint.erasures;
                outgoingCheckpoint = outgoingBeginCheckpoint;
                Q_ASSERT(outgoingCheckpoint.values.empty());
                Q_ASSERT(outgoingCheckpoint.erasures.empty());
                outgoingCheckpointSize = 0;

                qCDebug(log_sync_peer) << "Resumed final checkpoint " << outgoingCheckpoint.id;
            } else {
                outgoingPendingValues =
                        unacknowledgedCheckpoints[outgoingResumeCheckpointIndex].values;
                outgoingPendingErasures =
                        unacknowledgedCheckpoints[outgoingResumeCheckpointIndex].erasures;
                outgoingCheckpointSize = 0;

                qCDebug(log_sync_peer_checkpoint) << "Resending checkpoint"
                                                  << unacknowledgedCheckpoints[outgoingResumeCheckpointIndex]
                                                          .id;
            }
            continue;
        }

        if (!singleEnded && unacknowledgedCheckpoints.size() >= checkpointOutstandingLimit)
            return;

        /* The reference stall will prevent more incoming data from the archive, so we don't
         * need to check this */

        std::deque<ArchiveValue> chunkValues;
        std::deque<ArchiveErasure> chunkErasure;
        if (readSink.take(chunkValues, chunkErasure) &&
                chunkValues.empty() &&
                chunkErasure.empty()) {
            /* Wait for everything to be acknowledged before we close
             * the output */
            if (!singleEnded && !unacknowledgedCheckpoints.empty())
                return;
            outputCompleted();
            return;
        }

        /* Nothing available yet, so just wait */
        if (chunkValues.empty() && chunkErasure.empty())
            return;

        if (singleEnded || inputState == Input_Completed) {
            if (chunkErasure.empty()) {
                Q_ASSERT(!chunkValues.empty());
                if (FP::defined(chunkValues.front().getStart()))
                    feedback.emitProgressTime(chunkValues.front().getStart());
            } else if (chunkValues.empty()) {
                if (FP::defined(chunkErasure.front().getStart()))
                    feedback.emitProgressTime(chunkErasure.front().getStart());
            } else if (Range::compareStart(chunkValues.front().getStart(),
                                           chunkErasure.front().getStart()) < 0) {
                if (FP::defined(chunkValues.front().getStart()))
                    feedback.emitProgressTime(chunkValues.front().getStart());
            } else {
                if (FP::defined(chunkErasure.front().getStart()))
                    feedback.emitProgressTime(chunkErasure.front().getStart());
            }
        }

        Q_ASSERT(outgoingPendingValues.empty());
        Q_ASSERT(outgoingPendingErasures.empty());
        outgoingPendingValues = std::move(chunkValues);
        outgoingPendingErasures = std::move(chunkErasure);
    }
}

void Peer::sendHeartbeat()
{
    if (!outputBuffer.isEmpty())
        return;
    if (singleEnded)
        return;

    switch (outputState) {
    case Output_Initialize:
    case Output_RequestFlush:
    case Output_RequestWait:
        return;
    case Output_Acknowledging:
    case Output_Sending:
    case Output_Resuming:
    case Output_LocalSent:
    case Output_Completed:
        break;
    }

    *(prepareTargetBuffer(outputBuffer, 1)) = Code_Heartbeat;

    emit writeAvailable();
    emit flushRequested();
}

Peer::WriteResult Peer::writePending(QIODevice *target)
{
    if (!outputBuffer.isEmpty()) {
        qint64 n = target->write(outputBuffer);
        if (n == -1) {
            qCDebug(log_sync_peer) << "Target device encountered an error while writing:"
                                   << target->errorString();
            switch (outputState) {
            case Output_Initialize:
            case Output_RequestFlush:
            case Output_RequestWait:

                feedback.emitFailure(tr("Write error: %1").arg(target->errorString()));

                inError = Error_Fatal;
                emit errorDetected();
                break;
            case Output_Sending:
            case Output_Resuming:
            case Output_Acknowledging:
                inError = Error_Recoverable;
                emit errorDetected();
                break;
            case Output_LocalSent:
            case Output_Completed:
                break;
            }
            return WriteResult_Error;
        }

        if (singleEnded || inputState == Input_Completed) {
            bytesSinceLastTime += n;
        }

        int original = outputBuffer.size();
        if (static_cast<int>(n) >= original) {
            outputBuffer.clear();

            switch (outputState) {
            case Output_Initialize:
            case Output_RequestWait:
            case Output_Sending:
            case Output_Resuming:
            case Output_Acknowledging:
            case Output_LocalSent:
            case Output_Completed:
                break;
            case Output_RequestFlush:
                outputState = Output_RequestWait;
                emptyRead();
                break;
            }
        } else if (n > 0) {
            Q_ASSERT(!outputBuffer.isEmpty());
            int remaining = original - static_cast<int>(n);
            std::memmove(outputBuffer.data(), outputBuffer.data() + n, remaining);
            outputBuffer.resize(remaining);
        }
    }

    switch (outputState) {
    case Output_Initialize:
    case Output_RequestWait:
    case Output_Sending:
    case Output_Resuming:
    case Output_Acknowledging:
    case Output_LocalSent:
        break;
    case Output_RequestFlush:
        outputState = Output_RequestWait;
        emptyRead();
        break;
    case Output_Completed:
        if (outputBuffer.isEmpty()) {
            checkFinished();
            return WriteResult_Completed;
        }
        return WriteResult_Continue;
    }

    fillWriteBuffer();
    if (outputBuffer.isEmpty())
        return WriteResult_Wait;
    return WriteResult_Continue;
}

QString Peer::formatRate(double rate, const QString &units)
{
    if (rate < 10.0) {
        rate = floor(rate * 100.0) / 100.0;
        return tr("%1 %2", "rate base").arg(QString::number(rate, 'f', 2)).arg(units);
    } else if (rate < 100.0) {
        rate = floor(rate * 10.0) / 10.0;
        return tr("%1 %2", "rate base").arg(QString::number(rate, 'f', 1)).arg(units);
    } else {
        rate = floor(rate);
        return tr("%1 %2", "rate base").arg(QString::number(rate, 'f', 0)).arg(units);
    }
}

void Peer::updateByteRate()
{
    switch (inputState) {
    case Input_Version:
    case Input_Request:
        feedback.emitState(QString());
        return;

    case Input_Processing:
    case Input_Resuming:
        if (FP::defined(incomingActiveCheckpoint.latestTime)) {
            feedback.emitProgressTime(incomingActiveCheckpoint.latestTime);
        }
        break;

    case Input_Completed:
        switch (outputState) {
        case Output_Initialize:
        case Output_RequestFlush:
        case Output_RequestWait:
        case Output_Sending:
        case Output_Resuming:
            break;
        case Output_Acknowledging:
        case Output_LocalSent:
        case Output_Completed:
            feedback.emitState(QString());
            return;
        }
        break;
    }

    double now = Time::time();
    double elapsed = now - byteTrackingTime;
    if (elapsed < 0.25)
        return;
    double rate = (double) bytesSinceLastTime / elapsed;
    byteTrackingTime = elapsed;
    bytesSinceLastTime = 0;

    if (rate < 1.0) {
        feedback.emitState(tr("Stalled", "zero byte rate"));
        return;
    }

    if (rate < 9999.0) {
        feedback.emitState(formatRate(rate, tr("B/s")));
        return;
    }
    rate /= 1024.0;

    if (rate < 9999.0) {
        feedback.emitState(formatRate(rate, tr("KiB/s")));
        return;
    }
    rate /= 1024.0;

    if (rate < 9999.0) {
        feedback.emitState(formatRate(rate, tr("MiB/s")));
        return;
    }
    rate /= 1024.0;

    feedback.emitState(formatRate(rate, tr("GiB/s")));
}

void Peer::start()
{
    if (!singleEnded) {
        qCDebug(log_sync_peer) << "Bidirectional peer synchronization started";
        heartbeatTimer.start();
    } else if (outputState != Output_Completed) {
        qCDebug(log_sync_peer) << "Single ended output peer started";
    } else if (inputState != Input_Completed) {
        qCDebug(log_sync_peer) << "Single ended input peer started";
    } else {
        Q_ASSERT(false);
    }

    byteTrackingTime = Time::time();
    bytesSinceLastTime = 0;

    if (inputState != Input_Completed) {
        Q_ASSERT(writeAccess);
        Q_ASSERT(!synchronizeOperation);

        /* Can only do sorted in single ended mode, since otherwise we may have to
         * handle a resume, which will rewind data */
        bool sorted = singleEnded;

        synchronizeOperation = writeAccess->writeSynchronize(true, sorted);
        synchronizeOperation->complete.connect(this, "checkFinished");
        synchronizeOperation->softStallCleared.connect(this, [this] {
            emit incomingStallCleared();
        }, true);
    }

    fillWriteBuffer();
    Q_ASSERT(outputState != Output_Initialize);

    checkFinished();

    byteTrackingTimer.start();
}

void Peer::signalTerminate()
{
    if (readOperation)
        readOperation->signalTerminate();
    if (synchronizeOperation)
        synchronizeOperation->signalTerminate();
    if (referenceOperation)
        referenceOperation->signalTerminate();

    if (readAccess)
        readAccess->signalTerminate();
    if (writeAccess)
        writeAccess->signalTerminate();

    outputState = Output_Completed;
    inputState = Input_Completed;

    outputBuffer.clear();
    inputBuffer.clear();

    checkFinished();
}

bool Peer::transferCompleted() const
{
    switch (outputState) {
    case Output_Initialize:
    case Output_RequestFlush:
    case Output_RequestWait:
    case Output_Sending:
    case Output_Resuming:
    case Output_Acknowledging:
        return false;
    case Output_LocalSent:
        if (singleEnded)
            break;
        return false;
    case Output_Completed:
        break;
    }

    switch (inputState) {
    case Input_Version:
    case Input_Request:
    case Input_Processing:
    case Input_Resuming:
        return false;
    case Input_Completed:
        /* This is actually the check for the output buffer having had the
         * complete notification written to it */
        if (!singleEnded && writeAccess)
            return false;
        break;
    }

    if (!outputBuffer.isEmpty())
        return false;
    return true;
}

bool Peer::isFinished() const
{
    if (!transferCompleted())
        return false;

    if (synchronizeOperation && !synchronizeOperation->wait(0.0))
        return false;
    if (referenceOperation && !referenceOperation->wait(0.0))
        return false;

    /* Don't need to check the reader, since if the write stream is done
     * we can't do anything with it anyway */

    return true;
}

void Peer::reapWriter()
{
    if (singleEnded)
        return;

    switch (inputState) {
    case Input_Version:
    case Input_Request:
    case Input_Processing:
    case Input_Resuming:
        return;
    case Input_Completed:
        break;
    }

    if (!writeAccess)
        return;
    if (synchronizeOperation && !synchronizeOperation->wait(0.0))
        return;
    if (referenceOperation && !referenceOperation->wait(0.0))
        return;
    synchronizeOperation.reset();
    referenceOperation.reset();

    writeAccess->waitForLocks();
    writeAccess.reset();

    qCDebug(log_sync_peer) << "Writer completed, notifying peer";

    *(prepareTargetBuffer(outputBuffer, 1)) = Code_PeerCompleted;
    emit writeAvailable();
    emit flushRequested();
}

void Peer::reapReader()
{
    if (!readAccess)
        return;
    if (!readOperation)
        return;
    if (!readOperation->wait(0.0))
        return;

    readOperation.reset();
    qCDebug(log_sync_peer) << "Reader completed";
}

Peer::ErrorState Peer::errorState() const
{
    return inError;
}

void Peer::checkFinished()
{
    reapReader();
    reapWriter();

    if (!isFinished())
        return;

    if (readOperation) {
        readOperation->signalTerminate();
        readOperation->wait();
    }
    readOperation.reset();

    if (synchronizeOperation)
        synchronizeOperation->wait();
    synchronizeOperation.reset();

    if (referenceOperation)
        referenceOperation->wait();
    referenceOperation.reset();

    if (readAccess) {
        readAccess->waitForLocks();
        readAccess.reset();
    }
    if (writeAccess) {
        writeAccess->waitForLocks();
        writeAccess.reset();
    }

    heartbeatTimer.stop();
    byteTrackingTimer.stop();

    emit finished();
}

void Peer::rebuildSendLookup(const PeerCheckpoint &cp)
{
    outgoingUnitLookup.clear();
    outgoingUnitLookup.reserve(cp.unitLookup.size());
    quint16 i = 0;
    for (auto add = cp.unitLookup.begin(), endAdd = cp.unitLookup.end();
            add != endAdd;
            ++add, ++i) {
        outgoingUnitLookup.emplace(*add, i);
    }
    outgoingCheckpoint.nextUnit = cp.nextUnit;
    outgoingCheckpoint.unitLookup = cp.unitLookup;
}

bool Peer::rollback()
{
    if (inError == Error_Fatal) {
        qCDebug(log_sync_peer) << "Can't roll back with a fatal error";
        return false;
    }

    switch (outputState) {
    case Output_Initialize:
    case Output_RequestFlush:
    case Output_RequestWait:
        qCDebug(log_sync_peer)
            << "Can't roll back before the read request is received from the remote end";
        return false;
    case Output_Sending:
    case Output_Resuming:
        outputState = Output_Resuming;
        outgoingResumeCheckpointIndex = 0;
        if (!unacknowledgedCheckpoints.empty()) {
            qCDebug(log_sync_peer) << "Rolling back send to checkpoint"
                                   << unacknowledgedCheckpoints.front().id;
            outgoingPendingValues = unacknowledgedCheckpoints.front().values;
            outgoingPendingErasures = unacknowledgedCheckpoints.front().erasures;
            rebuildSendLookup(unacknowledgedCheckpoints.front());
        } else {
            qCDebug(log_sync_peer)
                << "No checkpoints outstanding, rollback is only to the start of the active one";
            rebuildSendLookup(outgoingBeginCheckpoint);
        }
        emit writeAvailable();
        break;
    case Output_Acknowledging:
    case Output_LocalSent:
    case Output_Completed:
        break;
    }

    switch (inputState) {
    case Input_Version:
    case Input_Request:
        qCDebug(log_sync_peer) << "Can't roll back before the read request has been fully received";
        return false;
    case Input_Processing:
    case Input_Resuming:
        qCDebug(log_sync_peer) << "Rolling back receive to checkpoint"
                               << incomingActiveCheckpoint.id;
        inputState = Input_Resuming;
        break;
    case Input_Completed:
        break;
    }

    outputBuffer.clear();
    inputBuffer.clear();
    inError = Error_None;
    outgoingCheckpointSize = 0;

    if (inputState != Input_Completed) {
        feedback.emitStage(tr("Receiving data"),
                           tr("Resumed data are being received from the remote end."), true);
    } else if (outputState != Output_Completed) {
        feedback.emitStage(tr("Sending data"), tr("Resumed data are being sent to the remote end."),
                           true);
    }

    return true;
}

bool Peer::shouldStallIncoming()
{
    if (!synchronizeOperation)
        return false;
    return synchronizeOperation->softStalled();
}

QByteArray Peer::getRemoteFilterState() const
{
    if (!remoteFilter)
        return QByteArray();
    return remoteFilter->state();
}


Peer::ReadSink::ReadSink(Peer &peer) : peer(peer), mutex(), values(), erasures(), isEnded(false)
{ }

Peer::ReadSink::~ReadSink() = default;

void Peer::ReadSink::incomingValue(const ArchiveValue &value)
{
    Q_ASSERT(peer.localFilter);
    peer.localFilter->advance(value.getStart());
    if (!peer.localFilter->accept(value))
        return;

    if (!value.isRemoteReferenced()) {
        Q_ASSERT(peer.referenceOperation);
        peer.referenceOperation->incomingValue(value);
    }

    {
        std::lock_guard<std::mutex> lock(mutex);
        values.emplace_back(value);
        if (values.size() != 1)
            return;
    }
    updated();
}

void Peer::ReadSink::incomingValue(ArchiveValue &&value)
{
    Q_ASSERT(peer.localFilter);
    peer.localFilter->advance(value.getStart());
    if (!peer.localFilter->accept(value))
        return;

    if (!value.isRemoteReferenced()) {
        Q_ASSERT(peer.referenceOperation);
        peer.referenceOperation->incomingValue(value);
    }

    {
        std::lock_guard<std::mutex> lock(mutex);
        values.emplace_back(std::move(value));
        if (values.size() != 1)
            return;
    }
    updated();
}

void Peer::ReadSink::incomingErasure(const ArchiveErasure &erasure)
{
    Q_ASSERT(peer.localFilter);
    peer.localFilter->advance(erasure.getStart());
    if (!peer.localFilter->accept(erasure))
        return;

    {
        std::lock_guard<std::mutex> lock(mutex);
        erasures.emplace_back(erasure);
        if (erasures.size() != 1)
            return;
    }
    updated();
}

void Peer::ReadSink::incomingErasure(ArchiveErasure &&erasure)
{
    Q_ASSERT(peer.localFilter);
    peer.localFilter->advance(erasure.getStart());
    if (!peer.localFilter->accept(erasure))
        return;

    {
        std::lock_guard<std::mutex> lock(mutex);
        erasures.emplace_back(std::move(erasure));
        if (erasures.size() != 1)
            return;
    }
    updated();
}

void Peer::ReadSink::endStream()
{
    /* Copy the signal, so it's not destroyed while we use it */
    auto scpy = updated;
    {
        std::lock_guard<std::mutex> lock(mutex);
        isEnded = true;
    }
    scpy();
}

bool Peer::ReadSink::take(std::deque<CPD3::Data::ArchiveValue> &v,
                          std::deque<CPD3::Data::ArchiveErasure> &e)
{
    Q_ASSERT(v.empty());
    Q_ASSERT(e.empty());
    std::lock_guard<std::mutex> lock(mutex);
    v = std::move(values);
    values.clear();
    e = std::move(erasures);
    erasures.clear();
    return isEnded;
}

}
}
