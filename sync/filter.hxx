/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SYNCFILTER_H
#define CPD3SYNCFILTER_H

#include "core/first.hxx"

#include <QtGlobal>

#include "sync/sync.hxx"
#include "datacore/archive/selection.hxx"
#include "datacore/stream.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencefilter.hxx"
#include "datacore/segment.hxx"


namespace CPD3 {
namespace Sync {


/**
 * A synchronization filter.
 */
class CPD3SYNC_EXPORT Filter {
public:
    virtual ~Filter();

    /**
     * Test if a value should be accepted by the filter.  This may be 
     * called for any time after the last advance, not necessarily in
     * strict time ascending order.
     * 
     * @param value     the value to test
     * @return          true if the value passes the filter
     */
    virtual bool accept(const CPD3::Data::ArchiveValue &value) = 0;

    /**
     * Test if a value should be accepted by the filter.  This may be 
     * called for any time after the last advance, not necessarily in
     * strict time ascending order.
     * 
     * @param value     the value to test
     * @return          true if the value passes the filter
     */
    virtual bool accept(const CPD3::Data::ArchiveErasure &value) = 0;

    /**
     * Advance the filter to a given time.  No values with start times
     * before this will be tested.
     * 
     * @param time      the time to advance to
     */
    virtual void advance(double time) = 0;

    /**
     * Get a list of archive selections that would return data that passes
     * the filter.  This is used during data read requesting.
     * 
     * @return          a list of read selections
     */
    virtual CPD3::Data::Archive::Selection::List toArchiveSelection(double modifiedAfter) const = 0;

    virtual QByteArray state() const;
};

/**
 * A synchronization filter based on a static set of configured segments.
 */
class CPD3SYNC_EXPORT BasicFilter : public Filter {
    CPD3::Data::SequenceFilter filter;
    CPD3::Data::SequenceName::Component station;
public:
    /**
     * Create the filter.
     * 
     * @param config            the configuration
     * @param station           the station
     */
    BasicFilter(const CPD3::Data::ValueSegment::Transfer &config,
                const CPD3::Data::SequenceName::Component &station);

    virtual ~BasicFilter();

    bool accept(const CPD3::Data::ArchiveValue &value) override;

    bool accept(const CPD3::Data::ArchiveErasure &value) override;

    void advance(double time) override;

    CPD3::Data::Archive::Selection::List toArchiveSelection(double modifiedAfter) const override;
};

/**
 * A synchronization filter based on a static set of configured segments, that
 * also tracks the modified time of bidirectional elements.
 */
class CPD3SYNC_EXPORT TrackingFilter : public Filter {
    class Tracking : public CPD3::Data::SequenceFilter {
        CPD3::Data::SequenceName::Map<double> latestModified;

        class TrackingData : public CPD3::Data::SequenceFilter::MatcherData {
            Tracking &parent;

            TrackingData(const TrackingData &);

            void integrateModified(const CPD3::Data::SequenceName &name, double modified);

        public:
            TrackingData(Tracking &parent);

            virtual ~TrackingData();

            void passed(const CPD3::Data::SequenceValue &value) override;

            void passed(const CPD3::Data::ArchiveValue &value) override;

            void passed(const CPD3::Data::ArchiveErasure &value) override;

            double overrideModified(double modifiedAfter,
                                    const CPD3::Data::SequenceMatch::Element &matcher) override;

            MatcherData *clone() override;
        };

        friend class Data;

        friend class TrackingFilter;

    public:
        Tracking();

        virtual ~Tracking();

    protected:
        std::unique_ptr<
                MatcherData> createMatchData(const CPD3::Data::Variant::Read &config) override;
    };

    Tracking filter;
    CPD3::Data::SequenceName::Component station;
public:
    /**
     * Create the filter.
     *
     * @param config            the configuration
     * @param station           the station
     * @param state             the state data to restore
     */
    TrackingFilter(const CPD3::Data::ValueSegment::Transfer &config,
                   const CPD3::Data::SequenceName::Component &station,
                   const QByteArray &state = QByteArray());

    virtual ~TrackingFilter();

    bool accept(const CPD3::Data::ArchiveValue &value) override;

    bool accept(const CPD3::Data::ArchiveErasure &value) override;

    void advance(double time) override;

    CPD3::Data::Archive::Selection::List toArchiveSelection(double modifiedAfter) const override;

    /**
     * Generate state data to save.
     *
     * @return      the state data
     */
    virtual QByteArray state() const;
};

}
}

#endif
