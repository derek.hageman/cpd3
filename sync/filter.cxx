/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "sync/filter.hxx"


Q_LOGGING_CATEGORY(log_sync_filter, "cpd3.sync.filter", QtWarningMsg)


using namespace CPD3::Data;

namespace CPD3 {
namespace Sync {

/** @file sync/filter.hxx
 * Synchronization filter interfaces.
 */

Filter::~Filter() = default;

QByteArray Filter::state() const
{ return QByteArray(); }


BasicFilter::BasicFilter(const ValueSegment::Transfer &config,
                         const SequenceName::Component &station) : filter(), station(station)
{
    filter.configure(config, {QString::fromStdString(station)});
    filter.setDefaultAccept(false);
}

BasicFilter::~BasicFilter() = default;

bool BasicFilter::accept(const ArchiveValue &value)
{ return filter.accept(value); }

bool BasicFilter::accept(const ArchiveErasure &value)
{ return filter.accept(value); }

void BasicFilter::advance(double time)
{ filter.advance(time); }

Archive::Selection::List BasicFilter::toArchiveSelection(double modifiedAfter) const
{ return filter.toArchiveSelection({station}, {}, {}, modifiedAfter); }


TrackingFilter::Tracking::Tracking() = default;

TrackingFilter::Tracking::~Tracking() = default;

std::unique_ptr<
        SequenceFilter::MatcherData> TrackingFilter::Tracking::createMatchData(const Variant::Read &config)
{
    if (!config["Bidirectional"].toBool())
        return std::unique_ptr<SequenceFilter::MatcherData>();
    return std::unique_ptr<SequenceFilter::MatcherData>(new TrackingData(*this));
}

TrackingFilter::Tracking::TrackingData::TrackingData(TrackingFilter::Tracking &parent) : parent(
        parent)
{ }

TrackingFilter::Tracking::TrackingData::~TrackingData() = default;

TrackingFilter::Tracking::TrackingData::TrackingData(const TrackingData &) = default;

SequenceFilter::MatcherData *TrackingFilter::Tracking::TrackingData::clone()
{ return new TrackingData(*this); }

void TrackingFilter::Tracking::TrackingData::integrateModified(const CPD3::Data::SequenceName &name,
                                                               double modified)
{
    if (!FP::defined(modified))
        return;

    auto check = parent.latestModified.find(name);
    if (check != parent.latestModified.end()) {
        Q_ASSERT(FP::defined(check->second));
        if (modified >= check->second)
            check->second = modified;
        return;
    }

    parent.latestModified.emplace(name, modified);
}

void TrackingFilter::Tracking::TrackingData::passed(const CPD3::Data::SequenceValue &)
{ }

void TrackingFilter::Tracking::TrackingData::passed(const CPD3::Data::ArchiveValue &value)
{ integrateModified(value.getUnit(), value.getModified()); }

void TrackingFilter::Tracking::TrackingData::passed(const CPD3::Data::ArchiveErasure &value)
{ integrateModified(value.getUnit(), value.getModified()); }

double TrackingFilter::Tracking::TrackingData::overrideModified(double modifiedAfter,
                                                                const CPD3::Data::SequenceMatch::Element &matcher)
{
    if (!FP::defined(modifiedAfter))
        return modifiedAfter;

    for (const auto &check : parent.latestModified) {
        if (!matcher.matches(check.first))
            continue;

        Q_ASSERT(FP::defined(check.second));

        if (check.second < modifiedAfter)
            modifiedAfter = check.second;
    }

    return modifiedAfter;
}

static constexpr qint8 trackingFilterStateVersion = 1;

TrackingFilter::TrackingFilter(const CPD3::Data::ValueSegment::Transfer &config,
                               const SequenceName::Component &station,
                               const QByteArray &state)
{
    filter.configure(config, {QString::fromStdString(station)});
    filter.setDefaultAccept(false);

    if (!state.isEmpty()) {
        QDataStream stream(const_cast<QByteArray *>(&state), QIODevice::ReadOnly);
        quint8 version = 0;
        stream >> version;

        if (stream.status() != QDataStream::Ok || version != trackingFilterStateVersion) {
            qCDebug(log_sync_filter) << "Invalid state data";
        } else {
            stream >> filter.latestModified;
        }
    }
}

QByteArray TrackingFilter::state() const
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::WriteOnly);
    stream << trackingFilterStateVersion;

    stream << filter.latestModified;

    return data;
}

TrackingFilter::~TrackingFilter() = default;

bool TrackingFilter::accept(const CPD3::Data::ArchiveValue &value)
{ return filter.accept(value); }

bool TrackingFilter::accept(const CPD3::Data::ArchiveErasure &value)
{ return filter.accept(value); }

void TrackingFilter::advance(double time)
{ filter.advance(time); }

CPD3::Data::Archive::Selection::List TrackingFilter::toArchiveSelection(double modifiedAfter) const
{ return filter.toArchiveSelection({station}, {}, {}, modifiedAfter); }

}
}
