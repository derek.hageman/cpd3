/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SYNCCLIENT_H
#define CPD3SYNCCLIENT_H

#include "core/first.hxx"

#include <memory>
#include <mutex>
#include <QtGlobal>
#include <QSslSocket>
#include <QString>
#include <QByteArray>
#include <QTimer>

#include "sync/sync.hxx"
#include "core/actioncomponent.hxx"
#include "core/compression.hxx"
#include "database/runlock.hxx"
#include "sync/peer.hxx"
#include "sync/filter.hxx"


namespace CPD3 {
namespace Sync {

class Client;

namespace Internal {
/**
 * The main synchronization client implementation.
 */
class ClientMain : public QObject {
Q_OBJECT

    Client *thread;

    enum {
        /* Initialization, no connection started */
                State_Initialize,
        /* Connecting, waiting for TLS handshake */
                State_Connecting,
        /* Connected, sending the new connection request */
                State_Send_NewConnection,
        /* Connected and awaiting the resume ID from the server */
                State_Receive_ResumeID,

        /* Resuming connection, waiting for TLS handshake */
                State_Reconnecting,
        /* Resuming connection, sending the resume request */
                State_Send_ResumeConnection,

        /* Connected and transferring data with the peer */
                State_Connected,

        /* Waiting after a reconnection failure */
                State_Reconnection_Wait,
        /* Waiting for all data to be written after peer completion */
                State_Closing,
        /* Synchronization completed or failed */
                State_Completed,
    } state;
    QTimer *timeoutTimer;
    QByteArray readBuffer;
    QByteArray writeBuffer;

    QSslSocket *socket;
    CompressedStream *compressor;
    std::unique_ptr<Database::RunLock> lock;
    QString lockKey;
    Peer *peer;

    CPD3::Data::SequenceName::Component station;
    QString authorization;

    std::string host;
    quint16 port;
    CPD3::Data::Variant::Read localSSLConfiguration;

    QByteArray resumeID;

    double retryBeginTime;

    void initializePeer();

    void createSocket();

    void attachSocket();

public:

    ClientMain(Client *thread,
               const CPD3::Data::SequenceName::Component &station,
               const QString &authorization);

    ClientMain(Client *thread,
               const QString &host,
               quint16 port = 14231,
               const CPD3::Data::Variant::Read &ssl = CPD3::Data::Variant::Read::empty());

    ~ClientMain();

    void run();

    ActionFeedback::Source feedback;

private slots:

    void updateSocket();

    void peerWrite();

    void directWrite();

    void disconnectResume();

    void connectionTimeout();

    void flushCompressor();
};
}

/**
 * The main synchronization client implementation.
 */
class CPD3SYNC_EXPORT Client : public CPD3Action {
Q_OBJECT

    std::mutex mutex;
    Internal::ClientMain *main;

    friend class Internal::ClientMain;

public:
    /**
     * Create a client bound to a specific station and authorization.
     * 
     * @param station       the station to synchronize with
     * @param authorization the authorization to use
     */
    Client(const CPD3::Data::SequenceName::Component &station, const QString &authorization);

    /**
     * Create a client that connects to a specific host that automatically
     * determines the authorization based on certificate the peer provides.
     * 
     * @param host      the host to connect to
     * @param port      the port to use
     * @param ssl       the SSL configuration to use
     */
    Client(const QString &host,
           quint16 port = 14231,
           const CPD3::Data::Variant::Read &ssl = CPD3::Data::Variant::Read::empty());

    virtual ~Client();

public slots:

    virtual void signalTerminate();

protected:
    virtual void run();
};

}
}

#endif
