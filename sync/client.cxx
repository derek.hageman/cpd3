/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QCoreApplication>
#include <QHostAddress>
#include <QLoggingCategory>

#include "sync/client.hxx"
#include "algorithms/cryptography.hxx"
#include "datacore/archive/access.hxx"


Q_LOGGING_CATEGORY(log_sync_client, "cpd3.sync.client", QtWarningMsg)

using namespace CPD3::Data;
using namespace CPD3::Algorithms;
using namespace CPD3::Sync::Internal;

namespace CPD3 {
namespace Sync {

/** @file sync/client.hxx
 * Synchronization client implementation.
 */


Client::Client(const SequenceName::Component &station, const QString &authorization)
        : mutex(), main(NULL)
{
    main = new ClientMain(this, station, authorization);
    main->moveToThread(this);
    main->feedback.forward(feedback);
}

Client::Client(const QString &host, quint16 port, const Variant::Read &ssl) : mutex(), main(NULL)
{
    main = new ClientMain(this, host, port, ssl);
    main->moveToThread(this);
    main->feedback.forward(feedback);
}

Client::~Client()
{
    if (main != NULL)
        delete main;
}

void Client::signalTerminate()
{
    quit();
}

void Client::run()
{
    Q_ASSERT(main != NULL);
    main->run();

    {
        std::lock_guard<std::mutex> lock(mutex);
        delete main;
        main = NULL;
    }
    QCoreApplication::processEvents();
}


namespace Internal {

namespace {
class ClientSSLSocket : public QSslSocket {
public:
    ClientSSLSocket(QObject *parent = 0) : QSslSocket(parent)
    { }

    virtual ~ClientSSLSocket()
    { }

    virtual bool atEnd() const
    {
        QAbstractSocket::SocketState s = state();
        return s == QAbstractSocket::UnconnectedState || s == QAbstractSocket::ClosingState;
    }
};
}

static bool readExactly(QIODevice *device, QByteArray &buffer, qint64 required)
{
    if (required <= 0)
        return false;
    qint64 n = device->bytesAvailable();
    if (n > 0) {
        if (buffer.isEmpty()) {
            buffer = device->read((int) qMin(required, n));
        } else {
            qint64 max = required - buffer.size();
            buffer.append(device->read((int) qMin(max, n)));
        }
        if (buffer.size() >= required)
            return false;
        if (device->bytesAvailable() > 0)
            return true;
    }
    return false;
}

ClientMain::ClientMain(Client *thd, const SequenceName::Component &station, const QString &auth)
        : thread(thd),
          state(State_Initialize),
          timeoutTimer(NULL),
          readBuffer(),
          writeBuffer(),
          socket(NULL),
          compressor(NULL),
          lock(),
          lockKey(),
          peer(NULL),
          station(station),
          authorization(auth),
          host(),
          port(0),
          localSSLConfiguration(),
          resumeID(),
          retryBeginTime(FP::undefined())
{
    localSSLConfiguration.detachFromRoot();
}

ClientMain::ClientMain(Client *thd, const QString &h, quint16 p, const Variant::Read &ssl) : thread(
        thd),
                                                                                             state(State_Initialize),
                                                                                             timeoutTimer(
                                                                                                     NULL),
                                                                                             readBuffer(),
                                                                                             writeBuffer(),
                                                                                             socket(NULL),
                                                                                             compressor(
                                                                                                     NULL),
                                                                                             lock(),
                                                                                             peer(NULL),
                                                                                             station(),
                                                                                             authorization(),
                                                                                             host(h.toStdString()),
                                                                                             port(p),
                                                                                             localSSLConfiguration(
                                                                                                     ssl),
                                                                                             resumeID(),
                                                                                             retryBeginTime(
                                                                                                     FP::undefined())
{
    localSSLConfiguration.detachFromRoot();
}

ClientMain::~ClientMain()
{
    if (lock) {
        lock->fail();
        lock.reset();
    }
    if (socket != NULL) {
        socket->abort();
        delete socket;
    }
    if (compressor != NULL) {
        compressor->abort();
        delete compressor;
    }
    if (peer != NULL)
        delete peer;
}

static quint16 toPort(const Variant::Read &v)
{
    qint64 check = v.toInt64();
    if (!INTEGER::defined(check))
        return 0;
    if (check <= 0 && check >= 65536)
        return 0;
    return (quint16) check;
}

static const quint8 rcSynchronizeStateVersion = 1;

void ClientMain::initializePeer()
{
    if (authorization.isEmpty())
        return;


    ValueSegment::Transfer config;

    if (station.empty()) {
        feedback.emitStage(tr("Loading %1").arg(authorization),
                           tr("The system is loading global settings to determine the station for %1.")
                                   .arg(authorization), false);

        SequenceName::ComponentSet allStations;
        SequenceSegment::Transfer confList;
        {
            Archive::Access access;
            Archive::Access::ReadLock lock(access);
            allStations = access.availableStations();
            confList = SequenceSegment::Stream::read(
                    Archive::Selection(FP::undefined(), FP::undefined(), {}, {"configuration"},
                                       {"synchronize"}), &access);
        }

        if (station.empty()) {
            for (auto seg = confList.rbegin(), endSeg = confList.rend(); seg != endSeg; ++seg) {
                for (const auto &check : allStations) {
                    if (check.empty() || check == "_")
                        continue;
                    SequenceName configUnit(check, "configuration", "synchronize");
                    auto stationConfig = seg->getValue(configUnit);
                    if (stationConfig.hash("Authorization").hash(authorization).exists()) {
                        station = check;
                        break;
                    }
                }
            }
        }
        if (!station.empty()) {
            SequenceName configUnit(station, "configuration", "synchronize");
            for (auto &seg : confList) {
                config.emplace_back(seg.getStart(), seg.getEnd(), seg.takeValue(configUnit));
            }
        }
    } else {
        feedback.emitStage(tr("Loading %1").arg(QString::fromStdString(station).toUpper()),
                           tr("The system is acquiring loading the synchronization settings for %1.")
                                   .arg(QString::fromStdString(station).toUpper()), false);
        config = ValueSegment::Stream::read(
                Archive::Selection(FP::undefined(), FP::undefined(), {station}, {"configuration"},
                                   {"synchronize"}));
    }

    if (station.empty()) {
        feedback.emitFailure(tr("No station set"));
        return;
    }

    feedback.emitStage(tr("Lock %1").arg(QString::fromStdString(station).toUpper()),
                       tr("The system is acquiring an exclusive lock for %1.").arg(
                               QString::fromStdString(station).toUpper()), false);

    qCInfo(log_sync_client) << "Locking " << station << "-" << authorization;

    lock.reset(new Database::RunLock);
    bool ok = false;
    lockKey = QString("synchronize %1 %2").arg(QString::fromStdString(station), authorization);
    double modified = lock->acquire(lockKey, 30.0, &ok);
    if (!ok) {
        feedback.emitFailure();
        qCInfo(log_sync_client) << "Synchronize aborted for" << station << "due to lock failure";
        return;
    }

    QString authPath(QString("Authorization/%1").arg(authorization));
    if (!config.empty()) {
        double now = Time::time();
        auto f = Range::findIntersecting(config, now);
        if (f != config.cend()) {
            if (host.empty()) {
                host = f->getValue().getPath(authPath)["Hostname"].toString();
            }
            if (host.empty()) {
                host = f->getValue()["Client/Hostname"].toString();
            }

            if (port == 0) {
                port = toPort(f->getValue().getPath(authPath)["Port"]);
            }
            if (port == 0) {
                port = toPort(f->getValue()["Client/Port"]);
            }

            if (!localSSLConfiguration.exists()) {
                localSSLConfiguration = f->getValue().getPath(authPath)["SSL"];
            }
            if (!localSSLConfiguration.exists()) {
                localSSLConfiguration = f->getValue()["Client/SSL"];
            }
        }
    }
    localSSLConfiguration.detachFromRoot();

    config = ValueSegment::withPath(std::move(config), authPath);

    QByteArray remoteState;
    {
        QByteArray data(lock->get(lockKey));
        if (!data.isEmpty()) {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 version = 0;
            stream >> version;

            if (stream.status() != QDataStream::Ok || version != rcSynchronizeStateVersion) {
                qCDebug(log_sync_client) << "Invalid state data";
            } else {
                stream >> remoteState;
            }
        }
    }

    peer = new Peer(new BasicFilter(ValueSegment::withPath(config, "Local/Synchronize"), station),
                    new TrackingFilter(ValueSegment::withPath(config, "Remote/Synchronize"),
                                       station, remoteState), Peer::Mode_Bidirectional, modified);
    Q_ASSERT(peer);
    connect(peer, SIGNAL(incomingStallCleared()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(peer, SIGNAL(writeAvailable()), this, SLOT(peerWrite()), Qt::QueuedConnection);
    connect(peer, SIGNAL(errorDetected()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(peer, SIGNAL(finished()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(peer, SIGNAL(flushRequested()), this, SLOT(flushCompressor()), Qt::QueuedConnection);

    peer->feedback.forward(feedback);
}

void ClientMain::flushCompressor()
{
    if (compressor == NULL)
        return;
    compressor->flush(0);
}

void ClientMain::createSocket()
{
    switch (state) {
    case State_Initialize:
        state = State_Connecting;
        break;
    case State_Connecting:
    case State_Reconnecting:
    case State_Receive_ResumeID:
    case State_Send_NewConnection:
    case State_Send_ResumeConnection:
    case State_Connected:
    case State_Closing:
    case State_Completed:
        return;
    case State_Reconnection_Wait:
        state = State_Reconnecting;
        break;
    }

    Q_ASSERT(socket == NULL);

    feedback.emitStage(tr("Connect to %1").arg(QString::fromStdString(host)),
                       tr("Initiating connection to %2 on port %1.").arg(port)
                                                                    .arg(QString::fromStdString(
                                                                            host)), false);

    /* Just try and load a global default if we don't have this (yet)
     * so we can present something */
    if (!localSSLConfiguration.exists()) {
        double now = Time::time();
        auto config = ValueSegment::Stream::read(
                Archive::Selection(now, now + 1.0, {"_"}, {"configuration"}, {"synchronize"}));
        if (!config.empty()) {
            auto f = Range::findIntersecting(config, now);
            if (f != config.end()) {
                localSSLConfiguration = f->getValue()["Client/SSL"];
                if (!localSSLConfiguration.exists()) {
                    localSSLConfiguration =
                            f->getValue().hash("Authorization").hash(QString()).hash("SSL");
                }
            }
        }
        localSSLConfiguration.detachFromRoot();
    }

    socket = new ClientSSLSocket;
    if (!Cryptography::setupSocket(socket, localSSLConfiguration)) {
        qCDebug(log_sync_client) << "Failed to setup SSL on socket";
    }

    connect(socket, SIGNAL(encrypted()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(socket, SIGNAL(modeChanged(QSslSocket::SslMode)), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(socket, SIGNAL(readyRead()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(socket, SIGNAL(readChannelFinished()), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(directWrite()), Qt::QueuedConnection);

    timeoutTimer->start(30000);

    QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);

    socket->connectToHostEncrypted(QString::fromStdString(host), port,
                                   QIODevice::ReadWrite | QIODevice::Unbuffered);

    qCInfo(log_sync_client) << "Starting connection to" << host << port;
}

void ClientMain::attachSocket()
{
    Q_ASSERT(socket != NULL);
    Q_ASSERT(compressor == NULL);

    qCInfo(log_sync_client) << "Attaching socket" << socket->peerAddress().toString();

    socket->disconnect(this);
    connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(updateSocket()),
            Qt::QueuedConnection);

    connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    compressor = new CompressedStream(socket, this);
    Q_ASSERT(compressor);
    compressor->setMaximumBufferSize(8388608);

    connect(compressor, SIGNAL(readyRead()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(compressor, SIGNAL(readChannelFinished()), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(compressor, SIGNAL(bytesWritten(qint64)), this, SLOT(peerWrite()),
            Qt::QueuedConnection);

    state = State_Connected;

    QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
    QMetaObject::invokeMethod(this, "peerWrite", Qt::QueuedConnection);

    timeoutTimer->start(120 * 1000);
}

void ClientMain::run()
{
    qRegisterMetaType<QAbstractSocket::SocketState>("QAbstractSocket::SocketState");
    qRegisterMetaType<QSslSocket::SslMode>("QSslSocket::SslMode");

    timeoutTimer = new QTimer(this);
    timeoutTimer->setSingleShot(true);
    connect(timeoutTimer, SIGNAL(timeout()), this, SLOT(connectionTimeout()));

    if (host.empty() || port == 0) {
        initializePeer();
    }

    if (host.empty() || port == 0) {
        qCDebug(log_sync_client) << "Unable to determine a host or port to connect to for"
                                 << station << authorization;

        feedback.emitFailure(
                tr("No server defined for %1 %2").arg(QString::fromStdString(station).toUpper(),
                                                      authorization));

        if (lock) {
            lock->fail();
            lock.reset();
        }

        delete timeoutTimer;
        return;
    }

    createSocket();
    Q_ASSERT(socket != NULL);

    thread->exec();

    if (lock) {
        lock->fail();
        lock.reset();
    }

    if (peer != NULL) {
        delete peer;
        peer = NULL;
    }

    if (compressor != NULL) {
        compressor->abort();
        delete compressor;
        compressor = NULL;
    }

    if (socket != NULL) {
        socket->abort();
        delete socket;
        socket = NULL;
    }

    delete timeoutTimer;

    state = State_Completed;
    QCoreApplication::processEvents();
}

void ClientMain::updateSocket()
{
    switch (state) {
    case State_Initialize:
        break;

    case State_Connecting:
        Q_ASSERT(socket != NULL);
        if (socket->state() == QAbstractSocket::UnconnectedState) {
            qCDebug(log_sync_client) << "Socket closed during initial handshake:"
                                     << socket->errorString();

            QString error(socket->errorString());
            if (error.isEmpty())
                error = tr("Connection closed");
            feedback.emitFailure(error);

            socket->deleteLater();
            socket = NULL;
            timeoutTimer->stop();
            state = State_Completed;
            thread->quit();
            return;
        }

        if (!socket->isEncrypted())
            return;

        if (authorization.isEmpty()) {
            QSslCertificate cert(socket->peerCertificate());
            if (cert.isNull()) {
                qCDebug(log_sync_client) << "Received null certificate from"
                                         << socket->peerAddress().toString();

                feedback.emitFailure(tr("No certificate"));

                socket->abort();
                socket->deleteLater();
                socket = NULL;
                timeoutTimer->stop();
                state = State_Completed;
                thread->quit();
                return;
            }
            QByteArray digest(Cryptography::sha512(cert).leftJustified(64, 0, true));
            authorization = digest.toHex().toLower();
        }

        if (peer == NULL) {
            initializePeer();
        }
        if (peer == NULL) {
            qCDebug(log_sync_client) << "No peer able to interface with"
                                     << socket->peerAddress().toString();

            feedback.emitFailure(tr("No peer interface"));

            socket->abort();
            socket->deleteLater();
            socket = NULL;
            timeoutTimer->stop();
            state = State_Completed;
            thread->quit();
            return;
        }

        qCDebug(log_sync_client) << "Established connection to" << socket->peerAddress().toString()
                                 << "on port" << socket->peerPort() << "with authorization"
                                 << authorization << "bound to"
                                 << QString::fromStdString(station).toUpper();

        state = State_Send_NewConnection;
        timeoutTimer->start(30000);

        writeBuffer.append((char) 0);

        QMetaObject::invokeMethod(this, "directWrite", Qt::QueuedConnection);
        return;

    case State_Reconnecting:
        Q_ASSERT(socket != NULL);
        if (socket->state() == QAbstractSocket::UnconnectedState) {
            qCDebug(log_sync_client) << "Socket closed during initial handshake";

            feedback.emitFailure(tr("Connection closed"));

            socket->deleteLater();
            socket = NULL;
            timeoutTimer->stop();
            state = State_Completed;
            thread->quit();
            return;
        }

        if (!socket->isEncrypted())
            return;

        if (peer == NULL) {
            initializePeer();
        }
        if (peer == NULL) {
            qCDebug(log_sync_client) << "No peer able to interface with"
                                     << socket->peerAddress().toString();

            feedback.emitFailure(tr("No peer interface"));

            socket->abort();
            socket->deleteLater();
            socket = NULL;
            timeoutTimer->stop();
            state = State_Completed;
            thread->quit();
            return;
        }

        qCDebug(log_sync_client) << "Reestablished connection to"
                                 << socket->peerAddress().toString() << "with authorization"
                                 << authorization << "bound to"
                                 << QString::fromStdString(station).toUpper();

        state = State_Send_ResumeConnection;
        timeoutTimer->start(30000);

        writeBuffer.append((char) 1);
        writeBuffer.append(resumeID);

        QMetaObject::invokeMethod(this, "directWrite", Qt::QueuedConnection);
        return;

    case State_Send_NewConnection:
        Q_ASSERT(socket != NULL);
        if (socket->state() == QAbstractSocket::UnconnectedState) {
            qCDebug(log_sync_client) << "Socket closed during initial handshake";

            feedback.emitFailure(tr("Connection closed"));

            socket->deleteLater();
            socket = NULL;
            state = State_Completed;
            thread->quit();
            return;
        }

        if (!writeBuffer.isEmpty())
            return;

        socket->disconnect(this, SLOT(directWrite()));

        state = State_Receive_ResumeID;
        timeoutTimer->start(30000);
        /* Fall through */
    case State_Receive_ResumeID:
        if (readExactly(socket, readBuffer, 16)) {
            QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        }
        if (readBuffer.size() < 16)
            return;

        retryBeginTime = FP::undefined();

        qCDebug(log_sync_client) << "Received resume ID, starting peer";

        resumeID = readBuffer;
        attachSocket();
        peer->start();
        return;

    case State_Send_ResumeConnection:
        Q_ASSERT(socket != NULL);
        Q_ASSERT(peer != NULL);
        if (socket->state() == QAbstractSocket::UnconnectedState) {
            qCDebug(log_sync_client) << "Socket closed during initial handshake";
            disconnectResume();
            return;
        }

        if (!writeBuffer.isEmpty())
            return;

        socket->disconnect(this, SLOT(directWrite()));

        if (!peer->rollback()) {
            qCDebug(log_sync_client) << "Peer rollback failed, synchronization aborted";

            socket->deleteLater();
            socket = NULL;
            state = State_Completed;
            thread->quit();
            return;
        }

        attachSocket();
        return;

    case State_Connected:
        Q_ASSERT(compressor != NULL);
        Q_ASSERT(peer != NULL);

        if (!peer->shouldStallIncoming() && compressor->bytesAvailable() > 0) {
            timeoutTimer->start(120 * 1000);
            peer->incomingData(compressor->read(65536));
            if (compressor->bytesAvailable() > 0) {
                QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
            }
            retryBeginTime = FP::undefined();
        }
        switch (peer->errorState()) {
        case Peer::Error_None:
            break;
        case Peer::Error_Fatal:
            qCDebug(log_sync_client) << "Fatal error, aborting synchronization";
            state = State_Completed;
            thread->quit();
            return;
        case Peer::Error_Recoverable:
            disconnectResume();
            return;
        }

        if (peer->isFinished()) {
            compressor->flush(0);

            if (lock) {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << rcSynchronizeStateVersion;
                    stream << peer->getRemoteFilterState();
                }
                lock->set(lockKey, data);

                lock->release();
                lock.reset();
            }

            peer->deleteLater();
            peer = NULL;
            state = State_Closing;
            QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);

            timeoutTimer->start(120 * 1000);

            qCDebug(log_sync_client) << "Synchronization completed, shutting down connection";
            return;
        }

        /* Transfer completed, so don't check the network status */
        if (peer->transferCompleted())
            return;

        if (socket->state() == QAbstractSocket::UnconnectedState) {
            qCDebug(log_sync_client) << "Socket disconnected";
            disconnectResume();
            return;
        }
        return;

    case State_Closing:
        if (compressor == NULL)
            break;
        Q_ASSERT(socket != NULL);
        {
            if (compressor->bytesToWrite() > 0) {
                compressor->flush(0);
                socket->flush();
            } else {
                if (socket->state() == QAbstractSocket::ConnectedState) {
                    socket->disconnectFromHost();
                }
            }
            if (socket) {
                if (socket->state() != QAbstractSocket::UnconnectedState)
                    return;
            }
        }

        qCDebug(log_sync_client) << "Connection shutdown complete";
        compressor->close();
        compressor->deleteLater();
        compressor = NULL;
        socket->deleteLater();
        socket = NULL;
        state = State_Completed;
        thread->quit();
        return;

    case State_Reconnection_Wait:
        return;
    case State_Completed:
        return;

    }

    qCDebug(log_sync_client) << "Invalid connection state" << state;

    feedback.emitFailure();

    state = State_Completed;
    thread->quit();
}

void ClientMain::directWrite()
{
    if (socket == NULL)
        return;

    QByteArray toWrite;
    qSwap(toWrite, writeBuffer);
    if (toWrite.isEmpty())
        return;

    qint64 n = socket->write(toWrite);
    if (n == -1) {
        qCDebug(log_sync_client) << "Socket disconnected during write operation:"
                                 << socket->errorString();

        feedback.emitFailure(tr("Socket write failure: %1").arg(socket->errorString()));

        socket->abort();
        socket->deleteLater();
        socket = NULL;
        state = State_Completed;
        thread->quit();
        return;
    }

    if ((int) n >= toWrite.size()) {
        QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        return;
    }

    Q_ASSERT(writeBuffer.isEmpty());

    int remaining = toWrite.size() - (int) n;
    writeBuffer.resize(remaining);
    memcpy(writeBuffer.data(), toWrite.constData() + n, (size_t) remaining);
}

void ClientMain::peerWrite()
{
    if (peer == NULL || compressor == NULL)
        return;
    switch (peer->writePending(compressor)) {
    case Peer::WriteResult_Wait:
    case Peer::WriteResult_Continue:
        break;
    case Peer::WriteResult_Completed:
        compressor->disconnect(this, SLOT(peerWrite()));
        compressor->flush(0);
        QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        break;
    case Peer::WriteResult_Error:
        qCDebug(log_sync_client) << "Write error encountered";
        QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        return;
    }

    switch (state) {
    case State_Initialize:
    case State_Connecting:
    case State_Reconnecting:
    case State_Receive_ResumeID:
    case State_Send_NewConnection:
    case State_Send_ResumeConnection:
    case State_Connected:
    case State_Completed:
    case State_Reconnection_Wait:
        break;
    case State_Closing:
        updateSocket();
        break;
    }
}

void ClientMain::disconnectResume()
{
    switch (state) {
    case State_Initialize:
    case State_Connecting:
    case State_Receive_ResumeID:
    case State_Send_NewConnection:
    case State_Completed:
    case State_Closing:
    case State_Reconnection_Wait:
        return;
    case State_Reconnecting:
    case State_Send_ResumeConnection:
    case State_Connected:
        break;
    }

    if (compressor != NULL) {
        compressor->abort();
        compressor->deleteLater();
        compressor = NULL;
    }

    if (socket != NULL) {
        socket->abort();
        socket->deleteLater();
        socket = NULL;
    }

    qCDebug(log_sync_client) << "Disconnected from peer, awaiting resume";

    state = State_Reconnection_Wait;
    timeoutTimer->start(10000);
}

void ClientMain::connectionTimeout()
{
    switch (state) {
    case State_Initialize:
    case State_Completed:
        break;
    case State_Connecting:
    case State_Receive_ResumeID:
    case State_Send_NewConnection:
        qCInfo(log_sync_client) << "Timeout in initial connection state " << state;

        feedback.emitFailure(tr("Timeout"));

        state = State_Completed;
        thread->quit();
        return;
    case State_Reconnecting:
    case State_Send_ResumeConnection:
        qCInfo(log_sync_client) << "Timeout in reconnection state " << state;
        disconnectResume();
        break;
    case State_Connected:
        qCInfo(log_sync_client) << "Timeout during synchronization processing";
        disconnectResume();
        break;
    case State_Closing:
        qCInfo(log_sync_client) << "Timeout during connection finalization";
        state = State_Completed;
        thread->quit();
        return;

    case State_Reconnection_Wait:
        if (!FP::defined(retryBeginTime)) {
            retryBeginTime = Time::time();
        } else if ((Time::time() - retryBeginTime) > 3600.0) {
            qCInfo(log_sync_client) << "Failed to reestablish connection";

            feedback.emitFailure(tr("Failed to reestablish connection"));

            state = State_Completed;
            thread->quit();
            return;
        }

        createSocket();
        Q_ASSERT(socket != NULL);
        return;
    }
}

}

}
}
