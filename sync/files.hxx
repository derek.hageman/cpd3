/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3SYNC_FILES_HXX
#define CPD3SYNC_FILES_HXX

#include "core/first.hxx"

#include <mutex>
#include <memory>
#include <functional>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QFile>

#include "sync/sync.hxx"
#include "core/actioncomponent.hxx"
#include "sync/filter.hxx"
#include "transfer/upload.hxx"
#include "transfer/download.hxx"
#include "transfer/package.hxx"
#include "transfer/datafile.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/variant/root.hxx"
#include "io/access.hxx"

namespace CPD3 {
namespace Sync {

/**
 * The handler that uploads modified data files.
 */
class CPD3SYNC_EXPORT SyncUpload : public CPD3Action {
Q_OBJECT

    std::unique_ptr<Filter> localFilter;
    CPD3::Data::Variant::Read encryption;
    CPD3::Data::Variant::Read signature;
    CPD3::Data::Variant::Read upload;
    std::shared_ptr<CPD3::Data::Archive::Access> readAccess;
    std::shared_ptr<CPD3::Data::Archive::Access> writeAccess;
    double modifiedTime;
    bool ownWriter;

    std::mutex mutex;
    bool terminated;
    bool success;

    class ReadSink : public CPD3::Data::ErasureSink {
        SyncUpload &upload;
    public:
        CPD3::Data::ErasureSink *target;

        ReadSink(SyncUpload &upload);

        virtual ~ReadSink();

        virtual void incomingValue(const CPD3::Data::ArchiveValue &value);

        virtual void incomingValue(CPD3::Data::ArchiveValue &&value);

        virtual void incomingErasure(const CPD3::Data::ArchiveErasure &erasure);

        virtual void incomingErasure(CPD3::Data::ArchiveErasure &&erasure);

        virtual void endStream();
    };

    ReadSink readSink;

    CPD3::Data::Variant::Root resultInformation;

    CPD3::Data::Archive::Access::ErasureHandle readOperation;
    CPD3::Data::Archive::Access::RemoteReferencedHandle referenceOperation;

    bool testTerminated();

    static QString toByteUnits(qint64 n);

    bool exportData(std::unique_ptr<IO::Generic::Stream> &&target,
                    const std::function<std::uint_fast64_t()> &totalSize);

    bool packageOutput(const IO::Access::Handle &input, const IO::Access::Handle &output);

    bool uploadPackage(IO::Access::Handle file);

    Threading::Signal<> terminateRequested;

public:
    /**
     * Create a synchronize uploader.
     *
     * @param local         the local read access to generate the upload file with
     * @param encryption    the encryption configuration
     * @param signature     the signature configuration
     * @param upload        the the upload transfer configuration
     * @param modifiedTime  the modification time to request data after
     * @param writeA   the write access to use
     * @param readA    the read access to use
     */
    SyncUpload(Filter *local,
               const CPD3::Data::Variant::Read &encryption,
               const CPD3::Data::Variant::Read &signature,
               const CPD3::Data::Variant::Read &upload,
               double modifiedTime = FP::undefined(),
               const std::shared_ptr<CPD3::Data::Archive::Access> &writeA = std::shared_ptr<
                       CPD3::Data::Archive::Access>(),
               const std::shared_ptr<CPD3::Data::Archive::Access> &readA = std::shared_ptr<
                       CPD3::Data::Archive::Access>());

    virtual ~SyncUpload();

    /**
     * Test if the synchronization upload succeeded.
     *
     * @return true if the upload was successful
     */
    bool succeeded();

    /**
     * Get the result information describing the upload.
     *
     * @return  the result information
     */
    inline const CPD3::Data::Variant::Root &getResultInformation() const
    { return resultInformation; }

    /**
     * Get the state information for the filter.
     *
     * @return  the state data
     */
    QByteArray getFilterState() const;

public slots:

    virtual void signalTerminate();

protected:
    /**
     * Create the actual transfer uploader for a given input file.
     *
     * @param configuration the configuration in use
     * @param inputFile     the file to be uploaded
     * @return          a new uploader
     */
    virtual std::unique_ptr<
            CPD3::Transfer::FileUploader> createUploader(const CPD3::Data::Variant::Read &configuration,
                                                         IO::Access::Handle inputFile);

    virtual void run();
};

/**
 * The handler that ingests new incoming data files.
 */
class CPD3SYNC_EXPORT SyncDownload : public CPD3Action {
Q_OBJECT
    CPD3::Data::Variant::Read download;

    std::shared_ptr<CPD3::Data::Archive::Access> writeAccess;
    bool ownWriter;

    std::mutex mutex;
    bool terminated;
    bool success;

    QList<CPD3::Data::Variant::Root> results;

    CPD3::Data::Archive::Access::SynchronizationHandle synchronizeOperation;

    bool testTerminated();

    bool processFile(std::unique_ptr<Transfer::DownloadFile> &&file,
                     CPD3::Data::Variant::Write &fileInformation,
                     bool canTerminate);

    class FileUnpacker : public Transfer::TransferUnpack {
        SyncDownload &parent;
        Transfer::DownloadFile &file;

        QString signingCertificate;
        bool signatureRejected;
    public:
        FileUnpacker(SyncDownload &parent, Transfer::DownloadFile &file);

        virtual ~FileUnpacker();

        QString getSigningCertificateID() const;

        bool isUnauthorized() const;

    protected:
        QString applySubstitutions(const QString &input) const override;

        virtual CPD3::Data::Variant::Read getKeyForCertificate(const QByteArray &id, int depth);

        virtual bool signatureAuthorized(const QList<QByteArray> &ids, int depth);
    };

    friend class FileUnpacker;

    QString unpackFile(Transfer::DownloadFile &file,
                       const IO::Access::Handle &input,
                       const IO::Access::Handle &output,
                       CPD3::Data::Variant::Write &fileInformation,
                       bool canTerminate);

    class UnpackSink : public CPD3::Data::ErasureSink {
        SyncDownload &parent;
        std::unique_ptr<Filter> filter;

        qint64 acceptedValuesCount;
        qint64 rejectedValuesCount;
        qint64 acceptedErasureCount;
        qint64 rejectedErasureCount;
    public:
        UnpackSink(SyncDownload &parent, Filter *filter);

        virtual ~UnpackSink();

        void injectInformation(CPD3::Data::Variant::Write &fileInformation) const;

        virtual void incomingValue(const CPD3::Data::ArchiveValue &value);

        virtual void incomingValue(CPD3::Data::ArchiveValue &&value);

        virtual void incomingErasure(const CPD3::Data::ArchiveErasure &erasure);

        virtual void incomingErasure(CPD3::Data::ArchiveErasure &&erasure);

        virtual void endStream();
    };

    friend class UnpackSink;

    bool unpackData(const IO::Access::Handle &input,
                    const QString &authorization,
                    CPD3::Data::Variant::Write &fileInformation,
                    bool canTerminate);

    Threading::Signal<> terminateRequested;

public:
    /**
     * Create a synchronize uploader.
     *
     * @param download      the download configuration
     * @param writeA   the archive write access
     */
    SyncDownload(const CPD3::Data::Variant::Read &download,
                 const std::shared_ptr<CPD3::Data::Archive::Access> &writeA = std::shared_ptr<
                         CPD3::Data::Archive::Access>());

    virtual ~SyncDownload();

    /**
     * Test if the synchronization download succeeded.
     *
     * @return true if the download was successful
     */
    bool succeeded();

    /**
     * Get the result information for all files that where downloaded.
     *
     * @return      the descriptive information of the file results
     */
    inline const QList<CPD3::Data::Variant::Root> &getFileResults() const
    { return results; }

public slots:

    virtual void signalTerminate();

protected:
    /**
     * Apply substitutions to strings being used.
     * <br>
     * By default this just returns the input.
     *
     * @param input the input
     * @return      the input with substitutions applied
     */
    virtual QString applySubstitutions(const QString &input) const;

    /**
     * Called when before substitutions are applied to a process invoke.
     * <br>
     * By default this does nothing.
     *
     * @param fileName  the file name created
     */
    virtual void pushInvokeStage(const QString &input, const QString &output);

    /**
     * Called when after process substitutions are done.
     * <br>
     * By default this does nothing.
     */
    virtual void popInvokeStage();

    /**
     * Create the actual transfer downloader to fetch files with.
     *
     * @return              a new downloader or NULL to abort
     */
    virtual Transfer::FileDownloader *createDownloader(const CPD3::Data::Variant::Read &configuration) = 0;

    /**
     * Called on successful completion of the download before the final
     * destruction of the downloader.
     *
     * @param downloader    the downloader in use
     */
    virtual void completeDownloader(Transfer::FileDownloader *downloader);

    /**
     * Test if the signature on a file is ever authorized.
     *
     * @param authorization the signature to check
     * @return              true to proceed
     */
    virtual bool signatureAuthorized(const QString &authorization);

    /**
     * Get the decryption data for a given signature, signing certificate (if available)
     * and input file.
     *
     * @param authorization     the signature on the file
     * @param encryptingCertificate the encrypting certificate ID, if available
     * @param file      the file being decrypted
     * @return          the decryption information
     */
    virtual CPD3::Data::Variant::Read decryptionInformation(const QString &authorization,
                                                            const QString &encryptingCertificate,
                                                            Transfer::DownloadFile *file) = 0;

    /**
     * Create the input filter for a given authorization.
     *
     * @param authorization the active authorization
     * @return              a new filter or NULL to reject it
     */
    virtual Filter *createFilter(const QString &authorization) = 0;

    virtual void run();
};

}
}

#endif //CPD3SYNC_FILES_HXX
