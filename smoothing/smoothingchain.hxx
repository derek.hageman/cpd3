/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGCHAIN_H
#define CPD3SMOOTHINGCHAIN_H

#include "core/first.hxx"

#include <vector>
#include <stdlib.h>
#include <math.h>
#include <QtGlobal>
#include <QObject>
#include <QThread>
#include <QDataStream>
#include <QSet>

#include "smoothing/smoothing.hxx"
#include "algorithms/dewpoint.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "datacore/stream.hxx"
#include "datacore/sinkmultiplexer.hxx"

namespace CPD3 {
namespace Smoothing {

/**
 * A node of the smoother chain.  This will not be deleted until the total 
 * chain is done or finished() returns true.  That is, it is safe to return 
 * member variables as chain targets.
 */
class CPD3SMOOTHING_EXPORT SmootherChainNode {
public:
    virtual ~SmootherChainNode();

    /**
     * Test if the node has completed.  This is not required for the system
     * but it allows the manager to clean up unused points before total
     * destruction so it is advised to implement it.
     * <br>
     * The default implementation returns false.
     * 
     * @return true if the node has finished and can be deleted
     */
    virtual bool finished();

    /**
     * Serialize the node.  This will only be called once all its possible
     * inputs have been paused (it will not receive any values during the
     * serialize) so it does not need to lock a mutex.
     * 
     * @param stream    the target stream
     */
    virtual void serialize(QDataStream &stream) const = 0;

    /**
     * Pause processing in preparation for serialization.  This will only be
     * called after all the nodes inputs have been paused.  During the pause
     * no values may be passed to outputs and no processing should be done.
     * <br>
     * For non threading nodes, this can be implemented by just locking
     * the mutex protecting the processing state to be serialized.  Threading
     * nodes must also ensure that all ongoing processing has finished before
     * returning.
     * <br>
     * The default implementation does nothing.
     */
    virtual void pause();

    /**
     * Resume processing after a pause state.
     * <br>
     * The default implementation does nothing.
     */
    virtual void resume();

    /**
     * Signal the node to terminate as soon as safely possible.  Termination
     * must call SmootherChainTarget::endData() for all targets.
     * <br>
     * The default implementation does nothing.
     */
    virtual void signalTerminate();

    /**
     * Called when by the manager before deletion of any elements.  This
     * is called in the same order as the network flows.  That is, it is 
     * only called once all the inputs to this node have finished it.  This
     * allows a thread to implement a shutdown and wait, for example.
     * <br>
     * The default implementation does nothing.
     */
    virtual void cleanup();
};

/**
 * A target for a data stream in the smoother chain.  A data stream consists
 * of a series of non-overlapping segments of single double values in ascending
 * time order.  That is, it is the result of the final overlay and breakdown
 * of input Data::DataValue into double values.  Each target is only
 * used for a single stream and will not be referenced after its endData() is
 * called.
 */
class CPD3SMOOTHING_EXPORT SmootherChainTarget {
public:
    virtual ~SmootherChainTarget();

    /**
     * Add data to the smoother chain target.  All values are non-overlapping
     * and added in ascending time order.  However they may be continuous
     * (start equal to the end of the previous one or the previous advance).
     * 
     * @param start the effective start time of the value
     * @param end   the effective end time of the value
     * @param value the value to add
     */
    virtual void incomingData(double start, double end, double value) = 0;

    /**
     * Notify the chain element that it will never receive any values before
     * the given time.  This should be used to emit as much data as possible.
     * Advances are non-overlapping with values; though they may be 
     * continuous (advance time equal to the end of the previous value and
     * the start of the next one, for example).
     * 
     * @param time  the time
     */
    virtual void incomingAdvance(double time) = 0;

    /**
     * Notify the chain element of the end of a stream of data.  No further
     * values or advances will be added.  The target will not be referenced
     * after this returns.
     */
    virtual void endData() = 0;
};

/**
 * A simple implementation that just forwards all calls to other chain
 * elements.
 * 
 * @param N the number of targets to forward to
 */
template<std::size_t N>
class SmootherChainForward : public SmootherChainTarget, public SmootherChainNode {
    static_assert(N > 0, "need at least one input");

    /* No mutex, since this has only a single input so it's always serial */
    bool ended;
    SmootherChainTarget *targets[N];
public:
    SmootherChainForward(std::initializer_list<SmootherChainTarget *> init) : ended(false)
    {
        Q_ASSERT(init.size() == N);
        std::size_t index = 0;
        for (auto add : init) {
            targets[index] = add;
            ++index;
        }
    }

    /**
     * Set the target at a specific index.
     * 
     * @param index     the index to set
     * @param target    the target
     */
    inline void setTarget(std::size_t index, SmootherChainTarget *target)
    {
        Q_ASSERT(index >= 0 && index < N);
        targets[index] = target;
    }

    virtual ~SmootherChainForward()
    { }

    virtual bool finished()
    { return ended; }

    virtual void serialize(QDataStream &stream) const
    { Q_UNUSED(stream); }

    virtual void incomingData(double start, double end, double value)
    {
        Q_ASSERT(!ended);
        for (std::size_t i = 0; i < N; i++) {
            targets[i]->incomingData(start, end, value);
        }
    }

    virtual void incomingAdvance(double time)
    {
        Q_ASSERT(!ended);
        for (std::size_t i = 0; i < N; i++) {
            targets[i]->incomingAdvance(time);
        }
    }

    virtual void endData()
    {
        for (std::size_t i = 0; i < N; i++) {
            targets[i]->endData();
        }
        ended = true;
    }
};

/**
 * A target for a flags stream in the smoother chain.  This behaves identical
 * to SmootherChainTarget except that it handles sets of flags instead of
 * real values.
 */
class CPD3SMOOTHING_EXPORT SmootherChainTargetFlags {
public:
    virtual ~SmootherChainTargetFlags();

    /**
     * Add data to the smoother chain target.  All values are non-overlapping
     * and added in ascending time order.  However they may be continuous
     * (start equal to the end of the previous one or the previous advance).
     * 
     * @param start the effective start time of the value
     * @param end   the effective end time of the value
     * @param value the value to add
     */
    virtual void incomingData(double start, double end, const Data::Variant::Flags &value) = 0;

    /**
     * Notify the chain element that it will never receive any values before
     * the given time.  This should be used to emit as much data as possible.
     * Advances are non-overlapping with values; though they may be 
     * continuous (advance time equal to the end of the previous value and
     * the start of the next one, for example).
     * 
     * @param time  the time
     */
    virtual void incomingAdvance(double time) = 0;

    /**
     * Notify the chain element of the end of a stream of data.  No further
     * values or advances will be added.  The target will not be referenced
     * after this returns.
     */
    virtual void endData() = 0;
};

/**
 * A target for a general stream in the smoother chain.  This behaves identical
 * to SmootherChainTarget except that it handles a general data value.
 */
class CPD3SMOOTHING_EXPORT SmootherChainTargetGeneral {
public:
    virtual ~SmootherChainTargetGeneral();

    /**
     * Add data to the smoother chain target.  All values are non-overlapping
     * and added in ascending time order.  However they may be continuous
     * (start equal to the end of the previous one or the previous advance).
     * 
     * @param start the effective start time of the value
     * @param end   the effective end time of the value
     * @param value the value to add
     */
    virtual void incomingData(double start, double end, const Data::Variant::Root &value) = 0;

    /**
     * Notify the chain element that it will never receive any values before
     * the given time.  This should be used to emit as much data as possible.
     * Advances are non-overlapping with values; though they may be 
     * continuous (advance time equal to the end of the previous value and
     * the start of the next one, for example).
     * 
     * @param time  the time
     */
    virtual void incomingAdvance(double time) = 0;

    /**
     * Notify the chain element of the end of a stream of data.  No further
     * values or advances will be added.  The target will not be referenced
     * after this returns.
     */
    virtual void endData() = 0;
};

/**
 * A class providing auxiliary interface functions to the engine interface
 * core.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreEngineAuxiliaryInterface {
public:
    SmootherChainCoreEngineAuxiliaryInterface();

    virtual ~SmootherChainCoreEngineAuxiliaryInterface();
};

/**
 * The interface provided to a smoother chain core to perform additional
 * operations on the engine handling the core.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreEngineInterface {
public:
    virtual ~SmootherChainCoreEngineInterface();

    /**
     * Add a node to the chain.  The nodes must be added from the end moving
     * towards the start.  That is, a node may only be added before any
     * of its inputs.  Similarly nodes are serialized in the same order they
     * are added, so de-serialization must follow the add order.  Once a node
     * is registered the engine takes ownership of it and will delete it
     * when possible.
     * <br>
     * Generally this simply means adding chain nodes as they are created, as
     * the above rules usually govern when nodes can be created.
     * 
     * @param add the node to add
     */
    virtual void addChainNode(SmootherChainNode *add) = 0;

    /**
     * Get the output at the given index.  This is owned by the the engine
     * and should not be deleted.
     * 
     * @param index the index of the output
     * @return      the output target
     */
    virtual SmootherChainTarget *getOutput(int index) = 0;

    /**
     * Get the flags output at the given index.  This is owned by the the engine
     * and should not be deleted.
     * 
     * @param index the index of the output
     * @return      the output target
     */
    virtual SmootherChainTargetFlags *getFlagsOutput(int index) = 0;

    /**
     * Get the general output at the given index.  This is owned by the the 
     * engine and should not be deleted.
     * 
     * @param index the index of the output
     * @return      the output target
     */
    virtual SmootherChainTargetGeneral *getGeneralOutput(int index) = 0;

    /**
     * Add a target of the chain from the given input index.  Ownership of the
     * input is assumed to remain elsewhere; it is never deleted.
     * 
     * @param index     the index of the input
     * @param target    the target to add
     */
    virtual void addInputTarget(int index, SmootherChainTarget *target) = 0;

    /**
     * Add a flags target of the chain from the given input index.  Ownership 
     * of the input is assumed to remain elsewhere; it is never deleted.
     * 
     * @param index     the index of the input
     * @param target    the target to add
     */
    virtual void addFlagsInputTarget(int index, SmootherChainTargetFlags *target) = 0;

    /**
     * Add a general target of the chain from the given input index.  Ownership 
     * of the input is assumed to remain elsewhere; it is never deleted.
     * 
     * @param index     the index of the input
     * @param target    the target to add
     */
    virtual void addGeneralInputTarget(int index, SmootherChainTargetGeneral *target) = 0;


    /**
     * Get the base unit that originated the creation of this chain.  Note 
     * that this is different than the inputs.  For example, for absorptions
     * this is the absorption unit, but for vector averages the index
     * is the same as the input order.
     * 
     * @param index the base unit index, dependent on the type of chain being constructed
     * @return      the base unit of the chain
     */
    virtual Data::SequenceName getBaseUnit(int index = 0) = 0;

    /**
     * Add a new output for the chain after the end of the existing ones.  This 
     * is owned by the the engine and should not be deleted.
     * 
     * @param unit  the output unit
     * @return      the new output target
     */
    virtual SmootherChainTarget *addNewOutput(const Data::SequenceName &unit) = 0;

    /**
     * Add a new flags output for the chain after the end of the existing ones.  
     * This is owned by the the engine and should not be deleted.
     * 
     * @param unit  the output unit
     * @return      the new output target
     */
    virtual SmootherChainTargetFlags *addNewFlagsOutput(const Data::SequenceName &unit) = 0;

    /**
     * Add a new general output for the chain after the end of the existing 
     * ones.  This is owned by the the engine and should not be deleted.
     * 
     * @param unit  the output unit
     * @return      the new output target
     */
    virtual SmootherChainTargetGeneral *addNewGeneralOutput(const Data::SequenceName &unit) = 0;

    /**
     * Add a new general purpose output to the final output multiplexer.
     * This is owned by the the engine and should not be deleted.
     * <br>
     * This is NOT restored on de-serialization and so must be created again.
     * 
     * @return      the new output ingress
     */
    virtual Data::SinkMultiplexer::Sink *addNewFinalOutput() = 0;


    /**
     * Add a new input for the chain after the end of the existing ones.  This 
     * is owned by the the engine and should not be deleted.
     * 
     * @param unit      the input unit
     * @param target    if not NULL then also add this target initially
     */
    virtual void addNewInput(const Data::SequenceName &unit,
                             SmootherChainTarget *target = NULL) = 0;

    /**
     * Add a new flags input for the chain after the end of the existing ones.  
     * This is owned by the the engine and should not be deleted.
     * 
     * @param unit      the input unit
     * @param target    if not NULL then also add this target initially
     */
    virtual void addNewFlagsInput(const Data::SequenceName &unit,
                                  SmootherChainTargetFlags *target = NULL) = 0;

    /**
     * Add a new general input for the chain after the end of the existing ones.  
     * This is owned by the the engine and should not be deleted.
     * 
     * @param unit      the input unit
     * @param target    if not NULL then also add this target initially
     */
    virtual void addNewGeneralInput(const Data::SequenceName &unit,
                                    SmootherChainTargetGeneral *target = NULL) = 0;

    /**
     * Get the options value that was used in the construction of the
     * current chain.  By default this returns an undefined value.
     * 
     * @return the options used in the chain
     */
    virtual Data::Variant::Read getOptions();

    /**
     * Get the auxiliary interface to the engine.  By default this returns
     * NULL.
     * 
     * @return the auxiliary engine interface
     */
    virtual SmootherChainCoreEngineAuxiliaryInterface *getAuxiliaryInterface();
};

/**
 * The core handler for a smoother chain.  This is responsible for
 * creating the chains involved in the actual smoothing.
 * <br>
 * If any output is not used by an implementation, it must have
 * endData() called on it by the creation function.  All targets are assumed
 * to be owned by other objects; that is they are never freed by the caller.
 * <br>
 * When serialized the creation type is saved and all nodes registered during
 * the call are serialized in the order of registration.  Nodes are paused
 * in reverse order of registration before serialization.  That is, nodes
 * should be registered from the end to the start of the chain.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCore {
public:
    virtual ~SmootherChainCore();

    /**
     * The types of smoother that exist.  This determines the predefined
     * inputs and outputs.
     */
    enum Type {
        /**
         * A general smoother, pre-defines a single input and output.
         */
                General = 0, /**
         * A difference measurement.  The inputs and outputs are the
         * start and end, in that order.
         */
                Difference, /**
         * A difference measurement starting from single values.  This takes
         * a single input and produces the first and last ones within
         * a bin time.  Assuming this is fully handled, the output smoothing
         * type is set to conventional difference.
         */
                DifferenceInitial, /**
         * A two dimensional vector reconstruction.  The inputs and outputs
         * are the direction and magnitude, in that order.
         */
                Vector2D, /**
         * A three dimensional vector reconstruction.  The inputs and outputs
         * are the azimuth angle, elevation angle, and magnitude, in that order.
         */
                Vector3D, /**
         * A Beer's law reconstruction of the absorption.  The inputs are the
         * start L, end L, start I and end I in that order.  The single output
         * is absorption.
         */
                BeersLawAbsorption, /**
         * A Beer's law reconstruction of the absorption from single values.  
         * The inputs are the L and I values in that order.  The single output
         * is absorption.  Assuming this and differences are fully handled, 
         * the output smoothing type is set to conventional Beer's law.
         */
                BeersLawAbsorptionInitial, /**
         * A dewpoint calculation.  The inputs are the temperature and RH,
         * in that order.  The single output is the dewpoint.  The input
         * units for the temperature and RH are available as base units 1 and
         * 2.
         */
                Dewpoint, /**
         * An RH calculation.  The inputs are the temperature and dewpoint,
         * in that order.  The single output is the RH.  The input
         * units for the temperature and dewpoints are available as base 
         * units 1 and 2.
         */
                RH, /**
         * A RH extrapolation.  The inputs are the temperature and RH at the
         * first point, then the temperature at the second point.  The
         * single output is the RH.  The input units for the input temperature,
         * input RH, and output RH are available as base units 1, 2, and 3.
         */
                RHExtrapolate,
    };

    /**
     * Create a general smoother.  The definition of the inputs and outputs
     * is determined by the type.
     * 
     * @param engine    the engine interface
     * @param type      the type of smoother
     */
    virtual void createSmoother(SmootherChainCoreEngineInterface *engine, Type type) = 0;

    /**
     * Create a general smoother.  The definition of the inputs and outputs
     * is determined by the type.  Any additional bindings are preserved
     * from the time of serialization.
     * 
     * @param stream    the stream to restore from
     * @param engine    the engine interface
     * @param type      the type of smoother
     */
    virtual void deserializeSmoother
            (QDataStream &stream, SmootherChainCoreEngineInterface *engine, Type type) = 0;

    /**
     * The possible ways this core can deal with special values.
     */
    enum HandlerMode {
        /**
         * The core can handle the special value and it preserves the
         * information needed to repeat the operation.  That is, the metadata
         * will not be changed to indicate that the value is no longer
         * handled.  For example, difference measurements on binned data
         * broken up on any gap.
         */
                Repeatable,

        /**
         * The core can handle the special value but it is not repeatable so
         * the metadata is changed to make it a traditional value after
         * the core.  For example, an absorption recalculated by a binned
         * smoother that smooths over any gap at all.
         */
                Handled,

        /**
         * The special type is not handled by the core at all and it should
         * be routed through the general case.
         */
                Unhandled,
    };

    /**
     * Test if this core can a smoother type.
     * 
     * @param type  the type of smoother
     * @return      the handler mode for difference measurements
     */
    virtual HandlerMode smootherHandler(Type type) = 0;

    /**
     * Test if this core does anything with flags.  If it does not
     * then they are simply passed through.
     */
    virtual bool flagsHandler() = 0;

    /**
     * Create a flags handler.  The input and output have a single defined
     * target.
     * 
     * @param engine    the engine interface
     */
    virtual void createFlags(SmootherChainCoreEngineInterface *engine) = 0;

    /**
     * Create a flags handler.
     * 
     * @param stream    the stream to restore from
     * @param engine    the engine interface
     */
    virtual void
            deserializeFlags(QDataStream &stream, SmootherChainCoreEngineInterface *engine) = 0;

    /**
     * Return the base metadata to describe the processing this smoother
     * does.  Generally this only needs to set a "Type" field with some
     * "unique" identifier as well as anything in the "Parameters" fields
     * that should be noted.
     * 
     * @param time  the start of the metadata
     * @return      the base processing metadata
     */
    virtual Data::Variant::Root getProcessingMetadata(double time) const = 0;

    /**
     * Return the set of times the processing metadata may change.  This is
     * generally the times when a description changes.
     * 
     * @return      the set of changed times
     */
    virtual QSet<double> getProcessingMetadataBreaks() const = 0;

    /**
     * Test if a unit is generated as an output of the chain.  This is distinct
     * from multiple input and output chains like vector averages.  An example
     * would be coverage.  If a unit is generated by the chain then it is
     * not passed through.
     * 
     * @param unit  the unit to test
     * @return      true if the unit is generated by the chain
     */
    virtual bool generatedOutput(const Data::SequenceName &unit) = 0;
};

/**
 * A simple smoother chain core.  This is only required to implement
 * the general interface and all other interfaces are constructed from that.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreSimple : public SmootherChainCore {
public:
    SmootherChainCoreSimple();

    virtual ~SmootherChainCoreSimple();

    /**
     * Create a general smoother.
     * 
     * @param engine    the engine interface
     * @param input     the input target
     * @param output    the output target
     */
    virtual void createSmoother(SmootherChainCoreEngineInterface *engine,
                                SmootherChainTarget *&input,
                                SmootherChainTarget *output) = 0;

    /**
     * Create a general smoother.
     * 
     * @param stream    the stream to restore from
     * @param engine    the engine interface
     * @param input     the input target
     * @param output    the output target
     */
    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainTarget *&input,
                                     SmootherChainTarget *output) = 0;

    virtual Data::Variant::Root getProcessingMetadata(double time) const = 0;

    virtual QSet<double> getProcessingMetadataBreaks() const = 0;

    virtual void
            createSmoother(SmootherChainCoreEngineInterface *engine, SmootherChainCore::Type type);

    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainCore::Type type);

    virtual SmootherChainCore::HandlerMode smootherHandler(SmootherChainCore::Type type);

    virtual bool flagsHandler();

    virtual void createFlags(SmootherChainCoreEngineInterface *engine);

    virtual void deserializeFlags(QDataStream &stream, SmootherChainCoreEngineInterface *engine);

    virtual bool generatedOutput(const Data::SequenceName &unit);
};

/**
 * A smoother chain core that immediately ends all its inputs.  This can be
 * used to force part of the editing to be ignored.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreDiscard : public SmootherChainCore {
public:
    SmootherChainCoreDiscard();

    virtual ~SmootherChainCoreDiscard();

    virtual Data::Variant::Root getProcessingMetadata(double time) const;

    virtual QSet<double> getProcessingMetadataBreaks() const;

    virtual void
            createSmoother(SmootherChainCoreEngineInterface *engine, SmootherChainCore::Type type);

    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainCore::Type type);

    virtual SmootherChainCore::HandlerMode smootherHandler(SmootherChainCore::Type type);

    virtual bool flagsHandler();

    virtual void createFlags(SmootherChainCoreEngineInterface *engine);

    virtual void deserializeFlags(QDataStream &stream, SmootherChainCoreEngineInterface *engine);

    virtual bool generatedOutput(const Data::SequenceName &unit);
};


/**
 * The general implementation for a segment breakdown stream.  This
 * allows simple combination of multiple inputs that require synchronized
 * processing.  This class requires that all input streams are non-overlapping.
 * That is, it will fail if values sent to it on a given stream overlap with
 * each other.
 * 
 * @param N             the number of dimensions
 * @param StorageType   the type used to store data in all dimensions, must be movable
 */
template<std::size_t N, typename StorageType>
class SmootherChainInputSegmentProcessorBase {
    static_assert(N > 0, "need at least one dimension");
    
    struct InputSegment {
        double start;
        double end;
        StorageType value;

        inline InputSegment() = default;

        inline InputSegment(double start, double end, const StorageType &value) : start(start),
                                                                                  end(end),
                                                                                  value(value)
        { }
    };

    std::deque<InputSegment> segments[N];
    double advanceTimes[N];
    bool ended[N];
    bool criticalStream[N];
    double lastAdvanceEmitted;

    void emitAllPossible()
    {
        std::size_t offsets[N];
        std::fill_n(&offsets[0], N, 0);
        std::fill_n(&criticalStream[0], N, false);

        double firstPossibleAdvance = FP::undefined();
        for (std::size_t i = 0; i < N; i++) {
            if (ended[i])
                continue;
            if (!FP::defined(advanceTimes[i])) {
                firstPossibleAdvance = FP::undefined();
                if (i != 0)
                    memset(criticalStream, 0, sizeof(bool) * i);
                /* Only mark this one as critical as we'll always need it
                 * before we can do anything even if others are also
                 * undefined */
                criticalStream[i] = true;
                break;
            }
            if (!FP::defined(firstPossibleAdvance) || firstPossibleAdvance > advanceTimes[i]) {
                firstPossibleAdvance = advanceTimes[i];
                if (i != 0)
                    memset(criticalStream, 0, sizeof(bool) * i);
                criticalStream[i] = true;
            } else if (firstPossibleAdvance == advanceTimes[i]) {
                criticalStream[i] = true;
            }
        }

        for (; ;) {
            double firstRealStart = FP::undefined();
            double segmentEndTime = FP::undefined();
            std::size_t nRealStarts = 0;
            bool haveAllStreams = true;
            for (std::size_t i = 0; i < N; i++) {
                if (segments[i].size() <= offsets[i]) {
                    if (!ended[i]) {
                        haveAllStreams = false;
                        criticalStream[i] = true;
                    }
                    continue;
                }

                const auto &s = segments[i][offsets[i]];

                if (nRealStarts == 0) {
                    firstRealStart = s.start;
                    segmentEndTime = s.end;
                    nRealStarts = 1;
                } else if (!FP::defined(s.start)) {
                    if (FP::defined(firstRealStart)) {
                        nRealStarts++;

                        if (FP::defined(s.end) &&
                                (!FP::defined(firstRealStart) || s.end < firstRealStart)) {
                            segmentEndTime = s.end;
                        } else {
                            segmentEndTime = firstRealStart;
                        }

                        firstRealStart = s.start;
                    } else {
                        if (FP::defined(s.end) &&
                                (!FP::defined(segmentEndTime) || s.end <= segmentEndTime)) {
                            segmentEndTime = s.end;
                        }
                    }
                } else if (!FP::defined(firstRealStart)) {
                    nRealStarts++;

                    if (!FP::defined(segmentEndTime) || s.start < segmentEndTime) {
                        segmentEndTime = s.start;
                    }
                } else {
                    if (firstRealStart < s.start) {
                        nRealStarts++;

                        if (!FP::defined(segmentEndTime) || s.start < segmentEndTime) {
                            segmentEndTime = s.start;
                        }
                    } else if (firstRealStart != s.start) {
                        nRealStarts++;

                        if (FP::defined(s.end) &&
                                (!FP::defined(firstRealStart) || s.end < firstRealStart)) {
                            segmentEndTime = s.end;
                        } else {
                            segmentEndTime = firstRealStart;
                        }

                        firstRealStart = s.start;
                    } else {
                        if (FP::defined(s.end) &&
                                (!FP::defined(segmentEndTime) || s.end <= segmentEndTime)) {
                            segmentEndTime = s.end;
                        }
                    }
                }
            }

            /* No segments, so done */
            if (nRealStarts < 1)
                break;

            if (haveAllStreams && nRealStarts == 1) {
                Q_ASSERT(Range::compareStartEnd(firstRealStart, segmentEndTime) < 0);
                /* All segments start at the same time, so emit a segment 
                 * starting there and ending at the soonest end.  Move all
                 * segments up to that time (discarding as needed). */

                StorageType outputValues[N];
                if (FP::defined(segmentEndTime)) {
                    for (std::size_t i = 0; i < N; i++) {
                        if (segments[i].size() <= offsets[i]) {
                            outputValues[i] = FP::undefined();
                            continue;
                        }
                        auto &s = segments[i][offsets[i]];
                        if (s.end == segmentEndTime) {
                            offsets[i]++;
                        } else {
                            s.start = segmentEndTime;
                        }
                        outputValues[i] = s.value;
                    }
                } else {
                    for (std::size_t i = 0; i < N; i++) {
                        if (segments[i].size() <= offsets[i]) {
                            outputValues[i] = FP::undefined();
                            continue;
                        }
                        const auto &s = segments[i][offsets[i]];
                        outputValues[i] = s.value;
                        offsets[i]++;
                    }
                }

                segmentDone(firstRealStart, segmentEndTime, outputValues);
                lastAdvanceEmitted = segmentEndTime;
            } else if (haveAllStreams) {
                Q_ASSERT(nRealStarts > 1);
                Q_ASSERT(Range::compareStartEnd(firstRealStart, segmentEndTime) < 0);
                /* There's a partial segment at the start, so emit that
                 * and move everything up to the end of it. */

                StorageType outputValues[N];
                if (!FP::defined(firstRealStart)) {
                    Q_ASSERT(!FP::defined(segmentEndTime));
                    /* This should have been handled by the all same start
                     * above (all streams are infinite) */

                    for (std::size_t i = 0; i < N; i++) {
                        if (segments[i].size() <= offsets[i]) {
                            outputValues[i] = FP::undefined();
                            continue;
                        }
                        auto &s = segments[i][offsets[i]];
                        if (!FP::defined(s.start)) {
                            outputValues[i] = s.value;
                            if (FP::defined(s.end) && s.end == segmentEndTime) {
                                offsets[i]++;
                            } else {
                                Q_ASSERT(!FP::defined(s.end) || s.end > segmentEndTime);
                                s.start = segmentEndTime;
                            }
                        } else {
                            outputValues[i] = FP::undefined();
                        }
                    }
                } else if (FP::defined(segmentEndTime)) {
                    for (std::size_t i = 0; i < N; i++) {
                        if (segments[i].size() <= offsets[i]) {
                            outputValues[i] = FP::undefined();
                            continue;
                        }
                        auto &s = segments[i][offsets[i]];
                        if (s.start == firstRealStart) {
                            outputValues[i] = s.value;
                            if (FP::defined(s.end) && s.end == segmentEndTime) {
                                offsets[i]++;
                            } else {
                                Q_ASSERT(!FP::defined(s.end) || s.end > segmentEndTime);
                                s.start = segmentEndTime;
                            }
                        } else {
                            outputValues[i] = FP::undefined();
                        }
                    }
                } else {
                    for (std::size_t i = 0; i < N; i++) {
                        if (segments[i].size() <= offsets[i]) {
                            outputValues[i] = FP::undefined();
                            continue;
                        }
                        const auto &s = segments[i][offsets[i]];
                        Q_ASSERT(s.start == firstRealStart);
                        Q_ASSERT(!FP::defined(s.end));
                        outputValues[i] = s.value;
                        offsets[i]++;
                    }
                }
                segmentDone(firstRealStart, segmentEndTime, outputValues);
                lastAdvanceEmitted = segmentEndTime;
            } else if (FP::defined(firstPossibleAdvance) &&
                    (!FP::defined(firstRealStart) || firstPossibleAdvance > firstRealStart) &&
                    FP::defined(segmentEndTime) &&
                    segmentEndTime <= firstPossibleAdvance) {
                /* All streams advanced past the first possible start, see if
                 * it's possible to emit something.  All streams with values
                 * must end before or equal to the first possible advance. */

                StorageType outputValues[N];
                for (std::size_t i = 0; i < N; i++) {
                    if (segments[i].size() <= offsets[i]) {
                        outputValues[i] = FP::undefined();
                        continue;
                    }
                    auto &s = segments[i][offsets[i]];
                    if (s.end == segmentEndTime) {
                        offsets[i]++;
                    } else {
                        s.start = segmentEndTime;
                    }
                    outputValues[i] = s.value;
                }

                segmentDone(firstRealStart, segmentEndTime, outputValues);
                lastAdvanceEmitted = segmentEndTime;
            } else {
                break;
            }
        }

        for (std::size_t i = 0; i < N; i++) {
            segments[i].erase(segments[i].begin(), segments[i].begin() + offsets[i]);
        }

        /* Emit an advance if we've moved up.  We can't advance if the
         * advance time lies within any existing segment data (since those
         * could generate an output segment at some later time). */
        if (Range::compareStart(firstPossibleAdvance, lastAdvanceEmitted) > 0) {
            Q_ASSERT(FP::defined(firstPossibleAdvance));
            for (std::size_t i = 0; i < N; i++) {
                if (segments[i].empty())
                    continue;
                const auto &s = segments[i].front();
                if (!FP::defined(s.start) || firstPossibleAdvance > s.start)
                    return;
            }
            lastAdvanceEmitted = firstPossibleAdvance;
            allStreamsAdvanced(lastAdvanceEmitted);
        }
    }

public:
    SmootherChainInputSegmentProcessorBase()
    {
        std::fill_n(&advanceTimes[0], N, FP::undefined());
        std::fill_n(&ended[0], N, false);
        std::fill_n(&criticalStream[0], N, true);
        lastAdvanceEmitted = FP::undefined();
    }

    SmootherChainInputSegmentProcessorBase(QDataStream &stream)
    { readSerial(stream); }

    virtual ~SmootherChainInputSegmentProcessorBase() = default;

    /**
     * Add a segment value.  This will call segmentDone(double, double, 
     * const StorageType[N]) and/or allStreamsAdvanced(double) when the time
     * has advanced.
     * 
     * @param stream    the segment stream to add to
     * @param start     the start time of the value
     * @param end       the end time of the value
     * @param value     the value to incorporate
     */
    void segmentValue(std::size_t stream, double start, double end, const StorageType &value)
    {
        Q_ASSERT(stream >= 0 && stream < N);
        Q_ASSERT(!ended[stream]);
        Q_ASSERT(Range::compareStartEnd(start, end) < 0);
        Q_ASSERT(Range::compareStart(start, advanceTimes[stream]) >= 0);
        Q_ASSERT(Range::compareStartEnd(advanceTimes[stream], end) <= 0);

        advanceTimes[stream] = end;
        segments[stream].emplace_back(start, end, value);
        if (criticalStream[stream])
            emitAllPossible();
    }

    /**
     * Advance the segment processor to the given time.  This indicates to the
     * processor that it will not receive any values before the given time.
     * This will call segmentDone(double, double, const double[N]) and/or 
     * allStreamsAdvanced(double) as needed.
     * 
     * @param stream    the stream being advanced
     * @param time      the time to advance to
     */
    void segmentAdvance(std::size_t stream, double time)
    {
        Q_ASSERT(!ended[stream]);
        int cr = Range::compareStart(time, advanceTimes[stream]);
        /* This is a failure since we should only see segments in strictly
         * advancing times (that is, no overlap) */
        Q_ASSERT(cr >= 0);
        if (cr == 0)
            return;

        advanceTimes[stream] = time;
        if (criticalStream[stream])
            emitAllPossible();
    }

    /**
     * Notify the segment reader of the end of a stream.  No further values
     * will be received in that stream.  This will call segmentDone(double, 
     * double, const double[N]) and/or allStreamsAdvanced(double) as needed.
     * Returns true when all streams have been ended.
     * 
     * @param stream    the stream being ended
     * @return          true if all streams have ended
     */
    bool segmentEnd(std::size_t stream)
    {
        Q_ASSERT(!ended[stream]);
        ended[stream] = true;

        bool allEnded = true;
        for (std::size_t i = 0; i < N; i++) {
            if (!ended[i]) {
                allEnded = false;
                break;
            }
        }

        emitAllPossible();
        return allEnded;
    }

    /**
     * Save the state to the given stream.  Note that this does NOT
     * lock the mutex, so it may not be called when any other thread
     * may be accessing the processor.
     * 
     * @param stream    the target stream
     */
    void writeSerial(QDataStream &stream) const
    {
        for (std::size_t idxStream = 0; idxStream < N; idxStream++) {
            Serialize::container(stream, segments[idxStream], [&](const InputSegment &seg) {
                stream << seg.start << seg.end << seg.value;
            });
            stream << advanceTimes[idxStream] << ended[idxStream];
        }
        stream << lastAdvanceEmitted;
    }

    /**
     * Save the state from the given stream.  Note that this does NOT
     * lock the mutex, so it may not be called when any other thread
     * may be accessing the processor.
     * 
     * @param stream    the source stream
     */
    void readSerial(QDataStream &stream)
    {
        for (std::size_t idxStream = 0; idxStream < N; idxStream++) {
            Deserialize::container(stream, segments[idxStream], [&]() {
                InputSegment seg;
                stream >> seg.start >> seg.end >> seg.value;
                return seg;
            });
            stream >> advanceTimes[idxStream] >> ended[idxStream];
            criticalStream[idxStream] = true;
        }
        stream >> lastAdvanceEmitted;
    }

    /**
     * Freeze the processor until it is unfrozen.  This will cause any
     * calls attempting to add values to block but also allows safe
     * serialization.
     */
    void freezeState()
    { }


    /**
     * Unfreeze the processor after a call to freezeState()
     */
    void unfreezeState()
    { }

protected:
    /**
     * Called when a segment is completed.  Values may still be undefined,
     * however.
     * 
     * @param start     the start time of the segment
     * @param end       the end time of the segment
     * @param values    the values of all streams for the segment
     */
    virtual void segmentDone(double start, double end, const StorageType values[N]) = 0;

    /**
     * Called when all streams have been advanced to the given time without
     * a segment being generated.
     * 
     * @param time      the time being advanced to
     */
    virtual void allStreamsAdvanced(double time) = 0;
};

/**
 * A base class that implements a simple stream segmenting breakdown.  This
 * allows simple combination of multiple inputs that require synchronized
 * processing.  This class requires that all input streams are non-overlapping.
 * That is, it will fail if values sent to it on a given stream overlap with
 * each other.
 * 
 * @param N the number of input streams
 */
template<std::size_t N>
class SmootherChainInputSegmentProcessor : public SmootherChainInputSegmentProcessorBase<N,
                                                                                         double> {
public:
    SmootherChainInputSegmentProcessor()
    { }

    SmootherChainInputSegmentProcessor(QDataStream &stream)
            : SmootherChainInputSegmentProcessorBase<N, double>(stream)
    { }

    virtual ~SmootherChainInputSegmentProcessor()
    { }
};

/**
 * The base implementation for 2D vector breakdowns.  This takes a mangitude
 * and direction and produces a series of X and Y values. 
 * <br>
 * The processor must support two data streams and implement:
 * <ul>
 *  <li>Processor(QDataStream &stream)
 *  <li>Processor::writeSerial(QDataStream &stream)
 *  <li>bool Processor::segmentEnd(std::size_t stream)
 *  <li>Processor::freezeState()
 *  <li>Processor::unfreezeState()
 *  <li>Processor::segmentValue(std::size_t stream, double start, double end, double value)
 * </ul>
 * It is expected to call:
 * <ul>
 *  <li>segmentDone( double, double, const double[2] )
 *  <li>allStreamsAdvanced( double )
 * </ul>
 * @param Processor the processor base class
 */
template<typename Processor>
class SmootherChainVector2DBreakdownBase : public SmootherChainNode, protected Processor {
    SmootherChainTarget *targetX;
    SmootherChainTarget *targetY;

    /* Don't need a mutex for this, since it's only written once we no
     * longer care. */
    bool segmentsFinished;

    class TargetDirection : public SmootherChainTarget {
        SmootherChainVector2DBreakdownBase<Processor> *node;
    public:
        TargetDirection(SmootherChainVector2DBreakdownBase<Processor> *n) : node(n)
        { }

        virtual ~TargetDirection()
        { }

        virtual void incomingData(double start, double end, double value)
        {
            if (FP::defined(value))
                node->segmentValue(1, start, end, (value - 180.0) * 0.0174532925199433);
            else
                node->segmentValue(1, start, end, FP::undefined());
        }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(1, time); }

        virtual void endData()
        { node->streamDone(1); }
    };

    friend class TargetDirection;

    TargetDirection targetDirection;

    class TargetMagnitude : public SmootherChainTarget {
        SmootherChainVector2DBreakdownBase<Processor> *node;
    public:
        TargetMagnitude(SmootherChainVector2DBreakdownBase<Processor> *n) : node(n)
        { }

        virtual ~TargetMagnitude()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(0, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(0, time); }

        virtual void endData()
        { node->streamDone(0); }
    };

    friend class TargetMagnitude;

    TargetMagnitude targetMagnitude;

    void streamDone(std::size_t stream)
    {
        if (Processor::segmentEnd(stream)) {
            if (targetX != NULL)
                targetX->endData();
            if (targetY != NULL)
                targetY->endData();
            segmentsFinished = true;
        }
    }

public:
    /**
     * Create a breakdown node.  Outputs may be NULL to discard the result.
     * 
     * @param outputX           the output target for X vector data
     * @param outputY           the output target for Y vector data
     */
    SmootherChainVector2DBreakdownBase(SmootherChainTarget *outputX, SmootherChainTarget *outputY)
            : targetX(outputX),
              targetY(outputY),
              segmentsFinished(false),
              targetDirection(this),
              targetMagnitude(this)
    { }

    /**
     * Create a breakdown, de-serializing the state.  Outputs may be NULL to 
     * discard the result.
     * 
     * @param stream            the stream to read state from
     * @param outputX           the output target for X vector data
     * @param outputY           the output target for Y vector data
     */
    SmootherChainVector2DBreakdownBase(QDataStream &stream,
                                       SmootherChainTarget *outputX,
                                       SmootherChainTarget *outputY) : Processor(stream),
                                                                       targetX(outputX),
                                                                       targetY(outputY),
                                                                       segmentsFinished(false),
                                                                       targetDirection(this),
                                                                       targetMagnitude(this)
    { }

    virtual bool finished()
    { return segmentsFinished; }

    virtual void serialize(QDataStream &stream) const
    { Processor::writeSerial(stream); }

    virtual void pause()
    { Processor::freezeState(); }

    virtual void resume()
    { Processor::unfreezeState(); }

    /**
     * Get the chain target for incoming direction data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetDirection()
    { return &targetDirection; }

    /**
     * Get the chain target for incoming magnitude data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetMagnitude()
    { return &targetMagnitude; }

protected:
    virtual void segmentDone(double start, double end, const double values[2])
    {
        if (FP::defined(values[0]) && FP::defined(values[1])) {
            if (targetX != NULL)
                targetX->incomingData(start, end, cos(values[1]) * values[0]);
            if (targetY != NULL)
                targetY->incomingData(start, end, sin(values[1]) * values[0]);
        } else {
            if (targetX != NULL)
                targetX->incomingData(start, end, FP::undefined());
            if (targetY != NULL)
                targetY->incomingData(start, end, FP::undefined());
        }
    }

    virtual void allStreamsAdvanced(double time)
    {
        if (targetX != NULL)
            targetX->incomingAdvance(time);
        if (targetY != NULL)
            targetY->incomingAdvance(time);
    }
};

/**
 * A smoother chain element that breaks down a two dimensional vector in
 * the form of a direction and a magnitude into X and Y streams.
 */
class CPD3SMOOTHING_EXPORT SmootherChainVector2DBreakdown
        : public SmootherChainVector2DBreakdownBase<SmootherChainInputSegmentProcessor<2> > {
public:
    /**
     * Create a breakdown node.  Outputs may be NULL to discard the result.
     * 
     * @param outputX           the output target for X vector data
     * @param outputY           the output target for Y vector data
     */
    inline SmootherChainVector2DBreakdown(SmootherChainTarget *outputX,
                                          SmootherChainTarget *outputY)
            : SmootherChainVector2DBreakdownBase<SmootherChainInputSegmentProcessor<2> >(outputX,
                                                                                         outputY)
    { }

    /**
     * Create a breakdown, de-serializing the state.  Outputs may be NULL to 
     * discard the result.
     * 
     * @param stream            the stream to read state from
     * @param outputX           the output target for X vector data
     * @param outputY           the output target for Y vector data
     */
    inline SmootherChainVector2DBreakdown(QDataStream &stream,
                                          SmootherChainTarget *outputX,
                                          SmootherChainTarget *outputY)
            : SmootherChainVector2DBreakdownBase<SmootherChainInputSegmentProcessor<2> >(stream,
                                                                                         outputX,
                                                                                         outputY)
    { }
};


/**
 * The base implementation for 2D vector reconstructors.  This takes a X and Y
 * value and produces a series of magnitudes and directions.
 * <br>
 * The processor must support two data streams and implement:
 * <ul>
 *  <li>Processor(QDataStream &stream)
 *  <li>Processor::writeSerial(QDataStream &stream)
 *  <li>bool Processor::segmentEnd(std::size_t stream)
 *  <li>Processor::freezeState()
 *  <li>Processor::unfreezeState()
 *  <li>Processor::segmentValue(std::size_t stream, double start, double end, double value)
 * </ul>
 * It is expected to call:
 * <ul>
 *  <li>segmentDone( double, double, const double[2] )
 *  <li>allStreamsAdvanced( double )
 * </ul>
 * @param Processor the processor base class
 */
template<typename Processor>
class SmootherChainVector2DReconstructBase : public SmootherChainNode, protected Processor {
    SmootherChainTarget *targetDirection;
    SmootherChainTarget *targetMagnitude;

    /* Don't need a mutex for this, since it's only written once we no
     * longer care. */
    bool segmentsFinished;

    class TargetX : public SmootherChainTarget {
        SmootherChainVector2DReconstructBase<Processor> *node;
    public:
        TargetX(SmootherChainVector2DReconstructBase<Processor> *n) : node(n)
        { }

        virtual ~TargetX()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(0, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(0, time); }

        virtual void endData()
        { node->streamDone(0); }
    };

    friend class TargetX;

    TargetX targetX;

    class TargetY : public SmootherChainTarget {
        SmootherChainVector2DReconstructBase<Processor> *node;
    public:
        TargetY(SmootherChainVector2DReconstructBase<Processor> *n) : node(n)
        { }

        virtual ~TargetY()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(1, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(1, time); }

        virtual void endData()
        { node->streamDone(1); }
    };

    friend class TargetY;

    TargetY targetY;

    void streamDone(std::size_t stream)
    {
        if (Processor::segmentEnd(stream)) {
            if (targetDirection != NULL)
                targetDirection->endData();
            if (targetMagnitude != NULL)
                targetMagnitude->endData();
            segmentsFinished = true;
        }
    }

public:
    /**
     * Create a reconstruction node.  Outputs may be NULL to discard the result.
     * 
     * @param outputDirection   the output target for the direction
     * @param outputMagnitude   the output target for magnitude
     */
    SmootherChainVector2DReconstructBase(SmootherChainTarget *outputDirection,
                                         SmootherChainTarget *outputMagnitude) : targetDirection(
            outputDirection),
                                                                                 targetMagnitude(
                                                                                         outputMagnitude),
                                                                                 segmentsFinished(
                                                                                         false),
                                                                                 targetX(this),
                                                                                 targetY(this)
    { }

    /**
     * Create a reconstruction node from the serialized state.  Outputs may be 
     * NULL to discard the result.
     * 
     * @param stream            the stream to read the state from
     * @param outputDirection   the output target for the direction
     * @param outputMagnitude   the output target for magnitude
     */
    SmootherChainVector2DReconstructBase(QDataStream &stream,
                                         SmootherChainTarget *outputDirection,
                                         SmootherChainTarget *outputMagnitude) : Processor(stream),
                                                                                 targetDirection(
                                                                                         outputDirection),
                                                                                 targetMagnitude(
                                                                                         outputMagnitude),
                                                                                 segmentsFinished(
                                                                                         false),
                                                                                 targetX(this),
                                                                                 targetY(this)
    { }

    virtual bool finished()
    { return segmentsFinished; }

    virtual void serialize(QDataStream &stream) const
    { Processor::writeSerial(stream); }

    virtual void pause()
    { Processor::freezeState(); }

    virtual void resume()
    { Processor::unfreezeState(); }

    /**
     * Get the chain target for incoming vector X data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetX()
    { return &targetX; }

    /**
     * Get the chain target for incoming vector Y data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetY()
    { return &targetY; }

protected:
    virtual void segmentDone(double start, double end, const double values[2])
    {
        if (FP::defined(values[0]) && FP::defined(values[1])) {
            if (targetDirection != NULL) {
                if (values[0] == 0.0 && values[1] == 0.0) {
                    targetDirection->incomingData(start, end, FP::undefined());
                } else {
                    double d = atan2(values[1], values[0]) * 57.2957795130823 + 180.0;
                    if (fabs(d - 360.0) < 1E-10)
                        d = 0.0;
                    targetDirection->incomingData(start, end, d);
                }
            }
            if (targetMagnitude != NULL) {
                targetMagnitude->incomingData(start, end,
                                              sqrt(values[0] * values[0] + values[1] * values[1]));
            }
        } else {
            if (targetDirection != NULL) {
                targetDirection->incomingData(start, end, FP::undefined());
            }
            if (targetMagnitude != NULL) {
                targetMagnitude->incomingData(start, end, FP::undefined());
            }
        }
    }

    virtual void allStreamsAdvanced(double time)
    {
        if (targetDirection != NULL)
            targetDirection->incomingAdvance(time);
        if (targetMagnitude != NULL)
            targetMagnitude->incomingAdvance(time);
    }
};

/**
 * A smoother chain element that reconstructs down a two dimensional vector's
 * magnitude and direction from X and Y inputs.
 */
class CPD3SMOOTHING_EXPORT SmootherChainVector2DReconstruct
        : public SmootherChainVector2DReconstructBase<SmootherChainInputSegmentProcessor<2> > {
public:
    /**
     * Create a reconstruction node.  Outputs may be NULL to discard the result.
     * 
     * @param outputDirection   the output target for the direction
     * @param outputMagnitude   the output target for magnitude
     */
    inline SmootherChainVector2DReconstruct(SmootherChainTarget *outputDirection,
                                            SmootherChainTarget *outputMagnitude)
            : SmootherChainVector2DReconstructBase<SmootherChainInputSegmentProcessor<2> >(
            outputDirection, outputMagnitude)
    { }

    /**
     * Create a reconstruction node from the serialized state.  Outputs may be 
     * NULL to discard the result.
     * 
     * @param stream            the stream to read the state from
     * @param outputDirection   the output target for the direction
     * @param outputMagnitude   the output target for magnitude
     */
    inline SmootherChainVector2DReconstruct(QDataStream &stream,
                                            SmootherChainTarget *outputDirection,
                                            SmootherChainTarget *outputMagnitude)
            : SmootherChainVector2DReconstructBase<SmootherChainInputSegmentProcessor<2> >(stream,
                                                                                           outputDirection,
                                                                                           outputMagnitude)
    { }
};

/**
 * The base implementation for 3D vector breakdowns.  This takes an
 * azimuth angle, elevation angle and magnitude and produces X, Y, and Z
 * vector values.
 * <br>
 * The processor must support three data streams and implement:
 * <ul>
 *  <li>Processor(QDataStream &stream)
 *  <li>Processor::writeSerial(QDataStream &stream)
 *  <li>bool Processor::segmentEnd(std::size_t stream)
 *  <li>Processor::freezeState()
 *  <li>Processor::unfreezeState()
 *  <li>Processor::segmentValue(std::size_t stream, double start, double end, double value)
 * </ul>
 * It is expected to call:
 * <ul>
 *  <li>segmentDone( double, double, const double[3] )
 *  <li>allStreamsAdvanced( double )
 * </ul>
 * @param Processor the processor base class
 */
template<typename Processor>
class SmootherChainVector3DBreakdownBase : public SmootherChainNode, protected Processor {

    SmootherChainTarget *targetX;
    SmootherChainTarget *targetY;
    SmootherChainTarget *targetZ;

    /* Don't need a mutex for this, since it's only written once we no
     * longer care. */
    bool segmentsFinished;

    class TargetAzimuth : public SmootherChainTarget {
        SmootherChainVector3DBreakdownBase<Processor> *node;
    public:
        TargetAzimuth(SmootherChainVector3DBreakdownBase<Processor> *n) : node(n)
        { }

        virtual ~TargetAzimuth()
        { }

        virtual void incomingData(double start, double end, double value)
        {
            if (FP::defined(value)) {
                node->segmentValue(1, start, end, (value - 180.0) * 0.0174532925199433);
            } else {
                node->segmentValue(1, start, end, FP::undefined());
            }
        }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(1, time); }

        virtual void endData()
        { node->streamDone(1); }
    };

    friend class TargetAzimuth;

    TargetAzimuth targetAzimuth;

    class TargetElevation : public SmootherChainTarget {
        SmootherChainVector3DBreakdownBase<Processor> *node;
    public:
        TargetElevation(SmootherChainVector3DBreakdownBase<Processor> *n) : node(n)
        { }

        virtual ~TargetElevation()
        { }

        virtual void incomingData(double start, double end, double value)
        {
            if (FP::defined(value))
                node->segmentValue(2, start, end, value * 0.0174532925199433);
            else
                node->segmentValue(2, start, end, FP::undefined());
        }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(2, time); }

        virtual void endData()
        { node->streamDone(2); }
    };

    friend class TargetElevation;

    TargetElevation targetElevation;

    class TargetMagnitude : public SmootherChainTarget {
        SmootherChainVector3DBreakdownBase<Processor> *node;
    public:
        TargetMagnitude(SmootherChainVector3DBreakdownBase<Processor> *n) : node(n)
        { }

        virtual ~TargetMagnitude()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(0, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(0, time); }

        virtual void endData()
        { node->streamDone(0); }
    };

    friend class TargetMagnitude;

    TargetMagnitude targetMagnitude;

    void streamDone(std::size_t stream)
    {
        if (Processor::segmentEnd(stream)) {
            if (targetX != NULL)
                targetX->endData();
            if (targetY != NULL)
                targetY->endData();
            if (targetZ != NULL)
                targetZ->endData();
            segmentsFinished = true;
        }
    }

public:
    /**
     * Create a breakdown node.  Outputs may be NULL to discard the result.
     * 
     * @param outputX           the output target for X vector data
     * @param outputY           the output target for Y vector data
     * @param outputZ           the output target for Z vector data
     */
    SmootherChainVector3DBreakdownBase(SmootherChainTarget *outputX,
                                       SmootherChainTarget *outputY,
                                       SmootherChainTarget *outputZ) : targetX(outputX),
                                                                       targetY(outputY),
                                                                       targetZ(outputZ),
                                                                       segmentsFinished(false),
                                                                       targetAzimuth(this),
                                                                       targetElevation(this),
                                                                       targetMagnitude(this)
    { }

    /**
     * Create a breakdown node reading the serialized state.  Outputs may be 
     * NULL to discard the result.
     * 
     * @param stream            the stream to read state from
     * @param outputX           the output target for X vector data
     * @param outputY           the output target for Y vector data
     * @param outputZ           the output target for Z vector data
     */
    SmootherChainVector3DBreakdownBase(QDataStream &stream,
                                       SmootherChainTarget *outputX,
                                       SmootherChainTarget *outputY,
                                       SmootherChainTarget *outputZ) : Processor(stream),
                                                                       targetX(outputX),
                                                                       targetY(outputY),
                                                                       targetZ(outputZ),
                                                                       segmentsFinished(false),
                                                                       targetAzimuth(this),
                                                                       targetElevation(this),
                                                                       targetMagnitude(this)
    { }

    virtual bool finished()
    { return segmentsFinished; }

    virtual void serialize(QDataStream &stream) const
    { Processor::writeSerial(stream); }

    virtual void pause()
    { Processor::freezeState(); }

    virtual void resume()
    { Processor::unfreezeState(); }

    /**
     * Get the chain target for incoming azimuth data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetAzimuth()
    { return &targetAzimuth; }

    /**
     * Get the chain target for incoming elevation data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetElevation()
    { return &targetElevation; }

    /**
     * Get the chain target for incoming magnitude data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetMagnitude()
    { return &targetMagnitude; }

protected:
    virtual void segmentDone(double start, double end, const double values[3])
    {
        if (FP::defined(values[0]) && FP::defined(values[1]) && FP::defined(values[2])) {
            double ct = cos(values[2]);
            if (targetX != NULL) {
                targetX->incomingData(start, end, cos(values[1]) * ct * values[0]);
            }
            if (targetY != NULL) {
                targetY->incomingData(start, end, sin(values[1]) * ct * values[0]);
            }
            if (targetZ != NULL) {
                targetZ->incomingData(start, end, sin(values[2]) * values[0]);
            }
        } else {
            if (targetX != NULL)
                targetX->incomingData(start, end, FP::undefined());
            if (targetY != NULL)
                targetY->incomingData(start, end, FP::undefined());
            if (targetZ != NULL)
                targetZ->incomingData(start, end, FP::undefined());
        }
    }

    virtual void allStreamsAdvanced(double time)
    {
        if (targetX != NULL)
            targetX->incomingAdvance(time);
        if (targetY != NULL)
            targetY->incomingAdvance(time);
        if (targetZ != NULL)
            targetZ->incomingAdvance(time);
    }
};

/**
 * A smoother chain element that breaks down a three dimensional vector in
 * the form of a direction and a magnitude into X, Y, and Z streams.
 */
class CPD3SMOOTHING_EXPORT SmootherChainVector3DBreakdown
        : public SmootherChainVector3DBreakdownBase<SmootherChainInputSegmentProcessor<3> > {
public:
    /**
     * Create a breakdown node.  Outputs may be NULL to discard the result.
     * 
     * @param outputX           the output target for X vector data
     * @param outputY           the output target for Y vector data
     * @param outputZ           the output target for Z vector data
     */
    inline SmootherChainVector3DBreakdown(SmootherChainTarget *outputX,
                                          SmootherChainTarget *outputY,
                                          SmootherChainTarget *outputZ)
            : SmootherChainVector3DBreakdownBase<SmootherChainInputSegmentProcessor<3> >(outputX,
                                                                                         outputY,
                                                                                         outputZ)
    { }

    /**
     * Create a breakdown node reading the serialized state.  Outputs may be 
     * NULL to discard the result.
     * 
     * @param stream            the stream to read state from
     * @param outputX           the output target for X vector data
     * @param outputY           the output target for Y vector data
     * @param outputZ           the output target for Z vector data
     */
    inline SmootherChainVector3DBreakdown(QDataStream &stream,
                                          SmootherChainTarget *outputX,
                                          SmootherChainTarget *outputY,
                                          SmootherChainTarget *outputZ)
            : SmootherChainVector3DBreakdownBase<SmootherChainInputSegmentProcessor<3> >(stream,
                                                                                         outputX,
                                                                                         outputY,
                                                                                         outputZ)
    { }
};


/**
 * The base implementation for 3D vector reconstructors.  This takes X, Y,
 * and Z vectors and produces an azimuth angle, elevation angle, and magnitude.
 * <br>
 * The processor must support three data streams and implement:
 * <ul>
 *  <li>Processor(QDataStream &stream)
 *  <li>Processor::writeSerial(QDataStream &stream)
 *  <li>bool Processor::segmentEnd(std::size_t stream)
 *  <li>Processor::freezeState()
 *  <li>Processor::unfreezeState()
 *  <li>Processor::segmentValue(std::size_t stream, double start, double end, double value)
 * </ul>
 * It is expected to call:
 * <ul>
 *  <li>segmentDone( double, double, const double[3] )
 *  <li>allStreamsAdvanced( double )
 * </ul>
 * @param Processor the processor base class
 */
template<typename Processor>
class SmootherChainVector3DReconstructBase : public SmootherChainNode, protected Processor {
    SmootherChainTarget *targetAzimuth;
    SmootherChainTarget *targetElevation;
    SmootherChainTarget *targetMagnitude;

    /* Don't need a mutex for this, since it's only written once we no
     * longer care. */
    bool segmentsFinished;

    class TargetX : public SmootherChainTarget {
        SmootherChainVector3DReconstructBase<Processor> *node;
    public:
        TargetX(SmootherChainVector3DReconstructBase<Processor> *n) : node(n)
        { }

        virtual ~TargetX()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(0, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(0, time); }

        virtual void endData()
        { node->streamDone(0); }
    };

    friend class TargetX;

    TargetX targetX;

    class TargetY : public SmootherChainTarget {
        SmootherChainVector3DReconstructBase<Processor> *node;
    public:
        TargetY(SmootherChainVector3DReconstructBase<Processor> *n) : node(n)
        { }

        virtual ~TargetY()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(1, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(1, time); }

        virtual void endData()
        { node->streamDone(1); }
    };

    friend class TargetY;

    TargetY targetY;

    class TargetZ : public SmootherChainTarget {
        SmootherChainVector3DReconstructBase<Processor> *node;
    public:
        TargetZ(SmootherChainVector3DReconstructBase<Processor> *n) : node(n)
        { }

        virtual ~TargetZ()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(2, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(2, time); }

        virtual void endData()
        { node->streamDone(2); }
    };

    friend class TargetZ;

    TargetZ targetZ;

    void streamDone(std::size_t stream)
    {
        if (Processor::segmentEnd(stream)) {
            if (targetAzimuth != NULL)
                targetAzimuth->endData();
            if (targetElevation != NULL)
                targetElevation->endData();
            if (targetMagnitude != NULL)
                targetMagnitude->endData();
            segmentsFinished = true;
        }
    }

public:
    /**
     * Create a reconstruction node.  Outputs may be NULL to discard the result.
     * 
     * @param outputAzimuth     the output target for the azimuth angle
     * @param outputElevation   the output target for the elevation angle
     * @param outputMagnitude   the output target for magnitude
     */
    SmootherChainVector3DReconstructBase(SmootherChainTarget *outputAzimuth,
                                         SmootherChainTarget *outputElevation,
                                         SmootherChainTarget *outputMagnitude) : targetAzimuth(
            outputAzimuth),
                                                                                 targetElevation(
                                                                                         outputElevation),
                                                                                 targetMagnitude(
                                                                                         outputMagnitude),
                                                                                 segmentsFinished(
                                                                                         false),
                                                                                 targetX(this),
                                                                                 targetY(this),
                                                                                 targetZ(this)
    { }

    /**
     * Create a reconstruction node reading the serialized state.  Outputs may 
     * be NULL to discard the result.
     * 
     * @param stream            the stream to read state from
     * @param outputAzimuth     the output target for the azimuth angle
     * @param outputElevation   the output target for the elevation angle
     * @param outputMagnitude   the output target for magnitude
     */
    SmootherChainVector3DReconstructBase(QDataStream &stream,
                                         SmootherChainTarget *outputAzimuth,
                                         SmootherChainTarget *outputElevation,
                                         SmootherChainTarget *outputMagnitude) : Processor(stream),
                                                                                 targetAzimuth(
                                                                                         outputAzimuth),
                                                                                 targetElevation(
                                                                                         outputElevation),
                                                                                 targetMagnitude(
                                                                                         outputMagnitude),
                                                                                 segmentsFinished(
                                                                                         false),
                                                                                 targetX(this),
                                                                                 targetY(this),
                                                                                 targetZ(this)
    { }

    virtual bool finished()
    { return segmentsFinished; }

    virtual void serialize(QDataStream &stream) const
    { Processor::writeSerial(stream); }

    virtual void pause()
    { Processor::freezeState(); }

    virtual void resume()
    { Processor::unfreezeState(); }

    /**
     * Get the chain target for incoming vector X data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetX()
    { return &targetX; }

    /**
     * Get the chain target for incoming vector Y data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetY()
    { return &targetY; }

    /**
     * Get the chain target for incoming vector Z data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetZ()
    { return &targetZ; }

protected:
    virtual void segmentDone(double start, double end, const double values[3])
    {
        if (FP::defined(values[0]) && FP::defined(values[1]) && FP::defined(values[2])) {
            double r = sqrt(values[0] * values[0] + values[1] * values[1] + values[2] * values[2]);
            if (targetMagnitude != NULL)
                targetMagnitude->incomingData(start, end, r);
            if (r == 0.0) {
                if (targetAzimuth != NULL)
                    targetAzimuth->incomingData(start, end, FP::undefined());
                if (targetElevation != NULL)
                    targetElevation->incomingData(start, end, FP::undefined());
            } else {
                if (targetAzimuth != NULL) {
                    double d = atan2(values[1], values[0]) * 57.2957795130823 + 180.0;
                    if (fabs(d - 360.0) < 1E-10 ||
                            (fabs(values[0]) < 1E-10 && fabs(values[1]) < 1E-10))
                        d = 0.0;
                    targetAzimuth->incomingData(start, end, d);
                }
                if (targetElevation != NULL) {
                    targetElevation->incomingData(start, end,
                                                  asin(values[2] / r) * 57.2957795130823);
                }
            }
        } else {
            if (targetAzimuth != NULL) {
                targetAzimuth->incomingData(start, end, FP::undefined());
            }
            if (targetElevation != NULL) {
                targetElevation->incomingData(start, end, FP::undefined());
            }
            if (targetMagnitude != NULL) {
                targetMagnitude->incomingData(start, end, FP::undefined());
            }
        }
    }

    virtual void allStreamsAdvanced(double time)
    {
        if (targetAzimuth != NULL)
            targetAzimuth->incomingAdvance(time);
        if (targetElevation != NULL)
            targetElevation->incomingAdvance(time);
        if (targetMagnitude != NULL)
            targetMagnitude->incomingAdvance(time);
    }
};

/**
 * A smoother chain element that reconstructs down a three dimensional vector's
 * azimuth, elevation and magnitude from X, Y, and Z inputs.
 */
class CPD3SMOOTHING_EXPORT SmootherChainVector3DReconstruct
        : public SmootherChainVector3DReconstructBase<SmootherChainInputSegmentProcessor<3> > {
public:
    /**
     * Create a reconstruction node.  Outputs may be NULL to discard the result.
     * 
     * @param outputAzimuth     the output target for the azimuth angle
     * @param outputElevation   the output target for the elevation angle
     * @param outputMagnitude   the output target for magnitude
     */
    inline SmootherChainVector3DReconstruct(SmootherChainTarget *outputAzimuth,
                                            SmootherChainTarget *outputElevation,
                                            SmootherChainTarget *outputMagnitude)
            : SmootherChainVector3DReconstructBase<SmootherChainInputSegmentProcessor<3> >(
            outputAzimuth, outputElevation, outputMagnitude)
    { }

    /**
     * Create a reconstruction node reading the serialized state.  Outputs may 
     * be NULL to discard the result.
     * 
     * @param stream            the stream to read state from
     * @param outputAzimuth     the output target for the azimuth angle
     * @param outputElevation   the output target for the elevation angle
     * @param outputMagnitude   the output target for magnitude
     */
    inline SmootherChainVector3DReconstruct(QDataStream &stream,
                                            SmootherChainTarget *outputAzimuth,
                                            SmootherChainTarget *outputElevation,
                                            SmootherChainTarget *outputMagnitude)
            : SmootherChainVector3DReconstructBase<SmootherChainInputSegmentProcessor<3> >(stream,
                                                                                           outputAzimuth,
                                                                                           outputElevation,
                                                                                           outputMagnitude)
    { }
};


/**
 * The base implementation for Beer's law based absorption calculators.  
 * This takes a start L, end L, start I and end I and produces an
 * absorption coefficient.
 * <br>
 * The processor must support four data streams and implement:
 * <ul>
 *  <li>Processor(QDataStream &stream)
 *  <li>Processor::writeSerial(QDataStream &stream)
 *  <li>bool Processor::segmentEnd(std::size_t stream)
 *  <li>Processor::freezeState()
 *  <li>Processor::unfreezeState()
 *  <li>Processor::segmentValue(std::size_t stream, double start, double end, double value)
 * </ul>
 * It is expected to call:
 * <ul>
 *  <li>segmentDone( double, double, const double[4] )
 *  <li>allStreamsAdvanced( double )
 * </ul>
 * @param Processor the processor base class
 */
template<typename Processor>
class SmootherChainBeersLawAbsorptionBase : public SmootherChainNode, protected Processor {
    SmootherChainTarget *targetAbsorption;

    /* Don't need a mutex for this, since it's only written once we no
     * longer care. */
    bool segmentsFinished;

    class TargetStartL : public SmootherChainTarget {
        SmootherChainBeersLawAbsorptionBase<Processor> *node;
    public:
        TargetStartL(SmootherChainBeersLawAbsorptionBase<Processor> *n) : node(n)
        { }

        virtual ~TargetStartL()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(0, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(0, time); }

        virtual void endData()
        { node->streamDone(0); }
    };

    friend class TargetStartL;

    TargetStartL targetStartL;

    class TargetEndL : public SmootherChainTarget {
        SmootherChainBeersLawAbsorptionBase<Processor> *node;
    public:
        TargetEndL(SmootherChainBeersLawAbsorptionBase<Processor> *n) : node(n)
        { }

        virtual ~TargetEndL()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(1, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(1, time); }

        virtual void endData()
        { node->streamDone(1); }
    };

    friend class TargetEndL;

    TargetEndL targetEndL;

    class TargetStartI : public SmootherChainTarget {
        SmootherChainBeersLawAbsorptionBase<Processor> *node;
    public:
        TargetStartI(SmootherChainBeersLawAbsorptionBase<Processor> *n) : node(n)
        { }

        virtual ~TargetStartI()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(2, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(2, time); }

        virtual void endData()
        { node->streamDone(2); }
    };

    friend class TargetStartI;

    TargetStartI targetStartI;

    class TargetEndI : public SmootherChainTarget {
        SmootherChainBeersLawAbsorptionBase<Processor> *node;
    public:
        TargetEndI(SmootherChainBeersLawAbsorptionBase<Processor> *n) : node(n)
        { }

        virtual ~TargetEndI()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(3, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(3, time); }

        virtual void endData()
        { node->streamDone(3); }
    };

    friend class TargetEndI;

    TargetEndI targetEndI;

    void streamDone(std::size_t stream)
    {
        if (Processor::segmentEnd(stream)) {
            targetAbsorption->endData();
            segmentsFinished = true;
        }
    }

public:
    /**
     * Create a calculation.  The output may not be NULL.
     * 
     * @param outputAbsorption  the output target for the absorption coefficient
     */
    SmootherChainBeersLawAbsorptionBase(SmootherChainTarget *outputAbsorption) : targetAbsorption(
            outputAbsorption),
                                                                                 targetStartL(this),
                                                                                 targetEndL(this),
                                                                                 targetStartI(this),
                                                                                 targetEndI(this)
    { Q_ASSERT(targetAbsorption != NULL); }

    /**
     * Create a calculation, reading the serialized state.  The output may not 
     * be NULL.
     * 
     * @param stream            the stream to read state from
     * @param outputAbsorption  the output target for the absorption coefficient
     */
    SmootherChainBeersLawAbsorptionBase(QDataStream &stream, SmootherChainTarget *outputAbsorption)
            : Processor(stream),
              targetAbsorption(outputAbsorption),
              targetStartL(this),
              targetEndL(this),
              targetStartI(this),
              targetEndI(this)
    { Q_ASSERT(targetAbsorption != NULL); }

    virtual bool finished()
    { return segmentsFinished; }

    virtual void serialize(QDataStream &stream) const
    { Processor::writeSerial(stream); }

    virtual void pause()
    { Processor::freezeState(); }

    virtual void resume()
    { Processor::unfreezeState(); }

    /**
     * Get the chain target for incoming start L data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetStartL()
    { return &targetStartL; }

    /**
     * Get the chain target for incoming end L data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetEndL()
    { return &targetEndL; }

    /**
     * Get the chain target for incoming start I data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetStartI()
    { return &targetStartI; }

    /**
     * Get the chain target for incoming end I data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetEndI()
    { return &targetEndI; }

protected:
    virtual void segmentDone(double start, double end, const double values[4])
    {
        if (!FP::defined(values[0]) ||
                !FP::defined(values[1]) ||
                !FP::defined(values[2]) ||
                !FP::defined(values[3]) ||
                values[3] == 0.0 ||
                values[2] == 0.0) {
            targetAbsorption->incomingData(start, end, FP::undefined());
        } else {
            double r = values[2] / values[3];
            if (r <= 0.0) {
                targetAbsorption->incomingData(start, end, FP::undefined());
            } else {
                targetAbsorption->incomingData(start, end,
                                               (1E6 * log(r)) / (values[1] - values[0]));
            }
        }
    }

    virtual void allStreamsAdvanced(double time)
    { targetAbsorption->incomingAdvance(time); }
};


/**
 * A smoother chain element that calculates an absorption coefficient
 * from a start sample length (L), end sample length, start 
 * transmittance/intensity (I), and end transmittance/intensity.
 */
class CPD3SMOOTHING_EXPORT SmootherChainBeersLawAbsorption
        : public SmootherChainBeersLawAbsorptionBase<SmootherChainInputSegmentProcessor<4> > {
public:
    /**
     * Create a calculation.  The output may not be NULL.
     * 
     * @param outputAbsorption  the output target for the absorption coefficient
     */
    inline SmootherChainBeersLawAbsorption(SmootherChainTarget *outputAbsorption)
            : SmootherChainBeersLawAbsorptionBase<SmootherChainInputSegmentProcessor<4> >(
            outputAbsorption)
    { }

    /**
     * Create a calculation, reading the serialized state.  The output may not 
     * be NULL.
     * 
     * @param stream            the stream to read state from
     * @param outputAbsorption  the output target for the absorption coefficient
     */
    inline SmootherChainBeersLawAbsorption(QDataStream &stream,
                                           SmootherChainTarget *outputAbsorption)
            : SmootherChainBeersLawAbsorptionBase<SmootherChainInputSegmentProcessor<4> >(stream,
                                                                                          outputAbsorption)
    { }
};


/**
 * The base implementation for dew point calculators.  This takes the
 * temperature and RH as inputs and produces a dew point.
 * <br>
 * The processor must support two data streams and implement:
 * <ul>
 *  <li>Processor(QDataStream &stream)
 *  <li>Processor::writeSerial(QDataStream &stream)
 *  <li>bool Processor::segmentEnd(std::size_t stream)
 *  <li>Processor::freezeState()
 *  <li>Processor::unfreezeState()
 *  <li>Processor::segmentValue(std::size_t stream, double start, double end, double value)
 * </ul>
 * It is expected to call:
 * <ul>
 *  <li>segmentDone( double, double, const double[2] )
 *  <li>allStreamsAdvanced( double )
 * </ul>
 * @param Processor the processor base class
 */
template<typename Processor>
class SmootherChainDewpointBase : public SmootherChainNode, protected Processor {
    SmootherChainTarget *targetDewpoint;
    bool forceWater;

    /* Don't need a mutex for this, since it's only written once we no
     * longer care. */
    bool segmentsFinished;

    class TargetTemperature : public SmootherChainTarget {
        SmootherChainDewpointBase<Processor> *node;
    public:
        TargetTemperature(SmootherChainDewpointBase<Processor> *n) : node(n)
        { }

        virtual ~TargetTemperature()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(0, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(0, time); }

        virtual void endData()
        { node->streamDone(0); }
    };

    friend class TargetTemperature;

    TargetTemperature targetTemperature;

    class TargetRH : public SmootherChainTarget {
        SmootherChainDewpointBase<Processor> *node;
    public:
        TargetRH(SmootherChainDewpointBase<Processor> *n) : node(n)
        { }

        virtual ~TargetRH()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(1, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(1, time); }

        virtual void endData()
        { node->streamDone(1); }
    };

    friend class TargetRH;

    TargetRH targetRH;

    void streamDone(std::size_t stream)
    {
        if (Processor::segmentEnd(stream)) {
            targetDewpoint->endData();
            segmentsFinished = true;
        }
    }

public:
    /**
     * Create a calculation.  The output may not be NULL.
     * 
     * @param outputDewpoint    the output target for the dewpoint
     * @param setForceWater     if set the always dew the calculation above water instead of selecting above ice as needed
     */
    SmootherChainDewpointBase(SmootherChainTarget *outputDewpoint, bool setForceWater = false)
            : targetDewpoint(outputDewpoint),
              forceWater(setForceWater),
              segmentsFinished(false),
              targetTemperature(this),
              targetRH(this)
    { Q_ASSERT(targetDewpoint != NULL); }

    /**
     * Create a calculation, reading the serialized state.  The output may not 
     * be NULL.
     * 
     * @param stream            the stream to read state from
     * @param outputDewpoint    outputDewpoint    the output target for the dewpoint
     */
    SmootherChainDewpointBase(QDataStream &stream, SmootherChainTarget *outputDewpoint) : Processor(
            stream),
                                                                                          targetDewpoint(
                                                                                                  outputDewpoint),
                                                                                          forceWater(
                                                                                                  false),
                                                                                          segmentsFinished(
                                                                                                  false),
                                                                                          targetTemperature(
                                                                                                  this),
                                                                                          targetRH(
                                                                                                  this)
    {
        Q_ASSERT(targetDewpoint != NULL);
        stream >> forceWater;
    }

    virtual bool finished()
    { return segmentsFinished; }

    virtual void serialize(QDataStream &stream) const
    {
        Processor::writeSerial(stream);
        stream << forceWater;
    }

    virtual void pause()
    { Processor::freezeState(); }

    virtual void resume()
    { Processor::unfreezeState(); }

    /**
     * Get the chain target for incoming temperature data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetTemperature()
    { return &targetTemperature; }

    /**
     * Get the chain target for incoming RH data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetRH()
    { return &targetRH; }

protected:
    virtual void segmentDone(double start, double end, const double values[2])
    {
        targetDewpoint->incomingData(start, end,
                                     Algorithms::Dewpoint::dewpoint(values[0], values[1]));
    }

    virtual void allStreamsAdvanced(double time)
    { targetDewpoint->incomingAdvance(time); }
};


/**
 * A smoother chain element that calculates a dewpoint from a temperature
 * and RH.
 */
class CPD3SMOOTHING_EXPORT SmootherChainDewpoint : public SmootherChainDewpointBase<
        SmootherChainInputSegmentProcessor<2> > {
public:
    /**
     * Create a calculation.  The output may not be NULL.
     * 
     * @param outputDewpoint    the output target for the dewpoint
     * @param setForceWater     if set the always dew the calculation above water instead of selecting above ice as needed
     */
    inline SmootherChainDewpoint(SmootherChainTarget *outputDewpoint, bool setForceWater = false)
            : SmootherChainDewpointBase<SmootherChainInputSegmentProcessor<2> >(outputDewpoint,
                                                                                setForceWater)
    { }

    /**
     * Create a calculation, reading the serialized state.  The output may not 
     * be NULL.
     * 
     * @param stream            the stream to read state from
     * @param outputDewpoint    the output target for the dewpoint
     */
    inline SmootherChainDewpoint(QDataStream &stream, SmootherChainTarget *outputDewpoint)
            : SmootherChainDewpointBase<SmootherChainInputSegmentProcessor<2> >(stream,
                                                                                outputDewpoint)
    { }
};


/**
 * The base implementation for RH calculators.  This takes the
 * temperature and dew point as inputs and produces a RH.
 * <br>
 * The processor must support two data streams and implement:
 * <ul>
 *  <li>Processor(QDataStream &stream)
 *  <li>Processor::writeSerial(QDataStream &stream)
 *  <li>bool Processor::segmentEnd(std::size_t stream)
 *  <li>Processor::freezeState()
 *  <li>Processor::unfreezeState()
 *  <li>Processor::segmentValue(std::size_t stream, double start, double end, double value)
 * </ul>
 * It is expected to call:
 * <ul>
 *  <li>segmentDone( double, double, const double[2] )
 *  <li>allStreamsAdvanced( double )
 * </ul>
 * @param Processor the processor base class
 */
template<typename Processor>
class SmootherChainRHBase : public SmootherChainNode, protected Processor {
    SmootherChainTarget *targetRH;
    bool forceWater;

    /* Don't need a mutex for this, since it's only written once we no
     * longer care. */
    bool segmentsFinished;

    class TargetTemperature : public SmootherChainTarget {
        SmootherChainRHBase<Processor> *node;
    public:
        TargetTemperature(SmootherChainRHBase<Processor> *n) : node(n)
        { }

        virtual ~TargetTemperature()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(0, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(0, time); }

        virtual void endData()
        { node->streamDone(0); }
    };

    friend class TargetTemperature;

    TargetTemperature targetTemperature;

    class TargetDewpoint : public SmootherChainTarget {
        SmootherChainRHBase<Processor> *node;
    public:
        TargetDewpoint(SmootherChainRHBase<Processor> *n) : node(n)
        { }

        virtual ~TargetDewpoint()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(1, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(1, time); }

        virtual void endData()
        { node->streamDone(1); }
    };

    friend class TargetDewpoint;

    TargetDewpoint targetDewpoint;

    void streamDone(std::size_t stream)
    {
        if (Processor::segmentEnd(stream)) {
            targetRH->endData();
            segmentsFinished = true;
        }
    }

public:
    /**
     * Create a calculation.  The output may not be NULL.
     * 
     * @param outputRH          the output target for the RH
     * @param setForceWater     if set the always dew the calculation above water instead of selecting above ice as needed
     */
    SmootherChainRHBase(SmootherChainTarget *outputRH, bool setForceWater = false) : targetRH(
            outputRH),
                                                                                     forceWater(
                                                                                             setForceWater),
                                                                                     segmentsFinished(
                                                                                             false),
                                                                                     targetTemperature(
                                                                                             this),
                                                                                     targetDewpoint(
                                                                                             this)
    { Q_ASSERT(targetRH != NULL); }

    /**
     * Create a calculation, reading the serialized state.  The output may not 
     * be NULL.
     * 
     * @param stream            the stream to read state from
     * @param outputRH          the output target for the RH
     */
    SmootherChainRHBase(QDataStream &stream, SmootherChainTarget *outputRH) : Processor(stream),
                                                                              targetRH(outputRH),
                                                                              forceWater(false),
                                                                              segmentsFinished(
                                                                                      false),
                                                                              targetTemperature(
                                                                                      this),
                                                                              targetDewpoint(this)
    {
        Q_ASSERT(targetRH != NULL);
        stream >> forceWater;
    }

    virtual bool finished()
    { return segmentsFinished; }

    virtual void serialize(QDataStream &stream) const
    {
        Processor::writeSerial(stream);
        stream << forceWater;
    }

    virtual void pause()
    { Processor::freezeState(); }

    virtual void resume()
    { Processor::unfreezeState(); }

    /**
     * Get the chain target for incoming temperature data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetTemperature()
    { return &targetTemperature; }

    /**
     * Get the chain target for incoming dew point data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetDewpoint()
    { return &targetDewpoint; }

protected:
    virtual void segmentDone(double start, double end, const double values[2])
    {
        targetRH->incomingData(start, end, Algorithms::Dewpoint::rh(values[0], values[1]));
    }

    virtual void allStreamsAdvanced(double time)
    { targetRH->incomingAdvance(time); }
};


/**
 * A smoother chain element that calculates a RH from a temperature
 * and dew point.
 */
class CPD3SMOOTHING_EXPORT SmootherChainRH : public SmootherChainRHBase<
        SmootherChainInputSegmentProcessor<2> > {
public:
    /**
     * Create a calculation.  The output may not be NULL.
     * 
     * @param outputRH          the output target for the RH
     * @param setForceWater     if set the always dew the calculation above water instead of selecting above ice as needed
     */
    inline SmootherChainRH(SmootherChainTarget *outputRH, bool setForceWater = false)
            : SmootherChainRHBase<SmootherChainInputSegmentProcessor<2> >(outputRH, setForceWater)
    { }

    /**
     * Create a calculation, reading the serialized state.  The output may not 
     * be NULL.
     * 
     * @param stream      the stream to read state from
     * @param outputRH    the output target for the RH
     */
    inline SmootherChainRH(QDataStream &stream, SmootherChainTarget *outputRH)
            : SmootherChainRHBase<SmootherChainInputSegmentProcessor<2> >(stream, outputRH)
    { }
};


/**
 * The base implementation for RH extrapolation.  This takes as input the
 * temperature and RH at one point and the temperature at another point
 * and calculates the RH at the second point assuming a constant dew point.
 * <br>
 * The processor must support three data streams and implement:
 * <ul>
 *  <li>Processor(QDataStream &stream)
 *  <li>Processor::writeSerial(QDataStream &stream)
 *  <li>bool Processor::segmentEnd(std::size_t stream)
 *  <li>Processor::freezeState()
 *  <li>Processor::unfreezeState()
 *  <li>Processor::segmentValue(std::size_t stream, double start, double end, double value)
 * </ul>
 * It is expected to call:
 * <ul>
 *  <li>segmentDone( double, double, const double[3] )
 *  <li>allStreamsAdvanced( double )
 * </ul>
 * @param Processor the processor base class
 */
template<typename Processor>
class SmootherChainRHExtrapolateBase : public SmootherChainNode, protected Processor {
    SmootherChainTarget *targetRH;
    bool forceWater;

    /* Don't need a mutex for this, since it's only written once we no
     * longer care. */
    bool segmentsFinished;

    class TargetTemperatureIn : public SmootherChainTarget {
        SmootherChainRHExtrapolateBase<Processor> *node;
    public:
        TargetTemperatureIn(SmootherChainRHExtrapolateBase<Processor> *n) : node(n)
        { }

        virtual ~TargetTemperatureIn()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(0, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(0, time); }

        virtual void endData()
        { node->streamDone(0); }
    };

    friend class TargetTemperatureIn;

    TargetTemperatureIn targetTemperatureIn;

    class TargetRHIn : public SmootherChainTarget {
        SmootherChainRHExtrapolateBase<Processor> *node;
    public:
        TargetRHIn(SmootherChainRHExtrapolateBase<Processor> *n) : node(n)
        { }

        virtual ~TargetRHIn()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(1, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(1, time); }

        virtual void endData()
        { node->streamDone(1); }
    };

    friend class TargetRHIn;

    TargetRHIn targetRHIn;

    class TargetTemperatureOut : public SmootherChainTarget {
        SmootherChainRHExtrapolateBase<Processor> *node;
    public:
        TargetTemperatureOut(SmootherChainRHExtrapolateBase<Processor> *n) : node(n)
        { }

        virtual ~TargetTemperatureOut()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(2, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(2, time); }

        virtual void endData()
        { node->streamDone(2); }
    };

    friend class TargetTemperatureOut;

    TargetTemperatureOut targetTemperatureOut;

    void streamDone(std::size_t stream)
    {
        if (Processor::segmentEnd(stream)) {
            targetRH->endData();
            segmentsFinished = true;
        }
    }

public:
    /**
     * Create a calculation.  The output may not be NULL.
     * 
     * @param outputRH          the output target for the RH
     * @param setForceWater     if set the always dew the calculation above water instead of selecting above ice as needed
     */
    SmootherChainRHExtrapolateBase(SmootherChainTarget *outputRH, bool setForceWater = false)
            : targetRH(outputRH),
              forceWater(setForceWater),
              segmentsFinished(false),
              targetTemperatureIn(this),
              targetRHIn(this),
              targetTemperatureOut(this)
    { Q_ASSERT(targetRH != NULL); }

    /**
     * Create a calculation, reading the serialized state.  The output may not 
     * be NULL.
     * 
     * @param stream            the stream to read state from
     * @param outputRH          the output target for the RH
     */
    SmootherChainRHExtrapolateBase(QDataStream &stream, SmootherChainTarget *outputRH) : Processor(
            stream),
                                                                                         targetRH(
                                                                                                 outputRH),
                                                                                         forceWater(
                                                                                                 false),
                                                                                         segmentsFinished(
                                                                                                 false),
                                                                                         targetTemperatureIn(
                                                                                                 this),
                                                                                         targetRHIn(
                                                                                                 this),
                                                                                         targetTemperatureOut(
                                                                                                 this)
    {
        Q_ASSERT(targetRH != NULL);
        stream >> forceWater;
    }

    virtual bool finished()
    { return segmentsFinished; }

    virtual void serialize(QDataStream &stream) const
    {
        Processor::writeSerial(stream);
        stream << forceWater;
    }

    virtual void pause()
    { Processor::freezeState(); }

    virtual void resume()
    { Processor::unfreezeState(); }

    /**
     * Get the chain target for incoming temperature data at the combined
     * sensor location.  The returned pointer is owned by this object and 
     * should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetTemperatureIn()
    { return &targetTemperatureIn; }

    /**
     * Get the chain target for incoming RH data at the combined sensor 
     * location.  The returned pointer is owned by this object and should not 
     * be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetRHIn()
    { return &targetRHIn; }

    /**
     * Get the chain target for incoming temperature data at output location.  
     * The returned pointer is owned by this object and  should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetTemperatureOut()
    { return &targetTemperatureOut; }

protected:
    virtual void segmentDone(double start, double end, const double values[3])
    {
        targetRH->incomingData(start, end, Algorithms::Dewpoint::rhExtrapolate(values[0], values[1],
                                                                               values[2]));
    }

    virtual void allStreamsAdvanced(double time)
    { targetRH->incomingAdvance(time); }
};


/**
 * A smoother chain element that calculates a RH from a temperature
 * and dew point.
 */
class CPD3SMOOTHING_EXPORT SmootherChainRHExtrapolate : public SmootherChainRHExtrapolateBase<
        SmootherChainInputSegmentProcessor<3> > {
public:
    /**
     * Create a calculation.  The output may not be NULL.
     * 
     * @param outputRH          the output target for the RH
     * @param setForceWater     if set the always dew the calculation above water instead of selecting above ice as needed
     */
    inline SmootherChainRHExtrapolate(SmootherChainTarget *outputRH, bool setForceWater = false)
            : SmootherChainRHExtrapolateBase<SmootherChainInputSegmentProcessor<3> >(outputRH,
                                                                                     setForceWater)
    { }

    /**
     * Create a calculation, reading the serialized state.  The output may not 
     * be NULL.
     * 
     * @param stream      the stream to read state from
     * @param outputRH    the output target for the RH
     */
    inline SmootherChainRHExtrapolate(QDataStream &stream, SmootherChainTarget *outputRH)
            : SmootherChainRHExtrapolateBase<SmootherChainInputSegmentProcessor<3> >(stream,
                                                                                     outputRH)
    { }
};


/**
 * A simple chain node that generates differences of consecutive values.  That 
 * is, it takes a single input and produces two outputs.  The "start" output
 * is the value at the start time of the segment and the "end" is the output
 * at the end of the segment (which is the value at the start of the next
 * segment).
 */
class CPD3SMOOTHING_EXPORT SmootherChainConsecutiveDifference : public SmootherChainNode {
    /* Don't need a mutex since this only has one input, so it's always
     * serial anyway. */
    double previousValue;
    double previousStart;
    double previousEnd;
    bool valuePending;
    bool ended;

    SmootherChainTarget *targetStart;
    SmootherChainTarget *targetEnd;

    class TargetInput : public SmootherChainTarget {
        SmootherChainConsecutiveDifference *node;
    public:
        TargetInput(SmootherChainConsecutiveDifference *n);

        virtual ~TargetInput();

        virtual void incomingData(double start, double end, double value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class TargetInput;

    TargetInput input;

    void handleData(double start, double end, double value);

    void handleAdvance(double time);

    void handleEnd();

public:
    /**
     * Create a new consecutive differencer.
     * 
     * @param outputStart   the output for the "start" values or NULL
     * @param outputEnd     the output for the "end" values or NULL
     */
    SmootherChainConsecutiveDifference
            (SmootherChainTarget *outputStart, SmootherChainTarget *outputEnd);

    /**
     * Create a new consecutive differencer from saved state.
     * 
     * @param stream        the stream to load state from
     * @param outputStart   the output for the "start" values or NULL
     * @param outputEnd     the output for the "end" values or NULL
     */
    SmootherChainConsecutiveDifference
            (QDataStream &stream, SmootherChainTarget *outputStart, SmootherChainTarget *outputEnd);

    virtual bool finished();

    virtual void serialize(QDataStream &stream) const;

    /**
     * Get the chain target for incoming data.  The returned pointer is owned 
     * by this object and  should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTarget()
    { return &input; }
};

}
}

#endif
