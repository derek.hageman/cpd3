/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGBLOCKSMOOTHERCHAIN_H
#define CPD3SMOOTHINGBLOCKSMOOTHERCHAIN_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QDataStream>

#include "smoothing/smoothing.hxx"
#include "smoothing/smoothingchain.hxx"
#include "datacore/dynamictimeinterval.hxx"

namespace CPD3 {
namespace Smoothing {

namespace Internal {
class BlockSmootherChainTask;
}

/**
 * The base class that implements the buffering and dispatch for
 * block buffering smoothers.
 */
class CPD3SMOOTHING_EXPORT BlockSmootherChain : public SmootherChainNode {
    friend class Internal::BlockSmootherChainTask;

protected:

    /** The possible modes to handle undefined values. */
    enum UndefinedMode {
        /** 
         * Dump the buffer completely.  That is, undefined values cause
         * gaps and break up the data stream.  They are not passed through
         * to the handler.
         */
                DumpBuffer = 0,

        /**
         * Pass undefined values through like any other value.
         */
                PassThrough,

        /**
         * Remove undefined values without forcing a data gap.  A gap may
         * still be emitted if the gap specification is exceeded.
         */
                Remove,
    };
private:

    SmootherChainTarget *output;
    Data::DynamicTimeInterval *gap;
    double previousTime;

    class TargetInput : public SmootherChainTarget {
        BlockSmootherChain *node;
    public:
        TargetInput(BlockSmootherChain *n);

        virtual ~TargetInput();

        virtual void incomingData(double start, double end, double value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class TargetInput;

    TargetInput targetInput;

    int maximumBuffer;
    UndefinedMode undefinedMode;

    std::mutex mutex;
    Internal::BlockSmootherChainTask *runningTask;
    QList<Internal::BlockSmootherChainTask *> processingQueue;
    bool pendingSendEnd;
    double pendingAdvanceTime;
    bool outputEnded;
    std::condition_variable internalWait;

    void taskFinished(bool finisher);

    void dumpBuffer(size_t count = 0);

    void handleValue(double start, double end, double value);

    void handleAdvance(double time);

    void handleEnd();

public:
    /**
     * Create a new block smoother chain node.  This takes ownership of gap
     * specification.
     * 
     * @param setGap            the maximum time to allow or NULL for no maximum
     * @param outputTarget      the output target for smoothed values
     */
    BlockSmootherChain(Data::DynamicTimeInterval *setGap, SmootherChainTarget *outputTarget);

    /**
     * Create a new block smoother chain from the saved state.
     * 
     * @param stream            the stream to read state from
     * @param outputTarget      the output target for smoothed values
     */
    BlockSmootherChain(QDataStream &stream, SmootherChainTarget *outputTarget);

    virtual ~BlockSmootherChain();

    /**
     * Get the chain target to send values to.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTarget()
    { return &targetInput; }

    virtual bool finished();

    virtual void serialize(QDataStream &stream) const;

    virtual void pause();

    virtual void resume();

    virtual void signalTerminate();

    virtual void cleanup();

    /**
     * A value in the buffer.  This will auto-cast to and from its double
     * value member.
     */
    struct CPD3SMOOTHING_EXPORT Value {
        /** The output value of this item. */
        double value;
        /** The start time of this item. */
        double start;
        /** The end time of this item. */
        double end;

        /**
         * Get the start time.
         * @return the start time
         */
        inline double getStart() const
        { return start; }

        /**
         * Get the end time.
         * @return the end time
         */
        inline double getEnd() const
        { return end; }

        /**
         * Get the value.
         * @return the value
         */
        inline double getValue() const
        { return value; }

        /** Auto casting to the "value" member. */
        inline operator double() const
        { return value; }

        /** Auto casting to the "value" member. */
        inline operator double &()
        { return value; }

        /** Set the "value" member. */
        inline Value &operator=(double v)
        {
            value = v;
            return *this;
        }
    };

    /**
     * A handler called on the data buffer.
     */
    class CPD3SMOOTHING_EXPORT Handler {
    public:
        Handler();

        virtual ~Handler();

        /**
         * Called once to handle the processing of the buffer.  It may 
         * reallocate the buffer with realloc if needed and may alter the
         * size as needed.  This must return true if anything other than
         * the values in the buffer is altered
         * 
         * @param buffer    the input and output buffer
         * @param size      the input and output size
         * @return          true if the buffer is changed other than altering the values
         */
        virtual bool process(Value *&buffer, size_t &size) = 0;
    };

protected:
    /**
     * Set the maximum number of points to buffer before a partial flush. 
     * Defaults to 65536.
     * @param n the number of points, must be at least two
     */
    inline void setMaximumBuffer(int n)
    {
        Q_ASSERT(n > 1);
        maximumBuffer = n;
    }

    /**
     * Set the way to handle undefined values.  Defaults to removal.
     * @param m the undefined handler mode
     */
    inline void setUndefinedMode(UndefinedMode m)
    { undefinedMode = m; }

    /**
     * Create a handler for the block smoother.  The bounds given are the
     * start and end of the data it will receive.
     * 
     * @param start     the start time of the first value in the buffer
     * @param end       the end time of the last value in the buffer
     * @return          a newly allocated handler
     */
    virtual Handler *createHandler(double start, double end) = 0;

private:
    Value *buffer;
    size_t size;
    size_t capacity;
};

}
}

#endif
