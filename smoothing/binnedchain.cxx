/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "smoothing/binnedchain.hxx"

using namespace CPD3::Data;
using namespace CPD3::Algorithms;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/binnedchain.hxx
 * General implementations for binned chain smoothers.
 */

SmootherChainCoreBinned::SmootherChainCoreBinned(bool setEnableStatistics) : enableStatistics(
        setEnableStatistics)
{ }

SmootherChainCoreBinned::~SmootherChainCoreBinned()
{ }

void SmootherChainCoreBinned::createSmoother(SmootherChainCoreEngineInterface *engine,
                                             SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General: {
        SmootherChainTarget *inputValue = NULL;
        SmootherChainTarget *inputCover = NULL;
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);
        SmootherChainTargetGeneral *outputStats;
        if (enableStatistics) {
            outputStats = engine->addNewGeneralOutput(
                    engine->getBaseUnit().withFlavor(SequenceName::flavor_stats));
        } else {
            outputStats = NULL;
        }

        createConventional(engine, inputValue, inputCover, engine->getOutput(0), outputCover,
                           outputStats);
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(0, inputValue);
        if (inputCover != NULL)
            engine->addNewInput(coverUnit, inputCover);

        break;
    }

    case SmootherChainCore::Difference: {
        SmootherChainTarget *inputStart = NULL;
        SmootherChainTarget *inputEnd = NULL;
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);

        createDifference(engine, false, inputStart, inputEnd, engine->getOutput(0),
                         engine->getOutput(1), outputCover);
        Q_ASSERT(inputStart != NULL || inputEnd != NULL);
        if (inputStart != NULL)
            engine->addInputTarget(0, inputStart);
        if (inputEnd != NULL)
            engine->addInputTarget(1, inputEnd);

        break;
    }

    case SmootherChainCore::DifferenceInitial: {
        SmootherChainTarget *inputStart = NULL;
        SmootherChainTarget *inputEnd = NULL;
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);

        createDifference(engine, false, inputStart, inputEnd, engine->getOutput(0),
                         engine->getOutput(1), outputCover);
        Q_ASSERT(inputStart != NULL || inputEnd != NULL);
        if (inputStart != NULL) {
            engine->addInputTarget(0, inputStart);
            if (inputEnd != NULL)
                inputEnd->endData();
        } else {
            Q_ASSERT(inputEnd != NULL);
            engine->addInputTarget(0, inputEnd);
        }

        break;
    }

    case SmootherChainCore::Vector2D: {
        SequenceName coverUnit(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);
        SmootherChainTargetGeneral *outputStats;
        if (enableStatistics) {
            outputStats = engine->addNewGeneralOutput(
                    engine->getBaseUnit(1).withFlavor(SequenceName::flavor_stats));
        } else {
            outputStats = NULL;
        }

        SmootherChainTarget *inputStatsMagnitude = NULL;
        SmootherChainTarget *inputStatsCover = NULL;
        SmootherChainTarget *inputStatsVectoredMagnitude = NULL;

        createVectorStatistics(engine, inputStatsMagnitude, inputStatsCover,
                               inputStatsVectoredMagnitude, NULL, outputCover, outputStats);
        if (inputStatsMagnitude != NULL)
            engine->addInputTarget(1, inputStatsMagnitude);

        SmootherChainTarget *targetVectoredMagnitude;
        if (inputStatsVectoredMagnitude != NULL) {
            SmootherChainForward<2> *vectorMeanSplit = new SmootherChainForward<2>(
                    {engine->getOutput(1), inputStatsVectoredMagnitude});
            engine->addChainNode(vectorMeanSplit);
            targetVectoredMagnitude = vectorMeanSplit;
        } else {
            targetVectoredMagnitude = engine->getOutput(1);
        }

        SmootherChainVector2DReconstruct *reconstruct =
                new SmootherChainVector2DReconstruct(engine->getOutput(0), targetVectoredMagnitude);
        engine->addChainNode(reconstruct);

        SmootherChainTarget *inputX = NULL;
        SmootherChainTarget *inputXCover = NULL;
        createConventional(engine, inputX, inputXCover, reconstruct->getTargetX());
        Q_ASSERT(inputX != NULL);

        SmootherChainTarget *inputY = NULL;
        SmootherChainTarget *inputYCover = NULL;
        createConventional(engine, inputY, inputYCover, reconstruct->getTargetY());
        Q_ASSERT(inputY != NULL);

        SmootherChainVector2DBreakdown
                *breakdown = new SmootherChainVector2DBreakdown(inputX, inputY);
        engine->addChainNode(breakdown);

        engine->addInputTarget(0, breakdown->getTargetDirection());
        engine->addInputTarget(1, breakdown->getTargetMagnitude());

        if (inputStatsCover != NULL || inputXCover != NULL || inputYCover != NULL) {
            engine->addNewInput(coverUnit);
            if (inputStatsCover != NULL)
                engine->addInputTarget(2, inputStatsCover);
            if (inputXCover != NULL)
                engine->addInputTarget(2, inputXCover);
            if (inputYCover != NULL)
                engine->addInputTarget(2, inputYCover);
        }
        break;
    }

    case SmootherChainCore::Vector3D: {
        SequenceName coverUnit(engine->getBaseUnit(2).withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);
        SmootherChainTargetGeneral *outputStats;
        if (enableStatistics) {
            outputStats = engine->addNewGeneralOutput(
                    engine->getBaseUnit(2).withFlavor(SequenceName::flavor_stats));
        } else {
            outputStats = NULL;
        }

        SmootherChainTarget *inputStatsMagnitude = NULL;
        SmootherChainTarget *inputStatsCover = NULL;
        SmootherChainTarget *inputStatsVectoredMagnitude = NULL;

        createVectorStatistics(engine, inputStatsMagnitude, inputStatsCover,
                               inputStatsVectoredMagnitude, NULL, outputCover, outputStats);
        if (inputStatsMagnitude != NULL)
            engine->addInputTarget(2, inputStatsMagnitude);

        SmootherChainTarget *targetVectoredMagnitude;
        if (inputStatsVectoredMagnitude != NULL) {
            SmootherChainForward<2> *vectorMeanSplit = new SmootherChainForward<2>(
                    {engine->getOutput(2), inputStatsVectoredMagnitude});
            engine->addChainNode(vectorMeanSplit);
            targetVectoredMagnitude = vectorMeanSplit;
        } else {
            targetVectoredMagnitude = engine->getOutput(2);
        }

        SmootherChainVector3DReconstruct *reconstruct =
                new SmootherChainVector3DReconstruct(engine->getOutput(0), engine->getOutput(1),
                                                     targetVectoredMagnitude);
        engine->addChainNode(reconstruct);

        SmootherChainTarget *inputX = NULL;
        SmootherChainTarget *inputXCover = NULL;
        createConventional(engine, inputX, inputXCover, reconstruct->getTargetX());
        Q_ASSERT(inputX != NULL);

        SmootherChainTarget *inputY = NULL;
        SmootherChainTarget *inputYCover = NULL;
        createConventional(engine, inputY, inputYCover, reconstruct->getTargetY());
        Q_ASSERT(inputY != NULL);

        SmootherChainTarget *inputZ = NULL;
        SmootherChainTarget *inputZCover = NULL;
        createConventional(engine, inputZ, inputZCover, reconstruct->getTargetZ());
        Q_ASSERT(inputZ != NULL);

        SmootherChainVector3DBreakdown
                *breakdown = new SmootherChainVector3DBreakdown(inputX, inputY, inputZ);
        engine->addChainNode(breakdown);

        engine->addInputTarget(0, breakdown->getTargetAzimuth());
        engine->addInputTarget(1, breakdown->getTargetElevation());
        engine->addInputTarget(2, breakdown->getTargetMagnitude());

        if (inputStatsCover != NULL || inputXCover != NULL || inputYCover != NULL) {
            engine->addNewInput(coverUnit);
            if (inputStatsCover != NULL)
                engine->addInputTarget(3, inputStatsCover);
            if (inputXCover != NULL)
                engine->addInputTarget(3, inputXCover);
            if (inputYCover != NULL)
                engine->addInputTarget(3, inputYCover);
            if (inputZCover != NULL)
                engine->addInputTarget(3, inputZCover);
        }
        break;
    }

    case SmootherChainCore::BeersLawAbsorption: {
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);
        SmootherChainTargetGeneral *outputStats;
        if (enableStatistics) {
            outputStats = engine->addNewGeneralOutput(
                    engine->getBaseUnit().withFlavor(SequenceName::flavor_stats));
        } else {
            outputStats = NULL;
        }
        SmootherChainTarget *inputValue = NULL;
        SmootherChainTarget *inputCover = NULL;
        createConventional(engine, inputValue, inputCover, NULL, outputCover, outputStats);
        Q_ASSERT(inputValue != NULL);
        engine->addNewInput(engine->getBaseUnit(), inputValue);
        if (inputCover != NULL)
            engine->addNewInput(coverUnit, inputCover);

        SmootherChainTarget *outputTarget;
        if (differencePreserving()) {
            outputTarget = engine->getOutput(0);
        } else {
            inputValue = NULL;
            inputCover = NULL;
            createConventional(engine, inputValue, inputCover, engine->getOutput(0));
            Q_ASSERT(inputValue != NULL);
            outputTarget = inputValue;
            if (inputCover != NULL)
                inputCover->endData();
        }

        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(outputTarget);
        engine->addChainNode(calculate);

        SmootherChainTarget *inputStartL = NULL;
        SmootherChainTarget *inputEndL = NULL;
        SmootherChainTarget *inputStartI = NULL;
        SmootherChainTarget *inputEndI = NULL;

        createDifference(engine, true, inputStartL, inputEndL, calculate->getTargetStartL(),
                         calculate->getTargetEndL());
        Q_ASSERT(inputStartL != NULL || inputEndL != NULL);
        if (inputStartL != NULL)
            engine->addInputTarget(0, inputStartL);
        if (inputEndL != NULL)
            engine->addInputTarget(1, inputEndL);

        createDifference(engine, true, inputStartI, inputEndI, calculate->getTargetStartI(),
                         calculate->getTargetEndI());
        Q_ASSERT(inputStartL != NULL || inputEndL != NULL);
        if (inputStartI != NULL)
            engine->addInputTarget(2, inputStartI);
        if (inputEndI != NULL)
            engine->addInputTarget(3, inputEndI);

        break;
    }

    case SmootherChainCore::BeersLawAbsorptionInitial: {
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);
        SmootherChainTargetGeneral *outputStats;
        if (enableStatistics) {
            outputStats = engine->addNewGeneralOutput(
                    engine->getBaseUnit().withFlavor(SequenceName::flavor_stats));
        } else {
            outputStats = NULL;
        }
        SmootherChainTarget *inputValue = NULL;
        SmootherChainTarget *inputCover = NULL;
        createConventional(engine, inputValue, inputCover, NULL, outputCover, outputStats);
        Q_ASSERT(inputValue != NULL);
        engine->addNewInput(engine->getBaseUnit(), inputValue);
        if (inputCover != NULL)
            engine->addNewInput(coverUnit, inputCover);

        SmootherChainTarget *outputTarget;
        if (differencePreserving()) {
            outputTarget = engine->getOutput(0);
        } else {
            inputValue = NULL;
            inputCover = NULL;
            createConventional(engine, inputValue, inputCover, engine->getOutput(0));
            Q_ASSERT(inputValue != NULL);
            outputTarget = inputValue;
            if (inputCover != NULL)
                inputCover->endData();
        }

        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(outputTarget);
        engine->addChainNode(calculate);

        SmootherChainTarget *inputStartL = NULL;
        SmootherChainTarget *inputEndL = NULL;
        SmootherChainTarget *inputStartI = NULL;
        SmootherChainTarget *inputEndI = NULL;

        createDifference(engine, true, inputStartL, inputEndL, calculate->getTargetStartL(),
                         calculate->getTargetEndL());
        Q_ASSERT(inputStartL != NULL || inputEndL != NULL);
        if (inputStartL != NULL) {
            engine->addInputTarget(0, inputStartL);
            if (inputEndL != NULL)
                inputEndL->endData();
        } else {
            Q_ASSERT(inputEndL != NULL);
            engine->addInputTarget(0, inputEndL);
        }

        createDifference(engine, true, inputStartI, inputEndI, calculate->getTargetStartI(),
                         calculate->getTargetEndI());
        Q_ASSERT(inputStartI != NULL || inputEndI != NULL);
        if (inputStartI != NULL) {
            engine->addInputTarget(1, inputStartI);
            if (inputEndI != NULL)
                inputEndI->endData();
        } else {
            Q_ASSERT(inputEndI != NULL);
            engine->addInputTarget(1, inputEndI);
        }

        break;
    }

    case SmootherChainCore::Dewpoint: {
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);
        SmootherChainTargetGeneral *outputStats;
        if (enableStatistics) {
            outputStats = engine->addNewGeneralOutput(
                    engine->getBaseUnit().withFlavor(SequenceName::flavor_stats));
        } else {
            outputStats = NULL;
        }

        SmootherChainTarget *inputValue = NULL;
        SmootherChainTarget *inputCover = NULL;
        createConventional(engine, inputValue, inputCover, NULL, outputCover, outputStats);
        Q_ASSERT(inputValue != NULL);
        engine->addNewInput(engine->getBaseUnit(), inputValue);
        if (inputCover != NULL)
            engine->addNewInput(coverUnit, inputCover);

        SmootherChainDewpoint *calculate = new SmootherChainDewpoint(engine->getOutput(0),
                                                                     engine->getOptions()
                                                                           .hash("AlwaysWater")
                                                                           .toBool());
        engine->addChainNode(calculate);

        inputValue = NULL;
        inputCover = NULL;
        createConventional(engine, inputValue, inputCover, calculate->getTargetTemperature());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(0, inputValue);
        if (inputCover != NULL) {
            engine->addNewInput(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover),
                                inputCover);
        }

        inputValue = NULL;
        inputCover = NULL;
        createConventional(engine, inputValue, inputCover, calculate->getTargetRH());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(1, inputValue);
        if (inputCover != NULL) {
            engine->addNewInput(engine->getBaseUnit(2).withFlavor(SequenceName::flavor_cover),
                                inputCover);
        }

        break;
    }

    case SmootherChainCore::RH: {
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);
        SmootherChainTargetGeneral *outputStats;
        if (enableStatistics) {
            outputStats = engine->addNewGeneralOutput(
                    engine->getBaseUnit().withFlavor(SequenceName::flavor_stats));
        } else {
            outputStats = NULL;
        }

        SmootherChainTarget *inputValue = NULL;
        SmootherChainTarget *inputCover = NULL;
        createConventional(engine, inputValue, inputCover, NULL, outputCover, outputStats);
        Q_ASSERT(inputValue != NULL);
        engine->addNewInput(engine->getBaseUnit(), inputValue);
        if (inputCover != NULL)
            engine->addNewInput(coverUnit, inputCover);

        SmootherChainRH *calculate = new SmootherChainRH(engine->getOutput(0), engine->getOptions()
                                                                                     .hash("AlwaysWater")
                                                                                     .toBool());
        engine->addChainNode(calculate);

        inputValue = NULL;
        inputCover = NULL;
        createConventional(engine, inputValue, inputCover, calculate->getTargetTemperature());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(0, inputValue);
        if (inputCover != NULL) {
            engine->addNewInput(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover),
                                inputCover);
        }

        inputValue = NULL;
        inputCover = NULL;
        createConventional(engine, inputValue, inputCover, calculate->getTargetDewpoint());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(1, inputValue);
        if (inputCover != NULL) {
            engine->addNewInput(engine->getBaseUnit(2).withFlavor(SequenceName::flavor_cover),
                                inputCover);
        }

        break;
    }

    case SmootherChainCore::RHExtrapolate: {
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);
        SmootherChainTargetGeneral *outputStats;
        if (enableStatistics) {
            outputStats = engine->addNewGeneralOutput(
                    engine->getBaseUnit().withFlavor(SequenceName::flavor_stats));
        } else {
            outputStats = NULL;
        }

        SmootherChainTarget *inputValue = NULL;
        SmootherChainTarget *inputCover = NULL;
        createConventional(engine, inputValue, inputCover, NULL, outputCover, outputStats);
        Q_ASSERT(inputValue != NULL);
        engine->addNewInput(engine->getBaseUnit(), inputValue);
        if (inputCover != NULL)
            engine->addNewInput(coverUnit, inputCover);

        SmootherChainRHExtrapolate *calculate = new SmootherChainRHExtrapolate(engine->getOutput(0),
                                                                               engine->getOptions()
                                                                                     .hash("AlwaysWater")
                                                                                     .toBool());
        engine->addChainNode(calculate);

        inputValue = NULL;
        inputCover = NULL;
        createConventional(engine, inputValue, inputCover, calculate->getTargetTemperatureIn());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(0, inputValue);
        if (inputCover != NULL) {
            engine->addNewInput(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover),
                                inputCover);
        }

        inputValue = NULL;
        inputCover = NULL;
        createConventional(engine, inputValue, inputCover, calculate->getTargetRHIn());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(1, inputValue);
        if (inputCover != NULL) {
            engine->addNewInput(engine->getBaseUnit(2).withFlavor(SequenceName::flavor_cover),
                                inputCover);
        }

        inputValue = NULL;
        inputCover = NULL;
        createConventional(engine, inputValue, inputCover, calculate->getTargetTemperatureOut());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(2, inputValue);
        if (inputCover != NULL) {
            engine->addNewInput(engine->getBaseUnit(3).withFlavor(SequenceName::flavor_cover),
                                inputCover);
        }

        break;
    }

    }
}

void SmootherChainCoreBinned::deserializeSmoother(QDataStream &stream,
                                                  SmootherChainCoreEngineInterface *engine,
                                                  SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General: {
        SmootherChainTarget *inputValue = NULL;
        SmootherChainTarget *inputCover = NULL;

        deserializeConventional(stream, engine, inputValue, inputCover, engine->getOutput(0),
                                engine->getOutput(1),
                                enableStatistics ? engine->getGeneralOutput(0) : NULL);
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(0, inputValue);
        if (inputCover != NULL)
            engine->addInputTarget(1, inputCover);

        break;
    }

    case SmootherChainCore::Difference: {
        SmootherChainTarget *inputStart = NULL;
        SmootherChainTarget *inputEnd = NULL;

        deserializeDifference(stream, engine, false, inputStart, inputEnd, engine->getOutput(0),
                              engine->getOutput(1), engine->getOutput(2));
        Q_ASSERT(inputStart != NULL || inputEnd != NULL);
        if (inputStart != NULL)
            engine->addInputTarget(0, inputStart);
        if (inputEnd != NULL)
            engine->addInputTarget(1, inputEnd);

        break;
    }

    case SmootherChainCore::DifferenceInitial: {
        SmootherChainTarget *inputStart = NULL;
        SmootherChainTarget *inputEnd = NULL;
        deserializeDifference(stream, engine, false, inputStart, inputEnd, engine->getOutput(0),
                              engine->getOutput(1), engine->getOutput(2));
        Q_ASSERT(inputStart != NULL || inputEnd != NULL);
        if (inputStart != NULL) {
            engine->addInputTarget(0, inputStart);
        } else {
            Q_ASSERT(inputEnd != NULL);
            engine->addInputTarget(0, inputEnd);
        }

        break;
    }

    case SmootherChainCore::Vector2D: {
        SmootherChainTarget *inputStatsMagnitude = NULL;
        SmootherChainTarget *inputStatsCover = NULL;
        SmootherChainTarget *inputStatsVectoredMagnitude = NULL;

        deserializeVectorStatistics(stream, engine, inputStatsMagnitude, inputStatsCover,
                                    inputStatsVectoredMagnitude, NULL, engine->getOutput(2),
                                    enableStatistics ? engine->getGeneralOutput(0) : NULL);
        if (inputStatsMagnitude != NULL)
            engine->addInputTarget(1, inputStatsMagnitude);

        SmootherChainTarget *targetVectoredMagnitude;
        if (inputStatsVectoredMagnitude != NULL) {
            SmootherChainForward<2> *vectorMeanSplit = new SmootherChainForward<2>(
                    {engine->getOutput(1), inputStatsVectoredMagnitude});
            engine->addChainNode(vectorMeanSplit);
            targetVectoredMagnitude = vectorMeanSplit;
        } else {
            targetVectoredMagnitude = engine->getOutput(1);
        }

        SmootherChainVector2DReconstruct *reconstruct =
                new SmootherChainVector2DReconstruct(stream, engine->getOutput(0),
                                                     targetVectoredMagnitude);
        engine->addChainNode(reconstruct);

        SmootherChainTarget *inputX = NULL;
        SmootherChainTarget *inputXCover = NULL;
        deserializeConventional(stream, engine, inputX, inputXCover, reconstruct->getTargetX());
        Q_ASSERT(inputX != NULL);

        SmootherChainTarget *inputY = NULL;
        SmootherChainTarget *inputYCover = NULL;
        deserializeConventional(stream, engine, inputY, inputYCover, reconstruct->getTargetY());
        Q_ASSERT(inputY != NULL);

        SmootherChainVector2DBreakdown
                *breakdown = new SmootherChainVector2DBreakdown(stream, inputX, inputY);
        engine->addChainNode(breakdown);

        engine->addInputTarget(0, breakdown->getTargetDirection());
        engine->addInputTarget(1, breakdown->getTargetMagnitude());

        if (inputStatsCover != NULL || inputXCover != NULL || inputYCover != NULL) {
            if (inputStatsCover != NULL)
                engine->addInputTarget(2, inputStatsCover);
            if (inputXCover != NULL)
                engine->addInputTarget(2, inputXCover);
            if (inputYCover != NULL)
                engine->addInputTarget(2, inputYCover);
        }
        break;
    }

    case SmootherChainCore::Vector3D: {
        SmootherChainTarget *inputStatsMagnitude = NULL;
        SmootherChainTarget *inputStatsCover = NULL;
        SmootherChainTarget *inputStatsVectoredMagnitude = NULL;

        deserializeVectorStatistics(stream, engine, inputStatsMagnitude, inputStatsCover,
                                    inputStatsVectoredMagnitude, NULL, engine->getOutput(3),
                                    enableStatistics ? engine->getGeneralOutput(0) : NULL);
        if (inputStatsMagnitude != NULL)
            engine->addInputTarget(2, inputStatsMagnitude);

        SmootherChainTarget *targetVectoredMagnitude;
        if (inputStatsVectoredMagnitude != NULL) {
            SmootherChainForward<2> *vectorMeanSplit = new SmootherChainForward<2>(
                    {engine->getOutput(2), inputStatsVectoredMagnitude});
            engine->addChainNode(vectorMeanSplit);
            targetVectoredMagnitude = vectorMeanSplit;
        } else {
            targetVectoredMagnitude = engine->getOutput(2);
        }

        SmootherChainVector3DReconstruct *reconstruct =
                new SmootherChainVector3DReconstruct(stream, engine->getOutput(0),
                                                     engine->getOutput(1), targetVectoredMagnitude);
        engine->addChainNode(reconstruct);

        SmootherChainTarget *inputX = NULL;
        SmootherChainTarget *inputXCover = NULL;
        deserializeConventional(stream, engine, inputX, inputXCover, reconstruct->getTargetX());
        Q_ASSERT(inputX != NULL);

        SmootherChainTarget *inputY = NULL;
        SmootherChainTarget *inputYCover = NULL;
        deserializeConventional(stream, engine, inputY, inputYCover, reconstruct->getTargetY());
        Q_ASSERT(inputY != NULL);

        SmootherChainTarget *inputZ = NULL;
        SmootherChainTarget *inputZCover = NULL;
        deserializeConventional(stream, engine, inputZ, inputZCover, reconstruct->getTargetZ());
        Q_ASSERT(inputZ != NULL);

        SmootherChainVector3DBreakdown
                *breakdown = new SmootherChainVector3DBreakdown(stream, inputX, inputY, inputZ);
        engine->addChainNode(breakdown);

        engine->addInputTarget(0, breakdown->getTargetAzimuth());
        engine->addInputTarget(1, breakdown->getTargetElevation());
        engine->addInputTarget(2, breakdown->getTargetMagnitude());

        if (inputStatsCover != NULL || inputXCover != NULL || inputYCover != NULL) {
            if (inputStatsCover != NULL)
                engine->addInputTarget(3, inputStatsCover);
            if (inputXCover != NULL)
                engine->addInputTarget(3, inputXCover);
            if (inputYCover != NULL)
                engine->addInputTarget(3, inputYCover);
            if (inputZCover != NULL)
                engine->addInputTarget(3, inputZCover);
        }
        break;
    }

    case SmootherChainCore::BeersLawAbsorption: {
        SmootherChainTarget *inputValue = NULL;
        SmootherChainTarget *inputCover = NULL;
        deserializeConventional(stream, engine, inputValue, inputCover, NULL, engine->getOutput(1),
                                enableStatistics ? engine->getGeneralOutput(0) : NULL);
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(4, inputValue);
        if (inputCover != NULL)
            engine->addInputTarget(5, inputCover);

        SmootherChainTarget *outputTarget;
        if (differencePreserving()) {
            outputTarget = engine->getOutput(0);
        } else {
            inputValue = NULL;
            inputCover = NULL;
            deserializeConventional(stream, engine, inputValue, inputCover, engine->getOutput(0));
            Q_ASSERT(inputValue != NULL);
            outputTarget = inputValue;
        }

        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(stream, outputTarget);
        engine->addChainNode(calculate);

        SmootherChainTarget *inputStartL = NULL;
        SmootherChainTarget *inputEndL = NULL;
        SmootherChainTarget *inputStartI = NULL;
        SmootherChainTarget *inputEndI = NULL;

        deserializeDifference(stream, engine, true, inputStartL, inputEndL,
                              calculate->getTargetStartL(), calculate->getTargetEndL());
        Q_ASSERT(inputStartL != NULL || inputEndL != NULL);
        if (inputStartL != NULL)
            engine->addInputTarget(0, inputStartL);
        if (inputEndL != NULL)
            engine->addInputTarget(1, inputEndL);

        deserializeDifference(stream, engine, true, inputStartI, inputEndI,
                              calculate->getTargetStartI(), calculate->getTargetEndI());
        Q_ASSERT(inputStartL != NULL || inputEndL != NULL);
        if (inputStartI != NULL)
            engine->addInputTarget(2, inputStartI);
        if (inputEndI != NULL)
            engine->addInputTarget(3, inputEndI);

        break;
    }

    case SmootherChainCore::BeersLawAbsorptionInitial: {
        SmootherChainTarget *inputValue = NULL;
        SmootherChainTarget *inputCover = NULL;
        deserializeConventional(stream, engine, inputValue, inputCover, NULL, engine->getOutput(1),
                                enableStatistics ? engine->getGeneralOutput(0) : NULL);
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(2, inputValue);
        if (inputCover != NULL)
            engine->addInputTarget(3, inputCover);

        SmootherChainTarget *outputTarget;
        if (differencePreserving()) {
            outputTarget = engine->getOutput(0);
        } else {
            inputValue = NULL;
            inputCover = NULL;
            deserializeConventional(stream, engine, inputValue, inputCover, engine->getOutput(0));
            Q_ASSERT(inputValue != NULL);
            outputTarget = inputValue;
        }

        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(stream, outputTarget);
        engine->addChainNode(calculate);

        SmootherChainTarget *inputStartL = NULL;
        SmootherChainTarget *inputEndL = NULL;
        SmootherChainTarget *inputStartI = NULL;
        SmootherChainTarget *inputEndI = NULL;

        deserializeDifference(stream, engine, true, inputStartL, inputEndL,
                              calculate->getTargetStartL(), calculate->getTargetEndL());
        Q_ASSERT(inputStartL != NULL || inputEndL != NULL);
        if (inputStartL != NULL) {
            engine->addInputTarget(0, inputStartL);
        } else {
            Q_ASSERT(inputEndL != NULL);
            engine->addInputTarget(0, inputEndL);
        }

        deserializeDifference(stream, engine, true, inputStartI, inputEndI,
                              calculate->getTargetStartI(), calculate->getTargetEndI());
        Q_ASSERT(inputStartI != NULL || inputEndI != NULL);
        if (inputStartI != NULL) {
            engine->addInputTarget(1, inputStartI);
        } else {
            Q_ASSERT(inputEndI != NULL);
            engine->addInputTarget(1, inputEndI);
        }

        break;
    }

    case SmootherChainCore::Dewpoint: {
        SmootherChainTarget *inputValue = NULL;
        SmootherChainTarget *inputCover = NULL;
        deserializeConventional(stream, engine, inputValue, inputCover, NULL, engine->getOutput(1),
                                enableStatistics ? engine->getGeneralOutput(0) : NULL);
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(2, inputValue);
        int index = 3;
        if (inputCover != NULL)
            engine->addInputTarget(index++, inputCover);

        SmootherChainDewpoint *calculate = new SmootherChainDewpoint(stream, engine->getOutput(0));
        engine->addChainNode(calculate);

        inputValue = NULL;
        inputCover = NULL;
        deserializeConventional(stream, engine, inputValue, inputCover,
                                calculate->getTargetTemperature());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(0, inputValue);
        if (inputCover != NULL)
            engine->addInputTarget(index++, inputCover);

        inputValue = NULL;
        inputCover = NULL;
        deserializeConventional(stream, engine, inputValue, inputCover, calculate->getTargetRH());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(1, inputValue);
        if (inputCover != NULL)
            engine->addInputTarget(index++, inputCover);

        break;
    }

    case SmootherChainCore::RH: {
        SmootherChainTarget *inputValue = NULL;
        SmootherChainTarget *inputCover = NULL;
        deserializeConventional(stream, engine, inputValue, inputCover, NULL, engine->getOutput(1),
                                enableStatistics ? engine->getGeneralOutput(0) : NULL);
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(2, inputValue);
        int index = 3;
        if (inputCover != NULL)
            engine->addInputTarget(index++, inputCover);

        SmootherChainRH *calculate = new SmootherChainRH(stream, engine->getOutput(0));
        engine->addChainNode(calculate);

        inputValue = NULL;
        inputCover = NULL;
        deserializeConventional(stream, engine, inputValue, inputCover,
                                calculate->getTargetTemperature());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(0, inputValue);
        if (inputCover != NULL)
            engine->addInputTarget(index++, inputCover);

        inputValue = NULL;
        inputCover = NULL;
        deserializeConventional(stream, engine, inputValue, inputCover,
                                calculate->getTargetDewpoint());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(1, inputValue);
        if (inputCover != NULL)
            engine->addInputTarget(index++, inputCover);

        break;
    }

    case SmootherChainCore::RHExtrapolate: {
        SmootherChainTarget *inputValue = NULL;
        SmootherChainTarget *inputCover = NULL;
        deserializeConventional(stream, engine, inputValue, inputCover, NULL, engine->getOutput(1),
                                enableStatistics ? engine->getGeneralOutput(0) : NULL);
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(3, inputValue);
        int index = 4;
        if (inputCover != NULL)
            engine->addInputTarget(index++, inputCover);

        SmootherChainRHExtrapolate
                *calculate = new SmootherChainRHExtrapolate(stream, engine->getOutput(0));
        engine->addChainNode(calculate);

        inputValue = NULL;
        inputCover = NULL;
        deserializeConventional(stream, engine, inputValue, inputCover,
                                calculate->getTargetTemperatureIn());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(0, inputValue);
        if (inputCover != NULL)
            engine->addInputTarget(index++, inputCover);

        inputValue = NULL;
        inputCover = NULL;
        deserializeConventional(stream, engine, inputValue, inputCover, calculate->getTargetRHIn());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(1, inputValue);
        if (inputCover != NULL)
            engine->addInputTarget(index++, inputCover);

        inputValue = NULL;
        inputCover = NULL;
        deserializeConventional(stream, engine, inputValue, inputCover,
                                calculate->getTargetTemperatureOut());
        Q_ASSERT(inputValue != NULL);
        engine->addInputTarget(2, inputValue);
        if (inputCover != NULL)
            engine->addInputTarget(index++, inputCover);

        break;
    }

    }
}

SmootherChainCore::HandlerMode SmootherChainCoreBinned::smootherHandler(SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General:
    case SmootherChainCore::Vector2D:
    case SmootherChainCore::Vector3D:
    case SmootherChainCore::Dewpoint:
    case SmootherChainCore::RH:
    case SmootherChainCore::RHExtrapolate:
        return SmootherChainCore::Repeatable;
    case SmootherChainCore::Difference:
    case SmootherChainCore::DifferenceInitial:
    case SmootherChainCore::BeersLawAbsorption:
    case SmootherChainCore::BeersLawAbsorptionInitial:
        if (differencePreserving())
            return SmootherChainCore::Repeatable;
        else
            return SmootherChainCore::Handled;
    }
    Q_ASSERT(false);
    return SmootherChainCore::Unhandled;
}

bool SmootherChainCoreBinned::flagsHandler()
{ return true; }

void SmootherChainCoreBinned::createFlags(SmootherChainCoreEngineInterface *engine)
{
    SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
    SmootherChainTarget *cover = engine->addNewOutput(coverUnit);

    SmootherChainTargetFlags *inputFlags = NULL;
    SmootherChainTarget *inputCover = NULL;
    createFlags(engine, inputFlags, inputCover, engine->getFlagsOutput(0), cover);

    if (inputFlags != NULL)
        engine->addFlagsInputTarget(0, inputFlags);
    if (inputCover != NULL)
        engine->addNewInput(coverUnit, inputCover);
}

void SmootherChainCoreBinned::deserializeFlags(QDataStream &stream,
                                               SmootherChainCoreEngineInterface *engine)
{
    SmootherChainTargetFlags *inputFlags = NULL;
    SmootherChainTarget *inputCover = NULL;
    deserializeFlags(stream, engine, inputFlags, inputCover, engine->getFlagsOutput(0),
                     engine->getOutput(0));
    if (inputFlags != NULL)
        engine->addFlagsInputTarget(0, inputFlags);
    if (inputCover != NULL)
        engine->addInputTarget(0, inputCover);
}

bool SmootherChainCoreBinned::generatedOutput(const SequenceName &unit)
{
    return unit.hasFlavor(SequenceName::flavor_stats) || unit.hasFlavor(SequenceName::flavor_cover);
}


SequenceName::Set SmootherChainCoreBinned::requestedInputs(const SequenceName::Set &inputs)
{
    SequenceName::Set result;
    for (auto add : inputs) {
        add.removeFlavor(SequenceName::flavor_end);
        add.removeFlavor(SequenceName::flavor_cover);
        add.removeFlavor(SequenceName::flavor_stats);
        result.insert(add);
        result.insert(add.withFlavor(SequenceName::flavor_end));
        result.insert(add.withFlavor(SequenceName::flavor_cover));
    }
    return result;
}

SequenceName::Set SmootherChainCoreBinned::predictedOutputs(const SequenceName::Set &inputs)
{
    SequenceName::Set result;
    for (auto add : inputs) {
        add.removeFlavor(SequenceName::flavor_end);
        add.removeFlavor(SequenceName::flavor_cover);
        add.removeFlavor(SequenceName::flavor_stats);
        result.insert(add);
        result.insert(add.withFlavor(SequenceName::flavor_end));
        result.insert(add.withFlavor(SequenceName::flavor_cover));
        result.insert(add.withFlavor(SequenceName::flavor_stats));
    }
    return result;
}

namespace Internal {

static const std::string keyCount("Count");
static const std::string keyUndefinedCount("UndefinedCount");
static const std::string keyMinimum("Minimum");
static const std::string keyMaximum("Maximum");
static const std::string keyMean("Mean");
static const std::string keyStandardDeviation("StandardDeviation");
static const std::string keyQuantiles("Quantiles");

Variant::Root binnedChainGenerateStatistics(QVector<double> values)
{
    Variant::Root result(Variant::Type::Hash);
    Variant::Write stats = result.write();

    int originalSize = values.size();
    Statistics::sortRemoveUndefined(values);
    stats.hash(keyCount).setInt64(values.size());
    stats.hash(keyUndefinedCount).setInt64(originalSize - values.size());
    if (!values.isEmpty()) {
        stats.hash(keyMinimum).setDouble(values.first());
        stats.hash(keyMaximum).setDouble(values.last());

        double mean, sd;
        Statistics::meanSDDefined(values, mean, sd);
        stats.hash(keyMean).setDouble(mean);
        stats.hash(keyStandardDeviation).setDouble(sd);

        auto targetQuantiles = stats.hash(keyQuantiles);
        targetQuantiles.keyframe(0.00135)
                       .setDouble(Algorithms::Statistics::quantile(values, 0.00135));
        targetQuantiles.keyframe(0.00621)
                       .setDouble(Algorithms::Statistics::quantile(values, 0.00621));
        targetQuantiles.keyframe(0.02275)
                       .setDouble(Algorithms::Statistics::quantile(values, 0.02275));
        targetQuantiles.keyframe(0.05).setDouble(Algorithms::Statistics::quantile(values, 0.05));
        targetQuantiles.keyframe(0.06681)
                       .setDouble(Algorithms::Statistics::quantile(values, 0.06681));
        targetQuantiles.keyframe(0.15866)
                       .setDouble(Algorithms::Statistics::quantile(values, 0.15866));
        targetQuantiles.keyframe(0.25).setDouble(Algorithms::Statistics::quantile(values, 0.25));
        targetQuantiles.keyframe(0.30854)
                       .setDouble(Algorithms::Statistics::quantile(values, 0.30854));
        targetQuantiles.keyframe(0.50).setDouble(Algorithms::Statistics::quantile(values, 0.50));
        targetQuantiles.keyframe(0.69146)
                       .setDouble(Algorithms::Statistics::quantile(values, 0.69146));
        targetQuantiles.keyframe(0.75).setDouble(Algorithms::Statistics::quantile(values, 0.75));
        targetQuantiles.keyframe(0.84134)
                       .setDouble(Algorithms::Statistics::quantile(values, 0.84134));
        targetQuantiles.keyframe(0.93319)
                       .setDouble(Algorithms::Statistics::quantile(values, 0.93319));
        targetQuantiles.keyframe(0.95).setDouble(Algorithms::Statistics::quantile(values, 0.95));
        targetQuantiles.keyframe(0.97725)
                       .setDouble(Algorithms::Statistics::quantile(values, 0.97725));
        targetQuantiles.keyframe(0.99379)
                       .setDouble(Algorithms::Statistics::quantile(values, 0.99379));
        targetQuantiles.keyframe(0.99865)
                       .setDouble(Algorithms::Statistics::quantile(values, 0.99865));
    }

    return result;
}

static const std::string keyVectorMean("VectorMean");
static const std::string keyConventionalMean("ConventionalMean");
static const std::string keyStabilityFactor("StabilityFactor");

Variant::Root binnedChainGenerateVectorStatistics(QVector<double> values,
                                                  double average,
                                                  double vectored)
{
    Variant::Root result = binnedChainGenerateStatistics(values);
    Variant::Write stats = result.write();

    if (FP::defined(vectored)) {
        stats.hash(keyVectorMean).setDouble(vectored);
    }

    if (FP::defined(average)) {
        stats.hash(keyConventionalMean).setDouble(average);
    }

    if (FP::defined(vectored) && FP::defined(average) && average != 0.0) {
        stats.hash(keyStabilityFactor).setDouble(vectored / average);
    }

    return result;
}

void binnedChainGenerateAverage(const QVector<double> &values,
                                const QVector<double> &cover,
                                const QVector<double> &durations,
                                double start,
                                double end,
                                double &targetAverage,
                                double &targetCover)
{
    Q_ASSERT(values.size() == cover.size());
    Q_ASSERT(values.size() == durations.size());

    double totalTime = 0.0;
    double sum = 0.0;
    for (QVector<double>::const_iterator addValue = values.constBegin(),
            addCover = cover.constBegin(), addDuration = durations.constBegin(),
            endDuration = durations.constEnd();
            addDuration != endDuration;
            ++addValue, ++addCover, ++addDuration) {
        if (!FP::defined(*addValue))
            continue;

        if (!FP::defined(*addDuration) || (*addDuration) < 0.0) {
            /* An undefined duration means we can't weight them correctly,
             * so just assume they're all equal weight. */
            sum = 0.0;
            totalTime = 0.0;
            for (addValue = values.constBegin(), addCover = cover.constBegin(), addDuration =
                    durations.constBegin(), endDuration = durations.constEnd();
                    addDuration != endDuration;
                    ++addValue, ++addCover, ++addDuration) {
                if (!FP::defined(*addValue))
                    continue;
                if (!FP::defined(*addCover)) {
                    totalTime += 1.0;
                    sum += *addValue;
                } else if (*addCover > 0.0) {
                    sum += (*addValue) * (*addCover);
                    totalTime += *addCover;
                }
            }

            if (totalTime != 0.0) {
                targetAverage = sum / totalTime;
            } else {
                targetAverage = FP::undefined();
            }

            targetCover = FP::undefined();
            return;
        }

        if (!FP::defined(*addCover)) {
            sum += (*addValue) * (*addDuration);
            totalTime += *addDuration;
        } else if (*addCover > 0.0) {
            double tAdd = (*addDuration) * (*addCover);
            sum += (*addValue) * tAdd;
            totalTime += tAdd;
        }
    }

    if (totalTime != 0.0) {
        targetAverage = sum / totalTime;
        if (FP::defined(start) && FP::defined(end) && end > start) {
            targetCover = totalTime / (end - start);
        } else {
            targetCover = FP::undefined();
        }
    } else {
        targetAverage = FP::undefined();
        targetCover = FP::undefined();
    }
}

QDataStream &operator<<(QDataStream &stream, const FlagsSegmentData &value)
{
    stream << value.cover << value.flags;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, FlagsSegmentData &value)
{
    stream >> value.cover >> value.flags;
    return stream;
}

}

}
}
