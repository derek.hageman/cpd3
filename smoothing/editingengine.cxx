/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "smoothing/editingengine.hxx"
#include "core/environment.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/editingchain.hxx
 * The smoothing engines used in the editing system.
 */

static const SequenceName::Component archiveCont = "cont";
static const SequenceName::Component archiveAvg = "avg";
static const SequenceName::Component archiveStats = "stats";
static const SequenceName::Component archiveContMeta = "cont_meta";
static const SequenceName::Component archiveAvgMeta = "avg_meta";
static const SequenceName::Component archiveStatsMeta = "stats_meta";

EditingEngineBase::EditingEngineBase(Data::DynamicTimeInterval *setInterval,
                                     Data::DynamicDouble *setRequiredCover) : SmoothingEngine(
        nullptr), interval(setInterval), requiredCover(setRequiredCover), registeredInputs()
{ }

EditingEngineBase::~EditingEngineBase() = default;

void EditingEngineBase::registerExpectedInputs(const QSet<SequenceName> &inputs)
{ registeredInputs = inputs; }

void EditingEngineBase::deserialize(QDataStream &stream)
{
    interval.reset();
    requiredCover.reset();
    Data::DynamicTimeInterval *i;
    Data::DynamicDouble *c;
    stream >> i >> c >> registeredInputs;
    interval.reset(i);
    requiredCover.reset(c);
    SmoothingEngine::deserialize(stream);
}

void EditingEngineBase::serialize(QDataStream &stream)
{
    pauseProcessing();
    stream << interval.get() << requiredCover.get() << registeredInputs;
    serializeInternal(stream);
    resumeProcessing();
}

SequenceName::Set EditingEngineBase::requestedInputs()
{
    SequenceName::Set result;
    for (const auto &add : registeredInputs) {
        SequenceName unit = add;
        unit.removeFlavor(SequenceName::flavor_end);
        unit.removeFlavor(SequenceName::flavor_cover);
        unit.removeFlavor(SequenceName::flavor_stats);

        result.insert(unit);
        result.insert(unit.withFlavor(SequenceName::flavor_end));
        result.insert(unit.withFlavor(SequenceName::flavor_cover));

        unit.setArchive(archiveCont);
        result.insert(unit);
        result.insert(unit.withFlavor(SequenceName::flavor_end));
        result.insert(unit.withFlavor(SequenceName::flavor_cover));

        unit.setArchive(archiveAvg);
        result.insert(unit);
        result.insert(unit.withFlavor(SequenceName::flavor_end));
        result.insert(unit.withFlavor(SequenceName::flavor_cover));
    }
    return result;
}

SequenceName::Set EditingEngineBase::predictedOutputs()
{
    SequenceName::Set result;
    for (const auto &add : registeredInputs) {
        SequenceName unit = add;
        unit.removeFlavor(SequenceName::flavor_end);
        unit.removeFlavor(SequenceName::flavor_cover);
        unit.removeFlavor(SequenceName::flavor_stats);

        result.insert(unit);
        result.insert(unit.withFlavor(SequenceName::flavor_end));
        result.insert(unit.withFlavor(SequenceName::flavor_cover));
        result.insert(unit.withFlavor(SequenceName::flavor_stats));

        unit.setArchive(archiveCont);
        result.insert(unit);
        result.insert(unit.withFlavor(SequenceName::flavor_end));
        result.insert(unit.withFlavor(SequenceName::flavor_cover));
        result.insert(unit.withFlavor(SequenceName::flavor_stats));

        unit.setArchive(archiveAvg);
        result.insert(unit);
        result.insert(unit.withFlavor(SequenceName::flavor_end));
        result.insert(unit.withFlavor(SequenceName::flavor_cover));
        result.insert(unit.withFlavor(SequenceName::flavor_stats));
    }
    return result;
}

void EditingEngineBase::startProcessing(IncomingBuffer::iterator incomingBegin,
                                        IncomingBuffer::iterator incomingEnd,
                                        double &advanceTime,
                                        double &holdbackTime)
{
    SmoothingEngine::startProcessing(incomingBegin, incomingEnd, advanceTime, holdbackTime);
    FixedTimeBinner<const double *>::applyRounding(interval.get(), &holdbackTime, nullptr);
}

void EditingEngineBase::predictMetadataBounds(double *start, double *end)
{
    FixedTimeBinner<const double *>::applyRounding(interval.get(), start, end);
}


EditingEngineFullCalculate::EditingEngineFullCalculate(DynamicTimeInterval *setInterval,
                                                       DynamicDouble *setRequiredCover,
                                                       Data::SequenceMatch::Composite *variables)
        : EditingEngineBase(setInterval, setRequiredCover), core(), bypassIngress(nullptr),
          affectedVariables(variables)
{
    core.reset(new EditingChainCoreFullCalculate(setInterval ? setInterval->clone() : nullptr,
                                                 setRequiredCover ? setRequiredCover->clone()
                                                                  : nullptr));
    bypassIngress = createOutputIngress();
    if (affectedVariables)
        affectedVariables->clearArchiveSpecification();
}

EditingEngineFullCalculate::~EditingEngineFullCalculate() = default;

SmootherChainCore::HandlerMode EditingEngineFullCalculate::handlerMode(const Data::SequenceName &unit,
                                                                       SmootherChainCore::Type type)
{
    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    if (affectedVariables && !affectedVariables->matches(check))
        return SmootherChainCore::Unhandled;
    if (unit.getArchive() == archiveCont) {
        return SmootherChainCore::Repeatable;
    } else if (unit.getArchive() == archiveAvg) {
        return core->smootherHandler(type);
    } else {
        return SmootherChainCore::Unhandled;
    }
}

bool EditingEngineFullCalculate::handleFlags(const SequenceName &unit)
{
    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    if (affectedVariables && !affectedVariables->matches(check))
        return false;
    if (unit.getArchive() == archiveCont || unit.getArchive() == archiveAvg) {
        return core->flagsHandler();
    } else {
        return false;
    }
}

QSet<double> EditingEngineFullCalculate::getMetadataBreaks(const Data::SequenceName &unit)
{
    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    if (affectedVariables && !affectedVariables->matches(check))
        return QSet<double>();
    if (unit.getArchive() == archiveContMeta || unit.getArchive() == archiveAvgMeta) {
        return core->getProcessingMetadataBreaks();
    } else {
        return QSet<double>();
    }
}

bool EditingEngineFullCalculate::generatedOutput(const SequenceName &unit)
{
    if (unit.getArchive() == archiveCont || unit.getArchive() == archiveAvg) {
        return core->generatedOutput(unit);
    } else {
        return false;
    }
}

void EditingEngineFullCalculate::constructChain(const Data::SequenceName &triggeringUnit,
                                                SmoothingEngine::EngineInterface *interface,
                                                SmootherChainCore::Type type)
{
#ifndef NDEBUG
    SequenceName check(triggeringUnit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    if (affectedVariables && !affectedVariables->matches(check)) {
        Q_ASSERT(false);
        return;
    }
#endif

    if (triggeringUnit.getArchive() == archiveAvg) {
        core->createSmoother(interface, type);
    } else {
        Q_ASSERT(false);
    }
}

void EditingEngineFullCalculate::deserializeChain(const std::vector<Data::SequenceName> &inputUnits,
                                                  QDataStream &stream,
                                                  SmoothingEngine::EngineInterface *interface,
                                                  SmootherChainCore::Type type)
{
#ifndef NDEBUG
    for (const auto &check : inputUnits) {
        Q_ASSERT(check.getArchive() == archiveAvg);
    }
#else
    Q_UNUSED(inputUnits);
#endif
    core->deserializeSmoother(stream, interface, type);
}

void EditingEngineFullCalculate::constructFlagsChain(const Data::SequenceName &triggeringUnit,
                                                     SmoothingEngine::EngineInterface *interface)
{
#ifndef NDEBUG
    SequenceName check(triggeringUnit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    if (affectedVariables && !affectedVariables->matches(check)) {
        Q_ASSERT(false);
        return;
    }
#endif

    if (triggeringUnit.getArchive() == archiveAvg) {
        core->createFlags(interface);
    } else {
        Q_ASSERT(false);
    }
}

void EditingEngineFullCalculate::deserializeFlagsChain(const std::vector<
        Data::SequenceName> &inputUnits,
                                                       QDataStream &stream,
                                                       SmoothingEngine::EngineInterface *interface)
{
#ifndef NDEBUG
    for (const auto &check : inputUnits) {
        Q_ASSERT(check.getArchive() == archiveAvg);
    }
#else
    Q_UNUSED(inputUnits);
#endif
    core->deserializeFlags(stream, interface);
}

void EditingEngineFullCalculate::auxiliaryEndAfter()
{
    bypassIngress->endData();
    bypassIngress = nullptr;
}

void EditingEngineFullCalculate::auxiliaryAdvance(double advanceTime)
{
    bypassIngress->incomingAdvance(advanceTime);
}

EditingEngineFullCalculate::DispatchMetadata::DispatchMetadata(EditingEngineFullCalculate *engine,
                                                               const SequenceName &setUnit,
                                                               SinkMultiplexer::Sink *target)
        : SmoothingEngine::DispatchMetadata(engine, setUnit, target),
          parent(engine),
          unitCont(setUnit)
{
    unitCont.setArchive(archiveContMeta);
}

EditingEngineFullCalculate::DispatchMetadata::~DispatchMetadata() = default;

void EditingEngineFullCalculate::DispatchMetadata::incomingSegment(ValueSegment &&segment)
{
    rebuildChainIfNeeded(segment.getValue());

    double start = segment.getStart();
    double end = segment.getEnd();

    auto contRoot = segment.root();
    {
        auto contMeta = contRoot.write();
        parent->transformMetadata(parent->core.get(), this, unitCont, start, contMeta);
    }

    auto root = std::move(segment.root());
    {
        auto metadata = root.write();
        parent->transformMetadata(parent->core.get(), this, getUnit(), start, metadata);
    }

    if (!predictSegmentBounds(start, end))
        return;
    outputTarget->emplaceData(SequenceIdentity(unitCont, start, end), std::move(contRoot));
    outputTarget->emplaceData(SequenceIdentity(getUnit(), start, end), std::move(root));
}

void EditingEngineFullCalculate::DispatchMetadata::incomingData(SequenceValue &&value)
{
    for (auto &add : segmenter.add(std::move(value))) {
        incomingSegment(std::move(add));
    }
}

void EditingEngineFullCalculate::DispatchMetadata::incomingAdvance(double time)
{
    for (auto &add : segmenter.advance(time)) {
        incomingSegment(std::move(add));
    }
    advancePredicted(time);
}

void EditingEngineFullCalculate::DispatchMetadata::endData()
{
    for (auto &add : segmenter.finish()) {
        incomingSegment(std::move(add));
    }
    outputTarget->endData();
}

bool EditingEngineFullCalculate::alwaysBypassOutput(const SequenceName &unit)
{
    if (!affectedVariables)
        return false;
    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    return !affectedVariables->matches(check);
}

std::unique_ptr<SmoothingEngine::DispatchTarget> EditingEngineFullCalculate::createMetaDispatch(
        const Data::SequenceName &unit,
        SmoothingEngine::DispatchTarget *underlay)
{
    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    if (affectedVariables && !affectedVariables->matches(check))
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchBypass(bypassIngress));

    if (unit.getArchive() == archiveAvgMeta) {
        auto meta = new DispatchMetadata(this, unit, createOutputIngress());
        auto defaultUnderlay = static_cast<DispatchDefaultUnderlay *>(underlay);
        defaultUnderlay->addForwardTarget(meta);
        meta->setBreaks(getMetadataBreaks(unit));
        meta->overlaySegmenter(defaultUnderlay->getSegmenter());
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(meta);
    } else if (unit.getArchive() == archiveContMeta) {
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchDiscard);
    } else {
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchBypass(bypassIngress));
    }
}

std::unique_ptr<SmoothingEngine::DispatchTarget> EditingEngineFullCalculate::createDataDispatch(
        const Data::SequenceName &unit,
        SmoothingEngine::DispatchTarget *underlay,
        SmoothingEngine::DispatchTarget *meta)
{
    if (unit.getArchive() == archiveAvg) {
        return EditingEngineBase::createDataDispatch(unit, underlay, meta);
    } else if (unit.getArchive() == archiveCont) {
        if (affectedVariables) {
            SequenceName check(unit);
            check.removeFlavor(SequenceName::flavor_cover);
            check.removeFlavor(SequenceName::flavor_stats);
            check.removeFlavor(SequenceName::flavor_end);
            if (!affectedVariables->matches(check)) {
                return std::unique_ptr<SmoothingEngine::DispatchTarget>(
                        new DispatchBypass(bypassIngress));
            }
        }
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchDiscard);
    } else {
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchBypass(bypassIngress));
    }
}


EditingEngineContinuousRecalculate::EditingEngineContinuousRecalculate(DynamicTimeInterval *setInterval,
                                                                       DynamicDouble *setRequiredCover,
                                                                       SequenceMatch::Composite *variables)
        : EditingEngineBase(setInterval, setRequiredCover), core(), bypassIngress(nullptr),
          affectedVariables(variables)
{
    core.reset(
            new EditingChainCoreContinuousRecalculate(setInterval ? setInterval->clone() : nullptr,
                                                      setRequiredCover ? setRequiredCover->clone()
                                                                       : nullptr));
    bypassIngress = createOutputIngress();
    if (affectedVariables)
        affectedVariables->clearArchiveSpecification();
}

EditingEngineContinuousRecalculate::~EditingEngineContinuousRecalculate() = default;

SmootherChainCore::HandlerMode EditingEngineContinuousRecalculate::handlerMode(const Data::SequenceName &unit,
                                                                               SmootherChainCore::Type type)
{
    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    if (affectedVariables && !affectedVariables->matches(check))
        return SmootherChainCore::Unhandled;
    if (unit.getArchive() == archiveCont) {
        return core->smootherHandler(type);
    } else {
        return SmootherChainCore::Unhandled;
    }
}

bool EditingEngineContinuousRecalculate::handleFlags(const SequenceName &unit)
{
    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    if (affectedVariables && !affectedVariables->matches(check))
        return false;
    if (unit.getArchive() == archiveCont) {
        return core->flagsHandler();
    } else {
        return false;
    }
}

QSet<double> EditingEngineContinuousRecalculate::getMetadataBreaks(const Data::SequenceName &unit)
{
    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    if (affectedVariables && !affectedVariables->matches(check))
        return QSet<double>();
    if (unit.getArchive() == archiveContMeta) {
        return core->getProcessingMetadataBreaks();
    } else {
        return QSet<double>();
    }
}

bool EditingEngineContinuousRecalculate::generatedOutput(const SequenceName &unit)
{
    if (unit.getArchive() == archiveCont) {
        return core->generatedOutput(unit);
    } else {
        return false;
    }
}

void EditingEngineContinuousRecalculate::constructChain(const Data::SequenceName &triggeringUnit,
                                                        SmoothingEngine::EngineInterface *interface,
                                                        SmootherChainCore::Type type)
{
#ifndef NDEBUG
    SequenceName check(triggeringUnit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    if (affectedVariables && !affectedVariables->matches(check)) {
        Q_ASSERT(false);
        return;
    }
#endif

    if (triggeringUnit.getArchive() == archiveCont) {
        core->createSmoother(interface, type);
    } else {
        Q_ASSERT(false);
    }
}

void EditingEngineContinuousRecalculate::deserializeChain(const std::vector<
        Data::SequenceName> &inputUnits,
                                                          QDataStream &stream,
                                                          SmoothingEngine::EngineInterface *interface,
                                                          SmootherChainCore::Type type)
{
#ifndef NDEBUG
    for (const auto &check : inputUnits) {
        Q_ASSERT(check.getArchive() == archiveCont);
    }
#else
    Q_UNUSED(inputUnits);
#endif
    core->deserializeSmoother(stream, interface, type);
}

void EditingEngineContinuousRecalculate::constructFlagsChain(const Data::SequenceName &triggeringUnit,
                                                             SmoothingEngine::EngineInterface *interface)
{
#ifndef NDEBUG
    SequenceName check(triggeringUnit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    if (affectedVariables && !affectedVariables->matches(triggeringUnit)) {
        Q_ASSERT(false);
        return;
    }
#endif

    if (triggeringUnit.getArchive() == archiveCont) {
        core->createFlags(interface);
    } else {
        Q_ASSERT(false);
    }
}

void EditingEngineContinuousRecalculate::deserializeFlagsChain(const std::vector<
        Data::SequenceName> &inputUnits,
                                                               QDataStream &stream,
                                                               SmoothingEngine::EngineInterface *interface)
{
#ifndef NDEBUG
    for (const auto &check : inputUnits) {
        Q_ASSERT(check.getArchive() == archiveCont);
    }
#else
    Q_UNUSED(inputUnits);
#endif
    core->deserializeFlags(stream, interface);
}

void EditingEngineContinuousRecalculate::auxiliaryEndAfter()
{
    bypassIngress->endData();
    bypassIngress = nullptr;
}

void EditingEngineContinuousRecalculate::auxiliaryAdvance(double advanceTime)
{
    bypassIngress->incomingAdvance(advanceTime);
}

EditingEngineContinuousRecalculate::DispatchMetadata::DispatchMetadata(
        EditingEngineContinuousRecalculate *engine,
        const SequenceName &setUnit,
        SinkMultiplexer::Sink *target) : SmoothingEngine::DispatchMetadata(engine, setUnit, target),
                                         parent(engine),
                                         unitAvg(setUnit),
                                         unitCont(setUnit)
{
    Q_ASSERT(setUnit.getArchive() == archiveContMeta);
    unitAvg.setArchive(archiveAvgMeta);
}

EditingEngineContinuousRecalculate::DispatchMetadata::~DispatchMetadata() = default;

void EditingEngineContinuousRecalculate::DispatchMetadata::incomingSegment(ValueSegment &&segment)
{
    rebuildChainIfNeeded(segment.getValue());

    double start = segment.getStart();
    double end = segment.getEnd();
    auto root = segment.root();
    {
        auto metadata = root.write();
        parent->transformMetadata(parent->core.get(), this, unitAvg, start, metadata);
    }

    bool ok = predictSegmentBounds(start, end);

    /* Have to use the predicted start here, so we don't get out of order, even if
     * we're not altering the actual value, however it shouldn't matter since this
     * is only used after full averaging, so it should already be rounded to begin with */
    outputTarget->emplaceData(SequenceIdentity(unitCont, start, segment.getEnd()),
                              std::move(segment.root()));
    if (!ok)
        return;
    outputTarget->emplaceData(SequenceIdentity(unitAvg, start, end), std::move(root));
}

void EditingEngineContinuousRecalculate::DispatchMetadata::incomingData(SequenceValue &&value)
{
    for (auto &add : segmenter.add(std::move(value))) {
        incomingSegment(std::move(add));
    }
}

void EditingEngineContinuousRecalculate::DispatchMetadata::incomingAdvance(double time)
{
    for (auto &add : segmenter.advance(time)) {
        incomingSegment(std::move(add));
    }
    advancePredicted(time);
}

void EditingEngineContinuousRecalculate::DispatchMetadata::endData()
{
    for (auto &add : segmenter.finish()) {
        incomingSegment(std::move(add));
    }
    outputTarget->endData();
}

bool EditingEngineContinuousRecalculate::alwaysBypassOutput(const SequenceName &unit)
{
    if (!affectedVariables)
        return false;
    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    return !affectedVariables->matches(check);
}

std::unique_ptr<
        SmoothingEngine::DispatchTarget> EditingEngineContinuousRecalculate::createMetaDispatch(
        const SequenceName &unit,
        SmoothingEngine::DispatchTarget *underlay)
{
    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);
    if (affectedVariables && !affectedVariables->matches(check))
        return std::unique_ptr<DispatchTarget>(new DispatchBypass(bypassIngress));
    if (unit.getArchive() == archiveContMeta) {
        auto meta = new DispatchMetadata(this, unit, createOutputIngress());
        auto defaultUnderlay = static_cast<DispatchDefaultUnderlay *>(underlay);
        defaultUnderlay->addForwardTarget(meta);
        meta->setBreaks(getMetadataBreaks(unit));
        meta->overlaySegmenter(defaultUnderlay->getSegmenter());
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(meta);
    } else if (unit.getArchive() == archiveAvgMeta) {
        if (affectedVariables) {
            check = unit;
            check.removeFlavor(SequenceName::flavor_cover);
            check.removeFlavor(SequenceName::flavor_stats);
            check.removeFlavor(SequenceName::flavor_end);
            if (!affectedVariables->matches(unit)) {
                return std::unique_ptr<SmoothingEngine::DispatchTarget>(
                        new DispatchBypass(bypassIngress));
            }
        }
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchDiscard);
    } else {
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchBypass(bypassIngress));
    }
}

std::unique_ptr<
        SmoothingEngine::DispatchTarget> EditingEngineContinuousRecalculate::createDataDispatch(
        const SequenceName &unit,
        SmoothingEngine::DispatchTarget *underlay,
        SmoothingEngine::DispatchTarget *meta)
{
    Q_UNUSED(meta);

    if (unit.getArchive() == archiveCont) {
        auto data = new DispatchData(this, unit);
        auto defaultUnderlay = static_cast<DispatchDefaultUnderlay *>(underlay);
        defaultUnderlay->addForwardTarget(data);
        data->overlaySegmenter(defaultUnderlay->getSegmenter());
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(data);
    } else if (unit.getArchive() == archiveAvg) {
        if (affectedVariables) {
            SequenceName check(unit);
            check.removeFlavor(SequenceName::flavor_cover);
            check.removeFlavor(SequenceName::flavor_stats);
            check.removeFlavor(SequenceName::flavor_end);
            if (!affectedVariables->matches(check)) {
                return std::unique_ptr<SmoothingEngine::DispatchTarget>(
                        new DispatchBypass(bypassIngress));
            }
        }
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchDiscard);
    } else {
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchBypass(bypassIngress));
    }
}

EditingEngineContinuousRecalculate::DispatchData::DispatchData(EditingEngineContinuousRecalculate *engine,
                                                               const SequenceName &setUnit)
        : SmoothingEngine::DispatchData(engine, setUnit),
          outputUnit(setUnit),
          bypass(engine->bypassIngress)
{
    if (engine->affectedVariables) {
        SequenceName check(setUnit);
        check.removeFlavor(SequenceName::flavor_cover);
        check.removeFlavor(SequenceName::flavor_stats);
        check.removeFlavor(SequenceName::flavor_end);
        if (!engine->affectedVariables->matches(check))
            bypass = nullptr;
    }

    outputUnit.setArchive(archiveAvg);
}

EditingEngineContinuousRecalculate::DispatchData::~DispatchData() = default;

SequenceName EditingEngineContinuousRecalculate::DispatchData::getOutputUnit()
{ return outputUnit; }

void EditingEngineContinuousRecalculate::DispatchData::incomingData(SequenceValue &&value)
{
    if (bypass)
        bypass->incomingData(value);
    SmoothingEngine::DispatchData::incomingData(std::move(value));
}

EditingEngineContinuousRecalculate::EngineInterface::EngineInterface(
        EditingEngineContinuousRecalculate *engine) : SmoothingEngine::EngineInterface(engine)
{ }

EditingEngineContinuousRecalculate::EngineInterface::~EngineInterface() = default;

SmootherChainTarget *EditingEngineContinuousRecalculate::EngineInterface::addNewOutput(const SequenceName &unit)
{
    SequenceName output(unit);
    output.setArchive(archiveAvg);
    return SmoothingEngine::EngineInterface::addNewOutput(output);
}

SmootherChainTargetFlags *EditingEngineContinuousRecalculate::EngineInterface::addNewFlagsOutput(
        const SequenceName &unit)
{
    SequenceName output(unit);
    output.setArchive(archiveAvg);
    return SmoothingEngine::EngineInterface::addNewFlagsOutput(output);
}

SmootherChainTargetGeneral *EditingEngineContinuousRecalculate::EngineInterface::addNewGeneralOutput(
        const SequenceName &unit)
{
    SequenceName output(unit);
    output.setArchive(archiveAvg);
    return SmoothingEngine::EngineInterface::addNewGeneralOutput(output);
}

std::unique_ptr<
        SmoothingEngine::EngineInterface> EditingEngineContinuousRecalculate::createEngineInterface()
{ return std::unique_ptr<SmoothingEngine::EngineInterface>(new EngineInterface(this)); }


EditingEngineFinal::DispatchBypassRename::DispatchBypassRename(SinkMultiplexer::Sink *t,
                                                               const SequenceName &setUnit)
        : SmoothingEngine::DispatchBypass(t), unit(setUnit)
{ }

EditingEngineFinal::DispatchBypassRename::~DispatchBypassRename() = default;

void EditingEngineFinal::DispatchBypassRename::incomingData(SequenceValue &&value)
{
    value.setUnit(unit);
    SmoothingEngine::DispatchBypass::incomingData(std::move(value));
}

EditingEngineFinal::DispatchData::DispatchData(EditingEngineFinal *engine,
                                               const SequenceName &setUnit)
        : SmoothingEngine::DispatchData(engine, setUnit), parent(engine), outputUnit(setUnit)
{
    outputUnit.setArchive(engine->averageArchive);
}

EditingEngineFinal::DispatchData::~DispatchData() = default;

SequenceName EditingEngineFinal::DispatchData::getOutputUnit()
{ return outputUnit; }

bool EditingEngineFinal::DispatchData::discardOutput(const SequenceName &unit)
{
    if (parent->alwaysBypassOutput(unit))
        return true;
    if (!parent->fullVariables && !parent->continuousVariables)
        return true;

    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);

    Q_ASSERT(check.getArchive() == parent->averageArchive);

    if (parent->calculateFull(check)) {
        return false;
    }

    if (parent->calculateContinuous(check)) {
        return false;
    }

    /* It's a statistics value output, so always discard it */
    return true;
}

EditingEngineFinal::DispatchDataClean::DispatchDataClean(EditingEngineFinal *engine,
                                                         const SequenceName &setUnit)
        : DispatchData(engine, setUnit), cleanUnit(setUnit), bypass(engine->bypassIngress)
{
    cleanUnit.setArchive(engine->cleanArchive);
}

EditingEngineFinal::DispatchDataClean::~DispatchDataClean() = default;

void EditingEngineFinal::DispatchDataClean::incomingData(SequenceValue &&value)
{
    {
        SequenceValue cleanValue = value;
        cleanValue.setUnit(cleanUnit);
        bypass->incomingData(std::move(cleanValue));
    }
    SmoothingEngine::DispatchData::incomingData(std::move(value));
}

EditingEngineFinal::DispatchDataStatistics::DispatchDataStatistics(EditingEngineFinal *engine,
                                                                   const SequenceName &setUnit)
        : DispatchData(engine, setUnit)
{ }

EditingEngineFinal::DispatchDataStatistics::~DispatchDataStatistics() = default;

bool EditingEngineFinal::DispatchDataStatistics::discardOutput(const SequenceName &)
{ return true; }

EditingEngineFinal::DispatchDataDiscard::DispatchDataDiscard(EditingEngineFinal *engine,
                                                             const SequenceName &setUnit)
        : DispatchData(engine, setUnit)
{ }

EditingEngineFinal::DispatchDataDiscard::~DispatchDataDiscard() = default;

bool EditingEngineFinal::DispatchDataDiscard::discardOutput(const SequenceName &unit)
{
    Q_UNUSED(unit);
    return true;
}

bool EditingEngineFinal::DispatchDataDiscard::disableChain()
{ return true; }


EditingEngineFinal::DispatchMetadataDiscard::DispatchMetadataDiscard(EditingEngineFinal *engine,
                                                                     const SequenceName &setUnit)
        : SmoothingEngine::DispatchMetadata(engine, setUnit, nullptr)
{ }

EditingEngineFinal::DispatchMetadataDiscard::~DispatchMetadataDiscard() = default;

void EditingEngineFinal::DispatchMetadataDiscard::incomingSegment(ValueSegment &&segment)
{ rebuildChainIfNeeded(std::move(segment.getValue())); }

void EditingEngineFinal::DispatchMetadataDiscard::incomingData(SequenceValue &&value)
{
    for (auto &add : segmenter.add(std::move(value))) {
        incomingSegment(std::move(add));
    }
}

void EditingEngineFinal::DispatchMetadataDiscard::incomingAdvance(double time)
{
    for (auto &add : segmenter.advance(time)) {
        incomingSegment(std::move(add));
    }
}

void EditingEngineFinal::DispatchMetadataDiscard::endData()
{
    for (auto &add : segmenter.finish()) {
        incomingSegment(std::move(add));
    }
}


EditingEngineFinal::DispatchMetadataRecalculate::DispatchMetadataRecalculate(EditingEngineFinal *engine,
                                                                             SmootherChainCore *c,
                                                                             const SequenceName &setUnit,
                                                                             SinkMultiplexer::Sink *target)
        : SmoothingEngine::DispatchMetadata(engine, setUnit, target), parent(engine), core(c),
          unitAverage(setUnit)
{
    unitAverage.setArchive(engine->averageArchiveMeta);
}

EditingEngineFinal::DispatchMetadataRecalculate::~DispatchMetadataRecalculate() = default;

void EditingEngineFinal::DispatchMetadataRecalculate::incomingSegment(ValueSegment &&segment)
{
    rebuildChainIfNeeded(segment.getValue());

    auto root = std::move(segment.root());
    double start = segment.getStart();
    double end = segment.getEnd();
    {
        auto metadata = root.write();
        parent->transformMetadata(core, this, unitAverage, start, metadata);
    }
    if (!predictSegmentBounds(start, end))
        return;
    outputTarget->emplaceData(SequenceIdentity(unitAverage, start, end), std::move(root));
}

void EditingEngineFinal::DispatchMetadataRecalculate::incomingData(SequenceValue &&value)
{
    for (auto &add : segmenter.add(std::move(value))) {
        incomingSegment(std::move(add));
    }
}

void EditingEngineFinal::DispatchMetadataRecalculate::incomingAdvance(double time)
{
    for (auto &add : segmenter.advance(time)) {
        incomingSegment(std::move(add));
    }
    advancePredicted(time);
}

void EditingEngineFinal::DispatchMetadataRecalculate::endData()
{
    for (auto &add : segmenter.finish()) {
        incomingSegment(std::move(add));
    }
    outputTarget->endData();
}


EditingEngineFinal::DispatchMetadataAverage::DispatchMetadataAverage(EditingEngineFinal *engine,
                                                                     const SequenceName &setUnit,
                                                                     SinkMultiplexer::Sink *target)
        : SmoothingEngine::DispatchMetadata(engine, setUnit, target),
          parent(engine),
          unitAverage(setUnit)
{
    unitAverage.setArchive(engine->averageArchiveMeta);
}

EditingEngineFinal::DispatchMetadataAverage::~DispatchMetadataAverage() = default;

void EditingEngineFinal::DispatchMetadataAverage::incomingSegment(ValueSegment &&segment)
{
    auto root = std::move(segment.root());
    double start = segment.getStart();
    double end = segment.getEnd();
    {
        auto metadata = root.write();
        parent->transformMetadata(parent->statisticsCore.get(), this, unitAverage, start, metadata);
    }
    if (!predictSegmentBounds(start, end))
        return;
    outputTarget->emplaceData(SequenceIdentity(unitAverage, start, end), std::move(root));
}

void EditingEngineFinal::DispatchMetadataAverage::incomingData(SequenceValue &&value)
{
    for (auto &add : segmenter.add(std::move(value))) {
        incomingSegment(std::move(add));
    }
}

void EditingEngineFinal::DispatchMetadataAverage::incomingAdvance(double time)
{
    for (auto &add : segmenter.advance(time)) {
        incomingSegment(std::move(add));
    }
    advancePredicted(time);
}

void EditingEngineFinal::DispatchMetadataAverage::endData()
{
    for (auto &add : segmenter.finish()) {
        incomingSegment(std::move(add));
    }
    outputTarget->endData();
}


EditingEngineFinal::EditingEngineFinal(DynamicTimeInterval *setInterval,
                                       DynamicDouble *setRequiredCover,
                                       SequenceMatch::Composite *variablesContinuous,
                                       SequenceMatch::Composite *variablesFull,
                                       const SequenceName::Component &setClean,
                                       const SequenceName::Component &setAverage)
        : EditingEngineBase(setInterval, setRequiredCover),
          statisticsCore(),
          continuousCore(),
          fullCore(),
          discardCore(
                                                                            new SmootherChainCoreDiscard),
          bypassIngress(nullptr),
          continuousVariables(
                                                                            variablesContinuous),
          fullVariables(variablesFull),
          cleanArchive(setClean),
          averageArchive(setAverage),
          cleanArchiveMeta(setClean),
          averageArchiveMeta(setAverage)
{
    statisticsCore.reset(
            new EditingChainCoreFinalStatistics(setInterval ? setInterval->clone() : nullptr,
                                                setRequiredCover ? setRequiredCover->clone()
                                                                 : nullptr));
    continuousCore.reset(
            new EditingChainCoreContinuousRecalculate(setInterval ? setInterval->clone() : nullptr,
                                                      setRequiredCover ? setRequiredCover->clone()
                                                                       : nullptr));
    fullCore.reset(
            new EditingChainCoreFinalRecalculate(setInterval ? setInterval->clone() : nullptr,
                                                 setRequiredCover ? setRequiredCover->clone()
                                                                  : nullptr));
    bypassIngress = createOutputIngress();
    if (continuousVariables)
        continuousVariables->clearArchiveSpecification();
    if (fullVariables)
        fullVariables->clearArchiveSpecification();

    cleanArchiveMeta.append("_meta");
    averageArchiveMeta.append("_meta");
}

EditingEngineFinal::~EditingEngineFinal() = default;

SmootherChainCore::HandlerMode EditingEngineFinal::handlerMode(const Data::SequenceName &unit,
                                                               SmootherChainCore::Type type)
{
    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);

    if (check.getArchive() == archiveStats) {
        if (calculateFull(check)) {
            return fullCore->smootherHandler(type);
        }
    }

    if (check.getArchive() == archiveCont) {
        if (calculateContinuous(check)) {
            return continuousCore->smootherHandler(type);
        }
        return discardCore->smootherHandler(type);
    }

    if (check.getArchive() == archiveAvg) {
        if (calculateFull(check) || calculateContinuous(check)) {
            return discardCore->smootherHandler(type);
        }
    }

    return SmootherChainCore::Repeatable;
}

bool EditingEngineFinal::handleFlags(const SequenceName &unit)
{
    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);

    if (check.getArchive() == archiveStats) {
        if (calculateFull(check)) {
            return fullCore->flagsHandler();
        }
    }

    if (check.getArchive() == archiveCont) {
        if (calculateContinuous(check)) {
            return continuousCore->flagsHandler();
        }
        return discardCore->flagsHandler();
    }

    if (check.getArchive() == archiveAvg) {
        if (calculateFull(check) || calculateContinuous(check)) {
            return discardCore->flagsHandler();
        }
    }

    return statisticsCore->flagsHandler();
}

QSet<double> EditingEngineFinal::getMetadataBreaks(const Data::SequenceName &unit)
{
    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);

    if (check.getArchive() == archiveStats) {
        if (calculateFull(check)) {
            return fullCore->getProcessingMetadataBreaks();
        }
    }

    if (check.getArchive() == archiveCont) {
        if (calculateContinuous(check)) {
            return continuousCore->getProcessingMetadataBreaks();
        }
        return discardCore->getProcessingMetadataBreaks();
    }

    if (check.getArchive() == archiveAvg) {
        if (calculateFull(check) || calculateContinuous(check)) {
            return discardCore->getProcessingMetadataBreaks();
        }
    }

    return statisticsCore->getProcessingMetadataBreaks();
}

bool EditingEngineFinal::generatedOutput(const SequenceName &unit)
{
    /* Cover is always either (re-)generated or passed through an alternate
     * bypass, so it's always generated by something else. */
    if (unit.hasFlavor(SequenceName::flavor_cover))
        return true;

    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);

    if (check.getArchive() == archiveStats) {
        if (calculateFull(check)) {
            return fullCore->generatedOutput(unit);
        }
    }

    if (check.getArchive() == archiveCont) {
        if (calculateContinuous(check)) {
            return continuousCore->generatedOutput(unit);
        }
        return discardCore->generatedOutput(unit);
    }

    if (check.getArchive() == archiveAvg) {
        if (calculateFull(check) || calculateContinuous(check)) {
            return discardCore->generatedOutput(unit);
        }
    }

    return statisticsCore->generatedOutput(unit);
}

void EditingEngineFinal::constructChain(const Data::SequenceName &triggeringUnit,
                                        SmoothingEngine::EngineInterface *interface,
                                        SmootherChainCore::Type type)
{
    SequenceName check(triggeringUnit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);

    if (check.getArchive() == archiveStats) {
        if (calculateFull(check)) {
            fullCore->createSmoother(interface, type);
            return;
        }
    }

    if (check.getArchive() == archiveCont) {
        if (calculateContinuous(check)) {
            continuousCore->createSmoother(interface, type);
            return;
        }
        discardCore->createSmoother(interface, type);
        return;
    }

    if (check.getArchive() == archiveAvg) {
        if (calculateFull(check) || calculateContinuous(check)) {
            discardCore->createSmoother(interface, type);
            return;
        }
    }

    statisticsCore->createSmoother(interface, type);
}

SmootherChainCore *EditingEngineFinal::checkDeserialize(const Data::SequenceName &unit)
{
    if (calculateFull(unit)) {
        if (unit.getArchive() == archiveStats) {
            return fullCore.get();
        } else if (unit.getArchive() == archiveAvg || unit.getArchive() == archiveCont) {
            return discardCore.get();
        }
    }
    if (calculateContinuous(unit)) {
        if (unit.getArchive() == archiveCont) {
            return continuousCore.get();
        } else if (unit.getArchive() == archiveAvg || unit.getArchive() == archiveStats) {
            return discardCore.get();
        }
    }
    return nullptr;
}

void EditingEngineFinal::deserializeChain(const std::vector<Data::SequenceName> &inputUnits,
                                          QDataStream &stream,
                                          SmoothingEngine::EngineInterface *interface,
                                          SmootherChainCore::Type type)
{
    SmootherChainCore *checkCore = nullptr;

    switch (type) {
    case SmootherChainCore::General:
    case SmootherChainCore::DifferenceInitial:
        Q_ASSERT(!inputUnits.empty());
        checkCore = checkDeserialize(inputUnits.front());
        if (checkCore) {
            checkCore->deserializeSmoother(stream, interface, type);
            return;
        }
        break;
    case SmootherChainCore::Vector2D:
    case SmootherChainCore::Difference:
        Q_ASSERT(inputUnits.size() >= 2);
        checkCore = checkDeserialize(inputUnits[0]);
        if (checkCore) {
            checkCore->deserializeSmoother(stream, interface, type);
            return;
        }
        checkCore = checkDeserialize(inputUnits[1]);
        if (checkCore) {
            checkCore->deserializeSmoother(stream, interface, type);
            return;
        }
        break;
    case SmootherChainCore::Vector3D:
        Q_ASSERT(inputUnits.size() >= 3);
        checkCore = checkDeserialize(inputUnits[0]);
        if (checkCore) {
            checkCore->deserializeSmoother(stream, interface, type);
            return;
        }
        checkCore = checkDeserialize(inputUnits[1]);
        if (checkCore) {
            checkCore->deserializeSmoother(stream, interface, type);
            return;
        }
        checkCore = checkDeserialize(inputUnits[2]);
        if (checkCore) {
            checkCore->deserializeSmoother(stream, interface, type);
            return;
        }
        break;
    case SmootherChainCore::Dewpoint:
    case SmootherChainCore::RH:
    case SmootherChainCore::BeersLawAbsorptionInitial:
        Q_ASSERT(inputUnits.size() >= 3);
        checkCore = checkDeserialize(inputUnits[2]);
        if (checkCore) {
            checkCore->deserializeSmoother(stream, interface, type);
            return;
        }
        break;
    case SmootherChainCore::RHExtrapolate:
        Q_ASSERT(inputUnits.size() >= 4);
        checkCore = checkDeserialize(inputUnits[3]);
        if (checkCore) {
            checkCore->deserializeSmoother(stream, interface, type);
            return;
        }
        break;
    case SmootherChainCore::BeersLawAbsorption:
        Q_ASSERT(inputUnits.size() >= 5);
        checkCore = checkDeserialize(inputUnits[4]);
        if (checkCore) {
            checkCore->deserializeSmoother(stream, interface, type);
            return;
        }
        break;
    }

    statisticsCore->deserializeSmoother(stream, interface, type);
}

void EditingEngineFinal::constructFlagsChain(const Data::SequenceName &triggeringUnit,
                                             SmoothingEngine::EngineInterface *interface)
{
    SequenceName check(triggeringUnit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);

    if (check.getArchive() == archiveStats) {
        if (calculateFull(check)) {
            static_cast<SmootherChainCore *>(fullCore.get())->createFlags(interface);
            return;
        }
    }

    if (check.getArchive() == archiveCont) {
        if (calculateContinuous(check)) {
            static_cast<SmootherChainCore *>(continuousCore.get())->createFlags(interface);
            return;
        }
    }

    if (check.getArchive() == archiveAvg) {
        if (calculateFull(check) || calculateContinuous(check)) {
            static_cast<SmootherChainCore *>(discardCore.get())->createFlags(interface);
            return;
        }
    }

    static_cast<SmootherChainCore *>(statisticsCore.get())->createFlags(interface);
}

void EditingEngineFinal::deserializeFlagsChain(const std::vector<Data::SequenceName> &inputUnits,
                                               QDataStream &stream,
                                               SmoothingEngine::EngineInterface *interface)
{
    Q_ASSERT(!inputUnits.empty());

    SmootherChainCore *checkCore = checkDeserialize(inputUnits.front());
    if (checkCore) {
        checkCore->deserializeFlags(stream, interface);
        return;
    }

    static_cast<SmootherChainCore *>(statisticsCore.get())->deserializeFlags(stream, interface);
}

void EditingEngineFinal::auxiliaryEndAfter()
{
    bypassIngress->endData();
    bypassIngress = nullptr;
}

void EditingEngineFinal::auxiliaryAdvance(double advanceTime)
{
    bypassIngress->incomingAdvance(advanceTime);
}

std::unique_ptr<
        SmoothingEngine::DispatchTarget> EditingEngineFinal::createMetaDispatch(const SequenceName &unit,
                                                                                SmoothingEngine::DispatchTarget *underlay)
{
    if (unit.hasFlavor(SequenceName::flavor_stats))
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchDiscard);

    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);

    if (unit.getArchive() == archiveStatsMeta) {
        DispatchMetadata *meta;
        if (calculateFull(check)) {
            meta = new DispatchMetadataRecalculate(this, fullCore.get(), unit,
                                                   createOutputIngress());
        } else {
            meta = new DispatchMetadataDiscard(this, unit);
        }
        auto defaultUnderlay = static_cast<DispatchDefaultUnderlay *>(underlay);
        defaultUnderlay->addForwardTarget(meta);
        meta->setBreaks(getMetadataBreaks(unit));
        meta->overlaySegmenter(defaultUnderlay->getSegmenter());
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(meta);
    }

    if (unit.getArchive() == archiveContMeta) {
        /* Short circuit for no possible cont chains (never need alternate inputs) */
        if (!continuousVariables)
            return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchDiscard);
        DispatchMetadata *meta;
        if (calculateContinuous(check) && !calculateFull(check)) {
            meta = new DispatchMetadataRecalculate(this, continuousCore.get(), unit,
                                                   createOutputIngress());
        } else {
            meta = new DispatchMetadataDiscard(this, unit);
        }
        auto defaultUnderlay = static_cast<DispatchDefaultUnderlay *>(underlay);
        defaultUnderlay->addForwardTarget(meta);
        meta->setBreaks(getMetadataBreaks(unit));
        meta->overlaySegmenter(defaultUnderlay->getSegmenter());
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(meta);
    }

    if (unit.getArchive() == archiveAvgMeta) {
        if (calculateContinuous(check) || calculateFull(check))
            return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchDiscard);
        auto meta = new DispatchMetadataAverage(this, unit, createOutputIngress());
        auto defaultUnderlay = static_cast<DispatchDefaultUnderlay *>(underlay);
        defaultUnderlay->addForwardTarget(meta);
        meta->setBreaks(getMetadataBreaks(unit));
        meta->overlaySegmenter(defaultUnderlay->getSegmenter());
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(meta);
    }

    SequenceName realUnit(unit);
    realUnit.setArchive(cleanArchiveMeta);
    return std::unique_ptr<SmoothingEngine::DispatchTarget>(
            new DispatchBypassRename(bypassIngress, realUnit));
}

std::unique_ptr<
        SmoothingEngine::DispatchTarget> EditingEngineFinal::createDataDispatch(const Data::SequenceName &unit,
                                                                                SmoothingEngine::DispatchTarget *underlay,
                                                                                SmoothingEngine::DispatchTarget *meta)
{
    Q_UNUSED(meta);

    if (unit.hasFlavor(SequenceName::flavor_stats)) {
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchDiscard);
    }

    SequenceName check(unit);
    check.removeFlavor(SequenceName::flavor_cover);
    check.removeFlavor(SequenceName::flavor_stats);
    check.removeFlavor(SequenceName::flavor_end);

    if (unit.getArchive() == archiveStats) {
        auto defaultUnderlay = static_cast<DispatchDefaultUnderlay *>(underlay);
        DispatchData *data;
        if (calculateFull(check)) {
            data = new DispatchData(this, unit);
        } else {
            /* Need to setup chains for statistics calculation, but not actual outputs so
             * we can't fully discard */
            data = new DispatchDataStatistics(this, unit);
        }
        defaultUnderlay->addForwardTarget(data);
        data->overlaySegmenter(defaultUnderlay->getSegmenter());
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(data);
    }

    if (unit.getArchive() == archiveCont) {
        /* Short circuit for no possible cont chains (never need alternate inputs) */
        if (!continuousVariables)
            return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchDiscard);
        auto defaultUnderlay = static_cast<DispatchDefaultUnderlay *>(underlay);
        DispatchData *data;
        if (calculateContinuous(check) && !calculateFull(check)) {
            data = new DispatchData(this, unit);
        } else {
            data = new DispatchDataDiscard(this, unit);
        }
        defaultUnderlay->addForwardTarget(data);
        data->overlaySegmenter(defaultUnderlay->getSegmenter());
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(data);
    }

    if (unit.getArchive() == archiveAvg) {
        if (calculateContinuous(check) || calculateFull(check)) {
            return std::unique_ptr<SmoothingEngine::DispatchTarget>(new DispatchDiscard);
        }
        SequenceName realUnit(unit);
        realUnit.setArchive(averageArchive);
        return std::unique_ptr<SmoothingEngine::DispatchTarget>(
                new DispatchBypassRename(bypassIngress, realUnit));
    }

    SequenceName realUnit(unit);
    realUnit.setArchive(cleanArchive);
    return std::unique_ptr<SmoothingEngine::DispatchTarget>(
            new DispatchBypassRename(bypassIngress, realUnit));
}

EditingEngineFinal::EngineInterface::EngineInterface(EditingEngineFinal *engine)
        : SmoothingEngine::EngineInterface(engine), parent(engine)
{ }

EditingEngineFinal::EngineInterface::~EngineInterface() = default;

SmootherChainTarget *EditingEngineFinal::EngineInterface::addNewOutput(const SequenceName &unit)
{
    SequenceName output(unit);
    output.setArchive(parent->averageArchive);
    return SmoothingEngine::EngineInterface::addNewOutput(output);
}

SmootherChainTargetFlags *EditingEngineFinal::EngineInterface::addNewFlagsOutput(const SequenceName &unit)
{
    SequenceName output(unit);
    output.setArchive(parent->averageArchive);
    return SmoothingEngine::EngineInterface::addNewFlagsOutput(output);
}

SmootherChainTargetGeneral *EditingEngineFinal::EngineInterface::addNewGeneralOutput(const SequenceName &unit)
{
    SequenceName output(unit);
    output.setArchive(parent->averageArchive);
    return SmoothingEngine::EngineInterface::addNewGeneralOutput(output);
}

std::unique_ptr<SmoothingEngine::EngineInterface> EditingEngineFinal::createEngineInterface()
{ return std::unique_ptr<SmoothingEngine::EngineInterface>(new EngineInterface(this)); }


}
}
