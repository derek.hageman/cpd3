/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGDIGITALFILTERCHAIN_H
#define CPD3SMOOTHINGDIGITALFILTERCHAIN_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDataStream>

#include "smoothing/smoothing.hxx"
#include "smoothing/smoothingchain.hxx"
#include "smoothing/digitalfilter.hxx"

namespace CPD3 {
namespace Smoothing {

/**
 * A single pole low pass digital filter implementation.
 */
class CPD3SMOOTHING_EXPORT DigitalFilterChainSinglePoleLowPass : public SmootherChainNode {
    SmootherChainTarget *output;
    bool targetFinished;
    DigitalFilterSinglePoleLowPass filter;

    class TargetInput : public SmootherChainTarget {
        DigitalFilterChainSinglePoleLowPass *node;
    public:
        TargetInput(DigitalFilterChainSinglePoleLowPass *n);

        virtual ~TargetInput();

        virtual void incomingData(double start, double end, double value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class TargetInput;

    TargetInput targetInput;

public:
    /**
     * Create a new single pole low pass filter.  This takes ownership of the
     * time constant specification and gap.
     * 
     * @param tc                time time constant specification
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     * @param outputTarget      the output target for smoothed values
     */
    DigitalFilterChainSinglePoleLowPass(Data::DynamicTimeInterval *tc,
                                        Data::DynamicTimeInterval *setGap,
                                        bool resetUndefined,
                                        SmootherChainTarget *outputTarget);

    /**
     * Create a new single pole low pass filter from saved state.
     * 
     * @param stream            the stream to read state from
     * @param outputTarget      the output target for smoothed values
     */
    DigitalFilterChainSinglePoleLowPass(QDataStream &stream, SmootherChainTarget *outputTarget);

    virtual ~DigitalFilterChainSinglePoleLowPass();

    /**
     * Get the chain target to send values to.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTarget()
    { return &targetInput; }

    virtual bool finished();

    virtual void serialize(QDataStream &stream) const;
};


/**
 * A generalized digital filter.  This processes data based on a set of
 * "a" and "b" coefficients.
 */
class CPD3SMOOTHING_EXPORT DigitalFilterChainGeneral : public SmootherChainNode {
    SmootherChainTarget *output;
    bool targetFinished;
    DigitalFilterGeneral filter;

    class TargetInput : public SmootherChainTarget {
        DigitalFilterChainGeneral *node;
    public:
        TargetInput(DigitalFilterChainGeneral *n);

        virtual ~TargetInput();

        virtual void incomingData(double start, double end, double value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class TargetInput;

    TargetInput targetInput;
public:
    /**
     * Create a new general low pass filter.  This takes ownership of the gap
     * specification.
     * 
     * @param setA              the "a" coefficients
     * @param setB              the "b" coefficients
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     * @param outputTarget      the output target for smoothed values
     */
    DigitalFilterChainGeneral(const QVector<double> &setA,
                              const QVector<double> &setB, Data::DynamicTimeInterval *setGap,
                              bool resetUndefined,
                              SmootherChainTarget *outputTarget);

    /**
     * Create a new single pole low pass filter from saved state.
     * 
     * @param stream            the stream to read state from
     * @param outputTarget      the output target for smoothed values
     */
    DigitalFilterChainGeneral(QDataStream &stream, SmootherChainTarget *outputTarget);

    virtual ~DigitalFilterChainGeneral();

    /**
     * Get the chain target to send values to.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTarget()
    { return &targetInput; }

    virtual bool finished();

    virtual void serialize(QDataStream &stream) const;
};

/**
 * A four pole low pass digital filter.
 */
class CPD3SMOOTHING_EXPORT DigitalFilterChainFourPoleLowPass : public SmootherChainNode {
    SmootherChainTarget *output;
    bool targetFinished;
    DigitalFilterFourPoleLowPass filter;

    class TargetInput : public SmootherChainTarget {
        DigitalFilterChainFourPoleLowPass *node;
    public:
        TargetInput(DigitalFilterChainFourPoleLowPass *n);

        virtual ~TargetInput();

        virtual void incomingData(double start, double end, double value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class TargetInput;

    TargetInput targetInput;
public:
    /**
     * Create a new single pole low pass filter.  This takes ownership of the
     * time constant specification and gap.
     * 
     * @param tc                time time constant specification
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     * @param outputTarget      the output target for smoothed values
     */
    DigitalFilterChainFourPoleLowPass(Data::DynamicTimeInterval *tc,
                                      Data::DynamicTimeInterval *setGap,
                                      bool resetUndefined,
                                      SmootherChainTarget *outputTarget);

    /**
     * Create a new four pole low pass filter from saved state.
     * 
     * @param stream            the stream to read state from
     * @param outputTarget      the output target for smoothed values
     */
    DigitalFilterChainFourPoleLowPass(QDataStream &stream, SmootherChainTarget *outputTarget);

    virtual ~DigitalFilterChainFourPoleLowPass();

    /**
     * Get the chain target to send values to.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTarget()
    { return &targetInput; }

    virtual bool finished();

    virtual void serialize(QDataStream &stream) const;
};


/**
 * A wrapper around any digital filter.
 */
class CPD3SMOOTHING_EXPORT DigitalFilterChainWrapper : public SmootherChainNode {
    SmootherChainTarget *output;
    bool targetFinished;
    DigitalFilter *filter;

    class TargetInput : public SmootherChainTarget {
        DigitalFilterChainWrapper *node;
    public:
        TargetInput(DigitalFilterChainWrapper *n);

        virtual ~TargetInput();

        virtual void incomingData(double start, double end, double value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class TargetInput;

    TargetInput targetInput;
public:
    /**
     * Create a new wrapper.  This takes ownership of the filter.
     * 
     * @param wrapFilter        the filter to wrap
     * @param outputTarget      the output target for smoothed values
     */
    DigitalFilterChainWrapper(DigitalFilter *wrapFilter, SmootherChainTarget *outputTarget);

    /**
     * Create a new wrapper from the saved state
     * 
     * @param stream            the stream to read state from
     * @param outputTarget      the output target for smoothed values
     */
    DigitalFilterChainWrapper(QDataStream &stream, SmootherChainTarget *outputTarget);

    virtual ~DigitalFilterChainWrapper();

    /**
     * Get the chain target to send values to.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTarget()
    { return &targetInput; }

    virtual bool finished();

    virtual void serialize(QDataStream &stream) const;
};

/**
 * A smoother chain for a single pole low pass digital filter.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreSinglePoleLowPass : public SmootherChainCoreSimple {
    Data::DynamicTimeInterval *timeConstant;
    Data::DynamicTimeInterval *gap;
    bool resetOnUndefined;
public:
    /**
     * Create a new core that uses a single pole low pass digital filter.
     * This takes ownership of the gap and time constant.
     * 
     * @param tc                time time constant specification
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     */
    SmootherChainCoreSinglePoleLowPass(Data::DynamicTimeInterval *tc,
                                       Data::DynamicTimeInterval *setGap = NULL,
                                       bool resetUndefined = true);

    virtual ~SmootherChainCoreSinglePoleLowPass();

    virtual void createSmoother(SmootherChainCoreEngineInterface *engine,
                                SmootherChainTarget *&input,
                                SmootherChainTarget *output);

    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainTarget *&input,
                                     SmootherChainTarget *output);

    virtual Data::Variant::Root getProcessingMetadata(double time) const;

    virtual QSet<double> getProcessingMetadataBreaks() const;
};

/**
 * A smoother chain for a four pole low pass digital filter.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreFourPoleLowPass : public SmootherChainCoreSimple {
    Data::DynamicTimeInterval *timeConstant;
    Data::DynamicTimeInterval *gap;
    bool resetOnUndefined;
public:
    /**
     * Create a new core that uses a four pole low pass digital filter.
     * This takes ownership of the gap and time constant.
     * 
     * @param tc                time time constant specification
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     */
    SmootherChainCoreFourPoleLowPass(Data::DynamicTimeInterval *tc,
                                     Data::DynamicTimeInterval *setGap = NULL,
                                     bool resetUndefined = true);

    virtual ~SmootherChainCoreFourPoleLowPass();

    virtual void createSmoother(SmootherChainCoreEngineInterface *engine,
                                SmootherChainTarget *&input,
                                SmootherChainTarget *output);

    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainTarget *&input,
                                     SmootherChainTarget *output);

    virtual Data::Variant::Root getProcessingMetadata(double time) const;

    virtual QSet<double> getProcessingMetadataBreaks() const;
};

/**
 * A smoother chain for a general digital filter.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreGeneralDigitalFilter : public SmootherChainCoreSimple {
    QVector<double> a;
    QVector<double> b;
    Data::DynamicTimeInterval *gap;
    bool resetOnUndefined;
public:
    /**
     * Create a new core that uses a general digital filter.  This takes
     * ownership of the gap specification, if any.
     * 
     * @param setA              the "a" coefficients
     * @param setB              the "b" coefficients
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     */
    SmootherChainCoreGeneralDigitalFilter(const QVector<double> &setA,
                                          const QVector<double> &setB,
                                          Data::DynamicTimeInterval *setGap = NULL,
                                          bool resetUndefined = true);

    virtual ~SmootherChainCoreGeneralDigitalFilter();

    virtual void createSmoother(SmootherChainCoreEngineInterface *engine,
                                SmootherChainTarget *&input,
                                SmootherChainTarget *output);

    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainTarget *&input,
                                     SmootherChainTarget *output);

    virtual Data::Variant::Root getProcessingMetadata(double time) const;

    virtual QSet<double> getProcessingMetadataBreaks() const;
};

/**
 * A smoother chain for wrapper around any digital filter.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreDigitalFilterWrapper : public SmootherChainCoreSimple {
    DigitalFilter *wrap;
public:
    /**
     * Create a new core that uses a general purpose digital filter interface.
     * This takes ownership oof the filter.
     * 
     * @param wrapFilter        the filter to wrap
     */
    SmootherChainCoreDigitalFilterWrapper(DigitalFilter *wrapFilter);

    virtual ~SmootherChainCoreDigitalFilterWrapper();

    virtual void createSmoother(SmootherChainCoreEngineInterface *engine,
                                SmootherChainTarget *&input,
                                SmootherChainTarget *output);

    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainTarget *&input,
                                     SmootherChainTarget *output);

    virtual Data::Variant::Root getProcessingMetadata(double time) const;

    virtual QSet<double> getProcessingMetadataBreaks() const;
};

}
}

#endif
