/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>
#include <QList>
#include <QVarLengthArray>

#include "smoothing/realtimechain.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "core/timeutils.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "algorithms/dewpoint.hxx"

using namespace CPD3;
using namespace CPD3::Smoothing;
using namespace CPD3::Data;
using namespace CPD3::Algorithms;

class TestRealtimeBinner : public RealtimeBinner<double> {
public:
    QList<double> time;
    QList<double> data;

    QList<double> allTimes;
    QList<double> allValues;

    QList<double> purged;
    QList<QList<double> > emitted;
    QList<double> emittedTimes;

    double lastTime;

    TestRealtimeBinner(Time::LogicalTimeUnit setUnit = Time::Minute,
                       int setCount = 1,
                       bool setAlign = true) : RealtimeBinner<double>(setUnit, setCount, setAlign),
                                               lastTime(FP::undefined())
    { }

    virtual ~TestRealtimeBinner()
    { }

    void add(double time, double data)
    { addIncomingValue(time, data); }

    void advance(double time)
    { addIncomingAdvance(time); }

protected:
    virtual void addToBin(double time, const double &data)
    {
        allTimes.append(time);
        allValues.append(data);

        this->time.append(time);
        this->data.append(data);

        Q_ASSERT(!FP::defined(lastTime) || time >= lastTime);
        lastTime = time;
    }

    virtual bool emitTotal(double time)
    {
        emitted.append(data);
        emittedTimes.append(time);

        Q_ASSERT(!FP::defined(lastTime) || time >= lastTime);
        lastTime = time;
        return true;
    }

    virtual void purgeBefore(double time)
    {
        while (!this->time.isEmpty() && this->time.at(0) <= time) {
            this->time.removeAt(0);
            this->data.removeAt(0);
        }
        purged.append(time);
    }
};

class TestEventBinner {
public:
    enum Type {
        Advance, Value, Check, SetInterval,
    };
    Type type;
    double time;
    double value;
    QList<double> purged;
    QList<double> emitted;
    QList<double> emittedTimes;
    int interval;
    bool align;

    TestEventBinner(Type t, double ti = FP::undefined(), double v = FP::undefined()) : type(t),
                                                                                       time(ti),
                                                                                       value(v),
                                                                                       purged(),
                                                                                       emitted(),
                                                                                       emittedTimes(),
                                                                                       interval(-1),
                                                                                       align(false)
    { }

    TestEventBinner(const QList<double> &pd,
                    const QList<double> &et = QList<double>(),
                    const QList<double> &ce = QList<double>()) : type(Check),
                                                                 time(FP::undefined()),
                                                                 value(FP::undefined()),
                                                                 purged(pd),
                                                                 emitted(ce),
                                                                 emittedTimes(et),
                                                                 interval(-1),
                                                                 align(false)
    { }

    TestEventBinner() : type(Check),
                        time(FP::undefined()),
                        value(FP::undefined()),
                        purged(),
                        emitted(),
                        emittedTimes(),
                        interval(-1),
                        align(false)
    { }

    TestEventBinner(bool a, int i) : type(SetInterval),
                                     time(FP::undefined()),
                                     value(FP::undefined()),
                                     purged(),
                                     emitted(),
                                     emittedTimes(),
                                     interval(i),
                                     align(a)
    { }
};

Q_DECLARE_METATYPE(TestEventBinner);

Q_DECLARE_METATYPE(QList<TestEventBinner>);

struct OverlayData {
    double start;
    double end;
    int index;
    double value;

    OverlayData(double s, double e, int i, double v) : start(s), end(e), index(i), value(v)
    { }

    double getStart() const
    { return start; }

    double getEnd() const
    { return end; }
};

template<int N>
struct SegmentData {
    double start;
    double end;
    double values[N];

    SegmentData() : start(FP::undefined()), end(FP::undefined())
    {
        for (int i = 0; i < N; i++) {
            values[i] = FP::undefined();
        }
    }

    SegmentData(double s, double e, const double v[N]) : start(s), end(e)
    {
        for (int i = 0; i < N; i++) {
            values[i] = v[i];
        }
    }

    SegmentData(const SegmentData<N> &other) : start(other.start), end(other.end)
    {
        for (int i = 0; i < N; i++) {
            values[i] = other.values[i];
        }
    }

    SegmentData<N> &operator=(const SegmentData<N> &other)
    {
        if (&other == this) return *this;
        start = other.start;
        end = other.end;
        for (int i = 0; i < N; i++) {
            values[i] = other.values[i];
        }
        return *this;
    }

    bool operator==(const SegmentData<N> &rhs) const
    {
        for (int i = 0; i < N; i++) {
            if (!FP::equal(values[i], rhs.values[i]))
                return false;
        }
        return FP::equal(start, rhs.start);
    }

    bool operator!=(const SegmentData<N> &rhs) const
    {
        for (int i = 0; i < N; i++) {
            if (!FP::equal(values[i], rhs.values[i]))
                return true;
        }
        return !FP::equal(start, rhs.start);
    }

    SegmentData(const SegmentData<N> &other, double s, double e) : start(s), end(e)
    {
        for (int i = 0; i < N; i++) {
            values[i] = other.values[i];
        }
    }

    SegmentData(const OverlayData &other, double s, double e) : start(s), end(e)
    {
        Q_ASSERT(other.index >= 0 && other.index < N);
        for (int i = 0; i < N; i++) {
            values[i] = FP::undefined();
        }
        values[other.index] = other.value;
    }

    SegmentData(const SegmentData<N> &under, const OverlayData &over, double s, double e) : start(
            s), end(e)
    {
        Q_ASSERT(over.index >= 0 && over.index < N);
        for (int i = 0; i < N; i++) {
            values[i] = under.values[i];
        }
        values[over.index] = over.value;
    }

    double getStart() const
    { return start; }

    double getEnd() const
    { return end; }

    void setStart(double v)
    { start = v; }

    void setEnd(double v)
    { end = v; }
};

template<int N>
static char *testToString(const QList<SegmentData<N> > &output)
{
    QByteArray ba;
    for (int i = 0, max = output.size(); i < max; i++) {
        if (i != 0) ba += ", ";

        ba += "(";
        if (FP::defined(output.at(i).start))
            ba += QByteArray::number(output.at(i).start);
        else
            ba += "Infinity";
        ba += "={";
        for (int j = 0; j < N; j++) {
            if (j != 0) ba += ",";
            if (FP::defined(output.at(i).values[j]))
                ba += QByteArray::number(output.at(i).values[j]);
            else
                ba += "Undefined";
        }
        ba += "})";
    }
    return qstrdup(ba.data());
}

namespace QTest {
template<>
char *toString(const QList<SegmentData<2> > &output)
{ return testToString(output); }

template<>
char *toString(const QList<double> &output)
{
    QByteArray ba;
    for (int i = 0, max = output.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        if (FP::defined(output.at(i)))
            ba += QByteArray::number(output.at(i));
        else
            ba += "Undefined";
    }
    return qstrdup(ba.data());
}
}

template<int N>
struct TestEventSegementer {
    enum Type {
        Advance, Value, End, Check,
    };
    Type type;
    int index;
    double time;
    double value;
    QList<SegmentData<N> > checkData;
    QList<double> checkAdvance;

    TestEventSegementer(Type t, int i, double tm = FP::undefined(), double v = FP::undefined())
            : type(t), index(i), time(tm), value(v), checkData(), checkAdvance()
    { }

    TestEventSegementer(const QList<SegmentData<N> > &cd, const QList<double> &ca = QList<double>())
            : type(Check),
              index(-1),
              time(FP::undefined()),
              value(FP::undefined()),
              checkData(cd),
              checkAdvance(ca)
    { }

    TestEventSegementer() : type(Check),
                            index(-1),
                            time(FP::undefined()),
                            value(FP::undefined()),
                            checkData(),
                            checkAdvance()
    { }
};

typedef TestEventSegementer<2> TestEventSegementer2;

Q_DECLARE_METATYPE(TestEventSegementer2);

Q_DECLARE_METATYPE(QList<TestEventSegementer2>);


template<int N>
class TestSegmentProcessor : public RealtimeSegmentProcessor<N> {
public:
    QList<SegmentData<N> > segments;
    QList<double> advances;
    double lastTime;

    TestSegmentProcessor() : segments(), advances(), lastTime(FP::undefined())
    { }

protected:
    virtual void segmentDone(double start, double end, const double values[N])
    {
        Q_ASSERT(Range::compareStart(start, lastTime) >= 0);
        lastTime = end;
        segments.append(SegmentData<N>(start, end, values));
    }

    virtual void allStreamsAdvanced(double time)
    {
        Q_ASSERT(Range::compareStart(time, lastTime) >= 0);
        lastTime = time;
        advances.append(time);
    }
};


class TestSimpleTarget : public SmootherChainTarget {
public:
    QVector<double> values;
    QVector<double> starts;
    QVector<double> ends;

    virtual void incomingData(double start, double end, double value)
    {
        values.append(value);
        starts.append(start);
        ends.append(end);
    }

    virtual void incomingAdvance(double time)
    { Q_UNUSED(time); }

    virtual void endData()
    { }
};

class TestSimpleTargetFlags : public SmootherChainTargetFlags {
public:
    QVector<Variant::Flags> values;
    QVector<double> starts;
    QVector<double> ends;

    virtual void incomingData(double start, double end, const Variant::Flags &value)
    {
        values.append(value);
        starts.append(start);
        ends.append(end);
    }

    virtual void incomingAdvance(double time)
    { Q_UNUSED(time); }

    virtual void endData()
    { }
};

class TestSimpleTargetGeneral : public SmootherChainTargetGeneral {
public:
    QVector<Variant::Read> values;
    QVector<double> starts;
    QVector<double> ends;

    virtual void incomingData(double start, double end, const Variant::Root &value)
    {
        values.append(value.read());
        starts.append(start);
        ends.append(end);
    }

    virtual void incomingAdvance(double time)
    { Q_UNUSED(time); }

    virtual void endData()
    { }
};


class TestChainEngine : public virtual SmootherChainCoreEngineInterface {
public:
    SinkMultiplexer mux;

    QList<SmootherChainNode *> chain;

    virtual void addChainNode(SmootherChainNode *add)
    { chain.append(add); }

    class AuxiliaryInterface : public RealtimeEngineInterface {
        TestChainEngine *parent;
    public:
        AuxiliaryInterface(TestChainEngine *engine) : parent(engine)
        { }

        virtual ~AuxiliaryInterface()
        { }

        virtual void addRealtimeController(RealtimeController *controller)
        {
            Q_UNUSED(controller);
            parent->nControllers++;
        }

        virtual Time::LogicalTimeUnit getAveragingUnit()
        { return Time::Second; }

        virtual int getAveragingCount()
        { return 100; }

        virtual bool getAveragingAlign()
        { return false; }
    };

    AuxiliaryInterface aux;

    virtual SmootherChainCoreEngineAuxiliaryInterface *getAuxiliaryInterface()
    { return &aux; }

    class TestChainOutput : public SmootherChainTarget {
    public:
        SinkMultiplexer::Sink *ingress;
        SequenceName unit;
        bool ended;

        TestChainOutput(SinkMultiplexer::Sink *i, const SequenceName &u) : ingress(i),
                                                                           unit(u),
                                                                           ended(false)
        { }

        virtual ~TestChainOutput()
        { }

        virtual void incomingData(double start, double end, double value)
        {
            Q_ASSERT(!ended);
            ingress->incomingData(SequenceValue(unit, Variant::Root(value), start, end));
        }

        virtual void incomingAdvance(double time)
        {
            Q_ASSERT(!ended);
            ingress->incomingAdvance(time);
        }

        virtual void endData()
        {
            Q_ASSERT(!ended);
            ingress->endData();
            ended = true;
        }
    };

    QList<TestChainOutput *> outputs;

    virtual SmootherChainTarget *addNewOutput(const SequenceName &unit)
    {
        for (QList<TestChainOutput *>::const_iterator check = outputs.constBegin(),
                end = outputs.constEnd(); check != end; ++check) {
            if ((*check)->unit == unit) {
                qDebug() << "Duplicate output added:" << unit;
                return *check;
            }
        }
        TestChainOutput *add = new TestChainOutput(mux.createSink(), unit);
        outputs.append(add);
        return add;
    }

    virtual SmootherChainTarget *getOutput(int index)
    {
        Q_ASSERT(index >= 0 && index < outputs.size());
        return outputs.at(index);
    }

    class TestChainOutputFlags : public SmootherChainTargetFlags {
    public:
        SinkMultiplexer::Sink *ingress;
        SequenceName unit;
        bool ended;

        TestChainOutputFlags(SinkMultiplexer::Sink *i, const SequenceName &u) : ingress(i),
                                                                                unit(u),
                                                                                ended(false)
        { }

        virtual ~TestChainOutputFlags()
        { }

        virtual void incomingData(double start, double end, const Variant::Flags &value)
        {
            Q_ASSERT(!ended);
            ingress->incomingData(SequenceValue(unit, Variant::Root(value), start, end));
        }

        virtual void incomingAdvance(double time)
        {
            Q_ASSERT(!ended);
            ingress->incomingAdvance(time);
        }

        virtual void endData()
        {
            Q_ASSERT(!ended);
            ingress->endData();
            ended = true;
        }
    };

    QList<TestChainOutputFlags *> outputsFlags;

    virtual SmootherChainTargetFlags *addNewFlagsOutput(const SequenceName &unit)
    {
        for (QList<TestChainOutputFlags *>::const_iterator check = outputsFlags.constBegin(),
                end = outputsFlags.constEnd(); check != end; ++check) {
            if ((*check)->unit == unit) {
                qDebug() << "Duplicate flags output added:" << unit;
                return *check;
            }
        }
        TestChainOutputFlags *add = new TestChainOutputFlags(mux.createSink(), unit);
        outputsFlags.append(add);
        return add;
    }

    virtual SmootherChainTargetFlags *getFlagsOutput(int index)
    {
        Q_ASSERT(index >= 0 && index <= outputsFlags.size());
        return outputsFlags.at(index);
    }

    class TestChainOutputGeneral : public SmootherChainTargetGeneral {
    public:
        SinkMultiplexer::Sink *ingress;
        SequenceName unit;
        bool ended;

        TestChainOutputGeneral(SinkMultiplexer::Sink *i, const SequenceName &u) : ingress(i),
                                                                                  unit(u),
                                                                                  ended(false)
        { }

        virtual ~TestChainOutputGeneral()
        { }

        virtual void incomingData(double start, double end, const Variant::Root &value)
        {
            Q_ASSERT(!ended);
            ingress->incomingData(SequenceValue(unit, value, start, end));
        }

        virtual void incomingAdvance(double time)
        {
            Q_ASSERT(!ended);
            ingress->incomingAdvance(time);
        }

        virtual void endData()
        {
            Q_ASSERT(!ended);
            ingress->endData();
            ended = true;
        }
    };

    QList<TestChainOutputGeneral *> outputsGeneral;

    virtual SmootherChainTargetGeneral *addNewGeneralOutput(const SequenceName &unit)
    {
        for (QList<TestChainOutputGeneral *>::const_iterator check = outputsGeneral.constBegin(),
                end = outputsGeneral.constEnd(); check != end; ++check) {
            if ((*check)->unit == unit) {
                qDebug() << "Duplicate general output added:" << unit;
                return *check;
            }
        }
        TestChainOutputGeneral *add = new TestChainOutputGeneral(mux.createSink(), unit);
        outputsGeneral.append(add);
        return add;
    }

    virtual SmootherChainTargetGeneral *getGeneralOutput(int index)
    {
        Q_ASSERT(index >= 0 && index <= outputsGeneral.size());
        return outputsGeneral.at(index);
    }

    struct TestChainInput {
        SequenceName unit;
        QList<SmootherChainTarget *> targets;

        void incomingData(double start, double end, double value)
        {
            for (QList<SmootherChainTarget *>::const_iterator t = targets.constBegin(),
                    endT = targets.constEnd(); t != endT; ++t) {
                (*t)->incomingData(start, end, value);
            }
        }

        void incomingAdvance(double time)
        {
            for (QList<SmootherChainTarget *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->incomingAdvance(time);
            }
        }

        void endData()
        {
            for (QList<SmootherChainTarget *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->endData();
            }
        }
    };

    QList<TestChainInput> inputs;

    virtual void addNewInput(const SequenceName &unit, SmootherChainTarget *target)
    {
        for (QList<TestChainInput>::iterator it = inputs.begin(); it != inputs.end(); ++it) {
            if (it->unit == unit) {
                if (target != NULL)
                    it->targets.append(target);
                return;
            }
        }
        inputs.append(TestChainInput());
        inputs.last().unit = unit;
        if (target != NULL)
            inputs.last().targets.append(target);
    }

    virtual void addInputTarget(int index, SmootherChainTarget *target)
    {
        Q_ASSERT(index >= 0 && index < inputs.size());
        Q_ASSERT(target != NULL);
        inputs[index].targets.append(target);
    }

    void advanceAllInputsAfter(int index, double time)
    {
        for (int i = index; i < inputs.size(); i++) {
            inputs[i].incomingAdvance(time);
        }
    }

    struct TestChainInputFlags {
        SequenceName unit;
        QList<SmootherChainTargetFlags *> targets;

        void incomingData(double start, double end, const Variant::Flags &value)
        {
            for (QList<SmootherChainTargetFlags *>::const_iterator t = targets.constBegin(),
                    endT = targets.constEnd(); t != endT; ++t) {
                (*t)->incomingData(start, end, value);
            }
        }

        void incomingAdvance(double time)
        {
            for (QList<SmootherChainTargetFlags *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->incomingAdvance(time);
            }
        }

        void endData()
        {
            for (QList<SmootherChainTargetFlags *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->endData();
            }
        }
    };

    QList<TestChainInputFlags> inputsFlags;

    virtual void addNewFlagsInput(const SequenceName &unit, SmootherChainTargetFlags *target)
    {
        for (QList<TestChainInputFlags>::iterator it = inputsFlags.begin();
                it != inputsFlags.end();
                ++it) {
            if (it->unit == unit) {
                if (target != NULL)
                    it->targets.append(target);
                return;
            }
        }
        inputsFlags.append(TestChainInputFlags());
        inputsFlags.last().unit = unit;
        if (target != NULL)
            inputsFlags.last().targets.append(target);
    }

    virtual void addFlagsInputTarget(int index, SmootherChainTargetFlags *target)
    {
        Q_ASSERT(index >= 0 && index < inputsFlags.size());
        Q_ASSERT(target != NULL);
        inputsFlags[index].targets.append(target);
    }

    void advanceAllInputsFlagsAfter(int index, double time)
    {
        for (int i = index; i < inputsFlags.size(); i++) {
            inputsFlags[i].incomingAdvance(time);
        }
    }

    struct TestChainInputGeneral {
        SequenceName unit;
        QList<SmootherChainTargetGeneral *> targets;

        void incomingData(double start, double end, const Variant::Root &value)
        {
            for (QList<SmootherChainTargetGeneral *>::const_iterator t = targets.constBegin(),
                    endT = targets.constEnd(); t != endT; ++t) {
                (*t)->incomingData(start, end, value);
            }
        }

        void incomingAdvance(double time)
        {
            for (QList<SmootherChainTargetGeneral *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->incomingAdvance(time);
            }
        }

        void endData()
        {
            for (QList<SmootherChainTargetGeneral *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->endData();
            }
        }
    };

    QList<TestChainInputGeneral> inputsGeneral;

    virtual void addNewGeneralInput(const SequenceName &unit, SmootherChainTargetGeneral *target)
    {
        for (QList<TestChainInputGeneral>::iterator it = inputsGeneral.begin();
                it != inputsGeneral.end();
                ++it) {
            if (it->unit == unit) {
                if (target != NULL)
                    it->targets.append(target);
                return;
            }
        }
        inputsGeneral.append(TestChainInputGeneral());
        inputsGeneral.last().unit = unit;
        if (target != NULL)
            inputsGeneral.last().targets.append(target);
    }

    virtual void addGeneralInputTarget(int index, SmootherChainTargetGeneral *target)
    {
        Q_ASSERT(index >= 0 && index < inputsGeneral.size());
        Q_ASSERT(target != NULL);
        inputsGeneral[index].targets.append(target);
    }

    void advanceAllInputsGeneralAfter(int index, double time)
    {
        for (int i = index; i < inputsGeneral.size(); i++) {
            inputsGeneral[i].incomingAdvance(time);
        }
    }

    void endAll()
    {
        mux.sinkCreationComplete();

        for (QList<TestChainInput>::iterator it = inputs.begin(); it != inputs.end(); ++it) {
            it->endData();
        }
        for (QList<TestChainInputFlags>::iterator it = inputsFlags.begin();
                it != inputsFlags.end();
                ++it) {
            it->endData();
        }
    }


    int nControllers;

    TestChainEngine() : mux(), chain(), aux(this)
    {
        nControllers = 0;
        mux.start();
    }

    virtual ~TestChainEngine()
    {
        qDeleteAll(chain);
        qDeleteAll(outputs);
        qDeleteAll(outputsFlags);
        qDeleteAll(outputsGeneral);
    }


    virtual SequenceName getBaseUnit(int index = 0)
    {
        return SequenceName("bnd", "raw", "var" + std::to_string(index + 1));
    }

    virtual SinkMultiplexer::Sink *addNewFinalOutput()
    { return mux.createSink(); }

    virtual Variant::Read getOptions()
    { return Variant::Read::empty(); }

    void serialize(QDataStream &stream)
    {
        mux.setEgress(NULL);
        for (int i = chain.size() - 1; i >= 0; i--) {
            chain[i]->pause();
        }

        stream << (quint32) outputs.size();
        for (int i = 0; i < outputs.size(); i++) {
            stream << outputs.at(i)->unit << outputs.at(i)->ended;
        }
        stream << (quint32) outputsFlags.size();
        for (int i = 0; i < outputsFlags.size(); i++) {
            stream << outputsFlags.at(i)->unit << outputsFlags.at(i)->ended;
        }
        stream << (quint32) outputsGeneral.size();
        for (int i = 0; i < outputsGeneral.size(); i++) {
            stream << outputsGeneral.at(i)->unit << outputsGeneral.at(i)->ended;
        }

        stream << (quint32) inputs.size();
        for (int i = 0; i < inputs.size(); i++) {
            stream << inputs.at(i).unit;
        }
        stream << (quint32) inputsFlags.size();
        for (int i = 0; i < inputsFlags.size(); i++) {
            stream << inputsFlags.at(i).unit;
        }
        stream << (quint32) inputsGeneral.size();
        for (int i = 0; i < inputsGeneral.size(); i++) {
            stream << inputsGeneral.at(i).unit;
        }

        stream << mux;

        for (int i = 0; i < chain.size(); i++) {
            chain[i]->serialize(stream);
        }
        for (int i = 0; i < chain.size(); i++) {
            chain[i]->resume();
        }
    }

    void deserialize(QDataStream &stream)
    {
        quint32 n;
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            SequenceName u;
            stream >> u;
            bool ended;
            stream >> ended;
            SmootherChainTarget *o = addNewOutput(u);
            if (ended)
                o->endData();
        }
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            SequenceName u;
            stream >> u;
            bool ended;
            stream >> ended;
            SmootherChainTargetFlags *o = addNewFlagsOutput(u);
            if (ended)
                o->endData();
        }
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            SequenceName u;
            stream >> u;
            bool ended;
            stream >> ended;
            SmootherChainTargetGeneral *o = addNewGeneralOutput(u);
            if (ended)
                o->endData();
        }

        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            TestChainInput add;
            stream >> add.unit;
            inputs.append(add);
        }
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            TestChainInputFlags add;
            stream >> add.unit;
            inputsFlags.append(add);
        }
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            TestChainInputGeneral add;
            stream >> add.unit;
            inputsGeneral.append(add);
        }

        stream >> mux;
    }

};


namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestRealtimeChain : public QObject {
Q_OBJECT

    static double vector2DMag(const QVector<double> &d, const QVector<double> &m)
    {
        double x = 0.0;
        double y = 0.0;
        Q_ASSERT(d.size() == m.size() && d.size() > 0);
        for (int i = 0; i < d.size(); i++) {
            double inD = (d[i] - 180.0) * 0.0174532925199433;
            x += cos(inD) * m[i];
            y += sin(inD) * m[i];
        }
        x /= (double) d.size();
        y /= (double) d.size();
        return sqrt(x * x + y * y);
    }

    static double vector2DDir(const QVector<double> &d, const QVector<double> &m)
    {
        double x = 0.0;
        double y = 0.0;
        Q_ASSERT(d.size() == m.size() && d.size() > 0);
        for (int i = 0; i < d.size(); i++) {
            double inD = (d[i] - 180.0) * 0.0174532925199433;
            x += cos(inD) * m[i];
            y += sin(inD) * m[i];
        }
        x /= (double) d.size();
        y /= (double) d.size();
        return atan2(y, x) * 57.2957795130823 + 180.0;
    }

    static double vector3DMag(const QVector<double> &az,
                              const QVector<double> &el,
                              const QVector<double> &m)
    {
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        Q_ASSERT(az.size() == m.size() && el.size() == m.size() && m.size() > 0);
        for (int i = 0; i < m.size(); i++) {
            double inA = (az[i] - 180.0) * 0.0174532925199433;
            double inE = el[i] * 0.0174532925199433;
            x += cos(inE) * cos(inA) * m[i];
            y += cos(inE) * sin(inA) * m[i];
            z += sin(inE) * m[i];
        }
        x /= (double) m.size();
        y /= (double) m.size();
        z /= (double) m.size();
        return sqrt(x * x + y * y + z * z);
    }

    static double vector3DAzi(const QVector<double> &az,
                              const QVector<double> &el,
                              const QVector<double> &m)
    {
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        Q_ASSERT(az.size() == m.size() && el.size() == m.size() && m.size() > 0);
        for (int i = 0; i < m.size(); i++) {
            double inA = (az[i] - 180.0) * 0.0174532925199433;
            double inE = el[i] * 0.0174532925199433;
            x += cos(inE) * cos(inA) * m[i];
            y += cos(inE) * sin(inA) * m[i];
            z += sin(inE) * m[i];
        }
        x /= (double) m.size();
        y /= (double) m.size();
        z /= (double) m.size();
        return atan2(y, x) * 57.2957795130823 + 180.0;
    }

    static double vector3DEle(const QVector<double> &az,
                              const QVector<double> &el,
                              const QVector<double> &m)
    {
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        Q_ASSERT(az.size() == m.size() && el.size() == m.size() && m.size() > 0);
        for (int i = 0; i < m.size(); i++) {
            double inA = (az[i] - 180.0) * 0.0174532925199433;
            double inE = el[i] * 0.0174532925199433;
            x += cos(inE) * cos(inA) * m[i];
            y += cos(inE) * sin(inA) * m[i];
            z += sin(inE) * m[i];
        }
        x /= (double) m.size();
        y /= (double) m.size();
        z /= (double) m.size();
        return asin(z / sqrt(x * x + y * y + z * z)) * 57.2957795130823;
    }

    static double beersLaw(double startL, double endL, double startI, double endI)
    {
        return (1E6 * log(startI / endI)) / (endL - startL);
    }

private slots:

    void binner()
    {
        QFETCH(int, intervalMinutes);
        QFETCH(bool, align);
        QFETCH(QList<TestEventBinner>, events);

        TestRealtimeBinner *binner = new TestRealtimeBinner(Time::Minute, intervalMinutes, align);

        QList<double> checkAllTimes;
        QList<double> checkAllValues;
        for (QList<TestEventBinner>::const_iterator e = events.constBegin(),
                end = events.constEnd(); e != end; ++e) {
            switch (e->type) {
            case TestEventBinner::Value:
                binner->add(e->time, e->value);
                checkAllTimes.append(e->time);
                checkAllValues.append(e->value);
                break;
            case TestEventBinner::Advance:
                binner->advance(e->time);
                break;
            case TestEventBinner::Check:
                QCOMPARE(binner->purged, e->purged);
                QCOMPARE(binner->emittedTimes, e->emittedTimes);
                if (!e->emitted.isEmpty()) {
                    QVERIFY(!binner->emitted.isEmpty());
                    QCOMPARE(binner->emitted.last(), e->emitted);
                }
                break;
            case TestEventBinner::SetInterval:
                binner->setBinning(Time::Minute, e->interval, e->align);
                break;
            }
        }
        QCOMPARE(binner->allTimes, checkAllTimes);
        QCOMPARE(binner->allValues, checkAllValues);

        delete binner;

        /* Run again with no advances */
        binner = new TestRealtimeBinner(Time::Minute, intervalMinutes, align);

        TestEventBinner lastCheck;
        for (QList<TestEventBinner>::const_iterator e = events.constBegin(),
                end = events.constEnd(); e != end; ++e) {
            switch (e->type) {
            case TestEventBinner::Value:
                binner->add(e->time, e->value);
                break;
            case TestEventBinner::Check:
                lastCheck = *e;
                break;
            case TestEventBinner::SetInterval:
                binner->setBinning(Time::Minute, e->interval, e->align);
                break;
            default:
                break;
            }
        }
        QCOMPARE(binner->purged, lastCheck.purged);
        QCOMPARE(binner->emittedTimes, lastCheck.emittedTimes);
        if (!lastCheck.emitted.isEmpty()) {
            QVERIFY(!binner->emitted.isEmpty());
            QCOMPARE(binner->emitted.last(), lastCheck.emitted);
        }
        QCOMPARE(binner->allTimes, checkAllTimes);
        QCOMPARE(binner->allValues, checkAllValues);

        delete binner;
    }

    void binner_data()
    {
        QTest::addColumn<int>("intervalMinutes");
        QTest::addColumn<bool>("align");
        QTest::addColumn<QList<TestEventBinner> >("events");

        QTest::newRow("Empty") << 0 << false << (QList<TestEventBinner>());

        QTest::newRow("Single") << 1 << true << (QList<TestEventBinner>()
                << TestEventBinner(TestEventBinner::Value, 60.0, 1.0)
                << TestEventBinner(QList<double>()));
        QTest::newRow("Emit unaligned") << 1 << false << (QList<TestEventBinner>()
                << TestEventBinner(TestEventBinner::Value, 65.0, 1.0)
                << TestEventBinner(QList<double>())
                << TestEventBinner(TestEventBinner::Value, 75.0, 2.0)
                << TestEventBinner(QList<double>() << 15.0)
                << TestEventBinner(TestEventBinner::Value, 126.0, 3.0)
                << TestEventBinner(QList<double>() << 15.0 << 66.0, QList<double>() << 125.0,
                                   QList<double>() << 1.0 << 2.0)
                << TestEventBinner(TestEventBinner::Value, 184.0, 4.0)
                << TestEventBinner(QList<double>() << 15.0 << 66.0 << 124.0,
                                   QList<double>() << 125.0, QList<double>() << 1.0 << 2.0)
                << TestEventBinner(TestEventBinner::Value, 185.0, 5.0)
                << TestEventBinner(QList<double>() << 15.0 << 66.0 << 124.0 << 125.0,
                                   QList<double>() << 125.0 << 185.0,
                                   QList<double>() << 3.0 << 4.0));
        QTest::newRow("Emit aligned") << 1 << true << (QList<TestEventBinner>()
                << TestEventBinner(TestEventBinner::Value, 65.0, 1.0)
                << TestEventBinner(QList<double>())
                << TestEventBinner(TestEventBinner::Value, 75.0, 2.0)
                << TestEventBinner(QList<double>() << 15.0)
                << TestEventBinner(TestEventBinner::Value, 126.0, 3.0)
                << TestEventBinner(QList<double>() << 15.0 << 66.0, QList<double>() << 120.0,
                                   QList<double>() << 1.0 << 2.0)
                << TestEventBinner(TestEventBinner::Value, 179.0, 4.0)
                << TestEventBinner(QList<double>() << 15.0 << 66.0 << 119.0,
                                   QList<double>() << 120.0, QList<double>() << 1.0 << 2.0)
                << TestEventBinner(TestEventBinner::Value, 180.0, 5.0)
                << TestEventBinner(QList<double>() << 15.0 << 66.0 << 119.0 << 120.0,
                                   QList<double>() << 120.0 << 180.0,
                                   QList<double>() << 3.0 << 4.0));
        QTest::newRow("Advance emit") << 1 << true << (QList<TestEventBinner>()
                << TestEventBinner(TestEventBinner::Value, 65.0, 1.0)
                << TestEventBinner(QList<double>())
                << TestEventBinner(TestEventBinner::Advance, 66.0)
                << TestEventBinner(QList<double>())
                << TestEventBinner(TestEventBinner::Value, 75.0, 2.0)
                << TestEventBinner(QList<double>() << 15.0)
                << TestEventBinner(TestEventBinner::Advance, 119.0)
                << TestEventBinner(QList<double>() << 15.0)
                << TestEventBinner(TestEventBinner::Advance, 120.0)
                << TestEventBinner(QList<double>() << 15.0, QList<double>() << 120.0,
                                   QList<double>() << 1.0 << 2.0)
                << TestEventBinner(TestEventBinner::Value, 126.0, 3.0)
                << TestEventBinner(QList<double>() << 15.0 << 66.0, QList<double>() << 120.0,
                                   QList<double>() << 1.0 << 2.0));
        QTest::newRow("Change interval") << 1 << true << (QList<TestEventBinner>()
                << TestEventBinner(TestEventBinner::Value, 60.0, 1.0)
                << TestEventBinner(QList<double>())
                << TestEventBinner(TestEventBinner::Value, 80.0, 2.0)
                << TestEventBinner(QList<double>() << 20.0)
                << TestEventBinner(TestEventBinner::Value, 121.0, 3.0)
                << TestEventBinner(QList<double>() << 20.0 << 61.0, QList<double>() << 120.0,
                                   QList<double>() << 1.0 << 2.0) << TestEventBinner(true, 2)
                << TestEventBinner(TestEventBinner::Value, 125.0, 4.0)
                << TestEventBinner(QList<double>() << 20.0 << 61.0, QList<double>() << 120.0,
                                   QList<double>() << 1.0 << 2.0)
                << TestEventBinner(TestEventBinner::Value, 201.0, 5.0)
                << TestEventBinner(QList<double>() << 20.0 << 61.0 << 81.0,
                                   QList<double>() << 120.0, QList<double>() << 1.0 << 2.0)
                << TestEventBinner(TestEventBinner::Value, 240.0, 6.0)
                << TestEventBinner(QList<double>() << 20.0 << 61.0 << 81.0 << 120.0,
                                   QList<double>() << 120.0 << 240.0,
                                   QList<double>() << 3.0 << 4.0 << 5.0));
    }

    void segmentProcessor2()
    {
        QFETCH(QList<TestEventSegementer2>, events);

        QList<SegmentData<2> > result;
        for (QList<TestEventSegementer2>::const_iterator e = events.constBegin(),
                end = events.constEnd(); e != end; ++e) {
            if (e->type != TestEventSegementer2::Value)
                continue;
            Range::overlayFragmenting(result,
                                      OverlayData(e->time, FP::undefined(), e->index, e->value));
        }

        TestSegmentProcessor<2> processor;
        bool ended[2];
        memset(ended, 0, sizeof(ended));
        bool hadAllEnd = false;

        for (QList<TestEventSegementer2>::const_iterator e = events.constBegin(),
                end = events.constEnd(); e != end; ++e) {
            switch (e->type) {
            case TestEventSegementer2::Value:
                processor.segmentValue(e->index, e->time, FP::undefined(), e->value);
                break;
            case TestEventSegementer2::Advance:
                processor.segmentAdvance(e->index, e->time);
                break;
            case TestEventSegementer2::End:
                hadAllEnd = processor.segmentEnd(e->index) || hadAllEnd;
                ended[e->index] = true;
                break;
            case TestEventSegementer2::Check:
                QCOMPARE(processor.segments, e->checkData);
                QCOMPARE(processor.advances, e->checkAdvance);
                break;
            }
        }

        for (int i = 0; i < 2; i++) {
            if (ended[i]) continue;
            hadAllEnd = processor.segmentEnd(i) || hadAllEnd;
        }

        QVERIFY(hadAllEnd);
        QCOMPARE(processor.segments, result);


        /* Run it again with no advances to make sure it's consistent */
        hadAllEnd = false;
        memset(ended, 0, sizeof(ended));
        TestSegmentProcessor<2> processor2;
        for (QList<TestEventSegementer2>::const_iterator e = events.constBegin(),
                end = events.constEnd(); e != end; ++e) {
            switch (e->type) {
            case TestEventSegementer2::Value:
                processor2.segmentValue(e->index, e->time, FP::undefined(), e->value);
                break;
            case TestEventSegementer2::End:
                hadAllEnd = processor2.segmentEnd(e->index) || hadAllEnd;
                ended[e->index] = true;
                break;
            default:
                break;
            }
        }
        for (int i = 0; i < 2; i++) {
            if (ended[i]) continue;
            hadAllEnd = processor2.segmentEnd(i) || hadAllEnd;
        }
        QVERIFY(hadAllEnd);
        QCOMPARE(processor2.segments, result);


        /* And again serializing and de-serializing */
        TestSegmentProcessor<2> processor3;
        hadAllEnd = false;
        memset(ended, 0, sizeof(ended));
        for (QList<TestEventSegementer2>::const_iterator e = events.constBegin(),
                end = events.constEnd(); e != end; ++e) {
            {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    processor3.writeSerial(stream);
                }
                {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    processor3.readSerial(stream);
                }
            }

            switch (e->type) {
            case TestEventSegementer2::Value:
                processor3.segmentValue(e->index, e->time, FP::undefined(), e->value);
                break;
            case TestEventSegementer2::Advance:
                processor3.segmentAdvance(e->index, e->time);
                break;
            case TestEventSegementer2::End:
                hadAllEnd = processor3.segmentEnd(e->index) || hadAllEnd;
                ended[e->index] = true;
                break;
            case TestEventSegementer2::Check:
                QCOMPARE(processor3.segments, e->checkData);
                QCOMPARE(processor3.advances, e->checkAdvance);
                break;
            }
        }

        for (int i = 0; i < 2; i++) {
            if (ended[i]) continue;
            hadAllEnd = processor3.segmentEnd(i) || hadAllEnd;
        }

        QVERIFY(hadAllEnd);
        QCOMPARE(processor3.segments, result);
    }

    void segmentProcessor2_data()
    {
        QTest::addColumn<QList<TestEventSegementer2> >("events");

        double v1[2];
        double v2[2];
        double v3[2];

        QTest::newRow("Empty") << (QList<TestEventSegementer2>());

        QTest::newRow("Single") << (QList<TestEventSegementer2>() << TestEventSegementer2()
                                                                  << TestEventSegementer2(
                                                                          TestEventSegementer2::Value,
                                                                          0, 100.0, 1.0)
                                                                  << TestEventSegementer2());

        v1[0] = 1.0;
        v1[1] = FP::undefined();
        v2[0] = 1.0;
        v2[1] = 2.0;
        v3[0] = FP::undefined();
        v3[1] = 2.0;

        QTest::newRow("Single advance") << (QList<TestEventSegementer2>()
                << TestEventSegementer2(TestEventSegementer2::Value, 0, 100.0, 1.0)
                << TestEventSegementer2()
                << TestEventSegementer2(TestEventSegementer2::Advance, 1, 200.0)
                << TestEventSegementer2(QList<SegmentData<2> >() << SegmentData<2>(100.0, -1, v1)));

        QTest::newRow("Multipart advance") << (QList<TestEventSegementer2>()
                << TestEventSegementer2(TestEventSegementer2::Value, 0, 100.0, 1.0)
                << TestEventSegementer2()
                << TestEventSegementer2(TestEventSegementer2::Advance, 1, 150.0)
                << TestEventSegementer2(QList<SegmentData<2> >() << SegmentData<2>(100.0, -1, v1))
                << TestEventSegementer2(TestEventSegementer2::Advance, 0, 200.0)
                << TestEventSegementer2(QList<SegmentData<2> >() << SegmentData<2>(100.0, -1, v1),
                                        QList<double>() << 150.0));

        QTest::newRow("Empty advance") << (QList<TestEventSegementer2>()
                << TestEventSegementer2(TestEventSegementer2::Advance, 1, 150.0)
                << TestEventSegementer2()
                << TestEventSegementer2(TestEventSegementer2::Advance, 0, 200.0)
                << TestEventSegementer2(QList<SegmentData<2> >(), QList<double>() << 150.0)
                << TestEventSegementer2(TestEventSegementer2::Advance, 1, 200.0)
                << TestEventSegementer2(QList<SegmentData<2> >(),
                                        QList<double>() << 150.0 << 200.0));

        QTest::newRow("Identical times") << (QList<TestEventSegementer2>()
                << TestEventSegementer2(TestEventSegementer2::Value, 0, 100.0, 1.0)
                << TestEventSegementer2()
                << TestEventSegementer2(TestEventSegementer2::Value, 1, 100.0, 2.0)
                << TestEventSegementer2(QList<SegmentData<2> >() << SegmentData<2>(100.0, -1, v2)));

        QTest::newRow("Existing overlap") << (QList<TestEventSegementer2>()
                << TestEventSegementer2(TestEventSegementer2::Value, 0, 100.0, 1.0)
                << TestEventSegementer2(TestEventSegementer2::Value, 1, 150.0, 2.0)
                << TestEventSegementer2(QList<SegmentData<2> >() << SegmentData<2>(100.0, -1, v1))
                << TestEventSegementer2(TestEventSegementer2::Advance, 0, 200.0)
                << TestEventSegementer2(QList<SegmentData<2> >() << SegmentData<2>(100.0, -1, v1)
                                                                 << SegmentData<2>(150.0, -1, v2))
                << TestEventSegementer2(TestEventSegementer2::Value, 0, 200.0, FP::undefined())
                << TestEventSegementer2(QList<SegmentData<2> >() << SegmentData<2>(100.0, -1, v1)
                                                                 << SegmentData<2>(150.0, -1, v2))
                << TestEventSegementer2(TestEventSegementer2::Advance, 1, 200.0)
                << TestEventSegementer2(QList<SegmentData<2> >() << SegmentData<2>(100.0, -1, v1)
                                                                 << SegmentData<2>(150.0, -1, v2)
                                                                 << SegmentData<2>(200.0, -1, v3)));

    }

    void conventional()
    {
        TestSimpleTarget targetComplete;
        TestSimpleTarget targetRunning;
        TestSimpleTargetGeneral targetCompleteStats;
        TestSimpleTargetGeneral targetRunningStats;

        RealtimeChainConventional t
                (Time::Second, 1000, true, &targetComplete, &targetRunning, &targetCompleteStats,
                 &targetRunningStats);

        t.getTarget()->incomingData(1000.0, FP::undefined(), 1.0);
        QVERIFY(!targetRunning.values.isEmpty());
        QVERIFY(!targetRunningStats.values.isEmpty());
        QVERIFY(targetComplete.values.isEmpty());
        QVERIFY(targetCompleteStats.values.isEmpty());
        QCOMPARE(targetRunning.values.last(), 1.0);
        t.getTarget()->incomingData(1500.0, FP::undefined(), 2.0);
        QCOMPARE(targetRunning.values.last(), 1.5);
        QVERIFY(targetComplete.values.isEmpty());
        QVERIFY(targetCompleteStats.values.isEmpty());
        t.getTarget()->incomingData(2000.0, FP::undefined(), 4.0);
        QCOMPARE(targetRunning.values.last(), 3.0);
        QVERIFY(!targetComplete.values.isEmpty());
        QCOMPARE(targetComplete.values.last(), 1.5);

        QVERIFY(!targetCompleteStats.values.isEmpty());
        QCOMPARE((int) targetCompleteStats.values.last()["Count"].toInt64(), 2);
        QCOMPARE((int) targetCompleteStats.values.last()["UndefinedCount"].toInt64(), 0);
        QCOMPARE(targetCompleteStats.values.last()["Minimum"].toDouble(), 1.0);
        QCOMPARE(targetCompleteStats.values.last()["Maximum"].toDouble(), 2.0);
        QCOMPARE(targetCompleteStats.values.last()["Mean"].toDouble(), 1.5);

        QVERIFY(!targetRunningStats.values.isEmpty());
        QCOMPARE((int) targetRunningStats.values.last()["Count"].toInt64(), 2);
        QCOMPARE((int) targetRunningStats.values.last()["UndefinedCount"].toInt64(), 0);
        QCOMPARE(targetRunningStats.values.last()["Minimum"].toDouble(), 2.0);
        QCOMPARE(targetRunningStats.values.last()["Maximum"].toDouble(), 4.0);
        QCOMPARE(targetRunningStats.values.last()["Mean"].toDouble(), 3.0);

        t.getTarget()->incomingData(2500.0, FP::undefined(), 8.0);
        QCOMPARE(targetRunning.values.last(), 6.0);
        QCOMPARE(targetComplete.values.last(), 1.5);

        t.getTarget()->incomingData(3000.0, FP::undefined(), 16.0);
        QCOMPARE(targetRunning.values.last(), 12.0);
        QCOMPARE(targetComplete.values.last(), 6.0);
    }

    void difference()
    {
        TestSimpleTarget targetCompleteStart;
        TestSimpleTarget targetCompleteEnd;
        TestSimpleTarget targetRunningStart;
        TestSimpleTarget targetRunningEnd;

        RealtimeChainDifference t
                (Time::Second, 1000, true, &targetCompleteStart, &targetCompleteEnd,
                 &targetRunningStart, &targetRunningEnd);

        t.getTarget()->incomingData(1000.0, FP::undefined(), 1.0);
        QVERIFY(targetCompleteEnd.values.isEmpty());
        QVERIFY(targetCompleteStart.values.isEmpty());
        QVERIFY(!targetRunningStart.values.isEmpty());
        QVERIFY(!targetRunningEnd.values.isEmpty());
        QCOMPARE(targetRunningStart.values.last(), 1.0);
        QVERIFY(!FP::defined(targetRunningEnd.values.last()));
        t.getTarget()->incomingData(1500.0, FP::undefined(), 2.0);
        QCOMPARE(targetRunningStart.values.last(), 1.0);
        QCOMPARE(targetRunningEnd.values.last(), 2.0);
        QVERIFY(targetCompleteEnd.values.isEmpty());
        QVERIFY(targetCompleteStart.values.isEmpty());
        t.getTarget()->incomingData(2000.0, FP::undefined(), 3.0);
        QCOMPARE(targetRunningStart.values.last(), 2.0);
        QCOMPARE(targetRunningEnd.values.last(), 3.0);
        QVERIFY(!targetCompleteEnd.values.isEmpty());
        QVERIFY(!targetCompleteStart.values.isEmpty());
        QCOMPARE(targetCompleteStart.values.last(), 1.0);
        QCOMPARE(targetCompleteEnd.values.last(), 2.0);

        t.getTarget()->incomingData(2500.0, FP::undefined(), 4.0);
        QCOMPARE(targetRunningStart.values.last(), 3.0);
        QCOMPARE(targetRunningEnd.values.last(), 4.0);
        QCOMPARE(targetCompleteStart.values.last(), 1.0);
        QCOMPARE(targetCompleteEnd.values.last(), 2.0);

        t.getTarget()->incomingData(2750.0, FP::undefined(), 5.0);
        QCOMPARE(targetRunningStart.values.last(), 3.0);
        QCOMPARE(targetRunningEnd.values.last(), 5.0);
        QCOMPARE(targetCompleteStart.values.last(), 1.0);
        QCOMPARE(targetCompleteEnd.values.last(), 2.0);

        t.getTarget()->incomingData(3000.0, FP::undefined(), 6.0);
        QCOMPARE(targetRunningStart.values.last(), 4.0);
        QCOMPARE(targetRunningEnd.values.last(), 6.0);
        QCOMPARE(targetCompleteStart.values.last(), 3.0);
        QCOMPARE(targetCompleteEnd.values.last(), 5.0);

        t.getTarget()->incomingData(3250.0, FP::undefined(), 7.0);
        QCOMPARE(targetRunningStart.values.last(), 4.0);
        QCOMPARE(targetRunningEnd.values.last(), 7.0);
        QCOMPARE(targetCompleteStart.values.last(), 3.0);
        QCOMPARE(targetCompleteEnd.values.last(), 5.0);

        t.getTarget()->incomingData(3500.0, FP::undefined(), 8.0);
        QCOMPARE(targetRunningStart.values.last(), 5.0);
        QCOMPARE(targetRunningEnd.values.last(), 8.0);
        QCOMPARE(targetCompleteStart.values.last(), 3.0);
        QCOMPARE(targetCompleteEnd.values.last(), 5.0);

        t.getTarget()->incomingData(3750.0, FP::undefined(), 9.0);
        QCOMPARE(targetRunningStart.values.last(), 6.0);
        QCOMPARE(targetRunningEnd.values.last(), 9.0);
        QCOMPARE(targetCompleteStart.values.last(), 3.0);
        QCOMPARE(targetCompleteEnd.values.last(), 5.0);

        t.getTarget()->incomingData(4000.0, FP::undefined(), 10.0);
        QCOMPARE(targetRunningStart.values.last(), 7.0);
        QCOMPARE(targetRunningEnd.values.last(), 10.0);
        QCOMPARE(targetCompleteStart.values.last(), 6.0);
        QCOMPARE(targetCompleteEnd.values.last(), 9.0);
    }

    void differenceNoAverage()
    {
        TestSimpleTarget targetCompleteStart;
        TestSimpleTarget targetCompleteEnd;
        TestSimpleTarget targetRunningStart;
        TestSimpleTarget targetRunningEnd;

        RealtimeChainDifference t(Time::Second, 0, true, &targetCompleteStart, &targetCompleteEnd,
                                  &targetRunningStart, &targetRunningEnd);

        t.getTarget()->incomingData(1000.0, FP::undefined(), 1.0);
        QVERIFY(!targetCompleteStart.values.isEmpty());
        QVERIFY(!targetCompleteEnd.values.isEmpty());
        QCOMPARE(targetCompleteStart.values.last(), 1.0);
        QVERIFY(!FP::defined(targetCompleteEnd.values.last()));
        QVERIFY(!targetRunningStart.values.isEmpty());
        QVERIFY(!targetRunningEnd.values.isEmpty());
        QCOMPARE(targetRunningStart.values.last(), 1.0);
        QVERIFY(!FP::defined(targetRunningEnd.values.last()));

        t.getTarget()->incomingData(1500.0, FP::undefined(), 2.0);
        QCOMPARE(targetRunningStart.values.last(), 1.0);
        QCOMPARE(targetRunningEnd.values.last(), 2.0);
        QCOMPARE(targetCompleteStart.values.last(), 1.0);
        QCOMPARE(targetCompleteEnd.values.last(), 2.0);
    }

    void flags()
    {
        TestSimpleTargetFlags targetComplete;
        TestSimpleTargetFlags targetRunning;

        RealtimeChainFlags t(Time::Second, 1000, true, &targetComplete, &targetRunning);

        t.getTarget()->incomingData(1000.0, FP::undefined(), Variant::Flags{"F1"});
        QVERIFY(!targetRunning.values.isEmpty());
        QVERIFY(targetComplete.values.isEmpty());
        QCOMPARE(targetRunning.values.last(), (Variant::Flags{"F1"}));

        t.getTarget()->incomingData(1500.0, FP::undefined(), Variant::Flags{"F2"});
        QVERIFY(targetComplete.values.isEmpty());
        QCOMPARE(targetRunning.values.last(), (Variant::Flags{"F1", "F2"}));

        t.getTarget()->incomingData(2000.0, FP::undefined(), Variant::Flags{"F3"});
        QVERIFY(!targetComplete.values.isEmpty());
        QCOMPARE(targetRunning.values.last(), (Variant::Flags{"F2", "F3"}));
        QCOMPARE(targetComplete.values.last(), (Variant::Flags{"F1", "F2"}));

        t.getTarget()->incomingData(2250.0, FP::undefined(), Variant::Flags{"F3"});
        QVERIFY(!targetComplete.values.isEmpty());
        QCOMPARE(targetRunning.values.last(), (Variant::Flags{"F2", "F3"}));
        QCOMPARE(targetComplete.values.last(), (Variant::Flags{"F1", "F2"}));

        t.getTarget()->incomingData(2400.0, FP::undefined(), Variant::Flags{"F4"});
        QVERIFY(!targetComplete.values.isEmpty());
        QCOMPARE(targetRunning.values.last(), (Variant::Flags{"F2", "F3", "F4"}));
        QCOMPARE(targetComplete.values.last(), (Variant::Flags{"F1", "F2"}));

        t.getTarget()->incomingData(3000.0, FP::undefined(), Variant::Flags{"F5"});
        QVERIFY(!targetComplete.values.isEmpty());
        QCOMPARE(targetRunning.values.last(), (Variant::Flags{"F3", "F4", "F5"}));
        QCOMPARE(targetComplete.values.last(), (Variant::Flags{"F2", "F3", "F4"}));
    }

    void vectorStats()
    {
        TestSimpleTarget targetComplete;
        TestSimpleTarget targetRunning;
        TestSimpleTargetGeneral targetCompleteStats;
        TestSimpleTargetGeneral targetRunningStats;

        RealtimeChainVectorStatistics tc
                (Time::Second, 1000, true, &targetComplete, &targetRunning, &targetCompleteStats,
                 &targetRunningStats);

        tc.getTargetValue()->incomingData(7000.0, FP::undefined(), 1.0);
        tc.getTargetValue()->incomingData(7500.0, FP::undefined(), 2.0);
        tc.getTargetValue()->incomingData(7750.0, FP::undefined(), 3.0);
        tc.getTargetValue()->incomingData(8000.0, FP::undefined(), 4.0);
        tc.getTargetVectoredMagnitude()->incomingData(7000.0, FP::undefined(), 1.5);
        tc.getTargetVectoredMagnitude()->incomingData(8000.0, FP::undefined(), 1.5);
        QVERIFY(!targetComplete.values.isEmpty());
        QCOMPARE(targetComplete.values.last(), (1.0 + 2.0 + 3.0) / 3.0);
        QVERIFY(!targetRunning.values.isEmpty());
        QCOMPARE(targetRunning.values.last(), (2.0 + 3.0 + 4.0) / 3.0);

        QVERIFY(!targetRunningStats.values.isEmpty());
        QCOMPARE((int) targetRunningStats.values.last()["Count"].toInt64(), 3);
        QCOMPARE((int) targetRunningStats.values.last()["UndefinedCount"].toInt64(), 0);
        QCOMPARE(targetRunningStats.values.last()["Minimum"].toDouble(), 2.0);
        QCOMPARE(targetRunningStats.values.last()["Maximum"].toDouble(), 4.0);
        QCOMPARE(targetRunningStats.values.last()["Mean"].toDouble(), 3.0);
        QCOMPARE(targetRunningStats.values.last()["VectorMean"].toDouble(), 1.5);
        QCOMPARE(targetRunningStats.values.last()["ConventionalMean"].toDouble(), 3.0);
        QCOMPARE(targetRunningStats.values.last()["StabilityFactor"].toDouble(), 1.5 / 3.0);

        QVERIFY(!targetCompleteStats.values.isEmpty());
        QCOMPARE((int) targetCompleteStats.values.last()["Count"].toInt64(), 3);
        QCOMPARE((int) targetCompleteStats.values.last()["UndefinedCount"].toInt64(), 0);
        QCOMPARE(targetCompleteStats.values.last()["Minimum"].toDouble(), 1.0);
        QCOMPARE(targetCompleteStats.values.last()["Maximum"].toDouble(), 3.0);
        QCOMPARE(targetCompleteStats.values.last()["Mean"].toDouble(), 2.0);
        QCOMPARE(targetCompleteStats.values.last()["VectorMean"].toDouble(), 1.5);
        QCOMPARE(targetCompleteStats.values.last()["ConventionalMean"].toDouble(), 2.0);
        QCOMPARE(targetCompleteStats.values.last()["StabilityFactor"].toDouble(), 1.5 / 2.0);

        tc.getTargetValue()->endData();
        tc.getTargetVectoredMagnitude()->endData();
    }

    void chainGeneral()
    {
        RealtimeChainCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        core.createSmoother(engine1, SmootherChainCore::General);

        QCOMPARE(engine1->inputs.size(), 1);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 1.0);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 1.5);
        engine1->inputs[0].incomingData(200.0, FP::undefined(), 2.0);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::General);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 1);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 3.0);
        engine2->inputs[0].incomingData(400.0, FP::undefined(), 4.0);
        engine2->inputs[0].incomingData(425.0, FP::undefined(), 4.25);
        engine2->inputs[0].incomingData(475.0, FP::undefined(), 4.75);
        engine2->inputs[0].incomingData(500.0, FP::undefined(), 5.0);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root((1.5 + 1.0) / 2.0),
                              200.0, FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root((2.0 + 1.5) / 2.0),
                              300.0, FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(3.0), 400.0,
                              FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"),
                                           Variant::Root((4.75 + 4.25 + 4.0) / 3.0), 500.0,
                                           FP::undefined())));

        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(1.0), 100.0,
                              FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root((1.0 + 1.5) / 2.0), 150.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root((1.5 + 2.0) / 2.0), 200.0,
                                           FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(3.0), 300.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(4.0), 400.0,
                              FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root((4.0 + 4.25) / 2.0), 425.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root((4.0 + 4.25 + 4.75) / 3.0), 475.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root((4.25 + 4.75 + 5.0) / 3.0), 500.0,
                                           FP::undefined())));

        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var1", {"stats"}), Variant::Root(-1),
                              200.0, FP::undefined()), std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var1", {"stats"}), Variant::Root(-1),
                              300.0, FP::undefined()), std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var1", {"stats"}), Variant::Root(-1),
                              400.0, FP::undefined()), std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var1", {"stats"}), Variant::Root(-1),
                              500.0, FP::undefined()), std::equal_to<SequenceIdentity>()));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 100.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 150.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 200.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 300.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 400.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 425.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 475.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 500.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
    }

    void chainGeneralNoStats()
    {
        RealtimeChainCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        core.createSmoother(engine1, SmootherChainCore::General);

        QCOMPARE(engine1->inputs.size(), 1);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 1.0);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 1.5);
        engine1->inputs[0].incomingData(200.0, FP::undefined(), 2.0);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::General);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 1);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 3.0);
        engine2->inputs[0].incomingData(400.0, FP::undefined(), 4.0);
        engine2->inputs[0].incomingData(425.0, FP::undefined(), 4.25);
        engine2->inputs[0].incomingData(475.0, FP::undefined(), 4.75);
        engine2->inputs[0].incomingData(500.0, FP::undefined(), 5.0);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root((1.5 + 1.0) / 2.0),
                              200.0, FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root((2.0 + 1.5) / 2.0),
                              300.0, FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(3.0), 400.0,
                              FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"),
                                           Variant::Root((4.75 + 4.25 + 4.0) / 3.0), 500.0,
                                           FP::undefined())));

        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(1.0), 100.0,
                              FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root((1.0 + 1.5) / 2.0), 150.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root((1.5 + 2.0) / 2.0), 200.0,
                                           FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(3.0), 300.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(4.0), 400.0,
                              FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root((4.0 + 4.25) / 2.0), 425.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root((4.0 + 4.25 + 4.75) / 3.0), 475.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root((4.25 + 4.75 + 5.0) / 3.0), 500.0,
                                           FP::undefined())));

        QVERIFY(!end.contains(SequenceName("bnd", "raw", "var1", {"stats"}),
                              std::equal_to<SequenceName>()));
        QVERIFY(!end.contains(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                              std::equal_to<SequenceName>()));
    }

    void chainDifference()
    {
        RealtimeChainCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "out2"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        core.createSmoother(engine1, SmootherChainCore::DifferenceInitial);

        QCOMPARE(engine1->inputs.size(), 1);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 1.0);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 1.5);
        engine1->inputs[0].incomingData(200.0, FP::undefined(), 2.0);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::DifferenceInitial);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 1);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 3.0);
        engine2->inputs[0].incomingData(400.0, FP::undefined(), 4.0);
        engine2->inputs[0].incomingData(425.0, FP::undefined(), 4.25);
        engine2->inputs[0].incomingData(475.0, FP::undefined(), 4.75);
        engine2->inputs[0].incomingData(500.0, FP::undefined(), 5.0);
        engine2->inputs[0].incomingData(525.0, FP::undefined(), FP::undefined());
        engine2->inputs[0].incomingData(550.0, FP::undefined(), 5.5);
        engine2->inputs[0].incomingData(600.0, FP::undefined(), 6.0);
        engine2->inputs[0].incomingData(650.0, FP::undefined(), 6.5);
        engine2->inputs[0].incomingData(700.0, FP::undefined(), FP::undefined());
        engine2->inputs[0].incomingData(725.0, FP::undefined(), FP::undefined());
        engine2->inputs[0].incomingData(750.0, FP::undefined(), FP::undefined());
        engine2->inputs[0].incomingData(800.0, FP::undefined(), FP::undefined());

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(1.0), 200.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(1.5), 200.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(1.5), 300.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(2.0), 300.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(3.0), 400.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(FP::undefined()),
                              400.0, FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(4.0), 500.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(4.75), 500.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(5.5), 600.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(FP::undefined()),
                              600.0, FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(6.0), 700.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(6.5), 700.0,
                              FP::undefined())));
        QVERIFY(!end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(FP::undefined()),
                              525.0, FP::undefined())));
        QVERIFY(!end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(FP::undefined()),
                              525.0, FP::undefined())));
        QVERIFY(!end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(FP::undefined()),
                              725.0, FP::undefined())));
        QVERIFY(!end.contains(
                SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(FP::undefined()),
                              725.0, FP::undefined())));

        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(1.0), 100.0,
                              FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"),
                                           Variant::Root(FP::undefined()), 100.0,
                                           FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(1.0), 150.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(1.5), 150.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(1.5), 200.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(2.0), 200.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(3.0), 300.0,
                              FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"),
                                           Variant::Root(FP::undefined()), 300.0,
                                           FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(4.0), 400.0,
                              FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"),
                                           Variant::Root(FP::undefined()), 400.0,
                                           FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(4.0), 425.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(4.25), 425.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(4.0), 475.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(4.75), 475.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(4.25), 500.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(5.0), 500.0,
                              FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(FP::undefined()), 525.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"),
                                           Variant::Root(FP::undefined()), 525.0,
                                           FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(5.5), 550.0,
                              FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"),
                                           Variant::Root(FP::undefined()), 550.0,
                                           FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(5.5), 600.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(6.0), 600.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(6.0), 650.0,
                              FP::undefined())));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(6.5), 650.0,
                              FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(FP::undefined()), 700.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"),
                                           Variant::Root(FP::undefined()), 700.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(FP::undefined()), 725.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"),
                                           Variant::Root(FP::undefined()), 725.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(FP::undefined()), 750.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"),
                                           Variant::Root(FP::undefined()), 750.0,
                                           FP::undefined())));
    }

    void chainVector2D()
    {
        RealtimeChainCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "out2"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in2"), NULL);
        core.createSmoother(engine1, SmootherChainCore::Vector2D);

        QCOMPARE(engine1->inputs.size(), 2);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 10.0);
        engine1->inputs[1].incomingData(100.0, FP::undefined(), 1.0);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 10.5);
        engine1->inputs[1].incomingData(150.0, FP::undefined(), 1.5);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::Vector2D);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 2);
        engine2->inputs[0].incomingData(200.0, FP::undefined(), 20.0);
        engine2->inputs[1].incomingData(200.0, FP::undefined(), 2.0);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 30.0);
        engine2->inputs[1].incomingData(300.0, FP::undefined(), 3.0);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                vector2DDir(QVector<double>() << 10.0 << 10.5, QVector<double>() << 1.0 << 1.5)),
                                           200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(
                vector2DMag(QVector<double>() << 10.0 << 10.5, QVector<double>() << 1.0 << 1.5)),
                                           200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                vector2DDir(QVector<double>() << 10.5 << 20.0, QVector<double>() << 1.5 << 2.0)),
                                           300.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(
                vector2DMag(QVector<double>() << 10.5 << 20.0, QVector<double>() << 1.5 << 2.0)),
                                           300.0, FP::undefined())));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector2DDir(QVector<double>() << 10.0, QVector<double>() << 1.0)), 100.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector2DMag(QVector<double>() << 10.0, QVector<double>() << 1.0)), 100.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector2DDir(QVector<double>() << 10.0 << 10.5, QVector<double>() << 1.0 << 1.5)),
                                           150.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector2DMag(QVector<double>() << 10.0 << 10.5, QVector<double>() << 1.0 << 1.5)),
                                           150.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector2DDir(QVector<double>() << 10.5 << 20.0, QVector<double>() << 1.5 << 2.0)),
                                           200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector2DMag(QVector<double>() << 10.5 << 20.0, QVector<double>() << 1.5 << 2.0)),
                                           200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector2DDir(QVector<double>() << 30.0, QVector<double>() << 3.0)), 300.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector2DMag(QVector<double>() << 30.0, QVector<double>() << 3.0)), 300.0,
                                           FP::undefined())));

        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var2", {"stats"}), Variant::Root(-1),
                              200.0, FP::undefined()), std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var2", {"stats"}), Variant::Root(-1),
                              300.0, FP::undefined()), std::equal_to<SequenceIdentity>()));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2", {"stats"}),
                                           Variant::Root(-1), 100.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2", {"stats"}),
                                           Variant::Root(-1), 150.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2", {"stats"}),
                                           Variant::Root(-1), 200.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2", {"stats"}),
                                           Variant::Root(-1), 300.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
    }

    void chainVector2DNoStats()
    {
        RealtimeChainCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "out2"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in2"), NULL);
        core.createSmoother(engine1, SmootherChainCore::Vector2D);

        QCOMPARE(engine1->inputs.size(), 2);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 10.0);
        engine1->inputs[1].incomingData(100.0, FP::undefined(), 1.0);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 10.5);
        engine1->inputs[1].incomingData(150.0, FP::undefined(), 1.5);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::Vector2D);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 2);
        engine2->inputs[0].incomingData(200.0, FP::undefined(), 20.0);
        engine2->inputs[1].incomingData(200.0, FP::undefined(), 2.0);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 30.0);
        engine2->inputs[1].incomingData(300.0, FP::undefined(), 3.0);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                vector2DDir(QVector<double>() << 10.0 << 10.5, QVector<double>() << 1.0 << 1.5)),
                                           200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(
                vector2DMag(QVector<double>() << 10.0 << 10.5, QVector<double>() << 1.0 << 1.5)),
                                           200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                vector2DDir(QVector<double>() << 10.5 << 20.0, QVector<double>() << 1.5 << 2.0)),
                                           300.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(
                vector2DMag(QVector<double>() << 10.5 << 20.0, QVector<double>() << 1.5 << 2.0)),
                                           300.0, FP::undefined())));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector2DDir(QVector<double>() << 10.0, QVector<double>() << 1.0)), 100.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector2DMag(QVector<double>() << 10.0, QVector<double>() << 1.0)), 100.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector2DDir(QVector<double>() << 10.0 << 10.5, QVector<double>() << 1.0 << 1.5)),
                                           150.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector2DMag(QVector<double>() << 10.0 << 10.5, QVector<double>() << 1.0 << 1.5)),
                                           150.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector2DDir(QVector<double>() << 10.5 << 20.0, QVector<double>() << 1.5 << 2.0)),
                                           200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector2DMag(QVector<double>() << 10.5 << 20.0, QVector<double>() << 1.5 << 2.0)),
                                           200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector2DDir(QVector<double>() << 30.0, QVector<double>() << 3.0)), 300.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector2DMag(QVector<double>() << 30.0, QVector<double>() << 3.0)), 300.0,
                                           FP::undefined())));

        QVERIFY(!end.contains(SequenceName("bnd", "raw", "var2", {"stats"}),
                              std::equal_to<SequenceName>()));
        QVERIFY(!end.contains(SequenceName("bnd", "rt_boxcar", "var2", {"stats"}),
                              std::equal_to<SequenceName>()));
    }

    void chainVector3D()
    {
        RealtimeChainCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "out2"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "out3"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in2"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in3"), NULL);
        core.createSmoother(engine1, SmootherChainCore::Vector3D);

        QCOMPARE(engine1->inputs.size(), 3);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 10.0);
        engine1->inputs[1].incomingData(100.0, FP::undefined(), 15.0);
        engine1->inputs[2].incomingData(100.0, FP::undefined(), 1.0);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 10.5);
        engine1->inputs[1].incomingData(150.0, FP::undefined(), 15.5);
        engine1->inputs[2].incomingData(150.0, FP::undefined(), 1.5);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::Vector3D);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 3);
        engine2->inputs[0].incomingData(200.0, FP::undefined(), 20.0);
        engine2->inputs[1].incomingData(200.0, FP::undefined(), 25.0);
        engine2->inputs[2].incomingData(200.0, FP::undefined(), 2.0);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 30.0);
        engine2->inputs[1].incomingData(300.0, FP::undefined(), 35.0);
        engine2->inputs[2].incomingData(300.0, FP::undefined(), 3.0);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                vector3DAzi(QVector<double>() << 10.0 << 10.5, QVector<double>() << 15.0 << 15.5,
                            QVector<double>() << 1.0 << 1.5)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(
                vector3DEle(QVector<double>() << 10.0 << 10.5, QVector<double>() << 15.0 << 15.5,
                            QVector<double>() << 1.0 << 1.5)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out3"), Variant::Root(
                vector3DMag(QVector<double>() << 10.0 << 10.5, QVector<double>() << 15.0 << 15.5,
                            QVector<double>() << 1.0 << 1.5)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                vector3DAzi(QVector<double>() << 10.5 << 20.0, QVector<double>() << 15.5 << 25.0,
                            QVector<double>() << 1.5 << 2.0)), 300.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(
                vector3DEle(QVector<double>() << 10.5 << 20.0, QVector<double>() << 15.5 << 25.0,
                            QVector<double>() << 1.5 << 2.0)), 300.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out3"), Variant::Root(
                vector3DMag(QVector<double>() << 10.5 << 20.0, QVector<double>() << 15.5 << 25.0,
                            QVector<double>() << 1.5 << 2.0)), 300.0, FP::undefined())));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector3DAzi(QVector<double>() << 10.0, QVector<double>() << 15.0,
                            QVector<double>() << 1.0)), 100.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector3DEle(QVector<double>() << 10.0, QVector<double>() << 15.0,
                            QVector<double>() << 1.0)), 100.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var3"), Variant::Root(
                vector3DMag(QVector<double>() << 10.0, QVector<double>() << 15.0,
                            QVector<double>() << 1.0)), 100.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector3DAzi(QVector<double>() << 10.0 << 10.5, QVector<double>() << 15.0 << 15.5,
                            QVector<double>() << 1.0 << 1.5)), 150.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector3DEle(QVector<double>() << 10.0 << 10.5, QVector<double>() << 15.0 << 15.5,
                            QVector<double>() << 1.0 << 1.5)), 150.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var3"), Variant::Root(
                vector3DMag(QVector<double>() << 10.0 << 10.5, QVector<double>() << 15.0 << 15.5,
                            QVector<double>() << 1.0 << 1.5)), 150.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector3DAzi(QVector<double>() << 10.5 << 20.0, QVector<double>() << 15.5 << 25.0,
                            QVector<double>() << 1.5 << 2.0)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector3DEle(QVector<double>() << 10.5 << 20.0, QVector<double>() << 15.5 << 25.0,
                            QVector<double>() << 1.5 << 2.0)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var3"), Variant::Root(
                vector3DMag(QVector<double>() << 10.5 << 20.0, QVector<double>() << 15.5 << 25.0,
                            QVector<double>() << 1.5 << 2.0)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector3DAzi(QVector<double>() << 30.0, QVector<double>() << 35.0,
                            QVector<double>() << 3.0)), 300.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector3DEle(QVector<double>() << 30.0, QVector<double>() << 35.0,
                            QVector<double>() << 3.0)), 300.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var3"), Variant::Root(
                vector3DMag(QVector<double>() << 30.0, QVector<double>() << 35.0,
                            QVector<double>() << 3.0)), 300.0, FP::undefined())));

        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var3", {"stats"}), Variant::Root(-1),
                              200.0, FP::undefined()), std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var3", {"stats"}), Variant::Root(-1),
                              300.0, FP::undefined()), std::equal_to<SequenceIdentity>()));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var3", {"stats"}),
                                           Variant::Root(-1), 100.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var3", {"stats"}),
                                           Variant::Root(-1), 150.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var3", {"stats"}),
                                           Variant::Root(-1), 200.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var3", {"stats"}),
                                           Variant::Root(-1), 300.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
    }

    void chainVector3DNoStats()
    {
        RealtimeChainCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "out2"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "out3"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in2"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in3"), NULL);
        core.createSmoother(engine1, SmootherChainCore::Vector3D);

        QCOMPARE(engine1->inputs.size(), 3);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 10.0);
        engine1->inputs[1].incomingData(100.0, FP::undefined(), 15.0);
        engine1->inputs[2].incomingData(100.0, FP::undefined(), 1.0);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 10.5);
        engine1->inputs[1].incomingData(150.0, FP::undefined(), 15.5);
        engine1->inputs[2].incomingData(150.0, FP::undefined(), 1.5);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::Vector3D);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 3);
        engine2->inputs[0].incomingData(200.0, FP::undefined(), 20.0);
        engine2->inputs[1].incomingData(200.0, FP::undefined(), 25.0);
        engine2->inputs[2].incomingData(200.0, FP::undefined(), 2.0);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 30.0);
        engine2->inputs[1].incomingData(300.0, FP::undefined(), 35.0);
        engine2->inputs[2].incomingData(300.0, FP::undefined(), 3.0);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                vector3DAzi(QVector<double>() << 10.0 << 10.5, QVector<double>() << 15.0 << 15.5,
                            QVector<double>() << 1.0 << 1.5)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(
                vector3DEle(QVector<double>() << 10.0 << 10.5, QVector<double>() << 15.0 << 15.5,
                            QVector<double>() << 1.0 << 1.5)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out3"), Variant::Root(
                vector3DMag(QVector<double>() << 10.0 << 10.5, QVector<double>() << 15.0 << 15.5,
                            QVector<double>() << 1.0 << 1.5)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                vector3DAzi(QVector<double>() << 10.5 << 20.0, QVector<double>() << 15.5 << 25.0,
                            QVector<double>() << 1.5 << 2.0)), 300.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(
                vector3DEle(QVector<double>() << 10.5 << 20.0, QVector<double>() << 15.5 << 25.0,
                            QVector<double>() << 1.5 << 2.0)), 300.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out3"), Variant::Root(
                vector3DMag(QVector<double>() << 10.5 << 20.0, QVector<double>() << 15.5 << 25.0,
                            QVector<double>() << 1.5 << 2.0)), 300.0, FP::undefined())));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector3DAzi(QVector<double>() << 10.0, QVector<double>() << 15.0,
                            QVector<double>() << 1.0)), 100.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector3DEle(QVector<double>() << 10.0, QVector<double>() << 15.0,
                            QVector<double>() << 1.0)), 100.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var3"), Variant::Root(
                vector3DMag(QVector<double>() << 10.0, QVector<double>() << 15.0,
                            QVector<double>() << 1.0)), 100.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector3DAzi(QVector<double>() << 10.0 << 10.5, QVector<double>() << 15.0 << 15.5,
                            QVector<double>() << 1.0 << 1.5)), 150.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector3DEle(QVector<double>() << 10.0 << 10.5, QVector<double>() << 15.0 << 15.5,
                            QVector<double>() << 1.0 << 1.5)), 150.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var3"), Variant::Root(
                vector3DMag(QVector<double>() << 10.0 << 10.5, QVector<double>() << 15.0 << 15.5,
                            QVector<double>() << 1.0 << 1.5)), 150.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector3DAzi(QVector<double>() << 10.5 << 20.0, QVector<double>() << 15.5 << 25.0,
                            QVector<double>() << 1.5 << 2.0)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector3DEle(QVector<double>() << 10.5 << 20.0, QVector<double>() << 15.5 << 25.0,
                            QVector<double>() << 1.5 << 2.0)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var3"), Variant::Root(
                vector3DMag(QVector<double>() << 10.5 << 20.0, QVector<double>() << 15.5 << 25.0,
                            QVector<double>() << 1.5 << 2.0)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                vector3DAzi(QVector<double>() << 30.0, QVector<double>() << 35.0,
                            QVector<double>() << 3.0)), 300.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var2"), Variant::Root(
                vector3DEle(QVector<double>() << 30.0, QVector<double>() << 35.0,
                            QVector<double>() << 3.0)), 300.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var3"), Variant::Root(
                vector3DMag(QVector<double>() << 30.0, QVector<double>() << 35.0,
                            QVector<double>() << 3.0)), 300.0, FP::undefined())));

        QVERIFY(!end.contains(SequenceName("bnd", "raw", "var3", {"stats"}),
                              std::equal_to<SequenceName>()));
        QVERIFY(!end.contains(SequenceName("bnd", "rt_boxcar", "var3", {"stats"}),
                              std::equal_to<SequenceName>()));
    }

    void chainBeersLaw()
    {
        RealtimeChainCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in2"), NULL);
        core.createSmoother(engine1, SmootherChainCore::BeersLawAbsorptionInitial);

        QCOMPARE(engine1->inputs.size(), 3);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 100.0);
        engine1->inputs[1].incomingData(100.0, FP::undefined(), 101.0);
        engine1->inputs[2].incomingData(100.0, FP::undefined(), -1.0);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 150.0);
        engine1->inputs[1].incomingData(150.0, FP::undefined(), 151.0);
        engine1->inputs[2].incomingData(150.0, FP::undefined(), -1.0);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::BeersLawAbsorptionInitial);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 3);
        engine2->inputs[0].incomingData(200.0, FP::undefined(), 200.0);
        engine2->inputs[1].incomingData(200.0, FP::undefined(), 201.0);
        engine2->inputs[2].incomingData(200.0, FP::undefined(), -1.0);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 300.0);
        engine2->inputs[1].incomingData(300.0, FP::undefined(), 301.0);
        engine2->inputs[2].incomingData(300.0, FP::undefined(), -1.0);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"),
                                           Variant::Root(beersLaw(100.0, 150.0, 101.0, 151.0)),
                                           200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"),
                                           Variant::Root(beersLaw(150.0, 200.0, 151.0, 201.0)),
                                           300.0, FP::undefined())));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(FP::undefined()), 100.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(beersLaw(100.0, 150.0, 101.0, 151.0)),
                                           150.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(beersLaw(150.0, 200.0, 151.0, 201.0)),
                                           200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(FP::undefined()), 300.0,
                                           FP::undefined())));

        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var1", {"stats"}), Variant::Root(-1),
                              200.0, FP::undefined()), std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var1", {"stats"}), Variant::Root(-1),
                              300.0, FP::undefined()), std::equal_to<SequenceIdentity>()));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 100.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 150.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 200.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 300.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
    }

    void chainDewpoint()
    {
        RealtimeChainCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in2"), NULL);
        core.createSmoother(engine1, SmootherChainCore::Dewpoint);

        QCOMPARE(engine1->inputs.size(), 3);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 10.10);
        engine1->inputs[1].incomingData(100.0, FP::undefined(), 11.10);
        engine1->inputs[2].incomingData(100.0, FP::undefined(), -1.0);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 10.15);
        engine1->inputs[1].incomingData(150.0, FP::undefined(), 11.15);
        engine1->inputs[2].incomingData(150.0, FP::undefined(), -1.0);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::Dewpoint);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 3);
        engine2->inputs[0].incomingData(200.0, FP::undefined(), 10.20);
        engine2->inputs[1].incomingData(200.0, FP::undefined(), 11.20);
        engine2->inputs[2].incomingData(200.0, FP::undefined(), -1.0);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 10.30);
        engine2->inputs[1].incomingData(300.0, FP::undefined(), 11.30);
        engine2->inputs[2].incomingData(300.0, FP::undefined(), -1.0);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                Dewpoint::dewpoint((10.10 + 10.15) / 2.0, (11.10 + 11.15) / 2.0)), 200.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                Dewpoint::dewpoint((10.20 + 10.15) / 2.0, (11.20 + 11.15) / 2.0)), 300.0,
                                           FP::undefined())));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(Dewpoint::dewpoint(10.10, 11.10)), 100.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::dewpoint((10.10 + 10.15) / 2.0, (11.10 + 11.15) / 2.0)), 150.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::dewpoint((10.15 + 10.20) / 2.0, (11.15 + 11.20) / 2.0)), 200.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(Dewpoint::dewpoint(10.30, 11.30)), 300.0,
                                           FP::undefined())));

        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var1", {"stats"}), Variant::Root(-1),
                              200.0, FP::undefined()), std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var1", {"stats"}), Variant::Root(-1),
                              300.0, FP::undefined()), std::equal_to<SequenceIdentity>()));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 100.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 150.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 200.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 300.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
    }

    void chainDewpointNoStats()
    {
        RealtimeChainCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in2"), NULL);
        core.createSmoother(engine1, SmootherChainCore::Dewpoint);

        QCOMPARE(engine1->inputs.size(), 2);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 10.10);
        engine1->inputs[1].incomingData(100.0, FP::undefined(), 11.10);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 10.15);
        engine1->inputs[1].incomingData(150.0, FP::undefined(), 11.15);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::Dewpoint);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 2);
        engine2->inputs[0].incomingData(200.0, FP::undefined(), 10.20);
        engine2->inputs[1].incomingData(200.0, FP::undefined(), 11.20);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 10.30);
        engine2->inputs[1].incomingData(300.0, FP::undefined(), 11.30);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                Dewpoint::dewpoint((10.10 + 10.15) / 2.0, (11.10 + 11.15) / 2.0)), 200.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                Dewpoint::dewpoint((10.20 + 10.15) / 2.0, (11.20 + 11.15) / 2.0)), 300.0,
                                           FP::undefined())));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(Dewpoint::dewpoint(10.10, 11.10)), 100.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::dewpoint((10.10 + 10.15) / 2.0, (11.10 + 11.15) / 2.0)), 150.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::dewpoint((10.15 + 10.20) / 2.0, (11.15 + 11.20) / 2.0)), 200.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(Dewpoint::dewpoint(10.30, 11.30)), 300.0,
                                           FP::undefined())));

        QVERIFY(!end.contains(SequenceName("bnd", "raw", "var1", {"stats"}),
                              std::equal_to<SequenceName>()));
        QVERIFY(!end.contains(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                              std::equal_to<SequenceName>()));
    }

    void chainRH()
    {
        RealtimeChainCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in2"), NULL);
        core.createSmoother(engine1, SmootherChainCore::RH);

        QCOMPARE(engine1->inputs.size(), 3);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 10.10);
        engine1->inputs[1].incomingData(100.0, FP::undefined(), 11.10);
        engine1->inputs[2].incomingData(100.0, FP::undefined(), -1.0);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 10.15);
        engine1->inputs[1].incomingData(150.0, FP::undefined(), 11.15);
        engine1->inputs[2].incomingData(150.0, FP::undefined(), -1.0);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::RH);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 3);
        engine2->inputs[0].incomingData(200.0, FP::undefined(), 10.20);
        engine2->inputs[1].incomingData(200.0, FP::undefined(), 11.20);
        engine2->inputs[2].incomingData(200.0, FP::undefined(), -1.0);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 10.30);
        engine2->inputs[1].incomingData(300.0, FP::undefined(), 11.30);
        engine2->inputs[2].incomingData(300.0, FP::undefined(), -1.0);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                Dewpoint::rh((10.10 + 10.15) / 2.0, (11.10 + 11.15) / 2.0)), 200.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                Dewpoint::rh((10.20 + 10.15) / 2.0, (11.20 + 11.15) / 2.0)), 300.0,
                                           FP::undefined())));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(Dewpoint::rh(10.10, 11.10)), 100.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::rh((10.10 + 10.15) / 2.0, (11.10 + 11.15) / 2.0)), 150.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::rh((10.15 + 10.20) / 2.0, (11.15 + 11.20) / 2.0)), 200.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(Dewpoint::rh(10.30, 11.30)), 300.0,
                                           FP::undefined())));

        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var1", {"stats"}), Variant::Root(-1),
                              200.0, FP::undefined()), std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var1", {"stats"}), Variant::Root(-1),
                              300.0, FP::undefined()), std::equal_to<SequenceIdentity>()));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 100.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 150.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 200.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 300.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
    }

    void chainRHNoStats()
    {
        RealtimeChainCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in2"), NULL);
        core.createSmoother(engine1, SmootherChainCore::RH);

        QCOMPARE(engine1->inputs.size(), 2);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 10.10);
        engine1->inputs[1].incomingData(100.0, FP::undefined(), 11.10);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 10.15);
        engine1->inputs[1].incomingData(150.0, FP::undefined(), 11.15);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::RH);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 2);
        engine2->inputs[0].incomingData(200.0, FP::undefined(), 10.20);
        engine2->inputs[1].incomingData(200.0, FP::undefined(), 11.20);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 10.30);
        engine2->inputs[1].incomingData(300.0, FP::undefined(), 11.30);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                Dewpoint::rh((10.10 + 10.15) / 2.0, (11.10 + 11.15) / 2.0)), 200.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                Dewpoint::rh((10.20 + 10.15) / 2.0, (11.20 + 11.15) / 2.0)), 300.0,
                                           FP::undefined())));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(Dewpoint::rh(10.10, 11.10)), 100.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::rh((10.10 + 10.15) / 2.0, (11.10 + 11.15) / 2.0)), 150.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::rh((10.15 + 10.20) / 2.0, (11.15 + 11.20) / 2.0)), 200.0,
                                           FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"),
                                           Variant::Root(Dewpoint::rh(10.30, 11.30)), 300.0,
                                           FP::undefined())));

        QVERIFY(!end.contains(SequenceName("bnd", "raw", "var1", {"stats"}),
                              std::equal_to<SequenceName>()));
        QVERIFY(!end.contains(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                              std::equal_to<SequenceName>()));
    }

    void chainRHExtrapolate()
    {
        RealtimeChainCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in2"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in3"), NULL);
        core.createSmoother(engine1, SmootherChainCore::RHExtrapolate);

        QCOMPARE(engine1->inputs.size(), 4);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 10.10);
        engine1->inputs[1].incomingData(100.0, FP::undefined(), 11.10);
        engine1->inputs[2].incomingData(100.0, FP::undefined(), 12.10);
        engine1->inputs[3].incomingData(100.0, FP::undefined(), -1.0);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 10.15);
        engine1->inputs[1].incomingData(150.0, FP::undefined(), 11.15);
        engine1->inputs[2].incomingData(150.0, FP::undefined(), 12.15);
        engine1->inputs[3].incomingData(150.0, FP::undefined(), -1.0);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::RHExtrapolate);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 4);
        engine2->inputs[0].incomingData(200.0, FP::undefined(), 10.20);
        engine2->inputs[1].incomingData(200.0, FP::undefined(), 11.20);
        engine2->inputs[2].incomingData(200.0, FP::undefined(), 12.20);
        engine2->inputs[3].incomingData(200.0, FP::undefined(), -1.0);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 10.30);
        engine2->inputs[1].incomingData(300.0, FP::undefined(), 11.30);
        engine2->inputs[2].incomingData(300.0, FP::undefined(), 12.30);
        engine2->inputs[3].incomingData(300.0, FP::undefined(), -1.0);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                Dewpoint::rhExtrapolate((10.10 + 10.15) / 2.0, (11.10 + 11.15) / 2.0,
                                        (12.10 + 12.15) / 2.0)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                Dewpoint::rhExtrapolate((10.20 + 10.15) / 2.0, (11.20 + 11.15) / 2.0,
                                        (12.20 + 12.15) / 2.0)), 300.0, FP::undefined())));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::rhExtrapolate(10.10, 11.10, 12.10)), 100.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::rhExtrapolate((10.10 + 10.15) / 2.0, (11.10 + 11.15) / 2.0,
                                        (12.10 + 12.15) / 2.0)), 150.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::rhExtrapolate((10.15 + 10.20) / 2.0, (11.15 + 11.20) / 2.0,
                                        (12.15 + 12.20) / 2.0)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::rhExtrapolate(10.30, 11.30, 12.30)), 300.0, FP::undefined())));

        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var1", {"stats"}), Variant::Root(-1),
                              200.0, FP::undefined()), std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(
                SequenceValue(SequenceName("bnd", "raw", "var1", {"stats"}), Variant::Root(-1),
                              300.0, FP::undefined()), std::equal_to<SequenceIdentity>()));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 100.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 150.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 200.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                                           Variant::Root(-1), 300.0, FP::undefined()),
                             std::equal_to<SequenceIdentity>()));
    }

    void chainRHExtrapolateNoStats()
    {
        RealtimeChainCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in2"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "in3"), NULL);
        core.createSmoother(engine1, SmootherChainCore::RHExtrapolate);

        QCOMPARE(engine1->inputs.size(), 3);
        engine1->inputs[0].incomingData(100.0, FP::undefined(), 10.10);
        engine1->inputs[1].incomingData(100.0, FP::undefined(), 11.10);
        engine1->inputs[2].incomingData(100.0, FP::undefined(), 12.10);
        engine1->inputs[0].incomingData(150.0, FP::undefined(), 10.15);
        engine1->inputs[1].incomingData(150.0, FP::undefined(), 11.15);
        engine1->inputs[2].incomingData(150.0, FP::undefined(), 12.15);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::RHExtrapolate);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 3);
        engine2->inputs[0].incomingData(200.0, FP::undefined(), 10.20);
        engine2->inputs[1].incomingData(200.0, FP::undefined(), 11.20);
        engine2->inputs[2].incomingData(200.0, FP::undefined(), 12.20);
        engine2->inputs[0].incomingData(300.0, FP::undefined(), 10.30);
        engine2->inputs[1].incomingData(300.0, FP::undefined(), 11.30);
        engine2->inputs[2].incomingData(300.0, FP::undefined(), 12.30);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                Dewpoint::rhExtrapolate((10.10 + 10.15) / 2.0, (11.10 + 11.15) / 2.0,
                                        (12.10 + 12.15) / 2.0)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(
                Dewpoint::rhExtrapolate((10.20 + 10.15) / 2.0, (11.20 + 11.15) / 2.0,
                                        (12.20 + 12.15) / 2.0)), 300.0, FP::undefined())));

        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::rhExtrapolate(10.10, 11.10, 12.10)), 100.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::rhExtrapolate((10.10 + 10.15) / 2.0, (11.10 + 11.15) / 2.0,
                                        (12.10 + 12.15) / 2.0)), 150.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::rhExtrapolate((10.15 + 10.20) / 2.0, (11.15 + 11.20) / 2.0,
                                        (12.15 + 12.20) / 2.0)), 200.0, FP::undefined())));
        QVERIFY(end.contains(SequenceValue(SequenceName("bnd", "rt_boxcar", "var1"), Variant::Root(
                Dewpoint::rhExtrapolate(10.30, 11.30, 12.30)), 300.0, FP::undefined())));

        QVERIFY(!end.contains(SequenceName("bnd", "raw", "var1", {"stats"}),
                              std::equal_to<SequenceName>()));
        QVERIFY(!end.contains(SequenceName("bnd", "rt_boxcar", "var1", {"stats"}),
                              std::equal_to<SequenceName>()));
    }
};

QTEST_APPLESS_MAIN(TestRealtimeChain)

#include "realtimechain.moc"
