/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>
#include <QVector>

#include "smoothing/blocksmootherchain.hxx"
#include "core/number.hxx"
#include "core/range.hxx"

using namespace CPD3;
using namespace CPD3::Smoothing;
using namespace CPD3::Data;

struct TV {
    double start;
    double end;
    double value;

    TV(double s, double e, double v) : start(s), end(e), value(v)
    { }

    TV() : start(FP::undefined()), end(FP::undefined()), value(FP::undefined())
    { }

    bool operator==(const TV &other) const
    {
        return FP::equal(start, other.start) &&
                FP::equal(end, other.end) &&
                FP::equal(value, other.value);
    }

    bool operator!=(const TV &other) const
    { return !(*this == other); }
};

Q_DECLARE_METATYPE(TV);

Q_DECLARE_METATYPE(QList<TV>);

namespace QTest {
template<>
char *toString(const QList<double> &output)
{
    QByteArray ba;
    for (int i = 0, max = output.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        if (FP::defined(output.at(i)))
            ba += QByteArray::number(output.at(i));
        else
            ba += "Undefined";
    }
    return qstrdup(ba.data());
}

template<>
char *toString(const QList<TV> &output)
{
    QByteArray ba;
    for (int i = 0, max = output.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        ba += "(";
        if (FP::defined(output.at(i).start))
            ba += QByteArray::number(output.at(i).start);
        else
            ba += "Undefined";
        ba += ",";
        if (FP::defined(output.at(i).end))
            ba += QByteArray::number(output.at(i).end);
        else
            ba += "Undefined";
        ba += ",";
        if (FP::defined(output.at(i).value))
            ba += QByteArray::number(output.at(i).value);
        else
            ba += "Undefined";
        ba += ")";
    }
    return qstrdup(ba.data());
}
}

class ChainInstance : public BlockSmootherChain {
public:
    class Target : public SmootherChainTarget {
    public:
        bool ended;
        QList<TV> values;
        QList<double> advances;

        Target() : ended(false)
        { }

        virtual ~Target()
        { }

        virtual void incomingData(double start, double end, double value)
        {
            Q_ASSERT(!ended);
            values.append(TV(start, end, value));
        }

        virtual void incomingAdvance(double time)
        {
            Q_ASSERT(!ended);
            advances.append(time);
        }

        virtual void endData()
        {
            ended = true;
        }
    };

    Target target;

    ChainInstance(DynamicTimeInterval *gap) : BlockSmootherChain(gap, &target)
    {
        setMaximumBuffer(4);
    }

    ChainInstance(QDataStream &stream) : BlockSmootherChain(stream, &target)
    { }

    virtual ~ChainInstance()
    { }

protected:
    class Handler : public virtual BlockSmootherChain::Handler {
    public:
        double key;

        Handler(double k) : key(k)
        { }

        virtual ~Handler()
        { }

        virtual bool process(BlockSmootherChain::Value *&buffer, size_t &size)
        {
            for (BlockSmootherChain::Value *v = buffer, *end = buffer + size; v != end; ++v) {
                v->value = key;
            }
            return false;
        }
    };

    virtual BlockSmootherChain::Handler *createHandler(double start, double end)
    {
        Q_UNUSED(end);
        return new Handler(start);
    }
};

struct TestEvent {
    enum Type {
        Advance, Value, Check,
    };
    Type type;
    double start;
    double end;
    double value;
    QList<TV> checkData;
    QList<double> checkAdvance;

    TestEvent(Type t, double s, double e = FP::undefined(), double v = FP::undefined()) : type(t),
                                                                                          start(s),
                                                                                          end(e),
                                                                                          value(v),
                                                                                          checkData(),
                                                                                          checkAdvance()
    { }

    TestEvent(double s, double e = FP::undefined(), double v = FP::undefined()) : type(Value),
                                                                                  start(s),
                                                                                  end(e),
                                                                                  value(v),
                                                                                  checkData(),
                                                                                  checkAdvance()
    { }

    TestEvent(const QList<TV> &data, const QList<double> &adv = QList<double>()) : type(Check),
                                                                                   start(FP::undefined()),
                                                                                   end(FP::undefined()),
                                                                                   value(FP::undefined()),
                                                                                   checkData(data),
                                                                                   checkAdvance(adv)
    { }

    TestEvent() : type(Check),
                  start(FP::undefined()),
                  end(FP::undefined()),
                  value(FP::undefined()),
                  checkData(),
                  checkAdvance()
    { }
};

Q_DECLARE_METATYPE(TestEvent);

Q_DECLARE_METATYPE(QList<TestEvent>);

class TestBlockSmootherChain : public QObject {
Q_OBJECT

private slots:

    void general()
    {
        QFETCH(int, gapSeconds);
        QFETCH(QList<TestEvent>, events);
        QFETCH(QList<TV>, final);

        DynamicTimeInterval *gap;
        if (gapSeconds > 0) {
            gap = new DynamicTimeInterval::Constant(Time::Second, gapSeconds, false);
        } else {
            gap = NULL;
        }

        ChainInstance *chain1 = new ChainInstance(gap == NULL ? NULL : gap->clone());
        for (QList<TestEvent>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            QVERIFY(!chain1->finished());
            switch (e->type) {
            case TestEvent::Advance:
                chain1->getTarget()->incomingAdvance(e->start);
                break;
            case TestEvent::Value:
                chain1->getTarget()->incomingData(e->start, e->end, e->value);
                break;
            case TestEvent::Check:
                chain1->pause();
                QCOMPARE(chain1->target.values, e->checkData);
                QCOMPARE(chain1->target.advances, e->checkAdvance);
                chain1->resume();
                break;
            }
        }
        QVERIFY(!chain1->finished());
        chain1->getTarget()->endData();
        chain1->cleanup();
        QVERIFY(chain1->finished());
        QCOMPARE(chain1->target.values, final);
        QVERIFY(chain1->target.ended);
        delete chain1;

        ChainInstance *chain2 = new ChainInstance(gap == NULL ? NULL : gap->clone());
        for (QList<TestEvent>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            switch (e->type) {
            case TestEvent::Value:
                chain2->getTarget()->incomingData(e->start, e->end, e->value);
                break;
            default:
                break;
            }
        }
        chain2->getTarget()->endData();
        chain2->cleanup();
        QCOMPARE(chain2->target.values, final);
        QVERIFY(chain2->target.ended);
        delete chain2;

        ChainInstance *chain3 = new ChainInstance(gap == NULL ? NULL : gap->clone());
        for (QList<TestEvent>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            {
                QByteArray data;
                chain3->pause();
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    chain3->serialize(stream);
                }
                QList<TV> saveValues(chain3->target.values);
                QList<double> saveAdvance(chain3->target.advances);
                chain3->resume();
                delete chain3;
                {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    chain3 = new ChainInstance(stream);
                }
                chain3->target.values = saveValues;
                chain3->target.advances = saveAdvance;
            }
            switch (e->type) {
            case TestEvent::Advance:
                chain3->getTarget()->incomingAdvance(e->start);
                break;
            case TestEvent::Value:
                chain3->getTarget()->incomingData(e->start, e->end, e->value);
                break;
            case TestEvent::Check:
                chain3->pause();
                QCOMPARE(chain3->target.values, e->checkData);
                QCOMPARE(chain3->target.advances, e->checkAdvance);
                chain3->resume();
                break;
            }
        }
        chain3->getTarget()->endData();
        chain3->cleanup();
        QCOMPARE(chain3->target.values, final);
        QVERIFY(chain3->target.ended);
        delete chain3;

        ChainInstance *chain4 = new ChainInstance(gap == NULL ? NULL : gap->clone());
        for (QList<TestEvent>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            switch (e->type) {
            case TestEvent::Advance:
                chain4->getTarget()->incomingAdvance(e->start);
                QTest::qSleep(50);
                break;
            case TestEvent::Value:
                chain4->getTarget()->incomingData(e->start, e->end, e->value);
                QTest::qSleep(50);
                break;
            case TestEvent::Check:
                chain4->pause();
                QCOMPARE(chain4->target.values, e->checkData);
                QCOMPARE(chain4->target.advances, e->checkAdvance);
                chain4->resume();
                break;
            }
        }
        chain4->getTarget()->endData();
        chain4->cleanup();
        QCOMPARE(chain4->target.values, final);
        QVERIFY(chain4->target.ended);
        delete chain4;

        if (gap != NULL)
            delete gap;
    }

    void general_data()
    {
        QTest::addColumn<int>("gapSeconds");
        QTest::addColumn<QList<TestEvent> >("events");
        QTest::addColumn<QList<TV> >("final");

        QTest::newRow("Empty") << 0 << QList<TestEvent>() << QList<TV>();

        QTest::newRow("Single Block") <<
                0 <<
                (QList<TestEvent>() <<
                        TestEvent(1.0, 2.0, 0.01) <<
                        TestEvent() <<
                        TestEvent(2.0, 3.0, 0.02) <<
                        TestEvent() <<
                        TestEvent(3.0, 4.0, 0.03) <<
                        TestEvent()) <<
                (QList<TV>() << TV(1.0, 2.0, 1.0) << TV(2.0, 3.0, 1.0) << TV(3.0, 4.0, 1.0));
        QTest::newRow("Single Block Removal") <<
                0 <<
                (QList<TestEvent>() <<
                        TestEvent(1.0, 2.0, 0.01) <<
                        TestEvent(2.0, 3.0, FP::undefined()) <<
                        TestEvent() <<
                        TestEvent(3.0, 4.0, 0.03) <<
                        TestEvent()) <<
                (QList<TV>() << TV(1.0, 2.0, 1.0) << TV(3.0, 4.0, 1.0));
        QTest::newRow("Multiple Block") <<
                0 <<
                (QList<TestEvent>() <<
                        TestEvent(1.0, 2.0, 0.01) <<
                        TestEvent(2.0, 3.0, 0.02) <<
                        TestEvent(3.0, 4.0, 0.03) <<
                        TestEvent(4.0, 5.0, 0.04) <<
                        TestEvent() <<
                        TestEvent(5.0, 6.0, 0.05) <<
                        TestEvent(QList<TV>() << TV(1.0, 2.0, 1.0) << TV(2.0, 3.0, 1.0))) <<
                (QList<TV>() <<
                        TV(1.0, 2.0, 1.0) <<
                        TV(2.0, 3.0, 1.0) <<
                        TV(3.0, 4.0, 3.0) <<
                        TV(4.0, 5.0, 3.0) <<
                        TV(5.0, 6.0, 3.0));
        QTest::newRow("Basic Gap") <<
                2 <<
                (QList<TestEvent>() <<
                        TestEvent(1.0, 2.0, 0.01) <<
                        TestEvent() <<
                        TestEvent(2.0, 2.9, 0.02) <<
                        TestEvent() <<
                        TestEvent(5.0, 6.0, 0.03) <<
                        TestEvent(QList<TV>() << TV(1.0, 2.0, 1.0) << TV(2.0, 2.9, 1.0)) <<
                        TestEvent(7.0, 8.0, 0.04) <<
                        TestEvent(QList<TV>() << TV(1.0, 2.0, 1.0) << TV(2.0, 2.9, 1.0)) <<
                        TestEvent(8.0, 9.0, 0.05)) <<
                (QList<TV>() <<
                        TV(1.0, 2.0, 1.0) <<
                        TV(2.0, 2.9, 1.0) <<
                        TV(5.0, 6.0, 5.0) <<
                        TV(7.0, 8.0, 5.0) <<
                        TV(8.0, 9.0, 5.0));
        QTest::newRow("Advance") <<
                2 <<
                (QList<TestEvent>() <<
                        TestEvent(1.0, 2.0, 0.01) <<
                        TestEvent(2.0, 3.0, 0.02) <<
                        TestEvent(TestEvent::Advance, 4.0) <<
                        TestEvent() <<
                        TestEvent(4.0, 5.0, 0.03) <<
                        TestEvent(5.0, 6.0, 0.04) <<
                        TestEvent(6.0, 7.0, 0.05) <<
                        TestEvent(QList<TV>() << TV(1.0, 2.0, 1.0) << TV(2.0, 3.0, 1.0)) <<
                        TestEvent(TestEvent::Advance, 10.0) <<
                        TestEvent(QList<TV>() <<
                                          TV(1.0, 2.0, 1.0) <<
                                          TV(2.0, 3.0, 1.0) <<
                                          TV(4.0, 5.0, 4.0) <<
                                          TV(5.0, 6.0, 4.0) <<
                                          TV(6.0, 7.0, 4.0), QList<double>() << 10.0) <<
                        TestEvent(TestEvent::Advance, 11.0) <<
                        TestEvent(QList<TV>() <<
                                          TV(1.0, 2.0, 1.0) <<
                                          TV(2.0, 3.0, 1.0) <<
                                          TV(4.0, 5.0, 4.0) <<
                                          TV(5.0, 6.0, 4.0) <<
                                          TV(6.0, 7.0, 4.0), QList<double>() << 10.0 << 11.0) <<
                        TestEvent(14.0, 15.0, 0.06) <<
                        TestEvent(QList<TV>() <<
                                          TV(1.0, 2.0, 1.0) <<
                                          TV(2.0, 3.0, 1.0) <<
                                          TV(4.0, 5.0, 4.0) <<
                                          TV(5.0, 6.0, 4.0) <<
                                          TV(6.0, 7.0, 4.0), QList<double>() << 10.0 << 11.0) <<
                        TestEvent(TestEvent::Advance, 16.0) <<
                        TestEvent(QList<TV>() <<
                                          TV(1.0, 2.0, 1.0) <<
                                          TV(2.0, 3.0, 1.0) <<
                                          TV(4.0, 5.0, 4.0) <<
                                          TV(5.0, 6.0, 4.0) <<
                                          TV(6.0, 7.0, 4.0), QList<double>() << 10.0 << 11.0)) <<
                (QList<TV>() <<
                        TV(1.0, 2.0, 1.0) <<
                        TV(2.0, 3.0, 1.0) <<
                        TV(4.0, 5.0, 4.0) <<
                        TV(5.0, 6.0, 4.0) <<
                        TV(6.0, 7.0, 4.0) <<
                        TV(14.0, 15.0, 14.0));
    }
};

QTEST_APPLESS_MAIN(TestBlockSmootherChain)

#include "blocksmootherchain.moc"
