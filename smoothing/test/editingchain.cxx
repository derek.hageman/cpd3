/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>
#include <QList>
#include <QVarLengthArray>

#include "smoothing/editingchain.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "core/timeutils.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "algorithms/dewpoint.hxx"

using namespace CPD3;
using namespace CPD3::Smoothing;
using namespace CPD3::Data;
using namespace CPD3::Algorithms;

class TestChainEngine : public virtual SmootherChainCoreEngineInterface {
public:
    SinkMultiplexer mux;

    QList<SmootherChainNode *> chain;

    virtual void addChainNode(SmootherChainNode *add)
    { chain.append(add); }

    class TestChainOutput : public SmootherChainTarget {
    public:
        SinkMultiplexer::Sink *ingress;
        SequenceName unit;
        bool ended;

        TestChainOutput(SinkMultiplexer::Sink *i, const SequenceName &u) : ingress(i),
                                                                           unit(u),
                                                                           ended(false)
        { }

        virtual ~TestChainOutput()
        { }

        virtual void incomingData(double start, double end, double value)
        {
            Q_ASSERT(!ended);
            ingress->incomingData(SequenceValue(unit, Variant::Root(value), start, end));
        }

        virtual void incomingAdvance(double time)
        {
            Q_ASSERT(!ended);
            ingress->incomingAdvance(time);
        }

        virtual void endData()
        {
            Q_ASSERT(!ended);
            ingress->endData();
            ended = true;
        }
    };

    QList<TestChainOutput *> outputs;

    virtual SmootherChainTarget *addNewOutput(const SequenceName &unit)
    {
        for (QList<TestChainOutput *>::const_iterator check = outputs.constBegin(),
                end = outputs.constEnd(); check != end; ++check) {
            if ((*check)->unit == unit) {
                qDebug() << "Duplicate output added:" << unit;
                return *check;
            }
        }
        TestChainOutput *add = new TestChainOutput(mux.createSegmented(), unit);
        outputs.append(add);
        return add;
    }

    virtual SmootherChainTarget *getOutput(int index)
    {
        Q_ASSERT(index >= 0 && index < outputs.size());
        return outputs.at(index);
    }

    class TestChainOutputFlags : public SmootherChainTargetFlags {
    public:
        SinkMultiplexer::Sink *ingress;
        SequenceName unit;
        bool ended;

        TestChainOutputFlags(SinkMultiplexer::Sink *i, const SequenceName &u) : ingress(i),
                                                                                unit(u),
                                                                                ended(false)
        { }

        virtual ~TestChainOutputFlags()
        { }

        virtual void incomingData(double start, double end, const Data::Variant::Flags &value)
        {
            Q_ASSERT(!ended);
            ingress->incomingData(SequenceValue(unit, Variant::Root(value), start, end));
        }

        virtual void incomingAdvance(double time)
        {
            Q_ASSERT(!ended);
            ingress->incomingAdvance(time);
        }

        virtual void endData()
        {
            Q_ASSERT(!ended);
            ingress->endData();
            ended = true;
        }
    };

    QList<TestChainOutputFlags *> outputsFlags;

    virtual SmootherChainTargetFlags *addNewFlagsOutput(const SequenceName &unit)
    {
        for (QList<TestChainOutputFlags *>::const_iterator check = outputsFlags.constBegin(),
                end = outputsFlags.constEnd(); check != end; ++check) {
            if ((*check)->unit == unit) {
                qDebug() << "Duplicate flags output added:" << unit;
                return *check;
            }
        }
        TestChainOutputFlags *add = new TestChainOutputFlags(mux.createSegmented(), unit);
        outputsFlags.append(add);
        return add;
    }

    virtual SmootherChainTargetFlags *getFlagsOutput(int index)
    {
        Q_ASSERT(index >= 0 && index <= outputsFlags.size());
        return outputsFlags.at(index);
    }

    class TestChainOutputGeneral : public SmootherChainTargetGeneral {
    public:
        SinkMultiplexer::Sink *ingress;
        SequenceName unit;
        bool ended;

        TestChainOutputGeneral(SinkMultiplexer::Sink *i, const SequenceName &u) : ingress(i),
                                                                                  unit(u),
                                                                                  ended(false)
        { }

        virtual ~TestChainOutputGeneral()
        { }

        virtual void incomingData(double start, double end, const Variant::Root &value)
        {
            Q_ASSERT(!ended);
            ingress->incomingData(SequenceValue(unit, value, start, end));
        }

        virtual void incomingAdvance(double time)
        {
            Q_ASSERT(!ended);
            ingress->incomingAdvance(time);
        }

        virtual void endData()
        {
            Q_ASSERT(!ended);
            ingress->endData();
            ended = true;
        }
    };

    QList<TestChainOutputGeneral *> outputsGeneral;

    virtual SmootherChainTargetGeneral *addNewGeneralOutput(const SequenceName &unit)
    {
        for (QList<TestChainOutputGeneral *>::const_iterator check = outputsGeneral.constBegin(),
                end = outputsGeneral.constEnd(); check != end; ++check) {
            if ((*check)->unit == unit) {
                qDebug() << "Duplicate general output added:" << unit;
                return *check;
            }
        }
        TestChainOutputGeneral *add = new TestChainOutputGeneral(mux.createSegmented(), unit);
        outputsGeneral.append(add);
        return add;
    }

    virtual SmootherChainTargetGeneral *getGeneralOutput(int index)
    {
        Q_ASSERT(index >= 0 && index <= outputsGeneral.size());
        return outputsGeneral.at(index);
    }


    struct TestChainInput {
        SequenceName unit;
        QList<SmootherChainTarget *> targets;

        void incomingData(double start, double end, double value)
        {
            for (QList<SmootherChainTarget *>::const_iterator t = targets.constBegin(),
                    endT = targets.constEnd(); t != endT; ++t) {
                (*t)->incomingData(start, end, value);
            }
        }

        void incomingAdvance(double time)
        {
            for (QList<SmootherChainTarget *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->incomingAdvance(time);
            }
        }

        void endData()
        {
            for (QList<SmootherChainTarget *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->endData();
            }
        }
    };

    QList<TestChainInput> inputs;

    virtual void addNewInput(const SequenceName &unit, SmootherChainTarget *target = NULL)
    {
        for (QList<TestChainInput>::iterator it = inputs.begin(); it != inputs.end(); ++it) {
            if (it->unit == unit) {
                if (target != NULL)
                    it->targets.append(target);
                return;
            }
        }
        inputs.append(TestChainInput());
        inputs.last().unit = unit;
        if (target != NULL)
            inputs.last().targets.append(target);
    }

    virtual void addInputTarget(int index, SmootherChainTarget *target)
    {
        Q_ASSERT(index >= 0 && index < inputs.size());
        Q_ASSERT(target != NULL);
        inputs[index].targets.append(target);
    }

    void advanceAllInputsAfter(int index, double time)
    {
        for (int i = index; i < inputs.size(); i++) {
            inputs[i].incomingAdvance(time);
        }
    }

    struct TestChainInputFlags {
        SequenceName unit;
        QList<SmootherChainTargetFlags *> targets;

        void incomingData(double start, double end, const Data::Variant::Flags &value)
        {
            for (QList<SmootherChainTargetFlags *>::const_iterator t = targets.constBegin(),
                    endT = targets.constEnd(); t != endT; ++t) {
                (*t)->incomingData(start, end, value);
            }
        }

        void incomingAdvance(double time)
        {
            for (QList<SmootherChainTargetFlags *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->incomingAdvance(time);
            }
        }

        void endData()
        {
            for (QList<SmootherChainTargetFlags *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->endData();
            }
        }
    };

    QList<TestChainInputFlags> inputsFlags;

    virtual void addNewFlagsInput(const SequenceName &unit, SmootherChainTargetFlags *target = NULL)
    {
        for (QList<TestChainInputFlags>::iterator it = inputsFlags.begin();
                it != inputsFlags.end();
                ++it) {
            if (it->unit == unit) {
                if (target != NULL)
                    it->targets.append(target);
                return;
            }
        }
        inputsFlags.append(TestChainInputFlags());
        inputsFlags.last().unit = unit;
        if (target != NULL)
            inputsFlags.last().targets.append(target);
    }

    virtual void addFlagsInputTarget(int index, SmootherChainTargetFlags *target)
    {
        Q_ASSERT(index >= 0 && index < inputsFlags.size());
        Q_ASSERT(target != NULL);
        inputsFlags[index].targets.append(target);
    }

    void advanceAllInputsFlagsAfter(int index, double time)
    {
        for (int i = index; i < inputsFlags.size(); i++) {
            inputsFlags[i].incomingAdvance(time);
        }
    }

    struct TestChainInputGeneral {
        SequenceName unit;
        QList<SmootherChainTargetGeneral *> targets;

        void incomingData(double start, double end, const Variant::Root &value)
        {
            for (QList<SmootherChainTargetGeneral *>::const_iterator t = targets.constBegin(),
                    endT = targets.constEnd(); t != endT; ++t) {
                (*t)->incomingData(start, end, value);
            }
        }

        void incomingAdvance(double time)
        {
            for (QList<SmootherChainTargetGeneral *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->incomingAdvance(time);
            }
        }

        void endData()
        {
            for (QList<SmootherChainTargetGeneral *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->endData();
            }
        }
    };

    QList<TestChainInputGeneral> inputsGeneral;

    virtual void addNewGeneralInput(const SequenceName &unit,
                                    SmootherChainTargetGeneral *target = NULL)
    {
        for (QList<TestChainInputGeneral>::iterator it = inputsGeneral.begin();
                it != inputsGeneral.end();
                ++it) {
            if (it->unit == unit) {
                if (target != NULL)
                    it->targets.append(target);
                return;
            }
        }
        inputsGeneral.append(TestChainInputGeneral());
        inputsGeneral.last().unit = unit;
        if (target != NULL)
            inputsGeneral.last().targets.append(target);
    }

    virtual void addGeneralInputTarget(int index, SmootherChainTargetGeneral *target)
    {
        Q_ASSERT(index >= 0 && index < inputsGeneral.size());
        Q_ASSERT(target != NULL);
        inputsGeneral[index].targets.append(target);
    }

    void advanceAllInputsGeneralAfter(int index, double time)
    {
        for (int i = index; i < inputsGeneral.size(); i++) {
            inputsGeneral[i].incomingAdvance(time);
        }
    }

    void endAll()
    {
        mux.sinkCreationComplete();

        for (QList<TestChainInput>::iterator it = inputs.begin(); it != inputs.end(); ++it) {
            it->endData();
        }
        for (QList<TestChainInputFlags>::iterator it = inputsFlags.begin();
                it != inputsFlags.end();
                ++it) {
            it->endData();
        }
    }

    TestChainEngine()
    {
        mux.start();
    }

    virtual ~TestChainEngine()
    {
        qDeleteAll(chain);
        qDeleteAll(outputs);
        qDeleteAll(outputsFlags);
        qDeleteAll(outputsGeneral);
    }


    virtual SequenceName getBaseUnit(int index = 0)
    {
        return SequenceName("bnd", "raw", "var" + std::to_string(index + 1));
    }

    virtual SinkMultiplexer::Sink *addNewFinalOutput()
    { return mux.createSink(); }

    virtual Variant::Read getOptions()
    { return Variant::Read::empty(); }

    void serialize(QDataStream &stream)
    {
        mux.setEgress(NULL);
        for (int i = chain.size() - 1; i >= 0; i--) {
            chain[i]->pause();
        }

        stream << (quint32) outputs.size();
        for (int i = 0; i < outputs.size(); i++) {
            stream << outputs.at(i)->unit << outputs.at(i)->ended;
        }
        stream << (quint32) outputsFlags.size();
        for (int i = 0; i < outputsFlags.size(); i++) {
            stream << outputsFlags.at(i)->unit << outputsFlags.at(i)->ended;
        }
        stream << (quint32) outputsGeneral.size();
        for (int i = 0; i < outputsGeneral.size(); i++) {
            stream << outputsGeneral.at(i)->unit << outputsGeneral.at(i)->ended;
        }

        stream << (quint32) inputs.size();
        for (int i = 0; i < inputs.size(); i++) {
            stream << inputs.at(i).unit;
        }
        stream << (quint32) inputsFlags.size();
        for (int i = 0; i < inputsFlags.size(); i++) {
            stream << inputsFlags.at(i).unit;
        }
        stream << (quint32) inputsGeneral.size();
        for (int i = 0; i < inputsGeneral.size(); i++) {
            stream << inputsGeneral.at(i).unit;
        }

        stream << mux;

        for (int i = 0; i < chain.size(); i++) {
            chain[i]->serialize(stream);
        }
        for (int i = 0; i < chain.size(); i++) {
            chain[i]->resume();
        }
    }

    void deserialize(QDataStream &stream)
    {
        quint32 n;
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            SequenceName u;
            stream >> u;
            bool ended;
            stream >> ended;
            SmootherChainTarget *o = addNewOutput(u);
            if (ended)
                o->endData();
        }
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            SequenceName u;
            stream >> u;
            bool ended;
            stream >> ended;
            SmootherChainTargetFlags *o = addNewFlagsOutput(u);
            if (ended)
                o->endData();
        }
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            SequenceName u;
            stream >> u;
            bool ended;
            stream >> ended;
            SmootherChainTargetGeneral *o = addNewGeneralOutput(u);
            if (ended)
                o->endData();
        }

        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            TestChainInput add;
            stream >> add.unit;
            inputs.append(add);
        }
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            TestChainInputFlags add;
            stream >> add.unit;
            inputsFlags.append(add);
        }
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            TestChainInputGeneral add;
            stream >> add.unit;
            inputsGeneral.append(add);
        }

        stream >> mux;
    }
};


enum CoreType {
    CoreFullCalculate, CoreContinuousRecalculate, CoreFinalStatistics,
};

Q_DECLARE_METATYPE(CoreType);

Q_DECLARE_METATYPE(SmootherChainCore::Type);

struct InputValue {
    int index;
    double start;
    double end;
    double value;

    InputValue() : index(-1), start(FP::undefined()), end(FP::undefined()), value(FP::undefined())
    { }

    InputValue(const InputValue &other) : index(other.index),
                                          start(other.start),
                                          end(other.end),
                                          value(other.value)
    { }

    InputValue(int i, double s, double e, double v) : index(i), start(s), end(e), value(v)
    { }
};

Q_DECLARE_METATYPE(InputValue);

Q_DECLARE_METATYPE(QList<InputValue>);


class TestEditingChain : public QObject {
Q_OBJECT

    static double vector2DMag(const QVector<double> &d,
                              const QVector<double> &m,
                              const QVector<double> &w = QVector<double>())
    {
        double x = 0.0;
        double y = 0.0;
        Q_ASSERT(d.size() == m.size() && d.size() > 0);
        double sw = 0.0;
        for (int i = 0; i < d.size(); i++) {
            double inD = (d[i] - 180.0) * 0.0174532925199433;
            double rw = 1.0;
            if (i < w.size()) rw = w[i];
            x += cos(inD) * m[i] * rw;
            y += sin(inD) * m[i] * rw;
            sw += rw;
        }
        x /= sw;
        y /= sw;
        return sqrt(x * x + y * y);
    }

    static double vector2DDir(const QVector<double> &d,
                              const QVector<double> &m,
                              const QVector<double> &w = QVector<double>())
    {
        double x = 0.0;
        double y = 0.0;
        Q_ASSERT(d.size() == m.size() && d.size() > 0);
        double sw = 0.0;
        for (int i = 0; i < d.size(); i++) {
            double inD = (d[i] - 180.0) * 0.0174532925199433;
            double rw = 1.0;
            if (i < w.size()) rw = w[i];
            x += cos(inD) * m[i] * rw;
            y += sin(inD) * m[i] * rw;
            sw += rw;
        }
        x /= sw;
        y /= sw;
        return atan2(y, x) * 57.2957795130823 + 180.0;
    }

    static double vector3DMag(const QVector<double> &az,
                              const QVector<double> &el,
                              const QVector<double> &m,
                              const QVector<double> &w = QVector<double>())
    {
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        double sw = 0.0;
        Q_ASSERT(az.size() == m.size() && el.size() == m.size() && m.size() > 0);
        for (int i = 0; i < m.size(); i++) {
            double inA = (az[i] - 180.0) * 0.0174532925199433;
            double inE = el[i] * 0.0174532925199433;
            double rw = 1.0;
            if (i < w.size()) rw = w[i];
            x += cos(inE) * cos(inA) * m[i] * rw;
            y += cos(inE) * sin(inA) * m[i] * rw;
            z += sin(inE) * m[i] * rw;
            sw += rw;
        }
        x /= sw;
        y /= sw;
        z /= sw;
        return sqrt(x * x + y * y + z * z);
    }

    static double vector3DAzi(const QVector<double> &az,
                              const QVector<double> &el,
                              const QVector<double> &m,
                              const QVector<double> &w = QVector<double>())
    {
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        double sw = 0.0;
        Q_ASSERT(az.size() == m.size() && el.size() == m.size() && m.size() > 0);
        for (int i = 0; i < m.size(); i++) {
            double inA = (az[i] - 180.0) * 0.0174532925199433;
            double inE = el[i] * 0.0174532925199433;
            double rw = 1.0;
            if (i < w.size()) rw = w[i];
            x += cos(inE) * cos(inA) * m[i] * rw;
            y += cos(inE) * sin(inA) * m[i] * rw;
            z += sin(inE) * m[i] * rw;
            sw += rw;
        }
        x /= sw;
        y /= sw;
        z /= sw;
        return atan2(y, x) * 57.2957795130823 + 180.0;
    }

    static double vector3DEle(const QVector<double> &az,
                              const QVector<double> &el,
                              const QVector<double> &m,
                              const QVector<double> &w = QVector<double>())
    {
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        double sw = 0.0;
        Q_ASSERT(az.size() == m.size() && el.size() == m.size() && m.size() > 0);
        for (int i = 0; i < m.size(); i++) {
            double inA = (az[i] - 180.0) * 0.0174532925199433;
            double inE = el[i] * 0.0174532925199433;
            double rw = 1.0;
            if (i < w.size()) rw = w[i];
            x += cos(inE) * cos(inA) * m[i] * rw;
            y += cos(inE) * sin(inA) * m[i] * rw;
            z += sin(inE) * m[i] * rw;
            sw += rw;
        }
        x /= sw;
        y /= sw;
        z /= sw;
        return asin(z / sqrt(x * x + y * y + z * z)) * 57.2957795130823;
    }

    static double beersLaw(double startL, double endL, double startI, double endI)
    {
        return (1E6 * log(startI / endI)) / (endL - startL);
    }


    static bool contains(SequenceValue::Transfer &values,
                         const SequenceValue &v,
                         bool ignoreValue = false)
    {
        for (auto i = values.begin(); i != values.end(); ++i) {
            if (!FP::equal(i->getStart(), v.getStart()))
                continue;
            if (!FP::equal(i->getEnd(), v.getEnd()))
                continue;
            if (i->getUnit() != v.getUnit())
                continue;
            if (!ignoreValue && i->getValue() != v.getValue()) {
                if (i->getValue().getType() != Variant::Type::Real ||
                        v.getValue().getType() != Variant::Type::Real ||
                        FP::defined(i->getValue().toDouble()) !=
                                FP::defined(v.getValue().toDouble()) ||
                        (FP::defined(i->getValue().toDouble()) &&
                                fabs(i->getValue().toDouble() - v.getValue().toDouble()) > 1E-6)) {
                    qDebug() << "Value comparison mismatch on" << v << ":" << i->getValue();
                    continue;
                }
            }
            values.erase(i);
            return true;
        }
        return false;
    }

private slots:

    void coreTest()
    {
        QFETCH(CoreType, coreType);
        QFETCH(SmootherChainCore::Type, engineType);
        QFETCH(int, nInputsDeclare);
        QFETCH(int, nOutputsDeclare);
        QFETCH(int, nInputsExpect);
        QFETCH(QList<InputValue>, input);
        QFETCH(SequenceValue::Transfer, expected);

        SmootherChainCore *core = NULL;
        switch (coreType) {
        case CoreFullCalculate:
            core = new EditingChainCoreFullCalculate(
                    new DynamicTimeInterval::Constant(Time::Second, 100, false),
                    new DynamicPrimitive<double>::Constant(0.1));
            break;
        case CoreContinuousRecalculate:
            core = new EditingChainCoreContinuousRecalculate(
                    new DynamicTimeInterval::Constant(Time::Second, 100, false),
                    new DynamicPrimitive<double>::Constant(0.1));
            break;
        case CoreFinalStatistics:
            core = new EditingChainCoreFinalStatistics(
                    new DynamicTimeInterval::Constant(Time::Second, 100, false),
                    new DynamicPrimitive<double>::Constant(0.1));
            break;
        }
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        for (int i = 0; i < nInputsDeclare; i++) {
            engine1->addNewInput(SequenceName("bnd", "raw", "in" + std::to_string(i + 1)));
        }
        for (int i = 0; i < nOutputsDeclare; i++) {
            engine1->addNewOutput(SequenceName("bnd", "raw", "out" + std::to_string(i + 1)));
        }
        core->createSmoother(engine1, engineType);

        QCOMPARE(engine1->inputs.size(), nInputsExpect);

        int nHalf = input.size() / 2;
        for (int i = 0; i < nHalf; i++) {
            QVERIFY(input.at(i).index >= 0);
            QVERIFY(engine1->inputs.size() > input.at(i).index);
            engine1->inputs[input.at(i).index].incomingData(input.at(i).start, input.at(i).end,
                                                            input.at(i).value);
        }

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            engine1->mux.wait(5);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core->deserializeSmoother(stream, engine2, engineType);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), nInputsExpect);

        for (int i = nHalf; i < input.size(); i++) {
            QVERIFY(input.at(i).index >= 0);
            QVERIFY(engine2->inputs.size() > input.at(i).index);
            engine2->inputs[input.at(i).index].incomingData(input.at(i).start, input.at(i).end,
                                                            input.at(i).value);
        }

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        for (const auto &check : expected) {
            if (check.getValue().getType() == Variant::Type::Real ||
                    check.getValue().getType() == Variant::Type::Integer) {
                QVERIFY(contains(values, check));
            } else {
                QVERIFY(contains(values, check, true));
            }
        }

        QVERIFY(values.empty());

        delete core;
    }

    void coreTest_data()
    {
        QTest::addColumn<CoreType>("coreType");
        QTest::addColumn<SmootherChainCore::Type>("engineType");
        QTest::addColumn<int>("nInputsDeclare");
        QTest::addColumn<int>("nOutputsDeclare");
        QTest::addColumn<int>("nInputsExpect");
        QTest::addColumn<QList<InputValue> >("input");
        QTest::addColumn<SequenceValue::Transfer>("expected");

        QTest::newRow("Full Calculate - General empty") << CoreFullCalculate
                                                        << SmootherChainCore::General << 1 << 1 << 2
                                                        << (QList<InputValue>())
                                                        << (SequenceValue::Transfer());
        QTest::newRow("Full Calculate - General") << CoreFullCalculate << SmootherChainCore::General
                                                  << 1 << 1 << 2 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, 1.0) << InputValue(1, 100.0, 150.0, 1.0)
                << InputValue(0, 150.0, 200.0, 1.5) << InputValue(1, 150.0, 200.0, 0.8)
                << InputValue(0, 300.0, 400.0, 3.0) << InputValue(1, 300.0, 400.0, 1.0)
                << InputValue(0, 400.0, 420.0, 4.0) << InputValue(1, 400.0, 420.0, 1.0)
                << InputValue(0, 480.0, 500.0, 4.5) << InputValue(1, 480.0, 500.0, 0.8)
                << InputValue(0, 500.0, 600.0, 5.0) << InputValue(1, 500.0, 600.0, 0.05))
                                                  << (SequenceValue::Transfer{SequenceValue(
                                                          SequenceName("bnd", "raw", "out1"),
                                                          Variant::Root((1.0 + 1.5 * 0.8) / 1.8),
                                                          100.0,
                                                          200.0), SequenceValue(
                                                          SequenceName("bnd", "raw", "var1",
                                                                       SequenceName::Flavors{
                                                                               "cover"}),
                                                          Variant::Root(0.9), 100.0, 200.0),
                                                                              SequenceValue(
                                                                                      SequenceName("bnd", "cont", "var1"),
                                                                                      Variant::Root(
                                                                                              (1.0 +
                                                                                                      1.5 *
                                                                                                              0.8) /
                                                                                                      1.8),
                                                                                      100.0,
                                                                                      200.0), SequenceValue(
                                                                  SequenceName("bnd", "cont", "var1",
                                                                       SequenceName::Flavors{
                                                                               "cover"}),
                                                                  Variant::Root(0.9), 100.0, 200.0),
                                                                              SequenceValue(
                                                                                      SequenceName("bnd", "raw", "out1"),
                                                                                      Variant::Root(
                                                                                              3.0),
                                                                                      300.0, 400.0),
                                                                              SequenceValue(
                                                                                      SequenceName("bnd", "cont", "var1"),
                                                                                      Variant::Root(
                                                                                              3.0),
                                                                                      300.0, 400.0),
                                                                              SequenceValue(
                                                                                      SequenceName("bnd", "raw", "out1"),
                                                                                      Variant::Root(
                                                                                              (4.0 +
                                                                                                      4.5 *
                                                                                                              0.8) /
                                                                                                      1.8),
                                                                                      400.0,
                                                                                      500.0), SequenceValue(
                                                                  SequenceName("bnd", "raw", "var1",
                                                                       SequenceName::Flavors{
                                                                               "cover"}),
                                                                  Variant::Root(0.36), 400.0,
                                                                  500.0), SequenceValue(
                                                                  SequenceName("bnd", "cont", "var1"),
                                                                  Variant::Root(4.0), 400.0, 420.0),
                                                                              SequenceValue(
                                                                                      SequenceName("bnd", "cont", "var1"),
                                                                                      Variant::Root(
                                                                                              4.5),
                                                                                      480.0, 500.0),
                                                                              SequenceValue(
                                                                                      SequenceName("bnd", "cont", "var1",
                                                                       SequenceName::Flavors{
                                                                               "cover"}),
                                                                                      Variant::Root(
                                                                                              0.8),
                                                                                      480.0, 500.0),
                                                                              SequenceValue(
                                                                                      SequenceName("bnd", "raw", "out1"),
                                                                                      Variant::Root(
                                                                                              FP::undefined()),
                                                                                      500.0, 600.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "bnd",
                                                                                              "raw",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                      Variant::Root(
                                                                                              0.05),
                                                                                      500.0, 600.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "bnd",
                                                                                              "cont",
                                                                                              "var1"),
                                                                                      Variant::Root(
                                                                                              5.0),
                                                                                      500.0, 600.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "bnd",
                                                                                              "cont",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                      Variant::Root(
                                                                                              0.05),
                                                                                      500.0,
                                                                                      600.0)});


        QTest::newRow("Full Calculate - Difference empty") << CoreFullCalculate
                                                           << SmootherChainCore::Difference << 2
                                                           << 2 << 2 << (QList<InputValue>())
                                                           << (SequenceValue::Transfer());
        QTest::newRow("Full Calculate - Difference") << CoreFullCalculate
                                                     << SmootherChainCore::Difference << 2 << 2 << 2
                                                     << (QList<InputValue>()
                                                             << InputValue(0, 100.0, 150.0, 1.0)
                                                             << InputValue(1, 100.0, 150.0, 1.5)
                                                             << InputValue(0, 150.0, 200.0, 1.6)
                                                             << InputValue(1, 150.0, 200.0, 1.8)
                                                             << InputValue(0, 300.0, 400.0, 3.0)
                                                             << InputValue(1, 300.0, 400.0, 3.1)
                                                             << InputValue(0, 400.0, 420.0, 4.0)
                                                             << InputValue(1, 400.0, 420.0, 4.1)
                                                             << InputValue(0, 480.0, 500.0, 4.5)
                                                             << InputValue(1, 480.0, 500.0, 4.6))
                                                     << (SequenceValue::Transfer{SequenceValue(
                                                             SequenceName("bnd", "raw", "out1"),
                                                             Variant::Root(1.0), 100.0, 200.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "out2"),
                                                                                         Variant::Root(
                                                                                                 1.8),
                                                                                         100.0,
                                                                                         200.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "cont",
                                                                                                 "var1"),
                                                                                         Variant::Root(
                                                                                                 1.0),
                                                                                         100.0,
                                                                                         200.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "cont",
                                                                                                 "var2"),
                                                                                         Variant::Root(
                                                                                                 1.8),
                                                                                         100.0,
                                                                                         200.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "out1"),
                                                                                         Variant::Root(
                                                                                                 3.0),
                                                                                         300.0,
                                                                                         400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "out2"),
                                                                                         Variant::Root(
                                                                                                 3.1),
                                                                                         300.0,
                                                                                         400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "cont",
                                                                                                 "var1"),
                                                                                         Variant::Root(
                                                                                                 3.0),
                                                                                         300.0,
                                                                                         400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "cont",
                                                                                                 "var2"),
                                                                                         Variant::Root(
                                                                                                 3.1),
                                                                                         300.0,
                                                                                         400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "out1"),
                                                                                         Variant::Root(
                                                                                                 4.0),
                                                                                         400.0,
                                                                                         500.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "out2"),
                                                                                         Variant::Root(
                                                                                                 4.6),
                                                                                         400.0,
                                                                                         500.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "var1",
                                                                                                 SequenceName::Flavors{
                                                                                                         "cover"}),
                                                                                         Variant::Root(
                                                                                                 0.4),
                                                                                         400.0,
                                                                                         500.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "cont",
                                                                                                 "var1"),
                                                                                         Variant::Root(
                                                                                                 4.0),
                                                                                         400.0,
                                                                                         420.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "cont",
                                                                                                 "var2"),
                                                                                         Variant::Root(
                                                                                                 4.1),
                                                                                         400.0,
                                                                                         420.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "cont",
                                                                                                 "var1"),
                                                                                         Variant::Root(
                                                                                                 4.5),
                                                                                         480.0,
                                                                                         500.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "cont",
                                                                                                 "var2"),
                                                                                         Variant::Root(
                                                                                                 4.6),
                                                                                         480.0,
                                                                                         500.0)});

        QTest::newRow("Full Calculate - Difference Initial empty") << CoreFullCalculate
                                                                   << SmootherChainCore::DifferenceInitial
                                                                   << 1 << 2 << 1
                                                                   << (QList<InputValue>())
                                                                   << (SequenceValue::Transfer());
        QTest::newRow("Full Calculate - Difference Initial") << CoreFullCalculate
                                                             << SmootherChainCore::DifferenceInitial
                                                             << 1 << 2 << 1 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, 1.0) << InputValue(0, 150.0, 200.0, 1.5)
                << InputValue(0, 300.0, 400.0, 3.0) << InputValue(0, 400.0, 420.0, 4.0)
                << InputValue(0, 480.0, 500.0, 4.5)) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(1.0), 100.0, 200.0),
                SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(1.5), 100.0, 200.0),
                SequenceValue(SequenceName("bnd", "cont", "var1"), Variant::Root(1.0), 100.0,
                              200.0),
                SequenceValue(SequenceName("bnd", "cont", "var2"), Variant::Root(1.5), 100.0,
                              200.0),
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(3.0), 300.0, 400.0),
                SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(FP::undefined()),
                              300.0,
                              400.0),
                SequenceValue(SequenceName("bnd", "cont", "var1"), Variant::Root(3.0), 300.0,
                              400.0),
                SequenceValue(SequenceName("bnd", "cont", "var2"), Variant::Root(FP::undefined()),
                              300.0,
                              400.0),
                SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(4.0), 400.0, 500.0),
                SequenceValue(SequenceName("bnd", "raw", "out2"), Variant::Root(4.5), 400.0, 500.0),
                SequenceValue(SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.4), 400.0, 500.0),
                SequenceValue(SequenceName("bnd", "cont", "var1"), Variant::Root(4.0), 400.0,
                              420.0),
                SequenceValue(SequenceName("bnd", "cont", "var2"), Variant::Root(FP::undefined()),
                              400.0,
                              420.0),
                SequenceValue(SequenceName("bnd", "cont", "var1"), Variant::Root(4.5), 480.0,
                              500.0),
                SequenceValue(SequenceName("bnd", "cont", "var2"), Variant::Root(FP::undefined()),
                              480.0,
                              500.0)});

        QTest::newRow("Full Calculate - Vector 2D empty") << CoreFullCalculate
                                                          << SmootherChainCore::Vector2D << 2 << 2
                                                          << 3 << (QList<InputValue>())
                                                          << (SequenceValue::Transfer());
        QTest::newRow("Full Calculate - Vector 2D") << CoreFullCalculate
                                                    << SmootherChainCore::Vector2D << 2 << 2 << 3
                                                    << (QList<InputValue>()
                                                            << InputValue(0, 100.0, 150.0, 10.0)
                                                            << InputValue(1, 100.0, 150.0, 1.0)
                                                            << InputValue(2, 100.0, 150.0, 1.0)
                                                            << InputValue(0, 150.0, 200.0, 20.0)
                                                            << InputValue(1, 150.0, 200.0, 0.9)
                                                            << InputValue(2, 150.0, 200.0, 0.8)
                                                            << InputValue(0, 300.0, 400.0, 30.0)
                                                            << InputValue(1, 300.0, 400.0, 0.8)
                                                            << InputValue(2, 300.0, 400.0, 1.0)
                                                            << InputValue(0, 400.0, 420.0, 40.0)
                                                            << InputValue(1, 400.0, 420.0, 1.5)
                                                            << InputValue(2, 400.0, 420.0, 0.8)
                                                            << InputValue(0, 480.0, 500.0, 45.0)
                                                            << InputValue(1, 480.0, 500.0, 2.0)
                                                            << InputValue(2, 480.0, 500.0, 1.0)
                                                            << InputValue(0, 500.0, 600.0, 90.0)
                                                            << InputValue(1, 500.0, 600.0, 4.0)
                                                            << InputValue(2, 500.0, 600.0, 0.05))
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "out1"),
                                                            Variant::Root(vector2DDir(
                                                                    QVector<double>() << 10.0
                                                                                      << 20.0,
                                                                    QVector<double>() << 1.0 << 0.9,
                                                                    QVector<double>() << 1.0
                                                                                      << 0.8)),
                                                            100.0, 200.0), SequenceValue(
                                                            SequenceName("bnd", "raw", "out2"),
                                                            Variant::Root(vector2DMag(
                                                                    QVector<double>() << 10.0
                                                                                      << 20.0,
                                                                    QVector<double>() << 1.0 << 0.9,
                                                                    QVector<double>() << 1.0
                                                                                      << 0.8)),
                                                            100.0, 200.0), SequenceValue(
                                                            SequenceName("bnd", "raw", "var2",
                                                                         SequenceName::Flavors{
                                                                                 "cover"}),
                                                            Variant::Root(0.9), 100.0, 200.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                vector2DDir(
                                                                                                QVector<double>()
                                                                                                        << 10.0
                                                                                                        << 20.0,
                                                                                                QVector<double>()
                                                                                                        << 1.0
                                                                                                        << 0.9,
                                                                                                QVector<double>()
                                                                                                        << 1.0
                                                                                                        << 0.8)),
                                                                                        100.0,
                                                                                        200.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var2"),
                                                                                        Variant::Root(
                                                                                                vector2DMag(
                                                                                                QVector<double>()
                                                                                                        << 10.0
                                                                                                        << 20.0,
                                                                                                QVector<double>()
                                                                                                        << 1.0
                                                                                                        << 0.9,
                                                                                                QVector<double>()
                                                                                                        << 1.0
                                                                                                        << 0.8)),
                                                                                        100.0,
                                                                                        200.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var2",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.9),
                                                                                        100.0,
                                                                                        200.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out1"),
                                                                                        Variant::Root(
                                                                                                30.0),
                                                                                        300.0,
                                                                                        400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out2"),
                                                                                        Variant::Root(
                                                                                                0.8),
                                                                                        300.0,
                                                                                        400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                30.0),
                                                                                        300.0,
                                                                                        400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var2"),
                                                                                        Variant::Root(
                                                                                                0.8),
                                                                                        300.0,
                                                                                        400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out1"),
                                                                                        Variant::Root(
                                                                                                vector2DDir(
                                                                                                QVector<double>()
                                                                                                        << 40.0
                                                                                                        << 45.0,
                                                                                                QVector<double>()
                                                                                                        << 1.5
                                                                                                        << 2.0,
                                                                                                QVector<double>()
                                                                                                        << 0.8
                                                                                                        << 1.0)),
                                                                                        400.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out2"),
                                                                                        Variant::Root(
                                                                                                vector2DMag(
                                                                                                QVector<double>()
                                                                                                        << 40.0
                                                                                                        << 45.0,
                                                                                                QVector<double>()
                                                                                                        << 1.5
                                                                                                        << 2.0,
                                                                                                QVector<double>()
                                                                                                        << 0.8
                                                                                                        << 1.0)),
                                                                                        400.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "var2",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.36),
                                                                                        400.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                40.0),
                                                                                        400.0,
                                                                                        420.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var2"),
                                                                                        Variant::Root(
                                                                                                1.5),
                                                                                        400.0,
                                                                                        420.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var2",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.8),
                                                                                        400.0,
                                                                                        420.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                45.0),
                                                                                        480.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var2"),
                                                                                        Variant::Root(
                                                                                                2.0),
                                                                                        480.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out1"),
                                                                                        Variant::Root(
                                                                                                FP::undefined()),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out2"),
                                                                                        Variant::Root(
                                                                                                FP::undefined()),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "var2",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.05),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                90.0),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var2"),
                                                                                        Variant::Root(
                                                                                                4.0),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var2",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.05),
                                                                                        500.0,
                                                                                        600.0)});

        QTest::newRow("Full Calculate - Vector 3D empty") << CoreFullCalculate
                                                          << SmootherChainCore::Vector3D << 3 << 3
                                                          << 4 << (QList<InputValue>())
                                                          << (SequenceValue::Transfer());
        QTest::newRow("Full Calculate - Vector 3D") << CoreFullCalculate
                                                    << SmootherChainCore::Vector3D << 3 << 3 << 4
                                                    << (QList<InputValue>()
                                                            << InputValue(0, 100.0, 150.0, 10.0)
                                                            << InputValue(1, 100.0, 150.0, 5.0)
                                                            << InputValue(2, 100.0, 150.0, 1.0)
                                                            << InputValue(3, 100.0, 150.0, 1.0)
                                                            << InputValue(0, 150.0, 200.0, 20.0)
                                                            << InputValue(1, 150.0, 200.0, 15.0)
                                                            << InputValue(2, 150.0, 200.0, 0.9)
                                                            << InputValue(3, 150.0, 200.0, 0.8)
                                                            << InputValue(0, 300.0, 400.0, 30.0)
                                                            << InputValue(1, 300.0, 400.0, 35.0)
                                                            << InputValue(2, 300.0, 400.0, 0.8)
                                                            << InputValue(3, 300.0, 400.0, 1.0)
                                                            << InputValue(0, 400.0, 420.0, 40.0)
                                                            << InputValue(1, 400.0, 420.0, 25.0)
                                                            << InputValue(2, 400.0, 420.0, 1.5)
                                                            << InputValue(3, 400.0, 420.0, 0.8)
                                                            << InputValue(0, 480.0, 500.0, 45.0)
                                                            << InputValue(1, 480.0, 500.0, 35.0)
                                                            << InputValue(2, 480.0, 500.0, 2.0)
                                                            << InputValue(3, 480.0, 500.0, 1.0)
                                                            << InputValue(0, 500.0, 600.0, 75.0)
                                                            << InputValue(1, 500.0, 600.0, 25.0)
                                                            << InputValue(2, 500.0, 600.0, 4.0)
                                                            << InputValue(3, 500.0, 600.0, 0.05))
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "out1"),
                                                            Variant::Root(vector3DAzi(
                                                                    QVector<double>() << 10.0
                                                                                      << 20.0,
                                                                    QVector<double>() << 5.0
                                                                                      << 15.0,
                                                                    QVector<double>() << 1.0 << 0.9,
                                                                    QVector<double>() << 1.0
                                                                                      << 0.8)),
                                                            100.0, 200.0), SequenceValue(
                                                            SequenceName("bnd", "raw", "out2"),
                                                            Variant::Root(vector3DEle(
                                                                    QVector<double>() << 10.0
                                                                                      << 20.0,
                                                                    QVector<double>() << 5.0
                                                                                      << 15.0,
                                                                    QVector<double>() << 1.0 << 0.9,
                                                                    QVector<double>() << 1.0
                                                                                      << 0.8)),
                                                            100.0, 200.0), SequenceValue(
                                                            SequenceName("bnd", "raw", "out3"),
                                                            Variant::Root(vector3DMag(
                                                                    QVector<double>() << 10.0
                                                                                      << 20.0,
                                                                    QVector<double>() << 5.0
                                                                                      << 15.0,
                                                                    QVector<double>() << 1.0 << 0.9,
                                                                    QVector<double>() << 1.0
                                                                                      << 0.8)),
                                                            100.0, 200.0), SequenceValue(
                                                            SequenceName("bnd", "raw", "var3",
                                                                         SequenceName::Flavors{
                                                                                 "cover"}),
                                                            Variant::Root(0.9), 100.0, 200.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                vector3DAzi(
                                                                                                QVector<double>()
                                                                                                        << 10.0
                                                                                                        << 20.0,
                                                                                                QVector<double>()
                                                                                                        << 5.0
                                                                                                        << 15.0,
                                                                                                QVector<double>()
                                                                                                        << 1.0
                                                                                                        << 0.9,
                                                                                                QVector<double>()
                                                                                                        << 1.0
                                                                                                        << 0.8)),
                                                                                        100.0,
                                                                                        200.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var2"),
                                                                                        Variant::Root(
                                                                                                vector3DEle(
                                                                                                QVector<double>()
                                                                                                        << 10.0
                                                                                                        << 20.0,
                                                                                                QVector<double>()
                                                                                                        << 5.0
                                                                                                        << 15.0,
                                                                                                QVector<double>()
                                                                                                        << 1.0
                                                                                                        << 0.9,
                                                                                                QVector<double>()
                                                                                                        << 1.0
                                                                                                        << 0.8)),
                                                                                        100.0,
                                                                                        200.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var3"),
                                                                                        Variant::Root(
                                                                                                vector3DMag(
                                                                                                QVector<double>()
                                                                                                        << 10.0
                                                                                                        << 20.0,
                                                                                                QVector<double>()
                                                                                                        << 5.0
                                                                                                        << 15.0,
                                                                                                QVector<double>()
                                                                                                        << 1.0
                                                                                                        << 0.9,
                                                                                                QVector<double>()
                                                                                                        << 1.0
                                                                                                        << 0.8)),
                                                                                        100.0,
                                                                                        200.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var3",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.9),
                                                                                        100.0,
                                                                                        200.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out1"),
                                                                                        Variant::Root(
                                                                                                30.0),
                                                                                        300.0,
                                                                                        400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out2"),
                                                                                        Variant::Root(
                                                                                                35.0),
                                                                                        300.0,
                                                                                        400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out3"),
                                                                                        Variant::Root(
                                                                                                0.8),
                                                                                        300.0,
                                                                                        400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                30.0),
                                                                                        300.0,
                                                                                        400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var2"),
                                                                                        Variant::Root(
                                                                                                35.0),
                                                                                        300.0,
                                                                                        400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var3"),
                                                                                        Variant::Root(
                                                                                                0.8),
                                                                                        300.0,
                                                                                        400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out1"),
                                                                                        Variant::Root(
                                                                                                vector3DAzi(
                                                                                                QVector<double>()
                                                                                                        << 40.0
                                                                                                        << 45.0,
                                                                                                QVector<double>()
                                                                                                        << 25.0
                                                                                                        << 35.0,
                                                                                                QVector<double>()
                                                                                                        << 1.5
                                                                                                        << 2.0,
                                                                                                QVector<double>()
                                                                                                        << 0.8
                                                                                                        << 1.0)),
                                                                                        400.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out2"),
                                                                                        Variant::Root(
                                                                                                vector3DEle(
                                                                                                QVector<double>()
                                                                                                        << 40.0
                                                                                                        << 45.0,
                                                                                                QVector<double>()
                                                                                                        << 25.0
                                                                                                        << 35.0,
                                                                                                QVector<double>()
                                                                                                        << 1.5
                                                                                                        << 2.0,
                                                                                                QVector<double>()
                                                                                                        << 0.8
                                                                                                        << 1.0)),
                                                                                        400.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out3"),
                                                                                        Variant::Root(
                                                                                                vector3DMag(
                                                                                                QVector<double>()
                                                                                                        << 40.0
                                                                                                        << 45.0,
                                                                                                QVector<double>()
                                                                                                        << 25.0
                                                                                                        << 35.0,
                                                                                                QVector<double>()
                                                                                                        << 1.5
                                                                                                        << 2.0,
                                                                                                QVector<double>()
                                                                                                        << 0.8
                                                                                                        << 1.0)),
                                                                                        400.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "var3",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.36),
                                                                                        400.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                40.0),
                                                                                        400.0,
                                                                                        420.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var2"),
                                                                                        Variant::Root(
                                                                                                25.0),
                                                                                        400.0,
                                                                                        420.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var3"),
                                                                                        Variant::Root(
                                                                                                1.5),
                                                                                        400.0,
                                                                                        420.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var3",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.8),
                                                                                        400.0,
                                                                                        420.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                45.0),
                                                                                        480.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var2"),
                                                                                        Variant::Root(
                                                                                                35.0),
                                                                                        480.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var3"),
                                                                                        Variant::Root(
                                                                                                2.0),
                                                                                        480.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out1"),
                                                                                        Variant::Root(
                                                                                                FP::undefined()),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out2"),
                                                                                        Variant::Root(
                                                                                                FP::undefined()),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out3"),
                                                                                        Variant::Root(
                                                                                                FP::undefined()),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "var3",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.05),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                75.0),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var2"),
                                                                                        Variant::Root(
                                                                                                25.0),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var3"),
                                                                                        Variant::Root(
                                                                                                4.0),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var3",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.05),
                                                                                        500.0,
                                                                                        600.0)});

        QTest::newRow("Full Calculate - Beers Law empty") << CoreFullCalculate
                                                          << SmootherChainCore::BeersLawAbsorption
                                                          << 4 << 1 << 6 << (QList<InputValue>())
                                                          << (SequenceValue::Transfer());
        QTest::newRow("Full Calculate - Beers Law") << CoreFullCalculate
                                                    << SmootherChainCore::BeersLawAbsorption << 4
                                                    << 1 << 6 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, 1.0) << InputValue(1, 100.0, 150.0, 1.1)
                << InputValue(2, 100.0, 150.0, 1.2) << InputValue(3, 100.0, 150.0, 1.3)
                << InputValue(4, 100.0, 150.0, -1.0) << InputValue(5, 100.0, 150.0, 1.0)
                << InputValue(0, 150.0, 200.0, 1.5) << InputValue(1, 150.0, 200.0, 1.6)
                << InputValue(2, 150.0, 200.0, 1.7) << InputValue(3, 150.0, 200.0, 1.8)
                << InputValue(4, 150.0, 200.0, -2.0) << InputValue(5, 150.0, 200.0, 0.8)
                << InputValue(0, 300.0, 400.0, 3.0) << InputValue(1, 300.0, 400.0, 3.1)
                << InputValue(2, 300.0, 400.0, 3.2) << InputValue(3, 300.0, 400.0, 3.3)
                << InputValue(4, 300.0, 400.0, -3.0) << InputValue(5, 300.0, 400.0, 1.0)
                << InputValue(0, 400.0, 420.0, 4.0) << InputValue(1, 400.0, 420.0, 4.1)
                << InputValue(2, 400.0, 420.0, 4.2) << InputValue(3, 400.0, 420.0, 4.3)
                << InputValue(4, 400.0, 420.0, -4.0) << InputValue(5, 400.0, 420.0, 0.8)
                << InputValue(0, 480.0, 500.0, 4.5) << InputValue(1, 480.0, 500.0, 4.6)
                << InputValue(2, 480.0, 500.0, 4.7) << InputValue(3, 480.0, 500.0, 4.8)
                << InputValue(4, 480.0, 500.0, -5.0) << InputValue(5, 480.0, 500.0, 1.0)
                << InputValue(0, 500.0, 501.0, 5.1) << InputValue(1, 500.0, 501.0, 5.2)
                << InputValue(2, 500.0, 501.0, 5.3) << InputValue(3, 500.0, 501.0, 5.4)
                << InputValue(4, 500.0, 501.0, -6.0) << InputValue(5, 500.0, 501.0, 1.0)
                << InputValue(0, 599.0, 600.0, 5.5) << InputValue(1, 599.0, 600.0, 5.6)
                << InputValue(2, 599.0, 600.0, 5.7) << InputValue(3, 599.0, 600.0, 5.8)
                << InputValue(4, 599.0, 600.0, -7.0) << InputValue(5, 599.0, 600.0, 1.0))
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "out1"),
                                                            Variant::Root(
                                                                    beersLaw(1.0, 1.6, 1.2, 1.8)),
                                                            100.0, 200.0), SequenceValue(
                                                            SequenceName("bnd", "raw", "var1",
                                                                         SequenceName::Flavors{
                                                                                 "cover"}),
                                                            Variant::Root(0.9), 100.0, 200.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                beersLaw(
                                                                                                1.0,
                                                                                                1.6,
                                                                                                1.2,
                                                                                                1.8)),
                                                                                        100.0,
                                                                                        200.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.9),
                                                                                        100.0,
                                                                                        200.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out1"),
                                                                                        Variant::Root(
                                                                                                beersLaw(
                                                                                                3.0,
                                                                                                3.1,
                                                                                                3.2,
                                                                                                3.3)),
                                                                                        300.0,
                                                                                        400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                beersLaw(
                                                                                                3.0,
                                                                                                3.1,
                                                                                                3.2,
                                                                                                3.3)),
                                                                                        300.0,
                                                                                        400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out1"),
                                                                                        Variant::Root(
                                                                                                (beersLaw(
                                                                                                4.0,
                                                                                                4.1,
                                                                                                4.2,
                                                                                                4.3) +
                                                                                                beersLaw(
                                                                                                        4.5,
                                                                                                        4.6,
                                                                                                        4.7,
                                                                                                        4.8)) /
                                                                                                      2.0),
                                                                                        400.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "var1",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.36),
                                                                                        400.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                beersLaw(
                                                                                                4.0,
                                                                                                4.1,
                                                                                                4.2,
                                                                                                4.3)),
                                                                                        400.0,
                                                                                        420.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.8),
                                                                                        400.0,
                                                                                        420.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                beersLaw(
                                                                                                4.5,
                                                                                                4.6,
                                                                                                4.7,
                                                                                                4.8)),
                                                                                        480.0,
                                                                                        500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "out1"),
                                                                                        Variant::Root(
                                                                                                FP::undefined()),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "var1",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.02),
                                                                                        500.0,
                                                                                        600.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                beersLaw(
                                                                                                5.1,
                                                                                                5.2,
                                                                                                5.3,
                                                                                                5.4)),
                                                                                        500.0,
                                                                                        501.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "cont",
                                                                                                "var1"),
                                                                                        Variant::Root(
                                                                                                beersLaw(
                                                                                                5.5,
                                                                                                5.6,
                                                                                                5.7,
                                                                                                5.8)),
                                                                                        599.0,
                                                                                        600.0)});

        QTest::newRow("Full Calculate - Beers Law Initial empty") << CoreFullCalculate
                                                                  << SmootherChainCore::BeersLawAbsorptionInitial
                                                                  << 2 << 1 << 4
                                                                  << (QList<InputValue>())
                                                                  << (SequenceValue::Transfer());
        QTest::newRow("Full Calculate - Beers Law Initial") << CoreFullCalculate
                                                            << SmootherChainCore::BeersLawAbsorptionInitial
                                                            << 2 << 1 << 4 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, 1.0) << InputValue(1, 100.0, 150.0, 1.1)
                << InputValue(2, 100.0, 150.0, -1.0) << InputValue(3, 100.0, 150.0, 1.0)
                << InputValue(0, 150.0, 200.0, 1.5) << InputValue(1, 150.0, 200.0, 1.6)
                << InputValue(2, 150.0, 200.0, -2.0) << InputValue(3, 150.0, 200.0, 0.8)
                << InputValue(0, 300.0, 400.0, 3.0) << InputValue(1, 300.0, 400.0, 3.1)
                << InputValue(2, 300.0, 400.0, -3.0) << InputValue(3, 300.0, 400.0, 1.0)
                << InputValue(0, 400.0, 410.0, 4.0) << InputValue(1, 400.0, 410.0, 4.1)
                << InputValue(2, 400.0, 410.0, -4.0) << InputValue(3, 400.0, 410.0, 0.8)
                << InputValue(0, 410.0, 420.0, 4.2) << InputValue(1, 410.0, 420.0, 4.3)
                << InputValue(2, 410.0, 420.0, -5.0) << InputValue(3, 410.0, 420.0, 0.8)
                << InputValue(0, 480.0, 490.0, 4.5) << InputValue(1, 480.0, 490.0, 4.6)
                << InputValue(2, 480.0, 490.0, -6.0) << InputValue(3, 480.0, 490.0, 1.0)
                << InputValue(0, 490.0, 500.0, 4.7) << InputValue(1, 490.0, 500.0, 4.8)
                << InputValue(2, 490.0, 500.0, -7.0) << InputValue(3, 490.0, 500.0, 1.0)
                << InputValue(0, 500.0, 501.0, 5.1) << InputValue(1, 500.0, 501.0, 5.2)
                << InputValue(2, 500.0, 501.0, -8.0) << InputValue(3, 500.0, 501.0, 1.0)
                << InputValue(0, 501.0, 502.0, 5.3) << InputValue(1, 501.0, 502.0, 5.4)
                << InputValue(2, 501.0, 502.0, -9.0) << InputValue(3, 501.0, 502.0, 1.0)
                << InputValue(0, 598.0, 599.0, 5.5) << InputValue(1, 598.0, 599.0, 5.6)
                << InputValue(2, 598.0, 599.0, -10.0) << InputValue(3, 598.0, 599.0, 1.0)
                << InputValue(0, 599.0, 600.0, 5.7) << InputValue(1, 599.0, 600.0, 5.8)
                << InputValue(2, 599.0, 600.0, -11.0) << InputValue(3, 599.0, 600.0, 1.0))
                                                            << (SequenceValue::Transfer{
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    beersLaw(1.0,
                                                                                             1.5,
                                                                                             1.1,
                                                                                             1.6)),
                                                                            100.0, 200.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "var1",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.9),
                                                                            100.0,
                                                                            200.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "cont",
                                                                                         "var1"),
                                                                            Variant::Root(
                                                                                    beersLaw(1.0,
                                                                                             1.5,
                                                                                             1.1,
                                                                                             1.6)),
                                                                            100.0, 200.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "cont",
                                                                                         "var1",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.9),
                                                                            100.0,
                                                                            200.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    FP::undefined()),
                                                                            300.0, 400.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "cont",
                                                                                         "var1"),
                                                                            Variant::Root(
                                                                                    FP::undefined()),
                                                                            300.0, 400.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    (beersLaw(4.0,
                                                                                              4.2,
                                                                                              4.1,
                                                                                              4.3) +
                                                                                    beersLaw(4.5,
                                                                                             4.7,
                                                                                             4.6,
                                                                                             4.8)) /
                                                                                          2.0),
                                                                            400.0, 500.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "var1",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.36),
                                                                            400.0,
                                                                            500.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "cont",
                                                                                         "var1"),
                                                                            Variant::Root(
                                                                                    beersLaw(4.0,
                                                                                             4.2,
                                                                                             4.1,
                                                                                             4.3)),
                                                                            400.0, 420.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "cont",
                                                                                         "var1",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.8),
                                                                            400.0,
                                                                            420.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "cont",
                                                                                         "var1"),
                                                                            Variant::Root(
                                                                                    beersLaw(4.5,
                                                                                             4.7,
                                                                                             4.6,
                                                                                             4.8)),
                                                                            480.0, 500.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    FP::undefined()),
                                                                            500.0, 600.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "var1",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.04),
                                                                            500.0,
                                                                            600.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "cont",
                                                                                         "var1"),
                                                                            Variant::Root(
                                                                                    beersLaw(5.1,
                                                                                             5.3,
                                                                                             5.2,
                                                                                             5.4)),
                                                                            500.0, 502.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "cont",
                                                                                         "var1"),
                                                                            Variant::Root(
                                                                                    beersLaw(5.5,
                                                                                             5.7,
                                                                                             5.6,
                                                                                             5.8)),
                                                                            598.0, 600.0)});


        QTest::newRow("Full Calculate - Dewpoint empty") << CoreFullCalculate
                                                         << SmootherChainCore::Dewpoint << 2 << 1
                                                         << 6 << (QList<InputValue>())
                                                         << (SequenceValue::Transfer());
        QTest::newRow("Full Calculate - Dewpoint") << CoreFullCalculate
                                                   << SmootherChainCore::Dewpoint << 2 << 1 << 6
                                                   << (QList<InputValue>()
                                                           << InputValue(0, 100.0, 150.0, 10.0)
                                                           << InputValue(1, 100.0, 150.0, 20.0)
                                                           << InputValue(2, 100.0, 150.0, -1.0)
                                                           << InputValue(3, 100.0, 150.0, 1.0)
                                                           << InputValue(4, 100.0, 150.0, 1.0)
                                                           << InputValue(5, 100.0, 150.0, 1.0)
                                                           << InputValue(0, 150.0, 200.0, 15.0)
                                                           << InputValue(1, 150.0, 200.0, 25.0)
                                                           << InputValue(2, 150.0, 200.0, -2.0)
                                                           << InputValue(3, 150.0, 200.0, 0.6)
                                                           << InputValue(4, 150.0, 200.0, 0.9)
                                                           << InputValue(5, 150.0, 200.0, 0.8)
                                                           << InputValue(0, 300.0, 400.0, 30.0)
                                                           << InputValue(1, 300.0, 400.0, 40.0)
                                                           << InputValue(2, 300.0, 400.0, -3.0)
                                                           << InputValue(3, 300.0, 400.0, 1.0)
                                                           << InputValue(4, 300.0, 400.0, 1.0)
                                                           << InputValue(5, 300.0, 400.0, 1.0)
                                                           << InputValue(0, 400.0, 420.0, 25.0)
                                                           << InputValue(1, 400.0, 420.0, 50.0)
                                                           << InputValue(2, 400.0, 420.0, -4.0)
                                                           << InputValue(3, 400.0, 420.0, 0.9)
                                                           << InputValue(4, 400.0, 420.0, 0.8)
                                                           << InputValue(5, 400.0, 420.0, 0.7)
                                                           << InputValue(0, 480.0, 500.0, 15.0)
                                                           << InputValue(1, 480.0, 500.0, 35.0)
                                                           << InputValue(2, 480.0, 500.0, -5.0)
                                                           << InputValue(3, 480.0, 500.0, 1.0)
                                                           << InputValue(4, 480.0, 500.0, 0.9)
                                                           << InputValue(5, 480.0, 500.0, 0.8)
                                                           << InputValue(0, 500.0, 600.0, 16.0)
                                                           << InputValue(1, 500.0, 600.0, 36.0)
                                                           << InputValue(2, 500.0, 600.0, -6.0)
                                                           << InputValue(3, 500.0, 600.0, 0.05)
                                                           << InputValue(4, 500.0, 600.0, 1.0)
                                                           << InputValue(5, 500.0, 600.0, 1.0)
                                                           << InputValue(0, 600.0, 700.0, 17.0)
                                                           << InputValue(1, 600.0, 700.0, 37.0)
                                                           << InputValue(2, 600.0, 700.0, -7.0)
                                                           << InputValue(3, 600.0, 700.0, 1.0)
                                                           << InputValue(4, 600.0, 700.0, 0.05)
                                                           << InputValue(5, 600.0, 700.0, 1.0)
                                                           << InputValue(0, 700.0, 800.0, 18.0)
                                                           << InputValue(1, 700.0, 800.0, 38.0)
                                                           << InputValue(2, 700.0, 800.0, -8.0)
                                                           << InputValue(3, 700.0, 800.0, 1.0)
                                                           << InputValue(4, 700.0, 800.0, 1.0)
                                                           << InputValue(5, 700.0, 800.0, 0.05))
                                                   << (SequenceValue::Transfer{SequenceValue(
                                                           SequenceName("bnd", "raw", "out1"),
                                                           Variant::Root(Dewpoint::dewpoint(
                                                                   (10.0 + 15.0 * 0.9) / 1.9,
                                                                   (20.0 + 25.0 * 0.8) / 1.8)),
                                                           100.0, 200.0), SequenceValue(
                                                           SequenceName("bnd", "raw", "var1",
                                                                        SequenceName::Flavors{
                                                                                "cover"}),
                                                           Variant::Root(0.8), 100.0, 200.0),
                                                                               SequenceValue(
                                                                                       SequenceName("bnd", "cont", "var1"),
                                                                                       Variant::Root(
                                                                                               Dewpoint::dewpoint(
                                                                   (10.0 + 15.0 * 0.9) / 1.9,
                                                                   (20.0 + 25.0 * 0.8) / 1.8)),
                                                                                       100.0, 200.0), SequenceValue(
                                                                   SequenceName("bnd", "cont", "var1",
                                                                        SequenceName::Flavors{
                                                                                "cover"}),
                                                                   Variant::Root(0.8), 100.0,
                                                                   200.0), SequenceValue(
                                                                   SequenceName("bnd", "raw", "out1"),
                                                                   Variant::Root(
                                                                           Dewpoint::dewpoint(30.0,
                                                                                              40.0)),
                                                                   300.0, 400.0), SequenceValue(
                                                                   SequenceName("bnd", "cont", "var1"),
                                                                   Variant::Root(
                                                                           Dewpoint::dewpoint(30.0,
                                                                                              40.0)),
                                                                   300.0, 400.0), SequenceValue(
                                                                   SequenceName("bnd", "raw", "out1"),
                                                                   Variant::Root(Dewpoint::dewpoint(
                                                                   (25.0 * 0.8 + 15.0 * 0.9) / 1.7,
                                                                   (50.0 * 0.7 + 35.0 * 0.8) /
                                                                           1.5)), 400.0, 500.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "raw",
                                                                                               "var1",
                                                                                               SequenceName::Flavors{
                                                                                                       "cover"}),
                                                                                       Variant::Root(
                                                                                               0.38),
                                                                                       400.0,
                                                                                       500.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "cont",
                                                                                               "var1"),
                                                                                       Variant::Root(
                                                                                               Dewpoint::dewpoint(
                                                                                               25.0,
                                                                                               50.0)),
                                                                                       400.0,
                                                                                       420.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "cont",
                                                                                               "var1",
                                                                                               SequenceName::Flavors{
                                                                                                       "cover"}),
                                                                                       Variant::Root(
                                                                                               0.9),
                                                                                       400.0,
                                                                                       420.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "cont",
                                                                                               "var1"),
                                                                                       Variant::Root(
                                                                                               Dewpoint::dewpoint(
                                                                                               15.0,
                                                                                               35.0)),
                                                                                       480.0,
                                                                                       500.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "raw",
                                                                                               "out1"),
                                                                                       Variant::Root(
                                                                                               Dewpoint::dewpoint(
                                                                                               16.0,
                                                                                               36.0)),
                                                                                       500.0,
                                                                                       600.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "raw",
                                                                                               "var1",
                                                                                               SequenceName::Flavors{
                                                                                                       "cover"}),
                                                                                       Variant::Root(
                                                                                               0.05),
                                                                                       500.0,
                                                                                       600.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "cont",
                                                                                               "var1"),
                                                                                       Variant::Root(
                                                                                               Dewpoint::dewpoint(
                                                                                               16.0,
                                                                                               36.0)),
                                                                                       500.0,
                                                                                       600.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "cont",
                                                                                               "var1",
                                                                                               SequenceName::Flavors{
                                                                                                       "cover"}),
                                                                                       Variant::Root(
                                                                                               0.05),
                                                                                       500.0,
                                                                                       600.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "raw",
                                                                                               "out1"),
                                                                                       Variant::Root(
                                                                                               FP::undefined()),
                                                                                       600.0,
                                                                                       700.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "cont",
                                                                                               "var1"),
                                                                                       Variant::Root(
                                                                                               Dewpoint::dewpoint(
                                                                                               17.0,
                                                                                               37.0)),
                                                                                       600.0,
                                                                                       700.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "raw",
                                                                                               "out1"),
                                                                                       Variant::Root(
                                                                                               FP::undefined()),
                                                                                       700.0,
                                                                                       800.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "cont",
                                                                                               "var1"),
                                                                                       Variant::Root(
                                                                                               Dewpoint::dewpoint(
                                                                                               18.0,
                                                                                               38.0)),
                                                                                       700.0,
                                                                                       800.0)});


        QTest::newRow("Full Calculate - RH empty") << CoreFullCalculate << SmootherChainCore::RH
                                                   << 2 << 1 << 6 << (QList<InputValue>())
                                                   << (SequenceValue::Transfer());
        QTest::newRow("Full Calculate - RH") << CoreFullCalculate << SmootherChainCore::RH << 2 << 1
                                             << 6 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, 10.0) << InputValue(1, 100.0, 150.0, 20.0)
                << InputValue(2, 100.0, 150.0, -1.0) << InputValue(3, 100.0, 150.0, 1.0)
                << InputValue(4, 100.0, 150.0, 1.0) << InputValue(5, 100.0, 150.0, 1.0)
                << InputValue(0, 150.0, 200.0, 15.0) << InputValue(1, 150.0, 200.0, 25.0)
                << InputValue(2, 150.0, 200.0, -2.0) << InputValue(3, 150.0, 200.0, 0.6)
                << InputValue(4, 150.0, 200.0, 0.9) << InputValue(5, 150.0, 200.0, 0.8)
                << InputValue(0, 300.0, 400.0, 30.0) << InputValue(1, 300.0, 400.0, 40.0)
                << InputValue(2, 300.0, 400.0, -3.0) << InputValue(3, 300.0, 400.0, 1.0)
                << InputValue(4, 300.0, 400.0, 1.0) << InputValue(5, 300.0, 400.0, 1.0)
                << InputValue(0, 400.0, 420.0, 25.0) << InputValue(1, 400.0, 420.0, 50.0)
                << InputValue(2, 400.0, 420.0, -4.0) << InputValue(3, 400.0, 420.0, 0.9)
                << InputValue(4, 400.0, 420.0, 0.8) << InputValue(5, 400.0, 420.0, 0.7)
                << InputValue(0, 480.0, 500.0, 15.0) << InputValue(1, 480.0, 500.0, 35.0)
                << InputValue(2, 480.0, 500.0, -5.0) << InputValue(3, 480.0, 500.0, 1.0)
                << InputValue(4, 480.0, 500.0, 0.9) << InputValue(5, 480.0, 500.0, 0.8)
                << InputValue(0, 500.0, 600.0, 16.0) << InputValue(1, 500.0, 600.0, 36.0)
                << InputValue(2, 500.0, 600.0, -6.0) << InputValue(3, 500.0, 600.0, 0.05)
                << InputValue(4, 500.0, 600.0, 1.0) << InputValue(5, 500.0, 600.0, 1.0)
                << InputValue(0, 600.0, 700.0, 17.0) << InputValue(1, 600.0, 700.0, 37.0)
                << InputValue(2, 600.0, 700.0, -7.0) << InputValue(3, 600.0, 700.0, 1.0)
                << InputValue(4, 600.0, 700.0, 0.05) << InputValue(5, 600.0, 700.0, 1.0)
                << InputValue(0, 700.0, 800.0, 18.0) << InputValue(1, 700.0, 800.0, 38.0)
                << InputValue(2, 700.0, 800.0, -8.0) << InputValue(3, 700.0, 800.0, 1.0)
                << InputValue(4, 700.0, 800.0, 1.0) << InputValue(5, 700.0, 800.0, 0.05))
                                             << (SequenceValue::Transfer{SequenceValue(
                                                     SequenceName("bnd", "raw", "out1"),
                                                     Variant::Root(
                                                             Dewpoint::rh((10.0 + 15.0 * 0.9) / 1.9,
                                                                          (20.0 + 25.0 * 0.8) / 1.8)),
                                                     100.0, 200.0), SequenceValue(
                                                     SequenceName("bnd", "raw", "var1",
                                                                  SequenceName::Flavors{"cover"}),
                                                     Variant::Root(0.8), 100.0, 200.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd", "cont", "var1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rh(
                                                                                                 (10.0 +
                                                                                                         15.0 *
                                                                                                                 0.9) /
                                                                                                         1.9,
                                                                                                 (20.0 + 25.0 * 0.8) / 1.8)),
                                                                                 100.0, 200.0), SequenceValue(
                                                             SequenceName("bnd", "cont", "var1",
                                                                  SequenceName::Flavors{"cover"}),
                                                             Variant::Root(0.8), 100.0, 200.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd", "raw", "out1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rh(
                                                                                                 30.0,
                                                                                                 40.0)),
                                                                                 300.0, 400.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "cont",
                                                                                              "var1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rh(
                                                                                         30.0,
                                                                                         40.0)),
                                                                                 300.0, 400.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rh(
                                                                                         (25.0 *
                                                                                                 0.8 +
                                                                                                 15.0 *
                                                                                                         0.9) /
                                                                                                 1.7,
                                                                                         (50.0 *
                                                                                                 0.7 +
                                                                                                 35.0 *
                                                                                                         0.8) /
                                                                                                 1.5)),
                                                                                 400.0, 500.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                 Variant::Root(
                                                                                         0.38),
                                                                                 400.0,
                                                                                 500.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "cont",
                                                                                              "var1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rh(
                                                                                         25.0,
                                                                                         50.0)),
                                                                                 400.0, 420.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "cont",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                 Variant::Root(0.9),
                                                                                 400.0,
                                                                                 420.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "cont",
                                                                                              "var1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rh(
                                                                                         15.0,
                                                                                         35.0)),
                                                                                 480.0, 500.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rh(
                                                                                         16.0,
                                                                                         36.0)),
                                                                                 500.0, 600.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                 Variant::Root(
                                                                                         0.05),
                                                                                 500.0,
                                                                                 600.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "cont",
                                                                                              "var1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rh(
                                                                                         16.0,
                                                                                         36.0)),
                                                                                 500.0, 600.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "cont",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                 Variant::Root(
                                                                                         0.05),
                                                                                 500.0,
                                                                                 600.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         FP::undefined()),
                                                                                 600.0, 700.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "cont",
                                                                                              "var1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rh(
                                                                                         17.0,
                                                                                         37.0)),
                                                                                 600.0, 700.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         FP::undefined()),
                                                                                 700.0, 800.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "cont",
                                                                                              "var1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rh(
                                                                                         18.0,
                                                                                         38.0)),
                                                                                 700.0, 800.0)});


        QTest::newRow("Full Calculate - RH Extrapolate empty") << CoreFullCalculate
                                                               << SmootherChainCore::RHExtrapolate
                                                               << 3 << 1 << 8
                                                               << (QList<InputValue>())
                                                               << (SequenceValue::Transfer());
        QTest::newRow("Full Calculate - RH Extrapolate") << CoreFullCalculate
                                                         << SmootherChainCore::RHExtrapolate << 3
                                                         << 1 << 8 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, 10.0) << InputValue(1, 100.0, 150.0, 20.0)
                << InputValue(2, 100.0, 150.0, 30.0) << InputValue(3, 100.0, 150.0, -1.0)
                << InputValue(4, 100.0, 150.0, 1.0) << InputValue(5, 100.0, 150.0, 1.0)
                << InputValue(6, 100.0, 150.0, 1.0) << InputValue(7, 100.0, 150.0, 1.0)
                << InputValue(0, 150.0, 200.0, 15.0) << InputValue(1, 150.0, 200.0, 25.0)
                << InputValue(2, 150.0, 200.0, 35.0) << InputValue(3, 150.0, 200.0, -2.0)
                << InputValue(4, 150.0, 200.0, 0.6) << InputValue(5, 150.0, 200.0, 0.9)
                << InputValue(6, 150.0, 200.0, 0.8) << InputValue(7, 150.0, 200.0, 0.7)
                << InputValue(0, 300.0, 400.0, 30.0) << InputValue(1, 300.0, 400.0, 40.0)
                << InputValue(2, 300.0, 400.0, 50.0) << InputValue(3, 300.0, 400.0, -3.0)
                << InputValue(4, 300.0, 400.0, 1.0) << InputValue(5, 300.0, 400.0, 1.0)
                << InputValue(6, 300.0, 400.0, 1.0) << InputValue(7, 300.0, 400.0, 1.0)
                << InputValue(0, 400.0, 420.0, 25.0) << InputValue(1, 400.0, 420.0, 50.0)
                << InputValue(2, 400.0, 420.0, 40.0) << InputValue(3, 400.0, 420.0, -4.0)
                << InputValue(4, 400.0, 420.0, 0.9) << InputValue(5, 400.0, 420.0, 0.8)
                << InputValue(6, 400.0, 420.0, 0.7) << InputValue(7, 400.0, 420.0, 0.6)
                << InputValue(0, 480.0, 500.0, 15.0) << InputValue(1, 480.0, 500.0, 35.0)
                << InputValue(2, 480.0, 500.0, 45.0) << InputValue(3, 480.0, 500.0, -5.0)
                << InputValue(4, 480.0, 500.0, 1.0) << InputValue(5, 480.0, 500.0, 0.9)
                << InputValue(6, 480.0, 500.0, 0.8) << InputValue(7, 480.0, 500.0, 0.7)
                << InputValue(0, 500.0, 600.0, 16.0) << InputValue(1, 500.0, 600.0, 36.0)
                << InputValue(2, 500.0, 600.0, 46.0) << InputValue(3, 500.0, 600.0, -6.0)
                << InputValue(4, 500.0, 600.0, 0.05) << InputValue(5, 500.0, 600.0, 1.0)
                << InputValue(6, 500.0, 600.0, 1.0) << InputValue(7, 500.0, 600.0, 1.0)
                << InputValue(0, 600.0, 700.0, 17.0) << InputValue(1, 600.0, 700.0, 37.0)
                << InputValue(2, 600.0, 700.0, 47.0) << InputValue(3, 600.0, 700.0, -7.0)
                << InputValue(4, 600.0, 700.0, 1.0) << InputValue(5, 600.0, 700.0, 0.05)
                << InputValue(6, 600.0, 700.0, 1.0) << InputValue(7, 600.0, 700.0, 1.0)
                << InputValue(0, 700.0, 800.0, 18.0) << InputValue(1, 700.0, 800.0, 38.0)
                << InputValue(2, 700.0, 800.0, 48.0) << InputValue(3, 700.0, 800.0, -8.0)
                << InputValue(4, 700.0, 800.0, 1.0) << InputValue(5, 700.0, 800.0, 1.0)
                << InputValue(6, 700.0, 800.0, 0.05) << InputValue(7, 700.0, 800.0, 1.0)
                << InputValue(0, 800.0, 900.0, 19.0) << InputValue(1, 800.0, 900.0, 39.0)
                << InputValue(2, 800.0, 900.0, 49.0) << InputValue(3, 800.0, 900.0, -9.0)
                << InputValue(4, 800.0, 900.0, 1.0) << InputValue(5, 800.0, 900.0, 1.0)
                << InputValue(6, 800.0, 900.0, 1.0) << InputValue(7, 800.0, 900.0, 0.05))
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("bnd", "raw", "out1"),
                                                                 Variant::Root(
                                                                         Dewpoint::rhExtrapolate(
                                                                         (10.0 + 15.0 * 0.9) / 1.9,
                                                                         (20.0 + 25.0 * 0.8) / 1.8,
                                                                         (30.0 + 35.0 * 0.7) /
                                                                                 1.7)), 100.0,
                                                                 200.0), SequenceValue(
                                                                 SequenceName("bnd", "raw", "var1",
                                                                              SequenceName::Flavors{
                                                                                      "cover"}),
                                                                 Variant::Root(0.8), 100.0, 200.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "cont",
                                                                                                     "var1"),
                                                                                             Variant::Root(
                                                                                                     Dewpoint::rhExtrapolate(
                                                                                                     (10.0 +
                                                                                                             15.0 *
                                                                                                                     0.9) /
                                                                                                             1.9,
                                                                                                     (20.0 +
                                                                                                             25.0 *
                                                                                                                     0.8) /
                                                                                                             1.8,
                                                                                                     (30.0 +
                                                                                                             35.0 *
                                                                                                                     0.7) /
                                                                                                             1.7)),
                                                                                             100.0,
                                                                                             200.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "cont",
                                                                                                     "var1",
                                                                                                     SequenceName::Flavors{
                                                                                                             "cover"}),
                                                                                             Variant::Root(
                                                                                                     0.8),
                                                                                             100.0,
                                                                                             200.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "out1"),
                                                                                             Variant::Root(
                                                                                                     Dewpoint::rhExtrapolate(
                                                                                                     30.0,
                                                                                                     40.0,
                                                                                                     50.0)),
                                                                                             300.0,
                                                                                             400.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "cont",
                                                                                                     "var1"),
                                                                                             Variant::Root(
                                                                                                     Dewpoint::rhExtrapolate(
                                                                                                     30.0,
                                                                                                     40.0,
                                                                                                     50.0)),
                                                                                             300.0,
                                                                                             400.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "out1"),
                                                                                             Variant::Root(
                                                                                                     Dewpoint::rhExtrapolate(
                                                                                                     (25.0 *
                                                                                                             0.8 +
                                                                                                             15.0 *
                                                                                                                     0.9) /
                                                                                                             1.7,
                                                                                                     (50.0 *
                                                                                                             0.7 +
                                                                                                             35.0 *
                                                                                                                     0.8) /
                                                                                                             1.5,
                                                                                                     (40.0 *
                                                                                                             0.6 +
                                                                                                             45.0 *
                                                                                                                     0.7) /
                                                                                                             1.3)),
                                                                                             400.0,
                                                                                             500.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "var1",
                                                                                                     SequenceName::Flavors{
                                                                                                             "cover"}),
                                                                                             Variant::Root(
                                                                                                     0.38),
                                                                                             400.0,
                                                                                             500.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "cont",
                                                                                                     "var1"),
                                                                                             Variant::Root(
                                                                                                     Dewpoint::rhExtrapolate(
                                                                                                     25.0,
                                                                                                     50.0,
                                                                                                     40.0)),
                                                                                             400.0,
                                                                                             420.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "cont",
                                                                                                     "var1",
                                                                                                     SequenceName::Flavors{
                                                                                                             "cover"}),
                                                                                             Variant::Root(
                                                                                                     0.9),
                                                                                             400.0,
                                                                                             420.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "cont",
                                                                                                     "var1"),
                                                                                             Variant::Root(
                                                                                                     Dewpoint::rhExtrapolate(
                                                                                                     15.0,
                                                                                                     35.0,
                                                                                                     45.0)),
                                                                                             480.0,
                                                                                             500.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "out1"),
                                                                                             Variant::Root(
                                                                                                     Dewpoint::rhExtrapolate(
                                                                                                     16.0,
                                                                                                     36.0,
                                                                                                     46.0)),
                                                                                             500.0,
                                                                                             600.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "var1",
                                                                                                     SequenceName::Flavors{
                                                                                                             "cover"}),
                                                                                             Variant::Root(
                                                                                                     0.05),
                                                                                             500.0,
                                                                                             600.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "cont",
                                                                                                     "var1"),
                                                                                             Variant::Root(
                                                                                                     Dewpoint::rhExtrapolate(
                                                                                                     16.0,
                                                                                                     36.0,
                                                                                                     46.0)),
                                                                                             500.0,
                                                                                             600.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "cont",
                                                                                                     "var1",
                                                                                                     SequenceName::Flavors{
                                                                                                             "cover"}),
                                                                                             Variant::Root(
                                                                                                     0.05),
                                                                                             500.0,
                                                                                             600.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "out1"),
                                                                                             Variant::Root(
                                                                                                     FP::undefined()),
                                                                                             600.0,
                                                                                             700.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "cont",
                                                                                                     "var1"),
                                                                                             Variant::Root(
                                                                                                     Dewpoint::rhExtrapolate(
                                                                                                     17.0,
                                                                                                     37.0,
                                                                                                     47.0)),
                                                                                             600.0,
                                                                                             700.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "out1"),
                                                                                             Variant::Root(
                                                                                                     FP::undefined()),
                                                                                             700.0,
                                                                                             800.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "cont",
                                                                                                     "var1"),
                                                                                             Variant::Root(
                                                                                                     Dewpoint::rhExtrapolate(
                                                                                                     18.0,
                                                                                                     38.0,
                                                                                                     48.0)),
                                                                                             700.0,
                                                                                             800.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "out1"),
                                                                                             Variant::Root(
                                                                                                     FP::undefined()),
                                                                                             800.0,
                                                                                             900.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "cont",
                                                                                                     "var1"),
                                                                                             Variant::Root(
                                                                                                     Dewpoint::rhExtrapolate(
                                                                                                     19.0,
                                                                                                     39.0,
                                                                                                     49.0)),
                                                                                             800.0,
                                                                                             900.0)});


        QTest::newRow("Continuous Recalculate - General empty") << CoreContinuousRecalculate
                                                                << SmootherChainCore::General << 1
                                                                << 1 << 2 << (QList<InputValue>())
                                                                << (SequenceValue::Transfer());
        QTest::newRow("Continuous Recalculate - General") << CoreContinuousRecalculate
                                                          << SmootherChainCore::General << 1 << 1
                                                          << 2 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, 1.0) << InputValue(1, 100.0, 150.0, 1.0)
                << InputValue(0, 150.0, 200.0, 1.5) << InputValue(1, 150.0, 200.0, 0.8)
                << InputValue(0, 300.0, 400.0, 3.0) << InputValue(1, 300.0, 400.0, 1.0)
                << InputValue(0, 400.0, 420.0, 4.0) << InputValue(1, 400.0, 420.0, 1.0)
                << InputValue(0, 480.0, 500.0, 4.5) << InputValue(1, 480.0, 500.0, 0.8)
                << InputValue(0, 500.0, 600.0, 5.0) << InputValue(1, 500.0, 600.0, 0.05))
                                                          << (SequenceValue::Transfer{SequenceValue(
                                                                  SequenceName("bnd", "raw",
                                                                               "out1"),
                                                                  Variant::Root(
                                                                          (1.0 + 1.5 * 0.8) / 1.8),
                                                                  100.0, 200.0), SequenceValue(
                                                                  SequenceName("bnd", "raw", "var1",
                                                                               SequenceName::Flavors{
                                                                                       "cover"}),
                                                                  Variant::Root(0.9), 100.0, 200.0),
                                                                                      SequenceValue(
                                                                                              SequenceName(
                                                                                                      "bnd",
                                                                                                      "raw",
                                                                                                      "out1"),
                                                                                              Variant::Root(
                                                                                                      3.0),
                                                                                              300.0,
                                                                                              400.0),
                                                                                      SequenceValue(
                                                                                              SequenceName(
                                                                                                      "bnd",
                                                                                                      "raw",
                                                                                                      "out1"),
                                                                                              Variant::Root(
                                                                                                      (4.0 +
                                                                                                      4.5 *
                                                                                                              0.8) /
                                                                                                            1.8),
                                                                                              400.0,
                                                                                              500.0),
                                                                                      SequenceValue(
                                                                                              SequenceName(
                                                                                                      "bnd",
                                                                                                      "raw",
                                                                                                      "var1",
                                                                                                      SequenceName::Flavors{
                                                                                                              "cover"}),
                                                                                              Variant::Root(
                                                                                                      0.36),
                                                                                              400.0,
                                                                                              500.0),
                                                                                      SequenceValue(
                                                                                              SequenceName(
                                                                                                      "bnd",
                                                                                                      "raw",
                                                                                                      "out1"),
                                                                                              Variant::Root(
                                                                                                      FP::undefined()),
                                                                                              500.0,
                                                                                              600.0),
                                                                                      SequenceValue(
                                                                                              SequenceName(
                                                                                                      "bnd",
                                                                                                      "raw",
                                                                                                      "var1",
                                                                                                      SequenceName::Flavors{
                                                                                                              "cover"}),
                                                                                              Variant::Root(
                                                                                                      0.05),
                                                                                              500.0,
                                                                                              600.0)});


        QTest::newRow("Continuous Recalculate - Difference empty") << CoreContinuousRecalculate
                                                                   << SmootherChainCore::Difference
                                                                   << 2 << 2 << 2
                                                                   << (QList<InputValue>())
                                                                   << (SequenceValue::Transfer());
        QTest::newRow("Continuous Recalculate - Difference") << CoreContinuousRecalculate
                                                             << SmootherChainCore::Difference << 2
                                                             << 2 << 2 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, 1.0) << InputValue(1, 100.0, 150.0, 1.5)
                << InputValue(0, 150.0, 200.0, 1.6) << InputValue(1, 150.0, 200.0, 1.8)
                << InputValue(0, 300.0, 400.0, 3.0) << InputValue(1, 300.0, 400.0, 3.1)
                << InputValue(0, 400.0, 420.0, 4.0) << InputValue(1, 400.0, 420.0, 4.1)
                << InputValue(0, 480.0, 500.0, 4.5) << InputValue(1, 480.0, 500.0, 4.6))
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "out1"),
                                                                             Variant::Root(1.0),
                                                                             100.0,
                                                                             200.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "out2"),
                                                                             Variant::Root(1.8),
                                                                             100.0,
                                                                             200.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "out1"),
                                                                             Variant::Root(3.0),
                                                                             300.0,
                                                                             400.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "out2"),
                                                                             Variant::Root(3.1),
                                                                             300.0,
                                                                             400.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "out1"),
                                                                             Variant::Root(4.0),
                                                                             400.0,
                                                                             500.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "out2"),
                                                                             Variant::Root(4.6),
                                                                             400.0,
                                                                             500.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "var1",
                                                                                          SequenceName::Flavors{
                                                                                                  "cover"}),
                                                                             Variant::Root(0.4),
                                                                             400.0,
                                                                             500.0)});

        QTest::newRow("Continuous Recalculate - Difference Initial empty")
                << CoreContinuousRecalculate << SmootherChainCore::DifferenceInitial << 1 << 2 << 1
                << (QList<InputValue>()) << (SequenceValue::Transfer());
        QTest::newRow("Continuous Recalculate - Difference Initial") << CoreContinuousRecalculate
                                                                     << SmootherChainCore::DifferenceInitial
                                                                     << 1 << 2 << 1
                                                                     << (QList<InputValue>()
                                                                             << InputValue(0, 100.0,
                                                                                           150.0,
                                                                                           1.0)
                                                                             << InputValue(0, 150.0,
                                                                                           200.0,
                                                                                           1.5)
                                                                             << InputValue(0, 300.0,
                                                                                           400.0,
                                                                                           3.0)
                                                                             << InputValue(0, 400.0,
                                                                                           420.0,
                                                                                           4.0)
                                                                             << InputValue(0, 480.0,
                                                                                           500.0,
                                                                                           4.5))
                                                                     << (SequenceValue::Transfer{
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "bnd",
                                                                                             "raw",
                                                                                             "out1"),
                                                                                     Variant::Root(
                                                                                             1.0),
                                                                                     100.0, 200.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "bnd",
                                                                                             "raw",
                                                                                             "out2"),
                                                                                     Variant::Root(
                                                                                             1.5),
                                                                                     100.0, 200.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "bnd",
                                                                                             "raw",
                                                                                             "out1"),
                                                                                     Variant::Root(
                                                                                             3.0),
                                                                                     300.0, 400.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "bnd",
                                                                                             "raw",
                                                                                             "out2"),
                                                                                     Variant::Root(
                                                                                             FP::undefined()),
                                                                                     300.0, 400.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "bnd",
                                                                                             "raw",
                                                                                             "out1"),
                                                                                     Variant::Root(
                                                                                             4.0),
                                                                                     400.0, 500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "bnd",
                                                                                             "raw",
                                                                                             "out2"),
                                                                                     Variant::Root(
                                                                                             4.5),
                                                                                     400.0, 500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "bnd",
                                                                                             "raw",
                                                                                             "var1",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.4),
                                                                                     400.0,
                                                                                     500.0)});

        QTest::newRow("Continuous Recalculate - Vector 2D empty") << CoreContinuousRecalculate
                                                                  << SmootherChainCore::Vector2D
                                                                  << 2 << 2 << 3
                                                                  << (QList<InputValue>())
                                                                  << (SequenceValue::Transfer());
        QTest::newRow("Continuous Recalculate - Vector 2D") << CoreContinuousRecalculate
                                                            << SmootherChainCore::Vector2D << 2 << 2
                                                            << 3 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, 10.0) << InputValue(1, 100.0, 150.0, 1.0)
                << InputValue(2, 100.0, 150.0, 1.0) << InputValue(0, 150.0, 200.0, 20.0)
                << InputValue(1, 150.0, 200.0, 0.9) << InputValue(2, 150.0, 200.0, 0.8)
                << InputValue(0, 300.0, 400.0, 30.0) << InputValue(1, 300.0, 400.0, 0.8)
                << InputValue(2, 300.0, 400.0, 1.0) << InputValue(0, 400.0, 420.0, 40.0)
                << InputValue(1, 400.0, 420.0, 1.5) << InputValue(2, 400.0, 420.0, 0.8)
                << InputValue(0, 480.0, 500.0, 45.0) << InputValue(1, 480.0, 500.0, 2.0)
                << InputValue(2, 480.0, 500.0, 1.0) << InputValue(0, 500.0, 600.0, 90.0)
                << InputValue(1, 500.0, 600.0, 4.0) << InputValue(2, 500.0, 600.0, 0.05))
                                                            << (SequenceValue::Transfer{
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    vector2DDir(
                                                                                    QVector<double>()
                                                                                            << 10.0
                                                                                            << 20.0,
                                                                                    QVector<double>()
                                                                                            << 1.0
                                                                                            << 0.9,
                                                                                    QVector<double>()
                                                                                            << 1.0
                                                                                            << 0.8)),
                                                                            100.0, 200.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out2"),
                                                                            Variant::Root(
                                                                                    vector2DMag(
                                                                                    QVector<double>()
                                                                                            << 10.0
                                                                                            << 20.0,
                                                                                    QVector<double>()
                                                                                            << 1.0
                                                                                            << 0.9,
                                                                                    QVector<double>()
                                                                                            << 1.0
                                                                                            << 0.8)),
                                                                            100.0, 200.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "var2",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.9),
                                                                            100.0,
                                                                            200.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(30.0),
                                                                            300.0,
                                                                            400.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out2"),
                                                                            Variant::Root(0.8),
                                                                            300.0,
                                                                            400.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    vector2DDir(
                                                                                    QVector<double>()
                                                                                            << 40.0
                                                                                            << 45.0,
                                                                                    QVector<double>()
                                                                                            << 1.5
                                                                                            << 2.0,
                                                                                    QVector<double>()
                                                                                            << 0.8
                                                                                            << 1.0)),
                                                                            400.0, 500.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out2"),
                                                                            Variant::Root(
                                                                                    vector2DMag(
                                                                                    QVector<double>()
                                                                                            << 40.0
                                                                                            << 45.0,
                                                                                    QVector<double>()
                                                                                            << 1.5
                                                                                            << 2.0,
                                                                                    QVector<double>()
                                                                                            << 0.8
                                                                                            << 1.0)),
                                                                            400.0, 500.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "var2",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.36),
                                                                            400.0,
                                                                            500.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    FP::undefined()),
                                                                            500.0, 600.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out2"),
                                                                            Variant::Root(
                                                                                    FP::undefined()),
                                                                            500.0, 600.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "var2",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.05),
                                                                            500.0,
                                                                            600.0)});


        QTest::newRow("Continuous Recalculate - Vector 3D empty") << CoreContinuousRecalculate
                                                                  << SmootherChainCore::Vector3D
                                                                  << 3 << 3 << 4
                                                                  << (QList<InputValue>())
                                                                  << (SequenceValue::Transfer());
        QTest::newRow("Continuous Recalculate - Vector 3D") << CoreContinuousRecalculate
                                                            << SmootherChainCore::Vector3D << 3 << 3
                                                            << 4 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, 10.0) << InputValue(1, 100.0, 150.0, 5.0)
                << InputValue(2, 100.0, 150.0, 1.0) << InputValue(3, 100.0, 150.0, 1.0)
                << InputValue(0, 150.0, 200.0, 20.0) << InputValue(1, 150.0, 200.0, 15.0)
                << InputValue(2, 150.0, 200.0, 0.9) << InputValue(3, 150.0, 200.0, 0.8)
                << InputValue(0, 300.0, 400.0, 30.0) << InputValue(1, 300.0, 400.0, 35.0)
                << InputValue(2, 300.0, 400.0, 0.8) << InputValue(3, 300.0, 400.0, 1.0)
                << InputValue(0, 400.0, 420.0, 40.0) << InputValue(1, 400.0, 420.0, 25.0)
                << InputValue(2, 400.0, 420.0, 1.5) << InputValue(3, 400.0, 420.0, 0.8)
                << InputValue(0, 480.0, 500.0, 45.0) << InputValue(1, 480.0, 500.0, 35.0)
                << InputValue(2, 480.0, 500.0, 2.0) << InputValue(3, 480.0, 500.0, 1.0)
                << InputValue(0, 500.0, 600.0, 46.0) << InputValue(1, 500.0, 600.0, 36.0)
                << InputValue(2, 500.0, 600.0, 4.0) << InputValue(3, 500.0, 600.0, 0.05))
                                                            << (SequenceValue::Transfer{
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    vector3DAzi(
                                                                                    QVector<double>()
                                                                                            << 10.0
                                                                                            << 20.0,
                                                                                    QVector<double>()
                                                                                            << 5.0
                                                                                            << 15.0,
                                                                                    QVector<double>()
                                                                                            << 1.0
                                                                                            << 0.9,
                                                                                    QVector<double>()
                                                                                            << 1.0
                                                                                            << 0.8)),
                                                                            100.0, 200.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out2"),
                                                                            Variant::Root(
                                                                                    vector3DEle(
                                                                                    QVector<double>()
                                                                                            << 10.0
                                                                                            << 20.0,
                                                                                    QVector<double>()
                                                                                            << 5.0
                                                                                            << 15.0,
                                                                                    QVector<double>()
                                                                                            << 1.0
                                                                                            << 0.9,
                                                                                    QVector<double>()
                                                                                            << 1.0
                                                                                            << 0.8)),
                                                                            100.0, 200.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out3"),
                                                                            Variant::Root(
                                                                                    vector3DMag(
                                                                                    QVector<double>()
                                                                                            << 10.0
                                                                                            << 20.0,
                                                                                    QVector<double>()
                                                                                            << 5.0
                                                                                            << 15.0,
                                                                                    QVector<double>()
                                                                                            << 1.0
                                                                                            << 0.9,
                                                                                    QVector<double>()
                                                                                            << 1.0
                                                                                            << 0.8)),
                                                                            100.0, 200.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "var3",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.9),
                                                                            100.0,
                                                                            200.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(30.0),
                                                                            300.0,
                                                                            400.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out2"),
                                                                            Variant::Root(35.0),
                                                                            300.0,
                                                                            400.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out3"),
                                                                            Variant::Root(0.8),
                                                                            300.0,
                                                                            400.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    vector3DAzi(
                                                                                    QVector<double>()
                                                                                            << 40.0
                                                                                            << 45.0,
                                                                                    QVector<double>()
                                                                                            << 25.0
                                                                                            << 35.0,
                                                                                    QVector<double>()
                                                                                            << 1.5
                                                                                            << 2.0,
                                                                                    QVector<double>()
                                                                                            << 0.8
                                                                                            << 1.0)),
                                                                            400.0, 500.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out2"),
                                                                            Variant::Root(
                                                                                    vector3DEle(
                                                                                    QVector<double>()
                                                                                            << 40.0
                                                                                            << 45.0,
                                                                                    QVector<double>()
                                                                                            << 25.0
                                                                                            << 35.0,
                                                                                    QVector<double>()
                                                                                            << 1.5
                                                                                            << 2.0,
                                                                                    QVector<double>()
                                                                                            << 0.8
                                                                                            << 1.0)),
                                                                            400.0, 500.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out3"),
                                                                            Variant::Root(
                                                                                    vector3DMag(
                                                                                    QVector<double>()
                                                                                            << 40.0
                                                                                            << 45.0,
                                                                                    QVector<double>()
                                                                                            << 25.0
                                                                                            << 35.0,
                                                                                    QVector<double>()
                                                                                            << 1.5
                                                                                            << 2.0,
                                                                                    QVector<double>()
                                                                                            << 0.8
                                                                                            << 1.0)),
                                                                            400.0, 500.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "var3",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.36),
                                                                            400.0,
                                                                            500.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    FP::undefined()),
                                                                            500.0, 600.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out2"),
                                                                            Variant::Root(
                                                                                    FP::undefined()),
                                                                            500.0, 600.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out3"),
                                                                            Variant::Root(
                                                                                    FP::undefined()),
                                                                            500.0, 600.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "var3",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.05),
                                                                            500.0,
                                                                            600.0)});

        QTest::newRow("Continuous Recalculate - Beers Law empty") << CoreContinuousRecalculate
                                                                  << SmootherChainCore::BeersLawAbsorption
                                                                  << 4 << 1 << 6
                                                                  << (QList<InputValue>())
                                                                  << (SequenceValue::Transfer());
        QTest::newRow("Continuous Recalculate - Beers Law") << CoreContinuousRecalculate
                                                            << SmootherChainCore::BeersLawAbsorption
                                                            << 4 << 1 << 6 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, 1.0) << InputValue(1, 100.0, 150.0, 1.1)
                << InputValue(2, 100.0, 150.0, 1.2) << InputValue(3, 100.0, 150.0, 1.3)
                << InputValue(4, 100.0, 150.0, -1.0) << InputValue(5, 100.0, 150.0, 1.0)
                << InputValue(0, 150.0, 200.0, 1.5) << InputValue(1, 150.0, 200.0, 1.6)
                << InputValue(2, 150.0, 200.0, 1.7) << InputValue(3, 150.0, 200.0, 1.8)
                << InputValue(4, 150.0, 200.0, -2.0) << InputValue(5, 150.0, 200.0, 0.8)
                << InputValue(0, 300.0, 400.0, 3.0) << InputValue(1, 300.0, 400.0, 3.1)
                << InputValue(2, 300.0, 400.0, 3.2) << InputValue(3, 300.0, 400.0, 3.3)
                << InputValue(4, 300.0, 400.0, -3.0) << InputValue(5, 300.0, 400.0, 1.0)
                << InputValue(0, 400.0, 420.0, 4.0) << InputValue(1, 400.0, 420.0, 4.1)
                << InputValue(2, 400.0, 420.0, 4.2) << InputValue(3, 400.0, 420.0, 4.3)
                << InputValue(4, 400.0, 420.0, -4.0) << InputValue(5, 400.0, 420.0, 0.8)
                << InputValue(0, 480.0, 500.0, 4.5) << InputValue(1, 480.0, 500.0, 4.6)
                << InputValue(2, 480.0, 500.0, 4.7) << InputValue(3, 480.0, 500.0, 4.8)
                << InputValue(4, 480.0, 500.0, -5.0) << InputValue(5, 480.0, 500.0, 1.0)
                << InputValue(0, 500.0, 501.0, 5.1) << InputValue(1, 500.0, 501.0, 5.2)
                << InputValue(2, 500.0, 501.0, 5.3) << InputValue(3, 500.0, 501.0, 5.4)
                << InputValue(4, 500.0, 501.0, -6.0) << InputValue(5, 500.0, 501.0, 1.0)
                << InputValue(0, 599.0, 600.0, 5.5) << InputValue(1, 599.0, 600.0, 5.6)
                << InputValue(2, 599.0, 600.0, 5.7) << InputValue(3, 599.0, 600.0, 5.8)
                << InputValue(4, 599.0, 600.0, -7.0) << InputValue(5, 599.0, 600.0, 1.0))
                                                            << (SequenceValue::Transfer{
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    beersLaw(1.0,
                                                                                             1.6,
                                                                                             1.2,
                                                                                             1.8)),
                                                                            100.0, 200.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "var1",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.9),
                                                                            100.0,
                                                                            200.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    beersLaw(3.0,
                                                                                             3.1,
                                                                                             3.2,
                                                                                             3.3)),
                                                                            300.0, 400.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    (beersLaw(4.0,
                                                                                              4.1,
                                                                                              4.2,
                                                                                              4.3) +
                                                                                    beersLaw(4.5,
                                                                                             4.6,
                                                                                             4.7,
                                                                                             4.8)) /
                                                                                          2.0),
                                                                            400.0, 500.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "var1",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.36),
                                                                            400.0,
                                                                            500.0), SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "out1"),
                                                                            Variant::Root(
                                                                                    FP::undefined()),
                                                                            500.0, 600.0),
                                                                    SequenceValue(
                                                                            SequenceName("bnd",
                                                                                         "raw",
                                                                                         "var1",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.02),
                                                                            500.0,
                                                                            600.0)});

        QTest::newRow("Continuous Recalculate - Beers Law Initial empty")
                << CoreContinuousRecalculate << SmootherChainCore::BeersLawAbsorptionInitial << 2
                << 1 << 4 << (QList<InputValue>()) << (SequenceValue::Transfer());
        QTest::newRow("Continuous Recalculate - Beers Law Initial") << CoreContinuousRecalculate
                                                                    << SmootherChainCore::BeersLawAbsorptionInitial
                                                                    << 2 << 1 << 4
                                                                    << (QList<InputValue>()
                                                                            << InputValue(0, 100.0,
                                                                                          150.0,
                                                                                          1.0)
                                                                            << InputValue(1, 100.0,
                                                                                          150.0,
                                                                                          1.1)
                                                                            << InputValue(2, 100.0,
                                                                                          150.0,
                                                                                          -1.0)
                                                                            << InputValue(3, 100.0,
                                                                                          150.0,
                                                                                          1.0)
                                                                            << InputValue(0, 150.0,
                                                                                          200.0,
                                                                                          1.5)
                                                                            << InputValue(1, 150.0,
                                                                                          200.0,
                                                                                          1.6)
                                                                            << InputValue(2, 150.0,
                                                                                          200.0,
                                                                                          -2.0)
                                                                            << InputValue(3, 150.0,
                                                                                          200.0,
                                                                                          0.8)
                                                                            << InputValue(0, 300.0,
                                                                                          400.0,
                                                                                          3.0)
                                                                            << InputValue(1, 300.0,
                                                                                          400.0,
                                                                                          3.1)
                                                                            << InputValue(2, 300.0,
                                                                                          400.0,
                                                                                          -3.0)
                                                                            << InputValue(3, 300.0,
                                                                                          400.0,
                                                                                          1.0)
                                                                            << InputValue(0, 400.0,
                                                                                          410.0,
                                                                                          4.0)
                                                                            << InputValue(1, 400.0,
                                                                                          410.0,
                                                                                          4.1)
                                                                            << InputValue(2, 400.0,
                                                                                          410.0,
                                                                                          -4.0)
                                                                            << InputValue(3, 400.0,
                                                                                          410.0,
                                                                                          0.8)
                                                                            << InputValue(0, 410.0,
                                                                                          420.0,
                                                                                          4.2)
                                                                            << InputValue(1, 410.0,
                                                                                          420.0,
                                                                                          4.3)
                                                                            << InputValue(2, 410.0,
                                                                                          420.0,
                                                                                          -5.0)
                                                                            << InputValue(3, 410.0,
                                                                                          420.0,
                                                                                          0.8)
                                                                            << InputValue(0, 480.0,
                                                                                          490.0,
                                                                                          4.5)
                                                                            << InputValue(1, 480.0,
                                                                                          490.0,
                                                                                          4.6)
                                                                            << InputValue(2, 480.0,
                                                                                          490.0,
                                                                                          -6.0)
                                                                            << InputValue(3, 480.0,
                                                                                          490.0,
                                                                                          1.0)
                                                                            << InputValue(0, 490.0,
                                                                                          500.0,
                                                                                          4.7)
                                                                            << InputValue(1, 490.0,
                                                                                          500.0,
                                                                                          4.8)
                                                                            << InputValue(2, 490.0,
                                                                                          500.0,
                                                                                          -7.0)
                                                                            << InputValue(3, 490.0,
                                                                                          500.0,
                                                                                          1.0)
                                                                            << InputValue(0, 500.0,
                                                                                          501.0,
                                                                                          5.1)
                                                                            << InputValue(1, 500.0,
                                                                                          501.0,
                                                                                          5.2)
                                                                            << InputValue(2, 500.0,
                                                                                          501.0,
                                                                                          -8.0)
                                                                            << InputValue(3, 500.0,
                                                                                          501.0,
                                                                                          1.0)
                                                                            << InputValue(0, 501.0,
                                                                                          502.0,
                                                                                          5.3)
                                                                            << InputValue(1, 501.0,
                                                                                          502.0,
                                                                                          5.4)
                                                                            << InputValue(2, 501.0,
                                                                                          502.0,
                                                                                          -9.0)
                                                                            << InputValue(3, 501.0,
                                                                                          502.0,
                                                                                          1.0)
                                                                            << InputValue(0, 598.0,
                                                                                          599.0,
                                                                                          5.5)
                                                                            << InputValue(1, 598.0,
                                                                                          599.0,
                                                                                          5.6)
                                                                            << InputValue(2, 598.0,
                                                                                          599.0,
                                                                                          -9.0)
                                                                            << InputValue(3, 598.0,
                                                                                          599.0,
                                                                                          1.0)
                                                                            << InputValue(0, 599.0,
                                                                                          600.0,
                                                                                          5.7)
                                                                            << InputValue(1, 599.0,
                                                                                          600.0,
                                                                                          5.8)
                                                                            << InputValue(2, 599.0,
                                                                                          600.0,
                                                                                          -10.0)
                                                                            << InputValue(3, 599.0,
                                                                                          600.0,
                                                                                          1.0))
                                                                    << (SequenceValue::Transfer{
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "out1"),
                                                                                    Variant::Root(
                                                                                            beersLaw(
                                                                                            1.0,
                                                                                            1.5,
                                                                                            1.1,
                                                                                            1.6)),
                                                                                    100.0, 200.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "var1",
                                                                                            SequenceName::Flavors{
                                                                                                    "cover"}),
                                                                                    Variant::Root(
                                                                                            0.9),
                                                                                    100.0, 200.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "out1"),
                                                                                    Variant::Root(
                                                                                            FP::undefined()),
                                                                                    300.0, 400.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "out1"),
                                                                                    Variant::Root(
                                                                                            (beersLaw(
                                                                                            4.0,
                                                                                            4.2,
                                                                                            4.1,
                                                                                            4.3) +
                                                                                            beersLaw(
                                                                                                    4.5,
                                                                                                    4.7,
                                                                                                    4.6,
                                                                                                    4.8)) /
                                                                                                  2.0),
                                                                                    400.0, 500.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "var1",
                                                                                            SequenceName::Flavors{
                                                                                                    "cover"}),
                                                                                    Variant::Root(
                                                                                            0.36),
                                                                                    400.0, 500.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "out1"),
                                                                                    Variant::Root(
                                                                                            FP::undefined()),
                                                                                    500.0, 600.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "var1",
                                                                                            SequenceName::Flavors{
                                                                                                    "cover"}),
                                                                                    Variant::Root(
                                                                                            0.04),
                                                                                    500.0, 600.0)});


        QTest::newRow("Continuous Recalculate - Dewpoint empty") << CoreContinuousRecalculate
                                                                 << SmootherChainCore::Dewpoint << 2
                                                                 << 1 << 6 << (QList<InputValue>())
                                                                 << (SequenceValue::Transfer());
        QTest::newRow("Continuous Recalculate - Dewpoint") << CoreContinuousRecalculate
                                                           << SmootherChainCore::Dewpoint << 2 << 1
                                                           << 6 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, 10.0) << InputValue(1, 100.0, 150.0, 20.0)
                << InputValue(2, 100.0, 150.0, -1.0) << InputValue(3, 100.0, 150.0, 1.0)
                << InputValue(4, 100.0, 150.0, 1.0) << InputValue(5, 100.0, 150.0, 1.0)
                << InputValue(0, 150.0, 200.0, 15.0) << InputValue(1, 150.0, 200.0, 25.0)
                << InputValue(2, 150.0, 200.0, -2.0) << InputValue(3, 150.0, 200.0, 0.6)
                << InputValue(4, 150.0, 200.0, 0.9) << InputValue(5, 150.0, 200.0, 0.8)
                << InputValue(0, 300.0, 400.0, 30.0) << InputValue(1, 300.0, 400.0, 40.0)
                << InputValue(2, 300.0, 400.0, -3.0) << InputValue(3, 300.0, 400.0, 1.0)
                << InputValue(4, 300.0, 400.0, 1.0) << InputValue(5, 300.0, 400.0, 1.0)
                << InputValue(0, 400.0, 420.0, 25.0) << InputValue(1, 400.0, 420.0, 50.0)
                << InputValue(2, 400.0, 420.0, -4.0) << InputValue(3, 400.0, 420.0, 0.9)
                << InputValue(4, 400.0, 420.0, 0.8) << InputValue(5, 400.0, 420.0, 0.7)
                << InputValue(0, 480.0, 500.0, 15.0) << InputValue(1, 480.0, 500.0, 35.0)
                << InputValue(2, 480.0, 500.0, -5.0) << InputValue(3, 480.0, 500.0, 1.0)
                << InputValue(4, 480.0, 500.0, 0.9) << InputValue(5, 480.0, 500.0, 0.8)
                << InputValue(0, 500.0, 600.0, 16.0) << InputValue(1, 500.0, 600.0, 36.0)
                << InputValue(2, 500.0, 600.0, -6.0) << InputValue(3, 500.0, 600.0, 0.05)
                << InputValue(4, 500.0, 600.0, 1.0) << InputValue(5, 500.0, 600.0, 1.0)
                << InputValue(0, 600.0, 700.0, 17.0) << InputValue(1, 600.0, 700.0, 37.0)
                << InputValue(2, 600.0, 700.0, -7.0) << InputValue(3, 600.0, 700.0, 1.0)
                << InputValue(4, 600.0, 700.0, 0.05) << InputValue(5, 600.0, 700.0, 1.0)
                << InputValue(0, 700.0, 800.0, 18.0) << InputValue(1, 700.0, 800.0, 38.0)
                << InputValue(2, 700.0, 800.0, -8.0) << InputValue(3, 700.0, 800.0, 1.0)
                << InputValue(4, 700.0, 800.0, 1.0) << InputValue(5, 700.0, 800.0, 0.05))
                                                           << (SequenceValue::Transfer{
                                                                   SequenceValue(SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::dewpoint(
                                                                                         (10.0 +
                                                                                                 15.0 *
                                                                                                         0.9) /
                                                                                                 1.9,
                                                                                         (20.0 +
                                                                                                 25.0 *
                                                                                                         0.8) /
                                                                                                 1.8)),
                                                                                 100.0, 200.0),
                                                                   SequenceValue(SequenceName("bnd",
                                                                                              "raw",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                 Variant::Root(0.8),
                                                                                 100.0,
                                                                                 200.0),
                                                                   SequenceValue(SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::dewpoint(
                                                                                         30.0,
                                                                                         40.0)),
                                                                                 300.0, 400.0),
                                                                   SequenceValue(SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::dewpoint(
                                                                                         (25.0 *
                                                                                                 0.8 +
                                                                                                 15.0 *
                                                                                                         0.9) /
                                                                                                 1.7,
                                                                                         (50.0 *
                                                                                                 0.7 +
                                                                                                 35.0 *
                                                                                                         0.8) /
                                                                                                 1.5)),
                                                                                 400.0, 500.0),
                                                                   SequenceValue(SequenceName("bnd",
                                                                                              "raw",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                 Variant::Root(
                                                                                         0.38),
                                                                                 400.0,
                                                                                 500.0),
                                                                   SequenceValue(SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::dewpoint(
                                                                                         16.0,
                                                                                         36.0)),
                                                                                 500.0, 600.0),
                                                                   SequenceValue(SequenceName("bnd",
                                                                                              "raw",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                 Variant::Root(
                                                                                         0.05),
                                                                                 500.0,
                                                                                 600.0),
                                                                   SequenceValue(SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         FP::undefined()),
                                                                                 600.0, 700.0),
                                                                   SequenceValue(SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         FP::undefined()),
                                                                                 700.0, 800.0)});


        QTest::newRow("Continuous Recalculate - RH empty") << CoreContinuousRecalculate
                                                           << SmootherChainCore::RH << 2 << 1 << 6
                                                           << (QList<InputValue>())
                                                           << (SequenceValue::Transfer());
        QTest::newRow("Continuous Recalculate - RH") << CoreContinuousRecalculate
                                                     << SmootherChainCore::RH << 2 << 1 << 6
                                                     << (QList<InputValue>()
                                                             << InputValue(0, 100.0, 150.0, 10.0)
                                                             << InputValue(1, 100.0, 150.0, 20.0)
                                                             << InputValue(2, 100.0, 150.0, -1.0)
                                                             << InputValue(3, 100.0, 150.0, 1.0)
                                                             << InputValue(4, 100.0, 150.0, 1.0)
                                                             << InputValue(5, 100.0, 150.0, 1.0)
                                                             << InputValue(0, 150.0, 200.0, 15.0)
                                                             << InputValue(1, 150.0, 200.0, 25.0)
                                                             << InputValue(2, 150.0, 200.0, -2.0)
                                                             << InputValue(3, 150.0, 200.0, 0.6)
                                                             << InputValue(4, 150.0, 200.0, 0.9)
                                                             << InputValue(5, 150.0, 200.0, 0.8)
                                                             << InputValue(0, 300.0, 400.0, 30.0)
                                                             << InputValue(1, 300.0, 400.0, 40.0)
                                                             << InputValue(2, 300.0, 400.0, -3.0)
                                                             << InputValue(3, 300.0, 400.0, 1.0)
                                                             << InputValue(4, 300.0, 400.0, 1.0)
                                                             << InputValue(5, 300.0, 400.0, 1.0)
                                                             << InputValue(0, 400.0, 420.0, 25.0)
                                                             << InputValue(1, 400.0, 420.0, 50.0)
                                                             << InputValue(2, 400.0, 420.0, -4.0)
                                                             << InputValue(3, 400.0, 420.0, 0.9)
                                                             << InputValue(4, 400.0, 420.0, 0.8)
                                                             << InputValue(5, 400.0, 420.0, 0.7)
                                                             << InputValue(0, 480.0, 500.0, 15.0)
                                                             << InputValue(1, 480.0, 500.0, 35.0)
                                                             << InputValue(2, 480.0, 500.0, -5.0)
                                                             << InputValue(3, 480.0, 500.0, 1.0)
                                                             << InputValue(4, 480.0, 500.0, 0.9)
                                                             << InputValue(5, 480.0, 500.0, 0.8)
                                                             << InputValue(0, 500.0, 600.0, 16.0)
                                                             << InputValue(1, 500.0, 600.0, 36.0)
                                                             << InputValue(2, 500.0, 600.0, -6.0)
                                                             << InputValue(3, 500.0, 600.0, 0.05)
                                                             << InputValue(4, 500.0, 600.0, 1.0)
                                                             << InputValue(5, 500.0, 600.0, 1.0)
                                                             << InputValue(0, 600.0, 700.0, 17.0)
                                                             << InputValue(1, 600.0, 700.0, 37.0)
                                                             << InputValue(2, 600.0, 700.0, -7.0)
                                                             << InputValue(3, 600.0, 700.0, 1.0)
                                                             << InputValue(4, 600.0, 700.0, 0.05)
                                                             << InputValue(5, 600.0, 700.0, 1.0)
                                                             << InputValue(0, 700.0, 800.0, 18.0)
                                                             << InputValue(1, 700.0, 800.0, 38.0)
                                                             << InputValue(2, 700.0, 800.0, -8.0)
                                                             << InputValue(3, 700.0, 800.0, 1.0)
                                                             << InputValue(4, 700.0, 800.0, 1.0)
                                                             << InputValue(5, 700.0, 800.0, 0.05))
                                                     << (SequenceValue::Transfer{SequenceValue(
                                                             SequenceName("bnd", "raw", "out1"),
                                                             Variant::Root(Dewpoint::rh(
                                                                     (10.0 + 15.0 * 0.9) / 1.9,
                                                                     (20.0 + 25.0 * 0.8) / 1.8)),
                                                             100.0, 200.0), SequenceValue(
                                                             SequenceName("bnd", "raw", "var1",
                                                                          SequenceName::Flavors{
                                                                                  "cover"}),
                                                             Variant::Root(0.8), 100.0, 200.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "out1"),
                                                                                         Variant::Root(
                                                                                                 Dewpoint::rh(
                                                                                                 30.0,
                                                                                                 40.0)),
                                                                                         300.0,
                                                                                         400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "out1"),
                                                                                         Variant::Root(
                                                                                                 Dewpoint::rh(
                                                                                                 (25.0 *
                                                                                                         0.8 +
                                                                                                         15.0 *
                                                                                                                 0.9) /
                                                                                                         1.7,
                                                                                                 (50.0 *
                                                                                                         0.7 +
                                                                                                         35.0 *
                                                                                                                 0.8) /
                                                                                                         1.5)),
                                                                                         400.0,
                                                                                         500.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "var1",
                                                                                                 SequenceName::Flavors{
                                                                                                         "cover"}),
                                                                                         Variant::Root(
                                                                                                 0.38),
                                                                                         400.0,
                                                                                         500.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "out1"),
                                                                                         Variant::Root(
                                                                                                 Dewpoint::rh(
                                                                                                 16.0,
                                                                                                 36.0)),
                                                                                         500.0,
                                                                                         600.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "var1",
                                                                                                 SequenceName::Flavors{
                                                                                                         "cover"}),
                                                                                         Variant::Root(
                                                                                                 0.05),
                                                                                         500.0,
                                                                                         600.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "out1"),
                                                                                         Variant::Root(
                                                                                                 FP::undefined()),
                                                                                         600.0,
                                                                                         700.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "out1"),
                                                                                         Variant::Root(
                                                                                                 FP::undefined()),
                                                                                         700.0,
                                                                                         800.0)});


        QTest::newRow("Continuous Recalculate - RH Extrapolate empty") << CoreContinuousRecalculate
                                                                       << SmootherChainCore::RHExtrapolate
                                                                       << 3 << 1 << 8
                                                                       << (QList<InputValue>())
                                                                       << (SequenceValue::Transfer());
        QTest::newRow("Continuous Recalculate - RH Extrapolate") << CoreContinuousRecalculate
                                                                 << SmootherChainCore::RHExtrapolate
                                                                 << 3 << 1 << 8
                                                                 << (QList<InputValue>()
                                                                         << InputValue(0, 100.0,
                                                                                       150.0, 10.0)
                                                                         << InputValue(1, 100.0,
                                                                                       150.0, 20.0)
                                                                         << InputValue(2, 100.0,
                                                                                       150.0, 30.0)
                                                                         << InputValue(3, 100.0,
                                                                                       150.0, -1.0)
                                                                         << InputValue(4, 100.0,
                                                                                       150.0, 1.0)
                                                                         << InputValue(5, 100.0,
                                                                                       150.0, 1.0)
                                                                         << InputValue(6, 100.0,
                                                                                       150.0, 1.0)
                                                                         << InputValue(7, 100.0,
                                                                                       150.0, 1.0)
                                                                         << InputValue(0, 150.0,
                                                                                       200.0, 15.0)
                                                                         << InputValue(1, 150.0,
                                                                                       200.0, 25.0)
                                                                         << InputValue(2, 150.0,
                                                                                       200.0, 35.0)
                                                                         << InputValue(3, 150.0,
                                                                                       200.0, -2.0)
                                                                         << InputValue(4, 150.0,
                                                                                       200.0, 0.6)
                                                                         << InputValue(5, 150.0,
                                                                                       200.0, 0.9)
                                                                         << InputValue(6, 150.0,
                                                                                       200.0, 0.8)
                                                                         << InputValue(7, 150.0,
                                                                                       200.0, 0.7)
                                                                         << InputValue(0, 300.0,
                                                                                       400.0, 30.0)
                                                                         << InputValue(1, 300.0,
                                                                                       400.0, 40.0)
                                                                         << InputValue(2, 300.0,
                                                                                       400.0, 50.0)
                                                                         << InputValue(3, 300.0,
                                                                                       400.0, -3.0)
                                                                         << InputValue(4, 300.0,
                                                                                       400.0, 1.0)
                                                                         << InputValue(5, 300.0,
                                                                                       400.0, 1.0)
                                                                         << InputValue(6, 300.0,
                                                                                       400.0, 1.0)
                                                                         << InputValue(7, 300.0,
                                                                                       400.0, 1.0)
                                                                         << InputValue(0, 400.0,
                                                                                       420.0, 25.0)
                                                                         << InputValue(1, 400.0,
                                                                                       420.0, 50.0)
                                                                         << InputValue(2, 400.0,
                                                                                       420.0, 40.0)
                                                                         << InputValue(3, 400.0,
                                                                                       420.0, -4.0)
                                                                         << InputValue(4, 400.0,
                                                                                       420.0, 0.9)
                                                                         << InputValue(5, 400.0,
                                                                                       420.0, 0.8)
                                                                         << InputValue(6, 400.0,
                                                                                       420.0, 0.7)
                                                                         << InputValue(7, 400.0,
                                                                                       420.0, 0.6)
                                                                         << InputValue(0, 480.0,
                                                                                       500.0, 15.0)
                                                                         << InputValue(1, 480.0,
                                                                                       500.0, 35.0)
                                                                         << InputValue(2, 480.0,
                                                                                       500.0, 45.0)
                                                                         << InputValue(3, 480.0,
                                                                                       500.0, -5.0)
                                                                         << InputValue(4, 480.0,
                                                                                       500.0, 1.0)
                                                                         << InputValue(5, 480.0,
                                                                                       500.0, 0.9)
                                                                         << InputValue(6, 480.0,
                                                                                       500.0, 0.8)
                                                                         << InputValue(7, 480.0,
                                                                                       500.0, 0.7)
                                                                         << InputValue(0, 500.0,
                                                                                       600.0, 16.0)
                                                                         << InputValue(1, 500.0,
                                                                                       600.0, 36.0)
                                                                         << InputValue(2, 500.0,
                                                                                       600.0, 46.0)
                                                                         << InputValue(3, 500.0,
                                                                                       600.0, -6.0)
                                                                         << InputValue(4, 500.0,
                                                                                       600.0, 0.05)
                                                                         << InputValue(5, 500.0,
                                                                                       600.0, 1.0)
                                                                         << InputValue(6, 500.0,
                                                                                       600.0, 1.0)
                                                                         << InputValue(7, 500.0,
                                                                                       600.0, 1.0)
                                                                         << InputValue(0, 600.0,
                                                                                       700.0, 17.0)
                                                                         << InputValue(1, 600.0,
                                                                                       700.0, 37.0)
                                                                         << InputValue(2, 600.0,
                                                                                       700.0, 47.0)
                                                                         << InputValue(3, 600.0,
                                                                                       700.0, -7.0)
                                                                         << InputValue(4, 600.0,
                                                                                       700.0, 1.0)
                                                                         << InputValue(5, 600.0,
                                                                                       700.0, 0.05)
                                                                         << InputValue(6, 600.0,
                                                                                       700.0, 1.0)
                                                                         << InputValue(7, 600.0,
                                                                                       700.0, 1.0)
                                                                         << InputValue(0, 700.0,
                                                                                       800.0, 18.0)
                                                                         << InputValue(1, 700.0,
                                                                                       800.0, 38.0)
                                                                         << InputValue(2, 700.0,
                                                                                       800.0, 48.0)
                                                                         << InputValue(3, 700.0,
                                                                                       800.0, -8.0)
                                                                         << InputValue(4, 700.0,
                                                                                       800.0, 1.0)
                                                                         << InputValue(5, 700.0,
                                                                                       800.0, 1.0)
                                                                         << InputValue(6, 700.0,
                                                                                       800.0, 0.05)
                                                                         << InputValue(7, 700.0,
                                                                                       800.0, 1.0)
                                                                         << InputValue(0, 800.0,
                                                                                       900.0, 19.0)
                                                                         << InputValue(1, 800.0,
                                                                                       900.0, 39.0)
                                                                         << InputValue(2, 800.0,
                                                                                       900.0, 49.0)
                                                                         << InputValue(3, 800.0,
                                                                                       900.0, -9.0)
                                                                         << InputValue(4, 800.0,
                                                                                       900.0, 1.0)
                                                                         << InputValue(5, 800.0,
                                                                                       900.0, 1.0)
                                                                         << InputValue(6, 800.0,
                                                                                       900.0, 1.0)
                                                                         << InputValue(7, 800.0,
                                                                                       900.0, 0.05))
                                                                 << (SequenceValue::Transfer{
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rhExtrapolate(
                                                                                         (10.0 +
                                                                                                 15.0 *
                                                                                                         0.9) /
                                                                                                 1.9,
                                                                                         (20.0 +
                                                                                                 25.0 *
                                                                                                         0.8) /
                                                                                                 1.8,
                                                                                         (30.0 +
                                                                                                 35.0 *
                                                                                                         0.7) /
                                                                                                 1.7)),
                                                                                 100.0, 200.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                 Variant::Root(0.8),
                                                                                 100.0,
                                                                                 200.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rhExtrapolate(
                                                                                         30.0, 40.0,
                                                                                         50.0)),
                                                                                 300.0, 400.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rhExtrapolate(
                                                                                         (25.0 *
                                                                                                 0.8 +
                                                                                                 15.0 *
                                                                                                         0.9) /
                                                                                                 1.7,
                                                                                         (50.0 *
                                                                                                 0.7 +
                                                                                                 35.0 *
                                                                                                         0.8) /
                                                                                                 1.5,
                                                                                         (40.0 *
                                                                                                 0.6 +
                                                                                                 45.0 *
                                                                                                         0.7) /
                                                                                                 1.3)),
                                                                                 400.0, 500.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                 Variant::Root(
                                                                                         0.38),
                                                                                 400.0,
                                                                                 500.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         Dewpoint::rhExtrapolate(
                                                                                         16.0, 36.0,
                                                                                         46.0)),
                                                                                 500.0, 600.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                 Variant::Root(
                                                                                         0.05),
                                                                                 500.0,
                                                                                 600.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         FP::undefined()),
                                                                                 600.0, 700.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         FP::undefined()),
                                                                                 700.0, 800.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "out1"),
                                                                                 Variant::Root(
                                                                                         FP::undefined()),
                                                                                 800.0, 900.0)});


        QTest::newRow("Final Statistics - General empty") << CoreFinalStatistics
                                                          << SmootherChainCore::General << 1 << 1
                                                          << 2 << (QList<InputValue>())
                                                          << (SequenceValue::Transfer());
        QTest::newRow("Final Statistics - General") << CoreFinalStatistics
                                                    << SmootherChainCore::General << 1 << 1 << 2
                                                    << (QList<InputValue>()
                                                            << InputValue(0, 100.0, 150.0, 1.0)
                                                            << InputValue(1, 100.0, 150.0, 1.0)
                                                            << InputValue(0, 150.0, 200.0, 1.5)
                                                            << InputValue(1, 150.0, 200.0, 0.8)
                                                            << InputValue(0, 300.0, 400.0, 3.0)
                                                            << InputValue(1, 300.0, 400.0, 1.0)
                                                            << InputValue(0, 400.0, 420.0, 4.0)
                                                            << InputValue(1, 400.0, 420.0, 1.0)
                                                            << InputValue(0, 480.0, 500.0, 4.5)
                                                            << InputValue(1, 480.0, 500.0, 0.8))
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "var1",
                                                                         SequenceName::Flavors{
                                                                                 "stats"}),
                                                            Variant::Root(),
                                                            100.0, 200.0), SequenceValue(
                                                            SequenceName("bnd", "raw", "var1",
                                                                         SequenceName::Flavors{
                                                                                 "stats"}),
                                                            Variant::Root(),
                                                            300.0, 400.0), SequenceValue(
                                                            SequenceName("bnd", "raw", "var1",
                                                                         SequenceName::Flavors{
                                                                                 "stats"}),
                                                            Variant::Root(),
                                                            400.0, 500.0)});

        QTest::newRow("Final Statistics - Difference empty") << CoreFinalStatistics
                                                             << SmootherChainCore::Difference << 2
                                                             << 2 << 2 << (QList<InputValue>())
                                                             << (SequenceValue::Transfer());
        QTest::newRow("Final Statistics - Difference") << CoreFinalStatistics
                                                       << SmootherChainCore::Difference << 2 << 2
                                                       << 2 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, 10.0) << InputValue(1, 100.0, 150.0, 10.1)
                << InputValue(0, 150.0, 200.0, 15.0) << InputValue(1, 150.0, 200.0, 15.1)
                << InputValue(0, 300.0, 400.0, 30.0) << InputValue(1, 300.0, 400.0, 30.1)
                << InputValue(0, 400.0, 420.0, 40.0) << InputValue(1, 400.0, 420.0, 40.1)
                << InputValue(0, 480.0, 500.0, 48.0) << InputValue(1, 480.0, 500.0, 48.1))
                                                       << (SequenceValue::Transfer());

        QTest::newRow("Final Statistics - Difference Initial empty") << CoreFinalStatistics
                                                                     << SmootherChainCore::DifferenceInitial
                                                                     << 1 << 2 << 1
                                                                     << (QList<InputValue>())
                                                                     << (SequenceValue::Transfer());
        QTest::newRow("Final Statistics - Difference Initial") << CoreFinalStatistics
                                                               << SmootherChainCore::DifferenceInitial
                                                               << 1 << 2 << 1
                                                               << (QList<InputValue>()
                                                                       << InputValue(0, 100.0,
                                                                                     150.0, 10.0)
                                                                       << InputValue(0, 150.0,
                                                                                     200.0, 15.0)
                                                                       << InputValue(0, 300.0,
                                                                                     400.0, 30.0)
                                                                       << InputValue(0, 400.0,
                                                                                     420.0, 40.0)
                                                                       << InputValue(0, 480.0,
                                                                                     500.0, 48.0))
                                                               << (SequenceValue::Transfer());

        QTest::newRow("Final Statistics - Vector 2D empty") << CoreFinalStatistics
                                                            << SmootherChainCore::Vector2D << 2 << 2
                                                            << 3 << (QList<InputValue>())
                                                            << (SequenceValue::Transfer());
        QTest::newRow("Final Statistics - Vector 2D") << CoreFinalStatistics
                                                      << SmootherChainCore::Vector2D << 2 << 2 << 3
                                                      << (QList<InputValue>()
                                                              << InputValue(2, 100.0, 200.0, 1.0)
                                                              << InputValue(0, 100.0, 150.0, 10.0)
                                                              << InputValue(1, 100.0, 150.0, 5.0)
                                                              << InputValue(0, 150.0, 200.0, 15.0)
                                                              << InputValue(1, 150.0, 200.0, 6.0)
                                                              << InputValue(0, 300.0, 400.0, 8.0)
                                                              << InputValue(1, 300.0, 400.0, 7.0)
                                                              << InputValue(2, 300.0, 400.0, 1.0)
                                                              << InputValue(0, 400.0, 420.0, 10.0)
                                                              << InputValue(1, 400.0, 420.0, 3.0)
                                                              << InputValue(2, 400.0, 420.0, 0.9)
                                                              << InputValue(0, 480.0, 500.0, 15.0)
                                                              << InputValue(1, 480.0, 500.0, 4.0)
                                                              << InputValue(2, 480.0, 500.0, 0.8))
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("bnd", "raw", "var2",
                                                                           SequenceName::Flavors{
                                                                                   "stats"}),
                                                              Variant::Root(), 100.0, 200.0),
                                                                                  SequenceValue(
                                                                                          SequenceName("bnd", "raw", "var2",
                                                                           SequenceName::Flavors{
                                                                                   "stats"}),
                                                                                          Variant::Root(),
                                                                                          300.0,
                                                                                          400.0),
                                                                                  SequenceValue(
                                                                                          SequenceName("bnd", "raw", "var2",
                                                                           SequenceName::Flavors{
                                                                                   "stats"}),
                                                                                          Variant::Root(),
                                                                                          400.0,
                                                                                          500.0)});

        QTest::newRow("Final Statistics - Vector 3D empty") << CoreFinalStatistics
                                                            << SmootherChainCore::Vector3D << 3 << 3
                                                            << 4 << (QList<InputValue>())
                                                            << (SequenceValue::Transfer());
        QTest::newRow("Final Statistics - Vector 3D") << CoreFinalStatistics
                                                      << SmootherChainCore::Vector3D << 3 << 3 << 4
                                                      << (QList<InputValue>()
                                                              << InputValue(3, 100.0, 200.0, 1.0)
                                                              << InputValue(0, 100.0, 150.0, 10.0)
                                                              << InputValue(1, 100.0, 150.0, 50.0)
                                                              << InputValue(2, 100.0, 150.0, 5.0)
                                                              << InputValue(0, 150.0, 200.0, 15.0)
                                                              << InputValue(1, 150.0, 200.0, 60.0)
                                                              << InputValue(2, 150.0, 200.0, 6.0)
                                                              << InputValue(0, 300.0, 400.0, 8.0)
                                                              << InputValue(1, 300.0, 400.0, 30.0)
                                                              << InputValue(2, 300.0, 400.0, 7.0)
                                                              << InputValue(3, 300.0, 400.0, 1.0)
                                                              << InputValue(0, 400.0, 420.0, 10.0)
                                                              << InputValue(1, 400.0, 420.0, 40.0)
                                                              << InputValue(2, 400.0, 420.0, 3.0)
                                                              << InputValue(3, 400.0, 420.0, 0.9)
                                                              << InputValue(0, 480.0, 500.0, 15.0)
                                                              << InputValue(1, 480.0, 500.0, 45.0)
                                                              << InputValue(2, 480.0, 500.0, 4.0)
                                                              << InputValue(3, 480.0, 500.0, 0.8))
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("bnd", "raw", "var3",
                                                                           SequenceName::Flavors{
                                                                                   "stats"}),
                                                              Variant::Root(), 100.0, 200.0),
                                                                                  SequenceValue(
                                                                                          SequenceName("bnd", "raw", "var3",
                                                                           SequenceName::Flavors{
                                                                                   "stats"}),
                                                                                          Variant::Root(),
                                                                                          300.0,
                                                                                          400.0),
                                                                                  SequenceValue(
                                                                                          SequenceName("bnd", "raw", "var3",
                                                                           SequenceName::Flavors{
                                                                                   "stats"}),
                                                                                          Variant::Root(),
                                                                                          400.0,
                                                                                          500.0)});


        QTest::newRow("Final Statistics - Beers Law Absorption empty") << CoreFinalStatistics
                                                                       << SmootherChainCore::BeersLawAbsorption
                                                                       << 4 << 1 << 6
                                                                       << (QList<InputValue>())
                                                                       << (SequenceValue::Transfer());
        QTest::newRow("Final Statistics - Beers Law Absorption") << CoreFinalStatistics
                                                                 << SmootherChainCore::BeersLawAbsorption
                                                                 << 4 << 1 << 6
                                                                 << (QList<InputValue>()
                                                                         << InputValue(0, 100.0,
                                                                                       150.0, -1.0)
                                                                         << InputValue(1, 100.0,
                                                                                       150.0, -2.0)
                                                                         << InputValue(2, 100.0,
                                                                                       150.0, -3.0)
                                                                         << InputValue(3, 100.0,
                                                                                       150.0, -4.0)
                                                                         << InputValue(4, 100.0,
                                                                                       150.0, 10.0)
                                                                         << InputValue(5, 100.0,
                                                                                       150.0, 0.9)
                                                                         << InputValue(0, 150.0,
                                                                                       200.0, -5.0)
                                                                         << InputValue(1, 150.0,
                                                                                       200.0, -6.0)
                                                                         << InputValue(2, 150.0,
                                                                                       200.0, -7.0)
                                                                         << InputValue(3, 150.0,
                                                                                       200.0, -8.0)
                                                                         << InputValue(4, 150.0,
                                                                                       200.0, 11.0)
                                                                         << InputValue(5, 150.0,
                                                                                       200.0, 1.0)
                                                                         << InputValue(0, 300.0,
                                                                                       400.0, -9.0)
                                                                         << InputValue(1, 300.0,
                                                                                       400.0, -10.0)
                                                                         << InputValue(2, 300.0,
                                                                                       400.0, -11.0)
                                                                         << InputValue(3, 300.0,
                                                                                       400.0, -12.0)
                                                                         << InputValue(4, 300.0,
                                                                                       400.0, 12.0)
                                                                         << InputValue(5, 300.0,
                                                                                       400.0, 1.0)
                                                                         << InputValue(0, 400.0,
                                                                                       420.0, -13.0)
                                                                         << InputValue(1, 400.0,
                                                                                       420.0, -14.0)
                                                                         << InputValue(2, 400.0,
                                                                                       420.0, -15.0)
                                                                         << InputValue(3, 400.0,
                                                                                       420.0, -16.0)
                                                                         << InputValue(4, 400.0,
                                                                                       420.0, 13.0)
                                                                         << InputValue(5, 400.0,
                                                                                       420.0, 1.0)
                                                                         << InputValue(0, 480.0,
                                                                                       500.0, -17.0)
                                                                         << InputValue(1, 480.0,
                                                                                       500.0, -18.0)
                                                                         << InputValue(2, 480.0,
                                                                                       500.0, -19.0)
                                                                         << InputValue(3, 480.0,
                                                                                       500.0, -20.0)
                                                                         << InputValue(4, 480.0,
                                                                                       500.0, 14.0)
                                                                         << InputValue(5, 480.0,
                                                                                       500.0, 0.8))
                                                                 << (SequenceValue::Transfer{
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "stats"}),
                                                                                 Variant::Root(),
                                                                                 100.0,
                                                                                 200.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "stats"}),
                                                                                 Variant::Root(),
                                                                                 300.0,
                                                                                 400.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "var1",
                                                                                              SequenceName::Flavors{
                                                                                                      "stats"}),
                                                                                 Variant::Root(),
                                                                                 400.0,
                                                                                 500.0)});


        QTest::newRow("Final Statistics - Beers Law Absorption Initial empty")
                << CoreFinalStatistics << SmootherChainCore::BeersLawAbsorptionInitial << 2 << 1
                << 4 << (QList<InputValue>()) << (SequenceValue::Transfer());
        QTest::newRow("Final Statistics - Beers Law Absorption Initial") << CoreFinalStatistics
                                                                         << SmootherChainCore::BeersLawAbsorptionInitial
                                                                         << 2 << 1 << 4
                                                                         << (QList<InputValue>()
                                                                                 << InputValue(0,
                                                                                               100.0,
                                                                                               150.0,
                                                                                               -1.0)
                                                                                 << InputValue(1,
                                                                                               100.0,
                                                                                               150.0,
                                                                                               -2.0)
                                                                                 << InputValue(2,
                                                                                               100.0,
                                                                                               150.0,
                                                                                               10.0)
                                                                                 << InputValue(3,
                                                                                               100.0,
                                                                                               150.0,
                                                                                               0.9)
                                                                                 << InputValue(0,
                                                                                               150.0,
                                                                                               200.0,
                                                                                               -5.0)
                                                                                 << InputValue(1,
                                                                                               150.0,
                                                                                               200.0,
                                                                                               -6.0)
                                                                                 << InputValue(2,
                                                                                               150.0,
                                                                                               200.0,
                                                                                               11.0)
                                                                                 << InputValue(3,
                                                                                               150.0,
                                                                                               200.0,
                                                                                               1.0)
                                                                                 << InputValue(0,
                                                                                               300.0,
                                                                                               400.0,
                                                                                               -9.0)
                                                                                 << InputValue(1,
                                                                                               300.0,
                                                                                               400.0,
                                                                                               -10.0)
                                                                                 << InputValue(2,
                                                                                               300.0,
                                                                                               400.0,
                                                                                               12.0)
                                                                                 << InputValue(3,
                                                                                               300.0,
                                                                                               400.0,
                                                                                               1.0)
                                                                                 << InputValue(0,
                                                                                               400.0,
                                                                                               420.0,
                                                                                               -13.0)
                                                                                 << InputValue(1,
                                                                                               400.0,
                                                                                               420.0,
                                                                                               -14.0)
                                                                                 << InputValue(2,
                                                                                               400.0,
                                                                                               420.0,
                                                                                               13.0)
                                                                                 << InputValue(3,
                                                                                               400.0,
                                                                                               420.0,
                                                                                               1.0)
                                                                                 << InputValue(0,
                                                                                               480.0,
                                                                                               500.0,
                                                                                               -17.0)
                                                                                 << InputValue(1,
                                                                                               480.0,
                                                                                               500.0,
                                                                                               -18.0)
                                                                                 << InputValue(2,
                                                                                               480.0,
                                                                                               500.0,
                                                                                               14.0)
                                                                                 << InputValue(3,
                                                                                               480.0,
                                                                                               500.0,
                                                                                               0.8))
                                                                         << (SequenceValue::Transfer{
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "var1",
                                                                                                 SequenceName::Flavors{
                                                                                                         "stats"}),
                                                                                         Variant::Root(),
                                                                                         100.0,
                                                                                         200.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "var1",
                                                                                                 SequenceName::Flavors{
                                                                                                         "stats"}),
                                                                                         Variant::Root(),
                                                                                         300.0,
                                                                                         400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "bnd",
                                                                                                 "raw",
                                                                                                 "var1",
                                                                                                 SequenceName::Flavors{
                                                                                                         "stats"}),
                                                                                         Variant::Root(),
                                                                                         400.0,
                                                                                         500.0)});

        QTest::newRow("Final Statistics - Dewpoint empty") << CoreFinalStatistics
                                                           << SmootherChainCore::Dewpoint << 2 << 1
                                                           << 4 << (QList<InputValue>())
                                                           << (SequenceValue::Transfer());
        QTest::newRow("Final Statistics - Dewpoint") << CoreFinalStatistics
                                                     << SmootherChainCore::Dewpoint << 2 << 1 << 4
                                                     << (QList<InputValue>()
                                                             << InputValue(0, 100.0, 150.0, -1.0)
                                                             << InputValue(1, 100.0, 150.0, -2.0)
                                                             << InputValue(2, 100.0, 150.0, 10.0)
                                                             << InputValue(3, 100.0, 150.0, 0.9)
                                                             << InputValue(0, 150.0, 200.0, -5.0)
                                                             << InputValue(1, 150.0, 200.0, -6.0)
                                                             << InputValue(2, 150.0, 200.0, 11.0)
                                                             << InputValue(3, 150.0, 200.0, 1.0)
                                                             << InputValue(0, 300.0, 400.0, -9.0)
                                                             << InputValue(1, 300.0, 400.0, -10.0)
                                                             << InputValue(2, 300.0, 400.0, 12.0)
                                                             << InputValue(3, 300.0, 400.0, 1.0)
                                                             << InputValue(0, 400.0, 420.0, -13.0)
                                                             << InputValue(1, 400.0, 420.0, -14.0)
                                                             << InputValue(2, 400.0, 420.0, 13.0)
                                                             << InputValue(3, 400.0, 420.0, 1.0)
                                                             << InputValue(0, 480.0, 500.0, -17.0)
                                                             << InputValue(1, 480.0, 500.0, -18.0)
                                                             << InputValue(2, 480.0, 500.0, 14.0)
                                                             << InputValue(3, 480.0, 500.0, 0.8))
                                                     << (SequenceValue::Transfer{SequenceValue(
                                                             SequenceName("bnd", "raw", "var1",
                                                                          SequenceName::Flavors{
                                                                                  "stats"}),
                                                             Variant::Root(), 100.0, 200.0),
                                                                                 SequenceValue(
                                                                                         SequenceName("bnd", "raw", "var1",
                                                                          SequenceName::Flavors{
                                                                                  "stats"}),
                                                                                         Variant::Root(),
                                                                                         300.0,
                                                                                         400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName("bnd", "raw", "var1",
                                                                          SequenceName::Flavors{
                                                                                  "stats"}),
                                                                                         Variant::Root(),
                                                                                         400.0,
                                                                                         500.0)});


        QTest::newRow("Final Statistics - RH empty") << CoreFinalStatistics << SmootherChainCore::RH
                                                     << 2 << 1 << 4 << (QList<InputValue>())
                                                     << (SequenceValue::Transfer());
        QTest::newRow("Final Statistics - RH") << CoreFinalStatistics << SmootherChainCore::RH << 2
                                               << 1 << 4 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, -1.0) << InputValue(1, 100.0, 150.0, -2.0)
                << InputValue(2, 100.0, 150.0, 10.0) << InputValue(3, 100.0, 150.0, 0.9)
                << InputValue(0, 150.0, 200.0, -5.0) << InputValue(1, 150.0, 200.0, -6.0)
                << InputValue(2, 150.0, 200.0, 11.0) << InputValue(3, 150.0, 200.0, 1.0)
                << InputValue(0, 300.0, 400.0, -9.0) << InputValue(1, 300.0, 400.0, -10.0)
                << InputValue(2, 300.0, 400.0, 12.0) << InputValue(3, 300.0, 400.0, 1.0)
                << InputValue(0, 400.0, 420.0, -13.0) << InputValue(1, 400.0, 420.0, -14.0)
                << InputValue(2, 400.0, 420.0, 13.0) << InputValue(3, 400.0, 420.0, 1.0)
                << InputValue(0, 480.0, 500.0, -17.0) << InputValue(1, 480.0, 500.0, -18.0)
                << InputValue(2, 480.0, 500.0, 14.0) << InputValue(3, 480.0, 500.0, 0.8))
                                               << (SequenceValue::Transfer{SequenceValue(
                                                       SequenceName("bnd", "raw", "var1",
                                                                    SequenceName::Flavors{"stats"}),
                                                       Variant::Root(), 100.0, 200.0),
                                                                           SequenceValue(
                                                                                   SequenceName("bnd", "raw", "var1",
                                                                                                SequenceName::Flavors{
                                                                                                        "stats"}),
                                                                                   Variant::Root(),
                                                                                   300.0, 400.0),
                                                                           SequenceValue(
                                                                                   SequenceName("bnd", "raw", "var1",
                                                                                                SequenceName::Flavors{
                                                                                                        "stats"}),
                                                                                   Variant::Root(),
                                                                                   400.0, 500.0)});


        QTest::newRow("Final Statistics - RH Extrapolate empty") << CoreFinalStatistics
                                                                 << SmootherChainCore::RHExtrapolate
                                                                 << 3 << 1 << 5
                                                                 << (QList<InputValue>())
                                                                 << (SequenceValue::Transfer());
        QTest::newRow("Final Statistics - RH Extrapolate") << CoreFinalStatistics
                                                           << SmootherChainCore::RHExtrapolate << 3
                                                           << 1 << 5 << (QList<InputValue>()
                << InputValue(0, 100.0, 150.0, -1.0) << InputValue(1, 100.0, 150.0, -2.0)
                << InputValue(2, 100.0, 150.0, -3.0) << InputValue(3, 100.0, 150.0, 10.0)
                << InputValue(4, 100.0, 150.0, 0.9) << InputValue(0, 150.0, 200.0, -5.0)
                << InputValue(1, 150.0, 200.0, -6.0) << InputValue(2, 150.0, 200.0, -7.0)
                << InputValue(3, 150.0, 200.0, 11.0) << InputValue(4, 150.0, 200.0, 1.0)
                << InputValue(0, 300.0, 400.0, -9.0) << InputValue(1, 300.0, 400.0, -10.0)
                << InputValue(2, 300.0, 400.0, -11.0) << InputValue(3, 300.0, 400.0, 12.0)
                << InputValue(4, 300.0, 400.0, 1.0) << InputValue(0, 400.0, 420.0, -13.0)
                << InputValue(1, 400.0, 420.0, -14.0) << InputValue(2, 400.0, 420.0, -15.0)
                << InputValue(3, 400.0, 420.0, 13.0) << InputValue(4, 400.0, 420.0, 1.0)
                << InputValue(0, 480.0, 500.0, -17.0) << InputValue(1, 480.0, 500.0, -18.0)
                << InputValue(2, 480.0, 500.0, -19.0) << InputValue(3, 480.0, 500.0, 14.0)
                << InputValue(4, 480.0, 500.0, 0.8)) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                              Variant::Root(), 100.0, 200.0),
                SequenceValue(SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                              Variant::Root(), 300.0, 400.0),
                SequenceValue(SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                              Variant::Root(), 400.0, 500.0)});
    }

    void testFullFlags()
    {
        EditingChainCoreFullCalculate
                core(new DynamicTimeInterval::Constant(Time::Second, 100, false));
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewFlagsInput(SequenceName("bnd", "raw", "in"));
        engine1->addNewFlagsOutput(SequenceName("bnd", "raw", "out"));
        core.createFlags(engine1);

        QCOMPARE(engine1->inputs.size(), 1);
        QCOMPARE(engine1->inputsFlags.size(), 1);

        engine1->inputs[0].incomingData(100.0, 150.0, 0.8);
        engine1->inputs[0].incomingData(150.0, 200.0, 1.0);
        engine1->inputsFlags[0].incomingData(100.0, 150.0, Variant::Flags{"F1"});
        engine1->inputsFlags[0].incomingData(150.0, 200.0, Variant::Flags{"F2"});
        engine1->inputs[0].incomingData(300.0, 400.0, 1.0);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            engine1->mux.wait(5);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeFlags(stream, engine2);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 1);
        QCOMPARE(engine2->inputsFlags.size(), 1);

        engine2->inputsFlags[0].incomingData(300.0, 400.0, Variant::Flags{"F3"});
        engine2->inputs[0].incomingData(400.0, 420.0, 1.0);
        engine2->inputsFlags[0].incomingData(400.0, 420.0, Variant::Flags{"F4"});
        engine2->inputs[0].incomingData(480.0, 500.0, 0.8);
        engine2->inputsFlags[0].incomingData(480.0, 500.0, Variant::Flags{"F5"});

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();

        Variant::Root check;
        check.write().setFlags(Variant::Flags{"F1", "F2"});
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out"), check, 100.0, 200.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(0.9),
                100.0, 200.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "cont", "var1"), check, 100.0, 200.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "cont", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(0.9),
                100.0, 200.0)));
        check.write().setFlags(Variant::Flags{"F3"});
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out"), check, 300.0, 400.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "cont", "var1"), check, 300.0, 400.0)));
        check.write().setFlags(Variant::Flags{"F4", "F5"});
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out"), check, 400.0, 500.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(0.36),
                400.0, 500.0)));
        check.write().setFlags(Variant::Flags{"F4"});
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "cont", "var1"), check, 400.0, 420.0)));
        check.write().setFlags(Variant::Flags{"F5"});
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "cont", "var1"), check, 480.0, 500.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "cont", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(0.8),
                480.0, 500.0)));

        QVERIFY(values.empty());
    }

    void testContFlags()
    {
        EditingChainCoreContinuousRecalculate
                core(new DynamicTimeInterval::Constant(Time::Second, 100, false));
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewFlagsInput(SequenceName("bnd", "raw", "in"));
        engine1->addNewFlagsOutput(SequenceName("bnd", "raw", "out"));
        core.createFlags(engine1);

        QCOMPARE(engine1->inputs.size(), 1);
        QCOMPARE(engine1->inputsFlags.size(), 1);

        engine1->inputs[0].incomingData(100.0, 150.0, 0.8);
        engine1->inputs[0].incomingData(150.0, 200.0, 1.0);
        engine1->inputsFlags[0].incomingData(100.0, 150.0, Variant::Flags{"F1"});
        engine1->inputsFlags[0].incomingData(150.0, 200.0, Variant::Flags{"F2"});
        engine1->inputs[0].incomingData(300.0, 400.0, 1.0);

        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            engine1->mux.wait(5);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeFlags(stream, engine2);
            engine2->mux.setEgress(&end);
        }

        QCOMPARE(engine2->inputs.size(), 1);
        QCOMPARE(engine2->inputsFlags.size(), 1);

        engine2->inputsFlags[0].incomingData(300.0, 400.0, Variant::Flags{"F3"});
        engine2->inputs[0].incomingData(400.0, 420.0, 1.0);
        engine2->inputsFlags[0].incomingData(400.0, 420.0, Variant::Flags{"F4"});
        engine2->inputs[0].incomingData(480.0, 500.0, 0.8);
        engine2->inputsFlags[0].incomingData(480.0, 500.0, Variant::Flags{"F5"});

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();

        Variant::Root check;
        check.write().setFlags(Variant::Flags{"F1", "F2"});
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out"), check, 100.0, 200.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(0.9),
                100.0, 200.0)));
        check.write().setFlags(Variant::Flags{"F3"});
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out"), check, 300.0, 400.0)));
        check.write().setFlags(Variant::Flags{"F4", "F5"});
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out"), check, 400.0, 500.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(0.36),
                400.0, 500.0)));

        QVERIFY(values.empty());
    }
};


QTEST_APPLESS_MAIN(TestEditingChain)

#include "editingchain.moc"
