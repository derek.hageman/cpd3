/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>

#include "smoothing/contamfilter.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Smoothing;
using namespace CPD3::Data;

static Variant::Root cms(const char *meta = nullptr,
                         const char *path = nullptr,
                         const char *str = nullptr)
{
    Variant::Root output;
    output.write().setType(Variant::Type::MetadataReal);
    if (!meta)
        return output;
    if (!path || !(*path))
        output.write().metadataReal(meta).setString(str);
    else
        output.write().metadataReal(meta)[path].setString(str);
    return output;
}

static Variant::Root cmb(const char *meta = nullptr, const char *path = nullptr, bool b = false)
{
    Variant::Root output;
    output.write().setType(Variant::Type::MetadataReal);
    if (!meta)
        return output;
    if (!path || !(*path))
        output.write().metadataReal(meta).setBool(b);
    else
        output.write().metadataReal(meta)[path].setBool(b);
    return output;
}

class TestContamFilter : public QObject {
Q_OBJECT

    static bool valuesEqual(const Variant::Read &a, const Variant::Read &b)
    {
        if (a.getType() != Variant::Type::Real)
            return a == b;
        if (b.getType() != Variant::Type::Real)
            return a == b;
        double va = a.toDouble();
        double vb = b.toDouble();
        if (!FP::defined(va))
            return !FP::defined(vb);
        if (!FP::defined(vb))
            return false;
        double eps = qMax(va, vb) * 1E-6;
        if (eps == 0)
            return va == vb;
        return fabs(va - vb) < eps;
    }

    static bool engineCompare(SequenceValue::Transfer values, SequenceValue::Transfer expected)
    {
        for (auto f = expected.begin(); f != expected.end();) {
            bool hit = false;
            for (auto c = values.begin(), endC = values.end(); c != endC; ++c) {
                if (!FP::equal(c->getStart(), f->getStart()))
                    continue;
                if (!FP::equal(c->getEnd(), f->getEnd()))
                    continue;
                if (c->getUnit() != f->getUnit())
                    continue;

                Variant::Root merged = Variant::Root::overlay(c->root(), f->root());
                if (!valuesEqual(merged, c->getValue())) {
                    qDebug() << "Value overlay mismatch for" << *f << "merged:" << merged
                             << "input:" << c->getValue();
                    return false;
                }

                values.erase(c);
                f = expected.erase(f);
                hit = true;
                break;
            }
            if (!hit)
                ++f;
        }
        if (!values.empty()) {
            qDebug() << "Unmatched values in result:" << values;
        }
        if (!expected.empty()) {
            qDebug() << "Unmatched values in expected:" << expected;
        }
        return values.empty() && expected.empty();
    }

private slots:

    void basic()
    {
        QFETCH(SequenceName, affected);
        QFETCH(SequenceValue::Transfer, input);
        QFETCH(SequenceValue::Transfer, expected);

        StreamSink::Buffer output;
        ContaminationFilter *f = nullptr;

        f = new ContaminationFilter(affected.isValid() ? new DynamicSequenceSelection::Match(
                affected.getStationQString(), affected.getArchiveQString(),
                affected.getVariableQString()) : new DynamicSequenceSelection::Match(QString(),
                                                                                     QString(),
                                                                                     QString()));
        f->start();
        f->setEgress(&output);
        f->incomingData(input);
        f->endData();
        QVERIFY(f->wait(30));
        delete f;
        QVERIFY(output.ended());
        QVERIFY(engineCompare(output.values(), expected));

        f = new ContaminationFilter(affected.isValid() ? new DynamicSequenceSelection::Match(
                affected.getStationQString(), affected.getArchiveQString(),
                affected.getVariableQString()) : new DynamicSequenceSelection::Match(QString(),
                                                                                     QString(),
                                                                                     QString()));
        f->start();
        output.reset();
        f->setEgress(&output);
        std::size_t half = input.size() / 2;
        for (std::size_t i = 0; i < half; i++) {
            f->incomingData(input.at(i));
        }
        QTest::qSleep(50);
        {
            f->setEgress(nullptr);
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                f->serialize(stream);
            }
            f->endData();
            f->signalTerminate();
            QVERIFY(f->wait(30));
            delete f;
            f = new ContaminationFilter(nullptr);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                f->deserialize(stream);
            }
            f->start();
            f->setEgress(&output);
        }
        for (std::size_t i = half; i < input.size(); i++) {
            f->incomingData(input.at(i));
        }
        f->endData();
        QVERIFY(f->wait(30));
        delete f;
        QVERIFY(output.ended());
        QVERIFY(engineCompare(output.values(), expected));

        /* Skip for bulk tests */
        if (input.size() > 100)
            return;

        f = new ContaminationFilter(affected.isValid() ? new DynamicSequenceSelection::Match(
                affected.getStationQString(), affected.getArchiveQString(),
                affected.getVariableQString())
                                                       : new DynamicSequenceSelection::Match(QString(), QString(),
                                                                                             QString()));
        f->start();
        output.reset();
        f->setEgress(&output);
        QTest::qSleep(50);
        for (std::size_t i = 0; i < input.size(); i++) {
            QTest::qSleep(50);
            f->incomingData(input.at(i));
        }
        f->endData();
        QVERIFY(f->wait(30));
        delete f;
        QVERIFY(output.ended());
        QVERIFY(engineCompare(output.values(), expected));
    }

    void basic_data()
    {
        QTest::addColumn<SequenceName>("affected");
        QTest::addColumn<SequenceValue::Transfer>("input");
        QTest::addColumn<SequenceValue::Transfer>("expected");

        QTest::newRow("Empty") << SequenceName() << (SequenceValue::Transfer())
                               << (SequenceValue::Transfer());

        QTest::newRow("Single no flags") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.0), 1000.0,
                              2000.0)})
                                         << (SequenceValue::Transfer{SequenceValue(
                                                 SequenceName("bnd", "raw", "BsG_S11"),
                                                 Variant::Root(1.0),
                                                 1000.0, 2000.0)});
        QTest::newRow("Single only meta") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cms("Thing", "Value", "Stuff"), 1000.0, 2000.0)})
                                          << (SequenceValue::Transfer{SequenceValue(
                                                  SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                  cms("Thing", "Value", "Stuff"), 1000.0, 2000.0)});
        QTest::newRow("Single only set") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, false), 1000.0, 2000.0)})
                                         << (SequenceValue::Transfer{SequenceValue(
                                                 SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                 cmb("ContaminationRemoved", NULL, true), 1000.0,
                                                 2000.0)});
        QTest::newRow("Single only flags") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Flag"}), 1000.0, 2000.0)})
                                           << (SequenceValue::Transfer{SequenceValue(
                                                   SequenceName("bnd", "raw", "F1_S11"),
                                                   Variant::Root(Variant::Flags{"Flag"}), 1000.0,
                                                   2000.0)});
        QTest::newRow("Single only flags contam") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0)})
                                                  << (SequenceValue::Transfer{SequenceValue(
                                                          SequenceName("bnd", "raw", "F1_S11"),
                                                          Variant::Root(
                                                                  Variant::Flags{"Contaminated"}),
                                                          1000.0, 2000.0)});

        QTest::newRow("Single no flags unaffected") << SequenceName("bnd", "raw", "BsB_S11")
                                                    << SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(1.0), 1000.0, 2000.0)}
                                                    << SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(1.0), 1000.0, 2000.0)};
        QTest::newRow("Single only meta unaffected") << SequenceName("bnd", "raw", "BsB_S11")
                                                     << SequenceValue::Transfer{SequenceValue(
                                                             SequenceName("bnd", "raw_meta",
                                                                          "BsG_S11"),
                                                             cms("Thing", "Value", "Stuff"), 1000.0,
                                                             2000.0)} << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cms("Thing", "Value", "Stuff"), 1000.0, 2000.0)};
        QTest::newRow("Single only set unaffected") << SequenceName("bnd", "raw", "BsB_S11")
                                                    << SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw_meta",
                                                                         "BsG_S11"),
                                                            cmb("ContaminationRemoved", NULL,
                                                                false), 1000.0, 2000.0)}
                                                    << SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw_meta",
                                                                         "BsG_S11"),
                                                            cmb("ContaminationRemoved", NULL,
                                                                false), 1000.0, 2000.0)};
        QTest::newRow("Single only flags unaffected") << SequenceName("bnd", "raw", "BsB_S11")
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              1000.0, 2000.0)})
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              1000.0, 2000.0)});
        QTest::newRow("Single only flags contam unaffected")
                << SequenceName("bnd", "raw", "BsB_S11") << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0)})
                << (SequenceValue::Transfer{SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                          Variant::Root(
                                                                  Variant::Flags{"Contaminated"}),
                                                          1000.0, 2000.0)});

        QTest::newRow("Two no flags") << SequenceName() << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 2000.0,
                              3000.0)}
                                      << SequenceValue::Transfer{
                                              SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(1.0), 1000.0, 2000.0),
                                              SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(2.0), 2000.0, 3000.0)};
        QTest::newRow("Two only meta") << SequenceName() << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cms("Thing", "Value", "Stuff"), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cms("Thing", "Value", "Stuff2"), 2000.0, 3000.0)}
                                       << SequenceValue::Transfer{SequenceValue(
                                               SequenceName("bnd", "raw_meta", "BsG_S11"),
                                               cms("Thing", "Value", "Stuff"), 1000.0, 2000.0),
                                                                  SequenceValue(SequenceName("bnd",
                                                                                             "raw_meta",
                                                                                             "BsG_S11"),
                                                                                cms("Thing",
                                                                                    "Value",
                                                                                    "Stuff2"),
                                                                                2000.0, 3000.0)};
        QTest::newRow("Two only set") << SequenceName() << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, false), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, false), 2000.0, 3000.0)}
                                      << SequenceValue::Transfer{SequenceValue(
                                              SequenceName("bnd", "raw_meta", "BsG_S11"),
                                              cmb("ContaminationRemoved", NULL, true), 1000.0,
                                              2000.0), SequenceValue(
                                              SequenceName("bnd", "raw_meta", "BsG_S11"),
                                              cmb("ContaminationRemoved", NULL, true), 2000.0,
                                              3000.0)};
        QTest::newRow("Two only flags") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Flag"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Flag"}), 2000.0, 3000.0)})
                                        << (SequenceValue::Transfer{
                                                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              1000.0, 2000.0),
                                                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              2000.0, 3000.0)});
        QTest::newRow("Two only flags contam") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 2000.0, 3000.0)})
                                               << (SequenceValue::Transfer{SequenceValue(
                                                       SequenceName("bnd", "raw", "F1_S11"),
                                                       Variant::Root(
                                                               Variant::Flags{"Contaminated"}),
                                                       1000.0, 2000.0), SequenceValue(
                                                       SequenceName("bnd", "raw", "F1_S11"),
                                                       Variant::Root(
                                                               Variant::Flags{"Contaminated"}),
                                                       2000.0, 3000.0)});


        QTest::newRow("Single contaminated") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 1000.0,
                              2000.0)})
                                             << (SequenceValue::Transfer{SequenceValue(
                                                     SequenceName("bnd", "raw", "F1_S11"),
                                                     Variant::Root(Variant::Flags{"Contaminated"}),
                                                     1000.0, 2000.0), SequenceValue(
                                                     SequenceName("bnd", "raw", "BsG_S11"),
                                                     Variant::Root(FP::undefined()), 1000.0,
                                                     2000.0)});
        QTest::newRow("Single contaminated reversed") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0)})
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{
                                                                      "Contaminated"}), 1000.0,
                                                              2000.0), SequenceValue(
                                                              SequenceName("bnd", "raw", "BsG_S11"),
                                                              Variant::Root(FP::undefined()),
                                                              1000.0,
                                                              2000.0)});
        QTest::newRow("Single contaminated internal") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 1100.0,
                              1900.0)})
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{
                                                                      "Contaminated"}), 1000.0,
                                                              2000.0), SequenceValue(
                                                              SequenceName("bnd", "raw", "BsG_S11"),
                                                              Variant::Root(FP::undefined()),
                                                              1100.0,
                                                              1900.0)});
        QTest::newRow("Single contaminated before") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 900.0,
                              2000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0)})
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "F1_S11"),
                                                            Variant::Root(
                                                                    Variant::Flags{"Contaminated"}),
                                                            1000.0,
                                                            2000.0), SequenceValue(
                                                            SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(2.0), 900.0, 1000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "BsG_S11"),
                                                                                        Variant::Root(
                                                                                                FP::undefined()),
                                                                                        1000.0,
                                                                                        2000.0)});
        QTest::newRow("Single contaminated before contained") << SequenceName()
                                                              << (SequenceValue::Transfer{
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "raw",
                                                                                           "BsG_S11"),
                                                                              Variant::Root(2.0),
                                                                              900.0,
                                                                              1900.0),
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "raw",
                                                                                           "F1_S11"),
                                                                              Variant::Root(
                                                                                      Variant::Flags{
                                                                                              "Contaminated"}),
                                                                              1000.0, 2000.0)})
                                                              << (SequenceValue::Transfer{
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "raw",
                                                                                           "F1_S11"),
                                                                              Variant::Root(
                                                                                      Variant::Flags{
                                                                                              "Contaminated"}),
                                                                              1000.0, 2000.0),
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "raw",
                                                                                           "BsG_S11"),
                                                                              Variant::Root(2.0),
                                                                              900.0,
                                                                              1000.0),
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "raw",
                                                                                           "BsG_S11"),
                                                                              Variant::Root(
                                                                                      FP::undefined()),
                                                                              1000.0, 1900.0)});
        QTest::newRow("Single contaminated after") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 1000.0,
                              2100.0)})
                                                   << (SequenceValue::Transfer{SequenceValue(
                                                           SequenceName("bnd", "raw", "F1_S11"),
                                                           Variant::Root(
                                                                   Variant::Flags{"Contaminated"}),
                                                           1000.0, 2000.0), SequenceValue(
                                                           SequenceName("bnd", "raw", "BsG_S11"),
                                                           Variant::Root(2.0), 2000.0, 2100.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "raw",
                                                                                               "BsG_S11"),
                                                                                       Variant::Root(
                                                                                               FP::undefined()),
                                                                                       1000.0,
                                                                                       2000.0)});
        QTest::newRow("Single contaminated after contained") << SequenceName()
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             1100.0,
                                                                             2100.0)})
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             2000.0,
                                                                             2100.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(
                                                                                     FP::undefined()),
                                                                             1100.0, 2000.0)});

        QTest::newRow("Contaminated middle switch") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 1000.0,
                              3000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"), Variant::Root(Variant::Flags()),
                              2000.0, 3000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"), Variant::Root(Variant::Flags()),
                              2000.0, 3000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(FP::undefined()),
                              1000.0,
                              2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 2000.0,
                              3000.0)});
        QTest::newRow("Contaminated middle switch reversed") << SequenceName()
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             1000.0,
                                                                             3000.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0)})
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(
                                                                                     FP::undefined()),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             2000.0,
                                                                             3000.0)});
        QTest::newRow("Contaminated middle switch extended") << SequenceName()
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             900.0,
                                                                             3100.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0)})
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             900.0,
                                                                             1000.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(
                                                                                     FP::undefined()),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             2000.0,
                                                                             3000.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             3000.0,
                                                                             3100.0)});

        QTest::newRow("Metadata contamination disabled") << SequenceName()
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("bnd", "raw",
                                                                              "F1_S11"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "Contaminated"}), 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("bnd", "raw_meta",
                                                                              "BsG_S11"),
                                                                 cmb("ContaminationRemoved", NULL,
                                                                     true), 1000.0, 2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "BsG_S11"),
                                                                                             Variant::Root(
                                                                                                     2.0),
                                                                                             1000.0,
                                                                                             2000.0)})
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("bnd", "raw",
                                                                              "F1_S11"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "Contaminated"}), 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("bnd", "raw_meta",
                                                                              "BsG_S11"),
                                                                 cmb("ContaminationRemoved", NULL,
                                                                     true), 1000.0, 2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "BsG_S11"),
                                                                                             Variant::Root(
                                                                                                     2.0),
                                                                                             1000.0,
                                                                                             2000.0)});
        QTest::newRow("Metadata contamination disabled reorder") << SequenceName()
                                                                 << (SequenceValue::Transfer{
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw_meta",
                                                                                              "BsG_S11"),
                                                                                 cmb("ContaminationRemoved",
                                                                                     NULL, true),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "F1_S11"),
                                                                                 Variant::Root(
                                                                                         Variant::Flags{
                                                                                                 "Contaminated"}),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "BsG_S11"),
                                                                                 Variant::Root(2.0),
                                                                                 1000.0,
                                                                                 2000.0)})
                                                                 << (SequenceValue::Transfer{
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "F1_S11"),
                                                                                 Variant::Root(
                                                                                         Variant::Flags{
                                                                                                 "Contaminated"}),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw_meta",
                                                                                              "BsG_S11"),
                                                                                 cmb("ContaminationRemoved",
                                                                                     NULL, true),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "BsG_S11"),
                                                                                 Variant::Root(2.0),
                                                                                 1000.0,
                                                                                 2000.0)});

        QTest::newRow("Metadata contamination disabled transition") << SequenceName()
                                                                    << (SequenceValue::Transfer{
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "F1_S11"),
                                                                                    Variant::Root(
                                                                                            Variant::Flags{
                                                                                                    "Contaminated"}),
                                                                                    1000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL, true),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            1.0),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL,
                                                                                        false),
                                                                                    2000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            2.0),
                                                                                    2000.0,
                                                                                    3000.0)})
                                                                    << (SequenceValue::Transfer{
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "F1_S11"),
                                                                                    Variant::Root(
                                                                                            Variant::Flags{
                                                                                                    "Contaminated"}),
                                                                                    1000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL, true),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            1.0),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL, true),
                                                                                    2000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            FP::undefined()),
                                                                                    2000.0,
                                                                                    3000.0)});

        QTest::newRow("Metadata contamination delete") << SequenceName()
                                                       << (SequenceValue::Transfer{SequenceValue(
                                                               SequenceName("bnd", "raw", "F1_S11"),
                                                               Variant::Root(Variant::Flags{
                                                                       "Contaminated"}), 1000.0,
                                                               2000.0), SequenceValue(
                                                               SequenceName("bnd", "raw_meta",
                                                                            "BsG_S11"),
                                                               cms("Smoothing", "Mode",
                                                                   "Difference"), 1000.0, 2000.0),
                                                                                   SequenceValue(
                                                                                           SequenceName(
                                                                                                   "bnd",
                                                                                                   "raw",
                                                                                                   "BsG_S11"),
                                                                                           Variant::Root(
                                                                                                   2.0),
                                                                                           1000.0,
                                                                                           2000.0)})
                                                       << (SequenceValue::Transfer{SequenceValue(
                                                               SequenceName("bnd", "raw", "F1_S11"),
                                                               Variant::Root(Variant::Flags{
                                                                       "Contaminated"}), 1000.0,
                                                               2000.0), SequenceValue(
                                                               SequenceName("bnd", "raw_meta",
                                                                            "BsG_S11"),
                                                               cms("Smoothing", "Mode",
                                                                   "Difference"), 1000.0, 2000.0)});

        QTest::newRow("Metadata contamination delete transition") << SequenceName()
                                                                  << (SequenceValue::Transfer{
                                                                          SequenceValue(
                                                                                  SequenceName(
                                                                                          "bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                                  Variant::Root(
                                                                                          Variant::Flags{
                                                                                                  "Contaminated"}),
                                                                                  1000.0, 3000.0),
                                                                          SequenceValue(
                                                                                  SequenceName(
                                                                                          "bnd",
                                                                                          "raw_meta",
                                                                                          "BsG_S11"),
                                                                                  cms("Smoothing",
                                                                                      "Mode",
                                                                                      "Difference"),
                                                                                  1000.0, 2000.0),
                                                                          SequenceValue(
                                                                                  SequenceName(
                                                                                          "bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                                  Variant::Root(
                                                                                          1.0),
                                                                                  1000.0, 2000.0),
                                                                          SequenceValue(
                                                                                  SequenceName(
                                                                                          "bnd",
                                                                                          "raw_meta",
                                                                                          "BsG_S11"),
                                                                                  cms("Smoothing",
                                                                                      "Mode",
                                                                                      "Vector2D"),
                                                                                  2000.0, 3000.0),
                                                                          SequenceValue(
                                                                                  SequenceName(
                                                                                          "bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                                  Variant::Root(
                                                                                          2.0),
                                                                                  2000.0, 3000.0)})
                                                                  << (SequenceValue::Transfer{
                                                                          SequenceValue(
                                                                                  SequenceName(
                                                                                          "bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                                  Variant::Root(
                                                                                          Variant::Flags{
                                                                                                  "Contaminated"}),
                                                                                  1000.0, 3000.0),
                                                                          SequenceValue(
                                                                                  SequenceName(
                                                                                          "bnd",
                                                                                          "raw_meta",
                                                                                          "BsG_S11"),
                                                                                  cms("Smoothing",
                                                                                      "Mode",
                                                                                      "Difference"),
                                                                                  1000.0, 2000.0),
                                                                          SequenceValue(
                                                                                  SequenceName(
                                                                                          "bnd",
                                                                                          "raw_meta",
                                                                                          "BsG_S11"),
                                                                                  cms("Smoothing",
                                                                                      "Mode",
                                                                                      "Vector2D"),
                                                                                  2000.0, 3000.0),
                                                                          SequenceValue(
                                                                                  SequenceName(
                                                                                          "bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                                  Variant::Root(
                                                                                          FP::undefined()),
                                                                                  2000.0, 3000.0)});

        Variant::Root meta;
        meta.write().metadataSingleFlag("ContaminateBlarg").hash("Stuff").setString("A");
        QTest::newRow("Metadata alternate flag") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"), meta, 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"ContaminateBlarg"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.0), 1000.0,
                              2000.0)})
                                                 << (SequenceValue::Transfer{SequenceValue(
                                                         SequenceName("bnd", "raw_meta", "F1_S11"),
                                                         meta, 1000.0, 2000.0), SequenceValue(
                                                         SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags{
                                                                 "ContaminateBlarg"}), 1000.0,
                                                         2000.0), SequenceValue(
                                                         SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(FP::undefined()), 1000.0,
                                                         2000.0)});


        QTest::newRow("Complex A") << SequenceName("bnd", "raw", "BsG_S11")
                                   << (SequenceValue::Transfer{
                                           SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"),
                                                         meta, 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                         cmb("ContaminationRemoved", NULL, false),
                                                         1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "BsB_S11"),
                                                         cms("A", "B", "C"), 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(1.0), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(1.1), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags{"Stuff"}),
                                                         2000.0,
                                                         3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(2.0), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(2.1), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(3.0), 3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(3.1), 3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 4000.0,
                                                         5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(4.0), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(4.1), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 5000.0,
                                                         6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(5.0), 5000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(5.1), 5000.0, 6000.0)})
                                   << (SequenceValue::Transfer{
                                           SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"),
                                                         meta, 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                         cmb("ContaminationRemoved", NULL, true),
                                                         1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "BsB_S11"),
                                                         cms("A", "B", "C"), 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(FP::undefined()), 1000.0,
                                                         2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(1.1), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags{"Stuff"}),
                                                         2000.0,
                                                         3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(2.0), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(2.1), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(FP::undefined()), 3000.0,
                                                         4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(3.1), 3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 4000.0,
                                                         5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(4.0), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(4.1), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 5000.0,
                                                         6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(5.0), 5000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(5.1), 5000.0, 6000.0)});


        QTest::newRow("Complex B") << SequenceName("bnd", "raw", "BsG_S11")
                                   << SequenceValue::Transfer{
                                           SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"),
                                                         Variant::Root(), FP::undefined(),
                                                         FP::undefined()),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "T1_S11"),
                                                         Variant::Root(), FP::undefined(),
                                                         FP::undefined()),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                         Variant::Root(), FP::undefined(),
                                                         FP::undefined()),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(1.0), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "T1_S11"),
                                                         Variant::Root(2.0), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 1000.0,
                                                         2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(1.0), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "T1_S11"),
                                                         Variant::Root(2.0), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 2000.0,
                                                         3000.0)} << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"), Variant::Root(),
                              FP::undefined(),
                              FP::undefined()),
                SequenceValue(SequenceName("bnd", "raw_meta", "T1_S11"), Variant::Root(),
                              FP::undefined(),
                              FP::undefined()),
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, true), FP::undefined(),
                              FP::undefined()),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("bnd", "raw", "T1_S11"), Variant::Root(2.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"), Variant::Root(Variant::Flags()),
                              1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.0), 2000.0,
                              3000.0),
                SequenceValue(SequenceName("bnd", "raw", "T1_S11"), Variant::Root(2.0), 2000.0,
                              3000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"), Variant::Root(Variant::Flags()),
                              2000.0, 3000.0)};


        SequenceValue::Transfer input
                {SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "F2_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "T_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "Tu_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "U_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "Uu_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "P_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "BsB_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "BsR_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "BbsB_S11"), Variant::Root(),
                               FP::undefined(), FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "BbsG_S11"), Variant::Root(),
                               FP::undefined(), FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "BbsR_S11"), Variant::Root(),
                               FP::undefined(), FP::undefined())};
        SequenceValue::Transfer result
                {SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "F2_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "T_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "Tu_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "U_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "Uu_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "P_S11"), Variant::Root(),
                               FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "BsB_S11"),
                               cmb("ContaminationRemoved", NULL, true), FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                               cmb("ContaminationRemoved", NULL, true), FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "BsR_S11"),
                               cmb("ContaminationRemoved", NULL, true), FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "BbsB_S11"),
                               cmb("ContaminationRemoved", NULL, true), FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "BbsG_S11"),
                               cmb("ContaminationRemoved", NULL, true), FP::undefined(),
                               FP::undefined()),
                 SequenceValue(SequenceName("bnd", "raw_meta", "BbsR_S11"),
                               cmb("ContaminationRemoved", NULL, true), FP::undefined(),
                               FP::undefined())};

        for (int i = 0; i < 86400; i += 60) {
            if (i % 3600 == 0) {
                input.emplace_back(SequenceName("bnd", "raw", "F1_S11"),
                                   Variant::Root(Variant::Flags{"Contaminated"}), 3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "F2_S11"), Variant::Root(1.00 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.01 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "Tu_S11"), Variant::Root(1.02 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "U_S11"), Variant::Root(1.03 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "Uu_S11"), Variant::Root(1.04 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "P_S11"), Variant::Root(1.05 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "BsB_S11"), Variant::Root(1.06 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.07 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "BsR_S11"), Variant::Root(1.08 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "BbsB_S11"), Variant::Root(1.09 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "BbsG_S11"), Variant::Root(1.11 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "BbsR_S11"), Variant::Root(1.12 + i),
                                   3600.0 + i,
                                   3660.0 + i);

                result.emplace_back(SequenceName("bnd", "raw", "F1_S11"),
                                    Variant::Root(Variant::Flags{"Contaminated"}), 3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "F2_S11"), Variant::Root(1.00 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.01 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "Tu_S11"), Variant::Root(1.02 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "U_S11"), Variant::Root(1.03 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "Uu_S11"), Variant::Root(1.04 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "P_S11"), Variant::Root(1.05 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "BsB_S11"),
                                    Variant::Root(FP::undefined()),
                                    3600.0 + i, 3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "BsG_S11"),
                                    Variant::Root(FP::undefined()),
                                    3600.0 + i, 3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "BsR_S11"),
                                    Variant::Root(FP::undefined()),
                                    3600.0 + i, 3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "BbsB_S11"),
                                    Variant::Root(FP::undefined()),
                                    3600.0 + i, 3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "BbsG_S11"),
                                    Variant::Root(FP::undefined()),
                                    3600.0 + i, 3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "BbsR_S11"),
                                    Variant::Root(FP::undefined()),
                                    3600.0 + i, 3660.0 + i);
            } else {
                input.emplace_back(SequenceName("bnd", "raw", "F1_S11"),
                                   Variant::Root(Variant::Flags()),
                                   3600.0 + i, 3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "F2_S11"), Variant::Root(1.00 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.01 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "Tu_S11"), Variant::Root(1.02 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "U_S11"), Variant::Root(1.03 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "Uu_S11"), Variant::Root(1.04 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "P_S11"), Variant::Root(1.05 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "BsB_S11"), Variant::Root(1.06 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.07 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "BsR_S11"), Variant::Root(1.08 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "BbsB_S11"), Variant::Root(1.09 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "BbsG_S11"), Variant::Root(1.11 + i),
                                   3600.0 + i,
                                   3660.0 + i);
                input.emplace_back(SequenceName("bnd", "raw", "BbsR_S11"), Variant::Root(1.12 + i),
                                   3600.0 + i,
                                   3660.0 + i);

                result.emplace_back(SequenceName("bnd", "raw", "F1_S11"),
                                    Variant::Root(Variant::Flags()),
                                    3600.0 + i, 3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "F2_S11"), Variant::Root(1.00 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.01 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "Tu_S11"), Variant::Root(1.02 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "U_S11"), Variant::Root(1.03 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "Uu_S11"), Variant::Root(1.04 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "P_S11"), Variant::Root(1.05 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "BsB_S11"), Variant::Root(1.06 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.07 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "BsR_S11"), Variant::Root(1.08 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "BbsB_S11"), Variant::Root(1.09 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "BbsG_S11"), Variant::Root(1.11 + i),
                                    3600.0 + i,
                                    3660.0 + i);
                result.emplace_back(SequenceName("bnd", "raw", "BbsR_S11"), Variant::Root(1.12 + i),
                                    3600.0 + i,
                                    3660.0 + i);
            }
        }

        QTest::newRow("Bulk") << SequenceName("bnd", "raw", "Bb?[aes](?:(?:[A-Z])|(?:[0-9]+))?_.*")
                              << input << result;
    }


    void editing()
    {
        QFETCH(SequenceName, affected);
        QFETCH(SequenceName, variable);
        QFETCH(SequenceValue::Transfer, input);
        QFETCH(SequenceValue::Transfer, expected);

        StreamSink::Buffer output;
        EditingContaminationFilter *f = nullptr;

        f = new EditingContaminationFilter(affected.isValid() ? new DynamicSequenceSelection::Match(
                affected.getStationQString(), affected.getArchiveQString(),
                affected.getVariableQString()) : new DynamicSequenceSelection::Match(QString(),
                                                                                     QString(),
                                                                                     QString()),
                                           variable.isValid() ? new SequenceMatch::Composite(
                                                   SequenceMatch::Element(
                                                           variable.getStationQString(),
                                                           variable.getArchiveQString(),
                                                           variable.getVariableQString()))
                                                              : nullptr);
        f->start();
        f->setEgress(&output);
        f->incomingData(input);
        f->endData();
        QVERIFY(f->wait(30));
        delete f;
        QVERIFY(output.ended());
        QVERIFY(engineCompare(output.values(), expected));

        f = new EditingContaminationFilter(affected.isValid() ? new DynamicSequenceSelection::Match(
                affected.getStationQString(), affected.getArchiveQString(),
                affected.getVariableQString()) : new DynamicSequenceSelection::Match(QString(),
                                                                                     QString(),
                                                                                     QString()),
                                           variable.isValid() ? new SequenceMatch::Composite(
                        SequenceMatch::Element(variable.getStationQString(),
                                               variable.getArchiveQString(),
                                               variable.getVariableQString())) : nullptr);
        f->start();
        output.reset();
        f->setEgress(&output);
        std::size_t half = input.size() / 2;
        for (std::size_t i = 0; i < half; i++) {
            f->incomingData(input.at(i));
        }
        QTest::qSleep(50);
        {
            f->setEgress(nullptr);
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                f->serialize(stream);
            }
            f->endData();
            f->signalTerminate();
            QVERIFY(f->wait(30));
            delete f;
            f = new EditingContaminationFilter(nullptr,
                                               variable.isValid() ? new SequenceMatch::Composite(
                                                       SequenceMatch::Element(
                                                               variable.getStationQString(),
                                                               variable.getArchiveQString(),
                                                               variable.getVariableQString()))
                                                                  : nullptr);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                f->deserialize(stream);
            }
            f->start();
            f->setEgress(&output);
        }
        for (std::size_t i = half; i < input.size(); i++) {
            f->incomingData(input.at(i));
        }
        f->endData();
        QVERIFY(f->wait(30));
        delete f;
        QVERIFY(output.ended());
        QVERIFY(engineCompare(output.values(), expected));

        f = new EditingContaminationFilter(affected.isValid() ? new DynamicSequenceSelection::Match(
                affected.getStationQString(), affected.getArchiveQString(),
                affected.getVariableQString()) : new DynamicSequenceSelection::Match(QString(),
                                                                                     QString(),
                                                                                     QString()),
                                           variable.isValid() ? new SequenceMatch::Composite(
                                                   SequenceMatch::Element(
                                                           variable.getStationQString(),
                                                           variable.getArchiveQString(),
                                                           variable.getVariableQString()))
                                                              : nullptr);
        f->start();
        output.reset();
        f->setEgress(&output);
        QTest::qSleep(50);
        for (std::size_t i = 0; i < input.size(); i++) {
            QTest::qSleep(50);
            f->incomingData(input.at(i));
        }
        f->endData();
        QVERIFY(f->wait(30));
        delete f;
        QVERIFY(output.ended());
        QVERIFY(engineCompare(output.values(), expected));
    }

    void editing_data()
    {
        QTest::addColumn<SequenceName>("affected");
        QTest::addColumn<SequenceName>("variable");
        QTest::addColumn<SequenceValue::Transfer>("input");
        QTest::addColumn<SequenceValue::Transfer>("expected");

        QTest::newRow("Empty") << SequenceName() << SequenceName() << (SequenceValue::Transfer())
                               << (SequenceValue::Transfer());

        QTest::newRow("Single no flags") << SequenceName() << SequenceName()
                                         << SequenceValue::Transfer{SequenceValue(
                                                 SequenceName("bnd", "raw", "BsG_S11"),
                                                 Variant::Root(1.0),
                                                 1000.0, 2000.0)} << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("bnd", "avg", "BsG_S11"), Variant::Root(1.0), 1000.0,
                              2000.0)};
        QTest::newRow("Single only meta") << SequenceName() << SequenceName()
                                          << SequenceValue::Transfer{SequenceValue(
                                                  SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                  cms("Thing", "Value", "Stuff"), 1000.0, 2000.0)}
                                          << SequenceValue::Transfer{SequenceValue(
                                                  SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                  cms("Thing", "Value", "Stuff"), 1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg_meta",
                                                                                          "BsG_S11"),
                                                                             cms("Thing", "Value",
                                                                                 "Stuff"), 1000.0,
                                                                             2000.0)};
        QTest::newRow("Single only set") << SequenceName() << SequenceName()
                                         << SequenceValue::Transfer{SequenceValue(
                                                 SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                 cmb("ContaminationRemoved", NULL, false), 1000.0,
                                                 2000.0)} << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, false), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "avg_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, true), 1000.0, 2000.0)};
        QTest::newRow("Single only flags") << SequenceName() << SequenceName()
                                           << (SequenceValue::Transfer{SequenceValue(
                                                   SequenceName("bnd", "raw", "F1_S11"),
                                                   Variant::Root(Variant::Flags{"Flag"}), 1000.0,
                                                   2000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Flag"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "avg", "F1_S11"),
                              Variant::Root(Variant::Flags{"Flag"}), 1000.0, 2000.0)});
        QTest::newRow("Single only flags contam") << SequenceName() << SequenceName()
                                                  << (SequenceValue::Transfer{SequenceValue(
                                                          SequenceName("bnd", "raw", "F1_S11"),
                                                          Variant::Root(
                                                                  Variant::Flags{"Contaminated"}),
                                                          1000.0, 2000.0)})
                                                  << (SequenceValue::Transfer{SequenceValue(
                                                          SequenceName("bnd", "raw", "F1_S11"),
                                                          Variant::Root(
                                                                  Variant::Flags{"Contaminated"}),
                                                          1000.0, 2000.0), SequenceValue(
                                                          SequenceName("bnd", "avg", "F1_S11"),
                                                          Variant::Root(
                                                                  Variant::Flags{"Contaminated"}),
                                                          1000.0, 2000.0)});

        QTest::newRow("Single no flags unaffected") << SequenceName("bnd", "raw", "BsB_S11")
                                                    << SequenceName() << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.0), 1000.0,
                              2000.0)}
                                                    << SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(1.0), 1000.0, 2000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "avg",
                                                                                               "BsG_S11"),
                                                                                       Variant::Root(
                                                                                               1.0),
                                                                                       1000.0,
                                                                                       2000.0)};
        QTest::newRow("Single no flags bypass") << SequenceName()
                                                << SequenceName("bnd", "raw", "BsB_S11")
                                                << SequenceValue::Transfer{SequenceValue(
                                                        SequenceName("bnd", "raw", "BsG_S11"),
                                                        Variant::Root(1.0), 1000.0, 2000.0)}
                                                << SequenceValue::Transfer{SequenceValue(
                                                        SequenceName("bnd", "raw", "BsG_S11"),
                                                        Variant::Root(1.0), 1000.0, 2000.0)};
        QTest::newRow("Single only meta unaffected") << SequenceName("bnd", "raw", "BsB_S11")
                                                     << SequenceName() << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cms("Thing", "Value", "Stuff"), 1000.0, 2000.0)}
                                                     << SequenceValue::Transfer{SequenceValue(
                                                             SequenceName("bnd", "raw_meta",
                                                                          "BsG_S11"),
                                                             cms("Thing", "Value", "Stuff"), 1000.0,
                                                             2000.0), SequenceValue(
                                                             SequenceName("bnd", "avg_meta",
                                                                          "BsG_S11"),
                                                             cms("Thing", "Value", "Stuff"), 1000.0,
                                                             2000.0)};
        QTest::newRow("Single only meta bypass") << SequenceName()
                                                 << SequenceName("bnd", "raw", "BsB_S11")
                                                 << SequenceValue::Transfer{SequenceValue(
                                                         SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                         cms("Thing", "Value", "Stuff"), 1000.0,
                                                         2000.0)} << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cms("Thing", "Value", "Stuff"), 1000.0, 2000.0)};
        QTest::newRow("Single only set unaffected") << SequenceName("bnd", "raw", "BsB_S11")
                                                    << SequenceName() << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, false), 1000.0, 2000.0)}
                                                    << SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw_meta",
                                                                         "BsG_S11"),
                                                            cmb("ContaminationRemoved", NULL,
                                                                false), 1000.0, 2000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "avg_meta",
                                                                                               "BsG_S11"),
                                                                                       cmb("ContaminationRemoved",
                                                                                           NULL,
                                                                                           false),
                                                                                       1000.0,
                                                                                       2000.0)};
        QTest::newRow("Single only set bypass") << SequenceName()
                                                << SequenceName("bnd", "raw", "BsB_S11")
                                                << SequenceValue::Transfer{SequenceValue(
                                                        SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                        cmb("ContaminationRemoved", NULL, false),
                                                        1000.0, 2000.0)} << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, false), 1000.0, 2000.0)};
        QTest::newRow("Single only flags unaffected") << SequenceName("bnd", "raw", "BsB_S11")
                                                      << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Flag"}), 1000.0, 2000.0)})
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              1000.0, 2000.0), SequenceValue(
                                                              SequenceName("bnd", "avg", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              1000.0, 2000.0)});
        QTest::newRow("Single only flags bypass") << SequenceName()
                                                  << SequenceName("bnd", "raw", "BsB_S11")
                                                  << (SequenceValue::Transfer{SequenceValue(
                                                          SequenceName("bnd", "raw", "F1_S11"),
                                                          Variant::Root(Variant::Flags{"Flag"}),
                                                          1000.0,
                                                          2000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Flag"}), 1000.0, 2000.0)});
        QTest::newRow("Single only flags contam unaffected")
                << SequenceName("bnd", "raw", "BsB_S11") << SequenceName()
                << (SequenceValue::Transfer{SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                          Variant::Root(
                                                                  Variant::Flags{"Contaminated"}),
                                                          1000.0, 2000.0)})
                << (SequenceValue::Transfer{SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                          Variant::Root(
                                                                  Variant::Flags{"Contaminated"}),
                                                          1000.0, 2000.0),
                                            SequenceValue(SequenceName("bnd", "avg", "F1_S11"),
                                                          Variant::Root(
                                                                  Variant::Flags{"Contaminated"}),
                                                          1000.0, 2000.0)});
        QTest::newRow("Single only flags contam bypass") << SequenceName()
                                                         << SequenceName("bnd", "raw", "BsB_S11")
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("bnd", "raw",
                                                                              "F1_S11"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "Contaminated"}), 1000.0,
                                                                 2000.0)})
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("bnd", "raw",
                                                                              "F1_S11"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "Contaminated"}), 1000.0,
                                                                 2000.0)});

        QTest::newRow("Two no flags") << SequenceName() << SequenceName()
                                      << SequenceValue::Transfer{
                                              SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(1.0), 1000.0, 2000.0),
                                              SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(2.0), 2000.0, 3000.0)}
                                      << SequenceValue::Transfer{
                                              SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(1.0), 1000.0, 2000.0),
                                              SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(2.0), 2000.0, 3000.0),
                                              SequenceValue(SequenceName("bnd", "avg", "BsG_S11"),
                                                            Variant::Root(1.0), 1000.0, 2000.0),
                                              SequenceValue(SequenceName("bnd", "avg", "BsG_S11"),
                                                            Variant::Root(2.0), 2000.0, 3000.0)};
        QTest::newRow("Two only meta") << SequenceName() << SequenceName()
                                       << SequenceValue::Transfer{SequenceValue(
                                               SequenceName("bnd", "raw_meta", "BsG_S11"),
                                               cms("Thing", "Value", "Stuff"), 1000.0, 2000.0),
                                                                  SequenceValue(SequenceName("bnd",
                                                                                             "raw_meta",
                                                                                             "BsG_S11"),
                                                                                cms("Thing",
                                                                                    "Value",
                                                                                    "Stuff2"),
                                                                                2000.0, 3000.0)}
                                       << SequenceValue::Transfer{SequenceValue(
                                               SequenceName("bnd", "raw_meta", "BsG_S11"),
                                               cms("Thing", "Value", "Stuff"), 1000.0, 2000.0),
                                                                  SequenceValue(SequenceName("bnd",
                                                                                             "raw_meta",
                                                                                             "BsG_S11"),
                                                                                cms("Thing",
                                                                                    "Value",
                                                                                    "Stuff2"),
                                                                                2000.0, 3000.0),
                                                                  SequenceValue(SequenceName("bnd",
                                                                                             "avg_meta",
                                                                                             "BsG_S11"),
                                                                                cms("Thing",
                                                                                    "Value",
                                                                                    "Stuff"),
                                                                                1000.0, 2000.0),
                                                                  SequenceValue(SequenceName("bnd",
                                                                                             "avg_meta",
                                                                                             "BsG_S11"),
                                                                                cms("Thing",
                                                                                    "Value",
                                                                                    "Stuff2"),
                                                                                2000.0, 3000.0)};
        QTest::newRow("Two only set") << SequenceName() << SequenceName()
                                      << SequenceValue::Transfer{SequenceValue(
                                              SequenceName("bnd", "raw_meta", "BsG_S11"),
                                              cmb("ContaminationRemoved", NULL, false), 1000.0,
                                              2000.0), SequenceValue(
                                              SequenceName("bnd", "raw_meta", "BsG_S11"),
                                              cmb("ContaminationRemoved", NULL, false), 2000.0,
                                              3000.0)} << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, false), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, false), 2000.0, 3000.0),
                SequenceValue(SequenceName("bnd", "avg_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, true), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "avg_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, true), 2000.0, 3000.0)};
        QTest::newRow("Two only flags") << SequenceName() << SequenceName()
                                        << (SequenceValue::Transfer{
                                                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              1000.0, 2000.0),
                                                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              2000.0, 3000.0)})
                                        << (SequenceValue::Transfer{
                                                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              1000.0, 2000.0),
                                                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              2000.0, 3000.0),
                                                SequenceValue(SequenceName("bnd", "avg", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              1000.0, 2000.0),
                                                SequenceValue(SequenceName("bnd", "avg", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              2000.0, 3000.0)});
        QTest::newRow("Two only flags contam") << SequenceName() << SequenceName()
                                               << (SequenceValue::Transfer{SequenceValue(
                                                       SequenceName("bnd", "raw", "F1_S11"),
                                                       Variant::Root(
                                                               Variant::Flags{"Contaminated"}),
                                                       1000.0, 2000.0), SequenceValue(
                                                       SequenceName("bnd", "raw", "F1_S11"),
                                                       Variant::Root(
                                                               Variant::Flags{"Contaminated"}),
                                                       2000.0, 3000.0)})
                                               << (SequenceValue::Transfer{SequenceValue(
                                                       SequenceName("bnd", "raw", "F1_S11"),
                                                       Variant::Root(
                                                               Variant::Flags{"Contaminated"}),
                                                       1000.0, 2000.0), SequenceValue(
                                                       SequenceName("bnd", "raw", "F1_S11"),
                                                       Variant::Root(
                                                               Variant::Flags{"Contaminated"}),
                                                       2000.0, 3000.0), SequenceValue(
                                                       SequenceName("bnd", "avg", "F1_S11"),
                                                       Variant::Root(
                                                               Variant::Flags{"Contaminated"}),
                                                       1000.0, 2000.0), SequenceValue(
                                                       SequenceName("bnd", "avg", "F1_S11"),
                                                       Variant::Root(
                                                               Variant::Flags{"Contaminated"}),
                                                       2000.0, 3000.0)});


        QTest::newRow("Single contaminated") << SequenceName() << SequenceName()
                                             << (SequenceValue::Transfer{SequenceValue(
                                                     SequenceName("bnd", "raw", "F1_S11"),
                                                     Variant::Root(Variant::Flags{"Contaminated"}),
                                                     1000.0, 2000.0), SequenceValue(
                                                     SequenceName("bnd", "raw", "BsG_S11"),
                                                     Variant::Root(2.0), 1000.0, 2000.0)})
                                             << (SequenceValue::Transfer{SequenceValue(
                                                     SequenceName("bnd", "raw", "F1_S11"),
                                                     Variant::Root(Variant::Flags{"Contaminated"}),
                                                     1000.0, 2000.0), SequenceValue(
                                                     SequenceName("bnd", "raw", "BsG_S11"),
                                                     Variant::Root(2.0), 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd", "avg", "F1_S11"),
                                                                                 Variant::Root(
                                                                                         Variant::Flags{
                                                                                                 "Contaminated"}),
                                                                                 1000.0, 2000.0), SequenceValue(
                                                             SequenceName("bnd", "avg", "BsG_S11"),
                                                             Variant::Root(FP::undefined()), 1000.0,
                                                             2000.0)});
        QTest::newRow("Single contaminated reversed") << SequenceName() << SequenceName()
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("bnd", "raw", "BsG_S11"),
                                                              Variant::Root(2.0), 1000.0, 2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "bnd",
                                                                                                  "raw",
                                                                                                  "F1_S11"),
                                                                                          Variant::Root(
                                                                                                  Variant::Flags{
                                                                                                          "Contaminated"}),
                                                                                          1000.0,
                                                                                          2000.0)})
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("bnd", "raw", "BsG_S11"),
                                                              Variant::Root(2.0), 1000.0, 2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "bnd",
                                                                                                  "raw",
                                                                                                  "F1_S11"),
                                                                                          Variant::Root(
                                                                                                  Variant::Flags{
                                                                                                          "Contaminated"}),
                                                                                          1000.0,
                                                                                          2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "bnd",
                                                                                                  "avg",
                                                                                                  "F1_S11"),
                                                                                          Variant::Root(
                                                                                                  Variant::Flags{
                                                                                                          "Contaminated"}),
                                                                                          1000.0,
                                                                                          2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "bnd",
                                                                                                  "avg",
                                                                                                  "BsG_S11"),
                                                                                          Variant::Root(
                                                                                                  FP::undefined()),
                                                                                          1000.0,
                                                                                          2000.0)});
        QTest::newRow("Single contaminated internal") << SequenceName() << SequenceName()
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{
                                                                      "Contaminated"}), 1000.0,
                                                              2000.0), SequenceValue(
                                                              SequenceName("bnd", "raw", "BsG_S11"),
                                                              Variant::Root(2.0), 1100.0, 1900.0)})
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{
                                                                      "Contaminated"}), 1000.0,
                                                              2000.0), SequenceValue(
                                                              SequenceName("bnd", "raw", "BsG_S11"),
                                                              Variant::Root(2.0), 1100.0, 1900.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "bnd",
                                                                                                  "avg",
                                                                                                  "F1_S11"),
                                                                                          Variant::Root(
                                                                                                  Variant::Flags{
                                                                                                          "Contaminated"}),
                                                                                          1000.0,
                                                                                          2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "bnd",
                                                                                                  "avg",
                                                                                                  "BsG_S11"),
                                                                                          Variant::Root(
                                                                                                  FP::undefined()),
                                                                                          1100.0,
                                                                                          1900.0)});
        QTest::newRow("Single contaminated before") << SequenceName() << SequenceName()
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(2.0), 900.0, 2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "F1_S11"),
                                                                                        Variant::Root(
                                                                                                Variant::Flags{
                                                                                                        "Contaminated"}),
                                                                                        1000.0,
                                                                                        2000.0)})
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(2.0), 900.0, 2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "F1_S11"),
                                                                                        Variant::Root(
                                                                                                Variant::Flags{
                                                                                                        "Contaminated"}),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "avg",
                                                                                                "F1_S11"),
                                                                                        Variant::Root(
                                                                                                Variant::Flags{
                                                                                                        "Contaminated"}),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "avg",
                                                                                                "BsG_S11"),
                                                                                        Variant::Root(
                                                                                                2.0),
                                                                                        900.0,
                                                                                        1000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "avg",
                                                                                                "BsG_S11"),
                                                                                        Variant::Root(
                                                                                                FP::undefined()),
                                                                                        1000.0,
                                                                                        2000.0)});
        QTest::newRow("Single contaminated before contained") << SequenceName() << SequenceName()
                                                              << (SequenceValue::Transfer{
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "raw",
                                                                                           "BsG_S11"),
                                                                              Variant::Root(2.0),
                                                                              900.0,
                                                                              1900.0),
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "raw",
                                                                                           "F1_S11"),
                                                                              Variant::Root(
                                                                                      Variant::Flags{
                                                                                              "Contaminated"}),
                                                                              1000.0, 2000.0)})
                                                              << (SequenceValue::Transfer{
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "raw",
                                                                                           "BsG_S11"),
                                                                              Variant::Root(2.0),
                                                                              900.0,
                                                                              1900.0),
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "raw",
                                                                                           "F1_S11"),
                                                                              Variant::Root(
                                                                                      Variant::Flags{
                                                                                              "Contaminated"}),
                                                                              1000.0, 2000.0),
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "avg",
                                                                                           "F1_S11"),
                                                                              Variant::Root(
                                                                                      Variant::Flags{
                                                                                              "Contaminated"}),
                                                                              1000.0, 2000.0),
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "avg",
                                                                                           "BsG_S11"),
                                                                              Variant::Root(2.0),
                                                                              900.0,
                                                                              1000.0),
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "avg",
                                                                                           "BsG_S11"),
                                                                              Variant::Root(
                                                                                      FP::undefined()),
                                                                              1000.0, 1900.0)});
        QTest::newRow("Single contaminated after") << SequenceName() << SequenceName()
                                                   << (SequenceValue::Transfer{SequenceValue(
                                                           SequenceName("bnd", "raw", "F1_S11"),
                                                           Variant::Root(
                                                                   Variant::Flags{"Contaminated"}),
                                                           1000.0, 2000.0), SequenceValue(
                                                           SequenceName("bnd", "raw", "BsG_S11"),
                                                           Variant::Root(2.0), 1000.0, 2100.0)})
                                                   << (SequenceValue::Transfer{SequenceValue(
                                                           SequenceName("bnd", "raw", "F1_S11"),
                                                           Variant::Root(
                                                                   Variant::Flags{"Contaminated"}),
                                                           1000.0, 2000.0), SequenceValue(
                                                           SequenceName("bnd", "raw", "BsG_S11"),
                                                           Variant::Root(2.0), 1000.0, 2100.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "avg",
                                                                                               "F1_S11"),
                                                                                       Variant::Root(
                                                                                               Variant::Flags{
                                                                                                       "Contaminated"}),
                                                                                       1000.0,
                                                                                       2000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "avg",
                                                                                               "BsG_S11"),
                                                                                       Variant::Root(
                                                                                               2.0),
                                                                                       2000.0,
                                                                                       2100.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "avg",
                                                                                               "BsG_S11"),
                                                                                       Variant::Root(
                                                                                               FP::undefined()),
                                                                                       1000.0,
                                                                                       2000.0)});
        QTest::newRow("Single contaminated after contained") << SequenceName() << SequenceName()
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             1100.0,
                                                                             2100.0)})
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             1100.0,
                                                                             2100.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             2000.0,
                                                                             2100.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(
                                                                                     FP::undefined()),
                                                                             1100.0, 2000.0)});

        QTest::newRow("Contaminated middle switch") << SequenceName() << SequenceName()
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "F1_S11"),
                                                            Variant::Root(
                                                                    Variant::Flags{"Contaminated"}),
                                                            1000.0,
                                                            2000.0), SequenceValue(
                                                            SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(2.0), 1000.0, 3000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "F1_S11"),
                                                                                        Variant::Root(
                                                                                                Variant::Flags()),
                                                                                        2000.0,
                                                                                        3000.0)})
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "F1_S11"),
                                                            Variant::Root(
                                                                    Variant::Flags{"Contaminated"}),
                                                            1000.0,
                                                            2000.0), SequenceValue(
                                                            SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(2.0), 1000.0, 3000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "F1_S11"),
                                                                                        Variant::Root(
                                                                                                Variant::Flags()),
                                                                                        2000.0,
                                                                                        3000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "avg",
                                                                                                "F1_S11"),
                                                                                        Variant::Root(
                                                                                                Variant::Flags{
                                                                                                        "Contaminated"}),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "avg",
                                                                                                "F1_S11"),
                                                                                        Variant::Root(
                                                                                                Variant::Flags()),
                                                                                        2000.0,
                                                                                        3000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "avg",
                                                                                                "BsG_S11"),
                                                                                        Variant::Root(
                                                                                                FP::undefined()),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "avg",
                                                                                                "BsG_S11"),
                                                                                        Variant::Root(
                                                                                                2.0),
                                                                                        2000.0,
                                                                                        3000.0)});
        QTest::newRow("Contaminated middle switch reversed") << SequenceName() << SequenceName()
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             1000.0,
                                                                             3000.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0)})
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             1000.0,
                                                                             3000.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(
                                                                                     FP::undefined()),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             2000.0,
                                                                             3000.0)});
        QTest::newRow("Contaminated middle switch extended") << SequenceName() << SequenceName()
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             900.0,
                                                                             3100.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0)})
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             900.0,
                                                                             3100.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             900.0,
                                                                             1000.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(
                                                                                     FP::undefined()),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             2000.0,
                                                                             3000.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "avg",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             3000.0,
                                                                             3100.0)});

        QTest::newRow("Metadata contamination disabled") << SequenceName() << SequenceName()
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("bnd", "raw",
                                                                              "F1_S11"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "Contaminated"}), 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("bnd", "raw_meta",
                                                                              "BsG_S11"),
                                                                 cmb("ContaminationRemoved", NULL,
                                                                     true), 1000.0, 2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "BsG_S11"),
                                                                                             Variant::Root(
                                                                                                     2.0),
                                                                                             1000.0,
                                                                                             2000.0)})
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("bnd", "raw",
                                                                              "F1_S11"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "Contaminated"}), 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("bnd", "raw_meta",
                                                                              "BsG_S11"),
                                                                 cmb("ContaminationRemoved", NULL,
                                                                     true), 1000.0, 2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "BsG_S11"),
                                                                                             Variant::Root(
                                                                                                     2.0),
                                                                                             1000.0,
                                                                                             2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "avg",
                                                                                                     "F1_S11"),
                                                                                             Variant::Root(
                                                                                                     Variant::Flags{
                                                                                                             "Contaminated"}),
                                                                                             1000.0,
                                                                                             2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "avg_meta",
                                                                                                     "BsG_S11"),
                                                                                             cmb("ContaminationRemoved",
                                                                                                 NULL,
                                                                                                 true),
                                                                                             1000.0,
                                                                                             2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "avg",
                                                                                                     "BsG_S11"),
                                                                                             Variant::Root(
                                                                                                     2.0),
                                                                                             1000.0,
                                                                                             2000.0)});
        QTest::newRow("Metadata contamination disabled reorder") << SequenceName() << SequenceName()
                                                                 << (SequenceValue::Transfer{
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw_meta",
                                                                                              "BsG_S11"),
                                                                                 cmb("ContaminationRemoved",
                                                                                     NULL, true),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "F1_S11"),
                                                                                 Variant::Root(
                                                                                         Variant::Flags{
                                                                                                 "Contaminated"}),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "BsG_S11"),
                                                                                 Variant::Root(2.0),
                                                                                 1000.0,
                                                                                 2000.0)})
                                                                 << (SequenceValue::Transfer{
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw_meta",
                                                                                              "BsG_S11"),
                                                                                 cmb("ContaminationRemoved",
                                                                                     NULL, true),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "F1_S11"),
                                                                                 Variant::Root(
                                                                                         Variant::Flags{
                                                                                                 "Contaminated"}),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "BsG_S11"),
                                                                                 Variant::Root(2.0),
                                                                                 1000.0,
                                                                                 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "avg",
                                                                                              "F1_S11"),
                                                                                 Variant::Root(
                                                                                         Variant::Flags{
                                                                                                 "Contaminated"}),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "avg_meta",
                                                                                              "BsG_S11"),
                                                                                 cmb("ContaminationRemoved",
                                                                                     NULL, true),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "avg",
                                                                                              "BsG_S11"),
                                                                                 Variant::Root(2.0),
                                                                                 1000.0,
                                                                                 2000.0)});

        QTest::newRow("Metadata contamination disabled transition") << SequenceName()
                                                                    << SequenceName()
                                                                    << (SequenceValue::Transfer{
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "F1_S11"),
                                                                                    Variant::Root(
                                                                                            Variant::Flags{
                                                                                                    "Contaminated"}),
                                                                                    1000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL, true),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            1.0),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL,
                                                                                        false),
                                                                                    2000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            2.0),
                                                                                    2000.0,
                                                                                    3000.0)})
                                                                    << (SequenceValue::Transfer{
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "F1_S11"),
                                                                                    Variant::Root(
                                                                                            Variant::Flags{
                                                                                                    "Contaminated"}),
                                                                                    1000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL, true),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            1.0),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL,
                                                                                        false),
                                                                                    2000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            2.0),
                                                                                    2000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "avg",
                                                                                            "F1_S11"),
                                                                                    Variant::Root(
                                                                                            Variant::Flags{
                                                                                                    "Contaminated"}),
                                                                                    1000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "avg_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL, true),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "avg",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            1.0),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "avg_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL, true),
                                                                                    2000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "avg",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            FP::undefined()),
                                                                                    2000.0,
                                                                                    3000.0)});

        Variant::Root meta;
        meta.write().metadataSingleFlag("ContaminateBlarg").hash("Stuff").setString("A");
        QTest::newRow("Metadata alternate flag") << SequenceName() << SequenceName()
                                                 << (SequenceValue::Transfer{SequenceValue(
                                                         SequenceName("bnd", "raw_meta", "F1_S11"),
                                                         meta, 1000.0, 2000.0), SequenceValue(
                                                         SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags{
                                                                 "ContaminateBlarg"}), 1000.0,
                                                         2000.0), SequenceValue(
                                                         SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(1.0), 1000.0, 2000.0)})
                                                 << (SequenceValue::Transfer{SequenceValue(
                                                         SequenceName("bnd", "raw_meta", "F1_S11"),
                                                         meta, 1000.0, 2000.0), SequenceValue(
                                                         SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags{
                                                                 "ContaminateBlarg"}), 1000.0,
                                                         2000.0), SequenceValue(
                                                         SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(1.0), 1000.0, 2000.0),
                                                                             SequenceValue(
                                                         SequenceName("bnd", "avg_meta", "F1_S11"),
                                                         meta, 1000.0, 2000.0), SequenceValue(
                                                                 SequenceName("bnd", "avg", "F1_S11"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "ContaminateBlarg"}),
                                                                 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("bnd", "avg", "BsG_S11"),
                                                                 Variant::Root(FP::undefined()),
                                                                 1000.0, 2000.0)});

        QTest::newRow("Bypass archive discard") << SequenceName()
                                                << SequenceName("bnd", "raw", "BsB_S11")
                                                << SequenceValue::Transfer{SequenceValue(
                                                        SequenceName("bnd", "raw", "BsG_S11"),
                                                        Variant::Root(1.0), 1000.0, 2000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("bnd", "avg", "BsG_S11"),
                                                                                   Variant::Root(
                                                                                           2.0),
                                                                                   1000.0, 2000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("bnd", "cont", "BsG_S11"),
                                                                                   Variant::Root(
                                                                                           3.0),
                                                                                   1000.0, 2000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("bnd", "raw", "BsB_S11"),
                                                                                   Variant::Root(
                                                                                           4.0),
                                                                                   1000.0, 2000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("bnd", "avg", "BsB_S11"),
                                                                                   Variant::Root(
                                                                                           5.0),
                                                                                   1000.0, 2000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("bnd", "cont", "BsB_S11"),
                                                                                   Variant::Root(
                                                                                           6.0),
                                                                                   1000.0, 2000.0)}
                                                << SequenceValue::Transfer{SequenceValue(
                                                        SequenceName("bnd", "raw", "BsG_S11"),
                                                        Variant::Root(1.0), 1000.0, 2000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("bnd", "avg", "BsG_S11"),
                                                                                   Variant::Root(
                                                                                           2.0),
                                                                                   1000.0, 2000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("bnd", "cont", "BsG_S11"),
                                                                                   Variant::Root(
                                                                                           3.0),
                                                                                   1000.0, 2000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("bnd", "raw", "BsB_S11"),
                                                                                   Variant::Root(
                                                                                           4.0),
                                                                                   1000.0, 2000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("bnd", "avg", "BsB_S11"),
                                                                                   Variant::Root(
                                                                                           4.0),
                                                                                   1000.0, 2000.0)};

        QTest::newRow("Complex A") << SequenceName("bnd", "raw", "BsG_S11") << SequenceName()
                                   << (SequenceValue::Transfer{
                                           SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"),
                                                         meta, 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                         cmb("ContaminationRemoved", NULL, false),
                                                         1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "BsB_S11"),
                                                         cms("A", "B", "C"), 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(1.0), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(1.1), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags{"Stuff"}),
                                                         2000.0,
                                                         3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(2.0), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(2.1), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(3.0), 3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(3.1), 3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 4000.0,
                                                         5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(4.0), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(4.1), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 5000.0,
                                                         6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(5.0), 5000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(5.1), 5000.0, 6000.0)})
                                   << (SequenceValue::Transfer{
                                           SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"),
                                                         meta, 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                         cmb("ContaminationRemoved", NULL, false),
                                                         1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "BsB_S11"),
                                                         cms("A", "B", "C"), 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(1.0), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(1.1), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags{"Stuff"}),
                                                         2000.0,
                                                         3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(2.0), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(2.1), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(3.0), 3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(3.1), 3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 4000.0,
                                                         5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(4.0), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(4.1), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 5000.0,
                                                         6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(5.0), 5000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(5.1), 5000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "avg_meta", "F1_S11"),
                                                         meta, 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "avg_meta", "BsG_S11"),
                                                         cmb("ContaminationRemoved", NULL, true),
                                                         1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "avg_meta", "BsB_S11"),
                                                         cms("A", "B", "C"), 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "BsG_S11"),
                                                         Variant::Root(FP::undefined()), 1000.0,
                                                         2000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "BsB_S11"),
                                                         Variant::Root(1.1), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "F1_S11"),
                                                         Variant::Root(Variant::Flags{"Stuff"}),
                                                         2000.0,
                                                         3000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "BsG_S11"),
                                                         Variant::Root(2.0), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "BsB_S11"),
                                                         Variant::Root(2.1), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "BsG_S11"),
                                                         Variant::Root(FP::undefined()), 3000.0,
                                                         4000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "BsB_S11"),
                                                         Variant::Root(3.1), 3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 4000.0,
                                                         5000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "BsG_S11"),
                                                         Variant::Root(4.0), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "BsB_S11"),
                                                         Variant::Root(4.1), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 5000.0,
                                                         6000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "BsG_S11"),
                                                         Variant::Root(5.0), 5000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "avg", "BsB_S11"),
                                                         Variant::Root(5.1), 5000.0, 6000.0)});
    }

    void editingFinal()
    {
        QFETCH(SequenceName, affected);
        QFETCH(SequenceValue::Transfer, input);
        QFETCH(SequenceValue::Transfer, expected);

        StreamSink::Buffer output;
        EditingContaminationFilter *f = nullptr;

        f = new EditingContaminationFinal(affected.isValid() ? new DynamicSequenceSelection::Match(
                affected.getStationQString(), affected.getArchiveQString(),
                affected.getVariableQString()) : new DynamicSequenceSelection::Match(QString(),
                                                                                     QString(),
                                                                                     QString()));
        f->start();
        f->setEgress(&output);
        f->incomingData(input);
        f->endData();
        QVERIFY(f->wait(30));
        delete f;
        QVERIFY(output.ended());
        QVERIFY(engineCompare(output.values(), expected));

        f = new EditingContaminationFinal(affected.isValid() ? new DynamicSequenceSelection::Match(
                affected.getStationQString(), affected.getArchiveQString(),
                affected.getVariableQString()) : new DynamicSequenceSelection::Match(QString(),
                                                                                     QString(),
                                                                                     QString())

        );
        f->start();

        output.reset();
        f->setEgress(&output);
        std::size_t half = input.size() / 2;
        for (std::size_t i = 0; i < half; i++) {
            f->incomingData(input.at(i));
        }
        QTest::qSleep(50);
        {
            f->setEgress(nullptr);
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                f->serialize(stream);
            }
            f->endData();
            f->signalTerminate();
            QVERIFY(f->wait(30));
            delete f;
            f = new EditingContaminationFinal(nullptr);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                f->deserialize(stream);
            }
            f->start();
            f->setEgress(&output);
        }
        for (std::size_t i = half; i < input.size(); i++) {
            f->incomingData(input.at(i));
        }
        f->endData();

        QVERIFY(f->wait(30));
        delete f;
        QVERIFY(output.ended());
        QVERIFY(engineCompare(output.values(), expected));

        f = new EditingContaminationFinal(affected.isValid() ? new DynamicSequenceSelection::Match(
                affected.getStationQString(), affected.getArchiveQString(),
                affected.getVariableQString()) : new DynamicSequenceSelection::Match(QString(),
                                                                                     QString(),
                                                                                     QString()));
        f->start();

        output.reset();
        f->setEgress(&output);
        QTest::qSleep(50);
        for (std::size_t i = 0; i < input.size(); i++) {
            QTest::qSleep(50);
            f->incomingData(input.at(i));
        }
        f->endData();

        QVERIFY(f->wait(30));
        delete f;
        QVERIFY(output.ended());
        QVERIFY(engineCompare(output.values(), expected));
    }

    void editingFinal_data()
    {
        QTest::addColumn<SequenceName>("affected");
        QTest::addColumn<SequenceValue::Transfer>("input");
        QTest::addColumn<SequenceValue::Transfer>("expected");

        QTest::newRow("Empty") << SequenceName() << (SequenceValue::Transfer())
                               << (SequenceValue::Transfer());

        QTest::newRow("Single no flags") << SequenceName() << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.0), 1000.0,
                              2000.0)}
                                         << SequenceValue::Transfer{SequenceValue(
                                                 SequenceName("bnd", "raw", "BsG_S11"),
                                                 Variant::Root(1.0),
                                                 1000.0, 2000.0), SequenceValue(
                                                 SequenceName("bnd", "stats", "BsG_S11"),
                                                 Variant::Root(1.0), 1000.0, 2000.0)};
        QTest::newRow("Single only meta") << SequenceName() << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cms("Thing", "Value", "Stuff"), 1000.0, 2000.0)}
                                          << SequenceValue::Transfer{SequenceValue(
                                                  SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                  cms("Thing", "Value", "Stuff"), 1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats_meta",
                                                                                          "BsG_S11"),
                                                                             cms("Thing", "Value",
                                                                                 "Stuff"), 1000.0,
                                                                             2000.0)};
        QTest::newRow("Single only set") << SequenceName() << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, false), 1000.0, 2000.0)}
                                         << SequenceValue::Transfer{SequenceValue(
                                                 SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                 cmb("ContaminationRemoved", NULL, false), 1000.0,
                                                 2000.0), SequenceValue(
                                                 SequenceName("bnd", "stats_meta", "BsG_S11"),
                                                 cmb("ContaminationRemoved", NULL, true), 1000.0,
                                                 2000.0)};
        QTest::newRow("Single only flags") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Flag"}), 1000.0, 2000.0)})
                                           << (SequenceValue::Transfer{SequenceValue(
                                                   SequenceName("bnd", "raw", "F1_S11"),
                                                   Variant::Root(Variant::Flags{"Flag"}), 1000.0,
                                                   2000.0), SequenceValue(
                                                   SequenceName("bnd", "stats", "F1_S11"),
                                                   Variant::Root(Variant::Flags{"Flag"}), 1000.0,
                                                   2000.0)});
        QTest::newRow("Single only flags contam") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0)})
                                                  << (SequenceValue::Transfer{SequenceValue(
                                                          SequenceName("bnd", "raw", "F1_S11"),
                                                          Variant::Root(
                                                                  Variant::Flags{"Contaminated"}),
                                                          1000.0, 2000.0), SequenceValue(
                                                          SequenceName("bnd", "stats", "F1_S11"),
                                                          Variant::Root(
                                                                  Variant::Flags{"Contaminated"}),
                                                          1000.0, 2000.0)});

        QTest::newRow("Single no flags unaffected") << SequenceName("bnd", "raw", "BsB_S11")
                                                    << SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(1.0), 1000.0, 2000.0)}
                                                    << SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(1.0), 1000.0, 2000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "stats",
                                                                                               "BsG_S11"),
                                                                                       Variant::Root(
                                                                                               1.0),
                                                                                       1000.0,
                                                                                       2000.0)};

        QTest::newRow("Single only meta unaffected") << SequenceName("bnd", "raw", "BsB_S11")
                                                     << SequenceValue::Transfer{SequenceValue(
                                                             SequenceName("bnd", "raw_meta",
                                                                          "BsG_S11"),
                                                             cms("Thing", "Value", "Stuff"), 1000.0,
                                                             2000.0)} << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cms("Thing", "Value", "Stuff"), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "stats_meta", "BsG_S11"),
                              cms("Thing", "Value", "Stuff"), 1000.0, 2000.0)};
        QTest::newRow("Single only set unaffected") << SequenceName("bnd", "raw", "BsB_S11")
                                                    << SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw_meta",
                                                                         "BsG_S11"),
                                                            cmb("ContaminationRemoved", NULL,
                                                                false), 1000.0, 2000.0)}
                                                    << SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw_meta",
                                                                         "BsG_S11"),
                                                            cmb("ContaminationRemoved", NULL,
                                                                false), 1000.0, 2000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "stats_meta",
                                                                                               "BsG_S11"),
                                                                                       cmb("ContaminationRemoved",
                                                                                           NULL,
                                                                                           false),
                                                                                       1000.0,
                                                                                       2000.0)};
        QTest::newRow("Single only flags contam unaffected")
                << SequenceName("bnd", "raw", "BsB_S11") << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0)})
                << (SequenceValue::Transfer{SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                          Variant::Root(
                                                                  Variant::Flags{"Contaminated"}),
                                                          1000.0, 2000.0),
                                            SequenceValue(SequenceName("bnd", "stats", "F1_S11"),
                                                          Variant::Root(
                                                                  Variant::Flags{"Contaminated"}),
                                                          1000.0, 2000.0)});

        QTest::newRow("Two no flags") << SequenceName() << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 2000.0,
                              3000.0)}
                                      << SequenceValue::Transfer{
                                              SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(1.0), 1000.0, 2000.0),
                                              SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(2.0), 2000.0, 3000.0),
                                              SequenceValue(SequenceName("bnd", "stats", "BsG_S11"),
                                                            Variant::Root(1.0), 1000.0, 2000.0),
                                              SequenceValue(SequenceName("bnd", "stats", "BsG_S11"),
                                                            Variant::Root(2.0), 2000.0, 3000.0)};
        QTest::newRow("Two only meta") << SequenceName() << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cms("Thing", "Value", "Stuff"), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cms("Thing", "Value", "Stuff2"), 2000.0, 3000.0)}
                                       << SequenceValue::Transfer{SequenceValue(
                                               SequenceName("bnd", "raw_meta", "BsG_S11"),
                                               cms("Thing", "Value", "Stuff"), 1000.0, 2000.0),
                                                                  SequenceValue(SequenceName("bnd",
                                                                                             "raw_meta",
                                                                                             "BsG_S11"),
                                                                                cms("Thing",
                                                                                    "Value",
                                                                                    "Stuff2"),
                                                                                2000.0, 3000.0),
                                                                  SequenceValue(SequenceName("bnd",
                                                                                             "stats_meta",
                                                                                             "BsG_S11"),
                                                                                cms("Thing",
                                                                                    "Value",
                                                                                    "Stuff"),
                                                                                1000.0, 2000.0),
                                                                  SequenceValue(SequenceName("bnd",
                                                                                             "stats_meta",
                                                                                             "BsG_S11"),
                                                                                cms("Thing",
                                                                                    "Value",
                                                                                    "Stuff2"),
                                                                                2000.0, 3000.0)};
        QTest::newRow("Two only set") << SequenceName() << SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, false), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                              cmb("ContaminationRemoved", NULL, false), 2000.0, 3000.0)}
                                      << SequenceValue::Transfer{SequenceValue(
                                              SequenceName("bnd", "raw_meta", "BsG_S11"),
                                              cmb("ContaminationRemoved", NULL, false), 1000.0,
                                              2000.0), SequenceValue(
                                              SequenceName("bnd", "raw_meta", "BsG_S11"),
                                              cmb("ContaminationRemoved", NULL, false), 2000.0,
                                              3000.0), SequenceValue(
                                              SequenceName("bnd", "stats_meta", "BsG_S11"),
                                              cmb("ContaminationRemoved", NULL, true), 1000.0,
                                              2000.0), SequenceValue(
                                              SequenceName("bnd", "stats_meta", "BsG_S11"),
                                              cmb("ContaminationRemoved", NULL, true), 2000.0,
                                              3000.0)};
        QTest::newRow("Two only flags") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Flag"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Flag"}), 2000.0, 3000.0)})
                                        << (SequenceValue::Transfer{
                                                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              1000.0, 2000.0),
                                                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"Flag"}),
                                                              2000.0, 3000.0), SequenceValue(
                                                        SequenceName("bnd", "stats", "F1_S11"),
                                                        Variant::Root(Variant::Flags{"Flag"}),
                                                        1000.0,
                                                        2000.0), SequenceValue(
                                                        SequenceName("bnd", "stats", "F1_S11"),
                                                        Variant::Root(Variant::Flags{"Flag"}),
                                                        2000.0,
                                                        3000.0)});
        QTest::newRow("Two only flags contam") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 2000.0, 3000.0)})
                                               << (SequenceValue::Transfer{SequenceValue(
                                                       SequenceName("bnd", "raw", "F1_S11"),
                                                       Variant::Root(
                                                               Variant::Flags{"Contaminated"}),
                                                       1000.0, 2000.0), SequenceValue(
                                                       SequenceName("bnd", "raw", "F1_S11"),
                                                       Variant::Root(
                                                               Variant::Flags{"Contaminated"}),
                                                       2000.0, 3000.0), SequenceValue(
                                                       SequenceName("bnd", "stats", "F1_S11"),
                                                       Variant::Root(
                                                               Variant::Flags{"Contaminated"}),
                                                       1000.0, 2000.0), SequenceValue(
                                                       SequenceName("bnd", "stats", "F1_S11"),
                                                       Variant::Root(
                                                               Variant::Flags{"Contaminated"}),
                                                       2000.0, 3000.0)});


        QTest::newRow("Single contaminated") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 1000.0,
                              2000.0)})
                                             << (SequenceValue::Transfer{SequenceValue(
                                                     SequenceName("bnd", "raw", "F1_S11"),
                                                     Variant::Root(Variant::Flags{"Contaminated"}),
                                                     1000.0, 2000.0), SequenceValue(
                                                     SequenceName("bnd", "raw", "BsG_S11"),
                                                     Variant::Root(2.0), 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd", "stats", "F1_S11"),
                                                                                 Variant::Root(
                                                                                         Variant::Flags{
                                                                                                 "Contaminated"}),
                                                                                 1000.0, 2000.0), SequenceValue(
                                                             SequenceName("bnd", "stats", "BsG_S11"),
                                                             Variant::Root(FP::undefined()), 1000.0,
                                                             2000.0)});
        QTest::newRow("Single contaminated reversed") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0)})
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("bnd", "raw", "BsG_S11"),
                                                              Variant::Root(2.0), 1000.0, 2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "bnd",
                                                                                                  "raw",
                                                                                                  "F1_S11"),
                                                                                          Variant::Root(
                                                                                                  Variant::Flags{
                                                                                                          "Contaminated"}),
                                                                                          1000.0,
                                                                                          2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "bnd",
                                                                                                  "stats",
                                                                                                  "F1_S11"),
                                                                                          Variant::Root(
                                                                                                  Variant::Flags{
                                                                                                          "Contaminated"}),
                                                                                          1000.0,
                                                                                          2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "bnd",
                                                                                                  "stats",
                                                                                                  "BsG_S11"),
                                                                                          Variant::Root(
                                                                                                  FP::undefined()),
                                                                                          1000.0,
                                                                                          2000.0)});
        QTest::newRow("Single contaminated internal") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 1100.0,
                              1900.0)})
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{
                                                                      "Contaminated"}), 1000.0,
                                                              2000.0), SequenceValue(
                                                              SequenceName("bnd", "raw", "BsG_S11"),
                                                              Variant::Root(2.0), 1100.0, 1900.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "bnd",
                                                                                                  "stats",
                                                                                                  "F1_S11"),
                                                                                          Variant::Root(
                                                                                                  Variant::Flags{
                                                                                                          "Contaminated"}),
                                                                                          1000.0,
                                                                                          2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "bnd",
                                                                                                  "stats",
                                                                                                  "BsG_S11"),
                                                                                          Variant::Root(
                                                                                                  FP::undefined()),
                                                                                          1100.0,
                                                                                          1900.0)});
        QTest::newRow("Single contaminated before") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 900.0,
                              2000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0)})
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("bnd", "raw", "BsG_S11"),
                                                            Variant::Root(2.0), 900.0, 2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "raw",
                                                                                                "F1_S11"),
                                                                                        Variant::Root(
                                                                                                Variant::Flags{
                                                                                                        "Contaminated"}),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "stats",
                                                                                                "F1_S11"),
                                                                                        Variant::Root(
                                                                                                Variant::Flags{
                                                                                                        "Contaminated"}),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "stats",
                                                                                                "BsG_S11"),
                                                                                        Variant::Root(
                                                                                                2.0),
                                                                                        900.0,
                                                                                        1000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "bnd",
                                                                                                "stats",
                                                                                                "BsG_S11"),
                                                                                        Variant::Root(
                                                                                                FP::undefined()),
                                                                                        1000.0,
                                                                                        2000.0)});
        QTest::newRow("Single contaminated before contained") << SequenceName()
                                                              << (SequenceValue::Transfer{
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "raw",
                                                                                           "BsG_S11"),
                                                                              Variant::Root(2.0),
                                                                              900.0,
                                                                              1900.0),
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "raw",
                                                                                           "F1_S11"),
                                                                              Variant::Root(
                                                                                      Variant::Flags{
                                                                                              "Contaminated"}),
                                                                              1000.0, 2000.0)})
                                                              << (SequenceValue::Transfer{
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "raw",
                                                                                           "BsG_S11"),
                                                                              Variant::Root(2.0),
                                                                              900.0,
                                                                              1900.0),
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "raw",
                                                                                           "F1_S11"),
                                                                              Variant::Root(
                                                                                      Variant::Flags{
                                                                                              "Contaminated"}),
                                                                              1000.0, 2000.0),
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "stats",
                                                                                           "F1_S11"),
                                                                              Variant::Root(
                                                                                      Variant::Flags{
                                                                                              "Contaminated"}),
                                                                              1000.0, 2000.0),
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "stats",
                                                                                           "BsG_S11"),
                                                                              Variant::Root(2.0),
                                                                              900.0,
                                                                              1000.0),
                                                                      SequenceValue(
                                                                              SequenceName("bnd",
                                                                                           "stats",
                                                                                           "BsG_S11"),
                                                                              Variant::Root(
                                                                                      FP::undefined()),
                                                                              1000.0, 1900.0)});
        QTest::newRow("Single contaminated after") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 1000.0,
                              2100.0)})
                                                   << (SequenceValue::Transfer{SequenceValue(
                                                           SequenceName("bnd", "raw", "F1_S11"),
                                                           Variant::Root(
                                                                   Variant::Flags{"Contaminated"}),
                                                           1000.0, 2000.0), SequenceValue(
                                                           SequenceName("bnd", "raw", "BsG_S11"),
                                                           Variant::Root(2.0), 1000.0, 2100.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "stats",
                                                                                               "F1_S11"),
                                                                                       Variant::Root(
                                                                                               Variant::Flags{
                                                                                                       "Contaminated"}),
                                                                                       1000.0,
                                                                                       2000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "stats",
                                                                                               "BsG_S11"),
                                                                                       Variant::Root(
                                                                                               2.0),
                                                                                       2000.0,
                                                                                       2100.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "bnd",
                                                                                               "stats",
                                                                                               "BsG_S11"),
                                                                                       Variant::Root(
                                                                                               FP::undefined()),
                                                                                       1000.0,
                                                                                       2000.0)});
        QTest::newRow("Single contaminated after contained") << SequenceName()
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             1100.0,
                                                                             2100.0)})
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             1100.0,
                                                                             2100.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             2000.0,
                                                                             2100.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(
                                                                                     FP::undefined()),
                                                                             1100.0, 2000.0)});

        QTest::newRow("Contaminated middle switch") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 1000.0,
                              3000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"), Variant::Root(Variant::Flags()),
                              2000.0, 3000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 1000.0,
                              3000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"), Variant::Root(Variant::Flags()),
                              2000.0, 3000.0), SequenceValue(SequenceName("bnd", "stats", "F1_S11"),
                                                             Variant::Root(Variant::Flags{
                                                                     "Contaminated"}), 1000.0,
                                                             2000.0),
                SequenceValue(SequenceName("bnd", "stats", "F1_S11"),
                              Variant::Root(Variant::Flags()),
                              2000.0, 3000.0),
                SequenceValue(SequenceName("bnd", "stats", "BsG_S11"),
                              Variant::Root(FP::undefined()),
                              1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "stats", "BsG_S11"), Variant::Root(2.0), 2000.0,
                              3000.0)});
        QTest::newRow("Contaminated middle switch reversed") << SequenceName()
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             1000.0,
                                                                             3000.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0)})
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             1000.0,
                                                                             3000.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(
                                                                                     FP::undefined()),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             2000.0,
                                                                             3000.0)});
        QTest::newRow("Contaminated middle switch extended") << SequenceName()
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             900.0,
                                                                             3100.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0)})
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             900.0,
                                                                             3100.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "raw",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags{
                                                                                             "Contaminated"}),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats",
                                                                                          "F1_S11"),
                                                                             Variant::Root(
                                                                                     Variant::Flags()),
                                                                             2000.0, 3000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             900.0,
                                                                             1000.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(
                                                                                     FP::undefined()),
                                                                             1000.0, 2000.0),
                                                                     SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             2000.0,
                                                                             3000.0), SequenceValue(
                                                                             SequenceName("bnd",
                                                                                          "stats",
                                                                                          "BsG_S11"),
                                                                             Variant::Root(2.0),
                                                                             3000.0,
                                                                             3100.0)});

        QTest::newRow("Metadata contamination disabled") << SequenceName()
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("bnd", "raw",
                                                                              "F1_S11"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "Contaminated"}), 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("bnd", "raw_meta",
                                                                              "BsG_S11"),
                                                                 cmb("ContaminationRemoved", NULL,
                                                                     true), 1000.0, 2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "BsG_S11"),
                                                                                             Variant::Root(
                                                                                                     2.0),
                                                                                             1000.0,
                                                                                             2000.0)})
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("bnd", "raw",
                                                                              "F1_S11"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "Contaminated"}), 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("bnd", "raw_meta",
                                                                              "BsG_S11"),
                                                                 cmb("ContaminationRemoved", NULL,
                                                                     true), 1000.0, 2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "raw",
                                                                                                     "BsG_S11"),
                                                                                             Variant::Root(
                                                                                                     2.0),
                                                                                             1000.0,
                                                                                             2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "stats",
                                                                                                     "F1_S11"),
                                                                                             Variant::Root(
                                                                                                     Variant::Flags{
                                                                                                             "Contaminated"}),
                                                                                             1000.0,
                                                                                             2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "stats_meta",
                                                                                                     "BsG_S11"),
                                                                                             cmb("ContaminationRemoved",
                                                                                                 NULL,
                                                                                                 true),
                                                                                             1000.0,
                                                                                             2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "bnd",
                                                                                                     "stats",
                                                                                                     "BsG_S11"),
                                                                                             Variant::Root(
                                                                                                     2.0),
                                                                                             1000.0,
                                                                                             2000.0)});
        QTest::newRow("Metadata contamination disabled reorder") << SequenceName()
                                                                 << (SequenceValue::Transfer{
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw_meta",
                                                                                              "BsG_S11"),
                                                                                 cmb("ContaminationRemoved",
                                                                                     NULL, true),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "F1_S11"),
                                                                                 Variant::Root(
                                                                                         Variant::Flags{
                                                                                                 "Contaminated"}),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "BsG_S11"),
                                                                                 Variant::Root(2.0),
                                                                                 1000.0,
                                                                                 2000.0)})
                                                                 << (SequenceValue::Transfer{
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw_meta",
                                                                                              "BsG_S11"),
                                                                                 cmb("ContaminationRemoved",
                                                                                     NULL, true),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "F1_S11"),
                                                                                 Variant::Root(
                                                                                         Variant::Flags{
                                                                                                 "Contaminated"}),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "raw",
                                                                                              "BsG_S11"),
                                                                                 Variant::Root(2.0),
                                                                                 1000.0,
                                                                                 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "stats",
                                                                                              "F1_S11"),
                                                                                 Variant::Root(
                                                                                         Variant::Flags{
                                                                                                 "Contaminated"}),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "stats_meta",
                                                                                              "BsG_S11"),
                                                                                 cmb("ContaminationRemoved",
                                                                                     NULL, true),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("bnd",
                                                                                              "stats",
                                                                                              "BsG_S11"),
                                                                                 Variant::Root(2.0),
                                                                                 1000.0,
                                                                                 2000.0)});

        QTest::newRow("Metadata contamination disabled transition") << SequenceName()
                                                                    << (SequenceValue::Transfer{
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "F1_S11"),
                                                                                    Variant::Root(
                                                                                            Variant::Flags{
                                                                                                    "Contaminated"}),
                                                                                    1000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL, true),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            1.0),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL,
                                                                                        false),
                                                                                    2000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            2.0),
                                                                                    2000.0,
                                                                                    3000.0)})
                                                                    << (SequenceValue::Transfer{
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "F1_S11"),
                                                                                    Variant::Root(
                                                                                            Variant::Flags{
                                                                                                    "Contaminated"}),
                                                                                    1000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL, true),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            1.0),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL,
                                                                                        false),
                                                                                    2000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "raw",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            2.0),
                                                                                    2000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "stats",
                                                                                            "F1_S11"),
                                                                                    Variant::Root(
                                                                                            Variant::Flags{
                                                                                                    "Contaminated"}),
                                                                                    1000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "stats_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL, true),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "stats",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            1.0),
                                                                                    1000.0, 2000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "stats_meta",
                                                                                            "BsG_S11"),
                                                                                    cmb("ContaminationRemoved",
                                                                                        NULL, true),
                                                                                    2000.0, 3000.0),
                                                                            SequenceValue(
                                                                                    SequenceName(
                                                                                            "bnd",
                                                                                            "stats",
                                                                                            "BsG_S11"),
                                                                                    Variant::Root(
                                                                                            FP::undefined()),
                                                                                    2000.0,
                                                                                    3000.0)});

        Variant::Root meta;
        meta.write().metadataSingleFlag("ContaminateBlarg").hash("Stuff").setString("A");
        QTest::newRow("Metadata alternate flag") << SequenceName() << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"), meta, 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"ContaminateBlarg"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.0), 1000.0,
                              2000.0)})
                                                 << (SequenceValue::Transfer{SequenceValue(
                                                         SequenceName("bnd", "raw_meta", "F1_S11"),
                                                         meta, 1000.0, 2000.0), SequenceValue(
                                                         SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags{
                                                                 "ContaminateBlarg"}), 1000.0,
                                                         2000.0), SequenceValue(
                                                         SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(1.0), 1000.0, 2000.0),
                                                                             SequenceValue(
                                                         SequenceName("bnd", "stats_meta",
                                                                      "F1_S11"), meta, 1000.0,
                                                         2000.0), SequenceValue(
                                                                 SequenceName("bnd", "stats", "F1_S11"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "ContaminateBlarg"}),
                                                                 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("bnd", "stats", "BsG_S11"),
                                                                 Variant::Root(FP::undefined()),
                                                                 1000.0, 2000.0)});

        QTest::newRow("Complex A") << SequenceName("bnd", "raw", "BsG_S11")
                                   << (SequenceValue::Transfer{
                                           SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"),
                                                         meta, 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                         cmb("ContaminationRemoved", NULL, false),
                                                         1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "BsB_S11"),
                                                         cms("A", "B", "C"), 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(1.0), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(1.1), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags{"Stuff"}),
                                                         2000.0,
                                                         3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(2.0), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(2.1), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(3.0), 3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(3.1), 3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 4000.0,
                                                         5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(4.0), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(4.1), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 5000.0,
                                                         6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(5.0), 5000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(5.1), 5000.0, 6000.0)})
                                   << (SequenceValue::Transfer{
                                           SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"),
                                                         meta, 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "BsG_S11"),
                                                         cmb("ContaminationRemoved", NULL, false),
                                                         1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw_meta", "BsB_S11"),
                                                         cms("A", "B", "C"), 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(1.0), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(1.1), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags{"Stuff"}),
                                                         2000.0,
                                                         3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(2.0), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(2.1), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(3.0), 3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(3.1), 3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 4000.0,
                                                         5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(4.0), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(4.1), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 5000.0,
                                                         6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsG_S11"),
                                                         Variant::Root(5.0), 5000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "raw", "BsB_S11"),
                                                         Variant::Root(5.1), 5000.0, 6000.0),
                                           SequenceValue(
                                                   SequenceName("bnd", "stats_meta", "F1_S11"),
                                                   meta, 1000.0, 6000.0), SequenceValue(
                                                   SequenceName("bnd", "stats_meta", "BsG_S11"),
                                                   cmb("ContaminationRemoved", NULL, true), 1000.0,
                                                   6000.0), SequenceValue(
                                                   SequenceName("bnd", "stats_meta", "BsB_S11"),
                                                   cms("A", "B", "C"), 1000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "BsG_S11"),
                                                         Variant::Root(FP::undefined()), 1000.0,
                                                         2000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "BsB_S11"),
                                                         Variant::Root(1.1), 1000.0, 2000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "F1_S11"),
                                                         Variant::Root(Variant::Flags{"Stuff"}),
                                                         2000.0,
                                                         3000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "BsG_S11"),
                                                         Variant::Root(2.0), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "BsB_S11"),
                                                         Variant::Root(2.1), 2000.0, 3000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "F1_S11"),
                                                         Variant::Root(
                                                                 Variant::Flags{"Contaminated"}),
                                                         3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "BsG_S11"),
                                                         Variant::Root(FP::undefined()), 3000.0,
                                                         4000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "BsB_S11"),
                                                         Variant::Root(3.1), 3000.0, 4000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 4000.0,
                                                         5000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "BsG_S11"),
                                                         Variant::Root(4.0), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "BsB_S11"),
                                                         Variant::Root(4.1), 4000.0, 5000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "F1_S11"),
                                                         Variant::Root(Variant::Flags()), 5000.0,
                                                         6000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "BsG_S11"),
                                                         Variant::Root(5.0), 5000.0, 6000.0),
                                           SequenceValue(SequenceName("bnd", "stats", "BsB_S11"),
                                                         Variant::Root(5.1), 5000.0, 6000.0)});
    }

};

QTEST_MAIN(TestContamFilter)

#include "contamfilter.moc"
