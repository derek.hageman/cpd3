/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>
#include <QVector>
#include <QVarLengthArray>

#include "smoothing/smoothingchain.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "algorithms/dewpoint.hxx"

using namespace CPD3;
using namespace CPD3::Smoothing;
using namespace CPD3::Algorithms;

struct OverlayData {
    double start;
    double end;
    int index;
    double value;

    OverlayData(double s, double e, int i, double v) : start(s), end(e), index(i), value(v)
    { }

    double getStart() const
    { return start; }

    double getEnd() const
    { return end; }
};

template<int N>
struct SegmentData {
    double start;
    double end;
    double values[N];

    SegmentData() : start(FP::undefined()), end(FP::undefined())
    {
        for (int i = 0; i < N; i++) {
            values[i] = FP::undefined();
        }
    }

    SegmentData(double s, double e, const double v[N]) : start(s), end(e)
    {
        for (int i = 0; i < N; i++) {
            values[i] = v[i];
        }
    }

    SegmentData(const SegmentData<N> &other) : start(other.start), end(other.end)
    {
        for (int i = 0; i < N; i++) {
            values[i] = other.values[i];
        }
    }

    SegmentData<N> &operator=(const SegmentData<N> &other)
    {
        if (&other == this) return *this;
        start = other.start;
        end = other.end;
        for (int i = 0; i < N; i++) {
            values[i] = other.values[i];
        }
        return *this;
    }

    bool operator==(const SegmentData<N> &rhs) const
    {
        for (int i = 0; i < N; i++) {
            if (!FP::equal(values[i], rhs.values[i]))
                return false;
        }
        return FP::equal(start, rhs.start) && FP::equal(end, rhs.end);
    }

    bool operator!=(const SegmentData<N> &rhs) const
    {
        for (int i = 0; i < N; i++) {
            if (!FP::equal(values[i], rhs.values[i]))
                return true;
        }
        return !FP::equal(start, rhs.start) || !FP::equal(end, rhs.end);
    }

    SegmentData(const SegmentData<N> &other, double s, double e) : start(s), end(e)
    {
        for (int i = 0; i < N; i++) {
            values[i] = other.values[i];
        }
    }

    SegmentData(const OverlayData &other, double s, double e) : start(s), end(e)
    {
        Q_ASSERT(other.index >= 0 && other.index < N);
        for (int i = 0; i < N; i++) {
            values[i] = FP::undefined();
        }
        values[other.index] = other.value;
    }

    SegmentData(const SegmentData<N> &under, const OverlayData &over, double s, double e) : start(
            s), end(e)
    {
        Q_ASSERT(over.index >= 0 && over.index < N);
        for (int i = 0; i < N; i++) {
            values[i] = under.values[i];
        }
        values[over.index] = over.value;
    }

    double getStart() const
    { return start; }

    double getEnd() const
    { return end; }

    void setStart(double v)
    { start = v; }

    void setEnd(double v)
    { end = v; }
};

template<int N>
static char *testToString(const QList<SegmentData<N> > &output)
{
    QByteArray ba;
    for (int i = 0, max = output.size(); i < max; i++) {
        if (i != 0) ba += ", ";

        ba += "(";
        if (FP::defined(output.at(i).start))
            ba += QByteArray::number(output.at(i).start);
        else
            ba += "-Infinity";
        ba += ",";
        if (FP::defined(output.at(i).end))
            ba += QByteArray::number(output.at(i).end);
        else
            ba += "+Infinity";
        ba += "={";
        for (int j = 0; j < N; j++) {
            if (j != 0) ba += ",";
            if (FP::defined(output.at(i).values[j]))
                ba += QByteArray::number(output.at(i).values[j]);
            else
                ba += "Undefined";
        }
        ba += "})";
    }
    return qstrdup(ba.data());
}

namespace QTest {
template<>
char *toString(const QList<SegmentData<2> > &output)
{ return testToString(output); }

template<>
char *toString(const QList<double> &output)
{
    QByteArray ba;
    for (int i = 0, max = output.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        if (FP::defined(output.at(i)))
            ba += QByteArray::number(output.at(i));
        else
            ba += "Undefined";
    }
    return qstrdup(ba.data());
}
}

template<int N>
struct TestEvent {
    enum Type {
        Advance, Value, End, Check,
    };
    Type type;
    int index;
    double start;
    double end;
    double value;
    QList<SegmentData<N> > checkData;
    QList<double> checkAdvance;

    TestEvent(Type t,
              int i,
              double s = FP::undefined(),
              double e = FP::undefined(),
              double v = FP::undefined()) : type(t),
                                            index(i),
                                            start(s),
                                            end(e),
                                            value(v),
                                            checkData(),
                                            checkAdvance()
    { }

    TestEvent(const QList<SegmentData<N> > &cd, const QList<double> &ca = QList<double>()) : type(
            Check),
                                                                                             index(-1),
                                                                                             start(FP::undefined()),
                                                                                             end(FP::undefined()),
                                                                                             value(FP::undefined()),
                                                                                             checkData(
                                                                                                     cd),
                                                                                             checkAdvance(
                                                                                                     ca)
    { }

    TestEvent() : type(Check),
                  index(-1),
                  start(FP::undefined()),
                  end(FP::undefined()),
                  value(FP::undefined()),
                  checkData(),
                  checkAdvance()
    { }
};

typedef TestEvent<2> TestEvent2;

Q_DECLARE_METATYPE(TestEvent2);

Q_DECLARE_METATYPE(QList<TestEvent2>);


template<int N>
class TestSegmentProcessor : public SmootherChainInputSegmentProcessor<N> {
public:
    QList<SegmentData<N> > segments;
    QList<double> advances;
    double lastTime;

    TestSegmentProcessor() : segments(), advances(), lastTime(FP::undefined())
    { }

protected:
    virtual void segmentDone(double start, double end, const double values[N])
    {
        Q_ASSERT(Range::compareStart(start, lastTime) >= 0);
        lastTime = end;
        segments.append(SegmentData<N>(start, end, values));
    }

    virtual void allStreamsAdvanced(double time)
    {
        Q_ASSERT(Range::compareStart(time, lastTime) >= 0);
        lastTime = time;
        advances.append(time);
    }
};

class TestChainTarget : public SmootherChainTarget {
public:
    double value;
    bool gotValue;

    TestChainTarget() : value(FP::undefined()), gotValue(false)
    { }

    virtual ~TestChainTarget()
    { }

    virtual void incomingData(double start, double end, double value)
    {
        Q_UNUSED(start);
        Q_UNUSED(end);
        this->value = value;
        gotValue = true;
    }

    virtual void incomingAdvance(double time)
    { Q_UNUSED(time); }

    virtual void endData()
    { }
};

class TestSmoothingChain : public QObject {
Q_OBJECT

private slots:

    void segmentProcessor2()
    {
        QFETCH(QList<TestEvent2>, events);

        QList<SegmentData<2> > result;
        for (QList<TestEvent2>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            if (e->type != TestEvent2::Value)
                continue;
            Range::overlayFragmenting(result, OverlayData(e->start, e->end, e->index, e->value));
        }

        TestSegmentProcessor<2> processor;
        bool ended[2];
        memset(ended, 0, sizeof(ended));
        bool hadAllEnd = false;

        for (QList<TestEvent2>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            switch (e->type) {
            case TestEvent2::Value:
                processor.segmentValue(e->index, e->start, e->end, e->value);
                break;
            case TestEvent2::Advance:
                processor.segmentAdvance(e->index, e->start);
                break;
            case TestEvent2::End:
                hadAllEnd = processor.segmentEnd(e->index) || hadAllEnd;
                ended[e->index] = true;
                break;
            case TestEvent2::Check:
                QCOMPARE(processor.segments, e->checkData);
                QCOMPARE(processor.advances, e->checkAdvance);
                break;
            }
        }

        for (int i = 0; i < 2; i++) {
            if (ended[i]) continue;
            hadAllEnd = processor.segmentEnd(i) || hadAllEnd;
        }

        QVERIFY(hadAllEnd);
        QCOMPARE(processor.segments, result);


        /* Run it again with no advances to make sure it's consistent */
        hadAllEnd = false;
        memset(ended, 0, sizeof(ended));
        TestSegmentProcessor<2> processor2;
        for (QList<TestEvent2>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            switch (e->type) {
            case TestEvent2::Value:
                processor2.segmentValue(e->index, e->start, e->end, e->value);
                break;
            case TestEvent2::End:
                hadAllEnd = processor2.segmentEnd(e->index) || hadAllEnd;
                ended[e->index] = true;
                break;
            default:
                break;
            }
        }
        for (int i = 0; i < 2; i++) {
            if (ended[i]) continue;
            hadAllEnd = processor2.segmentEnd(i) || hadAllEnd;
        }
        QVERIFY(hadAllEnd);
        QCOMPARE(processor2.segments, result);


        /* And again serializing and de-serializing */
        TestSegmentProcessor<2> processor3;
        hadAllEnd = false;
        memset(ended, 0, sizeof(ended));
        for (QList<TestEvent2>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    processor3.writeSerial(stream);
                }
                {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    processor3.readSerial(stream);
                }
            }

            switch (e->type) {
            case TestEvent2::Value:
                processor3.segmentValue(e->index, e->start, e->end, e->value);
                break;
            case TestEvent2::Advance:
                processor3.segmentAdvance(e->index, e->start);
                break;
            case TestEvent2::End:
                hadAllEnd = processor3.segmentEnd(e->index) || hadAllEnd;
                ended[e->index] = true;
                break;
            case TestEvent2::Check:
                QCOMPARE(processor3.segments, e->checkData);
                QCOMPARE(processor3.advances, e->checkAdvance);
                break;
            }
        }

        for (int i = 0; i < 2; i++) {
            if (ended[i]) continue;
            hadAllEnd = processor3.segmentEnd(i) || hadAllEnd;
        }

        QVERIFY(hadAllEnd);
        QCOMPARE(processor3.segments, result);
    }

    void segmentProcessor2_data()
    {
        QTest::addColumn<QList<TestEvent2> >("events");

        double v1[2];
        double v2[2];
        double v3[2];

        QTest::newRow("Empty") << (QList<TestEvent2>());

        QTest::newRow("Single") <<
                (QList<TestEvent2>() <<
                        TestEvent2() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 200.0, 1.0) <<
                        TestEvent2());

        v1[0] = 1.0;
        v1[1] = FP::undefined();
        v2[0] = 1.0;
        v2[1] = 2.0;
        v3[0] = FP::undefined();
        v3[1] = 2.0;

        QTest::newRow("Single advance") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 200.0, 1.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 200.0) <<
                        TestEvent2(QList<SegmentData<2> >() << SegmentData<2>(100.0, 200.0, v1)));

        QTest::newRow("Multipart advance") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 200.0, 1.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 150.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 200.0) <<
                        TestEvent2() <<
                        TestEvent2(TestEvent2::Advance, 1, 200.0) <<
                        TestEvent2(QList<SegmentData<2> >() << SegmentData<2>(100.0, 200.0, v1)));

        QTest::newRow("Empty advance") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Advance, 1, 150.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 200.0) <<
                        TestEvent2(QList<SegmentData<2> >(), QList<double>() << 150.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 200.0) <<
                        TestEvent2(QList<SegmentData<2> >(), QList<double>() << 150.0 << 200.0));

        QTest::newRow("Empty identical advance") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Advance, 1, 200.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 200.0) <<
                        TestEvent2(QList<SegmentData<2> >(), QList<double>() << 200.0));

        QTest::newRow("Identical times") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 200.0, 1.0) <<
                        TestEvent2() <<
                        TestEvent2(TestEvent2::Value, 1, 100.0, 200.0, 2.0) <<
                        TestEvent2(QList<SegmentData<2> >() << SegmentData<2>(100.0, 200.0, v2)));

        QTest::newRow("End difference") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 250.0, 1.0) <<
                        TestEvent2() <<
                        TestEvent2(TestEvent2::Value, 1, 100.0, 200.0, 2.0) <<
                        TestEvent2(QList<SegmentData<2> >() << SegmentData<2>(100.0, 200.0, v2)) <<
                        TestEvent2(TestEvent2::Advance, 1, 250.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 200.0, v2) <<
                                           SegmentData<2>(200.0, 250.0, v1)));

        QTest::newRow("Start difference") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 200.0, 1.0) <<
                        TestEvent2() <<
                        TestEvent2(TestEvent2::Value, 1, 150.0, 200.0, 2.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 150.0, v1) <<
                                           SegmentData<2>(150.0, 200.0, v2)));

        QTest::newRow("Start and end difference") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 200.0, 1.0) <<
                        TestEvent2() <<
                        TestEvent2(TestEvent2::Value, 1, 150.0, 250.0, 2.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 150.0, v1) <<
                                           SegmentData<2>(150.0, 200.0, v2)) <<
                        TestEvent2(TestEvent2::Advance, 0, 250.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 150.0, v1) <<
                                           SegmentData<2>(150.0, 200.0, v2) <<
                                           SegmentData<2>(200.0, 250.0, v3)));

        QTest::newRow("Contained") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 250.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 150.0, 200.0, 2.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 150.0, v1) <<
                                           SegmentData<2>(150.0, 200.0, v2)) <<
                        TestEvent2(TestEvent2::Advance, 1, 250.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 150.0, v1) <<
                                           SegmentData<2>(150.0, 200.0, v2) <<
                                           SegmentData<2>(200.0, 250.0, v1)));

        QTest::newRow("Advance Check") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 65.0, 125.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 65.0, 125.0, 2.0) <<
                        TestEvent2(TestEvent2::Value, 1, 180.0, 295.0, 2.0) <<
                        TestEvent2(QList<SegmentData<2> >() << SegmentData<2>(65.0, 125.0, v2)) <<
                        TestEvent2(TestEvent2::Advance, 0, 300.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(65.0, 125.0, v2) <<
                                           SegmentData<2>(180.0, 295.0, v3)) <<
                        TestEvent2(TestEvent2::Value, 1, 305.0, 420.0, 2.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(65.0, 125.0, v2) <<
                                           SegmentData<2>(180.0, 295.0, v3),
                                   QList<double>() << 300.0) <<
                        TestEvent2(TestEvent2::Value, 0, 305.0, 420.0, 1.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(65.0, 125.0, v2) <<
                                           SegmentData<2>(180.0, 295.0, v3) <<
                                           SegmentData<2>(305.0, 420.0, v2),
                                   QList<double>() << 300.0));

        QTest::newRow("Half Advance") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 150.0, 1.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 200.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 200.0) <<
                        TestEvent2(QList<SegmentData<2> >() << SegmentData<2>(100.0, 150.0, v1),
                                   QList<double>() << 200.0));

        QTest::newRow("Partial Gap Buffering") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 200.0, 1.0) <<
                        TestEvent2() <<
                        TestEvent2(TestEvent2::Value, 0, 200.0, 300.0, 1.0) <<
                        TestEvent2() <<
                        TestEvent2(TestEvent2::Value, 0, 300.0, 400.0, 1.0) <<
                        TestEvent2() <<
                        TestEvent2(TestEvent2::Value, 1, 300.0, 350.0, 2.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 200.0, v1) <<
                                           SegmentData<2>(200.0, 300.0, v1) <<
                                           SegmentData<2>(300.0, 350.0, v2)) <<
                        TestEvent2(TestEvent2::Value, 1, 350.0, 400.0, 2.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 200.0, v1) <<
                                           SegmentData<2>(200.0, 300.0, v1) <<
                                           SegmentData<2>(300.0, 350.0, v2) <<
                                           SegmentData<2>(350.0, 400.0, v2)));

        QTest::newRow("Partial Gap Advance") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 200.0, 1.0) <<
                        TestEvent2() <<
                        TestEvent2(TestEvent2::Value, 0, 200.0, 300.0, 1.0) <<
                        TestEvent2() <<
                        TestEvent2(TestEvent2::Value, 0, 300.0, 400.0, 1.0) <<
                        TestEvent2() <<
                        TestEvent2(TestEvent2::Advance, 1, 350.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 200.0, v1) <<
                                           SegmentData<2>(200.0, 300.0, v1)) <<
                        TestEvent2(TestEvent2::Value, 1, 350.0, 400.0, 2.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 200.0, v1) <<
                                           SegmentData<2>(200.0, 300.0, v1) <<
                                           SegmentData<2>(300.0, 350.0, v1) <<
                                           SegmentData<2>(350.0, 400.0, v2)));

        QTest::newRow("Input end adavnace") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::End, 1) <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 200.0, 1.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 200.0) <<
                        TestEvent2(QList<SegmentData<2> >() << SegmentData<2>(100.0, 200.0, v1)) <<
                        TestEvent2(TestEvent2::Advance, 0, 300.0) <<
                        TestEvent2(QList<SegmentData<2> >() << SegmentData<2>(100.0, 200.0, v1),
                                   QList<double>() << 300.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 400.0) <<
                        TestEvent2(QList<SegmentData<2> >() << SegmentData<2>(100.0, 200.0, v1),
                                   QList<double>() << 300.0 << 400.0));

        QTest::newRow("Complex Sequence One") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Advance, 1, 50.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 75.0) <<
                        TestEvent2(QList<SegmentData<2> >(), QList<double>() << 50.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 100.0) <<
                        TestEvent2(QList<SegmentData<2> >(), QList<double>() << 50.0 << 75.0) <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 200.0, 1.0) <<
                        TestEvent2(QList<SegmentData<2> >(),
                                   QList<double>() << 50.0 << 75.0 << 100.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 150.0) <<
                        TestEvent2(QList<SegmentData<2> >(),
                                   QList<double>() << 50.0 << 75.0 << 100.0) <<
                        TestEvent2(TestEvent2::Value, 1, 150.0, 200.0, 2.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 150.0, v1) <<
                                           SegmentData<2>(150.0, 200.0, v2),
                                   QList<double>() << 50.0 << 75.0 << 100.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 250.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 150.0, v1) <<
                                           SegmentData<2>(150.0, 200.0, v2),
                                   QList<double>() << 50.0 << 75.0 << 100.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 250.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 150.0, v1) <<
                                           SegmentData<2>(150.0, 200.0, v2),
                                   QList<double>() << 50.0 << 75.0 << 100.0 << 250.0));


        v1[0] = 1.0;
        v1[1] = 2.0;
        v2[0] = 1.1;
        v2[1] = FP::undefined();
        v3[0] = 1.2;
        v3[1] = FP::undefined();
        QTest::newRow("Complex Sequence Two") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 150.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 100.0, 150.0, 2.0) <<
                        TestEvent2(QList<SegmentData<2> >() << SegmentData<2>(100.0, 150.0, v1)) <<
                        TestEvent2(TestEvent2::Value, 0, 150.0, 200.0, 1.1) <<
                        TestEvent2(TestEvent2::Value, 0, 200.0, 250.0, 1.2) <<
                        TestEvent2(QList<SegmentData<2> >() << SegmentData<2>(100.0, 150.0, v1)) <<
                        TestEvent2(TestEvent2::Advance, 1, 150.0) <<
                        TestEvent2(QList<SegmentData<2> >() << SegmentData<2>(100.0, 150.0, v1)) <<
                        TestEvent2(TestEvent2::Advance, 1, 250.0) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 150.0, v1) <<
                                           SegmentData<2>(150.0, 200.0, v2) <<
                                           SegmentData<2>(200.0, 250.0, v3)));


        v1[0] = 1.0;
        v1[1] = 2.0;
        v2[0] = 1.1;
        v2[1] = 2.0;
        v3[0] = 1.2;
        v3[1] = 2.0;
        QTest::newRow("Complex Sequence Three") <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 150.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 100.0, 250.0, 2.0) <<
                        TestEvent2(QList<SegmentData<2> >() << SegmentData<2>(100.0, 150.0, v1)) <<
                        TestEvent2(TestEvent2::Value, 0, 150.0, 200.0, 1.1) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 150.0, v1) <<
                                           SegmentData<2>(150.0, 200.0, v2)) <<
                        TestEvent2(TestEvent2::Value, 0, 200.0, 250.0, 1.2) <<
                        TestEvent2(QList<SegmentData<2> >() <<
                                           SegmentData<2>(100.0, 150.0, v1) <<
                                           SegmentData<2>(150.0, 200.0, v2) <<
                                           SegmentData<2>(200.0, 250.0, v3)));
    }

    void vector2D()
    {
        QFETCH(double, direction);
        QFETCH(double, magnitude);

        TestChainTarget targetX;
        TestChainTarget targetY;
        SmootherChainVector2DBreakdown breakdown(&targetX, &targetY);

        breakdown.getTargetDirection()->incomingData(0, 1, direction);
        breakdown.getTargetMagnitude()->incomingData(0, 1, magnitude);

        QVERIFY(targetX.gotValue);
        QVERIFY(targetY.gotValue);
        if (!FP::defined(direction) || !FP::defined(magnitude)) {
            QVERIFY(!FP::defined(targetX.value));
            QVERIFY(!FP::defined(targetY.value));
        } else {
            if (magnitude != 0.0) {
                QVERIFY(targetX.value != 0.0 || targetY.value != 0.0);
            } else {
                QCOMPARE(targetX.value, 0.0);
                QCOMPARE(targetY.value, 0.0);
            }
        }

        TestChainTarget targetDirection;
        TestChainTarget targetMagnitude;
        SmootherChainVector2DReconstruct reconstruct(&targetDirection, &targetMagnitude);

        reconstruct.getTargetX()->incomingData(0, 1, targetX.value);
        reconstruct.getTargetY()->incomingData(0, 1, targetY.value);

        QVERIFY(targetDirection.gotValue);
        QVERIFY(targetMagnitude.gotValue);
        if (!FP::defined(targetX.value) || !FP::defined(targetY.value)) {
            QVERIFY(!FP::defined(targetDirection.value));
            QVERIFY(!FP::defined(targetMagnitude.value));
        } else {
            QCOMPARE(targetMagnitude.value, magnitude);
            if (magnitude != 0.0) {
                QCOMPARE(targetDirection.value, direction);
            } else if (fabs(targetDirection.value) > 1E-10) {
                QVERIFY(!FP::defined(targetDirection.value));
            }
        }
    }

    void vector2D_data()
    {
        QTest::addColumn<double>("direction");
        QTest::addColumn<double>("magnitude");

        QTest::newRow("Both missing") << FP::undefined() << FP::undefined();
        QTest::newRow("Direction missing") << FP::undefined() << 1.0;
        QTest::newRow("Magnitude missing") << 1.0 << FP::undefined();

        QTest::newRow("Zero magnitude") << 0.0 << 0.0;
        QTest::newRow("Zero magnitude with direction") << 50.0 << 0.0;

        QTest::newRow("0 5") << 0.0 << 5.0;
        QTest::newRow("50 1") << 50.0 << 1.0;
        QTest::newRow("70 1") << 70.0 << 1.0;
        QTest::newRow("90 0.25") << 90.0 << 0.25;
        QTest::newRow("110 3.0") << 110.0 << 3.0;
        QTest::newRow("180 2") << 180.0 << 2.0;
        QTest::newRow("190 1") << 190.0 << 1.0;
        QTest::newRow("270 1") << 270.0 << 1.0;
        QTest::newRow("290 0.5") << 290.0 << 0.5;
    }

    void vector3D()
    {
        QFETCH(double, azimuth);
        QFETCH(double, elevation);
        QFETCH(double, magnitude);

        TestChainTarget targetX;
        TestChainTarget targetY;
        TestChainTarget targetZ;
        SmootherChainVector3DBreakdown breakdown(&targetX, &targetY, &targetZ);

        breakdown.getTargetAzimuth()->incomingData(0, 1, azimuth);
        breakdown.getTargetElevation()->incomingData(0, 1, elevation);
        breakdown.getTargetMagnitude()->incomingData(0, 1, magnitude);

        QVERIFY(targetX.gotValue);
        QVERIFY(targetY.gotValue);
        QVERIFY(targetZ.gotValue);
        if (!FP::defined(azimuth) || !FP::defined(elevation) || !FP::defined(magnitude)) {
            QVERIFY(!FP::defined(targetX.value));
            QVERIFY(!FP::defined(targetY.value));
            QVERIFY(!FP::defined(targetZ.value));
        } else {
            if (magnitude != 0.0) {
                QVERIFY(targetX.value != 0.0 || targetY.value != 0.0 || targetZ.value != 0.0);
            } else {
                QCOMPARE(targetX.value, 0.0);
                QCOMPARE(targetY.value, 0.0);
                QCOMPARE(targetZ.value, 0.0);
            }
        }

        TestChainTarget targetAzimuth;
        TestChainTarget targetElevation;
        TestChainTarget targetMagnitude;
        SmootherChainVector3DReconstruct
                reconstruct(&targetAzimuth, &targetElevation, &targetMagnitude);

        reconstruct.getTargetX()->incomingData(0, 1, targetX.value);
        reconstruct.getTargetY()->incomingData(0, 1, targetY.value);
        reconstruct.getTargetZ()->incomingData(0, 1, targetZ.value);

        QVERIFY(targetAzimuth.gotValue);
        QVERIFY(targetElevation.gotValue);
        QVERIFY(targetMagnitude.gotValue);
        if (!FP::defined(targetX.value) ||
                !FP::defined(targetY.value) ||
                !FP::defined(targetZ.value)) {
            QVERIFY(!FP::defined(targetAzimuth.value));
            QVERIFY(!FP::defined(targetElevation.value));
            QVERIFY(!FP::defined(targetMagnitude.value));
        } else {
            QCOMPARE(targetMagnitude.value, magnitude);
            if (magnitude != 0.0) {
                QCOMPARE(targetAzimuth.value, azimuth);
                QCOMPARE(targetElevation.value, elevation);
            } else {
                QVERIFY(!FP::defined(targetAzimuth.value));
                QVERIFY(!FP::defined(targetElevation.value));
            }
        }
    }

    void vector3D_data()
    {
        QTest::addColumn<double>("azimuth");
        QTest::addColumn<double>("elevation");
        QTest::addColumn<double>("magnitude");

        QTest::newRow("All missing") << FP::undefined() << FP::undefined() << FP::undefined();
        QTest::newRow("Azimuth missing") << FP::undefined() << 1.0 << 1.0;
        QTest::newRow("Elevation missing") << 1.0 << FP::undefined() << 1.0;
        QTest::newRow("Magnitude missing") << 1.0 << 0.0 << FP::undefined();

        QTest::newRow("Zero magnitude") << 0.0 << 0.0 << 0.0;
        QTest::newRow("Zero magnitude with direction") << 50.0 << 10.0 << 0.0;

        QTest::newRow("0 0 5") << 0.0 << 0.0 << 5.0;
        QTest::newRow("0 90 5") << 0.0 << 90.0 << 5.0;
        QTest::newRow("0 -90 5") << 0.0 << -90.0 << 5.0;
        QTest::newRow("90 0 4") << 90.0 << 0.0 << 4.0;
        QTest::newRow("180 0 4") << 180.0 << 0.0 << 4.0;
        QTest::newRow("270 0 4") << 270.0 << 0.0 << 4.0;
        QTest::newRow("10 20 3") << 20.0 << 20.0 << 3.0;
        QTest::newRow("100 20 3") << 100.0 << 20.0 << 3.0;
        QTest::newRow("190 20 3") << 190.0 << 20.0 << 3.0;
        QTest::newRow("280 20 3") << 280.0 << 20.0 << 3.0;
        QTest::newRow("70 -50 2") << 70.0 << -50.0 << 2.0;
        QTest::newRow("170 -50 2") << 170.0 << -50.0 << 2.0;
        QTest::newRow("260 -50 2") << 260.0 << -50.0 << 2.0;
        QTest::newRow("350 -50 2") << 350.0 << -50.0 << 2.0;
    }

    void beersLawAbsorption()
    {
        QFETCH(double, startL);
        QFETCH(double, endL);
        QFETCH(double, startI);
        QFETCH(double, endI);

        TestChainTarget targetAbsorption;
        SmootherChainBeersLawAbsorption calculator(&targetAbsorption);

        calculator.getTargetStartL()->incomingData(0, 1, startL);
        calculator.getTargetEndL()->incomingData(0, 1, endL);
        calculator.getTargetStartI()->incomingData(0, 1, startI);
        calculator.getTargetEndI()->incomingData(0, 1, endI);

        QVERIFY(targetAbsorption.gotValue);
        if (!FP::defined(startL) ||
                !FP::defined(endL) ||
                !FP::defined(startI) ||
                !FP::defined(endI) ||
                endI == 0.0 ||
                startI == 0.0 ||
                endI / startI <= 0.0) {
            QVERIFY(!FP::defined(targetAbsorption.value));
        } else {
            QCOMPARE(targetAbsorption.value, (1E6 * log(startI / endI)) / (endL - startL));
        }
    }

    void beersLawAbsorption_data()
    {
        QTest::addColumn<double>("startL");
        QTest::addColumn<double>("endL");
        QTest::addColumn<double>("startI");
        QTest::addColumn<double>("endI");

        QTest::newRow("All missing") <<
                FP::undefined() <<
                FP::undefined() <<
                FP::undefined() <<
                FP::undefined();
        QTest::newRow("Start L missing") << FP::undefined() << 10.0 << 1.0 << 0.9;
        QTest::newRow("End L missing") << 10.0 << FP::undefined() << 1.0 << 0.9;
        QTest::newRow("Start I missing") << 10.0 << 20.0 << FP::undefined() << 0.9;
        QTest::newRow("End I missing") << 10.0 << 20.0 << 1.0 << FP::undefined();
        QTest::newRow("Invalid end I") << 10.0 << 20.0 << 1.0 << 0.0;
        QTest::newRow("Invalid start I") << 10.0 << 20.0 << 0.0 << 0.9;
        QTest::newRow("Invalid I ratio") << 10.0 << 20.0 << -1.0 << 0.9;

        QTest::newRow("General calculation one") << 10.0 << 20.0 << 1.0 << 0.95;
        QTest::newRow("General calculation two") << 5.0 << 6.0 << 1.0 << 0.99;
    }

    void dewpoint()
    {
        QFETCH(double, t);
        QFETCH(double, rh);

        TestChainTarget targetTD;
        SmootherChainDewpoint calculator(&targetTD);

        calculator.getTargetTemperature()->incomingData(0, 1, t);
        calculator.getTargetRH()->incomingData(0, 1, rh);

        QVERIFY(targetTD.gotValue);
        if (!FP::defined(t) || !FP::defined(rh)) {
            QVERIFY(!FP::defined(targetTD.value));
        } else {
            QCOMPARE(targetTD.value, Dewpoint::dewpoint(t, rh));
        }
    }

    void dewpoint_data()
    {
        QTest::addColumn<double>("t");
        QTest::addColumn<double>("rh");

        QTest::newRow("All missing") << FP::undefined() << FP::undefined();
        QTest::newRow("Temperature missing") << FP::undefined() << 10.0;
        QTest::newRow("RH missing") << 10.0 << FP::undefined();
        QTest::newRow("10 50") << 10.0 << 50.0;
    }

    void rh()
    {
        QFETCH(double, t);
        QFETCH(double, td);

        TestChainTarget targetRH;
        SmootherChainRH calculator(&targetRH);

        calculator.getTargetTemperature()->incomingData(0, 1, t);
        calculator.getTargetDewpoint()->incomingData(0, 1, td);

        QVERIFY(targetRH.gotValue);
        if (!FP::defined(t) || !FP::defined(td)) {
            QVERIFY(!FP::defined(targetRH.value));
        } else {
            QCOMPARE(targetRH.value, Dewpoint::rh(t, td));
        }
    }

    void rh_data()
    {
        QTest::addColumn<double>("t");
        QTest::addColumn<double>("td");

        QTest::newRow("All missing") << FP::undefined() << FP::undefined();
        QTest::newRow("Temperature missing") << FP::undefined() << 10.0;
        QTest::newRow("RH missing") << 10.0 << FP::undefined();
        QTest::newRow("20 10") << 10.0 << 10.0;
    }

    void rhExtrapolate()
    {
        QFETCH(double, t1);
        QFETCH(double, rh1);
        QFETCH(double, t2);

        TestChainTarget targetRH;
        SmootherChainRHExtrapolate calculator(&targetRH);

        calculator.getTargetTemperatureIn()->incomingData(0, 1, t1);
        calculator.getTargetRHIn()->incomingData(0, 1, rh1);
        calculator.getTargetTemperatureOut()->incomingData(0, 1, t2);

        QVERIFY(targetRH.gotValue);
        if (!FP::defined(t1) || !FP::defined(rh1) || !FP::defined(t2)) {
            QVERIFY(!FP::defined(targetRH.value));
        } else {
            QCOMPARE(targetRH.value, Dewpoint::rhExtrapolate(t1, rh1, t2));
        }
    }

    void rhExtrapolate_data()
    {
        QTest::addColumn<double>("t1");
        QTest::addColumn<double>("rh1");
        QTest::addColumn<double>("t2");

        QTest::newRow("All missing") << FP::undefined() << FP::undefined() << FP::undefined();
        QTest::newRow("In temperature missing") << FP::undefined() << 10.0 << 10.0;
        QTest::newRow("In RH missing") << 10.0 << FP::undefined() << 10.0;
        QTest::newRow("Out temperature missing") << 10.0 << 10.0 << FP::undefined();
        QTest::newRow("10 50 20") << 10.0 << 50.0 << 20.0;
    }

    void consecutiveDifference()
    {
        TestChainTarget targetStart;
        TestChainTarget targetEnd;

        SmootherChainConsecutiveDifference cd(&targetStart, &targetEnd);

        cd.getTarget()->incomingData(100.0, 200.0, 1.0);
        cd.getTarget()->incomingData(200.0, 300.0, 2.0);
        QCOMPARE(targetStart.value, 1.0);
        QCOMPARE(targetEnd.value, 2.0);
        cd.getTarget()->incomingData(300.0, 400.0, 3.0);
        QCOMPARE(targetStart.value, 2.0);
        QCOMPARE(targetEnd.value, 3.0);

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            cd.serialize(stream);
        }
        QDataStream stream(&data, QIODevice::ReadOnly);
        SmootherChainConsecutiveDifference cd2(stream, &targetStart, &targetEnd);

        cd2.getTarget()->incomingData(400.0, 500.0, 4.0);
        QCOMPARE(targetStart.value, 3.0);
        QCOMPARE(targetEnd.value, 4.0);

        cd2.getTarget()->incomingAdvance(600.0);
        QCOMPARE(targetStart.value, 4.0);
        QVERIFY(!FP::defined(targetEnd.value));

        cd2.getTarget()->incomingData(600.0, 700.0, 5.0);
        QCOMPARE(targetStart.value, 4.0);
        QVERIFY(!FP::defined(targetEnd.value));

        cd2.getTarget()->incomingData(800.0, 900.0, 6.0);
        QCOMPARE(targetStart.value, 5.0);
        QVERIFY(!FP::defined(targetEnd.value));

        cd2.getTarget()->endData();
        QCOMPARE(targetStart.value, 6.0);
        QVERIFY(!FP::defined(targetEnd.value));
    }
};

QTEST_APPLESS_MAIN(TestSmoothingChain)

#include "smoothingchain.moc"
