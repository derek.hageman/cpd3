/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <mutex>
#include <QtGlobal>
#include <QTest>
#include <QString>

#include "smoothing/smoothingengine.hxx"
#include "smoothing/fixedtime.hxx"
#include "smoothing/segmentcontrolled.hxx"
#include "smoothing/realtimechain.hxx"
#include "smoothing/editingengine.hxx"
#include "core/number.hxx"
#include "core/waitutils.hxx"
#include "datacore/stream.hxx"
#include "algorithms/dewpoint.hxx"

using namespace CPD3;
using namespace CPD3::Smoothing;
using namespace CPD3::Data;

class TestTarget {
public:
    virtual ~TestTarget()
    { }

    virtual void incomingData(double start, double end, const QString &value) = 0;

    virtual void incomingAdvance(double time) = 0;

    virtual void endData() = 0;
};

class TestArrayMerge : public SmoothingEngineArrayMerger<QString, TestTarget, const QString &> {
public:
    TestArrayMerge(int size, const SequenceName &unit, SinkMultiplexer::Sink *target)
            : SmoothingEngineArrayMerger<QString, TestTarget, const QString &>(size, unit, target)
    { }

    TestArrayMerge(QDataStream &stream, const SequenceName &unit, SinkMultiplexer::Sink *target)
            : SmoothingEngineArrayMerger<QString, TestTarget, const QString &>(stream, unit, target)
    { }
};

struct TestArrayInput {
    int index;
    double start;
    double end;
    QString value;

    TestArrayInput() : index(-1), start(FP::undefined()), end(FP::undefined()), value()
    { }

    TestArrayInput(int i, double s, double e = 0, const QString &v = QString()) : index(i),
                                                                                  start(s),
                                                                                  end(e),
                                                                                  value(v)
    { }
};

Q_DECLARE_METATYPE(TestArrayInput);

Q_DECLARE_METATYPE(QList<TestArrayInput>);

class TestMatrixMerge : public SmoothingEngineMatrixMerger<QString, TestTarget, const QString &> {
public:
    TestMatrixMerge(const Variant::PathElement::MatrixIndex &size, const SequenceName &unit,
                    SinkMultiplexer::Sink *target) : SmoothingEngineMatrixMerger<QString,
                                                                                 TestTarget,
                                                                                 const QString &>(
            size, unit, target)
    { }

    TestMatrixMerge(QDataStream &stream, const SequenceName &unit, SinkMultiplexer::Sink *target)
            : SmoothingEngineMatrixMerger<QString, TestTarget, const QString &>(stream, unit,
                                                                                target)
    { }
};

struct TestMatrixInput {
    Variant::PathElement::MatrixIndex index;
    double start;
    double end;
    QString value;

    TestMatrixInput() : index(), start(FP::undefined()), end(FP::undefined()), value()
    { }

    TestMatrixInput(const Variant::PathElement::MatrixIndex &i,
                    double s,
                    double e = 0,
                    const QString &v = QString()) : index(i), start(s), end(e), value(v)
    { }

    TestMatrixInput(std::size_t a,
                    std::size_t b,
                    double s,
                    double e = 0,
                    const QString &v = QString()) : index({a, b}), start(s), end(e), value(v)
    { }
};

Q_DECLARE_METATYPE(TestMatrixInput);

Q_DECLARE_METATYPE(QList<TestMatrixInput>);

enum EngineType {
    Engine_Basic = 0,
    Engine_FixedTime,
    Engine_SegmentControlled,
    Engine_Realtime,
    Engine_Editing_FullCalculate,
    Engine_Editing_ContinuousRecalculate,
    Engine_Editing_Final,
};

Q_DECLARE_METATYPE(EngineType);

class TestEndpoint : public StreamSink {
public:
    bool ended;

    TestEndpoint() : ended(false)
    { }

    virtual ~TestEndpoint()
    { }

    SequenceValue::Transfer values;
    std::mutex mutex;

    void incomingData(const SequenceValue::Transfer &values) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        Util::append(values, this->values);
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        Util::append(std::move(values), this->values);
    }

    void incomingData(const SequenceValue &value) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        this->values.emplace_back(value);
    }

    void incomingData(SequenceValue &&value) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        this->values.emplace_back(std::move(value));
    }

    virtual void endData()
    {
        std::lock_guard<std::mutex> lock(mutex);
        ended = true;
    }

    void reset()
    {
        values.clear();
        ended = false;
    }

    static bool valuesEqual(const Variant::Read &a, const Variant::Read &b)
    {
        if (a.getType() != Variant::Type::Real)
            return a == b;
        if (b.getType() != Variant::Type::Real)
            return a == b;
        double va = a.toDouble();
        double vb = b.toDouble();
        if (!FP::defined(va))
            return !FP::defined(vb);
        if (!FP::defined(vb))
            return false;
        double eps = qMax(va, vb) * 1E-6;
        if (eps == 0)
            return va == vb;
        return fabs(va - vb) < eps;
    }

    bool engineCompare(SequenceValue::Transfer expected, EngineType engine = Engine_Basic)
    {
        std::lock_guard<std::mutex> lock(mutex);
        for (auto f = expected.begin(); f != expected.end();) {
            bool hit = false;
            for (auto c = values.begin(), endC = values.end(); c != endC; ++c) {
                if (engine != Engine_Realtime) {
                    if (!FP::equal(c->getStart(), f->getStart()))
                        continue;
                    if (!FP::equal(c->getEnd(), f->getEnd()))
                        continue;
                }
                if (c->getUnit() != f->getUnit())
                    continue;

                Variant::Root merged;
                if (c->getValue().getType() != Variant::Type::Array ||
                        f->getValue().getType() != Variant::Type::Array) {
                    merged = Variant::Root::overlay(c->root(), f->root());
                } else {
                    for (std::size_t i = 0,
                            max = std::max(c->read().toArray().size(), f->read().toArray().size());
                            i < max;
                            i++) {
                        merged.write()
                              .array(i)
                              .set(Variant::Root::overlay(Variant::Root(c->read().array(i)),
                                                          Variant::Root(f->read().array(i))));
                    }
                }
                if (!valuesEqual(merged, c->getValue())) {
                    qDebug() << "Value overlay mismatch for" << *f << "merged:" << merged
                             << "input:" << c->getValue();
                    return false;
                }

                values.erase(c);
                f = expected.erase(f);
                hit = true;
                break;
            }
            if (!hit)
                ++f;
        }
        if (!values.empty()) {
            qDebug() << "Unmatched values in result:" << values;
        }
        if (engine == Engine_Realtime)
            return values.empty();
        if (!expected.empty()) {
            qDebug() << "Unmatched values in expected:" << expected;
        }
        return values.empty() && expected.empty();
    }

    bool hasValueAfter(const SequenceName &unit, double time)
    {
        std::lock_guard<std::mutex> lock(mutex);
        for (const auto &v : values) {
            if (Range::compareStart(v.getStart(), time) < 0)
                continue;
            if (v.getUnit() != unit)
                continue;
            return true;
        }
        return false;
    }
};

class BasicBinnedChainEngine : public SmoothingEngine {
public:
    BasicBinnedChainEngine() : SmoothingEngine(new SmootherChainCoreFixedTime(
            new DynamicTimeInterval::Constant(Time::Second, 1000, true)))
    { }

    virtual ~BasicBinnedChainEngine()
    { }

protected:
    virtual void startProcessing(IncomingBuffer::iterator incomingBegin,
                                 IncomingBuffer::iterator incomingEnd,
                                 double &advanceTime,
                                 double &holdbackTime)
    {
        SmoothingEngine::startProcessing(incomingBegin, incomingEnd, advanceTime, holdbackTime);
        if (FP::defined(holdbackTime))
            holdbackTime = floor(holdbackTime / 1000.0) * 1000.0;
    }
};

static Variant::Root chd(const char *path, double v)
{
    Variant::Root output;
    output.write().setType(Variant::Type::Hash);
    output[path].setDouble(v);
    return output;
}

static Variant::Root cms(const char *meta = nullptr,
                         const char *path = nullptr,
                         const char *str = nullptr)
{
    Variant::Root output;
    output.write().setType(Variant::Type::MetadataReal);
    if (!meta)
        return output;
    if (!path || !(*path))
        output.write().metadataReal(meta).setString(str);
    else
        output.write().metadataReal(meta)[path].setString(str);
    return output;
}

static Variant::Root arr(const Variant::Read &fill, int n = 2)
{
    Variant::Root output;
    for (int i = 0; i < n; i++) {
        output.write().array(i).set(fill);
    }
    return output;
}

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestSmoothingEngine : public QObject {
Q_OBJECT

private slots:

    void arrayMerge()
    {
        QFETCH(QList<TestArrayInput>, input);

        SequenceName unit("brw", "raw", "N_N11");
        SequenceValue::Transfer expected;
        int max = -1;
        {
            std::map<double, Variant::Root> values;
            std::map<double, double> ends;
            for (QList<TestArrayInput>::const_iterator in = input.constBegin(),
                    end = input.constEnd(); in != end; ++in) {
                if (in->value.isEmpty())
                    continue;
                values[in->start].write().array(in->index).setString(in->value);
                ends[in->start] = in->end;
                max = qMax(max, in->index);
            }

            auto ie = ends.begin();
            for (auto vi = values.begin(), viEnd = values.end(); vi != viEnd; ++vi, ++ie) {
                expected.emplace_back(unit, std::move(vi->second), vi->first, ie->second);
            }
        }

        TestEndpoint output;

        SinkMultiplexer mux1;
        mux1.start();
        TestArrayMerge merge1(max + 1, unit, mux1.createSink());
        mux1.sinkCreationComplete();
        mux1.setEgress(&output);
        for (QList<TestArrayInput>::const_iterator in = input.constBegin(), end = input.constEnd();
                in != end; ++in) {
            if (in->value.isEmpty()) {
                merge1.getTarget(in->index)->incomingAdvance(in->start);
            } else {
                merge1.getTarget(in->index)->incomingData(in->start, in->end, in->value);
            }
        }
        for (int i = 0; i <= max; i++) {
            merge1.getTarget(i)->endData();
        }
        QVERIFY(mux1.wait(30));
        QCOMPARE(output.values, expected);
        output.reset();

        SinkMultiplexer mux2;
        mux2.start();
        TestArrayMerge merge2(max + 1, unit, mux2.createSink());
        mux2.sinkCreationComplete();
        mux2.setEgress(&output);
        int half = input.size() / 2;
        for (int i = 0; i < half; i++) {
            TestArrayInput in(input.at(i));
            if (in.value.isEmpty()) {
                merge2.getTarget(in.index)->incomingAdvance(in.start);
            } else {
                merge2.getTarget(in.index)->incomingData(in.start, in.end, in.value);
            }
        }

        merge2.pause();
        mux2.setEgress(NULL);
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            merge2.serialize(stream);
            stream << mux2;
        }
        merge2.resume();
        for (int i = 0; i <= max; i++) {
            merge2.getTarget(i)->endData();
        }
        mux2.signalTerminate();
        QVERIFY(mux2.wait(30));

        SinkMultiplexer mux3;
        mux3.start();
        QDataStream stream(&data, QIODevice::ReadOnly);
        TestArrayMerge merge3(stream, unit, mux3.createSink());
        stream >> mux3;
        mux3.sinkCreationComplete();
        mux3.setEgress(&output);
        for (int i = half; i < input.size(); i++) {
            TestArrayInput in(input.at(i));
            if (in.value.isEmpty()) {
                merge3.getTarget(in.index)->incomingAdvance(in.start);
            } else {
                merge3.getTarget(in.index)->incomingData(in.start, in.end, in.value);
            }
        }
        for (int i = 0; i <= max; i++) {
            merge3.getTarget(i)->endData();
        }
        QVERIFY(mux3.wait(30));
        QCOMPARE(output.values, expected);
    }

    void arrayMerge_data()
    {
        QTest::addColumn<QList<TestArrayInput> >("input");

        QTest::newRow("Empty") << QList<TestArrayInput>();

        QTest::newRow("One A") << (QList<TestArrayInput>() << TestArrayInput(0, 100, 200, "A")
                                                           << TestArrayInput(0, 200, 300, "B")
                                                           << TestArrayInput(0, 300, 400, "C"));
        QTest::newRow("One B") << (QList<TestArrayInput>() << TestArrayInput(0, 100, 200, "A"));
        QTest::newRow("One C") << (QList<TestArrayInput>() << TestArrayInput(0, 100, 200, "A")
                                                           << TestArrayInput(0, 200));
        QTest::newRow("One D") << (QList<TestArrayInput>() << TestArrayInput(0, 100)
                                                           << TestArrayInput(0, 100, 200, "A"));
        QTest::newRow("One E") << (QList<TestArrayInput>() << TestArrayInput(0, 100)
                                                           << TestArrayInput(0, 100, 200, "A")
                                                           << TestArrayInput(0, 200, 300, "B")
                                                           << TestArrayInput(0, 300)
                                                           << TestArrayInput(0, 400, 500, "C"));

        QTest::newRow("Two A") << (QList<TestArrayInput>() << TestArrayInput(0, 100, 200, "A1")
                                                           << TestArrayInput(1, 100, 200, "A2")
                                                           << TestArrayInput(0, 200, 300, "B1")
                                                           << TestArrayInput(1, 200, 300, "B2")
                                                           << TestArrayInput(0, 300, 400, "C1")
                                                           << TestArrayInput(1, 300, 400, "C2"));
        QTest::newRow("Two B") << (QList<TestArrayInput>() << TestArrayInput(0, 100, 200, "A1")
                                                           << TestArrayInput(1, 100, 200, "A2")
                                                           << TestArrayInput(1, 200, 300, "B2")
                                                           << TestArrayInput(0, 200, 300, "B1")
                                                           << TestArrayInput(0, 300, 400, "C1")
                                                           << TestArrayInput(1, 300, 400, "C2"));
        QTest::newRow("Two C") << (QList<TestArrayInput>() << TestArrayInput(0, 100, 200, "A1")
                                                           << TestArrayInput(1, 100, 200, "A2")
                                                           << TestArrayInput(0, 100)
                                                           << TestArrayInput(1, 100)
                                                           << TestArrayInput(0, 200, 300, "B1")
                                                           << TestArrayInput(1, 200, 300, "B2")
                                                           << TestArrayInput(0, 300, 400, "C1")
                                                           << TestArrayInput(1, 300, 400, "C2")
                                                           << TestArrayInput(0, 400));
        QTest::newRow("Two D") << (QList<TestArrayInput>() << TestArrayInput(0, 100, 200, "A1")
                                                           << TestArrayInput(0, 200, 300, "B1")
                                                           << TestArrayInput(0, 300, 400, "C1")
                                                           << TestArrayInput(1, 100, 200, "A2")
                                                           << TestArrayInput(1, 200, 300, "B2")
                                                           << TestArrayInput(1, 300, 400, "C2"));
        QTest::newRow("Two E") << (QList<TestArrayInput>() << TestArrayInput(0, 100, 200, "A1")
                                                           << TestArrayInput(0, 200, 300, "B1")
                                                           << TestArrayInput(0, 300, 400, "C1")
                                                           << TestArrayInput(1, 100, 200, "A2")
                                                           << TestArrayInput(1, 200, 300, "B2")
                                                           << TestArrayInput(1, 300, 400, "C2")
                                                           << TestArrayInput(0, 400, 500, "D1"));

        QTest::newRow("Three A") << (QList<TestArrayInput>() << TestArrayInput(0, 100, 200, "A1")
                                                             << TestArrayInput(1, 100, 200, "A2")
                                                             << TestArrayInput(2, 100, 200, "A3")
                                                             << TestArrayInput(0, 200, 300, "B1")
                                                             << TestArrayInput(1, 200, 300, "B2")
                                                             << TestArrayInput(2, 200, 300, "B3")
                                                             << TestArrayInput(0, 300, 400, "C1")
                                                             << TestArrayInput(1, 300, 400, "C2")
                                                             << TestArrayInput(2, 300, 400, "C3"));

        QTest::newRow("Complex A") << (QList<TestArrayInput>() << TestArrayInput(0, 100, 200, "A1")
                                                               << TestArrayInput(1, 100, 200, "B1")
                                                               << TestArrayInput(0, 200)
                                                               << TestArrayInput(2, 100, 200, "C1")
                                                               << TestArrayInput(1, 200)
                                                               << TestArrayInput(2, 200)
                                                               << TestArrayInput(0, 200, 300, "B1")
                                                               << TestArrayInput(1, 200, 300, "B2")
                                                               << TestArrayInput(2, 200, 300, "B3")
                                                               << TestArrayInput(2, 200)
                                                               << TestArrayInput(1, 200)
                                                               << TestArrayInput(0, 200)
                                                               << TestArrayInput(0, 300, 400, "C1")
                                                               << TestArrayInput(1, 300, 400, "C2")
                                                               << TestArrayInput(2, 300, 400, "C3")
                                                               << TestArrayInput(2, 400)
                                                               << TestArrayInput(1, 400)
                                                               << TestArrayInput(0, 400));
    }

    void matrixMerge()
    {
        QFETCH(QList<TestMatrixInput>, input);

        SequenceName unit("brw", "raw", "N_N11");
        SequenceValue::Transfer expected;
        Variant::PathElement::MatrixIndex size;
        std::size_t arrayMax = 0;
        {
            std::map<double, double> ends;
            for (const auto &in : input) {
                if (in.value.isEmpty())
                    continue;
                ends[in.start] = in.end;
                for (std::size_t dim = 0; dim < in.index.size(); dim++) {
                    if (dim >= size.size()) {
                        size.emplace_back(in.index.at(dim) + 1);
                        continue;
                    }
                    if (in.index.at(dim) < size.at(dim))
                        continue;
                    size[dim] = in.index.at(dim) + 1;
                }
            }
            std::map<double, Variant::Root> values;
            for (const auto &in : input) {
                if (in.value.isEmpty())
                    continue;
                if (!values[in.start].read().exists())
                    values[in.start].write().toMatrix().reshape(size);
                values[in.start].write().matrix(in.index).setString(in.value);
            }

            if (!size.empty()) {
                arrayMax = 1;
                for (auto add : size) {
                    arrayMax *= add;
                }
                QVERIFY(arrayMax > 0);
            }

            auto ie = ends.begin();
            for (auto vi = values.begin(), viEnd = values.end(); vi != viEnd; ++vi, ++ie) {
                expected.emplace_back(unit, std::move(vi->second), vi->first, ie->second);
            }
        }

        TestEndpoint output;

        SinkMultiplexer mux1;
        mux1.start();
        TestMatrixMerge merge1(size, unit, mux1.createSink());
        mux1.sinkCreationComplete();
        mux1.setEgress(&output);
        for (QList<TestMatrixInput>::const_iterator in = input.constBegin(), end = input.constEnd();
                in != end;
                ++in) {
            if (in->value.isEmpty()) {
                merge1.getTarget(merge1.lookupIndex(in->index))->incomingAdvance(in->start);
            } else {
                merge1.getTarget(merge1.lookupIndex(in->index))
                      ->incomingData(in->start, in->end, in->value);
            }
        }
        for (std::size_t i = 0; i < arrayMax; i++) {
            merge1.getTarget(i)->endData();
        }
        QVERIFY(mux1.wait(30));
        QCOMPARE(output.values, expected);
        output.reset();

        SinkMultiplexer mux2;
        mux2.start();
        TestMatrixMerge merge2(size, unit, mux2.createSink());
        mux2.sinkCreationComplete();
        mux2.setEgress(&output);
        int half = input.size() / 2;
        for (int i = 0; i < half; i++) {
            TestMatrixInput in(input.at(i));
            if (in.value.isEmpty()) {
                merge2.getTarget(merge2.lookupIndex(in.index))->incomingAdvance(in.start);
            } else {
                merge2.getTarget(merge2.lookupIndex(in.index))
                      ->incomingData(in.start, in.end, in.value);
            }
        }

        merge2.pause();
        mux2.setEgress(NULL);
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            merge2.serialize(stream);
            stream << mux2;
        }
        merge2.resume();
        for (std::size_t i = 0; i < arrayMax; i++) {
            merge2.getTarget(i)->endData();
        }
        mux2.signalTerminate();
        QVERIFY(mux2.wait(30));

        SinkMultiplexer mux3;
        mux3.start();
        QDataStream stream(&data, QIODevice::ReadOnly);
        TestMatrixMerge merge3(stream, unit, mux3.createSink());
        stream >> mux3;
        mux3.sinkCreationComplete();
        mux3.setEgress(&output);
        for (int i = half; i < input.size(); i++) {
            TestMatrixInput in(input.at(i));
            if (in.value.isEmpty()) {
                merge3.getTarget(merge3.lookupIndex(in.index))->incomingAdvance(in.start);
            } else {
                merge3.getTarget(merge3.lookupIndex(in.index))
                      ->incomingData(in.start, in.end, in.value);
            }
        }
        for (std::size_t i = 0; i < arrayMax; i++) {
            merge3.getTarget(i)->endData();
        }
        QVERIFY(mux3.wait(30));
        QCOMPARE(output.values, expected);
    }

    void matrixMerge_data()
    {
        QTest::addColumn<QList<TestMatrixInput> >("input");

        QTest::newRow("Empty") << QList<TestMatrixInput>();

        QTest::newRow("One A") << (QList<TestMatrixInput>() << TestMatrixInput(0, 1, 100, 200, "A")
                                                            << TestMatrixInput(0, 1, 200, 300, "B")
                                                            << TestMatrixInput(0, 1, 300, 400,
                                                                               "C"));
        QTest::newRow("One B")
                << (QList<TestMatrixInput>() << TestMatrixInput(0, 0, 100, 200, "A"));
        QTest::newRow("One C") << (QList<TestMatrixInput>() << TestMatrixInput(1, 0, 100, 200, "A")
                                                            << TestMatrixInput(1, 0, 200));
        QTest::newRow("One D") << (QList<TestMatrixInput>() << TestMatrixInput(0, 0, 100)
                                                            << TestMatrixInput(0, 0, 100, 200,
                                                                               "A"));
        QTest::newRow("One E") << (QList<TestMatrixInput>() << TestMatrixInput(0, 0, 100)
                                                            << TestMatrixInput(0, 0, 100, 200, "A")
                                                            << TestMatrixInput(0, 0, 200, 300, "B")
                                                            << TestMatrixInput(0, 0, 300)
                                                            << TestMatrixInput(0, 0, 400, 500,
                                                                               "C"));

        QTest::newRow("Two A") << (QList<TestMatrixInput>() << TestMatrixInput(0, 0, 100, 200, "A1")
                                                            << TestMatrixInput(1, 1, 100, 200, "A2")
                                                            << TestMatrixInput(0, 0, 200, 300, "B1")
                                                            << TestMatrixInput(1, 1, 200, 300, "B2")
                                                            << TestMatrixInput(0, 0, 300, 400, "C1")
                                                            << TestMatrixInput(1, 1, 300, 400,
                                                                               "C2"));
        QTest::newRow("Two B") << (QList<TestMatrixInput>() << TestMatrixInput(0, 1, 100, 200, "A1")
                                                            << TestMatrixInput(1, 0, 100, 200, "A2")
                                                            << TestMatrixInput(1, 0, 200, 300, "B2")
                                                            << TestMatrixInput(0, 1, 200, 300, "B1")
                                                            << TestMatrixInput(0, 1, 300, 400, "C1")
                                                            << TestMatrixInput(1, 0, 300, 400,
                                                                               "C2"));
        QTest::newRow("Two C") << (QList<TestMatrixInput>() << TestMatrixInput(0, 2, 100, 200, "A1")
                                                            << TestMatrixInput(1, 2, 100, 200, "A2")
                                                            << TestMatrixInput(0, 2, 100)
                                                            << TestMatrixInput(1, 2, 100)
                                                            << TestMatrixInput(0, 2, 200, 300, "B1")
                                                            << TestMatrixInput(1, 2, 200, 300, "B2")
                                                            << TestMatrixInput(0, 2, 300, 400, "C1")
                                                            << TestMatrixInput(1, 2, 300, 400, "C2")
                                                            << TestMatrixInput(0, 2, 400));
        QTest::newRow("Two D")
                << (QList<TestMatrixInput>() << TestMatrixInput({2, 0, 1}, 100, 200, "A1")
                                             << TestMatrixInput({2, 0, 1}, 200, 300, "B1")
                                             << TestMatrixInput({2, 0, 1}, 300, 400, "C1")
                                             << TestMatrixInput({0, 1, 2}, 100, 200, "A2")
                                             << TestMatrixInput({0, 1, 2}, 200, 300, "B2")
                                             << TestMatrixInput({0, 1, 2}, 300, 400, "C2"));
        QTest::newRow("Two E") << (QList<TestMatrixInput>() << TestMatrixInput(0, 3, 100, 200, "A1")
                                                            << TestMatrixInput(0, 3, 200, 300, "B1")
                                                            << TestMatrixInput(0, 3, 300, 400, "C1")
                                                            << TestMatrixInput(1, 0, 100, 200, "A2")
                                                            << TestMatrixInput(1, 0, 200, 300, "B2")
                                                            << TestMatrixInput(1, 0, 300, 400, "C2")
                                                            << TestMatrixInput(0, 3, 400, 500,
                                                                               "D1"));

        QTest::newRow("Three A")
                << (QList<TestMatrixInput>() << TestMatrixInput(0, 2, 100, 200, "A1")
                                             << TestMatrixInput(1, 1, 100, 200, "A2")
                                             << TestMatrixInput(2, 0, 100, 200, "A3")
                                             << TestMatrixInput(0, 2, 200, 300, "B1")
                                             << TestMatrixInput(1, 1, 200, 300, "B2")
                                             << TestMatrixInput(2, 0, 200, 300, "B3")
                                             << TestMatrixInput(0, 2, 300, 400, "C1")
                                             << TestMatrixInput(1, 1, 300, 400, "C2")
                                             << TestMatrixInput(2, 0, 300, 400, "C3"));

        QTest::newRow("Complex A")
                << (QList<TestMatrixInput>() << TestMatrixInput(0, 1, 100, 200, "A1")
                                             << TestMatrixInput(1, 2, 100, 200, "B1")
                                             << TestMatrixInput(0, 1, 200)
                                             << TestMatrixInput(2, 3, 100, 200, "C1")
                                             << TestMatrixInput(1, 2, 200)
                                             << TestMatrixInput(2, 3, 200)
                                             << TestMatrixInput(0, 1, 200, 300, "B1")
                                             << TestMatrixInput(1, 2, 200, 300, "B2")
                                             << TestMatrixInput(2, 3, 200, 300, "B3")
                                             << TestMatrixInput(2, 3, 200)
                                             << TestMatrixInput(1, 2, 200)
                                             << TestMatrixInput(0, 1, 200)
                                             << TestMatrixInput(0, 1, 300, 400, "C1")
                                             << TestMatrixInput(1, 2, 300, 400, "C2")
                                             << TestMatrixInput(2, 3, 300, 400, "C3")
                                             << TestMatrixInput(2, 3, 400)
                                             << TestMatrixInput(1, 2, 400)
                                             << TestMatrixInput(0, 1, 400));
    }


    void engine()
    {
        QFETCH(EngineType, type);
        QFETCH(SequenceValue::Transfer, input);
        QFETCH(SequenceValue::Transfer, expected);

        TestEndpoint output;
        SmoothingEngine *e = NULL;

        switch (type) {
        case Engine_Basic:
            e = new BasicBinnedChainEngine;
            break;
        case Engine_FixedTime:
            e = new SmoothingEngineFixedTime(
                    new DynamicTimeInterval::Constant(Time::Second, 1000, true));
            break;
        case Engine_SegmentControlled:
            e = new SmoothingEngineSegmentControlled(
                    new DynamicSequenceSelection::Basic(SequenceName("a", "b", "seg")),
                    new DynamicSequenceSelection::Basic(SequenceName("a", "b", "out")));
            break;
        case Engine_Realtime:
            e = new RealtimeEngine;
            break;
        case Engine_Editing_FullCalculate:
            e = new EditingEngineFullCalculate(
                    new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL,
                    new SequenceMatch::Composite(SequenceMatch::Element::variable("c")));
            break;
        case Engine_Editing_ContinuousRecalculate:
            e = new EditingEngineContinuousRecalculate(
                    new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL,
                    new SequenceMatch::Composite(SequenceMatch::Element::variable("c")));
            break;
        case Engine_Editing_Final:
            e = new EditingEngineFinal(new DynamicTimeInterval::Constant(Time::Second, 1000, true),
                                       NULL,
                                       new SequenceMatch::Composite(SequenceName("a", "b", "cont")),
                                       new SequenceMatch::Composite(
                                               SequenceName("a", "b", "full")));
            break;
        }

        e->start();
        e->setEgress(&output);
        e->incomingData(input);
        e->endData();
        QVERIFY(e->wait(30));
        delete e;
        QVERIFY(output.ended);
        QVERIFY(output.engineCompare(expected, type));

        output.reset();
        switch (type) {
        case Engine_Basic:
            e = new BasicBinnedChainEngine;
            break;
        case Engine_FixedTime:
            e = new SmoothingEngineFixedTime(
                    new DynamicTimeInterval::Constant(Time::Second, 1000, true));
            break;
        case Engine_SegmentControlled:
            e = new SmoothingEngineSegmentControlled(
                    new DynamicSequenceSelection::Basic(SequenceName("a", "b", "seg")),
                    new DynamicSequenceSelection::Basic(SequenceName("a", "b", "out")));
            break;
        case Engine_Realtime:
            e = new RealtimeEngine;
            break;
        case Engine_Editing_FullCalculate:
            e = new EditingEngineFullCalculate(
                    new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL,
                    new SequenceMatch::Composite(SequenceMatch::Element::variable("c")));
            break;
        case Engine_Editing_ContinuousRecalculate:
            e = new EditingEngineContinuousRecalculate(
                    new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL,
                    new SequenceMatch::Composite(SequenceMatch::Element::variable("c")));
            break;
        case Engine_Editing_Final:
            e = new EditingEngineFinal(new DynamicTimeInterval::Constant(Time::Second, 1000, true),
                                       NULL,
                                       new SequenceMatch::Composite(SequenceName("a", "b", "cont")),
                                       new SequenceMatch::Composite(
                                               SequenceName("a", "b", "full")));
            break;
        }

        e->start();
        e->setEgress(&output);
        e->incomingData(input);
        e->endData();
        QVERIFY(e->wait(30));
        delete e;
        QVERIFY(output.ended);
        QVERIFY(output.engineCompare(expected, type));


        std::size_t half = input.size() / 2;
        output.reset();
        switch (type) {
        case Engine_Basic:
            e = new BasicBinnedChainEngine;
            break;
        case Engine_FixedTime:
            e = new SmoothingEngineFixedTime(
                    new DynamicTimeInterval::Constant(Time::Second, 1000, true));
            break;
        case Engine_SegmentControlled:
            e = new SmoothingEngineSegmentControlled(
                    new DynamicSequenceSelection::Basic(SequenceName("a", "b", "seg")),
                    new DynamicSequenceSelection::Basic(SequenceName("a", "b", "out")));
            break;
        case Engine_Realtime:
            e = new RealtimeEngine;
            break;
        case Engine_Editing_FullCalculate:
            e = new EditingEngineFullCalculate(
                    new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL,
                    new SequenceMatch::Composite(SequenceMatch::Element::variable("c")));
            break;
        case Engine_Editing_ContinuousRecalculate:
            e = new EditingEngineContinuousRecalculate(
                    new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL,
                    new SequenceMatch::Composite(SequenceMatch::Element::variable("c")));
            break;
        case Engine_Editing_Final:
            e = new EditingEngineFinal(new DynamicTimeInterval::Constant(Time::Second, 1000, true),
                                       NULL,
                                       new SequenceMatch::Composite(SequenceName("a", "b", "cont")),
                                       new SequenceMatch::Composite(
                                               SequenceName("a", "b", "full")));
            break;
        }
        e->start();
        e->setEgress(&output);
        for (std::size_t i = 0; i < half; i++) {
            e->incomingData(input.at(i));
        }
        QTest::qSleep(50);
        {
            e->setEgress(NULL);
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                e->serialize(stream);
            }
            e->endData();
            e->signalTerminate();
            QVERIFY(e->wait(30));
            delete e;
            switch (type) {
            case Engine_Basic:
                e = new BasicBinnedChainEngine;
                break;
            case Engine_FixedTime:
                e = new SmoothingEngineFixedTime(
                        new DynamicTimeInterval::Constant(Time::Second, 1000, true));
                break;
            case Engine_SegmentControlled:
                e = new SmoothingEngineSegmentControlled(
                        new DynamicSequenceSelection::Basic(SequenceName("a", "b", "seg")),
                        new DynamicSequenceSelection::Basic(SequenceName("a", "b", "out")));
                break;
            case Engine_Realtime:
                e = new RealtimeEngine;
                break;
            case Engine_Editing_FullCalculate:
                e = new EditingEngineFullCalculate(
                        new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL,
                        new SequenceMatch::Composite(SequenceMatch::Element::variable("c")));
                break;
            case Engine_Editing_ContinuousRecalculate:
                e = new EditingEngineContinuousRecalculate(
                        new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL,
                        new SequenceMatch::Composite(SequenceMatch::Element::variable("c")));
                break;
            case Engine_Editing_Final:
                e = new EditingEngineFinal(
                        new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL,
                        new SequenceMatch::Composite(SequenceName("a", "b", "cont")),
                        new SequenceMatch::Composite(SequenceName("a", "b", "full")));
                break;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                e->deserialize(stream);
            }
            e->start();
            e->setEgress(&output);
        }
        for (std::size_t i = half; i < input.size(); i++) {
            e->incomingData(input.at(i));
        }
        e->endData();
        QVERIFY(e->wait(30));
        delete e;
        QVERIFY(output.ended);
        QVERIFY(output.engineCompare(expected, type));


        if (type == Engine_FixedTime || type == Engine_Realtime) {
            output.reset();
            switch (type) {
            case Engine_FixedTime:
                e = new SmoothingEngineFixedTime(
                        new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL, NULL,
                        false, false);
                break;
            case Engine_Realtime:
                e = new RealtimeEngine(false);
                break;
            default:
                Q_ASSERT(false);
                break;
            }
            e->start();
            e->setEgress(&output);
            for (std::size_t i = 0; i < half; i++) {
                e->incomingData(input.at(i));
            }
            QTest::qSleep(50);
            {
                e->setEgress(NULL);
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    e->serialize(stream);
                }
                e->endData();
                e->signalTerminate();
                QVERIFY(e->wait(30));
                delete e;
                switch (type) {
                case Engine_FixedTime:
                    e = new SmoothingEngineFixedTime(
                            new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL, NULL,
                            false, false);
                    break;
                case Engine_Realtime:
                    e = new RealtimeEngine(false);
                    break;
                default:
                    Q_ASSERT(false);
                    break;
                }
                {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    e->deserialize(stream);
                }
                e->start();
                e->setEgress(&output);
            }
            for (std::size_t i = half; i < input.size(); i++) {
                e->incomingData(input.at(i));
            }
            e->endData();
            QVERIFY(e->wait(30));
            delete e;
            QVERIFY(output.ended);

            SequenceValue::Transfer withoutStats(expected);
            for (auto check = withoutStats.begin(); check != withoutStats.end();) {
                if (!check->getUnit().hasFlavor("stats")) {
                    ++check;
                    continue;
                }
                check = withoutStats.erase(check);
            }
            QVERIFY(output.engineCompare(withoutStats, type));
        }


        /* Skip this part for the bulk test (so it doesn't take forever) */
        if (input.size() > 1000)
            return;

        output.reset();
        switch (type) {
        case Engine_Basic:
            e = new BasicBinnedChainEngine;
            break;
        case Engine_FixedTime:
            e = new SmoothingEngineFixedTime(
                    new DynamicTimeInterval::Constant(Time::Second, 1000, true));
            break;
        case Engine_SegmentControlled:
            e = new SmoothingEngineSegmentControlled(
                    new DynamicSequenceSelection::Basic(SequenceName("a", "b", "seg")),
                    new DynamicSequenceSelection::Basic(SequenceName("a", "b", "out")));
            break;
        case Engine_Realtime:
            e = new RealtimeEngine;
            break;
        case Engine_Editing_FullCalculate:
            e = new EditingEngineFullCalculate(
                    new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL,
                    new SequenceMatch::Composite(SequenceMatch::Element::variable("c")));
            break;
        case Engine_Editing_ContinuousRecalculate:
            e = new EditingEngineContinuousRecalculate(
                    new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL,
                    new SequenceMatch::Composite(SequenceMatch::Element::variable("c")));
            break;
        case Engine_Editing_Final:
            e = new EditingEngineFinal(new DynamicTimeInterval::Constant(Time::Second, 1000, true),
                                       NULL,
                                       new SequenceMatch::Composite(SequenceName("a", "b", "cont")),
                                       new SequenceMatch::Composite(
                                               SequenceName("a", "b", "full")));
            break;
        }

        e->start();
        e->setEgress(&output);
        QTest::qSleep(50);
        for (std::size_t i = 0; i < input.size(); i++) {
            QTest::qSleep(50);
            e->incomingData(input.at(i));
        }
        e->endData();
        QVERIFY(e->wait(30));
        delete e;
        QVERIFY(output.ended);
        QVERIFY(output.engineCompare(expected, type));
    }

    void engine_data()
    {
        QTest::addColumn<EngineType>("type");
        QTest::addColumn<SequenceValue::Transfer>("input");
        QTest::addColumn<SequenceValue::Transfer>("expected");

        QTest::newRow("Empty Basic") << Engine_Basic << (SequenceValue::Transfer())
                                     << (SequenceValue::Transfer());
        QTest::newRow("Empty Fixed Time") << Engine_FixedTime << (SequenceValue::Transfer())
                                          << (SequenceValue::Transfer());
        QTest::newRow("Empty Segment Controlled") << Engine_SegmentControlled
                                                  << (SequenceValue::Transfer())
                                                  << (SequenceValue::Transfer());
        QTest::newRow("Empty Realtime") << Engine_Realtime << (SequenceValue::Transfer())
                                        << (SequenceValue::Transfer());
        QTest::newRow("Empty Editing Full") << Engine_Editing_FullCalculate
                                            << (SequenceValue::Transfer())
                                            << (SequenceValue::Transfer());
        QTest::newRow("Empty Editing Cont") << Engine_Editing_ContinuousRecalculate
                                            << (SequenceValue::Transfer())
                                            << (SequenceValue::Transfer());
        QTest::newRow("Empty Editing Final") << Engine_Editing_Final << (SequenceValue::Transfer())
                                             << (SequenceValue::Transfer());

        QTest::newRow("General single") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 2000.0)})
                                        << (SequenceValue::Transfer{
                                                SequenceValue(SequenceName("a", "b", "c"),
                                                              Variant::Root(1.0), 1000.0, 2000.0),
                                                SequenceValue(SequenceName("a", "b", "c",
                                                                           SequenceName::Flavors{
                                                                                   "stats"}),
                                                              chd("Mean", 1.0), 1000.0, 2000.0)});
        QTest::newRow("General single cover") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.9), 1000.0, 2000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.9), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"stats"}),
                              chd("Mean", 1.0), 1000.0, 2000.0)});
        QTest::newRow("General single composite") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.5), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"cover"}),
                              Variant::Root(1.0), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(4.0), 1500.0, 2000.0)})
                                                  << (SequenceValue::Transfer{
                                                          SequenceValue(SequenceName("a", "b", "c"),
                                                                        Variant::Root(3.0), 1000.0,
                                                                        2000.0), SequenceValue(
                                                                  SequenceName("a", "b", "c",
                                                                               SequenceName::Flavors{
                                                                                       "cover"}),
                                                                  Variant::Root(0.75), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "b", "c",
                                                                               SequenceName::Flavors{
                                                                                       "stats"}),
                                                                  chd("Mean", 2.5), 1000.0,
                                                                  2000.0)});
        QTest::newRow("General two") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(2.0), 2000.0, 3000.0)})
                                     << (SequenceValue::Transfer{
                                             SequenceValue(SequenceName("a", "b", "c"),
                                                           Variant::Root(1.0), 1000.0, 2000.0),
                                             SequenceValue(SequenceName("a", "b", "c",
                                                                        SequenceName::Flavors{
                                                                                "stats"}),
                                                           chd("Mean", 1.0), 1000.0, 2000.0),
                                             SequenceValue(SequenceName("a", "b", "c"),
                                                           Variant::Root(2.0), 2000.0, 3000.0),
                                             SequenceValue(SequenceName("a", "b", "c",
                                                                        SequenceName::Flavors{
                                                                                "stats"}),
                                                           chd("Mean", 2.0), 2000.0, 3000.0)});
        QTest::newRow("General single split cover") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(2.0), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.25), 1500.0, 2000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.2), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.625), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"stats"}),
                              chd("Mean", 1.5), 1000.0, 2000.0)});
        QTest::newRow("General single metadata") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b_meta", "c"), cms(), 1000.0, 2000.0)})
                                                 << (SequenceValue::Transfer{
                                                         SequenceValue(SequenceName("a", "b", "c"),
                                                                       Variant::Root(1.0), 1000.0,
                                                                       2000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c",
                                                                              SequenceName::Flavors{
                                                                                      "stats"}),
                                                                 chd("Mean", 1.0), 1000.0, 2000.0),
                                                         SequenceValue(
                                                                 SequenceName("a", "b_meta", "c"),
                                                                 cms(), 1000.0, 2000.0)});


        QTest::newRow("Bypass single") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 1800.0),
                SequenceValue(SequenceName("a", "b_meta", "c"), cms("Smoothing", "Mode", "Bypass"),
                              1000.0, 2000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 1800.0),
                SequenceValue(SequenceName("a", "b_meta", "c"), cms("Smoothing", "Mode", "Bypass"),
                              1000.0, 2000.0)});
        QTest::newRow("Bypass two") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b_meta", "c"), cms("Smoothing", "Mode", "Bypass"),
                              1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(2.0), 1500.0, 2000.0)})
                                    << (SequenceValue::Transfer{
                                            SequenceValue(SequenceName("a", "b", "c"),
                                                          Variant::Root(1.0), 1000.0, 1500.0),
                                            SequenceValue(SequenceName("a", "b_meta", "c"),
                                                          cms("Smoothing", "Mode", "Bypass"),
                                                          1000.0, 2000.0),
                                            SequenceValue(SequenceName("a", "b", "c"),
                                                          Variant::Root(2.0), 1500.0, 2000.0)});

        QTest::newRow("Transition from bypass") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b_meta", "c"), cms("Smoothing", "Mode", "Bypass"),
                              1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(2.0), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(3.0), 2000.0, 2500.0),
                SequenceValue(SequenceName("a", "b_meta", "c"), cms("Smoothing", "Mode", "General"),
                              2000.0, 3000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(4.0), 2500.0, 3000.0)})
                                                << (SequenceValue::Transfer{
                                                        SequenceValue(SequenceName("a", "b", "c"),
                                                                      Variant::Root(1.0), 1000.0,
                                                                      1500.0), SequenceValue(
                                                                SequenceName("a", "b_meta", "c"),
                                                                cms("Smoothing", "Mode", "Bypass"),
                                                                1000.0, 2000.0),
                                                        SequenceValue(SequenceName("a", "b", "c"),
                                                                      Variant::Root(2.0), 1500.0,
                                                                      2000.0),
                                                        SequenceValue(SequenceName("a", "b", "c"),
                                                                      Variant::Root(3.5), 2000.0,
                                                                      3000.0), SequenceValue(
                                                                SequenceName("a", "b", "c",
                                                                             SequenceName::Flavors{
                                                                                     "stats"}),
                                                                chd("Mean", 3.5), 2000.0, 3000.0),
                                                        SequenceValue(
                                                                SequenceName("a", "b_meta", "c"),
                                                                cms("Smoothing", "Mode", "General"),
                                                                2000.0, 3000.0)});
        QTest::newRow("Transition to bypass") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b_meta", "c"), cms("Smoothing", "Mode", "General"),
                              1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(2.0), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(3.0), 2000.0, 2500.0),
                SequenceValue(SequenceName("a", "b_meta", "c"), cms("Smoothing", "Mode", "Bypass"),
                              2000.0, 3000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(4.0), 2500.0, 3000.0)})
                                              << (SequenceValue::Transfer{
                                                      SequenceValue(SequenceName("a", "b", "c"),
                                                                    Variant::Root(1.5), 1000.0,
                                                                    2000.0), SequenceValue(
                                                              SequenceName("a", "b", "c",
                                                                           SequenceName::Flavors{
                                                                                   "stats"}),
                                                              chd("Mean", 1.5), 1000.0, 2000.0),
                                                      SequenceValue(
                                                              SequenceName("a", "b_meta", "c"),
                                                              cms("Smoothing", "Mode", "General"),
                                                              1000.0, 2000.0),
                                                      SequenceValue(SequenceName("a", "b", "c"),
                                                                    Variant::Root(3.0), 2000.0,
                                                                    2500.0), SequenceValue(
                                                              SequenceName("a", "b_meta", "c"),
                                                              cms("Smoothing", "Mode", "Bypass"),
                                                              2000.0, 3000.0),
                                                      SequenceValue(SequenceName("a", "b", "c"),
                                                                    Variant::Root(4.0), 2500.0,
                                                                    3000.0)});

        Variant::Root meta;
        meta.write().setType(Variant::Type::MetadataReal);
        meta.write().metadata("Smoothing").hash("Mode").setString("Vector2D");
        meta.write().metadata("Smoothing").hash("Parameters").hash("Direction").setString("dir");
        meta.write().metadata("Smoothing").hash("Parameters").hash("Magnitude").setString("mag");

        QTest::newRow("Multiple input vector") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b_meta", "dir"), meta, 1000.0, 3000.0),
                SequenceValue(SequenceName("a", "b_meta", "mag"), meta, 1000.0, 3000.0),
                SequenceValue(SequenceName("a", "b", "dir"), Variant::Root(30.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b", "mag"), Variant::Root(1.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b", "dir"), Variant::Root(150.0), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "mag"), Variant::Root(1.0), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "dir"), Variant::Root(120.0), 2000.0, 2500.0),
                SequenceValue(SequenceName("a", "b", "mag", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.5), 2000.0, 3000.0),
                SequenceValue(SequenceName("a", "b", "mag"), Variant::Root(1.0), 2000.0, 2500.0),
                SequenceValue(SequenceName("a", "b", "dir"), Variant::Root(240.0), 2500.0, 3000.0),
                SequenceValue(SequenceName("a", "b", "mag"), Variant::Root(1.0), 2500.0, 3000.0)})
                                               << (SequenceValue::Transfer{SequenceValue(
                                                       SequenceName("a", "b_meta", "dir"), meta,
                                                       1000.0, 3000.0), SequenceValue(
                                                       SequenceName("a", "b_meta", "mag"), meta,
                                                       1000.0, 3000.0), SequenceValue(
                                                       SequenceName("a", "b", "dir"),
                                                       Variant::Root(90.0), 1000.0, 2000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("a",
                                                                                                "b",
                                                                                                "mag"),
                                                                                   Variant::Root(
                                                                                           0.5),
                                                                                   1000.0, 2000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("a",
                                                                                                "b",
                                                                                                "mag",
                                                                                                SequenceName::Flavors{
                                                                                                        "stats"}),
                                                                                   chd("Mean", 1.0),
                                                                                   1000.0, 2000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("a",
                                                                                                "b",
                                                                                                "dir"),
                                                                                   Variant::Root(
                                                                                           180.0),
                                                                                   2000.0, 3000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("a",
                                                                                                "b",
                                                                                                "mag"),
                                                                                   Variant::Root(
                                                                                           0.5),
                                                                                   2000.0, 3000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("a",
                                                                                                "b",
                                                                                                "mag",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                   Variant::Root(
                                                                                           0.5),
                                                                                   2000.0, 3000.0),
                                                                           SequenceValue(
                                                                                   SequenceName("a",
                                                                                                "b",
                                                                                                "mag",
                                                                                                SequenceName::Flavors{
                                                                                                        "stats"}),
                                                                                   chd("Mean", 1.0),
                                                                                   2000.0,
                                                                                   3000.0)});

        meta.write().setEmpty();
        meta.write().setType(Variant::Type::MetadataReal);

        QTest::newRow("Difference basic") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b_meta", "c"),
                              cms("Smoothing", "Mode", "Difference"), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(2.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(3.0), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(4.0), 1500.0, 2000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(4.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b_meta", "c"), meta, 1000.0, 2000.0)});
        QTest::newRow("Difference reversed") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b_meta", "c"),
                              cms("Smoothing", "Mode", "Difference"), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(2.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(4.0), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(3.0), 1500.0, 2000.0)})
                                             << (SequenceValue::Transfer{
                                                     SequenceValue(SequenceName("a", "b", "c"),
                                                                   Variant::Root(1.0), 1000.0,
                                                                   2000.0), SequenceValue(
                                                             SequenceName("a", "b", "c",
                                                                          SequenceName::Flavors{
                                                                                  "end"}),
                                                             Variant::Root(4.0), 1000.0, 2000.0),
                                                     SequenceValue(SequenceName("a", "b_meta", "c"),
                                                                   meta, 1000.0, 2000.0)});

        QTest::newRow("Difference initial") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b_meta", "c"),
                              cms("Smoothing", "Mode", "DifferenceInitial"), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 1900.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(2.0), 1900.0, 2000.0)})
                                            << (SequenceValue::Transfer{
                                                    SequenceValue(SequenceName("a", "b", "c"),
                                                                  Variant::Root(1.0), 1000.0,
                                                                  2000.0), SequenceValue(
                                                            SequenceName("a", "b", "c",
                                                                         SequenceName::Flavors{
                                                                                 "end"}),
                                                            Variant::Root(2.0), 1000.0, 2000.0),
                                                    SequenceValue(SequenceName("a", "b_meta", "c"),
                                                                  meta, 1000.0, 2000.0)});
        QTest::newRow("Difference initial undefined") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b_meta", "c"),
                              cms("Smoothing", "Mode", "DifferenceInitial"), 1000.0, 4000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(FP::undefined()), 1000.0,
                              1900.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(FP::undefined()), 1900.0,
                              2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(FP::undefined()), 2000.0,
                              2900.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(FP::undefined()), 2900.0,
                              3000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(FP::undefined()), 3000.0,
                              4000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(FP::undefined()), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(FP::undefined()), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(FP::undefined()), 2000.0,
                              3000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(FP::undefined()), 2000.0, 3000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(FP::undefined()), 3000.0,
                              4000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(FP::undefined()), 3000.0, 4000.0),
                SequenceValue(SequenceName("a", "b_meta", "c"), meta, 1000.0, 4000.0)});

        meta.write().setEmpty();
        meta.write().setType(Variant::Type::MetadataArray);
        meta.write().metadata("Smoothing").hash("Mode").setString("General");

        Variant::Root v1;
        v1.write().array(0).setDouble(1.0);
        v1.write().array(1).setDouble(3.0);
        Variant::Root v2;
        v2.write().array(0).setDouble(2.0);
        v2.write().array(1).setDouble(4.0);
        Variant::Root v3;
        v3.write().array(0).setDouble(1.5);
        v3.write().array(1).setDouble(3.5);
        Variant::Root v4;
        v4.write().array(0).hash("Mean").setDouble(1.5);
        v4.write().array(1).hash("Mean").setDouble(3.5);
        QTest::newRow("Array basic") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b_meta", "c"), meta, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), v1, 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b", "c"), v2, 1500.0, 2000.0)})
                                     << (SequenceValue::Transfer{
                                             SequenceValue(SequenceName("a", "b", "c"), v3, 1000.0,
                                                           2000.0), SequenceValue(
                                                     SequenceName("a", "b", "c",
                                                                  SequenceName::Flavors{"stats"}),
                                                     v4, 1000.0, 2000.0),
                                             SequenceValue(SequenceName("a", "b_meta", "c"), meta,
                                                           1000.0, 2000.0)});

        v3.write().array(0).setDouble(1.2);
        v3.write().array(1).setDouble(3.2);
        Variant::Root v5;
        v5.write().array(0).setDouble(0.25);
        v5.write().array(1).setDouble(0.25);
        Variant::Root v6;
        v6.write().array(0).setDouble(0.625);
        v6.write().array(1).setDouble(0.625);
        QTest::newRow("Array cover") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b_meta", "c"), meta, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), v1, 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b", "c"), v2, 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"cover"}), v5,
                              1500.0, 2000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), v3, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"stats"}), v4,
                              1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"cover"}), v6,
                              1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b_meta", "c"), meta, 1000.0, 2000.0)});

        v4.write().setEmpty();
        v4.write().array(0).hash("Mean").setDouble(1.0);
        v4.write().array(1).hash("Mean").setDouble(3.0);
        QTest::newRow("Array type change") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), v1, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(10.0), 2000.0, 3000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.9), 2000.0, 3000.0),
                SequenceValue(SequenceName("a", "b", "c"), v1, 3000.0, 4000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"cover"}), v5,
                              3000.0, 4000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), v1, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"stats"}), v4,
                              1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(10.0), 2000.0, 3000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.9), 2000.0, 3000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"stats"}),
                              chd("Mean", 10.0), 2000.0, 3000.0),
                SequenceValue(SequenceName("a", "b", "c"), v1, 3000.0, 4000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"stats"}), v4,
                              3000.0, 4000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"cover"}), v5,
                              3000.0, 4000.0)});

        v1.write().setEmpty();
        v2.write().setEmpty();
        v3.write().setEmpty();
        v4.write().setEmpty();
        v5.write().setEmpty();
        v1.write().array(0).setDouble(1.0);
        v1.write().array(1).setDouble(3.0);
        v2.write().array(0).setDouble(2.0);
        v3.write().array(0).hash("Mean").setDouble(1.0);
        v3.write().array(1).hash("Mean").setDouble(3.0);
        v4.write().array(0).hash("Mean").setDouble(2.0);
        QTest::newRow("Array resize") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b_meta", "c"), meta, 1000.0, 3000.0),
                SequenceValue(SequenceName("a", "b", "c"), v1, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), v2, 2000.0, 3000.0)})
                                      << (SequenceValue::Transfer{
                                              SequenceValue(SequenceName("a", "b", "c"), v1, 1000.0,
                                                            2000.0), SequenceValue(
                                                      SequenceName("a", "b", "c",
                                                                   SequenceName::Flavors{"stats"}),
                                                      v3, 1000.0, 2000.0),
                                              SequenceValue(SequenceName("a", "b", "c"), v2, 2000.0,
                                                            3000.0), SequenceValue(
                                                      SequenceName("a", "b", "c",
                                                                   SequenceName::Flavors{"stats"}),
                                                      v4, 2000.0, 3000.0),
                                              SequenceValue(SequenceName("a", "b_meta", "c"), meta,
                                                            1000.0, 3000.0)});

        meta.write().setEmpty();
        meta.write().setType(Variant::Type::MetadataMatrix);
        meta.write().metadata("Smoothing").hash("Mode").setString("General");

        v1.write().setEmpty();
        v2.write().setEmpty();
        v3.write().setEmpty();
        v4.write().setEmpty();
        v1.write().matrix({2, 3}).setDouble(1.0);
        v1.write().matrix({0, 1}).setDouble(3.0);
        v2.write().matrix({2, 3}).setDouble(2.0);
        v2.write().matrix({0, 1}).setDouble(4.0);
        v3.write().matrix({2, 3}).setDouble(1.5);
        v3.write().matrix({0, 1}).setDouble(3.5);
        for (auto mod : v3.write().toMatrix()) {
            if (mod.second.exists())
                continue;
            mod.second.setReal(FP::undefined());
        }
        v4.write().matrix({0, 0}).setType(Variant::Type::Hash);
        v4.write().matrix({1, 0}).setType(Variant::Type::Hash);
        v4.write().matrix({2, 0}).setType(Variant::Type::Hash);
        v4.write().matrix({0, 1}).hash("Mean").setDouble(3.5);
        v4.write().matrix({1, 1}).setType(Variant::Type::Hash);
        v4.write().matrix({2, 1}).setType(Variant::Type::Hash);
        v4.write().matrix({0, 2}).setType(Variant::Type::Hash);
        v4.write().matrix({1, 2}).setType(Variant::Type::Hash);
        v4.write().matrix({2, 2}).setType(Variant::Type::Hash);
        v4.write().matrix({0, 3}).setType(Variant::Type::Hash);
        v4.write().matrix({1, 3}).setType(Variant::Type::Hash);
        v4.write().matrix({2, 3}).hash("Mean").setDouble(1.5);
        QTest::newRow("Matrix basic") << Engine_Basic << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b_meta", "c"), meta, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), v1, 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b", "c"), v2, 1500.0, 2000.0)})
                                      << (SequenceValue::Transfer{
                                              SequenceValue(SequenceName("a", "b", "c"), v3, 1000.0,
                                                            2000.0), SequenceValue(
                                                      SequenceName("a", "b", "c",
                                                                   SequenceName::Flavors{"stats"}),
                                                      v4, 1000.0, 2000.0),
                                              SequenceValue(SequenceName("a", "b_meta", "c"), meta,
                                                            1000.0, 2000.0)});


        QTest::newRow("Fixed time single") << Engine_FixedTime << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 2000.0)})
                                           << (SequenceValue::Transfer{
                                                   SequenceValue(SequenceName("a", "b", "c"),
                                                                 Variant::Root(1.0), 1000.0,
                                                                 2000.0), SequenceValue(
                                                           SequenceName("a", "b", "c",
                                                                        SequenceName::Flavors{
                                                                                "stats"}),
                                                           chd("Mean", 1.0), 1000.0, 2000.0)});
        QTest::newRow("Fixed time transition to bypass") << Engine_FixedTime
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("a", "b_meta", "c"),
                                                                 cms("Smoothing", "Mode",
                                                                     "General"), 1000.0, 2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "a",
                                                                                                     "b",
                                                                                                     "c"),
                                                                                             Variant::Root(
                                                                                                     1.0),
                                                                                             1000.0,
                                                                                             2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "a",
                                                                                                     "b",
                                                                                                     "c"),
                                                                                             Variant::Root(
                                                                                                     2.0),
                                                                                             1500.0,
                                                                                             2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "a",
                                                                                                     "b",
                                                                                                     "c"),
                                                                                             Variant::Root(
                                                                                                     3.0),
                                                                                             2000.0,
                                                                                             2500.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "a",
                                                                                                     "b_meta",
                                                                                                     "c"),
                                                                                             cms("Smoothing",
                                                                                                 "Mode",
                                                                                                 "Bypass"),
                                                                                             2000.0,
                                                                                             3000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "a",
                                                                                                     "b",
                                                                                                     "c"),
                                                                                             Variant::Root(
                                                                                                     4.0),
                                                                                             2500.0,
                                                                                             3000.0)})
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("a", "b", "c"),
                                                                 Variant::Root(1.5), 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c",
                                                                              SequenceName::Flavors{
                                                                                      "stats"}),
                                                                 chd("Mean", 1.5), 1000.0, 2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "a",
                                                                                                     "b_meta",
                                                                                                     "c"),
                                                                                             cms("Smoothing",
                                                                                                 "Mode",
                                                                                                 "General"),
                                                                                             1000.0,
                                                                                             2000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "a",
                                                                                                     "b",
                                                                                                     "c"),
                                                                                             Variant::Root(
                                                                                                     3.0),
                                                                                             2000.0,
                                                                                             2500.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "a",
                                                                                                     "b_meta",
                                                                                                     "c"),
                                                                                             cms("Smoothing",
                                                                                                 "Mode",
                                                                                                 "Bypass"),
                                                                                             2000.0,
                                                                                             3000.0),
                                                                                     SequenceValue(
                                                                                             SequenceName(
                                                                                                     "a",
                                                                                                     "b",
                                                                                                     "c"),
                                                                                             Variant::Root(
                                                                                                     4.0),
                                                                                             2500.0,
                                                                                             3000.0)});
        meta.write().setEmpty();
        meta.write().setType(Variant::Type::MetadataReal);
        meta.write().metadata("Test").hash("Value").setString("Result");
        meta.write().metadata("Smoothing").hash("Mode").setString("Difference");
        QTest::newRow("Fixed time meta") << Engine_FixedTime << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b_meta", "c"), meta, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(2.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "b", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(3.0), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(4.0), 1500.0, 2000.0)})
                                         << (SequenceValue::Transfer{
                                                 SequenceValue(SequenceName("a", "b", "c"),
                                                               Variant::Root(1.0), 1000.0, 2000.0),
                                                 SequenceValue(SequenceName("a", "b", "c",
                                                                            SequenceName::Flavors{
                                                                                    "end"}),
                                                               Variant::Root(3.0), 1000.0, 2000.0),
                                                 SequenceValue(SequenceName("a", "b_meta", "c"),
                                                               cms("Test", "Value", "Result"),
                                                               1000.0, 2000.0)});
        QTest::newRow("Fixed time flags handling") << Engine_FixedTime << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "F1"), Variant::Root(Variant::Flags()),
                              10000.0, 11000.0), SequenceValue(SequenceName("a", "b", "F1"),
                                                               Variant::Root(Variant::Flags{
                                                                       "Contaminated"}), 11000.0,
                                                               12000.0),
                SequenceValue(SequenceName("a", "b", "F1"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 12000.0, 13000.0),
                SequenceValue(SequenceName("a", "b", "F1"), Variant::Root(Variant::Flags()),
                              13000.0, 14000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "F1"), Variant::Root(Variant::Flags()),
                              10000.0, 11000.0), SequenceValue(SequenceName("a", "b", "F1"),
                                                               Variant::Root(Variant::Flags{
                                                                       "Contaminated"}), 11000.0,
                                                               12000.0),
                SequenceValue(SequenceName("a", "b", "F1"),
                              Variant::Root(Variant::Flags{"Contaminated"}), 12000.0, 13000.0),
                SequenceValue(SequenceName("a", "b", "F1"), Variant::Root(Variant::Flags()),
                              13000.0, 14000.0)});
        QTest::newRow("Fixed time degenerate threading") << Engine_FixedTime
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("a", "b", "F1"),
                                                                 Variant::Root(Variant::Flags()),
                                                                 10000.0, 11000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c"),
                                                                 Variant::Root(1.0), 10000.0,
                                                                 10500.0), SequenceValue(
                                                                 SequenceName("a", "b", "c"),
                                                                 Variant::Root(2.0), 10500.0,
                                                                 11000.0), SequenceValue(
                                                                 SequenceName("a", "b", "F1"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "Contaminated"}), 11000.0,
                                                                 12000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c"),
                                                                 Variant::Root(3.0), 11000.0,
                                                                 12000.0), SequenceValue(
                                                                 SequenceName("a", "b", "F1"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "Contaminated"}), 12000.0,
                                                                 13000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c"),
                                                                 Variant::Root(4.0), 12000.0,
                                                                 13000.0), SequenceValue(
                                                                 SequenceName("a", "b", "F1"),
                                                                 Variant::Root(Variant::Flags()),
                                                                 13000.0, 14000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c"),
                                                                 Variant::Root(5.0), 13000.0,
                                                                 13500.0), SequenceValue(
                                                                 SequenceName("a", "b", "c"),
                                                                 Variant::Root(6.0), 13500.0,
                                                                 14000.0)})
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("a", "b", "F1"),
                                                                 Variant::Root(Variant::Flags()),
                                                                 10000.0, 11000.0), SequenceValue(
                                                                 SequenceName("a", "b", "F1"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "Contaminated"}), 11000.0,
                                                                 12000.0), SequenceValue(
                                                                 SequenceName("a", "b", "F1"),
                                                                 Variant::Root(Variant::Flags{
                                                                         "Contaminated"}), 12000.0,
                                                                 13000.0), SequenceValue(
                                                                 SequenceName("a", "b", "F1"),
                                                                 Variant::Root(Variant::Flags()),
                                                                 13000.0, 14000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c"),
                                                                 Variant::Root(1.5), 10000.0,
                                                                 11000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c",
                                                                              SequenceName::Flavors{
                                                                                      "stats"}),
                                                                 chd("Mean", 1.5), 10000.0,
                                                                 11000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c"),
                                                                 Variant::Root(3.0), 11000.0,
                                                                 12000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c",
                                                                              SequenceName::Flavors{
                                                                                      "stats"}),
                                                                 chd("Mean", 3.0), 11000.0,
                                                                 12000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c"),
                                                                 Variant::Root(4.0), 12000.0,
                                                                 13000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c",
                                                                              SequenceName::Flavors{
                                                                                      "stats"}),
                                                                 chd("Mean", 4.0), 12000.0,
                                                                 13000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c"),
                                                                 Variant::Root(5.5), 13000.0,
                                                                 14000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c",
                                                                              SequenceName::Flavors{
                                                                                      "stats"}),
                                                                 chd("Mean", 5.5), 13000.0,
                                                                 14000.0)});
        meta.write().setEmpty();
        meta.write().setType(Variant::Type::MetadataReal);
        meta.write().metadata("Smoothing").hash("Mode").setString("RHExtrapolate");
        meta.write().metadata("Smoothing").hash("Parameters").hash("Tin").setString("d");
        meta.write().metadata("Smoothing").hash("Parameters").hash("Tout").setString("e");
        meta.write().metadata("Smoothing").hash("Parameters").hash("RHin").setString("f");
        QTest::newRow("Fixed RH Extrapolate") << Engine_FixedTime << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(15.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b_meta", "c"), meta, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "d"), Variant::Root(20.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "e"), Variant::Root(25.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "b", "f"), Variant::Root(40.0), 1000.0, 2000.0)})
                                              << (SequenceValue::Transfer{
                                                      SequenceValue(SequenceName("a", "b", "c"),
                                                                    Variant::Root(
                                                                            Algorithms::Dewpoint::rhExtrapolate(
                                                                                    20.0, 40.0,
                                                                                    25.0)), 1000.0,
                                                                    2000.0), SequenceValue(
                                                              SequenceName("a", "b", "c",
                                                                           SequenceName::Flavors{
                                                                                   "stats"}),
                                                              chd("Mean", 15.0), 1000.0, 2000.0),
                                                      SequenceValue(
                                                              SequenceName("a", "b_meta", "c"),
                                                              cms("Smoothing", "Mode",
                                                                  "RHExtrapolate"), 1000.0, 2000.0),
                                                      SequenceValue(SequenceName("a", "b", "d"),
                                                                    Variant::Root(20.0), 1000.0,
                                                                    2000.0), SequenceValue(
                                                              SequenceName("a", "b", "d",
                                                                           SequenceName::Flavors{
                                                                                   "stats"}),
                                                              chd("Mean", 20.0), 1000.0, 2000.0),
                                                      SequenceValue(SequenceName("a", "b", "e"),
                                                                    Variant::Root(25.0), 1000.0,
                                                                    2000.0), SequenceValue(
                                                              SequenceName("a", "b", "e",
                                                                           SequenceName::Flavors{
                                                                                   "stats"}),
                                                              chd("Mean", 25.0), 1000.0, 2000.0),
                                                      SequenceValue(SequenceName("a", "b", "f"),
                                                                    Variant::Root(40.0), 1000.0,
                                                                    2000.0), SequenceValue(
                                                              SequenceName("a", "b", "f",
                                                                           SequenceName::Flavors{
                                                                                   "stats"}),
                                                              chd("Mean", 40.0), 1000.0, 2000.0)});

        {
            SequenceValue::Transfer bulk;
            for (int i = 1000; i < 5000; i++) {
                bulk.emplace_back(SequenceName("a", "b", "c"), Variant::Root((double) i),
                                  (double) i, (double) (i + 1));
            }
            QTest::newRow("Fixed time bulk") << Engine_FixedTime << bulk
                                             << (SequenceValue::Transfer{
                                                     SequenceValue(SequenceName("a", "b", "c"),
                                                                   Variant::Root(1499.5), 1000.0,
                                                                   2000.0), SequenceValue(
                                                             SequenceName("a", "b", "c",
                                                                          SequenceName::Flavors{
                                                                                  "stats"}),
                                                             chd("Mean", 1499.5), 1000.0, 2000.0),
                                                     SequenceValue(SequenceName("a", "b", "c"),
                                                                   Variant::Root(2499.5), 2000.0,
                                                                   3000.0), SequenceValue(
                                                             SequenceName("a", "b", "c",
                                                                          SequenceName::Flavors{
                                                                                  "stats"}),
                                                             chd("Mean", 2499.5), 2000.0, 3000.0),
                                                     SequenceValue(SequenceName("a", "b", "c"),
                                                                   Variant::Root(3499.5), 3000.0,
                                                                   4000.0), SequenceValue(
                                                             SequenceName("a", "b", "c",
                                                                          SequenceName::Flavors{
                                                                                  "stats"}),
                                                             chd("Mean", 3499.5), 3000.0, 4000.0),
                                                     SequenceValue(SequenceName("a", "b", "c"),
                                                                   Variant::Root(4499.5), 4000.0,
                                                                   5000.0), SequenceValue(
                                                             SequenceName("a", "b", "c",
                                                                          SequenceName::Flavors{
                                                                                  "stats"}),
                                                             chd("Mean", 4499.5), 4000.0, 5000.0)});
        }


        QTest::newRow("Realtime single") << Engine_Realtime << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "asd", "c"), Variant::Root(1.0), 1000.0, 2000.0)})
                                         << (SequenceValue::Transfer{
                                                 SequenceValue(SequenceName("a", "raw", "c"),
                                                               Variant::Root(1.0), 1000.0, 2000.0),
                                                 SequenceValue(SequenceName("a", "raw", "c",
                                                                            SequenceName::Flavors{
                                                                                    "stats"}),
                                                               chd("Mean", 1.0), 1000.0, 2000.0),
                                                 SequenceValue(SequenceName("a", "rt_boxcar", "c"),
                                                               Variant::Root(1.0), 1000.0, 2000.0),
                                                 SequenceValue(SequenceName("a", "rt_boxcar", "c",
                                                                            SequenceName::Flavors{
                                                                                    "stats"}),
                                                               chd("Mean", 1.0), 1000.0, 2000.0),
                                                 SequenceValue(SequenceName("a", "rt_instant", "c"),
                                                               Variant::Root(1.0), 1000.0,
                                                               2000.0)});
        QTest::newRow("Realtime meta") << Engine_Realtime << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "asd_meta", "c"),
                              cms("Smoothing", "Mode", "Difference"), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "asd", "c"), Variant::Root(1.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "asd", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(2.0), 1000.0, 2000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "raw", "c"), Variant::Root(1.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "raw", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(FP::undefined()), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "rt_boxcar", "c"), Variant::Root(1.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "rt_boxcar", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(FP::undefined()), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "rt_instant", "c"), Variant::Root(1.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "rt_instant", "c", SequenceName::Flavors{"end"}),
                              Variant::Root(2.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "raw_meta", "c"),
                              cms("Smoothing", "Mode", "Difference"), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "rt_boxcar_meta", "c"),
                              cms("Smoothing", "Mode", "Difference"), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "rt_instant_meta", "c"),
                              cms("Smoothing", "Mode", "Difference"), 1000.0, 2000.0)});


        QTest::newRow("Segment controlled single") << Engine_SegmentControlled
                                                   << (SequenceValue::Transfer{SequenceValue(
                                                           SequenceName("a", "b", "seg"),
                                                           chd("Segment", 1.0), 1000.0, 2000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "c"),
                                                                                       Variant::Root(
                                                                                               1.0),
                                                                                       1000.0,
                                                                                       2000.0)})
                                                   << (SequenceValue::Transfer{SequenceValue(
                                                           SequenceName("a", "b", "seg"),
                                                           chd("Segment", 1.0), 1000.0, 2000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "out"),
                                                                                       chd("Segment",
                                                                                           1.0),
                                                                                       1000.0,
                                                                                       2000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "c"),
                                                                                       Variant::Root(
                                                                                               1.0),
                                                                                       1000.0,
                                                                                       2000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "c",
                                                                                               SequenceName::Flavors{
                                                                                                       "stats"}),
                                                                                       chd("Mean",
                                                                                           1.0),
                                                                                       1000.0,
                                                                                       2000.0)});
        QTest::newRow("Segment controlled single reversed") << Engine_SegmentControlled
                                                            << (SequenceValue::Transfer{
                                                                    SequenceValue(
                                                                            SequenceName("a", "b",
                                                                                         "c"),
                                                                            Variant::Root(1.0),
                                                                            1000.0, 2000.0),
                                                                    SequenceValue(
                                                                            SequenceName("a", "b",
                                                                                         "seg"),
                                                                            chd("Segment", 1.0),
                                                                            1000.0, 2000.0)})
                                                            << (SequenceValue::Transfer{
                                                                    SequenceValue(
                                                                            SequenceName("a", "b",
                                                                                         "seg"),
                                                                            chd("Segment", 1.0),
                                                                            1000.0, 2000.0),
                                                                    SequenceValue(
                                                                            SequenceName("a", "b",
                                                                                         "out"),
                                                                            chd("Segment", 1.0),
                                                                            1000.0, 2000.0),
                                                                    SequenceValue(
                                                                            SequenceName("a", "b",
                                                                                         "c"),
                                                                            Variant::Root(1.0),
                                                                            1000.0, 2000.0),
                                                                    SequenceValue(
                                                                            SequenceName("a", "b",
                                                                                         "c",
                                                                                         SequenceName::Flavors{
                                                                                                 "stats"}),
                                                                            chd("Mean", 1.0),
                                                                            1000.0, 2000.0)});
        QTest::newRow("Segment controlled single cover") << Engine_SegmentControlled
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("a", "b", "seg"),
                                                                 chd("Segment", 1.0), 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c"),
                                                                 Variant::Root(1.0), 1000.0,
                                                                 1800.0)})
                                                         << (SequenceValue::Transfer{SequenceValue(
                                                                 SequenceName("a", "b", "seg"),
                                                                 chd("Segment", 1.0), 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("a", "b", "out"),
                                                                 chd("Segment", 1.0), 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c"),
                                                                 Variant::Root(1.0), 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c",
                                                                              SequenceName::Flavors{
                                                                                      "cover"}),
                                                                 Variant::Root(0.8), 1000.0,
                                                                 2000.0), SequenceValue(
                                                                 SequenceName("a", "b", "c",
                                                                              SequenceName::Flavors{
                                                                                      "stats"}),
                                                                 chd("Mean", 1.0), 1000.0,
                                                                 2000.0)});
        QTest::newRow("Segment controlled two") << Engine_SegmentControlled
                                                << (SequenceValue::Transfer{
                                                        SequenceValue(SequenceName("a", "b", "seg"),
                                                                      chd("Segment", 1.0), 1000.0,
                                                                      2000.0),
                                                        SequenceValue(SequenceName("a", "b", "c"),
                                                                      Variant::Root(1.0), 1000.0,
                                                                      1500.0),
                                                        SequenceValue(SequenceName("a", "b", "c"),
                                                                      Variant::Root(2.0), 1500.0,
                                                                      2000.0),
                                                        SequenceValue(SequenceName("a", "b", "seg"),
                                                                      chd("Segment", 2.0), 2000.0,
                                                                      3000.0),
                                                        SequenceValue(SequenceName("a", "b", "c"),
                                                                      Variant::Root(3.0), 2000.0,
                                                                      2900.0)})
                                                << (SequenceValue::Transfer{
                                                        SequenceValue(SequenceName("a", "b", "seg"),
                                                                      chd("Segment", 1.0), 1000.0,
                                                                      2000.0),
                                                        SequenceValue(SequenceName("a", "b", "out"),
                                                                      chd("Segment", 1.0), 1000.0,
                                                                      2000.0),
                                                        SequenceValue(SequenceName("a", "b", "c"),
                                                                      Variant::Root(1.5), 1000.0,
                                                                      2000.0), SequenceValue(
                                                                SequenceName("a", "b", "c",
                                                                             SequenceName::Flavors{
                                                                                     "stats"}),
                                                                chd("Mean", 1.5), 1000.0, 2000.0),
                                                        SequenceValue(SequenceName("a", "b", "seg"),
                                                                      chd("Segment", 2.0), 2000.0,
                                                                      3000.0),
                                                        SequenceValue(SequenceName("a", "b", "out"),
                                                                      chd("Segment", 2.0), 2000.0,
                                                                      3000.0),
                                                        SequenceValue(SequenceName("a", "b", "c"),
                                                                      Variant::Root(3.0), 2000.0,
                                                                      3000.0), SequenceValue(
                                                                SequenceName("a", "b", "c",
                                                                             SequenceName::Flavors{
                                                                                     "cover"}),
                                                                Variant::Root(0.9), 2000.0, 3000.0),
                                                        SequenceValue(SequenceName("a", "b", "c",
                                                                                   SequenceName::Flavors{
                                                                                           "stats"}),
                                                                      chd("Mean", 3.0), 2000.0,
                                                                      3000.0)});
        QTest::newRow("Segment controlled three") << Engine_SegmentControlled
                                                  << (SequenceValue::Transfer{
                                                          SequenceValue(SequenceName("a", "b", "c"),
                                                                        Variant::Root(1.0), 1000.0,
                                                                        1500.0), SequenceValue(
                                                                  SequenceName("a", "b", "seg"),
                                                                  chd("Segment", 1.0), 1000.0,
                                                                  2000.0),
                                                          SequenceValue(SequenceName("a", "b", "c"),
                                                                        Variant::Root(2.0), 1500.0,
                                                                        2000.0), SequenceValue(
                                                                  SequenceName("a", "b", "seg"),
                                                                  chd("Segment", 2.0), 2000.0,
                                                                  3000.0),
                                                          SequenceValue(SequenceName("a", "b", "c"),
                                                                        Variant::Root(3.0), 2000.0,
                                                                        2900.0), SequenceValue(
                                                                  SequenceName("a", "b", "seg"),
                                                                  chd("Segment", 3.0), 3000.0,
                                                                  4000.0),
                                                          SequenceValue(SequenceName("a", "b", "c"),
                                                                        Variant::Root(4.0), 3000.0,
                                                                        4000.0)})
                                                  << (SequenceValue::Transfer{SequenceValue(
                                                          SequenceName("a", "b", "seg"),
                                                          chd("Segment", 1.0), 1000.0, 2000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "out"),
                                                                                      chd("Segment",
                                                                                          1.0),
                                                                                      1000.0,
                                                                                      2000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "c"),
                                                                                      Variant::Root(
                                                                                              1.5),
                                                                                      1000.0,
                                                                                      2000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "c",
                                                                                              SequenceName::Flavors{
                                                                                                      "stats"}),
                                                                                      chd("Mean",
                                                                                          1.5),
                                                                                      1000.0,
                                                                                      2000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "seg"),
                                                                                      chd("Segment",
                                                                                          2.0),
                                                                                      2000.0,
                                                                                      3000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "out"),
                                                                                      chd("Segment",
                                                                                          2.0),
                                                                                      2000.0,
                                                                                      3000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "c"),
                                                                                      Variant::Root(
                                                                                              3.0),
                                                                                      2000.0,
                                                                                      3000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "c",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                      Variant::Root(
                                                                                              0.9),
                                                                                      2000.0,
                                                                                      3000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "c",
                                                                                              SequenceName::Flavors{
                                                                                                      "stats"}),
                                                                                      chd("Mean",
                                                                                          3.0),
                                                                                      2000.0,
                                                                                      3000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "seg"),
                                                                                      chd("Segment",
                                                                                          3.0),
                                                                                      3000.0,
                                                                                      4000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "out"),
                                                                                      chd("Segment",
                                                                                          3.0),
                                                                                      3000.0,
                                                                                      4000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "c"),
                                                                                      Variant::Root(
                                                                                              4.0),
                                                                                      3000.0,
                                                                                      4000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "c",
                                                                                              SequenceName::Flavors{
                                                                                                      "stats"}),
                                                                                      chd("Mean",
                                                                                          4.0),
                                                                                      3000.0,
                                                                                      4000.0)});
        QTest::newRow("Segment controlled start empty") << Engine_SegmentControlled
                                                        << (SequenceValue::Transfer{SequenceValue(
                                                                SequenceName("a", "b", "seg"),
                                                                chd("Segment", 1.0), 1000.0,
                                                                2000.0), SequenceValue(
                                                                SequenceName("a", "b", "seg"),
                                                                chd("Segment", 2.0), 2000.0,
                                                                3000.0), SequenceValue(
                                                                SequenceName("a", "b", "seg"),
                                                                chd("Segment", 3.0), 3000.0,
                                                                4000.0), SequenceValue(
                                                                SequenceName("a", "b", "c"),
                                                                Variant::Root(1.0), 3000.0,
                                                                4000.0)})
                                                        << (SequenceValue::Transfer{SequenceValue(
                                                                SequenceName("a", "b", "seg"),
                                                                chd("Segment", 1.0), 1000.0,
                                                                2000.0), SequenceValue(
                                                                SequenceName("a", "b", "out"),
                                                                chd("Segment", 1.0), 1000.0,
                                                                2000.0), SequenceValue(
                                                                SequenceName("a", "b", "seg"),
                                                                chd("Segment", 2.0), 2000.0,
                                                                3000.0), SequenceValue(
                                                                SequenceName("a", "b", "out"),
                                                                chd("Segment", 2.0), 2000.0,
                                                                3000.0), SequenceValue(
                                                                SequenceName("a", "b", "seg"),
                                                                chd("Segment", 3.0), 3000.0,
                                                                4000.0), SequenceValue(
                                                                SequenceName("a", "b", "out"),
                                                                chd("Segment", 3.0), 3000.0,
                                                                4000.0), SequenceValue(
                                                                SequenceName("a", "b", "c"),
                                                                Variant::Root(1.0), 3000.0, 4000.0),
                                                                                    SequenceValue(
                                                                                            SequenceName(
                                                                                                    "a",
                                                                                                    "b",
                                                                                                    "c",
                                                                                                    SequenceName::Flavors{
                                                                                                            "stats"}),
                                                                                            chd("Mean",
                                                                                                1.0),
                                                                                            3000.0,
                                                                                            4000.0)});
        QTest::newRow("Segment controlled end empty") << Engine_SegmentControlled
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("a", "b", "c"),
                                                              Variant::Root(1.0), 1000.0, 2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "a",
                                                                                                  "b",
                                                                                                  "seg"),
                                                                                          chd("Segment",
                                                                                              1.0),
                                                                                          1000.0,
                                                                                          2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "a",
                                                                                                  "b",
                                                                                                  "seg"),
                                                                                          chd("Segment",
                                                                                              2.0),
                                                                                          2000.0,
                                                                                          3000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "a",
                                                                                                  "b",
                                                                                                  "seg"),
                                                                                          chd("Segment",
                                                                                              3.0),
                                                                                          3000.0,
                                                                                          4000.0)})
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("a", "b", "seg"),
                                                              chd("Segment", 1.0), 1000.0, 2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "a",
                                                                                                  "b",
                                                                                                  "out"),
                                                                                          chd("Segment",
                                                                                              1.0),
                                                                                          1000.0,
                                                                                          2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "a",
                                                                                                  "b",
                                                                                                  "seg"),
                                                                                          chd("Segment",
                                                                                              2.0),
                                                                                          2000.0,
                                                                                          3000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "a",
                                                                                                  "b",
                                                                                                  "out"),
                                                                                          chd("Segment",
                                                                                              2.0),
                                                                                          2000.0,
                                                                                          3000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "a",
                                                                                                  "b",
                                                                                                  "seg"),
                                                                                          chd("Segment",
                                                                                              3.0),
                                                                                          3000.0,
                                                                                          4000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "a",
                                                                                                  "b",
                                                                                                  "out"),
                                                                                          chd("Segment",
                                                                                              3.0),
                                                                                          3000.0,
                                                                                          4000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "a",
                                                                                                  "b",
                                                                                                  "c"),
                                                                                          Variant::Root(
                                                                                                  1.0),
                                                                                          1000.0,
                                                                                          2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "a",
                                                                                                  "b",
                                                                                                  "c",
                                                                                                  SequenceName::Flavors{
                                                                                                          "stats"}),
                                                                                          chd("Mean",
                                                                                              1.0),
                                                                                          1000.0,
                                                                                          2000.0)});
        QTest::newRow("Segment controlled middle") << Engine_SegmentControlled
                                                   << (SequenceValue::Transfer{SequenceValue(
                                                           SequenceName("a", "b", "seg"),
                                                           chd("Segment", 1.0), 1000.0, 2000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "seg"),
                                                                                       chd("Segment",
                                                                                           2.0),
                                                                                       2000.0,
                                                                                       3000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "c"),
                                                                                       Variant::Root(
                                                                                               1.0),
                                                                                       2000.0,
                                                                                       3000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "seg"),
                                                                                       chd("Segment",
                                                                                           3.0),
                                                                                       3000.0,
                                                                                       4000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "seg"),
                                                                                       chd("Segment",
                                                                                           4.0),
                                                                                       4000.0,
                                                                                       5000.0)})
                                                   << (SequenceValue::Transfer{SequenceValue(
                                                           SequenceName("a", "b", "seg"),
                                                           chd("Segment", 1.0), 1000.0, 2000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "out"),
                                                                                       chd("Segment",
                                                                                           1.0),
                                                                                       1000.0,
                                                                                       2000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "seg"),
                                                                                       chd("Segment",
                                                                                           2.0),
                                                                                       2000.0,
                                                                                       3000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "out"),
                                                                                       chd("Segment",
                                                                                           2.0),
                                                                                       2000.0,
                                                                                       3000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "seg"),
                                                                                       chd("Segment",
                                                                                           3.0),
                                                                                       3000.0,
                                                                                       4000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "out"),
                                                                                       chd("Segment",
                                                                                           3.0),
                                                                                       3000.0,
                                                                                       4000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "seg"),
                                                                                       chd("Segment",
                                                                                           4.0),
                                                                                       4000.0,
                                                                                       5000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "out"),
                                                                                       chd("Segment",
                                                                                           4.0),
                                                                                       4000.0,
                                                                                       5000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "c"),
                                                                                       Variant::Root(
                                                                                               1.0),
                                                                                       2000.0,
                                                                                       3000.0),
                                                                               SequenceValue(
                                                                                       SequenceName(
                                                                                               "a",
                                                                                               "b",
                                                                                               "c",
                                                                                               SequenceName::Flavors{
                                                                                                       "stats"}),
                                                                                       chd("Mean",
                                                                                           1.0),
                                                                                       2000.0,
                                                                                       3000.0)});
        QTest::newRow("Segment controlled overlap") << Engine_SegmentControlled
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("a", "b", "seg"),
                                                            chd("Segment", 1.0), 1000.0, 2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "b",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                1.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "b",
                                                                                                "seg"),
                                                                                        chd("Segment",
                                                                                            2.0),
                                                                                        1500.0,
                                                                                        2000.0)})
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("a", "b", "seg"),
                                                            chd("Segment", 1.0), 1000.0, 1500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "b",
                                                                                                "out"),
                                                                                        chd("Segment",
                                                                                            1.0),
                                                                                        1000.0,
                                                                                        1500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "b",
                                                                                                "seg"),
                                                                                        chd("Segment",
                                                                                            2.0),
                                                                                        1500.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "b",
                                                                                                "out"),
                                                                                        chd("Segment",
                                                                                            2.0),
                                                                                        1500.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "b",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                1.0),
                                                                                        1000.0,
                                                                                        1500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "b",
                                                                                                "c",
                                                                                                SequenceName::Flavors{
                                                                                                        "stats"}),
                                                                                        chd("Mean",
                                                                                            1.0),
                                                                                        1000.0,
                                                                                        1500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "b",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                1.0),
                                                                                        1500.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "b",
                                                                                                "c",
                                                                                                SequenceName::Flavors{
                                                                                                        "stats"}),
                                                                                        chd("Mean",
                                                                                            1.0),
                                                                                        1500.0,
                                                                                        2000.0)});
        QTest::newRow("Segment controlled flags") << Engine_SegmentControlled
                                                  << (SequenceValue::Transfer{SequenceValue(
                                                          SequenceName("a", "b", "seg"),
                                                          chd("Segment", 1.0), 1000.0, 2000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "c"),
                                                                                      Variant::Root(
                                                                                              Variant::Flags{
                                                                                                      "F1"}),
                                                                                      1000.0,
                                                                                      1500.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "c"),
                                                                                      Variant::Root(
                                                                                              Variant::Flags{
                                                                                                      "F2"}),
                                                                                      1500.0,
                                                                                      2000.0)})
                                                  << (SequenceValue::Transfer{SequenceValue(
                                                          SequenceName("a", "b", "seg"),
                                                          chd("Segment", 1.0), 1000.0, 2000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "out"),
                                                                                      chd("Segment",
                                                                                          1.0),
                                                                                      1000.0,
                                                                                      2000.0),
                                                                              SequenceValue(
                                                                                      SequenceName(
                                                                                              "a",
                                                                                              "b",
                                                                                              "c"),
                                                                                      Variant::Root(
                                                                                              Variant::Flags{
                                                                                                      "F1",
                                                                                                      "F2"}),
                                                                                      1000.0,
                                                                                      2000.0)});
        meta.write().setEmpty();
        meta.write().setType(Variant::Type::MetadataReal);
        meta.write().metadata("Test").hash("Value").setString("Result");
        meta.write().metadata("Smoothing").hash("Mode").setString("Difference");
        QTest::newRow("Segment controlled meta") << Engine_SegmentControlled
                                                 << (SequenceValue::Transfer{SequenceValue(
                                                         SequenceName("a", "b", "seg"),
                                                         chd("Segment", 1.0), 1000.0, 2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "b",
                                                                                             "c"),
                                                                                     Variant::Root(
                                                                                             1.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "b",
                                                                                             "c",
                                                                                             SequenceName::Flavors{
                                                                                                     "end"}),
                                                                                     Variant::Root(
                                                                                             2.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "b_meta",
                                                                                             "c"),
                                                                                     meta, 1000.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "b",
                                                                                             "c"),
                                                                                     Variant::Root(
                                                                                             3.0),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "b",
                                                                                             "c",
                                                                                             SequenceName::Flavors{
                                                                                                     "end"}),
                                                                                     Variant::Root(
                                                                                             4.0),
                                                                                     1500.0,
                                                                                     2000.0)})
                                                 << (SequenceValue::Transfer{SequenceValue(
                                                         SequenceName("a", "b", "seg"),
                                                         chd("Segment", 1.0), 1000.0, 2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "b",
                                                                                             "out"),
                                                                                     chd("Segment",
                                                                                         1.0),
                                                                                     1000.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "b",
                                                                                             "c"),
                                                                                     Variant::Root(
                                                                                             1.0),
                                                                                     1000.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "b",
                                                                                             "c",
                                                                                             SequenceName::Flavors{
                                                                                                     "end"}),
                                                                                     Variant::Root(
                                                                                             4.0),
                                                                                     1000.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "b_meta",
                                                                                             "c"),
                                                                                     cms("Test",
                                                                                         "Value",
                                                                                         "Result"),
                                                                                     1000.0,
                                                                                     2000.0)});


        QTest::newRow("Editing full calculate single") << Engine_Editing_FullCalculate
                                                       << (SequenceValue::Transfer{SequenceValue(
                                                               SequenceName("a", "raw", "c"),
                                                               Variant::Root(1.0), 1000.0, 2000.0),
                                                                                   SequenceValue(
                                                                                           SequenceName(
                                                                                                   "a",
                                                                                                   "cont",
                                                                                                   "c"),
                                                                                           Variant::Root(
                                                                                                   -1.0),
                                                                                           1000.0,
                                                                                           2000.0),
                                                                                   SequenceValue(
                                                                                           SequenceName(
                                                                                                   "a",
                                                                                                   "avg",
                                                                                                   "c"),
                                                                                           Variant::Root(
                                                                                                   2.0),
                                                                                           1000.0,
                                                                                           2000.0),
                                                                                   SequenceValue(
                                                                                           SequenceName(
                                                                                                   "a",
                                                                                                   "raw",
                                                                                                   "d"),
                                                                                           Variant::Root(
                                                                                                   3.0),
                                                                                           1000.0,
                                                                                           2000.0),
                                                                                   SequenceValue(
                                                                                           SequenceName(
                                                                                                   "a",
                                                                                                   "avg",
                                                                                                   "d"),
                                                                                           Variant::Root(
                                                                                                   4.0),
                                                                                           1000.0,
                                                                                           2000.0),
                                                                                   SequenceValue(
                                                                                           SequenceName(
                                                                                                   "a",
                                                                                                   "cont",
                                                                                                   "d"),
                                                                                           Variant::Root(
                                                                                                   5.0),
                                                                                           1000.0,
                                                                                           2000.0)})
                                                       << (SequenceValue::Transfer{SequenceValue(
                                                               SequenceName("a", "raw", "d"),
                                                               Variant::Root(3.0), 1000.0, 2000.0),
                                                                                   SequenceValue(
                                                                                           SequenceName(
                                                                                                   "a",
                                                                                                   "avg",
                                                                                                   "d"),
                                                                                           Variant::Root(
                                                                                                   4.0),
                                                                                           1000.0,
                                                                                           2000.0),
                                                                                   SequenceValue(
                                                                                           SequenceName(
                                                                                                   "a",
                                                                                                   "cont",
                                                                                                   "d"),
                                                                                           Variant::Root(
                                                                                                   5.0),
                                                                                           1000.0,
                                                                                           2000.0),
                                                                                   SequenceValue(
                                                                                           SequenceName(
                                                                                                   "a",
                                                                                                   "raw",
                                                                                                   "c"),
                                                                                           Variant::Root(
                                                                                                   1.0),
                                                                                           1000.0,
                                                                                           2000.0),
                                                                                   SequenceValue(
                                                                                           SequenceName(
                                                                                                   "a",
                                                                                                   "cont",
                                                                                                   "c"),
                                                                                           Variant::Root(
                                                                                                   2.0),
                                                                                           1000.0,
                                                                                           2000.0),
                                                                                   SequenceValue(
                                                                                           SequenceName(
                                                                                                   "a",
                                                                                                   "avg",
                                                                                                   "c"),
                                                                                           Variant::Root(
                                                                                                   2.0),
                                                                                           1000.0,
                                                                                           2000.0)});
        QTest::newRow("Editing full calculate two") << Engine_Editing_FullCalculate
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("a", "raw", "c"),
                                                            Variant::Root(1.0), 1000.0, 1500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avg",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                2.0),
                                                                                        1000.0,
                                                                                        1500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avg",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                3.0),
                                                                                        1500.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "raw",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                4.0),
                                                                                        1500.0,
                                                                                        2000.0)})
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("a", "raw", "c"),
                                                            Variant::Root(1.0), 1000.0, 1500.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "raw",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                4.0),
                                                                                        1500.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "cont",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                2.5),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avg",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                2.5),
                                                                                        1000.0,
                                                                                        2000.0)});
        QTest::newRow("Editing full calculate two cover") << Engine_Editing_FullCalculate
                                                          << (SequenceValue::Transfer{SequenceValue(
                                                                  SequenceName("a", "raw", "c"),
                                                                  Variant::Root(1.0), 1000.0,
                                                                  1500.0), SequenceValue(
                                                                  SequenceName("a", "avg", "c"),
                                                                  Variant::Root(2.0), 1000.0,
                                                                  1500.0), SequenceValue(
                                                                  SequenceName("a", "avg", "c"),
                                                                  Variant::Root(3.0), 1500.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "raw", "c"),
                                                                  Variant::Root(4.0), 1500.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "avg", "c",
                                                                               SequenceName::Flavors{
                                                                                       "cover"}),
                                                                  Variant::Root(0.6), 1500.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "raw", "d"),
                                                                  Variant::Root(5.0), 1500.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "raw", "d",
                                                                               SequenceName::Flavors{
                                                                                       "cover"}),
                                                                  Variant::Root(0.9), 1500.0,
                                                                  2000.0)})
                                                          << (SequenceValue::Transfer{SequenceValue(
                                                                  SequenceName("a", "raw", "d"),
                                                                  Variant::Root(5.0), 1500.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "raw", "d",
                                                                               SequenceName::Flavors{
                                                                                       "cover"}),
                                                                  Variant::Root(0.9), 1500.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "raw", "c"),
                                                                  Variant::Root(1.0), 1000.0,
                                                                  1500.0), SequenceValue(
                                                                  SequenceName("a", "raw", "c"),
                                                                  Variant::Root(4.0), 1500.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "cont", "c"),
                                                                  Variant::Root(2.375), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "cont", "c",
                                                                               SequenceName::Flavors{
                                                                                       "cover"}),
                                                                  Variant::Root(0.8), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "avg", "c"),
                                                                  Variant::Root(2.375), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "avg", "c",
                                                                               SequenceName::Flavors{
                                                                                       "cover"}),
                                                                  Variant::Root(0.8), 1000.0,
                                                                  2000.0)});
        QTest::newRow("Editing full calculate gap") << Engine_Editing_FullCalculate
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("a", "raw", "c"),
                                                            Variant::Root(1.0), 1000.0, 1400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avg",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                2.0),
                                                                                        1000.0,
                                                                                        1400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avg",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                3.0),
                                                                                        1600.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "raw",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                4.0),
                                                                                        1600.0,
                                                                                        2000.0)})
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("a", "raw", "c"),
                                                            Variant::Root(1.0), 1000.0, 1400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "raw",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                4.0),
                                                                                        1600.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "cont",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                2.0),
                                                                                        1000.0,
                                                                                        1400.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "cont",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                3.0),
                                                                                        1600.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avg",
                                                                                                "c"),
                                                                                        Variant::Root(
                                                                                                2.5),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avg",
                                                                                                "c",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.8),
                                                                                        1000.0,
                                                                                        2000.0)});
        QTest::newRow("Editing full calculate flags") << Engine_Editing_FullCalculate
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("a", "raw", "c"),
                                                              Variant::Root(Variant::Flags{"F1"}),
                                                              1000.0, 1500.0), SequenceValue(
                                                              SequenceName("a", "avg", "c"),
                                                              Variant::Root(Variant::Flags{"F2"}),
                                                              1000.0, 1400.0), SequenceValue(
                                                              SequenceName("a", "avg", "c"),
                                                              Variant::Root(Variant::Flags{"F3"}),
                                                              1500.0, 2000.0), SequenceValue(
                                                              SequenceName("a", "raw", "c"),
                                                              Variant::Root(Variant::Flags{"F4"}),
                                                              1500.0, 2000.0), SequenceValue(
                                                              SequenceName("a", "avg", "d"),
                                                              Variant::Root(Variant::Flags{"F5"}),
                                                              1500.0, 2000.0)})
                                                      << (SequenceValue::Transfer{SequenceValue(
                                                              SequenceName("a", "avg", "d"),
                                                              Variant::Root(Variant::Flags{"F5"}),
                                                              1500.0, 2000.0), SequenceValue(
                                                              SequenceName("a", "raw", "c"),
                                                              Variant::Root(Variant::Flags{"F1"}),
                                                              1000.0, 1500.0), SequenceValue(
                                                              SequenceName("a", "raw", "c"),
                                                              Variant::Root(Variant::Flags{"F4"}),
                                                              1500.0, 2000.0), SequenceValue(
                                                              SequenceName("a", "avg", "c"),
                                                              Variant::Root(
                                                                      Variant::Flags{"F2", "F3"}),
                                                              1000.0, 2000.0), SequenceValue(
                                                              SequenceName("a", "avg", "c",
                                                                           SequenceName::Flavors{
                                                                                   "cover"}),
                                                              Variant::Root(0.9), 1000.0, 2000.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "a",
                                                                                                  "cont",
                                                                                                  "c"),
                                                                                          Variant::Root(
                                                                                                  Variant::Flags{
                                                                                                          "F2"}),
                                                                                          1000.0,
                                                                                          1400.0),
                                                                                  SequenceValue(
                                                                                          SequenceName(
                                                                                                  "a",
                                                                                                  "cont",
                                                                                                  "c"),
                                                                                          Variant::Root(
                                                                                                  Variant::Flags{
                                                                                                          "F3"}),
                                                                                          1500.0,
                                                                                          2000.0)});
        Variant::Root meta2;
        Variant::Root meta3;
        meta.write().setEmpty();
        meta.write().setType(Variant::Type::MetadataReal);
        meta.write().metadata("Smoothing").hash("Mode").setString("Difference");
        meta.write().metadata("Test").hash("Value").setString("Result1");
        meta2 = meta;
        meta2.write().metadata("Test").hash("Value").setString("Result2");
        meta3 = meta;
        meta3.write().metadata("Test").hash("Value").setString("Result3");
        Variant::Root metaBypass;
        Variant::Root metaBypass2;
        Variant::Root metaBypass3;
        metaBypass.write().setEmpty();
        metaBypass.write().setType(Variant::Type::MetadataReal);
        metaBypass.write().metadata("Smoothing").hash("Mode").setString("DifferenceInitial");
        metaBypass.write().metadata("Test").hash("Bypass").setString("Result1");
        metaBypass2 = metaBypass;
        metaBypass2.write().metadata("Test").hash("Bypass").setString("Result2");
        metaBypass3 = metaBypass;
        metaBypass3.write().metadata("Test").hash("Bypass").setString("Result3");
        QTest::newRow("Editing full calculate meta") << Engine_Editing_FullCalculate
                                                     << (SequenceValue::Transfer{SequenceValue(
                                                             SequenceName("a", "raw_meta", "c"),
                                                             meta, 1000.0, 2000.0), SequenceValue(
                                                             SequenceName("a", "avg_meta", "c"),
                                                             meta2, 1000.0, 2000.0), SequenceValue(
                                                             SequenceName("a", "cont_meta", "c"),
                                                             meta3, 1000.0, 2000.0), SequenceValue(
                                                             SequenceName("a", "raw_meta", "d"),
                                                             metaBypass, 1000.0, 2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "cont_meta",
                                                                                                 "d"),
                                                                                         metaBypass2,
                                                                                         1000.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "avg_meta",
                                                                                                 "d"),
                                                                                         metaBypass3,
                                                                                         1000.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "raw",
                                                                                                 "c"),
                                                                                         Variant::Root(
                                                                                                 1.0),
                                                                                         1000.0,
                                                                                         1400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "cont",
                                                                                                 "c"),
                                                                                         Variant::Root(
                                                                                                 2.0),
                                                                                         1000.0,
                                                                                         1400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "avg",
                                                                                                 "c"),
                                                                                         Variant::Root(
                                                                                                 3.0),
                                                                                         1000.0,
                                                                                         1400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "raw",
                                                                                                 "c",
                                                                                                 SequenceName::Flavors{
                                                                                                         "end"}),
                                                                                         Variant::Root(
                                                                                                 1.1),
                                                                                         1000.0,
                                                                                         1400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "cont",
                                                                                                 "c",
                                                                                                 SequenceName::Flavors{
                                                                                                         "end"}),
                                                                                         Variant::Root(
                                                                                                 2.1),
                                                                                         1000.0,
                                                                                         1400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "avg",
                                                                                                 "c",
                                                                                                 SequenceName::Flavors{
                                                                                                         "end"}),
                                                                                         Variant::Root(
                                                                                                 3.1),
                                                                                         1000.0,
                                                                                         1400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "raw",
                                                                                                 "c"),
                                                                                         Variant::Root(
                                                                                                 4.0),
                                                                                         1600.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "cont",
                                                                                                 "c"),
                                                                                         Variant::Root(
                                                                                                 5.0),
                                                                                         1600.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "avg",
                                                                                                 "c"),
                                                                                         Variant::Root(
                                                                                                 6.0),
                                                                                         1600.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "raw",
                                                                                                 "c",
                                                                                                 SequenceName::Flavors{
                                                                                                         "end"}),
                                                                                         Variant::Root(
                                                                                                 4.1),
                                                                                         1600.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "cont",
                                                                                                 "c",
                                                                                                 SequenceName::Flavors{
                                                                                                         "end"}),
                                                                                         Variant::Root(
                                                                                                 5.1),
                                                                                         1600.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "avg",
                                                                                                 "c",
                                                                                                 SequenceName::Flavors{
                                                                                                         "end"}),
                                                                                         Variant::Root(
                                                                                                 6.1),
                                                                                         1600.0,
                                                                                         2000.0)})
                                                     << (SequenceValue::Transfer{SequenceValue(
                                                             SequenceName("a", "raw_meta", "d"),
                                                             metaBypass, 1000.0, 2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "cont_meta",
                                                                                                 "d"),
                                                                                         metaBypass2,
                                                                                         1000.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "avg_meta",
                                                                                                 "d"),
                                                                                         metaBypass3,
                                                                                         1000.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "raw",
                                                                                                 "c"),
                                                                                         Variant::Root(
                                                                                                 1.0),
                                                                                         1000.0,
                                                                                         1400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "raw",
                                                                                                 "c",
                                                                                                 SequenceName::Flavors{
                                                                                                         "end"}),
                                                                                         Variant::Root(
                                                                                                 1.1),
                                                                                         1000.0,
                                                                                         1400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "raw",
                                                                                                 "c"),
                                                                                         Variant::Root(
                                                                                                 4.0),
                                                                                         1600.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "raw",
                                                                                                 "c",
                                                                                                 SequenceName::Flavors{
                                                                                                         "end"}),
                                                                                         Variant::Root(
                                                                                                 4.1),
                                                                                         1600.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "cont",
                                                                                                 "c"),
                                                                                         Variant::Root(
                                                                                                 3.0),
                                                                                         1000.0,
                                                                                         1400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "cont",
                                                                                                 "c",
                                                                                                 SequenceName::Flavors{
                                                                                                         "end"}),
                                                                                         Variant::Root(
                                                                                                 3.1),
                                                                                         1000.0,
                                                                                         1400.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "cont",
                                                                                                 "c"),
                                                                                         Variant::Root(
                                                                                                 6.0),
                                                                                         1600.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "cont",
                                                                                                 "c",
                                                                                                 SequenceName::Flavors{
                                                                                                         "end"}),
                                                                                         Variant::Root(
                                                                                                 6.1),
                                                                                         1600.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "avg",
                                                                                                 "c"),
                                                                                         Variant::Root(
                                                                                                 3.0),
                                                                                         1000.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "avg",
                                                                                                 "c",
                                                                                                 SequenceName::Flavors{
                                                                                                         "end"}),
                                                                                         Variant::Root(
                                                                                                 6.1),
                                                                                         1000.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "avg",
                                                                                                 "c",
                                                                                                 SequenceName::Flavors{
                                                                                                         "cover"}),
                                                                                         Variant::Root(
                                                                                                 0.8),
                                                                                         1000.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "raw_meta",
                                                                                                 "c"),
                                                                                         meta,
                                                                                         1000.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "cont_meta",
                                                                                                 "c"),
                                                                                         meta2,
                                                                                         1000.0,
                                                                                         2000.0),
                                                                                 SequenceValue(
                                                                                         SequenceName(
                                                                                                 "a",
                                                                                                 "avg_meta",
                                                                                                 "c"),
                                                                                         cms("Test",
                                                                                             "Value",
                                                                                             "Result2"),
                                                                                         1000.0,
                                                                                         2000.0)});


        QTest::newRow("Editing continuous calculate single") << Engine_Editing_ContinuousRecalculate
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(SequenceName("a",
                                                                                                "raw",
                                                                                                "c"),
                                                                                   Variant::Root(
                                                                                           1.0),
                                                                                   1000.0, 2000.0),
                                                                     SequenceValue(SequenceName("a",
                                                                                                "cont",
                                                                                                "c"),
                                                                                   Variant::Root(
                                                                                           2.0),
                                                                                   1000.0, 2000.0),
                                                                     SequenceValue(SequenceName("a",
                                                                                                "avg",
                                                                                                "c"),
                                                                                   Variant::Root(
                                                                                           -1.0),
                                                                                   1000.0, 2000.0),
                                                                     SequenceValue(SequenceName("a",
                                                                                                "raw",
                                                                                                "d"),
                                                                                   Variant::Root(
                                                                                           3.0),
                                                                                   1000.0, 2000.0),
                                                                     SequenceValue(SequenceName("a",
                                                                                                "avg",
                                                                                                "d"),
                                                                                   Variant::Root(
                                                                                           4.0),
                                                                                   1000.0, 2000.0),
                                                                     SequenceValue(SequenceName("a",
                                                                                                "cont",
                                                                                                "d"),
                                                                                   Variant::Root(
                                                                                           5.0),
                                                                                   1000.0, 2000.0)})
                                                             << (SequenceValue::Transfer{
                                                                     SequenceValue(SequenceName("a",
                                                                                                "raw",
                                                                                                "d"),
                                                                                   Variant::Root(
                                                                                           3.0),
                                                                                   1000.0, 2000.0),
                                                                     SequenceValue(SequenceName("a",
                                                                                                "avg",
                                                                                                "d"),
                                                                                   Variant::Root(
                                                                                           4.0),
                                                                                   1000.0, 2000.0),
                                                                     SequenceValue(SequenceName("a",
                                                                                                "cont",
                                                                                                "d"),
                                                                                   Variant::Root(
                                                                                           5.0),
                                                                                   1000.0, 2000.0),
                                                                     SequenceValue(SequenceName("a",
                                                                                                "raw",
                                                                                                "c"),
                                                                                   Variant::Root(
                                                                                           1.0),
                                                                                   1000.0, 2000.0),
                                                                     SequenceValue(SequenceName("a",
                                                                                                "cont",
                                                                                                "c"),
                                                                                   Variant::Root(
                                                                                           2.0),
                                                                                   1000.0, 2000.0),
                                                                     SequenceValue(SequenceName("a",
                                                                                                "avg",
                                                                                                "c"),
                                                                                   Variant::Root(
                                                                                           2.0),
                                                                                   1000.0,
                                                                                   2000.0)});
        QTest::newRow("Editing continuous calculate two") << Engine_Editing_ContinuousRecalculate
                                                          << (SequenceValue::Transfer{SequenceValue(
                                                                  SequenceName("a", "raw", "c"),
                                                                  Variant::Root(1.0), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "cont", "c"),
                                                                  Variant::Root(2.0), 1000.0,
                                                                  1500.0), SequenceValue(
                                                                  SequenceName("a", "avg", "c"),
                                                                  Variant::Root(-1.0), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "raw", "d"),
                                                                  Variant::Root(3.0), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "avg", "d"),
                                                                  Variant::Root(4.0), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "cont", "d"),
                                                                  Variant::Root(5.0), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "cont", "c"),
                                                                  Variant::Root(8.0), 1500.0,
                                                                  2000.0)})
                                                          << (SequenceValue::Transfer{SequenceValue(
                                                                  SequenceName("a", "raw", "d"),
                                                                  Variant::Root(3.0), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "avg", "d"),
                                                                  Variant::Root(4.0), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "cont", "d"),
                                                                  Variant::Root(5.0), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "raw", "c"),
                                                                  Variant::Root(1.0), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "cont", "c"),
                                                                  Variant::Root(2.0), 1000.0,
                                                                  1500.0), SequenceValue(
                                                                  SequenceName("a", "cont", "c"),
                                                                  Variant::Root(8.0), 1500.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "avg", "c"),
                                                                  Variant::Root(5.0), 1000.0,
                                                                  2000.0)});
        QTest::newRow("Editing continuous calculate two cover")
                << Engine_Editing_ContinuousRecalculate << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "raw", "c"), Variant::Root(1.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "c"), Variant::Root(2.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "avg", "c"), Variant::Root(-1.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "c", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.1), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "raw", "d"), Variant::Root(3.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "d"), Variant::Root(4.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "d", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.25), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "d"), Variant::Root(5.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "c"), Variant::Root(3.0), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "c", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.6), 1500.0, 2000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "raw", "d"), Variant::Root(3.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "d"), Variant::Root(4.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "d", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.25), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "d"), Variant::Root(5.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "raw", "c"), Variant::Root(1.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "c"), Variant::Root(2.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "cont", "c"), Variant::Root(3.0), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "c"), Variant::Root(2.375), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "c", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.6), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "c", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.8), 1000.0, 2000.0)});
        QTest::newRow("Editing continuous calculate gap") << Engine_Editing_ContinuousRecalculate
                                                          << (SequenceValue::Transfer{SequenceValue(
                                                                  SequenceName("a", "raw", "c"),
                                                                  Variant::Root(1.0), 1000.0,
                                                                  1400.0), SequenceValue(
                                                                  SequenceName("a", "raw", "c",
                                                                               SequenceName::Flavors{
                                                                                       "cover"}),
                                                                  Variant::Root(0.5), 1000.0,
                                                                  1400.0), SequenceValue(
                                                                  SequenceName("a", "cont", "c"),
                                                                  Variant::Root(2.0), 1000.0,
                                                                  1400.0), SequenceValue(
                                                                  SequenceName("a", "avg", "c"),
                                                                  Variant::Root(-1.0), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "cont", "c"),
                                                                  Variant::Root(3.0), 1600.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "raw", "c"),
                                                                  Variant::Root(4.0), 1600.0,
                                                                  2000.0)})
                                                          << (SequenceValue::Transfer{SequenceValue(
                                                                  SequenceName("a", "raw", "c"),
                                                                  Variant::Root(1.0), 1000.0,
                                                                  1400.0), SequenceValue(
                                                                  SequenceName("a", "raw", "c",
                                                                               SequenceName::Flavors{
                                                                                       "cover"}),
                                                                  Variant::Root(0.5), 1000.0,
                                                                  1400.0), SequenceValue(
                                                                  SequenceName("a", "raw", "c"),
                                                                  Variant::Root(4.0), 1600.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "cont", "c"),
                                                                  Variant::Root(2.0), 1000.0,
                                                                  1400.0), SequenceValue(
                                                                  SequenceName("a", "cont", "c"),
                                                                  Variant::Root(3.0), 1600.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "avg", "c"),
                                                                  Variant::Root(2.5), 1000.0,
                                                                  2000.0), SequenceValue(
                                                                  SequenceName("a", "avg", "c",
                                                                               SequenceName::Flavors{
                                                                                       "cover"}),
                                                                  Variant::Root(0.8), 1000.0,
                                                                  2000.0)});
        QTest::newRow("Editing continuous calculate flags") << Engine_Editing_ContinuousRecalculate
                                                            << (SequenceValue::Transfer{
                                                                    SequenceValue(
                                                                            SequenceName("a", "raw",
                                                                                         "c"),
                                                                            Variant::Root(
                                                                                    Variant::Flags{
                                                                                            "F1"}),
                                                                            1000.0, 1500.0),
                                                                    SequenceValue(SequenceName("a",
                                                                                               "cont",
                                                                                               "c"),
                                                                                  Variant::Root(
                                                                                          Variant::Flags{
                                                                                                  "F2"}),
                                                                                  1000.0, 1400.0),
                                                                    SequenceValue(
                                                                            SequenceName("a", "avg",
                                                                                         "c"),
                                                                            Variant::Root(
                                                                                    Variant::Flags{
                                                                                            "F3"}),
                                                                            1000.0, 2000.0),
                                                                    SequenceValue(
                                                                            SequenceName("a", "raw",
                                                                                         "d"),
                                                                            Variant::Root(
                                                                                    Variant::Flags{
                                                                                            "F4"}),
                                                                            1000.0, 2000.0),
                                                                    SequenceValue(SequenceName("a",
                                                                                               "cont",
                                                                                               "d"),
                                                                                  Variant::Root(
                                                                                          Variant::Flags{
                                                                                                  "F5"}),
                                                                                  1000.0, 2000.0),
                                                                    SequenceValue(
                                                                            SequenceName("a", "avg",
                                                                                         "d"),
                                                                            Variant::Root(
                                                                                    Variant::Flags{
                                                                                            "F6"}),
                                                                            1000.0, 2000.0),
                                                                    SequenceValue(
                                                                            SequenceName("a", "raw",
                                                                                         "c"),
                                                                            Variant::Root(
                                                                                    Variant::Flags{
                                                                                            "F7"}),
                                                                            1500.0, 2000.0),
                                                                    SequenceValue(SequenceName("a",
                                                                                               "cont",
                                                                                               "c"),
                                                                                  Variant::Root(
                                                                                          Variant::Flags{
                                                                                                  "F8"}),
                                                                                  1600.0, 2000.0)})
                                                            << (SequenceValue::Transfer{
                                                                    SequenceValue(
                                                                            SequenceName("a", "raw",
                                                                                         "d"),
                                                                            Variant::Root(
                                                                                    Variant::Flags{
                                                                                            "F4"}),
                                                                            1000.0, 2000.0),
                                                                    SequenceValue(SequenceName("a",
                                                                                               "cont",
                                                                                               "d"),
                                                                                  Variant::Root(
                                                                                          Variant::Flags{
                                                                                                  "F5"}),
                                                                                  1000.0, 2000.0),
                                                                    SequenceValue(
                                                                            SequenceName("a", "avg",
                                                                                         "d"),
                                                                            Variant::Root(
                                                                                    Variant::Flags{
                                                                                            "F6"}),
                                                                            1000.0, 2000.0),
                                                                    SequenceValue(
                                                                            SequenceName("a", "raw",
                                                                                         "c"),
                                                                            Variant::Root(
                                                                                    Variant::Flags{
                                                                                            "F1"}),
                                                                            1000.0, 1500.0),
                                                                    SequenceValue(SequenceName("a",
                                                                                               "cont",
                                                                                               "c"),
                                                                                  Variant::Root(
                                                                                          Variant::Flags{
                                                                                                  "F2"}),
                                                                                  1000.0, 1400.0),
                                                                    SequenceValue(
                                                                            SequenceName("a", "raw",
                                                                                         "c"),
                                                                            Variant::Root(
                                                                                    Variant::Flags{
                                                                                            "F7"}),
                                                                            1500.0, 2000.0),
                                                                    SequenceValue(SequenceName("a",
                                                                                               "cont",
                                                                                               "c"),
                                                                                  Variant::Root(
                                                                                          Variant::Flags{
                                                                                                  "F8"}),
                                                                                  1600.0, 2000.0),
                                                                    SequenceValue(
                                                                            SequenceName("a", "avg",
                                                                                         "c"),
                                                                            Variant::Root(
                                                                                    Variant::Flags{
                                                                                            "F2",
                                                                                            "F8"}),
                                                                            1000.0, 2000.0),
                                                                    SequenceValue(
                                                                            SequenceName("a", "avg",
                                                                                         "c",
                                                                                         SequenceName::Flavors{
                                                                                                 "cover"}),
                                                                            Variant::Root(0.8),
                                                                            1000.0, 2000.0)});
        meta.write().setEmpty();
        meta.write().setType(Variant::Type::MetadataReal);
        meta.write().metadata("Smoothing").hash("Mode").setString("Difference");
        meta.write().metadata("Test").hash("Value").setString("Result1");
        meta2 = meta;
        meta2.write().metadata("Test").hash("Value").setString("Result2");
        meta3 = meta;
        meta3.write().metadata("Test").hash("Value").setString("Result3");
        metaBypass.write().setEmpty();
        metaBypass.write().setType(Variant::Type::MetadataReal);
        metaBypass.write().metadata("Smoothing").hash("Mode").setString("DifferenceInitial");
        metaBypass.write().metadata("Test").hash("Bypass").setString("Result1");
        metaBypass2 = metaBypass;
        metaBypass2.write().metadata("Test").hash("Bypass").setString("Result2");
        metaBypass3 = metaBypass;
        metaBypass3.write().metadata("Test").hash("Bypass").setString("Result3");
        QTest::newRow("Editing continuous calculate meta") << Engine_Editing_ContinuousRecalculate
                                                           << (SequenceValue::Transfer{
                                                                   SequenceValue(
                                                                           SequenceName("a", "raw",
                                                                                        "c"),
                                                                           Variant::Root(1.0),
                                                                           1000.0, 1400.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "raw",
                                                                                        "c",
                                                                                        SequenceName::Flavors{
                                                                                                "end"}),
                                                                           Variant::Root(1.1),
                                                                           1000.0, 1400.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "cont",
                                                                                        "c"),
                                                                           Variant::Root(2.0),
                                                                           1000.0, 1400.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "cont",
                                                                                        "c",
                                                                                        SequenceName::Flavors{
                                                                                                "end"}),
                                                                           Variant::Root(2.1),
                                                                           1000.0, 1400.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "avg",
                                                                                        "c"),
                                                                           Variant::Root(-1.0),
                                                                           1000.0, 2000.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "avg",
                                                                                        "c",
                                                                                        SequenceName::Flavors{
                                                                                                "end"}),
                                                                           Variant::Root(-1.1),
                                                                           1000.0, 2000.0),
                                                                   SequenceValue(SequenceName("a",
                                                                                              "raw_meta",
                                                                                              "c"),
                                                                                 meta, 1000.0,
                                                                                 2000.0),
                                                                   SequenceValue(SequenceName("a",
                                                                                              "avg_meta",
                                                                                              "c"),
                                                                                 meta2, 1000.0,
                                                                                 2000.0),
                                                                   SequenceValue(SequenceName("a",
                                                                                              "cont_meta",
                                                                                              "c"),
                                                                                 meta3, 1000.0,
                                                                                 2000.0),
                                                                   SequenceValue(SequenceName("a",
                                                                                              "raw_meta",
                                                                                              "d"),
                                                                                 metaBypass, 1000.0,
                                                                                 2000.0),
                                                                   SequenceValue(SequenceName("a",
                                                                                              "avg_meta",
                                                                                              "d"),
                                                                                 metaBypass2,
                                                                                 1000.0, 2000.0),
                                                                   SequenceValue(SequenceName("a",
                                                                                              "cont_meta",
                                                                                              "d"),
                                                                                 metaBypass3,
                                                                                 1000.0, 2000.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "raw",
                                                                                        "c"),
                                                                           Variant::Root(3.0),
                                                                           1600.0, 2000.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "raw",
                                                                                        "c",
                                                                                        SequenceName::Flavors{
                                                                                                "end"}),
                                                                           Variant::Root(3.1),
                                                                           1600.0, 2000.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "cont",
                                                                                        "c"),
                                                                           Variant::Root(4.0),
                                                                           1600.0, 2000.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "cont",
                                                                                        "c",
                                                                                        SequenceName::Flavors{
                                                                                                "end"}),
                                                                           Variant::Root(4.1),
                                                                           1600.0, 2000.0)})
                                                           << (SequenceValue::Transfer{
                                                                   SequenceValue(SequenceName("a",
                                                                                              "raw_meta",
                                                                                              "d"),
                                                                                 metaBypass, 1000.0,
                                                                                 2000.0),
                                                                   SequenceValue(SequenceName("a",
                                                                                              "avg_meta",
                                                                                              "d"),
                                                                                 metaBypass2,
                                                                                 1000.0, 2000.0),
                                                                   SequenceValue(SequenceName("a",
                                                                                              "cont_meta",
                                                                                              "d"),
                                                                                 metaBypass3,
                                                                                 1000.0, 2000.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "raw",
                                                                                        "c"),
                                                                           Variant::Root(1.0),
                                                                           1000.0, 1400.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "raw",
                                                                                        "c",
                                                                                        SequenceName::Flavors{
                                                                                                "end"}),
                                                                           Variant::Root(1.1),
                                                                           1000.0, 1400.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "raw",
                                                                                        "c"),
                                                                           Variant::Root(3.0),
                                                                           1600.0, 2000.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "raw",
                                                                                        "c",
                                                                                        SequenceName::Flavors{
                                                                                                "end"}),
                                                                           Variant::Root(3.1),
                                                                           1600.0, 2000.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "cont",
                                                                                        "c"),
                                                                           Variant::Root(2.0),
                                                                           1000.0, 1400.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "cont",
                                                                                        "c",
                                                                                        SequenceName::Flavors{
                                                                                                "end"}),
                                                                           Variant::Root(2.1),
                                                                           1000.0, 1400.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "cont",
                                                                                        "c"),
                                                                           Variant::Root(4.0),
                                                                           1600.0, 2000.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "cont",
                                                                                        "c",
                                                                                        SequenceName::Flavors{
                                                                                                "end"}),
                                                                           Variant::Root(4.1),
                                                                           1600.0, 2000.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "avg",
                                                                                        "c"),
                                                                           Variant::Root(2.0),
                                                                           1000.0, 2000.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "avg",
                                                                                        "c",
                                                                                        SequenceName::Flavors{
                                                                                                "end"}),
                                                                           Variant::Root(4.1),
                                                                           1000.0, 2000.0),
                                                                   SequenceValue(
                                                                           SequenceName("a", "avg",
                                                                                        "c",
                                                                                        SequenceName::Flavors{
                                                                                                "cover"}),
                                                                           Variant::Root(0.8),
                                                                           1000.0, 2000.0),
                                                                   SequenceValue(SequenceName("a",
                                                                                              "raw_meta",
                                                                                              "c"),
                                                                                 meta, 1000.0,
                                                                                 2000.0),
                                                                   SequenceValue(SequenceName("a",
                                                                                              "cont_meta",
                                                                                              "c"),
                                                                                 meta3, 1000.0,
                                                                                 2000.0),
                                                                   SequenceValue(SequenceName("a",
                                                                                              "avg_meta",
                                                                                              "c"),
                                                                                 cms("Test",
                                                                                     "Value",
                                                                                     "Result3"),
                                                                                 1000.0, 2000.0)});


        QTest::newRow("Editing final single") << Engine_Editing_Final << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "raw", "stats"), Variant::Root(1.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avg", "stats"), Variant::Root(2.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "cont", "stats"), Variant::Root(3.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "stats", "stats"), Variant::Root(4.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "raw", "cont"), Variant::Root(5.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "cont"), Variant::Root(6.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "cont"), Variant::Root(7.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "stats", "cont"), Variant::Root(8.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "raw", "full"), Variant::Root(9.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "full"), Variant::Root(10.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "cont", "full"), Variant::Root(11.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "stats", "full"), Variant::Root(12.0), 1000.0,
                              2000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "clean", "stats"), Variant::Root(1.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "stats"), Variant::Root(2.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "stats", SequenceName::Flavors{"stats"}),
                              chd("Mean", 4.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "clean", "cont"), Variant::Root(5.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "cont"), Variant::Root(7.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "cont", SequenceName::Flavors{"stats"}),
                              chd("Mean", 8.0), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "clean", "full"), Variant::Root(9.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "full"), Variant::Root(12.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "full", SequenceName::Flavors{"stats"}),
                              chd("Mean", 12.0), 1000.0, 2000.0)});
        QTest::newRow("Editing final single cover") << Engine_Editing_Final
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("a", "raw", "stats",
                                                                         SequenceName::Flavors{
                                                                                 "cover"}),
                                                            Variant::Root(0.01), 1000.0, 2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "raw",
                                                                                                "stats"),
                                                                                        Variant::Root(
                                                                                                1.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avg",
                                                                                                "stats"),
                                                                                        Variant::Root(
                                                                                                2.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avg",
                                                                                                "stats",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.02),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "cont",
                                                                                                "stats"),
                                                                                        Variant::Root(
                                                                                                3.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "cont",
                                                                                                "stats",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.03),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "stats",
                                                                                                "stats"),
                                                                                        Variant::Root(
                                                                                                4.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "stats",
                                                                                                "stats",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.04),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "raw",
                                                                                                "cont"),
                                                                                        Variant::Root(
                                                                                                5.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "raw",
                                                                                                "cont",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.05),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avg",
                                                                                                "cont"),
                                                                                        Variant::Root(
                                                                                                6.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avg",
                                                                                                "cont",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.06),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "cont",
                                                                                                "cont"),
                                                                                        Variant::Root(
                                                                                                7.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "cont",
                                                                                                "cont",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.07),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "stats",
                                                                                                "cont"),
                                                                                        Variant::Root(
                                                                                                8.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "stats",
                                                                                                "cont",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.08),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "raw",
                                                                                                "full"),
                                                                                        Variant::Root(
                                                                                                9.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "raw",
                                                                                                "full",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.09),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avg",
                                                                                                "full"),
                                                                                        Variant::Root(
                                                                                                10.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avg",
                                                                                                "full",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.10),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "cont",
                                                                                                "full",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.11),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "cont",
                                                                                                "full"),
                                                                                        Variant::Root(
                                                                                                11.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "stats",
                                                                                                "full",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.12),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "stats",
                                                                                                "full"),
                                                                                        Variant::Root(
                                                                                                12.0),
                                                                                        1000.0,
                                                                                        2000.0)})
                                                    << (SequenceValue::Transfer{SequenceValue(
                                                            SequenceName("a", "clean", "stats"),
                                                            Variant::Root(1.0), 1000.0, 2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "clean",
                                                                                                "stats",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.01),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avgh",
                                                                                                "stats"),
                                                                                        Variant::Root(
                                                                                                2.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avgh",
                                                                                                "stats",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.02),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avgh",
                                                                                                "stats",
                                                                                                SequenceName::Flavors{
                                                                                                        "stats"}),
                                                                                        chd("Mean",
                                                                                            4.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "clean",
                                                                                                "cont"),
                                                                                        Variant::Root(
                                                                                                5.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "clean",
                                                                                                "cont",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.05),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avgh",
                                                                                                "cont"),
                                                                                        Variant::Root(
                                                                                                7.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avgh",
                                                                                                "cont",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.07),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avgh",
                                                                                                "cont",
                                                                                                SequenceName::Flavors{
                                                                                                        "stats"}),
                                                                                        chd("Mean",
                                                                                            8.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "clean",
                                                                                                "full"),
                                                                                        Variant::Root(
                                                                                                9.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "clean",
                                                                                                "full",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.09),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avgh",
                                                                                                "full"),
                                                                                        Variant::Root(
                                                                                                12.0),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avgh",
                                                                                                "full",
                                                                                                SequenceName::Flavors{
                                                                                                        "cover"}),
                                                                                        Variant::Root(
                                                                                                0.12),
                                                                                        1000.0,
                                                                                        2000.0),
                                                                                SequenceValue(
                                                                                        SequenceName(
                                                                                                "a",
                                                                                                "avgh",
                                                                                                "full",
                                                                                                SequenceName::Flavors{
                                                                                                        "stats"}),
                                                                                        chd("Mean",
                                                                                            12.0),
                                                                                        1000.0,
                                                                                        2000.0)});
        QTest::newRow("Editing final two") << Engine_Editing_Final << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "raw", "stats"), Variant::Root(1.0), 1000.0,
                              1400.0),
                SequenceValue(SequenceName("a", "avg", "stats"), Variant::Root(2.0), 1000.0,
                              1400.0),
                SequenceValue(SequenceName("a", "cont", "stats"), Variant::Root(3.0), 1000.0,
                              1400.0),
                SequenceValue(SequenceName("a", "stats", "stats"), Variant::Root(4.0), 1000.0,
                              1400.0),
                SequenceValue(SequenceName("a", "raw", "cont"), Variant::Root(5.0), 1000.0, 1400.0),
                SequenceValue(SequenceName("a", "avg", "cont"), Variant::Root(6.0), 1000.0, 1400.0),
                SequenceValue(SequenceName("a", "cont", "cont"), Variant::Root(7.0), 1000.0,
                              1400.0),
                SequenceValue(SequenceName("a", "stats", "cont"), Variant::Root(8.0), 1000.0,
                              1400.0),
                SequenceValue(SequenceName("a", "raw", "full"), Variant::Root(9.0), 1000.0, 1400.0),
                SequenceValue(SequenceName("a", "avg", "full"), Variant::Root(10.0), 1000.0,
                              1400.0),
                SequenceValue(SequenceName("a", "cont", "full"), Variant::Root(11.0), 1000.0,
                              1400.0),
                SequenceValue(SequenceName("a", "stats", "full"), Variant::Root(12.0), 1000.0,
                              1400.0),
                SequenceValue(SequenceName("a", "raw", "stats"), Variant::Root(1.5), 1600.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avg", "stats"), Variant::Root(2.5), 1600.0,
                              2000.0),
                SequenceValue(SequenceName("a", "cont", "stats"), Variant::Root(3.5), 1600.0,
                              2000.0),
                SequenceValue(SequenceName("a", "stats", "stats"), Variant::Root(4.5), 1600.0,
                              2000.0),
                SequenceValue(SequenceName("a", "raw", "cont"), Variant::Root(5.5), 1600.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "cont"), Variant::Root(6.5), 1600.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "cont"), Variant::Root(7.5), 1600.0,
                              2000.0),
                SequenceValue(SequenceName("a", "stats", "cont"), Variant::Root(8.5), 1600.0,
                              2000.0),
                SequenceValue(SequenceName("a", "raw", "full"), Variant::Root(9.5), 1600.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "full"), Variant::Root(10.5), 1600.0,
                              2000.0),
                SequenceValue(SequenceName("a", "cont", "full"), Variant::Root(11.5), 1600.0,
                              2000.0),
                SequenceValue(SequenceName("a", "stats", "full"), Variant::Root(12.5), 1600.0,
                              2000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "clean", "stats"), Variant::Root(1.0), 1000.0,
                              1400.0),
                SequenceValue(SequenceName("a", "avgh", "stats"), Variant::Root(2.0), 1000.0,
                              1400.0),
                SequenceValue(SequenceName("a", "clean", "stats"), Variant::Root(1.5), 1600.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "stats"), Variant::Root(2.5), 1600.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "stats", SequenceName::Flavors{"stats"}),
                              chd("Mean", 4.25), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "clean", "cont"), Variant::Root(5.0), 1000.0,
                              1400.0),
                SequenceValue(SequenceName("a", "clean", "cont"), Variant::Root(5.5), 1600.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "cont"), Variant::Root(7.25), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "cont", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.8), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avgh", "cont", SequenceName::Flavors{"stats"}),
                              chd("Mean", 8.25), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "clean", "full"), Variant::Root(9.0), 1000.0,
                              1400.0),
                SequenceValue(SequenceName("a", "clean", "full"), Variant::Root(9.5), 1600.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "full"), Variant::Root(12.25), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "full", SequenceName::Flavors{"cover"}),
                              Variant::Root(0.8), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avgh", "full", SequenceName::Flavors{"stats"}),
                              chd("Mean", 12.25), 1000.0, 2000.0)});
        QTest::newRow("Editing final two cover") << Engine_Editing_Final
                                                 << (SequenceValue::Transfer{SequenceValue(
                                                         SequenceName("a", "raw", "stats",
                                                                      SequenceName::Flavors{
                                                                              "cover"}),
                                                         Variant::Root(0.01), 1000.0, 1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "raw",
                                                                                             "stats"),
                                                                                     Variant::Root(
                                                                                             1.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avg",
                                                                                             "stats"),
                                                                                     Variant::Root(
                                                                                             2.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avg",
                                                                                             "stats",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.02),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "cont",
                                                                                             "stats"),
                                                                                     Variant::Root(
                                                                                             3.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "cont",
                                                                                             "stats",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.03),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "stats",
                                                                                             "stats"),
                                                                                     Variant::Root(
                                                                                             4.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "stats",
                                                                                             "stats",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.04),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "raw",
                                                                                             "cont"),
                                                                                     Variant::Root(
                                                                                             5.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "raw",
                                                                                             "cont",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.05),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avg",
                                                                                             "cont"),
                                                                                     Variant::Root(
                                                                                             6.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avg",
                                                                                             "cont",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.06),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "cont",
                                                                                             "cont"),
                                                                                     Variant::Root(
                                                                                             7.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "cont",
                                                                                             "cont",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.07),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "stats",
                                                                                             "cont"),
                                                                                     Variant::Root(
                                                                                             8.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "stats",
                                                                                             "cont",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.08),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "raw",
                                                                                             "full"),
                                                                                     Variant::Root(
                                                                                             9.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "raw",
                                                                                             "full",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.09),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avg",
                                                                                             "full"),
                                                                                     Variant::Root(
                                                                                             10.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avg",
                                                                                             "full",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.10),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "cont",
                                                                                             "full",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.11),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "cont",
                                                                                             "full"),
                                                                                     Variant::Root(
                                                                                             11.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "stats",
                                                                                             "full",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.12),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "stats",
                                                                                             "full"),
                                                                                     Variant::Root(
                                                                                             12.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "raw",
                                                                                             "stats"),
                                                                                     Variant::Root(
                                                                                             1.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "raw",
                                                                                             "stats",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.015),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avg",
                                                                                             "stats"),
                                                                                     Variant::Root(
                                                                                             2.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avg",
                                                                                             "stats",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.025),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "cont",
                                                                                             "stats"),
                                                                                     Variant::Root(
                                                                                             3.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "cont",
                                                                                             "stats",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.035),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "stats",
                                                                                             "stats"),
                                                                                     Variant::Root(
                                                                                             4.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "stats",
                                                                                             "stats",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.045),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "raw",
                                                                                             "cont"),
                                                                                     Variant::Root(
                                                                                             5.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "raw",
                                                                                             "cont",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.055),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avg",
                                                                                             "cont"),
                                                                                     Variant::Root(
                                                                                             6.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avg",
                                                                                             "cont",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.065),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "cont",
                                                                                             "cont"),
                                                                                     Variant::Root(
                                                                                             7.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "cont",
                                                                                             "cont",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.075),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "stats",
                                                                                             "cont"),
                                                                                     Variant::Root(
                                                                                             8.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "stats",
                                                                                             "cont",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.085),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "raw",
                                                                                             "full"),
                                                                                     Variant::Root(
                                                                                             9.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "raw",
                                                                                             "full",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.095),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avg",
                                                                                             "full"),
                                                                                     Variant::Root(
                                                                                             10.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avg",
                                                                                             "full",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.105),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "cont",
                                                                                             "full",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.115),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "cont",
                                                                                             "full"),
                                                                                     Variant::Root(
                                                                                             11.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "stats",
                                                                                             "full",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.125),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "stats",
                                                                                             "full"),
                                                                                     Variant::Root(
                                                                                             12.5),
                                                                                     1500.0,
                                                                                     2000.0)})
                                                 << (SequenceValue::Transfer{SequenceValue(
                                                         SequenceName("a", "clean", "stats"),
                                                         Variant::Root(1.0), 1000.0, 1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "clean",
                                                                                             "stats"),
                                                                                     Variant::Root(
                                                                                             1.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "clean",
                                                                                             "stats",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.01),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "clean",
                                                                                             "stats",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.015),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avgh",
                                                                                             "stats"),
                                                                                     Variant::Root(
                                                                                             2.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avgh",
                                                                                             "stats"),
                                                                                     Variant::Root(
                                                                                             2.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avgh",
                                                                                             "stats",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.02),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avgh",
                                                                                             "stats",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.025),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avgh",
                                                                                             "stats",
                                                                                             SequenceName::Flavors{
                                                                                                     "stats"}),
                                                                                     chd("Mean",
                                                                                         4.25),
                                                                                     1000.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "clean",
                                                                                             "cont"),
                                                                                     Variant::Root(
                                                                                             5.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "clean",
                                                                                             "cont"),
                                                                                     Variant::Root(
                                                                                             5.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "clean",
                                                                                             "cont",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.05),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "clean",
                                                                                             "cont",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.055),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avgh",
                                                                                             "cont"),
                                                                                     Variant::Root(
                                                                                             (0.07 *
                                                                                                     7.0 +
                                                                                                     0.075 *
                                                                                                             7.5) /
                                                                                                     (0.07 +
                                                                                                             0.075)),
                                                                                     1000.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avgh",
                                                                                             "cont",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.0725),
                                                                                     1000.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avgh",
                                                                                             "cont",
                                                                                             SequenceName::Flavors{
                                                                                                     "stats"}),
                                                                                     chd("Mean",
                                                                                         8.25),
                                                                                     1000.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "clean",
                                                                                             "full"),
                                                                                     Variant::Root(
                                                                                             9.0),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "clean",
                                                                                             "full"),
                                                                                     Variant::Root(
                                                                                             9.5),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "clean",
                                                                                             "full",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.09),
                                                                                     1000.0,
                                                                                     1500.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "clean",
                                                                                             "full",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.095),
                                                                                     1500.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avgh",
                                                                                             "full"),
                                                                                     Variant::Root(
                                                                                             (0.12 *
                                                                                                     12.0 +
                                                                                                     0.125 *
                                                                                                             12.5) /
                                                                                                     (0.12 +
                                                                                                             0.125)),
                                                                                     1000.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avgh",
                                                                                             "full",
                                                                                             SequenceName::Flavors{
                                                                                                     "cover"}),
                                                                                     Variant::Root(
                                                                                             0.1225),
                                                                                     1000.0,
                                                                                     2000.0),
                                                                             SequenceValue(
                                                                                     SequenceName(
                                                                                             "a",
                                                                                             "avgh",
                                                                                             "full",
                                                                                             SequenceName::Flavors{
                                                                                                     "stats"}),
                                                                                     chd("Mean",
                                                                                         12.25),
                                                                                     1000.0,
                                                                                     2000.0)});
        QTest::newRow("Editing final flags") << Engine_Editing_Final << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "raw", "stats"),
                              Variant::Root(Variant::Flags{"F1"}), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "avg", "stats"),
                              Variant::Root(Variant::Flags{"F2"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "stats"),
                              Variant::Root(Variant::Flags{"F4"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "stats", "stats"),
                              Variant::Root(Variant::Flags{"F5"}), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "raw", "cont"), Variant::Root(Variant::Flags{"F6"}),
                              1000.0, 1400.0),
                SequenceValue(SequenceName("a", "avg", "cont"), Variant::Root(Variant::Flags{"F7"}),
                              1000.0, 2000.0), SequenceValue(SequenceName("a", "cont", "cont"),
                                                             Variant::Root(Variant::Flags{"F8"}),
                                                             1000.0, 1400.0),
                SequenceValue(SequenceName("a", "stats", "cont"),
                              Variant::Root(Variant::Flags{"F9"}), 1000.0, 1400.0),
                SequenceValue(SequenceName("a", "raw", "full"),
                              Variant::Root(Variant::Flags{"F10"}), 1000.0, 1400.0),
                SequenceValue(SequenceName("a", "avg", "full"),
                              Variant::Root(Variant::Flags{"F11"}), 1000.0, 1400.0),
                SequenceValue(SequenceName("a", "cont", "full"),
                              Variant::Root(Variant::Flags{"F12"}), 1000.0, 1400.0),
                SequenceValue(SequenceName("a", "stats", "full"),
                              Variant::Root(Variant::Flags{"F13"}), 1000.0, 1400.0),
                SequenceValue(SequenceName("a", "raw", "stats"),
                              Variant::Root(Variant::Flags{"F14"}), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "raw", "cont"),
                              Variant::Root(Variant::Flags{"F15"}), 1600.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "cont"),
                              Variant::Root(Variant::Flags{"F16"}), 1600.0, 2000.0),
                SequenceValue(SequenceName("a", "stats", "cont"),
                              Variant::Root(Variant::Flags{"F17"}), 1600.0, 2000.0),
                SequenceValue(SequenceName("a", "raw", "full"),
                              Variant::Root(Variant::Flags{"F18"}), 1600.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "full"),
                              Variant::Root(Variant::Flags{"F19"}), 1600.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "full"),
                              Variant::Root(Variant::Flags{"F20"}), 1600.0, 2000.0),
                SequenceValue(SequenceName("a", "stats", "full"),
                              Variant::Root(Variant::Flags{"F21"}), 1600.0, 2000.0)})
                                             << (SequenceValue::Transfer{SequenceValue(
                                                     SequenceName("a", "clean", "stats"),
                                                     Variant::Root(Variant::Flags{"F1"}), 1000.0,
                                                     1500.0), SequenceValue(
                                                     SequenceName("a", "clean", "stats"),
                                                     Variant::Root(Variant::Flags{"F14"}), 1500.0,
                                                     2000.0), SequenceValue(
                                                     SequenceName("a", "avgh", "stats"),
                                                     Variant::Root(Variant::Flags{"F2"}), 1000.0,
                                                     2000.0), SequenceValue(
                                                     SequenceName("a", "clean", "cont"),
                                                     Variant::Root(Variant::Flags{"F6"}), 1000.0,
                                                     1400.0), SequenceValue(
                                                     SequenceName("a", "clean", "cont"),
                                                     Variant::Root(Variant::Flags{"F15"}), 1600.0,
                                                     2000.0), SequenceValue(
                                                     SequenceName("a", "avgh", "cont"),
                                                     Variant::Root(Variant::Flags{"F8", "F16"}),
                                                     1000.0, 2000.0), SequenceValue(
                                                     SequenceName("a", "avgh", "cont",
                                                                  SequenceName::Flavors{"cover"}),
                                                     Variant::Root(0.8), 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("a",
                                                                                              "clean",
                                                                                              "full"),
                                                                                 Variant::Root(
                                                                                         Variant::Flags{
                                                                                                 "F10"}),
                                                                                 1000.0, 1400.0),
                                                                         SequenceValue(
                                                                                 SequenceName("a",
                                                                                              "clean",
                                                                                              "full"),
                                                                                 Variant::Root(
                                                                                         Variant::Flags{
                                                                                                 "F18"}),
                                                                                 1600.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("a",
                                                                                              "avgh",
                                                                                              "full"),
                                                                                 Variant::Root(
                                                                                         Variant::Flags{
                                                                                                 "F13",
                                                                                                 "F21"}),
                                                                                 1000.0, 2000.0),
                                                                         SequenceValue(
                                                                                 SequenceName("a",
                                                                                              "avgh",
                                                                                              "full",
                                                                                              SequenceName::Flavors{
                                                                                                      "cover"}),
                                                                                 Variant::Root(0.8),
                                                                                 1000.0, 2000.0)});
        meta.write().setEmpty();
        meta.write().setType(Variant::Type::MetadataReal);
        meta.write().metadata("Smoothing").hash("Mode").setString("Difference");
        meta.write().metadata("Test").hash("Value").setString("Result1");
        meta2 = meta;
        meta2.write().metadata("Test").hash("Value").setString("Result2");
        meta3 = meta;
        meta3.write().metadata("Test").hash("Value").setString("Result3");
        Variant::Root meta4;
        Variant::Root meta5;
        Variant::Root meta6;
        Variant::Root meta7;
        Variant::Root meta8;
        Variant::Root meta9;
        Variant::Root meta10;
        Variant::Root meta11;
        Variant::Root meta12;
        meta4 = meta;
        meta4.write().metadata("Test").hash("Value").setString("Result4");
        meta5 = meta;
        meta5.write().metadata("Test").hash("Value").setString("Result5");
        meta6 = meta;
        meta6.write().metadata("Test").hash("Value").setString("Result6");
        meta7 = meta;
        meta7.write().metadata("Test").hash("Value").setString("Result7");
        meta8 = meta;
        meta8.write().metadata("Test").hash("Value").setString("Result8");
        meta9 = meta;
        meta9.write().metadata("Test").hash("Value").setString("Result9");
        meta10 = meta;
        meta10.write().metadata("Test").hash("Value").setString("Result10");
        meta11 = meta;
        meta11.write().metadata("Test").hash("Value").setString("Result11");
        meta12 = meta;
        meta12.write().metadata("Test").hash("Value").setString("Result12");
        QTest::newRow("Editing final meta") << Engine_Editing_Final << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "raw_meta", "stats"), meta, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avg_meta", "stats"), meta2, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "cont_meta", "stats"), meta3, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "stats_meta", "stats"), meta4, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "raw", "stats"), Variant::Root(1.0), 1000.0,
                              1500.0),
                SequenceValue(SequenceName("a", "avg", "stats"), Variant::Root(2.0), 1000.0,
                              1500.0),
                SequenceValue(SequenceName("a", "cont", "stats"), Variant::Root(3.0), 1000.0,
                              1500.0),
                SequenceValue(SequenceName("a", "stats", "stats"), Variant::Root(4.0), 1000.0,
                              1500.0),
                SequenceValue(SequenceName("a", "raw", "stats", SequenceName::Flavors{"end"}),
                              Variant::Root(1.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "avg", "stats", SequenceName::Flavors{"end"}),
                              Variant::Root(2.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "cont", "stats", SequenceName::Flavors{"end"}),
                              Variant::Root(3.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "stats", "stats", SequenceName::Flavors{"end"}),
                              Variant::Root(4.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "raw_meta", "cont"), meta5, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avg_meta", "cont"), meta6, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "cont_meta", "cont"), meta7, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "stats_meta", "cont"), meta8, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "raw", "cont"), Variant::Root(5.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "avg", "cont"), Variant::Root(6.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "cont", "cont"), Variant::Root(7.0), 1000.0,
                              1500.0),
                SequenceValue(SequenceName("a", "stats", "cont"), Variant::Root(8.0), 1000.0,
                              1500.0),
                SequenceValue(SequenceName("a", "raw", "cont", SequenceName::Flavors{"end"}),
                              Variant::Root(5.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "avg", "cont", SequenceName::Flavors{"end"}),
                              Variant::Root(6.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "cont", "cont", SequenceName::Flavors{"end"}),
                              Variant::Root(7.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "stats", "cont", SequenceName::Flavors{"end"}),
                              Variant::Root(8.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "raw_meta", "full"), meta9, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avg_meta", "full"), meta10, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "cont_meta", "full"), meta11, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "stats_meta", "full"), meta12, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "raw", "full"), Variant::Root(9.0), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "avg", "full"), Variant::Root(10.0), 1000.0,
                              1500.0),
                SequenceValue(SequenceName("a", "cont", "full"), Variant::Root(11.0), 1000.0,
                              1500.0),
                SequenceValue(SequenceName("a", "stats", "full"), Variant::Root(12.0), 1000.0,
                              1500.0),
                SequenceValue(SequenceName("a", "raw", "full", SequenceName::Flavors{"end"}),
                              Variant::Root(9.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "avg", "full", SequenceName::Flavors{"end"}),
                              Variant::Root(10.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "cont", "full", SequenceName::Flavors{"end"}),
                              Variant::Root(11.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "stats", "full", SequenceName::Flavors{"end"}),
                              Variant::Root(12.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "raw", "stats"), Variant::Root(1.2), 1500.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avg", "stats"), Variant::Root(2.2), 1500.0,
                              2000.0),
                SequenceValue(SequenceName("a", "cont", "stats"), Variant::Root(3.2), 1500.0,
                              2000.0),
                SequenceValue(SequenceName("a", "stats", "stats"), Variant::Root(4.2), 1500.0,
                              2000.0),
                SequenceValue(SequenceName("a", "raw", "stats", SequenceName::Flavors{"end"}),
                              Variant::Root(1.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "stats", SequenceName::Flavors{"end"}),
                              Variant::Root(2.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "stats", SequenceName::Flavors{"end"}),
                              Variant::Root(3.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "stats", "stats", SequenceName::Flavors{"end"}),
                              Variant::Root(4.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "raw", "cont"), Variant::Root(5.2), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "cont"), Variant::Root(6.2), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "cont"), Variant::Root(7.2), 1500.0,
                              2000.0),
                SequenceValue(SequenceName("a", "stats", "cont"), Variant::Root(8.2), 1500.0,
                              2000.0),
                SequenceValue(SequenceName("a", "raw", "cont", SequenceName::Flavors{"end"}),
                              Variant::Root(5.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "cont", SequenceName::Flavors{"end"}),
                              Variant::Root(6.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "cont", SequenceName::Flavors{"end"}),
                              Variant::Root(7.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "stats", "cont", SequenceName::Flavors{"end"}),
                              Variant::Root(8.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "raw", "full"), Variant::Root(9.2), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "full"), Variant::Root(10.2), 1500.0,
                              2000.0),
                SequenceValue(SequenceName("a", "cont", "full"), Variant::Root(11.2), 1500.0,
                              2000.0),
                SequenceValue(SequenceName("a", "stats", "full"), Variant::Root(12.2), 1500.0,
                              2000.0),
                SequenceValue(SequenceName("a", "raw", "full", SequenceName::Flavors{"end"}),
                              Variant::Root(9.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "avg", "full", SequenceName::Flavors{"end"}),
                              Variant::Root(10.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "cont", "full", SequenceName::Flavors{"end"}),
                              Variant::Root(11.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "stats", "full", SequenceName::Flavors{"end"}),
                              Variant::Root(12.3), 1500.0, 2000.0)}) << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "clean_meta", "stats"), meta, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avgh_meta", "stats"),
                              cms("Test", "Value", "Result2"), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "clean", "stats"), Variant::Root(1.0), 1000.0,
                              1500.0),
                SequenceValue(SequenceName("a", "clean", "stats", SequenceName::Flavors{"end"}),
                              Variant::Root(1.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "clean", "stats"), Variant::Root(1.2), 1500.0,
                              2000.0),
                SequenceValue(SequenceName("a", "clean", "stats", SequenceName::Flavors{"end"}),
                              Variant::Root(1.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "avgh", "stats"), Variant::Root(2.0), 1000.0,
                              1500.0),
                SequenceValue(SequenceName("a", "avgh", "stats", SequenceName::Flavors{"end"}),
                              Variant::Root(2.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "avgh", "stats"), Variant::Root(2.2), 1500.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "stats", SequenceName::Flavors{"end"}),
                              Variant::Root(2.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "clean_meta", "cont"), meta5, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avgh_meta", "cont"),
                              cms("Test", "Value", "Result7"), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "clean", "cont"), Variant::Root(5.0), 1000.0,
                              1500.0),
                SequenceValue(SequenceName("a", "clean", "cont", SequenceName::Flavors{"end"}),
                              Variant::Root(5.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "clean", "cont"), Variant::Root(5.2), 1500.0,
                              2000.0),
                SequenceValue(SequenceName("a", "clean", "cont", SequenceName::Flavors{"end"}),
                              Variant::Root(5.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "avgh", "cont"), Variant::Root(7.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "cont", SequenceName::Flavors{"end"}),
                              Variant::Root(7.3), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "clean_meta", "full"), meta9, 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "avgh_meta", "full"),
                              cms("Test", "Value", "Result12"), 1000.0, 2000.0),
                SequenceValue(SequenceName("a", "clean", "full"), Variant::Root(9.0), 1000.0,
                              1500.0),
                SequenceValue(SequenceName("a", "clean", "full", SequenceName::Flavors{"end"}),
                              Variant::Root(9.1), 1000.0, 1500.0),
                SequenceValue(SequenceName("a", "clean", "full"), Variant::Root(9.2), 1500.0,
                              2000.0),
                SequenceValue(SequenceName("a", "clean", "full", SequenceName::Flavors{"end"}),
                              Variant::Root(9.3), 1500.0, 2000.0),
                SequenceValue(SequenceName("a", "avgh", "full"), Variant::Root(12.0), 1000.0,
                              2000.0),
                SequenceValue(SequenceName("a", "avgh", "full", SequenceName::Flavors{"end"}),
                              Variant::Root(12.3), 1000.0, 2000.0)});

        meta.write().setType(Variant::Type::MetadataReal);
        meta.write().metadata("Blarg").hash("Foo").setString("ASF");
        QTest::newRow("General transition") << Engine_FixedTime << (SequenceValue::Transfer{
                SequenceValue(SequenceName("a", "b", "c"), meta, 1000.0, 4000.0),
                SequenceValue(SequenceName("a", "b", "c"), Variant::Root(1.0), 1000.0, 1100.0),
                SequenceValue(SequenceName("a", "b", "d"), Variant::Root(2.0), 1000.0, 1100.0),
                SequenceValue(SequenceName("a", "b", "d"), Variant::Root(3.0), 2000.0, 2100.0)})
                                            << (SequenceValue::Transfer{
                                                    SequenceValue(SequenceName("a", "b", "c"),
                                                                  Variant::Root(1.0), 1000.0,
                                                                  2000.0), SequenceValue(
                                                            SequenceName("a", "b", "c",
                                                                         SequenceName::Flavors{
                                                                                 "cover"}),
                                                            Variant::Root(0.1), 1000.0, 2000.0),
                                                    SequenceValue(SequenceName("a", "b", "c",
                                                                               SequenceName::Flavors{
                                                                                       "stats"}),
                                                                  chd("Mean", 1.0), 1000.0, 2000.0),
                                                    SequenceValue(SequenceName("a", "b", "c"), meta,
                                                                  1100.0, 4000.0),
                                                    SequenceValue(SequenceName("a", "b", "d"),
                                                                  Variant::Root(2.0), 1000.0,
                                                                  2000.0), SequenceValue(
                                                            SequenceName("a", "b", "d",
                                                                         SequenceName::Flavors{
                                                                                 "cover"}),
                                                            Variant::Root(0.1), 1000.0, 2000.0),
                                                    SequenceValue(SequenceName("a", "b", "d",
                                                                               SequenceName::Flavors{
                                                                                       "stats"}),
                                                                  chd("Mean", 2.0), 1000.0, 2000.0),
                                                    SequenceValue(SequenceName("a", "b", "d"),
                                                                  Variant::Root(3.0), 2000.0,
                                                                  3000.0), SequenceValue(
                                                            SequenceName("a", "b", "d",
                                                                         SequenceName::Flavors{
                                                                                 "cover"}),
                                                            Variant::Root(0.1), 2000.0, 3000.0),
                                                    SequenceValue(SequenceName("a", "b", "d",
                                                                               SequenceName::Flavors{
                                                                                       "stats"}),
                                                                  chd("Mean", 3.0), 2000.0,
                                                                  3000.0)});
    }

    void advanceFixedTime()
    {
        TestEndpoint output;
        SmoothingEngine *e = new SmoothingEngineFixedTime(
                new DynamicTimeInterval::Constant(Time::Second, 1000, true));
        e->start();
        e->setEgress(&output);

        SequenceName u1("a", "b", "v1");
        SequenceName u2("a", "b", "v2");
        e->incomingData(SequenceValue(u1, Variant::Root(1.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2, Variant::Root(2.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2, Variant::Root(3.0), 1600.0, 1700.0));

        QTest::qSleep(50);
        QVERIFY(!output.hasValueAfter(u1, 1000.0));
        QVERIFY(!output.hasValueAfter(u2, 1000.0));

        ElapsedTimer to;
        to.start();

        e->incomingAdvance(2000.0);
        while (to.elapsed() < 10000.0) {
            if (output.hasValueAfter(u1, 1000.0) && output.hasValueAfter(u2, 1000.0))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(output.hasValueAfter(u1, 1000.0));
        QVERIFY(output.hasValueAfter(u2, 1000.0));

        e->incomingData(SequenceValue(u2, Variant::Root(4.0), 2000.0, 2100.0));
        QTest::qSleep(50);
        QVERIFY(!output.hasValueAfter(u2, 2000.0));
        e->incomingData(SequenceValue(u2, Variant::Root(5.0), 3000.0, 3100.0));
        e->incomingData(SequenceValue(u2, Variant::Root(6.0), 3100.0, 3200.0));
        e->incomingData(SequenceValue(u2, Variant::Root(7.0), 3200.0, 3300.0));

        to.start();
        while (to.elapsed() < 10000.0) {
            if (output.hasValueAfter(u2, 2000.0))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(output.hasValueAfter(u2, 2000.0));

        e->endData();
        QVERIFY(e->wait(30));
        delete e;
        QVERIFY(output.ended);

        QVERIFY(output.engineCompare(
                SequenceValue::Transfer{SequenceValue(u1, Variant::Root(1.0), 1000.0, 2000.0),
                                        SequenceValue(u1.withFlavor("cover"), Variant::Root(0.1),
                                                      1000.0, 2000.0),
                                        SequenceValue(u1.withFlavor("stats"), chd("Mean", 1.0),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2, Variant::Root(2.5), 1000.0, 2000.0),
                                        SequenceValue(u2.withFlavor("cover"), Variant::Root(0.2),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2.withFlavor("stats"), chd("Mean", 2.5),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2, Variant::Root(4.0), 2000.0, 3000.0),
                                        SequenceValue(u2.withFlavor("cover"), Variant::Root(0.1),
                                                      2000.0, 3000.0),
                                        SequenceValue(u2.withFlavor("stats"), chd("Mean", 4.0),
                                                      2000.0, 3000.0),
                                        SequenceValue(u2, Variant::Root(6.0), 3000.0, 4000.0),
                                        SequenceValue(u2.withFlavor("cover"), Variant::Root(0.3),
                                                      3000.0, 4000.0),
                                        SequenceValue(u2.withFlavor("stats"), chd("Mean", 6.0),
                                                      3000.0, 4000.0)}, Engine_FixedTime));
    }

    void advancedFixedTimeArray()
    {
        TestEndpoint output;
        SmoothingEngine *e = new SmoothingEngineFixedTime(
                new DynamicTimeInterval::Constant(Time::Second, 1000, true));
        e->start();
        e->setEgress(&output);

        SequenceName u1("a", "b", "v1");
        SequenceName u2("a", "b", "v2");
        e->incomingData(SequenceValue(u1, arr(Variant::Root(1.0)), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2, arr(Variant::Root(2.0)), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2, arr(Variant::Root(3.0)), 1600.0, 1700.0));

        QTest::qSleep(50);
        QVERIFY(!output.hasValueAfter(u1, 1000.0));
        QVERIFY(!output.hasValueAfter(u2, 1000.0));

        e->incomingData(SequenceValue(u2, arr(Variant::Root(4.0)), 2000.0, 2100.0));
        e->incomingData(SequenceValue(u2, arr(Variant::Root(5.0)), 3000.0, 3100.0));
        e->incomingData(SequenceValue(u2, arr(Variant::Root(6.0)), 3100.0, 3200.0));
        e->incomingData(SequenceValue(u2, arr(Variant::Root(7.0)), 3200.0, 3300.0));

        e->incomingAdvance(4500.0);

        ElapsedTimer to;
        to.start();
        while (to.elapsed() < 10000.0) {
            if (output.hasValueAfter(u1, 1000.0) && output.hasValueAfter(u2, 1000.0))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(output.hasValueAfter(u1, 1000.0));
        QVERIFY(output.hasValueAfter(u2, 1000.0));

        e->endData();
        QVERIFY(e->wait(30));
        delete e;
        QVERIFY(output.ended);

        QVERIFY(output.engineCompare(
                SequenceValue::Transfer{SequenceValue(u1, arr(Variant::Root(1.0)), 1000.0, 2000.0),
                                        SequenceValue(u1.withFlavor("cover"),
                                                      arr(Variant::Root(0.1)), 1000.0, 2000.0),
                                        SequenceValue(u1.withFlavor("stats"), arr(chd("Mean", 1.0)),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2, arr(Variant::Root(2.5)), 1000.0, 2000.0),
                                        SequenceValue(u2.withFlavor("cover"),
                                                      arr(Variant::Root(0.2)), 1000.0, 2000.0),
                                        SequenceValue(u2.withFlavor("stats"), arr(chd("Mean", 2.5)),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2, arr(Variant::Root(4.0)), 2000.0, 3000.0),
                                        SequenceValue(u2.withFlavor("cover"),
                                                      arr(Variant::Root(0.1)), 2000.0, 3000.0),
                                        SequenceValue(u2.withFlavor("stats"), arr(chd("Mean", 4.0)),
                                                      2000.0, 3000.0),
                                        SequenceValue(u2, arr(Variant::Root(6.0)), 3000.0, 4000.0),
                                        SequenceValue(u2.withFlavor("cover"),
                                                      arr(Variant::Root(0.3)), 3000.0, 4000.0),
                                        SequenceValue(u2.withFlavor("stats"), arr(chd("Mean", 6.0)),
                                                      3000.0, 4000.0)}, Engine_FixedTime));
    }

    void advanceFixedTimeRHExtrapolate()
    {
        TestEndpoint output;
        SmoothingEngine *e = new SmoothingEngineFixedTime(
                new DynamicTimeInterval::Constant(Time::Second, 1000, true));
        e->start();
        e->setEgress(&output);

        SequenceName u1("a", "b", "v1");
        SequenceName u1meta("a", "b_meta", "v1");
        SequenceName u2("a", "b", "v2");
        SequenceName u3("a", "b", "v3");
        SequenceName u4("a", "b", "v4");
        SequenceName u5("a", "b", "v5");
        SequenceName u5meta("a", "b_meta", "v5");

        Variant::Root meta;
        meta.write().setType(Variant::Type::MetadataReal);
        meta.write().metadata("Smoothing").hash("Mode").setString("RHExtrapolate");
        meta.write().metadata("Smoothing").hash("Parameters").hash("Tin").setString("v2");
        meta.write().metadata("Smoothing").hash("Parameters").hash("RHin").setString("v3");
        meta.write().metadata("Smoothing").hash("Parameters").hash("Tout").setString("v4");
        e->incomingData(SequenceValue(u1meta, meta, 1000.0, 4000.0));
        meta.write().setEmpty();
        meta.write().metadata("Smoothing").hash("Mode").setString("RHExtrapolate");
        meta.write().metadata("Smoothing").hash("Parameters").hash("Tin").setString("v2");
        meta.write().metadata("Smoothing").hash("Parameters").hash("RHin").setString("BAD1");
        meta.write().metadata("Smoothing").hash("Parameters").hash("Tout").setString("BAD2");
        e->incomingData(SequenceValue(u5meta, meta, 1000.0, 4000.0));

        e->incomingData(SequenceValue(u1, Variant::Root(15.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2, Variant::Root(20.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u3, Variant::Root(30.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u4, Variant::Root(40.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u5, Variant::Root(5.0), 1000.0, 1100.0));

        QTest::qSleep(50);
        QVERIFY(!output.hasValueAfter(u1, 1000.0));
        QVERIFY(!output.hasValueAfter(u2, 1000.0));
        QVERIFY(!output.hasValueAfter(u3, 1000.0));
        QVERIFY(!output.hasValueAfter(u4, 1000.0));

        ElapsedTimer to;
        to.start();
        e->incomingAdvance(2000.0);
        while (to.elapsed() < 10000.0) {
            if (output.hasValueAfter(u1, 1000.0) &&
                    output.hasValueAfter(u2, 1000.0) &&
                    output.hasValueAfter(u3, 1000.0) &&
                    output.hasValueAfter(u4, 1000.0))
                break;
            e->incomingAdvance(2000.0);
            QTest::qSleep(50);
        }
        QVERIFY(output.hasValueAfter(u1, 1000.0));
        QVERIFY(output.hasValueAfter(u2, 1000.0));
        QVERIFY(output.hasValueAfter(u3, 1000.0));
        QVERIFY(output.hasValueAfter(u4, 1000.0));

        e->incomingData(SequenceValue(u1, Variant::Root(16.0), 2000.0, 2100.0));
        e->incomingData(SequenceValue(u2, Variant::Root(21.0), 2000.0, 2100.0));
        e->incomingData(SequenceValue(u3, Variant::Root(31.0), 2000.0, 2100.0));
        e->incomingData(SequenceValue(u4, Variant::Root(41.0), 2000.0, 2100.0));
        QTest::qSleep(50);
        QVERIFY(!output.hasValueAfter(u1, 2000.0));
        QVERIFY(!output.hasValueAfter(u2, 2000.0));
        QVERIFY(!output.hasValueAfter(u3, 2000.0));
        QVERIFY(!output.hasValueAfter(u4, 2000.0));
        e->incomingData(SequenceValue(u2, Variant::Root(5.0), 3000.0, 3100.0));
        e->incomingData(SequenceValue(u2, Variant::Root(6.0), 3100.0, 3200.0));
        e->incomingData(SequenceValue(u2, Variant::Root(7.0), 3200.0, 3300.0));

        to.start();
        while (to.elapsed() < 10000.0) {
            if (output.hasValueAfter(u1, 2000.0) &&
                    output.hasValueAfter(u2, 2000.0) &&
                    output.hasValueAfter(u3, 2000.0) && output.hasValueAfter(u4, 2000.0))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(output.hasValueAfter(u1, 2000.0));
        QVERIFY(output.hasValueAfter(u2, 2000.0));
        QVERIFY(output.hasValueAfter(u3, 2000.0));
        QVERIFY(output.hasValueAfter(u4, 2000.0));

        e->endData();
        QVERIFY(e->wait(30));
        delete e;
        QVERIFY(output.ended);

        QVERIFY(output.engineCompare(SequenceValue::Transfer{
                                             SequenceValue(u1meta, cms("Smoothing", "Mode", "RHExtrapolate"), 1000.0, 4000.0),
                                             SequenceValue(u5meta, cms("Smoothing", "Mode", "RHExtrapolate"), 1000.0, 4000.0),

                                             SequenceValue(u1,
                                                           Variant::Root(Algorithms::Dewpoint::rhExtrapolate(20.0, 30.0, 40.0)),
                                                           1000.0, 2000.0),
                                             SequenceValue(u1.withFlavor("cover"), Variant::Root(0.1), 1000.0, 2000.0),
                                             SequenceValue(u1.withFlavor("stats"), chd("Mean", 15.0), 1000.0, 2000.0),
                                             SequenceValue(u2, Variant::Root(20.0), 1000.0, 2000.0),
                                             SequenceValue(u2.withFlavor("cover"), Variant::Root(0.1), 1000.0, 2000.0),
                                             SequenceValue(u2.withFlavor("stats"), chd("Mean", 20.0), 1000.0, 2000.0),
                                             SequenceValue(u3, Variant::Root(30.0), 1000.0, 2000.0),
                                             SequenceValue(u3.withFlavor("cover"), Variant::Root(0.1), 1000.0, 2000.0),
                                             SequenceValue(u3.withFlavor("stats"), chd("Mean", 30.0), 1000.0, 2000.0),
                                             SequenceValue(u4, Variant::Root(40.0), 1000.0, 2000.0),
                                             SequenceValue(u4.withFlavor("cover"), Variant::Root(0.1), 1000.0, 2000.0),
                                             SequenceValue(u4.withFlavor("stats"), chd("Mean", 40.0), 1000.0, 2000.0),
                                             SequenceValue(u5, Variant::Root(FP::undefined()), 1000.0, 2000.0),
                                             SequenceValue(u5.withFlavor("cover"), Variant::Root(0.1), 1000.0, 2000.0),
                                             SequenceValue(u5.withFlavor("stats"), chd("Mean", 5.0), 1000.0, 2000.0),

                                             SequenceValue(u1,
                                                           Variant::Root(Algorithms::Dewpoint::rhExtrapolate(21.0, 31.0, 41.0)),
                                                           2000.0, 3000.0),
                                             SequenceValue(u1.withFlavor("cover"), Variant::Root(0.1), 2000.0, 3000.0),
                                             SequenceValue(u1.withFlavor("stats"), chd("Mean", 16.0), 2000.0, 3000.0),
                                             SequenceValue(u2, Variant::Root(21.0), 2000.0, 3000.0),
                                             SequenceValue(u2.withFlavor("cover"), Variant::Root(0.1), 2000.0, 3000.0),
                                             SequenceValue(u2.withFlavor("stats"), chd("Mean", 21.0), 2000.0, 3000.0),
                                             SequenceValue(u3, Variant::Root(31.0), 2000.0, 3000.0),
                                             SequenceValue(u3.withFlavor("cover"), Variant::Root(0.1), 2000.0, 3000.0),
                                             SequenceValue(u3.withFlavor("stats"), chd("Mean", 31.0), 2000.0, 3000.0),
                                             SequenceValue(u4, Variant::Root(41.0), 2000.0, 3000.0),
                                             SequenceValue(u4.withFlavor("cover"), Variant::Root(0.1), 2000.0, 3000.0),
                                             SequenceValue(u4.withFlavor("stats"), chd("Mean", 41.0), 2000.0, 3000.0),
                                             SequenceValue(u5, Variant::Root(FP::undefined()), 2000.0, 3000.0),

                                             SequenceValue(u2, Variant::Root(6.0), 3000.0, 4000.0),
                                             SequenceValue(u2.withFlavor("cover"), Variant::Root(0.3), 3000.0, 4000.0),
                                             SequenceValue(u2.withFlavor("stats"), chd("Mean", 6.0), 3000.0, 4000.0),
                                             SequenceValue(u1, Variant::Root(FP::undefined()), 3000.0, 4000.0),
                                             SequenceValue(u5, Variant::Root(FP::undefined()), 3000.0, 4000.0)},
                                     Engine_FixedTime));
    }


    void advanceSegmentControlled()
    {
        TestEndpoint output;
        SmoothingEngine *e = new SmoothingEngineSegmentControlled(
                new DynamicSequenceSelection::Basic(SequenceName("a", "b", "seg")),
                new DynamicSequenceSelection::Basic(SequenceName("a", "b", "out")));
        e->start();
        e->setEgress(&output);

        SequenceName u1("a", "b", "v1");
        SequenceName u2("a", "b", "v2");
        SequenceName seg("a", "b", "seg");
        SequenceName out("a", "b", "out");
        e->incomingData(SequenceValue(seg, chd("Segment", 1.0), 1000.0, 2000.0));
        e->incomingData(SequenceValue(u1, Variant::Root(1.0), 1100.0, 1200.0));
        e->incomingData(SequenceValue(u2, Variant::Root(2.0), 1200.0, 1300.0));
        e->incomingData(SequenceValue(u2, Variant::Root(3.0), 1600.0, 1700.0));

        QTest::qSleep(50);
        QVERIFY(!output.hasValueAfter(u1, 1000.0));
        QVERIFY(!output.hasValueAfter(u2, 1000.0));

        e->incomingData(SequenceValue(u2, Variant::Root(4.0), 2000.0, 2100.0));
        e->incomingData(SequenceValue(seg, chd("Segment", 2.0), 2000.0, 3000.0));
        QTest::qSleep(50);
        QVERIFY(!output.hasValueAfter(u2, 2000.0));
        e->incomingData(SequenceValue(seg, chd("Segment", 3.0), 3000.0, 4000.0));
        e->incomingData(SequenceValue(u2, Variant::Root(5.0), 3000.0, 3100.0));
        e->incomingData(SequenceValue(u2, Variant::Root(6.0), 3100.0, 3200.0));
        e->incomingData(SequenceValue(u2, Variant::Root(7.0), 3200.0, 3300.0));
        e->incomingData(SequenceValue(u2, Variant::Root(-1.0), 5000.0, 6000.0));

        ElapsedTimer to;
        to.start();
        while (to.elapsed() < 10000.0) {
            if (output.hasValueAfter(u2, 2000.0))
                break;
            e->incomingAdvance(6000.0);
            QTest::qSleep(50);
        }
        QVERIFY(output.hasValueAfter(u2, 2000.0));

        e->endData();
        QVERIFY(e->wait(30));
        delete e;
        QVERIFY(output.ended);

        QVERIFY(output.engineCompare(
                SequenceValue::Transfer{SequenceValue(seg, chd("Segment", 1.0), 1000.0, 2000.0),
                                        SequenceValue(out, chd("Segment", 1.0), 1000.0, 2000.0),
                                        SequenceValue(u1, Variant::Root(1.0), 1000.0, 2000.0),
                                        SequenceValue(u1.withFlavor("cover"), Variant::Root(0.1),
                                                      1000.0, 2000.0),
                                        SequenceValue(u1.withFlavor("stats"), chd("Mean", 1.0),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2, Variant::Root(2.5), 1000.0, 2000.0),
                                        SequenceValue(u2.withFlavor("cover"), Variant::Root(0.2),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2.withFlavor("stats"), chd("Mean", 2.5),
                                                      1000.0, 2000.0),
                                        SequenceValue(seg, chd("Segment", 2.0), 2000.0, 3000.0),
                                        SequenceValue(out, chd("Segment", 2.0), 2000.0, 3000.0),
                                        SequenceValue(u2, Variant::Root(4.0), 2000.0, 3000.0),
                                        SequenceValue(u2.withFlavor("cover"), Variant::Root(0.1),
                                                      2000.0, 3000.0),
                                        SequenceValue(u2.withFlavor("stats"), chd("Mean", 4.0),
                                                      2000.0, 3000.0),
                                        SequenceValue(seg, chd("Segment", 3.0), 3000.0, 4000.0),
                                        SequenceValue(out, chd("Segment", 3.0), 3000.0, 4000.0),
                                        SequenceValue(u2, Variant::Root(6.0), 3000.0, 4000.0),
                                        SequenceValue(u2.withFlavor("cover"), Variant::Root(0.3),
                                                      3000.0, 4000.0),
                                        SequenceValue(u2.withFlavor("stats"), chd("Mean", 6.0),
                                                      3000.0, 4000.0)}, Engine_SegmentControlled));
    }

    void advanceRealtime()
    {
        TestEndpoint output;
        SmoothingEngine *e = new RealtimeEngine;
        e->start();
        e->setEgress(&output);

        double tNow = Time::time();
        e->incomingData(
                SequenceValue(SequenceName("a", "b", "v1"), Variant::Root(1.0), 1000.0, 1100.0));
        e->incomingData(
                SequenceValue(SequenceName("a", "b", "v2"), Variant::Root(2.0), 1000.0, 1100.0));

        ElapsedTimer to;
        to.start();
        e->incomingAdvance(2000.0);
        while (to.elapsed() < 10000.0) {
            if (output.hasValueAfter(SequenceName("a", "rt_boxcar", "v2"), tNow))
                break;
            e->incomingData(SequenceValue(SequenceName("a", "b", "v2"), Variant::Root(2.0), 2000.0,
                                          2100.0));
            QTest::qSleep(50);
        }
        QVERIFY(output.hasValueAfter(SequenceName("a", "rt_boxcar", "v2"), tNow));

        e->endData();
        QVERIFY(e->wait(30));
        delete e;
        QVERIFY(output.ended);
    }

    void advanceEditingFull()
    {
        TestEndpoint output;
        SmoothingEngine *e = new EditingEngineFullCalculate(
                new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL,
                new SequenceMatch::Composite(SequenceMatch::Element::variable("v[12]")));
        e->start();
        e->setEgress(&output);

        SequenceName u1("a", "raw", "v1");
        SequenceName u2("a", "raw", "v2");
        SequenceName u1a("a", "avg", "v1");
        SequenceName u1c("a", "cont", "v1");
        SequenceName u2a("a", "avg", "v2");
        SequenceName u2c("a", "cont", "v2");
        e->incomingData(SequenceValue(u1, Variant::Root(1.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u1a, Variant::Root(1.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2, Variant::Root(2.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2a, Variant::Root(2.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2, Variant::Root(3.0), 1100.0, 1200.0));
        e->incomingData(SequenceValue(u2a, Variant::Root(3.0), 1100.0, 1200.0));

        QTest::qSleep(50);
        QVERIFY(!output.hasValueAfter(u1a, 1000.0));
        QVERIFY(!output.hasValueAfter(u2a, 1000.0));

        ElapsedTimer to;
        to.start();
        e->incomingAdvance(2000.0);
        while (to.elapsed() < 10000.0) {
            if (output.hasValueAfter(u1a, 1000.0) && output.hasValueAfter(u2a, 1000.0))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(output.hasValueAfter(u1a, 1000.0));
        QVERIFY(output.hasValueAfter(u2a, 1000.0));

        e->incomingData(SequenceValue(u2, Variant::Root(4.0), 2000.0, 2100.0));
        e->incomingData(SequenceValue(u2a, Variant::Root(4.0), 2000.0, 2100.0));
        QTest::qSleep(50);
        QVERIFY(!output.hasValueAfter(u2a, 2000.0));
        e->incomingData(SequenceValue(u2a, Variant::Root(5.0), 3000.0, 3250.0));
        e->incomingData(SequenceValue(u2, Variant::Root(5.0), 3000.0, 3250.0));
        e->incomingData(SequenceValue(u2a, Variant::Root(6.0), 3250.0, 3500.0));
        e->incomingData(SequenceValue(u2, Variant::Root(6.0), 3250.0, 3500.0));
        e->incomingData(SequenceValue(u2a, Variant::Root(7.0), 3500.0, 3750.0));
        e->incomingData(SequenceValue(u2, Variant::Root(7.0), 3500.0, 3750.0));
        e->incomingData(SequenceValue(u2a, Variant::Root(8.0), 3750.0, 4000.0));
        e->incomingData(SequenceValue(u2, Variant::Root(8.0), 3750.0, 4000.0));

        to.start();
        while (to.elapsed() < 10000.0) {
            if (output.hasValueAfter(u2a, 2000.0))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(output.hasValueAfter(u2a, 2000.0));

        e->endData();
        QVERIFY(e->wait(30));
        delete e;
        QVERIFY(output.ended);

        QVERIFY(output.engineCompare(
                SequenceValue::Transfer{SequenceValue(u1, Variant::Root(1.0), 1000.0, 1100.0),
                                        SequenceValue(u2, Variant::Root(2.0), 1000.0, 1100.0),
                                        SequenceValue(u2, Variant::Root(3.0), 1100.0, 1200.0),
                                        SequenceValue(u2, Variant::Root(4.0), 2000.0, 2100.0),
                                        SequenceValue(u2, Variant::Root(5.0), 3000.0, 3250.0),
                                        SequenceValue(u2, Variant::Root(6.0), 3250.0, 3500.0),
                                        SequenceValue(u2, Variant::Root(7.0), 3500.0, 3750.0),
                                        SequenceValue(u2, Variant::Root(8.0), 3750.0, 4000.0),
                                        SequenceValue(u1a, Variant::Root(1.0), 1000.0, 2000.0),
                                        SequenceValue(u1a.withFlavor("cover"), Variant::Root(0.1),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2a, Variant::Root(2.5), 1000.0, 2000.0),
                                        SequenceValue(u2a.withFlavor("cover"), Variant::Root(0.2),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2a, Variant::Root(4.0), 2000.0, 3000.0),
                                        SequenceValue(u2a.withFlavor("cover"), Variant::Root(0.1),
                                                      2000.0, 3000.0),
                                        SequenceValue(u2a, Variant::Root(6.5), 3000.0, 4000.0),
                                        SequenceValue(u1c, Variant::Root(1.0), 1000.0, 1100.0),
                                        SequenceValue(u2c, Variant::Root(2.5), 1000.0, 1200.0),
                                        SequenceValue(u2c, Variant::Root(4.0), 2000.0, 2100.0),
                                        SequenceValue(u2c, Variant::Root(6.5), 3000.0, 4000.0)},
                Engine_Editing_FullCalculate));
    }

    void advanceEditingContinuous()
    {
        TestEndpoint output;
        SmoothingEngine *e = new EditingEngineContinuousRecalculate(
                new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL,
                new SequenceMatch::Composite(SequenceMatch::Element::variable("v[12]")));
        e->start();
        e->setEgress(&output);

        SequenceName u1("a", "raw", "v1");
        SequenceName u2("a", "raw", "v2");
        SequenceName u1a("a", "avg", "v1");
        SequenceName u1c("a", "cont", "v1");
        SequenceName u2a("a", "avg", "v2");
        SequenceName u2c("a", "cont", "v2");
        e->incomingData(SequenceValue(u1, Variant::Root(1.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u1c, Variant::Root(1.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2, Variant::Root(2.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2c, Variant::Root(2.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2, Variant::Root(3.0), 1100.0, 1200.0));
        e->incomingData(SequenceValue(u2c, Variant::Root(3.0), 1100.0, 1200.0));

        QTest::qSleep(50);
        QVERIFY(!output.hasValueAfter(u1a, 1000.0));
        QVERIFY(!output.hasValueAfter(u2a, 1000.0));

        ElapsedTimer to;
        to.start();
        e->incomingAdvance(2000.0);
        while (to.elapsed() < 10000.0) {
            if (output.hasValueAfter(u1c, 1000.0) && output.hasValueAfter(u2c, 1000.0))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(output.hasValueAfter(u1c, 1000.0));
        QVERIFY(output.hasValueAfter(u2c, 1000.0));

        e->incomingData(SequenceValue(u2, Variant::Root(4.0), 2000.0, 2100.0));
        e->incomingData(SequenceValue(u2c, Variant::Root(4.0), 2000.0, 2100.0));
        QTest::qSleep(50);
        QVERIFY(!output.hasValueAfter(u2c, 2000.0));
        e->incomingData(SequenceValue(u2c, Variant::Root(5.0), 3000.0, 3100.0));
        e->incomingData(SequenceValue(u2, Variant::Root(5.0), 3000.0, 3100.0));
        e->incomingData(SequenceValue(u2c, Variant::Root(6.0), 3100.0, 3200.0));
        e->incomingData(SequenceValue(u2, Variant::Root(6.0), 3100.0, 3200.0));
        e->incomingData(SequenceValue(u2c, Variant::Root(7.0), 3200.0, 3300.0));
        e->incomingData(SequenceValue(u2, Variant::Root(7.0), 3200.0, 3300.0));

        to.start();
        while (to.elapsed() < 10000.0) {
            if (output.hasValueAfter(u2a, 2000.0))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(output.hasValueAfter(u2a, 2000.0));

        e->endData();
        QVERIFY(e->wait(30));
        delete e;
        QVERIFY(output.ended);

        QVERIFY(output.engineCompare(
                SequenceValue::Transfer{SequenceValue(u1, Variant::Root(1.0), 1000.0, 1100.0),
                                        SequenceValue(u2, Variant::Root(2.0), 1000.0, 1100.0),
                                        SequenceValue(u2, Variant::Root(3.0), 1100.0, 1200.0),
                                        SequenceValue(u2, Variant::Root(4.0), 2000.0, 2100.0),
                                        SequenceValue(u2, Variant::Root(5.0), 3000.0, 3100.0),
                                        SequenceValue(u2, Variant::Root(6.0), 3100.0, 3200.0),
                                        SequenceValue(u2, Variant::Root(7.0), 3200.0, 3300.0),
                                        SequenceValue(u1c, Variant::Root(1.0), 1000.0, 1100.0),
                                        SequenceValue(u2c, Variant::Root(2.0), 1000.0, 1100.0),
                                        SequenceValue(u2c, Variant::Root(3.0), 1100.0, 1200.0),
                                        SequenceValue(u2c, Variant::Root(4.0), 2000.0, 2100.0),
                                        SequenceValue(u2c, Variant::Root(5.0), 3000.0, 3100.0),
                                        SequenceValue(u2c, Variant::Root(6.0), 3100.0, 3200.0),
                                        SequenceValue(u2c, Variant::Root(7.0), 3200.0, 3300.0),
                                        SequenceValue(u1a, Variant::Root(1.0), 1000.0, 2000.0),
                                        SequenceValue(u1a.withFlavor("cover"), Variant::Root(0.1),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2a, Variant::Root(2.5), 1000.0, 2000.0),
                                        SequenceValue(u2a.withFlavor("cover"), Variant::Root(0.2),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2a, Variant::Root(4.0), 2000.0, 3000.0),
                                        SequenceValue(u2a.withFlavor("cover"), Variant::Root(0.1),
                                                      2000.0, 3000.0),
                                        SequenceValue(u2a, Variant::Root(6.0), 3000.0, 4000.0),
                                        SequenceValue(u2a.withFlavor("cover"), Variant::Root(0.3),
                                                      3000.0, 4000.0)},
                Engine_Editing_FullCalculate));
    }

    void advanceEditingFinal()
    {
        TestEndpoint output;
        SmoothingEngine *e =
                new EditingEngineFinal(new DynamicTimeInterval::Constant(Time::Second, 1000, true),
                                       NULL, new SequenceMatch::Composite(
                                SequenceMatch::Element::variable("v1")),
                                       new SequenceMatch::Composite(
                                               SequenceMatch::Element::variable("v2")));
        e->start();
        e->setEgress(&output);

        SequenceName u1("a", "raw", "v1");
        SequenceName u1a("a", "avg", "v1");
        SequenceName u1c("a", "cont", "v1");
        SequenceName u1s("a", "stats", "v1");
        SequenceName u1oc("a", "clean", "v1");
        SequenceName u1oa("a", "avgh", "v1");

        SequenceName u2("a", "raw", "v2");
        SequenceName u2a("a", "avg", "v2");
        SequenceName u2c("a", "cont", "v2");
        SequenceName u2s("a", "stats", "v2");
        SequenceName u2oc("a", "clean", "v2");
        SequenceName u2oa("a", "avgh", "v2");

        SequenceName u3("a", "raw", "v3");
        SequenceName u3a("a", "avg", "v3");
        SequenceName u3c("a", "cont", "v3");
        SequenceName u3s("a", "stats", "v3");
        SequenceName u3oc("a", "clean", "v3");
        SequenceName u3oa("a", "avgh", "v3");

        e->incomingData(SequenceValue(u1, Variant::Root(1.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u1c, Variant::Root(1.1), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u1a, Variant::Root(1.2), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u1s, Variant::Root(1.3), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2, Variant::Root(2.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2c, Variant::Root(2.1), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2a, Variant::Root(2.2), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u2s, Variant::Root(2.3), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u3, Variant::Root(3.0), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u3c, Variant::Root(3.1), 1000.0, 1100.0));
        e->incomingData(SequenceValue(u3a, Variant::Root(3.2), 1000.0, 2000.0));
        e->incomingData(SequenceValue(u3s, Variant::Root(3.3), 1000.0, 2000.0));
        e->incomingData(SequenceValue(u2, Variant::Root(2.5), 1100.0, 1200.0));

        QTest::qSleep(50);
        QVERIFY(!output.hasValueAfter(u1oa, 1000.0));
        QVERIFY(!output.hasValueAfter(u2oa, 1000.0));

        ElapsedTimer to;
        to.start();
        e->incomingAdvance(2000.0);
        while (to.elapsed() < 10000.0) {
            if (output.hasValueAfter(u1oa, 1000.0) && output.hasValueAfter(u2oa, 1000.0))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(output.hasValueAfter(u1oa, 1000.0));
        QVERIFY(output.hasValueAfter(u2oa, 1000.0));

        e->incomingData(SequenceValue(u2s, Variant::Root(4.0), 2000.0, 2100.0));
        QTest::qSleep(50);
        QVERIFY(!output.hasValueAfter(u2oa, 2000.0));
        /* Chain construction here means that we need to process all the values for the
         * advance to propagate, and we won't process a single isolated one.  So we send them
         * in bulk to make sure they all go through, regardless of the scheduling. */
        e->incomingData(
                SequenceValue::Transfer{SequenceValue(u2c, Variant::Root(5.0), 3000.0, 3100.0),
                                        SequenceValue(u2s, Variant::Root(6.0), 3100.0, 3200.0),
                                        SequenceValue(u2s, Variant::Root(7.0), 3200.0, 3300.0),
                                        SequenceValue(u2, Variant::Root(8.0), 4100.0, 5000.0)});

        to.start();
        while (to.elapsed() < 10000.0) {
            if (output.hasValueAfter(u2oa, 2000.0))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(output.hasValueAfter(u2oa, 2000.0));

        e->endData();
        QVERIFY(e->wait(30));
        delete e;
        QVERIFY(output.ended);

        QVERIFY(output.engineCompare(
                SequenceValue::Transfer{SequenceValue(u1oc, Variant::Root(1.0), 1000.0, 1100.0),
                                        SequenceValue(u1oa, Variant::Root(1.1), 1000.0, 2000.0),
                                        SequenceValue(u1oa.withFlavor("cover"), Variant::Root(0.1),
                                                      1000.0, 2000.0),
                                        SequenceValue(u1oa.withFlavor("stats"), chd("Mean", 1.3),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2oc, Variant::Root(2.0), 1000.0, 1100.0),
                                        SequenceValue(u2oc, Variant::Root(2.5), 1100.0, 1200.0),
                                        SequenceValue(u2oa, Variant::Root(2.3), 1000.0, 2000.0),
                                        SequenceValue(u2oa.withFlavor("cover"), Variant::Root(0.1),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2oa.withFlavor("stats"), chd("Mean", 2.3),
                                                      1000.0, 2000.0),
                                        SequenceValue(u3oc, Variant::Root(3.0), 1000.0, 1100.0),
                                        SequenceValue(u3oa, Variant::Root(3.2), 1000.0, 2000.0),
                                        SequenceValue(u3oa.withFlavor("stats"), chd("Mean", 3.3),
                                                      1000.0, 2000.0),
                                        SequenceValue(u2oa, Variant::Root(4.0), 2000.0, 3000.0),
                                        SequenceValue(u2oa.withFlavor("cover"), Variant::Root(0.1),
                                                      2000.0, 3000.0),
                                        SequenceValue(u2oa.withFlavor("stats"), chd("Mean", 4.0),
                                                      2000.0, 3000.0),
                                        SequenceValue(u2oa, Variant::Root(6.5), 3000.0, 4000.0),
                                        SequenceValue(u2oa.withFlavor("cover"), Variant::Root(0.2),
                                                      3000.0, 4000.0),
                                        SequenceValue(u2oa.withFlavor("stats"), chd("Mean", 6.5),
                                                      3000.0, 4000.0),
                                        SequenceValue(u2oc, Variant::Root(8.0), 4100.0, 5000.0)},
                Engine_Editing_Final));
    }

};

QTEST_MAIN(TestSmoothingEngine)

#include "smoothingengine.moc"
