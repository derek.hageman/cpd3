/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>

#include "smoothing/baseline.hxx"
#include "core/number.hxx"
#include "datacore/segment.hxx"

using namespace CPD3;
using namespace CPD3::Smoothing;
using namespace CPD3::Data;

class TestBaseline : public QObject {
Q_OBJECT
private slots:

    void relativeBand()
    {
        QFETCH(double, smoothed);
        QFETCH(double, check);
        QFETCH(double, band);
        QFETCH(bool, expected);

        QCOMPARE(BaselineSmoother::inRelativeBand(smoothed, check, band), expected);
    }

    void relativeBand_data()
    {
        QTest::addColumn<double>("smoothed");
        QTest::addColumn<double>("check");
        QTest::addColumn<double>("band");
        QTest::addColumn<bool>("expected");

        QTest::newRow("Twice inside") << 10.0 << 15.0 << 1.0 << true;
        QTest::newRow("Twice outside") << 10.0 << 25.0 << 1.0 << false;
        QTest::newRow("Half inside") << 10.0 << 7.0 << 1.0 << true;
        QTest::newRow("Half outside") << 10.0 << 4.0 << 1.0 << false;
        QTest::newRow("Negative twice inside") << 10.0 << 12.0 << -1.0 << true;
        QTest::newRow("Negative twice outside") << 10.0 << 25.0 << -1.0 << false;
    }

    void singlePoint()
    {
        BaselineSmoother *i = new BaselineSinglePoint;
        QVERIFY(!i->ready());
        i->add(FP::undefined());
        QVERIFY(!i->ready());
        i->add(1.0);
        QVERIFY(i->ready());
        QVERIFY(i->stable());
        QVERIFY(!i->spike());
        QCOMPARE(i->value(), 1.0);
        i->reset();
        QVERIFY(!i->ready());
        i->add(1.0);

        BaselineSmoother *j = i->clone();
        delete i;
        QVERIFY(j->ready());
        QVERIFY(j->stable());
        QVERIFY(!j->spike());
        QCOMPARE(j->value(), 1.0);
        BaselineSmoother *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QVERIFY(k->ready());
        QVERIFY(k->stable());
        QVERIFY(!k->spike());
        QCOMPARE(k->value(), 1.0);
        delete k;
    }

    void latest()
    {
        BaselineSmoother *i = new BaselineLatest;
        QVERIFY(!i->ready());
        i->add(FP::undefined());
        QVERIFY(!i->ready());
        i->add(1.0);
        QVERIFY(i->ready());
        QVERIFY(i->stable());
        QVERIFY(!i->spike());
        QCOMPARE(i->value(), 1.0);
        i->reset();
        QVERIFY(!i->ready());
        i->add(1.0);
        i->add(FP::undefined());
        QVERIFY(!i->ready());
        QVERIFY(!FP::defined(i->value()));
        i->add(1.0);

        BaselineSmoother *j = i->clone();
        delete i;
        QVERIFY(j->ready());
        QVERIFY(j->stable());
        QVERIFY(!j->spike());
        QCOMPARE(j->value(), 1.0);
        BaselineSmoother *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QVERIFY(k->ready());
        QVERIFY(k->stable());
        QVERIFY(!k->spike());
        QCOMPARE(k->value(), 1.0);
        delete k;
    }

    void foreverBaseline()
    {
        BaselineSmoother *i = new BaselineForever;
        QVERIFY(!i->ready());
        i->add(FP::undefined());
        QVERIFY(!i->ready());
        i->add(1.0);
        QVERIFY(i->ready());
        QVERIFY(i->stable());
        QVERIFY(!i->spike());
        QCOMPARE(i->value(), 1.0);
        i->reset();
        QVERIFY(!i->ready());
        i->add(1.0);
        i->add(2.0);
        QCOMPARE(i->value(), 1.5);

        BaselineSmoother *j = i->clone();
        delete i;
        QVERIFY(j->ready());
        QVERIFY(j->stable());
        QVERIFY(!j->spike());
        QCOMPARE(j->value(), 1.5);
        BaselineSmoother *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QVERIFY(k->ready());
        QVERIFY(k->stable());
        QVERIFY(!k->spike());
        QCOMPARE(k->value(), 1.5);
        delete k;
    }

    void fixedTime()
    {
        BaselineSmoother *i = new BaselineFixedTime(30.0, 30.0, 0.5, 2.0);
        QVERIFY(!i->ready());
        i->add(FP::undefined(), 1.0);
        QVERIFY(!i->ready());
        i->add(1.0, 100.0);
        QVERIFY(!i->ready());
        i->add(2.0, 120.0);
        QVERIFY(!i->ready());
        i->add(1.0, 130.0);
        QVERIFY(i->ready());
        QVERIFY(i->stable());
        QVERIFY(!i->spike());
        QCOMPARE(i->value(), 4.0 / 3.0);
        i->reset();
        QVERIFY(!i->ready());
        i->add(1.0, 100.0);
        i->add(1.0, 120.0);
        i->add(1.0, 125.0);
        QVERIFY(!i->ready());
        i->add(50.0, 130.0);
        QVERIFY(i->ready());
        QVERIFY(!i->stable());
        QVERIFY(i->spike());
        QCOMPARE(i->value(), 53.0 / 4.0);

        BaselineSmoother *j = i->clone();
        delete i;
        QVERIFY(j->ready());
        QVERIFY(!j->stable());
        QVERIFY(j->spike());
        QCOMPARE(j->value(), 53.0 / 4.0);
        BaselineSmoother *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QVERIFY(k->ready());
        QVERIFY(!k->stable());
        QVERIFY(k->spike());
        QCOMPARE(k->value(), 53.0 / 4.0);
        delete k;
    }

    void composite()
    {
        BaselineComposite *i = new BaselineComposite;
        i->overlay(new BaselineSinglePoint, FP::undefined(), FP::undefined());
        i->overlay(new BaselineSinglePoint, 200.0, FP::undefined());
        QVERIFY(!i->ready());
        i->add(1.0, 100.0);
        QVERIFY(i->ready());
        i->add(2.0, 150.0);
        QVERIFY(i->ready());
        QCOMPARE(i->value(), 2.0);
        i->add(FP::undefined(), 200.0);
        QVERIFY(!i->ready());
        i->add(3.0, 250.0);
        QCOMPARE(i->value(), 3.0);
        i->reset();
        QVERIFY(!i->ready());
        i->add(1.0, 100.0);
        QVERIFY(i->ready());
        i->add(2.0, 150.0);
        QVERIFY(i->ready());
        QCOMPARE(i->value(), 2.0);
        i->add(FP::undefined(), 200.0);
        QVERIFY(!i->ready());
        i->add(3.0, 250.0);
        QCOMPARE(i->value(), 3.0);

        BaselineSmoother *j = i->clone();
        delete i;
        QVERIFY(j->ready());
        QCOMPARE(j->value(), 3.0);
        j->reset();
        QVERIFY(!j->ready());
        j->add(1.0, 100.0);
        QVERIFY(j->ready());
        j->add(2.0, 150.0);
        BaselineSmoother *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QVERIFY(k->ready());
        QCOMPARE(k->value(), 2.0);
        delete k;
    }

    void fromConfiguration()
    {
        Variant::Root invalid;
        invalid["Type"].setString("Invalid");
        Variant::Root singlePoint;
        singlePoint["Type"].setString("SinglePoint");

        ValueSegment::Transfer config;
        config.emplace_back(FP::undefined(), FP::undefined(), singlePoint);
        BaselineSmoother *smoother = BaselineSmoother::fromConfiguration(invalid, config);
        QVERIFY(smoother != NULL);

        smoother->add(1.0);
        smoother->add(2.0);

        QCOMPARE(smoother->value(), 2.0);

        delete smoother;
    }
};

QTEST_APPLESS_MAIN(TestBaseline)

#include "baseline.moc"
