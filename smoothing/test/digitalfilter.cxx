/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>
#include <QVector>

#include "smoothing/digitalfilter.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "datacore/dynamictimeinterval.hxx"

using namespace CPD3;
using namespace CPD3::Smoothing;
using namespace CPD3::Data;

template<typename T>
void serializeTest(T &io)
{
    QByteArray data;
    {
        QDataStream stream(&data, QIODevice::WriteOnly);
        stream << io;
    }
    {
        QDataStream stream(&data, QIODevice::ReadOnly);
        io = T(stream);
    }
}

class TestDigitalFilter : public QObject {
Q_OBJECT

private slots:

    void singlePoleLowPass()
    {
        DigitalFilterSinglePoleLowPass filter(new DynamicTimeInterval::Constant(Time::Second, 10),
                                              new DynamicTimeInterval::Constant(Time::Second, 100),
                                              true);

        QCOMPARE(filter.apply(100.0, 1.0), 1.0);
        QCOMPARE(filter.apply(110.0, 1.0), 1.0);
        QCOMPARE(filter.apply(120.0, 2.0), 1.63212055882856);
        QCOMPARE(filter.apply(140.0, 2.0), 1.95021293163214);
        QCOMPARE(filter.apply(250.0, 1.0), 1.0);
        QCOMPARE(filter.apply(260.0, 2.0), 1.63212055882856);
        QVERIFY(!FP::defined(filter.apply(270.0, FP::undefined())));
        QCOMPARE(filter.apply(280.0, 1.0), 1.0);
        QCOMPARE(filter.apply(290.0, 2.0), 1.63212055882856);

        filter = DigitalFilterSinglePoleLowPass(new DynamicTimeInterval::Constant(Time::Second, 10),
                                                new DynamicTimeInterval::Constant(Time::Second,
                                                                                  100), true);
        serializeTest(filter);
        QCOMPARE(filter.apply(100.0, 1.0), 1.0);
        serializeTest(filter);
        QCOMPARE(filter.apply(110.0, 1.0), 1.0);
        serializeTest(filter);
        QCOMPARE(filter.apply(120.0, 2.0), 1.63212055882856);
        serializeTest(filter);
        QCOMPARE(filter.apply(140.0, 2.0), 1.95021293163214);
        serializeTest(filter);
        QCOMPARE(filter.apply(250.0, 1.0), 1.0);
        serializeTest(filter);
        QCOMPARE(filter.apply(260.0, 2.0), 1.63212055882856);
        serializeTest(filter);
        QVERIFY(!FP::defined(filter.apply(270.0, FP::undefined())));
        serializeTest(filter);
        QCOMPARE(filter.apply(280.0, 1.0), 1.0);
        serializeTest(filter);
        QCOMPARE(filter.apply(290.0, 2.0), 1.63212055882856);
    }

    void fourPoleLowPass()
    {
        DigitalFilterFourPoleLowPass filter(new DynamicTimeInterval::Constant(Time::Second, 10),
                                            new DynamicTimeInterval::Constant(Time::Second, 100),
                                            true);

        QCOMPARE(filter.apply(100.0, 1.0), 1.0);
        QCOMPARE(filter.apply(110.0, 2.0), 1.15966130015119);
        QCOMPARE(filter.apply(120.0, 3.0), 1.55426703980767);
        QCOMPARE(filter.apply(130.0, 4.0), 2.16495085224301);
        QCOMPARE(filter.apply(140.0, 5.0), 2.93461602600494);
        QCOMPARE(filter.apply(150.0, 6.0), 3.80663165489949);
        QCOMPARE(filter.apply(300.0, 1.0), 1.0);
        QCOMPARE(filter.apply(310.0, 2.0), 1.15966130015119);
        QCOMPARE(filter.apply(320.0, 3.0), 1.55426703980767);
        QCOMPARE(filter.apply(330.0, 4.0), 2.16495085224301);
        QCOMPARE(filter.apply(340.0, 5.0), 2.93461602600494);
        QCOMPARE(filter.apply(350.0, 6.0), 3.80663165489949);
        QVERIFY(!FP::defined(filter.apply(360.0, FP::undefined())));
        QCOMPARE(filter.apply(370.0, 1.0), 1.0);
        QCOMPARE(filter.apply(380.0, 2.0), 1.15966130015119);
        QCOMPARE(filter.apply(390.0, 3.0), 1.55426703980767);
        QCOMPARE(filter.apply(400.0, 4.0), 2.16495085224301);
        QCOMPARE(filter.apply(410.0, 5.0), 2.93461602600494);
        QCOMPARE(filter.apply(420.0, 6.0), 3.80663165489949);

        filter = DigitalFilterFourPoleLowPass(new DynamicTimeInterval::Constant(Time::Second, 10),
                                              new DynamicTimeInterval::Constant(Time::Second, 100),
                                              true);
        serializeTest(filter);
        QCOMPARE(filter.apply(100.0, 1.0), 1.0);
        serializeTest(filter);
        QCOMPARE(filter.apply(110.0, 2.0), 1.15966130015119);
        serializeTest(filter);
        QCOMPARE(filter.apply(120.0, 3.0), 1.55426703980767);
        serializeTest(filter);
        QCOMPARE(filter.apply(130.0, 4.0), 2.16495085224301);
        serializeTest(filter);
        QCOMPARE(filter.apply(140.0, 5.0), 2.93461602600494);
        serializeTest(filter);
        QCOMPARE(filter.apply(150.0, 6.0), 3.80663165489949);
        serializeTest(filter);
        QCOMPARE(filter.apply(300.0, 1.0), 1.0);
        serializeTest(filter);
        QCOMPARE(filter.apply(310.0, 2.0), 1.15966130015119);
        serializeTest(filter);
        QCOMPARE(filter.apply(320.0, 3.0), 1.55426703980767);
        serializeTest(filter);
        QCOMPARE(filter.apply(330.0, 4.0), 2.16495085224301);
        serializeTest(filter);
        QCOMPARE(filter.apply(340.0, 5.0), 2.93461602600494);
        serializeTest(filter);
        QCOMPARE(filter.apply(350.0, 6.0), 3.80663165489949);
        serializeTest(filter);
        QVERIFY(!FP::defined(filter.apply(360.0, FP::undefined())));
        serializeTest(filter);
        QCOMPARE(filter.apply(370.0, 1.0), 1.0);
        serializeTest(filter);
        QCOMPARE(filter.apply(380.0, 2.0), 1.15966130015119);
        serializeTest(filter);
        QCOMPARE(filter.apply(390.0, 3.0), 1.55426703980767);
        serializeTest(filter);
        QCOMPARE(filter.apply(400.0, 4.0), 2.16495085224301);
        serializeTest(filter);
        QCOMPARE(filter.apply(410.0, 5.0), 2.93461602600494);
    }

    void general()
    {
        /* Single pole high pass, TC = 1 */
        DigitalFilterGeneral filter(QVector<double>() << 0.683939720585721 << -0.683939720585721,
                                    QVector<double>() << 0.367879441171442,
                                    new DynamicTimeInterval::Constant(Time::Second, 100), true);
        QCOMPARE(filter.apply(100.0, 1.0), 1.0);
        QCOMPARE(filter.apply(110.0, 2.0), 1.05181916175716);
        QCOMPARE(filter.apply(120.0, 1.0), -0.296997075145081);

        filter = DigitalFilterGeneral(QVector<double>() << 0.683939720585721 << -0.683939720585721,
                                      QVector<double>() << 0.367879441171442,
                                      new DynamicTimeInterval::Constant(Time::Second, 100), true);
        serializeTest(filter);
        QCOMPARE(filter.apply(100.0, 1.0), 1.0);
        serializeTest(filter);
        QCOMPARE(filter.apply(110.0, 2.0), 1.05181916175716);
        serializeTest(filter);
        QCOMPARE(filter.apply(120.0, 1.0), -0.296997075145081);
    }
};

QTEST_APPLESS_MAIN(TestDigitalFilter)

#include "digitalfilter.moc"
