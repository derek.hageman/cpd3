/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>
#include <QVector>

#include "smoothing/fixedtime.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "datacore/dynamictimeinterval.hxx"

using namespace CPD3;
using namespace CPD3::Smoothing;
using namespace CPD3::Data;

template<int N>
struct BinData {
    double start;
    double end;
    QVector<double> values[N];
    QVector<double> durations;

    BinData() : start(FP::undefined()), end(FP::undefined()), values(), durations()
    {
    }

    BinData(double s, double e, const QVector<double> v[N], const QVector<double> &d) : start(s),
                                                                                        end(e),
                                                                                        values(),
                                                                                        durations(d)
    {
        for (int i = 0; i < N; i++) {
            values[i] = v[i];
        }
    }

    BinData(const BinData<N> &other) : start(other.start),
                                       end(other.end),
                                       values(),
                                       durations(other.durations)
    {
        for (int i = 0; i < N; i++) {
            values[i] = other.values[i];
        }
    }

    BinData<N> &operator=(const BinData<N> &other)
    {
        if (&other == this) return *this;
        start = other.start;
        end = other.end;
        durations = other.durations;
        for (int i = 0; i < N; i++) {
            values[i] = other.values[i];
        }
        return *this;
    }

    bool operator==(const BinData<N> &rhs) const
    {
        for (int i = 0; i < N; i++) {
            if (values[i].size() != rhs.values[i].size())
                return false;
            for (int j = 0; j < values[i].size(); j++) {
                if (!FP::equal(values[i].at(j), rhs.values[i].at(j)))
                    return false;
            }
        }
        if (durations.size() != rhs.durations.size())
            return false;
        for (int j = 0; j < durations.size(); j++) {
            if (!FP::equal(durations.at(j), rhs.durations.at(j)))
                return false;
        }
        return FP::equal(start, rhs.start) && FP::equal(end, rhs.end);
    }

    bool operator!=(const BinData<N> &rhs) const
    { return !(*this == rhs); }
};

template<int N>
static char *testToString(const QList<BinData<N> > &output)
{
    QByteArray ba;
    for (int i = 0, max = output.size(); i < max; i++) {
        if (i != 0) ba += ", ";

        ba += "(";
        if (FP::defined(output.at(i).start))
            ba += QByteArray::number(output.at(i).start);
        else
            ba += "-Infinity";
        ba += ",";
        if (FP::defined(output.at(i).end))
            ba += QByteArray::number(output.at(i).end);
        else
            ba += "+Infinity";
        ba += "={";
        for (int j = 0; j < N; j++) {
            if (j != 0) ba += ",";
            ba += "[";
            for (int k = 0; k < output.at(i).values[j].size(); k++) {
                if (k != 0) ba += ",";
                if (FP::defined(output.at(i).values[j].at(k)))
                    ba += QByteArray::number(output.at(i).values[j].at(k));
                else
                    ba += "Undefined";
            }
            ba += "]";
        }
        ba += ",[";
        for (int k = 0; k < output.at(i).durations.size(); k++) {
            if (k != 0) ba += ",";
            if (FP::defined(output.at(i).durations.at(k)))
                ba += QByteArray::number(output.at(i).durations.at(k));
            else
                ba += "Undefined";
        }
        ba += "]})";
    }
    return qstrdup(ba.data());
}

namespace QTest {
template<>
char *toString(const QList<BinData<2> > &output)
{ return testToString(output); }

template<>
char *toString(const QList<double> &output)
{
    QByteArray ba;
    for (int i = 0, max = output.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        if (FP::defined(output.at(i)))
            ba += QByteArray::number(output.at(i));
        else
            ba += "Undefined";
    }
    return qstrdup(ba.data());
}
}

template<int N>
struct TestEvent {
    enum Type {
        Advance, Value, End, Check,
    };
    Type type;
    int index;
    double start;
    double end;
    double value;
    QList<BinData<N> > checkData;
    QList<double> checkAdvance;

    TestEvent(Type t,
              int i,
              double s = FP::undefined(),
              double e = FP::undefined(),
              double v = FP::undefined()) : type(t),
                                            index(i),
                                            start(s),
                                            end(e),
                                            value(v),
                                            checkData(),
                                            checkAdvance()
    { }

    TestEvent(const QList<BinData<N> > &cd, const QList<double> &ca = QList<double>()) : type(
            Check),
                                                                                         index(-1),
                                                                                         start(FP::undefined()),
                                                                                         end(FP::undefined()),
                                                                                         value(FP::undefined()),
                                                                                         checkData(
                                                                                                 cd),
                                                                                         checkAdvance(
                                                                                                 ca)
    { }

    TestEvent() : type(Check),
                  index(-1),
                  start(FP::undefined()),
                  end(FP::undefined()),
                  value(FP::undefined()),
                  checkData(),
                  checkAdvance()
    { }
};

typedef TestEvent<2> TestEvent2;

Q_DECLARE_METATYPE(TestEvent2);

Q_DECLARE_METATYPE(QList<TestEvent2>);

typedef BinData<2> TestBinData2;

Q_DECLARE_METATYPE(TestBinData2);

Q_DECLARE_METATYPE(QList<TestBinData2>);

template<int N>
class TestBinner : public SmootherChainInputFixedTimeBinning<N> {
public:
    QList<BinData<N> > segments;
    QList<double> advances;
    bool ended;
    double lastTime;

    TestBinner(DynamicTimeInterval *interval, DynamicTimeInterval *gap)
            : SmootherChainInputFixedTimeBinning<N>(interval, gap),
              segments(),
              advances(),
              ended(false),
              lastTime(FP::undefined())
    { }

    TestBinner(QDataStream &stream) : SmootherChainInputFixedTimeBinning<N>(stream),
                                      segments(),
                                      advances(),
                                      ended(false),
                                      lastTime(FP::undefined())
    { }

    SmootherChainTarget *target(int index)
    { return SmootherChainInputFixedTimeBinning<N>::getTarget(index); }

protected:
    virtual void processBin(double start,
                            double end,
                            const QVector<double> values[N],
                            const QVector<double> &durations)
    {
        Q_ASSERT(Range::compareStart(start, lastTime) >= 0);
        lastTime = end;
        segments.append(BinData<N>(start, end, values, durations));
    }

    virtual void advanceBin(double time)
    {
        Q_ASSERT(Range::compareStart(time, lastTime) >= 0);
        lastTime = time;
        advances.append(time);
    }

    virtual void completedBinning()
    {
        Q_ASSERT(!ended);
        ended = true;
    }
};

class TestSimpleTarget : public SmootherChainTarget {
public:
    QVector<double> values;
    QVector<double> starts;
    QVector<double> ends;

    virtual void incomingData(double start, double end, double value)
    {
        Q_ASSERT(ends.isEmpty() || start >= ends.last());
        values.append(value);
        starts.append(start);
        ends.append(end);
    }

    virtual void incomingAdvance(double time)
    {
        Q_ASSERT(ends.isEmpty() || time >= ends.last());
    }

    virtual void endData()
    { }
};

class TestSimpleTargetFlags : public SmootherChainTargetFlags {
public:
    QVector<Variant::Flags> values;
    QVector<double> starts;
    QVector<double> ends;

    virtual void incomingData(double start, double end, const Variant::Flags &value)
    {
        Q_ASSERT(ends.isEmpty() || start >= ends.last());
        values.append(value);
        starts.append(start);
        ends.append(end);
    }

    virtual void incomingAdvance(double time)
    {
        Q_ASSERT(ends.isEmpty() || time >= ends.last());
    }

    virtual void endData()
    { }
};

class TestSimpleTargetGeneral : public SmootherChainTargetGeneral {
public:
    QVector<Variant::Read> values;
    QVector<double> starts;
    QVector<double> ends;

    virtual void incomingData(double start, double end, const Variant::Root &value)
    {
        values.append(value.read());
        starts.append(start);
        ends.append(end);
    }

    virtual void incomingAdvance(double time)
    {
        Q_ASSERT(ends.isEmpty() || time >= ends.last());
    }

    virtual void endData()
    { }
};

class TestFixedTime : public QObject {
Q_OBJECT

private slots:

    void binner2()
    {
        QFETCH(int, intervalMinutes);
        QFETCH(int, gapMinutes);
        QFETCH(QList<TestEvent2>, events);
        QFETCH(QList<TestBinData2>, result);

        DynamicTimeInterval *interval = NULL;
        DynamicTimeInterval *gap = NULL;

        if (intervalMinutes == 0) {
            interval = NULL;
        } else if (intervalMinutes < 0) {
            interval = new DynamicTimeInterval::Undefined;
        } else {
            interval = new DynamicTimeInterval::Constant(Time::Minute, intervalMinutes,
                                                         intervalMinutes % 2 == 0);
        }
        if (gapMinutes == 0) {
            gap = NULL;
        } else if (gapMinutes < 0) {
            gap = new DynamicTimeInterval::Undefined;
        } else {
            gap = new DynamicTimeInterval::Constant(Time::Minute, gapMinutes, gapMinutes % 2 == 0);
        }

        TestBinner<2> processor
                (interval != NULL ? interval->clone() : NULL, gap != NULL ? gap->clone() : NULL);
        bool ended[2];
        memset(ended, 0, sizeof(ended));

        for (QList<TestEvent2>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            switch (e->type) {
            case TestEvent2::Value:
                processor.target(e->index)->incomingData(e->start, e->end, e->value);
                break;
            case TestEvent2::Advance:
                processor.target(e->index)->incomingAdvance(e->start);
                break;
            case TestEvent2::End:
                processor.target(e->index)->endData();
                ended[e->index] = true;
                break;
            case TestEvent2::Check:
                QCOMPARE(processor.segments, e->checkData);
                QCOMPARE(processor.advances, e->checkAdvance);
                break;
            }
        }

        for (int i = 0; i < 2; i++) {
            if (ended[i]) continue;
            processor.target(i)->endData();
        }

        QVERIFY(processor.ended);
        QCOMPARE(processor.segments, result);


        /* Run it again with no advances to make sure it's consistent */
        memset(ended, 0, sizeof(ended));
        TestBinner<2> processor2
                (interval != NULL ? interval->clone() : NULL, gap != NULL ? gap->clone() : NULL);
        for (QList<TestEvent2>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            switch (e->type) {
            case TestEvent2::Value:
                processor2.target(e->index)->incomingData(e->start, e->end, e->value);
                break;
            case TestEvent2::End:
                processor2.target(e->index)->endData();
                ended[e->index] = true;
                break;
            default:
                break;
            }
        }
        for (int i = 0; i < 2; i++) {
            if (ended[i]) continue;
            processor2.target(i)->endData();
        }
        QVERIFY(processor2.ended);
        QCOMPARE(processor2.segments, result);


        /* And again serializing and de-serializing */
        TestBinner<2> *processor3 = new TestBinner<2>(interval != NULL ? interval->clone() : NULL,
                                                      gap != NULL ? gap->clone() : NULL);
        memset(ended, 0, sizeof(ended));
        for (QList<TestEvent2>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    processor3->serialize(stream);
                }
                QList<BinData<2> > segments(processor3->segments);
                QList<double> advances(processor3->advances);
                bool pended = processor3->ended;
                delete processor3;
                {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    processor3 = new TestBinner<2>(stream);
                }
                processor3->segments = segments;
                processor3->advances = advances;
                processor3->ended = pended;
            }

            switch (e->type) {
            case TestEvent2::Value:
                processor3->target(e->index)->incomingData(e->start, e->end, e->value);
                break;
            case TestEvent2::Advance:
                processor3->target(e->index)->incomingAdvance(e->start);
                break;
            case TestEvent2::End:
                processor3->target(e->index)->endData();
                ended[e->index] = true;
                break;
            case TestEvent2::Check:
                QCOMPARE(processor3->segments, e->checkData);
                QCOMPARE(processor3->advances, e->checkAdvance);
                break;
            }
        }
        for (int i = 0; i < 2; i++) {
            if (ended[i]) continue;
            processor3->target(i)->endData();
        }
        QVERIFY(processor3->ended);
        QCOMPARE(processor3->segments, result);
        delete processor3;

        if (interval != NULL)
            delete interval;
        if (gap != NULL)
            delete gap;
    }

    void binner2_data()
    {
        QTest::addColumn<int>("intervalMinutes");
        QTest::addColumn<int>("gapMinutes");
        QTest::addColumn<QList<TestEvent2> >("events");
        QTest::addColumn<QList<TestBinData2> >("result");

        QVector<double> v1[2];
        QVector<double> v2[2];
        QVector<double> v3[2];
        QVector<double> v4[2];

        QTest::newRow("Empty") << 0 << 0 << (QList<TestEvent2>()) << (QList<TestBinData2>());

        v1[0].append(1.0);
        v1[1].append(2.0);
        v2[0].append(FP::undefined());
        v2[1].append(2.1);
        v3[0].append(1.0);
        v3[0].append(1.1);
        v3[1].append(2.0);
        v3[1].append(2.1);
        v4[0].append(1.1);
        v4[1].append(2.1);

        QTest::newRow("Single All") <<
                0 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 200.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 100.0, 200.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 100.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(100.0, 200.0, v1, QVector<double>() << 100.0));
        QTest::newRow("Single All Invalid") <<
                -1 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 100.0, 200.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 100.0, 200.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 100.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(100.0, 200.0, v1, QVector<double>() << 100.0));
        QTest::newRow("Single Aligned") <<
                2 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 120.0, 240.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 120.0, 240.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 240.0, v1,
                                                        QVector<double>() << 120.0))) <<
                (QList<TestBinData2>() <<
                        TestBinData2(120.0, 240.0, v1, QVector<double>() << 120.0));
        QTest::newRow("Single Unaligned") <<
                1 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 55.0, 115.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 55.0, 115.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 115.0, v1,
                                                        QVector<double>() << 60.0))) <<
                (QList<TestBinData2>() << TestBinData2(55.0, 115.0, v1, QVector<double>() << 60.0));
        QTest::newRow("Single Unaligned Incomplete") <<
                1 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 55.0, 114.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 55.0, 114.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 55.0)) <<
                (QList<TestBinData2>() << TestBinData2(55.0, 115.0, v1, QVector<double>() << 59.0));
        QTest::newRow("Single Fragment Aligned") <<
                2 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 125.0, 245.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 125.0, 245.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 240.0, v1,
                                                        QVector<double>() << 115.0))) <<
                (QList<TestBinData2>() <<
                        TestBinData2(120.0, 240.0, v1, QVector<double>() << 115.0) <<
                        TestBinData2(240.0, 360.0, v1, QVector<double>() << 5.0));
        QTest::newRow("Single Extending Unaligned") <<
                1 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 55.0, 354.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 55.0, 354.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 295.0, v1,
                                                        QVector<double>() << 240.0))) <<
                (QList<TestBinData2>() <<
                        TestBinData2(55.0, 295.0, v1, QVector<double>() << 240.0) <<
                        TestBinData2(295.0, 355.0, v1, QVector<double>() << 59.0));
        QTest::newRow("Single Extending Aligned") <<
                2 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 125.0, 475.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 125.0, 475.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 360.0, v1,
                                                        QVector<double>() << 235.0))) <<
                (QList<TestBinData2>() <<
                        TestBinData2(120.0, 360.0, v1, QVector<double>() << 235.0) <<
                        TestBinData2(360.0, 480.0, v1, QVector<double>() << 115.0));
        QTest::newRow("Single Extending Aligned Advance") <<
                2 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 125.0, 475.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 125.0, 475.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 360.0, v1,
                                                        QVector<double>() << 235.0)) <<
                        TestEvent2(TestEvent2::Advance, 0, 480.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 480.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 360.0, v1,
                                                        QVector<double>() << 235.0) <<
                                           TestBinData2(360.0, 480.0, v1,
                                                        QVector<double>() << 115.0))) <<
                (QList<TestBinData2>() <<
                        TestBinData2(120.0, 360.0, v1, QVector<double>() << 235.0) <<
                        TestBinData2(360.0, 480.0, v1, QVector<double>() << 115.0));
        QTest::newRow("Single Advance Undefined") <<
                0 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Advance, 1, 100.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 100.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 100.0)) <<
                (QList<TestBinData2>());

        QTest::newRow("Multiple Exact Unaligned") <<
                1 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 55.0, 115.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 55.0, 115.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 115.0, v1,
                                                        QVector<double>() << 60.0)) <<
                        TestEvent2(TestEvent2::Value, 1, 115.0, 235.0, 2.1) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 115.0, v1,
                                                        QVector<double>() << 60.0)) <<
                        TestEvent2(TestEvent2::Advance, 0, 235.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 115.0, v1,
                                                        QVector<double>() << 60.0) <<
                                           TestBinData2(115.0, 235.0, v2,
                                                        QVector<double>() << 120.0),
                                   QList<double>() << 115.0) <<
                        TestEvent2(TestEvent2::Value, 1, 235.0, 295.0, 2.0) <<
                        TestEvent2(TestEvent2::Value, 0, 235.0, 295.0, 1.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 115.0, v1,
                                                        QVector<double>() << 60.0) <<
                                           TestBinData2(115.0, 235.0, v2,
                                                        QVector<double>() << 120.0) <<
                                           TestBinData2(235.0, 295.0, v1,
                                                        QVector<double>() << 60.0),
                                   QList<double>() << 115.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(55.0, 115.0, v1, QVector<double>() << 60.0) <<
                        TestBinData2(115.0, 235.0, v2, QVector<double>() << 120.0) <<
                        TestBinData2(235.0, 295.0, v1, QVector<double>() << 60.0));

        QTest::newRow("Multiple Exact Aligned") <<
                2 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 125.0, 185.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 125.0, 185.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 120.0) <<
                        TestEvent2(TestEvent2::Value, 1, 240.0, 355.0, 2.1) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 120.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 360.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 240.0, v1,
                                                        QVector<double>() << 60.0),
                                   QList<double>() << 120.0) <<
                        TestEvent2(TestEvent2::Value, 1, 365.0, 480.0, 2.0) <<
                        TestEvent2(TestEvent2::Value, 0, 365.0, 480.0, 1.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 240.0, v1,
                                                        QVector<double>() << 60.0) <<
                                           TestBinData2(240.0, 360.0, v2,
                                                        QVector<double>() << 115.0) <<
                                           TestBinData2(360.0, 480.0, v1,
                                                        QVector<double>() << 115.0),
                                   QList<double>() << 120.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(120.0, 240.0, v1, QVector<double>() << 60.0) <<
                        TestBinData2(240.0, 360.0, v2, QVector<double>() << 115.0) <<
                        TestBinData2(360.0, 480.0, v1, QVector<double>() << 115.0));
        QTest::newRow("Single Infinite") <<
                2 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, FP::undefined(), FP::undefined(), 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, FP::undefined(), FP::undefined(), 2.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(FP::undefined(), FP::undefined(), v1,
                                     QVector<double>() << FP::undefined()));
        QTest::newRow("Infinite Start Unaligned") <<
                1 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, FP::undefined(), 55.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, FP::undefined(), 55.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(FP::undefined(), 55.0, v1,
                                                        QVector<double>() << FP::undefined())) <<
                        TestEvent2(TestEvent2::Value, 1, 55.0, 115.0, 2.0) <<
                        TestEvent2(TestEvent2::Value, 0, 55.0, 115.0, 1.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(FP::undefined(), 55.0, v1,
                                     QVector<double>() << FP::undefined()) <<
                        TestBinData2(55.0, 115.0, v1, QVector<double>() << 60.0));
        QTest::newRow("Infinite Start Aligned") <<
                2 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, FP::undefined(), 175.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, FP::undefined(), 175.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(FP::undefined(), 120.0, v1,
                                                        QVector<double>() << FP::undefined()))) <<
                (QList<TestBinData2>() <<
                        TestBinData2(FP::undefined(), 120.0, v1,
                                     QVector<double>() << FP::undefined()) <<
                        TestBinData2(120.0, 240.0, v1, QVector<double>() << 55.0));
        QTest::newRow("Infinite End Unaligned") <<
                1 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 55.0, FP::undefined(), 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 55.0, FP::undefined(), 2.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(55.0, FP::undefined(), v1,
                                     QVector<double>() << FP::undefined()));
        QTest::newRow("Infinite End Aligned") <<
                2 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 125.0, FP::undefined(), 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 125.0, FP::undefined(), 2.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(120.0, FP::undefined(), v1,
                                     QVector<double>() << FP::undefined()));
        QTest::newRow("Contained Unaligned Composite") <<
                1 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 55.0, 75.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 55.0, 75.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 55.0) <<
                        TestEvent2(TestEvent2::Value, 1, 110.0, 115.0, 2.1) <<
                        TestEvent2(TestEvent2::Value, 0, 110.0, 115.0, 1.1) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 115.0, v3,
                                                        QVector<double>() << 20.0 << 5.0),
                                   QList<double>() << 55.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(55.0, 115.0, v3, QVector<double>() << 20.0 << 5.0));
        QTest::newRow("Contained Aligned Composite") <<
                2 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 125.0, 155.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 125.0, 155.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 120.0) <<
                        TestEvent2(TestEvent2::Value, 1, 210.0, 235.0, 2.1) <<
                        TestEvent2(TestEvent2::Value, 0, 210.0, 235.0, 1.1) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 120.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 240.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 240.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 240.0, v3,
                                                        QVector<double>() << 30.0 << 25.0),
                                   QList<double>() << 120.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(120.0, 240.0, v3, QVector<double>() << 30.0 << 25.0));
        QTest::newRow("Contained Aligned Composite Gap Undefined") <<
                2 <<
                -1 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 125.0, 155.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 125.0, 155.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 120.0) <<
                        TestEvent2(TestEvent2::Value, 1, 210.0, 235.0, 2.1) <<
                        TestEvent2(TestEvent2::Value, 0, 210.0, 235.0, 1.1) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 120.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 240.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 240.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 240.0, v3,
                                                        QVector<double>() << 30.0 << 25.0),
                                   QList<double>() << 120.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(120.0, 240.0, v3, QVector<double>() << 30.0 << 25.0));
        QTest::newRow("Bridging Unaligned Composite") <<
                1 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 55.0, 120.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 55.0, 120.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 115.0, v1,
                                                        QVector<double>() << 60.0)) <<
                        TestEvent2(TestEvent2::Value, 1, 120.0, 175.0, 2.1) <<
                        TestEvent2(TestEvent2::Value, 0, 120.0, 175.0, 1.1) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 115.0, v1,
                                                        QVector<double>() << 60.0) <<
                                           TestBinData2(115.0, 175.0, v3,
                                                        QVector<double>() << 5.0 << 55.0))) <<
                (QList<TestBinData2>() <<
                        TestBinData2(55.0, 115.0, v1, QVector<double>() << 60.0) <<
                        TestBinData2(115.0, 175.0, v3, QVector<double>() << 5.0 << 55.0));
        QTest::newRow("Bridging Aligned Composite") <<
                2 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 125.0, 245.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 125.0, 245.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 240.0, v1,
                                                        QVector<double>() << 115.0)) <<
                        TestEvent2(TestEvent2::Value, 1, 340.0, 355.0, 2.1) <<
                        TestEvent2(TestEvent2::Value, 0, 340.0, 355.0, 1.1) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 240.0, v1,
                                                        QVector<double>() << 115.0)) <<
                        TestEvent2(TestEvent2::Advance, 0, 360.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 360.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 240.0, v1,
                                                        QVector<double>() << 115.0) <<
                                           TestBinData2(240.0, 360.0, v3,
                                                        QVector<double>() << 5.0 << 15.0))) <<
                (QList<TestBinData2>() <<
                        TestBinData2(120.0, 240.0, v1, QVector<double>() << 115.0) <<
                        TestBinData2(240.0, 360.0, v3, QVector<double>() << 5.0 << 15.0));
        QTest::newRow("Tail Transition Unaligned") <<
                1 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 55.0, 110.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 55.0, 110.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 55.0) <<
                        TestEvent2(TestEvent2::Value, 1, 110.0, 175.0, 2.1) <<
                        TestEvent2(TestEvent2::Value, 0, 110.0, 175.0, 1.1) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 115.0, v3,
                                                        QVector<double>() << 55.0 << 5.0) <<
                                           TestBinData2(115.0, 175.0, v4,
                                                        QVector<double>() << 60.0),
                                   QList<double>() << 55.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(55.0, 115.0, v3, QVector<double>() << 55.0 << 5.0) <<
                        TestBinData2(115.0, 175.0, v4, QVector<double>() << 60.0));
        QTest::newRow("Tail Transition Aligned") <<
                2 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 125.0, 135.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 125.0, 135.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 120.0) <<
                        TestEvent2(TestEvent2::Value, 1, 210.0, 255.0, 2.1) <<
                        TestEvent2(TestEvent2::Value, 0, 210.0, 255.0, 1.1) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 240.0, v3,
                                                        QVector<double>() << 10.0 << 30.0),
                                   QList<double>() << 120.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 370.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 370.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(120.0, 240.0, v3,
                                                        QVector<double>() << 10.0 << 30.0) <<
                                           TestBinData2(240.0, 360.0, v4,
                                                        QVector<double>() << 15.0),
                                   QList<double>() << 120.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(120.0, 240.0, v3, QVector<double>() << 10.0 << 30.0) <<
                        TestBinData2(240.0, 360.0, v4, QVector<double>() << 15.0));
        QTest::newRow("Infinite Gap Unaligned") <<
                0 <<
                1 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 55.0, 60.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 55.0, 60.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 55.0) <<
                        TestEvent2(TestEvent2::Value, 0, 60.0, 120.0, 1.1) <<
                        TestEvent2(TestEvent2::Value, 1, 60.0, 120.0, 2.1) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 55.0) <<
                        TestEvent2(TestEvent2::Value, 1, 181.0, 190.0, 2.0) <<
                        TestEvent2(TestEvent2::Value, 0, 181.0, 190.0, 1.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 120.0, v3,
                                                        QVector<double>() << 5.0 << 60.0),
                                   QList<double>() << 55.0 << 181.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 251.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 251.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 120.0, v3,
                                                        QVector<double>() << 5.0 << 60.0),
                                   QList<double>() << 55.0 << 181.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(55.0, 120.0, v3, QVector<double>() << 5.0 << 60.0) <<
                        TestBinData2(181.0, 190.0, v1, QVector<double>() << 9.0));
        QTest::newRow("Infinite Gap Aligned") <<
                0 <<
                2 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 55.0, 60.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 55.0, 60.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 55.0) <<
                        TestEvent2(TestEvent2::Value, 0, 60.0, 125.0, 1.1) <<
                        TestEvent2(TestEvent2::Value, 1, 60.0, 125.0, 2.1) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 55.0) <<
                        TestEvent2(TestEvent2::Value, 1, 241.0, 251.0, 2.0) <<
                        TestEvent2(TestEvent2::Value, 0, 241.0, 251.0, 1.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 125.0, v3,
                                                        QVector<double>() << 5.0 << 65.0),
                                   QList<double>() << 55.0 << 241.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 361.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 361.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 125.0, v3,
                                                        QVector<double>() << 5.0 << 65.0),
                                   QList<double>() << 55.0 << 241.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(55.0, 125.0, v3, QVector<double>() << 5.0 << 65.0) <<
                        TestBinData2(241.0, 251.0, v1, QVector<double>() << 10.0));
        QTest::newRow("Finite Gap Unaligned") <<
                3 <<
                1 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 55.0, 60.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 55.0, 60.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 55.0) <<
                        TestEvent2(TestEvent2::Value, 0, 60.0, 70.0, 1.1) <<
                        TestEvent2(TestEvent2::Value, 1, 60.0, 70.0, 2.1) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 55.0) <<
                        TestEvent2(TestEvent2::Value, 0, 131.0, 230.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 131.0, 230.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 70.0, v3,
                                                        QVector<double>() << 5.0 << 10.0),
                                   QList<double>() << 55.0 << 131.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 235.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 235.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 70.0, v3,
                                                        QVector<double>() << 5.0 << 10.0) <<
                                           TestBinData2(131.0, 235.0, v1,
                                                        QVector<double>() << 99.0),
                                   QList<double>() << 55.0 << 131.0) <<
                        TestEvent2(TestEvent2::Value, 1, 235.0, 265.0, 2.1) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 70.0, v3,
                                                        QVector<double>() << 5.0 << 10.0) <<
                                           TestBinData2(131.0, 235.0, v1,
                                                        QVector<double>() << 99.0),
                                   QList<double>() << 55.0 << 131.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 326.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 326.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(55.0, 70.0, v3,
                                                        QVector<double>() << 5.0 << 10.0) <<
                                           TestBinData2(131.0, 235.0, v1,
                                                        QVector<double>() << 99.0),
                                   QList<double>() << 55.0 << 131.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(55.0, 70.0, v3, QVector<double>() << 5.0 << 10.0) <<
                        TestBinData2(131.0, 235.0, v1, QVector<double>() << 99.0) <<
                        TestBinData2(235.0, 415.0, v2, QVector<double>() << 30.0));

        QTest::newRow("Finite Gap Aligned") <<
                6 <<
                2 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 365.0, 370.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 365.0, 370.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 360.0) <<
                        TestEvent2(TestEvent2::Value, 0, 370.0, 380.0, 1.1) <<
                        TestEvent2(TestEvent2::Value, 1, 370.0, 380.0, 2.1) <<
                        TestEvent2(QList<TestBinData2>(), QList<double>() << 360.0) <<
                        TestEvent2(TestEvent2::Value, 0, 481.0, 600.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 481.0, 600.0, 2.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(360.0, 380.0, v3,
                                                        QVector<double>() << 5.0 << 10.0),
                                   QList<double>() << 360.0 << 481.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 720.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 720.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(360.0, 380.0, v3,
                                                        QVector<double>() << 5.0 << 10.0) <<
                                           TestBinData2(481.0, 720.0, v1,
                                                        QVector<double>() << 119.0),
                                   QList<double>() << 360.0 << 481.0) <<
                        TestEvent2(TestEvent2::Value, 1, 720.0, 745.0, 2.1) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(360.0, 380.0, v3,
                                                        QVector<double>() << 5.0 << 10.0) <<
                                           TestBinData2(481.0, 720.0, v1,
                                                        QVector<double>() << 119.0),
                                   QList<double>() << 360.0 << 481.0) <<
                        TestEvent2(TestEvent2::Advance, 1, 841.0) <<
                        TestEvent2(TestEvent2::Advance, 0, 841.0) <<
                        TestEvent2(QList<TestBinData2>() <<
                                           TestBinData2(360.0, 380.0, v3,
                                                        QVector<double>() << 5.0 << 10.0) <<
                                           TestBinData2(481.0, 720.0, v1,
                                                        QVector<double>() << 119.0),
                                   QList<double>() << 360.0 << 481.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(360.0, 380.0, v3, QVector<double>() << 5.0 << 10.0) <<
                        TestBinData2(481.0, 720.0, v1, QVector<double>() << 119.0) <<
                        TestBinData2(720.0, 1080.0, v2, QVector<double>() << 25.0));

        QTest::newRow("Exact break advance") <<
                1 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 60.0, 120.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 60.0, 120.0, 2.0) <<
                        TestEvent2(TestEvent2::Value, 0, 180.0, 240.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 180.0, 240.0, 2.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(60.0, 120.0, v1, QVector<double>() << 60.0) <<
                        TestBinData2(180.0, 240.0, v1, QVector<double>() << 60.0));

        QTest::newRow("Exact break advance end") <<
                1 <<
                0 <<
                (QList<TestEvent2>() <<
                        TestEvent2(TestEvent2::Value, 0, 60.0, 120.0, 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 60.0, 120.0, 2.0) <<
                        TestEvent2(TestEvent2::Value, 0, 180.0, FP::undefined(), 1.0) <<
                        TestEvent2(TestEvent2::Value, 1, 180.0, FP::undefined(), 2.0)) <<
                (QList<TestBinData2>() <<
                        TestBinData2(60.0, 120.0, v1, QVector<double>() << 60.0) <<
                        TestBinData2(180.0, FP::undefined(), v1,
                                     QVector<double>() << FP::undefined()));
    }

    void conventional()
    {
        TestSimpleTarget targetValues;
        TestSimpleTarget targetCover;
        TestSimpleTargetGeneral targetStats;

        SmootherChainFixedTimeConventional tc(new DynamicPrimitive<double>::Constant(0.1),
                                              new DynamicTimeInterval::Constant(Time::Second, 1000,
                                                                                true), NULL,
                                              &targetValues, &targetCover, &targetStats);

        tc.getTargetValue()->incomingData(1000.0, 1500.0, 1.0);
        tc.getTargetValue()->incomingData(1500.0, 2000.0, 2.0);
        tc.getTargetCover()->incomingAdvance(2000.0);
        QVERIFY(!targetValues.values.isEmpty());
        QVERIFY(targetCover.values.isEmpty());
        QCOMPARE(targetValues.values.last(), 1.5);

        tc.getTargetValue()->incomingData(2000.0, 2500.0, 1.0);
        tc.getTargetValue()->incomingData(2500.0, 3000.0, FP::undefined());
        tc.getTargetCover()->incomingAdvance(3000.0);
        QCOMPARE(targetValues.values.last(), 1.0);
        QVERIFY(!targetCover.values.isEmpty());
        QCOMPARE(targetCover.values.last(), 0.5);

        tc.getTargetValue()->incomingData(3000.0, 4000.0, FP::undefined());
        tc.getTargetCover()->incomingAdvance(4000.0);
        QVERIFY(!FP::defined(targetValues.values.last()));

        tc.getTargetValue()->incomingData(4000.0, 4800.0, 1.0);
        tc.getTargetValue()->incomingData(4800.0, 5000.0, 2.0);
        tc.getTargetCover()->incomingAdvance(5000.0);
        QCOMPARE(targetValues.values.last(), 1.2);

        tc.getTargetValue()->incomingData(5000.0, 5800.0, 1.0);
        tc.getTargetValue()->incomingData(5900.0, 6000.0, 2.0);
        tc.getTargetCover()->incomingAdvance(6000.0);
        QCOMPARE(targetValues.values.last(), 1.0 / 0.9);
        QCOMPARE(targetCover.values.last(), 0.9);

        tc.getTargetValue()->incomingData(6000.0, 6500.0, 1.0);
        tc.getTargetValue()->incomingData(6500.0, 7000.0, 2.0);
        tc.getTargetCover()->incomingData(6000.0, 6500.0, 0.5);
        tc.getTargetCover()->incomingData(6500.0, 7000.0, 0.75);
        QCOMPARE(targetValues.values.last(), 1.6);
        QCOMPARE(targetCover.values.last(), 0.625);

        tc.getTargetValue()->incomingData(7000.0, 8000.0, 1.0);
        tc.getTargetCover()->incomingData(7000.0, 8000.0, 0.05);
        QVERIFY(!FP::defined(targetValues.values.last()));
        QCOMPARE(targetCover.values.last(), 0.05);

        tc.getTargetValue()->incomingData(8000.0, 8500.0, 1.0);
        tc.getTargetValue()->incomingData(8500.0, 8750.0, 2.0);
        tc.getTargetValue()->incomingData(8750.0, 9000.0, 3.0);
        tc.getTargetCover()->incomingData(8000.0, 8500.0, 0.5);
        tc.getTargetCover()->incomingData(8500.0, 8750.0, 0.75);
        tc.getTargetCover()->incomingData(8750.0, 9000.0, 0.9);
        QCOMPARE(targetValues.values.last(), 1300.0 / (250 + 187.50 + 225.0));
        QCOMPARE(targetCover.values.last(), 0.6625);

        tc.getTargetCover()->endData();
        tc.getTargetValue()->endData();

        QVERIFY(!targetStats.values.isEmpty());
        QCOMPARE((int) targetStats.values.last()["Count"].toInt64(), 3);
        QCOMPARE((int) targetStats.values.last()["UndefinedCount"].toInt64(), 0);
        QCOMPARE(targetStats.values.last()["Minimum"].toDouble(), 1.0);
        QCOMPARE(targetStats.values.last()["Maximum"].toDouble(), 3.0);
        QCOMPARE(targetStats.values.last()["Mean"].toDouble(), 2.0);
        QCOMPARE(targetStats.values.last()["Quantiles"].keyframe(0.5).toDouble(), 2.0);
        QCOMPARE(targetStats.values.last()["Quantiles"].keyframe(0.75).toDouble(), 2.5);
    }

    void difference()
    {
        TestSimpleTarget targetFirst;
        TestSimpleTarget targetLast;
        TestSimpleTarget targetCover;

        SmootherChainFixedTimeDifference td
                (new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL, &targetFirst,
                 &targetLast, &targetCover);

        td.getTargetStart()->incomingData(1000.0, 2000.0, 1.00);
        td.getTargetEnd()->incomingAdvance(2000.0);
        QVERIFY(!targetFirst.values.isEmpty());
        QVERIFY(!targetLast.values.isEmpty());
        QCOMPARE(targetFirst.values.last(), 1.00);
        QVERIFY(targetCover.values.isEmpty());
        QVERIFY(!FP::defined(targetLast.values.last()));

        td.getTargetStart()->incomingData(2000.0, 3000.0, 1.01);
        td.getTargetEnd()->incomingData(2000.0, 3000.0, 2.01);
        QCOMPARE(targetFirst.values.last(), 1.01);
        QCOMPARE(targetLast.values.last(), 2.01);

        td.getTargetStart()->incomingData(3000.0, 3900.0, 1.02);
        td.getTargetEnd()->incomingData(3000.0, 3900.0, 2.02);
        td.getTargetStart()->incomingAdvance(4000.0);
        td.getTargetEnd()->incomingAdvance(4000.0);
        QCOMPARE(targetFirst.values.last(), 1.02);
        QCOMPARE(targetLast.values.last(), 2.02);
        QVERIFY(!targetCover.values.isEmpty());
        QCOMPARE(targetCover.values.last(), 0.9);

        td.getTargetStart()->incomingData(4000.0, 4500.0, 1.03);
        td.getTargetEnd()->incomingData(4000.0, 4500.0, 2.03);
        td.getTargetStart()->incomingData(4500.0, 5000.0, 1.04);
        td.getTargetEnd()->incomingData(4500.0, 5000.0, 2.04);
        QCOMPARE(targetFirst.values.last(), 1.03);
        QCOMPARE(targetLast.values.last(), 2.04);

        td.getTargetStart()->incomingData(5100.0, 5250.0, 1.05);
        td.getTargetEnd()->incomingData(5100.0, 5250.0, 2.05);
        td.getTargetStart()->incomingData(5250.0, 5400.0, 1.06);
        td.getTargetEnd()->incomingData(5250.0, 5400.0, 2.06);
        td.getTargetStart()->incomingData(5600.0, 5750.0, 1.07);
        td.getTargetEnd()->incomingData(5600.0, 5750.0, 2.07);
        td.getTargetStart()->incomingData(5750.0, 5900.0, 1.08);
        td.getTargetEnd()->incomingData(5750.0, 5900.0, 2.08);
        td.getTargetEnd()->incomingAdvance(6000.0);
        td.getTargetStart()->incomingAdvance(6000.0);
        QCOMPARE(targetFirst.values.last(), 1.05);
        QCOMPARE(targetLast.values.last(), 2.08);
        QCOMPARE(targetCover.values.last(), 0.6);

        td.getTargetStart()->incomingData(6000.0, 6500.0, FP::undefined());
        td.getTargetEnd()->incomingData(6000.0, 6500.0, FP::undefined());
        td.getTargetStart()->incomingData(6500.0, 7000.0, 1.09);
        td.getTargetEnd()->incomingData(6500.0, 7000.0, 2.09);
        QCOMPARE(targetFirst.values.last(), 1.09);
        QCOMPARE(targetLast.values.last(), 2.09);
        QCOMPARE(targetCover.values.last(), 0.5);

        td.getTargetStart()->incomingData(7000.0, 7500.0, 1.10);
        td.getTargetEnd()->incomingData(7000.0, 7500.0, 2.10);
        td.getTargetStart()->incomingData(7500.0, 8000.0, FP::undefined());
        td.getTargetEnd()->incomingData(7500.0, 8000.0, FP::undefined());
        QCOMPARE(targetFirst.values.last(), 1.10);
        QCOMPARE(targetLast.values.last(), 2.10);
        QCOMPARE(targetCover.values.last(), 0.5);

        td.getTargetStart()->incomingData(8000.0, 8500.0, 1.10);
        td.getTargetEnd()->incomingData(8000.0, 8500.0, 2.10);
        td.getTargetStart()->incomingData(8600.0, 9000.0, 1.11);
        td.getTargetEnd()->incomingData(8600.0, 9000.0, FP::undefined());
        QCOMPARE(targetFirst.values.last(), 1.10);
        QCOMPARE(targetLast.values.last(), 1.11);
        QCOMPARE(targetCover.values.last(), 0.9);

        td.getTargetStart()->incomingData(9000.0, 9500.0, FP::undefined());
        td.getTargetEnd()->incomingData(9000.0, 9500.0, 2.12);
        td.getTargetStart()->incomingData(9500.0, 10000.0, 1.13);
        td.getTargetEnd()->incomingData(9500.0, 10000.0, FP::undefined());
        QCOMPARE(targetFirst.values.last(), 2.12);
        QCOMPARE(targetLast.values.last(), 1.13);

        td.getTargetStart()->incomingData(10000.0, 10500.0, FP::undefined());
        td.getTargetEnd()->incomingData(10000.0, 10500.0, 2.14);
        td.getTargetStart()->incomingData(10600.0, 11000.0, 1.14);
        td.getTargetEnd()->incomingData(10600.0, 11000.0, 2.15);
        QCOMPARE(targetFirst.values.last(), 2.14);
        QCOMPARE(targetLast.values.last(), 2.15);
        QCOMPARE(targetCover.values.last(), 0.4);

        td.getTargetStart()->incomingData(11000.0, 11500.0, 1.16);
        td.getTargetEnd()->incomingData(11000.0, 11500.0, 2.16);
        td.getTargetStart()->incomingData(11600.0, 11750.0, FP::undefined());
        td.getTargetEnd()->incomingData(11600.0, 11750.0, FP::undefined());
        td.getTargetStart()->incomingData(11750.0, 12000.0, 1.17);
        td.getTargetEnd()->incomingData(11750.0, 12000.0, 2.17);
        QCOMPARE(targetFirst.values.last(), 1.16);
        QCOMPARE(targetLast.values.last(), 2.17);
        QCOMPARE(targetCover.values.last(), 0.9);

        td.getTargetStart()->endData();
        td.getTargetEnd()->endData();
    }

    void differenceGap()
    {
        TestSimpleTarget targetFirst;
        TestSimpleTarget targetLast;
        TestSimpleTarget targetCover;

        SmootherChainFixedTimeDifference td
                (new DynamicTimeInterval::Constant(Time::Second, 1000, true),
                 new DynamicTimeInterval::Constant(Time::Millisecond, 1, false), &targetFirst,
                 &targetLast, &targetCover);

        td.getTargetStart()->incomingData(1000.0, 2000.0, 1.00);
        td.getTargetEnd()->incomingAdvance(2000.0);
        QVERIFY(!targetFirst.values.isEmpty());
        QVERIFY(!targetLast.values.isEmpty());
        QVERIFY(targetCover.values.isEmpty());
        QCOMPARE(targetFirst.values.last(), 1.00);
        QVERIFY(!FP::defined(targetLast.values.last()));

        td.getTargetStart()->incomingData(2000.0, 3000.0, 1.01);
        td.getTargetEnd()->incomingData(2000.0, 3000.0, 2.01);
        QCOMPARE(targetFirst.values.last(), 1.01);
        QCOMPARE(targetLast.values.last(), 2.01);
        QVERIFY(targetCover.values.isEmpty());

        td.getTargetStart()->incomingData(3000.0, 3900.0, 1.02);
        td.getTargetEnd()->incomingData(3000.0, 3900.0, 2.02);
        td.getTargetStart()->incomingAdvance(3999.0);
        td.getTargetEnd()->incomingAdvance(3999.0);
        td.getTargetStart()->incomingAdvance(4000.0);
        td.getTargetEnd()->incomingAdvance(4000.0);
        QCOMPARE(targetFirst.values.last(), 1.02);
        QCOMPARE(targetLast.values.last(), 2.02);
        QVERIFY(targetCover.values.isEmpty());

        td.getTargetStart()->incomingData(4000.0, 4500.0, 1.03);
        td.getTargetEnd()->incomingData(4000.0, 4500.0, 2.03);
        td.getTargetStart()->incomingData(4500.0, 5000.0, 1.04);
        td.getTargetEnd()->incomingData(4500.0, 5000.0, 2.04);
        QCOMPARE(targetFirst.values.last(), 1.03);
        QCOMPARE(targetLast.values.last(), 2.04);
        QVERIFY(targetCover.values.isEmpty());

        td.getTargetStart()->incomingData(5100.0, 5250.0, 1.05);
        td.getTargetEnd()->incomingAdvance(5100.0);
        td.getTargetEnd()->incomingData(5100.0, 5250.0, 2.05);
        td.getTargetStart()->incomingData(5250.0, 5400.0, 1.06);
        td.getTargetEnd()->incomingData(5250.0, 5400.0, 2.06);
        td.getTargetStart()->incomingData(5600.0, 5750.0, 1.07);
        td.getTargetEnd()->incomingData(5600.0, 5750.0, 2.07);
        QCOMPARE(targetFirst.starts.last(), 5100.0);
        QCOMPARE(targetFirst.ends.last(), 5400.0);
        QCOMPARE(targetFirst.values.last(), 1.05);
        QCOMPARE(targetLast.values.last(), 2.06);
        QVERIFY(targetCover.values.isEmpty());
        td.getTargetStart()->incomingData(5750.0, 5900.0, 1.08);
        td.getTargetEnd()->incomingData(5750.0, 5900.0, 2.08);
        td.getTargetEnd()->incomingAdvance(6000.0);
        td.getTargetStart()->incomingAdvance(6000.0);
        QCOMPARE(targetFirst.starts.last(), 5600.0);
        QCOMPARE(targetFirst.values.last(), 1.07);
        QCOMPARE(targetLast.values.last(), 2.08);

        td.getTargetEnd()->incomingAdvance(7000.0);
        td.getTargetStart()->incomingAdvance(7000.0);
        td.getTargetStart()->incomingData(7000.0, 7250.0, 1.09);
        td.getTargetEnd()->incomingData(7000.0, 7250.0, 2.09);
        td.getTargetStart()->incomingData(7250.0, 7500.0, 1.10);
        td.getTargetEnd()->incomingData(7250.0, 7500.0, 2.10);

        td.getTargetStart()->incomingData(8000.0, 8500.0, 1.11);
        td.getTargetEnd()->incomingData(8000.0, 8500.0, 2.11);

        QCOMPARE(targetFirst.starts.last(), 7000.0);
        QCOMPARE(targetFirst.ends.last(), 7500.0);
        QCOMPARE(targetFirst.values.last(), 1.09);
        QCOMPARE(targetLast.values.last(), 2.10);

        td.getTargetStart()->endData();
        td.getTargetEnd()->endData();
    }

    void differenceInitial()
    {
        TestSimpleTarget targetFirst;
        TestSimpleTarget targetLast;
        TestSimpleTarget targetCover;

        SmootherChainFixedTimeDifference td
                (new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL, &targetFirst,
                 &targetLast, &targetCover);
        td.getTargetEnd()->endData();

        td.getTargetStart()->incomingData(1000.0, 2000.0, 1.00);
        QVERIFY(!targetFirst.values.isEmpty());
        QVERIFY(!targetLast.values.isEmpty());
        QCOMPARE(targetFirst.values.last(), 1.00);
        QVERIFY(targetCover.values.isEmpty());
        QVERIFY(!FP::defined(targetLast.values.last()));

        td.getTargetStart()->incomingData(2000.0, 3000.0, 1.01);
        QCOMPARE(targetFirst.values.last(), 1.01);
        QVERIFY(!FP::defined(targetLast.values.last()));

        td.getTargetStart()->incomingData(3000.0, 3900.0, 1.02);
        td.getTargetStart()->incomingData(3900.0, 4000.0, 2.02);
        QCOMPARE(targetFirst.values.last(), 1.02);
        QCOMPARE(targetLast.values.last(), 2.02);
        QVERIFY(targetCover.values.isEmpty());

        td.getTargetStart()->endData();
    }

    void flags()
    {
        TestSimpleTargetFlags targetFlags;
        TestSimpleTarget targetCover;

        SmootherChainFixedTimeFlags fl
                (new DynamicTimeInterval::Constant(Time::Second, 1000, true), NULL, &targetFlags,
                 &targetCover);

        fl.getTargetFlags()->incomingData(1000.0, 2000.0, Variant::Flags{"F01"});
        fl.getTargetCover()->incomingAdvance(2000.0);
        QVERIFY(!targetFlags.values.isEmpty());
        QVERIFY(targetCover.values.isEmpty());
        QCOMPARE(targetFlags.values.last(), Variant::Flags{"F01"});

        fl.getTargetFlags()->incomingData(2000.0, 2500.0, Variant::Flags{"F02"});
        fl.getTargetFlags()->incomingData(2500.0, 3000.0, Variant::Flags{"F03"});
        fl.getTargetCover()->incomingAdvance(3000.0);
        QCOMPARE(targetFlags.values.last(), (Variant::Flags{"F02", "F03"}));
        QVERIFY(targetCover.values.isEmpty());

        fl.getTargetFlags()->incomingData(3000.0, 3500.0, Variant::Flags{"F04"});
        fl.getTargetFlags()->incomingData(3600.0, 4000.0, Variant::Flags{"F04"});
        fl.getTargetCover()->incomingAdvance(4000.0);
        QCOMPARE(targetFlags.values.last(), Variant::Flags{"F04"});
        QVERIFY(!targetCover.values.isEmpty());
        QCOMPARE(targetCover.values.last(), 0.9);

        fl.getTargetFlags()->incomingData(4000.0, 4500.0, Variant::Flags{"F05", "F06"});
        fl.getTargetFlags()->incomingAdvance(5000.0);
        fl.getTargetCover()->incomingAdvance(5000.0);
        QCOMPARE(targetFlags.values.last(), (Variant::Flags{"F05", "F06"}));
        QCOMPARE(targetCover.values.last(), 0.5);

        fl.getTargetFlags()->incomingData(5000.0, 5500.0, Variant::Flags{"F06"});
        fl.getTargetCover()->incomingData(5000.0, 5500.0, 0.5);
        fl.getTargetFlags()->incomingData(5500.0, 6000.0, Variant::Flags{"F07"});
        fl.getTargetCover()->incomingData(5500.0, 6000.0, 1.0);
        QCOMPARE(targetFlags.values.last(), (Variant::Flags{"F06", "F07"}));
        QCOMPARE(targetCover.values.last(), 0.75);
    }

    void vectorStats()
    {
        TestSimpleTarget targetValues;
        TestSimpleTarget targetCover;
        TestSimpleTargetGeneral targetStats;

        SmootherChainFixedTimeVectorStatistics tc(new DynamicPrimitive<double>::Constant(0.1),
                                                  new DynamicTimeInterval::Constant(Time::Second,
                                                                                    1000, true),
                                                  NULL, &targetValues, &targetCover, &targetStats);

        tc.getTargetValue()->incomingData(1000.0, 2000.0, 1.0);
        tc.getTargetCover()->incomingData(1000.0, 2000.0, 0.05);
        tc.getTargetVectoredMagnitude()->incomingData(1000.0, 2000.0, 1.0);
        QVERIFY(!targetValues.values.isEmpty());
        QVERIFY(!targetCover.values.isEmpty());
        QCOMPARE(targetCover.values.last(), 0.05);
        QVERIFY(!FP::defined(targetValues.values.last()));


        tc.getTargetValue()->incomingData(7000.0, 7500.0, 1.0);
        tc.getTargetValue()->incomingData(7500.0, 7750.0, 2.0);
        tc.getTargetValue()->incomingData(7750.0, 8000.0, 3.0);
        tc.getTargetCover()->incomingData(7000.0, 7500.0, 0.5);
        tc.getTargetCover()->incomingData(7500.0, 7750.0, 0.75);
        tc.getTargetCover()->incomingData(7750.0, 8000.0, 0.9);
        tc.getTargetVectoredMagnitude()
          ->incomingData(7000.0, 8000.0, 1300.0 / (250 + 187.50 + 225.0) * 0.9);
        QCOMPARE(targetValues.values.last(), 1300.0 / (250 + 187.50 + 225.0));
        QCOMPARE(targetCover.values.last(), 0.6625);

        tc.getTargetCover()->endData();
        tc.getTargetValue()->endData();
        tc.getTargetVectoredMagnitude()->endData();

        QVERIFY(!targetStats.values.isEmpty());
        QCOMPARE((int) targetStats.values.last()["Count"].toInt64(), 3);
        QCOMPARE((int) targetStats.values.last()["UndefinedCount"].toInt64(), 0);
        QCOMPARE(targetStats.values.last()["Minimum"].toDouble(), 1.0);
        QCOMPARE(targetStats.values.last()["Maximum"].toDouble(), 3.0);
        QCOMPARE(targetStats.values.last()["Mean"].toDouble(), 2.0);
        QCOMPARE(targetStats.values.last()["StabilityFactor"].toDouble(), 0.9);
        QCOMPARE(targetStats.values.last()["Quantiles"].keyframe(0.5).toDouble(), 2.0);
        QCOMPARE(targetStats.values.last()["Quantiles"].keyframe(0.75).toDouble(), 2.5);
    }
};

QTEST_APPLESS_MAIN(TestFixedTime)

#include "fixedtime.moc"
