/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>

#include "smoothing/segmentcontrolled.hxx"

using namespace CPD3;
using namespace CPD3::Smoothing;
using namespace CPD3::Data;

struct SegmentData {
    double start;
    double end;
    double value;

    SegmentData() : start(FP::undefined()), end(FP::undefined()), value(FP::undefined())
    { }

    SegmentData(double s, double e, double v) : start(s), end(e), value(v)
    { }

    bool operator==(const SegmentData &other) const
    {
        return FP::equal(start, other.start) &&
                FP::equal(end, other.end) &&
                FP::equal(value, other.value);
    }

    bool operator!=(const SegmentData &other) const
    { return !(*this == other); }
};

Q_DECLARE_METATYPE(SegmentData);

Q_DECLARE_METATYPE(QList<SegmentData>);

struct BinData {
    double start;
    double end;
    QList<SegmentData> data;

    BinData() : start(FP::undefined()), end(FP::undefined()), data()
    { }

    BinData(double s, double e, const QList<SegmentData> &d) : start(s), end(e), data(d)
    { }

    bool operator==(const BinData &other) const
    {
        return FP::equal(start, other.start) && FP::equal(end, other.end) && data == other.data;
    }

    bool operator!=(const BinData &other) const
    { return !(*this == other); }
};

Q_DECLARE_METATYPE(BinData);

Q_DECLARE_METATYPE(QList<BinData>);

namespace QTest {
template<>
char *toString(const QList<BinData> &output)
{
    QByteArray ba;
    for (int i = 0, maxBin = output.size(); i < maxBin; i++) {
        if (i != 0) ba += ", ";
        ba += "(";
        if (FP::defined(output.at(i).start))
            ba += QByteArray::number(output.at(i).start);
        else
            ba += "-Infinity";
        ba += ",";
        if (FP::defined(output.at(i).end))
            ba += QByteArray::number(output.at(i).end);
        else
            ba += "+Infinity";
        ba += ",[";
        for (int j = 0, maxValue = output.at(i).data.size(); j < maxValue; ++j) {
            if (j != 0) ba += ",";
            ba += "{";
            if (FP::defined(output.at(i).data.at(j).start))
                ba += QByteArray::number(output.at(i).data.at(j).start);
            else
                ba += "-Infinity";
            ba += ",";
            if (FP::defined(output.at(i).data.at(j).end))
                ba += QByteArray::number(output.at(i).data.at(j).end);
            else
                ba += "+Infinity";
            ba += ",";
            if (FP::defined(output.at(i).data.at(j).value))
                ba += QByteArray::number(output.at(i).data.at(j).value);
            else
                ba += "+Infinity";
            ba += "}";

        }
        ba += "])";
    }
    return qstrdup(ba.data());
}
};

class TestSegmentBinner : public SegmentControlledBinner<double> {
public:
    QList<SegmentData> buffer;
    QList<BinData> result;
    QList<double> advances;
    bool ended;
    double checkTime;

    TestSegmentBinner(DynamicTimeInterval *gap = NULL, bool discardIntersecting = false)
            : SegmentControlledBinner<double>(gap, discardIntersecting),
              buffer(),
              result(),
              advances(),
              ended(false),
              checkTime(FP::undefined())
    { }

    void addValue(double start, double end, double data)
    { incomingSegment(start, end, data); }

    void addAdvance(double time)
    { incomingSegmentAdvance(time); }

    void endData()
    { incomingSegmentsCompleted(); }

protected:
    virtual void addToBin(double start, double end, const double &data)
    {
        Q_ASSERT(!ended);
        buffer.append(SegmentData(start, end, data));
    }

    virtual void completedBinning()
    {
        Q_ASSERT(!ended);
        ended = true;
    }

    virtual void processBin(double start, double end)
    {
        Q_ASSERT(!ended);
        Q_ASSERT(Range::compareStart(checkTime, start) <= 0);
        checkTime = end;
        result.append(BinData(start, end, buffer));
        buffer.clear();
    }

    virtual void advanceBin(double time)
    {
        Q_ASSERT(!ended);
        Q_ASSERT(Range::compareStart(checkTime, time) <= 0);
        checkTime = time;
        advances.append(time);
    }
};

struct TestEvent {
    enum Type {
        Value, Advance, SegmentStart, SegmentEnd, Check,
    };
    Type type;
    double v1;
    double v2;
    double v3;
    QList<BinData> v4;
    QList<double> v5;

    TestEvent(Type t = Check,
              double vs1 = FP::undefined(),
              double vs2 = FP::undefined(),
              double vs3 = FP::undefined()) : type(t), v1(vs1), v2(vs2), v3(vs3), v4(), v5()
    { }

    TestEvent(Type t, const QList<BinData> &vs4, const QList<double> &vs5 = QList<double>()) : type(
            t), v1(FP::undefined()), v2(FP::undefined()), v3(FP::undefined()), v4(vs4), v5(vs5)
    { }

    TestEvent(const QList<BinData> &vs4, const QList<double> &vs5 = QList<double>()) : type(Check),
                                                                                       v1(FP::undefined()),
                                                                                       v2(FP::undefined()),
                                                                                       v3(FP::undefined()),
                                                                                       v4(vs4),
                                                                                       v5(vs5)
    { }
};

Q_DECLARE_METATYPE(TestEvent);

Q_DECLARE_METATYPE(QList<TestEvent>);

namespace QTest {
template<>
char *toString(const QList<double> &output)
{
    QByteArray ba;
    for (int i = 0, max = output.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        if (FP::defined(output.at(i)))
            ba += QByteArray::number(output.at(i));
        else
            ba += "Undefined";
    }
    return qstrdup(ba.data());
}
};

class TestSimpleTarget : public SmootherChainTarget {
public:
    QVector<double> values;
    QVector<double> starts;
    QVector<double> ends;

    virtual void incomingData(double start, double end, double value)
    {
        Q_ASSERT(ends.isEmpty() || start >= ends.last());
        values.append(value);
        starts.append(start);
        ends.append(end);
    }

    virtual void incomingAdvance(double time)
    {
        Q_ASSERT(ends.isEmpty() || time >= ends.last());
    }

    virtual void endData()
    { }
};

class TestSimpleTargetFlags : public SmootherChainTargetFlags {
public:
    QVector<Variant::Flags> values;
    QVector<double> starts;
    QVector<double> ends;

    virtual void incomingData(double start, double end, const Variant::Flags &value)
    {
        Q_ASSERT(ends.isEmpty() || start >= ends.last());
        values.append(value);
        starts.append(start);
        ends.append(end);
    }

    virtual void incomingAdvance(double time)
    {
        Q_ASSERT(ends.isEmpty() || time >= ends.last());
    }

    virtual void endData()
    { }
};

class TestSimpleTargetGeneral : public SmootherChainTargetGeneral {
public:
    QVector<Variant::Read> values;
    QVector<double> starts;
    QVector<double> ends;

    virtual void incomingData(double start, double end, const Variant::Root &value)
    {
        Q_ASSERT(ends.isEmpty() || start >= ends.last());
        values.append(value.read());
        starts.append(start);
        ends.append(end);
    }

    virtual void incomingAdvance(double time)
    {
        Q_ASSERT(ends.isEmpty() || time >= ends.last());
    }

    virtual void endData()
    { }
};

class TestSegmentControlled : public QObject {
Q_OBJECT
private slots:

    void binner()
    {
        QFETCH(int, gapMinutes);
        QFETCH(bool, discardIntersecting);
        QFETCH(QList<TestEvent>, events);
        QFETCH(QList<BinData>, result);

        DynamicTimeInterval *gap;
        if (gapMinutes == 0) {
            gap = NULL;
        } else if (gapMinutes < 0) {
            gap = new DynamicTimeInterval::Undefined;
        } else {
            gap = new DynamicTimeInterval::Constant(Time::Minute, gapMinutes);
        }

        /* Basic */
        TestSegmentBinner *binner =
                new TestSegmentBinner(gap == NULL ? NULL : gap->clone(), discardIntersecting);
        for (QList<TestEvent>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            QVERIFY(!binner->ended);
            switch (e->type) {
            case TestEvent::Value:
                binner->addValue(e->v1, e->v2, e->v3);
                break;
            case TestEvent::Advance:
                binner->addAdvance(e->v1);
                break;
            case TestEvent::SegmentStart:
                binner->getController()->segmentStart(e->v1);
                break;
            case TestEvent::SegmentEnd:
                binner->getController()->segmentEnd(e->v1);
                break;
            case TestEvent::Check:
                QCOMPARE(binner->result, e->v4);
                QCOMPARE(binner->advances, e->v5);
                break;
            }
        }
        QVERIFY(!binner->ended);
        binner->endData();
        QVERIFY(binner->ended);
        QCOMPARE(binner->result, result);
        delete binner;

        /* Without advances */
        binner = new TestSegmentBinner(gap == NULL ? NULL : gap->clone(), discardIntersecting);
        for (QList<TestEvent>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            QVERIFY(!binner->ended);
            switch (e->type) {
            case TestEvent::Value:
                binner->addValue(e->v1, e->v2, e->v3);
                break;
            case TestEvent::SegmentStart:
                binner->getController()->segmentStart(e->v1);
                break;
            case TestEvent::SegmentEnd:
                binner->getController()->segmentEnd(e->v1);
                break;
            case TestEvent::Check:
            case TestEvent::Advance:
                break;
            }
        }
        QVERIFY(!binner->ended);
        binner->endData();
        QVERIFY(binner->ended);
        QCOMPARE(binner->result, result);
        delete binner;

        /* With serialize and deserialize */
        binner = new TestSegmentBinner(gap == NULL ? NULL : gap->clone(), discardIntersecting);
        for (QList<TestEvent>::const_iterator e = events.constBegin(), end = events.constEnd();
                e != end;
                ++e) {
            QVERIFY(!binner->ended);
            {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    binner->writeSerial(stream);
                }

                QList<SegmentData> saveBuffer(binner->buffer);
                QList<BinData> saveResult(binner->result);
                QList<double> saveAdvances(binner->advances);
                double saveCheckTime = binner->checkTime;

                delete binner;
                binner = new TestSegmentBinner;
                {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    binner->readSerial(stream);
                }
                binner->buffer = saveBuffer;
                binner->result = saveResult;
                binner->advances = saveAdvances;
                binner->checkTime = saveCheckTime;
            }
            QVERIFY(!binner->ended);

            switch (e->type) {
            case TestEvent::Value:
                binner->addValue(e->v1, e->v2, e->v3);
                break;
            case TestEvent::Advance:
                binner->addAdvance(e->v1);
                break;
            case TestEvent::SegmentStart:
                binner->getController()->segmentStart(e->v1);
                break;
            case TestEvent::SegmentEnd:
                binner->getController()->segmentEnd(e->v1);
                break;
            case TestEvent::Check:
                QCOMPARE(binner->result, e->v4);
                QCOMPARE(binner->advances, e->v5);
                break;
            }
        }
        QVERIFY(!binner->ended);
        binner->endData();
        QVERIFY(binner->ended);
        QCOMPARE(binner->result, result);
        delete binner;

        delete gap;
    }

    void binner_data()
    {
        QTest::addColumn<int>("gapMinutes");
        QTest::addColumn<bool>("discardIntersecting");
        QTest::addColumn<QList<TestEvent> >("events");
        QTest::addColumn<QList<BinData> >("result");

        QTest::newRow("Empty") << 0 << false << (QList<TestEvent>()) << (QList<BinData>());

        QTest::newRow("Single value single segment") <<
                0 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Value, 100.0, 200.0, 1.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100.0, 200.0, QList<SegmentData>() <<
                                                  SegmentData(100.0, 200.0, 1.0)))) <<
                (QList<BinData>() <<
                        BinData(100.0, 200.0,
                                QList<SegmentData>() << SegmentData(100.0, 200.0, 1.0)));
        QTest::newRow("Single value single segment no discard") <<
                0 <<
                true <<
                (QList<TestEvent>() <<
                        TestEvent() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Value, 100.0, 200.0, 1.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100.0, 200.0, QList<SegmentData>() <<
                                                  SegmentData(100.0, 200.0, 1.0)))) <<
                (QList<BinData>() <<
                        BinData(100.0, 200.0,
                                QList<SegmentData>() << SegmentData(100.0, 200.0, 1.0)));
        QTest::newRow("Single value single discard start") <<
                0 <<
                true <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::Value, 90.0, 200.0, 1.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::SegmentEnd, 200.0)) <<
                (QList<BinData>());
        QTest::newRow("Single value single discard end") <<
                0 <<
                true <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::Value, 110.0, 210.0, 1.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent(QList<BinData>(), QList<double>() << 200.0)) <<
                (QList<BinData>());
        QTest::newRow("Single value single segment clip start") <<
                0 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::Value, 90.0, 200.0, 1.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100.0, 200.0, QList<SegmentData>() <<
                                                  SegmentData(100.0, 200.0, 1.0)))) <<
                (QList<BinData>() <<
                        BinData(100.0, 200.0,
                                QList<SegmentData>() << SegmentData(100.0, 200.0, 1.0)));
        QTest::newRow("Single value single segment before") <<
                0 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Value, 90.0, 220.0, 1.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100.0, 200.0, QList<SegmentData>() <<
                                                  SegmentData(100.0, 200.0, 1.0)),
                                  QList<double>() << 220.0)) <<
                (QList<BinData>() <<
                        BinData(100.0, 200.0,
                                QList<SegmentData>() << SegmentData(100.0, 200.0, 1.0)));
        QTest::newRow("Single segment internal value") <<
                0 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::Value, 110.0, 180.0, 1.0) <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Advance, 190.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Advance, 220.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100.0, 200.0, QList<SegmentData>() <<
                                                  SegmentData(110.0, 180.0, 1.0)),
                                  QList<double>() << 220.0) <<
                        TestEvent(TestEvent::Advance, 240.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100.0, 200.0, QList<SegmentData>() <<
                                                  SegmentData(110.0, 180.0, 1.0)),
                                  QList<double>() << 220.0 << 240.0)) <<
                (QList<BinData>() <<
                        BinData(100.0, 200.0,
                                QList<SegmentData>() << SegmentData(110.0, 180.0, 1.0)));
        QTest::newRow("Single segment internal value exact") <<
                0 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::Value, 110.0, 180.0, 1.0) <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent(TestEvent::Advance, 190.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Advance, 200.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100.0, 200.0, QList<SegmentData>() <<
                                                  SegmentData(110.0, 180.0, 1.0)),
                                  QList<double>() << 200.0)) <<
                (QList<BinData>() <<
                        BinData(100.0, 200.0,
                                QList<SegmentData>() << SegmentData(110.0, 180.0, 1.0)));
        QTest::newRow("Multiple value single segment") <<
                0 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::Value, 100.0, 140.0, 1.0) <<
                        TestEvent(TestEvent::Value, 140.0, 160.0, 2.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Value, 160.0, 200.0, 3.0) <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100.0, 200.0, QList<SegmentData>() <<
                                                  SegmentData(100.0, 140.0, 1.0) <<
                                                  SegmentData(140.0, 160.0, 2.0) <<
                                                  SegmentData(160.0, 200.0, 3.0)))) <<
                (QList<BinData>() <<
                        BinData(100.0, 200.0, QList<SegmentData>() <<
                                SegmentData(100.0, 140.0, 1.0) <<
                                SegmentData(140.0, 160.0, 2.0) <<
                                SegmentData(160.0, 200.0, 3.0)));
        QTest::newRow("Multiple value single segment ahead") <<
                0 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::Value, 100.0, 140.0, 1.0) <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent(TestEvent::Value, 140.0, 160.0, 2.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Value, 160.0, 200.0, 3.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100.0, 200.0, QList<SegmentData>() <<
                                                  SegmentData(100.0, 140.0, 1.0) <<
                                                  SegmentData(140.0, 160.0, 2.0) <<
                                                  SegmentData(160.0, 200.0, 3.0)),
                                  QList<double>() << 200.0)) <<
                (QList<BinData>() <<
                        BinData(100.0, 200.0, QList<SegmentData>() <<
                                SegmentData(100.0, 140.0, 1.0) <<
                                SegmentData(140.0, 160.0, 2.0) <<
                                SegmentData(160.0, 200.0, 3.0)));
        QTest::newRow("Multiple value single segment ahead gap") <<
                1 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::Value, 100.0, 140.0, 1.0) <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent(TestEvent::Value, 140.0, 160.0, 2.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Value, 170.0, 200.0, 3.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100.0, 200.0, QList<SegmentData>() <<
                                                  SegmentData(100.0, 140.0, 1.0) <<
                                                  SegmentData(140.0, 160.0, 2.0) <<
                                                  SegmentData(170.0, 200.0, 3.0)),
                                  QList<double>() << 200.0)) <<
                (QList<BinData>() <<
                        BinData(100.0, 200.0, QList<SegmentData>() <<
                                SegmentData(100.0, 140.0, 1.0) <<
                                SegmentData(140.0, 160.0, 2.0) <<
                                SegmentData(170.0, 200.0, 3.0)));
        QTest::newRow("Multiple value single segment discard") <<
                0 <<
                true <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::Value, 80.0, 110.0, 0.1) <<
                        TestEvent(TestEvent::Value, 110.0, 140.0, 1.0) <<
                        TestEvent(TestEvent::Value, 140.0, 160.0, 2.0) <<
                        TestEvent(TestEvent::Value, 160.0, 190.0, 3.0) <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Value, 190.0, 210.0, 0.2) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100.0, 200.0, QList<SegmentData>() <<
                                                  SegmentData(110.0, 140.0, 1.0) <<
                                                  SegmentData(140.0, 160.0, 2.0) <<
                                                  SegmentData(160.0, 190.0, 3.0)),
                                  QList<double>() << 210.0)) <<
                (QList<BinData>() <<
                        BinData(100.0, 200.0, QList<SegmentData>() <<
                                SegmentData(110.0, 140.0, 1.0) <<
                                SegmentData(140.0, 160.0, 2.0) <<
                                SegmentData(160.0, 190.0, 3.0)));
        QTest::newRow("Segment infinite start") <<
                0 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, FP::undefined()) <<
                        TestEvent(TestEvent::Value, FP::undefined(), 100.0, 1.0) <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Value, 100.0, 200.0, 2.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(FP::undefined(), 200.0, QList<SegmentData>() <<
                                                  SegmentData(FP::undefined(), 100.0, 1.0) <<
                                                  SegmentData(100.0, 200.0, 2.0)),
                                  QList<double>() << 200.0)) <<
                (QList<BinData>() <<
                        BinData(FP::undefined(), 200.0, QList<SegmentData>() <<
                                SegmentData(FP::undefined(), 100.0, 1.0) <<
                                SegmentData(100.0, 200.0, 2.0)));
        QTest::newRow("Segment infinite end value first") <<
                0 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::Value, FP::undefined(), 100.0, 1.0) <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::Value, 100.0, 200.0, 2.0) <<
                        TestEvent(TestEvent::Value, 200.0, FP::undefined(), 3.0) <<
                        TestEvent(TestEvent::SegmentEnd, FP::undefined()) <<
                        TestEvent(QList<BinData>(), QList<double>() << 100.0)) <<
                (QList<BinData>() <<
                        BinData(100.0, FP::undefined(), QList<SegmentData>() <<
                                SegmentData(100.0, 200.0, 2.0) <<
                                SegmentData(200.0, FP::undefined(), 3.0)));
        QTest::newRow("Segment infinite end value last") <<
                0 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::Value, FP::undefined(), 100.0, 1.0) <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::Value, 100.0, 200.0, 2.0) <<
                        TestEvent(TestEvent::SegmentEnd, FP::undefined()) <<
                        TestEvent(TestEvent::Value, 200.0, FP::undefined(), 3.0) <<
                        TestEvent(QList<BinData>(), QList<double>() << 100.0)) <<
                (QList<BinData>() <<
                        BinData(100.0, FP::undefined(), QList<SegmentData>() <<
                                SegmentData(100.0, 200.0, 2.0) <<
                                SegmentData(200.0, FP::undefined(), 3.0)));
        QTest::newRow("Single segment gap break") <<
                1 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::Value, 100, 120.0, 1.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Value, 190.0, 200.0, 2.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100, 120.0, QList<SegmentData>() <<
                                                  SegmentData(100, 120.0, 1.0))) <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100, 120.0, QList<SegmentData>() <<
                                                  SegmentData(100, 120.0, 1.0)) <<
                                          BinData(190, 200.0, QList<SegmentData>() <<
                                                  SegmentData(190, 200.0, 2.0)))) <<
                (QList<BinData>() <<
                        BinData(100, 120.0, QList<SegmentData>() << SegmentData(100, 120.0, 1.0)) <<
                        BinData(190, 200.0, QList<SegmentData>() << SegmentData(190, 200.0, 2.0)));
        QTest::newRow("Value into multiple segments") <<
                0 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent(TestEvent::SegmentStart, 200.0) <<
                        TestEvent(TestEvent::SegmentEnd, 300.0) <<
                        TestEvent(TestEvent::SegmentStart, 400.0) <<
                        TestEvent(TestEvent::SegmentEnd, 500.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Value, 90.0, 490.0, 1.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100, 200.0, QList<SegmentData>() <<
                                                  SegmentData(100, 200.0, 1.0)) <<
                                          BinData(200, 300.0, QList<SegmentData>() <<
                                                  SegmentData(200, 300.0, 1.0)))) <<
                (QList<BinData>() <<
                        BinData(100, 200.0, QList<SegmentData>() << SegmentData(100, 200.0, 1.0)) <<
                        BinData(200, 300.0, QList<SegmentData>() << SegmentData(200, 300.0, 1.0)) <<
                        BinData(400, 500.0, QList<SegmentData>() << SegmentData(400, 490.0, 1.0)));
        QTest::newRow("Value into multiple segments last open") <<
                0 <<
                false <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent(TestEvent::SegmentStart, 200.0) <<
                        TestEvent(TestEvent::SegmentEnd, 300.0) <<
                        TestEvent(TestEvent::SegmentStart, 400.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Value, 90.0, 500.0, 1.0) <<
                        TestEvent(QList<BinData>() <<
                                          BinData(100, 200.0, QList<SegmentData>() <<
                                                  SegmentData(100, 200.0, 1.0)) <<
                                          BinData(200, 300.0, QList<SegmentData>() <<
                                                  SegmentData(200, 300.0, 1.0))) <<
                        TestEvent(TestEvent::SegmentEnd, 500.0)) <<
                (QList<BinData>() <<
                        BinData(100, 200.0, QList<SegmentData>() << SegmentData(100, 200.0, 1.0)) <<
                        BinData(200, 300.0, QList<SegmentData>() << SegmentData(200, 300.0, 1.0)) <<
                        BinData(400, 500.0, QList<SegmentData>() << SegmentData(400, 500.0, 1.0)));
        QTest::newRow("Value into multiple segments discard") <<
                0 <<
                true <<
                (QList<TestEvent>() <<
                        TestEvent(TestEvent::SegmentStart, 100.0) <<
                        TestEvent(TestEvent::SegmentEnd, 200.0) <<
                        TestEvent(TestEvent::SegmentStart, 200.0) <<
                        TestEvent(TestEvent::SegmentEnd, 300.0) <<
                        TestEvent(TestEvent::SegmentStart, 400.0) <<
                        TestEvent(TestEvent::SegmentEnd, 500.0) <<
                        TestEvent() <<
                        TestEvent(TestEvent::Value, 90.0, 490.0, 1.0) <<
                        TestEvent(QList<BinData>(), QList<double>() << 200.0 << 300.0)) <<
                (QList<BinData>());
    }

    void conventional()
    {
        TestSimpleTarget targetValues;
        TestSimpleTarget targetCover;
        TestSimpleTargetGeneral targetStats;

        SmootherChainSegmentControlledConventional tc
                (new DynamicPrimitive<double>::Constant(0.1), NULL, false, &targetValues,
                 &targetCover, &targetStats);

        tc.getController()->segmentStart(1000.0);
        tc.getController()->segmentEnd(2000.0);
        tc.getController()->segmentStart(2000.0);
        tc.getController()->segmentEnd(3000.0);
        tc.getController()->segmentStart(3000.0);
        tc.getController()->segmentEnd(4000.0);
        tc.getController()->segmentStart(4000.0);

        tc.getTargetValue()->incomingData(1000.0, 1500.0, 1.0);
        tc.getTargetValue()->incomingData(1500.0, 2000.0, 2.0);
        tc.getTargetCover()->incomingAdvance(2000.0);
        QVERIFY(!targetValues.values.isEmpty());
        QCOMPARE(targetValues.values.last(), 1.5);
        QVERIFY(targetCover.values.isEmpty());
        QVERIFY(!targetStats.values.isEmpty());
        QCOMPARE(targetStats.values.last()["Mean"].toDouble(), 1.5);

        tc.getTargetValue()->incomingData(2000.0, 2500.0, 1.0);
        tc.getTargetValue()->incomingData(2500.0, 3000.0, FP::undefined());
        tc.getTargetCover()->incomingAdvance(3000.0);
        QCOMPARE(targetValues.values.last(), 1.0);
        QVERIFY(!targetCover.values.isEmpty());
        QCOMPARE(targetCover.values.last(), 0.5);

        tc.getController()->segmentEnd(5000.0);
        tc.getController()->segmentStart(5000.0);

        tc.getTargetValue()->incomingData(3000.0, 4000.0, FP::undefined());
        tc.getTargetCover()->incomingAdvance(4000.0);
        QVERIFY(!FP::defined(targetValues.values.last()));

        tc.getTargetValue()->incomingData(4000.0, 4800.0, 1.0);
        tc.getTargetValue()->incomingData(4800.0, 5000.0, 2.0);
        tc.getTargetCover()->incomingAdvance(5000.0);
        QCOMPARE(targetValues.values.last(), 1.2);

        tc.getTargetValue()->incomingData(5000.0, 5800.0, 1.0);
        tc.getTargetValue()->incomingData(5900.0, 6000.0, 2.0);
        tc.getController()->segmentEnd(6000.0);
        tc.getTargetCover()->incomingAdvance(6000.0);
        QCOMPARE(targetValues.values.last(), 1.0 / 0.9);
        QCOMPARE(targetCover.values.last(), 0.9);

        tc.getTargetValue()->incomingData(6000.0, 6500.0, 1.0);
        tc.getController()->segmentStart(6000.0);
        tc.getTargetValue()->incomingData(6500.0, 7000.0, 2.0);
        tc.getTargetCover()->incomingData(6000.0, 6500.0, 0.5);
        tc.getTargetCover()->incomingData(6500.0, 7000.0, 0.75);
        tc.getController()->segmentEnd(7000.0);
        QCOMPARE(targetValues.values.last(), 1.6);
        QCOMPARE(targetCover.values.last(), 0.625);

        tc.getController()->segmentStart(8000.0);
        tc.getController()->segmentEnd(9000.0);

        tc.getTargetValue()->incomingData(7000.0, 7500.0, 0.1);
        tc.getTargetCover()->incomingData(7250.0, 7500.0, 0.2);

        tc.getTargetValue()->incomingData(8000.0, 8500.0, 1.0);
        tc.getTargetValue()->incomingData(8500.0, 8750.0, 2.0);
        tc.getTargetValue()->incomingData(8750.0, 9000.0, 3.0);
        tc.getTargetCover()->incomingData(8000.0, 8500.0, 0.5);
        tc.getTargetCover()->incomingData(8500.0, 8750.0, 0.75);
        tc.getTargetCover()->incomingData(8750.0, 9000.0, 0.9);
        QCOMPARE(targetValues.values.last(), 1300.0 / (250 + 187.50 + 225.0));
        QCOMPARE(targetCover.values.last(), 0.6625);

        QCOMPARE((int) targetStats.values.last()["Count"].toInt64(), 3);
        QCOMPARE((int) targetStats.values.last()["UndefinedCount"].toInt64(), 0);
        QCOMPARE(targetStats.values.last()["Minimum"].toDouble(), 1.0);
        QCOMPARE(targetStats.values.last()["Maximum"].toDouble(), 3.0);
        QCOMPARE(targetStats.values.last()["Mean"].toDouble(), 2.0);
        QCOMPARE(targetStats.values.last()["Quantiles"].keyframe(0.5).toDouble(), 2.0);
        QCOMPARE(targetStats.values.last()["Quantiles"].keyframe(0.75).toDouble(), 2.5);

        tc.getController()->segmentStart(9000.0);
        tc.getController()->segmentEnd(10000.0);
        tc.getTargetValue()->incomingData(9000.0, 10000.0, 1.5);
        tc.getTargetCover()->incomingData(9000.0, 10000.0, 0.05);
        QVERIFY(!FP::defined(targetValues.values.last()));
        QCOMPARE(targetCover.values.last(), 0.05);

        tc.getTargetCover()->endData();
        tc.getTargetValue()->endData();
    }

    void difference()
    {
        TestSimpleTarget targetFirst;
        TestSimpleTarget targetLast;
        TestSimpleTarget targetCover;

        SmootherChainSegmentControlledDifference
                td(NULL, false, &targetFirst, &targetLast, &targetCover);

        td.getController()->segmentStart(1000.0);
        td.getController()->segmentEnd(2000.0);
        td.getController()->segmentStart(2000.0);
        td.getController()->segmentEnd(3000.0);
        td.getController()->segmentStart(3000.0);
        td.getController()->segmentEnd(4000.0);
        td.getController()->segmentStart(4000.0);
        td.getController()->segmentEnd(5000.0);
        td.getController()->segmentStart(5000.0);
        td.getController()->segmentEnd(6000.0);
        td.getController()->segmentStart(6000.0);
        td.getController()->segmentEnd(7000.0);
        td.getController()->segmentStart(7000.0);

        td.getTargetStart()->incomingData(1000.0, 2000.0, 1.00);
        td.getTargetEnd()->incomingAdvance(2000.0);
        QVERIFY(!targetFirst.values.isEmpty());
        QVERIFY(!targetLast.values.isEmpty());
        QVERIFY(targetCover.values.isEmpty());
        QCOMPARE(targetFirst.values.last(), 1.00);
        QVERIFY(!FP::defined(targetLast.values.last()));

        td.getTargetStart()->incomingData(2000.0, 3000.0, 1.01);
        td.getTargetEnd()->incomingData(2000.0, 3000.0, 2.01);
        QCOMPARE(targetFirst.values.last(), 1.01);
        QCOMPARE(targetLast.values.last(), 2.01);
        QVERIFY(targetCover.values.isEmpty());

        td.getTargetStart()->incomingData(3000.0, 3900.0, 1.02);
        td.getTargetEnd()->incomingData(3000.0, 3900.0, 2.02);
        td.getTargetStart()->incomingAdvance(4000.0);
        td.getTargetEnd()->incomingAdvance(4000.0);
        QCOMPARE(targetFirst.values.last(), 1.02);
        QCOMPARE(targetLast.values.last(), 2.02);
        QVERIFY(!targetCover.values.isEmpty());
        QCOMPARE(targetCover.values.last(), 0.9);

        td.getController()->segmentEnd(8000.0);
        td.getController()->segmentStart(8000.0);
        td.getController()->segmentEnd(9000.0);
        td.getController()->segmentStart(9000.0);
        td.getController()->segmentEnd(10000.0);
        td.getController()->segmentStart(10000.0);
        td.getController()->segmentEnd(11000.0);
        td.getController()->segmentStart(11000.0);
        td.getController()->segmentEnd(12000.0);

        td.getTargetStart()->incomingData(4000.0, 4500.0, 1.03);
        td.getTargetEnd()->incomingData(4000.0, 4500.0, 2.03);
        td.getTargetStart()->incomingData(4500.0, 5000.0, 1.04);
        td.getTargetEnd()->incomingData(4500.0, 5000.0, 2.04);
        QCOMPARE(targetFirst.values.last(), 1.03);
        QCOMPARE(targetLast.values.last(), 2.04);

        td.getTargetStart()->incomingData(5100.0, 5250.0, 1.05);
        td.getTargetEnd()->incomingData(5100.0, 5250.0, 2.05);
        td.getTargetStart()->incomingData(5250.0, 5400.0, 1.06);
        td.getTargetEnd()->incomingData(5250.0, 5400.0, 2.06);
        td.getTargetStart()->incomingData(5600.0, 5750.0, 1.07);
        td.getTargetEnd()->incomingData(5600.0, 5750.0, 2.07);
        td.getTargetStart()->incomingData(5750.0, 5900.0, 1.08);
        td.getTargetEnd()->incomingData(5750.0, 5900.0, 2.08);
        td.getTargetEnd()->incomingAdvance(6000.0);
        td.getTargetStart()->incomingAdvance(6000.0);
        QCOMPARE(targetFirst.values.last(), 1.05);
        QCOMPARE(targetLast.values.last(), 2.08);
        QCOMPARE(targetCover.values.last(), 0.6);

        td.getTargetStart()->incomingData(6000.0, 6500.0, FP::undefined());
        td.getTargetEnd()->incomingData(6000.0, 6500.0, FP::undefined());
        td.getTargetStart()->incomingData(6500.0, 7000.0, 1.09);
        td.getTargetEnd()->incomingData(6500.0, 7000.0, 2.09);
        QCOMPARE(targetFirst.values.last(), 1.09);
        QCOMPARE(targetLast.values.last(), 2.09);
        QCOMPARE(targetCover.values.last(), 0.5);

        td.getTargetStart()->incomingData(7000.0, 7500.0, 1.10);
        td.getTargetEnd()->incomingData(7000.0, 7500.0, 2.10);
        td.getTargetStart()->incomingData(7500.0, 8000.0, FP::undefined());
        td.getTargetEnd()->incomingData(7500.0, 8000.0, FP::undefined());
        QCOMPARE(targetFirst.values.last(), 1.10);
        QCOMPARE(targetLast.values.last(), 2.10);
        QCOMPARE(targetCover.values.last(), 0.5);

        td.getTargetStart()->incomingData(8000.0, 8500.0, 1.10);
        td.getTargetEnd()->incomingData(8000.0, 8500.0, 2.10);
        td.getTargetStart()->incomingData(8600.0, 9000.0, 1.11);
        td.getTargetEnd()->incomingData(8600.0, 9000.0, FP::undefined());
        QCOMPARE(targetFirst.values.last(), 1.10);
        QCOMPARE(targetLast.values.last(), 1.11);
        QCOMPARE(targetCover.values.last(), 0.9);

        td.getTargetStart()->incomingData(9000.0, 9500.0, FP::undefined());
        td.getTargetEnd()->incomingData(9000.0, 9500.0, 2.12);
        td.getTargetStart()->incomingData(9500.0, 10000.0, 1.13);
        td.getTargetEnd()->incomingData(9500.0, 10000.0, FP::undefined());
        QCOMPARE(targetFirst.values.last(), 2.12);
        QCOMPARE(targetLast.values.last(), 1.13);

        td.getTargetStart()->incomingData(10000.0, 10500.0, FP::undefined());
        td.getTargetEnd()->incomingData(10000.0, 10500.0, 2.14);
        td.getTargetStart()->incomingData(10600.0, 11000.0, 1.14);
        td.getTargetEnd()->incomingData(10600.0, 11000.0, 2.15);
        QCOMPARE(targetFirst.values.last(), 2.14);
        QCOMPARE(targetLast.values.last(), 2.15);
        QCOMPARE(targetCover.values.last(), 0.4);

        td.getTargetStart()->incomingData(11000.0, 11500.0, 1.16);
        td.getTargetEnd()->incomingData(11000.0, 11500.0, 2.16);
        td.getTargetStart()->incomingData(11600.0, 11750.0, FP::undefined());
        td.getTargetEnd()->incomingData(11600.0, 11750.0, FP::undefined());
        td.getTargetStart()->incomingData(11750.0, 12000.0, 1.17);
        td.getTargetEnd()->incomingData(11750.0, 12000.0, 2.17);
        QCOMPARE(targetFirst.values.last(), 1.16);
        QCOMPARE(targetLast.values.last(), 2.17);
        QCOMPARE(targetCover.values.last(), 0.9);

        td.getTargetStart()->endData();
        td.getTargetEnd()->endData();
    }

    void differenceGap()
    {
        TestSimpleTarget targetFirst;
        TestSimpleTarget targetLast;
        TestSimpleTarget targetCover;

        SmootherChainSegmentControlledDifference td
                (new DynamicTimeInterval::Constant(Time::Millisecond, 1, false), false,
                 &targetFirst, &targetLast, &targetCover);

        td.getController()->segmentStart(1000.0);
        td.getController()->segmentEnd(2000.0);
        td.getController()->segmentStart(2000.0);
        td.getController()->segmentEnd(3000.0);
        td.getController()->segmentStart(3000.0);
        td.getController()->segmentEnd(4000.0);
        td.getController()->segmentStart(4000.0);
        td.getController()->segmentEnd(5000.0);
        td.getController()->segmentStart(5000.0);
        td.getController()->segmentEnd(6000.0);
        td.getController()->segmentStart(6000.0);
        td.getController()->segmentEnd(7000.0);
        td.getController()->segmentStart(7000.0);
        td.getController()->segmentEnd(8000.0);
        td.getController()->segmentStart(8000.0);

        td.getTargetStart()->incomingData(1000.0, 2000.0, 1.00);
        td.getTargetEnd()->incomingAdvance(2000.0);
        QVERIFY(!targetFirst.values.isEmpty());
        QVERIFY(!targetLast.values.isEmpty());
        QVERIFY(targetCover.values.isEmpty());
        QCOMPARE(targetFirst.values.last(), 1.00);
        QVERIFY(!FP::defined(targetLast.values.last()));

        td.getTargetStart()->incomingData(2000.0, 3000.0, 1.01);
        td.getTargetEnd()->incomingData(2000.0, 3000.0, 2.01);
        QCOMPARE(targetFirst.values.last(), 1.01);
        QCOMPARE(targetLast.values.last(), 2.01);
        QVERIFY(targetCover.values.isEmpty());

        td.getTargetStart()->incomingData(3000.0, 3900.0, 1.02);
        td.getTargetEnd()->incomingData(3000.0, 3900.0, 2.02);
        td.getTargetStart()->incomingAdvance(3999.0);
        td.getTargetEnd()->incomingAdvance(3999.0);
        td.getTargetStart()->incomingAdvance(4000.0);
        td.getTargetEnd()->incomingAdvance(4000.0);
        QCOMPARE(targetFirst.values.last(), 1.02);
        QCOMPARE(targetLast.values.last(), 2.02);
        QVERIFY(targetCover.values.isEmpty());

        td.getTargetStart()->incomingData(4000.0, 4500.0, 1.03);
        td.getTargetEnd()->incomingData(4000.0, 4500.0, 2.03);
        td.getTargetStart()->incomingData(4500.0, 5000.0, 1.04);
        td.getTargetEnd()->incomingData(4500.0, 5000.0, 2.04);
        QCOMPARE(targetFirst.values.last(), 1.03);
        QCOMPARE(targetLast.values.last(), 2.04);
        QVERIFY(targetCover.values.isEmpty());

        td.getTargetStart()->incomingData(5100.0, 5250.0, 1.05);
        td.getTargetEnd()->incomingAdvance(5100.0);
        td.getTargetEnd()->incomingData(5100.0, 5250.0, 2.05);
        td.getTargetStart()->incomingData(5250.0, 5400.0, 1.06);
        td.getTargetEnd()->incomingData(5250.0, 5400.0, 2.06);
        td.getTargetStart()->incomingData(5600.0, 5750.0, 1.07);
        td.getTargetEnd()->incomingData(5600.0, 5750.0, 2.07);
        QCOMPARE(targetFirst.starts.last(), 5100.0);
        QCOMPARE(targetFirst.ends.last(), 5400.0);
        QCOMPARE(targetFirst.values.last(), 1.05);
        QCOMPARE(targetLast.values.last(), 2.06);
        QVERIFY(targetCover.values.isEmpty());
        td.getTargetStart()->incomingData(5750.0, 5900.0, 1.08);
        td.getTargetEnd()->incomingData(5750.0, 5900.0, 2.08);
        td.getTargetEnd()->incomingAdvance(6000.0);
        td.getTargetStart()->incomingAdvance(6000.0);
        QCOMPARE(targetFirst.starts.last(), 5600.0);
        QCOMPARE(targetFirst.values.last(), 1.07);
        QCOMPARE(targetLast.values.last(), 2.08);

        td.getTargetEnd()->incomingAdvance(7000.0);
        td.getTargetStart()->incomingAdvance(7000.0);
        td.getTargetStart()->incomingData(7000.0, 7250.0, 1.09);
        td.getTargetEnd()->incomingData(7000.0, 7250.0, 2.09);
        td.getTargetStart()->incomingData(7250.0, 7500.0, 1.10);
        td.getTargetEnd()->incomingData(7250.0, 7500.0, 2.10);

        td.getTargetStart()->incomingData(8000.0, 8500.0, 1.11);
        td.getTargetEnd()->incomingData(8000.0, 8500.0, 2.11);

        QCOMPARE(targetFirst.starts.last(), 7000.0);
        QCOMPARE(targetFirst.ends.last(), 7500.0);
        QCOMPARE(targetFirst.values.last(), 1.09);
        QCOMPARE(targetLast.values.last(), 2.10);

        td.getTargetStart()->endData();
        td.getTargetEnd()->endData();
    }

    void flags()
    {
        TestSimpleTargetFlags targetFlags;
        TestSimpleTarget targetCover;

        SmootherChainSegmentControlledFlags fl(NULL, false, &targetFlags, &targetCover);

        fl.getController()->segmentStart(1000.0);
        fl.getTargetFlags()->incomingData(1000.0, 2000.0, Variant::Flags{"F01"});
        fl.getTargetCover()->incomingAdvance(2000.0);
        QVERIFY(targetFlags.values.isEmpty());
        QVERIFY(targetCover.values.isEmpty());
        fl.getController()->segmentEnd(2000.0);
        QVERIFY(!targetFlags.values.isEmpty());
        QVERIFY(targetCover.values.isEmpty());
        QCOMPARE(targetFlags.values.last(), Variant::Flags{"F01"});

        fl.getController()->segmentStart(2000.0);
        fl.getTargetFlags()->incomingData(2000.0, 2500.0, Variant::Flags{"F02"});
        fl.getTargetFlags()->incomingData(2500.0, 3000.0, Variant::Flags{"F03"});
        fl.getController()->segmentEnd(3000.0);
        fl.getController()->segmentStart(3000.0);
        fl.getTargetCover()->incomingAdvance(3000.0);
        QCOMPARE(targetFlags.values.last(), (Variant::Flags{"F02", "F03"}));
        QVERIFY(targetCover.values.isEmpty());

        fl.getController()->segmentEnd(4000.0);
        fl.getTargetFlags()->incomingData(3000.0, 3500.0, Variant::Flags{"F04"});
        fl.getTargetFlags()->incomingData(3600.0, 4000.0, Variant::Flags{"F04"});
        fl.getTargetCover()->incomingAdvance(4000.0);
        QCOMPARE(targetFlags.values.last(), Variant::Flags{"F04"});
        QVERIFY(!targetCover.values.isEmpty());
        QCOMPARE(targetCover.values.last(), 0.9);

        fl.getController()->segmentStart(4000.0);
        fl.getController()->segmentEnd(5000.0);
        fl.getTargetFlags()->incomingData(4000.0, 4500.0, Variant::Flags{"F05", "F06"});
        fl.getTargetFlags()->incomingAdvance(5000.0);
        fl.getTargetCover()->incomingAdvance(5000.0);
        QCOMPARE(targetFlags.values.last(), (Variant::Flags{"F05", "F06"}));
        QCOMPARE(targetCover.values.last(), 0.5);

        fl.getController()->segmentStart(5000.0);
        fl.getTargetFlags()->incomingData(5000.0, 5500.0, Variant::Flags{"F06"});
        fl.getTargetCover()->incomingData(5000.0, 5500.0, 0.5);
        fl.getTargetFlags()->incomingData(5500.0, 6000.0, Variant::Flags{"F07"});
        fl.getTargetCover()->incomingData(5500.0, 6000.0, 1.0);
        fl.getController()->segmentEnd(6000.0);
        QCOMPARE(targetFlags.values.last(), (Variant::Flags{"F06", "F07"}));
        QCOMPARE(targetCover.values.last(), 0.75);
    }

    void vectorStats()
    {
        TestSimpleTarget targetValues;
        TestSimpleTarget targetCover;
        TestSimpleTargetGeneral targetStats;

        SmootherChainSegmentControlledVectorStatistics tc
                (new DynamicPrimitive<double>::Constant(0.1), NULL, false, &targetValues,
                 &targetCover, &targetStats);

        tc.getController()->segmentStart(1000.0);
        tc.getController()->segmentEnd(2000.0);

        tc.getTargetValue()->incomingData(1000.0, 2000.0, 1.5);
        tc.getTargetCover()->incomingData(1000.0, 2000.0, 0.05);
        tc.getTargetVectoredMagnitude()->incomingData(1000.0, 2000.0, 1.6);
        QVERIFY(!targetCover.values.isEmpty());
        QVERIFY(!targetValues.values.isEmpty());
        QVERIFY(!FP::defined(targetValues.values.last()));
        QCOMPARE(targetCover.values.last(), 0.05);

        tc.getController()->segmentStart(7000.0);
        tc.getController()->segmentEnd(8000.0);

        tc.getTargetValue()->incomingData(7000.0, 7500.0, 1.0);
        tc.getTargetValue()->incomingData(7500.0, 7750.0, 2.0);
        tc.getTargetValue()->incomingData(7750.0, 8000.0, 3.0);
        tc.getTargetCover()->incomingData(7000.0, 7500.0, 0.5);
        tc.getTargetCover()->incomingData(7500.0, 7750.0, 0.75);
        tc.getTargetCover()->incomingData(7750.0, 8000.0, 0.9);
        tc.getTargetVectoredMagnitude()
          ->incomingData(7000.0, 8000.0, 1300.0 / (250 + 187.50 + 225.0) * 0.9);
        QCOMPARE(targetValues.values.last(), 1300.0 / (250 + 187.50 + 225.0));
        QCOMPARE(targetCover.values.last(), 0.6625);

        tc.getTargetCover()->endData();
        tc.getTargetValue()->endData();
        tc.getTargetVectoredMagnitude()->endData();

        QVERIFY(!targetStats.values.isEmpty());
        QCOMPARE((int) targetStats.values.last()["Count"].toInt64(), 3);
        QCOMPARE((int) targetStats.values.last()["UndefinedCount"].toInt64(), 0);
        QCOMPARE(targetStats.values.last()["Minimum"].toDouble(), 1.0);
        QCOMPARE(targetStats.values.last()["Maximum"].toDouble(), 3.0);
        QCOMPARE(targetStats.values.last()["Mean"].toDouble(), 2.0);
        QCOMPARE(targetStats.values.last()["StabilityFactor"].toDouble(), 0.9);
        QCOMPARE(targetStats.values.last()["Quantiles"].keyframe(0.5).toDouble(), 2.0);
        QCOMPARE(targetStats.values.last()["Quantiles"].keyframe(0.75).toDouble(), 2.5);
    }
};

QTEST_APPLESS_MAIN(TestSegmentControlled)

#include "segmentcontrolled.moc"
