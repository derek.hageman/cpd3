/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>
#include <QVector>

#include "smoothing/binnedchain.hxx"
#include "core/number.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "algorithms/dewpoint.hxx"

using namespace CPD3;
using namespace CPD3::Smoothing;
using namespace CPD3::Data;
using namespace CPD3::Algorithms;

class TestChainNodeCommon : public SmootherChainNode {
public:
    int uid;

    TestChainNodeCommon(int &guid) : uid(guid)
    { ++guid; }

    TestChainNodeCommon(QDataStream &stream)
    {
        quint32 ds;
        stream >> ds;
        uid = ds;
    }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint32) uid;
    }
};

class TestChainEngine : public virtual SmootherChainCoreEngineInterface {
public:
    SinkMultiplexer mux;

    QList<SmootherChainNode *> chain;

    virtual void addChainNode(SmootherChainNode *add)
    { chain.append(add); }

    class TestChainOutput : public SmootherChainTarget {
    public:
        SinkMultiplexer::Sink *ingress;
        SequenceName unit;
        bool ended;

        TestChainOutput(SinkMultiplexer::Sink *i, const SequenceName &u) : ingress(i),
                                                                           unit(u),
                                                                           ended(false)
        { }

        virtual ~TestChainOutput()
        { }

        virtual void incomingData(double start, double end, double value)
        {
            Q_ASSERT(!ended);
            ingress->incomingData(SequenceValue(unit, Variant::Root(value), start, end));
        }

        virtual void incomingAdvance(double time)
        {
            Q_ASSERT(!ended);
            ingress->incomingAdvance(time);
        }

        virtual void endData()
        {
            Q_ASSERT(!ended);
            ingress->endData();
            ended = true;
        }
    };

    QList<TestChainOutput *> outputs;

    virtual SmootherChainTarget *addNewOutput(const SequenceName &unit)
    {
        for (QList<TestChainOutput *>::const_iterator check = outputs.constBegin(),
                end = outputs.constEnd(); check != end; ++check) {
            if ((*check)->unit == unit) {
                qDebug() << "Duplicate output added:" << unit;
                return *check;
            }
        }
        TestChainOutput *add = new TestChainOutput(mux.createSegmented(), unit);
        outputs.append(add);
        return add;
    }

    virtual SmootherChainTarget *getOutput(int index)
    {
        Q_ASSERT(index >= 0 && index < outputs.size());
        return outputs.at(index);
    }

    class TestChainOutputFlags : public SmootherChainTargetFlags {
    public:
        SinkMultiplexer::Sink *ingress;
        SequenceName unit;
        bool ended;

        TestChainOutputFlags(SinkMultiplexer::Sink *i, const SequenceName &u) : ingress(i),
                                                                                unit(u),
                                                                                ended(false)
        { }

        virtual ~TestChainOutputFlags()
        { }

        virtual void incomingData(double start, double end, const Data::Variant::Flags &value)
        {
            Q_ASSERT(!ended);
            ingress->incomingData(SequenceValue(unit, Variant::Root(value), start, end));
        }

        virtual void incomingAdvance(double time)
        {
            Q_ASSERT(!ended);
            ingress->incomingAdvance(time);
        }

        virtual void endData()
        {
            Q_ASSERT(!ended);
            ingress->endData();
            ended = true;
        }
    };

    QList<TestChainOutputFlags *> outputsFlags;

    virtual SmootherChainTargetFlags *addNewFlagsOutput(const SequenceName &unit)
    {
        for (QList<TestChainOutputFlags *>::const_iterator check = outputsFlags.constBegin(),
                end = outputsFlags.constEnd(); check != end; ++check) {
            if ((*check)->unit == unit) {
                qDebug() << "Duplicate flags output added:" << unit;
                return *check;
            }
        }
        TestChainOutputFlags *add = new TestChainOutputFlags(mux.createSegmented(), unit);
        outputsFlags.append(add);
        return add;
    }

    virtual SmootherChainTargetFlags *getFlagsOutput(int index)
    {
        Q_ASSERT(index >= 0 && index <= outputsFlags.size());
        return outputsFlags.at(index);
    }

    class TestChainOutputGeneral : public SmootherChainTargetGeneral {
    public:
        SinkMultiplexer::Sink *ingress;
        SequenceName unit;
        bool ended;

        TestChainOutputGeneral(SinkMultiplexer::Sink *i, const SequenceName &u) : ingress(i),
                                                                                  unit(u),
                                                                                  ended(false)
        { }

        virtual ~TestChainOutputGeneral()
        { }

        virtual void incomingData(double start, double end, const Variant::Root &value)
        {
            Q_ASSERT(!ended);
            ingress->incomingData(SequenceValue(unit, value, start, end));
        }

        virtual void incomingAdvance(double time)
        {
            Q_ASSERT(!ended);
            ingress->incomingAdvance(time);
        }

        virtual void endData()
        {
            Q_ASSERT(!ended);
            ingress->endData();
            ended = true;
        }
    };

    QList<TestChainOutputGeneral *> outputsGeneral;

    virtual SmootherChainTargetGeneral *addNewGeneralOutput(const SequenceName &unit)
    {
        for (QList<TestChainOutputGeneral *>::const_iterator check = outputsGeneral.constBegin(),
                end = outputsGeneral.constEnd(); check != end; ++check) {
            if ((*check)->unit == unit) {
                qDebug() << "Duplicate general output added:" << unit;
                return *check;
            }
        }
        TestChainOutputGeneral *add = new TestChainOutputGeneral(mux.createSegmented(), unit);
        outputsGeneral.append(add);
        return add;
    }

    virtual SmootherChainTargetGeneral *getGeneralOutput(int index)
    {
        Q_ASSERT(index >= 0 && index <= outputsGeneral.size());
        return outputsGeneral.at(index);
    }


    struct TestChainInput {
        SequenceName unit;
        QList<SmootherChainTarget *> targets;

        void incomingData(double start, double end, double value)
        {
            for (QList<SmootherChainTarget *>::const_iterator t = targets.constBegin(),
                    endT = targets.constEnd(); t != endT; ++t) {
                (*t)->incomingData(start, end, value);
            }
        }

        void incomingAdvance(double time)
        {
            for (QList<SmootherChainTarget *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->incomingAdvance(time);
            }
        }

        void endData()
        {
            for (QList<SmootherChainTarget *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->endData();
            }
        }
    };

    QList<TestChainInput> inputs;

    virtual void addNewInput(const SequenceName &unit, SmootherChainTarget *target)
    {
        for (QList<TestChainInput>::iterator it = inputs.begin(); it != inputs.end(); ++it) {
            if (it->unit == unit) {
                if (target != NULL)
                    it->targets.append(target);
                return;
            }
        }
        inputs.append(TestChainInput());
        inputs.last().unit = unit;
        if (target != NULL)
            inputs.last().targets.append(target);
    }

    virtual void addInputTarget(int index, SmootherChainTarget *target)
    {
        Q_ASSERT(index >= 0 && index < inputs.size());
        Q_ASSERT(target != NULL);
        inputs[index].targets.append(target);
    }

    void advanceAllInputsAfter(int index, double time)
    {
        for (int i = index; i < inputs.size(); i++) {
            inputs[i].incomingAdvance(time);
        }
    }

    struct TestChainInputFlags {
        SequenceName unit;
        QList<SmootherChainTargetFlags *> targets;

        void incomingData(double start, double end, const Data::Variant::Flags &value)
        {
            for (QList<SmootherChainTargetFlags *>::const_iterator t = targets.constBegin(),
                    endT = targets.constEnd(); t != endT; ++t) {
                (*t)->incomingData(start, end, value);
            }
        }

        void incomingAdvance(double time)
        {
            for (QList<SmootherChainTargetFlags *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->incomingAdvance(time);
            }
        }

        void endData()
        {
            for (QList<SmootherChainTargetFlags *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->endData();
            }
        }
    };

    QList<TestChainInputFlags> inputsFlags;

    virtual void addNewFlagsInput(const SequenceName &unit, SmootherChainTargetFlags *target)
    {
        for (QList<TestChainInputFlags>::iterator it = inputsFlags.begin();
                it != inputsFlags.end();
                ++it) {
            if (it->unit == unit) {
                if (target != NULL)
                    it->targets.append(target);
                return;
            }
        }
        inputsFlags.append(TestChainInputFlags());
        inputsFlags.last().unit = unit;
        if (target != NULL)
            inputsFlags.last().targets.append(target);
    }

    virtual void addFlagsInputTarget(int index, SmootherChainTargetFlags *target)
    {
        Q_ASSERT(index >= 0 && index < inputsFlags.size());
        Q_ASSERT(target != NULL);
        inputsFlags[index].targets.append(target);
    }

    void advanceAllInputsFlagsAfter(int index, double time)
    {
        for (int i = index; i < inputsFlags.size(); i++) {
            inputsFlags[i].incomingAdvance(time);
        }
    }

    struct TestChainInputGeneral {
        SequenceName unit;
        QList<SmootherChainTargetGeneral *> targets;

        void incomingData(double start, double end, const Variant::Root &value)
        {
            for (QList<SmootherChainTargetGeneral *>::const_iterator t = targets.constBegin(),
                    endT = targets.constEnd(); t != endT; ++t) {
                (*t)->incomingData(start, end, value);
            }
        }

        void incomingAdvance(double time)
        {
            for (QList<SmootherChainTargetGeneral *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->incomingAdvance(time);
            }
        }

        void endData()
        {
            for (QList<SmootherChainTargetGeneral *>::const_iterator t = targets.constBegin(),
                    end = targets.constEnd(); t != end; ++t) {
                (*t)->endData();
            }
        }
    };

    QList<TestChainInputGeneral> inputsGeneral;

    virtual void addNewGeneralInput(const SequenceName &unit, SmootherChainTargetGeneral *target)
    {
        for (QList<TestChainInputGeneral>::iterator it = inputsGeneral.begin();
                it != inputsGeneral.end();
                ++it) {
            if (it->unit == unit) {
                if (target != NULL)
                    it->targets.append(target);
                return;
            }
        }
        inputsGeneral.append(TestChainInputGeneral());
        inputsGeneral.last().unit = unit;
        if (target != NULL)
            inputsGeneral.last().targets.append(target);
    }

    virtual void addGeneralInputTarget(int index, SmootherChainTargetGeneral *target)
    {
        Q_ASSERT(index >= 0 && index < inputsGeneral.size());
        Q_ASSERT(target != NULL);
        inputsGeneral[index].targets.append(target);
    }

    void advanceAllInputsGeneralAfter(int index, double time)
    {
        for (int i = index; i < inputsGeneral.size(); i++) {
            inputsGeneral[i].incomingAdvance(time);
        }
    }


    void endAll()
    {
        mux.sinkCreationComplete();

        for (QList<TestChainInput>::iterator it = inputs.begin(); it != inputs.end(); ++it) {
            it->endData();
        }
        for (QList<TestChainInputFlags>::iterator it = inputsFlags.begin();
                it != inputsFlags.end();
                ++it) {
            it->endData();
        }
        for (QList<TestChainInputGeneral>::iterator it = inputsGeneral.begin();
                it != inputsGeneral.end();
                ++it) {
            it->endData();
        }
    }


    TestChainEngine()
    {
        mux.start();
    }

    virtual ~TestChainEngine()
    {
        qDeleteAll(chain);
        qDeleteAll(outputs);
        qDeleteAll(outputsFlags);
        qDeleteAll(outputsGeneral);
    }


    virtual SequenceName getBaseUnit(int index = 0)
    {
        return SequenceName("bnd", "raw", "var" + std::to_string(index + 1));
    }

    virtual SinkMultiplexer::Sink *addNewFinalOutput()
    { return mux.createSink(); }

    virtual Variant::Read getOptions()
    { return Variant::Read::empty(); }

    void serialize(QDataStream &stream)
    {
        mux.setEgress(NULL);
        for (int i = chain.size() - 1; i >= 0; i--) {
            chain[i]->pause();
        }

        stream << (quint32) outputs.size();
        for (int i = 0; i < outputs.size(); i++) {
            stream << outputs.at(i)->unit << outputs.at(i)->ended;
        }
        stream << (quint32) outputsFlags.size();
        for (int i = 0; i < outputsFlags.size(); i++) {
            stream << outputsFlags.at(i)->unit << outputsFlags.at(i)->ended;
        }
        stream << (quint32) outputsGeneral.size();
        for (int i = 0; i < outputsGeneral.size(); i++) {
            stream << outputsGeneral.at(i)->unit << outputsGeneral.at(i)->ended;
        }

        stream << (quint32) inputs.size();
        for (int i = 0; i < inputs.size(); i++) {
            stream << inputs.at(i).unit;
        }
        stream << (quint32) inputsFlags.size();
        for (int i = 0; i < inputsFlags.size(); i++) {
            stream << inputsFlags.at(i).unit;
        }
        stream << (quint32) inputsGeneral.size();
        for (int i = 0; i < inputsGeneral.size(); i++) {
            stream << inputsGeneral.at(i).unit;
        }

        stream << mux;

        for (int i = 0; i < chain.size(); i++) {
            chain[i]->serialize(stream);
        }
        for (int i = 0; i < chain.size(); i++) {
            chain[i]->resume();
        }
    }

    void deserialize(QDataStream &stream)
    {
        quint32 n;
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            SequenceName u;
            stream >> u;
            bool ended;
            stream >> ended;
            SmootherChainTarget *o = addNewOutput(u);
            if (ended)
                o->endData();
        }
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            SequenceName u;
            stream >> u;
            bool ended;
            stream >> ended;
            SmootherChainTargetFlags *o = addNewFlagsOutput(u);
            if (ended)
                o->endData();
        }
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            SequenceName u;
            stream >> u;
            bool ended;
            stream >> ended;
            Q_ASSERT(ended);
            ended = false;
            SmootherChainTargetGeneral *o = addNewGeneralOutput(u);
            if (ended)
                o->endData();
        }

        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            TestChainInput add;
            stream >> add.unit;
            inputs.append(add);
        }
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            TestChainInputFlags add;
            stream >> add.unit;
            inputsFlags.append(add);
        }
        stream >> n;
        for (int i = 0; i < (int) n; i++) {
            TestChainInputGeneral add;
            stream >> add.unit;
            inputsGeneral.append(add);
        }

        stream >> mux;
    }
};

template<int NInputs, int NOutputs>
class TestChainNode : public TestChainNodeCommon {
public:
    int uid;

    SmootherChainTarget *outputs[NOutputs];
    bool outputsEnded[NOutputs];

    class Input : public virtual SmootherChainTarget {
    public:
        Input() : index(-1), parent(NULL), lastTime(FP::undefined()), ended(false)
        { }

        virtual ~Input()
        { }

        int index;
        TestChainNode<NInputs, NOutputs> *parent;
        double lastTime;
        bool ended;

        virtual void incomingData(double start, double end, double value)
        {
            if (NInputs == NOutputs) {
                if (FP::defined(value))
                    parent->outputs[index]->incomingData(start, end, value + 1.0);
                else
                    parent->outputs[index]->incomingData(start, end, value);
            } else if (NInputs < NOutputs) {
                if (FP::defined(value)) {
                    parent->outputs[index]->incomingData(start, end, value + 1.0);
                    if (index == NInputs - 1) {
                        for (int i = NInputs; i < NOutputs; i++) {
                            parent->outputs[i]->incomingData(start, end, value + 2.0 + i);
                        }
                    }
                } else {
                    parent->outputs[index]->incomingData(start, end, value);
                    if (index == NInputs - 1) {
                        for (int i = NInputs; i < NOutputs; i++) {
                            parent->outputs[i]->incomingData(start, end, value);
                        }
                    }
                }
            } else {
                if (index < NOutputs) {
                    if (FP::defined(value)) {
                        parent->outputs[index]->incomingData(start, end, value + 1.0);
                    } else {
                        parent->outputs[index]->incomingData(start, end, value);
                    }
                }
                if (FP::defined(end)) {
                    bool hit = true;
                    for (int i = 0; i < NInputs; i++) {
                        if (i == index)
                            continue;
                        if (!FP::defined(parent->inputs[i].lastTime) &&
                                parent->inputs[i].lastTime < end) {
                            hit = false;
                            break;
                        }
                    }
                    if (hit) {
                        for (int i = NInputs; i < NOutputs; i++) {
                            if (FP::defined(value)) {
                                parent->outputs[i]->incomingData(start, end, value + 1.0);
                            } else {
                                parent->outputs[i]->incomingData(start, end, value);
                            }
                        }
                    }
                }
            }
            lastTime = end;
        }

        virtual void incomingAdvance(double time)
        {
            if (NInputs == NOutputs) {
                parent->outputs[index]->incomingAdvance(time);
            } else if (NInputs < NOutputs) {
                parent->outputs[index]->incomingAdvance(time);
                if (index == NInputs - 1) {
                    for (int i = NInputs; i < NOutputs; i++) {
                        parent->outputs[i]->incomingAdvance(time);
                    }
                }
            } else {
                if (index < NOutputs)
                    parent->outputs[index]->incomingAdvance(time);
                bool hit = true;
                for (int i = 0; i < NInputs; i++) {
                    if (i == index)
                        continue;
                    if (!FP::defined(parent->inputs[i].lastTime) &&
                            parent->inputs[i].lastTime < time) {
                        hit = false;
                        break;
                    }
                }
                if (hit) {
                    for (int i = NInputs; i < NOutputs; i++) {
                        parent->outputs[i]->incomingAdvance(time);
                    }
                }
            }

            lastTime = time;
        }

        virtual void endData()
        {
            ended = true;
            if (NInputs == NOutputs) {
                parent->outputs[index]->endData();
                parent->outputsEnded[index] = true;
            } else if (NInputs < NOutputs) {
                parent->outputs[index]->endData();
                parent->outputsEnded[index] = true;
                if (index == NInputs - 1) {
                    for (int i = NInputs; i < NOutputs; i++) {
                        parent->outputs[i]->endData();
                        parent->outputsEnded[i] = true;
                    }
                }
            } else {
                if (index < NOutputs) {
                    parent->outputs[index]->endData();
                    parent->outputsEnded[index] = true;
                }

                bool hit = true;
                for (int i = 0; i < NInputs; i++) {
                    if (i == index)
                        continue;
                    if (!parent->inputs[i].ended) {
                        hit = false;
                        break;
                    }
                }
                if (hit) {
                    for (int i = NInputs; i < NOutputs; i++) {
                        parent->outputs[i]->endData();
                        parent->outputsEnded[i] = true;
                    }
                }
            }

            lastTime = FP::undefined();
        }
    };

    Input inputs[NInputs];

    TestChainNode(int &guid,
                  SmootherChainTarget *t1 = NULL,
                  SmootherChainTarget *t2 = NULL,
                  SmootherChainTarget *t3 = NULL,
                  SmootherChainTarget *t4 = NULL) : TestChainNodeCommon(guid)
    {
        Q_ASSERT(NOutputs > 0 && NOutputs <= 4);
        outputs[0] = t1;
        if (NOutputs > 1)
            outputs[1] = t2;
        if (NOutputs > 2)
            outputs[2] = t3;
        if (NOutputs > 3)
            outputs[3] = t4;
        for (int i = 0; i < NInputs; i++) {
            inputs[i].index = i;
            inputs[i].parent = this;
        }
        memset(outputsEnded, 0, sizeof(outputsEnded));
    }

    TestChainNode(QDataStream &stream,
                  SmootherChainTarget *t1 = NULL,
                  SmootherChainTarget *t2 = NULL,
                  SmootherChainTarget *t3 = NULL,
                  SmootherChainTarget *t4 = NULL) : TestChainNodeCommon(stream)
    {
        Q_ASSERT(NOutputs > 0 && NOutputs <= 4);
        outputs[0] = t1;
        if (NOutputs > 1)
            outputs[1] = t2;
        if (NOutputs > 2)
            outputs[2] = t3;
        if (NOutputs > 3)
            outputs[3] = t4;
        for (int i = 0; i < NInputs; i++) {
            inputs[i].index = i;
            inputs[i].parent = this;
        }
        memset(outputsEnded, 0, sizeof(outputsEnded));

        quint32 n = 0;
        stream >> n;
        Q_ASSERT(n == NInputs);
        stream >> n;
        Q_ASSERT(n == NOutputs);
        for (int i = 0; i < NInputs; i++) {
            stream >> inputs[i].ended;
        }
        for (int i = 0; i < NOutputs; i++) {
            stream >> outputsEnded[i];
        }
    }

    virtual ~TestChainNode()
    {
    }

    SmootherChainTarget *getTarget(int index)
    {
        Q_ASSERT(index >= 0 && index < NInputs);
        return &inputs[index];
    }

    virtual void serialize(QDataStream &stream) const
    {
        TestChainNodeCommon::serialize(stream);
        stream << (quint32) NInputs << (quint32) NOutputs;
        for (int i = 0; i < NInputs; i++) {
            stream << inputs[i].ended;
        }
        for (int i = 0; i < NOutputs; i++) {
            stream << outputsEnded[i];
        }
    }
};

class TestChainFlagsNode : public virtual SmootherChainTargetFlags, public TestChainNodeCommon {
public:
    SmootherChainTargetFlags *target;

    TestChainFlagsNode(int &guid, SmootherChainTargetFlags *st) : TestChainNodeCommon(guid),
                                                                  target(st)
    { }

    TestChainFlagsNode(QDataStream &stream, SmootherChainTargetFlags *st) : TestChainNodeCommon(
            stream), target(st)
    { }

    virtual ~TestChainFlagsNode()
    {
    }

    virtual void incomingData(double start, double end, const Data::Variant::Flags &value)
    {
        Data::Variant::Flags add(value);
        add.emplace("added");
        target->incomingData(start, end, add);
    }

    virtual void incomingAdvance(double time)
    {
        target->incomingAdvance(time);
    }

    virtual void endData()
    {
        target->endData();
    }
};

class TestCore : public SmootherChainCoreBinned {
public:
    double statsStartTime;
    double statsValue;
    int uid;

    QList<TestChainNodeCommon *> createdNodes;

    TestCore(bool enableStatistics = true) : SmootherChainCoreBinned(enableStatistics), uid(0)
    { }

    virtual ~TestCore()
    {
    }

    virtual Variant::Root getProcessingMetadata(double) const
    { return Variant::Root(); }

    virtual QSet<double> getProcessingMetadataBreaks() const
    { return QSet<double>(); }

    virtual bool differencePreserving() const
    { return false; }

    virtual void createConventional(SmootherChainCoreEngineInterface *engine,
                                    SmootherChainTarget *&inputValue,
                                    SmootherChainTarget *&inputCover,
                                    SmootherChainTarget *outputAverage,
                                    SmootherChainTarget *outputCover = NULL,
                                    SmootherChainTargetGeneral *outputStats = NULL)
    {
        if (outputCover == NULL && outputAverage != NULL) {
            TestChainNode<2, 1> *node = new TestChainNode<2, 1>(uid, outputAverage);
            engine->addChainNode(node);
            createdNodes.append(node);
            inputValue = node->getTarget(0);
            inputCover = node->getTarget(1);
        } else if (outputCover != NULL && outputAverage == NULL) {
            TestChainNode<2, 1> *node = new TestChainNode<2, 1>(uid, outputCover);
            engine->addChainNode(node);
            createdNodes.append(node);
            inputValue = node->getTarget(0);
            inputCover = node->getTarget(1);
        } else if (outputAverage != NULL && outputCover != NULL) {
            TestChainNode<2, 2> *node = new TestChainNode<2, 2>(uid, outputAverage, outputCover);
            engine->addChainNode(node);
            createdNodes.append(node);
            inputValue = node->getTarget(0);
            inputCover = node->getTarget(1);
        }
        if (outputStats != NULL) {
            outputStats->incomingData(statsStartTime, FP::undefined(), Variant::Root(statsValue));
            outputStats->endData();
        }
    }

    virtual void deserializeConventional(QDataStream &stream,
                                         SmootherChainCoreEngineInterface *engine,
                                         SmootherChainTarget *&inputValue,
                                         SmootherChainTarget *&inputCover,
                                         SmootherChainTarget *outputAverage,
                                         SmootherChainTarget *outputCover = NULL,
                                         SmootherChainTargetGeneral *outputStats = NULL)
    {
        if (outputCover == NULL && outputAverage != NULL) {
            TestChainNode<2, 1> *node = new TestChainNode<2, 1>(stream, outputAverage);
            engine->addChainNode(node);
            createdNodes.append(node);
            inputValue = node->getTarget(0);
            inputCover = node->getTarget(1);
        } else if (outputCover != NULL && outputAverage == NULL) {
            TestChainNode<2, 1> *node = new TestChainNode<2, 1>(stream, outputCover);
            engine->addChainNode(node);
            createdNodes.append(node);
            inputValue = node->getTarget(0);
            inputCover = node->getTarget(1);
        } else if (outputAverage != NULL && outputCover != NULL) {
            TestChainNode<2, 2> *node = new TestChainNode<2, 2>(stream, outputAverage, outputCover);
            engine->addChainNode(node);
            createdNodes.append(node);
            inputValue = node->getTarget(0);
            inputCover = node->getTarget(1);
        }
        if (outputStats != NULL) {
            outputStats->incomingData(statsStartTime, FP::undefined(), Variant::Root(statsValue));
            outputStats->endData();
        }
    }

    virtual void createVectorStatistics(SmootherChainCoreEngineInterface *engine,
                                        SmootherChainTarget *&inputMagnitude,
                                        SmootherChainTarget *&inputCover,
                                        SmootherChainTarget *&inputVectoredMagnitude,
                                        SmootherChainTarget *outputAverage = NULL,
                                        SmootherChainTarget *outputCover = NULL,
                                        SmootherChainTargetGeneral *outputStats = NULL)
    {
        if (outputAverage != NULL)
            outputAverage->endData();
        TestChainNode<3, 1> *node = new TestChainNode<3, 1>(uid, outputCover);
        engine->addChainNode(node);
        createdNodes.append(node);
        inputMagnitude = node->getTarget(0);
        inputCover = node->getTarget(1);
        inputVectoredMagnitude = node->getTarget(2);
        if (outputStats != NULL) {
            outputStats->incomingData(statsStartTime, FP::undefined(), Variant::Root(statsValue));
            outputStats->endData();
        }
    }

    virtual void deserializeVectorStatistics(QDataStream &stream,
                                             SmootherChainCoreEngineInterface *engine,
                                             SmootherChainTarget *&inputMagnitude,
                                             SmootherChainTarget *&inputCover,
                                             SmootherChainTarget *&inputVectoredMagnitude,
                                             SmootherChainTarget *outputAverage = NULL,
                                             SmootherChainTarget *outputCover = NULL,
                                             SmootherChainTargetGeneral *outputStats = NULL)
    {
        if (outputAverage != NULL)
            outputAverage->endData();
        TestChainNode<3, 1> *node = new TestChainNode<3, 1>(stream, outputCover);
        engine->addChainNode(node);
        createdNodes.append(node);
        inputMagnitude = node->getTarget(0);
        inputCover = node->getTarget(1);
        inputVectoredMagnitude = node->getTarget(2);
        if (outputStats != NULL) {
            outputStats->incomingData(statsStartTime, FP::undefined(), Variant::Root(statsValue));
            outputStats->endData();
        }
    }

    virtual void createDifference(SmootherChainCoreEngineInterface *engine,
                                  bool alwaysBreak,
                                  SmootherChainTarget *&inputStart,
                                  SmootherChainTarget *&inputEnd,
                                  SmootherChainTarget *outputStart,
                                  SmootherChainTarget *outputEnd,
                                  SmootherChainTarget *outputCover = NULL)
    {
        Q_UNUSED(alwaysBreak);
        if (outputCover != NULL) {
            TestChainNode<2, 3>
                    *node = new TestChainNode<2, 3>(uid, outputStart, outputEnd, outputCover);
            engine->addChainNode(node);
            createdNodes.append(node);
            inputStart = node->getTarget(0);
            inputEnd = node->getTarget(1);
        } else {
            TestChainNode<2, 2>
                    *node = new TestChainNode<2, 2>(uid, outputStart, outputEnd, outputCover);
            engine->addChainNode(node);
            createdNodes.append(node);
            inputStart = node->getTarget(0);
            inputEnd = node->getTarget(1);
        }
    }

    virtual void deserializeDifference(QDataStream &stream,
                                       SmootherChainCoreEngineInterface *engine,
                                       bool alwaysBreak,
                                       SmootherChainTarget *&inputStart,
                                       SmootherChainTarget *&inputEnd,
                                       SmootherChainTarget *outputStart,
                                       SmootherChainTarget *outputEnd,
                                       SmootherChainTarget *outputCover = NULL)
    {
        Q_UNUSED(alwaysBreak);
        if (outputCover != NULL) {
            TestChainNode<2, 3>
                    *node = new TestChainNode<2, 3>(stream, outputStart, outputEnd, outputCover);
            engine->addChainNode(node);
            createdNodes.append(node);
            inputStart = node->getTarget(0);
            inputEnd = node->getTarget(1);
        } else {
            TestChainNode<2, 2>
                    *node = new TestChainNode<2, 2>(stream, outputStart, outputEnd, outputCover);
            engine->addChainNode(node);
            createdNodes.append(node);
            inputStart = node->getTarget(0);
            inputEnd = node->getTarget(1);
        }
    }

    virtual void createFlags(SmootherChainCoreEngineInterface *engine,
                             SmootherChainTargetFlags *&inputFlags,
                             SmootherChainTarget *&inputCover,
                             SmootherChainTargetFlags *outputFlags,
                             SmootherChainTarget *outputCover = NULL)
    {
        TestChainFlagsNode *flags = new TestChainFlagsNode(uid, outputFlags);
        engine->addChainNode(flags);
        createdNodes.append(flags);
        inputFlags = flags;

        TestChainNode<1, 1> *node = new TestChainNode<1, 1>(uid, outputCover);
        engine->addChainNode(node);
        createdNodes.append(node);
        inputCover = node->getTarget(0);
    }

    virtual void deserializeFlags(QDataStream &stream,
                                  SmootherChainCoreEngineInterface *engine,
                                  SmootherChainTargetFlags *&inputFlags,
                                  SmootherChainTarget *&inputCover,
                                  SmootherChainTargetFlags *outputFlags,
                                  SmootherChainTarget *outputCover = NULL)
    {
        TestChainFlagsNode *flags = new TestChainFlagsNode(stream, outputFlags);
        engine->addChainNode(flags);
        createdNodes.append(flags);
        inputFlags = flags;

        TestChainNode<1, 1> *node = new TestChainNode<1, 1>(stream, outputCover);
        engine->addChainNode(node);
        createdNodes.append(node);
        inputCover = node->getTarget(0);
    }
};

class TestBinnedChain : public QObject {
Q_OBJECT

    static double vector2DMag(double inD, double inM)
    {
        inD = (inD - 180.0) * 0.0174532925199433;
        double x = cos(inD) * inM;
        double y = sin(inD) * inM;
        x += 1.0;
        y += 1.0;
        return sqrt(x * x + y * y);
    }

    static double vector2DDir(double inD, double inM)
    {
        inD = (inD - 180.0) * 0.0174532925199433;
        double x = cos(inD) * inM;
        double y = sin(inD) * inM;
        x += 1.0;
        y += 1.0;
        return atan2(y, x) * 57.2957795130823 + 180.0;
    }

    static double vector3DMag(double inA, double inE, double inM)
    {
        inA = (inA - 180.0) * 0.0174532925199433;
        inE *= 0.0174532925199433;
        double x = cos(inE) * cos(inA) * inM;
        double y = cos(inE) * sin(inA) * inM;
        double z = sin(inE) * inM;
        x += 1.0;
        y += 1.0;
        z += 1.0;
        return sqrt(x * x + y * y + z * z);
    }

    static double vector3DAzi(double inA, double inE, double inM)
    {
        inA = (inA - 180.0) * 0.0174532925199433;
        inE *= 0.0174532925199433;
        double x = cos(inE) * cos(inA) * inM;
        double y = cos(inE) * sin(inA) * inM;
        x += 1.0;
        y += 1.0;
        return atan2(y, x) * 57.2957795130823 + 180.0;
    }

    static double vector3DEle(double inA, double inE, double inM)
    {
        inA = (inA - 180.0) * 0.0174532925199433;
        inE *= 0.0174532925199433;
        double x = cos(inE) * cos(inA) * inM;
        double y = cos(inE) * sin(inA) * inM;
        double z = sin(inE) * inM;
        x += 1.0;
        y += 1.0;
        z += 1.0;
        return asin(z / sqrt(x * x + y * y + z * z)) * 57.2957795130823;
    }

    static double beersLaw(double startL, double endL, double startI, double endI)
    {
        return (1E6 * log(startI / endI)) / (endL - startL);
    }

    static bool contains(SequenceValue::Transfer &values,
                         const SequenceValue &v,
                         bool ignoreValue = false)
    {
        for (auto i = values.begin(); i != values.end(); ++i) {
            if (!FP::equal(i->getStart(), v.getStart()))
                continue;
            if (!FP::equal(i->getEnd(), v.getEnd()))
                continue;
            if (i->getUnit() != v.getUnit())
                continue;
            if (!ignoreValue && i->getValue() != v.getValue()) {
                qDebug() << "Value comparison mismatch on" << v << ":" << i->getValue();
                continue;
            }
            values.erase(i);
            return true;
        }
        return false;
    }

private slots:

    void general()
    {
        TestCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::General);

        QCOMPARE(engine1->inputs.size(), 2);
        QCOMPARE(engine1->inputs[1].unit,
                 SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 100.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 100.002);
        engine1->inputs[1].incomingData(1.0, 2.0, 200.001);
        engine1->inputs[1].incomingData(2.0, 3.0, 200.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::General);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 2);
        engine2->inputs[1].incomingData(3.0, 4.0, 200.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 200.004);
        engine2->inputs[0].incomingData(3.0, 4.0, 100.003);
        engine2->inputs[0].incomingData(4.0, 5.0, 100.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(101.001),
                                       1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(201.001),
                1.0, 2.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(101.002),
                                       2.0,
                                       3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(201.002),
                2.0, 3.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(101.003),
                                       3.0,
                                       4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(201.003),
                3.0, 4.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(101.004),
                                       4.0,
                                       5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(201.004),
                4.0, 5.0)));

        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                Variant::Root(-1.0),
                1.0, FP::undefined())));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                Variant::Root(-2.0),
                3.0, FP::undefined())));
        QVERIFY(values.empty());
    }

    void generalNoStats()
    {
        TestCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "out1"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in1"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::General);

        QCOMPARE(engine1->inputs.size(), 2);
        QCOMPARE(engine1->inputs[1].unit,
                 SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 100.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 100.002);
        engine1->inputs[1].incomingData(1.0, 2.0, 200.001);
        engine1->inputs[1].incomingData(2.0, 3.0, 200.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::General);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 2);
        engine2->inputs[1].incomingData(3.0, 4.0, 200.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 200.004);
        engine2->inputs[0].incomingData(3.0, 4.0, 100.003);
        engine2->inputs[0].incomingData(4.0, 5.0, 100.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(101.001),
                                       1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(201.001),
                1.0, 2.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(101.002),
                                       2.0,
                                       3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(201.002),
                2.0, 3.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(101.003),
                                       3.0,
                                       4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(201.003),
                3.0, 4.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "out1"), Variant::Root(101.004),
                                       4.0,
                                       5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(201.004),
                4.0, 5.0)));

        QVERIFY(values.empty());
    }

    void difference()
    {
        TestCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "start"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "end"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inStart"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inEnd"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::Difference);

        QCOMPARE(engine1->inputs.size(), 2);
        engine1->inputs[0].incomingData(1.0, 2.0, 100.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 100.002);
        engine1->inputs[1].incomingData(1.0, 2.0, 200.001);
        engine1->inputs[1].incomingData(2.0, 3.0, 200.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::Difference);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 2);
        engine2->inputs[1].incomingData(3.0, 4.0, 200.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 200.004);
        engine2->inputs[0].incomingData(3.0, 4.0, 100.003);
        engine2->inputs[0].incomingData(4.0, 5.0, 100.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "start"), Variant::Root(101.001),
                                       1.0, 2.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "end"), Variant::Root(201.001),
                                       1.0,
                                       2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(204.001),
                1.0, 2.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "start"), Variant::Root(101.002),
                                       2.0,
                                       3.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "end"), Variant::Root(201.002),
                                       2.0,
                                       3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(204.002),
                2.0, 3.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "start"), Variant::Root(101.003),
                                       3.0,
                                       4.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "end"), Variant::Root(201.003),
                                       3.0,
                                       4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(204.003),
                3.0, 4.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "start"), Variant::Root(101.004),
                                       4.0,
                                       5.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "end"), Variant::Root(201.004),
                                       4.0,
                                       5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(204.004),
                4.0, 5.0)));
        QVERIFY(values.empty());
    }

    void differenceInitial()
    {
        TestCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "start"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "end"));
        engine1->addNewInput(SequenceName("bnd", "raw", "in"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::DifferenceInitial);

        QCOMPARE(engine1->inputs.size(), 1);
        engine1->inputs[0].incomingData(1.0, 2.0, 100.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 100.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::DifferenceInitial);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 1);
        engine2->inputs[0].incomingData(3.0, 4.0, 100.003);
        engine2->inputs[0].incomingData(4.0, 5.0, 100.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "start"), Variant::Root(101.001),
                                       1.0, 2.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "start"), Variant::Root(101.002),
                                       2.0,
                                       3.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "start"), Variant::Root(101.003),
                                       3.0,
                                       4.0)));
        QVERIFY(contains(values,
                         SequenceValue(SequenceName("bnd", "raw", "start"), Variant::Root(101.004),
                                       4.0,
                                       5.0)));
        QVERIFY(values.empty());
    }

    void vector2D()
    {
        TestCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outD"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "outM"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inD"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inM"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::Vector2D);

        QCOMPARE(engine1->inputs.size(), 3);
        QCOMPARE(engine1->inputs[2].unit,
                 SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 100.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 100.002);
        engine1->inputs[2].incomingData(1.0, 2.0, 300.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 200.001);
        engine1->inputs[1].incomingData(2.0, 3.0, 200.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 300.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::Vector2D);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 3);
        engine2->inputs[1].incomingData(3.0, 4.0, 200.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 200.004);
        engine2->inputs[2].incomingData(3.0, 4.0, 300.003);
        engine2->inputs[2].incomingData(4.0, 5.0, 300.004);
        engine2->inputs[0].incomingData(3.0, 4.0, 100.003);
        engine2->inputs[0].incomingData(4.0, 5.0, 100.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outD"),
                                               Variant::Root(vector2DDir(100.001, 200.001)), 1.0,
                                               2.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"),
                                               Variant::Root(vector2DMag(100.001, 200.001)), 1.0,
                                               2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}),
                Variant::Root(201.001),
                1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outD"),
                                               Variant::Root(vector2DDir(100.002, 200.002)), 2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"),
                                               Variant::Root(vector2DMag(100.002, 200.002)), 2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}),
                Variant::Root(201.002),
                2.0, 3.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outD"),
                                               Variant::Root(vector2DDir(100.003, 200.003)), 3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"),
                                               Variant::Root(vector2DMag(100.003, 200.003)), 3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}),
                Variant::Root(201.003),
                3.0, 4.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outD"),
                                               Variant::Root(vector2DDir(100.004, 200.004)), 4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"),
                                               Variant::Root(vector2DMag(100.004, 200.004)), 4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}),
                Variant::Root(201.004),
                4.0, 5.0)));

        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"stats"}),
                Variant::Root(-1.0),
                1.0, FP::undefined())));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"stats"}),
                Variant::Root(-2.0),
                3.0, FP::undefined())));
        QVERIFY(values.empty());
    }

    void vector2DNoStats()
    {
        TestCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outD"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "outM"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inD"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inM"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::Vector2D);

        QCOMPARE(engine1->inputs.size(), 3);
        QCOMPARE(engine1->inputs[2].unit,
                 SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 100.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 100.002);
        engine1->inputs[2].incomingData(1.0, 2.0, 300.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 200.001);
        engine1->inputs[1].incomingData(2.0, 3.0, 200.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 300.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::Vector2D);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 3);
        engine2->inputs[1].incomingData(3.0, 4.0, 200.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 200.004);
        engine2->inputs[2].incomingData(3.0, 4.0, 300.003);
        engine2->inputs[2].incomingData(4.0, 5.0, 300.004);
        engine2->inputs[0].incomingData(3.0, 4.0, 100.003);
        engine2->inputs[0].incomingData(4.0, 5.0, 100.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outD"),
                                               Variant::Root(vector2DDir(100.001, 200.001)), 1.0,
                                               2.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"),
                                               Variant::Root(vector2DMag(100.001, 200.001)), 1.0,
                                               2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}),
                Variant::Root(201.001),
                1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outD"),
                                               Variant::Root(vector2DDir(100.002, 200.002)), 2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"),
                                               Variant::Root(vector2DMag(100.002, 200.002)), 2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}),
                Variant::Root(201.002),
                2.0, 3.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outD"),
                                               Variant::Root(vector2DDir(100.003, 200.003)), 3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"),
                                               Variant::Root(vector2DMag(100.003, 200.003)), 3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}),
                Variant::Root(201.003),
                3.0, 4.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outD"),
                                               Variant::Root(vector2DDir(100.004, 200.004)), 4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"),
                                               Variant::Root(vector2DMag(100.004, 200.004)), 4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}),
                Variant::Root(201.004),
                4.0, 5.0)));

        QVERIFY(values.empty());
    }

    void vector3D()
    {
        TestCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outA"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "outE"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "outM"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inA"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inE"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inM"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::Vector3D);

        QCOMPARE(engine1->inputs.size(), 4);
        QCOMPARE(engine1->inputs[3].unit,
                 SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 100.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 200.001);
        engine1->inputs[2].incomingData(1.0, 2.0, 300.001);
        engine1->inputs[3].incomingData(1.0, 2.0, 400.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 100.002);
        engine1->inputs[1].incomingData(2.0, 3.0, 200.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 300.002);
        engine1->inputs[3].incomingData(2.0, 3.0, 400.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::Vector3D);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 4);
        engine2->inputs[3].incomingData(3.0, 4.0, 400.003);
        engine2->inputs[2].incomingData(3.0, 4.0, 300.003);
        engine2->inputs[1].incomingData(3.0, 4.0, 200.003);
        engine2->inputs[0].incomingData(3.0, 4.0, 100.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 200.004);
        engine2->inputs[2].incomingData(4.0, 5.0, 300.004);
        engine2->inputs[0].incomingData(4.0, 5.0, 100.004);
        engine2->inputs[3].incomingData(4.0, 5.0, 400.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                vector3DAzi(100.001, 200.001, 300.001)), 1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outE"), Variant::Root(
                vector3DEle(100.001, 200.001, 300.001)), 1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"), Variant::Root(
                vector3DMag(100.001, 200.001, 300.001)), 1.0,
                                               2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}),
                Variant::Root(301.001),
                1.0, 2.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                vector3DAzi(100.002, 200.002, 300.002)), 2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outE"), Variant::Root(
                vector3DEle(100.002, 200.002, 300.002)), 2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"), Variant::Root(
                vector3DMag(100.002, 200.002, 300.002)), 2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}),
                Variant::Root(301.002),
                2.0, 3.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                vector3DAzi(100.003, 200.003, 300.003)), 3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outE"), Variant::Root(
                vector3DEle(100.003, 200.003, 300.003)), 3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"), Variant::Root(
                vector3DMag(100.003, 200.003, 300.003)), 3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}),
                Variant::Root(301.003),
                3.0, 4.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                vector3DAzi(100.004, 200.004, 300.004)), 4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outE"), Variant::Root(
                vector3DEle(100.004, 200.004, 300.004)), 4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"), Variant::Root(
                vector3DMag(100.004, 200.004, 300.004)), 4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}),
                Variant::Root(301.004),
                4.0, 5.0)));

        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"stats"}),
                Variant::Root(-1.0),
                1.0, FP::undefined())));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"stats"}),
                Variant::Root(-2.0),
                3.0, FP::undefined())));
        QVERIFY(values.empty());
    }

    void vector3DNoStats()
    {
        TestCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outA"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "outE"));
        engine1->addNewOutput(SequenceName("bnd", "raw", "outM"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inA"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inE"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inM"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::Vector3D);

        QCOMPARE(engine1->inputs.size(), 4);
        QCOMPARE(engine1->inputs[3].unit,
                 SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 100.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 200.001);
        engine1->inputs[2].incomingData(1.0, 2.0, 300.001);
        engine1->inputs[3].incomingData(1.0, 2.0, 400.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 100.002);
        engine1->inputs[1].incomingData(2.0, 3.0, 200.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 300.002);
        engine1->inputs[3].incomingData(2.0, 3.0, 400.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::Vector3D);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 4);
        engine2->inputs[3].incomingData(3.0, 4.0, 400.003);
        engine2->inputs[2].incomingData(3.0, 4.0, 300.003);
        engine2->inputs[1].incomingData(3.0, 4.0, 200.003);
        engine2->inputs[0].incomingData(3.0, 4.0, 100.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 200.004);
        engine2->inputs[2].incomingData(4.0, 5.0, 300.004);
        engine2->inputs[0].incomingData(4.0, 5.0, 100.004);
        engine2->inputs[3].incomingData(4.0, 5.0, 400.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                vector3DAzi(100.001, 200.001, 300.001)), 1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outE"), Variant::Root(
                vector3DEle(100.001, 200.001, 300.001)), 1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"), Variant::Root(
                vector3DMag(100.001, 200.001, 300.001)), 1.0,
                                               2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}),
                Variant::Root(301.001),
                1.0, 2.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                vector3DAzi(100.002, 200.002, 300.002)), 2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outE"), Variant::Root(
                vector3DEle(100.002, 200.002, 300.002)), 2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"), Variant::Root(
                vector3DMag(100.002, 200.002, 300.002)), 2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}),
                Variant::Root(301.002),
                2.0, 3.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                vector3DAzi(100.003, 200.003, 300.003)), 3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outE"), Variant::Root(
                vector3DEle(100.003, 200.003, 300.003)), 3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"), Variant::Root(
                vector3DMag(100.003, 200.003, 300.003)), 3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}),
                Variant::Root(301.003),
                3.0, 4.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                vector3DAzi(100.004, 200.004, 300.004)), 4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outE"), Variant::Root(
                vector3DEle(100.004, 200.004, 300.004)), 4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outM"), Variant::Root(
                vector3DMag(100.004, 200.004, 300.004)), 4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}),
                Variant::Root(301.004),
                4.0, 5.0)));

        QVERIFY(values.empty());
    }

    void beersLawAbsorption()
    {
        TestCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outA"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inStartL"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inEndL"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inStartI"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inEndI"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::BeersLawAbsorption);

        QCOMPARE(engine1->inputs.size(), 6);
        QCOMPARE(engine1->inputs[4].unit, SequenceName("bnd", "raw", "var1"));
        QCOMPARE(engine1->inputs[5].unit,
                 SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 100.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 200.001);
        engine1->inputs[2].incomingData(1.0, 2.0, 300.001);
        engine1->inputs[3].incomingData(1.0, 2.0, 400.001);
        engine1->inputs[4].incomingData(1.0, 2.0, 500.001);
        engine1->inputs[5].incomingData(1.0, 2.0, 600.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 100.002);
        engine1->inputs[1].incomingData(2.0, 3.0, 200.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 300.002);
        engine1->inputs[3].incomingData(2.0, 3.0, 400.002);
        engine1->inputs[4].incomingData(2.0, 3.0, 500.002);
        engine1->inputs[5].incomingData(2.0, 3.0, 600.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::BeersLawAbsorption);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 6);
        engine2->inputs[5].incomingData(3.0, 4.0, 600.003);
        engine2->inputs[4].incomingData(3.0, 4.0, 500.003);
        engine2->inputs[3].incomingData(3.0, 4.0, 400.003);
        engine2->inputs[2].incomingData(3.0, 4.0, 300.003);
        engine2->inputs[1].incomingData(3.0, 4.0, 200.003);
        engine2->inputs[0].incomingData(3.0, 4.0, 100.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 200.004);
        engine2->inputs[2].incomingData(4.0, 5.0, 300.004);
        engine2->inputs[0].incomingData(4.0, 5.0, 100.004);
        engine2->inputs[3].incomingData(4.0, 5.0, 400.004);
        engine2->inputs[4].incomingData(4.0, 5.0, 500.004);
        engine2->inputs[5].incomingData(4.0, 5.0, 600.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                beersLaw(101.001, 201.001, 301.001, 401.001) + 1.0), 1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(501.001), 1.0, 2.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                beersLaw(101.002, 201.002, 301.002, 401.002) +
                                                             1.0), 2.0, 3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(501.002),
                2.0, 3.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                beersLaw(101.003, 201.003, 301.003, 401.003) +
                                                             1.0), 3.0, 4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(501.003),
                3.0, 4.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                beersLaw(101.004, 201.004, 301.004, 401.004) +
                                                             1.0), 4.0, 5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(501.004),
                4.0, 5.0)));

        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                Variant::Root(-1.0),
                1.0, FP::undefined())));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                Variant::Root(-2.0),
                3.0, FP::undefined())));
        QVERIFY(values.empty());
    }

    void beersLawAbsorptionNoStats()
    {
        TestCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outA"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inStartL"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inEndL"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inStartI"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inEndI"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::BeersLawAbsorption);

        QCOMPARE(engine1->inputs.size(), 6);
        QCOMPARE(engine1->inputs[4].unit, SequenceName("bnd", "raw", "var1"));
        QCOMPARE(engine1->inputs[5].unit,
                 SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 100.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 200.001);
        engine1->inputs[2].incomingData(1.0, 2.0, 300.001);
        engine1->inputs[3].incomingData(1.0, 2.0, 400.001);
        engine1->inputs[4].incomingData(1.0, 2.0, 500.001);
        engine1->inputs[5].incomingData(1.0, 2.0, 600.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 100.002);
        engine1->inputs[1].incomingData(2.0, 3.0, 200.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 300.002);
        engine1->inputs[3].incomingData(2.0, 3.0, 400.002);
        engine1->inputs[4].incomingData(2.0, 3.0, 500.002);
        engine1->inputs[5].incomingData(2.0, 3.0, 600.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::BeersLawAbsorption);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 6);
        engine2->inputs[5].incomingData(3.0, 4.0, 600.003);
        engine2->inputs[4].incomingData(3.0, 4.0, 500.003);
        engine2->inputs[3].incomingData(3.0, 4.0, 400.003);
        engine2->inputs[2].incomingData(3.0, 4.0, 300.003);
        engine2->inputs[1].incomingData(3.0, 4.0, 200.003);
        engine2->inputs[0].incomingData(3.0, 4.0, 100.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 200.004);
        engine2->inputs[2].incomingData(4.0, 5.0, 300.004);
        engine2->inputs[0].incomingData(4.0, 5.0, 100.004);
        engine2->inputs[3].incomingData(4.0, 5.0, 400.004);
        engine2->inputs[4].incomingData(4.0, 5.0, 500.004);
        engine2->inputs[5].incomingData(4.0, 5.0, 600.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                beersLaw(101.001, 201.001, 301.001, 401.001) + 1.0), 1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(501.001), 1.0, 2.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                beersLaw(101.002, 201.002, 301.002, 401.002) +
                                                             1.0), 2.0, 3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(501.002),
                2.0, 3.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                beersLaw(101.003, 201.003, 301.003, 401.003) +
                                                             1.0), 3.0, 4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(501.003),
                3.0, 4.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"), Variant::Root(
                beersLaw(101.004, 201.004, 301.004, 401.004) +
                                                             1.0), 4.0, 5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(501.004),
                4.0, 5.0)));

        QVERIFY(values.empty());
    }

    void beersLawAbsorptionInitial()
    {
        TestCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outA"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inL"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inI"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::BeersLawAbsorptionInitial);

        QCOMPARE(engine1->inputs.size(), 4);
        QCOMPARE(engine1->inputs[2].unit, SequenceName("bnd", "raw", "var1"));
        QCOMPARE(engine1->inputs[3].unit,
                 SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 100.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 200.001);
        engine1->inputs[2].incomingData(1.0, 2.0, 300.001);
        engine1->inputs[3].incomingData(1.0, 2.0, 400.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 100.002);
        engine1->inputs[1].incomingData(2.0, 3.0, 200.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 300.002);
        engine1->inputs[3].incomingData(2.0, 3.0, 400.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::BeersLawAbsorptionInitial);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 4);
        engine2->inputs[3].incomingData(3.0, 4.0, 400.003);
        engine2->inputs[2].incomingData(3.0, 4.0, 300.003);
        engine2->inputs[1].incomingData(3.0, 4.0, 200.003);
        engine2->inputs[0].incomingData(3.0, 4.0, 100.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 200.004);
        engine2->inputs[2].incomingData(4.0, 5.0, 300.004);
        engine2->inputs[0].incomingData(4.0, 5.0, 100.004);
        engine2->inputs[3].incomingData(4.0, 5.0, 400.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"),
                                               Variant::Root(FP::undefined()), 1.0, 2.0), true));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(301.001), 1.0, 2.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"),
                                               Variant::Root(FP::undefined()),
                                               2.0, 3.0), true));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(301.002),
                2.0, 3.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"),
                                               Variant::Root(FP::undefined()),
                                               3.0, 4.0), true));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(301.003),
                3.0, 4.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"),
                                               Variant::Root(FP::undefined()),
                                               4.0, 5.0), true));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(301.004),
                4.0, 5.0)));

        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                Variant::Root(-1.0),
                1.0, FP::undefined())));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                Variant::Root(-2.0),
                3.0, FP::undefined())));
        QVERIFY(values.empty());
    }

    void beersLawAbsorptionInitialNoStats()
    {
        TestCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outA"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inL"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inI"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::BeersLawAbsorptionInitial);

        QCOMPARE(engine1->inputs.size(), 4);
        QCOMPARE(engine1->inputs[2].unit, SequenceName("bnd", "raw", "var1"));
        QCOMPARE(engine1->inputs[3].unit,
                 SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 100.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 200.001);
        engine1->inputs[2].incomingData(1.0, 2.0, 300.001);
        engine1->inputs[3].incomingData(1.0, 2.0, 400.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 100.002);
        engine1->inputs[1].incomingData(2.0, 3.0, 200.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 300.002);
        engine1->inputs[3].incomingData(2.0, 3.0, 400.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::BeersLawAbsorptionInitial);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 4);
        engine2->inputs[3].incomingData(3.0, 4.0, 400.003);
        engine2->inputs[2].incomingData(3.0, 4.0, 300.003);
        engine2->inputs[1].incomingData(3.0, 4.0, 200.003);
        engine2->inputs[0].incomingData(3.0, 4.0, 100.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 200.004);
        engine2->inputs[2].incomingData(4.0, 5.0, 300.004);
        engine2->inputs[0].incomingData(4.0, 5.0, 100.004);
        engine2->inputs[3].incomingData(4.0, 5.0, 400.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"),
                                               Variant::Root(FP::undefined()), 1.0, 2.0), true));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(301.001), 1.0, 2.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"),
                                               Variant::Root(FP::undefined()),
                                               2.0, 3.0), true));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(301.002),
                2.0, 3.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"),
                                               Variant::Root(FP::undefined()),
                                               3.0, 4.0), true));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(301.003),
                3.0, 4.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outA"),
                                               Variant::Root(FP::undefined()),
                                               4.0, 5.0), true));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(301.004),
                4.0, 5.0)));

        QVERIFY(values.empty());
    }

    void dewpoint()
    {
        TestCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outTD"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inT"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inRH"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::Dewpoint);

        QCOMPARE(engine1->inputs.size(), 6);
        QCOMPARE(engine1->inputs[2].unit, SequenceName("bnd", "raw", "var1"));
        QCOMPARE(engine1->inputs[3].unit,
                 SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[4].unit,
                 SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[5].unit,
                 SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 10.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 20.001);
        engine1->inputs[2].incomingData(1.0, 2.0, 30.001);
        engine1->inputs[3].incomingData(1.0, 2.0, 40.001);
        engine1->inputs[4].incomingData(1.0, 2.0, 50.001);
        engine1->inputs[5].incomingData(1.0, 2.0, 60.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 10.002);
        engine1->inputs[1].incomingData(2.0, 3.0, 20.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 30.002);
        engine1->inputs[3].incomingData(2.0, 3.0, 40.002);
        engine1->inputs[4].incomingData(2.0, 3.0, 50.002);
        engine1->inputs[5].incomingData(2.0, 3.0, 60.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::Dewpoint);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 6);
        engine2->inputs[5].incomingData(3.0, 4.0, 60.003);
        engine2->inputs[4].incomingData(3.0, 4.0, 50.003);
        engine2->inputs[3].incomingData(3.0, 4.0, 40.003);
        engine2->inputs[2].incomingData(3.0, 4.0, 30.003);
        engine2->inputs[1].incomingData(3.0, 4.0, 20.003);
        engine2->inputs[0].incomingData(3.0, 4.0, 10.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 20.004);
        engine2->inputs[2].incomingData(4.0, 5.0, 30.004);
        engine2->inputs[0].incomingData(4.0, 5.0, 10.004);
        engine2->inputs[3].incomingData(4.0, 5.0, 40.004);
        engine2->inputs[4].incomingData(4.0, 5.0, 50.004);
        engine2->inputs[5].incomingData(4.0, 5.0, 60.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outTD"),
                                               Variant::Root(Dewpoint::dewpoint(11.001, 21.001)),
                                               1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.001),
                1.0, 2.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outTD"),
                                               Variant::Root(Dewpoint::dewpoint(11.002, 21.002)),
                                               2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.002),
                2.0, 3.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outTD"),
                                               Variant::Root(Dewpoint::dewpoint(11.003, 21.003)),
                                               3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.003),
                3.0, 4.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outTD"),
                                               Variant::Root(Dewpoint::dewpoint(11.004, 21.004)),
                                               4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.004),
                4.0, 5.0)));

        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                Variant::Root(-1.0),
                1.0, FP::undefined())));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                Variant::Root(-2.0),
                3.0, FP::undefined())));
        QVERIFY(values.empty());
    }

    void dewpointNoStats()
    {
        TestCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outTD"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inT"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inRH"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::Dewpoint);

        QCOMPARE(engine1->inputs.size(), 6);
        QCOMPARE(engine1->inputs[2].unit, SequenceName("bnd", "raw", "var1"));
        QCOMPARE(engine1->inputs[3].unit,
                 SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[4].unit,
                 SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[5].unit,
                 SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 10.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 20.001);
        engine1->inputs[2].incomingData(1.0, 2.0, 30.001);
        engine1->inputs[3].incomingData(1.0, 2.0, 40.001);
        engine1->inputs[4].incomingData(1.0, 2.0, 50.001);
        engine1->inputs[5].incomingData(1.0, 2.0, 60.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 10.002);
        engine1->inputs[1].incomingData(2.0, 3.0, 20.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 30.002);
        engine1->inputs[3].incomingData(2.0, 3.0, 40.002);
        engine1->inputs[4].incomingData(2.0, 3.0, 50.002);
        engine1->inputs[5].incomingData(2.0, 3.0, 60.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::Dewpoint);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 6);
        engine2->inputs[5].incomingData(3.0, 4.0, 60.003);
        engine2->inputs[4].incomingData(3.0, 4.0, 50.003);
        engine2->inputs[3].incomingData(3.0, 4.0, 40.003);
        engine2->inputs[2].incomingData(3.0, 4.0, 30.003);
        engine2->inputs[1].incomingData(3.0, 4.0, 20.003);
        engine2->inputs[0].incomingData(3.0, 4.0, 10.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 20.004);
        engine2->inputs[2].incomingData(4.0, 5.0, 30.004);
        engine2->inputs[0].incomingData(4.0, 5.0, 10.004);
        engine2->inputs[3].incomingData(4.0, 5.0, 40.004);
        engine2->inputs[4].incomingData(4.0, 5.0, 50.004);
        engine2->inputs[5].incomingData(4.0, 5.0, 60.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outTD"),
                                               Variant::Root(Dewpoint::dewpoint(11.001, 21.001)),
                                               1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.001),
                1.0, 2.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outTD"),
                                               Variant::Root(Dewpoint::dewpoint(11.002, 21.002)),
                                               2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.002),
                2.0, 3.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outTD"),
                                               Variant::Root(Dewpoint::dewpoint(11.003, 21.003)),
                                               3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.003),
                3.0, 4.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outTD"),
                                               Variant::Root(Dewpoint::dewpoint(11.004, 21.004)),
                                               4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.004),
                4.0, 5.0)));

        QVERIFY(values.empty());
    }

    void rh()
    {
        TestCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outRH"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inT"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inTD"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::RH);

        QCOMPARE(engine1->inputs.size(), 6);
        QCOMPARE(engine1->inputs[2].unit, SequenceName("bnd", "raw", "var1"));
        QCOMPARE(engine1->inputs[3].unit,
                 SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[4].unit,
                 SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[5].unit,
                 SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 10.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 20.001);
        engine1->inputs[2].incomingData(1.0, 2.0, 30.001);
        engine1->inputs[3].incomingData(1.0, 2.0, 40.001);
        engine1->inputs[4].incomingData(1.0, 2.0, 50.001);
        engine1->inputs[5].incomingData(1.0, 2.0, 60.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 10.002);
        engine1->inputs[1].incomingData(2.0, 3.0, 20.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 30.002);
        engine1->inputs[3].incomingData(2.0, 3.0, 40.002);
        engine1->inputs[4].incomingData(2.0, 3.0, 50.002);
        engine1->inputs[5].incomingData(2.0, 3.0, 60.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::RH);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 6);
        engine2->inputs[5].incomingData(3.0, 4.0, 60.003);
        engine2->inputs[4].incomingData(3.0, 4.0, 50.003);
        engine2->inputs[3].incomingData(3.0, 4.0, 40.003);
        engine2->inputs[2].incomingData(3.0, 4.0, 30.003);
        engine2->inputs[1].incomingData(3.0, 4.0, 20.003);
        engine2->inputs[0].incomingData(3.0, 4.0, 10.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 20.004);
        engine2->inputs[2].incomingData(4.0, 5.0, 30.004);
        engine2->inputs[0].incomingData(4.0, 5.0, 10.004);
        engine2->inputs[3].incomingData(4.0, 5.0, 40.004);
        engine2->inputs[4].incomingData(4.0, 5.0, 50.004);
        engine2->inputs[5].incomingData(4.0, 5.0, 60.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rh(11.001, 21.001)), 1.0,
                                               2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.001), 1.0, 2.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rh(11.002, 21.002)), 2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.002),
                2.0, 3.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rh(11.003, 21.003)), 3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.003),
                3.0, 4.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rh(11.004, 21.004)), 4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.004),
                4.0, 5.0)));

        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                Variant::Root(-1.0),
                1.0, FP::undefined())));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                Variant::Root(-2.0),
                3.0, FP::undefined())));
        QVERIFY(values.empty());
    }

    void rhNoStats()
    {
        TestCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outRH"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inT"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inTD"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::RH);

        QCOMPARE(engine1->inputs.size(), 6);
        QCOMPARE(engine1->inputs[2].unit, SequenceName("bnd", "raw", "var1"));
        QCOMPARE(engine1->inputs[3].unit,
                 SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[4].unit,
                 SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[5].unit,
                 SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 10.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 20.001);
        engine1->inputs[2].incomingData(1.0, 2.0, 30.001);
        engine1->inputs[3].incomingData(1.0, 2.0, 40.001);
        engine1->inputs[4].incomingData(1.0, 2.0, 50.001);
        engine1->inputs[5].incomingData(1.0, 2.0, 60.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 10.002);
        engine1->inputs[1].incomingData(2.0, 3.0, 20.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 30.002);
        engine1->inputs[3].incomingData(2.0, 3.0, 40.002);
        engine1->inputs[4].incomingData(2.0, 3.0, 50.002);
        engine1->inputs[5].incomingData(2.0, 3.0, 60.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::RH);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 6);
        engine2->inputs[5].incomingData(3.0, 4.0, 60.003);
        engine2->inputs[4].incomingData(3.0, 4.0, 50.003);
        engine2->inputs[3].incomingData(3.0, 4.0, 40.003);
        engine2->inputs[2].incomingData(3.0, 4.0, 30.003);
        engine2->inputs[1].incomingData(3.0, 4.0, 20.003);
        engine2->inputs[0].incomingData(3.0, 4.0, 10.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 20.004);
        engine2->inputs[2].incomingData(4.0, 5.0, 30.004);
        engine2->inputs[0].incomingData(4.0, 5.0, 10.004);
        engine2->inputs[3].incomingData(4.0, 5.0, 40.004);
        engine2->inputs[4].incomingData(4.0, 5.0, 50.004);
        engine2->inputs[5].incomingData(4.0, 5.0, 60.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rh(11.001, 21.001)), 1.0,
                                               2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.001), 1.0, 2.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rh(11.002, 21.002)), 2.0,
                                               3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.002),
                2.0, 3.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rh(11.003, 21.003)), 3.0,
                                               4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.003),
                3.0, 4.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rh(11.004, 21.004)), 4.0,
                                               5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(31.004),
                4.0, 5.0)));

        QVERIFY(values.empty());
    }

    void rhExtrapolate()
    {
        TestCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outRH"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inT1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inRH1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inT2"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::RHExtrapolate);

        QCOMPARE(engine1->inputs.size(), 8);
        QCOMPARE(engine1->inputs[3].unit, SequenceName("bnd", "raw", "var1"));
        QCOMPARE(engine1->inputs[4].unit,
                 SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[5].unit,
                 SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[6].unit,
                 SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[7].unit,
                 SequenceName("bnd", "raw", "var4", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 10.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 20.001);
        engine1->inputs[2].incomingData(1.0, 2.0, 30.001);
        engine1->inputs[3].incomingData(1.0, 2.0, 40.001);
        engine1->inputs[4].incomingData(1.0, 2.0, 50.001);
        engine1->inputs[5].incomingData(1.0, 2.0, 60.001);
        engine1->inputs[6].incomingData(1.0, 2.0, 70.001);
        engine1->inputs[7].incomingData(1.0, 2.0, 80.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 10.002);
        engine1->inputs[1].incomingData(2.0, 3.0, 20.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 30.002);
        engine1->inputs[3].incomingData(2.0, 3.0, 40.002);
        engine1->inputs[4].incomingData(2.0, 3.0, 50.002);
        engine1->inputs[5].incomingData(2.0, 3.0, 60.002);
        engine1->inputs[6].incomingData(2.0, 3.0, 70.002);
        engine1->inputs[7].incomingData(2.0, 3.0, 80.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::RHExtrapolate);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 8);
        engine2->inputs[7].incomingData(3.0, 4.0, 80.003);
        engine2->inputs[6].incomingData(3.0, 4.0, 70.003);
        engine2->inputs[5].incomingData(3.0, 4.0, 60.003);
        engine2->inputs[4].incomingData(3.0, 4.0, 50.003);
        engine2->inputs[3].incomingData(3.0, 4.0, 40.003);
        engine2->inputs[2].incomingData(3.0, 4.0, 30.003);
        engine2->inputs[1].incomingData(3.0, 4.0, 20.003);
        engine2->inputs[0].incomingData(3.0, 4.0, 10.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 20.004);
        engine2->inputs[2].incomingData(4.0, 5.0, 30.004);
        engine2->inputs[0].incomingData(4.0, 5.0, 10.004);
        engine2->inputs[3].incomingData(4.0, 5.0, 40.004);
        engine2->inputs[4].incomingData(4.0, 5.0, 50.004);
        engine2->inputs[5].incomingData(4.0, 5.0, 60.004);
        engine2->inputs[6].incomingData(4.0, 5.0, 70.004);
        engine2->inputs[7].incomingData(4.0, 5.0, 80.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"), Variant::Root(
                Dewpoint::rhExtrapolate(11.001, 21.001, 31.001)), 1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(41.001), 1.0, 2.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rhExtrapolate(11.002, 21.002,
                                                                                     31.002)), 2.0, 3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(41.002),
                2.0, 3.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rhExtrapolate(11.003, 21.003,
                                                                                     31.003)), 3.0, 4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(41.003),
                3.0, 4.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rhExtrapolate(11.004, 21.004,
                                                                                     31.004)), 4.0, 5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(41.004),
                4.0, 5.0)));

        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                Variant::Root(-1.0),
                1.0, FP::undefined())));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"stats"}),
                Variant::Root(-2.0),
                3.0, FP::undefined())));
        QVERIFY(values.empty());
    }

    void rhExtrapolateNoStats()
    {
        TestCore core(false);
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewOutput(SequenceName("bnd", "raw", "outRH"));
        engine1->addNewInput(SequenceName("bnd", "raw", "inT1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inRH1"), NULL);
        engine1->addNewInput(SequenceName("bnd", "raw", "inT2"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        core.createSmoother(engine1, SmootherChainCore::RHExtrapolate);

        QCOMPARE(engine1->inputs.size(), 8);
        QCOMPARE(engine1->inputs[3].unit, SequenceName("bnd", "raw", "var1"));
        QCOMPARE(engine1->inputs[4].unit,
                 SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[5].unit,
                 SequenceName("bnd", "raw", "var2", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[6].unit,
                 SequenceName("bnd", "raw", "var3", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputs[7].unit,
                 SequenceName("bnd", "raw", "var4", SequenceName::Flavors{"cover"}));
        engine1->inputs[0].incomingData(1.0, 2.0, 10.001);
        engine1->inputs[1].incomingData(1.0, 2.0, 20.001);
        engine1->inputs[2].incomingData(1.0, 2.0, 30.001);
        engine1->inputs[3].incomingData(1.0, 2.0, 40.001);
        engine1->inputs[4].incomingData(1.0, 2.0, 50.001);
        engine1->inputs[5].incomingData(1.0, 2.0, 60.001);
        engine1->inputs[6].incomingData(1.0, 2.0, 70.001);
        engine1->inputs[7].incomingData(1.0, 2.0, 80.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 10.002);
        engine1->inputs[1].incomingData(2.0, 3.0, 20.002);
        engine1->inputs[2].incomingData(2.0, 3.0, 30.002);
        engine1->inputs[3].incomingData(2.0, 3.0, 40.002);
        engine1->inputs[4].incomingData(2.0, 3.0, 50.002);
        engine1->inputs[5].incomingData(2.0, 3.0, 60.002);
        engine1->inputs[6].incomingData(2.0, 3.0, 70.002);
        engine1->inputs[7].incomingData(2.0, 3.0, 80.002);

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            core.deserializeSmoother(stream, engine2, SmootherChainCore::RHExtrapolate);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 8);
        engine2->inputs[7].incomingData(3.0, 4.0, 80.003);
        engine2->inputs[6].incomingData(3.0, 4.0, 70.003);
        engine2->inputs[5].incomingData(3.0, 4.0, 60.003);
        engine2->inputs[4].incomingData(3.0, 4.0, 50.003);
        engine2->inputs[3].incomingData(3.0, 4.0, 40.003);
        engine2->inputs[2].incomingData(3.0, 4.0, 30.003);
        engine2->inputs[1].incomingData(3.0, 4.0, 20.003);
        engine2->inputs[0].incomingData(3.0, 4.0, 10.003);
        engine2->inputs[1].incomingData(4.0, 5.0, 20.004);
        engine2->inputs[2].incomingData(4.0, 5.0, 30.004);
        engine2->inputs[0].incomingData(4.0, 5.0, 10.004);
        engine2->inputs[3].incomingData(4.0, 5.0, 40.004);
        engine2->inputs[4].incomingData(4.0, 5.0, 50.004);
        engine2->inputs[5].incomingData(4.0, 5.0, 60.004);
        engine2->inputs[6].incomingData(4.0, 5.0, 70.004);
        engine2->inputs[7].incomingData(4.0, 5.0, 80.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"), Variant::Root(
                Dewpoint::rhExtrapolate(11.001, 21.001, 31.001)), 1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(41.001), 1.0, 2.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rhExtrapolate(11.002, 21.002,
                                                                                     31.002)), 2.0, 3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(41.002),
                2.0, 3.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rhExtrapolate(11.003, 21.003,
                                                                                     31.003)), 3.0, 4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(41.003),
                3.0, 4.0)));

        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "outRH"),
                                               Variant::Root(Dewpoint::rhExtrapolate(11.004, 21.004,
                                                                                     31.004)), 4.0, 5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(41.004),
                4.0, 5.0)));

        QVERIFY(values.empty());
    }

    void flags()
    {
        TestCore core;
        StreamSink::Buffer end;
        TestChainEngine *engine1 = new TestChainEngine;

        engine1->mux.setEgress(&end);

        engine1->addNewFlagsOutput(SequenceName("bnd", "raw", "out"));
        engine1->addNewFlagsInput(SequenceName("bnd", "raw", "in"), NULL);
        core.statsStartTime = 1.0;
        core.statsValue = -1.0;
        ((SmootherChainCoreBinned *) &core)->createFlags(engine1);

        QCOMPARE(engine1->inputs.size(), 1);
        QCOMPARE(engine1->inputs[0].unit,
                 SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}));
        QCOMPARE(engine1->inputsFlags.size(), 1);
        engine1->inputs[0].incomingData(1.0, 2.0, 100.001);
        engine1->inputs[0].incomingData(2.0, 3.0, 100.002);
        engine1->inputsFlags[0].incomingData(1.0, 2.0, Variant::Flags{"F1"});
        engine1->inputsFlags[0].incomingData(2.0, 3.0, Variant::Flags{"F2"});

        core.statsStartTime = 3.0;
        core.statsValue = -2.0;
        TestChainEngine *engine2 = new TestChainEngine;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                engine1->serialize(stream);
            }
            engine1->endAll();
            engine1->mux.signalTerminate(true);
            QVERIFY(engine1->mux.wait(30));
            delete engine1;

            core.createdNodes.clear();
            core.uid = 0;
            QDataStream stream(&data, QIODevice::ReadOnly);
            engine2->deserialize(stream);
            ((SmootherChainCoreBinned *) &core)->deserializeFlags(stream, engine2);
            engine2->mux.setEgress(&end);
        }

        for (int i = 0; i < core.createdNodes.size(); i++) {
            QCOMPARE(core.createdNodes.at(i)->uid, i);
        }

        QCOMPARE(engine2->inputs.size(), 1);
        QCOMPARE(engine2->inputsFlags.size(), 1);
        engine2->inputsFlags[0].incomingData(3.0, 4.0, Variant::Flags{"F3"});
        engine2->inputsFlags[0].incomingData(4.0, 5.0, Variant::Flags{"F4"});
        engine2->inputs[0].incomingData(3.0, 4.0, 100.003);
        engine2->inputs[0].incomingData(4.0, 5.0, 100.004);

        engine2->endAll();
        QVERIFY(engine2->mux.wait(30));
        delete engine2;

        auto values = end.values();

        Variant::Root f;

        f.write().setFlags({"F1", "added"});
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "out"), f, 1.0, 2.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(101.001),
                1.0, 2.0)));
        f.write().setFlags({"F2", "added"});
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "out"), f, 2.0, 3.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(101.002),
                2.0, 3.0)));
        f.write().setFlags({"F3", "added"});
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "out"), f, 3.0, 4.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(101.003),
                3.0, 4.0)));
        f.write().setFlags({"F4", "added"});
        QVERIFY(contains(values, SequenceValue(SequenceName("bnd", "raw", "out"), f, 4.0, 5.0)));
        QVERIFY(contains(values, SequenceValue(
                SequenceName("bnd", "raw", "var1", SequenceName::Flavors{"cover"}),
                Variant::Root(101.004),
                4.0, 5.0)));
    }

};

QTEST_APPLESS_MAIN(TestBinnedChain)

#include "binnedchain.moc"
