/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>
#include <QVector>

#include "smoothing/tukey.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Smoothing;

Q_DECLARE_METATYPE(QVector<double>);

namespace QTest {
template<>
char *toString(const QVector<double> &output)
{
    QByteArray ba;
    for (int i = 0, max = output.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        if (FP::defined(output.at(i)))
            ba += QByteArray::number(output.at(i));
        else
            ba += "Undefined";
    }
    return qstrdup(ba.data());
}
}

class TestTukey : public QObject {
Q_OBJECT

private slots:

    void median3()
    {
        QFETCH(QVector<double>, input);
        QFETCH(QVector<double>, expected);
        QVector<double> result(input.size(), -1.0);
        Tukey::median3(input.constBegin(), input.constEnd(), result.begin());
        QCOMPARE(result, expected);
    }

    void median3_data()
    {
        QTest::addColumn<QVector<double> >("input");
        QTest::addColumn<QVector<double> >("expected");

        QTest::newRow("Empty") << QVector<double>() << QVector<double>();
        QTest::newRow("One") << (QVector<double>() << 1.0) << (QVector<double>() << 1.0);
        QTest::newRow("Two") <<
                (QVector<double>() << 1.0 << 2.0) <<
                (QVector<double>() << 1.0 << 2.0);
        QTest::newRow("Three A") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0) <<
                (QVector<double>() << 1.0 << 2.0 << 3.0);
        QTest::newRow("Three B") <<
                (QVector<double>() << 2.0 << 1.0 << 3.0) <<
                (QVector<double>() << 2.0 << 2.0 << 3.0);
        QTest::newRow("Three C") <<
                (QVector<double>() << 3.0 << 2.0 << 1.0) <<
                (QVector<double>() << 3.0 << 2.0 << 1.0);
        QTest::newRow("Three D") <<
                (QVector<double>() << 1.0 << 1.0 << 3.0) <<
                (QVector<double>() << 1.0 << 1.0 << 3.0);
        QTest::newRow("Three E") <<
                (QVector<double>() << 3.0 << 2.0 << 3.0) <<
                (QVector<double>() << 3.0 << 3.0 << 3.0);
        QTest::newRow("Three F") <<
                (QVector<double>() << 1.0 << 1.0 << 1.0) <<
                (QVector<double>() << 1.0 << 1.0 << 1.0);
        QTest::newRow("Three G") <<
                (QVector<double>() << 3.0 << 1.0 << 1.0) <<
                (QVector<double>() << 3.0 << 1.0 << 1.0);
        QTest::newRow("Four") <<
                (QVector<double>() << 1.0 << 2.0 << 2.0 << 3.0) <<
                (QVector<double>() << 1.0 << 2.0 << 2.0 << 3.0);
        QTest::newRow("Five") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0 << 2.0 << 3.0) <<
                (QVector<double>() << 1.0 << 2.0 << 2.0 << 3.0 << 3.0);
        QTest::newRow("Six") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0 << 2.0 << 3.0 << 4.0) <<
                (QVector<double>() << 1.0 << 2.0 << 2.0 << 3.0 << 3.0 << 4.0);
    }

    void median3R()
    {
        QFETCH(QVector<double>, input);
        QFETCH(QVector<double>, expected);
        QVector<double> result(input.size(), -1.0);
        bool code = Tukey::median3R(input.begin(), input.end(), result.begin(), result.end(), true);
        QVERIFY(code);
        QCOMPARE(result, expected);
    }

    void median3R_data()
    {
        QTest::addColumn<QVector<double> >("input");
        QTest::addColumn<QVector<double> >("expected");

        QTest::newRow("Empty") << QVector<double>() << QVector<double>();
        QTest::newRow("One") << (QVector<double>() << 1.0) << (QVector<double>() << 1.0);
        QTest::newRow("Two") <<
                (QVector<double>() << 1.0 << 2.0) <<
                (QVector<double>() << 1.0 << 2.0);
        QTest::newRow("Three") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0) <<
                (QVector<double>() << 1.0 << 2.0 << 3.0);
        QTest::newRow("Four") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0 << 3.0) <<
                (QVector<double>() << 1.0 << 2.0 << 3.0 << 3.0);
        QTest::newRow("Six") <<
                (QVector<double>() << 3.0 << 2.0 << 5.0 << 1.0 << 4.0 << 5.0) <<
                (QVector<double>() << 3.0 << 3.0 << 3.0 << 4.0 << 4.0 << 5.0);
    }

    void split()
    {
        QFETCH(QVector<double>, input);
        QFETCH(QVector<double>, expected);
        QVector<double> result(input.size(), -1.0);
        Tukey::split(input.begin(), input.end(), result.begin());
        QCOMPARE(result, expected);
    }

    void split_data()
    {
        QTest::addColumn<QVector<double> >("input");
        QTest::addColumn<QVector<double> >("expected");

        QTest::newRow("Empty") << QVector<double>() << QVector<double>();
        QTest::newRow("One") << (QVector<double>() << 1.0) << (QVector<double>() << 1.0);
        QTest::newRow("Two") <<
                (QVector<double>() << 1.0 << 2.0) <<
                (QVector<double>() << 1.0 << 2.0);
        QTest::newRow("Three") <<
                (QVector<double>() << 1.0 << 2.0 << 2.0) <<
                (QVector<double>() << 1.0 << 2.0 << 2.0);
        QTest::newRow("Four A") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0 << 4.0) <<
                (QVector<double>() << 1.0 << 2.0 << 3.0 << 4.0);
        QTest::newRow("Four B") <<
                (QVector<double>() << 1.0 << 1.0 << 4.0 << 1.0) <<
                (QVector<double>() << 1.0 << 4.0 << 4.0 << 1.0);
        QTest::newRow("Five A") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0 << 4.0 << 5.0) <<
                (QVector<double>() << 1.0 << 2.0 << 3.0 << 4.0 << 5.0);
        QTest::newRow("Five B") <<
                (QVector<double>() << 1.0 << 1.0 << 4.0 << 1.0 << 2.0) <<
                (QVector<double>() << 1.0 << 4.0 << 4.0 << 1.0 << 2.0);
        QTest::newRow("Five C") <<
                (QVector<double>() << 3.0 << 5.0 << 4.0 << 1.0 << 1.0) <<
                (QVector<double>() << 3.0 << 5.0 << 4.0 << 2.0 << 1.0);
        QTest::newRow("Five D") <<
                (QVector<double>() << 1.0 << 1.0 << 4.0 << 2.0 << 2.0) <<
                (QVector<double>() << 1.0 << 4.0 << 4.0 << 4.0 << 2.0);
        QTest::newRow("Six A") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0 << 4.0 << 5.0 << 6.0) <<
                (QVector<double>() << 1.0 << 2.0 << 3.0 << 4.0 << 5.0 << 6.0);
        QTest::newRow("Six B") <<
                (QVector<double>() << 1.0 << 1.0 << 4.0 << 1.0 << 2.0 << 2.0) <<
                (QVector<double>() << 1.0 << 4.0 << 4.0 << 1.0 << 1.0 << 2.0);
        QTest::newRow("Six C") <<
                (QVector<double>() << 2.25 << 2.0 << 1.0 << 1.0 << 3.0 << 3.25) <<
                (QVector<double>() << 2.25 << 2.0 << 1.5 << 2.5 << 3.0 << 3.25);
        QTest::newRow("Seven A") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0 << 4.0 << 5.0 << 6.0 << 7.0) <<
                (QVector<double>() << 1.0 << 2.0 << 3.0 << 4.0 << 5.0 << 6.0 << 7.0);
        QTest::newRow("Seven B") <<
                (QVector<double>() << 1.0 << 2.75 << 3.0 << 4.0 << 4.0 << 2.0 << 1.5) <<
                (QVector<double>() << 1.0 << 2.75 << 3.0 << 3.5 << 3.0 << 2.0 << 1.5);
        QTest::newRow("Seven C") <<
                (QVector<double>() << 2.75 << 3.0 << 4.0 << 4.0 << 4.0 << 2.0 << 1.5) <<
                (QVector<double>() << 2.75 << 3.0 << 3.5 << 4.0 << 3.0 << 2.0 << 1.5);
    }

    void hanning()
    {
        QFETCH(QVector<double>, input);
        QFETCH(QVector<double>, expected);
        QVector<double> result(input.size(), -1.0);
        Tukey::hanning(input.constBegin(), input.constEnd(), result.begin());
        QCOMPARE(result, expected);
    }

    void hanning_data()
    {
        QTest::addColumn<QVector<double> >("input");
        QTest::addColumn<QVector<double> >("expected");

        QTest::newRow("Empty") << QVector<double>() << QVector<double>();
        QTest::newRow("One") << (QVector<double>() << 1.0) << (QVector<double>() << 1.0);
        QTest::newRow("Two") <<
                (QVector<double>() << 1.0 << 2.0) <<
                (QVector<double>() << 1.0 << 2.0);
        QTest::newRow("Three A") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0) <<
                (QVector<double>() << 1.0 << 2.0 << 3.0);
        QTest::newRow("Three B") <<
                (QVector<double>() << 2.0 << 1.0 << 3.0) <<
                (QVector<double>() << 2.0 << 1.75 << 3.0);
        QTest::newRow("Four") <<
                (QVector<double>() << 1.0 << 2.0 << 1.0 << 3.0) <<
                (QVector<double>() << 1.0 << 1.5 << 1.75 << 3.0);
        QTest::newRow("Five") <<
                (QVector<double>() << 1.0 << 2.0 << 1.0 << 3.0 << 2.0) <<
                (QVector<double>() << 1.0 << 1.5 << 1.75 << 2.25 << 2.0);
    }
};

QTEST_APPLESS_MAIN(TestTukey)

#include "tukey.moc"
