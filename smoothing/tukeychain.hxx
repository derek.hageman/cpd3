/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGTUKEYCHAIN_H
#define CPD3SMOOTHINGTUKEYCHAIN_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDataStream>

#include "smoothing/smoothing.hxx"
#include "smoothing/smoothingchain.hxx"
#include "smoothing/blocksmootherchain.hxx"

namespace CPD3 {
namespace Smoothing {

/**
 * A smoother that handles data based on a Tukey style "3RSSH" smoother.
 */
class CPD3SMOOTHING_EXPORT Tukey3RSSHChainNode : public BlockSmootherChain {
    class Handler3RSSH : public BlockSmootherChain::Handler {
    public:
        Handler3RSSH();

        virtual ~Handler3RSSH();

        virtual bool process(BlockSmootherChain::Value *&buffer, size_t &size);
    };

public:
    /**
     * Create a new Tukey 3RSSH smoother.  This takes ownership of the gap
     * specification
     * 
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param smoothOverUndefined if set then undefined values are simply ignored instead of breaking up the smoother
     * @param outputTarget      the output target for smoothed values
     */
    Tukey3RSSHChainNode(Data::DynamicTimeInterval *setGap,
                        bool smoothOverUndefined,
                        SmootherChainTarget *outputTarget);

    /**
     * Create a new 3RSSH smoother from saved state.
     * 
     * @param stream            the stream to read state from
     * @param outputTarget      the output target for smoothed values
     */
    Tukey3RSSHChainNode(QDataStream &stream, SmootherChainTarget *outputTarget);

    virtual ~Tukey3RSSHChainNode();

protected:
    virtual BlockSmootherChain::Handler *createHandler(double start, double end);
};

/**
 * A chain core for Tukey 3RSSH smoothers.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreTukey3RSSH : public SmootherChainCoreSimple {
    Data::DynamicTimeInterval *gap;
    bool smoothOverUndefined;
public:
    /**
     * Create a new core that uses a Tukey 3RSSH smoother.  This takes 
     * ownership of the gap, if any
     * 
     * @param setGap                the maximum time to allow gaps or NULL for none
     * @param setSmoothUndefined    if set then undefined values are simply ignored instead of breaking up the smoother
     */
    SmootherChainCoreTukey3RSSH(Data::DynamicTimeInterval *setGap = NULL,
                                bool setSmoothUndefined = true);

    virtual ~SmootherChainCoreTukey3RSSH();

    virtual void createSmoother(SmootherChainCoreEngineInterface *engine,
                                SmootherChainTarget *&input,
                                SmootherChainTarget *output);

    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainTarget *&input,
                                     SmootherChainTarget *output);

    virtual Data::Variant::Root getProcessingMetadata(double time) const;

    virtual QSet<double> getProcessingMetadataBreaks() const;
};

}
}

#endif
