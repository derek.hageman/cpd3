/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGFOURIERCHAIN_H
#define CPD3SMOOTHINGFOURIERCHAIN_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDataStream>

#include "smoothing/smoothing.hxx"
#include "smoothing/smoothingchain.hxx"
#include "smoothing/blocksmootherchain.hxx"

namespace CPD3 {
namespace Smoothing {

/**
 * A smoother that handles data based on high and/or low pass filters
 * in Fourier frequency space.
 */
class CPD3SMOOTHING_EXPORT FourierChainNode : public BlockSmootherChain {
    class HandlerFourier : public BlockSmootherChain::Handler {
        double low;
        double high;
    public:
        HandlerFourier(double setLow, double setHigh);

        virtual ~HandlerFourier();

        virtual bool process(BlockSmootherChain::Value *&buffer, size_t &size);
    };

    Data::DynamicTimeInterval *low;
    Data::DynamicTimeInterval *high;
public:
    /**
     * Create a new Fourier smoother.  This takes ownership of the high, low
     * and gap specifications.
     * 
     * @param setLow            the low cutoff interval or NULL for none
     * @param setHigh           the high cutoff interval or NULL for none
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param smoothOverUndefined if set then undefined values are simply ignored instead of breaking up the smoother
     * @param outputTarget      the output target for smoothed values
     */
    FourierChainNode(Data::DynamicTimeInterval *setLow,
                     Data::DynamicTimeInterval *setHigh,
                     Data::DynamicTimeInterval *setGap,
                     bool smoothOverUndefined,
                     SmootherChainTarget *outputTarget);

    /**
     * Create a new Fourier smoother from saved state.
     * 
     * @param stream            the stream to read state from
     * @param outputTarget      the output target for smoothed values
     */
    FourierChainNode(QDataStream &stream, SmootherChainTarget *outputTarget);

    virtual ~FourierChainNode();

    virtual void serialize(QDataStream &stream) const;

protected:
    virtual BlockSmootherChain::Handler *createHandler(double start, double end);
};

/**
 * A chain core for Fourier smoothers.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreFourier : public SmootherChainCoreSimple {
    Data::DynamicTimeInterval *low;
    Data::DynamicTimeInterval *high;
    Data::DynamicTimeInterval *gap;
    bool smoothOverUndefined;
public:
    /**
     * Create a new core that uses a Fourier transform filter.  This takes 
     * ownership of the gap, high, and low bands, if any
     * 
     * @param setLow                the low cutoff interval or NULL for none
     * @param setHigh               the high cutoff interval or NULL for none
     * @param setGap                the maximum time to allow gaps or NULL for none
     * @param setSmoothUndefined    if set then undefined values are simply ignored instead of breaking up the smoother
     */
    SmootherChainCoreFourier(Data::DynamicTimeInterval *setLow = NULL,
                             Data::DynamicTimeInterval *setHigh = NULL,
                             Data::DynamicTimeInterval *setGap = NULL,
                             bool setSmoothUndefined = true);

    virtual ~SmootherChainCoreFourier();

    virtual void createSmoother(SmootherChainCoreEngineInterface *engine,
                                SmootherChainTarget *&input,
                                SmootherChainTarget *output);

    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainTarget *&input,
                                     SmootherChainTarget *output);

    virtual Data::Variant::Root getProcessingMetadata(double time) const;

    virtual QSet<double> getProcessingMetadataBreaks() const;
};

}
}

#endif
