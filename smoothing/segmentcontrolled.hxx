/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGSEGMENTCONTROLLED_H
#define CPD3SMOOTHINGSEGMENTCONTROLLED_H

#include "core/first.hxx"

#include <memory>
#include <unordered_map>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QDataStream>

#include "smoothing/smoothing.hxx"
#include "smoothing/smoothingchain.hxx"
#include "smoothing/binnedchain.hxx"
#include "smoothing/smoothingengine.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "datacore/stream.hxx"
#include "datacore/dynamicsequenceselection.hxx"

namespace CPD3 {
namespace Smoothing {

/**
 * A segment controller.  This is used to communicate to the segment averages
 * the start and end times of the active segment.  The controller is always
 * fed values before later values could ever reach it.  That is, it always
 * has the start and end time before any of the data streams advance past
 * that time (but they may receive values before it).  This ensures that
 * the segment controller is always "ahead" of anything it could receive
 * otherwise.
 */
class CPD3SMOOTHING_EXPORT SegmentController {
public:
    virtual ~SegmentController();

    /**
     * Mark the start of a segment.  This may not be called before the
     * end of the current segment (if any).
     * 
     * @param time  the start time of the segment
     */
    virtual void segmentStart(double time) = 0;

    /**
     * Mark the end of a segment.  This may only be called after the start
     * of a segment.
     * 
     * @param time  the end time of the segment
     */
    virtual void segmentEnd(double time) = 0;
};

/**
 * The interface provided to a segment controlled smoother engine
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreEngineInterfaceSegmenter
        : public SmootherChainCoreEngineAuxiliaryInterface {
public:
    virtual ~SmootherChainCoreEngineInterfaceSegmenter();

    /**
     * Add a segment controller to the engine's dispatch.  This controller
     * remains owned by the current chain and is expected to remain valid
     * until the entire chain can be deleted.
     * 
     * @param controller    the controller to add
     */
    virtual void addSegmentController(SegmentController *controller) = 0;
};

/**
 * The base implementation for externally controlled segment binning.  This 
 * takes segments of non-overlapping data and combined them into bins controlled
 * by an external interface.
 * <br>
 * This may keep copies of the data, so it needs to be copyable and 
 * serializable.
 * 
 * @param BinData   the data type used as bin data
 */
template<typename BinData>
class SegmentControlledBinner {
    Data::DynamicTimeInterval *gap;

    bool haveValidData;
    double dataStart;
    double dataEnd;
    BinData currentData;
    double lastValueEnd;

    bool discardIntersecting;

    bool binEmpty;
    bool incomingEnded;

    struct SegmentBuffer {
        double start;
        double end;

        inline SegmentBuffer() = default;

        inline explicit SegmentBuffer(double start) : start(start), end(FP::undefined())
        { }
    };

    std::deque<SegmentBuffer> segments;

    class Controller : public SegmentController {
        SegmentControlledBinner<BinData> *binner;
    public:
        Controller(SegmentControlledBinner<BinData> *b) : binner(b)
        { }

        virtual ~Controller()
        { }

        virtual void segmentStart(double time)
        { binner->segmentStart(time); }

        virtual void segmentEnd(double time)
        { binner->segmentEnd(time); }
    };

    friend class Controller;

    Controller controller;

    void segmentStart(double time)
    {
        if (incomingEnded)
            return;

        /* Any existing segments have ended already */
        Q_ASSERT(segments.empty() ||
                         (FP::defined(time) &&
                                 FP::defined(segments.back().end) &&
                                 segments.back().end <= time));
        Q_ASSERT(!segments.empty() || !haveValidData);

        segments.emplace_back(time);
    }

    void segmentEnd(double time)
    {
        if (incomingEnded)
            return;

        Q_ASSERT(!segments.empty());
        Q_ASSERT(!FP::defined(time) ||
                         !FP::defined(segments.back().start) ||
                         time >= segments.back().start);

        auto &seg = segments.back();
        seg.end = time;

        /* Check degenerate case of an empty segment ending.  This can occur
         * due to a gap termination equal to the end time. */
        if (gap &&
                segments.size() == 1 &&
                FP::defined(seg.end) &&
                FP::defined(seg.start) &&
                seg.end <= seg.start) {
            Q_ASSERT(seg.end == seg.start);
            segments.clear();
            return;
        }

        /* If this is the first segment, see if we can add to it or complete it.
         * If it isn't then it's already been handled. */
        if (segments.size() == 1 && haveValidData) {
            if (!FP::defined(time)) {
                addToBin(dataStart, dataEnd, currentData);
                haveValidData = false;
                binEmpty = false;
            } else if (FP::defined(dataEnd)) {
                if (dataEnd > time) {
                    if (discardIntersecting) {
                        if (!binEmpty) {
                            processBin(seg.start, time);
                        } else {
                            advanceBin(time);
                        }
                    } else {
                        addToBin(dataStart, time, currentData);
                        processBin(seg.start, time);
                    }
                    segments.clear();
                    binEmpty = true;
                } else {
                    addToBin(dataStart, dataEnd, currentData);
                    haveValidData = false;
                    if (dataEnd == time) {
                        processBin(seg.start, time);
                        segments.clear();
                        binEmpty = true;
                    } else {
                        binEmpty = false;
                    }
                }
            } else {
                if (discardIntersecting) {
                    if (!binEmpty) {
                        processBin(seg.start, time);
                    } else {
                        advanceBin(time);
                    }
                } else {
                    addToBin(dataStart, time, currentData);
                    processBin(seg.start, time);
                }
                segments.clear();
                binEmpty = true;
            }
        }
    }

public:
    /**
     * Construct a binner with the given gap.  This takes ownership of the gap 
     * specification.
     * 
     * @param g         the gap interval to use, or NULL
     * @param discard   the intersecting discard state
     */
    SegmentControlledBinner(Data::DynamicTimeInterval *g, bool discard = false) : gap(g),
                                                                                  haveValidData(
                                                                                          false),
                                                                                  dataStart(
                                                                                          FP::undefined()),
                                                                                  dataEnd(FP::undefined()),
                                                                                  currentData(),
                                                                                  lastValueEnd(
                                                                                          FP::undefined()),
                                                                                  discardIntersecting(
                                                                                          discard),
                                                                                  binEmpty(true),
                                                                                  incomingEnded(
                                                                                          false),
                                                                                  segments(),
                                                                                  controller(this)
    { }

    /**
     * Construct a binner with no gap specification.
     */
    SegmentControlledBinner() : gap(NULL),
                                haveValidData(false),
                                dataStart(FP::undefined()),
                                dataEnd(FP::undefined()),
                                currentData(),
                                lastValueEnd(FP::undefined()),
                                discardIntersecting(false),
                                binEmpty(true),
                                incomingEnded(false),
                                segments(),
                                controller(this)
    { }

    virtual ~SegmentControlledBinner()
    {
        if (gap)
            delete gap;
    }

    /**
     * Save the binner to the given stream.
     * 
     * @param stream    the stream target
     */
    void writeSerial(QDataStream &stream) const
    {
        stream << gap << lastValueEnd << discardIntersecting << binEmpty;

        stream << haveValidData;
        if (haveValidData)
            stream << dataStart << dataEnd << currentData;

        Serialize::container(stream, segments, [&](const SegmentBuffer &value) {
            stream << value.start << value.end;
        });
    }

    /**
     * Restore the binner from the given stream.
     * 
     * @param stream    the stream source
     */
    void readSerial(QDataStream &stream)
    {
        if (gap)
            delete gap;
        stream >> gap >> lastValueEnd >> discardIntersecting >> binEmpty;

        stream >> haveValidData;
        if (haveValidData) {
            stream >> dataStart >> dataEnd >> currentData;
        } else {
            dataStart = FP::undefined();
            dataEnd = FP::undefined();
        }

        Deserialize::container(stream, segments, [&]() {
            SegmentBuffer value;
            stream >> value.start >> value.end;
            return value;
        });
    }

    /**
     * Get the segment controller.  The returned class is owned by the
     * binner and should not be deleted.
     * 
     * @return a segment controller
     */
    inline SegmentController *getController()
    { return &controller; }

protected:
    /**
     * Set the gap specification.  This takes ownership.
     * 
     * @param g     the gap interval to use, or NULL
     */
    void setGap(Data::DynamicTimeInterval *g)
    {
        Q_ASSERT(segments.empty());
        if (gap != NULL)
            delete gap;
        gap = g;
    }

    /**
     * Change the intersecting discard state.  If set then any segments that
     * intersect (those that are not entirely contained within) the segment
     * are discarded instead of clipped.
     * <br>
     * The default is false.
     * 
     * @param d the intersecting discard state
     */
    void setDiscardIntersecting(bool d)
    {
        Q_ASSERT(segments.empty());
        discardIntersecting = d;
    }

    /**
     * Add an incoming segment of data to the binner.  This may cause
     * the emission of output bins with processBin(double, double).
     * 
     * @param start     the start time of the segment
     * @param end       the end time of the segment
     * @param data      the data to add
     */
    void incomingSegment(double start, double end, const BinData &data)
    {
        Q_ASSERT(!incomingEnded);

        auto firstSegment = segments.begin();
        auto endSegments = segments.end();

        /* Gap check, change the current segment if there's a gap with
         * the previous data value. */
        if (gap && firstSegment != endSegments && FP::defined(lastValueEnd)) {
            double gapCheck = gap->apply(start, lastValueEnd);
            if (FP::defined(gapCheck) && start > gapCheck) {
                /* If we have data in the bin, process it and adjust the
                 * segment if required. */
                if (haveValidData || !binEmpty) {
                    if (haveValidData) {
                        addToBin(dataStart, dataEnd, currentData);
                        haveValidData = false;
                    }

                    /* The gap also completes this bin. */
                    if (FP::defined(firstSegment->end) && start > firstSegment->end) {
                        processBin(firstSegment->start, firstSegment->end);
                        ++firstSegment;
                    } else {
                        processBin(firstSegment->start, lastValueEnd);
                        firstSegment->start = start;
                    }

                    binEmpty = true;
                } else if (!FP::defined(firstSegment->end) || start < firstSegment->end) {
                    /* No data in the bin, so we just adjust the segment */
                    firstSegment->start = start;
                }
            }
        }
        Q_ASSERT(Range::compareStart(lastValueEnd, start) <= 0);
        lastValueEnd = end;

        /* If we have valid data then it will go into the first bin, but
         * never any other ones (since they'd always begin after its end). */
        if (haveValidData) {
            Q_ASSERT(firstSegment != endSegments);
            Q_ASSERT(FP::defined(dataEnd));
            Q_ASSERT(Range::compareEnd(firstSegment->end, dataEnd) >= 0);

            if (!FP::defined(firstSegment->end) || dataEnd < firstSegment->end) {
                addToBin(dataStart, dataEnd, currentData);
                binEmpty = false;
            } else {
                addToBin(dataStart, dataEnd, currentData);
                processBin(firstSegment->start, firstSegment->end);
                ++firstSegment;
                binEmpty = true;
            }
            haveValidData = false;
        }

        /* An undefined start means this is the last possible value, so it
         * may be co-temporal with the last segment. */
        if (!FP::defined(end)) {
            /* No segments left, so done */
            if (firstSegment == endSegments) {
                segments.clear();
                return;
            }

            Q_ASSERT(firstSegment == endSegments - 1);
            Q_ASSERT(Range::compareStartEnd(firstSegment->start, end) < 0);

            /* This might be a truely undefined end or the segments might
             * just not have received their end yet, so buffer it either way */
            if (!FP::defined(firstSegment->end)) {
                segments.erase(segments.begin(), firstSegment);

                const auto &f = segments.front();
                if (FP::defined(f.start) && (!FP::defined(start) || start < f.start)) {
                    if (discardIntersecting)
                        return;
                    dataStart = f.start;
                } else {
                    dataStart = start;
                }
                dataEnd = end;
                currentData = data;
                haveValidData = true;
                return;
            }

            /* Finite segment end, so our infinite end must be clipped to it */
            if (discardIntersecting)
                return;

            if (FP::defined(firstSegment->start) &&
                    (!FP::defined(start) || start < firstSegment->start)) {
                addToBin(firstSegment->start, firstSegment->end, data);
            } else {
                addToBin(start, firstSegment->end, data);
            }
            processBin(firstSegment->start, firstSegment->end);
            binEmpty = true;
            segments.clear();
            return;
        }

        for (;; ++firstSegment) {
            /* Segments are always ahead, so we can discard the old value if
             * we don't have an active segment since it could never enter
             * into an upcoming segment. */
            if (firstSegment == endSegments) {
                segments.clear();
                advanceBin(end);
                return;
            }

            /* Before the start of the first segment, so this value can never
             * enter into it (the start time would always be defined before
             * the end). */
            if (FP::defined(firstSegment->start) && end <= firstSegment->start) {
                segments.erase(segments.begin(), firstSegment);
                advanceBin(end);
                return;
            }

            /* Undefined end, so this is either the last segment or an
             * in progress one.  In either case we queue the incoming value for
             * addition. */
            if (!FP::defined(firstSegment->end)) {
                Q_ASSERT(firstSegment == endSegments - 1);
                Q_ASSERT(Range::compareStartEnd(firstSegment->start, end) < 0);

                segments.erase(segments.begin(), firstSegment);

                const auto &f = segments.front();
                if (FP::defined(f.start) && (!FP::defined(start) || start < f.start)) {
                    if (discardIntersecting)
                        return;
                    dataStart = f.start;
                } else {
                    dataStart = start;
                }
                dataEnd = end;
                currentData = data;
                haveValidData = true;
                return;
            }

            if (FP::defined(firstSegment->start) && start < firstSegment->start) {
                if (discardIntersecting) {
                    /* Final bin we can fill */
                    if (end < firstSegment->end) {
                        break;
                    } else {
                        if (binEmpty) {
                            advanceBin(firstSegment->end);
                        } else {
                            processBin(firstSegment->start, firstSegment->end);
                            binEmpty = true;
                        }
                    }
                } else {
                    if (end >= firstSegment->end) {
                        addToBin(firstSegment->start, firstSegment->end, data);
                        processBin(firstSegment->start, firstSegment->end);
                        binEmpty = true;
                    } else {
                        /* Final bin we can fill */
                        addToBin(firstSegment->start, end, data);
                        binEmpty = false;
                        break;
                    }
                }
            } else if (FP::defined(start) && start >= firstSegment->end) {
                /* Starts after the end of the first segment, so just keep 
                 * advancing until we hit one that we should actually insert
                 * into. */
                if (binEmpty) {
                    advanceBin(firstSegment->end);
                } else {
                    processBin(firstSegment->start, firstSegment->end);
                }
                binEmpty = true;
            } else {
                if (end >= firstSegment->end) {
                    if (discardIntersecting && end != firstSegment->end) {
                        if (binEmpty) {
                            advanceBin(firstSegment->end);
                        } else {
                            processBin(firstSegment->start, firstSegment->end);
                            binEmpty = true;
                        }
                    } else {
                        addToBin(start, firstSegment->end, data);
                        processBin(firstSegment->start, firstSegment->end);
                        binEmpty = true;
                    }
                } else {
                    /* Ends before the end of the first segment, so this is
                     * the last one it could fill. */
                    addToBin(start, end, data);
                    binEmpty = false;
                    break;
                }
            }
        }

        segments.erase(segments.begin(), firstSegment);
    }

    /**
     * Advance the binner to the given time.
     * 
     * @param time      the time to advance to
     */
    void incomingSegmentAdvance(double time)
    {
        Q_ASSERT(!incomingEnded);
        Q_ASSERT(FP::defined(time));

        if (segments.empty()) {
            advanceBin(time);
            return;
        }

        auto firstSegment = segments.begin();
        for (auto segEnd = segments.end(); firstSegment != segEnd; ++firstSegment) {
            if (!FP::defined(firstSegment->end)) {
                Q_ASSERT(firstSegment == segEnd - 1);
                break;
            }
            if (time < firstSegment->end)
                break;

            if (binEmpty) {
                advanceBin(firstSegment->end);
            } else {
                if (gap != NULL) {
                    double gapCheck = gap->apply(time, lastValueEnd);
                    if (FP::defined(gapCheck) && time > gapCheck) {
                        processBin(firstSegment->start, lastValueEnd);
                        binEmpty = true;
                    }
                }
                if (!binEmpty)
                    processBin(firstSegment->start, firstSegment->end);
            }
            binEmpty = true;
        }

        segments.erase(segments.begin(), firstSegment);

        if (segments.empty()) {
            advanceBin(time);
        }
    }

    /**
     * Notify the binner that all incoming segments have been received.  This
     * will cause the emission of the final bins, if any.
     */
    void incomingSegmentsCompleted()
    {
        Q_ASSERT(!incomingEnded);

        /* At this point we have the segment end time so if it's
         * infinite, the segment truly is infinite. */

        if (haveValidData && !segments.empty()) {
            if (FP::defined(segments.front().end) &&
                    FP::defined(dataEnd) &&
                    dataEnd <= segments.front().end) {
                addToBin(dataStart, dataEnd, currentData);
                binEmpty = false;
            } else if (!discardIntersecting) {
                addToBin(dataStart, segments.front().end, currentData);
                binEmpty = false;
            }
        }

        if (!binEmpty) {
            Q_ASSERT(!segments.empty());
            processBin(segments.front().start, segments.front().end);
        }

        /* Could technically advance all other bins, but it's pointless
         * since this is the end of the stream anyway. */

        segments.clear();
        incomingEnded = true;

        completedBinning();
    }

    /**
     * Add the data to the end of the current bin.
     * 
     * @param start     the start time of the data
     * @param end       the end time of the data
     * @param data      the data being added
     */
    virtual void addToBin(double start, double end, const BinData &data) = 0;

    /**
     * Called after all bin data has been emitted
     */
    virtual void completedBinning() = 0;

    /**
     * Called when a bin is completed.  This should also clear the contents
     * of the bin data.  This will only be called when the bin is not empty.
     * 
     * @param start     the start time of the bin
     * @param end       the end time of the bin
     */
    virtual void processBin(double start, double end) = 0;

    /**
     * Called when the binning time has been advanced without data
     * being processed.
     * 
     * @param time      the next possible bin time
     */
    virtual void advanceBin(double time) = 0;
};

/**
 * A smoother chain element for calculating fixed time binning.  This allows
 * the subclass to just implement a processor that is given a list of
 * values to process.
 * 
 * @param N the number of inputs
 */
template<std::size_t N>
class SmootherChainInputSegmentedBinning
        : public SmootherChainNode,
          protected SmootherChainInputSegmentProcessor<N>,
          protected SegmentControlledBinner<Internal::BinArrayWrapper<N> > {

    typedef Internal::BinArrayWrapper<N> Storage;
    typedef SegmentControlledBinner<Storage> Binner;
    typedef SmootherChainInputSegmentProcessor<N> Processor;

    /* Don't need a mutex for this, since it's only written once we no
     * longer care. */
    bool segmentsFinished;

    inline void streamDone(std::size_t stream)
    {
        if (Processor::segmentEnd(stream)) {
            Binner::incomingSegmentsCompleted();
            segmentsFinished = true;
        }
    }

    class TargetN : public SmootherChainTarget {
    public:
        SmootherChainInputSegmentedBinning *node;
        std::size_t index;

        TargetN()
        { }

        virtual ~TargetN()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(index, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(index, time); }

        virtual void endData()
        { node->streamDone(index); }
    };

    friend class TargetN;

    TargetN targets[N];

    QVector<double> buffer[N];
    QVector<double> durations;

public:
    /**
     * Create a new binning object.
     * 
     * @param gap           the maximum gap to use, the binner takes ownership
     */
    SmootherChainInputSegmentedBinning(Data::DynamicTimeInterval *gap) : Binner(gap),
                                                                         buffer(),
                                                                         durations()
    {
        for (std::size_t i = 0; i < N; i++) {
            targets[i].node = this;
            targets[i].index = i;
        }
    }

    /**
     * Create a new binning object with no interval or gap specification.
     */
    SmootherChainInputSegmentedBinning() : Binner(), buffer(), durations()
    {
        for (std::size_t i = 0; i < N; i++) {
            targets[i].node = this;
            targets[i].index = i;
        }
    }

    /**
     * Create a new binning object, deserializing the state from the given
     * stream.
     * 
     * @param stream        the stream to read state from
     */
    SmootherChainInputSegmentedBinning(QDataStream &stream) : Binner(NULL), buffer(), durations()
    {
        Processor::readSerial(stream);
        Binner::readSerial(stream);
        stream >> durations;
        for (std::size_t i = 0; i < N; i++) {
            stream >> buffer[i];
        }

        for (std::size_t i = 0; i < N; i++) {
            targets[i].node = this;
            targets[i].index = i;
        }
    }

    virtual ~SmootherChainInputSegmentedBinning()
    { }

    virtual bool finished()
    { return segmentsFinished; }

    virtual void pause()
    { Processor::freezeState(); }

    virtual void resume()
    { Processor::unfreezeState(); }

    virtual void serialize(QDataStream &stream) const
    {
        Processor::writeSerial(stream);
        Binner::writeSerial(stream);
        stream << durations;
        for (std::size_t i = 0; i < N; i++) {
            stream << buffer[i];
        }
    }

    /**
     * Get the segment controller.  The returned class is owned by the
     * binner and should not be deleted.
     * 
     * @return a segment controller
     */
    inline SegmentController *getController()
    { return Binner::getController(); }

protected:
    virtual void segmentDone(double start, double end, const double values[N])
    { Binner::incomingSegment(start, end, Storage(values)); }

    virtual void allStreamsAdvanced(double time)
    { Binner::incomingSegmentAdvance(time); }

    virtual void addToBin(double start, double end, const Storage &data)
    {
        for (std::size_t i = 0; i < N; i++) {
            buffer[i].append(data[i]);
        }
        if (FP::defined(start) && FP::defined(end))
            durations.append(end - start);
        else
            durations.append(FP::undefined());
    }

    virtual void processBin(double start, double end)
    {
        processBin(start, end, buffer, durations);
        for (std::size_t i = 0; i < N; i++) {
            buffer[i].clear();
        }
        durations.clear();
    }


    /**
     * Get the a target for the given input index.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @param index the input index
     * @return a chain target
     */
    inline SmootherChainTarget *getTarget(std::size_t index)
    {
        Q_ASSERT(index >= 0 && index < N);
        return &targets[index];
    }

    /**
     * Process the data for a given bin.
     * 
     * @param start     the start time of the bin
     * @param end       the end time of the bin
     * @param values    the vectors of values in the bin
     * @param durations the durations of the values added to the bin
     */
    virtual void processBin(double start,
                            double end,
                            const QVector<double> values[N],
                            const QVector<double> &durations) = 0;
};


/**
 * Conventional averaging for segment controlled bins.  This takes as input
 * a stream of values and their coverage factors and produces the same
 * as output (averaged to intervals determined by the segments) as well as a 
 * statistics variable for the averaged time.
 */
class CPD3SMOOTHING_EXPORT SmootherChainSegmentControlledConventional
        : public SmootherChainBinnedConventional<SmootherChainInputSegmentedBinning> {
public:
    /**
     * Create a conventional averager with the given parameters.
     * 
     * @param coverRequired the minimum coverage required or NULL, this takes ownership
     * @param gap           the maximum time to allow before breaking on a gap or NULL for never
     * @param discardIntersecting if set then discard all segment intersecing values
     * @param outputAverage the output target for the average or NULL
     * @param outputCover   the output target for the cover factor or NULL
     * @param outputStats   the output target for the statistics variable or NULL
     */
    SmootherChainSegmentControlledConventional(Data::DynamicDouble *coverRequired,
                                               Data::DynamicTimeInterval *gap,
                                               bool discardIntersecting,
                                               SmootherChainTarget *outputAverage,
                                               SmootherChainTarget *outputCover = NULL,
                                               SmootherChainTargetGeneral *outputStats = NULL);

    /**
     * Restore the averager from the serialized state in the stream.
     * 
     * @param stream        the stream to restore from
     * @param outputAverage the output target for the average or NULL
     * @param outputCover   the output target for the cover factor or NULL
     * @param outputStats   the output target for the statistics variable or NULL
     */
    SmootherChainSegmentControlledConventional(QDataStream &stream,
                                               SmootherChainTarget *outputAverage,
                                               SmootherChainTarget *outputCover = NULL,
                                               SmootherChainTargetGeneral *outputStats = NULL);
};

/**
 * Vector statistics generating for time bins.  This takes as input the streams
 * of unaveraged vector magnitude values, their coverage factors, and the
 * averaged vector magnitude (calculated with the normal component breakdown
 * then average routine).  It produces as output the un-vectored magnitude
 * average and coverage as well statistics about the magnitude (including a 
 * "stability factor" that is the ratio of the un-vectored average to the 
 * vectored one).
 */
class CPD3SMOOTHING_EXPORT SmootherChainSegmentControlledVectorStatistics
        : public SmootherChainBinnedVectorStatistics<SmootherChainInputSegmentedBinning> {
public:
    /**
     * Create a conventional averager with the given parameters.
     * 
     * @param coverRequired the minimum coverage required or NULL, this takes ownership
     * @param gap           the maximum time to allow before breaking on a gap or NULL for never
     * @param discardIntersecting if set then discard all segment intersecing values
     * @param outputAverage the output target for the average or NULL
     * @param outputCover   the output target for the cover factor or NULL
     * @param outputStats   the output target for the statistics variable or NULL
     */
    SmootherChainSegmentControlledVectorStatistics(Data::DynamicDouble *coverRequired,
                                                   Data::DynamicTimeInterval *gap,
                                                   bool discardIntersecting,
                                                   SmootherChainTarget *outputAverage,
                                                   SmootherChainTarget *outputCover = NULL,
                                                   SmootherChainTargetGeneral *outputStats = NULL);

    /**
     * Restore the averager from the serialized state in the stream.
     * 
     * @param stream        the stream to restore from
     * @param outputAverage the output target for the average or NULL
     * @param outputCover   the output target for the cover factor or NULL
     * @param outputStats   the output target for the statistics variable or NULL
     */
    SmootherChainSegmentControlledVectorStatistics(QDataStream &stream,
                                                   SmootherChainTarget *outputAverage,
                                                   SmootherChainTarget *outputCover = NULL,
                                                   SmootherChainTargetGeneral *outputStats = NULL);
};


/**
 * Start and end value selection for segment controlled bins.  This takes as 
 * input a stream of values at the start and end of intervals (e.x. the normal 
 * values and those with the "end" flavor).  It produces a value stream for the
 * start and end as well as a coverage figure.  This is often fed into a
 * combining chain element (e.x. SmootherChainBeersLawAbsorption).
 */
class CPD3SMOOTHING_EXPORT SmootherChainSegmentControlledDifference
        : public SmootherChainBinnedDifference<SmootherChainInputSegmentedBinning> {
public:
    /**
     * Create a difference averager with the given parameters.
     * 
     * @param gap           the maximum time to allow before breaking on a gap or NULL for never
     * @param discardIntersecting if set then discard all segment intersecing values
     * @param outputStart   the output target for the value at the start or NULL
     * @param outputEnd     the output target for the value at the end or NULL
     * @param outputCover   the output target for the coverage or NULL
     */
    SmootherChainSegmentControlledDifference(Data::DynamicTimeInterval *gap,
                                             bool discardIntersecting,
                                             SmootherChainTarget *outputStart,
                                             SmootherChainTarget *outputEnd = NULL,
                                             SmootherChainTarget *outputCover = NULL);

    /**
     * Restore the averager from the serialized state in the stream.
     * 
     * @param stream        the stream to restore from
     * @param outputStart   the output target for the value at the start or NULL
     * @param outputEnd     the output target for the value at the end or NULL
     * @param outputCover   the output target for the coverage or NULL
     */
    SmootherChainSegmentControlledDifference(QDataStream &stream,
                                             SmootherChainTarget *outputStart,
                                             SmootherChainTarget *outputEnd = NULL,
                                             SmootherChainTarget *outputCover = NULL);
};

/**
 * A segment controlled binner for flags handling.  This simply combined all 
 * flags in the bin together.
 */
class CPD3SMOOTHING_EXPORT SmootherChainSegmentControlledFlags : public SmootherChainBinnedFlags<
        SegmentControlledBinner> {
public:
    /**
     * Create a new fixed time binning flags averager.
     * 
     * @param gap               the maximum time to allow before breaking on a gap or NULL for never
     * @param discardIntersecting if set then discard all segment intersecing values
     * @param outputFlags       the output target for the averaged flags
     * @param outputCover       the output target for the coverage or NULL
     */
    SmootherChainSegmentControlledFlags(Data::DynamicTimeInterval *gap,
                                        bool discardIntersecting,
                                        SmootherChainTargetFlags *outputFlags,
                                        SmootherChainTarget *outputCover = NULL);

    /**
     * Restore a flags averager from the given state.
     * 
     * @param stream            the stream to restore from
     * @param outputFlags       the output target for the averaged flags
     * @param outputCover       the output target for the coverage or NULL
     */
    SmootherChainSegmentControlledFlags(QDataStream &stream,
                                        SmootherChainTargetFlags *outputFlags,
                                        SmootherChainTarget *outputCover = NULL);
};

/**
 * A smoother chain core for segment controlled binned data.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreSegmentControlled : public SmootherChainCoreBinned {
    Data::DynamicDouble *requiredCover;
    Data::DynamicTimeInterval *gap;
    bool discardIntersecting;

    Data::DynamicDouble *childCover() const;

    Data::DynamicTimeInterval *childGap() const;

public:
    /**
     * Create a new core.  This takes ownership of the gap and coverage.
     * 
     * @param setGap        the maximum gap to allow or NULL for none
     * @param intersectingDiscard if set the intersecting data is discarded instead of weighted into segments
     * @param setRequiredCover the minimum cover to require or NULL for none
     */
    SmootherChainCoreSegmentControlled(Data::DynamicTimeInterval *setGap,
                                       bool intersectingDiscard = false,
                                       Data::DynamicDouble *setRequiredCover = NULL);

    virtual ~SmootherChainCoreSegmentControlled();

    virtual Data::Variant::Root getProcessingMetadata(double time) const;

    virtual QSet<double> getProcessingMetadataBreaks() const;

    virtual bool differencePreserving() const;

    virtual void createConventional(SmootherChainCoreEngineInterface *engine,
                                    SmootherChainTarget *&inputValue,
                                    SmootherChainTarget *&inputCover,
                                    SmootherChainTarget *outputAverage,
                                    SmootherChainTarget *outputCover = NULL,
                                    SmootherChainTargetGeneral *outputStats = NULL);

    virtual void deserializeConventional(QDataStream &stream,
                                         SmootherChainCoreEngineInterface *engine,
                                         SmootherChainTarget *&inputValue,
                                         SmootherChainTarget *&inputCover,
                                         SmootherChainTarget *outputAverage,
                                         SmootherChainTarget *outputCover = NULL,
                                         SmootherChainTargetGeneral *outputStats = NULL);

    virtual void createVectorStatistics(SmootherChainCoreEngineInterface *engine,
                                        SmootherChainTarget *&inputMagnitude,
                                        SmootherChainTarget *&inputCover,
                                        SmootherChainTarget *&inputVectoredMagnitude,
                                        SmootherChainTarget *outputAverage = NULL,
                                        SmootherChainTarget *outputCover = NULL,
                                        SmootherChainTargetGeneral *outputStats = NULL);

    virtual void deserializeVectorStatistics(QDataStream &stream,
                                             SmootherChainCoreEngineInterface *engine,
                                             SmootherChainTarget *&inputMagnitude,
                                             SmootherChainTarget *&inputCover,
                                             SmootherChainTarget *&inputVectoredMagnitude,
                                             SmootherChainTarget *outputAverage = NULL,
                                             SmootherChainTarget *outputCover = NULL,
                                             SmootherChainTargetGeneral *outputStats = NULL);

    virtual void createDifference(SmootherChainCoreEngineInterface *engine,
                                  bool alwaysBreak,
                                  SmootherChainTarget *&inputStart,
                                  SmootherChainTarget *&inputEnd,
                                  SmootherChainTarget *outputStart,
                                  SmootherChainTarget *outputEnd,
                                  SmootherChainTarget *outputCover = NULL);

    virtual void deserializeDifference(QDataStream &stream,
                                       SmootherChainCoreEngineInterface *engine,
                                       bool alwaysBreak,
                                       SmootherChainTarget *&inputStart,
                                       SmootherChainTarget *&inputEnd,
                                       SmootherChainTarget *outputStart,
                                       SmootherChainTarget *outputEnd,
                                       SmootherChainTarget *outputCover = NULL);

    virtual void createFlags(SmootherChainCoreEngineInterface *engine,
                             SmootherChainTargetFlags *&inputFlags,
                             SmootherChainTarget *&inputCover,
                             SmootherChainTargetFlags *outputFlags,
                             SmootherChainTarget *outputCover = NULL);

    virtual void deserializeFlags(QDataStream &stream,
                                  SmootherChainCoreEngineInterface *engine,
                                  SmootherChainTargetFlags *&inputFlags,
                                  SmootherChainTarget *&inputCover,
                                  SmootherChainTargetFlags *outputFlags,
                                  SmootherChainTarget *outputCover = NULL);
};

/**
 * A smoother chain core for segment controlled binned data that always 
 * preserved smoothability of inputs.  This means it breaks on any gaps 
 * detected but it also preserves the smoothing state of differential 
 * components.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreSegmentControlledPreserving
        : public SmootherChainCoreSegmentControlled {
public:
    /**
     * Create a new core.  This takes ownership of the interval and gap.
     * 
     * @param intersectingDiscard if set the intersecting data is discarded instead of weighted into segments
     * @param setRequiredCover the minimum cover to require or NULL for none
     */
    SmootherChainCoreSegmentControlledPreserving(bool intersectingDiscard = false,
                                                 Data::DynamicDouble *setRequiredCover = NULL);

    virtual ~SmootherChainCoreSegmentControlledPreserving();

    virtual bool differencePreserving() const;
};

/**
 * The implementation for the engine that controls a segment based smoother.
 */
class CPD3SMOOTHING_EXPORT SmoothingEngineSegmentControlled : public SmoothingEngine {
    Data::DynamicSequenceSelection *segmentSelection;
    Data::DynamicSequenceSelection *outputSelection;
    Data::ValueSegment::Stream segmenter;
    Data::SequenceName::Set registeredInputs;

    Data::DynamicTimeInterval *gap;
    Data::DynamicDouble *requiredCover;
    bool discardIntersecting;
    bool gapPreserving;

    std::deque<Data::ValueSegment> segments;
    bool haveOpenSegment;

    Data::SinkMultiplexer::Sink *segmentIngress;

    class AuxiliaryChainController;

    class EngineInterface;

    friend class EngineInterface;

    friend class AuxiliaryChainController;

    class EngineInterface
            : public SmoothingEngine::EngineInterface,
              public virtual SmootherChainCoreEngineInterfaceSegmenter {
        SmoothingEngineSegmentControlled *parent;
        AuxiliaryChainController *chainController;
    public:
        EngineInterface(SmoothingEngineSegmentControlled *engine);

        virtual ~EngineInterface();

        virtual void addSegmentController(SegmentController *controller);

        virtual SmootherChainCoreEngineAuxiliaryInterface *getAuxiliaryInterface();
    };

    class AuxiliaryChainController : public SmoothingEngine::AuxiliaryChainController {
        std::shared_ptr<
                std::unordered_map<AuxiliaryChainController *, std::vector<SegmentController *>>>
                chainSegmentControllers;
    public:
        AuxiliaryChainController(SmoothingEngineSegmentControlled *engine);

        virtual ~AuxiliaryChainController();
    };

    std::shared_ptr<
            std::unordered_map<AuxiliaryChainController *, std::vector<SegmentController *>>>
            chainSegmentControllers;

    void dispatchEnd(double end);

    void dispatchStart(double start);

    void dispatchStartEnd(double start, double end);

    QSet<Data::SequenceName> buildSegmentingUnits(double time);

    void processCompletedSegments(Data::ValueSegment::Transfer &&process);

    void processSegmentsUpdated();

public:
    SmoothingEngineSegmentControlled(Data::DynamicSequenceSelection *segmentInput,
                                     Data::DynamicSequenceSelection *output,
                                     bool intersectingDiscard = false,
                                     Data::DynamicTimeInterval *setGap = NULL,
                                     Data::DynamicDouble *setRequiredCover = NULL,
                                     bool setGapPreserving = false);

    virtual ~SmoothingEngineSegmentControlled();

    void registerExpectedInputs(const QSet<Data::SequenceName> &inputs);

    void deserialize(QDataStream &stream) override;

    void serialize(QDataStream &stream) override;


    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

protected:
    void startProcessing(IncomingBuffer::iterator incomingBegin,
                         IncomingBuffer::iterator incomingEnd,
                         double &advanceTime,
                         double &holdbackTime) override;

    void predictMetadataBounds(double *start, double *end) override;

    void auxiliaryAdvance(double advanceTime) override;

    void auxiliaryEndBefore() override;

    void auxiliaryEndAfter() override;

    std::unique_ptr<SmoothingEngine::EngineInterface> createEngineInterface() override;
};

}
}

#endif
