/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "smoothing/fixedtime.hxx"
#include "core/environment.hxx"


Q_LOGGING_CATEGORY(log_smoothing_fixedtime, "cpd3.smoothing.fixedtime", QtWarningMsg)

using namespace CPD3::Data;
using namespace CPD3::Algorithms;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/fixedtime.hxx
 * Fixed time binning smoothers.
 */


SmootherChainFixedTimeConventional::SmootherChainFixedTimeConventional(Data::DynamicDouble *coverRequired,
                                                                       Data::DynamicTimeInterval *interval,
                                                                       Data::DynamicTimeInterval *gap,
                                                                       SmootherChainTarget *outputAverage,
                                                                       SmootherChainTarget *outputCover,
                                                                       SmootherChainTargetGeneral *outputStats)
        : SmootherChainBinnedConventional<CPD3::Smoothing::SmootherChainInputFixedTimeBinning>(
        coverRequired, outputAverage, outputCover, outputStats)
{
    setInterval(interval);
    setGap(gap);
}

SmootherChainFixedTimeConventional::SmootherChainFixedTimeConventional(QDataStream &stream,
                                                                       SmootherChainTarget *outputAverage,
                                                                       SmootherChainTarget *outputCover,
                                                                       SmootherChainTargetGeneral *outputStats)
        : SmootherChainBinnedConventional<CPD3::Smoothing::SmootherChainInputFixedTimeBinning>(
        stream, outputAverage, outputCover, outputStats)
{ }


SmootherChainFixedTimeVectorStatistics::SmootherChainFixedTimeVectorStatistics(Data::DynamicDouble *coverRequired,
                                                                               Data::DynamicTimeInterval *interval,
                                                                               Data::DynamicTimeInterval *gap,
                                                                               SmootherChainTarget *outputAverage,
                                                                               SmootherChainTarget *outputCover,
                                                                               SmootherChainTargetGeneral *outputStats)
        : SmootherChainBinnedVectorStatistics<CPD3::Smoothing::SmootherChainInputFixedTimeBinning>(
        coverRequired, outputAverage, outputCover, outputStats)
{
    setInterval(interval);
    setGap(gap);
}

SmootherChainFixedTimeVectorStatistics::SmootherChainFixedTimeVectorStatistics(QDataStream &stream,
                                                                               SmootherChainTarget *outputAverage,
                                                                               SmootherChainTarget *outputCover,
                                                                               SmootherChainTargetGeneral *outputStats)
        : SmootherChainBinnedVectorStatistics<CPD3::Smoothing::SmootherChainInputFixedTimeBinning>(
        stream, outputAverage, outputCover, outputStats)
{ }

SmootherChainFixedTimeDifference::SmootherChainFixedTimeDifference(Data::DynamicTimeInterval *interval,
                                                                   Data::DynamicTimeInterval *gap,
                                                                   SmootherChainTarget *outputStart,
                                                                   SmootherChainTarget *outputEnd,
                                                                   SmootherChainTarget *outputCover)
        : SmootherChainBinnedDifference<CPD3::Smoothing::SmootherChainInputFixedTimeBinning>(
        outputStart, outputEnd, outputCover)
{
    setInterval(interval);
    setGap(gap);
}

SmootherChainFixedTimeDifference::SmootherChainFixedTimeDifference(QDataStream &stream,
                                                                   SmootherChainTarget *outputStart,
                                                                   SmootherChainTarget *outputEnd,
                                                                   SmootherChainTarget *outputCover)
        : SmootherChainBinnedDifference<CPD3::Smoothing::SmootherChainInputFixedTimeBinning>(stream,
                                                                                             outputStart,
                                                                                             outputEnd,
                                                                                             outputCover)
{ }

SmootherChainFixedTimeFlags::SmootherChainFixedTimeFlags(DynamicTimeInterval *interval,
                                                         DynamicTimeInterval *gap,
                                                         SmootherChainTargetFlags *outputFlags,
                                                         SmootherChainTarget *outputCover)
        : SmootherChainBinnedFlags<CPD3::Smoothing::FixedTimeBinner>(outputFlags, outputCover)
{
    setInterval(interval);
    setGap(gap);
}

SmootherChainFixedTimeFlags::SmootherChainFixedTimeFlags(QDataStream &stream,
                                                         SmootherChainTargetFlags *outputFlags,
                                                         SmootherChainTarget *outputCover)
        : SmootherChainBinnedFlags<CPD3::Smoothing::FixedTimeBinner>(stream, outputFlags,
                                                                     outputCover)
{ }

SmootherChainCoreFixedTime::SmootherChainCoreFixedTime(Data::DynamicTimeInterval *setInterval,
                                                       Data::DynamicTimeInterval *setGap,
                                                       Data::DynamicDouble *setRequiredCover,
                                                       bool setEnableStatistics)
        : SmootherChainCoreBinned(setEnableStatistics),
          requiredCover(setRequiredCover),
          interval(setInterval),
          gap(setGap)
{ }

SmootherChainCoreFixedTime::~SmootherChainCoreFixedTime()
{
    if (requiredCover != NULL)
        delete requiredCover;
    if (interval != NULL)
        delete interval;
    if (gap != NULL)
        delete gap;
}

Variant::Root SmootherChainCoreFixedTime::getProcessingMetadata(double time) const
{
    Variant::Root meta;
    meta.write().hash("Type").setString("Traditional");
    if (requiredCover != NULL) {
        meta.write()
            .hash("Parameters")
            .hash("RequiredCoverage")
            .setDouble(requiredCover->get(time));
    }
    if (interval != NULL) {
        meta.write().hash("Parameters").hash("Interval").setString(interval->describe(time));
    }
    if (gap != NULL) {
        meta.write().hash("Parameters").hash("Gap").setString(gap->describe(time));
    }
    return meta;
}

QSet<double> SmootherChainCoreFixedTime::getProcessingMetadataBreaks() const
{
    QSet<double> result;
    if (requiredCover != NULL)
        result |= requiredCover->getChangedPoints();
    if (interval != NULL)
        result |= interval->getChangedPoints();
    if (gap != NULL)
        result |= gap->getChangedPoints();
    return result;
}

bool SmootherChainCoreFixedTime::differencePreserving() const
{ return false; }

DynamicDouble *SmootherChainCoreFixedTime::childCover() const
{ return requiredCover != NULL ? requiredCover->clone() : NULL; }

DynamicTimeInterval *SmootherChainCoreFixedTime::childInterval() const
{ return interval != NULL ? interval->clone() : NULL; }

DynamicTimeInterval *SmootherChainCoreFixedTime::childGap() const
{ return gap != NULL ? gap->clone() : NULL; }

void SmootherChainCoreFixedTime::createConventional(SmootherChainCoreEngineInterface *engine,
                                                    SmootherChainTarget *&inputValue,
                                                    SmootherChainTarget *&inputCover,
                                                    SmootherChainTarget *outputAverage,
                                                    SmootherChainTarget *outputCover,
                                                    SmootherChainTargetGeneral *outputStats)
{
    SmootherChainFixedTimeConventional *add =
            new SmootherChainFixedTimeConventional(childCover(), childInterval(), childGap(),
                                                   outputAverage, outputCover, outputStats);
    engine->addChainNode(add);
    inputValue = add->getTargetValue();
    inputCover = add->getTargetCover();
}

void SmootherChainCoreFixedTime::deserializeConventional(QDataStream &stream,
                                                         SmootherChainCoreEngineInterface *engine,
                                                         SmootherChainTarget *&inputValue,
                                                         SmootherChainTarget *&inputCover,
                                                         SmootherChainTarget *outputAverage,
                                                         SmootherChainTarget *outputCover,
                                                         SmootherChainTargetGeneral *outputStats)
{
    SmootherChainFixedTimeConventional *add =
            new SmootherChainFixedTimeConventional(stream, outputAverage, outputCover, outputStats);
    engine->addChainNode(add);
    inputValue = add->getTargetValue();
    inputCover = add->getTargetCover();
}

void SmootherChainCoreFixedTime::createVectorStatistics(SmootherChainCoreEngineInterface *engine,
                                                        SmootherChainTarget *&inputMagnitude,
                                                        SmootherChainTarget *&inputCover,
                                                        SmootherChainTarget *&inputVectoredMagnitude,
                                                        SmootherChainTarget *outputAverage,
                                                        SmootherChainTarget *outputCover,
                                                        SmootherChainTargetGeneral *outputStats)
{
    SmootherChainFixedTimeVectorStatistics *add =
            new SmootherChainFixedTimeVectorStatistics(childCover(), childInterval(), childGap(),
                                                       outputAverage, outputCover, outputStats);
    engine->addChainNode(add);
    inputMagnitude = add->getTargetValue();
    inputCover = add->getTargetCover();
    inputVectoredMagnitude = add->getTargetVectoredMagnitude();
}

void SmootherChainCoreFixedTime::deserializeVectorStatistics(QDataStream &stream,
                                                             SmootherChainCoreEngineInterface *engine,
                                                             SmootherChainTarget *&inputMagnitude,
                                                             SmootherChainTarget *&inputCover,
                                                             SmootherChainTarget *&inputVectoredMagnitude,
                                                             SmootherChainTarget *outputAverage,
                                                             SmootherChainTarget *outputCover,
                                                             SmootherChainTargetGeneral *outputStats)
{
    SmootherChainFixedTimeVectorStatistics *add =
            new SmootherChainFixedTimeVectorStatistics(stream, outputAverage, outputCover,
                                                       outputStats);
    engine->addChainNode(add);
    inputMagnitude = add->getTargetValue();
    inputCover = add->getTargetCover();
    inputVectoredMagnitude = add->getTargetVectoredMagnitude();
}

void SmootherChainCoreFixedTime::createDifference(SmootherChainCoreEngineInterface *engine,
                                                  bool alwaysBreak,
                                                  SmootherChainTarget *&inputStart,
                                                  SmootherChainTarget *&inputEnd,
                                                  SmootherChainTarget *outputStart,
                                                  SmootherChainTarget *outputEnd,
                                                  SmootherChainTarget *outputCover)
{
    SmootherChainFixedTimeDifference *add = new SmootherChainFixedTimeDifference(childInterval(),
                                                                                 (alwaysBreak
                                                                                  ? new DynamicTimeInterval::Constant(
                                                                                         Time::Millisecond,
                                                                                         1)
                                                                                  : childGap()),
                                                                                 outputStart,
                                                                                 outputEnd,
                                                                                 outputCover);
    engine->addChainNode(add);
    inputStart = add->getTargetStart();
    inputEnd = add->getTargetEnd();
}

void SmootherChainCoreFixedTime::deserializeDifference(QDataStream &stream,
                                                       SmootherChainCoreEngineInterface *engine,
                                                       bool alwaysBreak,
                                                       SmootherChainTarget *&inputStart,
                                                       SmootherChainTarget *&inputEnd,
                                                       SmootherChainTarget *outputStart,
                                                       SmootherChainTarget *outputEnd,
                                                       SmootherChainTarget *outputCover)
{
    Q_UNUSED(alwaysBreak);
    SmootherChainFixedTimeDifference *add =
            new SmootherChainFixedTimeDifference(stream, outputStart, outputEnd, outputCover);
    engine->addChainNode(add);
    inputStart = add->getTargetStart();
    inputEnd = add->getTargetEnd();
}

void SmootherChainCoreFixedTime::createFlags(SmootherChainCoreEngineInterface *engine,
                                             SmootherChainTargetFlags *&inputFlags,
                                             SmootherChainTarget *&inputCover,
                                             SmootherChainTargetFlags *outputFlags,
                                             SmootherChainTarget *outputCover)
{
    SmootherChainFixedTimeFlags *add =
            new SmootherChainFixedTimeFlags(childInterval(), childGap(), outputFlags, outputCover);
    engine->addChainNode(add);
    inputFlags = add->getTargetFlags();
    inputCover = add->getTargetCover();
}

void SmootherChainCoreFixedTime::deserializeFlags(QDataStream &stream,
                                                  SmootherChainCoreEngineInterface *engine,
                                                  SmootherChainTargetFlags *&inputFlags,
                                                  SmootherChainTarget *&inputCover,
                                                  SmootherChainTargetFlags *outputFlags,
                                                  SmootherChainTarget *outputCover)
{
    SmootherChainFixedTimeFlags
            *add = new SmootherChainFixedTimeFlags(stream, outputFlags, outputCover);
    engine->addChainNode(add);
    inputFlags = add->getTargetFlags();
    inputCover = add->getTargetCover();
}

SmootherChainCoreFixedTimePreserving::SmootherChainCoreFixedTimePreserving(Data::DynamicTimeInterval *setInterval,
                                                                           Data::DynamicDouble *setRequiredCover,
                                                                           bool setEnableStatistics)
        : SmootherChainCoreFixedTime(setInterval,
                                     new DynamicTimeInterval::Constant(Time::Millisecond, 1),
                                     setRequiredCover, setEnableStatistics)
{ }

SmootherChainCoreFixedTimePreserving::~SmootherChainCoreFixedTimePreserving()
{ }

bool SmootherChainCoreFixedTimePreserving::differencePreserving() const
{ return true; }


SmoothingEngineFixedTime::SmoothingEngineFixedTime(Data::DynamicTimeInterval *setInterval,
                                                   Data::DynamicTimeInterval *setGap,
                                                   Data::DynamicDouble *setRequiredCover,
                                                   bool setGapPreserving,
                                                   bool setEnableStatistics) : SmoothingEngine(
        (setGapPreserving ? new SmootherChainCoreFixedTimePreserving(setInterval, setRequiredCover,
                                                                     setEnableStatistics)
                          : new SmootherChainCoreFixedTime(setInterval, setGap, setRequiredCover,
                                                           setEnableStatistics))),
                                                                               interval(
                                                                                       setInterval),
                                                                               gap(setGap),
                                                                               requiredCover(
                                                                                       setRequiredCover),
                                                                               gapPreserving(
                                                                                       setGapPreserving),
                                                                               enableStatistics(
                                                                                       setEnableStatistics),
                                                                               registeredInputs()
{ }

SmoothingEngineFixedTime::~SmoothingEngineFixedTime()
{ }

void SmoothingEngineFixedTime::registerExpectedInputs(const QSet<SequenceName> &inputs)
{ Util::merge(inputs, registeredInputs); }

void SmoothingEngineFixedTime::deserialize(QDataStream &stream)
{
    stream >>
            interval >>
            gap >>
            requiredCover >>
            gapPreserving >>
            registeredInputs >>
            enableStatistics;
    changeCore(gapPreserving ? new SmootherChainCoreFixedTimePreserving(interval, requiredCover,
                                                                        enableStatistics)
                             : new SmootherChainCoreFixedTime(interval, gap, requiredCover,
                                                              enableStatistics));
    SmoothingEngine::deserialize(stream);
}

void SmoothingEngineFixedTime::serialize(QDataStream &stream)
{
    pauseProcessing();
    stream <<
            interval <<
            gap <<
            requiredCover <<
            gapPreserving <<
            registeredInputs <<
            enableStatistics;
    serializeInternal(stream);
    resumeProcessing();
}

SequenceName::Set SmoothingEngineFixedTime::requestedInputs()
{ return SmootherChainCoreBinned::requestedInputs(registeredInputs); }

SequenceName::Set SmoothingEngineFixedTime::predictedOutputs()
{ return SmootherChainCoreBinned::predictedOutputs(registeredInputs); }

void SmoothingEngineFixedTime::startProcessing(IncomingBuffer::iterator incomingBegin,
                                               IncomingBuffer::iterator incomingEnd,
                                               double &advanceTime,
                                               double &holdbackTime)
{
    SmoothingEngine::startProcessing(incomingBegin, incomingEnd, advanceTime, holdbackTime);
    FixedTimeBinner<const double *>::applyRounding(interval, &holdbackTime, NULL);
}

void SmoothingEngineFixedTime::predictMetadataBounds(double *start, double *end)
{
    FixedTimeBinner<const double *>::applyRounding(interval, start, end);
}

}
}
