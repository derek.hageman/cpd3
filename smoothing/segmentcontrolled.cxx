/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "smoothing/segmentcontrolled.hxx"
#include "core/environment.hxx"

using namespace CPD3::Data;
using namespace CPD3::Algorithms;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/segmentcontrolled.hxx
 * External segment controlled binning averages.
 */

SegmentController::~SegmentController()
{ }

SmootherChainCoreEngineInterfaceSegmenter::~SmootherChainCoreEngineInterfaceSegmenter()
{ }


SmootherChainSegmentControlledConventional::SmootherChainSegmentControlledConventional(Data::DynamicDouble *coverRequired,
                                                                                       Data::DynamicTimeInterval *gap,
                                                                                       bool discardIntersecting,
                                                                                       SmootherChainTarget *outputAverage,
                                                                                       SmootherChainTarget *outputCover,
                                                                                       SmootherChainTargetGeneral *outputStats)
        : SmootherChainBinnedConventional<CPD3::Smoothing::SmootherChainInputSegmentedBinning>(
        coverRequired, outputAverage, outputCover, outputStats)
{
    setGap(gap);
    setDiscardIntersecting(discardIntersecting);
}

SmootherChainSegmentControlledConventional::SmootherChainSegmentControlledConventional(QDataStream &stream,
                                                                                       SmootherChainTarget *outputAverage,
                                                                                       SmootherChainTarget *outputCover,
                                                                                       SmootherChainTargetGeneral *outputStats)
        : SmootherChainBinnedConventional<CPD3::Smoothing::SmootherChainInputSegmentedBinning>(
        stream, outputAverage, outputCover, outputStats)
{ }

SmootherChainSegmentControlledVectorStatistics::SmootherChainSegmentControlledVectorStatistics(Data::DynamicDouble *coverRequired,
                                                                                               Data::DynamicTimeInterval *gap,
                                                                                               bool discardIntersecting,
                                                                                               SmootherChainTarget *outputAverage,
                                                                                               SmootherChainTarget *outputCover,
                                                                                               SmootherChainTargetGeneral *outputStats)
        : SmootherChainBinnedVectorStatistics<CPD3::Smoothing::SmootherChainInputSegmentedBinning>(
        coverRequired, outputAverage, outputCover, outputStats)
{
    setGap(gap);
    setDiscardIntersecting(discardIntersecting);
}

SmootherChainSegmentControlledVectorStatistics::SmootherChainSegmentControlledVectorStatistics(
        QDataStream &stream,
        SmootherChainTarget *outputAverage,
        SmootherChainTarget *outputCover,
        SmootherChainTargetGeneral *outputStats) : SmootherChainBinnedVectorStatistics<
        CPD3::Smoothing::SmootherChainInputSegmentedBinning>(stream, outputAverage, outputCover,
                                                             outputStats)
{ }

SmootherChainSegmentControlledDifference::SmootherChainSegmentControlledDifference(Data::DynamicTimeInterval *gap,
                                                                                   bool discardIntersecting,
                                                                                   SmootherChainTarget *outputStart,
                                                                                   SmootherChainTarget *outputEnd,
                                                                                   SmootherChainTarget *outputCover)
        : SmootherChainBinnedDifference<CPD3::Smoothing::SmootherChainInputSegmentedBinning>(
        outputStart, outputEnd, outputCover)
{
    setGap(gap);
    setDiscardIntersecting(discardIntersecting);
}

SmootherChainSegmentControlledDifference::SmootherChainSegmentControlledDifference(QDataStream &stream,
                                                                                   SmootherChainTarget *outputStart,
                                                                                   SmootherChainTarget *outputEnd,
                                                                                   SmootherChainTarget *outputCover)
        : SmootherChainBinnedDifference<CPD3::Smoothing::SmootherChainInputSegmentedBinning>(stream,
                                                                                             outputStart,
                                                                                             outputEnd,
                                                                                             outputCover)
{ }

SmootherChainSegmentControlledFlags::SmootherChainSegmentControlledFlags(Data::DynamicTimeInterval *gap,
                                                                         bool discardIntersecting,
                                                                         SmootherChainTargetFlags *outputFlags,
                                                                         SmootherChainTarget *outputCover)
        : SmootherChainBinnedFlags<CPD3::Smoothing::SegmentControlledBinner>(outputFlags,
                                                                             outputCover)
{
    setGap(gap);
    setDiscardIntersecting(discardIntersecting);
}

SmootherChainSegmentControlledFlags::SmootherChainSegmentControlledFlags(QDataStream &stream,
                                                                         SmootherChainTargetFlags *outputFlags,
                                                                         SmootherChainTarget *outputCover)
        : SmootherChainBinnedFlags<CPD3::Smoothing::SegmentControlledBinner>(stream, outputFlags,
                                                                             outputCover)
{ }


SmootherChainCoreSegmentControlled::SmootherChainCoreSegmentControlled(Data::DynamicTimeInterval *setGap,
                                                                       bool intersectingDiscard,
                                                                       Data::DynamicDouble *setRequiredCover)
        : requiredCover(setRequiredCover), gap(setGap), discardIntersecting(intersectingDiscard)
{ }

SmootherChainCoreSegmentControlled::~SmootherChainCoreSegmentControlled()
{
    if (requiredCover != NULL)
        delete requiredCover;
    if (gap != NULL)
        delete gap;
}

Variant::Root SmootherChainCoreSegmentControlled::getProcessingMetadata(double time) const
{
    Variant::Root meta;
    meta.write().hash("Type").setString("Segmented");
    if (requiredCover != NULL) {
        meta.write()
            .hash("Parameters")
            .hash("RequiredCoverage")
            .setDouble(requiredCover->get(time));
    }
    if (gap != NULL) {
        meta.write().hash("Parameters").hash("Gap").setString(gap->describe(time));
    }
    return meta;
}

QSet<double> SmootherChainCoreSegmentControlled::getProcessingMetadataBreaks() const
{
    QSet<double> result;
    if (requiredCover != NULL)
        result |= requiredCover->getChangedPoints();
    if (gap != NULL)
        result |= gap->getChangedPoints();
    return result;
}

bool SmootherChainCoreSegmentControlled::differencePreserving() const
{ return false; }

DynamicDouble *SmootherChainCoreSegmentControlled::childCover() const
{ return requiredCover != NULL ? requiredCover->clone() : NULL; }

DynamicTimeInterval *SmootherChainCoreSegmentControlled::childGap() const
{ return gap != NULL ? gap->clone() : NULL; }

void SmootherChainCoreSegmentControlled::createConventional(SmootherChainCoreEngineInterface *engine,
                                                            SmootherChainTarget *&inputValue,
                                                            SmootherChainTarget *&inputCover,
                                                            SmootherChainTarget *outputAverage,
                                                            SmootherChainTarget *outputCover,
                                                            SmootherChainTargetGeneral *outputStats)
{
    SmootherChainSegmentControlledConventional *add =
            new SmootherChainSegmentControlledConventional(childCover(), childGap(),
                                                           discardIntersecting, outputAverage,
                                                           outputCover, outputStats);
    engine->addChainNode(add);
    static_cast<SmootherChainCoreEngineInterfaceSegmenter *>
    (engine->getAuxiliaryInterface())->addSegmentController(add->getController());
    inputValue = add->getTargetValue();
    inputCover = add->getTargetCover();
}

void SmootherChainCoreSegmentControlled::deserializeConventional(QDataStream &stream,
                                                                 SmootherChainCoreEngineInterface *engine,
                                                                 SmootherChainTarget *&inputValue,
                                                                 SmootherChainTarget *&inputCover,
                                                                 SmootherChainTarget *outputAverage,
                                                                 SmootherChainTarget *outputCover,
                                                                 SmootherChainTargetGeneral *outputStats)
{
    SmootherChainSegmentControlledConventional *add =
            new SmootherChainSegmentControlledConventional(stream, outputAverage, outputCover,
                                                           outputStats);
    engine->addChainNode(add);
    static_cast<SmootherChainCoreEngineInterfaceSegmenter *>
    (engine->getAuxiliaryInterface())->addSegmentController(add->getController());
    inputValue = add->getTargetValue();
    inputCover = add->getTargetCover();
}

void SmootherChainCoreSegmentControlled::createVectorStatistics(SmootherChainCoreEngineInterface *engine,
                                                                SmootherChainTarget *&inputMagnitude,
                                                                SmootherChainTarget *&inputCover,
                                                                SmootherChainTarget *&inputVectoredMagnitude,
                                                                SmootherChainTarget *outputAverage,
                                                                SmootherChainTarget *outputCover,
                                                                SmootherChainTargetGeneral *outputStats)
{
    SmootherChainSegmentControlledVectorStatistics *add =
            new SmootherChainSegmentControlledVectorStatistics(childCover(), childGap(),
                                                               discardIntersecting, outputAverage,
                                                               outputCover, outputStats);
    engine->addChainNode(add);
    static_cast<SmootherChainCoreEngineInterfaceSegmenter *>
    (engine->getAuxiliaryInterface())->addSegmentController(add->getController());
    inputMagnitude = add->getTargetValue();
    inputCover = add->getTargetCover();
    inputVectoredMagnitude = add->getTargetVectoredMagnitude();
}

void SmootherChainCoreSegmentControlled::deserializeVectorStatistics(QDataStream &stream,
                                                                     SmootherChainCoreEngineInterface *engine,
                                                                     SmootherChainTarget *&inputMagnitude,
                                                                     SmootherChainTarget *&inputCover,
                                                                     SmootherChainTarget *&inputVectoredMagnitude,
                                                                     SmootherChainTarget *outputAverage,
                                                                     SmootherChainTarget *outputCover,
                                                                     SmootherChainTargetGeneral *outputStats)
{
    SmootherChainSegmentControlledVectorStatistics *add =
            new SmootherChainSegmentControlledVectorStatistics(stream, outputAverage, outputCover,
                                                               outputStats);
    engine->addChainNode(add);
    static_cast<SmootherChainCoreEngineInterfaceSegmenter *>
    (engine->getAuxiliaryInterface())->addSegmentController(add->getController());
    inputMagnitude = add->getTargetValue();
    inputCover = add->getTargetCover();
    inputVectoredMagnitude = add->getTargetVectoredMagnitude();
}

void SmootherChainCoreSegmentControlled::createDifference(SmootherChainCoreEngineInterface *engine,
                                                          bool alwaysBreak,
                                                          SmootherChainTarget *&inputStart,
                                                          SmootherChainTarget *&inputEnd,
                                                          SmootherChainTarget *outputStart,
                                                          SmootherChainTarget *outputEnd,
                                                          SmootherChainTarget *outputCover)
{
    SmootherChainSegmentControlledDifference *add = new SmootherChainSegmentControlledDifference(
            alwaysBreak ? new DynamicTimeInterval::Constant(Time::Millisecond, 1) : childGap(),
            discardIntersecting, outputStart, outputEnd, outputCover);
    engine->addChainNode(add);
    static_cast<SmootherChainCoreEngineInterfaceSegmenter *>
    (engine->getAuxiliaryInterface())->addSegmentController(add->getController());
    inputStart = add->getTargetStart();
    inputEnd = add->getTargetEnd();
}

void SmootherChainCoreSegmentControlled::deserializeDifference(QDataStream &stream,
                                                               SmootherChainCoreEngineInterface *engine,
                                                               bool alwaysBreak,
                                                               SmootherChainTarget *&inputStart,
                                                               SmootherChainTarget *&inputEnd,
                                                               SmootherChainTarget *outputStart,
                                                               SmootherChainTarget *outputEnd,
                                                               SmootherChainTarget *outputCover)
{
    Q_UNUSED(alwaysBreak);
    SmootherChainSegmentControlledDifference *add =
            new SmootherChainSegmentControlledDifference(stream, outputStart, outputEnd,
                                                         outputCover);
    engine->addChainNode(add);
    static_cast<SmootherChainCoreEngineInterfaceSegmenter *>
    (engine->getAuxiliaryInterface())->addSegmentController(add->getController());
    inputStart = add->getTargetStart();
    inputEnd = add->getTargetEnd();
}

void SmootherChainCoreSegmentControlled::createFlags(SmootherChainCoreEngineInterface *engine,
                                                     SmootherChainTargetFlags *&inputFlags,
                                                     SmootherChainTarget *&inputCover,
                                                     SmootherChainTargetFlags *outputFlags,
                                                     SmootherChainTarget *outputCover)
{
    SmootherChainSegmentControlledFlags *add =
            new SmootherChainSegmentControlledFlags(childGap(), discardIntersecting, outputFlags,
                                                    outputCover);
    engine->addChainNode(add);
    static_cast<SmootherChainCoreEngineInterfaceSegmenter *>
    (engine->getAuxiliaryInterface())->addSegmentController(add->getController());
    inputFlags = add->getTargetFlags();
    inputCover = add->getTargetCover();
}

void SmootherChainCoreSegmentControlled::deserializeFlags(QDataStream &stream,
                                                          SmootherChainCoreEngineInterface *engine,
                                                          SmootherChainTargetFlags *&inputFlags,
                                                          SmootherChainTarget *&inputCover,
                                                          SmootherChainTargetFlags *outputFlags,
                                                          SmootherChainTarget *outputCover)
{
    SmootherChainSegmentControlledFlags
            *add = new SmootherChainSegmentControlledFlags(stream, outputFlags, outputCover);
    engine->addChainNode(add);
    static_cast<SmootherChainCoreEngineInterfaceSegmenter *>
    (engine->getAuxiliaryInterface())->addSegmentController(add->getController());
    inputFlags = add->getTargetFlags();
    inputCover = add->getTargetCover();
}

SmootherChainCoreSegmentControlledPreserving::SmootherChainCoreSegmentControlledPreserving(bool intersectingDiscard,
                                                                                           Data::DynamicDouble *setRequiredCover)
        : SmootherChainCoreSegmentControlled(
        new DynamicTimeInterval::Constant(Time::Millisecond, 1), intersectingDiscard,
        setRequiredCover)
{ }

SmootherChainCoreSegmentControlledPreserving::~SmootherChainCoreSegmentControlledPreserving()
{ }

bool SmootherChainCoreSegmentControlledPreserving::differencePreserving() const
{ return true; }


SmoothingEngineSegmentControlled::SmoothingEngineSegmentControlled(Data::DynamicSequenceSelection *segmentInput,
                                                                   Data::DynamicSequenceSelection *output,
                                                                   bool intersectingDiscard,
                                                                   Data::DynamicTimeInterval *setGap,
                                                                   Data::DynamicDouble *setRequiredCover,
                                                                   bool setGapPreserving)
        : SmoothingEngine(
        setGapPreserving ? new SmootherChainCoreSegmentControlledPreserving(intersectingDiscard,
                                                                            setRequiredCover)
                         : new SmootherChainCoreSegmentControlled(setGap, intersectingDiscard,
                                                                  setRequiredCover)),
          segmentSelection(segmentInput),
          outputSelection(output),
          segmenter(),
          registeredInputs(),
          gap(setGap),
          requiredCover(setRequiredCover),
          discardIntersecting(intersectingDiscard),
          gapPreserving(setGapPreserving),
          segments(),
          haveOpenSegment(false),
          segmentIngress(NULL),
          chainSegmentControllers(std::make_shared<std::unordered_map<AuxiliaryChainController *,
                                                                      std::vector<
                                                                              SegmentController *>>>())
{
    segmentIngress = createOutputIngress();
}

SmoothingEngineSegmentControlled::~SmoothingEngineSegmentControlled()
{
    endAllChains();

    if (segmentSelection != NULL)
        delete segmentSelection;
    if (outputSelection != NULL)
        delete outputSelection;
}

void SmoothingEngineSegmentControlled::registerExpectedInputs(const QSet<SequenceName> &inputs)
{
    Util::merge(inputs, registeredInputs);
    for (const auto &add : inputs) {
        segmentSelection->registerInput(add);
    }
}

void SmoothingEngineSegmentControlled::deserialize(QDataStream &stream)
{
    if (segmentSelection != NULL)
        delete segmentSelection;
    if (outputSelection != NULL)
        delete outputSelection;

    /* Don't send these to the saved chains as they will have serialized
     * their own copies. */
    segments.clear();
    haveOpenSegment = false;
    std::deque<ValueSegment> saveSegments;
    bool saveHaveOpenSegment = false;

    stream >> segmentSelection >> outputSelection >> segmenter >> registeredInputs >> gap
           >> requiredCover >> gapPreserving >> discardIntersecting >> saveSegments
           >> saveHaveOpenSegment;
    changeCore(gapPreserving ? new SmootherChainCoreSegmentControlledPreserving(discardIntersecting,
                                                                                requiredCover)
                             : new SmootherChainCoreSegmentControlled(gap, discardIntersecting,
                                                                      requiredCover));

    SmoothingEngine::deserialize(stream);

    /* Now restore them for future un-created chains. */
    haveOpenSegment = saveHaveOpenSegment;
    segments = std::move(saveSegments);
}

void SmoothingEngineSegmentControlled::serialize(QDataStream &stream)
{
    pauseProcessing();
    stream << segmentSelection << outputSelection << segmenter << registeredInputs << gap
           << requiredCover << gapPreserving << discardIntersecting << segments << haveOpenSegment;
    serializeInternal(stream);
    resumeProcessing();
}

SequenceName::Set SmoothingEngineSegmentControlled::requestedInputs()
{ return SmootherChainCoreBinned::requestedInputs(registeredInputs); }

SequenceName::Set SmoothingEngineSegmentControlled::predictedOutputs()
{ return SmootherChainCoreBinned::predictedOutputs(registeredInputs); }

QSet<SequenceName> SmoothingEngineSegmentControlled::buildSegmentingUnits(double time)
{
    if (!segmentSelection)
        return QSet<SequenceName>();
    QSet<SequenceName> result;
    for (const auto &i : segmentSelection->get(time)) {
        result.insert(i);
        result.insert(i.toDefaultStation());
    }
    return result;
}

void SmoothingEngineSegmentControlled::dispatchEnd(double end)
{
    for (const auto &targetChain : *chainSegmentControllers) {
        for (const auto &target : targetChain.second) {
            target->segmentEnd(end);
        }
    }
}

void SmoothingEngineSegmentControlled::dispatchStart(double start)
{
    for (const auto &targetChain : *chainSegmentControllers) {
        for (const auto &target : targetChain.second) {
            target->segmentStart(start);
        }
    }
}

void SmoothingEngineSegmentControlled::dispatchStartEnd(double start, double end)
{
    for (const auto &targetChain : *chainSegmentControllers) {
        for (const auto &target : targetChain.second) {
            target->segmentStart(start);
            target->segmentEnd(end);
        }
    }
}

void SmoothingEngineSegmentControlled::processCompletedSegments(ValueSegment::Transfer &&process)
{
    if (process.empty())
        return;

    std::vector<Time::Bounds> toDispatch;

    /* Handle ending the open one if needed */
    if (haveOpenSegment) {
        Q_ASSERT(!segments.empty());
        Q_ASSERT(FP::equal(segments.back().getStart(), process.front().getStart()));

        for (auto add = process.cbegin() + 1, endAdd = process.cend(); add != endAdd; ++add) {
            toDispatch.emplace_back(add->getStart(), add->getEnd());
        }
        double endedTime = process.front().getEnd();
        Util::append(std::move(process), segments);

        segments.pop_back();
        dispatchEnd(endedTime);
        haveOpenSegment = false;
    } else {
        for (const auto &add : process) {
            toDispatch.emplace_back(add.getStart(), add.getEnd());
        }
        Util::append(std::move(process), segments);
    }

    for (const auto &dS : toDispatch) {
        dispatchStartEnd(dS.getStart(), dS.getEnd());
    }
}

void SmoothingEngineSegmentControlled::processSegmentsUpdated()
{
    bool validTail = false;
    ValueSegment possibleTail(segmenter.intermediateNext(&validTail));

    if (!validTail) {
        if (haveOpenSegment) {
            Q_ASSERT(!segments.empty());
            dispatchEnd(segments.back().getEnd());
            haveOpenSegment = false;
        }
    } else {
        if (haveOpenSegment) {
            Q_ASSERT(!segments.empty());
            Q_ASSERT(FP::equal(segments.back().getStart(), possibleTail.getStart()));
        } else {
            segments.emplace_back(std::move(possibleTail));
            dispatchStart(segments.back().getStart());
            haveOpenSegment = true;
        }
    }
}

void SmoothingEngineSegmentControlled::startProcessing(IncomingBuffer::iterator incomingBegin,
                                                       IncomingBuffer::iterator incomingEnd,
                                                       double &advanceTime,
                                                       double &holdbackTime)
{
    SmoothingEngine::startProcessing(incomingBegin, incomingEnd, advanceTime, holdbackTime);

    if (segmentSelection != NULL) {
        double previousTime = FP::undefined();
        QSet<SequenceName> segmentingUnits;
        for (; incomingBegin != incomingEnd; ++incomingBegin) {
            if (!registeredInputs.count(incomingBegin->getUnit())) {
                registeredInputs.insert(incomingBegin->getUnit());
                if (segmentSelection->registerInput(incomingBegin->getUnit())) {
                    previousTime = incomingBegin->getStart();
                    segmentingUnits = buildSegmentingUnits(previousTime);
                }

                if (outputSelection != NULL) {
                    outputSelection->registerExpected(incomingBegin->getUnit().getStation(),
                                                      incomingBegin->getUnit().getArchive(),
                                                      incomingBegin->getUnit().getVariable(),
                                                      incomingBegin->getUnit().getFlavors());
                }
            }
            if (!FP::defined(previousTime) || previousTime != incomingBegin->getStart()) {
                previousTime = incomingBegin->getStart();
                segmentingUnits = buildSegmentingUnits(previousTime);
            }

            if (!segmentingUnits.contains(incomingBegin->getUnit()))
                continue;

            processCompletedSegments(segmenter.add(*incomingBegin));
        }

        processSegmentsUpdated();
    }

    /* Have to do this now because we need to run it before we actually
     * process data otherwise they can end up in the wrong segment
     * bin */
    if (FP::defined(advanceTime)) {
        processCompletedSegments(segmenter.completedAdvance(advanceTime));
        processSegmentsUpdated();
    }

    if (segments.empty())
        return;

    if (!FP::defined(holdbackTime) ||
            !FP::defined(segments.front().getStart()) ||
            segments.front().getStart() < holdbackTime)
        holdbackTime = segments.front().getStart();
}

void SmoothingEngineSegmentControlled::predictMetadataBounds(double *start, double *end)
{
    if (segments.empty())
        return;
    if (start != NULL) {
        if (FP::defined(*start) &&
                FP::defined(segments.front().getStart()) &&
                *start > segments.front().getStart()) {
            *start = segments.front().getStart();
        }
    }
    if (end != NULL) {
        if (FP::defined(*end) &&
                FP::defined(segments.front().getEnd()) &&
                *end < segments.front().getEnd()) {
            *end = segments.front().getEnd();
        }
    }
}

void SmoothingEngineSegmentControlled::auxiliaryAdvance(double advanceTime)
{
    Q_ASSERT(FP::defined(advanceTime));

    processCompletedSegments(segmenter.completedAdvance(advanceTime));
    processSegmentsUpdated();

    auto endErase = segments.begin();
    auto endExamine = segments.end();
    if (haveOpenSegment) {
        Q_ASSERT(endErase != endExamine);
        --endExamine;
    }

    double endAdvance = FP::undefined();
    for (; endErase != endExamine; ++endErase) {
        if (!FP::defined(endErase->getEnd()) || endErase->getEnd() >= advanceTime)
            break;
        if (outputSelection) {
            for (const auto &output : outputSelection->get(*endErase)) {
                segmentIngress->emplaceData(
                        SequenceIdentity(output, endErase->getStart(), endErase->getEnd()),
                        endErase->root());
            }
        }
        endAdvance = endErase->getEnd();
    }

    segments.erase(segments.begin(), endErase);

    if (FP::defined(endAdvance)) {
        segmentIngress->incomingAdvance(endAdvance);
    }
}

void SmoothingEngineSegmentControlled::auxiliaryEndBefore()
{
    auto process = segmenter.finish();
    Q_ASSERT(!haveOpenSegment || !process.empty());
    processCompletedSegments(std::move(process));
}

void SmoothingEngineSegmentControlled::auxiliaryEndAfter()
{
    Q_ASSERT(!haveOpenSegment);

    for (const auto &seg : segments) {
        if (outputSelection) {
            for (const auto &output : outputSelection->get(seg)) {
                segmentIngress->emplaceData(SequenceIdentity(output, seg.getStart(), seg.getEnd()),
                                            seg.root());
            }
        }
    }
    segments.clear();

    if (segmentIngress != NULL) {
        segmentIngress->endData();
        segmentIngress = NULL;
    }

    chainSegmentControllers->clear();
}

std::unique_ptr<
        SmoothingEngine::EngineInterface> SmoothingEngineSegmentControlled::createEngineInterface()
{ return std::unique_ptr<SmoothingEngine::EngineInterface>(new EngineInterface(this)); }


SmoothingEngineSegmentControlled::EngineInterface::EngineInterface(SmoothingEngineSegmentControlled *engine)
        : SmoothingEngine::EngineInterface(engine), parent(engine)
{
    chainController = new SmoothingEngineSegmentControlled::AuxiliaryChainController(engine);
    setChainAuxiliary(chainController);
}

SmoothingEngineSegmentControlled::EngineInterface::~EngineInterface() = default;

void SmoothingEngineSegmentControlled::EngineInterface::addSegmentController(SegmentController *controller)
{
    auto endSeg = parent->segments.cend();
    if (parent->haveOpenSegment) {
        Q_ASSERT(endSeg != parent->segments.cbegin());
        --endSeg;
    }
    for (auto seg = parent->segments.cbegin();
            seg != endSeg;
            ++seg) {
        controller->segmentStart(seg->getStart());
        controller->segmentEnd(seg->getEnd());
    }

    if (parent->haveOpenSegment) {
        controller->segmentStart(endSeg->getStart());
    }

    (*(parent->chainSegmentControllers))[chainController].push_back(controller);
}

SmootherChainCoreEngineAuxiliaryInterface *SmoothingEngineSegmentControlled::EngineInterface::getAuxiliaryInterface()
{ return this; }

SmoothingEngineSegmentControlled::AuxiliaryChainController::AuxiliaryChainController(
        SmoothingEngineSegmentControlled *engine) : chainSegmentControllers(
        engine->chainSegmentControllers)
{ }

SmoothingEngineSegmentControlled::AuxiliaryChainController::~AuxiliaryChainController()
{
    auto check = chainSegmentControllers->find(this);
    if (check != chainSegmentControllers->end()) {
        chainSegmentControllers->erase(check);
    }
}

}
}
