/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGBINNEDCHAIN_H
#define CPD3SMOOTHINGBINNEDCHAIN_H

#include "core/first.hxx"

#include <stdlib.h>
#include <cstring>
#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QDataStream>

#include "smoothing/smoothing.hxx"
#include "smoothing/smoothingchain.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "datacore/stream.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "algorithms/statistics.hxx"

namespace CPD3 {
namespace Smoothing {
namespace Internal {
struct FlagsSegmentData {
    double cover;
    Data::Variant::Flags flags;

    inline FlagsSegmentData &operator=(double v)
    {
        cover = v;
        flags.clear();
        return *this;
    }
};

template<std::size_t N>
class BinArrayWrapper {
    double buffer[N];
public:
    inline BinArrayWrapper()
    { }

    inline BinArrayWrapper(const double in[N])
    { std::memcpy(buffer, in, N * sizeof(double)); }

    inline BinArrayWrapper(const BinArrayWrapper<N> &other)
    { std::memcpy(buffer, other.buffer, N * sizeof(double)); }

    inline BinArrayWrapper<N> &operator=(const BinArrayWrapper<N> &other)
    {
        Q_ASSERT(&other != this);
        std::memcpy(buffer, other.buffer, N * sizeof(double));
        return *this;
    }

    inline BinArrayWrapper<N> &operator=(const double in[N])
    {
        std::memcpy(buffer, in, N * sizeof(double));
        return *this;
    }

    inline double &operator[](std::size_t i)
    { return buffer[i]; }

    inline const double &operator[](std::size_t i) const
    { return buffer[i]; }
};

template<std::size_t N>
QDataStream &operator<<(QDataStream &stream, const BinArrayWrapper<N> &value)
{
    for (std::size_t i = 0; i < N; i++) {
        stream << value[i];
    }
    return stream;
}

template<std::size_t N>
QDataStream &operator>>(QDataStream &stream, BinArrayWrapper<N> &value)
{
    for (std::size_t i = 0; i < N; i++) {
        stream >> value[i];
    }
    return stream;
}

Data::Variant::Root binnedChainGenerateStatistics(QVector<double> values);

Data::Variant::Root binnedChainGenerateVectorStatistics
        (QVector<double> values, double average, double vectored);

void binnedChainGenerateAverage(const QVector<double> &values,
                                const QVector<double> &cover,
                                const QVector<double> &durations,
                                double start,
                                double end,
                                double &targetAverage,
                                double &targetCover);

CPD3SMOOTHING_EXPORT QDataStream &operator<<(QDataStream &stream, const FlagsSegmentData &value);

CPD3SMOOTHING_EXPORT QDataStream &operator>>(QDataStream &stream, FlagsSegmentData &value);

}
}
}

/* Primitive type, declared like this since we can't do an unspecialized
 * template. */
template<std::size_t N>
class QTypeInfo<CPD3::Smoothing::Internal::BinArrayWrapper<N> > {
public:
    enum {
        isComplex = false, isStatic = false, isLarge = true, isPointer = false, isDummy = false,
    };

    static inline const char *name()
    { return "CPD3::Smoothing::Internal::BinArrayWrapper<N>"; }
};

namespace CPD3 {
namespace Smoothing {

/**
 * Conventional averaging for time bins.  This takes as input
 * a stream of values and their coverage factors and produces the same
 * as output (averaged to the binning) as well as a statistics variable for 
 * the binned time.
 * <br>
 * This requires that the binner provide the following methods:
 * <ul>
 *  <li> SmootherChainTarget *getTarget(int i) - Returns a target for input
 *  <li> processBin( double, double, const QVector<double> [N], const QVector<double> & ) - Called to process a bin's data
 *  <li> advanceBin( double ) - Called on bin advance
 *  <li> completedBinning() - Called when binning is completed
 * </ul>
 * 
 * @param BinnerControl     the base binner class
 */
template<template<std::size_t N> class BinnerControl>
class SmootherChainBinnedConventional : public BinnerControl<2> {
    Data::DynamicDouble *requiredCover;
    SmootherChainTarget *targetAverage;
    SmootherChainTarget *targetCover;
    SmootherChainTargetGeneral *targetStats;

    typedef BinnerControl<2> Parent;

public:
    /**
     * Create a conventional averager with the given parameters.
     * 
     * @param coverRequired the minimum coverage required or NULL, this takes ownership
     * @param outputAverage the output target for the average or NULL
     * @param outputCover   the output target for the cover factor or NULL
     * @param outputStats   the output target for the statistics variable or NULL
     */
    SmootherChainBinnedConventional(Data::DynamicDouble *coverRequired,
                                    SmootherChainTarget *outputAverage,
                                    SmootherChainTarget *outputCover = NULL,
                                    SmootherChainTargetGeneral *outputStats = NULL)
            : Parent(),
              requiredCover(coverRequired),
              targetAverage(outputAverage),
              targetCover(outputCover),
              targetStats(outputStats)
    { }

    /**
     * Restore the averager from the serialized state in the stream.
     * 
     * @param stream        the stream to restore from
     * @param outputAverage the output target for the average or NULL
     * @param outputCover   the output target for the cover factor or NULL
     * @param outputStats   the output target for the statistics variable or NULL
     */
    SmootherChainBinnedConventional(QDataStream &stream,
                                    SmootherChainTarget *outputAverage,
                                    SmootherChainTarget *outputCover = NULL,
                                    SmootherChainTargetGeneral *outputStats = NULL) : Parent(
            stream),
                                                                                      requiredCover(
                                                                                              NULL),
                                                                                      targetAverage(
                                                                                              outputAverage),
                                                                                      targetCover(
                                                                                              outputCover),
                                                                                      targetStats(
                                                                                              outputStats)
    {
        stream >> requiredCover;
    }

    virtual ~SmootherChainBinnedConventional()
    {
        if (requiredCover != NULL)
            delete requiredCover;
    }

    virtual void serialize(QDataStream &stream) const
    {
        Parent::serialize(stream);
        stream << requiredCover;
    }

    /**
     * Get the target to receive value data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetValue()
    { return Parent::getTarget(0); }

    /**
     * Get the target to receive coverage data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetCover()
    { return Parent::getTarget(1); }

protected:
    virtual void processBin(double start,
                            double end,
                            const QVector<double> values[2],
                            const QVector<double> &durations)
    {
        Q_ASSERT(values[0].size() == durations.size());
        Q_ASSERT(values[1].size() == durations.size());

        if (targetStats != NULL) {
            targetStats->incomingData(start, end,
                                      Internal::binnedChainGenerateStatistics(values[0]));
        }

        double average = 0;
        double cover = 0;
        Internal::binnedChainGenerateAverage(values[0], values[1], durations, start, end, average,
                                             cover);

        if (targetCover != NULL) {
            if (!FP::defined(cover) || cover >= 1.0) {
                if (FP::defined(end))
                    targetCover->incomingAdvance(end);
            } else {
                targetCover->incomingData(start, end, cover);
            }
        }

        if (targetAverage == NULL)
            return;

        if (requiredCover != NULL && FP::defined(cover)) {
            double rq = requiredCover->get(start);
            if (FP::defined(rq) && cover < rq) {
                targetAverage->incomingData(start, end, FP::undefined());
                return;
            }
        }

        targetAverage->incomingData(start, end, average);
    }

    virtual void advanceBin(double time)
    {
        if (targetAverage != NULL)
            targetAverage->incomingAdvance(time);
        if (targetCover != NULL)
            targetCover->incomingAdvance(time);
        if (targetStats != NULL)
            targetStats->incomingAdvance(time);
    }

    virtual void completedBinning()
    {
        if (targetAverage != NULL)
            targetAverage->endData();
        if (targetCover != NULL)
            targetCover->endData();
        if (targetStats != NULL)
            targetStats->endData();
    }
};

/**
 * Vector statistics generating for time bins.  This takes as input the streams
 * of unaveraged vector magnitude values, their coverage factors, and the
 * averaged vector magnitude (calculated with the normal component breakdown
 * then average routine).  It produces as output the un-vectored magnitude
 * average and coverage as well statistics about the magnitude (including a 
 * "stability factor" that is the ratio of the un-vectored average to the 
 * vectored one).
 * <br>
 * This requires that the binner provide the following methods:
 * <ul>
 *  <li> SmootherChainTarget *getTarget(int i) - Returns a target for input
 *  <li> processBin( double, double, const QVector<double> [N], const QVector<double> & ) - Called to process a bin's data
 *  <li> advanceBin( double ) - Called on bin advance
 *  <li> completedBinning() - Called when binning is completed
 * </ul>
 * 
 * @param BinnerControl     the base binner class
 */
template<template<std::size_t N> class BinnerControl>
class SmootherChainBinnedVectorStatistics : public BinnerControl<3> {
    Data::DynamicDouble *requiredCover;
    SmootherChainTarget *targetAverage;
    SmootherChainTarget *targetCover;
    SmootherChainTargetGeneral *targetStats;

    typedef BinnerControl<3> Parent;

public:
    /**
     * Create a vector statistics average with the given parameters.
     * 
     * @param coverRequired the minimum coverage required or NULL, this takes ownership
     * @param outputAverage the output target for the average or NULL
     * @param outputCover   the output target for the cover factor or NULL
     * @param outputStats   the output target for the statistics variable or NULL
     */
    SmootherChainBinnedVectorStatistics(Data::DynamicDouble *coverRequired,
                                        SmootherChainTarget *outputAverage,
                                        SmootherChainTarget *outputCover = NULL,
                                        SmootherChainTargetGeneral *outputStats = NULL)
            : Parent(),
              requiredCover(coverRequired),
              targetAverage(outputAverage),
              targetCover(outputCover),
              targetStats(outputStats)
    { }

    /**
     * Restore the averager from the serialized state in the stream.
     * 
     * @param stream        the stream to restore from
     * @param outputAverage the output target for the average or NULL
     * @param outputCover   the output target for the cover factor or NULL
     * @param outputStats   the output target for the statistics variable or NULL
     */
    SmootherChainBinnedVectorStatistics(QDataStream &stream,
                                        SmootherChainTarget *outputAverage,
                                        SmootherChainTarget *outputCover = NULL,
                                        SmootherChainTargetGeneral *outputStats = NULL) : Parent(
            stream),
                                                                                          requiredCover(
                                                                                                  NULL),
                                                                                          targetAverage(
                                                                                                  outputAverage),
                                                                                          targetCover(
                                                                                                  outputCover),
                                                                                          targetStats(
                                                                                                  outputStats)
    {
        stream >> requiredCover;
    }

    virtual ~SmootherChainBinnedVectorStatistics()
    {
        if (requiredCover != NULL)
            delete requiredCover;
    }

    virtual void serialize(QDataStream &stream) const
    {
        Parent::serialize(stream);
        stream << requiredCover;
    }

    /**
     * Get the target to receive value data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetValue()
    { return Parent::getTarget(0); }

    /**
     * Get the target to receive coverage data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetCover()
    { return Parent::getTarget(1); }

    /**
     * Get the target to receive vector averaged magnitude data.  The returned 
     * pointer is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetVectoredMagnitude()
    { return Parent::getTarget(2); }

protected:
    virtual void processBin(double start,
                            double end,
                            const QVector<double> values[3],
                            const QVector<double> &durations)
    {
        Q_ASSERT(values[0].size() == durations.size());
        Q_ASSERT(values[1].size() == durations.size());
        Q_ASSERT(values[2].size() == durations.size());

        double average = 0;
        double cover = 0;
        Internal::binnedChainGenerateAverage(values[0], values[1], durations, start, end, average,
                                             cover);

        if (targetStats != NULL) {
            double sumVectored = 0.0;
            int countVectored = 0;
            for (QVector<double>::const_iterator addValue = values[2].constBegin(),
                    endValue = values[2].constEnd(); addValue != endValue; ++addValue) {
                if (!FP::defined(*addValue))
                    continue;
                sumVectored += *addValue;
                ++countVectored;
            }

            if (countVectored != 0) {
                sumVectored /= (double) countVectored;
            } else {
                sumVectored = FP::undefined();
            }

            targetStats->incomingData(start, end,
                                      Internal::binnedChainGenerateVectorStatistics(values[0],
                                                                                    average,
                                                                                    sumVectored));
        }

        if (targetCover != NULL) {
            if (!FP::defined(cover) || cover >= 1.0) {
                if (FP::defined(end))
                    targetCover->incomingAdvance(end);
            } else {
                targetCover->incomingData(start, end, cover);
            }
        }

        if (targetAverage == NULL)
            return;

        if (requiredCover != NULL && FP::defined(cover)) {
            double rq = requiredCover->get(start);
            if (FP::defined(rq) && cover < rq) {
                targetAverage->incomingData(start, end, FP::undefined());
                return;
            }
        }
        targetAverage->incomingData(start, end, average);
    }

    virtual void advanceBin(double time)
    {
        if (targetAverage != NULL)
            targetAverage->incomingAdvance(time);
        if (targetCover != NULL)
            targetCover->incomingAdvance(time);
        if (targetStats != NULL)
            targetStats->incomingAdvance(time);
    }

    virtual void completedBinning()
    {
        if (targetAverage != NULL)
            targetAverage->endData();
        if (targetCover != NULL)
            targetCover->endData();
        if (targetStats != NULL)
            targetStats->endData();
    }
};

/**
 * Start and end value selection for binned data.  This takes as input
 * a stream of values at the start and end of intervals (e.x. the normal values
 * and those with the "end" flavor).  It produces a value stream for the
 * start and end as well as a coverage figure.  This is often fed into a
 * combining chain element (e.x. SmootherChainBeersLawAbsorption).
 * <br>
 * This requires that the binner provide the following methods:
 * <ul>
 *  <li> SmootherChainTarget *getTarget(int i) - Returns a target for input
 *  <li> processBin( double, double, const QVector<double> [N], const QVector<double> & ) - Called to process a bin's data
 *  <li> advanceBin( double ) - Called on bin advance
 *  <li> completedBinning() - Called when binning is completed
 * </ul>
 * 
 * @param BinnerControl     the base binner class
 */
template<template<std::size_t N> class BinnerControl>
class SmootherChainBinnedDifference : public BinnerControl<2> {
    SmootherChainTarget *targetStart;
    SmootherChainTarget *targetEnd;
    SmootherChainTarget *targetCover;

    typedef BinnerControl<2> Parent;

public:
    /**
     * Create a difference averager with the given outputs.
     * 
     * @param outputStart   the output target for the value at the start or NULL
     * @param outputEnd     the output target for the value at the end or NULL
     * @param outputCover   the output target for the coverage or NULL
     */
    SmootherChainBinnedDifference(SmootherChainTarget *outputStart,
                                  SmootherChainTarget *outputEnd = NULL,
                                  SmootherChainTarget *outputCover = NULL)
            : Parent(), targetStart(outputStart), targetEnd(outputEnd), targetCover(outputCover)
    { }

    /**
     * Restore the averager from the serialized state in the stream.
     * 
     * @param stream        the stream to restore from
     * @param outputStart   the output target for the value at the start or NULL
     * @param outputEnd     the output target for the value at the end or NULL
     * @param outputCover   the output target for the coverage or NULL
     */
    SmootherChainBinnedDifference(QDataStream &stream,
                                  SmootherChainTarget *outputStart,
                                  SmootherChainTarget *outputEnd = NULL,
                                  SmootherChainTarget *outputCover = NULL) : Parent(stream),
                                                                             targetStart(
                                                                                     outputStart),
                                                                             targetEnd(outputEnd),
                                                                             targetCover(
                                                                                     outputCover)
    { }


    /**
     * Get the target to receive start value data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetStart()
    { return Parent::getTarget(0); }

    /**
     * Get the target to receive end value data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetEnd()
    { return Parent::getTarget(1); }

protected:
    virtual void processBin(double start,
                            double end,
                            const QVector<double> values[2],
                            const QVector<double> &durations)
    {
        Q_ASSERT(values[0].size() == durations.size());
        Q_ASSERT(values[1].size() == durations.size());

        QVector<double>::const_iterator startValue = values[0].constBegin();
        QVector<double>::const_iterator endValue = values[1].constBegin();
        QVector<double>::const_iterator firstDuration = durations.constBegin();
        QVector<double>::const_iterator finalDuration = durations.constEnd();
        double totalTime = 0.0;
        for (; firstDuration != finalDuration; ++startValue, ++endValue, ++firstDuration) {
            if (FP::defined(*startValue)) {
                if (FP::defined(*firstDuration) && *firstDuration > 0.0)
                    totalTime += *firstDuration;
                if (targetStart != NULL)
                    targetStart->incomingData(start, end, *startValue);
                break;
            }
            if (FP::defined(*endValue)) {
                if (targetStart != NULL)
                    targetStart->incomingData(start, end, *endValue);
                break;
            }
        }
        if (firstDuration == finalDuration) {
            if (targetStart != NULL) {
                targetStart->incomingData(start, end, FP::undefined());
            }
            if (targetEnd != NULL) {
                targetEnd->incomingData(start, end, FP::undefined());
            }
            if (targetCover != NULL) {
                if (FP::defined(end)) {
                    targetCover->incomingAdvance(end);
                }
            }
            return;
        }

        --finalDuration;
        for (startValue = values[0].constEnd() - 1, endValue = values[1].constEnd() - 1;
                finalDuration != firstDuration;
                --startValue, --endValue, --finalDuration) {
            if (FP::defined(*endValue)) {
                if (FP::defined(*finalDuration) && *finalDuration > 0.0)
                    totalTime += *finalDuration;
                if (targetEnd != NULL)
                    targetEnd->incomingData(start, end, *endValue);
                break;
            }
            if (FP::defined(*startValue)) {
                if (FP::defined(*finalDuration) && *finalDuration > 0.0)
                    totalTime += *finalDuration;
                if (targetEnd != NULL)
                    targetEnd->incomingData(start, end, *startValue);
                break;
            }
        }

        if (firstDuration == finalDuration) {
            /* Check for the case of a single point with both start and end 
             * values */
            if (FP::defined(*startValue)) {
                if (targetEnd != NULL)
                    targetEnd->incomingData(start, end, *endValue);
                if (targetCover != NULL) {
                    if (FP::defined(*endValue) &&
                            FP::defined(start) &&
                            FP::defined(end) &&
                            end > start &&
                            totalTime > 0.0) {
                        double cover = totalTime / (end - start);
                        if (cover >= 1.0) {
                            targetCover->incomingAdvance(end);
                        } else {
                            targetCover->incomingData(start, end, cover);
                        }
                    } else if (FP::defined(end)) {
                        targetCover->incomingAdvance(end);
                    }
                }
            } else {
                if (targetEnd != NULL) {
                    targetEnd->incomingData(start, end, FP::undefined());
                }
                if (targetCover != NULL && FP::defined(end)) {
                    targetCover->incomingAdvance(end);
                }
            }
            return;
        }

        if (targetCover == NULL)
            return;
        if (!FP::defined(start) || !FP::defined(end) || end <= start) {
            if (FP::defined(end))
                targetCover->incomingAdvance(end);
            return;
        }

        for (++firstDuration; firstDuration != finalDuration; ++firstDuration) {
            if (!FP::defined(*firstDuration) || *firstDuration <= 0.0) {
                totalTime = 0.0;
                break;
            }
            totalTime += *firstDuration;
        }

        if (totalTime > 0.0) {
            double cover = totalTime / (end - start);
            if (cover >= 1.0) {
                targetCover->incomingAdvance(end);
            } else {
                targetCover->incomingData(start, end, cover);
            }
        } else {
            targetCover->incomingAdvance(end);
        }
    }

    virtual void advanceBin(double time)
    {
        if (targetStart != NULL)
            targetStart->incomingAdvance(time);
        if (targetEnd != NULL)
            targetEnd->incomingAdvance(time);
        if (targetCover != NULL)
            targetCover->incomingAdvance(time);
    }

    virtual void completedBinning()
    {
        if (targetStart != NULL)
            targetStart->endData();
        if (targetEnd != NULL)
            targetEnd->endData();
        if (targetCover != NULL)
            targetCover->endData();
    }
};


/**
 * Flags combining binner.  This simply ORs all flags within a bin together.
 */
template<template<typename T> class BinnerBase>
class SmootherChainBinnedFlags
        : public SmootherChainNode,
          protected SmootherChainInputSegmentProcessorBase<2, Internal::FlagsSegmentData>,
          public BinnerBase<Internal::FlagsSegmentData> {
    SmootherChainTargetFlags *targetFlags;
    SmootherChainTarget *targetCover;

    typedef Internal::FlagsSegmentData Storage;
    typedef BinnerBase<Storage> Binner;
    typedef SmootherChainInputSegmentProcessorBase<2, Internal::FlagsSegmentData> Processor;

    /* Don't need a mutex for this, since it's only written once we no
     * longer care. */
    bool segmentsFinished;

    inline void streamDone(int stream)
    {
        if (Processor::segmentEnd(stream)) {
            Binner::incomingSegmentsCompleted();
            segmentsFinished = true;
        }
    }

    class TargetCover : public SmootherChainTarget {
    public:
        SmootherChainBinnedFlags<BinnerBase> *node;

        TargetCover(SmootherChainBinnedFlags<BinnerBase> *n) : node(n)
        { }

        virtual ~TargetCover()
        { }

        virtual void incomingData(double start, double end, double value)
        {
            Internal::FlagsSegmentData data;
            data.cover = value;
            node->segmentValue(1, start, end, data);
        }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(1, time); }

        virtual void endData()
        { node->streamDone(1); }
    };

    friend class TargetCover;

    TargetCover targetIncomingCover;

    class TargetFlags : public SmootherChainTargetFlags {
    public:
        SmootherChainBinnedFlags<BinnerBase> *node;

        TargetFlags(SmootherChainBinnedFlags<BinnerBase> *n) : node(n)
        { }

        virtual ~TargetFlags()
        { }

        virtual void incomingData(double start, double end, const Data::Variant::Flags &value)
        {
            Internal::FlagsSegmentData data;
            data.flags = value;
            node->segmentValue(0, start, end, data);
        }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(0, time); }

        virtual void endData()
        { node->streamDone(0); }
    };

    friend class TargetFlags;

    TargetFlags targetIncomingFlags;

    struct BufferData {
        Data::Variant::Flags flags;
        double cover;
        double duration;

        BufferData(const Data::Variant::Flags &flags, double cover, double start, double end)
                : flags(flags), cover(cover)
        {
            if (FP::defined(start) && FP::defined(end))
                duration = end - start;
            else
                duration = FP::undefined();
        }

        BufferData() = default;
    };

    std::vector<BufferData> buffer;

public:
    /**
     * Create a new flags binning combiner.
     * 
     * @param outputFlags       the output target for the averaged flags
     * @param outputCover       the output target for the coverage or NULL
     */
    SmootherChainBinnedFlags(SmootherChainTargetFlags *outputFlags,
                             SmootherChainTarget *outputCover = NULL)
            : Processor(),
              Binner(),
              targetFlags(outputFlags),
              targetCover(outputCover),
              segmentsFinished(false),
              targetIncomingCover(this),
              targetIncomingFlags(this)
    { }

    /**
     * Create a new binning object, deserializing the state from the given
     * stream.
     * 
     * @param stream        the stream to read state from
     * @param outputFlags       the output target for the averaged flags
     * @param outputCover       the output target for the coverage or NULL
     */
    SmootherChainBinnedFlags(QDataStream &stream,
                             SmootherChainTargetFlags *outputFlags,
                             SmootherChainTarget *outputCover = NULL)
            : Processor(),
              Binner(),
              targetFlags(outputFlags),
              targetCover(outputCover),
              segmentsFinished(false),
              targetIncomingCover(this), targetIncomingFlags(this)
    {
        Processor::readSerial(stream);
        Binner::readSerial(stream);
        Deserialize::container(stream, buffer, [&]() {
            BufferData data;
            stream >> data.flags >> data.cover >> data.duration;
            return data;
        });
    }

    virtual ~SmootherChainBinnedFlags() = default;

    virtual bool finished()
    { return segmentsFinished; }

    virtual void pause()
    { Processor::freezeState(); }

    virtual void resume()
    { Processor::unfreezeState(); }

    virtual void serialize(QDataStream &stream) const
    {
        Processor::writeSerial(stream);
        Binner::writeSerial(stream);
        Serialize::container(stream, buffer, [&](const BufferData &data) {
            stream << data.flags << data.cover << data.duration;
        });
    }

    /**
     * Get the target for flags data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTargetFlags *getTargetFlags()
    { return &targetIncomingFlags; }

    /**
     * Get the target for coverage data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetCover()
    { return &targetIncomingCover; }

protected:
    virtual void segmentDone(double start, double end, const Internal::FlagsSegmentData values[2])
    {
        Internal::FlagsSegmentData combined = values[0];
        combined.cover = values[1].cover;
        Binner::incomingSegment(start, end, combined);
    }

    virtual void allStreamsAdvanced(double time)
    { Binner::incomingSegmentAdvance(time); }

    virtual void addToBin(double start, double end, const Storage &data)
    {
        buffer.emplace_back(Data::Variant::Flags(data.flags), data.cover, start, end);
    }

    virtual void processBin(double start, double end)
    {
        Data::Variant::Flags combined;
        double totalTime = 0.0;
        for (auto add = buffer.begin(), endAdd = buffer.end(); add != endAdd; ++add) {
            Util::merge(std::move(add->flags), combined);

            if (!FP::defined(add->duration)) {
                totalTime = 0.0;
                for (++add; add != endAdd; ++add) {
                    Util::merge(std::move(add->flags), combined);
                }
                break;
            }
            if (!FP::defined(add->cover)) {
                totalTime += add->duration;
            } else if (add->cover > 0.0) {
                totalTime += add->duration * add->cover;
            }
        }
        buffer.clear();

        if (targetFlags)
            targetFlags->incomingData(start, end, std::move(combined));

        if (targetCover) {
            if (totalTime != 0.0) {
                if (FP::defined(start) && FP::defined(end) && end > start) {
                    double cover = totalTime / (end - start);
                    if (cover >= 1.0) {
                        targetCover->incomingAdvance(end);
                    } else {
                        targetCover->incomingData(start, end, cover);
                    }
                } else if (FP::defined(end)) {
                    targetCover->incomingAdvance(end);
                }
            } else if (FP::defined(end)) {
                targetCover->incomingAdvance(end);
            }
        }
    }

    virtual void completedBinning()
    {
        if (targetFlags)
            targetFlags->endData();
        if (targetCover)
            targetCover->endData();
    }

    virtual void advanceBin(double time)
    {
        if (targetFlags)
            targetFlags->incomingAdvance(time);
        if (targetCover)
            targetCover->incomingAdvance(time);
    }
};

/**
 * A smoother chain core for general binned chains.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreBinned : public SmootherChainCore {
    bool enableStatistics;
public:
    /**
     * Create a new binned chain core.
     * 
     * @param setEnableStatistics   enable statistics generation
     */
    SmootherChainCoreBinned(bool setEnableStatistics = true);

    virtual ~SmootherChainCoreBinned();

    virtual void
            createSmoother(SmootherChainCoreEngineInterface *engine, SmootherChainCore::Type type);

    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainCore::Type type);

    virtual SmootherChainCore::HandlerMode smootherHandler(SmootherChainCore::Type type);

    virtual bool flagsHandler();

    virtual void createFlags(SmootherChainCoreEngineInterface *engine);

    virtual void deserializeFlags(QDataStream &stream, SmootherChainCoreEngineInterface *engine);

    virtual bool generatedOutput(const Data::SequenceName &unit);

    virtual Data::Variant::Root getProcessingMetadata(double time) const = 0;

    virtual QSet<double> getProcessingMetadataBreaks() const = 0;

    /**
     * Test if this core preserves differences.  That is, if it will split
     * a bin to preserve continuity of differences.  If it does not then
     * some calculations are not repeatable.
     * 
     * @return true if the core always preserves differences
     */
    virtual bool differencePreserving() const = 0;

    /**
     * Create a conventional averager.  Generally this should just 
     * allocate a sub-class of SmootherChainBinnedConventional.
     * 
     * @param engine        the engine interface
     * @param inputValue    set by the implementation to the input target for values
     * @param inputCover    set by the implementation to the input target for coverage
     * @param outputAverage the output target for the average or NULL to discard
     * @param outputCover   the output target for coverage or NULL to discard
     * @param outputStats   the output target for statistics or NULL to discard
     */
    virtual void createConventional(SmootherChainCoreEngineInterface *engine,
                                    SmootherChainTarget *&inputValue,
                                    SmootherChainTarget *&inputCover,
                                    SmootherChainTarget *outputAverage,
                                    SmootherChainTarget *outputCover = NULL,
                                    SmootherChainTargetGeneral *outputStats = NULL) = 0;

    /**
     * Create a conventional averager.  Generally this should just 
     * allocate a sub-class of SmootherChainBinnedVectorStatistics.
     * 
     * @param stream    the stream to restore from
     * @param engine        the engine interface
     * @param inputValue    set by the implementation to the input target for values
     * @param inputCover    set by the implementation to the input target for coverage
     * @param outputAverage the output target for the average or NULL to discard
     * @param outputCover   the output target for coverage or NULL to discard
     * @param outputStats   the output target for statistics or NULL to discard
     */
    virtual void deserializeConventional(QDataStream &stream,
                                         SmootherChainCoreEngineInterface *engine,
                                         SmootherChainTarget *&inputValue,
                                         SmootherChainTarget *&inputCover,
                                         SmootherChainTarget *outputAverage,
                                         SmootherChainTarget *outputCover = NULL,
                                         SmootherChainTargetGeneral *outputStats = NULL) = 0;


    /**
     * Create a vector statistics averager.  Generally this should just 
     * allocate a sub-class of SmootherChainBinnedVectorStatistics.
     * 
     * @param engine        the engine interface
     * @param inputMagnitude    set by the implementation to the input target for values
     * @param inputCover    set by the implementation to the input target for coverage
     * @param inputVectoredMagnitude set by the implementation to the input for the vector averaged magnitude
     * @param outputAverage the output target for the average or NULL to discard
     * @param outputCover   the output target for coverage or NULL to discard
     * @param outputStats   the output target for statistics or NULL to discard
     */
    virtual void createVectorStatistics(SmootherChainCoreEngineInterface *engine,
                                        SmootherChainTarget *&inputMagnitude,
                                        SmootherChainTarget *&inputCover,
                                        SmootherChainTarget *&inputVectoredMagnitude,
                                        SmootherChainTarget *outputAverage = NULL,
                                        SmootherChainTarget *outputCover = NULL,
                                        SmootherChainTargetGeneral *outputStats = NULL) = 0;

    /**
     * Create a vector statistics averager.  Generally this should just 
     * allocate a sub-class of SmootherChainBinnedConventional.
     * 
     * @param stream    the stream to restore from
     * @param engine        the engine interface
     * @param inputMagnitude    set by the implementation to the input target for values
     * @param inputCover    set by the implementation to the input target for coverage
     * @param inputVectoredMagnitude set by the implementation to the input for the vector averaged magnitude
     * @param outputAverage the output target for the average or NULL to discard
     * @param outputCover   the output target for coverage or NULL to discard
     * @param outputStats   the output target for statistics or NULL to discard
     */
    virtual void deserializeVectorStatistics(QDataStream &stream,
                                             SmootherChainCoreEngineInterface *engine,
                                             SmootherChainTarget *&inputMagnitude,
                                             SmootherChainTarget *&inputCover,
                                             SmootherChainTarget *&inputVectoredMagnitude,
                                             SmootherChainTarget *outputAverage = NULL,
                                             SmootherChainTarget *outputCover = NULL,
                                             SmootherChainTargetGeneral *outputStats = NULL) = 0;


    /**
     * Create a difference averager.  Generally this should just 
     * allocate a sub-class of SmootherChainBinnedDifference.
     * 
     * @param engine        the engine interface
     * @param alwaysBreak   if set then use a differencer that breaks on any data gap (e.x. for Beer's law differences)
     * @param inputStart    set by the implementation to the input target for start values
     * @param inputEnd      set by the implementation to the input target for end values
     * @param outputStart   the output target for start values or NULL to discard
     * @param outputEnd     the output target for end values or NULL to discard
     * @param outputCover   the output target for coverage or NULL to discard
     */
    virtual void createDifference(SmootherChainCoreEngineInterface *engine,
                                  bool alwaysBreak,
                                  SmootherChainTarget *&inputStart,
                                  SmootherChainTarget *&inputEnd,
                                  SmootherChainTarget *outputStart,
                                  SmootherChainTarget *outputEnd,
                                  SmootherChainTarget *outputCover = NULL) = 0;

    /**
     * Create a difference averager.  Generally this should just 
     * allocate a sub-class of SmootherChainBinnedDifference.
     * 
     * @param stream    the stream to restore from
     * @param engine        the engine interface
     * @param alwaysBreak   if set then use a differencer that breaks on any data gap (e.x. for Beer's law differences)
     * @param inputStart    set by the implementation to the input target for start values
     * @param inputEnd      set by the implementation to the input target for end values
     * @param outputStart   the output target for start values or NULL to discard
     * @param outputEnd     the output target for end values or NULL to discard
     * @param outputCover   the output target for coverage or NULL to discard
     */
    virtual void deserializeDifference(QDataStream &stream,
                                       SmootherChainCoreEngineInterface *engine,
                                       bool alwaysBreak,
                                       SmootherChainTarget *&inputStart,
                                       SmootherChainTarget *&inputEnd,
                                       SmootherChainTarget *outputStart,
                                       SmootherChainTarget *outputEnd,
                                       SmootherChainTarget *outputCover = NULL) = 0;


    /**
     * Create a flags averager.  Generally this should just 
     * allocate a sub-class of SmootherChainBinnedFlags.
     * 
     * @param engine        the engine interface
     * @param inputFlags    set by the implementation to the input target for the flags
     * @param inputCover    set by the implementation to the input target for coverage
     * @param outputFlags   the output target for the flags
     * @param outputCover   the output target for coverage or NULL to discard
     */
    virtual void createFlags(SmootherChainCoreEngineInterface *engine,
                             SmootherChainTargetFlags *&inputFlags,
                             SmootherChainTarget *&inputCover,
                             SmootherChainTargetFlags *outputFlags,
                             SmootherChainTarget *outputCover = NULL) = 0;

    /**
     * Create a flags averager.  Generally this should just 
     * allocate a sub-class of SmootherChainBinnedFlags.
     * 
     * @param stream    the stream to restore from
     * @param engine        the engine interface
     * @param inputFlags    set by the implementation to the input target for the flags
     * @param inputCover    set by the implementation to the input target for coverage
     * @param outputFlags   the output target for the flags
     * @param outputCover   the output target for coverage or NULL to discard
     */
    virtual void deserializeFlags(QDataStream &stream,
                                  SmootherChainCoreEngineInterface *engine,
                                  SmootherChainTargetFlags *&inputFlags,
                                  SmootherChainTarget *&inputCover,
                                  SmootherChainTargetFlags *outputFlags,
                                  SmootherChainTarget *outputCover = NULL) = 0;

    /**
     * Calculate the requested inputs from a given set of known inputs.
     * 
     * @param inputs    the known inputs
     * @return          the requested inputs
     */
    static Data::SequenceName::Set requestedInputs(const Data::SequenceName::Set &inputs);

    /**
     * Calculate the expected outputs from a given set of known inputs.
     * 
     * @param inputs    the known inputs
     * @return          the expected outputs
     */
    static Data::SequenceName::Set predictedOutputs(const Data::SequenceName::Set &inputs);
};

}
}

#endif
