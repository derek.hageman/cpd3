/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGBASELINE_H
#define CPD3SMOOTHINGBASELINE_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDataStream>
#include <QList>
#include <QDebug>

#include "smoothing/smoothing.hxx"
#include "core/number.hxx"
#include "core/component.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "datacore/valueoptionparse.hxx"
#include "smoothing/digitalfilter.hxx"

namespace CPD3 {
namespace Smoothing {

class BaselineSmoother;

CPD3SMOOTHING_EXPORT QDataStream &operator<<(QDataStream &stream, const BaselineSmoother *baseline);

CPD3SMOOTHING_EXPORT QDataStream &operator>>(QDataStream &stream, BaselineSmoother *&baseline);

CPD3SMOOTHING_EXPORT QDebug operator<<(QDebug stream, const BaselineSmoother *baseline);

CPD3SMOOTHING_EXPORT QDataStream &operator<<(QDataStream &stream,
                                             const std::unique_ptr<BaselineSmoother> &baseline);

CPD3SMOOTHING_EXPORT QDataStream &operator>>(QDataStream &stream,
                                             std::unique_ptr<BaselineSmoother> &baseline);

CPD3SMOOTHING_EXPORT QDebug operator<<(QDebug stream,
                                       const std::unique_ptr<BaselineSmoother> &baseline);

/**
 * The base class for baseline smoothers.
 */
class CPD3SMOOTHING_EXPORT BaselineSmoother {
public:
    virtual ~BaselineSmoother();

    /**
     * Add a value to the smoother.
     * 
     * @param value the value to add
     * @param start the start time the value is effective at
     * @param end   the end time the value is effective at
     */
    virtual void
            add(double value, double start = FP::undefined(), double end = FP::undefined()) = 0;

    /**
     * Reset the smoother.  This clears all points being considered.
     */
    virtual void reset() = 0;

    /**
     * Test if the smoother is ready.  Smoothers that are not ready are not
     * available for stability tests.
     * 
     * @return true if the smoother is ready
     */
    virtual bool ready() const = 0;

    /**
     * Test if the smoother is stable.
     * 
     * @return true if the smoother is stable
     */
    virtual bool stable() const = 0;

    /**
     * Test if the previously inserted point is considered a spike.
     * 
     * @return true if the last point added was a spike
     */
    virtual bool spike() const = 0;

    /**
     * Get the smoothed baseline value.
     * 
     * @return the baseline value
     */
    virtual double value() const = 0;

    /**
     * Get the smoothed baseline stability number (e.x. RSD).
     * 
     * @return the baseline stability value
     */
    virtual double stabilityFactor() const;

    /**
     * Create a copy of the smoother including all polymorphism.
     * 
     * @return a copy of the smoother, owned by the caller
     */
    virtual BaselineSmoother *clone() const = 0;

    /**
     * Return a value describing the state of the baseline smoother.  This
     * can be used by acquisition components for message metadata.
     * 
     * @return a value describing the state
     */
    virtual Data::Variant::Root describeState() const;

    /**
     * Get the start time needed to fully spin up the baseline given an
     * initial start time.
     * 
     * @param initial   the initial start time
     * @return          the time to begin so that the smoother is running by the initial time
     */
    virtual double spinupTime(double initial) const;

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator<<(QDataStream &stream, const BaselineSmoother *baseline);

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator>>(QDataStream &stream, BaselineSmoother *&baseline);

    friend CPD3SMOOTHING_EXPORT QDebug operator<<(QDebug stream, const BaselineSmoother *baseline);

    static BaselineSmoother *fromConfiguration(const Data::Variant::Read &defaultConfig,
                                               Data::SequenceSegment::Transfer &config,
                                               const Data::SequenceName &unit,
                                               const QString &path = QString(),
                                               double start = FP::undefined(),
                                               double end = FP::undefined());

    static BaselineSmoother *fromConfiguration(const Data::Variant::Read &defaultConfig,
                                               const Data::ValueSegment::Transfer &config,
                                               const QString &path = QString(),
                                               double start = FP::undefined(),
                                               double end = FP::undefined());

    static bool inRelativeBand(double smoothed, double check, double band);

protected:
    /**
     * Serialize the smoother into the given data stream.
     * 
     * @param stream the target serialization stream
     */
    virtual void serialize(QDataStream &stream) const = 0;

    /**
     * Output the smoother to a log stream.
     * 
     * @param stream the target stream
     */
    virtual void printLog(QDebug &stream) const;
};

/**
 * A baseline that does nothing and is never valid.
 */
class CPD3SMOOTHING_EXPORT BaselineInvalid : public BaselineSmoother {
public:
    BaselineInvalid();

    virtual ~BaselineInvalid();

    virtual void add(double value, double start = FP::undefined(), double end = FP::undefined());

    virtual void reset();

    virtual bool ready() const;

    virtual bool stable() const;

    virtual bool spike() const;

    virtual double value() const;

    virtual Data::Variant::Root describeState() const;

    virtual BaselineSmoother *clone() const;

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator>>(QDataStream &stream, BaselineSmoother *&baseline);

protected:
    BaselineInvalid(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

/**
 * A baseline consisting of the last valid point inserted.  It is always
 * stable and never an outlier if it has a single point.
 */
class CPD3SMOOTHING_EXPORT BaselineSinglePoint : public BaselineSmoother {
    double latestValue;
public:
    BaselineSinglePoint();

    BaselineSinglePoint(const BaselineSinglePoint &other);

    virtual ~BaselineSinglePoint();

    virtual void add(double value, double start = FP::undefined(), double end = FP::undefined());

    virtual void reset();

    virtual bool ready() const;

    virtual bool stable() const;

    virtual bool spike() const;

    virtual double value() const;

    virtual Data::Variant::Root describeState() const;

    virtual BaselineSmoother *clone() const;

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator>>(QDataStream &stream, BaselineSmoother *&baseline);

protected:
    BaselineSinglePoint(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

/**
 * A baseline consisting of the last point even if it is invalid.  It is always
 * stable and never an outlier if it has a valid point.
 */
class CPD3SMOOTHING_EXPORT BaselineLatest : public BaselineSmoother {
    double latestValue;
public:
    BaselineLatest();

    BaselineLatest(const BaselineLatest &other);

    virtual ~BaselineLatest();

    virtual void add(double value, double start = FP::undefined(), double end = FP::undefined());

    virtual void reset();

    virtual bool ready() const;

    virtual bool stable() const;

    virtual bool spike() const;

    virtual double value() const;

    virtual Data::Variant::Root describeState() const;

    virtual BaselineSmoother *clone() const;

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator>>(QDataStream &stream, BaselineSmoother *&baseline);

protected:
    BaselineLatest(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

/**
 * A baseline consisting of the the mean of all points inserted.  It is always
 * stable and never an outlier if it has a single point.
 */
class CPD3SMOOTHING_EXPORT BaselineForever : public BaselineSmoother {
    double sum;
    int count;
public:
    BaselineForever();

    BaselineForever(const BaselineForever &other);

    virtual ~BaselineForever();

    virtual void add(double value, double start = FP::undefined(), double end = FP::undefined());

    virtual void reset();

    virtual bool ready() const;

    virtual bool stable() const;

    virtual bool spike() const;

    virtual double value() const;

    virtual Data::Variant::Root describeState() const;

    virtual BaselineSmoother *clone() const;

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator>>(QDataStream &stream, BaselineSmoother *&baseline);

protected:
    BaselineForever(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

/**
 * A baseline consisting of a fixed amount of time with stability determined
 * by the relative standard deviation and spikes by the last point being
 * outside some bound of the mean.
 */
class CPD3SMOOTHING_EXPORT BaselineFixedTime : public BaselineSmoother {
    QVector<double> values;
    QVector<double> startTimes;
    bool haveFullTimes;
    double firstTime;
    double requireTime;
    double maximumTime;
    double stableRSD;
    double spikeBand;
    double waitTime;
public:
    BaselineFixedTime(double setRequireTime = 30.0,
                      double setMaximumTime = 60.0,
                      double setStableRSD = 0.01,
                      double setSpikeBand = 2.0,
                      double setWaitTime = 0.0);

    BaselineFixedTime(const BaselineFixedTime &other);

    virtual ~BaselineFixedTime();

    virtual void add(double value, double start = FP::undefined(), double end = FP::undefined());

    virtual void reset();

    virtual bool ready() const;

    virtual bool stable() const;

    virtual bool spike() const;

    virtual double value() const;

    virtual double stabilityFactor() const;

    virtual double spinupTime(double initial) const;

    virtual Data::Variant::Root describeState() const;

    virtual BaselineSmoother *clone() const;

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator>>(QDataStream &stream, BaselineSmoother *&baseline);

protected:
    BaselineFixedTime(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

/**
 * A baseline consisting of a digital filter.  The smoother is considered 
 * stable if the last added point is within a band of its smoothed value
 * value and a spike is detected if it is outside of a band of it.
 */
class CPD3SMOOTHING_EXPORT BaselineDigitalFilter : public BaselineSmoother {
    DigitalFilter *filter;
    double spikeBand;
    double stableBand;
    double previousInput;
    double previousOutput;
    bool wasSmoothed;
public:
    BaselineDigitalFilter
            (DigitalFilter *setFilter, double setStableBand = 2.0, double setSpikeBand = 2.0);

    BaselineDigitalFilter(const BaselineDigitalFilter &other);

    virtual ~BaselineDigitalFilter();

    virtual void add(double value, double start = FP::undefined(), double end = FP::undefined());

    virtual void reset();

    virtual bool ready() const;

    virtual bool stable() const;

    virtual bool spike() const;

    virtual double value() const;

    virtual double stabilityFactor() const;

    virtual double spinupTime(double initial) const;

    virtual Data::Variant::Root describeState() const;

    virtual BaselineSmoother *clone() const;

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator>>(QDataStream &stream, BaselineSmoother *&baseline);

protected:
    BaselineDigitalFilter(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

class BaselineSmootherOption;

/**
 * A baseline consisting of a composite of other smoothers.
 */
class CPD3SMOOTHING_EXPORT BaselineComposite : public BaselineSmoother {
    struct Entry {
        Entry();

        Entry(BaselineSmoother *setSmoother, double setStart, double setEnd);

        Entry(const Entry &other);

        Entry &operator=(const Entry &other);

        Entry(const Entry &other, double setStart, double setEnd);

        Entry(const Entry &under, const Entry &over, double setStart, double setEnd);

        ~Entry();

        BaselineSmoother *smoother;
        double start;
        double end;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double s)
        { start = s; }

        inline void setEnd(double s)
        { end = s; }
    };

    QList<Entry> smoothers;
    QList<Entry> allSmoothers;
    bool validTarget;

    void overlay(const Data::Variant::Read &config,
                 const Data::Variant::Read &defaultConfig,
                 double start,
                 double end);

    static BaselineSmoother *reduce(const BaselineComposite &in, double start, double end);

public:
    BaselineComposite();

    BaselineComposite(const BaselineComposite &other);

    virtual ~BaselineComposite();

    void overlay(BaselineSmoother *smoother, double start, double end);

    void clear();

    virtual void add(double value, double start = FP::undefined(), double end = FP::undefined());

    virtual void reset();

    virtual bool ready() const;

    virtual bool stable() const;

    virtual bool spike() const;

    virtual double value() const;

    virtual double stabilityFactor() const;

    virtual double spinupTime(double initial) const;

    virtual Data::Variant::Root describeState() const;

    virtual BaselineSmoother *clone() const;

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator>>(QDataStream &stream, BaselineSmoother *&baseline);

    friend class BaselineSmoother;

    friend class BaselineSmootherOption;

protected:
    BaselineComposite(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

/** 
 * A component option specifying a baseline smoother.
 */
class CPD3SMOOTHING_EXPORT BaselineSmootherOption
        : public ComponentOptionBase, virtual public Data::ComponentOptionValueParsable {
Q_OBJECT

    Q_INTERFACES(CPD3::Data::ComponentOptionValueParsable)

    BaselineComposite smoother;
    bool stability;
    bool spike;
public:
    BaselineSmootherOption(const QString &argumentName,
                           const QString &displayName,
                           const QString &description,
                           const QString &defaultBehavior,
                           int sortPriority = 0);

    BaselineSmootherOption(const BaselineSmootherOption &other);

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    BaselineSmoother *getSmoother() const;

    void setDefault(BaselineSmoother *smoother);

    void overlay(BaselineSmoother *smoother, double start, double end);

    void clear();

    void setStabilityDetection(bool s);

    void setSpikeDetection(bool s);

    bool getStabilityDetection() const;

    bool getSpikeDetection() const;

    virtual bool parseFromValue(const Data::Variant::Read &value);
};

}
}

#endif
