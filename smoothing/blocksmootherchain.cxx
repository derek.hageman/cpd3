/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include "core/threadpool.hxx"
#include "smoothing/blocksmootherchain.hxx"

using namespace CPD3::Data;
using namespace CPD3::Smoothing::Internal;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/blocksmootherchain.hxx
 * The base implementation for large buffering block smoothers.
 */

BlockSmootherChain::BlockSmootherChain(DynamicTimeInterval *setGap,
                                       SmootherChainTarget *outputTarget) : output(outputTarget),
                                                                            gap(setGap),
                                                                            previousTime(
                                                                                    FP::undefined()),
                                                                            targetInput(this),
                                                                            maximumBuffer(65536),
                                                                            undefinedMode(Remove),
                                                                            mutex(),
                                                                            runningTask(NULL),
                                                                            processingQueue(),
                                                                            pendingSendEnd(false),
                                                                            pendingAdvanceTime(
                                                                                    FP::undefined()),
                                                                            outputEnded(false),
                                                                            internalWait(),
                                                                            buffer(NULL),
                                                                            size(0),
                                                                            capacity(0)
{
    capacity = 15;
    buffer = (Value *) malloc(capacity * sizeof(Value));
    Q_ASSERT(buffer);
}

BlockSmootherChain::BlockSmootherChain(QDataStream &stream, SmootherChainTarget *outputTarget)
        : output(outputTarget),
          gap(NULL),
          previousTime(FP::undefined()),
          targetInput(this),
          maximumBuffer(0),
          undefinedMode(DumpBuffer),
          mutex(),
          runningTask(NULL),
          processingQueue(),
          pendingSendEnd(false),
          pendingAdvanceTime(FP::undefined()),
          outputEnded(false),
          internalWait(),
          buffer(NULL),
          size(0),
          capacity(0)
{
    quint32 n;
    stream >> gap >> previousTime >> outputEnded;
    stream >> n;
    maximumBuffer = n;
    quint8 m;
    stream >> m;
    undefinedMode = (UndefinedMode) m;
    stream >> n;
    if (n < 15) {
        capacity = 15;
    } else {
        capacity = n;
    }
    size = n;

    buffer = (Value *) malloc(capacity * sizeof(Value));
    Q_ASSERT(buffer);

    for (Value *v = buffer, *end = buffer + size; v != end; ++v) {
        stream >> v->value >> v->start >> v->end;
    }
}

void BlockSmootherChain::serialize(QDataStream &stream) const
{
    Q_ASSERT(runningTask == NULL);
    Q_ASSERT(processingQueue.isEmpty());

    stream << gap << previousTime << outputEnded;
    stream << (quint32) maximumBuffer;
    stream << (quint8) undefinedMode;
    stream << (quint32) size;
    for (const Value *v = buffer, *end = buffer + size; v != end; ++v) {
        stream << v->value << v->start << v->end;
    }
}

BlockSmootherChain::Handler::Handler()
{ }

BlockSmootherChain::Handler::~Handler()
{ }

static bool findLastEmit(double a, const BlockSmootherChain::Value &b)
{
    if (!FP::defined(b.end))
        return true;
    return a < b.end;
}

namespace Internal {
class BlockSmootherChainTask : public QRunnable {
    BlockSmootherChain *chain;
    BlockSmootherChain::Handler *handler;
    BlockSmootherChain::Value *buffer;
    size_t size;
    bool finishChain;
    size_t emitTotal;
public:
    BlockSmootherChainTask(BlockSmootherChain *bsc,
                           BlockSmootherChain::Handler *h,
                           BlockSmootherChain::Value *b,
                           size_t s,
                           size_t countEmit,
                           bool finish = false) : chain(bsc),
                                                  handler(h),
                                                  buffer(b),
                                                  size(s),
                                                  finishChain(finish),
                                                  emitTotal(countEmit)
    {
        Q_ASSERT(handler != NULL);

        setAutoDelete(true);
    }

    virtual ~BlockSmootherChainTask()
    {
        delete handler;
        free(buffer);
    }

    virtual void run()
    {
        if (buffer != NULL && size != 0) {
            double realEnd = (buffer + emitTotal - 1)->end;
            if (handler->process(buffer, size)) {
                if (buffer == NULL) {
                    emitTotal = 0;
                } else if (!FP::defined(realEnd)) {
                    emitTotal = size;
                } else {
                    const BlockSmootherChain::Value
                            *end = std::upper_bound(buffer, buffer + size, realEnd, findLastEmit);
                    emitTotal = end - buffer;
                }
            }
        }

        if (buffer != NULL && emitTotal != 0) {
            SmootherChainTarget *target = chain->output;
            for (const BlockSmootherChain::Value *add = buffer, *end = buffer + emitTotal;
                    add != end;
                    ++add) {
                target->incomingData(add->start, add->end, add->value);
            }
        }

        chain->taskFinished(finishChain);
    }
};
}

void BlockSmootherChain::taskFinished(bool finisher)
{
    std::lock_guard<std::mutex> lock(mutex);
    Q_ASSERT(!outputEnded);

    if (processingQueue.isEmpty()) {
        runningTask = NULL;
        if (finisher || pendingSendEnd) {
            output->endData();
            output = NULL;
            outputEnded = true;
        } else if (FP::defined(pendingAdvanceTime)) {
            output->incomingAdvance(pendingAdvanceTime);
            pendingAdvanceTime = FP::undefined();
        }

        internalWait.notify_all();
    } else {
        runningTask = processingQueue.takeFirst();
        ThreadPool::system()->run(runningTask);
    }
}

BlockSmootherChain::~BlockSmootherChain()
{
    std::unique_lock<std::mutex> lock(mutex);
    qDeleteAll(processingQueue);
    processingQueue.clear();
    while (runningTask != NULL) {
        internalWait.wait(lock);
    }
    lock.unlock();
    if (buffer != NULL)
        free(buffer);

    if (gap != NULL)
        delete gap;
}

void BlockSmootherChain::dumpBuffer(size_t count)
{
    if (size == 0)
        return;
    if (count == 0)
        count = size;
    const Value *endDump = buffer + count;

    Value *bufferCopy = (Value *) malloc(size * sizeof(Value));
    Q_ASSERT(bufferCopy);
    memcpy(bufferCopy, buffer, size * sizeof(Value));

    BlockSmootherChainTask *task = new BlockSmootherChainTask(this, createHandler(bufferCopy->start,
                                                                                  (bufferCopy +
                                                                                          count -
                                                                                          1)->end),
                                                              bufferCopy, size, count);

    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingAdvanceTime = FP::undefined();
        if (runningTask == NULL && !outputEnded) {
            runningTask = task;
            ThreadPool::system()->run(task);
        } else {
            processingQueue.append(task);
        }
    }

    size -= count;
    if (size == 0)
        return;
    memmove(buffer, endDump, size * sizeof(Value));
}

void BlockSmootherChain::handleValue(double start, double end, double value)
{
    if (gap != NULL && FP::defined(previousTime)) {
        double checkGap = gap->apply(start, previousTime);
        if (FP::defined(checkGap) && checkGap < start) {
            dumpBuffer();
        }
    }
    previousTime = end;

    if (!FP::defined(value)) {
        switch (undefinedMode) {
        case DumpBuffer:
            dumpBuffer();
            return;
        case PassThrough:
            break;
        case Remove:
            return;
        }
    }

    if (size >= (size_t) maximumBuffer)
        dumpBuffer((size_t) (maximumBuffer / 2));

    if (size >= capacity) {
        capacity = INTEGER::growAlloc(size + 1);
        buffer = (Value *) realloc(buffer, capacity * sizeof(Value));
        Q_ASSERT(buffer);
    }
    Value *target = buffer + size;
    ++size;
    target->start = start;
    target->end = end;
    target->value = value;
}

void BlockSmootherChain::handleAdvance(double time)
{
    if (gap != NULL && FP::defined(previousTime)) {
        double checkGap = gap->apply(time, previousTime);
        if (FP::defined(checkGap) && checkGap < time) {
            dumpBuffer();
        }
    }

    /* Don't try to queue advances when we might still have to process
     * data. */
    if (size != 0)
        return;

    std::lock_guard<std::mutex> lock(mutex);
    if (runningTask == NULL && !outputEnded) {
        output->incomingAdvance(time);
    } else {
        pendingAdvanceTime = time;
    }
}

void BlockSmootherChain::handleEnd()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (outputEnded)
        return;
    pendingAdvanceTime = FP::undefined();
    if (size != 0) {
        BlockSmootherChainTask *task = new BlockSmootherChainTask(this, createHandler(buffer->start,
                                                                                      (buffer +
                                                                                              size -
                                                                                              1)->end),
                                                                  buffer, size, size, true);
        buffer = NULL;
        size = 0;

        if (runningTask == NULL) {
            runningTask = task;
            ThreadPool::system()->run(task);
        } else {
            processingQueue.append(task);
        }
    } else if (runningTask == NULL) {
        output->endData();
        output = NULL;
        outputEnded = true;
        internalWait.notify_all();
    } else {
        pendingSendEnd = true;
    }
}

BlockSmootherChain::TargetInput::TargetInput(BlockSmootherChain *n) : node(n)
{ }

BlockSmootherChain::TargetInput::~TargetInput()
{ }

void BlockSmootherChain::TargetInput::incomingData(double start, double end, double value)
{ node->handleValue(start, end, value); }

void BlockSmootherChain::TargetInput::incomingAdvance(double time)
{ node->handleAdvance(time); }

void BlockSmootherChain::TargetInput::endData()
{ node->handleEnd(); }

bool BlockSmootherChain::finished()
{
    /* Don't need this in the mutex, since this is an advisory function and
     * this is only ever written true.  That is, the worst case is that
     * the element gets cleaned up a bit later than it could have been. */
    if (!outputEnded)
        return false;
    std::lock_guard<std::mutex> lock(mutex);
    return runningTask == NULL && processingQueue.isEmpty() && outputEnded;
}

void BlockSmootherChain::pause()
{
    /* Wait until there's nothing outstanding.  This also ensures that
     * all advances/ends have been sent. */
    std::unique_lock<std::mutex> lock(mutex);
    while (runningTask != NULL || !processingQueue.isEmpty()) {
        internalWait.wait(lock);
    }
    lock.release();
}

void BlockSmootherChain::resume()
{
    mutex.unlock();
}

void BlockSmootherChain::signalTerminate()
{
    std::lock_guard<std::mutex> lock(mutex);
    qDeleteAll(processingQueue);
    processingQueue.clear();
    /* Can't stop a running task, so just tell it to end when it finishes */
    if (runningTask != NULL) {
        pendingSendEnd = true;
    } else {
        if (output != NULL) {
            output->endData();
            output = NULL;
        }
        outputEnded = true;
        internalWait.notify_all();
    }
}

void BlockSmootherChain::cleanup()
{
    std::unique_lock<std::mutex> lock(mutex);
    while (!outputEnded || runningTask != NULL || !processingQueue.isEmpty()) {
        internalWait.wait(lock);
    }
}

}
}
