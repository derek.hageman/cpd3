/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGREALTIMECHAIN_H
#define CPD3SMOOTHINGREALTIMECHAIN_H

#include "core/first.hxx"

#include <vector>
#include <deque>
#include <memory>
#include <unordered_map>
#include <stdlib.h>
#include <QtGlobal>
#include <QObject>
#include <QDataStream>

#include "smoothing/smoothing.hxx"
#include "smoothing/smoothingchain.hxx"
#include "smoothing/binnedchain.hxx"
#include "smoothing/smoothingengine.hxx"
#include "core/timeutils.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "datacore/stream.hxx"

namespace CPD3 {
namespace Smoothing {

/**
 * A realtime control interface.  This is used by the realtime engine to
 * communicate with the realtime chain elements.  This is only called from
 * times when it would be "safe" to add data.  That is, it is called from
 * the same context as the incoming functions of the chain inputs.
 */
class CPD3SMOOTHING_EXPORT RealtimeController {
public:
    virtual ~RealtimeController();

    /**
     * Change the realtime binning behavior.  This is used when the realtime
     * averaging has changed and the chain needs to adapt.  How the chain
     * handles this is left open as long as it is handled.
     * 
     * @param setUnit   the time binning unit
     * @param setCount  the number of units to bin
     * @param setAlign  if set then bins are aligned to the units
     */
    virtual void setBinning(Time::LogicalTimeUnit setUnit = Time::Minute,
                            int setCount = 1,
                            bool setAlign = true) =
    0;
};

/**
 * The interface provided to a realtime smoother engine
 */
class CPD3SMOOTHING_EXPORT RealtimeEngineInterface
        : public SmootherChainCoreEngineAuxiliaryInterface {
public:
    virtual ~RealtimeEngineInterface();

    /**
     * Add a realtime controller to the engine's dispatch.  This controller
     * remains owned by the current chain and is expected to remain valid
     * until the entire chain can be deleted.
     * 
     * @param controller    the controller to add
     */
    virtual void addRealtimeController(RealtimeController *controller) = 0;

    /**
     * Get the averaging unit.
     * 
     * @return the averaging unit
     */
    virtual Time::LogicalTimeUnit getAveragingUnit() = 0;

    /**
     * Get the averaging number of units.
     * 
     * @return the averaging count
     */
    virtual int getAveragingCount() = 0;

    /**
     * Get if the averaging is aligned.
     * 
     * @return true if the averaging is aligned
     */
    virtual bool getAveragingAlign() = 0;
};

/**
 * The base class for realtime averaging.
 * 
 * @param BinData   the data type used as bin data
 */
template<typename BinData>
class RealtimeBinner : public RealtimeController {
    Time::LogicalTimeUnit unit;
    int count;
    bool align;

    double fixedEnd;
    enum BinControlState {
        Initial = 0, DataAdded, EmittedAndNoNewData, EmittedTerminal
    } binState;

public:
    /**
     * Construct a binner with the given interval and duration.  This
     * takes ownership of the interval and duration.
     * 
     * @param setUnit   the time unit to average on
     * @param setCount  the number of units to average
     * @param setAlign  true to force alignment
     */
    RealtimeBinner(Time::LogicalTimeUnit setUnit = Time::Minute,
                   int setCount = 1,
                   bool setAlign = true) : unit(setUnit),
                                           count(setCount),
                                           align(setAlign),
                                           fixedEnd(FP::undefined()), binState(Initial)
    { }

    /**
     * Restore the realtime binner from saved state.
     * 
     * @param stream    the stream to read state from
     */
    RealtimeBinner(QDataStream &stream) : unit(Time::Minute),
                                          count(1),
                                          align(true),
                                          fixedEnd(FP::undefined()), binState(Initial)
    {
        readSerial(stream);
    }

    virtual ~RealtimeBinner() = default;

    /**
     * Change the current binning.
     * 
     * @param setUnit   the time unit to average on
     * @param setCount  the number of units to average
     * @param setAlign  true to force alignment
     */
    virtual void setBinning(Time::LogicalTimeUnit setUnit = Time::Minute,
                            int setCount = 1,
                            bool setAlign = true)
    {
        if (setUnit == unit && setCount == count && ((setAlign && align) || !setAlign)) {
            align = setAlign;
            return;
        }
        unit = setUnit;
        count = setCount;
        align = setAlign;
        fixedEnd = FP::undefined();
    }


    /**
     * Save the state to the given stream.
     * 
     * @param stream    the target stream
     */
    void writeSerial(QDataStream &stream) const
    {
        stream << static_cast<quint8>(unit) << static_cast<quint32>(count) << align << fixedEnd
               << static_cast<quint8>(binState);
    }

    /**
     * Save the state from the given stream.
     * 
     * @param stream    the source stream
     */
    void readSerial(QDataStream &stream)
    {
        quint8 ui;
        stream >> ui;
        unit = (Time::LogicalTimeUnit) ui;
        quint32 nc;
        stream >> nc;
        count = nc;
        stream >> align >> fixedEnd;
        stream >> ui;
        binState = static_cast<BinControlState>(ui);
    }


protected:

    /**
     * Add the data to the end of the current bin.  This is only called after
     * when all the old values have been purged, so it can also emit running 
     * values.
     * 
     * @param purgeBefore   the time to remove all data before
     * @param time          the start time of the data
     * @param data          the data being added
     */
    virtual void addToBin(double time, const BinData &data) = 0;

    /**
     * Called when a bin total has been completed.  This will be called
     * after purging.
     * 
     * @param time      the emission time
     * @return          true if anything was emitted and would need to be terminated
     */
    virtual bool emitTotal(double time) = 0;

    /**
     * Called when a total has been completed to terminate an average.
     * This is used to generate a final invalidation when values go
     * away (e.x. a size switch).
     *
     * @param time      the emission time
     */
    virtual void emitTerminal(double time)
    {
        purgeBefore(time);
        emitTotal(time);
    }

    /**
     * Called to purge all data before or equal to the given time.
     *
     * @param time  the time to purge data before
     */
    virtual void purgeBefore(double time) = 0;

    /**
     * Called to purge data in unaveraged mode.
     * <br>
     * By default this simples does purgeBefore(time)
     *
     * @param time  the time to purge data before
     */
    virtual void purgeUnaveraged(double time)
    { purgeBefore(time); }

    /**
     * Add an incoming value.
     * 
     * @param time      the incoming time
     * @param data      the data to add
     */
    void addIncomingValue(double time, BinData data)
    {
        if (count <= 0) {
            purgeUnaveraged(time);
            addToBin(time, data);
            if (emitTotal(time))
                binState = EmittedAndNoNewData;
            else
                binState = EmittedTerminal;
            return;
        }

        Q_ASSERT(FP::defined(time));

        if (!FP::defined(fixedEnd)) {
            double startTime;
            if (align) {
                startTime = Time::floor(time, unit, count);
            } else {
                startTime = time;
            }
            fixedEnd = Time::logical(startTime, unit, count, align, true);
            addToBin(time, data);
            binState = DataAdded;
            return;
        }

        if (time >= fixedEnd) {
            switch (binState) {
            case Initial:
                break;
            case EmittedTerminal:
                break;
            case EmittedAndNoNewData:
                emitTerminal(fixedEnd);
                binState = EmittedTerminal;
                break;
            case DataAdded:
                if (emitTotal(fixedEnd))
                    binState = EmittedAndNoNewData;
                else
                    binState = EmittedTerminal;
                break;
            }
            for (int t = 0;; t++) {
                fixedEnd = Time::logical(fixedEnd, unit, count, align, true);
                if (fixedEnd > time)
                    break;
                if (t > 1000) {
                    fixedEnd = Time::logical(time, unit, count, align, true);
                    break;
                }
            }
        }

        purgeBefore(Time::logical(time, unit, count, false, false, -1));
        addToBin(time, data);
        binState = DataAdded;
    }

    /**
     * Add an incoming advance.
     */
    void addIncomingAdvance(double time)
    {
        if (count <= 0)
            return;
        if (!FP::defined(fixedEnd))
            return;

        if (time >= fixedEnd) {
            switch (binState) {
            case Initial:
                break;
            case EmittedTerminal:
                break;
            case EmittedAndNoNewData:
                emitTerminal(fixedEnd);
                binState = EmittedTerminal;
                break;
            case DataAdded:
                if (emitTotal(fixedEnd))
                    binState = EmittedAndNoNewData;
                else
                    binState = EmittedTerminal;
                break;
            }
            for (int t = 0;; t++) {
                fixedEnd = Time::logical(fixedEnd, unit, count, align, true);
                if (fixedEnd > time)
                    break;
                if (t > 1000) {
                    fixedEnd = Time::logical(time, unit, count, align, true);
                    break;
                }
            }
        }
    }
};

/**
 * Implements a segment processor for realtime data.  This generates segments
 * of active values for each unique start time.  The end times are left as
 * infinite.
 * 
 * @tparam N    the number of input streams
 */
template<std::size_t N>
class RealtimeSegmentProcessor {
    static_assert(N > 0, "need at least one merge stream");

    struct InputValue {
        double time;
        double value;

        inline InputValue() = default;

        inline InputValue(double time, double value) : time(time), value(value)
        { }
    };

    std::deque<InputValue> values[N];
    double advanceTimes[N];
    double existing[N];
    bool criticalStream[N];
    bool ended[N];
    double lastAdvanceEmitted;

    void appendValue(std::size_t stream, double time, double value)
    {
        values[stream].emplace_back(time, value);
    }

    void removeFirst(std::size_t stream, std::size_t count)
    {
        if (count <= 0)
            return;
        Q_ASSERT(count <= values[stream].size());
        values[stream].erase(values[stream].begin(), values[stream].begin() + count);
    }

    bool emitAllPossible()
    {
        if (!ended[0] && !FP::defined(advanceTimes[0])) {
            criticalStream[0] = true;
            if (N > 1) {
                std::fill_n(&criticalStream[1], N - 1, false);
            }
            return false;
        }

        std::size_t offsets[N];
        std::fill_n(&offsets[0], N, false);
        criticalStream[0] = true;
        if (N > 1) {
            std::fill_n(&criticalStream[1], N - 1, false);
        }

        /* Find the latest possible time we can emit: the earliest advanced
         * stream time */
        double firstPossibleAdvance = advanceTimes[0];
        bool allEnded = ended[0];
        for (std::size_t i = 1; i < N; i++) {
            if (ended[i])
                continue;
            if (!FP::defined(advanceTimes[i])) {
                firstPossibleAdvance = FP::undefined();
                std::fill_n(&criticalStream[0], i, false);
                /* Even if others are undefined we'll always need this one
                 * anyway, so only mark it */
                criticalStream[i] = true;
                return false;
            }
            if (allEnded || firstPossibleAdvance > advanceTimes[i]) {
                firstPossibleAdvance = advanceTimes[i];
                std::fill_n(&criticalStream[0], i, false);
                criticalStream[i] = true;
            } else if (firstPossibleAdvance == advanceTimes[i]) {
                criticalStream[i] = true;
            }
            allEnded = false;
        }

        /* Successively find the earliest streams and emit them, filling the
         * values not present with their last known values. */
        double outputValues[N];
        bool take[N];
        double firstRealStart;
        for (;;) {
            firstRealStart = FP::undefined();
            std::fill_n(&take[0], N, false);
            for (std::size_t i = 0; i < N; i++) {
                /* Exhausted the stream, but should be marked as critical
                 * above so don't need to repeat that */
                if (values[i].size() <= offsets[i])
                    continue;

                const auto &v = values[i][offsets[i]];
                /* Don't add this one if it's ahead of the possible time unless
                 * all streams have ended */
                if (!allEnded && v.time > firstPossibleAdvance)
                    continue;

                if (!FP::defined(firstRealStart)) {
                    firstRealStart = v.time;
                    take[i] = true;
                } else if (v.time < firstRealStart) {
                    firstRealStart = v.time;
                    std::fill_n(&take[0], i, false);
                    take[i] = true;
                } else if (v.time == firstRealStart) {
                    take[i] = true;
                }
            }

            /* No time means we've done all we can do */
            if (!FP::defined(firstRealStart))
                break;

            for (std::size_t i = 0; i < N; i++) {
                if (take[i]) {
                    outputValues[i] = values[i][offsets[i]].value;
                    existing[i] = outputValues[i];
                    offsets[i]++;
                } else {
                    outputValues[i] = existing[i];
                }
            }

            segmentDone(firstRealStart, FP::undefined(), outputValues);
            lastAdvanceEmitted = firstRealStart;
        }

        for (std::size_t i = 0; i < N; i++) {
            removeFirst(i, offsets[i]);
        }

        if (allEnded)
            return true;

        /* If the advance time is ahead of the emit time, then advance to that
         * time without a value */
        if (Range::compareStart(firstPossibleAdvance, lastAdvanceEmitted) > 0) {
            Q_ASSERT(FP::defined(firstPossibleAdvance));
            lastAdvanceEmitted = firstPossibleAdvance;
            allStreamsAdvanced(lastAdvanceEmitted);
        }

        return false;
    }

public:
    RealtimeSegmentProcessor() : lastAdvanceEmitted(FP::undefined())
    {
        std::fill_n(&advanceTimes[0], N, FP::undefined());
        std::fill_n(&existing[0], N, FP::undefined());
        std::fill_n(&ended[0], N, false);
        std::fill_n(&criticalStream[0], N, true);
    }

    RealtimeSegmentProcessor(QDataStream &stream)
    { readSerial(stream); }

    virtual ~RealtimeSegmentProcessor() = default;

    /**
     * Add a segment value.  This will call segmentDone(double, double, 
     * const double[N]) and/or allStreamsAdvanced(double) when the time
     * has advanced.
     * 
     * @param stream    the segment stream to add to
     * @param start     the start time of the value
     * @param end       the end time of the value (un-unsed)
     * @param value     the value to incorporate
     */
    void segmentValue(std::size_t stream, double start, double end, double value)
    {
        Q_UNUSED(end);
        Q_ASSERT(stream >= 0 && stream < N);
        Q_ASSERT(!ended[stream]);
        Q_ASSERT(FP::defined(start));
        appendValue(stream, start, value);
        if (FP::defined(advanceTimes[stream]) && advanceTimes[stream] == start)
            return;
        advanceTimes[stream] = start;
        if (criticalStream[stream])
            emitAllPossible();
    }

    /**
     * Advance the segment processor to the given time.  This indicates to the
     * processor that it will not receive any values before the given time.
     * This will call segmentDone(double, double, const double[N]) and/or 
     * allStreamsAdvanced(double) as needed.
     * 
     * @param stream    the stream being advanced
     * @param time      the time to advance to
     */
    void segmentAdvance(std::size_t stream, double time)
    {
        Q_ASSERT(!ended[stream]);
        Q_ASSERT(FP::defined(time));
        if (FP::defined(advanceTimes[stream]) && advanceTimes[stream] == time)
            return;
        advanceTimes[stream] = time;
        if (criticalStream[stream])
            emitAllPossible();
    }

    /**
     * Notify the segment reader of the end of a stream.  No further values
     * will be received in that stream.  This will call segmentDone(double, 
     * double, const double[N]) and/or allStreamsAdvanced(double) as needed.
     * Returns true when all streams have been ended.
     * 
     * @param stream    the stream being ended
     * @return          true if all streams have ended
     */
    bool segmentEnd(std::size_t stream)
    {
        Q_ASSERT(!ended[stream]);
        ended[stream] = true;

        return emitAllPossible();
    }

    /**
     * Save the state to the given stream.  Note that this does NOT
     * lock the mutex, so it may not be called when any other thread
     * may be accessing the processor.
     * 
     * @param stream    the target stream
     */
    void writeSerial(QDataStream &stream) const
    {
        for (std::size_t idxStream = 0; idxStream < N; idxStream++) {
            Serialize::container(stream, values[idxStream], [&](const InputValue &value) {
                stream << value.time << value.value;
            });
            stream << advanceTimes[idxStream] << ended[idxStream] << existing[idxStream];
        }
        stream << lastAdvanceEmitted;
    }

    /**
     * Save the state from the given stream.  Note that this does NOT
     * lock the mutex, so it may not be called when any other thread
     * may be accessing the processor.
     * 
     * @param stream    the source stream
     */
    void readSerial(QDataStream &stream)
    {
        for (std::size_t idxStream = 0; idxStream < N; idxStream++) {
            Deserialize::container(stream, values[idxStream], [&]() {
                InputValue value;
                stream >> value.time >> value.value;
                return value;
            });
            stream >> advanceTimes[idxStream] >> ended[idxStream] >> existing[idxStream];
            criticalStream[idxStream] = true;
        }
        stream >> lastAdvanceEmitted;
    }

    /**
     * Freeze the processor until it is unfrozen.  This will cause any
     * calls attempting to add values to block but also allows safe
     * serialization.
     */
    void freezeState()
    { }


    /**
     * Unfreeze the processor after a call to freezeState()
     */
    void unfreezeState()
    { }

protected:
    /**
     * Called when a segment is completed.  Values may still be undefined,
     * however.
     * 
     * @param start     the start time of the segment
     * @param end       the end time of the segment
     * @param values    the values of all streams for the segment
     */
    virtual void segmentDone(double start, double end, const double values[N]) = 0;

    /**
     * Called when all streams have been advanced to the given time without
     * a segment being generated.
     * 
     * @param time      the time being advanced to
     */
    virtual void allStreamsAdvanced(double time) = 0;
};

/**
 * A class implementing conventional averages for realtime data.
 */
class CPD3SMOOTHING_EXPORT RealtimeChainConventional
        : public SmootherChainNode, public RealtimeBinner<double> {
    SmootherChainTarget *targetComplete;
    SmootherChainTarget *targetRunning;
    SmootherChainTargetGeneral *targetCompleteStats;
    SmootherChainTargetGeneral *targetRunningStats;

    bool targetFinished;

    class Target : public SmootherChainTarget {
        RealtimeChainConventional *node;
    public:
        Target(RealtimeChainConventional *n);

        virtual ~Target();

        virtual void incomingData(double start, double end, double value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class Target;

    Target target;

    void handleAdvance(double time);

    void dataFinished();

    struct BufferValue {
        double time;
        double value;

        inline double getStart() const
        { return time; }

        inline BufferValue() = default;

        inline BufferValue(double time, double value) : time(time), value(value)
        { }
    };

    std::deque<BufferValue> buffer;

    bool processBuffer(double endTime, SmootherChainTarget *target);

    bool processBufferStats(double time, SmootherChainTargetGeneral *target);

public:
    /**
     * Create a new conventional averager.
     * 
     * @param outputComplete    the output target for the completed averages
     * @param outputRunning     the output target for the running averages
     * @param outputStatsComplete the output target for completed statistics
     * @param outputStatsCompleteUnit the output unit for completed statistics
     * @param outputStatsRunning the output target for running statistics
     * @param outputStatsRunningUnit the output unit for running statistics
     * @param setUnit   the time unit to average on
     * @param setCount  the number of units to average
     * @param setAlign  true to force alignment
     */
    RealtimeChainConventional(Time::LogicalTimeUnit setUnit,
                              int setCount,
                              bool setAlign,
                              SmootherChainTarget *outputComplete,
                              SmootherChainTarget *outputRunning = NULL,
                              SmootherChainTargetGeneral *outputStatsComplete = NULL,
                              SmootherChainTargetGeneral *outputStatsRunning = NULL);

    /**
     * Restore the conventional averager from a saved state.
     * 
     * @param stream            the stream to read state from
     * @param outputComplete    the output target for the completed averages
     * @param outputRunning     the output target for the running averages
     * @param outputStatsComplete the output target for completed statistics
     * @param outputStatsCompleteUnit the output unit for completed statistics
     * @param outputStatsRunning the output target for running statistics
     * @param outputStatsRunningUnit the output unit for running statistics
     */
    RealtimeChainConventional(QDataStream &stream,
                              SmootherChainTarget *outputComplete,
                              SmootherChainTarget *outputRunning = NULL,
                              SmootherChainTargetGeneral *outputStatsComplete = NULL,
                              SmootherChainTargetGeneral *outputStatsRunning = NULL);

    virtual ~RealtimeChainConventional();

    virtual bool finished();

    virtual void serialize(QDataStream &stream) const;

    virtual void pause();

    virtual void resume();

    /**
     * Get the chain target to send values to.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTarget()
    { return &target; }

protected:
    virtual void addToBin(double time, const double &data);

    virtual bool emitTotal(double time);

    virtual void emitTerminal(double time);

    virtual void purgeBefore(double time);
};

/**
 * A class implementing vector statistics for realtime data.
 */
class CPD3SMOOTHING_EXPORT RealtimeChainVectorStatistics
        : public SmootherChainNode,
          public RealtimeBinner<Internal::BinArrayWrapper<2> >,
          protected RealtimeSegmentProcessor<2> {

    typedef Internal::BinArrayWrapper<2> Storage;
    typedef RealtimeBinner<Storage> Binner;
    typedef RealtimeSegmentProcessor<2> Processor;

    SmootherChainTarget *targetComplete;
    SmootherChainTarget *targetRunning;
    SmootherChainTargetGeneral *targetCompleteStats;
    SmootherChainTargetGeneral *targetRunningStats;

    bool targetFinished;

    class TargetValue : public SmootherChainTarget {
        RealtimeChainVectorStatistics *node;
    public:
        TargetValue(RealtimeChainVectorStatistics *n);

        virtual ~TargetValue();

        virtual void incomingData(double start, double end, double value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class TargetValue;

    TargetValue targetValue;

    class TargetVectoredMagnitude : public SmootherChainTarget {
        RealtimeChainVectorStatistics *node;
    public:
        TargetVectoredMagnitude(RealtimeChainVectorStatistics *n);

        virtual ~TargetVectoredMagnitude();

        virtual void incomingData(double start, double end, double value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class TargetVectoredMagnitude;

    TargetVectoredMagnitude targetVectoredMagnitude;

    void streamDone(std::size_t stream);

    struct BufferValue {
        double time;
        double value;
        double vectored;

        inline double getStart() const
        { return time; }

        inline BufferValue() = default;

        inline BufferValue(double time, double value, double vectored) : time(time),
                                                                         value(value),
                                                                         vectored(vectored)
        { }
    };

    std::deque<BufferValue> buffer;

    bool processBuffer(double endTime,
                       SmootherChainTarget *target,
                       SmootherChainTargetGeneral *targetStats);

public:
    /**
     * Create a new conventional averager.
     * 
     * @param outputComplete    the output target for the completed averages
     * @param outputRunning     the output target for the running averages
     * @param outputStatsComplete the output target for completed statistics
     * @param outputStatsCompleteUnit the output unit for completed statistics
     * @param outputStatsRunning the output target for running statistics
     * @param outputStatsRunningUnit the output unit for running statistics
     * @param setUnit   the time unit to average on
     * @param setCount  the number of units to average
     * @param setAlign  true to force alignment
     */
    RealtimeChainVectorStatistics(Time::LogicalTimeUnit setUnit,
                                  int setCount,
                                  bool setAlign,
                                  SmootherChainTarget *outputComplete,
                                  SmootherChainTarget *outputRunning = NULL,
                                  SmootherChainTargetGeneral *outputStatsComplete = NULL,
                                  SmootherChainTargetGeneral *outputStatsRunning = NULL);

    /**
     * Restore the conventional averager from a saved state.
     * 
     * @param stream            the stream to read state from
     * @param outputComplete    the output target for the completed averages
     * @param outputRunning     the output target for the running averages
     * @param outputStatsComplete the output target for completed statistics
     * @param outputStatsCompleteUnit the output unit for completed statistics
     * @param outputStatsRunning the output target for running statistics
     * @param outputStatsRunningUnit the output unit for running statistics
     */
    RealtimeChainVectorStatistics(QDataStream &stream,
                                  SmootherChainTarget *outputComplete,
                                  SmootherChainTarget *outputRunning = NULL,
                                  SmootherChainTargetGeneral *outputStatsComplete = NULL,
                                  SmootherChainTargetGeneral *outputStatsRunning = NULL);

    virtual ~RealtimeChainVectorStatistics();

    virtual bool finished();

    virtual void serialize(QDataStream &stream) const;

    virtual void pause();

    virtual void resume();

    /**
     * Get the target to receive value data.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetValue()
    { return &targetValue; }

    /**
     * Get the target to receive vector averaged magnitude data.  The returned 
     * pointer is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTargetVectoredMagnitude()
    { return &targetVectoredMagnitude; }

protected:
    virtual void addToBin(double time, const Storage &data);

    virtual bool emitTotal(double time);

    virtual void emitTerminal(double time);

    virtual void purgeBefore(double time);

    virtual void segmentDone(double start, double end, const double values[2]);

    virtual void allStreamsAdvanced(double time);
};

/**
 * A class implementing differencing averages for realtime data.
 */
class CPD3SMOOTHING_EXPORT RealtimeChainDifference
        : public SmootherChainNode, public RealtimeBinner<double> {
    SmootherChainTarget *targetCompleteStart;
    SmootherChainTarget *targetCompleteEnd;
    SmootherChainTarget *targetRunningStart;
    SmootherChainTarget *targetRunningEnd;

    bool targetFinished;

    class Target : public SmootherChainTarget {
        RealtimeChainDifference *node;
    public:
        Target(RealtimeChainDifference *n);

        virtual ~Target();

        virtual void incomingData(double start, double end, double value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class Target;

    Target target;

    void handleAdvance(double time);

    void dataFinished();

    struct BufferValue {
        double time;
        double value;

        inline double getStart() const
        { return time; }

        inline BufferValue() = default;

        inline BufferValue(double time, double value) : time(time), value(value)
        { }
    };

    std::deque<BufferValue> buffer;

    bool processBuffer(double endTime,
                       SmootherChainTarget *targetFirst,
                       SmootherChainTarget *targetLast);

public:
    /**
     * Create a new conventional averager.
     * 
     * @param outputCompleteStart   the output target for the completed start values
     * @param outputCompleteEnd     the output target for the completed end values
     * @param outputRunningStart    the output target for the running start values
     * @param outputRunningEnd      the output target for the running end values
     * @param setUnit   the time unit to average on
     * @param setCount  the number of units to average
     * @param setAlign  true to force alignment
     */
    RealtimeChainDifference(Time::LogicalTimeUnit setUnit,
                            int setCount,
                            bool setAlign,
                            SmootherChainTarget *outputCompleteStart,
                            SmootherChainTarget *outputCompleteEnd = NULL,
                            SmootherChainTarget *outputRunningStart = NULL,
                            SmootherChainTarget *outputRunningEnd = NULL);

    /**
     * Restore the conventional averager from a saved state.
     * 
     * @param stream                the stream to read state from
     * @param outputCompleteStart   the output target for the completed start values
     * @param outputCompleteEnd     the output target for the completed end values
     * @param outputRunningStart    the output target for the running start values
     * @param outputRunningEnd      the output target for the running end values
     */
    RealtimeChainDifference(QDataStream &stream,
                            SmootherChainTarget *outputCompleteStart,
                            SmootherChainTarget *outputCompleteEnd = NULL,
                            SmootherChainTarget *outputRunningStart = NULL,
                            SmootherChainTarget *outputRunningEnd = NULL);

    virtual ~RealtimeChainDifference();

    virtual bool finished();

    virtual void serialize(QDataStream &stream) const;

    virtual void pause();

    virtual void resume();

    /**
     * Get the chain target to send values to.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTarget *getTarget()
    { return &target; }

protected:
    virtual void addToBin(double time, const double &data);

    virtual bool emitTotal(double time);

    virtual void emitTerminal(double time);

    virtual void purgeBefore(double time);

    virtual void purgeUnaveraged(double time);
};

/**
 * A class implementing flags merging for realtime data.
 */
class CPD3SMOOTHING_EXPORT RealtimeChainFlags
        : public SmootherChainNode, public RealtimeBinner<Data::Variant::Flags> {
    SmootherChainTargetFlags *targetComplete;
    SmootherChainTargetFlags *targetRunning;

    bool targetFinished;

    class Target : public SmootherChainTargetFlags {
        RealtimeChainFlags *node;
    public:
        Target(RealtimeChainFlags *n);

        virtual ~Target();

        virtual void incomingData(double start, double end, const Data::Variant::Flags &value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class Target;

    Target target;

    struct BufferValue {
        double time;
        Data::Variant::Flags value;

        inline BufferValue(double time, const Data::Variant::Flags &value) : time(time),
                                                                             value(value)
        { }

        inline BufferValue() = default;

        inline double getStart() const
        { return time; }
    };

    std::deque<BufferValue> buffer;

    void handleAdvance(double time);

    void dataFinished();

    bool processBuffer(double time, SmootherChainTargetFlags *target);

public:
    /**
     * Create a new flags merging chain element.
     * 
     * @param outputComplete        the output target for completed bins
     * @param outputRunning         the output target for running bins
     * @param setUnit               the time unit to average on
     * @param setCount              the number of units to average
     * @param setAlign              true to force alignment
     */
    RealtimeChainFlags(Time::LogicalTimeUnit setUnit,
                       int setCount,
                       bool setAlign,
                       SmootherChainTargetFlags *outputComplete,
                       SmootherChainTargetFlags *outputRunning = NULL);

    /**
     * Create a new flags merging chain element from saved state.
     * 
     * @param stream                the stream to read state from
     * @param outputComplete        the output target for completed bins
     * @param outputRunning         the output target for running bins
     */
    RealtimeChainFlags(QDataStream &stream,
                       SmootherChainTargetFlags *outputComplete,
                       SmootherChainTargetFlags *outputRunning = NULL);

    virtual ~RealtimeChainFlags();

    /**
     * Get the chain target to send values to.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @return a chain target
     */
    inline SmootherChainTargetFlags *getTarget()
    { return &target; }

    virtual bool finished();

    virtual void serialize(QDataStream &stream) const;

    virtual void pause();

    virtual void resume();

protected:
    virtual void addToBin(double time, const Data::Variant::Flags &data);

    virtual bool emitTotal(double time);

    virtual void emitTerminal(double time);

    virtual void purgeBefore(double time);
};

/**
 * A smoother chain core for realtime data.
 */
class CPD3SMOOTHING_EXPORT RealtimeChainCore : public SmootherChainCore {
    bool enableStatistics;
public:
    /**
     * Create a new realtime smoothing core.
     * 
     * @param setEnableStatistics enable statistics variable generation
     */
    RealtimeChainCore(bool setEnableStatistics = true);

    virtual ~RealtimeChainCore();

    virtual void createSmoother(SmootherChainCoreEngineInterface *engine,
                                SmootherChainCore::Type type);

    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainCore::Type type);

    virtual SmootherChainCore::HandlerMode smootherHandler(SmootherChainCore::Type type);

    virtual bool flagsHandler();

    virtual void createFlags(SmootherChainCoreEngineInterface *engine);

    virtual void deserializeFlags(QDataStream &stream, SmootherChainCoreEngineInterface *engine);

    virtual bool generatedOutput(const Data::SequenceName &unit);

    virtual Data::Variant::Root getProcessingMetadata(double time) const;

    virtual QSet<double> getProcessingMetadataBreaks() const;
};

/**
 * The engine that processes realtime data.  This accepts data in any
 * order (as it sets the times to the time of processing).
 */
class CPD3SMOOTHING_EXPORT RealtimeEngine : public SmoothingEngine {
    bool enableStatistics;

    Time::LogicalTimeUnit averageUnit;
    int averageCount;
    bool averageAlign;

    double lastAdvanceTime;
    Data::SinkMultiplexer::Sink *instantIngress;

    class AuxiliaryChainController;

    class EngineInterface;

    friend class EngineInterface;

    friend class AuxiliaryChainController;

    class EngineInterface
            : public SmoothingEngine::EngineInterface, public virtual RealtimeEngineInterface {
        RealtimeEngine *parent;
        AuxiliaryChainController *chainController;
    public:
        EngineInterface(RealtimeEngine *engine);

        virtual ~EngineInterface();

        virtual void addRealtimeController(RealtimeController *controller);

        virtual Time::LogicalTimeUnit getAveragingUnit();

        virtual int getAveragingCount();

        virtual bool getAveragingAlign();

        virtual SmootherChainCoreEngineAuxiliaryInterface *getAuxiliaryInterface();
    };

    class AuxiliaryChainController : public SmoothingEngine::AuxiliaryChainController {
        std::shared_ptr<
                std::unordered_map<AuxiliaryChainController *, std::vector<RealtimeController *>>>
                chainRealtimeControllers;
    public:
        AuxiliaryChainController(RealtimeEngine *engine);

        virtual ~AuxiliaryChainController();
    };

    std::shared_ptr<
            std::unordered_map<AuxiliaryChainController *, std::vector<RealtimeController *>>>
            chainRealtimeControllers;

    class DispatchMetadata : public SmoothingEngine::DispatchMetadata {
        RealtimeEngine *parent;
        bool haveHadValue;
        Data::ValueSegment lastValue;
        Data::SinkMultiplexer::Sink *outputTarget;
        Data::SequenceName unitBoxcar;
    public:
        DispatchMetadata(RealtimeEngine *engine, const Data::SequenceName &setUnit,
                         Data::SinkMultiplexer::Sink *target);

        virtual ~DispatchMetadata();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);
    };

    class DispatchData : public SmoothingEngine::DispatchData {
        bool haveHadValue;
        Data::ValueSegment lastValue;
    public:
        DispatchData(RealtimeEngine *engine, const Data::SequenceName &setUnit);

        virtual ~DispatchData();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);
    };

public:
    /**
     * Create a new realtime smoothing engine.
     * 
     * @param setEnableStatistics enable statistics variable generation
     */
    RealtimeEngine(bool setEnableStatistics = true);

    virtual ~RealtimeEngine();

    virtual void deserialize(QDataStream &stream);

    virtual void serialize(QDataStream &stream);

    void setAveraging(Time::LogicalTimeUnit setUnit = Time::Minute,
                      int setCount = 1,
                      bool setAlign = true);

protected:
    virtual bool canProcess(IncomingBuffer::const_iterator incomingBegin,
                            IncomingBuffer::const_iterator incomingEnd,
                            double advanceTime);

    virtual void startProcessing(IncomingBuffer::iterator incomingBegin,
                                 IncomingBuffer::iterator incomingEnd,
                                 double &advanceTime,
                                 double &holdbackTime);

    virtual void auxiliaryEndAfter();

    virtual std::unique_ptr<SmoothingEngine::EngineInterface> createEngineInterface();

    virtual Data::SinkMultiplexer::Sink *createDataIngress(const Data::SequenceName &unit);

    virtual std::unique_ptr<
            SmoothingEngine::DispatchTarget> createMetaDispatch(const Data::SequenceName &unit,
                                                                DispatchTarget *underlay);

    virtual std::unique_ptr<
            SmoothingEngine::DispatchTarget> createDataDispatch(const Data::SequenceName &unit,
                                                                DispatchTarget *underlay,
                                                                DispatchTarget *meta);
};

}
}

#endif
