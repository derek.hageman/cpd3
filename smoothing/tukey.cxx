/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "smoothing/tukey.hxx"

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/tukey.hxx
 * Tukey smoother base implementations.
 */

double Tukey::median3Point(double vm, double v, double vp)
{
    Q_ASSERT(FP::defined(vm));
    Q_ASSERT(FP::defined(v));
    Q_ASSERT(FP::defined(vp));

    /* 1st the the smallest, median is the smaller of the 2nd two */
    if (v < vm && v < vp) {
        if (vm < vp)
            return vm;
        else
            return vp;
    } else if (vm < v && vm < vp) {
        /* 2nd */
        if (v < vp)
            return v;
        else
            return vp;
    } else {
        /* 3rd */
        if (v < vm)
            return v;
        else
            return vm;
    }
}

}
}
