/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGEDITINGENGINE_H
#define CPD3SMOOTHINGEDITINGENGINE_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "smoothing/smoothing.hxx"
#include "smoothing/editingchain.hxx"
#include "smoothing/smoothingengine.hxx"
#include "datacore/sequencematch.hxx"

namespace CPD3 {
namespace Smoothing {

/**
 * The base class for all editing engines.
 */
class CPD3SMOOTHING_EXPORT EditingEngineBase : public SmoothingEngine {
protected:
    std::unique_ptr<Data::DynamicTimeInterval> interval;
    std::unique_ptr<Data::DynamicDouble> requiredCover;

    QSet<Data::SequenceName> registeredInputs;
public:
    /**
     * Create an editing engine.
     * 
     * @param setInterval   the interval to average over
     * @param setRequiredCover the minimum coverage allowed in an average
     */
    EditingEngineBase(Data::DynamicTimeInterval *setInterval,
                      Data::DynamicDouble *setRequiredCover = NULL);

    virtual ~EditingEngineBase();

    /**
     * Register the inputs the engine is expected to see.
     * 
     * @param inputs    the expected inputs
     */
    void registerExpectedInputs(const QSet<Data::SequenceName> &inputs);

    void deserialize(QDataStream &stream) override;

    void serialize(QDataStream &stream) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

protected:
    void startProcessing(IncomingBuffer::iterator incomingBegin,
                         IncomingBuffer::iterator incomingEnd,
                         double &advanceTime,
                         double &holdbackTime) override;

    void predictMetadataBounds(double *start, double *end) override;

    virtual SmootherChainCore::HandlerMode handlerMode(const Data::SequenceName &unit,
                                                       SmootherChainCore::Type type) = 0;

    virtual bool handleFlags(const Data::SequenceName &unit) = 0;

    virtual QSet<double> getMetadataBreaks(const Data::SequenceName &unit) = 0;


    virtual void constructChain(const Data::SequenceName &triggeringUnit,
                                EngineInterface *interface,
                                SmootherChainCore::Type type) = 0;

    virtual void deserializeChain(const std::vector<Data::SequenceName> &inputUnits,
                                  QDataStream &stream,
                                  EngineInterface *interface,
                                  SmootherChainCore::Type type) = 0;

    virtual void constructFlagsChain(const Data::SequenceName &triggeringUnit,
                                     EngineInterface *interface) = 0;

    virtual void deserializeFlagsChain(const std::vector<Data::SequenceName> &inputUnits,
                                       QDataStream &stream,
                                       EngineInterface *interface) = 0;
};

/**
 * The smoothing engine implementing the initial averaging and full recalculation 
 * stages of the editing chain.  This assumes that the data has already been 
 * fanned out into contamination filtered data (to the "avg" archive) and the 
 * un-filtered data left as the input archive (generally "raw").
 */
class CPD3SMOOTHING_EXPORT EditingEngineFullCalculate : public EditingEngineBase {
    std::unique_ptr<EditingChainCoreFullCalculate> core;
    Data::SinkMultiplexer::Sink *bypassIngress;
    std::unique_ptr<Data::SequenceMatch::Composite> affectedVariables;

    class DispatchMetadata;

    friend class DispatchMetadata;

    class DispatchMetadata : public SmoothingEngine::DispatchMetadata {
        EditingEngineFullCalculate *parent;
        Data::SequenceName unitCont;

        void incomingSegment(Data::ValueSegment &&segment);

    public:
        DispatchMetadata(EditingEngineFullCalculate *engine, const Data::SequenceName &setUnit,
                         Data::SinkMultiplexer::Sink *target);

        virtual ~DispatchMetadata();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

public:
    /**
     * Create an initial editing engine.
     * 
     * @param setInterval   the interval to average over
     * @param setRequiredCover the minimum coverage allowed in an average
     * @param variables     the variables to calculate (all others are passed through unchanged) for or NULL for all
     */
    EditingEngineFullCalculate(Data::DynamicTimeInterval *setInterval,
                               Data::DynamicDouble *setRequiredCover = NULL,
                               Data::SequenceMatch::Composite *variables = NULL);

    virtual ~EditingEngineFullCalculate();

protected:
    virtual SmootherChainCore::HandlerMode handlerMode(const Data::SequenceName &unit,
                                                       SmootherChainCore::Type type);

    virtual bool handleFlags(const Data::SequenceName &unit);

    virtual QSet<double> getMetadataBreaks(const Data::SequenceName &unit);

    virtual bool generatedOutput(const Data::SequenceName &unit);

    virtual void constructChain(const Data::SequenceName &triggeringUnit,
                                SmoothingEngine::EngineInterface *interface,
                                SmootherChainCore::Type type);

    virtual void deserializeChain(const std::vector<Data::SequenceName> &inputUnits,
                                  QDataStream &stream,
                                  SmoothingEngine::EngineInterface *interface,
                                  SmootherChainCore::Type type);

    virtual void constructFlagsChain(const Data::SequenceName &triggeringUnit,
                                     SmoothingEngine::EngineInterface *interface);

    virtual void deserializeFlagsChain(const std::vector<Data::SequenceName> &inputUnits,
                                       QDataStream &stream,
                                       EngineInterface *interface);

    virtual void auxiliaryEndAfter();

    virtual void auxiliaryAdvance(double advanceTime);

    virtual bool alwaysBypassOutput(const Data::SequenceName &unit);

    virtual std::unique_ptr<
            SmoothingEngine::DispatchTarget> createMetaDispatch(const Data::SequenceName &unit,
                                                                SmoothingEngine::DispatchTarget *underlay);

    virtual std::unique_ptr<
            SmoothingEngine::DispatchTarget> createDataDispatch(const Data::SequenceName &unit,
                                                                SmoothingEngine::DispatchTarget *underlay,
                                                                SmoothingEngine::DispatchTarget *meta);

    using SmoothingEngine::transformMetadata;
};


/**
 * The smoothing engine implementing recalculation of full averages from the
 * continuous segments.  This assumes the presence of the the "cont" archive
 * data stream and will discard any incoming "avg" archive data.  All other
 * data is passed through unchanged.
 */
class CPD3SMOOTHING_EXPORT EditingEngineContinuousRecalculate : public EditingEngineBase {
    std::unique_ptr<EditingChainCoreContinuousRecalculate> core;
    Data::SinkMultiplexer::Sink *bypassIngress;
    std::unique_ptr<Data::SequenceMatch::Composite> affectedVariables;

    class DispatchMetadata : public SmoothingEngine::DispatchMetadata {
        EditingEngineContinuousRecalculate *parent;
        Data::SequenceName unitAvg;
        Data::SequenceName unitCont;

        void incomingSegment(Data::ValueSegment &&segment);

    public:
        DispatchMetadata(EditingEngineContinuousRecalculate *engine,
                         const Data::SequenceName &setUnit, Data::SinkMultiplexer::Sink *target);

        virtual ~DispatchMetadata();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class DispatchMetadata;

    class DispatchData : public SmoothingEngine::DispatchData {
        Data::SequenceName outputUnit;
        Data::SinkMultiplexer::Sink *bypass;
    public:
        DispatchData(EditingEngineContinuousRecalculate *engine, const Data::SequenceName &setUnit);

        virtual ~DispatchData();

        virtual void incomingData(Data::SequenceValue &&value);

    protected:
        virtual Data::SequenceName getOutputUnit();
    };

    /* Have to use a custom one since we handle a rename too: the base
     * unit is set to cont, but flavors need to be output to avg */
    class EngineInterface : public SmoothingEngine::EngineInterface {
    public:
        EngineInterface(EditingEngineContinuousRecalculate *engine);

        virtual ~EngineInterface();

        virtual SmootherChainTarget *addNewOutput(const Data::SequenceName &unit);

        virtual SmootherChainTargetFlags *addNewFlagsOutput(const Data::SequenceName &unit);

        virtual SmootherChainTargetGeneral *addNewGeneralOutput(const Data::SequenceName &unit);
    };

    friend class EngineInterface;

public:
    /**
     * Create a recalculation editing engine.
     * 
     * @param setInterval   the interval to average over
     * @param setRequiredCover the minimum coverage allowed in an average
     * @param variables     the variables to calculate (all others are passed through unchanged) for or NULL for all
     */
    EditingEngineContinuousRecalculate(Data::DynamicTimeInterval *setInterval,
                                       Data::DynamicDouble *setRequiredCover = NULL,
                                       Data::SequenceMatch::Composite *variables = NULL);

    virtual ~EditingEngineContinuousRecalculate();

protected:
    virtual SmootherChainCore::HandlerMode handlerMode(const Data::SequenceName &unit,
                                                       SmootherChainCore::Type type);

    virtual bool handleFlags(const Data::SequenceName &unit);

    virtual QSet<double> getMetadataBreaks(const Data::SequenceName &unit);

    virtual bool generatedOutput(const Data::SequenceName &unit);


    virtual void constructChain(const Data::SequenceName &triggeringUnit,
                                SmoothingEngine::EngineInterface *interface,
                                SmootherChainCore::Type type);

    virtual void deserializeChain(const std::vector<Data::SequenceName> &inputUnits,
                                  QDataStream &stream,
                                  SmoothingEngine::EngineInterface *interface,
                                  SmootherChainCore::Type type);

    virtual void constructFlagsChain(const Data::SequenceName &triggeringUnit,
                                     SmoothingEngine::EngineInterface *interface);

    virtual void deserializeFlagsChain(const std::vector<Data::SequenceName> &inputUnits,
                                       QDataStream &stream,
                                       SmoothingEngine::EngineInterface *interface);

    virtual void auxiliaryEndAfter();

    virtual void auxiliaryAdvance(double advanceTime);

    virtual bool alwaysBypassOutput(const Data::SequenceName &unit);

    virtual std::unique_ptr<
            SmoothingEngine::DispatchTarget> createMetaDispatch(const Data::SequenceName &unit,
                                                                SmoothingEngine::DispatchTarget *underlay);

    virtual std::unique_ptr<
            SmoothingEngine::DispatchTarget> createDataDispatch(const Data::SequenceName &unit,
                                                                SmoothingEngine::DispatchTarget *underlay,
                                                                SmoothingEngine::DispatchTarget *meta);

    virtual std::unique_ptr<SmoothingEngine::EngineInterface> createEngineInterface();

    using SmoothingEngine::transformMetadata;
};


/**
 * The smoothing engine implementing the final calculation for editing.  This
 * assumes that data has already been fanned out into the "avg" and "cont"
 * archives.  In the event of recalculation from high resolution data, the
 * "avg" archive is assumed to have been replaced with the contamination
 * filtered high resolution data.
 */
class CPD3SMOOTHING_EXPORT EditingEngineFinal : public EditingEngineBase {
    std::unique_ptr<EditingChainCoreFinalStatistics> statisticsCore;
    std::unique_ptr<EditingChainCoreContinuousRecalculate> continuousCore;
    std::unique_ptr<EditingChainCoreFinalRecalculate> fullCore;
    std::unique_ptr<SmootherChainCoreDiscard> discardCore;
    Data::SinkMultiplexer::Sink *bypassIngress;
    std::unique_ptr<Data::SequenceMatch::Composite> continuousVariables;
    std::unique_ptr<Data::SequenceMatch::Composite> fullVariables;
    Data::SequenceName::Component cleanArchive;
    Data::SequenceName::Component averageArchive;
    Data::SequenceName::Component cleanArchiveMeta;
    Data::SequenceName::Component averageArchiveMeta;

    class DispatchBypassRename : public SmoothingEngine::DispatchBypass {
        Data::SequenceName unit;
    public:
        DispatchBypassRename(Data::SinkMultiplexer::Sink *t,
                             const Data::SequenceName &setUnit);

        virtual ~DispatchBypassRename();

        virtual void incomingData(Data::SequenceValue &&value);
    };

    class DispatchMetadataDiscard : public SmoothingEngine::DispatchMetadata {
        void incomingSegment(Data::ValueSegment &&segment);

    public:
        DispatchMetadataDiscard(EditingEngineFinal *engine, const Data::SequenceName &setUnit);

        virtual ~DispatchMetadataDiscard();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    class DispatchMetadataRecalculate : public SmoothingEngine::DispatchMetadata {
        EditingEngineFinal *parent;
        SmootherChainCore *core;
        Data::SequenceName unitAverage;

        void incomingSegment(Data::ValueSegment &&segment);

    public:
        DispatchMetadataRecalculate(EditingEngineFinal *engine,
                                    SmootherChainCore *core, const Data::SequenceName &setUnit,
                                    Data::SinkMultiplexer::Sink *target);

        virtual ~DispatchMetadataRecalculate();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class DispatchMetadataRecalculate;

    class DispatchMetadataAverage : public SmoothingEngine::DispatchMetadata {
        EditingEngineFinal *parent;
        Data::SequenceName unitAverage;

        void incomingSegment(Data::ValueSegment &&segment);

    public:
        DispatchMetadataAverage(EditingEngineFinal *engine, const Data::SequenceName &setUnit,
                                Data::SinkMultiplexer::Sink *target);

        virtual ~DispatchMetadataAverage();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    friend class DispatchMetadataStatisticsProcessing;

    class DispatchData : public SmoothingEngine::DispatchData {
        EditingEngineFinal *parent;
        Data::SequenceName outputUnit;
    public:
        DispatchData(EditingEngineFinal *engine, const Data::SequenceName &setUnit);

        virtual ~DispatchData();

    protected:
        virtual Data::SequenceName getOutputUnit();

        virtual bool discardOutput(const Data::SequenceName &unit);
    };

    friend class DispatchData;

    class DispatchDataClean : public DispatchData {
        Data::SequenceName cleanUnit;
        Data::SinkMultiplexer::Sink *bypass;
    public:
        DispatchDataClean(EditingEngineFinal *engine, const Data::SequenceName &setUnit);

        virtual ~DispatchDataClean();

        virtual void incomingData(Data::SequenceValue &&value);
    };

    friend class DispatchDataClean;

    class DispatchDataStatistics : public DispatchData {
    public:
        DispatchDataStatistics(EditingEngineFinal *engine, const Data::SequenceName &setUnit);

        virtual ~DispatchDataStatistics();

    protected:
        virtual bool discardOutput(const Data::SequenceName &unit);
    };

    class DispatchDataDiscard : public DispatchData {
    public:
        DispatchDataDiscard(EditingEngineFinal *engine, const Data::SequenceName &setUnit);

        virtual ~DispatchDataDiscard();

    protected:
        virtual bool discardOutput(const Data::SequenceName &unit);

        virtual bool disableChain();
    };

    /* Have to use a custom one since we handle a rename too: the base
     * unit is set to cont, but flavors need to be output to avg */
    class EngineInterface : public SmoothingEngine::EngineInterface {
        EditingEngineFinal *parent;
    public:
        EngineInterface(EditingEngineFinal *engine);

        virtual ~EngineInterface();

        virtual SmootherChainTarget *addNewOutput(const Data::SequenceName &unit);

        virtual SmootherChainTargetFlags *addNewFlagsOutput(const Data::SequenceName &unit);

        virtual SmootherChainTargetGeneral *addNewGeneralOutput(const Data::SequenceName &unit);
    };

    friend class EngineInterface;

    SmootherChainCore *checkDeserialize(const Data::SequenceName &unit);

    inline bool calculateFull(const Data::SequenceName &unit) const
    { return fullVariables != NULL && fullVariables->matches(unit); }

    inline bool calculateContinuous(const Data::SequenceName &unit) const
    {
        return continuousVariables != NULL && continuousVariables->matches(unit);
    }

public:
    /**
     * Create a final editing engine
     * 
     * @param setInterval   the interval to average over
     * @param setRequiredCover the minimum coverage allowed in an average
     * @param variablesContinuous the variables that need recalculation from the continuous segments or NULL for none
     * @param fullVariables the variables that recalculation from the high resolution data or NULL for none
     * @param setClean      the output name for the clean data
     * @param setAverage    the output name for the averaged data
     */
    EditingEngineFinal(Data::DynamicTimeInterval *setInterval,
                       Data::DynamicDouble *setRequiredCover = NULL,
                       Data::SequenceMatch::Composite *variablesContinuous = NULL,
                       Data::SequenceMatch::Composite *variablesFull = NULL,
                       const Data::SequenceName::Component &setClean = "clean",
                       const Data::SequenceName::Component &setAverage = "avgh");

    virtual ~EditingEngineFinal();

protected:
    virtual SmootherChainCore::HandlerMode handlerMode(const Data::SequenceName &unit,
                                                       SmootherChainCore::Type type);

    virtual bool handleFlags(const Data::SequenceName &unit);

    virtual QSet<double> getMetadataBreaks(const Data::SequenceName &unit);

    virtual bool generatedOutput(const Data::SequenceName &unit);


    virtual void constructChain(const Data::SequenceName &triggeringUnit,
                                SmoothingEngine::EngineInterface *interface,
                                SmootherChainCore::Type type);

    virtual void deserializeChain(const std::vector<Data::SequenceName> &inputUnits,
                                  QDataStream &stream,
                                  SmoothingEngine::EngineInterface *interface,
                                  SmootherChainCore::Type type);

    virtual void constructFlagsChain(const Data::SequenceName &triggeringUnit,
                                     SmoothingEngine::EngineInterface *interface);

    virtual void deserializeFlagsChain(const std::vector<Data::SequenceName> &inputUnits,
                                       QDataStream &stream,
                                       SmoothingEngine::EngineInterface *interface);

    virtual void auxiliaryEndAfter();

    virtual void auxiliaryAdvance(double advanceTime);

    virtual std::unique_ptr<
            SmoothingEngine::DispatchTarget> createMetaDispatch(const Data::SequenceName &unit,
                                                                SmoothingEngine::DispatchTarget *underlay);

    virtual std::unique_ptr<
            SmoothingEngine::DispatchTarget> createDataDispatch(const Data::SequenceName &unit,
                                                                SmoothingEngine::DispatchTarget *underlay,
                                                                SmoothingEngine::DispatchTarget *meta);

    virtual std::unique_ptr<SmoothingEngine::EngineInterface> createEngineInterface();

    using SmoothingEngine::transformMetadata;
};

}
}

#endif
