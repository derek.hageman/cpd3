/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "smoothing/digitalfilterchain.hxx"
#include "core/environment.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/digitialfilterchain.hxx
 * Digital filter based smoothers.
 */

DigitalFilterChainSinglePoleLowPass::DigitalFilterChainSinglePoleLowPass(Data::DynamicTimeInterval *tc,
                                                                         Data::DynamicTimeInterval *setGap,
                                                                         bool resetUndefined,
                                                                         SmootherChainTarget *outputTarget)
        : output(outputTarget),
          targetFinished(false),
          filter(tc, setGap, resetUndefined),
          targetInput(this)
{
    Q_ASSERT(outputTarget != NULL);
}

DigitalFilterChainSinglePoleLowPass::DigitalFilterChainSinglePoleLowPass(QDataStream &stream,
                                                                         SmootherChainTarget *outputTarget)
        : output(outputTarget), targetFinished(false), filter(stream), targetInput(this)
{
    Q_ASSERT(outputTarget != NULL);
    stream >> targetFinished;
}

DigitalFilterChainSinglePoleLowPass::~DigitalFilterChainSinglePoleLowPass()
{ }

bool DigitalFilterChainSinglePoleLowPass::finished()
{ return targetFinished; }

void DigitalFilterChainSinglePoleLowPass::serialize(QDataStream &stream) const
{ stream << filter << targetFinished; }

DigitalFilterChainSinglePoleLowPass::TargetInput::TargetInput(DigitalFilterChainSinglePoleLowPass *n)
        : node(n)
{ }

DigitalFilterChainSinglePoleLowPass::TargetInput::~TargetInput()
{ }

void DigitalFilterChainSinglePoleLowPass::TargetInput::incomingData(double start,
                                                                    double end,
                                                                    double value)
{
    value = node->filter.apply(start, value);
    node->output->incomingData(start, end, value);
}

void DigitalFilterChainSinglePoleLowPass::TargetInput::incomingAdvance(double time)
{ node->output->incomingAdvance(time); }

void DigitalFilterChainSinglePoleLowPass::TargetInput::endData()
{
    node->output->endData();
    node->targetFinished = true;
}

DigitalFilterChainFourPoleLowPass::DigitalFilterChainFourPoleLowPass(Data::DynamicTimeInterval *tc,
                                                                     Data::DynamicTimeInterval *setGap,
                                                                     bool resetUndefined,
                                                                     SmootherChainTarget *outputTarget)
        : output(outputTarget),
          targetFinished(false),
          filter(tc, setGap, resetUndefined),
          targetInput(this)
{
    Q_ASSERT(outputTarget != NULL);
}

DigitalFilterChainFourPoleLowPass::DigitalFilterChainFourPoleLowPass(QDataStream &stream,
                                                                     SmootherChainTarget *outputTarget)
        : output(outputTarget), targetFinished(false), filter(stream), targetInput(this)
{
    Q_ASSERT(outputTarget != NULL);
    stream >> targetFinished;
}

DigitalFilterChainFourPoleLowPass::~DigitalFilterChainFourPoleLowPass()
{ }

bool DigitalFilterChainFourPoleLowPass::finished()
{ return targetFinished; }

void DigitalFilterChainFourPoleLowPass::serialize(QDataStream &stream) const
{ stream << filter << targetFinished; }

DigitalFilterChainFourPoleLowPass::TargetInput::TargetInput(DigitalFilterChainFourPoleLowPass *n)
        : node(n)
{ }

DigitalFilterChainFourPoleLowPass::TargetInput::~TargetInput()
{ }

void DigitalFilterChainFourPoleLowPass::TargetInput::incomingData(double start,
                                                                  double end,
                                                                  double value)
{
    value = node->filter.apply(start, value);
    node->output->incomingData(start, end, value);
}

void DigitalFilterChainFourPoleLowPass::TargetInput::incomingAdvance(double time)
{ node->output->incomingAdvance(time); }

void DigitalFilterChainFourPoleLowPass::TargetInput::endData()
{
    node->output->endData();
    node->targetFinished = true;
}

DigitalFilterChainGeneral::DigitalFilterChainGeneral(const QVector<double> &setA,
                                                     const QVector<double> &setB,
                                                     Data::DynamicTimeInterval *setGap,
                                                     bool resetUndefined,
                                                     SmootherChainTarget *outputTarget) : output(
        outputTarget),
                                                                                          targetFinished(
                                                                                                  false),
                                                                                          filter(setA,
                                                                                                 setB,
                                                                                                 setGap,
                                                                                                 resetUndefined),
                                                                                          targetInput(
                                                                                                  this)
{
    Q_ASSERT(outputTarget != NULL);
}

DigitalFilterChainGeneral::DigitalFilterChainGeneral(QDataStream &stream,
                                                     SmootherChainTarget *outputTarget) : output(
        outputTarget), targetFinished(false), filter(stream), targetInput(this)
{
    Q_ASSERT(outputTarget != NULL);
    stream >> targetFinished;
}

DigitalFilterChainGeneral::~DigitalFilterChainGeneral()
{ }

bool DigitalFilterChainGeneral::finished()
{ return targetFinished; }

void DigitalFilterChainGeneral::serialize(QDataStream &stream) const
{ stream << filter << targetFinished; }

DigitalFilterChainGeneral::TargetInput::TargetInput(DigitalFilterChainGeneral *n) : node(n)
{ }

DigitalFilterChainGeneral::TargetInput::~TargetInput()
{ }

void DigitalFilterChainGeneral::TargetInput::incomingData(double start, double end, double value)
{
    value = node->filter.apply(start, value);
    node->output->incomingData(start, end, value);
}

void DigitalFilterChainGeneral::TargetInput::incomingAdvance(double time)
{ node->output->incomingAdvance(time); }

void DigitalFilterChainGeneral::TargetInput::endData()
{
    node->output->endData();
    node->targetFinished = true;
}


DigitalFilterChainWrapper::DigitalFilterChainWrapper(DigitalFilter *wrapFilter,
                                                     SmootherChainTarget *outputTarget) : output(
        outputTarget), targetFinished(false), filter(wrapFilter), targetInput(this)
{
    Q_ASSERT(outputTarget != NULL);
    Q_ASSERT(wrapFilter != NULL);
}

DigitalFilterChainWrapper::DigitalFilterChainWrapper(QDataStream &stream,
                                                     SmootherChainTarget *outputTarget) : output(
        outputTarget), targetFinished(false), filter(NULL), targetInput(this)
{
    Q_ASSERT(outputTarget != NULL);
    stream >> filter >> targetFinished;
}

DigitalFilterChainWrapper::~DigitalFilterChainWrapper()
{
    delete filter;
}

bool DigitalFilterChainWrapper::finished()
{ return targetFinished; }

void DigitalFilterChainWrapper::serialize(QDataStream &stream) const
{ stream << filter << targetFinished; }

DigitalFilterChainWrapper::TargetInput::TargetInput(DigitalFilterChainWrapper *n) : node(n)
{ }

DigitalFilterChainWrapper::TargetInput::~TargetInput()
{ }

void DigitalFilterChainWrapper::TargetInput::incomingData(double start, double end, double value)
{
    value = node->filter->apply(start, value);
    node->output->incomingData(start, end, value);
}

void DigitalFilterChainWrapper::TargetInput::incomingAdvance(double time)
{ node->output->incomingAdvance(time); }

void DigitalFilterChainWrapper::TargetInput::endData()
{
    node->output->endData();
    node->targetFinished = true;
}


SmootherChainCoreSinglePoleLowPass::SmootherChainCoreSinglePoleLowPass(Data::DynamicTimeInterval *tc,
                                                                       Data::DynamicTimeInterval *setGap,
                                                                       bool resetUndefined)
        : timeConstant(tc), gap(setGap), resetOnUndefined(resetUndefined)
{
    Q_ASSERT(timeConstant != NULL);
}

SmootherChainCoreSinglePoleLowPass::~SmootherChainCoreSinglePoleLowPass()
{
    delete timeConstant;
    if (gap != NULL)
        delete gap;
}

void SmootherChainCoreSinglePoleLowPass::createSmoother(SmootherChainCoreEngineInterface *engine,
                                                        SmootherChainTarget *&input,
                                                        SmootherChainTarget *output)
{
    DigitalFilterChainSinglePoleLowPass *filter =
            new DigitalFilterChainSinglePoleLowPass(timeConstant->clone(),
                                                    gap == NULL ? NULL : gap->clone(),
                                                    resetOnUndefined, output);
    engine->addChainNode(filter);
    input = filter->getTarget();
}

void SmootherChainCoreSinglePoleLowPass::deserializeSmoother(QDataStream &stream,
                                                             SmootherChainCoreEngineInterface *engine,
                                                             SmootherChainTarget *&input,
                                                             SmootherChainTarget *output)
{
    DigitalFilterChainSinglePoleLowPass
            *filter = new DigitalFilterChainSinglePoleLowPass(stream, output);
    engine->addChainNode(filter);
    input = filter->getTarget();
}

Variant::Root SmootherChainCoreSinglePoleLowPass::getProcessingMetadata(double time) const
{
    Variant::Root meta;
    meta.write().hash("Type").setString("SinglePoleLowPass");
    meta.write().hash("Parameters").hash("TimeConstant").setString(timeConstant->describe(time));
    if (gap != NULL) {
        meta.write().hash("Parameters").hash("Gap").setString(gap->describe(time));
    }
    meta.write().hash("Parameters").hash("ResetOnUndefined").setBool(resetOnUndefined);
    return meta;
}

QSet<double> SmootherChainCoreSinglePoleLowPass::getProcessingMetadataBreaks() const
{
    QSet<double> result(timeConstant->getChangedPoints());
    if (gap != NULL)
        result |= gap->getChangedPoints();
    return result;
}


SmootherChainCoreFourPoleLowPass::SmootherChainCoreFourPoleLowPass(Data::DynamicTimeInterval *tc,
                                                                   Data::DynamicTimeInterval *setGap,
                                                                   bool resetUndefined)
        : timeConstant(tc), gap(setGap), resetOnUndefined(resetUndefined)
{
    Q_ASSERT(timeConstant != NULL);
}

SmootherChainCoreFourPoleLowPass::~SmootherChainCoreFourPoleLowPass()
{
    delete timeConstant;
    if (gap != NULL)
        delete gap;
}

void SmootherChainCoreFourPoleLowPass::createSmoother(SmootherChainCoreEngineInterface *engine,
                                                      SmootherChainTarget *&input,
                                                      SmootherChainTarget *output)
{
    DigitalFilterChainFourPoleLowPass *filter =
            new DigitalFilterChainFourPoleLowPass(timeConstant->clone(),
                                                  gap == NULL ? NULL : gap->clone(),
                                                  resetOnUndefined, output);
    engine->addChainNode(filter);
    input = filter->getTarget();
}

void SmootherChainCoreFourPoleLowPass::deserializeSmoother(QDataStream &stream,
                                                           SmootherChainCoreEngineInterface *engine,
                                                           SmootherChainTarget *&input,
                                                           SmootherChainTarget *output)
{
    DigitalFilterChainFourPoleLowPass
            *filter = new DigitalFilterChainFourPoleLowPass(stream, output);
    engine->addChainNode(filter);
    input = filter->getTarget();
}

Variant::Root SmootherChainCoreFourPoleLowPass::getProcessingMetadata(double time) const
{
    Variant::Root meta;
    meta.write().hash("Type").setString("FourPoleLowPass");
    meta.write().hash("Parameters").hash("TimeConstant").setString(timeConstant->describe(time));
    if (gap != NULL) {
        meta.write().hash("Parameters").hash("Gap").setString(gap->describe(time));
    }
    meta.write().hash("Parameters").hash("ResetOnUndefined").setBool(resetOnUndefined);
    return meta;
}

QSet<double> SmootherChainCoreFourPoleLowPass::getProcessingMetadataBreaks() const
{
    QSet<double> result(timeConstant->getChangedPoints());
    if (gap != NULL)
        result |= gap->getChangedPoints();
    return result;
}


SmootherChainCoreGeneralDigitalFilter::SmootherChainCoreGeneralDigitalFilter(const QVector<
        double> &setA,
                                                                             const QVector<
                                                                                     double> &setB,
                                                                             Data::DynamicTimeInterval *setGap,
                                                                             bool resetUndefined)
        : a(setA), b(setB), gap(setGap), resetOnUndefined(resetUndefined)
{
}

SmootherChainCoreGeneralDigitalFilter::~SmootherChainCoreGeneralDigitalFilter()
{
    if (gap != NULL)
        delete gap;
}

void SmootherChainCoreGeneralDigitalFilter::createSmoother(SmootherChainCoreEngineInterface *engine,
                                                           SmootherChainTarget *&input,
                                                           SmootherChainTarget *output)
{
    DigitalFilterChainGeneral *filter =
            new DigitalFilterChainGeneral(a, b, gap == NULL ? NULL : gap->clone(), resetOnUndefined,
                                          output);
    engine->addChainNode(filter);
    input = filter->getTarget();
}

void SmootherChainCoreGeneralDigitalFilter::deserializeSmoother(QDataStream &stream,
                                                                SmootherChainCoreEngineInterface *engine,
                                                                SmootherChainTarget *&input,
                                                                SmootherChainTarget *output)
{
    DigitalFilterChainGeneral *filter = new DigitalFilterChainGeneral(stream, output);
    engine->addChainNode(filter);
    input = filter->getTarget();
}

Variant::Root SmootherChainCoreGeneralDigitalFilter::getProcessingMetadata(double time) const
{
    Variant::Root meta;
    meta.write().hash("Type").setString("DigitalFilter");
    for (auto add : a) {
        meta.write().hash("Parameters").hash("A").toArray().after_back().setReal(add);
    }
    for (auto add : b) {
        meta.write().hash("Parameters").hash("B").toArray().after_back().setReal(add);
    }
    if (gap != NULL) {
        meta.write().hash("Parameters").hash("Gap").setString(gap->describe(time));
    }
    meta.write().hash("Parameters").hash("ResetOnUndefined").setBool(resetOnUndefined);
    return meta;
}

QSet<double> SmootherChainCoreGeneralDigitalFilter::getProcessingMetadataBreaks() const
{
    if (gap == NULL)
        return QSet<double>();
    return gap->getChangedPoints();
}


SmootherChainCoreDigitalFilterWrapper::SmootherChainCoreDigitalFilterWrapper(DigitalFilter *wrapFilter)
        : wrap(wrapFilter)
{
    Q_ASSERT(wrap != NULL);
}

SmootherChainCoreDigitalFilterWrapper::~SmootherChainCoreDigitalFilterWrapper()
{
    delete wrap;
}

void SmootherChainCoreDigitalFilterWrapper::createSmoother(SmootherChainCoreEngineInterface *engine,
                                                           SmootherChainTarget *&input,
                                                           SmootherChainTarget *output)
{
    DigitalFilterChainWrapper *filter = new DigitalFilterChainWrapper(wrap->clone(), output);
    engine->addChainNode(filter);
    input = filter->getTarget();
}

void SmootherChainCoreDigitalFilterWrapper::deserializeSmoother(QDataStream &stream,
                                                                SmootherChainCoreEngineInterface *engine,
                                                                SmootherChainTarget *&input,
                                                                SmootherChainTarget *output)
{
    DigitalFilterChainGeneral *filter = new DigitalFilterChainGeneral(stream, output);
    engine->addChainNode(filter);
    input = filter->getTarget();
}

Variant::Root SmootherChainCoreDigitalFilterWrapper::getProcessingMetadata(double) const
{
    Variant::Root meta;
    meta.write().hash("Type").setString("DigitalFilterWrapper");
    meta.write().hash("Parameters").hash("Filter").set(wrap->describeState());
    return meta;
}

QSet<double> SmootherChainCoreDigitalFilterWrapper::getProcessingMetadataBreaks() const
{ return QSet<double>(); }

}
}
