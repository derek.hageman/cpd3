/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGENGINE_H
#define CPD3SMOOTHINGENGINE_H

#include "core/first.hxx"

#include <vector>
#include <deque>
#include <mutex>
#include <condition_variable>
#include <QtGlobal>
#include <QObject>
#include <QThread>
#include <QDataStream>

#include "smoothing/smoothing.hxx"
#include "smoothing/smoothingchain.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "datacore/segment.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "core/waitutils.hxx"

namespace CPD3 {
namespace Smoothing {

class SmoothingEngine;

class SmoothingEngineArrayMergeBasic;

class SmoothingEngineArrayMergeFlags;

class SmoothingEngineArrayMergeGeneral;

class SmoothingEngineMatrixMergeBasic;

class SmoothingEngineMatrixMergeFlags;

class SmoothingEngineMatrixMergeGeneral;

class DigitalFilter;

/**
 * The base implementation for a smoothing engine.  A smoothing engine handles
 * the interface between a smoother chain core and the data stream.
 */
class CPD3SMOOTHING_EXPORT SmoothingEngine : public Data::ProcessingStage {
public:
    enum {
        Type_Discard = 0, Type_Bypass, Type_DefaultUnderlay, Type_Metadata, Type_Value,
    };
    typedef std::deque<CPD3::Data::SequenceValue> IncomingBuffer;
protected:
    class AuxiliaryChainController;

private:
    class Chain;

    class ChainGeneralBypass;

    class ChainBasic;

    class ChainFlags;

    class ChainArray;

    class ChainMatrix;

    struct ChainData {
        std::unique_ptr<AuxiliaryChainController> auxiliary;

        /* Need to keep associated with the chain for unregistration */
        struct Input {
            Data::SequenceName unit;
            std::vector<SmootherChainTarget *> targets;

            explicit inline Input(const Data::SequenceName &unit) : unit(unit)
            { }
        };

        std::vector<Input> inputs;

        struct InputFlags {
            Data::SequenceName unit;
            std::vector<SmootherChainTargetFlags *> targets;

            explicit inline InputFlags(const Data::SequenceName &unit) : unit(unit)
            { }
        };

        std::vector<InputFlags> inputsFlags;

        struct InputGeneral {
            Data::SequenceName unit;
            std::vector<SmootherChainTargetGeneral *> targets;

            explicit inline InputGeneral(const Data::SequenceName &unit) : unit(unit)
            { }
        };

        std::vector<InputGeneral> inputsGeneral;

        std::vector<Data::SequenceName> outputs;
        std::vector<Data::SequenceName> outputsFlags;
        std::vector<Data::SequenceName> outputsGeneral;

        std::vector<std::unique_ptr<SmootherChainNode>> nodes;
    };

    struct ChainArraySharedData {
        std::size_t size;

        std::vector<std::unique_ptr<SmootherChainNode>> sharedNodes;

        Data::SequenceName::Map<SmoothingEngineArrayMergeBasic *> outputsBasic;
        Data::SequenceName::Map<SmoothingEngineArrayMergeFlags *> outputsFlags;
        Data::SequenceName::Map<SmoothingEngineArrayMergeGeneral *> outputsGeneral;
    };
    struct ChainMatrixSharedData {
        Data::Variant::PathElement::MatrixIndex size;

        std::vector<std::unique_ptr<SmootherChainNode>> sharedNodes;

        Data::SequenceName::Map<SmoothingEngineMatrixMergeBasic *> outputsBasic;
        Data::SequenceName::Map<SmoothingEngineMatrixMergeFlags *> outputsFlags;
        Data::SequenceName::Map<SmoothingEngineMatrixMergeGeneral *> outputsGeneral;
    };
protected:
    /**
     * The general interface to the engine dispatch.
     */
    class CPD3SMOOTHING_EXPORT DispatchTarget {
    public:
        /**
         * If set then an incoming value to this dispatch will cause all
         * dispatches to be advanced on the next time change.  This is used
         * by metadata to trigger updates after all values of the same time
         * have been dispatched.
         */
        bool explictTimeAdvance;

        DispatchTarget();

        virtual ~DispatchTarget();

        /**
         * Add an incoming value to the dispatch.
         * 
         * @param value the incoming value
         */
        virtual void incomingData(Data::SequenceValue &&value) = 0;

        /**
         * Advance the dispatch to the given time.
         * 
         * @param time  the time to advance to
         */
        virtual void incomingAdvance(double time) = 0;

        /**
         * Signal the end of data to the dispatch.
         */
        virtual void endData() = 0;

        /**
         * Get the time of the next possible transition.  This is used
         * to prevent the holdback from getting too far ahead and preventing
         * chain conversion.
         * 
         * @return the time of the next possible transition
         */
        virtual double nextPossibleTransition() const = 0;

        /**
         * Get the type code of the dispatch target.
         */
        virtual int getType() const = 0;

        /**
         * Serialize the dispatch target.  This only needs to save the
         * internal state of the dispatch, not any targets that are the
         * result of chain construction.
         * 
         * @param stream    the target data stream
         */
        virtual void serialize(QDataStream &stream) const = 0;

        /**
         * Restore the target from serialized state.  Deseralized targets
         * are constructed using the standard procedure for the unit.
         * 
         * @param stream    the target data stream
         */
        virtual void deserialize(QDataStream &stream) = 0;
    };

    /**
     * The default implementation for the default station underlay data.
     */
    class CPD3SMOOTHING_EXPORT DispatchDefaultUnderlay : public DispatchTarget {
        Data::ValueSegment::Stream segmenter;
        std::vector<DispatchTarget *> forward;
    public:
        DispatchDefaultUnderlay();

        virtual ~DispatchDefaultUnderlay();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();

        virtual double nextPossibleTransition() const;

        virtual int getType() const;

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);

        /**
         * Get the segmenter.
         * 
         * @return the underlay segmenter
         */
        inline const Data::ValueSegment::Stream &getSegmenter() const
        { return segmenter; }

        /**
         * Add a forwarder target.
         * 
         * @param target    the target to forward values to
         */
        void addForwardTarget(DispatchTarget *target);

        /**
         * Remove a forwarder target.
         * 
         * @param target    the target to forward values to
         */
        void removeForwardTarget(DispatchTarget *target);
    };

    /**
     * A simple implementation of a dispatch that discards values.
     */
    class CPD3SMOOTHING_EXPORT DispatchDiscard : public DispatchTarget {
    public:
        DispatchDiscard();

        virtual ~DispatchDiscard();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual double nextPossibleTransition() const;

        virtual void endData();

        virtual int getType() const;

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);
    };

    /**
     * A simple implementation of a dispatch that just bypasses all
     * processing done.
     */
    class CPD3SMOOTHING_EXPORT DispatchBypass : public DispatchTarget {
        Data::SinkMultiplexer::Sink *bypassTarget;
    public:
        DispatchBypass(Data::SinkMultiplexer::Sink *t);

        virtual ~DispatchBypass();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual double nextPossibleTransition() const;

        virtual void endData();

        virtual int getType() const;

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);
    };

    /**
     * The general implementation that handles metadata.
     */
    class CPD3SMOOTHING_EXPORT DispatchMetadata : public DispatchTarget {
        friend class SmoothingEngine;

        SmoothingEngine *parent;
        Data::SequenceName unit;

        double startTimeLimit;

        bool smoothingEnable;
        SmootherChainCore::Type chainType;
        Data::Variant::Read chainParameters;

        void rebuildChain();

    protected:
        /** The target for data output. */
        Data::SinkMultiplexer::Sink *outputTarget;

        /** The segmenter used in data generation. */
        Data::ValueSegment::Stream segmenter;

        /**
         * Rebuild the chain as needed if it has changed.
         * 
         * @param value the metadata value of the segment
         */
        void rebuildChainIfNeeded(const Data::Variant::Read &value);

        /**
         * Dispatch and handle an incoming segment
         * @param segment the segment to handle
         */
        void incomingSegment(Data::ValueSegment &&segment);

        /**
         * Apply segment bounds prediction.
         *
         * @param start the input and output start time
         * @param end   the input and output end time
         */
        bool predictSegmentBounds(double &start, double &end);

        /**
         * Perform an advance based on predicted timing.
         *
         * @param time  the input advance time
         */
        void advancePredicted(double time);

    public:
        DispatchMetadata(SmoothingEngine *engine, const Data::SequenceName &setUnit,
                         Data::SinkMultiplexer::Sink *target);

        virtual ~DispatchMetadata();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();

        virtual double nextPossibleTransition() const;

        virtual int getType() const;

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);

        /**
         * Overlay the given segmenter on top of the internal one.
         * 
         * @param add   the segmenter to add
         */
        inline void overlaySegmenter(const Data::ValueSegment::Stream &add)
        { segmenter.overlay(add); }

        /**
         * Set the break times for the internal segmenter.
         * 
         * @param breaks    the break times
         */
        inline void setBreaks(const QSet<double> &breaks)
        { segmenter.setBreaks(breaks); }

        /**
         * Get the dispatch unit.
         * 
         * @return the dispatch unit
         */
        inline Data::SequenceName getUnit() const
        { return unit; }
    };

    friend class DispatchMetadata;

    class EngineInterface;

    /**
     * A dispatch of values to chain targets.
     */
    class CPD3SMOOTHING_EXPORT DispatchData : public DispatchTarget {
        friend class EngineInterface;

        friend class DispatchMetadata;

        friend class Chain;

        friend class ChainGeneralBypass;

        SmoothingEngine *parent;
        Data::SequenceName unit;

        Data::ValueSegment::Stream segmenter;
        bool ended;
        Data::ValueSegment::Transfer lastSegments;
        std::vector<SmootherChainTarget *> targetsBasic;
        std::vector<SmootherChainTargetFlags *> targetsFlags;
        std::vector<SmootherChainTargetGeneral *> targetsGeneral;
        std::vector<std::vector<SmootherChainTarget *> > targetsArray;
        enum ChainMode {
            Basic = 0, Flags, Array, Matrix, General,

            Undefined,
        };
        ChainMode chainMode;
        Data::Variant::PathElement::MatrixIndex matrixSize;
        Data::SinkMultiplexer::Sink *directBypass;
        bool pendingBypassDisable;
        Data::SinkMultiplexer::Sink *initialTimeHoldback;

        void destroyChain();

        void convertToBasic(double time);

        void convertToFlags(double time);

        void convertToArray(double time, std::size_t size);

        void convertToMatrix(double time, const Data::Variant::PathElement::MatrixIndex &size);

        void convertToGeneral(double time);

        bool checkMultiplePrimary(SmoothingEngine::DispatchMetadata *meta);

        void setupInterface(SmoothingEngine::EngineInterface *interface,
                            SmoothingEngine::DispatchMetadata *meta,
                            SmootherChainCore::Type &effectiveType);

        void addTarget(double discardTime, SmootherChainTarget *target);

        void addTarget(double discardTime, SmootherChainTargetFlags *target);

        void addTarget(double discardTime, SmootherChainTargetGeneral *target);

        void addTarget(double discardTime, SmootherChainTarget *target, std::size_t arrayIndex);

    public:
        DispatchData(SmoothingEngine *engine, const Data::SequenceName &setUnit);

        virtual ~DispatchData();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();

        virtual double nextPossibleTransition() const;

        virtual int getType() const;

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);

        /**
         * Overlay the given segmenter on top of the internal one.
         * 
         * @param add   the segmenter to add
         */
        inline void overlaySegmenter(const Data::ValueSegment::Stream &add)
        { segmenter.overlay(add); }

        /**
         * Set the break times for the internal segmenter.
         * 
         * @param breaks    the break times
         */
        inline void setBreaks(const QSet<double> &breaks)
        { segmenter.setBreaks(breaks); }

        /**
         * Get the dispatch unit.
         * 
         * @return the dispatch unit
         */
        inline Data::SequenceName getUnit() const
        { return unit; }

    protected:
        /**
         * Handle the dispatch for a list of segments.
         * 
         * @param result    the segments to dispatch
         */
        void handleSegments(Data::ValueSegment::Transfer &&result);

        /**
         * Send the advance signal to all targets.
         * 
         * @param time      the time to advance until
         */
        void advanceAll(double time);

        /**
         * Send the end data signal to all targets.
         */
        void endAll();

        /**
         * Get the output unit for the dispatch.  Normally this just
         * returns the input unit.
         * 
         * @return the output unit for the dispatch
         */
        virtual Data::SequenceName getOutputUnit();

        /**
         * Test if the dispatch should do anything with its input.  If
         * this returns true then the chain (and all outputs) are not
         * created.  This can be used to allow for certain inputs to
         * be dispatched without actually generating output.
         * <br>
         * Be default this always returns false
         * 
         * @return true if the chain is disabled.
         */
        virtual bool disableChain();

        /**
         * Test if the given output should be discarded from chain outputs.
         * By default this just returns the result of the engine's
         * alwaysBypassOutput(const Data::SequenceName &).
         * 
         * @param unit  the unit to test
         * @return      true if the unit should always be discarded
         */
        virtual bool discardOutput(const Data::SequenceName &unit);
    };

    friend class DispatchData;

    class CPD3SMOOTHING_EXPORT AuxiliaryChainController {
    public:
        AuxiliaryChainController();

        virtual ~AuxiliaryChainController();
    };

    /**
     * The interface for chains to interact with the engine.
     */
    class CPD3SMOOTHING_EXPORT EngineInterface : public virtual SmootherChainCoreEngineInterface {
        friend class SmoothingEngine;

        friend class Chain;

        friend class ChainBasic;

        friend class ChainFlags;

        friend class ChainArray;

        SmoothingEngine *parent;
        ChainData *target;
        std::size_t arrayIndex;
        ChainArraySharedData *targetArray;
        ChainMatrixSharedData *targetMatrix;
        double discardTime;
        std::unique_ptr<AuxiliaryChainController> auxiliary;

        std::vector<Data::SequenceName> baseUnits;
        Data::Variant::Read options;
        std::vector<SmootherChainTarget *> outputs;
        std::vector<SmootherChainTargetFlags *> outputsFlags;
        std::vector<SmootherChainTargetGeneral *> outputsGeneral;
    public:
        EngineInterface(SmoothingEngine *engine);

        virtual ~EngineInterface();

        virtual void addChainNode(SmootherChainNode *add);

        virtual SmootherChainTarget *getOutput(int index);

        virtual SmootherChainTargetFlags *getFlagsOutput(int index);

        virtual SmootherChainTargetGeneral *getGeneralOutput(int index);

        virtual void addInputTarget(int index, SmootherChainTarget *target);

        virtual void addFlagsInputTarget(int index, SmootherChainTargetFlags *target);

        virtual void addGeneralInputTarget(int index, SmootherChainTargetGeneral *target);

        virtual Data::SequenceName getBaseUnit(int index = 0);

        virtual SmootherChainTarget *addNewOutput(const Data::SequenceName &unit);

        virtual SmootherChainTargetFlags *addNewFlagsOutput(const Data::SequenceName &unit);

        virtual SmootherChainTargetGeneral *addNewGeneralOutput(const Data::SequenceName &unit);

        virtual Data::SinkMultiplexer::Sink *addNewFinalOutput();

        virtual void addNewInput(const Data::SequenceName &unit,
                                 SmootherChainTarget *target = NULL);

        virtual void addNewFlagsInput(const Data::SequenceName &unit,
                                      SmootherChainTargetFlags *target = NULL);

        virtual void addNewGeneralInput(const Data::SequenceName &unit,
                                        SmootherChainTargetGeneral *target = NULL);

        virtual Data::Variant::Read getOptions();

        /**
         * Add a new output that discards everything it receives.  This is
         * used to allow multiple output chains to properly handle units
         * that fully bypass the engine.
         * 
         * @param unit  the unit to assign
         * @return      a discarding output
         */
        virtual SmootherChainTarget *addNewDiscardOutput(const Data::SequenceName &unit);

        /** @see addNewDiscardOutput(const Data::SequenceName &) */
        virtual SmootherChainTargetFlags *addNewDiscardFlagsOutput(const Data::SequenceName &unit);

        /** @see addNewDiscardOutput(const Data::SequenceName &) */
        virtual SmootherChainTargetGeneral *addNewDiscardGeneralOutput(const Data::SequenceName &unit);

    protected:
        /**
         * Set the chain specific auxiliary controller.
         * 
         * @param aux   the new auxiliary controller
         */
        void setChainAuxiliary(AuxiliaryChainController *aux);
    };

    friend class EngineInterface;

    /**
     * A simple target that implements data value reconstruction.
     */
    class CPD3SMOOTHING_EXPORT BasicReconstruct
            : public SmootherChainTarget, public SmootherChainNode {
        Data::SequenceName unit;
        Data::SinkMultiplexer::Sink *ingress;
        bool ended;
    public:
        BasicReconstruct(const Data::SequenceName &u, Data::SinkMultiplexer::Sink *in);

        virtual ~BasicReconstruct();

        virtual bool finished();

        virtual void serialize(QDataStream &stream) const;

        virtual void incomingData(double start, double end, double value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    /**
     * A simple target that implements data value reconstruction.
     */
    class CPD3SMOOTHING_EXPORT FlagsReconstruct
            : public SmootherChainTargetFlags, public SmootherChainNode {
        Data::SequenceName unit;
        Data::SinkMultiplexer::Sink *ingress;
        bool ended;
    public:
        FlagsReconstruct(const Data::SequenceName &u, Data::SinkMultiplexer::Sink *in);

        virtual ~FlagsReconstruct();

        virtual bool finished();

        virtual void serialize(QDataStream &stream) const;

        virtual void incomingData(double start, double end, const Data::Variant::Flags &value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    /**
     * A simple target that implements data value reconstruction.
     */
    class CPD3SMOOTHING_EXPORT GeneralReconstruct
            : public SmootherChainTargetGeneral, public SmootherChainNode {
        friend class ChainGeneralBypass;

        Data::SequenceName unit;
        Data::SinkMultiplexer::Sink *ingress;
        bool ended;
    public:
        GeneralReconstruct(const Data::SequenceName &u, Data::SinkMultiplexer::Sink *in);

        virtual ~GeneralReconstruct();

        virtual bool finished();

        virtual void serialize(QDataStream &stream) const;

        virtual void incomingData(double start, double end, const Data::Variant::Root &value);

        virtual void incomingAdvance(double time);

        virtual void endData();
    };

    /**
     * Create a new engine interface used by chain construction to pass
     * to the core.
     * 
     * @return a new engine interface owned by the caller
     */
    virtual std::unique_ptr<EngineInterface> createEngineInterface();

    /**
     * Create a dispatch target for default station values.  Normally this
     * just tracks them so they can be underlaid with normal values.
     * 
     * @param unit  the unit to create for
     * @return      a new default station dispatch target
     */
    virtual std::unique_ptr<DispatchTarget> createDefaultDispatch(const Data::SequenceName &unit);

    /**
     * Create a metadata dispatch target.  This dispatch will be passed to
     * any value units it corresponds to.  Normally this functions to track
     * the current metadata and notify dependent values of changes.
     * 
     * @param unit      the unit to create for
     * @param underlay  the default station underlay
     * @return          a new metadata dispatch target
     */
    virtual std::unique_ptr<DispatchTarget> createMetaDispatch(const Data::SequenceName &unit,
                                                               DispatchTarget *underlay);

    /**
     * Create a conventional data dispatch target.
     * 
     * @param unit      the unit to create for
     * @param underlay  the underlay for the unit
     * @param meta      the metadata for the unit
     * @return          a new dispatch target
     */
    virtual std::unique_ptr<DispatchTarget> createDataDispatch(const Data::SequenceName &unit,
                                                               DispatchTarget *underlay,
                                                               DispatchTarget *meta);

    /**
     * Test if the engine should process the incoming buffer.
     * <br>
     * The default implementation waits until the time advances (so that
     * the flags and metadata can be sorted to the start and handled first).
     * 
     * @param incomingBegin the start of incoming data values to be dispatched
     * @param incomingEnd   the end of incoming data values to be dispatched
     * @param advanceTime   the advance time to be issued after the data dispatch
     */
    virtual bool canProcess(IncomingBuffer::const_iterator incomingBegin,
                            IncomingBuffer::const_iterator incomingEnd,
                            double advanceTime);

    /**
     * Called at the start of a processing run.
     * 
     * @param incomingBegin the start of incoming data values to be dispatched
     * @param incomingEnd   the end of incoming data values to be dispatched
     * @param advanceTime   the advance time to be issued after the data dispatch
     * @param holdbackTime  the time to advance the hold back output until
     */
    virtual void startProcessing(IncomingBuffer::iterator incomingBegin,
                                 IncomingBuffer::iterator incomingEnd,
                                 double &advanceTime,
                                 double &holdbackTime);

    /**
     * Sets the dispatch for a given unit (which must exist).  This will delete 
     * the current dispatch when control returns to the main processing loop.
     * 
     * @param unit      the unit to change
     * @param replace   the dispatch to replace it with
     */
    void replaceDispatch(const Data::SequenceName &unit, std::unique_ptr<DispatchTarget> &&replace);

    /**
     * Creates an ingress to the main output multiplexer.  This ingress should
     * be advanced and ended with the appropriate hook functions.
     *
     * @return a new output ingress
     */
    inline Data::SinkMultiplexer::Sink *createOutputIngress()
    {
        return outputMux.createSink();
    }

    /**
     * Creates a segmented ingress to the main output multiplexer.  This ingress should
     * be advanced and ended with the appropriate hook functions.
     *
     * @return a new output ingress
     */
    inline Data::SinkMultiplexer::Sink *createOutputSegmented()
    {
        return outputMux.createSegmented();
    }

    /**
     * Creates a placeholder ingress to the main output multiplexer.  This ingress should
     * be advanced and ended with the appropriate hook functions.
     *
     * @return a new output ingress
     */
    inline Data::SinkMultiplexer::Sink *createOutputPlaceholder()
    {
        return outputMux.createPlaceholder();
    }

    /**
     * Create an ingress for data output.  Normally this just
     * calls createOutputIngress(true) so it must be overridden if the
     * data segmenter is not strict.
     * 
     * @param unit      the unit in question
     * @return          the ingress for data      
     */
    virtual Data::SinkMultiplexer::Sink *createDataIngress(const Data::SequenceName &unit);

    /**
     * Get the handler mode for a given unit and chain type.  By default
     * this just calls the appropriate function on the engine core.
     * 
     * @param unit      the unit in question
     * @param type      the chain type
     * @return          the chain handler mode
     */
    virtual SmootherChainCore::HandlerMode handlerMode(const Data::SequenceName &unit,
                                                       SmootherChainCore::Type type);

    /**
     * Test if the engine handles flags for the given unit.  That is, test
     * if flags should just be passed through unchanged.  By default
     * this just calls the appropriate function on the engine core.
     * 
     * @param unit      the unit in question
     * @return          true if flags are handled
     */
    virtual bool handleFlags(const Data::SequenceName &unit);

    /**
     * Get the break times for the given metadata unit.
     * 
     * @param unit  the metadata unit
     * @return      the break times of the metadata
     */
    virtual QSet<double> getMetadataBreaks(const Data::SequenceName &unit);

    /**
     * Apply the transformation of the given metadata caused by the the chain.
     * 
     * @param unit  the metadata unit
     * @param time  the start time of the metadata
     * @param metadata the input and output metadata
     */
    virtual void transformMetadata(const Data::SequenceName &unit,
                                   double time, Data::Variant::Write &metadata);

    /**
     * Apply the actual metadata transformation for a given smoothing core.
     * 
     * @param core  the core to use, may not be NULL
     * @param unit  the metadata unit
     * @param time  the start time of the metadata
     * @param metadata the input and output metadata
     */
    void transformMetadata(SmootherChainCore *core, const Data::SequenceName &unit,
                           double time, Data::Variant::Write &metadata);

    /**
     * Do the metadata transformation given the core and dispatch to use.
     * 
     * @param core  the core to use, may not be NULL
     * @param dispatch  the dispatch to use, may not be NULL
     * @param unit  the metadata unit
     * @param time  the start time of the metadata
     * @param metadata the input and output metadata
     */
    void transformMetadata(SmootherChainCore *core,
                           DispatchMetadata *dispatch,
                           const Data::SequenceName &unit,
                           double time, Data::Variant::Write &metadata);

    /**
     * Perform metadata bounds prediction.
     *
     * @param start     the input and output start time if not NULL
     * @param end       the input and output end time if not NULL
     */
    virtual void predictMetadataBounds(double *start, double *end);

    /**
     * Construct a chain for the given triggering unit.  Normally all this
     * does is call the appropriate function on the engine's chain.
     * 
     * @param triggeringUnit    the unit that triggered chain construction
     * @param interface         the configured engine interface
     * @param type              the type of chain to construct
     */
    virtual void constructChain(const Data::SequenceName &triggeringUnit,
                                EngineInterface *interface,
                                SmootherChainCore::Type type);

    /**
     * De-serialize a chain for the given triggering unit.  Normally all this
     * does is call the appropriate function on the engine's chain.
     * 
     * @param inputUnits        all simple input units for the chain
     * @param interface         the configured engine interface
     * @param type              the type of chain to construct
     */
    virtual void deserializeChain(const std::vector<Data::SequenceName> &inputUnits,
                                  QDataStream &stream,
                                  EngineInterface *interface,
                                  SmootherChainCore::Type type);

    /**
     * Construct a flags chain for the given triggering unit.  Normally all this
     * does is call the appropriate function on the engine's chain.
     * 
     * @param triggeringUnit    the unit that triggered chain construction
     * @param interface         the configured engine interface
     */
    virtual void constructFlagsChain(const Data::SequenceName &triggeringUnit,
                                     EngineInterface *interface);

    /**
     * De-serialize a chain for the given triggering unit.  Normally all this
     * does is call the appropriate function on the engine's chain.
     * 
     * @param inputUnits        all flags input units for the chain
     * @param interface         the configured engine interface
     * @param type              the type of chain to construct
     */
    virtual void deserializeFlagsChain(const std::vector<Data::SequenceName> &inputUnits,
                                       QDataStream &stream,
                                       EngineInterface *interface);

    /**
     * Serialize the internal elements of the engine.  This may only be
     * used between a pauseProcessing() and resumeProcessing() call.
     * 
     * @param stream the target output data stream
     */
    void serializeInternal(QDataStream &stream);

    /**
     * Pause all processing.
     */
    void pauseProcessing();

    /**
     * Resume processing after a pause.
     */
    void resumeProcessing();

    /**
     * Test if the chain generates the given unit as an output.  Normally all 
     * this does is call the appropriate function on the engine's chain.
     * 
     * @param unit      the unit to test
     * @return          true if the chain handles coverage
     */
    virtual bool generatedOutput(const Data::SequenceName &unit);

    /**
     * Test if the given output unit is always passed through the chain 
     * unchanged.  Normally this returns false.
     * 
     * @param unit      the unit to test
     */
    virtual bool alwaysBypassOutput(const Data::SequenceName &unit);

    /**
     * Called immediately before all chains are to be ended.
     * <br>
     * The default implementation does nothing.
     */
    virtual void auxiliaryEndBefore();

    /**
     * Called after all chains have been ended.
     * <br>
     * The default implementation does nothing.
     */
    virtual void auxiliaryEndAfter();

    /**
     * Called after the main data advance in the processing loop.
     * <br>
     * The default implementation does nothing.
     */
    virtual void auxiliaryAdvance(double advanceTime);

    /**
     * Change the core used by the chain to the given one.  This deletes the
     * existing core if any.
     * 
     * @param c the new core
     */
    inline void changeCore(SmootherChainCore *c)
    { core.reset(c); }

    /**
     * End all chains.  Generally this should only be called by a subclass
     * as part of the destructor (so the appropriate virtual functions get
     * called before the v-table is reset).
     */
    void endAllChains();

    /**
     * Lookup and create as needed the target for a given dispatch unit.
     * 
     * @param unit      the dispatch unit
     * @return          the dispatch for the unit
     */
    DispatchTarget *dispatchTarget(const Data::SequenceName &unit);

private:
    std::unique_ptr<SmootherChainCore> core;

    Data::SinkMultiplexer outputMux;
    Data::SinkMultiplexer::Sink *holdback;

#ifndef NDEBUG
    bool enableIncomingCheck;
    double lastIncomingTime;
#endif
    std::deque<Data::SequenceValue> pending;
    std::deque<Data::SequenceValue> incoming;
    bool dataEnded;
    double dataAdvance;
    double advanceStartTime;
    enum {
        NotStarted,
        Running, PauseRequested, Paused, Terminated, Finalizing, FinalizeTerminated, Completed,
    } state;

    std::thread thread;
    std::mutex mutex;
    std::condition_variable request;
    std::condition_variable response;

    Data::SequenceName::Map<std::unique_ptr<DispatchTarget>> dispatch;
    Data::SequenceName::Map<std::unique_ptr<DispatchTarget>> dispatchNew;
    std::vector<std::unique_ptr<DispatchTarget>> dispatchPendingDelete;

    friend class Chain;

    friend class ChainGeneralBypass;

    friend class ChainBasic;

    friend class ChainFlags;

    friend class ChainArray;

    class Chain {
    public:
        virtual ~Chain();

        static void unreferenceAll(DispatchData *dispatch, const Data::SequenceName &unit,
                                   ChainData &data,
                                   int arrayIndex = -1);

        static Data::SequenceName::Set definedInputs(const ChainData &data);

        static bool isCompleted(const std::vector<std::unique_ptr<SmootherChainNode>> &nodes);

        static void sendTerminate(const std::vector<std::unique_ptr<SmootherChainNode>> &nodes);

        static void pauseChain(const std::vector<std::unique_ptr<SmootherChainNode>> &nodes);

        static void resumeChain(const std::vector<std::unique_ptr<SmootherChainNode>> &nodes);

        static void cleanupChain(const std::vector<std::unique_ptr<SmootherChainNode>> &nodes);

        static void serializeChain(const ChainData &data, QDataStream &stream);

        void deserializeChain(ChainData &data,
                              SmoothingEngine::EngineInterface *interface,
                              QDataStream &stream);

        virtual void unregisterUnit(DispatchData *dispatch, const Data::SequenceName &unit) = 0;

        virtual Data::SequenceName::Set remainingReferencedUnits() = 0;

        virtual bool checkCompleted() = 0;

        virtual void signalTerminate() = 0;

        virtual void pause() = 0;

        virtual void resume() = 0;

        virtual void cleanup() = 0;

        static Chain *deserialize(SmoothingEngine *engine, QDataStream &stream);

        virtual void serialize(QDataStream &stream) const = 0;
    };

    class ChainGeneralBypass : public Chain {
    public:
        virtual ~ChainGeneralBypass();

        std::unique_ptr<GeneralReconstruct> node;
        Data::SequenceName unit;

        virtual void unregisterUnit(DispatchData *dispatch, const Data::SequenceName &unit);

        virtual Data::SequenceName::Set remainingReferencedUnits();

        virtual bool checkCompleted();

        virtual void signalTerminate();

        virtual void pause();

        virtual void resume();

        virtual void cleanup();

        virtual void serialize(QDataStream &stream) const;

        void restore(SmoothingEngine *engine, QDataStream &stream);
    };

    class ChainBasic : public Chain {
    public:
        virtual ~ChainBasic();

        SmootherChainCore::Type type;
        ChainData data;

        virtual void unregisterUnit(DispatchData *dispatch, const Data::SequenceName &unit);

        virtual Data::SequenceName::Set remainingReferencedUnits();

        virtual bool checkCompleted();

        virtual void signalTerminate();

        virtual void pause();

        virtual void resume();

        virtual void cleanup();

        virtual void serialize(QDataStream &stream) const;

        void restore(SmoothingEngine *engine, QDataStream &stream);
    };

    class ChainFlags : public Chain {
    public:
        virtual ~ChainFlags();

        ChainData data;

        virtual void unregisterUnit(DispatchData *dispatch, const Data::SequenceName &unit);

        virtual Data::SequenceName::Set remainingReferencedUnits();

        virtual bool checkCompleted();

        virtual void signalTerminate();

        virtual void pause();

        virtual void resume();

        virtual void cleanup();

        virtual void serialize(QDataStream &stream) const;

        void restore(SmoothingEngine *engine, QDataStream &stream);
    };

    class ChainArray : public Chain {
    public:
        virtual ~ChainArray();

        SmootherChainCore::Type type;
        std::vector<ChainData> indices;
        ChainArraySharedData data;

        virtual void unregisterUnit(DispatchData *dispatch, const Data::SequenceName &unit);

        virtual Data::SequenceName::Set remainingReferencedUnits();

        virtual bool checkCompleted();

        virtual void signalTerminate();

        virtual void pause();

        virtual void resume();

        virtual void cleanup();

        virtual void serialize(QDataStream &stream) const;

        void restore(SmoothingEngine *engine, QDataStream &stream);
    };

    class ChainMatrix : public Chain {
    public:
        virtual ~ChainMatrix();

        SmootherChainCore::Type type;
        std::vector<ChainData> indices;
        ChainMatrixSharedData data;

        virtual void unregisterUnit(DispatchData *dispatch, const Data::SequenceName &unit);

        virtual Data::SequenceName::Set remainingReferencedUnits();

        virtual bool checkCompleted();

        virtual void signalTerminate();

        virtual void pause();

        virtual void resume();

        virtual void cleanup();

        virtual void serialize(QDataStream &stream) const;

        void restore(SmoothingEngine *engine, QDataStream &stream);
    };

    std::vector<std::unique_ptr<Chain>> chains;
    Data::SequenceName::Map<Chain *> primaryChains;

    DispatchMetadata *getUnitMetadataDispatch(const Data::SequenceName &unit);

    void uniteNewDispatch();

    void reapFinishedChains();

    void run();

    void stall(std::unique_lock<std::mutex> &lock);

    void installPrimaryChain(const Data::SequenceName &name, Chain *chain);

    void outputMuxFinished();

public:
    /**
     * Create a new smoothing engine.
     * 
     * @param setCore               the engine core to use, this takes ownership if it is non-NULL, if it it NULL then the child must implement all the appropriate functions that would call into it
     * @param enableIncomingSanityCheck if set the engine can (usually only in debug mode) check the sanity of incoming data
     */
    SmoothingEngine(SmootherChainCore *setCore = NULL, bool enableIncomingSanityCheck = true);

    virtual ~SmoothingEngine();

    virtual void deserialize(QDataStream &stream);

    void serialize(QDataStream &stream) override;

    void incomingData(const Data::SequenceValue::Transfer &values) override;

    void incomingData(Data::SequenceValue::Transfer &&values) override;

    void incomingData(const Data::SequenceValue &value) override;

    void incomingData(Data::SequenceValue &&value) override;

    void endData() override;

    void incomingAdvance(double time);

    bool isFinished() override;

    bool wait(double timeout = FP::undefined()) override;

    void setEgress(Data::StreamSink *egress) override;

    /**
     * Create a new engine that uses a single pole low pass digital filter.
     * This takes ownership of the gap and time constant.
     * 
     * @param tc                time time constant specification
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     * @param serializeRawData  raw data writen to the serialize but NOT deserialized
     * @return                  a new smoothing engine
     */
    static SmoothingEngine *createSinglePoleLowPassDigitalFilter(Data::DynamicTimeInterval *tc,
                                                                 Data::DynamicTimeInterval *setGap = NULL,
                                                                 bool resetUndefined = true,
                                                                 const QByteArray &serializeRawData = QByteArray());

    /**
     * Deserailize a single pole low pass digital filter engine.
     * 
     * @param stream            the stream to read from
     * @return                  a new smoothing engine
     */
    static SmoothingEngine *deserializeSinglePoleLowPassDigitalFilter(QDataStream &stream);

    /**
     * Create a engine that uses a four pole low pass digital filter.
     * This takes ownership of the gap and time constant.
     * 
     * @param tc                time time constant specification
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     * @param serializeRawData  raw data writen to the serialize but NOT deserialized
     * @return                  a new smoothing engine
     */
    static SmoothingEngine *createFourPoleLowPassDigitalFilter(Data::DynamicTimeInterval *tc,
                                                               Data::DynamicTimeInterval *setGap = NULL,
                                                               bool resetUndefined = true,
                                                               const QByteArray &serializeRawData = QByteArray());

    /**
     * Deserailize a four pole low pass digital filter engine.
     * 
     * @param stream            the stream to read from
     * @return                  a new smoothing engine
     */
    static SmoothingEngine *deserializeFourPoleLowPassDigitalFilter(QDataStream &stream);

    /**
     * Create a engine that uses a general digital filter.  This takes
     * ownership of the gap specification, if any.
     * 
     * @param setA              the "a" coefficients
     * @param setB              the "b" coefficients
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     * @param serializeRawData  raw data writen to the serialize but NOT deserialized
     * @return                  a new smoothing engine
     */
    static SmoothingEngine *createGeneralDigitalFilter(const QVector<double> &setA,
                                                       const QVector<double> &setB,
                                                       Data::DynamicTimeInterval *setGap = NULL,
                                                       bool resetUndefined = true,
                                                       const QByteArray &serializeRawData = QByteArray());

    /**
     * Deserailize a general digital filter engine.
     * 
     * @param stream            the stream to read from
     * @return                  a new smoothing engine
     */
    static SmoothingEngine *deserializeGeneralDigitalFilter(QDataStream &stream);

    /**
     * Create a engine core that uses a general purpose digital filter 
     * interface.  This takes ownership oof the filter.
     * 
     * @param wrapFilter        the filter to wrap
     * @param serializeRawData  raw data writen to the serialize but NOT deserialized
     * @return                  a new smoothing engine
     */
    static SmoothingEngine *createDigitalFilterWrapper(DigitalFilter *wrapFilter,
                                                       const QByteArray &serializeRawData = QByteArray());

    /**
     * Deserailize a digital filter wrapping engine.
     * 
     * @param stream            the stream to read from
     * @return                  a new smoothing engine
     */
    static SmoothingEngine *deserializeDigitalFilterWrapper(QDataStream &stream);

    /**
     * Create a new engine that uses a Tukey 3RSSH smoother.  This takes 
     * ownership of the gap, if any
     * 
     * @param setGap                the maximum time to allow gaps or NULL for none
     * @param setSmoothUndefined    if set then undefined values are simply ignored instead of breaking up the smoother
     * @param serializeRawData      raw data writen to the serialize but NOT deserialized
     * @return                      a new smoothing engine
     */
    static SmoothingEngine *createTukey3RSSH(Data::DynamicTimeInterval *setGap = NULL,
                                             bool setSmoothUndefined = true,
                                             const QByteArray &serializeRawData = QByteArray());

    /**
     * Deserailize a Tukey 3RSSH engine.
     * 
     * @param stream            the stream to read from
     * @return                  a new smoothing engine
     */
    static SmoothingEngine *deserializeTukey3RSSH(QDataStream &stream);

    /**
     * Create a new engine that uses a Fourier frequency based smoother.  
     * This takes ownership of the low, high, and gap, if any
     * 
     * @param setLow                the low threshold time or NULL for none
     * @param setHigh               the high threshold time or NULL for none
     * @param setGap                the maximum time to allow gaps or NULL for none
     * @param setSmoothUndefined    if set then undefined values are simply ignored instead of breaking up the smoother
     * @param serializeRawData      raw data writen to the serialize but NOT deserialized
     * @return                      a new smoothing engine
     */
    static SmoothingEngine *createFourier(Data::DynamicTimeInterval *setLow = NULL,
                                          Data::DynamicTimeInterval *setHigh = NULL,
                                          Data::DynamicTimeInterval *setGap = NULL,
                                          bool setSmoothUndefined = true,
                                          const QByteArray &serializeRawData = QByteArray());

    /**
     * Deserailize a Fourier engine.
     * 
     * @param stream            the stream to read from
     * @return                  a new smoothing engine
     */
    static SmoothingEngine *deserializeFourier(QDataStream &stream);


    void signalTerminate() override;

    void start() override;
};


/**
 * A class implementing the functions needed to recombine set of broken
 * down chain outputs into a single stream of array values.
 * 
 * @param StorageType   the storage data type
 * @param Interface     the interface supported by the targets
 */
template<typename StorageType, typename Interface, typename InterfaceType>
class SmoothingEngineArrayMerger : public SmootherChainNode {
    Data::SequenceName unit;
    Data::SinkMultiplexer::Sink *target;

    class Target : public Interface {
    public:
        SmoothingEngineArrayMerger<StorageType, Interface, InterfaceType> *parent;
        std::size_t index;

        Target() = default;

        virtual ~Target() = default;

        virtual void incomingData(double start, double end, InterfaceType value)
        { parent->addData(index, start, end, value); }

        virtual void incomingAdvance(double time)
        { parent->advanceData(index, time); }

        virtual void endData()
        { parent->endData(index); }
    };

    friend class Target;

    struct IndexData;
    friend struct IndexData;

    struct IndexData {
        double latestTime;
        bool ended;
        bool blocking;

        struct BufferData {
            double start;
            double end;
            StorageType value;

            inline BufferData() = default;

            inline BufferData(double start, double end, InterfaceType value) : start(start),
                                                                               end(end),
                                                                               value(value)
            { }
        };

        std::deque<BufferData> buffer;

        Target target;

        IndexData() : latestTime(FP::undefined()), ended(false), blocking(true), target()
        { }

        ~IndexData() = default;

        void serialize(QDataStream &stream) const
        {
            stream << latestTime << ended << blocking;
            Serialize::container(stream, buffer, [&](const BufferData &data) {
                stream << data.start << data.end << data.value;
            });
        }

        void deserialize(QDataStream &stream)
        {
            blocking = true;
            stream >> latestTime >> ended >> blocking;
            Deserialize::container(stream, buffer, [&]() {
                BufferData data;
                stream >> data.start >> data.end >> data.value;
                return data;
            });
        }

        IndexData(const IndexData &) = delete;

        IndexData &operator=(const IndexData &) = delete;

        IndexData(IndexData &&) = default;

        IndexData &operator=(IndexData &&) = default;
    };

    std::vector<IndexData> indices;

    static inline void assignHandle(Data::Variant::Write &&output, const Data::Variant::Read &input)
    { output.set(input); }

    static inline void assignHandle(Data::Variant::Write &&output, double input)
    { output.setDouble(input); }

    static inline void assignHandle(Data::Variant::Write &&output,
                                    const Data::Variant::Flags &input)
    { output.setFlags(input); }

    static inline void assignHandle(Data::Variant::Write &&output, const QString &input)
    { output.setString(input); }

    void emitAllPossible()
    {
        bool haveLatest = false;
        double latest = FP::undefined();
        double first = 0;
        double selectedEnd = 0;
        bool haveData = false;
        bool allEnded = true;
        int cr = 0;
        Data::Variant::Root output;
        std::size_t arrayIndex;

        arrayIndex = 0;
        auto array = output.write().toArray();
        for (auto idx = indices.begin(), endIdx = indices.end();
                idx != endIdx;
                ++idx, ++arrayIndex) {
            idx->blocking = false;
            if (!idx->ended) {
                if (!FP::defined(idx->latestTime)) {
                    for (auto unblock = indices.begin(); unblock != endIdx; ++unblock) {
                        unblock->blocking = false;
                    }
                    idx->blocking = true;
                    return;
                }

                if (!haveLatest) {
                    latest = idx->latestTime;
                    idx->blocking = true;
                } else if (idx->latestTime < latest) {
                    latest = idx->latestTime;

                    for (auto unblock = indices.begin(); unblock != endIdx; ++unblock) {
                        unblock->blocking = false;
                    }
                    idx->blocking = true;
                }
                allEnded = false;
                haveLatest = true;
            }

            if (idx->buffer.empty())
                continue;
            const auto &next = idx->buffer.front();
            cr = Range::compareStart(next.start, first);
            if (!haveData || cr < 0) {
                first = next.start;
                selectedEnd = next.end;
                assignHandle(array[arrayIndex], next.value);
            } else if (cr == 0) {
                assignHandle(array[arrayIndex], next.value);
                if (Range::compareEnd(selectedEnd, next.end) < 0)
                    selectedEnd = next.end;
            } else {
                array[arrayIndex].setEmpty();
            }
            haveData = true;
        }

        double removeTime = 0;
        while (haveData && (allEnded || (!FP::defined(first) || first < latest))) {
            removeTime = first;
            target->incomingData(Data::SequenceValue(unit, output, first, selectedEnd));

            haveData = false;
            arrayIndex = 0;
            for (auto idx = indices.begin(), endIdx = indices.end();
                    idx != endIdx;
                    ++idx, ++arrayIndex) {
                if (idx->buffer.empty())
                    continue;
                if (FP::equal(idx->buffer.front().start, removeTime)) {
                    idx->buffer.pop_front();
                    if (idx->buffer.empty())
                        continue;
                }

                const auto &next = idx->buffer.front();
                cr = Range::compareStart(next.start, first);
                if (!haveData || cr < 0) {
                    first = next.start;
                    selectedEnd = next.end;
                    output = Data::Variant::Root();
                    array = output.write().toArray();
                    assignHandle(array[arrayIndex], next.value);
                } else if (cr == 0) {
                    assignHandle(array[arrayIndex], next.value);
                    if (Range::compareEnd(selectedEnd, next.end) < 0)
                        selectedEnd = next.end;
                } else {
                    array[arrayIndex].setEmpty();
                }
                haveData = true;
            }
        }

        if (allEnded) {
            if (target != NULL)
                target->endData();
            target = NULL;
        } else if (haveLatest) {
            target->incomingAdvance(latest);
        }
    }

    void addData(std::size_t index, double start, double end, InterfaceType value)
    {
        Q_ASSERT(index >= 0 && index < indices.size());
        auto &target = indices[index];
        target.buffer.emplace_back(start, end, value);
        Q_ASSERT(Range::compareStart(target.latestTime, start) <= 0);
        target.latestTime = start;
        if (!target.blocking)
            return;
        emitAllPossible();
    }

    void advanceData(std::size_t index, double time)
    {
        Q_ASSERT(index >= 0 && index < indices.size());
        auto &target = indices[index];
        target.latestTime = time;
        Q_ASSERT(Range::compareStart(target.latestTime, time) <= 0);
        if (!target.blocking)
            return;
        emitAllPossible();
    }

    void endData(std::size_t index)
    {
        Q_ASSERT(index >= 0 && index < indices.size());
        auto &target = indices[index];
        Q_ASSERT(!target.ended);
        target.ended = true;
        if (!target.blocking)
            return;
        emitAllPossible();
    }

public:
    /**
     * Construct a new array merger.
     * 
     * @param size      the number of elements in the array
     * @param setUnit   the output data value unit
     * @param setTarget the output multiplexer target
     */
    SmoothingEngineArrayMerger(std::size_t size, const Data::SequenceName &setUnit,
                               Data::SinkMultiplexer::Sink *setTarget) : unit(setUnit),
                                                                         target(setTarget)
    {
        if (size == 0) {
            target->endData();
            target = nullptr;
            return;
        }
        indices.resize(size);
        for (std::size_t i = 0; i < size; i++) {
            indices[i].target.parent = this;
            indices[i].target.index = i;
        }
    }

    /**
     * Construct a new array merger from saved state.
     * 
     * @param stream    the stream to restore state from
     * @param setUnit   the output data value unit
     * @param setTarget the output multiplexer target
     */
    SmoothingEngineArrayMerger(QDataStream &stream, const Data::SequenceName &setUnit,
                               Data::SinkMultiplexer::Sink *setTarget) : unit(setUnit),
                                                                         target(setTarget)
    {
        quint32 n;
        stream >> n;
        if (n == 0) {
            target->endData();
            target = nullptr;
            return;
        }

        indices.resize(n);
        std::size_t setIndex = 0;
        for (auto idx = indices.begin(), endIdx = indices.end(); idx != endIdx; ++idx, ++setIndex) {
            idx->deserialize(stream);
            idx->target.parent = this;
            idx->target.index = setIndex;
        }
    }

    virtual ~SmoothingEngineArrayMerger() = default;

	SmoothingEngineArrayMerger() = delete;

	SmoothingEngineArrayMerger(const SmoothingEngineArrayMerger<StorageType, Interface, InterfaceType> &) = delete;

	SmoothingEngineArrayMerger &operator=(const SmoothingEngineArrayMerger<StorageType, Interface, InterfaceType> &) = delete;

    virtual void serialize(QDataStream &stream) const
    {
        Q_ASSERT(static_cast<quint32>(indices.size()) == indices.size());
        stream << static_cast<quint32>(indices.size());
        for (const auto &idx : indices) {
            idx.serialize(stream);
        }
    }

    virtual void pause()
    { }

    virtual void resume()
    { }

    virtual bool finished()
    { return target == nullptr; }

    /**
     * Get the target for the given index of data.
     * 
     * @param index     the index to fetch
     * @return          a target for incoming data
     */
    inline Interface *getTarget(std::size_t index)
    {
        Q_ASSERT(index >= 0 && index < indices.size());
        return &indices[index].target;
    }
};

/**
 * A base implementation for recombining array values.
 */
class CPD3SMOOTHING_EXPORT SmoothingEngineArrayMergeBasic : public SmoothingEngineArrayMerger<
        double, SmootherChainTarget, double> {
public:
    SmoothingEngineArrayMergeBasic(std::size_t size, const Data::SequenceName &setUnit,
                                   Data::SinkMultiplexer::Sink *setTarget);

    SmoothingEngineArrayMergeBasic(QDataStream &stream, const Data::SequenceName &setUnit,
                                   Data::SinkMultiplexer::Sink *setTarget);
};

/**
 * A base implementation for recombining flags values.
 */
class CPD3SMOOTHING_EXPORT SmoothingEngineArrayMergeFlags : public SmoothingEngineArrayMerger<
        Data::Variant::Flags, SmootherChainTargetFlags, const Data::Variant::Flags &> {
public:
    SmoothingEngineArrayMergeFlags(std::size_t size, const Data::SequenceName &setUnit,
                                   Data::SinkMultiplexer::Sink *setTarget);

    SmoothingEngineArrayMergeFlags(QDataStream &stream, const Data::SequenceName &setUnit,
                                   Data::SinkMultiplexer::Sink *setTarget);
};

/**
 * A base implementation for recombining general values.
 */
class CPD3SMOOTHING_EXPORT SmoothingEngineArrayMergeGeneral : public SmoothingEngineArrayMerger<
        Data::Variant::Root, SmootherChainTargetGeneral, const Data::Variant::Root &> {
public:
    SmoothingEngineArrayMergeGeneral(std::size_t size, const Data::SequenceName &setUnit,
                                     Data::SinkMultiplexer::Sink *setTarget);

    SmoothingEngineArrayMergeGeneral(QDataStream &stream, const Data::SequenceName &setUnit,
                                     Data::SinkMultiplexer::Sink *setTarget);
};


/**
 * A class implementing the functions needed to recombine set of broken
 * down chain outputs into a single stream of matrix values.
 * 
 * @param StorageType   the storage data type
 * @param Interface     the interface supported by the targets
 */
template<typename StorageType, typename Interface, typename InterfaceType>
class SmoothingEngineMatrixMerger : public SmootherChainNode {
    Data::SequenceName unit;
    Data::SinkMultiplexer::Sink *target;

    Data::Variant::PathElement::MatrixIndex matrixSize;

    class Target : public Interface {
    public:
        SmoothingEngineMatrixMerger<StorageType, Interface, InterfaceType> *parent;
        int index;

        Target()
        { }

        virtual ~Target()
        { }

        virtual void incomingData(double start, double end, InterfaceType value)
        { parent->addData(index, start, end, value); }

        virtual void incomingAdvance(double time)
        { parent->advanceData(index, time); }

        virtual void endData()
        { parent->endData(index); }
    };

    friend class Target;

    struct IndexData;
    friend struct IndexData;

    struct IndexData {
        double latestTime;
        bool ended;
        bool blocking;

        struct BufferData {
            double start;
            double end;
            StorageType value;

            inline BufferData() = default;

            inline BufferData(double start, double end, InterfaceType value) : start(start),
                                                                               end(end),
                                                                               value(value)
            { }
        };

        std::deque<BufferData> buffer;

        Target target;
        Data::Variant::PathElement::MatrixIndex matrixIndex;

        IndexData() : latestTime(FP::undefined()),
                      ended(false),
                      blocking(true), buffer(),
                      target(),
                      matrixIndex()
        { }

        ~IndexData() = default;

        void serialize(QDataStream &stream) const
        {
            stream << latestTime << ended << blocking;
            for (auto add : matrixIndex) {
                stream << static_cast<quint32>(add);
            }
            Serialize::container(stream, buffer, [&](const BufferData &data) {
                stream << data.start << data.end << data.value;
            });
        }

        void deserialize(QDataStream &stream, quint8 matrixIndexSize)
        {
            blocking = true;
            stream >> latestTime >> ended >> blocking;

            matrixIndex.clear();
            for (quint8 i = 0; i < matrixIndexSize; i++) {
                quint32 add;
                stream >> add;
                matrixIndex.emplace_back(add);
            }

            Deserialize::container(stream, buffer, [&]() {
                BufferData data;
                stream >> data.start >> data.end >> data.value;
                return data;
            });
        }

        IndexData(const IndexData &) = delete;

        IndexData &operator=(const IndexData &) = delete;

        IndexData(IndexData &&) = default;

        IndexData &operator=(IndexData &&) = default;
    };

    std::vector<IndexData> indices;

    static inline void assignHandle(Data::Variant::Write &&output, const Data::Variant::Read &input)
    { output.set(input); }

    static inline void assignHandle(Data::Variant::Write &&output, double input)
    { output.setDouble(input); }

    static inline void assignHandle(Data::Variant::Write &&output,
                                    const Data::Variant::Flags &input)
    { output.setFlags(input); }

    static inline void assignHandle(Data::Variant::Write &&output, const QString &input)
    { output.setString(input); }

    void emitAllPossible()
    {
        bool haveLatest = false;
        double latest = FP::undefined();
        double first = 0;
        double selectedEnd = 0;
        bool haveData = false;
        bool allEnded = true;
        int cr = 0;
        Data::Variant::Root output;

        auto matrix = output.write().toMatrix();
        matrix.reshape(matrixSize);
        for (auto &idx : indices) {
            idx.blocking = false;
            if (!idx.ended) {
                if (!FP::defined(idx.latestTime)) {
                    for (auto &unblock : indices) {
                        unblock.blocking = false;
                    }
                    idx.blocking = true;
                    return;
                }

                if (!haveLatest) {
                    latest = idx.latestTime;
                    idx.blocking = true;
                } else if (idx.latestTime < latest) {
                    latest = idx.latestTime;

                    for (auto &unblock : indices) {
                        unblock.blocking = false;
                    }
                    idx.blocking = true;
                }
                allEnded = false;
                haveLatest = true;
            }

            if (idx.buffer.empty())
                continue;

            const auto &next = idx.buffer.front();
            cr = Range::compareStart(next.start, first);
            if (!haveData || cr < 0) {
                first = next.start;
                selectedEnd = next.end;
                output = Data::Variant::Root();
                matrix = output.write().toMatrix();
                matrix.reshape(matrixSize);
                assignHandle(matrix[idx.matrixIndex], next.value);
            } else if (cr == 0) {
                assignHandle(matrix[idx.matrixIndex], next.value);
                if (Range::compareEnd(selectedEnd, next.end) < 0)
                    selectedEnd = next.end;
            } else {
                matrix[idx.matrixIndex].setEmpty();
            }
            haveData = true;
        }

        double removeTime = 0;
        while (haveData && (allEnded || (!FP::defined(first) || first < latest))) {
            removeTime = first;
            target->emplaceData(CPD3::Data::SequenceIdentity(unit, first, selectedEnd), output);

            haveData = false;
            for (auto &idx : indices) {
                if (idx.buffer.empty())
                    continue;
                if (FP::equal(idx.buffer.front().start, removeTime)) {
                    idx.buffer.pop_front();
                    if (idx.buffer.empty())
                        continue;
                }

                const auto &next = idx.buffer.front();
                cr = Range::compareStart(next.start, first);
                if (!haveData || cr < 0) {
                    first = next.start;
                    selectedEnd = next.end;
                    output = Data::Variant::Root();
                    matrix = output.write().toMatrix();
                    matrix.reshape(matrixSize);
                    assignHandle(matrix[idx.matrixIndex], next.value);
                } else if (cr == 0) {
                    assignHandle(matrix[idx.matrixIndex], next.value);
                    if (Range::compareEnd(selectedEnd, next.end) < 0)
                        selectedEnd = next.end;
                } else {
                    matrix[idx.matrixIndex].setEmpty();
                }
                haveData = true;
            }
        }

        if (allEnded) {
            if (target)
                target->endData();
            target = nullptr;
        } else if (haveLatest) {
            target->incomingAdvance(latest);
        }
    }

    void addData(std::size_t index, double start, double end, InterfaceType value)
    {
        Q_ASSERT(index >= 0 && index < indices.size());
        auto &target = indices[index];
        target.buffer.emplace_back(start, end, value);
        Q_ASSERT(Range::compareStart(target.latestTime, start) <= 0);
        target.latestTime = start;
        if (!target.blocking)
            return;
        emitAllPossible();
    }

    void advanceData(std::size_t index, double time)
    {
        Q_ASSERT(index >= 0 && index < indices.size());
        auto &target = indices[index];
        target.latestTime = time;
        Q_ASSERT(Range::compareStart(target.latestTime, time) <= 0);
        if (!target.blocking)
            return;
        emitAllPossible();
    }

    void endData(std::size_t index)
    {
        Q_ASSERT(index >= 0 && index < indices.size());
        auto &target = indices[index];
        Q_ASSERT(!target.ended);
        target.ended = true;
        if (!target.blocking)
            return;
        emitAllPossible();
    }

public:
    /**
     * Construct a new matrix merger.
     * 
     * @param size      the number of elements in the matrix
     * @param setUnit   the output data value unit
     * @param setTarget the output multiplexer target
     */
    SmoothingEngineMatrixMerger(const Data::Variant::PathElement::MatrixIndex &size,
                                const Data::SequenceName &setUnit,
                                Data::SinkMultiplexer::Sink *setTarget) : unit(setUnit),
                                                                          target(setTarget),
                                                                          matrixSize(size),
                                                                          indices()
    {
        if (size.empty()) {
            target->endData();
            target = nullptr;
            return;
        }

        std::size_t total = 1;
        for (auto add : size) {
            total *= add;
        }

        indices.resize(total);
        for (std::size_t i = 0; i < total; ++i) {
            indices[i].target.parent = this;
            indices[i].target.index = i;
            indices[i].matrixIndex = Data::Variant::Container::ReadMatrix::key(i, matrixSize);
        }
    }

    /**
     * Construct a new matrix merger from saved state.
     * 
     * @param stream    the stream to restore state from
     * @param setUnit   the output data value unit
     * @param setTarget the output multiplexer target
     */
    SmoothingEngineMatrixMerger(QDataStream &stream, const Data::SequenceName &setUnit,
                                Data::SinkMultiplexer::Sink *setTarget) : unit(setUnit),
                                                                          target(setTarget),
                                                                          matrixSize(), indices()
    {

        quint8 ms;
        stream >> ms;
        for (quint8 i = 0; i < ms; i++) {
            quint32 add;
            stream >> add;
            matrixSize.emplace_back(add);
        }

        quint32 n;
        stream >> n;
        if (n == 0) {
            target->endData();
            target = nullptr;
            return;
        }

        indices.resize(n);
        for (std::size_t i = 0; i < n; ++i) {
            indices[i].deserialize(stream, ms);
            indices[i].target.parent = this;
            indices[i].target.index = i;
        }
    }

    virtual ~SmoothingEngineMatrixMerger() = default;

	SmoothingEngineMatrixMerger() = delete;

	SmoothingEngineMatrixMerger(const SmoothingEngineMatrixMerger<StorageType, Interface, InterfaceType> &) = delete;

	SmoothingEngineMatrixMerger &operator=(const SmoothingEngineMatrixMerger<StorageType, Interface, InterfaceType> &) = delete;

    virtual void serialize(QDataStream &stream) const
    {
        Q_ASSERT(matrixSize.size() < 255);
        stream << static_cast<quint8>(matrixSize.size());
        for (auto add : matrixSize) {
            stream << static_cast<quint32>(add);
        }

        Q_ASSERT(static_cast<quint32>(indices.size()) == indices.size());
        stream << static_cast<quint32>(indices.size());
        for (const auto &idx : indices) {
            idx.serialize(stream);
        }
    }

    virtual void pause()
    { }

    virtual void resume()
    { }

    virtual bool finished()
    { return target == nullptr; }

    /**
     * Get the target for the given index of data.
     * 
     * @param index     the index to fetch
     * @return          a target for incoming data
     */
    inline Interface *getTarget(std::size_t index)
    {
        Q_ASSERT(index >= 0 && index < indices.size());
        return &indices[index].target;
    }

    /**
     * Lookup the index for a given matrix index.
     * 
     * @param   index the input matrix indices
     * @return  the internal translation index
     */
    std::size_t lookupIndex(const Data::Variant::PathElement::MatrixIndex &index) const
    {
        Q_ASSERT(!matrixSize.empty());
        Q_ASSERT(index.size() <= matrixSize.size());
        return Data::Variant::Container::ReadMatrix::index(index, matrixSize);
    }
};

/**
 * A base implementation for recombining matrix values.
 */
class CPD3SMOOTHING_EXPORT SmoothingEngineMatrixMergeBasic : public SmoothingEngineMatrixMerger<
        double, SmootherChainTarget, double> {
public:
    SmoothingEngineMatrixMergeBasic(const Data::Variant::PathElement::MatrixIndex &size,
                                    const Data::SequenceName &setUnit,
                                    Data::SinkMultiplexer::Sink *setTarget);

    SmoothingEngineMatrixMergeBasic(QDataStream &stream, const Data::SequenceName &setUnit,
                                    Data::SinkMultiplexer::Sink *setTarget);
};

/**
 * A base implementation for recombining flags values.
 */
class CPD3SMOOTHING_EXPORT SmoothingEngineMatrixMergeFlags : public SmoothingEngineMatrixMerger<
        Data::Variant::Flags, SmootherChainTargetFlags, const Data::Variant::Flags &> {
public:
    SmoothingEngineMatrixMergeFlags(const Data::Variant::PathElement::MatrixIndex &size,
                                    const Data::SequenceName &setUnit,
                                    Data::SinkMultiplexer::Sink *setTarget);

    SmoothingEngineMatrixMergeFlags(QDataStream &stream, const Data::SequenceName &setUnit,
                                    Data::SinkMultiplexer::Sink *setTarget);
};

/**
 * A base implementation for recombining general values.
 */
class CPD3SMOOTHING_EXPORT SmoothingEngineMatrixMergeGeneral : public SmoothingEngineMatrixMerger<
        Data::Variant::Root, SmootherChainTargetGeneral, const Data::Variant::Root &> {
public:
    SmoothingEngineMatrixMergeGeneral(const Data::Variant::PathElement::MatrixIndex &size,
                                      const Data::SequenceName &setUnit,
                                      Data::SinkMultiplexer::Sink *setTarget);

    SmoothingEngineMatrixMergeGeneral(QDataStream &stream, const Data::SequenceName &setUnit,
                                      Data::SinkMultiplexer::Sink *setTarget);
};


}
}

#endif
