/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "smoothing/editingchain.hxx"
#include "core/environment.hxx"

using namespace CPD3::Data;


namespace CPD3 {
namespace Smoothing {

/** @file smoothing/editingchain.hxx
 * Data editing chain elements.  The editing chain consists of three parallel
 * data streams: the high resolution data, the continuous averaged data,
 * and the full averaged data.  The high resolution data is just the data
 * at the highest time resolution (e.x. one minute data).  The continuous
 * averaged data is the data averaged to the largest continuous intervals
 * within the total averaging window (e.x. averages of each section of cut
 * size split data).  The full averaged data is the data averaged to the full
 * averaging window (e.x. conventional one hour averages).  The continuous
 * and full averages may be identical (e.x. non-cut size split parameters).
 * <br>
 * The editing chain has six different modes it uses smoothing in:
 * <ul>
 *  <li>The initial stage.  This takes the high resolution data and converts
 *      it into continuous averages and full averages.
 *  <li>Intermediate recalculation stages.  These take the high resolution data
 *      and recalculate the continuous averages and full averages.
 *  <li>Intermediate continuous recalculation stages.  These take the
 *      continuous averages and recalculate the full average.
 *  <li>Final statistics generator.  This takes the high resolution data and
 *      generates statistics.
 *  <li>Final recalculation.  This takes the high resolution data and produces
 *      full averages and statistics.
 *  <li>Final continuous recalculation.  This takes the high resolution data
 *      and produces statistics from it and takes the continuous data and 
 *      produces full averages from that.
 * </ul>
 * These modes break down into four different averaging cores:
 * <ul>
 *  <li>Full calculation.  This takes the high resolution data and generates
 *      full and continuous averages.  This is used to satisfy the initial
 *      and intermediate recalculation stages.
 *  <li>Continuous recalculation.  This takes the continuous averages and
 *      produces full averages.  This is used to satisfy the intermediate
 *      continuous recalculation stages as well as the averaging component of
 *      the final continuous recalculation.
 *  <li>Final statistics.  This takes the high resolution data and produces
 *      statistics.  This is used to satisfy the simple case of final statistics
 *      as well as the statistics component of a final continuous recalculation.
 *  <li>Final recalculation.  This takes high resolution data and produces
 *      full averages and statistics.  This is only used for a single mode and
 *      is identical in function to a SmootherChainCoreFixedTime with an
 *      infinite gap allowance.
 * </ul>
 */


static const SequenceName::Component archiveCont = "cont";

DynamicDouble *EditingChainCoreFullCalculate::childCover() const
{ return requiredCover != NULL ? requiredCover->clone() : NULL; }

DynamicTimeInterval *EditingChainCoreFullCalculate::childInterval() const
{ return interval != NULL ? interval->clone() : NULL; }

DynamicTimeInterval *EditingChainCoreFullCalculate::contGap() const
{ return new DynamicTimeInterval::Constant(Time::Millisecond, 1); }

EditingChainCoreFullCalculate::EditingChainCoreFullCalculate(DynamicTimeInterval *setInterval,
                                                             DynamicDouble *setRequiredCover)
        : requiredCover(setRequiredCover), interval(setInterval)
{ }

EditingChainCoreFullCalculate::~EditingChainCoreFullCalculate()
{
    if (interval != NULL)
        delete interval;
    if (requiredCover != NULL)
        delete requiredCover;
}

void EditingChainCoreFullCalculate::createSmoother(SmootherChainCoreEngineInterface *engine,
                                                   SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General: {
        SequenceName avgCoverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputAvgCover =
                engine->addNewOutput(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));

        SequenceName contUnit(engine->getBaseUnit());
        contUnit.setArchive(archiveCont);
        SmootherChainTarget *outputCont = engine->addNewOutput(contUnit);
        SmootherChainTarget *outputContCover =
                engine->addNewOutput(contUnit.withFlavor(SequenceName::flavor_cover));

        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       engine->getOutput(0), outputAvgCover);
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetValue());
        engine->addNewInput(avgCoverUnit, avg->getTargetCover());

        SmootherChainFixedTimeConventional *cont =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(), outputCont,
                                                       outputContCover);
        engine->addChainNode(cont);
        engine->addInputTarget(0, cont->getTargetValue());
        engine->addInputTarget(1, cont->getTargetCover());

        break;
    }

    case SmootherChainCore::Difference: {
        SmootherChainTarget *outputAvgCover =
                engine->addNewOutput(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));

        SequenceName contStartUnit(engine->getBaseUnit(0));
        contStartUnit.setArchive(archiveCont);
        SmootherChainTarget *outputContStart = engine->addNewOutput(contStartUnit);

        SequenceName contEndUnit(engine->getBaseUnit(1));
        contEndUnit.setArchive(archiveCont);
        SmootherChainTarget *outputContEnd = engine->addNewOutput(contEndUnit);

        SmootherChainTarget *outputContCover =
                engine->addNewOutput(contStartUnit.withFlavor(SequenceName::flavor_cover));

        SmootherChainFixedTimeDifference *avg =
                new SmootherChainFixedTimeDifference(childInterval(), NULL, engine->getOutput(0),
                                                     engine->getOutput(1), outputAvgCover);
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetStart());
        engine->addInputTarget(1, avg->getTargetEnd());

        SmootherChainFixedTimeDifference *cont =
                new SmootherChainFixedTimeDifference(childInterval(), contGap(), outputContStart,
                                                     outputContEnd, outputContCover);
        engine->addChainNode(cont);
        engine->addInputTarget(0, cont->getTargetStart());
        engine->addInputTarget(1, cont->getTargetEnd());

        break;
    }

    case SmootherChainCore::DifferenceInitial: {
        SmootherChainTarget *outputAvgCover =
                engine->addNewOutput(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));

        SequenceName contStartUnit(engine->getBaseUnit(0));
        contStartUnit.setArchive(archiveCont);
        SmootherChainTarget *outputContStart = engine->addNewOutput(contStartUnit);

        SequenceName contEndUnit(engine->getBaseUnit(1));
        contEndUnit.setArchive(archiveCont);
        SmootherChainTarget *outputContEnd = engine->addNewOutput(contEndUnit);

        SmootherChainTarget *outputContCover =
                engine->addNewOutput(contStartUnit.withFlavor(SequenceName::flavor_cover));

        SmootherChainFixedTimeDifference *avg =
                new SmootherChainFixedTimeDifference(childInterval(), NULL, engine->getOutput(0),
                                                     engine->getOutput(1), outputAvgCover);
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetStart());
        avg->getTargetEnd()->endData();

        SmootherChainFixedTimeDifference *cont =
                new SmootherChainFixedTimeDifference(childInterval(), contGap(), outputContStart,
                                                     outputContEnd, outputContCover);
        engine->addChainNode(cont);
        engine->addInputTarget(0, cont->getTargetStart());
        cont->getTargetEnd()->endData();

        break;
    }

    case SmootherChainCore::Vector2D: {
        SequenceName avgCoverUnit(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputAvgCover = engine->addNewOutput(avgCoverUnit);

        SequenceName contDirectionUnit(engine->getBaseUnit(0));
        contDirectionUnit.setArchive(archiveCont);
        SmootherChainTarget *outputContDirection = engine->addNewOutput(contDirectionUnit);

        SequenceName contMagnitudeUnit(engine->getBaseUnit(1));
        contMagnitudeUnit.setArchive(archiveCont);
        SmootherChainTarget *outputContMagnitude = engine->addNewOutput(contMagnitudeUnit);

        SmootherChainTarget *outputContCover =
                engine->addNewOutput(contMagnitudeUnit.withFlavor(SequenceName::flavor_cover));

        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputAvgCover);
        engine->addChainNode(avgCover);
        engine->addInputTarget(1, avgCover->getTargetValue());
        engine->addNewInput(avgCoverUnit, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(), NULL,
                                                       outputContCover);
        engine->addChainNode(contCover);
        engine->addInputTarget(1, contCover->getTargetValue());
        engine->addInputTarget(2, contCover->getTargetCover());

        SmootherChainVector2DReconstruct *reconstructAvg =
                new SmootherChainVector2DReconstruct(engine->getOutput(0), engine->getOutput(1));
        engine->addChainNode(reconstructAvg);

        SmootherChainVector2DReconstruct *reconstructCont =
                new SmootherChainVector2DReconstruct(outputContDirection, outputContMagnitude);
        engine->addChainNode(reconstructCont);

        SmootherChainFixedTimeConventional *avgX =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstructAvg->getTargetX());
        engine->addChainNode(avgX);
        engine->addInputTarget(2, avgX->getTargetCover());

        SmootherChainFixedTimeConventional *avgY =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstructAvg->getTargetY());
        engine->addChainNode(avgY);
        engine->addInputTarget(2, avgY->getTargetCover());

        SmootherChainFixedTimeConventional *contX =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(),
                                                       reconstructCont->getTargetX());
        engine->addChainNode(contX);
        engine->addInputTarget(2, contX->getTargetCover());

        SmootherChainFixedTimeConventional *contY =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(),
                                                       reconstructCont->getTargetY());
        engine->addChainNode(contY);
        engine->addInputTarget(2, contY->getTargetCover());

        SmootherChainForward<2> *splitX =
                new SmootherChainForward<2>({avgX->getTargetValue(), contX->getTargetValue()});
        engine->addChainNode(splitX);

        SmootherChainForward<2> *splitY =
                new SmootherChainForward<2>({avgY->getTargetValue(), contY->getTargetValue()});
        engine->addChainNode(splitY);

        SmootherChainVector2DBreakdown
                *breakdown = new SmootherChainVector2DBreakdown(splitX, splitY);
        engine->addChainNode(breakdown);
        engine->addInputTarget(0, breakdown->getTargetDirection());
        engine->addInputTarget(1, breakdown->getTargetMagnitude());

        break;
    }

    case SmootherChainCore::Vector3D: {
        SequenceName avgCoverUnit(engine->getBaseUnit(2).withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputAvgCover = engine->addNewOutput(avgCoverUnit);

        SequenceName contAzimuthUnit(engine->getBaseUnit(0));
        contAzimuthUnit.setArchive(archiveCont);
        SmootherChainTarget *outputContAzimuth = engine->addNewOutput(contAzimuthUnit);

        SequenceName contElevationUnit(engine->getBaseUnit(1));
        contElevationUnit.setArchive(archiveCont);
        SmootherChainTarget *outputContElevation = engine->addNewOutput(contElevationUnit);

        SequenceName contMagnitudeUnit(engine->getBaseUnit(2));
        contMagnitudeUnit.setArchive(archiveCont);
        SmootherChainTarget *outputContMagnitude = engine->addNewOutput(contMagnitudeUnit);

        SmootherChainTarget *outputContCover =
                engine->addNewOutput(contMagnitudeUnit.withFlavor(SequenceName::flavor_cover));

        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputAvgCover);
        engine->addChainNode(avgCover);
        engine->addInputTarget(2, avgCover->getTargetValue());
        engine->addNewInput(avgCoverUnit, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(), NULL,
                                                       outputContCover);
        engine->addChainNode(contCover);
        engine->addInputTarget(2, contCover->getTargetValue());
        engine->addInputTarget(3, contCover->getTargetCover());

        SmootherChainVector3DReconstruct *reconstructAvg =
                new SmootherChainVector3DReconstruct(engine->getOutput(0), engine->getOutput(1),
                                                     engine->getOutput(2));
        engine->addChainNode(reconstructAvg);

        SmootherChainVector3DReconstruct *reconstructCont =
                new SmootherChainVector3DReconstruct(outputContAzimuth, outputContElevation,
                                                     outputContMagnitude);
        engine->addChainNode(reconstructCont);

        SmootherChainFixedTimeConventional *avgX =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstructAvg->getTargetX());
        engine->addChainNode(avgX);
        engine->addInputTarget(3, avgX->getTargetCover());

        SmootherChainFixedTimeConventional *avgY =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstructAvg->getTargetY());
        engine->addChainNode(avgY);
        engine->addInputTarget(3, avgY->getTargetCover());

        SmootherChainFixedTimeConventional *avgZ =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstructAvg->getTargetZ());
        engine->addChainNode(avgZ);
        engine->addInputTarget(3, avgZ->getTargetCover());

        SmootherChainFixedTimeConventional *contX =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(),
                                                       reconstructCont->getTargetX());
        engine->addChainNode(contX);
        engine->addInputTarget(3, contX->getTargetCover());

        SmootherChainFixedTimeConventional *contY =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(),
                                                       reconstructCont->getTargetY());
        engine->addChainNode(contY);
        engine->addInputTarget(3, contY->getTargetCover());

        SmootherChainFixedTimeConventional *contZ =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(),
                                                       reconstructCont->getTargetZ());
        engine->addChainNode(contZ);
        engine->addInputTarget(3, contZ->getTargetCover());

        SmootherChainForward<2> *splitX =
                new SmootherChainForward<2>({avgX->getTargetValue(), contX->getTargetValue()});
        engine->addChainNode(splitX);

        SmootherChainForward<2> *splitY =
                new SmootherChainForward<2>({avgY->getTargetValue(), contY->getTargetValue()});
        engine->addChainNode(splitY);

        SmootherChainForward<2> *splitZ =
                new SmootherChainForward<2>({avgZ->getTargetValue(), contZ->getTargetValue()});
        engine->addChainNode(splitZ);

        SmootherChainVector3DBreakdown
                *breakdown = new SmootherChainVector3DBreakdown(splitX, splitY, splitZ);
        engine->addChainNode(breakdown);
        engine->addInputTarget(0, breakdown->getTargetAzimuth());
        engine->addInputTarget(1, breakdown->getTargetElevation());
        engine->addInputTarget(2, breakdown->getTargetMagnitude());

        break;
    }

    case SmootherChainCore::BeersLawAbsorption: {
        SequenceName avgCoverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputAvgCover =
                engine->addNewOutput(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));

        SequenceName contUnit(engine->getBaseUnit());
        contUnit.setArchive(archiveCont);
        SmootherChainTarget *outputCont = engine->addNewOutput(contUnit);
        SmootherChainTarget *outputContCover =
                engine->addNewOutput(contUnit.withFlavor(SequenceName::flavor_cover));

        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputAvgCover);
        engine->addChainNode(avgCover);
        engine->addNewInput(engine->getBaseUnit(), avgCover->getTargetValue());
        engine->addNewInput(avgCoverUnit, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(), NULL,
                                                       outputContCover);
        engine->addChainNode(contCover);
        engine->addInputTarget(4, contCover->getTargetValue());
        engine->addInputTarget(5, contCover->getTargetCover());

        SmootherChainFixedTimeConventional *avgCombine =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       engine->getOutput(0));
        engine->addChainNode(avgCombine);
        avgCombine->getTargetCover()->endData();

        SmootherChainForward<2> *splitValue =
                new SmootherChainForward<2>({avgCombine->getTargetValue(), outputCont});
        engine->addChainNode(splitValue);

        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(splitValue);
        engine->addChainNode(calculate);

        SmootherChainFixedTimeDifference *diffL =
                new SmootherChainFixedTimeDifference(childInterval(), contGap(),
                                                     calculate->getTargetStartL(),
                                                     calculate->getTargetEndL());
        engine->addChainNode(diffL);
        engine->addInputTarget(0, diffL->getTargetStart());
        engine->addInputTarget(1, diffL->getTargetEnd());

        SmootherChainFixedTimeDifference *diffI =
                new SmootherChainFixedTimeDifference(childInterval(), contGap(),
                                                     calculate->getTargetStartI(),
                                                     calculate->getTargetEndI());
        engine->addChainNode(diffI);
        engine->addInputTarget(2, diffI->getTargetStart());
        engine->addInputTarget(3, diffI->getTargetEnd());

        break;
    }

    case SmootherChainCore::BeersLawAbsorptionInitial: {
        SequenceName avgCoverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputAvgCover =
                engine->addNewOutput(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));

        SequenceName contUnit(engine->getBaseUnit());
        contUnit.setArchive(archiveCont);
        SmootherChainTarget *outputCont = engine->addNewOutput(contUnit);
        SmootherChainTarget *outputContCover =
                engine->addNewOutput(contUnit.withFlavor(SequenceName::flavor_cover));

        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputAvgCover);
        engine->addChainNode(avgCover);
        engine->addNewInput(engine->getBaseUnit(), avgCover->getTargetValue());
        engine->addNewInput(avgCoverUnit, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(), NULL,
                                                       outputContCover);
        engine->addChainNode(contCover);
        engine->addInputTarget(2, contCover->getTargetValue());
        engine->addInputTarget(3, contCover->getTargetCover());

        SmootherChainFixedTimeConventional *avgCombine =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       engine->getOutput(0));
        engine->addChainNode(avgCombine);
        avgCombine->getTargetCover()->endData();

        SmootherChainForward<2> *splitValue =
                new SmootherChainForward<2>({avgCombine->getTargetValue(), outputCont});
        engine->addChainNode(splitValue);

        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(splitValue);
        engine->addChainNode(calculate);

        SmootherChainFixedTimeDifference *diffL =
                new SmootherChainFixedTimeDifference(childInterval(), contGap(),
                                                     calculate->getTargetStartL(),
                                                     calculate->getTargetEndL());
        engine->addChainNode(diffL);
        engine->addInputTarget(0, diffL->getTargetStart());
        diffL->getTargetEnd()->endData();

        SmootherChainFixedTimeDifference *diffI =
                new SmootherChainFixedTimeDifference(childInterval(), contGap(),
                                                     calculate->getTargetStartI(),
                                                     calculate->getTargetEndI());
        engine->addChainNode(diffI);
        engine->addInputTarget(1, diffI->getTargetStart());
        diffI->getTargetEnd()->endData();

        break;
    }

    case SmootherChainCore::Dewpoint: {
        SequenceName avgCoverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputAvgCover = engine->addNewOutput(avgCoverUnit);

        SequenceName contUnit(engine->getBaseUnit());
        contUnit.setArchive(archiveCont);
        SmootherChainTarget *outputCont = engine->addNewOutput(contUnit);
        SmootherChainTarget *outputContCover =
                engine->addNewOutput(contUnit.withFlavor(SequenceName::flavor_cover));

        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputAvgCover);
        engine->addChainNode(avgCover);
        engine->addNewInput(engine->getBaseUnit(), avgCover->getTargetValue());
        engine->addNewInput(avgCoverUnit, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(), NULL,
                                                       outputContCover);
        engine->addChainNode(contCover);
        engine->addInputTarget(2, contCover->getTargetValue());
        engine->addInputTarget(3, contCover->getTargetCover());

        SmootherChainDewpoint *calculateAvg = new SmootherChainDewpoint(engine->getOutput(0), engine
                ->getOptions()
                .hash("AlwaysWater")
                .toBool());
        engine->addChainNode(calculateAvg);

        SmootherChainDewpoint *calculateCont = new SmootherChainDewpoint(outputCont,
                                                                         engine->getOptions()
                                                                               .hash("AlwaysWater")
                                                                               .toBool());
        engine->addChainNode(calculateCont);

        SmootherChainFixedTimeConventional *avgTemperature =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculateAvg->getTargetTemperature());
        engine->addChainNode(avgTemperature);
        engine->addInputTarget(0, avgTemperature->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover),
                            avgTemperature->getTargetCover());

        SmootherChainFixedTimeConventional *contTemperature =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(),
                                                       calculateCont->getTargetTemperature());
        engine->addChainNode(contTemperature);
        engine->addInputTarget(0, contTemperature->getTargetValue());
        engine->addInputTarget(4, contTemperature->getTargetCover());

        SmootherChainFixedTimeConventional *avgRH =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculateAvg->getTargetRH());
        engine->addChainNode(avgRH);
        engine->addInputTarget(1, avgRH->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(2).withFlavor(SequenceName::flavor_cover),
                            avgRH->getTargetCover());

        SmootherChainFixedTimeConventional *contRH =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(),
                                                       calculateCont->getTargetRH());
        engine->addChainNode(contRH);
        engine->addInputTarget(1, contRH->getTargetValue());
        engine->addInputTarget(5, contRH->getTargetCover());

        break;
    }

    case SmootherChainCore::RH: {
        SequenceName avgCoverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputAvgCover = engine->addNewOutput(avgCoverUnit);

        SequenceName contUnit(engine->getBaseUnit());
        contUnit.setArchive(archiveCont);
        SmootherChainTarget *outputCont = engine->addNewOutput(contUnit);
        SmootherChainTarget *outputContCover =
                engine->addNewOutput(contUnit.withFlavor(SequenceName::flavor_cover));

        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputAvgCover);
        engine->addChainNode(avgCover);
        engine->addNewInput(engine->getBaseUnit(), avgCover->getTargetValue());
        engine->addNewInput(avgCoverUnit, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(), NULL,
                                                       outputContCover);
        engine->addChainNode(contCover);
        engine->addInputTarget(2, contCover->getTargetValue());
        engine->addInputTarget(3, contCover->getTargetCover());

        SmootherChainRH *calculateAvg = new SmootherChainRH(engine->getOutput(0),
                                                            engine->getOptions()
                                                                  .hash("AlwaysWater")
                                                                  .toBool());
        engine->addChainNode(calculateAvg);

        SmootherChainRH *calculateCont =
                new SmootherChainRH(outputCont, engine->getOptions().hash("AlwaysWater").toBool());
        engine->addChainNode(calculateCont);

        SmootherChainFixedTimeConventional *avgTemperature =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculateAvg->getTargetTemperature());
        engine->addChainNode(avgTemperature);
        engine->addInputTarget(0, avgTemperature->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover),
                            avgTemperature->getTargetCover());

        SmootherChainFixedTimeConventional *contTemperature =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(),
                                                       calculateCont->getTargetTemperature());
        engine->addChainNode(contTemperature);
        engine->addInputTarget(0, contTemperature->getTargetValue());
        engine->addInputTarget(4, contTemperature->getTargetCover());

        SmootherChainFixedTimeConventional *avgDewpoint =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculateAvg->getTargetDewpoint());
        engine->addChainNode(avgDewpoint);
        engine->addInputTarget(1, avgDewpoint->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(2).withFlavor(SequenceName::flavor_cover),
                            avgDewpoint->getTargetCover());

        SmootherChainFixedTimeConventional *contDewpoint =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(),
                                                       calculateCont->getTargetDewpoint());
        engine->addChainNode(contDewpoint);
        engine->addInputTarget(1, contDewpoint->getTargetValue());
        engine->addInputTarget(5, contDewpoint->getTargetCover());

        break;
    }

    case SmootherChainCore::RHExtrapolate: {
        SequenceName avgCoverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputAvgCover = engine->addNewOutput(avgCoverUnit);

        SequenceName contUnit(engine->getBaseUnit());
        contUnit.setArchive(archiveCont);
        SmootherChainTarget *outputCont = engine->addNewOutput(contUnit);
        SmootherChainTarget *outputContCover =
                engine->addNewOutput(contUnit.withFlavor(SequenceName::flavor_cover));

        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputAvgCover);
        engine->addChainNode(avgCover);
        engine->addNewInput(engine->getBaseUnit(), avgCover->getTargetValue());
        engine->addNewInput(avgCoverUnit, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(), NULL,
                                                       outputContCover);
        engine->addChainNode(contCover);
        engine->addInputTarget(3, contCover->getTargetValue());
        engine->addInputTarget(4, contCover->getTargetCover());

        SmootherChainRHExtrapolate *calculateAvg =
                new SmootherChainRHExtrapolate(engine->getOutput(0),
                                               engine->getOptions().hash("AlwaysWater").toBool());
        engine->addChainNode(calculateAvg);

        SmootherChainRHExtrapolate *calculateCont = new SmootherChainRHExtrapolate(outputCont,
                                                                                   engine->getOptions()
                                                                                         .hash("AlwaysWater")
                                                                                         .toBool());
        engine->addChainNode(calculateCont);

        SmootherChainFixedTimeConventional *avgTemperatureIn =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculateAvg->getTargetTemperatureIn());
        engine->addChainNode(avgTemperatureIn);
        engine->addInputTarget(0, avgTemperatureIn->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover),
                            avgTemperatureIn->getTargetCover());

        SmootherChainFixedTimeConventional *contTemperatureIn =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(),
                                                       calculateCont->getTargetTemperatureIn());
        engine->addChainNode(contTemperatureIn);
        engine->addInputTarget(0, contTemperatureIn->getTargetValue());
        engine->addInputTarget(5, contTemperatureIn->getTargetCover());

        SmootherChainFixedTimeConventional *avgRHIn =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculateAvg->getTargetRHIn());
        engine->addChainNode(avgRHIn);
        engine->addInputTarget(1, avgRHIn->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(2).withFlavor(SequenceName::flavor_cover),
                            avgRHIn->getTargetCover());

        SmootherChainFixedTimeConventional *contRHIn =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(),
                                                       calculateCont->getTargetRHIn());
        engine->addChainNode(contRHIn);
        engine->addInputTarget(1, contRHIn->getTargetValue());
        engine->addInputTarget(6, contRHIn->getTargetCover());

        SmootherChainFixedTimeConventional *avgTemperatureOut =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculateAvg->getTargetTemperatureOut());
        engine->addChainNode(avgTemperatureOut);
        engine->addInputTarget(2, avgTemperatureOut->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(3).withFlavor(SequenceName::flavor_cover),
                            avgTemperatureOut->getTargetCover());

        SmootherChainFixedTimeConventional *contTemperatureOut =
                new SmootherChainFixedTimeConventional(NULL, childInterval(), contGap(),
                                                       calculateCont->getTargetTemperatureOut());
        engine->addChainNode(contTemperatureOut);
        engine->addInputTarget(2, contTemperatureOut->getTargetValue());
        engine->addInputTarget(7, contTemperatureOut->getTargetCover());

        break;
    }

    }
}

void EditingChainCoreFullCalculate::deserializeSmoother(QDataStream &stream,
                                                        SmootherChainCoreEngineInterface *engine,
                                                        SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General: {
        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(stream, engine->getOutput(0),
                                                       engine->getOutput(1));
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetValue());
        engine->addInputTarget(1, avg->getTargetCover());

        SmootherChainFixedTimeConventional *cont =
                new SmootherChainFixedTimeConventional(stream, engine->getOutput(2),
                                                       engine->getOutput(3));
        engine->addChainNode(cont);
        engine->addInputTarget(0, cont->getTargetValue());
        engine->addInputTarget(1, cont->getTargetCover());

        break;
    }

    case SmootherChainCore::Difference: {
        SmootherChainFixedTimeDifference *avg =
                new SmootherChainFixedTimeDifference(stream, engine->getOutput(0),
                                                     engine->getOutput(1), engine->getOutput(2));
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetStart());
        engine->addInputTarget(1, avg->getTargetEnd());

        SmootherChainFixedTimeDifference *cont =
                new SmootherChainFixedTimeDifference(stream, engine->getOutput(3),
                                                     engine->getOutput(4), engine->getOutput(5));
        engine->addChainNode(cont);
        engine->addInputTarget(0, cont->getTargetStart());
        engine->addInputTarget(1, cont->getTargetEnd());

        break;
    }

    case SmootherChainCore::DifferenceInitial: {
        SmootherChainFixedTimeDifference *avg =
                new SmootherChainFixedTimeDifference(stream, engine->getOutput(0),
                                                     engine->getOutput(1), engine->getOutput(2));
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetStart());

        SmootherChainFixedTimeDifference *cont =
                new SmootherChainFixedTimeDifference(stream, engine->getOutput(3),
                                                     engine->getOutput(4), engine->getOutput(5));
        engine->addChainNode(cont);
        engine->addInputTarget(0, cont->getTargetStart());

        break;
    }

    case SmootherChainCore::Vector2D: {
        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(2));
        engine->addChainNode(avgCover);
        engine->addInputTarget(1, avgCover->getTargetValue());
        engine->addInputTarget(2, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(5));
        engine->addChainNode(contCover);
        engine->addInputTarget(1, contCover->getTargetValue());
        engine->addInputTarget(2, contCover->getTargetCover());

        SmootherChainVector2DReconstruct *reconstructAvg =
                new SmootherChainVector2DReconstruct(stream, engine->getOutput(0),
                                                     engine->getOutput(1));
        engine->addChainNode(reconstructAvg);

        SmootherChainVector2DReconstruct *reconstructCont =
                new SmootherChainVector2DReconstruct(stream, engine->getOutput(3),
                                                     engine->getOutput(4));
        engine->addChainNode(reconstructCont);

        SmootherChainFixedTimeConventional *avgX =
                new SmootherChainFixedTimeConventional(stream, reconstructAvg->getTargetX());
        engine->addChainNode(avgX);
        engine->addInputTarget(2, avgX->getTargetCover());

        SmootherChainFixedTimeConventional *avgY =
                new SmootherChainFixedTimeConventional(stream, reconstructAvg->getTargetY());
        engine->addChainNode(avgY);
        engine->addInputTarget(2, avgY->getTargetCover());

        SmootherChainFixedTimeConventional *contX =
                new SmootherChainFixedTimeConventional(stream, reconstructCont->getTargetX());
        engine->addChainNode(contX);
        engine->addInputTarget(2, contX->getTargetCover());

        SmootherChainFixedTimeConventional *contY =
                new SmootherChainFixedTimeConventional(stream, reconstructCont->getTargetY());
        engine->addChainNode(contY);
        engine->addInputTarget(2, contY->getTargetCover());

        SmootherChainForward<2> *splitX =
                new SmootherChainForward<2>({avgX->getTargetValue(), contX->getTargetValue()});
        engine->addChainNode(splitX);

        SmootherChainForward<2> *splitY =
                new SmootherChainForward<2>({avgY->getTargetValue(), contY->getTargetValue()});
        engine->addChainNode(splitY);

        SmootherChainVector2DBreakdown
                *breakdown = new SmootherChainVector2DBreakdown(stream, splitX, splitY);
        engine->addChainNode(breakdown);
        engine->addInputTarget(0, breakdown->getTargetDirection());
        engine->addInputTarget(1, breakdown->getTargetMagnitude());

        break;
    }

    case SmootherChainCore::Vector3D: {
        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(3));
        engine->addChainNode(avgCover);
        engine->addInputTarget(2, avgCover->getTargetValue());
        engine->addInputTarget(3, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(7));
        engine->addChainNode(contCover);
        engine->addInputTarget(2, contCover->getTargetValue());
        engine->addInputTarget(3, contCover->getTargetCover());

        SmootherChainVector3DReconstruct *reconstructAvg =
                new SmootherChainVector3DReconstruct(stream, engine->getOutput(0),
                                                     engine->getOutput(1), engine->getOutput(2));
        engine->addChainNode(reconstructAvg);

        SmootherChainVector3DReconstruct *reconstructCont =
                new SmootherChainVector3DReconstruct(stream, engine->getOutput(4),
                                                     engine->getOutput(5), engine->getOutput(6));
        engine->addChainNode(reconstructCont);

        SmootherChainFixedTimeConventional *avgX =
                new SmootherChainFixedTimeConventional(stream, reconstructAvg->getTargetX());
        engine->addChainNode(avgX);
        engine->addInputTarget(3, avgX->getTargetCover());

        SmootherChainFixedTimeConventional *avgY =
                new SmootherChainFixedTimeConventional(stream, reconstructAvg->getTargetY());
        engine->addChainNode(avgY);
        engine->addInputTarget(3, avgY->getTargetCover());

        SmootherChainFixedTimeConventional *avgZ =
                new SmootherChainFixedTimeConventional(stream, reconstructAvg->getTargetZ());
        engine->addChainNode(avgZ);
        engine->addInputTarget(3, avgZ->getTargetCover());

        SmootherChainFixedTimeConventional *contX =
                new SmootherChainFixedTimeConventional(stream, reconstructCont->getTargetX());
        engine->addChainNode(contX);
        engine->addInputTarget(3, contX->getTargetCover());

        SmootherChainFixedTimeConventional *contY =
                new SmootherChainFixedTimeConventional(stream, reconstructCont->getTargetY());
        engine->addChainNode(contY);
        engine->addInputTarget(3, contY->getTargetCover());

        SmootherChainFixedTimeConventional *contZ =
                new SmootherChainFixedTimeConventional(stream, reconstructCont->getTargetZ());
        engine->addChainNode(contZ);
        engine->addInputTarget(3, contZ->getTargetCover());

        SmootherChainForward<2> *splitX =
                new SmootherChainForward<2>({avgX->getTargetValue(), contX->getTargetValue()});
        engine->addChainNode(splitX);

        SmootherChainForward<2> *splitY =
                new SmootherChainForward<2>({avgY->getTargetValue(), contY->getTargetValue()});
        engine->addChainNode(splitY);

        SmootherChainForward<2> *splitZ =
                new SmootherChainForward<2>({avgZ->getTargetValue(), contZ->getTargetValue()});
        engine->addChainNode(splitZ);

        SmootherChainVector3DBreakdown
                *breakdown = new SmootherChainVector3DBreakdown(stream, splitX, splitY, splitZ);
        engine->addChainNode(breakdown);
        engine->addInputTarget(0, breakdown->getTargetAzimuth());
        engine->addInputTarget(1, breakdown->getTargetElevation());
        engine->addInputTarget(2, breakdown->getTargetMagnitude());

        break;
    }

    case SmootherChainCore::BeersLawAbsorption: {
        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(1));
        engine->addChainNode(avgCover);
        engine->addInputTarget(4, avgCover->getTargetValue());
        engine->addInputTarget(5, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(3));
        engine->addChainNode(contCover);
        engine->addInputTarget(4, contCover->getTargetValue());
        engine->addInputTarget(5, contCover->getTargetCover());

        SmootherChainFixedTimeConventional
                *avgCombine = new SmootherChainFixedTimeConventional(stream, engine->getOutput(0));
        engine->addChainNode(avgCombine);

        SmootherChainForward<2> *splitValue =
                new SmootherChainForward<2>({avgCombine->getTargetValue(), engine->getOutput(2)});
        engine->addChainNode(splitValue);

        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(stream, splitValue);
        engine->addChainNode(calculate);

        SmootherChainFixedTimeDifference *diffL =
                new SmootherChainFixedTimeDifference(stream, calculate->getTargetStartL(),
                                                     calculate->getTargetEndL());
        engine->addChainNode(diffL);
        engine->addInputTarget(0, diffL->getTargetStart());
        engine->addInputTarget(1, diffL->getTargetEnd());

        SmootherChainFixedTimeDifference *diffI =
                new SmootherChainFixedTimeDifference(stream, calculate->getTargetStartI(),
                                                     calculate->getTargetEndI());
        engine->addChainNode(diffI);
        engine->addInputTarget(2, diffI->getTargetStart());
        engine->addInputTarget(3, diffI->getTargetEnd());

        break;
    }

    case SmootherChainCore::BeersLawAbsorptionInitial: {
        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(1));
        engine->addChainNode(avgCover);
        engine->addInputTarget(2, avgCover->getTargetValue());
        engine->addInputTarget(3, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(3));
        engine->addChainNode(contCover);
        engine->addInputTarget(2, contCover->getTargetValue());
        engine->addInputTarget(3, contCover->getTargetCover());

        SmootherChainFixedTimeConventional
                *avgCombine = new SmootherChainFixedTimeConventional(stream, engine->getOutput(0));
        engine->addChainNode(avgCombine);

        SmootherChainForward<2> *splitValue =
                new SmootherChainForward<2>({avgCombine->getTargetValue(), engine->getOutput(2)});
        engine->addChainNode(splitValue);

        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(stream, splitValue);
        engine->addChainNode(calculate);

        SmootherChainFixedTimeDifference *diffL =
                new SmootherChainFixedTimeDifference(stream, calculate->getTargetStartL(),
                                                     calculate->getTargetEndL());
        engine->addChainNode(diffL);
        engine->addInputTarget(0, diffL->getTargetStart());

        SmootherChainFixedTimeDifference *diffI =
                new SmootherChainFixedTimeDifference(stream, calculate->getTargetStartI(),
                                                     calculate->getTargetEndI());
        engine->addChainNode(diffI);
        engine->addInputTarget(1, diffI->getTargetStart());

        break;
    }

    case SmootherChainCore::Dewpoint: {
        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(1));
        engine->addChainNode(avgCover);
        engine->addInputTarget(2, avgCover->getTargetValue());
        engine->addInputTarget(3, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(3));
        engine->addChainNode(contCover);
        engine->addInputTarget(2, contCover->getTargetValue());
        engine->addInputTarget(3, contCover->getTargetCover());

        SmootherChainDewpoint
                *calculateAvg = new SmootherChainDewpoint(stream, engine->getOutput(0));
        engine->addChainNode(calculateAvg);

        SmootherChainDewpoint
                *calculateCont = new SmootherChainDewpoint(stream, engine->getOutput(2));
        engine->addChainNode(calculateCont);

        SmootherChainFixedTimeConventional *avgTemperature =
                new SmootherChainFixedTimeConventional(stream,
                                                       calculateAvg->getTargetTemperature());
        engine->addChainNode(avgTemperature);
        engine->addInputTarget(0, avgTemperature->getTargetValue());
        engine->addInputTarget(4, avgTemperature->getTargetCover());

        SmootherChainFixedTimeConventional *contTemperature =
                new SmootherChainFixedTimeConventional(stream,
                                                       calculateCont->getTargetTemperature());
        engine->addChainNode(contTemperature);
        engine->addInputTarget(0, contTemperature->getTargetValue());
        engine->addInputTarget(4, contTemperature->getTargetCover());

        SmootherChainFixedTimeConventional *avgRH =
                new SmootherChainFixedTimeConventional(stream, calculateAvg->getTargetRH());
        engine->addChainNode(avgRH);
        engine->addInputTarget(1, avgRH->getTargetValue());
        engine->addInputTarget(5, avgRH->getTargetCover());

        SmootherChainFixedTimeConventional *contRH =
                new SmootherChainFixedTimeConventional(stream, calculateCont->getTargetRH());
        engine->addChainNode(contRH);
        engine->addInputTarget(1, contRH->getTargetValue());
        engine->addInputTarget(5, contRH->getTargetCover());

        break;
    }

    case SmootherChainCore::RH: {
        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(1));
        engine->addChainNode(avgCover);
        engine->addInputTarget(2, avgCover->getTargetValue());
        engine->addInputTarget(3, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(3));
        engine->addChainNode(contCover);
        engine->addInputTarget(2, contCover->getTargetValue());
        engine->addInputTarget(3, contCover->getTargetCover());

        SmootherChainRH *calculateAvg = new SmootherChainRH(stream, engine->getOutput(0));
        engine->addChainNode(calculateAvg);

        SmootherChainRH *calculateCont = new SmootherChainRH(stream, engine->getOutput(2));
        engine->addChainNode(calculateCont);

        SmootherChainFixedTimeConventional *avgTemperature =
                new SmootherChainFixedTimeConventional(stream,
                                                       calculateAvg->getTargetTemperature());
        engine->addChainNode(avgTemperature);
        engine->addInputTarget(0, avgTemperature->getTargetValue());
        engine->addInputTarget(4, avgTemperature->getTargetCover());

        SmootherChainFixedTimeConventional *contTemperature =
                new SmootherChainFixedTimeConventional(stream,
                                                       calculateCont->getTargetTemperature());
        engine->addChainNode(contTemperature);
        engine->addInputTarget(0, contTemperature->getTargetValue());
        engine->addInputTarget(4, contTemperature->getTargetCover());

        SmootherChainFixedTimeConventional *avgDewpoint =
                new SmootherChainFixedTimeConventional(stream, calculateAvg->getTargetDewpoint());
        engine->addChainNode(avgDewpoint);
        engine->addInputTarget(1, avgDewpoint->getTargetValue());
        engine->addInputTarget(5, avgDewpoint->getTargetCover());

        SmootherChainFixedTimeConventional *contDewpoint =
                new SmootherChainFixedTimeConventional(stream, calculateCont->getTargetDewpoint());
        engine->addChainNode(contDewpoint);
        engine->addInputTarget(1, contDewpoint->getTargetValue());
        engine->addInputTarget(5, contDewpoint->getTargetCover());

        break;
    }

    case SmootherChainCore::RHExtrapolate: {
        SmootherChainFixedTimeConventional *avgCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(1));
        engine->addChainNode(avgCover);
        engine->addInputTarget(3, avgCover->getTargetValue());
        engine->addInputTarget(4, avgCover->getTargetCover());

        SmootherChainFixedTimeConventional *contCover =
                new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(3));
        engine->addChainNode(contCover);
        engine->addInputTarget(3, contCover->getTargetValue());
        engine->addInputTarget(4, contCover->getTargetCover());

        SmootherChainRHExtrapolate
                *calculateAvg = new SmootherChainRHExtrapolate(stream, engine->getOutput(0));
        engine->addChainNode(calculateAvg);

        SmootherChainRHExtrapolate
                *calculateCont = new SmootherChainRHExtrapolate(stream, engine->getOutput(2));
        engine->addChainNode(calculateCont);

        SmootherChainFixedTimeConventional *avgTemperatureIn =
                new SmootherChainFixedTimeConventional(stream,
                                                       calculateAvg->getTargetTemperatureIn());
        engine->addChainNode(avgTemperatureIn);
        engine->addInputTarget(0, avgTemperatureIn->getTargetValue());
        engine->addInputTarget(5, avgTemperatureIn->getTargetCover());

        SmootherChainFixedTimeConventional *contTemperatureIn =
                new SmootherChainFixedTimeConventional(stream,
                                                       calculateCont->getTargetTemperatureIn());
        engine->addChainNode(contTemperatureIn);
        engine->addInputTarget(0, contTemperatureIn->getTargetValue());
        engine->addInputTarget(5, contTemperatureIn->getTargetCover());

        SmootherChainFixedTimeConventional *avgRHIn =
                new SmootherChainFixedTimeConventional(stream, calculateAvg->getTargetRHIn());
        engine->addChainNode(avgRHIn);
        engine->addInputTarget(1, avgRHIn->getTargetValue());
        engine->addInputTarget(6, avgRHIn->getTargetCover());

        SmootherChainFixedTimeConventional *contRHIn =
                new SmootherChainFixedTimeConventional(stream, calculateCont->getTargetRHIn());
        engine->addChainNode(contRHIn);
        engine->addInputTarget(1, contRHIn->getTargetValue());
        engine->addInputTarget(6, contRHIn->getTargetCover());

        SmootherChainFixedTimeConventional *avgTemperatureOut =
                new SmootherChainFixedTimeConventional(stream,
                                                       calculateAvg->getTargetTemperatureOut());
        engine->addChainNode(avgTemperatureOut);
        engine->addInputTarget(2, avgTemperatureOut->getTargetValue());
        engine->addInputTarget(7, avgTemperatureOut->getTargetCover());

        SmootherChainFixedTimeConventional *contTemperatureOut =
                new SmootherChainFixedTimeConventional(stream,
                                                       calculateCont->getTargetTemperatureOut());
        engine->addChainNode(contTemperatureOut);
        engine->addInputTarget(2, contTemperatureOut->getTargetValue());
        engine->addInputTarget(7, contTemperatureOut->getTargetCover());

        break;
    }

    }
}

SmootherChainCore::HandlerMode EditingChainCoreFullCalculate::smootherHandler(SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General:
    case SmootherChainCore::Vector2D:
    case SmootherChainCore::Vector3D:
    case SmootherChainCore::Dewpoint:
    case SmootherChainCore::RH:
    case SmootherChainCore::RHExtrapolate:
        return SmootherChainCore::Repeatable;
    case SmootherChainCore::Difference:
    case SmootherChainCore::DifferenceInitial:
    case SmootherChainCore::BeersLawAbsorption:
    case SmootherChainCore::BeersLawAbsorptionInitial:
        /* Continuous is repeatable so the engine needs to deal with that */
        return SmootherChainCore::Handled;
    }
    Q_ASSERT(false);
    return SmootherChainCore::Unhandled;
}

bool EditingChainCoreFullCalculate::flagsHandler()
{ return true; }

void EditingChainCoreFullCalculate::createFlags(SmootherChainCoreEngineInterface *engine)
{
    SequenceName avgCoverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
    SmootherChainTarget *avgCover = engine->addNewOutput(avgCoverUnit);

    SequenceName contUnit(engine->getBaseUnit());
    contUnit.setArchive(archiveCont);

    SmootherChainFixedTimeFlags *avgFlags =
            new SmootherChainFixedTimeFlags(childInterval(), NULL, engine->getFlagsOutput(0),
                                            avgCover);
    engine->addChainNode(avgFlags);
    engine->addFlagsInputTarget(0, avgFlags->getTargetFlags());
    engine->addNewInput(avgCoverUnit, avgFlags->getTargetCover());

    SmootherChainFixedTimeFlags *contFlags =
            new SmootherChainFixedTimeFlags(childInterval(), contGap(),
                                            engine->addNewFlagsOutput(contUnit),
                                            engine->addNewOutput(contUnit.withFlavor(
                                                    SequenceName::flavor_cover)));
    engine->addChainNode(contFlags);
    engine->addFlagsInputTarget(0, contFlags->getTargetFlags());
    engine->addInputTarget(0, contFlags->getTargetCover());
}

void EditingChainCoreFullCalculate::deserializeFlags(QDataStream &stream,
                                                     SmootherChainCoreEngineInterface *engine)
{
    SmootherChainFixedTimeFlags *avgFlags =
            new SmootherChainFixedTimeFlags(stream, engine->getFlagsOutput(0),
                                            engine->getOutput(0));
    engine->addChainNode(avgFlags);
    engine->addFlagsInputTarget(0, avgFlags->getTargetFlags());
    engine->addInputTarget(0, avgFlags->getTargetCover());

    SmootherChainFixedTimeFlags *contFlags =
            new SmootherChainFixedTimeFlags(stream, engine->getFlagsOutput(1),
                                            engine->getOutput(1));
    engine->addChainNode(contFlags);
    engine->addFlagsInputTarget(0, contFlags->getTargetFlags());
    engine->addInputTarget(0, contFlags->getTargetCover());
}

bool EditingChainCoreFullCalculate::generatedOutput(const SequenceName &unit)
{
    return unit.hasFlavor(SequenceName::flavor_cover);
}

Variant::Root EditingChainCoreFullCalculate::getProcessingMetadata(double time) const
{
    Variant::Root meta;
    meta.write().hash("Type").setString("FullCalculate");
    if (requiredCover != NULL) {
        meta.write()
            .hash("Parameters")
            .hash("RequiredCoverage")
            .setDouble(requiredCover->get(time));
    }
    if (interval != NULL) {
        meta.write().hash("Parameters").hash("Interval").setString(interval->describe(time));
    }
    return meta;
}

QSet<double> EditingChainCoreFullCalculate::getProcessingMetadataBreaks() const
{
    QSet<double> result;
    if (requiredCover != NULL)
        result |= requiredCover->getChangedPoints();
    if (interval != NULL)
        result |= interval->getChangedPoints();
    return result;
}


DynamicDouble *EditingChainCoreFinalStatistics::childCover() const
{ return requiredCover != NULL ? requiredCover->clone() : NULL; }

DynamicTimeInterval *EditingChainCoreFinalStatistics::childInterval() const
{ return interval != NULL ? interval->clone() : NULL; }

EditingChainCoreFinalStatistics::EditingChainCoreFinalStatistics(DynamicTimeInterval *setInterval,
                                                                 DynamicDouble *setRequiredCover)
        : requiredCover(setRequiredCover), interval(setInterval)
{ }

EditingChainCoreFinalStatistics::~EditingChainCoreFinalStatistics()
{
    if (interval != NULL)
        delete interval;
    if (requiredCover != NULL)
        delete requiredCover;
}

void EditingChainCoreFinalStatistics::createSmoother(SmootherChainCoreEngineInterface *engine,
                                                     SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General: {
        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       NULL, engine->addNewGeneralOutput(
                                engine->getBaseUnit().withFlavor(SequenceName::flavor_stats)));
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetValue());
        engine->addNewInput(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover),
                            avg->getTargetCover());

        engine->getOutput(0)->endData();
        break;
    }

    case SmootherChainCore::Difference:
    case SmootherChainCore::DifferenceInitial:
        engine->getOutput(0)->endData();
        engine->getOutput(1)->endData();
        break;

    case SmootherChainCore::Vector2D: {
        SequenceName avgCoverUnit(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover));
        SmootherChainFixedTimeVectorStatistics *avg =
                new SmootherChainFixedTimeVectorStatistics(childCover(), childInterval(), NULL,
                                                           NULL, NULL, engine->addNewGeneralOutput(
                                engine->getBaseUnit(1).withFlavor(SequenceName::flavor_stats)));
        engine->addChainNode(avg);
        engine->addInputTarget(1, avg->getTargetValue());
        engine->addNewInput(avgCoverUnit, avg->getTargetCover());

        SmootherChainVector2DReconstruct *reconstruct =
                new SmootherChainVector2DReconstruct(NULL, avg->getTargetVectoredMagnitude());
        engine->addChainNode(reconstruct);

        SmootherChainFixedTimeConventional *avgX =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstruct->getTargetX());
        engine->addChainNode(avgX);
        engine->addInputTarget(2, avgX->getTargetCover());

        SmootherChainFixedTimeConventional *avgY =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstruct->getTargetY());
        engine->addChainNode(avgY);
        engine->addInputTarget(2, avgY->getTargetCover());

        SmootherChainVector2DBreakdown *breakdown =
                new SmootherChainVector2DBreakdown(avgX->getTargetValue(), avgY->getTargetValue());
        engine->addChainNode(breakdown);

        engine->addInputTarget(0, breakdown->getTargetDirection());
        engine->addInputTarget(1, breakdown->getTargetMagnitude());

        engine->getOutput(0)->endData();
        engine->getOutput(1)->endData();
        break;
    }

    case SmootherChainCore::Vector3D: {
        SequenceName avgCoverUnit(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover));
        SmootherChainFixedTimeVectorStatistics *avg =
                new SmootherChainFixedTimeVectorStatistics(childCover(), childInterval(), NULL,
                                                           NULL, NULL, engine->addNewGeneralOutput(
                                engine->getBaseUnit(2).withFlavor(SequenceName::flavor_stats)));
        engine->addChainNode(avg);
        engine->addInputTarget(1, avg->getTargetValue());
        engine->addNewInput(avgCoverUnit, avg->getTargetCover());

        SmootherChainVector3DReconstruct *reconstruct =
                new SmootherChainVector3DReconstruct(NULL, NULL, avg->getTargetVectoredMagnitude());
        engine->addChainNode(reconstruct);

        SmootherChainFixedTimeConventional *avgX =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstruct->getTargetX());
        engine->addChainNode(avgX);
        engine->addInputTarget(3, avgX->getTargetCover());

        SmootherChainFixedTimeConventional *avgY =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstruct->getTargetY());
        engine->addChainNode(avgY);
        engine->addInputTarget(3, avgY->getTargetCover());

        SmootherChainFixedTimeConventional *avgZ =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstruct->getTargetZ());
        engine->addChainNode(avgZ);
        engine->addInputTarget(3, avgZ->getTargetCover());

        SmootherChainVector3DBreakdown *breakdown =
                new SmootherChainVector3DBreakdown(avgX->getTargetValue(), avgY->getTargetValue(),
                                                   avgZ->getTargetValue());
        engine->addChainNode(breakdown);

        engine->addInputTarget(0, breakdown->getTargetAzimuth());
        engine->addInputTarget(1, breakdown->getTargetElevation());
        engine->addInputTarget(2, breakdown->getTargetMagnitude());

        engine->getOutput(0)->endData();
        engine->getOutput(1)->endData();
        engine->getOutput(2)->endData();
        break;
    }

    case SmootherChainCore::BeersLawAbsorption: {
        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       NULL, engine->addNewGeneralOutput(
                                engine->getBaseUnit().withFlavor(SequenceName::flavor_stats)));
        engine->addChainNode(avg);
        engine->addNewInput(engine->getBaseUnit(), avg->getTargetValue());
        engine->addNewInput(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover),
                            avg->getTargetCover());

        engine->getOutput(0)->endData();
        break;
    }

    case SmootherChainCore::BeersLawAbsorptionInitial:
    case SmootherChainCore::Dewpoint:
    case SmootherChainCore::RH: {
        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       NULL, engine->addNewGeneralOutput(
                                engine->getBaseUnit().withFlavor(SequenceName::flavor_stats)));
        engine->addChainNode(avg);
        engine->addNewInput(engine->getBaseUnit(), avg->getTargetValue());
        engine->addNewInput(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover),
                            avg->getTargetCover());

        engine->getOutput(0)->endData();
        break;
    }

    case SmootherChainCore::RHExtrapolate: {
        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       NULL, engine->addNewGeneralOutput(
                                engine->getBaseUnit().withFlavor(SequenceName::flavor_stats)));
        engine->addChainNode(avg);
        engine->addNewInput(engine->getBaseUnit(), avg->getTargetValue());
        engine->addNewInput(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover),
                            avg->getTargetCover());

        engine->getOutput(0)->endData();
        break;
    }

    }
}

void EditingChainCoreFinalStatistics::deserializeSmoother(QDataStream &stream,
                                                          SmootherChainCoreEngineInterface *engine,
                                                          SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General: {
        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(stream, NULL, NULL,
                                                       engine->getGeneralOutput(0));
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetValue());
        engine->addInputTarget(1, avg->getTargetCover());

        break;
    }

    case SmootherChainCore::Difference:
    case SmootherChainCore::DifferenceInitial:
        break;

    case SmootherChainCore::Vector2D: {
        SmootherChainFixedTimeVectorStatistics *avg =
                new SmootherChainFixedTimeVectorStatistics(stream, NULL, NULL,
                                                           engine->getGeneralOutput(0));
        engine->addChainNode(avg);
        engine->addInputTarget(1, avg->getTargetValue());
        engine->addInputTarget(2, avg->getTargetCover());

        SmootherChainVector2DReconstruct *reconstruct =
                new SmootherChainVector2DReconstruct(stream, NULL,
                                                     avg->getTargetVectoredMagnitude());
        engine->addChainNode(reconstruct);

        SmootherChainFixedTimeConventional
                *avgX = new SmootherChainFixedTimeConventional(stream, reconstruct->getTargetX());
        engine->addChainNode(avgX);
        engine->addInputTarget(2, avgX->getTargetCover());

        SmootherChainFixedTimeConventional
                *avgY = new SmootherChainFixedTimeConventional(stream, reconstruct->getTargetY());
        engine->addChainNode(avgY);
        engine->addInputTarget(2, avgY->getTargetCover());

        SmootherChainVector2DBreakdown *breakdown =
                new SmootherChainVector2DBreakdown(stream, avgX->getTargetValue(),
                                                   avgY->getTargetValue());
        engine->addChainNode(breakdown);

        engine->addInputTarget(0, breakdown->getTargetDirection());
        engine->addInputTarget(1, breakdown->getTargetMagnitude());

        break;
    }

    case SmootherChainCore::Vector3D: {
        SmootherChainFixedTimeVectorStatistics *avg =
                new SmootherChainFixedTimeVectorStatistics(stream, NULL, NULL,
                                                           engine->getGeneralOutput(0));
        engine->addChainNode(avg);
        engine->addInputTarget(1, avg->getTargetValue());
        engine->addInputTarget(3, avg->getTargetCover());

        SmootherChainVector3DReconstruct *reconstruct =
                new SmootherChainVector3DReconstruct(stream, NULL, NULL,
                                                     avg->getTargetVectoredMagnitude());
        engine->addChainNode(reconstruct);

        SmootherChainFixedTimeConventional
                *avgX = new SmootherChainFixedTimeConventional(stream, reconstruct->getTargetX());
        engine->addChainNode(avgX);
        engine->addInputTarget(3, avgX->getTargetCover());

        SmootherChainFixedTimeConventional
                *avgY = new SmootherChainFixedTimeConventional(stream, reconstruct->getTargetY());
        engine->addChainNode(avgY);
        engine->addInputTarget(3, avgY->getTargetCover());

        SmootherChainFixedTimeConventional
                *avgZ = new SmootherChainFixedTimeConventional(stream, reconstruct->getTargetZ());
        engine->addChainNode(avgZ);
        engine->addInputTarget(3, avgZ->getTargetCover());

        SmootherChainVector3DBreakdown *breakdown =
                new SmootherChainVector3DBreakdown(stream, avgX->getTargetValue(),
                                                   avgY->getTargetValue(), avgZ->getTargetValue());
        engine->addChainNode(breakdown);

        engine->addInputTarget(0, breakdown->getTargetAzimuth());
        engine->addInputTarget(1, breakdown->getTargetElevation());
        engine->addInputTarget(2, breakdown->getTargetMagnitude());

        break;
    }

    case SmootherChainCore::BeersLawAbsorption: {
        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(stream, NULL, NULL,
                                                       engine->getGeneralOutput(0));
        engine->addChainNode(avg);
        engine->addInputTarget(4, avg->getTargetValue());
        engine->addInputTarget(5, avg->getTargetCover());

        break;
    }

    case SmootherChainCore::BeersLawAbsorptionInitial:
    case SmootherChainCore::Dewpoint:
    case SmootherChainCore::RH: {
        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(stream, NULL, NULL,
                                                       engine->getGeneralOutput(0));
        engine->addChainNode(avg);
        engine->addInputTarget(2, avg->getTargetValue());
        engine->addInputTarget(3, avg->getTargetCover());

        break;
    }

    case SmootherChainCore::RHExtrapolate: {
        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(stream, NULL, NULL,
                                                       engine->getGeneralOutput(0));
        engine->addChainNode(avg);
        engine->addInputTarget(3, avg->getTargetValue());
        engine->addInputTarget(4, avg->getTargetCover());

        break;
    }

    }
}

SmootherChainCore::HandlerMode
EditingChainCoreFinalStatistics::smootherHandler(SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General:
    case SmootherChainCore::Vector2D:
    case SmootherChainCore::Vector3D:
    case SmootherChainCore::Dewpoint:
    case SmootherChainCore::RH:
    case SmootherChainCore::RHExtrapolate:
    case SmootherChainCore::Difference:
    case SmootherChainCore::DifferenceInitial:
    case SmootherChainCore::BeersLawAbsorption:
    case SmootherChainCore::BeersLawAbsorptionInitial:
        return SmootherChainCore::Repeatable;
    }
    Q_ASSERT(false);
    return SmootherChainCore::Unhandled;
}

bool EditingChainCoreFinalStatistics::flagsHandler()
{ return false; }

void EditingChainCoreFinalStatistics::createFlags(SmootherChainCoreEngineInterface *engine)
{
    engine->getFlagsOutput(0)->endData();
}

void EditingChainCoreFinalStatistics::deserializeFlags(QDataStream &stream,
                                                       SmootherChainCoreEngineInterface *engine)
{
    Q_UNUSED(stream);
    Q_UNUSED(engine);
}

bool EditingChainCoreFinalStatistics::generatedOutput(const SequenceName &unit)
{
    return unit.hasFlavor(SequenceName::flavor_stats);
}

Variant::Root EditingChainCoreFinalStatistics::getProcessingMetadata(double time) const
{
    Variant::Root meta;
    meta.write().hash("Type").setString("FinalStatistics");
    if (requiredCover != NULL) {
        meta.write()
            .hash("Parameters")
            .hash("RequiredCoverage")
            .setDouble(requiredCover->get(time));
    }
    if (interval != NULL) {
        meta.write().hash("Parameters").hash("Interval").setString(interval->describe(time));
    }
    return meta;
}

QSet<double> EditingChainCoreFinalStatistics::getProcessingMetadataBreaks() const
{
    QSet<double> result;
    if (requiredCover != NULL)
        result |= requiredCover->getChangedPoints();
    if (interval != NULL)
        result |= interval->getChangedPoints();
    return result;
}


DynamicDouble *EditingChainCoreContinuousRecalculate::childCover() const
{ return requiredCover != NULL ? requiredCover->clone() : NULL; }

DynamicTimeInterval *EditingChainCoreContinuousRecalculate::childInterval() const
{ return interval != NULL ? interval->clone() : NULL; }

EditingChainCoreContinuousRecalculate::EditingChainCoreContinuousRecalculate(DynamicTimeInterval *setInterval,
                                                                             DynamicDouble *setRequiredCover)
        : requiredCover(setRequiredCover), interval(setInterval)
{ }

EditingChainCoreContinuousRecalculate::~EditingChainCoreContinuousRecalculate()
{
    if (interval != NULL)
        delete interval;
    if (requiredCover != NULL)
        delete requiredCover;
}

void EditingChainCoreContinuousRecalculate::createSmoother(SmootherChainCoreEngineInterface *engine,
                                                           SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General: {
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);

        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       engine->getOutput(0), outputCover);
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetValue());
        engine->addNewInput(coverUnit, avg->getTargetCover());

        break;
    }

    case SmootherChainCore::Difference: {
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);

        SmootherChainFixedTimeDifference *avg =
                new SmootherChainFixedTimeDifference(childInterval(), NULL, engine->getOutput(0),
                                                     engine->getOutput(1), outputCover);
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetStart());
        engine->addInputTarget(1, avg->getTargetEnd());

        break;
    }

    case SmootherChainCore::DifferenceInitial: {
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);

        SmootherChainFixedTimeDifference *avg =
                new SmootherChainFixedTimeDifference(childInterval(), NULL, engine->getOutput(0),
                                                     engine->getOutput(1), outputCover);
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetStart());
        avg->getTargetEnd()->endData();

        break;
    }

    case SmootherChainCore::Vector2D: {
        SequenceName coverUnit(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);

        SmootherChainFixedTimeConventional *cover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputCover);
        engine->addChainNode(cover);
        engine->addInputTarget(1, cover->getTargetValue());
        engine->addNewInput(coverUnit, cover->getTargetCover());

        SmootherChainVector2DReconstruct *reconstruct =
                new SmootherChainVector2DReconstruct(engine->getOutput(0), engine->getOutput(1));
        engine->addChainNode(reconstruct);

        SmootherChainFixedTimeConventional *avgX =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstruct->getTargetX());
        engine->addChainNode(avgX);
        engine->addInputTarget(2, avgX->getTargetCover());

        SmootherChainFixedTimeConventional *avgY =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstruct->getTargetY());
        engine->addChainNode(avgY);
        engine->addInputTarget(2, avgY->getTargetCover());

        SmootherChainVector2DBreakdown *breakdown =
                new SmootherChainVector2DBreakdown(avgX->getTargetValue(), avgY->getTargetValue());
        engine->addChainNode(breakdown);
        engine->addInputTarget(0, breakdown->getTargetDirection());
        engine->addInputTarget(1, breakdown->getTargetMagnitude());

        break;
    }

    case SmootherChainCore::Vector3D: {
        SequenceName coverUnit(engine->getBaseUnit(2).withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);

        SmootherChainFixedTimeConventional *cover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputCover);
        engine->addChainNode(cover);
        engine->addInputTarget(2, cover->getTargetValue());
        engine->addNewInput(coverUnit, cover->getTargetCover());

        SmootherChainVector3DReconstruct *reconstruct =
                new SmootherChainVector3DReconstruct(engine->getOutput(0), engine->getOutput(1),
                                                     engine->getOutput(2));
        engine->addChainNode(reconstruct);

        SmootherChainFixedTimeConventional *avgX =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstruct->getTargetX());
        engine->addChainNode(avgX);
        engine->addInputTarget(3, avgX->getTargetCover());

        SmootherChainFixedTimeConventional *avgY =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstruct->getTargetY());
        engine->addChainNode(avgY);
        engine->addInputTarget(3, avgY->getTargetCover());

        SmootherChainFixedTimeConventional *avgZ =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       reconstruct->getTargetZ());
        engine->addChainNode(avgZ);
        engine->addInputTarget(3, avgZ->getTargetCover());

        SmootherChainVector3DBreakdown *breakdown =
                new SmootherChainVector3DBreakdown(avgX->getTargetValue(), avgY->getTargetValue(),
                                                   avgZ->getTargetValue());
        engine->addChainNode(breakdown);
        engine->addInputTarget(0, breakdown->getTargetAzimuth());
        engine->addInputTarget(1, breakdown->getTargetElevation());
        engine->addInputTarget(2, breakdown->getTargetMagnitude());

        break;
    }

    case SmootherChainCore::BeersLawAbsorption: {
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);

        SmootherChainFixedTimeConventional *cover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputCover);
        engine->addChainNode(cover);
        engine->addNewInput(engine->getBaseUnit(), cover->getTargetValue());
        engine->addNewInput(coverUnit, cover->getTargetCover());

        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       engine->getOutput(0));
        engine->addChainNode(avg);
        avg->getTargetCover()->endData();

        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(avg->getTargetValue());
        engine->addChainNode(calculate);

        SmootherChainFixedTimeDifference *diffL =
                new SmootherChainFixedTimeDifference(childInterval(),
                                                     new DynamicTimeInterval::Constant(
                                                             Time::Millisecond, 1),
                                                     calculate->getTargetStartL(),
                                                     calculate->getTargetEndL());
        engine->addChainNode(diffL);
        engine->addInputTarget(0, diffL->getTargetStart());
        engine->addInputTarget(1, diffL->getTargetEnd());

        SmootherChainFixedTimeDifference *diffI =
                new SmootherChainFixedTimeDifference(childInterval(),
                                                     new DynamicTimeInterval::Constant(
                                                             Time::Millisecond, 1),
                                                     calculate->getTargetStartI(),
                                                     calculate->getTargetEndI());
        engine->addChainNode(diffI);
        engine->addInputTarget(2, diffI->getTargetStart());
        engine->addInputTarget(3, diffI->getTargetEnd());

        break;
    }

    case SmootherChainCore::BeersLawAbsorptionInitial: {
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);

        SmootherChainFixedTimeConventional *cover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputCover);
        engine->addChainNode(cover);
        engine->addNewInput(engine->getBaseUnit(), cover->getTargetValue());
        engine->addNewInput(coverUnit, cover->getTargetCover());

        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       engine->getOutput(0));
        engine->addChainNode(avg);
        avg->getTargetCover()->endData();

        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(avg->getTargetValue());
        engine->addChainNode(calculate);

        SmootherChainFixedTimeDifference *diffL =
                new SmootherChainFixedTimeDifference(childInterval(),
                                                     new DynamicTimeInterval::Constant(
                                                             Time::Millisecond, 1),
                                                     calculate->getTargetStartL(),
                                                     calculate->getTargetEndL());
        engine->addChainNode(diffL);
        engine->addInputTarget(0, diffL->getTargetStart());
        diffL->getTargetEnd()->endData();

        SmootherChainFixedTimeDifference *diffI =
                new SmootherChainFixedTimeDifference(childInterval(),
                                                     new DynamicTimeInterval::Constant(
                                                             Time::Millisecond, 1),
                                                     calculate->getTargetStartI(),
                                                     calculate->getTargetEndI());
        engine->addChainNode(diffI);
        engine->addInputTarget(1, diffI->getTargetStart());
        diffI->getTargetEnd()->endData();

        break;
    }

    case SmootherChainCore::Dewpoint: {
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);

        SmootherChainFixedTimeConventional *cover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputCover);
        engine->addChainNode(cover);
        engine->addNewInput(engine->getBaseUnit(), cover->getTargetValue());
        engine->addNewInput(coverUnit, cover->getTargetCover());

        SmootherChainDewpoint *calculate = new SmootherChainDewpoint(engine->getOutput(0),
                                                                     engine->getOptions()
                                                                           .hash("AlwaysWater")
                                                                           .toBool());
        engine->addChainNode(calculate);

        SmootherChainFixedTimeConventional *avgTemperature =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculate->getTargetTemperature());
        engine->addChainNode(avgTemperature);
        engine->addInputTarget(0, avgTemperature->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover),
                            avgTemperature->getTargetCover());

        SmootherChainFixedTimeConventional *avgRH =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculate->getTargetRH());
        engine->addChainNode(avgRH);
        engine->addInputTarget(1, avgRH->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(2).withFlavor(SequenceName::flavor_cover),
                            avgRH->getTargetCover());
        break;
    }

    case SmootherChainCore::RH: {
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);

        SmootherChainFixedTimeConventional *cover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputCover);
        engine->addChainNode(cover);
        engine->addNewInput(engine->getBaseUnit(), cover->getTargetValue());
        engine->addNewInput(coverUnit, cover->getTargetCover());

        SmootherChainRH *calculate = new SmootherChainRH(engine->getOutput(0), engine->getOptions()
                                                                                     .hash("AlwaysWater")
                                                                                     .toBool());
        engine->addChainNode(calculate);

        SmootherChainFixedTimeConventional *avgTemperature =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculate->getTargetTemperature());
        engine->addChainNode(avgTemperature);
        engine->addInputTarget(0, avgTemperature->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover),
                            avgTemperature->getTargetCover());

        SmootherChainFixedTimeConventional *avgDewpoint =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculate->getTargetDewpoint());
        engine->addChainNode(avgDewpoint);
        engine->addInputTarget(1, avgDewpoint->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(2).withFlavor(SequenceName::flavor_cover),
                            avgDewpoint->getTargetCover());
        break;
    }

    case SmootherChainCore::RHExtrapolate: {
        SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
        SmootherChainTarget *outputCover = engine->addNewOutput(coverUnit);

        SmootherChainFixedTimeConventional *cover =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL, NULL,
                                                       outputCover);
        engine->addChainNode(cover);
        engine->addNewInput(engine->getBaseUnit(), cover->getTargetValue());
        engine->addNewInput(coverUnit, cover->getTargetCover());

        SmootherChainRHExtrapolate *calculate = new SmootherChainRHExtrapolate(engine->getOutput(0),
                                                                               engine->getOptions()
                                                                                     .hash("AlwaysWater")
                                                                                     .toBool());
        engine->addChainNode(calculate);

        SmootherChainFixedTimeConventional *avgTemperatureIn =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculate->getTargetTemperatureIn());
        engine->addChainNode(avgTemperatureIn);
        engine->addInputTarget(0, avgTemperatureIn->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(1).withFlavor(SequenceName::flavor_cover),
                            avgTemperatureIn->getTargetCover());

        SmootherChainFixedTimeConventional *avgRHIn =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculate->getTargetRHIn());
        engine->addChainNode(avgRHIn);
        engine->addInputTarget(1, avgRHIn->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(2).withFlavor(SequenceName::flavor_cover),
                            avgRHIn->getTargetCover());

        SmootherChainFixedTimeConventional *avgTemperatureOut =
                new SmootherChainFixedTimeConventional(childCover(), childInterval(), NULL,
                                                       calculate->getTargetTemperatureOut());
        engine->addChainNode(avgTemperatureOut);
        engine->addInputTarget(2, avgTemperatureOut->getTargetValue());
        engine->addNewInput(engine->getBaseUnit(3).withFlavor(SequenceName::flavor_cover),
                            avgTemperatureOut->getTargetCover());
        break;
    }

    }
}

void EditingChainCoreContinuousRecalculate::deserializeSmoother(QDataStream &stream,
                                                                SmootherChainCoreEngineInterface *engine,
                                                                SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General: {
        SmootherChainFixedTimeConventional *avg =
                new SmootherChainFixedTimeConventional(stream, engine->getOutput(0),
                                                       engine->getOutput(1));
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetValue());
        engine->addInputTarget(1, avg->getTargetCover());

        break;
    }

    case SmootherChainCore::Difference: {
        SmootherChainFixedTimeDifference *avg =
                new SmootherChainFixedTimeDifference(stream, engine->getOutput(0),
                                                     engine->getOutput(1), engine->getOutput(2));
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetStart());
        engine->addInputTarget(1, avg->getTargetEnd());

        break;
    }

    case SmootherChainCore::DifferenceInitial: {
        SmootherChainFixedTimeDifference *avg =
                new SmootherChainFixedTimeDifference(stream, engine->getOutput(0),
                                                     engine->getOutput(1), engine->getOutput(2));
        engine->addChainNode(avg);
        engine->addInputTarget(0, avg->getTargetStart());

        break;
    }

    case SmootherChainCore::Vector2D: {
        SmootherChainFixedTimeConventional
                *cover = new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(2));
        engine->addChainNode(cover);
        engine->addInputTarget(1, cover->getTargetValue());
        engine->addInputTarget(2, cover->getTargetCover());

        SmootherChainVector2DReconstruct *reconstruct =
                new SmootherChainVector2DReconstruct(stream, engine->getOutput(0),
                                                     engine->getOutput(1));
        engine->addChainNode(reconstruct);

        SmootherChainFixedTimeConventional
                *avgX = new SmootherChainFixedTimeConventional(stream, reconstruct->getTargetX());
        engine->addChainNode(avgX);
        engine->addInputTarget(2, avgX->getTargetCover());

        SmootherChainFixedTimeConventional
                *avgY = new SmootherChainFixedTimeConventional(stream, reconstruct->getTargetY());
        engine->addChainNode(avgY);
        engine->addInputTarget(2, avgY->getTargetCover());

        SmootherChainVector2DBreakdown *breakdown =
                new SmootherChainVector2DBreakdown(stream, avgX->getTargetValue(),
                                                   avgY->getTargetValue());
        engine->addChainNode(breakdown);
        engine->addInputTarget(0, breakdown->getTargetDirection());
        engine->addInputTarget(1, breakdown->getTargetMagnitude());

        break;
    }

    case SmootherChainCore::Vector3D: {
        SmootherChainFixedTimeConventional
                *cover = new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(3));
        engine->addChainNode(cover);
        engine->addInputTarget(2, cover->getTargetValue());
        engine->addInputTarget(3, cover->getTargetCover());

        SmootherChainVector3DReconstruct *reconstruct =
                new SmootherChainVector3DReconstruct(stream, engine->getOutput(0),
                                                     engine->getOutput(1), engine->getOutput(2));
        engine->addChainNode(reconstruct);

        SmootherChainFixedTimeConventional
                *avgX = new SmootherChainFixedTimeConventional(stream, reconstruct->getTargetX());
        engine->addChainNode(avgX);
        engine->addInputTarget(3, avgX->getTargetCover());

        SmootherChainFixedTimeConventional
                *avgY = new SmootherChainFixedTimeConventional(stream, reconstruct->getTargetY());
        engine->addChainNode(avgY);
        engine->addInputTarget(3, avgY->getTargetCover());

        SmootherChainFixedTimeConventional
                *avgZ = new SmootherChainFixedTimeConventional(stream, reconstruct->getTargetZ());
        engine->addChainNode(avgZ);
        engine->addInputTarget(3, avgZ->getTargetCover());

        SmootherChainVector3DBreakdown *breakdown =
                new SmootherChainVector3DBreakdown(stream, avgX->getTargetValue(),
                                                   avgY->getTargetValue(), avgZ->getTargetValue());
        engine->addChainNode(breakdown);
        engine->addInputTarget(0, breakdown->getTargetAzimuth());
        engine->addInputTarget(1, breakdown->getTargetElevation());
        engine->addInputTarget(2, breakdown->getTargetMagnitude());

        break;
    }

    case SmootherChainCore::BeersLawAbsorption: {
        SmootherChainFixedTimeConventional
                *cover = new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(1));
        engine->addChainNode(cover);
        engine->addInputTarget(4, cover->getTargetValue());
        engine->addInputTarget(5, cover->getTargetCover());

        SmootherChainFixedTimeConventional
                *avg = new SmootherChainFixedTimeConventional(stream, engine->getOutput(0));
        engine->addChainNode(avg);

        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(stream, avg->getTargetValue());
        engine->addChainNode(calculate);

        SmootherChainFixedTimeDifference *diffL =
                new SmootherChainFixedTimeDifference(stream, calculate->getTargetStartL(),
                                                     calculate->getTargetEndL());
        engine->addChainNode(diffL);
        engine->addInputTarget(0, diffL->getTargetStart());
        engine->addInputTarget(1, diffL->getTargetEnd());

        SmootherChainFixedTimeDifference *diffI =
                new SmootherChainFixedTimeDifference(stream, calculate->getTargetStartI(),
                                                     calculate->getTargetEndI());
        engine->addChainNode(diffI);
        engine->addInputTarget(2, diffI->getTargetStart());
        engine->addInputTarget(3, diffI->getTargetEnd());

        break;
    }

    case SmootherChainCore::BeersLawAbsorptionInitial: {
        SmootherChainFixedTimeConventional
                *cover = new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(1));
        engine->addChainNode(cover);
        engine->addInputTarget(2, cover->getTargetValue());
        engine->addInputTarget(3, cover->getTargetCover());

        SmootherChainFixedTimeConventional
                *avg = new SmootherChainFixedTimeConventional(stream, engine->getOutput(0));
        engine->addChainNode(avg);

        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(stream, avg->getTargetValue());
        engine->addChainNode(calculate);

        SmootherChainFixedTimeDifference *diffL =
                new SmootherChainFixedTimeDifference(stream, calculate->getTargetStartL(),
                                                     calculate->getTargetEndL());
        engine->addChainNode(diffL);
        engine->addInputTarget(0, diffL->getTargetStart());

        SmootherChainFixedTimeDifference *diffI =
                new SmootherChainFixedTimeDifference(stream, calculate->getTargetStartI(),
                                                     calculate->getTargetEndI());
        engine->addChainNode(diffI);
        engine->addInputTarget(1, diffI->getTargetStart());

        break;
    }

    case SmootherChainCore::Dewpoint: {
        SmootherChainFixedTimeConventional
                *cover = new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(1));
        engine->addChainNode(cover);
        engine->addInputTarget(2, cover->getTargetValue());
        engine->addInputTarget(3, cover->getTargetCover());

        SmootherChainDewpoint *calculate = new SmootherChainDewpoint(stream, engine->getOutput(0));
        engine->addChainNode(calculate);

        SmootherChainFixedTimeConventional *avgTemperature =
                new SmootherChainFixedTimeConventional(stream, calculate->getTargetTemperature());
        engine->addChainNode(avgTemperature);
        engine->addInputTarget(0, avgTemperature->getTargetValue());
        engine->addInputTarget(4, avgTemperature->getTargetCover());

        SmootherChainFixedTimeConventional
                *avgRH = new SmootherChainFixedTimeConventional(stream, calculate->getTargetRH());
        engine->addChainNode(avgRH);
        engine->addInputTarget(1, avgRH->getTargetValue());
        engine->addInputTarget(5, avgRH->getTargetCover());
        break;
    }

    case SmootherChainCore::RH: {
        SmootherChainFixedTimeConventional
                *cover = new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(1));
        engine->addChainNode(cover);
        engine->addInputTarget(2, cover->getTargetValue());
        engine->addInputTarget(3, cover->getTargetCover());

        SmootherChainRH *calculate = new SmootherChainRH(stream, engine->getOutput(0));
        engine->addChainNode(calculate);

        SmootherChainFixedTimeConventional *avgTemperature =
                new SmootherChainFixedTimeConventional(stream, calculate->getTargetTemperature());
        engine->addChainNode(avgTemperature);
        engine->addInputTarget(0, avgTemperature->getTargetValue());
        engine->addInputTarget(4, avgTemperature->getTargetCover());

        SmootherChainFixedTimeConventional *avgDewpoint =
                new SmootherChainFixedTimeConventional(stream, calculate->getTargetDewpoint());
        engine->addChainNode(avgDewpoint);
        engine->addInputTarget(1, avgDewpoint->getTargetValue());
        engine->addInputTarget(5, avgDewpoint->getTargetCover());
        break;
    }

    case SmootherChainCore::RHExtrapolate: {
        SmootherChainFixedTimeConventional
                *cover = new SmootherChainFixedTimeConventional(stream, NULL, engine->getOutput(1));
        engine->addChainNode(cover);
        engine->addInputTarget(3, cover->getTargetValue());
        engine->addInputTarget(4, cover->getTargetCover());

        SmootherChainRHExtrapolate
                *calculate = new SmootherChainRHExtrapolate(stream, engine->getOutput(0));
        engine->addChainNode(calculate);

        SmootherChainFixedTimeConventional *avgTemperatureIn =
                new SmootherChainFixedTimeConventional(stream, calculate->getTargetTemperatureIn());
        engine->addChainNode(avgTemperatureIn);
        engine->addInputTarget(0, avgTemperatureIn->getTargetValue());
        engine->addInputTarget(5, avgTemperatureIn->getTargetCover());

        SmootherChainFixedTimeConventional *avgRHIn =
                new SmootherChainFixedTimeConventional(stream, calculate->getTargetRHIn());
        engine->addChainNode(avgRHIn);
        engine->addInputTarget(1, avgRHIn->getTargetValue());
        engine->addInputTarget(6, avgRHIn->getTargetCover());

        SmootherChainFixedTimeConventional *avgTemperatureOut =
                new SmootherChainFixedTimeConventional(stream,
                                                       calculate->getTargetTemperatureOut());
        engine->addChainNode(avgTemperatureOut);
        engine->addInputTarget(2, avgTemperatureOut->getTargetValue());
        engine->addInputTarget(7, avgTemperatureOut->getTargetCover());
        break;
    }

    }
}

SmootherChainCore::HandlerMode
EditingChainCoreContinuousRecalculate::smootherHandler(SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General:
    case SmootherChainCore::Vector2D:
    case SmootherChainCore::Vector3D:
    case SmootherChainCore::Dewpoint:
    case SmootherChainCore::RH:
    case SmootherChainCore::RHExtrapolate:
        return SmootherChainCore::Repeatable;
    case SmootherChainCore::Difference:
    case SmootherChainCore::DifferenceInitial:
    case SmootherChainCore::BeersLawAbsorption:
    case SmootherChainCore::BeersLawAbsorptionInitial:
        return SmootherChainCore::Handled;
    }
    Q_ASSERT(false);
    return SmootherChainCore::Unhandled;
}

bool EditingChainCoreContinuousRecalculate::flagsHandler()
{ return true; }

void EditingChainCoreContinuousRecalculate::createFlags(SmootherChainCoreEngineInterface *engine)
{
    SequenceName coverUnit(engine->getBaseUnit().withFlavor(SequenceName::flavor_cover));
    SmootherChainTarget *cover = engine->addNewOutput(coverUnit);

    SmootherChainFixedTimeFlags *flags =
            new SmootherChainFixedTimeFlags(childInterval(), NULL, engine->getFlagsOutput(0),
                                            cover);
    engine->addChainNode(flags);
    engine->addFlagsInputTarget(0, flags->getTargetFlags());
    engine->addNewInput(coverUnit, flags->getTargetCover());
}

void EditingChainCoreContinuousRecalculate::deserializeFlags(QDataStream &stream,
                                                             SmootherChainCoreEngineInterface *engine)
{
    SmootherChainFixedTimeFlags *flags =
            new SmootherChainFixedTimeFlags(stream, engine->getFlagsOutput(0),
                                            engine->getOutput(0));
    engine->addChainNode(flags);
    engine->addFlagsInputTarget(0, flags->getTargetFlags());
    engine->addInputTarget(0, flags->getTargetCover());
}

bool EditingChainCoreContinuousRecalculate::generatedOutput(const SequenceName &unit)
{
    return unit.hasFlavor(SequenceName::flavor_cover);
}

Variant::Root EditingChainCoreContinuousRecalculate::getProcessingMetadata(double time) const
{
    Variant::Root meta;
    meta.write().hash("Type").setString("ContinuousRecalculate");
    if (requiredCover != NULL) {
        meta.write()
            .hash("Parameters")
            .hash("RequiredCoverage")
            .setDouble(requiredCover->get(time));
    }
    if (interval != NULL) {
        meta.write().hash("Parameters").hash("Interval").setString(interval->describe(time));
    }
    return meta;
}

QSet<double> EditingChainCoreContinuousRecalculate::getProcessingMetadataBreaks() const
{
    QSet<double> result;
    if (requiredCover != NULL)
        result |= requiredCover->getChangedPoints();
    if (interval != NULL)
        result |= interval->getChangedPoints();
    return result;
}


EditingChainCoreFinalRecalculate::EditingChainCoreFinalRecalculate(Data::DynamicTimeInterval *setInterval,
                                                                   Data::DynamicDouble *setRequiredCover)
        : SmootherChainCoreFixedTime(setInterval, NULL, setRequiredCover, true)
{ }

EditingChainCoreFinalRecalculate::~EditingChainCoreFinalRecalculate()
{ }

Variant::Root EditingChainCoreFinalRecalculate::getProcessingMetadata(double time) const
{
    Variant::Root meta = SmootherChainCoreFixedTime::getProcessingMetadata(time);
    meta.write().hash("Type").setString("FinalRecalculate");
    return meta;
}

}
}
