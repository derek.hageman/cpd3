/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGEDITINGCHAIN_H
#define CPD3SMOOTHINGEDITINGCHAIN_H

#include "core/first.hxx"

#include <vector>
#include <stdlib.h>
#include <QtGlobal>
#include <QObject>
#include <QDataStream>

#include "smoothing/smoothing.hxx"
#include "smoothing/smoothingchain.hxx"
#include "smoothing/binnedchain.hxx"
#include "smoothing/fixedtime.hxx"
#include "core/timeutils.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "datacore/stream.hxx"
#include "datacore/dynamictimeinterval.hxx"

namespace CPD3 {
namespace Smoothing {

/**
 * A smoother chain core for full calculation of edited data.  This can
 * calculate the conventional averages and continuous averages.  This is
 * normally used at the input to the chain and after any filter that 
 * requires full recalculation if it is not the last filter.
 */
class CPD3SMOOTHING_EXPORT EditingChainCoreFullCalculate : public SmootherChainCore {
    Data::DynamicDouble *requiredCover;
    Data::DynamicTimeInterval *interval;

    Data::DynamicDouble *childCover() const;

    Data::DynamicTimeInterval *childInterval() const;

    Data::DynamicTimeInterval *contGap() const;

public:
    /**
     * Create a new core.  This takes ownership of the interval and coverage.
     * 
     * @param setInterval   the interval to smooth
     * @param setRequiredCover the minimum cover to require or NULL for none
     */
    EditingChainCoreFullCalculate(Data::DynamicTimeInterval *setInterval,
                                  Data::DynamicDouble *setRequiredCover = NULL);

    virtual ~EditingChainCoreFullCalculate();

    virtual void
            createSmoother(SmootherChainCoreEngineInterface *engine, SmootherChainCore::Type type);

    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainCore::Type type);

    virtual SmootherChainCore::HandlerMode smootherHandler(SmootherChainCore::Type type);

    virtual bool flagsHandler();

    virtual void createFlags(SmootherChainCoreEngineInterface *engine);

    virtual void deserializeFlags(QDataStream &stream, SmootherChainCoreEngineInterface *engine);

    virtual bool generatedOutput(const Data::SequenceName &unit);

    virtual Data::Variant::Root getProcessingMetadata(double time) const;

    virtual QSet<double> getProcessingMetadataBreaks() const;
};

/**
 * A smoother chain core for calculation of averaged data from the continuous
 * segments.  This is normally used after a filter that requests recalculation
 * from them (e.x. CTS).
 */
class CPD3SMOOTHING_EXPORT EditingChainCoreContinuousRecalculate : public SmootherChainCore {
    Data::DynamicDouble *requiredCover;
    Data::DynamicTimeInterval *interval;

    Data::DynamicDouble *childCover() const;

    Data::DynamicTimeInterval *childInterval() const;

public:
    /**
     * Create a new core.  This takes ownership of the interval and coverage.
     * 
     * @param setInterval   the interval to smooth
     * @param setRequiredCover the minimum cover to require or NULL for none
     */
    EditingChainCoreContinuousRecalculate(Data::DynamicTimeInterval *setInterval,
                                          Data::DynamicDouble *setRequiredCover = NULL);

    virtual ~EditingChainCoreContinuousRecalculate();

    virtual void
            createSmoother(SmootherChainCoreEngineInterface *engine, SmootherChainCore::Type type);

    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainCore::Type type);

    virtual SmootherChainCore::HandlerMode smootherHandler(SmootherChainCore::Type type);

    virtual bool flagsHandler();

    virtual void createFlags(SmootherChainCoreEngineInterface *engine);

    virtual void deserializeFlags(QDataStream &stream, SmootherChainCoreEngineInterface *engine);

    virtual bool generatedOutput(const Data::SequenceName &unit);

    virtual Data::Variant::Root getProcessingMetadata(double time) const;

    virtual QSet<double> getProcessingMetadataBreaks() const;
};

/**
 * A smoother chain core used to generate statistics as the last element of
 * the editing chain.  This expects the "avg" archive to be in the input
 * data stream.
 */
class CPD3SMOOTHING_EXPORT EditingChainCoreFinalStatistics : public SmootherChainCore {
    Data::DynamicDouble *requiredCover;
    Data::DynamicTimeInterval *interval;

    Data::DynamicDouble *childCover() const;

    Data::DynamicTimeInterval *childInterval() const;

public:
    /**
     * Create a new core.  This takes ownership of the interval.
     * 
     * @param setInterval   the interval to smooth
     * @param setRequiredCover the minimum cover to require or NULL for none
     */
    EditingChainCoreFinalStatistics(Data::DynamicTimeInterval *setInterval,
                                    Data::DynamicDouble *setRequiredCover = NULL);

    virtual ~EditingChainCoreFinalStatistics();

    virtual void
            createSmoother(SmootherChainCoreEngineInterface *engine, SmootherChainCore::Type type);

    virtual void deserializeSmoother(QDataStream &stream,
                                     SmootherChainCoreEngineInterface *engine,
                                     SmootherChainCore::Type type);

    virtual SmootherChainCore::HandlerMode smootherHandler(SmootherChainCore::Type type);

    virtual bool flagsHandler();

    virtual void createFlags(SmootherChainCoreEngineInterface *engine);

    virtual void deserializeFlags(QDataStream &stream, SmootherChainCoreEngineInterface *engine);

    virtual bool generatedOutput(const Data::SequenceName &unit);

    virtual Data::Variant::Root getProcessingMetadata(double time) const;

    virtual QSet<double> getProcessingMetadataBreaks() const;
};

/**
 * A smoother chain core used as the final element of the chain when
 * a full recalculation is requested by the last filter.
 */
class CPD3SMOOTHING_EXPORT EditingChainCoreFinalRecalculate : public SmootherChainCoreFixedTime {
public:
    /**
     * Create a new core.  This takes ownership of the interval and coverage.
     * 
     * @param setInterval   the interval to smooth
     * @param setRequiredCover the minimum cover to require or NULL for none
     */
    EditingChainCoreFinalRecalculate(Data::DynamicTimeInterval *setInterval,
                                     Data::DynamicDouble *setRequiredCover = NULL);

    virtual ~EditingChainCoreFinalRecalculate();

    virtual Data::Variant::Root getProcessingMetadata(double time) const;
};

}
}

#endif
