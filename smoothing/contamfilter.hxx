/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGCONTAMFILTER_H
#define CPD3SMOOTHINGCONTAMFILTER_H

#include "core/first.hxx"

#include <vector>
#include <deque>
#include <mutex>
#include <condition_variable>
#include <QtGlobal>
#include <QObject>
#include <QDataStream>

#include "smoothing/smoothing.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/stream.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "datacore/segment.hxx"
#include "core/waitutils.hxx"

namespace CPD3 {
namespace Smoothing {

class SmoothingEngine;

/**
 * A class that implements the removal of contaminated data from the data
 * stream.  This is used in conjunction with conventional averagering to
 * produce standard averages.
 */
class CPD3SMOOTHING_EXPORT ContaminationFilter : public Data::ProcessingStage {
    struct TimeRange {
        double start;
        double end;

        TimeRange() = default;

        inline TimeRange(double start, double end) : start(start), end(end)
        { }
    };

    struct RangeTracking {
        std::deque<TimeRange> ranges;

        RangeTracking();

        ~RangeTracking();

        RangeTracking(const RangeTracking &other);

        void shiftActive(double start);

        bool isActive(double start, double end) const;

        void add(double start, double end);

        void clear(double time);

        void serialize(QDataStream &stream) const;

        void deserialize(QDataStream &stream);

        void copy(const RangeTracking &other);
    };

protected:
    class DispatchBypass;

    class DispatchDiscard;

    class DispatchFlagsMeta;

    class DispatchFlags;

    class DispatchValueMeta;

    class DispatchValue;

    /**
     * The base interface for dispatching data values for processing.
     * Advances are called at every time step in the order defined
     * by the sorting function.
     */
    class CPD3SMOOTHING_EXPORT Dispatch {
    public:
        virtual ~Dispatch();

        /**
         * Add an incoming value to this unit's dispatch.
         * @param value the value to dispatch
         */
        virtual void incomingData(Data::SequenceValue &&value) = 0;

        /**
         * Advance the time.  Called at every time step in the orde
         * defined by advancePriority().
         * @param time the time of the advance
         */
        virtual void incomingAdvance(double time) = 0;

        /**
         * Called at the end of data.
         */
        virtual void endData() = 0;

        /**
         * Get the segmenter associated with this dispatch if any.  This is
         * used to underlay the default values.
         * <br>
         * This returns NULL by default.
         * @return the segmenter for the dispatch of NULL for none
         */
        virtual Data::ValueSegment::Stream *getSegmenter();

        /**
         * Get the priority of the advance call.
         * <br>
         * This returns zero by default
         * @return  the advance priority
         */
        virtual int advancePriority() const;

        /**
         * Save any internal state to the given stream
         * @param stream    the target stream
         */
        virtual void serialize(QDataStream &stream) const = 0;

        /**
         * Restore state from the given stream
         * @param stream    the source stream
         */
        virtual void deserialize(QDataStream &stream) = 0;
    };

    /**
     * A dispatch the does nothing but forward to a given output.
     */
    class CPD3SMOOTHING_EXPORT DispatchBypass : public Dispatch {
        Data::SinkMultiplexer::Sink *egress;
    public:
        /**
         * Create a new bypass dispatch with the given target.
         * @param e the bypass ingress
         */
        DispatchBypass(Data::SinkMultiplexer::Sink *e);

        virtual ~DispatchBypass();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);
    };

    /**
     * A dispatch that discards all values.
     */
    class CPD3SMOOTHING_EXPORT DispatchDiscard : public Dispatch {
    public:
        DispatchDiscard();

        virtual ~DispatchDiscard();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);
    };

    /**
     * The dispatch handler for system flags metadata.  This handles processing
     * of the flags that trigger contamination.
     */
    class CPD3SMOOTHING_EXPORT DispatchFlagsMeta : public Dispatch {
        Data::SinkMultiplexer::Sink *egress;
        Data::ValueSegment::Stream segmenter;
        bool needUpdate;
        double forceUpdateTime;
        std::vector<Data::Variant::Flag> contaminationFlags;

        std::vector<DispatchFlags *> notifyFlags;

        void updateFlags(double time);

    public:
        /**
         * Create a new flags dispatch metadata target.
         * @param e the bypass ingress
         */
        DispatchFlagsMeta(Data::SinkMultiplexer::Sink *e);

        virtual ~DispatchFlagsMeta();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();

        virtual Data::ValueSegment::Stream *getSegmenter();

        virtual int advancePriority() const;

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);

        /**
         * Register the given flags handler as the one associated with the
         * metadata.
         * @param flags the flags target
         */
        void registerFlags(DispatchFlags *flags);

        /**
         * Get all known flags.
         *
         * @return all known flags
         */
        inline const std::vector<Data::Variant::Flag> &allFlags() const
        { return contaminationFlags; }
    };

    /**
     * The dispatch that handles the actual flags processing.
     */
    class CPD3SMOOTHING_EXPORT DispatchFlags : public Dispatch {
        Data::SinkMultiplexer::Sink *egress;
        DispatchFlagsMeta *meta;
        Data::ValueSegment::Stream segmenter;
        bool needUpdate;
        double forceUpdateTime;
        bool contaminated;

        std::vector<DispatchValue *> notifyValues;

        friend class DispatchValue;

    public:
        /**
         * Create a new flags dispatch metadata target.
         * @param e the bypass ingress
         * @param m the flags metadata
         */
        DispatchFlags(Data::SinkMultiplexer::Sink *e, DispatchFlagsMeta *m);

        virtual ~DispatchFlags();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);

        virtual Data::ValueSegment::Stream *getSegmenter();

        virtual int advancePriority() const;

        /**
         * Test if the dispatch is currently contaminated by a flag.
         */
        inline bool isContaminated() const
        { return contaminated; }

        /**
         * Notify the dispatch to recalculate the contaminated state.
         */
        inline void notifyUpdated()
        { needUpdate = true; }

        /**
         * Register a value for updates by this dispatch.
         * @param t the target value
         */
        void registerValue(DispatchValue *t);
    };

    /**
     * The dispatch handling the processing of value metadata.
     */
    class CPD3SMOOTHING_EXPORT DispatchValueMeta : public Dispatch {
        Data::SinkMultiplexer::Sink *egress;
        std::unique_ptr<Data::DynamicSequenceSelection> affected;
        Data::SequenceName unit;
        Data::ValueSegment::Stream segmenter;
        Data::SequenceName unitValue;

        RangeTracking disabled;
        RangeTracking remove;
        std::vector<DispatchValue *> notifyValues;

        void processMeta(Data::ValueSegment::Transfer &&values);

        friend class DispatchValue;

    public:
        /**
         * Create a new flags dispatch metadata target.
         * @param e an egress of the metadata
         * @param a the time operate defining when the value is active
         * @param u the output unit
         */
        DispatchValueMeta(Data::SinkMultiplexer::Sink *e,
                          Data::DynamicSequenceSelection *a,
                          const Data::SequenceName &u);

        virtual ~DispatchValueMeta();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);

        virtual int advancePriority() const;

        virtual Data::ValueSegment::Stream *getSegmenter();

        /**
         * Register a value for updates by this dispatch.
         * @param t the target value
         */
        void registerValue(DispatchValue *t);
    };

    /**
     * The dispatch for values that can be affected by contamination.
     */
    class CPD3SMOOTHING_EXPORT DispatchValue : public Dispatch {
        Data::SinkMultiplexer::Sink *egress;
        std::unique_ptr<Data::DynamicSequenceSelection> affected;
        DispatchFlags *flags;
        DispatchValueMeta *meta;
        Data::SequenceName unit;
        Data::ValueSegment::Stream segmenter;
        bool contaminated;

        void processSegment(Data::ValueSegment &&segment);

        void processValues(Data::ValueSegment::Transfer &&values);

        RangeTracking disabled;
        RangeTracking remove;

        friend class DispatchValueMeta;

    public:
        /**
         * Create a new flags dispatch target.
         * @param e an egress of the data
         * @param a the time operate defining when the value is active
         * @param f the flags
         * @param m the metadata
         * @param u the output unit
         */
        DispatchValue(Data::SinkMultiplexer::Sink *e, Data::DynamicSequenceSelection *a,
                      DispatchFlags *f,
                      DispatchValueMeta *m, const Data::SequenceName &u);

        virtual ~DispatchValue();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);

        virtual int advancePriority() const;

        virtual Data::ValueSegment::Stream *getSegmenter();

        /**
         * Add a time range that contamination is disabled for (by the metadata
         * generally).
         * 
         * @param start the start of the disabled time
         * @param end   the end of the disabled time
         */
        inline void addDisabledRange(double start, double end)
        { disabled.add(start, end); }

        /**
         * Add a time for which contamination is enabled.  This is used to end
         * a disabled range before it's actual normal end time.
         * 
         * @param time  the time of the enable
         */
        inline void setEnabled(double time)
        { disabled.clear(time); }

        /**
        * Add a time range that contamination removes data instead of
        * undefining it (set for difference measurements, generally).
        *
        * @param start the start of the disabled time
        * @param end   the end of the disabled time
        */
        inline void addRemoveRange(double start, double end)
        { remove.add(start, end); }

        /**
         * Add a time for which contamination is does not remove data
         * (non-difference measurements, generally).
         * 
         * @param time  the time of the enable
         */
        inline void clearRemove(double time)
        { remove.clear(time); }

        /**
         * Add a time when the flags are expected to change so that the value
         * can be fragmented to reflect the contamination update.
         */
        inline void addFlagsBreak(double time)
        { segmenter.addBreak(time); }
    };

    /**
     * Create the override dispatch of the given unit.  An override dispatch
     * receives no special handling outside of this call.
     * <br>
     * By default this creates a bypass for stats and cover units as well as
     * any unit that is never affected by contamination.
     * 
     * @param unit  the unit in question
     * @return      if not NULL then a new dispatch for the unit
     */
    virtual Dispatch *overrideDispatch(const Data::SequenceName &unit);

    /**
     * Create a general dispatch for the unit.  The default values will be
     * underlaid with the segmenter of the result, if any.
     * 
     * @param unit  the unit to create for
     * @return      a new dispatch
     */
    virtual Dispatch *createGeneralDispatch(const Data::SequenceName &unit);

    /**
     * Get the system bypass ingress.  This ingress is advanced and ended
     * independently by the main control loop.
     * 
     * @return the system bypass ingress
     */
    inline Data::SinkMultiplexer::Sink *getBypassIngress()
    { return bypassIngress; }

    /**
     * Create a new ingress to the output multiplexer.
     * 
     * @return a new ingress point
     */
    inline Data::SinkMultiplexer::Sink *createOutputIngress()
    { return outputMux.createSink(); }

    /**
     * Get the affected variables controller.
     * 
     * @return the affected variables
     */
    inline Data::DynamicSequenceSelection *getAffected()
    { return affectedVariables.get(); }

    /**
     * Get the dispatch for a give unit.
     * 
     * @param unit  the unit to look up or create
     * @return      the unit dispatch
     */
    Dispatch *getDispatch(const Data::SequenceName &unit);

    /**
     * Called when the filter has been terminated after everything internal
     * to it has finished.  This can be used by a child class to clean up
     * anything it wraps.
     */
    virtual void finishTerminate();

    /**
     * Called during the final filter completion before the child wait
     * loop begins.  This should return the number of additional threads
     * being waited on.  By default this returns zero.
     * @return the number of additional threads that will signal
     */
    virtual void initiateFinish();

    /**
     * Called during the final completion to test if all child threads
     * have been completed.  The default implementation always returns
     * true.
     * @return true if all child threads have exited
     */
    virtual bool childThreadWait(int timeout = -1);

    /**
     * Complete the finish step.  This should wait for all child threads to
     * exit.
     */
    virtual void completeFinish();

    /**
     * Used during exit to signal that a child thread has completed;
     */
    void childThreadFinished();

private:
    class DispatchUnderlay : public Dispatch {
        std::vector<Data::SequenceValue> values;
        std::vector<Dispatch *> forwardTargets;
        Data::SinkMultiplexer::Sink *egress;

        friend class ContaminationFilter;

    public:
        DispatchUnderlay(Data::SinkMultiplexer::Sink *e);

        virtual ~DispatchUnderlay();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);
    };

#ifndef NDEBUG
    double lastIncomingTime;
#endif

    Data::SequenceValue::Transfer pending;
    std::deque<Data::SequenceValue> incoming;
    bool dataEnded;
    enum {
        NotStarted,
        Running, PauseRequested, Paused, Terminated, Finalizing, FinalizeTerminated, Completed,
    } state;

    std::thread thread;
    std::mutex mutex;
    std::condition_variable request;
    std::condition_variable response;

    Data::SequenceName::Map<std::unique_ptr<Dispatch>> dispatch;
    std::vector<Dispatch *> advanceDispatch;

    std::unique_ptr<Data::DynamicSequenceSelection> affectedVariables;
    QString systemContamID;
    std::unordered_set<Data::SequenceName::Component> loadedSystemStations;

    Data::SinkMultiplexer outputMux;
    Data::SinkMultiplexer::Sink *bypassIngress;

    void endDispatch();

    void run();

    static bool advanceSortCompare(Dispatch *a, Dispatch *b);

    void loadSystemContam(const Data::SequenceName::Component &station);

    std::unique_ptr<Dispatch> createDispatch(const Data::SequenceName &unit);

    void stall(std::unique_lock<std::mutex> &lock);

public:
    /**
     * Create a new contamination filter with the given affected variables.
     * 
     * @param affected  the variables affected by contamination
     */
    ContaminationFilter(Data::DynamicSequenceSelection *affected);

    /**
     * Create a new contamination filter that dynamically loads affected 
     * variables from the standard system configuration for the given
     * type.
     * 
     * @param id        the system contamination type
     */
    ContaminationFilter(const QString &id);

    virtual ~ContaminationFilter();

    CPD3::Data::SequenceName::Set requestedInputs() override;

    void serialize(QDataStream &stream) override;

    /**
     * Deserialize the contamination filter from the given data stream.
     * 
     * @param stream    the stream to restore from
     */
    virtual void deserialize(QDataStream &stream);

    void setEgress(Data::StreamSink *egress) override;

    void incomingData(const Data::SequenceValue::Transfer &values) override;

    void incomingData(Data::SequenceValue::Transfer &&values) override;

    void incomingData(const Data::SequenceValue &value) override;

    void incomingData(Data::SequenceValue &&value) override;

    void endData() override;

    bool isFinished() override;

    bool wait(double timeout = FP::undefined()) override;

    void start() override;

    /**
     * Register an expected station.  This will cause pre-loading of
     * system contamination ID data.
     * 
     * @param station   the expected station
     * @param archive   the expected archive
     */
    void registerExpected(const Data::SequenceName::Component &station,
                          const Data::SequenceName::Component &archive);

    /**
     * Get the standard system contamination affected variables for the
     * given master type (e.x. "aerosol").
     * 
     * @param station   the station
     * @param type      the type of system in use
     * @param start     the start time
     * @param end       the end time
     * @return          a new affected object
     */
    virtual Data::DynamicSequenceSelection *systemContaminationAffected(const Data::SequenceName::Component &station,
                                                                        const QString &type = QString::fromLatin1(
                                                                                "aerosol"),
                                                                        double start = FP::undefined(),
                                                                        double end = FP::undefined());

    void signalTerminate() override;
};

/**
 * The contamination filter for data editing.  This handles the bifurcation
 * of values into the filtered and unfiltered ones as well as removing
 * the "cont" and "avg" archive for any filtered ones.
 */
class CPD3SMOOTHING_EXPORT EditingContaminationFilter : public ContaminationFilter {
    Data::DynamicSequenceSelection *affected;
    std::unique_ptr<Data::SequenceMatch::Composite> variables;

    class DispatchBypassDuplicate : public ContaminationFilter::Dispatch {
        Data::SinkMultiplexer::Sink *egress;
        Data::SequenceName duplicateUnit;
    public:
        /**
         * Create a new bypass dispatch with the given target.
         * @param e the bypass ingress
         * @param d the unit to duplicate to
         */
        DispatchBypassDuplicate(Data::SinkMultiplexer::Sink *e,
                                const Data::SequenceName &d);

        virtual ~DispatchBypassDuplicate();

        virtual void incomingData(Data::SequenceValue &&value);

        virtual void incomingAdvance(double time);

        virtual void endData();

        virtual void serialize(QDataStream &stream) const;

        virtual void deserialize(QDataStream &stream);
    };

    class DispatchFlagsMetaDuplicate : public ContaminationFilter::DispatchFlagsMeta {
        Data::SinkMultiplexer::Sink *egress;
        Data::SequenceName duplicateUnit;
    public:
        DispatchFlagsMetaDuplicate(Data::SinkMultiplexer::Sink *e,
                                   const Data::SequenceName &d);

        virtual ~DispatchFlagsMetaDuplicate();

        virtual void incomingData(Data::SequenceValue &&value);
    };

    class DispatchFlagsDuplicate : public ContaminationFilter::DispatchFlags {
        Data::SinkMultiplexer::Sink *egress;
        Data::SequenceName duplicateUnit;
    public:
        DispatchFlagsDuplicate(Data::SinkMultiplexer::Sink *e,
                               ContaminationFilter::DispatchFlagsMeta *m,
                               const Data::SequenceName &d);

        virtual ~DispatchFlagsDuplicate();

        virtual void incomingData(Data::SequenceValue &&value);
    };

    class DispatchValueOutput : public ContaminationFilter::DispatchValue {
        Data::SinkMultiplexer::Sink *unchangedEgress;
    public:
        DispatchValueOutput(Data::SinkMultiplexer::Sink *e, Data::DynamicSequenceSelection *a,
                            ContaminationFilter::DispatchFlags *f,
                            ContaminationFilter::DispatchValueMeta *m, const Data::SequenceName &u,
                            Data::SinkMultiplexer::Sink *unchangedE);

        virtual ~DispatchValueOutput();

        virtual void incomingData(Data::SequenceValue &&value);
    };

    class DispatchValueMetaOutput : public ContaminationFilter::DispatchValueMeta {
        Data::SinkMultiplexer::Sink *unchangedEgress;
    public:
        DispatchValueMetaOutput(Data::SinkMultiplexer::Sink *e,
                                Data::DynamicSequenceSelection *a,
                                const Data::SequenceName &u,
                                Data::SinkMultiplexer::Sink *unchangedE);

        virtual ~DispatchValueMetaOutput();

        virtual void incomingData(Data::SequenceValue &&value);
    };

public:
    /**
     * Create a new editing contamination filter.
     * 
     * @param affected  the variables affected by contamination
     * @param variables the variables that need processing or NULL for all
     */
    EditingContaminationFilter(Data::DynamicSequenceSelection *affected,
                               Data::SequenceMatch::Composite *variables = NULL);

    /**
     * Create a new editing contamination filter using dynamically
     * loaded contamination types.
     * 
     * @param id        the system contamination type
     */
    EditingContaminationFilter(const QString &id, Data::SequenceMatch::Composite *variables = NULL);

    virtual ~EditingContaminationFilter();

protected:
    virtual ContaminationFilter::Dispatch *overrideDispatch(const Data::SequenceName &unit);

    virtual ContaminationFilter::Dispatch *createGeneralDispatch(const Data::SequenceName &unit);

    /**
     * The classification of a unit as seen by the filter.
     */
    enum UnitClassification {
        /** The unit is potentially affected by the filter (it passes the variable selection) */
                Unit_Affected,
        /** The unit is generated by the filter or the smoother that immediately follows */
                Unit_Generated,
        /** The unit bypasses (is unchanged by) the filter */
                Unit_Bypassed,
    };

    /**
     * Get the classification of an incoming unit.
     *
     * @param unit  the incoming unit
     * @return      the classification of the new unit
     */
    virtual UnitClassification classifyUnit(const Data::SequenceName &unit) const;

    /**
     * Get the base output archive of the filter.
     *
     * @return      the output archive name
     */
    virtual Data::SequenceName::Component outputArchive() const;
};

/**
 * The contamination filter for the final stage of data editing.  This handles the bifurcation
 * of values into the filtered and unfiltered ones.
 */
class CPD3SMOOTHING_EXPORT EditingContaminationFinal : public EditingContaminationFilter {
public:
    /**
     * Create a new editing contamination filter.
     *
     * @param affected  the variables affected by contamination
     * @param variables the variables that need processing or NULL for all
     */
    EditingContaminationFinal(Data::DynamicSequenceSelection *affected);

    /**
     * Create a new editing contamination filter using dynamically
     * loaded contamination types.
     *
     * @param id        the system contamination type
     */
    EditingContaminationFinal(const QString &id);

    virtual ~EditingContaminationFinal();

protected:
    virtual UnitClassification classifyUnit(const Data::SequenceName &unit) const;

    virtual Data::SequenceName::Component outputArchive() const;
};

/**
 * The base class for implementing a contamination filter that outputs
 * directly to a smoothing engine.  This is used by the external components
 * to filter out contaminated data from their input during smoothing.
 * <br>
 * The engine is serialized first, but not deserialized by the filter at all.
 * That is, during deserialization the wrapper must deserialize the engine
 * then construct and deserialize the filter with that engine.
 */
class CPD3SMOOTHING_EXPORT SmoothingContaminationFilter : public ContaminationFilter {
    std::unique_ptr<SmoothingEngine> engine;
public:
    /**
     * Create a new contamination filter.  This takes owership of the
     * engine and affected.
     * 
     * @param engine    the smoothing engine
     * @param affected  the affected variables
     */
    SmoothingContaminationFilter(SmoothingEngine *engine, Data::DynamicSequenceSelection *affected);

    /**
    * Create a new contamination filter.  This takes owership of the
    * engine.  This loads the affected contamination variables for the
    * system type on the fly.
    *
    * @param engine    the smoothing engine
    * @param id        the system contamination type
    */
    SmoothingContaminationFilter(SmoothingEngine *engine, const QString &id);

    virtual ~SmoothingContaminationFilter();

    void serialize(QDataStream &stream) override;

    void setEgress(Data::StreamSink *egress) override;

protected:
    void finishTerminate() override;

    void initiateFinish() override;

    bool childThreadWait(int timeout = -1) override;

    void completeFinish() override;

    void signalTerminate() override;

    void start() override;
};

}
}

#endif
