/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "smoothing/smoothingchain.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/smoothingchain.hxx
 * The general interfaces for dealing with elements of the smoothing chain.
 * An element of the smoothing chain is a filter that accepts values
 * to produce output to a subsequent one.
 */

SmootherChainNode::~SmootherChainNode()
{ }

bool SmootherChainNode::finished()
{ return false; }

void SmootherChainNode::pause()
{ }

void SmootherChainNode::resume()
{ }

void SmootherChainNode::signalTerminate()
{ }

void SmootherChainNode::cleanup()
{ }

SmootherChainTarget::~SmootherChainTarget()
{ }

SmootherChainTargetFlags::~SmootherChainTargetFlags()
{ }

SmootherChainTargetGeneral::~SmootherChainTargetGeneral()
{ }

SmootherChainCoreEngineAuxiliaryInterface::SmootherChainCoreEngineAuxiliaryInterface()
{ }

SmootherChainCoreEngineAuxiliaryInterface::~SmootherChainCoreEngineAuxiliaryInterface()
{ }

SmootherChainCoreEngineInterface::~SmootherChainCoreEngineInterface()
{ }

Data::Variant::Read SmootherChainCoreEngineInterface::getOptions()
{ return Variant::Read::empty(); }

SmootherChainCoreEngineAuxiliaryInterface *SmootherChainCoreEngineInterface::getAuxiliaryInterface()
{ return NULL; }

SmootherChainCore::~SmootherChainCore()
{ }

SmootherChainConsecutiveDifference::TargetInput::TargetInput(SmootherChainConsecutiveDifference *n)
        : node(n)
{ }

SmootherChainConsecutiveDifference::TargetInput::~TargetInput()
{ }

void SmootherChainConsecutiveDifference::TargetInput::incomingData(double start,
                                                                   double end,
                                                                   double value)
{ node->handleData(start, end, value); }

void SmootherChainConsecutiveDifference::TargetInput::incomingAdvance(double time)
{ node->handleAdvance(time); }

void SmootherChainConsecutiveDifference::TargetInput::endData()
{ node->handleEnd(); }

SmootherChainConsecutiveDifference::SmootherChainConsecutiveDifference(SmootherChainTarget *outputStart,
                                                                       SmootherChainTarget *outputEnd)
        : previousValue(FP::undefined()),
          previousStart(FP::undefined()),
          previousEnd(FP::undefined()),
          valuePending(false),
          ended(false),
          targetStart(outputStart),
          targetEnd(outputEnd),
          input(this)
{ }

SmootherChainConsecutiveDifference::SmootherChainConsecutiveDifference(QDataStream &stream,
                                                                       SmootherChainTarget *outputStart,
                                                                       SmootherChainTarget *outputEnd)
        : previousValue(FP::undefined()),
          previousStart(FP::undefined()),
          previousEnd(FP::undefined()),
          valuePending(false),
          ended(false),
          targetStart(outputStart),
          targetEnd(outputEnd),
          input(this)
{
    stream >> previousValue >> previousStart >> previousEnd >> valuePending >> ended;
}

void SmootherChainConsecutiveDifference::serialize(QDataStream &stream) const
{
    stream << previousValue << previousStart << previousEnd << valuePending << ended;
}

bool SmootherChainConsecutiveDifference::finished()
{ return ended; }

void SmootherChainConsecutiveDifference::handleData(double start, double end, double value)
{
    if (!valuePending) {
        previousStart = start;
        previousEnd = end;
        previousValue = value;
        valuePending = true;
        return;
    }

    Q_ASSERT(FP::defined(previousEnd));
    Q_ASSERT(FP::defined(start));
    Q_ASSERT(start >= previousEnd);
    if ((start - previousEnd) >= 0.001) {
        if (targetStart != NULL) {
            targetStart->incomingData(previousStart, previousEnd, previousValue);
            if (targetEnd != NULL) {
                targetEnd->incomingData(previousStart, previousEnd, FP::undefined());
            }
        } else if (targetEnd != NULL) {
            targetEnd->incomingData(previousStart, previousEnd, previousValue);
        }

        previousStart = start;
        previousEnd = end;
        previousValue = value;
        return;
    }

    Q_ASSERT(Range::compareStart(previousEnd, start) <= 0);
    Q_ASSERT(!FP::equal(start, end));

    if (targetStart != NULL)
        targetStart->incomingData(previousStart, previousEnd, previousValue);
    if (targetEnd != NULL)
        targetEnd->incomingData(previousStart, previousEnd, value);

    previousStart = start;
    previousEnd = end;
    previousValue = value;
}

void SmootherChainConsecutiveDifference::handleAdvance(double time)
{
    Q_ASSERT(FP::defined(time));
    Q_ASSERT(Range::compareStart(previousStart, time) <= 0);

    if (valuePending) {
        Q_ASSERT(FP::defined(previousEnd));
        Q_ASSERT(Range::compareStart(previousEnd, time) <= 0);
        if ((time - previousEnd) < 0.001)
            return;
        if (targetStart != NULL) {
            targetStart->incomingData(previousStart, previousEnd, previousValue);
            if (targetEnd != NULL) {
                targetEnd->incomingData(previousStart, previousEnd, FP::undefined());
            }
        } else if (targetEnd != NULL) {
            targetEnd->incomingData(previousStart, previousEnd, previousValue);
        }

        valuePending = false;
        previousEnd = FP::undefined();
        previousValue = FP::undefined();
    }

    previousStart = time;

    if (targetStart != NULL)
        targetStart->incomingAdvance(time);
    if (targetEnd != NULL)
        targetEnd->incomingAdvance(time);
}

void SmootherChainConsecutiveDifference::handleEnd()
{
    if (valuePending) {
        if (targetStart != NULL) {
            targetStart->incomingData(previousStart, previousEnd, previousValue);
            if (targetEnd != NULL) {
                targetEnd->incomingData(previousStart, previousEnd, FP::undefined());
            }
        } else if (targetEnd != NULL) {
            targetEnd->incomingData(previousStart, previousEnd, previousValue);
        }
        valuePending = false;
    }

    if (targetStart != NULL)
        targetStart->endData();
    if (targetEnd != NULL)
        targetEnd->endData();
    ended = true;
}


SmootherChainCoreSimple::SmootherChainCoreSimple()
{ }

SmootherChainCoreSimple::~SmootherChainCoreSimple()
{ }

void SmootherChainCoreSimple::createSmoother(SmootherChainCoreEngineInterface *engine,
                                             SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General: {
        SmootherChainTarget *input = NULL;
        createSmoother(engine, input, engine->getOutput(0));
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);
        break;
    }

    case SmootherChainCore::Difference: {
        SmootherChainTarget *input = NULL;
        createSmoother(engine, input, engine->getOutput(0));
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);
        input = NULL;
        createSmoother(engine, input, engine->getOutput(1));
        Q_ASSERT(input != NULL);
        engine->addInputTarget(1, input);
        break;
    }

    case SmootherChainCore::DifferenceInitial: {
        SmootherChainConsecutiveDifference *difference =
                new SmootherChainConsecutiveDifference(engine->getOutput(0), engine->getOutput(1));
        engine->addChainNode(difference);

        SmootherChainTarget *input = NULL;
        createSmoother(engine, input, difference->getTarget());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);
        break;
    }

    case SmootherChainCore::Vector2D: {
        SmootherChainVector2DReconstruct *reconstruct =
                new SmootherChainVector2DReconstruct(engine->getOutput(0), engine->getOutput(1));
        engine->addChainNode(reconstruct);

        SmootherChainTarget *inputX = NULL;
        createSmoother(engine, inputX, reconstruct->getTargetX());
        Q_ASSERT(inputX != NULL);

        SmootherChainTarget *inputY = NULL;
        createSmoother(engine, inputY, reconstruct->getTargetY());
        Q_ASSERT(inputY != NULL);

        SmootherChainVector2DBreakdown
                *breakdown = new SmootherChainVector2DBreakdown(inputX, inputY);
        engine->addChainNode(breakdown);

        engine->addInputTarget(0, breakdown->getTargetDirection());
        engine->addInputTarget(1, breakdown->getTargetMagnitude());
        break;
    }

    case SmootherChainCore::Vector3D: {
        SmootherChainVector3DReconstruct *reconstruct =
                new SmootherChainVector3DReconstruct(engine->getOutput(0), engine->getOutput(1),
                                                     engine->getOutput(2));
        engine->addChainNode(reconstruct);

        SmootherChainTarget *inputX = NULL;
        createSmoother(engine, inputX, reconstruct->getTargetX());
        Q_ASSERT(inputX != NULL);

        SmootherChainTarget *inputY = NULL;
        createSmoother(engine, inputY, reconstruct->getTargetY());
        Q_ASSERT(inputY != NULL);

        SmootherChainTarget *inputZ = NULL;
        createSmoother(engine, inputZ, reconstruct->getTargetZ());
        Q_ASSERT(inputZ != NULL);

        SmootherChainVector3DBreakdown
                *breakdown = new SmootherChainVector3DBreakdown(inputX, inputY, inputZ);
        engine->addChainNode(breakdown);

        engine->addInputTarget(0, breakdown->getTargetAzimuth());
        engine->addInputTarget(1, breakdown->getTargetElevation());
        engine->addInputTarget(2, breakdown->getTargetMagnitude());
        break;
    }

    case SmootherChainCore::BeersLawAbsorption: {
        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(engine->getOutput(0));
        engine->addChainNode(calculate);

        SmootherChainTarget *input = NULL;
        createSmoother(engine, input, calculate->getTargetStartL());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);

        input = NULL;
        createSmoother(engine, input, calculate->getTargetEndL());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(1, input);

        input = NULL;
        createSmoother(engine, input, calculate->getTargetStartI());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(2, input);

        input = NULL;
        createSmoother(engine, input, calculate->getTargetEndI());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(3, input);
        break;
    }

    case SmootherChainCore::BeersLawAbsorptionInitial: {
        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(engine->getOutput(0));
        engine->addChainNode(calculate);

        SmootherChainConsecutiveDifference *differenceL =
                new SmootherChainConsecutiveDifference(calculate->getTargetStartL(),
                                                       calculate->getTargetEndL());
        engine->addChainNode(differenceL);

        SmootherChainTarget *input = NULL;
        createSmoother(engine, input, differenceL->getTarget());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);

        SmootherChainConsecutiveDifference *differenceI =
                new SmootherChainConsecutiveDifference(calculate->getTargetStartI(),
                                                       calculate->getTargetEndI());
        engine->addChainNode(differenceI);

        input = NULL;
        createSmoother(engine, input, differenceI->getTarget());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(1, input);

        break;
    }

    case SmootherChainCore::Dewpoint: {
        SmootherChainDewpoint *calculate = new SmootherChainDewpoint(engine->getOutput(0),
                                                                     engine->getOptions()
                                                                           .hash("AlwaysWater")
                                                                           .toBool());
        engine->addChainNode(calculate);

        SmootherChainTarget *input = NULL;
        createSmoother(engine, input, calculate->getTargetTemperature());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);

        input = NULL;
        createSmoother(engine, input, calculate->getTargetRH());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(1, input);
        break;
    }

    case SmootherChainCore::RH: {
        SmootherChainRH *calculate = new SmootherChainRH(engine->getOutput(0), engine->getOptions()
                                                                                     .hash("AlwaysWater")
                                                                                     .toBool());
        engine->addChainNode(calculate);

        SmootherChainTarget *input = NULL;
        createSmoother(engine, input, calculate->getTargetTemperature());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);

        input = NULL;
        createSmoother(engine, input, calculate->getTargetDewpoint());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(1, input);
        break;
    }

    case SmootherChainCore::RHExtrapolate: {
        SmootherChainRHExtrapolate *calculate = new SmootherChainRHExtrapolate(engine->getOutput(0),
                                                                               engine->getOptions()
                                                                                     .hash("AlwaysWater")
                                                                                     .toBool());
        engine->addChainNode(calculate);

        SmootherChainTarget *input = NULL;
        createSmoother(engine, input, calculate->getTargetTemperatureIn());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);

        input = NULL;
        createSmoother(engine, input, calculate->getTargetRHIn());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(1, input);

        input = NULL;
        createSmoother(engine, input, calculate->getTargetTemperatureOut());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(2, input);
        break;
    }

    }
}

void SmootherChainCoreSimple::deserializeSmoother(QDataStream &stream,
                                                  SmootherChainCoreEngineInterface *engine,
                                                  SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General: {
        SmootherChainTarget *input = NULL;
        deserializeSmoother(stream, engine, input, engine->getOutput(0));
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);
        break;
    }

    case SmootherChainCore::Difference: {
        SmootherChainTarget *input = NULL;
        deserializeSmoother(stream, engine, input, engine->getOutput(0));
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);
        input = NULL;
        deserializeSmoother(stream, engine, input, engine->getOutput(1));
        Q_ASSERT(input != NULL);
        engine->addInputTarget(1, input);
        break;
    }

    case SmootherChainCore::DifferenceInitial: {
        SmootherChainConsecutiveDifference *difference =
                new SmootherChainConsecutiveDifference(stream, engine->getOutput(0),
                                                       engine->getOutput(1));
        engine->addChainNode(difference);

        SmootherChainTarget *input = NULL;
        deserializeSmoother(stream, engine, input, difference->getTarget());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);
        break;
    }

    case SmootherChainCore::Vector2D: {
        SmootherChainVector2DReconstruct *reconstruct =
                new SmootherChainVector2DReconstruct(stream, engine->getOutput(0),
                                                     engine->getOutput(1));
        engine->addChainNode(reconstruct);

        SmootherChainTarget *inputX = NULL;
        deserializeSmoother(stream, engine, inputX, reconstruct->getTargetX());
        Q_ASSERT(inputX != NULL);

        SmootherChainTarget *inputY = NULL;
        deserializeSmoother(stream, engine, inputY, reconstruct->getTargetY());
        Q_ASSERT(inputY != NULL);

        SmootherChainVector2DBreakdown
                *breakdown = new SmootherChainVector2DBreakdown(stream, inputX, inputY);
        engine->addChainNode(breakdown);

        engine->addInputTarget(0, breakdown->getTargetDirection());
        engine->addInputTarget(1, breakdown->getTargetMagnitude());
        break;
    }

    case SmootherChainCore::Vector3D: {
        SmootherChainVector3DReconstruct *reconstruct =
                new SmootherChainVector3DReconstruct(stream, engine->getOutput(0),
                                                     engine->getOutput(1), engine->getOutput(2));
        engine->addChainNode(reconstruct);

        SmootherChainTarget *inputX = NULL;
        deserializeSmoother(stream, engine, inputX, reconstruct->getTargetX());
        Q_ASSERT(inputX != NULL);

        SmootherChainTarget *inputY = NULL;
        deserializeSmoother(stream, engine, inputY, reconstruct->getTargetY());
        Q_ASSERT(inputY != NULL);

        SmootherChainTarget *inputZ = NULL;
        deserializeSmoother(stream, engine, inputZ, reconstruct->getTargetZ());
        Q_ASSERT(inputZ != NULL);

        SmootherChainVector3DBreakdown
                *breakdown = new SmootherChainVector3DBreakdown(stream, inputX, inputY, inputZ);
        engine->addChainNode(breakdown);

        engine->addInputTarget(0, breakdown->getTargetAzimuth());
        engine->addInputTarget(1, breakdown->getTargetElevation());
        engine->addInputTarget(2, breakdown->getTargetMagnitude());
        break;
    }

    case SmootherChainCore::BeersLawAbsorption: {
        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(stream, engine->getOutput(0));
        engine->addChainNode(calculate);

        SmootherChainTarget *input = NULL;
        deserializeSmoother(stream, engine, input, calculate->getTargetStartL());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);

        input = NULL;
        deserializeSmoother(stream, engine, input, calculate->getTargetEndL());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(1, input);

        input = NULL;
        deserializeSmoother(stream, engine, input, calculate->getTargetStartI());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(2, input);

        input = NULL;
        deserializeSmoother(stream, engine, input, calculate->getTargetEndI());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(3, input);
        break;
    }

    case SmootherChainCore::BeersLawAbsorptionInitial: {
        SmootherChainBeersLawAbsorption
                *calculate = new SmootherChainBeersLawAbsorption(stream, engine->getOutput(0));
        engine->addChainNode(calculate);

        SmootherChainConsecutiveDifference *differenceL =
                new SmootherChainConsecutiveDifference(stream, calculate->getTargetStartL(),
                                                       calculate->getTargetEndL());
        engine->addChainNode(differenceL);

        SmootherChainTarget *input = NULL;
        deserializeSmoother(stream, engine, input, differenceL->getTarget());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);

        SmootherChainConsecutiveDifference *differenceI =
                new SmootherChainConsecutiveDifference(stream, calculate->getTargetStartI(),
                                                       calculate->getTargetEndI());
        engine->addChainNode(differenceI);

        input = NULL;
        deserializeSmoother(stream, engine, input, differenceI->getTarget());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(1, input);

        break;
    }

    case SmootherChainCore::Dewpoint: {
        SmootherChainDewpoint *calculate = new SmootherChainDewpoint(stream, engine->getOutput(0));
        engine->addChainNode(calculate);

        SmootherChainTarget *input = NULL;
        deserializeSmoother(stream, engine, input, calculate->getTargetTemperature());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);

        input = NULL;
        deserializeSmoother(stream, engine, input, calculate->getTargetRH());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(1, input);
        break;
    }

    case SmootherChainCore::RH: {
        SmootherChainRH *calculate = new SmootherChainRH(stream, engine->getOutput(0));
        engine->addChainNode(calculate);

        SmootherChainTarget *input = NULL;
        deserializeSmoother(stream, engine, input, calculate->getTargetTemperature());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);

        input = NULL;
        deserializeSmoother(stream, engine, input, calculate->getTargetDewpoint());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(1, input);
        break;
    }

    case SmootherChainCore::RHExtrapolate: {
        SmootherChainRHExtrapolate
                *calculate = new SmootherChainRHExtrapolate(stream, engine->getOutput(0));
        engine->addChainNode(calculate);

        SmootherChainTarget *input = NULL;
        deserializeSmoother(stream, engine, input, calculate->getTargetTemperatureIn());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(0, input);

        input = NULL;
        deserializeSmoother(stream, engine, input, calculate->getTargetRHIn());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(1, input);

        input = NULL;
        deserializeSmoother(stream, engine, input, calculate->getTargetTemperatureOut());
        Q_ASSERT(input != NULL);
        engine->addInputTarget(2, input);
        break;
    }

    }
}

SmootherChainCore::HandlerMode SmootherChainCoreSimple::smootherHandler(SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General:
    case SmootherChainCore::Difference:
    case SmootherChainCore::DifferenceInitial:
    case SmootherChainCore::Vector2D:
    case SmootherChainCore::Vector3D:
    case SmootherChainCore::BeersLawAbsorption:
    case SmootherChainCore::BeersLawAbsorptionInitial:
    case SmootherChainCore::Dewpoint:
    case SmootherChainCore::RH:
    case SmootherChainCore::RHExtrapolate:
        /* The initial ones are considered repeatable, since they'll generate
         * two point differences. */
        return SmootherChainCore::Repeatable;
    }
    Q_ASSERT(false);
    return SmootherChainCore::Unhandled;
}

bool SmootherChainCoreSimple::flagsHandler()
{ return false; }

void SmootherChainCoreSimple::createFlags(SmootherChainCoreEngineInterface *engine)
{
    engine->addFlagsInputTarget(0, engine->getFlagsOutput(0));
}

void SmootherChainCoreSimple::deserializeFlags(QDataStream &stream,
                                               SmootherChainCoreEngineInterface *engine)
{
    Q_UNUSED(stream);
    engine->addFlagsInputTarget(0, engine->getFlagsOutput(0));
}

bool SmootherChainCoreSimple::generatedOutput(const SequenceName &unit)
{
    Q_UNUSED(unit);
    return false;
}


SmootherChainCoreDiscard::SmootherChainCoreDiscard()
{ }

SmootherChainCoreDiscard::~SmootherChainCoreDiscard()
{ }

void SmootherChainCoreDiscard::createSmoother(SmootherChainCoreEngineInterface *engine,
                                              SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General:
        engine->getOutput(0)->endData();
        break;

    case SmootherChainCore::Difference:
        engine->getOutput(0)->endData();
        engine->getOutput(1)->endData();
        break;

    case SmootherChainCore::DifferenceInitial:
        engine->getOutput(0)->endData();
        engine->getOutput(1)->endData();
        break;

    case SmootherChainCore::Vector2D:
        engine->getOutput(0)->endData();
        engine->getOutput(1)->endData();
        break;

    case SmootherChainCore::Vector3D:
        engine->getOutput(0)->endData();
        engine->getOutput(1)->endData();
        engine->getOutput(2)->endData();
        break;

    case SmootherChainCore::BeersLawAbsorption:
        engine->getOutput(0)->endData();
        break;

    case SmootherChainCore::BeersLawAbsorptionInitial:
        engine->getOutput(0)->endData();
        break;

    case SmootherChainCore::Dewpoint:
        engine->getOutput(0)->endData();
        break;

    case SmootherChainCore::RH:
        engine->getOutput(0)->endData();
        break;

    case SmootherChainCore::RHExtrapolate:
        engine->getOutput(0)->endData();
        break;

    }
}

void SmootherChainCoreDiscard::deserializeSmoother(QDataStream &stream,
                                                   SmootherChainCoreEngineInterface *engine,
                                                   SmootherChainCore::Type type)
{
    Q_UNUSED(stream);
    Q_UNUSED(engine);
    Q_UNUSED(type);
}

SmootherChainCore::HandlerMode SmootherChainCoreDiscard::smootherHandler(SmootherChainCore::Type type)
{
    Q_UNUSED(type);
    return SmootherChainCore::Repeatable;
}

bool SmootherChainCoreDiscard::flagsHandler()
{ return false; }

void SmootherChainCoreDiscard::createFlags(SmootherChainCoreEngineInterface *engine)
{
    engine->getFlagsOutput(0)->endData();
}

void SmootherChainCoreDiscard::deserializeFlags(QDataStream &stream,
                                                SmootherChainCoreEngineInterface *engine)
{
    Q_UNUSED(stream);
    Q_UNUSED(engine);
}

bool SmootherChainCoreDiscard::generatedOutput(const SequenceName &unit)
{
    Q_UNUSED(unit);
    return false;
}

Variant::Root SmootherChainCoreDiscard::getProcessingMetadata(double) const
{ return Variant::Root(); }

QSet<double> SmootherChainCoreDiscard::getProcessingMetadataBreaks() const
{ return QSet<double>(); }

}
}
