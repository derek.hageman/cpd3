/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "smoothing/realtimechain.hxx"
#include "core/environment.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/realtimechain.hxx
 * Realtime acquisition smoothers.
 */

RealtimeController::~RealtimeController() = default;

RealtimeEngineInterface::~RealtimeEngineInterface() = default;

static const SequenceName::Component archiveInstant = "rt_instant";
static const SequenceName::Component archiveInstantMeta = "rt_instant_meta";
static const SequenceName::Component archiveBoxcar = "rt_boxcar";
static const SequenceName::Component archiveBoxcarMeta = "rt_boxcar_meta";
static const SequenceName::Component archiveRaw = "raw";
static const SequenceName::Component archiveRawMeta = "raw_meta";

RealtimeChainConventional::RealtimeChainConventional(Time::LogicalTimeUnit setUnit,
                                                     int setCount,
                                                     bool setAlign,
                                                     SmootherChainTarget *outputComplete,
                                                     SmootherChainTarget *outputRunning,
                                                     SmootherChainTargetGeneral *outputStatsComplete,
                                                     SmootherChainTargetGeneral *outputStatsRunning)
        : RealtimeBinner<double>(setUnit, setCount, setAlign),
          targetComplete(outputComplete),
          targetRunning(outputRunning),
          targetCompleteStats(outputStatsComplete),
          targetRunningStats(outputStatsRunning),
          targetFinished(false), target(this)
{ }

RealtimeChainConventional::RealtimeChainConventional(QDataStream &stream,
                                                     SmootherChainTarget *outputComplete,
                                                     SmootherChainTarget *outputRunning,
                                                     SmootherChainTargetGeneral *outputStatsComplete,
                                                     SmootherChainTargetGeneral *outputStatsRunning)
        : RealtimeBinner<double>(stream),
          targetComplete(outputComplete),
          targetRunning(outputRunning),
          targetCompleteStats(outputStatsComplete),
          targetRunningStats(outputStatsRunning),
          targetFinished(false), target(this)
{
    Deserialize::container(stream, buffer, [&]() {
        BufferValue value;
        stream >> value.time >> value.value;
        return value;
    });
    stream >> targetFinished;
}

RealtimeChainConventional::~RealtimeChainConventional() = default;

RealtimeChainConventional::Target::Target(RealtimeChainConventional *n) : node(n)
{ }

RealtimeChainConventional::Target::~Target() = default;

void RealtimeChainConventional::Target::incomingData(double start, double end, double value)
{
    Q_UNUSED(end);
    node->addIncomingValue(start, value);
}

void RealtimeChainConventional::Target::incomingAdvance(double time)
{ node->handleAdvance(time); }

void RealtimeChainConventional::Target::endData()
{ node->dataFinished(); }

/* Don't need to protect this since this is "approximate" and only written
 * once anyway */
bool RealtimeChainConventional::finished()
{ return targetFinished; }

void RealtimeChainConventional::serialize(QDataStream &stream) const
{
    RealtimeBinner<double>::writeSerial(stream);
    Serialize::container(stream, buffer, [&](const BufferValue &value) {
        stream << value.time << value.value;
    });
    stream << targetFinished;
}

void RealtimeChainConventional::pause()
{ }

void RealtimeChainConventional::resume()
{ }

void RealtimeChainConventional::handleAdvance(double time)
{
    addIncomingAdvance(time);

    if (targetRunning)
        targetRunning->incomingAdvance(time);
    if (targetRunningStats)
        targetRunningStats->incomingAdvance(time);
    if (targetComplete)
        targetComplete->incomingAdvance(time);
    if (targetCompleteStats)
        targetCompleteStats->incomingAdvance(time);
}

void RealtimeChainConventional::dataFinished()
{
    if (targetComplete)
        targetComplete->endData();
    if (targetRunning)
        targetRunning->endData();
    if (targetCompleteStats)
        targetCompleteStats->endData();
    if (targetRunningStats)
        targetRunningStats->endData();

    targetFinished = true;
}

void RealtimeChainConventional::addToBin(double time, const double &data)
{
    Q_ASSERT(FP::defined(time));

    buffer.emplace_back(time, data);

    if (FP::defined(data)) {
        if (targetRunning)
            processBuffer(time, targetRunning);
        if (targetRunningStats)
            processBufferStats(time, targetRunningStats);
    } else {
        if (targetRunning)
            targetRunning->incomingData(time, FP::undefined(), FP::undefined());
        if (targetRunningStats)
            targetRunningStats->incomingData(time, FP::undefined(), Variant::Root());
    }
}

bool RealtimeChainConventional::emitTotal(double time)
{
    if (buffer.empty())
        return false;
    bool result = false;
    if (targetComplete)
        result = processBuffer(time, targetComplete) || result;
    if (targetCompleteStats)
        result = processBufferStats(time, targetCompleteStats) || result;
    return result;
}

void RealtimeChainConventional::emitTerminal(double time)
{
    buffer.clear();
    if (targetComplete)
        targetComplete->incomingData(time, FP::undefined(), FP::undefined());
    if (targetCompleteStats)
        targetCompleteStats->incomingData(time, FP::undefined(), Variant::Root());
}

void RealtimeChainConventional::purgeBefore(double time)
{
    Q_ASSERT(FP::defined(time));
    auto endRemove = Range::heuristicUpperBoundDefined(buffer.begin(), buffer.end(), time);
    buffer.erase(buffer.begin(), endRemove);
}

bool RealtimeChainConventional::processBuffer(double time, SmootherChainTarget *target)
{
    double sum = 0.0;
    int count = 0;
    for (const auto &add : buffer) {
        if (!FP::defined(add.value))
            continue;
        ++count;
        sum += add.value;
    }
    if (count == 0) {
        target->incomingData(time, FP::undefined(), FP::undefined());
        return false;
    } else {
        target->incomingData(time, FP::undefined(), sum / (double) count);
        return true;
    }
}

bool RealtimeChainConventional::processBufferStats(double time, SmootherChainTargetGeneral *target)
{
    QVector<double> values;
    for (const auto &add : buffer) {
        values.append(add.value);
    }
    target->incomingData(time, FP::undefined(), Internal::binnedChainGenerateStatistics(values));
    return true;
}


RealtimeChainVectorStatistics::RealtimeChainVectorStatistics(Time::LogicalTimeUnit setUnit,
                                                             int setCount,
                                                             bool setAlign,
                                                             SmootherChainTarget *outputComplete,
                                                             SmootherChainTarget *outputRunning,
                                                             SmootherChainTargetGeneral *outputStatsComplete,
                                                             SmootherChainTargetGeneral *outputStatsRunning)
        : Binner(setUnit, setCount, setAlign),
          Processor(),
          targetComplete(outputComplete),
          targetRunning(outputRunning),
          targetCompleteStats(outputStatsComplete),
          targetRunningStats(outputStatsRunning),
          targetFinished(false),
          targetValue(this), targetVectoredMagnitude(this)
{ }

RealtimeChainVectorStatistics::RealtimeChainVectorStatistics(QDataStream &stream,
                                                             SmootherChainTarget *outputComplete,
                                                             SmootherChainTarget *outputRunning,
                                                             SmootherChainTargetGeneral *outputStatsComplete,
                                                             SmootherChainTargetGeneral *outputStatsRunning)
        : Binner(stream),
          Processor(stream),
          targetComplete(outputComplete),
          targetRunning(outputRunning),
          targetCompleteStats(outputStatsComplete),
          targetRunningStats(outputStatsRunning),
          targetFinished(false),
          targetValue(this), targetVectoredMagnitude(this)
{
    Deserialize::container(stream, buffer, [&]() {
        BufferValue value;
        stream >> value.time >> value.value >> value.vectored;
        return value;
    });
    stream >> targetFinished;
}

RealtimeChainVectorStatistics::~RealtimeChainVectorStatistics() = default;

RealtimeChainVectorStatistics::TargetValue::TargetValue(RealtimeChainVectorStatistics *n) : node(n)
{ }

RealtimeChainVectorStatistics::TargetValue::~TargetValue() = default;

void RealtimeChainVectorStatistics::TargetValue::incomingData(double start,
                                                              double end,
                                                              double value)
{ node->segmentValue(0, start, end, value); }

void RealtimeChainVectorStatistics::TargetValue::incomingAdvance(double time)
{ node->segmentAdvance(0, time); }

void RealtimeChainVectorStatistics::TargetValue::endData()
{ node->streamDone(0); }

RealtimeChainVectorStatistics::TargetVectoredMagnitude::TargetVectoredMagnitude(
        RealtimeChainVectorStatistics *n) : node(n)
{ }

RealtimeChainVectorStatistics::TargetVectoredMagnitude::~TargetVectoredMagnitude() = default;

void RealtimeChainVectorStatistics::TargetVectoredMagnitude::incomingData(double start,
                                                                          double end,
                                                                          double value)
{ node->segmentValue(1, start, end, value); }

void RealtimeChainVectorStatistics::TargetVectoredMagnitude::incomingAdvance(double time)
{ node->segmentAdvance(1, time); }

void RealtimeChainVectorStatistics::TargetVectoredMagnitude::endData()
{ node->streamDone(1); }

/* Don't need to protect this since this is "approximate" and only written
 * once anyway */
bool RealtimeChainVectorStatistics::finished()
{ return targetFinished; }

void RealtimeChainVectorStatistics::serialize(QDataStream &stream) const
{
    Binner::writeSerial(stream);
    Processor::writeSerial(stream);
    Serialize::container(stream, buffer, [&](const BufferValue &value) {
        stream << value.time << value.value << value.vectored;
    });
    stream << targetFinished;
}

void RealtimeChainVectorStatistics::pause()
{ Processor::freezeState(); }

void RealtimeChainVectorStatistics::resume()
{ Processor::unfreezeState(); }

void RealtimeChainVectorStatistics::streamDone(std::size_t stream)
{
    Q_ASSERT(!targetFinished);
    if (Processor::segmentEnd(stream)) {
        if (targetComplete)
            targetComplete->endData();
        if (targetRunning)
            targetRunning->endData();
        if (targetCompleteStats)
            targetCompleteStats->endData();
        if (targetRunningStats)
            targetRunningStats->endData();

        targetFinished = true;
    }
}

void RealtimeChainVectorStatistics::segmentDone(double start, double end, const double values[2])
{
    Q_UNUSED(end);
    Binner::addIncomingValue(start, Storage(values));
}

void RealtimeChainVectorStatistics::allStreamsAdvanced(double time)
{
    Binner::addIncomingAdvance(time);
    if (targetRunning)
        targetRunning->incomingAdvance(time);
    if (targetRunningStats)
        targetRunningStats->incomingAdvance(time);
    if (targetComplete)
        targetComplete->incomingAdvance(time);
    if (targetCompleteStats)
        targetCompleteStats->incomingAdvance(time);
}

void RealtimeChainVectorStatistics::addToBin(double time, const Storage &data)
{
    Q_ASSERT(FP::defined(time));
    buffer.emplace_back(time, data[0], data[1]);
    if (targetRunningStats || targetRunning) {
        processBuffer(time, targetRunning, targetRunningStats);
    }
}

bool RealtimeChainVectorStatistics::emitTotal(double time)
{
    if (buffer.empty())
        return false;
    bool result = false;
    if (targetCompleteStats || targetComplete) {
        result = processBuffer(time, targetComplete, targetCompleteStats) || result;
    }
    return result;
}

void RealtimeChainVectorStatistics::emitTerminal(double time)
{
    buffer.clear();
    if (targetComplete)
        targetComplete->incomingData(time, FP::undefined(), FP::undefined());
    if (targetCompleteStats)
        targetCompleteStats->incomingData(time, FP::undefined(), Variant::Root());
}

void RealtimeChainVectorStatistics::purgeBefore(double time)
{
    Q_ASSERT(FP::defined(time));
    auto endRemove = Range::heuristicUpperBoundDefined(buffer.begin(), buffer.end(), time);
    buffer.erase(buffer.begin(), endRemove);
}

bool RealtimeChainVectorStatistics::processBuffer(double endTime,
                                                  SmootherChainTarget *target,
                                                  SmootherChainTargetGeneral *targetStats)
{
    double average = 0.0;
    int count = 0;

    for (const auto &add : buffer) {
        if (!FP::defined(add.value))
            continue;
        ++count;
        average += add.value;
    }
    if (count != 0) {
        average /= (double) count;
    } else {
        average = FP::undefined();
    }

    bool result = false;

    if (target) {
        target->incomingData(endTime, FP::undefined(), average);
        result = FP::defined(average) || result;
    }

    if (targetStats) {
        QVector<double> values;
        double sumVectored = 0.0;
        int countVectored = 0;
        for (const auto &add : buffer) {
            values.append(add.value);

            if (!FP::defined(add.vectored))
                continue;
            sumVectored += add.vectored;
            ++countVectored;
        }
        if (countVectored != 0) {
            sumVectored /= (double) countVectored;
        } else {
            sumVectored = FP::undefined();
        }

        targetStats->incomingData(endTime, FP::undefined(),
                                  Internal::binnedChainGenerateVectorStatistics(values, average,
                                                                                sumVectored));

        result = true;
    }

    return result;
}


RealtimeChainDifference::RealtimeChainDifference(Time::LogicalTimeUnit setUnit,
                                                 int setCount,
                                                 bool setAlign,
                                                 SmootherChainTarget *outputCompleteStart,
                                                 SmootherChainTarget *outputCompleteEnd,
                                                 SmootherChainTarget *outputRunningStart,
                                                 SmootherChainTarget *outputRunningEnd)
        : RealtimeBinner<double>(setUnit, setCount, setAlign),
          targetCompleteStart(outputCompleteStart),
          targetCompleteEnd(outputCompleteEnd),
          targetRunningStart(outputRunningStart),
          targetRunningEnd(outputRunningEnd),
          targetFinished(false), target(this)
{ }

RealtimeChainDifference::RealtimeChainDifference(QDataStream &stream,
                                                 SmootherChainTarget *outputCompleteStart,
                                                 SmootherChainTarget *outputCompleteEnd,
                                                 SmootherChainTarget *outputRunningStart,
                                                 SmootherChainTarget *outputRunningEnd)
        : RealtimeBinner<double>(stream),
          targetCompleteStart(outputCompleteStart),
          targetCompleteEnd(outputCompleteEnd),
          targetRunningStart(outputRunningStart),
          targetRunningEnd(outputRunningEnd),
          targetFinished(false), target(this)
{
    Deserialize::container(stream, buffer, [&]() {
        BufferValue value;
        stream >> value.time >> value.value;
        return value;
    });
    stream >> targetFinished;
}

RealtimeChainDifference::~RealtimeChainDifference() = default;

RealtimeChainDifference::Target::Target(RealtimeChainDifference *n) : node(n)
{ }

RealtimeChainDifference::Target::~Target() = default;

void RealtimeChainDifference::Target::incomingData(double start, double end, double value)
{
    Q_UNUSED(end);
    node->addIncomingValue(start, value);
}

void RealtimeChainDifference::Target::incomingAdvance(double time)
{ node->handleAdvance(time); }

void RealtimeChainDifference::Target::endData()
{ node->dataFinished(); }

/* Don't need to protect this since this is "approximate" and only written
 * once anyway */
bool RealtimeChainDifference::finished()
{ return targetFinished; }

void RealtimeChainDifference::serialize(QDataStream &stream) const
{
    RealtimeBinner<double>::writeSerial(stream);
    Serialize::container(stream, buffer, [&](const BufferValue &value) {
        stream << value.time << value.value;
    });
    stream << targetFinished;
}

void RealtimeChainDifference::pause()
{ }

void RealtimeChainDifference::resume()
{ }

void RealtimeChainDifference::handleAdvance(double time)
{
    addIncomingAdvance(time);
    if (targetRunningStart)
        targetRunningStart->incomingAdvance(time);
    if (targetRunningEnd)
        targetRunningEnd->incomingAdvance(time);
    if (targetCompleteStart)
        targetCompleteStart->incomingAdvance(time);
    if (targetCompleteEnd)
        targetCompleteEnd->incomingAdvance(time);
}

void RealtimeChainDifference::dataFinished()
{
    if (targetCompleteStart)
        targetCompleteStart->endData();
    if (targetCompleteEnd)
        targetCompleteEnd->endData();
    if (targetRunningStart)
        targetRunningStart->endData();
    if (targetRunningEnd)
        targetRunningEnd->endData();

    targetFinished = true;
}

void RealtimeChainDifference::addToBin(double time, const double &data)
{
    Q_ASSERT(FP::defined(time));

    buffer.emplace_back(time, data);

    if (FP::defined(data)) {
        if (targetRunningStart || targetRunningEnd)
            processBuffer(time, targetRunningStart, targetRunningEnd);
    } else {
        if (targetRunningStart)
            targetRunningStart->incomingData(time, FP::undefined(), FP::undefined());
        if (targetRunningEnd)
            targetRunningEnd->incomingData(time, FP::undefined(), FP::undefined());

        /* Undefined values cause the buffer to split */
        if (buffer.size() <= 1)
            return;
        /* If it's already starting undefined then we don't need to do 
         * anything */
        if (!FP::defined(buffer[0].value))
            return;

        auto endRemove = buffer.begin();
        ++endRemove;
        for (auto endBuffer = buffer.end(); endRemove != endBuffer; ++endRemove) {
            if (!FP::defined(endRemove->value))
                break;
        }

        buffer.erase(buffer.begin(), endRemove);
    }
}

bool RealtimeChainDifference::emitTotal(double time)
{
    if (buffer.empty())
        return false;
    bool result = false;
    if (targetCompleteStart || targetCompleteEnd)
        result = processBuffer(time, targetCompleteStart, targetCompleteEnd) || result;
    return result;
}

void RealtimeChainDifference::emitTerminal(double time)
{
    buffer.clear();
    if (targetCompleteStart)
        targetCompleteStart->incomingData(time, FP::undefined(), FP::undefined());
    if (targetCompleteEnd)
        targetCompleteEnd->incomingData(time, FP::undefined(), FP::undefined());
}

void RealtimeChainDifference::purgeBefore(double time)
{
    Q_ASSERT(FP::defined(time));
    auto endRemove = Range::heuristicUpperBoundDefined(buffer.begin(), buffer.end(), time);
    buffer.erase(buffer.begin(), endRemove);
}

void RealtimeChainDifference::purgeUnaveraged(double)
{
    if (buffer.empty())
        return;
    buffer.erase(buffer.begin(), buffer.end() - 1);
}

bool RealtimeChainDifference::processBuffer(double time,
                                            SmootherChainTarget *targetFirst,
                                            SmootherChainTarget *targetLast)
{
    if (!targetFirst) {
        if (!targetLast)
            return false;
        auto last = buffer.crbegin();
        auto endBuffer = buffer.crend();
        for (; last != endBuffer && !FP::defined(last->value); ++last) { }
        if (last == endBuffer) {
            targetLast->incomingData(time, FP::undefined(), FP::undefined());
            return false;
        } else {
            targetLast->incomingData(time, FP::undefined(), last->value);
            return FP::defined(last->value);
        }
    }

    Q_ASSERT(targetFirst);

    auto last = buffer.crbegin();
    auto endBuffer = buffer.crend();
    for (; last != endBuffer && !FP::defined(last->value); ++last) { }
    if (last == endBuffer) {
        targetFirst->incomingData(time, FP::undefined(), FP::undefined());
        if (targetLast)
            targetLast->incomingData(time, FP::undefined(), FP::undefined());
        return false;
    }
    Q_ASSERT(FP::defined(last->value));

    auto first = last;
    for (++first; first != endBuffer && FP::defined(first->value); ++first) { }
    --first;

    Q_ASSERT(first != endBuffer);
    Q_ASSERT(FP::defined(first->value));
    targetFirst->incomingData(time, FP::undefined(), first->value);

    if (!targetLast)
        return true;

    Q_ASSERT(targetLast);
    if (first == last) {
        targetLast->incomingData(time, FP::undefined(), FP::undefined());
    } else {
        targetLast->incomingData(time, FP::undefined(), last->value);
    }

    return true;
}


RealtimeChainFlags::RealtimeChainFlags(Time::LogicalTimeUnit setUnit,
                                       int setCount,
                                       bool setAlign,
                                       SmootherChainTargetFlags *outputComplete,
                                       SmootherChainTargetFlags *outputRunning) : RealtimeBinner<
        Data::Variant::Flags>(setUnit, setCount, setAlign),
                                                                                  targetComplete(
                                                                                          outputComplete),
                                                                                  targetRunning(
                                                                                          outputRunning),
                                                                                  targetFinished(
                                                                                          false),
                                                                                  target(this)
{ }

RealtimeChainFlags::RealtimeChainFlags(QDataStream &stream,
                                       SmootherChainTargetFlags *outputComplete,
                                       SmootherChainTargetFlags *outputRunning) : RealtimeBinner<
        Data::Variant::Flags>(stream),
                                                                                  targetComplete(
                                                                                          outputComplete),
                                                                                  targetRunning(
                                                                                          outputRunning),
                                                                                  targetFinished(
                                                                                          false),
                                                                                  target(this)
{
    Deserialize::container(stream, buffer, [&]() {
        BufferValue value;
        stream >> value.time >> value.value;
        return value;
    });
    stream >> targetFinished;
}

RealtimeChainFlags::~RealtimeChainFlags() = default;

bool RealtimeChainFlags::finished()
{ return targetFinished; }

void RealtimeChainFlags::serialize(QDataStream &stream) const
{
    RealtimeBinner<Data::Variant::Flags>::writeSerial(stream);
    Serialize::container(stream, buffer, [&](const BufferValue &value) {
        stream << value.time << value.value;
    });
    stream << targetFinished;
}

void RealtimeChainFlags::pause()
{ }

void RealtimeChainFlags::resume()
{ }

RealtimeChainFlags::Target::Target(RealtimeChainFlags *n) : node(n)
{ }

RealtimeChainFlags::Target::~Target() = default;

void RealtimeChainFlags::Target::incomingData(double start,
                                              double end,
                                              const Data::Variant::Flags &value)
{
    Q_UNUSED(end);
    node->addIncomingValue(start, value);
}

void RealtimeChainFlags::Target::incomingAdvance(double time)
{ node->handleAdvance(time); }

void RealtimeChainFlags::Target::endData()
{ node->dataFinished(); }

void RealtimeChainFlags::handleAdvance(double time)
{
    addIncomingAdvance(time);

    if (targetRunning)
        targetRunning->incomingAdvance(time);
    if (targetComplete)
        targetComplete->incomingAdvance(time);
}

void RealtimeChainFlags::dataFinished()
{
    if (targetComplete)
        targetComplete->endData();
    if (targetRunning)
        targetRunning->endData();

    targetFinished = true;
}

void RealtimeChainFlags::addToBin(double time, const Data::Variant::Flags &data)
{
    Q_ASSERT(FP::defined(time));
    buffer.emplace_back(time, data);
    if (targetRunning)
        processBuffer(time, targetRunning);
}

bool RealtimeChainFlags::emitTotal(double time)
{
    if (buffer.empty())
        return false;
    bool result = false;
    if (targetComplete)
        result = processBuffer(time, targetComplete) || result;
    return result;
}

void RealtimeChainFlags::emitTerminal(double time)
{
    buffer.clear();
    if (targetComplete)
        targetComplete->incomingData(time, FP::undefined(), Data::Variant::Flags());
}

void RealtimeChainFlags::purgeBefore(double time)
{
    Q_ASSERT(FP::defined(time));
    auto endRemove = Range::heuristicUpperBoundDefined(buffer.begin(), buffer.end(), time);
    buffer.erase(buffer.begin(), endRemove);
}

bool RealtimeChainFlags::processBuffer(double time, SmootherChainTargetFlags *target)
{
    Data::Variant::Flags sum;
    for (const auto &add : buffer) {
        Util::merge(add.value, sum);
    }
    bool result = !sum.empty();
    target->incomingData(time, FP::undefined(), std::move(sum));
    return result;
}


RealtimeChainCore::RealtimeChainCore(bool setEnableStatistics) : enableStatistics(
        setEnableStatistics)
{ }

RealtimeChainCore::~RealtimeChainCore() = default;

void RealtimeChainCore::createSmoother(SmootherChainCoreEngineInterface *engine,
                                       SmootherChainCore::Type type)
{
    auto realtime = static_cast<RealtimeEngineInterface *>(engine->getAuxiliaryInterface());

    switch (type) {
    case SmootherChainCore::General: {
        SequenceName runningUnit(engine->getBaseUnit());
        runningUnit.setArchive(archiveBoxcar);
        SmootherChainTargetGeneral *outputStatsComplete;
        SmootherChainTargetGeneral *outputStatsRunning;
        if (enableStatistics) {
            outputStatsComplete = engine->addNewGeneralOutput(
                    engine->getBaseUnit().withFlavor(SequenceName::flavor_stats));
            outputStatsRunning =
                    engine->addNewGeneralOutput(runningUnit.withFlavor(SequenceName::flavor_stats));
        } else {
            outputStatsComplete = nullptr;
            outputStatsRunning = nullptr;
        }

        auto chain = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                   realtime->getAveragingCount(),
                                                   realtime->getAveragingAlign(),
                                                   engine->getOutput(0),
                                                   engine->addNewOutput(runningUnit),
                                                   outputStatsComplete, outputStatsRunning);
        engine->addChainNode(chain);
        engine->addInputTarget(0, chain->getTarget());
        realtime->addRealtimeController(chain);
        break;
    }

    case SmootherChainCore::Difference:
    case SmootherChainCore::DifferenceInitial: {
        SequenceName runningStartUnit(engine->getBaseUnit(0));
        runningStartUnit.setArchive(archiveBoxcar);
        auto outputRunningStart = engine->addNewOutput(runningStartUnit);

        SequenceName runningEndUnit(engine->getBaseUnit(1));
        runningEndUnit.setArchive(archiveBoxcar);
        auto outputRunningEnd = engine->addNewOutput(runningEndUnit);

        auto chain = new RealtimeChainDifference(realtime->getAveragingUnit(),
                                                 realtime->getAveragingCount(),
                                                 realtime->getAveragingAlign(),
                                                 engine->getOutput(0), engine->getOutput(1),
                                                 outputRunningStart, outputRunningEnd);
        engine->addChainNode(chain);
        engine->addInputTarget(0, chain->getTarget());
        /* Realtime doesn't have ends so ignore input 1 */
        realtime->addRealtimeController(chain);
        break;
    }

    case SmootherChainCore::Vector2D: {
        SequenceName runningDirectionUnit(engine->getBaseUnit(0));
        runningDirectionUnit.setArchive(archiveBoxcar);
        auto outputRunningDirection = engine->addNewOutput(runningDirectionUnit);

        SequenceName runningMagnitudeUnit(engine->getBaseUnit(1));
        runningMagnitudeUnit.setArchive(archiveBoxcar);
        auto outputRunningMagnitude = engine->addNewOutput(runningMagnitudeUnit);

        SmootherChainVector2DReconstructBase<RealtimeSegmentProcessor<2> > *reconstructComplete;
        SmootherChainVector2DReconstructBase<RealtimeSegmentProcessor<2> > *reconstructRunning;

        if (enableStatistics) {
            auto outputStatsComplete = engine->addNewGeneralOutput(
                    engine->getBaseUnit(1).withFlavor(SequenceName::flavor_stats));
            auto outputStatsRunning = engine->addNewGeneralOutput(
                    runningMagnitudeUnit.withFlavor(SequenceName::flavor_stats));

            auto statsComplete = new RealtimeChainVectorStatistics(realtime->getAveragingUnit(),
                                                                   realtime->getAveragingCount(),
                                                                   realtime->getAveragingAlign(),
                                                                   nullptr, nullptr,
                                                                   outputStatsComplete);
            engine->addChainNode(statsComplete);
            engine->addInputTarget(1, statsComplete->getTargetValue());
            realtime->addRealtimeController(statsComplete);

            auto statsRunning = new RealtimeChainVectorStatistics(realtime->getAveragingUnit(),
                                                                  realtime->getAveragingCount(),
                                                                  realtime->getAveragingAlign(),
                                                                  nullptr, nullptr, nullptr,
                                                                  outputStatsRunning);
            engine->addChainNode(statsRunning);
            engine->addInputTarget(1, statsRunning->getTargetValue());
            realtime->addRealtimeController(statsRunning);

            auto vectorMeanSplitComplete = new SmootherChainForward<2>(
                    {engine->getOutput(1), statsComplete->getTargetVectoredMagnitude()});
            engine->addChainNode(vectorMeanSplitComplete);

            auto vectorMeanSplitRunning = new SmootherChainForward<2>(
                    {outputRunningMagnitude, statsRunning->getTargetVectoredMagnitude()});
            engine->addChainNode(vectorMeanSplitRunning);

            reconstructComplete =
                    new SmootherChainVector2DReconstructBase<RealtimeSegmentProcessor<2> >(
                            engine->getOutput(0), vectorMeanSplitComplete);
            engine->addChainNode(reconstructComplete);

            reconstructRunning =
                    new SmootherChainVector2DReconstructBase<RealtimeSegmentProcessor<2> >(
                            outputRunningDirection, vectorMeanSplitRunning);
            engine->addChainNode(reconstructRunning);
        } else {
            reconstructComplete =
                    new SmootherChainVector2DReconstructBase<RealtimeSegmentProcessor<2> >(
                            engine->getOutput(0), engine->getOutput(1));
            engine->addChainNode(reconstructComplete);

            reconstructRunning =
                    new SmootherChainVector2DReconstructBase<RealtimeSegmentProcessor<2> >(
                            outputRunningDirection, outputRunningMagnitude);
            engine->addChainNode(reconstructRunning);
        }

        auto chainX = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                    realtime->getAveragingCount(),
                                                    realtime->getAveragingAlign(),
                                                    reconstructComplete->getTargetX(),
                                                    reconstructRunning->getTargetX());
        engine->addChainNode(chainX);
        realtime->addRealtimeController(chainX);

        auto chainY = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                    realtime->getAveragingCount(),
                                                    realtime->getAveragingAlign(),
                                                    reconstructComplete->getTargetY(),
                                                    reconstructRunning->getTargetY());
        engine->addChainNode(chainY);
        realtime->addRealtimeController(chainY);

        auto breakdown = new SmootherChainVector2DBreakdownBase<RealtimeSegmentProcessor<2> >(
                chainX->getTarget(), chainY->getTarget());
        engine->addChainNode(breakdown);
        engine->addInputTarget(0, breakdown->getTargetDirection());
        engine->addInputTarget(1, breakdown->getTargetMagnitude());
        break;
    }

    case SmootherChainCore::Vector3D: {
        SequenceName runningAzimuthUnit(engine->getBaseUnit(0));
        runningAzimuthUnit.setArchive(archiveBoxcar);
        auto outputRunningAzimuth = engine->addNewOutput(runningAzimuthUnit);

        SequenceName runningElevationUnit(engine->getBaseUnit(1));
        runningElevationUnit.setArchive(archiveBoxcar);
        auto outputRunningElevation = engine->addNewOutput(runningElevationUnit);

        SequenceName runningMagnitudeUnit(engine->getBaseUnit(2));
        runningMagnitudeUnit.setArchive(archiveBoxcar);
        auto outputRunningMagnitude = engine->addNewOutput(runningMagnitudeUnit);

        SmootherChainVector3DReconstructBase<RealtimeSegmentProcessor<3> > *reconstructComplete;
        SmootherChainVector3DReconstructBase<RealtimeSegmentProcessor<3> > *reconstructRunning;

        if (enableStatistics) {
            auto outputStatsComplete = engine->addNewGeneralOutput(
                    engine->getBaseUnit(2).withFlavor(SequenceName::flavor_stats));
            auto outputStatsRunning = engine->addNewGeneralOutput(
                    runningMagnitudeUnit.withFlavor(SequenceName::flavor_stats));

            auto statsComplete = new RealtimeChainVectorStatistics(realtime->getAveragingUnit(),
                                                                   realtime->getAveragingCount(),
                                                                   realtime->getAveragingAlign(),
                                                                   NULL, NULL, outputStatsComplete);
            engine->addChainNode(statsComplete);
            engine->addInputTarget(2, statsComplete->getTargetValue());
            realtime->addRealtimeController(statsComplete);

            auto statsRunning = new RealtimeChainVectorStatistics(realtime->getAveragingUnit(),
                                                                  realtime->getAveragingCount(),
                                                                  realtime->getAveragingAlign(),
                                                                  NULL, NULL, NULL,
                                                                  outputStatsRunning);
            engine->addChainNode(statsRunning);
            engine->addInputTarget(2, statsRunning->getTargetValue());
            realtime->addRealtimeController(statsRunning);

            auto vectorMeanSplitComplete = new SmootherChainForward<2>(
                    {engine->getOutput(2), statsComplete->getTargetVectoredMagnitude()});
            engine->addChainNode(vectorMeanSplitComplete);

            auto vectorMeanSplitRunning = new SmootherChainForward<2>(
                    {outputRunningMagnitude, statsRunning->getTargetVectoredMagnitude()});
            engine->addChainNode(vectorMeanSplitRunning);

            reconstructComplete =
                    new SmootherChainVector3DReconstructBase<RealtimeSegmentProcessor<3> >(
                            engine->getOutput(0), engine->getOutput(1), vectorMeanSplitComplete);
            engine->addChainNode(reconstructComplete);

            reconstructRunning =
                    new SmootherChainVector3DReconstructBase<RealtimeSegmentProcessor<3> >(
                            outputRunningAzimuth, outputRunningElevation, vectorMeanSplitRunning);
            engine->addChainNode(reconstructRunning);
        } else {
            reconstructComplete =
                    new SmootherChainVector3DReconstructBase<RealtimeSegmentProcessor<3> >(
                            engine->getOutput(0), engine->getOutput(1), engine->getOutput(2));
            engine->addChainNode(reconstructComplete);

            reconstructRunning =
                    new SmootherChainVector3DReconstructBase<RealtimeSegmentProcessor<3> >(
                            outputRunningAzimuth, outputRunningElevation, outputRunningMagnitude);
            engine->addChainNode(reconstructRunning);
        }

        auto chainX = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                    realtime->getAveragingCount(),
                                                    realtime->getAveragingAlign(),
                                                    reconstructComplete->getTargetX(),
                                                    reconstructRunning->getTargetX());
        engine->addChainNode(chainX);
        realtime->addRealtimeController(chainX);

        auto chainY = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                    realtime->getAveragingCount(),
                                                    realtime->getAveragingAlign(),
                                                    reconstructComplete->getTargetY(),
                                                    reconstructRunning->getTargetY());
        engine->addChainNode(chainY);
        realtime->addRealtimeController(chainY);

        auto chainZ = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                    realtime->getAveragingCount(),
                                                    realtime->getAveragingAlign(),
                                                    reconstructComplete->getTargetZ(),
                                                    reconstructRunning->getTargetZ());
        engine->addChainNode(chainZ);
        realtime->addRealtimeController(chainZ);

        auto breakdown = new SmootherChainVector3DBreakdownBase<RealtimeSegmentProcessor<3> >(
                chainX->getTarget(), chainY->getTarget(), chainZ->getTarget());
        engine->addChainNode(breakdown);
        engine->addInputTarget(0, breakdown->getTargetAzimuth());
        engine->addInputTarget(1, breakdown->getTargetElevation());
        engine->addInputTarget(2, breakdown->getTargetMagnitude());
        break;
    }

    case SmootherChainCore::BeersLawAbsorption:
    case SmootherChainCore::BeersLawAbsorptionInitial: {
        SequenceName runningUnit(engine->getBaseUnit());
        runningUnit.setArchive(archiveBoxcar);
        auto outputRunning = engine->addNewOutput(runningUnit);

        if (enableStatistics) {
            auto outputStatsComplete = engine->addNewGeneralOutput(
                    engine->getBaseUnit().withFlavor(SequenceName::flavor_stats));
            auto outputStatsRunning =
                    engine->addNewGeneralOutput(runningUnit.withFlavor(SequenceName::flavor_stats));

            auto stats = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                       realtime->getAveragingCount(),
                                                       realtime->getAveragingAlign(), nullptr,
                                                       nullptr, outputStatsComplete,
                                                       outputStatsRunning);
            engine->addChainNode(stats);
            engine->addNewInput(engine->getBaseUnit(), stats->getTarget());
            realtime->addRealtimeController(stats);
        }

        auto calculateComplete =
                new SmootherChainBeersLawAbsorptionBase<RealtimeSegmentProcessor<4> >(
                        engine->getOutput(0));
        engine->addChainNode(calculateComplete);

        auto calculateRunning =
                new SmootherChainBeersLawAbsorptionBase<RealtimeSegmentProcessor<4> >(
                        outputRunning);
        engine->addChainNode(calculateRunning);

        auto differenceL = new RealtimeChainDifference(realtime->getAveragingUnit(),
                                                       realtime->getAveragingCount(),
                                                       realtime->getAveragingAlign(),
                                                       calculateComplete->getTargetStartL(),
                                                       calculateComplete->getTargetEndL(),
                                                       calculateRunning->getTargetStartL(),
                                                       calculateRunning->getTargetEndL());
        engine->addChainNode(differenceL);
        engine->addInputTarget(0, differenceL->getTarget());
        realtime->addRealtimeController(differenceL);

        auto differenceI = new RealtimeChainDifference(realtime->getAveragingUnit(),
                                                       realtime->getAveragingCount(),
                                                       realtime->getAveragingAlign(),
                                                       calculateComplete->getTargetStartI(),
                                                       calculateComplete->getTargetEndI(),
                                                       calculateRunning->getTargetStartI(),
                                                       calculateRunning->getTargetEndI());
        engine->addChainNode(differenceI);
        if (type == SmootherChainCore::BeersLawAbsorption)
            engine->addInputTarget(2, differenceI->getTarget());
        else
            engine->addInputTarget(1, differenceI->getTarget());
        realtime->addRealtimeController(differenceI);
        /* Realtime doesn't have ends so ignore inputs 1 and 3 for full 
         * differences */
        break;
    }

    case SmootherChainCore::Dewpoint: {
        SequenceName runningUnit(engine->getBaseUnit());
        runningUnit.setArchive(archiveBoxcar);
        auto outputRunning = engine->addNewOutput(runningUnit);

        if (enableStatistics) {
            auto outputStatsComplete = engine->addNewGeneralOutput(
                    engine->getBaseUnit().withFlavor(SequenceName::flavor_stats));
            auto outputStatsRunning =
                    engine->addNewGeneralOutput(runningUnit.withFlavor(SequenceName::flavor_stats));

            auto stats = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                       realtime->getAveragingCount(),
                                                       realtime->getAveragingAlign(), nullptr,
                                                       nullptr, outputStatsComplete,
                                                       outputStatsRunning);
            engine->addChainNode(stats);
            engine->addNewInput(engine->getBaseUnit(), stats->getTarget());
            realtime->addRealtimeController(stats);
        }

        auto calculateComplete =
                new SmootherChainDewpointBase<RealtimeSegmentProcessor<2> >(engine->getOutput(0),
                                                                            engine->getOptions()
                                                                                  .hash("AlwaysWater")
                                                                                  .toBool());
        engine->addChainNode(calculateComplete);

        auto calculateRunning =
                new SmootherChainDewpointBase<RealtimeSegmentProcessor<2> >(outputRunning,
                                                                            engine->getOptions()
                                                                                  .hash("AlwaysWater")
                                                                                  .toBool());
        engine->addChainNode(calculateRunning);

        auto temperature = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                         realtime->getAveragingCount(),
                                                         realtime->getAveragingAlign(),
                                                         calculateComplete->getTargetTemperature(),
                                                         calculateRunning->getTargetTemperature());
        engine->addChainNode(temperature);
        engine->addInputTarget(0, temperature->getTarget());
        realtime->addRealtimeController(temperature);

        auto rh = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                realtime->getAveragingCount(),
                                                realtime->getAveragingAlign(),
                                                calculateComplete->getTargetRH(),
                                                calculateRunning->getTargetRH());
        engine->addChainNode(rh);
        engine->addInputTarget(1, rh->getTarget());
        realtime->addRealtimeController(rh);
        break;
    }

    case SmootherChainCore::RH: {
        SequenceName runningUnit(engine->getBaseUnit());
        runningUnit.setArchive(archiveBoxcar);
        auto outputRunning = engine->addNewOutput(runningUnit);

        if (enableStatistics) {
            auto outputStatsComplete = engine->addNewGeneralOutput(
                    engine->getBaseUnit().withFlavor(SequenceName::flavor_stats));
            auto outputStatsRunning =
                    engine->addNewGeneralOutput(runningUnit.withFlavor(SequenceName::flavor_stats));

            auto stats = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                       realtime->getAveragingCount(),
                                                       realtime->getAveragingAlign(), nullptr,
                                                       nullptr, outputStatsComplete,
                                                       outputStatsRunning);
            engine->addChainNode(stats);
            engine->addNewInput(engine->getBaseUnit(), stats->getTarget());
            realtime->addRealtimeController(stats);
        }

        auto calculateComplete =
                new SmootherChainRHBase<RealtimeSegmentProcessor<2> >(engine->getOutput(0),
                                                                      engine->getOptions()
                                                                            .hash("AlwaysWater")
                                                                            .toBool());
        engine->addChainNode(calculateComplete);

        auto calculateRunning = new SmootherChainRHBase<RealtimeSegmentProcessor<2> >(outputRunning,
                                                                                      engine->getOptions()
                                                                                            .hash("AlwaysWater")
                                                                                            .toBool());
        engine->addChainNode(calculateRunning);

        auto temperature = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                         realtime->getAveragingCount(),
                                                         realtime->getAveragingAlign(),
                                                         calculateComplete->getTargetTemperature(),
                                                         calculateRunning->getTargetTemperature());
        engine->addChainNode(temperature);
        engine->addInputTarget(0, temperature->getTarget());
        realtime->addRealtimeController(temperature);

        auto dewpoint = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                      realtime->getAveragingCount(),
                                                      realtime->getAveragingAlign(),
                                                      calculateComplete->getTargetDewpoint(),
                                                      calculateRunning->getTargetDewpoint());
        engine->addChainNode(dewpoint);
        engine->addInputTarget(1, dewpoint->getTarget());
        realtime->addRealtimeController(dewpoint);
        break;
    }

    case SmootherChainCore::RHExtrapolate: {
        SequenceName runningUnit(engine->getBaseUnit());
        runningUnit.setArchive(archiveBoxcar);
        auto outputRunning = engine->addNewOutput(runningUnit);

        if (enableStatistics) {
            auto outputStatsComplete = engine->addNewGeneralOutput(
                    engine->getBaseUnit().withFlavor(SequenceName::flavor_stats));
            auto outputStatsRunning =
                    engine->addNewGeneralOutput(runningUnit.withFlavor(SequenceName::flavor_stats));

            auto stats = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                       realtime->getAveragingCount(),
                                                       realtime->getAveragingAlign(), nullptr,
                                                       nullptr, outputStatsComplete,
                                                       outputStatsRunning);
            engine->addChainNode(stats);
            engine->addNewInput(engine->getBaseUnit(), stats->getTarget());
            realtime->addRealtimeController(stats);
        }

        auto calculateComplete = new SmootherChainRHExtrapolateBase<RealtimeSegmentProcessor<3> >(
                engine->getOutput(0), engine->getOptions().hash("AlwaysWater").toBool());
        engine->addChainNode(calculateComplete);

        auto calculateRunning =
                new SmootherChainRHExtrapolateBase<RealtimeSegmentProcessor<3> >(outputRunning,
                                                                                 engine->getOptions()
                                                                                       .hash("AlwaysWater")
                                                                                       .toBool());
        engine->addChainNode(calculateRunning);

        auto temperatureIn = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                           realtime->getAveragingCount(),
                                                           realtime->getAveragingAlign(),
                                                           calculateComplete->getTargetTemperatureIn(),
                                                           calculateRunning->getTargetTemperatureIn());
        engine->addChainNode(temperatureIn);
        engine->addInputTarget(0, temperatureIn->getTarget());
        realtime->addRealtimeController(temperatureIn);

        auto rhIn = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                  realtime->getAveragingCount(),
                                                  realtime->getAveragingAlign(),
                                                  calculateComplete->getTargetRHIn(),
                                                  calculateRunning->getTargetRHIn());
        engine->addChainNode(rhIn);
        engine->addInputTarget(1, rhIn->getTarget());
        realtime->addRealtimeController(rhIn);

        auto temperatureOut = new RealtimeChainConventional(realtime->getAveragingUnit(),
                                                            realtime->getAveragingCount(),
                                                            realtime->getAveragingAlign(),
                                                            calculateComplete->getTargetTemperatureOut(),
                                                            calculateRunning->getTargetTemperatureOut());
        engine->addChainNode(temperatureOut);
        engine->addInputTarget(2, temperatureOut->getTarget());
        realtime->addRealtimeController(temperatureOut);
        break;
    }

    }
}

void RealtimeChainCore::deserializeSmoother(QDataStream &stream,
                                            SmootherChainCoreEngineInterface *engine,
                                            SmootherChainCore::Type type)
{
    auto realtime = static_cast<RealtimeEngineInterface *>(engine->getAuxiliaryInterface());

    switch (type) {
    case SmootherChainCore::General: {
        auto chain =
                new RealtimeChainConventional(stream, engine->getOutput(0), engine->getOutput(1),
                                              enableStatistics ? engine->getGeneralOutput(0)
                                                               : nullptr,
                                              enableStatistics ? engine->getGeneralOutput(1)
                                                               : nullptr);
        engine->addChainNode(chain);
        engine->addInputTarget(0, chain->getTarget());
        realtime->addRealtimeController(chain);

        break;
    }

    case SmootherChainCore::DifferenceInitial:
    case SmootherChainCore::Difference: {
        auto chain = new RealtimeChainDifference(stream, engine->getOutput(0), engine->getOutput(1),
                                                 engine->getOutput(2), engine->getOutput(3));
        engine->addChainNode(chain);
        engine->addInputTarget(0, chain->getTarget());
        /* Realtime doesn't have ends so ignore input 1 */
        realtime->addRealtimeController(chain);
        break;
    }

    case SmootherChainCore::Vector2D: {
        SmootherChainVector2DReconstructBase<RealtimeSegmentProcessor<2> > *reconstructComplete;
        SmootherChainVector2DReconstructBase<RealtimeSegmentProcessor<2> > *reconstructRunning;

        if (enableStatistics) {
            auto statsComplete = new RealtimeChainVectorStatistics(stream, nullptr, nullptr,
                                                                   engine->getGeneralOutput(0));
            engine->addChainNode(statsComplete);
            engine->addInputTarget(1, statsComplete->getTargetValue());
            realtime->addRealtimeController(statsComplete);

            auto statsRunning = new RealtimeChainVectorStatistics(stream, nullptr, nullptr, nullptr,
                                                                  engine->getGeneralOutput(1));
            engine->addChainNode(statsRunning);
            engine->addInputTarget(1, statsRunning->getTargetValue());
            realtime->addRealtimeController(statsRunning);

            auto vectorMeanSplitComplete = new SmootherChainForward<2>(
                    {engine->getOutput(1), statsComplete->getTargetVectoredMagnitude()});
            engine->addChainNode(vectorMeanSplitComplete);

            auto vectorMeanSplitRunning = new SmootherChainForward<2>(
                    {engine->getOutput(3), statsRunning->getTargetVectoredMagnitude()});
            engine->addChainNode(vectorMeanSplitRunning);

            reconstructComplete =
                    new SmootherChainVector2DReconstructBase<RealtimeSegmentProcessor<2> >(stream,
                                                                                           engine->getOutput(
                                                                                                   0),
                                                                                           vectorMeanSplitComplete);
            engine->addChainNode(reconstructComplete);

            reconstructRunning =
                    new SmootherChainVector2DReconstructBase<RealtimeSegmentProcessor<2> >(stream,
                                                                                           engine->getOutput(
                                                                                                   2),
                                                                                           vectorMeanSplitRunning);
            engine->addChainNode(reconstructRunning);
        } else {
            reconstructComplete =
                    new SmootherChainVector2DReconstructBase<RealtimeSegmentProcessor<2> >(stream,
                                                                                           engine->getOutput(
                                                                                                   0),
                                                                                           engine->getOutput(
                                                                                                   1));
            engine->addChainNode(reconstructComplete);

            reconstructRunning =
                    new SmootherChainVector2DReconstructBase<RealtimeSegmentProcessor<2> >(stream,
                                                                                           engine->getOutput(
                                                                                                   2),
                                                                                           engine->getOutput(
                                                                                                   3));
            engine->addChainNode(reconstructRunning);
        }


        auto chainX = new RealtimeChainConventional(stream, reconstructComplete->getTargetX(),
                                                    reconstructRunning->getTargetX());
        engine->addChainNode(chainX);
        realtime->addRealtimeController(chainX);

        auto chainY = new RealtimeChainConventional(stream, reconstructComplete->getTargetY(),
                                                    reconstructRunning->getTargetY());
        engine->addChainNode(chainY);
        realtime->addRealtimeController(chainY);

        auto breakdown =
                new SmootherChainVector2DBreakdownBase<RealtimeSegmentProcessor<2> >(stream,
                                                                                     chainX->getTarget(),
                                                                                     chainY->getTarget());
        engine->addChainNode(breakdown);
        engine->addInputTarget(0, breakdown->getTargetDirection());
        engine->addInputTarget(1, breakdown->getTargetMagnitude());
        break;
    }

    case SmootherChainCore::Vector3D: {
        SmootherChainVector3DReconstructBase<RealtimeSegmentProcessor<3> > *reconstructComplete;
        SmootherChainVector3DReconstructBase<RealtimeSegmentProcessor<3> > *reconstructRunning;

        if (enableStatistics) {
            auto statsComplete = new RealtimeChainVectorStatistics(stream, nullptr, nullptr,
                                                                   engine->getGeneralOutput(0));
            engine->addChainNode(statsComplete);
            engine->addInputTarget(1, statsComplete->getTargetValue());
            realtime->addRealtimeController(statsComplete);

            auto statsRunning = new RealtimeChainVectorStatistics(stream, nullptr, nullptr, nullptr,
                                                                  engine->getGeneralOutput(1));
            engine->addChainNode(statsRunning);
            engine->addInputTarget(1, statsRunning->getTargetValue());
            realtime->addRealtimeController(statsRunning);

            auto vectorMeanSplitComplete = new SmootherChainForward<2>(
                    {engine->getOutput(2), statsComplete->getTargetVectoredMagnitude()});
            engine->addChainNode(vectorMeanSplitComplete);

            auto vectorMeanSplitRunning = new SmootherChainForward<2>(
                    {engine->getOutput(5), statsRunning->getTargetVectoredMagnitude()});
            engine->addChainNode(vectorMeanSplitRunning);

            reconstructComplete =
                    new SmootherChainVector3DReconstructBase<RealtimeSegmentProcessor<3> >(stream,
                                                                                           engine->getOutput(
                                                                                                   0),
                                                                                           engine->getOutput(
                                                                                                   1),
                                                                                           vectorMeanSplitComplete);
            engine->addChainNode(reconstructComplete);

            reconstructRunning =
                    new SmootherChainVector3DReconstructBase<RealtimeSegmentProcessor<3> >(stream,
                                                                                           engine->getOutput(
                                                                                                   3),
                                                                                           engine->getOutput(
                                                                                                   4),
                                                                                           vectorMeanSplitRunning);
            engine->addChainNode(reconstructRunning);
        } else {
            reconstructComplete =
                    new SmootherChainVector3DReconstructBase<RealtimeSegmentProcessor<3> >(stream,
                                                                                           engine->getOutput(
                                                                                                   0),
                                                                                           engine->getOutput(
                                                                                                   1),
                                                                                           engine->getOutput(
                                                                                                   2));
            engine->addChainNode(reconstructComplete);

            reconstructRunning =
                    new SmootherChainVector3DReconstructBase<RealtimeSegmentProcessor<3> >(stream,
                                                                                           engine->getOutput(
                                                                                                   3),
                                                                                           engine->getOutput(
                                                                                                   4),
                                                                                           engine->getOutput(
                                                                                                   5));
            engine->addChainNode(reconstructRunning);
        }

        auto chainX = new RealtimeChainConventional(stream, reconstructComplete->getTargetX(),
                                                    reconstructRunning->getTargetX());
        engine->addChainNode(chainX);
        realtime->addRealtimeController(chainX);

        auto chainY = new RealtimeChainConventional(stream, reconstructComplete->getTargetY(),
                                                    reconstructRunning->getTargetY());
        engine->addChainNode(chainY);
        realtime->addRealtimeController(chainY);

        auto chainZ = new RealtimeChainConventional(stream, reconstructComplete->getTargetZ(),
                                                    reconstructRunning->getTargetZ());
        engine->addChainNode(chainZ);
        realtime->addRealtimeController(chainZ);

        auto breakdown =
                new SmootherChainVector3DBreakdownBase<RealtimeSegmentProcessor<3> >(stream,
                                                                                     chainX->getTarget(),
                                                                                     chainY->getTarget(),
                                                                                     chainZ->getTarget());
        engine->addChainNode(breakdown);
        engine->addInputTarget(0, breakdown->getTargetAzimuth());
        engine->addInputTarget(1, breakdown->getTargetElevation());
        engine->addInputTarget(2, breakdown->getTargetMagnitude());
        break;
    }

    case SmootherChainCore::BeersLawAbsorption:
    case SmootherChainCore::BeersLawAbsorptionInitial: {
        if (enableStatistics) {
            auto stats = new RealtimeChainConventional(stream, nullptr, nullptr,
                                                       engine->getGeneralOutput(0),
                                                       engine->getGeneralOutput(1));
            engine->addChainNode(stats);
            if (type == SmootherChainCore::BeersLawAbsorption)
                engine->addInputTarget(4, stats->getTarget());
            else
                engine->addInputTarget(2, stats->getTarget());
            realtime->addRealtimeController(stats);
        }

        auto calculateComplete =
                new SmootherChainBeersLawAbsorptionBase<RealtimeSegmentProcessor<4> >(stream,
                                                                                      engine->getOutput(
                                                                                              0));
        engine->addChainNode(calculateComplete);

        auto calculateRunning =
                new SmootherChainBeersLawAbsorptionBase<RealtimeSegmentProcessor<4> >(stream,
                                                                                      engine->getOutput(
                                                                                              1));
        engine->addChainNode(calculateRunning);

        auto differenceL = new RealtimeChainDifference(stream, calculateComplete->getTargetStartL(),
                                                       calculateComplete->getTargetEndL(),
                                                       calculateRunning->getTargetStartL(),
                                                       calculateRunning->getTargetEndL());
        engine->addChainNode(differenceL);
        engine->addInputTarget(0, differenceL->getTarget());
        realtime->addRealtimeController(differenceL);

        auto differenceI = new RealtimeChainDifference(stream, calculateComplete->getTargetStartI(),
                                                       calculateComplete->getTargetEndI(),
                                                       calculateRunning->getTargetStartI(),
                                                       calculateRunning->getTargetEndI());
        engine->addChainNode(differenceI);
        if (type == SmootherChainCore::BeersLawAbsorption)
            engine->addInputTarget(2, differenceI->getTarget());
        else
            engine->addInputTarget(1, differenceI->getTarget());
        realtime->addRealtimeController(differenceI);
        /* Realtime doesn't have ends so ignore inputs 1 and 3 for full 
         * differences */
        break;
    }

    case SmootherChainCore::Dewpoint: {
        if (enableStatistics) {
            auto stats = new RealtimeChainConventional(stream, nullptr, nullptr,
                                                       engine->getGeneralOutput(0),
                                                       engine->getGeneralOutput(1));
            engine->addChainNode(stats);
            engine->addInputTarget(2, stats->getTarget());
            realtime->addRealtimeController(stats);
        }

        auto calculateComplete = new SmootherChainDewpointBase<RealtimeSegmentProcessor<2> >(stream,
                                                                                             engine->getOutput(
                                                                                                     0));
        engine->addChainNode(calculateComplete);

        auto calculateRunning = new SmootherChainDewpointBase<RealtimeSegmentProcessor<2> >(stream,
                                                                                            engine->getOutput(
                                                                                                    1));
        engine->addChainNode(calculateRunning);

        auto temperature =
                new RealtimeChainConventional(stream, calculateComplete->getTargetTemperature(),
                                              calculateRunning->getTargetTemperature());
        engine->addChainNode(temperature);
        engine->addInputTarget(0, temperature->getTarget());
        realtime->addRealtimeController(temperature);

        auto rh = new RealtimeChainConventional(stream, calculateComplete->getTargetRH(),
                                                calculateRunning->getTargetRH());
        engine->addChainNode(rh);
        engine->addInputTarget(1, rh->getTarget());
        realtime->addRealtimeController(rh);
        break;
    }

    case SmootherChainCore::RH: {
        if (enableStatistics) {
            auto stats = new RealtimeChainConventional(stream, nullptr, nullptr,
                                                       engine->getGeneralOutput(0),
                                                       engine->getGeneralOutput(1));
            engine->addChainNode(stats);
            engine->addInputTarget(2, stats->getTarget());
            realtime->addRealtimeController(stats);
        }

        auto calculateComplete =
                new SmootherChainRHBase<RealtimeSegmentProcessor<2> >(stream, engine->getOutput(0));
        engine->addChainNode(calculateComplete);

        auto calculateRunning =
                new SmootherChainRHBase<RealtimeSegmentProcessor<2> >(stream, engine->getOutput(1));
        engine->addChainNode(calculateRunning);

        auto temperature =
                new RealtimeChainConventional(stream, calculateComplete->getTargetTemperature(),
                                              calculateRunning->getTargetTemperature());
        engine->addChainNode(temperature);
        engine->addInputTarget(0, temperature->getTarget());
        realtime->addRealtimeController(temperature);

        auto dewpoint =
                new RealtimeChainConventional(stream, calculateComplete->getTargetDewpoint(),
                                              calculateRunning->getTargetDewpoint());
        engine->addChainNode(dewpoint);
        engine->addInputTarget(1, dewpoint->getTarget());
        realtime->addRealtimeController(dewpoint);
        break;
    }

    case SmootherChainCore::RHExtrapolate: {
        if (enableStatistics) {
            auto stats = new RealtimeChainConventional(stream, nullptr, nullptr,
                                                       engine->getGeneralOutput(0),
                                                       engine->getGeneralOutput(1));
            engine->addChainNode(stats);
            engine->addInputTarget(3, stats->getTarget());
            realtime->addRealtimeController(stats);
        }

        auto calculateComplete =
                new SmootherChainRHExtrapolateBase<RealtimeSegmentProcessor<3> >(stream,
                                                                                 engine->getOutput(
                                                                                         0));
        engine->addChainNode(calculateComplete);

        auto calculateRunning =
                new SmootherChainRHExtrapolateBase<RealtimeSegmentProcessor<3> >(stream,
                                                                                 engine->getOutput(
                                                                                         1));
        engine->addChainNode(calculateRunning);

        auto temperatureIn =
                new RealtimeChainConventional(stream, calculateComplete->getTargetTemperatureIn(),
                                              calculateRunning->getTargetTemperatureIn());
        engine->addChainNode(temperatureIn);
        engine->addInputTarget(0, temperatureIn->getTarget());
        realtime->addRealtimeController(temperatureIn);

        auto rhIn = new RealtimeChainConventional(stream, calculateComplete->getTargetRHIn(),
                                                  calculateRunning->getTargetRHIn());
        engine->addChainNode(rhIn);
        engine->addInputTarget(1, rhIn->getTarget());
        realtime->addRealtimeController(rhIn);

        auto temperatureOut =
                new RealtimeChainConventional(stream, calculateComplete->getTargetTemperatureOut(),
                                              calculateRunning->getTargetTemperatureOut());
        engine->addChainNode(temperatureOut);
        engine->addInputTarget(2, temperatureOut->getTarget());
        realtime->addRealtimeController(temperatureOut);
        break;
    }

    }
}

SmootherChainCore::HandlerMode RealtimeChainCore::smootherHandler(SmootherChainCore::Type type)
{
    switch (type) {
    case SmootherChainCore::General:
    case SmootherChainCore::Vector2D:
    case SmootherChainCore::Vector3D:
    case SmootherChainCore::Dewpoint:
    case SmootherChainCore::RH:
    case SmootherChainCore::RHExtrapolate:
    case SmootherChainCore::Difference:
    case SmootherChainCore::DifferenceInitial:
    case SmootherChainCore::BeersLawAbsorption:
    case SmootherChainCore::BeersLawAbsorptionInitial:
        return SmootherChainCore::Repeatable;
    }
    Q_ASSERT(false);
    return SmootherChainCore::Unhandled;
}

bool RealtimeChainCore::flagsHandler()
{ return true; }

void RealtimeChainCore::createFlags(SmootherChainCoreEngineInterface *engine)
{
    auto realtime = static_cast<RealtimeEngineInterface *>(engine->getAuxiliaryInterface());
    SequenceName runningUnit(engine->getBaseUnit());
    runningUnit.setArchive(archiveBoxcar);
    auto flags = new RealtimeChainFlags(realtime->getAveragingUnit(), realtime->getAveragingCount(),
                                        realtime->getAveragingAlign(), engine->getFlagsOutput(0),
                                        engine->addNewFlagsOutput(runningUnit));
    engine->addChainNode(flags);
    engine->addFlagsInputTarget(0, flags->getTarget());
    realtime->addRealtimeController(flags);
}

void RealtimeChainCore::deserializeFlags(QDataStream &stream,
                                         SmootherChainCoreEngineInterface *engine)
{
    auto realtime = static_cast<RealtimeEngineInterface *>(engine->getAuxiliaryInterface());
    auto flags =
            new RealtimeChainFlags(stream, engine->getFlagsOutput(0), engine->getFlagsOutput(1));
    engine->addChainNode(flags);
    engine->addFlagsInputTarget(0, flags->getTarget());
    realtime->addRealtimeController(flags);
}

bool RealtimeChainCore::generatedOutput(const SequenceName &unit)
{ return unit.hasFlavor(SequenceName::flavor_stats); }

Variant::Root RealtimeChainCore::getProcessingMetadata(double) const
{
    Variant::Root meta;
    meta.write().hash("Type").setString("Realtime");
    return meta;
}

QSet<double> RealtimeChainCore::getProcessingMetadataBreaks() const
{ return QSet<double>(); }


RealtimeEngine::RealtimeEngine(bool setEnableStatistics) : SmoothingEngine(
        new RealtimeChainCore(setEnableStatistics), false),
                                                           enableStatistics(setEnableStatistics),
                                                           averageUnit(Time::Minute),
                                                           averageCount(1),
                                                           averageAlign(true),
                                                           lastAdvanceTime(Time::time()),
                                                           instantIngress(nullptr),
                                                           chainRealtimeControllers(
                                                                   std::make_shared<
                                                                           std::unordered_map<
                                                                                   AuxiliaryChainController *,
                                                                                   std::vector<
                                                                                           RealtimeController *>>>())
{
    instantIngress = createOutputIngress();
}

RealtimeEngine::~RealtimeEngine() = default;

void RealtimeEngine::deserialize(QDataStream &stream)
{
    bool wasStatisticsEnabled = enableStatistics;
    stream >> enableStatistics;
    if (enableStatistics != wasStatisticsEnabled)
        changeCore(new RealtimeChainCore(enableStatistics));
    {
        quint8 i8;
        stream >> i8;
        averageUnit = static_cast<Time::LogicalTimeUnit>(i8);
    }
    {
        quint32 i32;
        stream >> i32;
        averageCount = static_cast<int>(i32);
    }
    stream >> averageAlign >> lastAdvanceTime;
    SmoothingEngine::deserialize(stream);
}

void RealtimeEngine::serialize(QDataStream &stream)
{
    pauseProcessing();
    stream << enableStatistics;
    stream << static_cast<quint8>(averageUnit) << static_cast<quint32>(averageCount) << averageAlign
           << lastAdvanceTime;
    serializeInternal(stream);
    resumeProcessing();
}

void RealtimeEngine::setAveraging(Time::LogicalTimeUnit setUnit, int setCount, bool setAlign)
{
    averageUnit = setUnit;
    averageCount = setCount;
    averageAlign = setAlign;

    pauseProcessing();

    for (const auto &targetChain : *chainRealtimeControllers) {
        for (const auto &target : targetChain.second) {
            target->setBinning(averageUnit, averageCount, averageAlign);
        }
    }

    resumeProcessing();
}

bool RealtimeEngine::canProcess(IncomingBuffer::const_iterator incomingBegin,
                                IncomingBuffer::const_iterator incomingEnd,
                                double advanceTime)
{ return FP::defined(advanceTime) || (incomingBegin != incomingEnd); }

void RealtimeEngine::startProcessing(IncomingBuffer::iterator incomingBegin,
                                     IncomingBuffer::iterator incomingEnd,
                                     double &advanceTime,
                                     double &holdbackTime)
{
    double now = Time::time();
    if (now < lastAdvanceTime) {
        now = lastAdvanceTime;
        advanceTime = now;
    } else {
        advanceTime = now + 1E-6;
    }
    lastAdvanceTime = advanceTime;
    holdbackTime = now;

    for (; incomingBegin != incomingEnd; ++incomingBegin) {
        incomingBegin->setStart(now);
        incomingBegin->setEnd(FP::undefined());

        if (incomingBegin->getUnit().isMeta()) {
            SequenceValue instant = *incomingBegin;
            instant.setArchive(archiveInstantMeta);
            instantIngress->incomingData(std::move(instant));

            incomingBegin->setArchive(archiveRawMeta);
        } else {
            SequenceValue instant = *incomingBegin;
            instant.setArchive(archiveInstant);
            instantIngress->incomingData(std::move(instant));

            incomingBegin->setArchive(archiveRaw);
        }
    }

    instantIngress->incomingAdvance(advanceTime);
}

void RealtimeEngine::auxiliaryEndAfter()
{ instantIngress->endData(); }

SinkMultiplexer::Sink *RealtimeEngine::createDataIngress(const SequenceName &unit)
{
    Q_UNUSED(unit);
    return createOutputIngress();
}

std::unique_ptr<SmoothingEngine::EngineInterface> RealtimeEngine::createEngineInterface()
{ return std::unique_ptr<SmoothingEngine::EngineInterface>(new EngineInterface(this)); }

RealtimeEngine::EngineInterface::EngineInterface(RealtimeEngine *engine)
        : SmoothingEngine::EngineInterface(engine), parent(engine)
{
    chainController = new RealtimeEngine::AuxiliaryChainController(engine);
    setChainAuxiliary(chainController);
}

RealtimeEngine::EngineInterface::~EngineInterface() = default;

void RealtimeEngine::EngineInterface::addRealtimeController(RealtimeController *controller)
{
    (*(parent->chainRealtimeControllers))[chainController].push_back(controller);
}

Time::LogicalTimeUnit RealtimeEngine::EngineInterface::getAveragingUnit()
{ return parent->averageUnit; }

int RealtimeEngine::EngineInterface::getAveragingCount()
{ return parent->averageCount; }

bool RealtimeEngine::EngineInterface::getAveragingAlign()
{ return parent->averageAlign; }

SmootherChainCoreEngineAuxiliaryInterface *RealtimeEngine::EngineInterface::getAuxiliaryInterface()
{ return this; }

RealtimeEngine::AuxiliaryChainController::AuxiliaryChainController(RealtimeEngine *engine)
        : chainRealtimeControllers(engine->chainRealtimeControllers)
{ }

RealtimeEngine::AuxiliaryChainController::~AuxiliaryChainController()
{
    auto check = chainRealtimeControllers->find(this);
    if (check != chainRealtimeControllers->end()) {
        chainRealtimeControllers->erase(check);
    }
}

std::unique_ptr<
        SmoothingEngine::DispatchTarget> RealtimeEngine::createMetaDispatch(const Data::SequenceName &unit,
                                                                            DispatchTarget *underlay)
{
    auto meta = new DispatchMetadata(this, unit, createOutputIngress());
    auto defaultUnderlay = static_cast<DispatchDefaultUnderlay *>(underlay);
    defaultUnderlay->addForwardTarget(meta);
    meta->setBreaks(getMetadataBreaks(unit));
    meta->overlaySegmenter(defaultUnderlay->getSegmenter());
    return std::unique_ptr<SmoothingEngine::DispatchTarget>(meta);
}

std::unique_ptr<
        SmoothingEngine::DispatchTarget> RealtimeEngine::createDataDispatch(const Data::SequenceName &unit,
                                                                            DispatchTarget *underlay,
                                                                            DispatchTarget *meta)
{
    Q_UNUSED(meta);
    auto data = new DispatchData(this, unit);
    auto defaultUnderlay = static_cast<DispatchDefaultUnderlay *>(underlay);
    defaultUnderlay->addForwardTarget(data);
    data->overlaySegmenter(defaultUnderlay->getSegmenter());
    return std::unique_ptr<SmoothingEngine::DispatchTarget>(data);
}


RealtimeEngine::DispatchMetadata::DispatchMetadata(RealtimeEngine *engine,
                                                   const SequenceName &setUnit,
                                                   SinkMultiplexer::Sink *target)
        : SmoothingEngine::DispatchMetadata(engine, setUnit, target),
          parent(engine),
          haveHadValue(false),
          lastValue(),
          outputTarget(target),
          unitBoxcar(setUnit)
{
    unitBoxcar.setArchive(archiveBoxcarMeta);
}

RealtimeEngine::DispatchMetadata::~DispatchMetadata() = default;

void RealtimeEngine::DispatchMetadata::incomingData(SequenceValue &&value)
{
    Q_ASSERT(FP::defined(value.getStart()));
    if (!haveHadValue) {
        haveHadValue = true;
        incomingSegment(ValueSegment(value.getStart(), FP::undefined(), value.root()));

        auto root = value.root();
        {
            auto metadata = root.write();
            parent->transformMetadata(unitBoxcar, value.getStart(), metadata);
        }
        outputTarget->emplaceData(SequenceIdentity(unitBoxcar, value.getStart(), FP::undefined()),
                                  std::move(root));
        return;
    } else if (FP::defined(lastValue.getStart()) && lastValue.getStart() != value.getStart()) {
        incomingSegment(ValueSegment(lastValue));

        auto root = lastValue.root();
        {
            auto metadata = root.write();
            parent->transformMetadata(unitBoxcar, lastValue.getStart(), metadata);
        }
        outputTarget->emplaceData(
                SequenceIdentity(unitBoxcar, lastValue.getStart(), lastValue.getEnd()),
                std::move(root));
    }
    lastValue = ValueSegment(value.getStart(), FP::undefined(), std::move(value.root()));
}

void RealtimeEngine::DispatchMetadata::incomingAdvance(double time)
{
    Q_ASSERT(!FP::defined(lastValue.getStart()) || lastValue.getStart() <= time);
    if (FP::defined(lastValue.getStart())) {
        if (time == lastValue.getStart())
            return;
        incomingSegment(ValueSegment(lastValue));

        auto root = std::move(lastValue.root());
        {
            auto metadata = root.write();
            parent->transformMetadata(unitBoxcar, lastValue.getStart(), metadata);
        }
        outputTarget->incomingData(SequenceValue(unitBoxcar, std::move(root), lastValue.getStart(),
                                                 lastValue.getEnd()));

        lastValue = ValueSegment();
    }

    outputTarget->incomingAdvance(time);
}

void RealtimeEngine::DispatchMetadata::endData()
{
    if (FP::defined(lastValue.getStart())) {
        incomingSegment(ValueSegment(lastValue));

        auto root = lastValue.root();
        {
            auto metadata = root.write();
            parent->transformMetadata(unitBoxcar, lastValue.getStart(), metadata);
        }
        outputTarget->emplaceData(
                SequenceIdentity(unitBoxcar, lastValue.getStart(), lastValue.getEnd()),
                std::move(root));
    }

    outputTarget->endData();
}

void RealtimeEngine::DispatchMetadata::serialize(QDataStream &stream) const
{
    stream << lastValue;
    SmoothingEngine::DispatchMetadata::serialize(stream);
}

void RealtimeEngine::DispatchMetadata::deserialize(QDataStream &stream)
{
    stream >> lastValue;
    SmoothingEngine::DispatchMetadata::deserialize(stream);
}


RealtimeEngine::DispatchData::DispatchData(RealtimeEngine *engine, const SequenceName &setUnit)
        : SmoothingEngine::DispatchData(engine, setUnit), haveHadValue(false), lastValue()
{ }

RealtimeEngine::DispatchData::~DispatchData() = default;

void RealtimeEngine::DispatchData::incomingData(SequenceValue &&value)
{
    Q_ASSERT(FP::defined(value.getStart()));
    if (!haveHadValue) {
        haveHadValue = true;
        handleSegments({ValueSegment(value.getStart(), FP::undefined(), std::move(value.root()))});
        return;
    } else if (FP::defined(lastValue.getStart()) && lastValue.getStart() != value.getStart()) {
        handleSegments({std::move(lastValue)});
    }
    lastValue = ValueSegment(value.getStart(), FP::undefined(), std::move(value.root()));
}

void RealtimeEngine::DispatchData::incomingAdvance(double time)
{
    Q_ASSERT(!FP::defined(lastValue.getStart()) || lastValue.getStart() <= time);
    if (FP::defined(lastValue.getStart())) {
        if (time == lastValue.getStart())
            return;
        handleSegments({std::move(lastValue)});
        lastValue = ValueSegment();
    }
    advanceAll(time);
}

void RealtimeEngine::DispatchData::endData()
{
    if (FP::defined(lastValue.getStart())) {
        handleSegments({std::move(lastValue)});
        lastValue = ValueSegment();
    }
    endAll();
}

void RealtimeEngine::DispatchData::serialize(QDataStream &stream) const
{
    stream << lastValue;
    SmoothingEngine::DispatchData::serialize(stream);
}

void RealtimeEngine::DispatchData::deserialize(QDataStream &stream)
{
    stream >> lastValue;
    SmoothingEngine::DispatchData::deserialize(stream);
}

}
}
