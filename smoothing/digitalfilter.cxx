/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QDebugStateSaver>

#include "smoothing/digitalfilter.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/digitialfilter.hxx
 * Digital filter implementations.
 */


enum DigitalFilterType {
    DF_NULL = 0, DF_SINGLEPOLELOWPASS, DF_GENERAL, DF_FOURPOLELOWPASS,
};

DigitalFilter::~DigitalFilter()
{ }

double DigitalFilter::spinupTime(double initial) const
{ return initial; }

DigitalFilterSinglePoleLowPass::DigitalFilterSinglePoleLowPass(Data::DynamicTimeInterval *tc,
                                                               Data::DynamicTimeInterval *setGap,
                                                               bool resetUndefined) : gap(setGap),
                                                                                      timeConstantInterval(
                                                                                              tc),
                                                                                      previousTC(
                                                                                              FP::undefined()),
                                                                                      a(FP::undefined()),
                                                                                      b(FP::undefined()),
                                                                                      previousValue(
                                                                                              FP::undefined()),
                                                                                      previousTime(
                                                                                              FP::undefined()),
                                                                                      previousInterval(
                                                                                              FP::undefined()),
                                                                                      resetOnUndefined(
                                                                                              resetUndefined)
{
    Q_ASSERT(timeConstantInterval != NULL);
}

DigitalFilterSinglePoleLowPass::DigitalFilterSinglePoleLowPass(QDataStream &stream) : gap(NULL),
                                                                                      timeConstantInterval(
                                                                                              NULL),
                                                                                      previousTC(
                                                                                              FP::undefined()),
                                                                                      a(FP::undefined()),
                                                                                      b(FP::undefined()),
                                                                                      previousValue(
                                                                                              FP::undefined()),
                                                                                      previousTime(
                                                                                              FP::undefined()),
                                                                                      previousInterval(
                                                                                              FP::undefined()),
                                                                                      resetOnUndefined(
                                                                                              true)
{
    stream >> gap >> timeConstantInterval >> previousValue >> previousTime >> resetOnUndefined;
}

DigitalFilterSinglePoleLowPass::DigitalFilterSinglePoleLowPass(int count,
                                                               Time::LogicalTimeUnit unit,
                                                               Data::DynamicTimeInterval *setGap,
                                                               bool resetUndefined) : gap(setGap),
                                                                                      timeConstantInterval(
                                                                                              new DynamicTimeInterval::Constant(
                                                                                                      unit,
                                                                                                      count,
                                                                                                      false)),
                                                                                      previousTC(
                                                                                              FP::undefined()),
                                                                                      a(FP::undefined()),
                                                                                      b(FP::undefined()),
                                                                                      previousValue(
                                                                                              FP::undefined()),
                                                                                      previousTime(
                                                                                              FP::undefined()),
                                                                                      previousInterval(
                                                                                              FP::undefined()),
                                                                                      resetOnUndefined(
                                                                                              resetUndefined)
{ }

DigitalFilterSinglePoleLowPass::DigitalFilterSinglePoleLowPass(const DigitalFilterSinglePoleLowPass &other)
        : DigitalFilter(),
          gap(other.gap == NULL ? NULL : other.gap->clone()),
          timeConstantInterval(
                  other.timeConstantInterval == NULL ? NULL : other.timeConstantInterval->clone()),
          previousTC(other.previousTC),
          a(other.a),
          b(other.b),
          previousValue(other.previousValue),
          previousTime(other.previousTime),
          previousInterval(other.previousInterval),
          resetOnUndefined(other.resetOnUndefined)
{ }

DigitalFilterSinglePoleLowPass &DigitalFilterSinglePoleLowPass::operator=(const DigitalFilterSinglePoleLowPass &other)
{
    if (&other == this) return *this;
    if (gap != NULL)
        delete gap;
    if (timeConstantInterval != NULL)
        delete timeConstantInterval;
    gap = (other.gap == NULL ? NULL : other.gap->clone());
    timeConstantInterval =
            (other.timeConstantInterval == NULL ? NULL : other.timeConstantInterval->clone());
    previousTC = other.previousTC;
    a = other.a;
    b = other.b;
    previousValue = other.previousValue;
    previousTime = other.previousTime;
    previousInterval = other.previousInterval;
    resetOnUndefined = other.resetOnUndefined;
    return *this;
}

DigitalFilter *DigitalFilterSinglePoleLowPass::clone() const
{ return new DigitalFilterSinglePoleLowPass(*this); }

DigitalFilterSinglePoleLowPass::~DigitalFilterSinglePoleLowPass()
{
    if (gap != NULL)
        delete gap;
    if (timeConstantInterval != NULL)
        delete timeConstantInterval;
}

void DigitalFilterSinglePoleLowPass::writeSerial(QDataStream &stream) const
{
    stream << (quint8) DF_SINGLEPOLELOWPASS;
    serialize(stream);
}

void DigitalFilterSinglePoleLowPass::serialize(QDataStream &stream) const
{
    stream << gap << timeConstantInterval << previousValue << previousTime << resetOnUndefined;
}

void DigitalFilterSinglePoleLowPass::reset()
{
    previousValue = FP::undefined();
}

bool DigitalFilterSinglePoleLowPass::smoothing() const
{
    return FP::defined(previousValue);
}

double DigitalFilterSinglePoleLowPass::spinupTime(double initial) const
{
    if (!FP::defined(initial))
        return initial;
    double tc = timeConstantInterval->applyConst(initial, initial) - initial;
    if (!FP::defined(tc) || tc <= 0.0)
        return initial;
    /* To 99.3% of final DC response */
    return initial - tc * 5;
}

double DigitalFilterSinglePoleLowPass::apply(double time, double value)
{
    if (!FP::defined(previousTC)) {
        previousTC = timeConstantInterval->apply(time, time) - time;
        if (!FP::defined(previousTC) || previousTC <= 0.0) {
            previousTC = 0.0;
        } else {
            previousInterval = FP::undefined();
        }
    } else {
        double checkTC = timeConstantInterval->apply(time, time) - time;
        if (!FP::defined(checkTC) || checkTC <= 0.0) {
            previousTC = 0.0;
        } else if (previousTC <= 0.0 || fabs(checkTC - previousTC) > 1E-4) {
            previousInterval = FP::undefined();
        }
    }

    if (!FP::defined(value)) {
        if (resetOnUndefined) {
            previousValue = value;
            previousTime = time;
        }
        return value;
    }

    if (!FP::defined(previousTime) || !FP::defined(previousValue) || previousTC <= 0.0) {
        previousValue = value;
        previousTime = time;
        return value;
    }

    if (gap != NULL) {
        double checkGap = gap->apply(time, previousTime);
        if (FP::defined(checkGap) && checkGap < time) {
            previousValue = value;
            previousTime = time;
            return value;
        }
    }

    Q_ASSERT(FP::defined(previousTime));
    Q_ASSERT(FP::defined(time));
    Q_ASSERT(FP::defined(value));
    Q_ASSERT(FP::defined(previousValue));

    double interval = time - previousTime;
    Q_ASSERT(interval > 0.0);
    if (!FP::defined(previousInterval) || fabs(interval - previousInterval) > 1E-4) {
        b = exp(-1.0 / (previousTC / interval));
        a = 1.0 - b;
        previousInterval = interval;
    }

    previousValue = a * value + b * previousValue;
    previousTime = time;
    return previousValue;
}

void DigitalFilterSinglePoleLowPass::printLog(QDebug &stream) const
{
    stream << "DigitalFilterSinglePoleLowPass(" << timeConstantInterval << ')';
}

Variant::Root DigitalFilterSinglePoleLowPass::describeState() const
{
    Variant::Root result;
    result["Type"].setString("SinglePoleLowPass");
    result["TimeConstant"].setDouble(previousTC);
    result["LatestInterval"].setDouble(previousInterval);
    result["LastestSmoothed"].setDouble(previousValue);
    result["LatestTime"].setDouble(previousTime);
    result["RestOnUndefined"].setBool(resetOnUndefined);
    return result;
}

QDataStream &operator<<(QDataStream &stream, const DigitalFilterSinglePoleLowPass &filter)
{
    filter.serialize(stream);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, DigitalFilterSinglePoleLowPass &filter)
{
    filter = DigitalFilterSinglePoleLowPass(stream);
    return stream;
}

QDebug operator<<(QDebug stream, const DigitalFilterSinglePoleLowPass &filter)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    filter.printLog(stream);
    return stream;
}

DigitalFilterFourPoleLowPass::DigitalFilterFourPoleLowPass(Data::DynamicTimeInterval *tc,
                                                           Data::DynamicTimeInterval *setGap,
                                                           bool resetUndefined)
        : DigitalFilterGeneral(QVector<double>(1), QVector<double>(4), setGap, resetUndefined),
          timeConstantInterval(tc),
          previousTC(FP::undefined())
{
    Q_ASSERT(tc != NULL);
}

DigitalFilterFourPoleLowPass::DigitalFilterFourPoleLowPass(QDataStream &stream)
        : DigitalFilterGeneral(stream), timeConstantInterval(NULL), previousTC(FP::undefined())
{
    stream >> timeConstantInterval;
}

DigitalFilterFourPoleLowPass::DigitalFilterFourPoleLowPass(int count,
                                                           Time::LogicalTimeUnit unit,
                                                           Data::DynamicTimeInterval *setGap,
                                                           bool resetUndefined)
        : DigitalFilterGeneral(QVector<double>(1), QVector<double>(4), setGap, resetUndefined),
          timeConstantInterval(new DynamicTimeInterval::Constant(unit, count, false)),
          previousTC(FP::undefined())
{ }

DigitalFilterFourPoleLowPass::~DigitalFilterFourPoleLowPass()
{
    if (timeConstantInterval != NULL)
        delete timeConstantInterval;
}

DigitalFilterFourPoleLowPass::DigitalFilterFourPoleLowPass(const DigitalFilterFourPoleLowPass &other)
        : DigitalFilter(),
          DigitalFilterGeneral(other),
          timeConstantInterval(
                  other.timeConstantInterval == NULL ? NULL : other.timeConstantInterval->clone()),
          previousTC(other.previousTC)
{ }

DigitalFilterFourPoleLowPass &DigitalFilterFourPoleLowPass::operator=(const DigitalFilterFourPoleLowPass &other)
{
    if (&other == this) return *this;
    DigitalFilterGeneral::operator=(other);
    if (timeConstantInterval != NULL)
        delete timeConstantInterval;
    timeConstantInterval =
            (other.timeConstantInterval == NULL ? NULL : other.timeConstantInterval->clone());
    previousTC = other.previousTC;
    return *this;
}

DigitalFilter *DigitalFilterFourPoleLowPass::clone() const
{ return new DigitalFilterFourPoleLowPass(*this); }

void DigitalFilterFourPoleLowPass::serialize(QDataStream &stream) const
{
    DigitalFilterGeneral::serialize(stream);
    stream << timeConstantInterval;
}

void DigitalFilterFourPoleLowPass::writeSerial(QDataStream &stream) const
{
    stream << (quint8) DF_FOURPOLELOWPASS;
    DigitalFilterFourPoleLowPass::serialize(stream);
}

double DigitalFilterFourPoleLowPass::spinupTime(double initial) const
{
    if (!FP::defined(initial))
        return initial;
    double tc = timeConstantInterval->applyConst(initial, initial) - initial;
    if (!FP::defined(tc) || tc <= 0.0)
        return initial;
    /* To 99.3% of final DC response */
    return initial - tc * 5;
}

double DigitalFilterFourPoleLowPass::apply(double time, double value)
{
    Q_ASSERT(a.size() == 1);
    Q_ASSERT(b.size() == 4);

    if (!FP::defined(time) || !FP::defined(getPreviousTime())) {
        a[0] = 1.0;
        b.fill(0);
        previousTC = FP::undefined();
        return DigitalFilterGeneral::apply(time, value);
    }

    double interval = time - getPreviousTime();
    if (interval <= 0.0) {
        a[0] = 1.0;
        b.fill(0);
        previousTC = FP::undefined();
        return DigitalFilterGeneral::apply(time, value);
    }

    if (!FP::defined(previousTC)) {
        previousTC = timeConstantInterval->apply(time, time) - time;
        if (!FP::defined(previousTC) || previousTC <= 0.0) {
            a[0] = 1.0;
            b.fill(0);
            previousTC = 0.0;
            return DigitalFilterGeneral::apply(time, value);
        }
    } else {
        double checkTC = timeConstantInterval->apply(time, time) - time;
        if (!FP::defined(checkTC) || checkTC <= 0.0) {
            a[0] = 1.0;
            b.fill(0);
            previousTC = 0.0;
            return DigitalFilterGeneral::apply(time, value);
        } else if (previousTC > 0.0 && fabs(checkTC - previousTC) <= 1E-4) {
            return DigitalFilterGeneral::apply(time, value);
        }
        previousTC = checkTC;
    }

    if (FP::defined(getPreviousInterval()) &&
            fabs(getPreviousInterval() - interval) / interval < 1E-6)
        return DigitalFilterGeneral::apply(time, value);

    double x = exp(-1.0 / (previousTC / interval));
    double t = 1.0 - x;
    t *= t;
    t *= t;
    a[0] = t;
    t = x;
    b[0] = 4.0 * t;
    t *= x;
    b[1] = -6.0 * t;
    t *= x;
    b[2] = 4.0 * t;
    t *= x;
    b[3] = -t;

    return DigitalFilterGeneral::apply(time, value);
}

void DigitalFilterFourPoleLowPass::printLog(QDebug &stream) const
{
    stream << "DigitalFilterFourPoleLowPass(" << timeConstantInterval << ")";
}

Variant::Root DigitalFilterFourPoleLowPass::describeState() const
{
    Variant::Root result = DigitalFilterGeneral::describeState();
    result["Type"].setString("FourPoleLowPass");
    result["TimeConstant"].setDouble(previousTC);
    return result;
}

QDataStream &operator<<(QDataStream &stream, const DigitalFilterFourPoleLowPass &filter)
{
    filter.serialize(stream);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, DigitalFilterFourPoleLowPass &filter)
{
    filter = DigitalFilterFourPoleLowPass(stream);
    return stream;
}

QDebug operator<<(QDebug stream, const DigitalFilterFourPoleLowPass &filter)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    filter.printLog(stream);
    return stream;
}


DigitalFilterGeneral::DigitalFilterGeneral(const QVector<double> &setA,
                                           const QVector<double> &setB,
                                           Data::DynamicTimeInterval *setGap,
                                           bool resetUndefined) : gap(setGap),
                                                                  previousInputs(),
                                                                  previousOutputs(),
                                                                  previousTime(FP::undefined()),
                                                                  previousInterval(FP::undefined()),
                                                                  resetOnUndefined(resetUndefined),
                                                                  a(setA),
                                                                  b(setB)
{
    if (a.size() > 1)
        previousInputs.reserve(a.size() - 1);
    if (b.size() > 0)
        previousOutputs.reserve(b.size());
}

DigitalFilterGeneral::DigitalFilterGeneral(QDataStream &stream) : gap(NULL),
                                                                  previousInputs(),
                                                                  previousOutputs(),
                                                                  previousTime(FP::undefined()),
                                                                  previousInterval(FP::undefined()),
                                                                  resetOnUndefined(true),
                                                                  a(),
                                                                  b()
{
    stream >> gap >> a >> b >> previousInputs >> previousOutputs >> previousInterval
           >> resetOnUndefined >> previousTime;
}

void DigitalFilterGeneral::serialize(QDataStream &stream) const
{
    stream << gap << a << b << previousInputs << previousOutputs << previousInterval
           << resetOnUndefined << previousTime;
}

DigitalFilterGeneral::DigitalFilterGeneral(const DigitalFilterGeneral &other)
        : DigitalFilter(),
          gap(other.gap == NULL ? NULL : other.gap->clone()),
          previousInputs(other.previousInputs),
          previousOutputs(other.previousOutputs),
          previousTime(other.previousTime),
          previousInterval(other.previousInterval),
          resetOnUndefined(other.resetOnUndefined),
          a(other.a),
          b(other.b)
{ }

DigitalFilterGeneral &DigitalFilterGeneral::operator=(const DigitalFilterGeneral &other)
{
    if (&other == this) return *this;
    if (gap != NULL)
        delete gap;
    gap = (other.gap == NULL ? NULL : other.gap->clone());
    a = other.a;
    b = other.b;
    previousInputs = other.previousInputs;
    previousOutputs = other.previousOutputs;
    previousTime = other.previousTime;
    previousInterval = other.previousInterval;
    resetOnUndefined = other.resetOnUndefined;
    return *this;
}

void DigitalFilterGeneral::writeSerial(QDataStream &stream) const
{
    stream << (quint8) DF_GENERAL;
    DigitalFilterGeneral::serialize(stream);
}

DigitalFilter *DigitalFilterGeneral::clone() const
{ return new DigitalFilterGeneral(*this); }

DigitalFilterGeneral::~DigitalFilterGeneral()
{
    if (gap != NULL)
        delete gap;
}

void DigitalFilterGeneral::printLog(QDebug &stream) const
{
    stream << "DigitalFilterGeneral(a=[" << a << "],b=[" << b << "])";
}

Variant::Root DigitalFilterGeneral::describeState() const
{
    Variant::Root result;
    result["Type"].setString("DigitalFilter");
    result["LatestInterval"].setDouble(previousInterval);
    result["LatestTime"].setDouble(previousTime);
    result["RestOnUndefined"].setBool(resetOnUndefined);
    for (auto add : a) {
        result["Coefficients/A"].toArray().after_back().setReal(add);
    }
    for (auto add : b) {
        result["Coefficients/B"].toArray().after_back().setReal(add);
    }
    for (auto add : previousInputs) {
        result["Values/Input"].toArray().after_back().setReal(add);
    }
    for (auto add : previousOutputs) {
        result["Values/Output"].toArray().after_back().setReal(add);
    }
    return result;
}

void DigitalFilterGeneral::reset()
{
    previousTime = FP::undefined();
    previousInterval = FP::undefined();
    previousInputs.clear();
    previousOutputs.clear();
}

bool DigitalFilterGeneral::smoothing() const
{
    return (previousInputs.size() >= a.size() - 1) && (previousOutputs.size() >= b.size());
}

double DigitalFilterGeneral::resetFinish(double time, double value)
{
    previousTime = time;
    previousInterval = FP::undefined();
    previousInputs.clear();
    previousOutputs.clear();
    if (a.size() > 0) {
        previousOutputs.fill(value, b.size());
        previousInputs.fill(value, a.size());
        return value;
    } else {
        return FP::undefined();
    }
}

double DigitalFilterGeneral::apply(double time, double value)
{
    if (!FP::defined(value)) {
        if (resetOnUndefined) {
            previousTime = time;
            previousInterval = FP::undefined();
            previousInputs.clear();
            previousOutputs.clear();
        }
        return value;
    }
    if (!FP::defined(previousTime) || !FP::defined(time))
        return resetFinish(time, value);
    if (gap != NULL) {
        double checkGap = gap->apply(time, previousTime);
        if (FP::defined(checkGap) && checkGap < time)
            return resetFinish(time, value);
    }

    double interval = time - previousTime;
    previousTime = time;
    if (interval <= 0.0)
        return resetFinish(time, value);

    if (a.isEmpty()) {
        previousTime = time;
        previousInterval = interval;
        previousInputs.clear();
        previousOutputs.clear();
        return FP::undefined();
    }

    if (FP::defined(previousInterval) && (fabs(previousInterval - interval)) / interval > 1E-6) {
        previousInterval = interval;
        return resetFinish(time, value);
    }
    previousInterval = interval;

    if ((a.size() > 1 && previousInputs.size() < a.size() - 1) ||
            (b.size() > 0 && previousOutputs.size() < b.size())) {
        return resetFinish(time, value);
    }

    if (previousInputs.size() > a.size() - 1)
        previousInputs.remove(0, previousInputs.size() - a.size() + 1);
    if (previousOutputs.size() > b.size())
        previousOutputs.remove(0, previousOutputs.size() - b.size());

    double output = value * a.at(0);
    for (QVector<double>::const_iterator i = a.constBegin() + 1, v = previousInputs.constBegin(),
            end = a.constEnd(); i != end; ++i, ++v) {
        output += (*i) * (*v);
    }

    for (QVector<double>::const_iterator i = b.constBegin(), v = previousOutputs.constBegin(),
            end = b.constEnd(); i != end; ++i, ++v) {
        output += (*i) * (*v);
    }

    if (a.size() > 1) {
        previousInputs.pop_back();
        previousInputs.push_front(value);
    }
    if (b.size() > 0) {
        previousOutputs.pop_back();
        previousOutputs.push_front(output);
    }
    return output;
}


QDataStream &operator<<(QDataStream &stream, const DigitalFilterGeneral &filter)
{
    filter.serialize(stream);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, DigitalFilterGeneral &filter)
{
    filter = DigitalFilterGeneral(stream);
    return stream;
}

QDebug operator<<(QDebug stream, const DigitalFilterGeneral &filter)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    filter.printLog(stream);
    return stream;
}


QDataStream &operator<<(QDataStream &stream, const DigitalFilter *filter)
{
    if (filter == NULL) {
        quint8 t = (quint8) DF_NULL;
        stream << t;
        return stream;
    }
    filter->writeSerial(stream);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, DigitalFilter *&filter)
{
    quint8 tRaw;
    stream >> tRaw;
    switch ((DigitalFilterType) tRaw) {
    case DF_NULL:
        filter = NULL;
        return stream;
    case DF_SINGLEPOLELOWPASS:
        filter = new DigitalFilterSinglePoleLowPass(stream);
        return stream;
    case DF_GENERAL:
        filter = new DigitalFilterGeneral(stream);
        return stream;
    case DF_FOURPOLELOWPASS:
        filter = new DigitalFilterFourPoleLowPass(stream);
        return stream;
    }
    Q_ASSERT(false);
    filter = NULL;
    return stream;
}

QDebug operator<<(QDebug stream, const DigitalFilter *filter)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << static_cast<const void *>(filter) << ':';
    if (filter == NULL) {
        stream << "DigitalFilter(NULL)";
    } else {
        filter->printLog(stream);
    }
    return stream;
}

}
}
