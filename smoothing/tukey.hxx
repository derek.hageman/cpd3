/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGTUKEY_H
#define CPD3SMOOTHINGTUKEY_H

#include "core/first.hxx"

#include <math.h>
#include <algorithm>
#include <QtGlobal>
#include <QObject>
#include <QDataStream>

#include "smoothing/smoothing.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Smoothing {

/**
 * Implementations for Tukey's smoothers.
 */
class CPD3SMOOTHING_EXPORT Tukey {
    static double median3Point(double vm, double v, double vp);

public:

    /**
     * Calculate the running 3-median of a set of data.  All points must
     * be defined.
     * 
     * @param inputBegin    the input start iterator for the data
     * @param inputEnd      the input end iterator for the data
     * @param output        the output start iterator for the data, must have the same size as the input
     */
    template<typename InputIterator, typename OutputIterator>
    static void median3(InputIterator inputBegin, InputIterator inputEnd, OutputIterator output)
    {
        if (inputBegin == inputEnd)
            return;
        InputIterator inputPrior = inputBegin;
        ++inputBegin;
        *output = *inputPrior;
        output++;
        if (inputBegin == inputEnd)
            return;
        for (InputIterator inputNext = inputBegin + 1;
                inputNext != inputEnd;
                inputPrior++, ++inputBegin, ++inputNext, ++output) {
            *output = median3Point(*inputPrior, *inputBegin, *inputNext);
        }
        *output = *inputBegin;
    }

    /**
     * Calculate the running 3-median of a set of data and repeat until 
     * convergence.  All points must be defined.  This will overwrite the
     * input data.
     * 
     * @param inputBegin    the input start iterator for the data
     * @param inputEnd      the input end iterator for the data
     * @param outputBegin   the output start iterator for the data, must have the same size as the input
     * @param outputEnd     the output end iterator
     * @param copyOut       if set then the result is always copied to the output
     * @param maximumAverageError the maximum average error to accept
     * @param maximumIterations the absolute maximum number of iterations
     * @return              true if the result was placed in the output (otherwise it is still in the input)
     */
    template<typename InputIterator, typename OutputIterator>
    static bool median3R(InputIterator inputBegin,
                         InputIterator inputEnd,
                         OutputIterator outputBegin,
                         OutputIterator outputEnd,
                         bool copyOut = true,
                         double maximumAverageError = 1E-10,
                         int maximumIterations = 20)
    {
        if (inputBegin == inputEnd)
            return true;
        Q_ASSERT(outputBegin != outputEnd);

        int itter = 0;
        double err = 0;
        size_t div = (inputEnd - inputBegin);
        Q_ASSERT(div != 0);
        bool sense = false;
        InputIterator ia;
        OutputIterator ib;
        do {
            if (sense) {
                median3(outputBegin, outputEnd, inputBegin);
                sense = false;
            } else {
                median3(inputBegin, inputEnd, outputBegin);
                sense = true;
            }

            for (err = 0.0, ia = inputBegin, ib = outputBegin; ia != inputEnd; ++ia, ++ib, ++div) {
                err += fabs((*ia) - (*ib));
            }
            err /= (double) div;
        } while (itter < maximumIterations && err > maximumAverageError);
        if (sense)
            return true;
        if (!copyOut)
            return false;
        std::copy(inputBegin, inputEnd, outputBegin);
        return true;
    }

    /**
     * Apply Tukey's splitting for same valued runs.  This is normally followed
     * by an implicit 3R step.  All points must be  defined. 
     * 
     * @param inputBegin    the input start iterator for the data
     * @param inputEnd      the input end iterator for the data
     * @param output        the output start iterator for the data, must have the same size as the input
     */
    template<typename InputIterator, typename OutputIterator>
    static void split(InputIterator inputBegin, InputIterator inputEnd, OutputIterator output)
    {
        if (inputBegin == inputEnd)
            return;
        InputIterator inputPrior1 = inputBegin;
        *output = *inputBegin;
        ++inputBegin;
        if (inputBegin == inputEnd)
            return;
        ++output;

        InputIterator inputNext1 = inputBegin + 1;
        if (inputNext1 == inputEnd) {
            *output = *inputBegin;
            return;
        }
        InputIterator inputNext2 = inputNext1 + 1;
        if (inputNext2 == inputEnd) {
            *output = *inputBegin;
            output++;
            inputBegin++;
            *output = *inputBegin;
            return;
        }

        /* Initial run handling */
        if (*inputBegin == *inputPrior1 && *inputNext1 != *inputPrior1) {
            *output = median3Point(*inputBegin, *inputNext1,
                                   3.0 * (*inputNext1) - 2.0 * (*inputNext2));
        } else {
            *output = *inputBegin;
        }

        InputIterator inputPrior2 = inputPrior1;
        inputPrior1 = inputBegin;
        inputBegin = inputNext1;
        inputNext1 = inputNext2;
        ++inputNext2;
        ++output;

        if (inputNext2 == inputEnd) {
            *output = *inputBegin;
            output++;
            inputBegin++;
            *output = *inputBegin;
            return;
        }

        /* Initial run handling */
        if (*inputBegin == *inputPrior1 && *inputNext1 != *inputPrior1) {
            *output = median3Point(*inputBegin, *inputNext1,
                                   3.0 * (*inputNext1) - 2.0 * (*inputNext2));
        } else {
            *output = *inputBegin;
        }

        InputIterator inputPrior3 = inputPrior2;
        inputPrior2 = inputPrior1;
        inputPrior1 = inputBegin;
        inputBegin = inputNext1;
        inputNext1 = inputNext2;
        ++inputNext2;
        OutputIterator outputPrior = output;
        ++output;

        for (; inputNext2 != inputEnd;
                inputPrior3 = inputPrior2, inputPrior2 = inputPrior1, inputPrior1 = inputBegin,
                inputBegin = inputNext1, inputNext1 = inputNext2, ++inputNext2, outputPrior =
                                                                                        output, ++output) {
            /* Not a two point run */
            if (*inputBegin != *inputPrior1) {
                *output = *inputBegin;
                continue;
            }

            /* Extrapolate v0 from the points after it, but only if this
               isn't a >=3 point run (in which case this would do nothing) */
            if (*inputBegin != *inputNext1) {
                *output = median3Point(*inputBegin, *inputNext1,
                                       3.0 * (*inputNext1) - 2.0 * (*inputNext2));
            } else {
                *output = *inputBegin;
            }

            /* Extrapolate backwards for v-1, but again only if this wasn't
               the middle of a >= 3 run */
            if (*inputPrior1 != *inputPrior2) {
                *outputPrior = median3Point(*inputPrior1, *inputPrior2,
                                            3.0 * (*inputPrior2) - 2.0 * (*inputPrior3));
            }
        }

        Q_ASSERT(inputBegin != inputEnd);
        *output = *inputBegin;

        /* Handle the last extrapolates, since the loop stops two before
         * the end. */
        if (*inputBegin == *inputPrior1 && *inputPrior1 != *inputPrior2) {
            *outputPrior = median3Point(*inputPrior1, *inputPrior2,
                                        3.0 * (*inputPrior2) - 2.0 * (*inputPrior3));
        }

        inputPrior3 = inputPrior2;
        inputPrior2 = inputPrior1;
        inputPrior1 = inputBegin;
        inputBegin = inputNext1;
        outputPrior = output;
        ++output;
        Q_ASSERT(inputBegin != inputEnd);
        *output = *inputBegin;

        if (*inputBegin == *inputPrior1 && *inputPrior1 != *inputPrior2) {
            *outputPrior = median3Point(*inputPrior1, *inputPrior2,
                                        3.0 * (*inputPrior2) - 2.0 * (*inputPrior3));
        }
        Q_ASSERT(inputBegin + 1 == inputEnd);
    }


    /**
     * Calculate the Hanning (weighted 3-mean) smoother of a set of points.  
     * All points must be defined.
     * 
     * @param inputBegin    the input start iterator for the data
     * @param inputEnd      the input end iterator for the data
     * @param output        the output start iterator for the data, must have the same size as the input
     */
    template<typename InputIterator, typename OutputIterator>
    static void hanning(InputIterator inputBegin, InputIterator inputEnd, OutputIterator output)
    {
        if (inputBegin == inputEnd)
            return;
        InputIterator inputPrior = inputBegin;
        ++inputBegin;
        *output = *inputPrior;
        output++;
        if (inputBegin == inputEnd)
            return;
        for (InputIterator inputNext = inputBegin + 1;
                inputNext != inputEnd;
                inputPrior++, ++inputBegin, ++inputNext, ++output) {
            *output = 0.25 * (*inputPrior) + 0.5 * (*inputBegin) + 0.25 * (*inputNext);
        }
        *output = *inputBegin;
    }
};

}
}

#endif
