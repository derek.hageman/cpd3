/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "smoothing/fourierchain.hxx"
#include "algorithms/fourier.hxx"
#include "core/environment.hxx"

using namespace CPD3::Data;
using namespace CPD3::Algorithms;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/fourierchain.hxx
 * Smoothing chain implementations that wrap various transforms of data
 * in Fourier frequency space.
 */

FourierChainNode::FourierChainNode(Data::DynamicTimeInterval *setLow,
                                   Data::DynamicTimeInterval *setHigh,
                                   Data::DynamicTimeInterval *setGap,
                                   bool smoothOverUndefined,
                                   SmootherChainTarget *outputTarget) : BlockSmootherChain(setGap,
                                                                                           outputTarget),
                                                                        low(setLow),
                                                                        high(setHigh)
{
    if (smoothOverUndefined) {
        setUndefinedMode(BlockSmootherChain::Remove);
    } else {
        setUndefinedMode(BlockSmootherChain::DumpBuffer);
    }
}

FourierChainNode::FourierChainNode(QDataStream &stream, SmootherChainTarget *outputTarget)
        : BlockSmootherChain(stream, outputTarget)
{
    stream >> low >> high;
}

FourierChainNode::~FourierChainNode()
{
    if (low != NULL)
        delete low;
    if (high != NULL)
        delete high;
}

void FourierChainNode::serialize(QDataStream &stream) const
{
    BlockSmootherChain::serialize(stream);
    stream << low << high;
}

static void applyFFT(BlockSmootherChain::Value *inputBegin,
                     BlockSmootherChain::Value *inputEnd,
                     BlockSmootherChain::Value *outputBegin,
                     BlockSmootherChain::Value *outputEnd,
                     double low,
                     double high, QVector<std::complex<double> > &working)
{
    working.clear();
    working.reserve((int) (inputEnd - inputBegin));

    int countIntervals = 0;
    double sumIntervals = 0.0;
    for (BlockSmootherChain::Value *add = inputBegin; add != inputEnd; ++add) {
        Q_ASSERT(FP::defined(add->value));
        working.append(std::complex<double>(add->value, 0.0));
        if (FP::defined(add->start) && FP::defined(add->end)) {
            sumIntervals += add->end - add->start;
            countIntervals++;
        }
    }
    if (countIntervals <= 0 || sumIntervals <= 0.0)
        return;

    double sampleFrequency = 1.0 / (sumIntervals / (double) countIntervals);
    double frequencyResolution = sampleFrequency / (double) working.size();
    if (FP::defined(low))
        low = 1.0 / (frequencyResolution * low);
    if (FP::defined(high))
        high = 1.0 / (frequencyResolution * high);

    int lowerCutoff = -1;
    int upperCutoff = -1;
    bool removeBetween = false;
    if (FP::defined(low) && low > 0.0) {
        if (FP::defined(high) && high > 0.0) {
            /* A low pass cutoff less than the high pass cutoff means
             * we interpret it to be a band stop filter, so we round the
             * low and and the high up.  Otherwise it's a band pass filter,
             * so we expand both ends. */
            if (low <= high) {
                lowerCutoff = (int) floor(low);
                upperCutoff = (int) ceil(high);
                removeBetween = true;
            } else {
                lowerCutoff = (int) ceil(low);
                upperCutoff = (int) floor(high);
            }
        } else {
            lowerCutoff = (int) floor(low);
        }
    } else if (FP::defined(high) && high > 0.0) {
        upperCutoff = (int) ceil(high);
    } else {
        return;
    }

    Fourier::forward(working);

    int mid = working.size() / 2;
    if (removeBetween) {
        Q_ASSERT(lowerCutoff != -1 && upperCutoff != -1);
        lowerCutoff = qBound(0, lowerCutoff, mid - 1);
        upperCutoff = qBound(0, upperCutoff, mid - 1);
        for (int rm = lowerCutoff, end = working.size() - 1; rm <= upperCutoff; rm++) {
            working[1 + rm] = std::complex<double>(0, 0);
            working[end - rm] = std::complex<double>(0, 0);
        }
    } else {
        if (lowerCutoff != -1) {
            lowerCutoff = qBound(0, lowerCutoff, mid - 1);
            working[mid] = std::complex<double>(0, 0);
            for (int rm = lowerCutoff, end = working.size() - 1; rm < mid; rm++) {
                working[1 + rm] = std::complex<double>(0, 0);
                working[end - rm] = std::complex<double>(0, 0);
            }
        }
        if (upperCutoff != -1) {
            upperCutoff = qBound(0, upperCutoff, mid - 1);
            working[0] = std::complex<double>(0, 0);
            for (int rm = upperCutoff, end = working.size() - 1; rm >= 0; rm--) {
                working[1 + rm] = std::complex<double>(0, 0);
                working[end - rm] = std::complex<double>(0, 0);
            }
        }
    }

    Fourier::inverse(working);

    Q_ASSERT(outputBegin >= inputBegin);
    QVector<std::complex<double> >::const_iterator
            unpackOrigin = working.constBegin() + (outputBegin - inputBegin);
    for (; outputBegin != outputEnd; ++outputBegin, ++unpackOrigin) {
        /* This should still be purely std::real because we've preserved the
         * symmetry */
        outputBegin->value = std::real(*unpackOrigin);
    }
}

FourierChainNode::HandlerFourier::HandlerFourier(double setLow, double setHigh) : low(setLow),
                                                                                  high(setHigh)
{ }

FourierChainNode::HandlerFourier::~HandlerFourier()
{ }

bool FourierChainNode::HandlerFourier::process(BlockSmootherChain::Value *&buffer, size_t &size)
{
    if (size < 4)
        return false;

    BlockSmootherChain::Value *bufferEnd = buffer + size;

    QVector<std::complex<double> > working;
    /* For small buffers do the whole thing */
    if (size <= 1 << 16) {
        applyFFT(buffer, bufferEnd, buffer, bufferEnd, low, high, working);
        return false;
    }

    /* For powers of two, also do the whole thing */
    int rounded = 1 << (int) (31 - INTEGER::clz32((quint32) size));
    if (rounded == (int) size) {
        applyFFT(buffer, bufferEnd, buffer, bufferEnd, low, high, working);
        return false;
    }
    Q_ASSERT(rounded < (int) size);

    /* For everything else, we divide it into two halves that are powers of
     * two (from the beginning and from the end) then use those halves to
     * fill out the result.  This means that we should always be fast
     * even if we end up with a prime number of input points.  It does mean
     * that we may be slower in the general case, however. */

    BlockSmootherChain::Value *mid = buffer + size / 2;

    Q_ASSERT(rounded <= (int) size);
    Q_ASSERT(mid > bufferEnd - rounded);
    applyFFT(buffer, buffer + rounded, buffer, mid, low, high, working);
    applyFFT(bufferEnd - rounded, bufferEnd, mid, bufferEnd, low, high, working);

    return false;
}

BlockSmootherChain::Handler *FourierChainNode::createHandler(double start, double end)
{
    Q_UNUSED(start);

    double sl = FP::undefined();
    if (low != NULL) {
        if (FP::defined(start)) {
            sl = low->apply(start, start, true);
            if (FP::defined(sl))
                sl -= start;
        } else if (FP::defined(end)) {
            sl = low->apply(end, end, false, -1);
            if (FP::defined(sl))
                sl = end - sl;
        }
    }

    double sh = FP::undefined();
    if (high != NULL) {
        if (FP::defined(start)) {
            sh = high->apply(start, start, true);
            if (FP::defined(sh))
                sh -= start;
        } else if (FP::defined(end)) {
            sh = high->apply(end, end, false, -1);
            if (FP::defined(sh))
                sh = end - sh;
        }
    }

    return new HandlerFourier(sl, sh);
}

SmootherChainCoreFourier::SmootherChainCoreFourier(Data::DynamicTimeInterval *setLow,
                                                   Data::DynamicTimeInterval *setHigh,
                                                   Data::DynamicTimeInterval *setGap,
                                                   bool setSmoothUndefined) : low(setLow),
                                                                              high(setHigh),
                                                                              gap(setGap),
                                                                              smoothOverUndefined(
                                                                                      setSmoothUndefined)
{ }

SmootherChainCoreFourier::~SmootherChainCoreFourier()
{
    if (low != NULL)
        delete low;
    if (high != NULL)
        delete high;
    if (gap != NULL)
        delete gap;
}

void SmootherChainCoreFourier::createSmoother(SmootherChainCoreEngineInterface *engine,
                                              SmootherChainTarget *&input,
                                              SmootherChainTarget *output)
{
    FourierChainNode *node = new FourierChainNode(low == NULL ? NULL : low->clone(),
                                                  high == NULL ? NULL : high->clone(),
                                                  gap == NULL ? NULL : gap->clone(),
                                                  smoothOverUndefined, output);
    engine->addChainNode(node);
    input = node->getTarget();
}

void SmootherChainCoreFourier::deserializeSmoother(QDataStream &stream,
                                                   SmootherChainCoreEngineInterface *engine,
                                                   SmootherChainTarget *&input,
                                                   SmootherChainTarget *output)
{
    FourierChainNode *node = new FourierChainNode(stream, output);
    engine->addChainNode(node);
    input = node->getTarget();
}

Variant::Root SmootherChainCoreFourier::getProcessingMetadata(double time) const
{
    Variant::Root meta;
    meta.write().hash("Type").setString("Fourier");
    if (low != NULL) {
        meta.write().hash("Parameters").hash("Low").setString(low->describe(time));
    }
    if (high != NULL) {
        meta.write().hash("Parameters").hash("High").setString(high->describe(time));
    }
    if (gap != NULL) {
        meta.write().hash("Parameters").hash("Gap").setString(gap->describe(time));
    }
    meta.write().hash("Parameters").hash("SmoothOverUndefined").setBool(smoothOverUndefined);
    return meta;
}

QSet<double> SmootherChainCoreFourier::getProcessingMetadataBreaks() const
{
    QSet<double> result;
    if (low != NULL)
        result |= low->getChangedPoints();
    if (high != NULL)
        result |= high->getChangedPoints();
    if (gap != NULL)
        result |= gap->getChangedPoints();
    return result;
}

}
}
