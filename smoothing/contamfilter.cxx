/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTimer>
#include <QLoggingCategory>

#include "smoothing/contamfilter.hxx"
#include "core/environment.hxx"
#include "core/threadpool.hxx"
#include "smoothing/smoothingengine.hxx"
#include "datacore/variant/composite.hxx"


Q_LOGGING_CATEGORY(log_smoothing_contamfilter, "cpd3.smoothing.contaminationfilter", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/contamfilter.hxx
 * A component that filters out contaminated data from a data stream.
 */

template<typename T>
static QDataStream &operator<<(QDataStream &stream, const std::vector<T> &value)
{
    quint32 n = (quint32) value.size();
    stream << n;
    for (quint32 i = 0; i < n; i++) {
        stream << value[i];
    }
    return stream;
}

template<typename T>
static QDataStream &operator>>(QDataStream &stream, std::vector<T> &value)
{
    quint32 n;
    stream >> n;
    value.clear();
    value.reserve((size_t) n);
    for (size_t i = 0; i < (size_t) n; i++) {
        T v;
        stream >> v;
        value.push_back(v);
    }
    return stream;
}

ContaminationFilter::ContaminationFilter(DynamicSequenceSelection *affected) :
/* @formatter:off */
#ifndef NDEBUG
        lastIncomingTime(FP::undefined()),
#endif
/* @formatter:on */
        dataEnded(false),
        state(NotStarted),
        affectedVariables(affected),
        bypassIngress(nullptr)
{
    bypassIngress = createOutputIngress();
}

ContaminationFilter::ContaminationFilter(const QString &id) :
/* @formatter:off */
#ifndef NDEBUG
        lastIncomingTime(FP::undefined()),
#endif
/* @formatter:on */
        dataEnded(false),
        state(NotStarted),
        affectedVariables(nullptr),
        systemContamID(id),
        bypassIngress(nullptr)
{
    bypassIngress = createOutputIngress();
}

void ContaminationFilter::deserialize(QDataStream &stream)
{
    Q_ASSERT(state == NotStarted);
    Q_ASSERT(dispatch.empty());
    Q_ASSERT(advanceDispatch.empty());

    affectedVariables.reset();

    Data::DynamicSequenceSelection *av;

    stream >> pending >> incoming >> dataEnded >> av >> systemContamID >> loadedSystemStations
           >> outputMux;

    affectedVariables.reset(av);

    quint32 n;
    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        SequenceName u;
        stream >> u;
        getDispatch(u)->deserialize(stream);
    }
}

void ContaminationFilter::serialize(QDataStream &stream)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            if (state == Terminated || state == NotStarted || state == Paused || state == Completed)
                break;
            state = PauseRequested;
            request.notify_all();
            response.wait(lock);
        }
    }

    stream << pending << incoming << dataEnded << affectedVariables.get() << systemContamID
           << loadedSystemStations << outputMux;

    stream << static_cast<quint32>(dispatch.size());
    for (const auto &d : dispatch) {
        stream << d.first;
        d.second->serialize(stream);
    }

    std::lock_guard<std::mutex> lock(mutex);
    if (state == Paused)
        state = Running;
    request.notify_all();
}

ContaminationFilter::~ContaminationFilter()
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        if (state == Running)
            state = Terminated;
    }
    request.notify_all();
    if (thread.joinable())
        thread.join();

    outputMux.sinkCreationComplete();

    for (auto del : advanceDispatch) {
        del->endData();
    }
    dispatch.clear();
    if (bypassIngress)
        bypassIngress->endData();

    outputMux.signalTerminate(true);
    outputMux.wait();
}

SequenceName::Set ContaminationFilter::requestedInputs()
{
    if (!affectedVariables)
        return {};
    SequenceName::Set requested;
    for (auto flagsUnit : affectedVariables->getAllUnits()) {
        flagsUnit.setVariable("F1_" + Util::suffix(flagsUnit.getVariable(), '_'));
        requested.insert(flagsUnit);
    }
    return requested;
}

void ContaminationFilter::setEgress(StreamSink *egress)
{
    outputMux.setEgress(egress);
}


bool ContaminationFilter::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, mutex, response,
                                     [this] { return state == Completed; });
}

void ContaminationFilter::start()
{
    outputMux.start();

    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(state == NotStarted);
        state = Running;
    }
    thread = std::thread(std::bind(&ContaminationFilter::run, this));
}

bool ContaminationFilter::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == Completed;
}

void ContaminationFilter::signalTerminate()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (state == Finalizing)
        state = FinalizeTerminated;
    else if (state != Completed)
        state = Terminated;
    else
        return;
    request.notify_all();
    response.notify_all();
}

void ContaminationFilter::incomingData(const Data::SequenceValue::Transfer &values)
{
    if (values.empty())
        return;

    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!dataEnded);
#ifndef NDEBUG
        Q_ASSERT(Range::compareStart(values.front().getStart(), lastIncomingTime) >= 0);
        lastIncomingTime = values.back().getStart();
        for (auto check = values.cbegin() + 1, endCheck = values.cend();
                check != endCheck;
                ++check) {
            Q_ASSERT(Range::compareStart(check->getStart(), (check - 1)->getStart()) >= 0);
        }
#endif

        Util::append(values, pending);
    }
    request.notify_all();
}

void ContaminationFilter::incomingData(Data::SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;

    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!dataEnded);
#ifndef NDEBUG
        Q_ASSERT(Range::compareStart(values.front().getStart(), lastIncomingTime) >= 0);
        lastIncomingTime = values.back().getStart();
        for (auto check = values.cbegin() + 1, endCheck = values.cend();
                check != endCheck;
                ++check) {
            Q_ASSERT(Range::compareStart(check->getStart(), (check - 1)->getStart()) >= 0);
        }
#endif

        Util::append(std::move(values), pending);
    }
    request.notify_all();
}

void ContaminationFilter::incomingData(const Data::SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!dataEnded);
#ifndef NDEBUG
        Q_ASSERT(Range::compareStart(value.getStart(), lastIncomingTime) >= 0);
        lastIncomingTime = value.getStart();
#endif

        pending.emplace_back(value);
    }
    request.notify_all();
}

void ContaminationFilter::incomingData(Data::SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!dataEnded);
#ifndef NDEBUG
        Q_ASSERT(Range::compareStart(value.getStart(), lastIncomingTime) >= 0);
        lastIncomingTime = value.getStart();
#endif

        pending.emplace_back(std::move(value));
    }
    request.notify_all();
}

void ContaminationFilter::endData()
{
    std::lock_guard<std::mutex> lock(mutex);
    dataEnded = true;
    if (state == Completed)
        return;
    request.notify_all();
}

void ContaminationFilter::stall(std::unique_lock<std::mutex> &lock)
{
    while (pending.size() > stallThreshold) {
        switch (state) {
        case NotStarted:
        case Running:
        case PauseRequested:
        case Paused:
            break;
        case Terminated:
        case Finalizing:
        case FinalizeTerminated:
        case Completed:
            return;
        }
        response.wait(lock);
    }
}

void ContaminationFilter::loadSystemContam(const SequenceName::Component &station)
{
    if (systemContamID.isEmpty())
        return;
    if (loadedSystemStations.count(station))
        return;
    loadedSystemStations.insert(station);
    DynamicSequenceSelection *add = systemContaminationAffected(station, systemContamID);
    add->forceUnit(station);
    if (loadedSystemStations.size() == 1) {
        Q_ASSERT(!affectedVariables);
        affectedVariables.reset(add);
    } else {
        Q_ASSERT(affectedVariables);
        if (loadedSystemStations.size() == 2) {
            affectedVariables.reset(
                    new DynamicSequenceSelection::Composite(std::move(affectedVariables)));
        }
        static_cast<DynamicSequenceSelection::Composite *>(affectedVariables.get())->add(add);
    }
    qCDebug(log_smoothing_contamfilter) << "Loaded system contamination" << systemContamID
                                        << "for station" << station;
}

void ContaminationFilter::registerExpected(const SequenceName::Component &station,
                                           const SequenceName::Component &archive)
{
    loadSystemContam(station);
    if (!affectedVariables)
        return;
    affectedVariables->registerExpected(station, archive);
}

std::unique_ptr<
        ContaminationFilter::Dispatch> ContaminationFilter::createDispatch(const SequenceName &unit)
{
    if (unit.isDefaultStation())
        return std::unique_ptr<Dispatch>(new DispatchUnderlay(getBypassIngress()));

    loadSystemContam(unit.getStation());

    std::unique_ptr<Dispatch> d(overrideDispatch(unit));
    if (d)
        return std::move(d);

    d.reset(createGeneralDispatch(unit));

    DispatchUnderlay *underlay = static_cast<DispatchUnderlay *>(
            getDispatch(unit.toDefaultStation()));
    underlay->forwardTargets.push_back(d.get());
    auto segmenter = d->getSegmenter();
    if (segmenter)
        segmenter->assign(underlay->values);
    return std::move(d);
}

ContaminationFilter::Dispatch *ContaminationFilter::overrideDispatch(const SequenceName &unit)
{
    if (unit.hasFlavor(SequenceName::flavor_cover) || unit.hasFlavor(SequenceName::flavor_stats))
        return new DispatchBypass(getBypassIngress());

    if (Util::starts_with(unit.getVariable(), "F1_"))
        return nullptr;
    if (unit.isMeta()) {
        if (!affectedVariables->registerInput(unit.fromMeta())) {
            return new DispatchBypass(getBypassIngress());
        }
    } else {
        if (!affectedVariables->registerInput(unit)) {
            return new DispatchBypass(getBypassIngress());
        }
    }
    return nullptr;
}

ContaminationFilter::Dispatch *ContaminationFilter::createGeneralDispatch(const SequenceName &unit)
{
    if (unit.isMeta()) {
        if (Util::starts_with(unit.getVariable(), "F1_"))
            return new DispatchFlagsMeta(getBypassIngress());
        return new DispatchValueMeta(createOutputIngress(), affectedVariables->clone(), unit);
    }
    if (Util::starts_with(unit.getVariable(), "F1_")) {
        return new DispatchFlags(getBypassIngress(),
                                 static_cast<DispatchFlagsMeta *>(getDispatch(unit.toMeta())));
    }

    SequenceName flagsUnit = unit;
    flagsUnit.setVariable("F1_" + Util::suffix(flagsUnit.getVariable(), '_'));
    flagsUnit.removeFlavor(SequenceName::flavor_end);
    return new DispatchValue(createOutputIngress(), affectedVariables->clone(),
                             static_cast<DispatchFlags *>(getDispatch(flagsUnit)),
                             static_cast<DispatchValueMeta *>(getDispatch(
                                     unit.toMeta().withoutFlavor("end"))), unit);
}

bool ContaminationFilter::advanceSortCompare(Dispatch *a, Dispatch *b)
{ return a->advancePriority() < b->advancePriority(); }

ContaminationFilter::Dispatch *ContaminationFilter::getDispatch(const Data::SequenceName &unit)
{
    auto target = dispatch.find(unit);
    if (target == dispatch.end()) {
        target = dispatch.emplace(unit, createDispatch(unit)).first;
        auto pos = std::upper_bound(advanceDispatch.begin(), advanceDispatch.end(),
                                    target->second.get(), ContaminationFilter::advanceSortCompare);
        advanceDispatch.insert(pos, target->second.get());
    }

    return target->second.get();
}

void ContaminationFilter::endDispatch()
{
    outputMux.sinkCreationComplete();
    for (auto target : advanceDispatch) {
        target->endData();
    }
    advanceDispatch.clear();
    dispatch.clear();
    bypassIngress->endData();
    bypassIngress = nullptr;
}

void ContaminationFilter::run()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        bool finishData = false;
        bool wasStalled = false;

        if (!lock)
            lock.lock();
        switch (state) {
        case NotStarted:
        case Completed:
            return;
        case Paused:
            request.wait(lock);
            continue;

        case PauseRequested:
            state = Paused;
            response.notify_all();
            continue;

        case Terminated:
            lock.unlock();

            endDispatch();
            outputMux.signalTerminate(true);
            outputMux.wait();
            finishTerminate();

            lock.lock();
            state = Completed;
            lock.unlock();
            response.notify_all();
            finished();
            return;

        case FinalizeTerminated:
            lock.unlock();

            /* Same as above but the dispatch is already ended */
            outputMux.signalTerminate(true);
            outputMux.wait();
            finishTerminate();

            lock.lock();
            state = Completed;
            lock.unlock();
            response.notify_all();
            finished();
            return;

        case Finalizing:
            lock.unlock();

            /* Have to do this blocking, since we have no way of re-waking the thread
             * and the end signals may come in during the worker exit sequence (so it
             * wouldn't get re-awoken. */
            if (!outputMux.wait(0.25) || !childThreadWait(250)) {
                continue;
            }
            outputMux.wait();
            completeFinish();

            lock.lock();
            if (state != Finalizing)
                continue;

            dataEnded = true;
            state = Completed;
            lock.unlock();
            response.notify_all();
            finished();
            return;

        case Running:
            if (!pending.empty()) {
                wasStalled = pending.size() > stallThreshold;
                Util::append(std::move(pending), incoming);
                pending.clear();
            }
            finishData = dataEnded;

            if (finishData)
                break;
            if (!incoming.empty() &&
                    !FP::equal(incoming.front().getStart(), incoming.back().getStart()))
                break;

            request.wait(lock);
            continue;
        }
        lock.unlock();

        if (wasStalled)
            response.notify_all();

        while (!incoming.empty() &&
                (finishData ||
                        !FP::equal(incoming.front().getStart(), incoming.back().getStart()))) {
            auto endUpdate = incoming.begin();
            double startTime = incoming.front().getStart();
            getDispatch(endUpdate->getUnit())->incomingData(std::move(*endUpdate));

            ++endUpdate;
            for (auto endList = incoming.end(); endUpdate != endList; ++endUpdate) {
                if (!FP::equal(endUpdate->getStart(), startTime))
                    break;
                getDispatch(endUpdate->getUnit())->incomingData(std::move(*endUpdate));
            }

            incoming.erase(incoming.begin(), endUpdate);

            for (auto target : advanceDispatch) {
                target->incomingAdvance(startTime);
            }
            bypassIngress->incomingAdvance(startTime);
        }


        if (finishData) {
            outputMux.sinkCreationComplete();
            endDispatch();

            /* This is more complicated then a simple wait because the 
             * terminate and endData() calls could be re-ordered.  That means 
             * there's a possibility the output multiplexer does not have an
             * egress but we've been terminated.
             */
            outputMux.finished.connect(std::bind(&ContaminationFilter::childThreadFinished, this));
            initiateFinish();

            lock.lock();
            if (state == Terminated)
                state = FinalizeTerminated;
            else
                state = Finalizing;
        }
    }
}

void ContaminationFilter::childThreadFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (state != Finalizing && state != FinalizeTerminated)
        return;
    request.notify_all();
}

void ContaminationFilter::finishTerminate()
{ }

void ContaminationFilter::initiateFinish()
{ }

bool ContaminationFilter::childThreadWait(int)
{ return true; }

void ContaminationFilter::completeFinish()
{ }


ContaminationFilter::RangeTracking::RangeTracking() = default;

ContaminationFilter::RangeTracking::~RangeTracking() = default;

ContaminationFilter::RangeTracking::RangeTracking(const RangeTracking &other) = default;

void ContaminationFilter::RangeTracking::shiftActive(double start)
{
    if (ranges.empty())
        return;
    if (!FP::defined(start))
        return;

    auto range = ranges.begin();
    auto end = ranges.end();
    while (range != end && FP::defined(range->end) && start >= range->end) {
        ++range;
    }
    if (range == ranges.begin())
        return;
    if (range == end) {
        ranges.clear();
        return;
    }

    ranges.erase(ranges.begin(), range);
}

bool ContaminationFilter::RangeTracking::isActive(double start, double end) const
{
    if (ranges.empty())
        return false;

    const auto &active = ranges.front();
    return (!FP::defined(active.start) || (FP::defined(start) && active.start <= start)) &&
            (!FP::defined(active.end) || (FP::defined(end) && active.end >= end));
}

void ContaminationFilter::RangeTracking::serialize(QDataStream &stream) const
{
    Serialize::container(stream, ranges, [&](const TimeRange &range) {
        stream << range.start << range.end;
    });
}

void ContaminationFilter::RangeTracking::deserialize(QDataStream &stream)
{
    Deserialize::container(stream, ranges, [&]() {
        TimeRange range;
        stream >> range.start >> range.end;
        return range;
    });
}

void ContaminationFilter::RangeTracking::copy(const RangeTracking &other)
{ ranges = other.ranges; }


ContaminationFilter::Dispatch::~Dispatch() = default;

ValueSegment::Stream *ContaminationFilter::Dispatch::getSegmenter()
{ return nullptr; }

int ContaminationFilter::Dispatch::advancePriority() const
{ return 0; }

ContaminationFilter::DispatchUnderlay::DispatchUnderlay(SinkMultiplexer::Sink *e)
        : values(), forwardTargets(), egress(e)
{ }

ContaminationFilter::DispatchUnderlay::~DispatchUnderlay() = default;

void ContaminationFilter::DispatchUnderlay::incomingData(SequenceValue &&value)
{
    SequenceSegment::Stream::maintainValues(values, value);
    for (auto target : forwardTargets) {
        target->incomingData(SequenceValue(value));
    }
    egress->incomingData(std::move(value));
}

void ContaminationFilter::DispatchUnderlay::incomingAdvance(double time)
{ Q_UNUSED(time); }

void ContaminationFilter::DispatchUnderlay::endData()
{ }

void ContaminationFilter::DispatchUnderlay::serialize(QDataStream &stream) const
{
    stream << values;
}

void ContaminationFilter::DispatchUnderlay::deserialize(QDataStream &stream)
{
    stream >> values;
}

ContaminationFilter::DispatchBypass::DispatchBypass(SinkMultiplexer::Sink *e) : egress(e)
{ }

ContaminationFilter::DispatchBypass::~DispatchBypass() = default;

void ContaminationFilter::DispatchBypass::incomingData(SequenceValue &&value)
{ egress->incomingData(std::move(value)); }

void ContaminationFilter::DispatchBypass::incomingAdvance(double time)
{ Q_UNUSED(time); }

void ContaminationFilter::DispatchBypass::endData()
{ }

void ContaminationFilter::DispatchBypass::serialize(QDataStream &stream) const
{ Q_UNUSED(stream); }

void ContaminationFilter::DispatchBypass::deserialize(QDataStream &stream)
{ Q_UNUSED(stream); }

ContaminationFilter::DispatchDiscard::DispatchDiscard()
{ }

ContaminationFilter::DispatchDiscard::~DispatchDiscard() = default;

void ContaminationFilter::DispatchDiscard::incomingData(SequenceValue &&)
{ }

void ContaminationFilter::DispatchDiscard::incomingAdvance(double time)
{ Q_UNUSED(time); }

void ContaminationFilter::DispatchDiscard::endData()
{ }

void ContaminationFilter::DispatchDiscard::serialize(QDataStream &stream) const
{ Q_UNUSED(stream); }

void ContaminationFilter::DispatchDiscard::deserialize(QDataStream &stream)
{ Q_UNUSED(stream); }

static const Variant::Flag contaminatedFlag = "Contaminated";

ContaminationFilter::DispatchFlagsMeta::DispatchFlagsMeta(SinkMultiplexer::Sink *e) : egress(e),
                                                                                      segmenter(),
                                                                                      needUpdate(
                                                                                              false),
                                                                                      forceUpdateTime(
                                                                                              FP::undefined()),
                                                                                      notifyFlags()
{
    contaminationFlags.push_back(contaminatedFlag);
}

ContaminationFilter::DispatchFlagsMeta::~DispatchFlagsMeta() = default;

void ContaminationFilter::DispatchFlagsMeta::incomingData(SequenceValue &&value)
{
    segmenter.add(value);
    egress->incomingData(std::move(value));
    needUpdate = true;
}

void ContaminationFilter::DispatchFlagsMeta::updateFlags(double time)
{
    contaminationFlags.clear();
    contaminationFlags.push_back(contaminatedFlag);

    bool valid = false;
    ValueSegment seg(segmenter.intermediateNext(&valid));
    if (!valid) {
        forceUpdateTime = FP::undefined();
        return;
    }
    if (FP::defined(seg.getStart()) && time < seg.getStart()) {
        forceUpdateTime = seg.getStart();
        return;
    }
    forceUpdateTime = seg.getEnd();
    if (seg.getValue().getType() != Variant::Type::MetadataFlags)
        return;

    for (const auto &check : seg.read().toMetadataSingleFlag().keys()) {
        if (check == contaminatedFlag)
            continue;
        if (!Variant::Composite::isContaminationFlag(check))
            continue;
        contaminationFlags.push_back(check);
    }
}

void ContaminationFilter::DispatchFlagsMeta::incomingAdvance(double time)
{
    if (!segmenter.completedAdvance(time).empty())
        needUpdate = true;
    if (!needUpdate && FP::defined(forceUpdateTime) && time >= forceUpdateTime)
        needUpdate = true;
    if (!needUpdate)
        return;
    needUpdate = false;

    updateFlags(time);
    for (std::vector<DispatchFlags *>::const_iterator v = notifyFlags.begin(),
            endV = notifyFlags.end(); v != endV; ++v) {
        (*v)->notifyUpdated();
    }
}

void ContaminationFilter::DispatchFlagsMeta::endData()
{ }

int ContaminationFilter::DispatchFlagsMeta::advancePriority() const
{ return -2; }

ValueSegment::Stream *ContaminationFilter::DispatchFlagsMeta::getSegmenter()
{ return &segmenter; }

void ContaminationFilter::DispatchFlagsMeta::registerFlags(DispatchFlags *flags)
{
    notifyFlags.push_back(flags);
}

void ContaminationFilter::DispatchFlagsMeta::serialize(QDataStream &stream) const
{
    Q_ASSERT(!needUpdate);
    stream << segmenter << forceUpdateTime << contaminationFlags;
}

void ContaminationFilter::DispatchFlagsMeta::deserialize(QDataStream &stream)
{
    stream >> segmenter >> forceUpdateTime >> contaminationFlags;
}

ContaminationFilter::DispatchFlags::DispatchFlags(SinkMultiplexer::Sink *e, DispatchFlagsMeta *m)
        : egress(e),
          meta(m),
          segmenter(),
          needUpdate(true),
          forceUpdateTime(FP::undefined()),
          contaminated(false),
          notifyValues()
{
    meta->registerFlags(this);
}

ContaminationFilter::DispatchFlags::~DispatchFlags() = default;

void ContaminationFilter::DispatchFlags::incomingData(SequenceValue &&value)
{
    segmenter.add(value);
    egress->incomingData(std::move(value));
    needUpdate = true;
}

void ContaminationFilter::DispatchFlags::incomingAdvance(double time)
{
    if (!segmenter.completedAdvance(time).empty())
        needUpdate = true;
    if (!needUpdate && FP::defined(forceUpdateTime) && time >= forceUpdateTime)
        needUpdate = true;
    if (!needUpdate)
        return;
    needUpdate = false;

    bool valid = false;
    ValueSegment seg(segmenter.intermediateNext(&valid));
    if (!valid) {
        forceUpdateTime = FP::undefined();
        return;
    }

    /* Set this explicitly after the validity check, so we retain the last
     * known contaminated state if the flags are removed (but not invalidated).
     * This is required so that times when the flags end immediately before
     * a partial value segment (e.x. from STP correction) does not result
     * in the partial value being "uncontaminated". */
    contaminated = false;

    if (FP::defined(seg.getStart())) {
        forceUpdateTime = seg.getStart();
        for (std::vector<DispatchValue *>::iterator target = notifyValues.begin(),
                endTarget = notifyValues.end(); target != endTarget; ++target) {
            (*target)->addFlagsBreak(forceUpdateTime);
        }

        if (time < forceUpdateTime)
            return;
    }

    forceUpdateTime = seg.getEnd();
    if (FP::defined(forceUpdateTime)) {
        for (std::vector<DispatchValue *>::iterator target = notifyValues.begin(),
                endTarget = notifyValues.end(); target != endTarget; ++target) {
            (*target)->addFlagsBreak(forceUpdateTime);
        }
    }

    if (seg.getValue().getType() != Variant::Type::Flags)
        return;

    const auto &haveFlags = seg.getValue().toFlags();
    if (!haveFlags.empty()) {
        for (const auto &check : meta->allFlags()) {
            if (haveFlags.count(check) != 0) {
                contaminated = true;
                return;
            }
        }
    }
}

void ContaminationFilter::DispatchFlags::endData()
{ }

int ContaminationFilter::DispatchFlags::advancePriority() const
{ return -1; }

ValueSegment::Stream *ContaminationFilter::DispatchFlags::getSegmenter()
{ return &segmenter; }

void ContaminationFilter::DispatchFlags::serialize(QDataStream &stream) const
{
    stream << segmenter << needUpdate << forceUpdateTime << contaminated;
}

void ContaminationFilter::DispatchFlags::deserialize(QDataStream &stream)
{
    stream >> segmenter >> needUpdate >> forceUpdateTime >> contaminated;
}

void ContaminationFilter::DispatchFlags::registerValue(DispatchValue *t)
{
    notifyValues.push_back(t);
    auto reader = t->getSegmenter();
    if (reader) {
        if (FP::defined(forceUpdateTime))
            reader->addBreak(forceUpdateTime);
    }
}


ContaminationFilter::DispatchValueMeta::DispatchValueMeta(SinkMultiplexer::Sink *e,
                                                          DynamicSequenceSelection *a,
                                                          const SequenceName &u) : egress(e),
                                                                                   affected(a),
                                                                                   unit(u),
                                                                                   segmenter(),
                                                                                   unitValue(
                                                                                           u.fromMeta()),
                                                                                   disabled(),
                                                                                   remove(),
                                                                                   notifyValues()
{ }

ContaminationFilter::DispatchValueMeta::~DispatchValueMeta()
{
    Q_ASSERT(!egress);
}

void ContaminationFilter::DispatchValueMeta::processMeta(Data::ValueSegment::Transfer &&values)
{
    if (values.empty())
        return;

    Variant::Write meta = Variant::Write::empty();
    meta.hash("By").setString("corr_removecontam");
    meta.hash("At").setDouble(Time::time());
    meta.hash("Environment").setString(Environment::describe());
    meta.hash("Revision").setString(Environment::revision());

    for (auto &segment : values) {
        disabled.shiftActive(segment.getStart());
        remove.shiftActive(segment.getStart());

        auto &output = segment.root();

        auto smoothing = output.write().metadata("Smoothing");
        if (!smoothing.exists()) {
            switch (output.read().getType()) {
            case Variant::Type::MetadataArray:
            case Variant::Type::MetadataMatrix:
                smoothing = output.write().metadataArray("Children").metadata("Smoothing");
                break;
            default:
                break;
            }
        }
        const auto &mode = smoothing.hash("Mode").toString();
        bool deleteContam = false;
        /* Don't include the Beer's law variants, since those are handled
         * by their complementary inputs rather than the actual averaged
         * unit */
        if (Util::equal_insensitive(mode, "difference", "differenceinitial"))
            deleteContam = true;

        if (deleteContam) {
            for (auto v : notifyValues) {
                v->addRemoveRange(segment.getStart(), segment.getEnd());
            }
            remove.add(segment.getStart(), segment.getEnd());
        } else {
            for (auto v : notifyValues) {
                v->clearRemove(segment.getStart());
            }
            remove.clear(segment.getStart());
        }

        if (!affected->get(segment).count(unitValue) ||
                output.read().metadata("ContaminationRemoved").toBool()) {
            for (auto v : notifyValues) {
                v->addDisabledRange(segment.getStart(), segment.getEnd());
            }
            disabled.add(segment.getStart(), segment.getEnd());

            egress->incomingData(
                    SequenceValue({unit, segment.getStart(), segment.getEnd()}, std::move(output)));
            continue;
        }

        /* Don't need a detach here, since we haven't emitted the output anywhere else
         * and the segment merge includes an implicit one from the input values */
        output.write().metadata("Processing").toArray().after_back().set(meta);
        output.write().metadata("ContaminationRemoved").setBool(true);

        for (auto v : notifyValues) {
            v->setEnabled(segment.getStart());
        }
        disabled.clear(segment.getStart());

        egress->incomingData(
                SequenceValue({unit, segment.getStart(), segment.getEnd()}, std::move(output)));
    }
}

void ContaminationFilter::DispatchValueMeta::incomingData(SequenceValue &&value)
{ processMeta(segmenter.add(std::move(value))); }

void ContaminationFilter::DispatchValueMeta::incomingAdvance(double time)
{
    processMeta(segmenter.advance(time));
    egress->incomingAdvance(time);
}

void ContaminationFilter::DispatchValueMeta::endData()
{
    processMeta(segmenter.finish());
    egress->endData();
    egress = nullptr;
}

int ContaminationFilter::DispatchValueMeta::advancePriority() const
{ return -1; }

ValueSegment::Stream *ContaminationFilter::DispatchValueMeta::getSegmenter()
{ return &segmenter; }

void ContaminationFilter::DispatchValueMeta::registerValue(DispatchValue *t)
{
    notifyValues.push_back(t);
}

void ContaminationFilter::DispatchValueMeta::serialize(QDataStream &stream) const
{
    stream << segmenter << affected.get();
}

void ContaminationFilter::DispatchValueMeta::deserialize(QDataStream &stream)
{
    affected.reset();
    Data::DynamicSequenceSelection *a;
    stream >> segmenter >> a;
    affected.reset(a);
}


ContaminationFilter::DispatchValue::DispatchValue(SinkMultiplexer::Sink *e,
                                                  DynamicSequenceSelection *a,
                                                  DispatchFlags *f,
                                                  DispatchValueMeta *m,
                                                  const Data::SequenceName &u) : egress(e),
                                                                                 affected(a),
                                                                                 flags(f),
                                                                                 meta(m),
                                                                                 unit(u),
                                                                                 segmenter(),
                                                                                 contaminated(
                                                                                         false),
                                                                                 disabled(
                                                                                         meta->disabled),
                                                                                 remove(meta->remove)
{
    meta->registerValue(this);
    flags->registerValue(this);
}

ContaminationFilter::DispatchValue::~DispatchValue()
{
    Q_ASSERT(!egress);
}

void ContaminationFilter::DispatchValue::processSegment(ValueSegment &&segment)
{
    auto &output = segment.root();
    if (!contaminated || !affected->get(segment).count(unit)) {
        egress->incomingData(
                SequenceValue({unit, segment.getStart(), segment.getEnd()}, std::move(output)));
        return;
    }
    disabled.shiftActive(segment.getStart());
    if (disabled.isActive(segment.getStart(), segment.getEnd())) {
        egress->incomingData(
                SequenceValue({unit, segment.getStart(), segment.getEnd()}, std::move(output)));
        return;
    }

    remove.shiftActive(segment.getStart());
    if (remove.isActive(segment.getStart(), segment.getEnd())) {
        egress->incomingAdvance(segment.getStart());
        return;
    }

    auto v = output.write();
    switch (v.getType()) {
    case Variant::Type::Real:
        v.setDouble(FP::undefined());
        break;
    case Variant::Type::Integer:
        v.setInteger(INTEGER::undefined());
        break;
    default:
        v.setEmpty();
        break;
    }
    egress->incomingData(
            SequenceValue({unit, segment.getStart(), segment.getEnd()}, std::move(output)));
}

void ContaminationFilter::DispatchValue::processValues(ValueSegment::Transfer &&values)
{
    if (values.empty())
        return;

    for (auto &segment : values) {
        processSegment(std::move(segment));
    }
}

void ContaminationFilter::DispatchValue::incomingData(SequenceValue &&value)
{ processValues(segmenter.add(std::move(value))); }

void ContaminationFilter::DispatchValue::incomingAdvance(double time)
{
    bool future = true;
    processValues(segmenter.completedAdvance(time, &future));
    if (!future)
        egress->incomingAdvance(time);

    /* Save the contaminated state now that the flags have advanced, so anything
     * until the next advance has this state */
    contaminated = flags->isContaminated();
}

void ContaminationFilter::DispatchValue::endData()
{
    for (auto &segment : segmenter.finish()) {
        /* Handle the degenerate case of the flags ending before the
         * end of data (thus uncontaminating) */
        if (FP::defined(flags->forceUpdateTime) &&
                FP::defined(segment.getStart()) &&
                segment.getStart() >= flags->forceUpdateTime)
            contaminated = false;

        processSegment(std::move(segment));
    }
    egress->endData();
    egress = nullptr;
}

int ContaminationFilter::DispatchValue::advancePriority() const
{ return 1; }

ValueSegment::Stream *ContaminationFilter::DispatchValue::getSegmenter()
{ return &segmenter; }


void ContaminationFilter::DispatchValue::serialize(QDataStream &stream) const
{
    stream << segmenter << affected.get() << contaminated;

    disabled.serialize(stream);
    remove.serialize(stream);
}

void ContaminationFilter::DispatchValue::deserialize(QDataStream &stream)
{
    affected.reset();

    Data::DynamicSequenceSelection *a;
    stream >> segmenter >> a >> contaminated;
    affected.reset(a);

    disabled.deserialize(stream);
    remove.deserialize(stream);
}

void ContaminationFilter::RangeTracking::add(double start, double end)
{
    if (!ranges.empty()) {
        auto &last = ranges.back();
        if (FP::defined(last.end)) {
            Q_ASSERT(FP::defined(start));
            if (last.end >= start) {
                last.end = end;
                return;
            }
        }
    }

    ranges.emplace_back(start, end);
}

void ContaminationFilter::RangeTracking::clear(double time)
{
    if (ranges.empty())
        return;
    Q_ASSERT(FP::defined(time));
    auto &last = ranges.back();
    if (!FP::defined(last.end) || last.end > time) {
        if (FP::defined(last.start) && last.start == time) {
            ranges.pop_back();
            return;
        }
        last.end = time;
    }
}


EditingContaminationFilter::EditingContaminationFilter(DynamicSequenceSelection *affected,
                                                       SequenceMatch::Composite *setVariables)
        : ContaminationFilter(affected), variables(setVariables)
{
    if (variables)
        variables->clearArchiveSpecification();
}

EditingContaminationFilter::EditingContaminationFilter(const QString &id,
                                                       SequenceMatch::Composite *setVariables)
        : ContaminationFilter(id), variables(setVariables)
{
    if (variables)
        variables->clearArchiveSpecification();
}

EditingContaminationFilter::~EditingContaminationFilter() = default;

static const SequenceName::Component archiveCont = "cont";
static const SequenceName::Component archiveContMeta = "cont_meta";
static const SequenceName::Component archiveAvg = "avg";
static const SequenceName::Component archiveAvgMeta = "avg_meta";

EditingContaminationFilter::UnitClassification EditingContaminationFilter::classifyUnit(const Data::SequenceName &unit) const
{
    if (unit.getArchive() == archiveCont ||
            unit.getArchive() == archiveContMeta ||
            unit.getArchive() == archiveAvg ||
            unit.getArchive() == archiveAvgMeta) {
        if (!variables || variables->matches(unit))
            return Unit_Generated;
        return Unit_Bypassed;
    }
    if (!variables || variables->matches(unit))
        return Unit_Affected;
    return Unit_Bypassed;
}

SequenceName::Component EditingContaminationFilter::outputArchive() const
{ return archiveAvg; }


ContaminationFilter::Dispatch *EditingContaminationFilter::overrideDispatch(const SequenceName &unit)
{
    SequenceName baseUnit(unit);
    baseUnit.removeFlavor(SequenceName::flavor_cover);
    baseUnit.removeFlavor(SequenceName::flavor_stats);
    switch (classifyUnit(baseUnit)) {
    case Unit_Affected:
        if (Util::starts_with(unit.getVariable(), "F1_"))
            return nullptr;
        break;
    case Unit_Generated:
        return new DispatchDiscard;
    case Unit_Bypassed:
        if (Util::starts_with(unit.getVariable(), "F1_"))
            return nullptr;
        return new DispatchBypass(getBypassIngress());
    }

    if (unit.isMeta()) {
        if (!getAffected()->registerInput(unit.fromMeta())) {
            SequenceName duplicateUnit(unit);
            duplicateUnit.setArchive(outputArchive() + "_meta");
            return new DispatchBypassDuplicate(getBypassIngress(), duplicateUnit);
        }
    } else {
        if (!getAffected()->registerInput(unit)) {
            SequenceName duplicateUnit(unit);
            duplicateUnit.setArchive(outputArchive());
            return new DispatchBypassDuplicate(getBypassIngress(), duplicateUnit);
        }
    }
    return nullptr;
}

ContaminationFilter::Dispatch *EditingContaminationFilter::createGeneralDispatch(const SequenceName &unit)
{
    if (Util::starts_with(unit.getVariable(), "F1_")) {
        if (unit.isMeta()) {
            if (classifyUnit(unit) != Unit_Affected)
                return new DispatchFlagsMeta(getBypassIngress());

            SequenceName duplicateUnit(unit);
            duplicateUnit.setArchive(outputArchive() + "_meta");
            return new DispatchFlagsMetaDuplicate(getBypassIngress(), duplicateUnit);
        } else {
            DispatchFlagsMeta *meta = static_cast<DispatchFlagsMeta *>(
                    getDispatch(unit.toMeta()));
            if (classifyUnit(unit) != Unit_Affected)
                return new DispatchFlags(getBypassIngress(), meta);

            SequenceName duplicateUnit(unit);
            duplicateUnit.setArchive(outputArchive());
            return new DispatchFlagsDuplicate(getBypassIngress(), meta, duplicateUnit);
        }
    }
    Q_ASSERT(classifyUnit(unit.withoutFlavor("cover").withoutFlavor("stats")) == Unit_Affected);

    SequenceName outputUnit(unit);
    if (unit.isMeta()) {
        outputUnit.setArchive(outputArchive() + "_meta");
        DynamicSequenceSelection *o = getAffected()->clone();
        o->forceUnit({}, outputArchive());
        return new DispatchValueMetaOutput(createOutputIngress(), o, outputUnit,
                                           getBypassIngress());
    }
    outputUnit.setArchive(outputArchive());

    SequenceName flagsUnit = unit;
    flagsUnit.setVariable("F1_" + Util::suffix(flagsUnit.getVariable(), '_'));
    flagsUnit.removeFlavor(SequenceName::flavor_end);
    DynamicSequenceSelection *o = getAffected()->clone();
    o->forceUnit({}, outputArchive());
    return new DispatchValueOutput(createOutputIngress(), o,
                                   static_cast<DispatchFlags *>(getDispatch(flagsUnit)),
                                   static_cast<DispatchValueMeta *>(getDispatch(
                                           unit.toMeta().withoutFlavor(SequenceName::flavor_end))),
                                   outputUnit,
                                   getBypassIngress());
}

EditingContaminationFilter::DispatchBypassDuplicate::DispatchBypassDuplicate(SinkMultiplexer::Sink *e,
                                                                             const SequenceName &d)
        : egress(e), duplicateUnit(d)
{ }

EditingContaminationFilter::DispatchBypassDuplicate::~DispatchBypassDuplicate() = default;

void EditingContaminationFilter::DispatchBypassDuplicate::incomingData(SequenceValue &&value)
{
    SequenceValue dup = value;
    egress->incomingData(std::move(value));
    dup.setUnit(duplicateUnit);
    egress->incomingData(std::move(dup));
}

void EditingContaminationFilter::DispatchBypassDuplicate::incomingAdvance(double time)
{ Q_UNUSED(time); }

void EditingContaminationFilter::DispatchBypassDuplicate::endData()
{ }

void EditingContaminationFilter::DispatchBypassDuplicate::serialize(QDataStream &stream) const
{ Q_UNUSED(stream); }

void EditingContaminationFilter::DispatchBypassDuplicate::deserialize(QDataStream &stream)
{ Q_UNUSED(stream); }

EditingContaminationFilter::DispatchFlagsMetaDuplicate::DispatchFlagsMetaDuplicate(SinkMultiplexer::Sink *e,
                                                                                   const SequenceName &d)
        : ContaminationFilter::DispatchFlagsMeta(e), egress(e), duplicateUnit(d)
{ }

EditingContaminationFilter::DispatchFlagsMetaDuplicate::~DispatchFlagsMetaDuplicate() = default;

void EditingContaminationFilter::DispatchFlagsMetaDuplicate::incomingData(SequenceValue &&value)
{
    if (!value.getName().isDefaultStation()) {
        SequenceValue dup = value;
        dup.setUnit(duplicateUnit);
        egress->incomingData(std::move(dup));
    }
    ContaminationFilter::DispatchFlagsMeta::incomingData(std::move(value));
}

EditingContaminationFilter::DispatchFlagsDuplicate::DispatchFlagsDuplicate(SinkMultiplexer::Sink *e,
                                                                           ContaminationFilter::DispatchFlagsMeta *m,
                                                                           const SequenceName &d)
        : ContaminationFilter::DispatchFlags(e, m), egress(e), duplicateUnit(d)
{ }

EditingContaminationFilter::DispatchFlagsDuplicate::~DispatchFlagsDuplicate() = default;

void EditingContaminationFilter::DispatchFlagsDuplicate::incomingData(SequenceValue &&value)
{
    if (!value.getUnit().isDefaultStation()) {
        SequenceValue dup = value;
        dup.setUnit(duplicateUnit);
        egress->incomingData(std::move(dup));
    }
    ContaminationFilter::DispatchFlags::incomingData(std::move(value));
}

EditingContaminationFilter::DispatchValueOutput::DispatchValueOutput(SinkMultiplexer::Sink *e,
                                                                     DynamicSequenceSelection *a,
                                                                     ContaminationFilter::DispatchFlags *f,
                                                                     ContaminationFilter::DispatchValueMeta *m,
                                                                     const SequenceName &u,
                                                                     SinkMultiplexer::Sink *unchangedE)
        : ContaminationFilter::DispatchValue(e, a, f, m, u), unchangedEgress(unchangedE)
{ }

EditingContaminationFilter::DispatchValueOutput::~DispatchValueOutput() = default;

void EditingContaminationFilter::DispatchValueOutput::incomingData(SequenceValue &&value)
{
    if (!value.getUnit().isDefaultStation())
        unchangedEgress->incomingData(value);
    ContaminationFilter::DispatchValue::incomingData(std::move(value));
}

EditingContaminationFilter::DispatchValueMetaOutput::DispatchValueMetaOutput(SinkMultiplexer::Sink *e,
                                                                             DynamicSequenceSelection *a,
                                                                             const SequenceName &u,
                                                                             SinkMultiplexer::Sink *unchangedE)
        : ContaminationFilter::DispatchValueMeta(e, a, u), unchangedEgress(unchangedE)
{ }

EditingContaminationFilter::DispatchValueMetaOutput::~DispatchValueMetaOutput() = default;

void EditingContaminationFilter::DispatchValueMetaOutput::incomingData(SequenceValue &&value)
{
    if (!value.getUnit().isDefaultStation())
        unchangedEgress->incomingData(value);
    ContaminationFilter::DispatchValueMeta::incomingData(std::move(value));
}


static const SequenceName::Component archiveStats = "stats";
static const SequenceName::Component archiveStatsMeta = "stats_meta";

EditingContaminationFinal::EditingContaminationFinal(Data::DynamicSequenceSelection *affected)
        : EditingContaminationFilter(affected)
{ }

EditingContaminationFinal::EditingContaminationFinal(const QString &id)
        : EditingContaminationFilter(id)
{ }

EditingContaminationFinal::~EditingContaminationFinal() = default;

EditingContaminationFinal::UnitClassification EditingContaminationFinal::classifyUnit(const SequenceName &unit) const
{
    /* Always generate the "stats" temporary archive for the final calculator */
    if (unit.getArchive() == archiveStats || unit.getArchive() == archiveStatsMeta)
        return Unit_Generated;
    /* Don't touch any existing averages: they'll either be removed by the final calculator
     * or we don't want to duplicate them if they're not (we'd create two copies in the "stats"
     * archive if we did) */
    if (unit.getArchive() == archiveCont ||
            unit.getArchive() == archiveContMeta ||
            unit.getArchive() == archiveAvg ||
            unit.getArchive() == archiveAvgMeta) {
        return Unit_Bypassed;
    }
    return Unit_Affected;
}

SequenceName::Component EditingContaminationFinal::outputArchive() const
{ return archiveStats; }


SmoothingContaminationFilter::SmoothingContaminationFilter(SmoothingEngine *e,
                                                           DynamicSequenceSelection *affected)
        : ContaminationFilter(affected), engine(e)
{
    Q_ASSERT(engine);
}

SmoothingContaminationFilter::SmoothingContaminationFilter(SmoothingEngine *e, const QString &id)
        : ContaminationFilter(id), engine(e)
{
    Q_ASSERT(engine);
}

SmoothingContaminationFilter::~SmoothingContaminationFilter() = default;

void SmoothingContaminationFilter::serialize(QDataStream &stream)
{
    engine->serialize(stream);
    ContaminationFilter::serialize(stream);
}

void SmoothingContaminationFilter::setEgress(StreamSink *egress)
{
    if (!egress) {
        ContaminationFilter::setEgress(nullptr);
        engine->setEgress(nullptr);
    } else {
        engine->setEgress(egress);
        ContaminationFilter::setEgress(engine.get());
    }
}

void SmoothingContaminationFilter::finishTerminate()
{
    engine->signalTerminate();
    engine->wait();
}

void SmoothingContaminationFilter::initiateFinish()
{
    engine->finished.connect(std::bind(&SmoothingContaminationFilter::childThreadFinished, this));
}

bool SmoothingContaminationFilter::childThreadWait(int timeout)
{ return engine->wait(timeout / 1000.0); }

void SmoothingContaminationFilter::completeFinish()
{ engine->wait(); }

void SmoothingContaminationFilter::signalTerminate()
{
    engine->signalTerminate();
    ContaminationFilter::signalTerminate();
}

void SmoothingContaminationFilter::start()
{
    engine->start();
    ContaminationFilter::start();
}


DynamicSequenceSelection *ContaminationFilter::systemContaminationAffected(const SequenceName::Component &station,
                                                                           const QString &type,
                                                                           double start,
                                                                           double end)
{
    auto config = ValueSegment::Stream::read(
            {start, end, {station}, {"configuration"}, {"contamination"}});
    return DynamicSequenceSelection::fromConfiguration(config, QString("Affected/%1").arg(type),
                                                       start, end);
}

}
}
