/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QThread>
#include <QTimer>
#include <QLoggingCategory>

#include "smoothing/smoothingengine.hxx"
#include "core/environment.hxx"
#include "core/threadpool.hxx"
#include "smoothing/digitalfilterchain.hxx"
#include "smoothing/tukeychain.hxx"
#include "smoothing/fourierchain.hxx"


Q_LOGGING_CATEGORY(log_smoothing_smoothingengine, "cpd3.smoothing.smoothingengine", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/smoothingengine.hxx
 * This is implementation handler that deals with constructing smoother 
 * networks. */

SmoothingEngineArrayMergeBasic::SmoothingEngineArrayMergeBasic(std::size_t size,
                                                               const SequenceName &setUnit,
                                                               SinkMultiplexer::Sink *setTarget)
        : SmoothingEngineArrayMerger<double, SmootherChainTarget, double>(size, setUnit, setTarget)
{ }

SmoothingEngineArrayMergeBasic::SmoothingEngineArrayMergeBasic(QDataStream &stream,
                                                               const SequenceName &setUnit,
                                                               SinkMultiplexer::Sink *setTarget)
        : SmoothingEngineArrayMerger<double, SmootherChainTarget, double>(stream, setUnit,
                                                                          setTarget)
{ }

SmoothingEngineArrayMergeFlags::SmoothingEngineArrayMergeFlags(std::size_t size,
                                                               const SequenceName &setUnit,
                                                               SinkMultiplexer::Sink *setTarget)
        : SmoothingEngineArrayMerger<Data::Variant::Flags, SmootherChainTargetFlags,
                                     const Data::Variant::Flags &>(size, setUnit, setTarget)
{ }

SmoothingEngineArrayMergeFlags::SmoothingEngineArrayMergeFlags(QDataStream &stream,
                                                               const SequenceName &setUnit,
                                                               SinkMultiplexer::Sink *setTarget)
        : SmoothingEngineArrayMerger<Data::Variant::Flags, SmootherChainTargetFlags,
                                     const Data::Variant::Flags &>(stream, setUnit, setTarget)
{ }

SmoothingEngineArrayMergeGeneral::SmoothingEngineArrayMergeGeneral(std::size_t size,
                                                                   const SequenceName &setUnit,
                                                                   SinkMultiplexer::Sink *setTarget)
        : SmoothingEngineArrayMerger<Data::Variant::Root, SmootherChainTargetGeneral,
                                     const Variant::Root &>(size,
                                                            setUnit,
                                                            setTarget)
{ }

SmoothingEngineArrayMergeGeneral::SmoothingEngineArrayMergeGeneral(QDataStream &stream,
                                                                   const SequenceName &setUnit,
                                                                   SinkMultiplexer::Sink *setTarget)
        : SmoothingEngineArrayMerger<Data::Variant::Root, SmootherChainTargetGeneral,
                                     const Variant::Root &>(stream,
                                                            setUnit,
                                                            setTarget)
{ }

SmoothingEngineMatrixMergeBasic::SmoothingEngineMatrixMergeBasic(const Data::Variant::PathElement::MatrixIndex &size,
                                                                 const SequenceName &setUnit,
                                                                 SinkMultiplexer::Sink *setTarget)
        : SmoothingEngineMatrixMerger<double, SmootherChainTarget, double>(size, setUnit, setTarget)
{ }

SmoothingEngineMatrixMergeBasic::SmoothingEngineMatrixMergeBasic(QDataStream &stream,
                                                                 const SequenceName &setUnit,
                                                                 SinkMultiplexer::Sink *setTarget)
        : SmoothingEngineMatrixMerger<double, SmootherChainTarget, double>(stream, setUnit,
                                                                           setTarget)
{ }

SmoothingEngineMatrixMergeFlags::SmoothingEngineMatrixMergeFlags(const Data::Variant::PathElement::MatrixIndex &size,
                                                                 const SequenceName &setUnit,
                                                                 SinkMultiplexer::Sink *setTarget)
        : SmoothingEngineMatrixMerger<Data::Variant::Flags, SmootherChainTargetFlags,
                                      const Data::Variant::Flags &>(size, setUnit, setTarget)
{ }

SmoothingEngineMatrixMergeFlags::SmoothingEngineMatrixMergeFlags(QDataStream &stream,
                                                                 const SequenceName &setUnit,
                                                                 SinkMultiplexer::Sink *setTarget)
        : SmoothingEngineMatrixMerger<Data::Variant::Flags, SmootherChainTargetFlags,
                                      const Data::Variant::Flags &>(stream, setUnit, setTarget)
{ }

SmoothingEngineMatrixMergeGeneral::SmoothingEngineMatrixMergeGeneral(const Data::Variant::PathElement::MatrixIndex &size,
                                                                     const SequenceName &setUnit,
                                                                     SinkMultiplexer::Sink *setTarget)
        : SmoothingEngineMatrixMerger<Data::Variant::Root, SmootherChainTargetGeneral,
                                      const Variant::Root &>(size,
                                                             setUnit,
                                                             setTarget)
{ }

SmoothingEngineMatrixMergeGeneral::SmoothingEngineMatrixMergeGeneral(QDataStream &stream,
                                                                     const SequenceName &setUnit,
                                                                     SinkMultiplexer::Sink *setTarget)
        : SmoothingEngineMatrixMerger<Data::Variant::Root, SmootherChainTargetGeneral,
                                      const Variant::Root &>(stream,
                                                             setUnit,
                                                             setTarget)
{ }

template<typename FindType>
static void eraseFromList(std::vector<FindType *> &list, FindType *f)
{
    auto check = std::find(list.begin(), list.end(), f);
    if (check != list.end()) {
        list.erase(check);
    }
}


void SmoothingEngine::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;

    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!dataEnded);
#ifndef NDEBUG
        if (enableIncomingCheck) {
            Q_ASSERT(Range::compareStart(values.front().getStart(), lastIncomingTime) >= 0);
            lastIncomingTime = values.back().getStart();
        }
#endif

        dataAdvance = FP::undefined();
        Util::append(values, pending);
    }
    request.notify_all();
}

void SmoothingEngine::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;

    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!dataEnded);
#ifndef NDEBUG
        if (enableIncomingCheck) {
            Q_ASSERT(Range::compareStart(values.front().getStart(), lastIncomingTime) >= 0);
            lastIncomingTime = values.back().getStart();
        }
#endif

        dataAdvance = FP::undefined();
        Util::append(std::move(values), pending);
    }
    request.notify_all();
}

void SmoothingEngine::incomingData(const SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!dataEnded);
#ifndef NDEBUG
        if (enableIncomingCheck) {
            Q_ASSERT(Range::compareStart(value.getStart(), lastIncomingTime) >= 0);
            lastIncomingTime = value.getStart();
        }
#endif

        dataAdvance = FP::undefined();
        pending.emplace_back(value);
    }
    request.notify_all();
}

void SmoothingEngine::incomingData(SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!dataEnded);
#ifndef NDEBUG
        if (enableIncomingCheck) {
            Q_ASSERT(Range::compareStart(value.getStart(), lastIncomingTime) >= 0);
            lastIncomingTime = value.getStart();
        }
#endif

        dataAdvance = FP::undefined();
        pending.emplace_back(std::move(value));
    }
    request.notify_all();
}

void SmoothingEngine::endData()
{
    std::lock_guard<std::mutex> lock(mutex);
    dataEnded = true;
    if (state == Completed)
        return;
    request.notify_all();
}

void SmoothingEngine::incomingAdvance(double time)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(!dataEnded);
#ifndef NDEBUG
        if (enableIncomingCheck) {
            Q_ASSERT(Range::compareStart(time, lastIncomingTime) >= 0);
            lastIncomingTime = time;
        }
#endif
        dataAdvance = time;
    }
    request.notify_all();
}

void SmoothingEngine::stall(std::unique_lock<std::mutex> &lock)
{
    while (pending.size() > stallThreshold) {
        switch (state) {
        case NotStarted:
        case Running:
        case PauseRequested:
        case Paused:
            break;
        case Terminated:
        case Finalizing:
        case FinalizeTerminated:
        case Completed:
            return;
        }
        response.wait(lock);
    }
}

void SmoothingEngine::setEgress(StreamSink *egress)
{
    outputMux.setEgress(egress);
}

void SmoothingEngine::signalTerminate()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (state == Finalizing)
        state = FinalizeTerminated;
    else if (state != Completed)
        state = Terminated;
    request.notify_all();
    response.notify_all();
}


SmoothingEngine::SmoothingEngine(SmootherChainCore *setCore, bool enableIncomingSanityCheck)
        : ProcessingStage(),
          core(setCore),
          outputMux(), holdback(nullptr),
#ifndef NDEBUG
          enableIncomingCheck(enableIncomingSanityCheck),
          lastIncomingTime(FP::undefined()),
#endif
          dataEnded(false),
          dataAdvance(FP::undefined()),
          advanceStartTime(FP::undefined()), state(NotStarted)
{
#ifdef NDEBUG
    Q_UNUSED(enableIncomingSanityCheck);
#endif
    /* Created here so it exists before a possible deserialize */
    holdback = createOutputPlaceholder();
}

SmoothingEngine::~SmoothingEngine()
{
    endAllChains();

    {
        std::unique_lock<std::mutex> lock(mutex);
        if (state == Running)
            state = Terminated;
    }
    request.notify_all();
    if (thread.joinable())
        thread.join();

    outputMux.signalTerminate(true);
    outputMux.wait();

    core.reset();

    dispatch.clear();
    dispatchNew.clear();
    dispatchPendingDelete.clear();

    for (const auto &cleanup : chains) {
        cleanup->cleanup();
    }
    chains.clear();
}

void SmoothingEngine::uniteNewDispatch()
{
    Util::merge(std::move(dispatchNew), dispatch);
    dispatchNew.clear();
}

/**
 * Reconstruct the smoothing engine from saved state.  The engine must not
 * not have been started yet.
 * 
 * @param stream    the source stream
 */
void SmoothingEngine::deserialize(QDataStream &stream)
{
    Q_ASSERT(state == NotStarted);
    Q_ASSERT(chains.empty());
    Q_ASSERT(primaryChains.empty());
    Q_ASSERT(dispatch.empty());
    Q_ASSERT(dispatchNew.empty());

    stream >> pending >> dataAdvance >> outputMux;

    quint32 n;
    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        SequenceName unit;
        stream >> unit;
        DispatchTarget *target = dispatchTarget(unit);
        Q_ASSERT(target);
        target->deserialize(stream);
    }
    uniteNewDispatch();

    stream >> n;
    chains.reserve(n);
    for (quint32 i = 0; i < n; i++) {
        chains.emplace_back(Chain::deserialize(this, stream));
    }
    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        SequenceName unit;
        stream >> unit;
        quint32 idx;
        stream >> idx;
        Q_ASSERT(idx < chains.size());
        primaryChains.emplace(unit, chains[idx].get());
    }
}

void SmoothingEngine::serializeInternal(QDataStream &stream)
{
    Q_ASSERT(incoming.empty());
    Q_ASSERT(state != Running);

    stream << pending << dataAdvance << outputMux;

    stream << static_cast<quint32>(dispatch.size());
    for (const auto &d : dispatch) {
        stream << d.first;
        d.second->serialize(stream);
    }

    std::unordered_map<Chain *, quint32> chainUIDLookup;
    stream << static_cast<quint32>(chains.size());
    quint32 idx = 0;
    for (const auto &chain : chains) {
        chain->serialize(stream);
        chainUIDLookup.emplace(chain.get(), idx);
        ++idx;
    }
    stream << static_cast<quint32>(primaryChains.size());
    for (const auto &pc : primaryChains) {
        stream << pc.first;
        stream << chainUIDLookup[pc.second];
    }
}

/**
 * Serialize the smoothing engine.  The egress must have been set to NULL
 * before this is called.  That is, the engine must have been paused.
 * <br>
 * Internally this just calls pauseProcessing(), 
 * serializeInternal(QDataStream &), then resumeProcessing().  So a child class 
 * that requires serialization of a non-constant element can override that 
 * sequence (serializing it between the pause and resume).
 * 
 * @param stream    the target stream
 */
void SmoothingEngine::serialize(QDataStream &stream)
{
    pauseProcessing();
    serializeInternal(stream);
    resumeProcessing();
}

void SmoothingEngine::pauseProcessing()
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            if (state == Terminated || state == NotStarted || state == Paused)
                break;
            state = PauseRequested;
            request.notify_all();
            response.wait(lock);
        }
    }

    reapFinishedChains();
    uniteNewDispatch();

    for (const auto &chain : chains) {
        chain->pause();
    }
}

void SmoothingEngine::resumeProcessing()
{
    for (const auto &chain : chains) {
        chain->resume();
    }

    std::lock_guard<std::mutex> lock(mutex);
    if (state == Paused)
        state = Running;
    request.notify_all();
}

void SmoothingEngine::reapFinishedChains()
{
    std::unordered_set<Chain *> reaped;
    for (auto check = chains.begin(); check != chains.end();) {
        if (!(*check)->checkCompleted()) {
            ++check;
            continue;
        }
        (*check)->cleanup();
        reaped.insert(check->get());
        check = chains.erase(check);
    }
    if (reaped.empty())
        return;
    for (auto check = primaryChains.begin(); check != primaryChains.end();) {
        if (!reaped.count(check->second)) {
            ++check;
            continue;
        }
        check = primaryChains.erase(check);
    }
}

void SmoothingEngine::installPrimaryChain(const Data::SequenceName &name, Chain *chain)
{
    Util::insert_or_assign(primaryChains, name, chain);
}

void SmoothingEngine::auxiliaryEndBefore()
{ }

void SmoothingEngine::auxiliaryEndAfter()
{ }

void SmoothingEngine::endAllChains()
{
    auxiliaryEndBefore();

    /* Two stage advance so the metadata gets values first, since this may
     * be the first time the chains actually see values. */
    for (const auto &target : dispatch) {
        if (!target.second->explictTimeAdvance)
            continue;
        target.second->endData();
    }
    for (const auto &target : dispatch) {
        if (target.second->explictTimeAdvance)
            continue;
        target.second->endData();
    }
    while (!dispatchNew.empty()) {
        std::vector<DispatchTarget *> process;
        for (const auto &add : dispatchNew) {
            process.emplace_back(add.second.get());
        }
        uniteNewDispatch();
        for (auto target : process) {
            if (!target->explictTimeAdvance)
                continue;
            target->endData();
        }
        for (auto target : process) {
            if (target->explictTimeAdvance)
                continue;
            target->endData();
        }
    }

    dispatch.clear();
    dispatchPendingDelete.clear();

    auxiliaryEndAfter();
    if (holdback) {
        holdback->endData();
        holdback = nullptr;
    }
    outputMux.sinkCreationComplete();
}

bool SmoothingEngine::canProcess(IncomingBuffer::const_iterator incomingBegin,
                                 IncomingBuffer::const_iterator incomingEnd,
                                 double advanceTime)
{
    if (incomingBegin == incomingEnd)
        return FP::defined(advanceTime);

    auto last = incomingEnd;
    --last;
    if (!FP::equal(incomingBegin->getStart(), last->getStart()))
        return true;
    if (FP::defined(advanceTime)) {
        if (!FP::defined(last->getStart()) || last->getStart() < advanceTime)
            return true;
    }
    return false;
}

void SmoothingEngine::startProcessing(IncomingBuffer::iterator incomingBegin,
                                      IncomingBuffer::iterator incomingEnd,
                                      double &advanceTime,
                                      double &holdbackTime)
{
    if (incomingBegin == incomingEnd) {
        holdbackTime = advanceTime;
        return;
    }
    if (!FP::defined(advanceTime))
        advanceTime = (incomingEnd - 1)->getStart();
    holdbackTime = advanceTime;
}

SmoothingEngine::DispatchTarget *SmoothingEngine::dispatchTarget(const Data::SequenceName &unit)
{
    auto target = dispatch.find(unit);
    if (target != dispatch.end())
        return target->second.get();
    target = dispatchNew.find(unit);
    if (target != dispatchNew.end())
        return target->second.get();

    if (unit.isDefaultStation()) {
        target = dispatchNew.emplace(unit, createDefaultDispatch(unit)).first;
    } else if (unit.isMeta()) {
        SequenceName defaultUnit(unit.toDefaultStation());
        auto defaultUnder = dispatch.find(defaultUnit);
        if (defaultUnder == dispatch.end() &&
                (defaultUnder = dispatchNew.find(defaultUnit)) == dispatchNew.end()) {
            defaultUnder =
                    dispatchNew.emplace(defaultUnit, createDefaultDispatch(defaultUnit)).first;
        }

        target = dispatchNew.emplace(unit, createMetaDispatch(unit, defaultUnder->second.get()))
                            .first;
    } else {
        SequenceName metaUnit = unit.toMeta();
        metaUnit.removeFlavor(SequenceName::flavor_cover);
        metaUnit.removeFlavor(SequenceName::flavor_stats);
        metaUnit.removeFlavor(SequenceName::flavor_end);

        auto meta = dispatch.find(metaUnit);
        if (meta == dispatch.end() && (meta = dispatchNew.find(metaUnit)) == dispatchNew.end()) {
            SequenceName defaultUnit(metaUnit.toDefaultStation());
            auto defaultUnder = dispatch.find(defaultUnit);
            if (defaultUnder == dispatch.end() &&
                    (defaultUnder = dispatchNew.find(defaultUnit)) == dispatchNew.end()) {
                defaultUnder =
                        dispatchNew.emplace(defaultUnit, createDefaultDispatch(defaultUnit)).first;
            }
            meta = dispatchNew.emplace(metaUnit,
                                       createMetaDispatch(metaUnit, defaultUnder->second.get()))
                              .first;
        }
        DispatchTarget *metaTarget = meta->second.get();

        SequenceName defaultUnit(unit.toDefaultStation());
        auto defaultUnder = dispatch.find(defaultUnit);
        if (defaultUnder == dispatch.end() &&
                (defaultUnder = dispatchNew.find(defaultUnit)) == dispatchNew.end()) {
            defaultUnder =
                    dispatchNew.emplace(defaultUnit, createDefaultDispatch(defaultUnit)).first;
        }

        target = dispatchNew.emplace(unit, createDataDispatch(unit, defaultUnder->second.get(),
                                                              metaTarget)).first;
    }
    return target->second.get();
}

void SmoothingEngine::replaceDispatch(const Data::SequenceName &unit,
                                      std::unique_ptr<DispatchTarget> &&replace)
{
    auto target = dispatch.find(unit);
    if (target == dispatch.end()) {
        target = dispatchNew.find(unit);
        Q_ASSERT(target != dispatchNew.end());
    }
    dispatchPendingDelete.emplace_back(std::move(target->second));
    target->second = std::move(replace);
}

SinkMultiplexer::Sink *SmoothingEngine::createDataIngress(const SequenceName &unit)
{
    Q_UNUSED(unit);
    return createOutputSegmented();
}

SmootherChainCore::HandlerMode SmoothingEngine::handlerMode(const SequenceName &,
                                                            SmootherChainCore::Type type)
{
    Q_ASSERT(core);
    return core->smootherHandler(type);
}

bool SmoothingEngine::handleFlags(const SequenceName &)
{
    Q_ASSERT(core);
    return core->flagsHandler();
}

bool SmoothingEngine::generatedOutput(const SequenceName &unit)
{
    Q_ASSERT(core);
    return core->generatedOutput(unit);
}

bool SmoothingEngine::alwaysBypassOutput(const SequenceName &)
{ return false; }

QSet<double> SmoothingEngine::getMetadataBreaks(const SequenceName &)
{
    Q_ASSERT(core);
    return core->getProcessingMetadataBreaks();
}

void SmoothingEngine::transformMetadata(SmootherChainCore *core,
                                        DispatchMetadata *dispatch,
                                        const SequenceName &unit,
                                        double time, Variant::Write &metadata)
{
    switch (handlerMode(unit.fromMeta(), dispatch->chainType)) {
    case SmootherChainCore::Repeatable:
        switch (dispatch->chainType) {
        case SmootherChainCore::General:
        case SmootherChainCore::Vector2D:
        case SmootherChainCore::Vector3D:
        case SmootherChainCore::Dewpoint:
        case SmootherChainCore::RH:
        case SmootherChainCore::RHExtrapolate:
        case SmootherChainCore::Difference:
        case SmootherChainCore::BeersLawAbsorption:
            break;

        case SmootherChainCore::DifferenceInitial: {
            auto smoothing = metadata.metadata("Smoothing");
            if (!smoothing.exists() &&
                    (metadata.getType() == Variant::Type::MetadataArray ||
                            metadata.getType() == Variant::Type::MetadataMatrix)) {
                smoothing = metadata.metadataArray("Children").metadata("Smoothing");
            }
            smoothing.hash("Mode").setString("Difference");
            break;
        }
        case SmootherChainCore::BeersLawAbsorptionInitial: {
            auto smoothing = metadata.metadata("Smoothing");
            if (!smoothing.exists() &&
                    (metadata.getType() == Variant::Type::MetadataArray ||
                            metadata.getType() == Variant::Type::MetadataMatrix)) {
                smoothing = metadata.metadataArray("Children").metadata("Smoothing");
            }
            smoothing.hash("Mode").setString("BeersLawAbsorption");
            break;
        }
        }
        break;

    case SmootherChainCore::Handled:
    case SmootherChainCore::Unhandled:
        metadata.metadata("Smoothing").remove();
        if (metadata.getType() == Variant::Type::MetadataArray ||
                metadata.getType() == Variant::Type::MetadataMatrix) {
            metadata.metadataArray("Children").metadata("Smoothing").remove(false);
        }
        break;
    }

    Variant::Write processing;
    if (dispatch->smoothingEnable) {
        processing = core->getProcessingMetadata(time).write();
    } else {
        processing.detachFromRoot();
        processing.hash("Type").setString("Disabled");
    }
    processing.hash("By").setString("smoothing_engine");
    processing.hash("At").setDouble(Time::time());
    processing.hash("Environment").setString(Environment::describe());
    processing.hash("Revision").setString(Environment::revision());
    metadata.metadata("Processing").toArray().after_back().set(processing);
}

void SmoothingEngine::transformMetadata(SmootherChainCore *core, const SequenceName &unit,
                                        double time, Variant::Write &metadata)
{
    Q_ASSERT(core);

    transformMetadata(core, static_cast<DispatchMetadata *>(dispatchTarget(unit)), unit, time,
                      metadata);
}

void SmoothingEngine::transformMetadata(const SequenceName &unit,
                                        double time,
                                        Variant::Write &metadata)
{
    Q_ASSERT(core);
    transformMetadata(core.get(), unit, time, metadata);
}

void SmoothingEngine::predictMetadataBounds(double *, double *)
{ }

void SmoothingEngine::constructChain(const SequenceName &triggeringUnit,
                                     EngineInterface *interface,
                                     SmootherChainCore::Type type)
{
    Q_ASSERT(core);
    Q_UNUSED(triggeringUnit);
    core->createSmoother(interface, type);
}

void SmoothingEngine::deserializeChain(const std::vector<SequenceName> &,
                                       QDataStream &stream,
                                       EngineInterface *interface,
                                       SmootherChainCore::Type type)
{
    Q_ASSERT(core);
    core->deserializeSmoother(stream, interface, type);
}

void SmoothingEngine::constructFlagsChain(const SequenceName &, EngineInterface *interface)
{
    Q_ASSERT(core);
    core->createFlags(interface);
}

void SmoothingEngine::deserializeFlagsChain(const std::vector<SequenceName> &,
                                            QDataStream &stream,
                                            EngineInterface *interface)
{
    Q_ASSERT(core);
    core->deserializeFlags(stream, interface);
}

std::unique_ptr<SmoothingEngine::EngineInterface> SmoothingEngine::createEngineInterface()
{ return std::unique_ptr<SmoothingEngine::EngineInterface>(new EngineInterface(this)); }

void SmoothingEngine::auxiliaryAdvance(double)
{ }

bool SmoothingEngine::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, mutex, response,
                                     [this] { return state == Completed; });
}

bool SmoothingEngine::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == Completed;
}

void SmoothingEngine::start()
{
    outputMux.start();

    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(state == NotStarted);
        state = Running;
    }
    thread = std::thread(std::bind(&SmoothingEngine::run, this));
}

void SmoothingEngine::run()
{
    double advanceTime = FP::undefined();
    double holdbackTime = FP::undefined();
    double effectiveAdvance = FP::undefined();
    IncomingBuffer::iterator incomingBegin;
    IncomingBuffer::iterator incomingEnd;

    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        bool finishData = false;
        bool wasStalled = false;

        if (!lock)
            lock.lock();
        switch (state) {
        case NotStarted:
        case Completed:
            return;
        case Paused:
            request.wait(lock);
            continue;

        case PauseRequested:
            /* Put any remaining incoming back into the pending
             * so it can be serialized. */
            if (!incoming.empty()) {
                Util::append(std::move(pending), incoming);
                pending = std::move(incoming);
                incoming.clear();
            }

            state = Paused;
            response.notify_all();
            continue;

        case Terminated:
            lock.unlock();

            for (const auto &chain : chains) {
                chain->signalTerminate();
            }

            endAllChains();
            outputMux.signalTerminate(true);
            outputMux.wait();

            lock.lock();
            state = Completed;
            lock.unlock();
            response.notify_all();
            finished();
            return;

        case FinalizeTerminated:
            lock.unlock();

            /* Same as above but the chains are already ended */
            outputMux.signalTerminate(true);
            outputMux.wait();

            lock.lock();
            dataEnded = true;
            state = Completed;
            lock.unlock();
            response.notify_all();
            finished();
            return;

        case Finalizing:
            lock.unlock();

            /* Have to do this blocking, since we can't know exactly when the mux
             * terminates and we have no way of re-waking the thread */
            if (!outputMux.wait(0.25)) {
                continue;
            }

            lock.lock();
            if (state != Finalizing)
                continue;

            dispatchPendingDelete.clear();
            dispatch.clear();

            for (const auto &cleanup : chains) {
                cleanup->cleanup();
            }
            chains.clear();
            primaryChains.clear();

            dataEnded = true;
            state = Completed;
            lock.unlock();
            response.notify_all();
            finished();
            return;

        case Running:
            if (!dataEnded && !canProcess(pending.cbegin(), pending.cend(), dataAdvance)) {
                request.wait(lock);
                continue;
            }

            if (!pending.empty()) {
                wasStalled = pending.size() > stallThreshold;
                Util::append(std::move(pending), incoming);
                pending.clear();
            }
            finishData = dataEnded;
            advanceTime = dataAdvance;
            dataAdvance = FP::undefined();
            break;
        }
        lock.unlock();

        if (wasStalled)
            response.notify_all();

        static constexpr std::size_t processChunkSize = 1024;
        std::size_t incomingRemaining = incoming.size();
        incomingBegin = incoming.begin();
        bool processingAccepted = false;
        std::size_t chunkAdvanceSize = processChunkSize;
        for (;; chunkAdvanceSize *= 2) {
            if (chunkAdvanceSize >= incomingRemaining) {
                incomingEnd = incoming.end();
                chunkAdvanceSize = incomingRemaining;
                effectiveAdvance = advanceTime;
                processingAccepted = canProcess(incomingBegin, incomingEnd, advanceTime);
                break;
            }
            incomingEnd = incomingBegin + chunkAdvanceSize;

            effectiveAdvance = FP::undefined();
            if (finishData)
                break;

            if (canProcess(incomingBegin, incomingEnd, advanceTime)) {
                processingAccepted = true;
                break;
            }
        }
        while (processingAccepted || finishData) {
            startProcessing(incomingBegin, incomingEnd, effectiveAdvance, holdbackTime);

            bool checkTimeAdvance = false;
            for (auto addIncoming = incomingBegin, endAddIncoming = incomingEnd;
                    addIncoming != endAddIncoming;
                    ++addIncoming) {
                if (checkTimeAdvance && !FP::equal(advanceStartTime, addIncoming->getStart())) {
                    advanceStartTime = addIncoming->getStart();
                    /* Have to do this in two stages so the metadata gets the
                     * advance first.  Otherwise it might not have gotten the
                     * latest change updated before a chain gets constructed. */
                    for (const auto &target : dispatch) {
                        if (!target.second->explictTimeAdvance)
                            continue;
                        target.second->incomingAdvance(advanceStartTime);
                    }
                    for (const auto &target : dispatch) {
                        if (target.second->explictTimeAdvance)
                            continue;
                        target.second->incomingAdvance(advanceStartTime);
                    }
                    while (!dispatchNew.empty()) {
                        std::vector<DispatchTarget *> process;
                        for (const auto &add : dispatchNew) {
                            process.emplace_back(add.second.get());
                        }
                        uniteNewDispatch();
                        for (auto target : process) {
                            target->incomingAdvance(advanceStartTime);
                        }
                    }

                    checkTimeAdvance = false;
                }

                DispatchTarget *lookupTarget = dispatchTarget(addIncoming->getUnit());
                if (!checkTimeAdvance) {
                    if (lookupTarget->explictTimeAdvance) {
                        advanceStartTime = addIncoming->getStart();
                        checkTimeAdvance = true;
                    }
                }
                lookupTarget->incomingData(std::move(*addIncoming));
                uniteNewDispatch();
            }

            if (checkTimeAdvance && !FP::defined(effectiveAdvance))
                effectiveAdvance = advanceStartTime;
            if (FP::defined(effectiveAdvance)) {
                /* Again this has to be two stage so metadata is updated first */
                for (const auto &target : dispatch) {
                    if (!target.second->explictTimeAdvance)
                        continue;
                    target.second->incomingAdvance(effectiveAdvance);
                }
                for (const auto &target : dispatch) {
                    if (target.second->explictTimeAdvance)
                        continue;
                    target.second->incomingAdvance(effectiveAdvance);
                }
                while (!dispatchNew.empty()) {
                    std::vector<DispatchTarget *> process;
                    for (const auto &add : dispatchNew) {
                        process.emplace_back(add.second.get());
                    }
                    uniteNewDispatch();
                    for (auto target : process) {
                        target->incomingAdvance(effectiveAdvance);
                    }
                }

                auxiliaryAdvance(effectiveAdvance);
            }
            if (FP::defined(holdbackTime)) {
                /* Don't allow the holdback to exceed how far we've actually
                 * gotten data for.  This is so the chains can construct
                 * bypasses for data that transitions to unaveragable
                 * (e.x. averaged and binned to unaveraged and thus starting
                 * before the bin end time) */
                if (FP::defined(advanceTime) && holdbackTime > advanceTime)
                    holdbackTime = advanceTime;
                for (const auto &target : dispatch) {
                    double m = target.second->nextPossibleTransition();
                    if (!FP::defined(m))
                        continue;
                    if (m < holdbackTime)
                        holdbackTime = m;
                }

                holdback->incomingAdvance(holdbackTime);
                holdbackTime = FP::undefined();
            }

            dispatchPendingDelete.clear();

            incomingRemaining -= chunkAdvanceSize;
            incomingBegin = incomingEnd;
            if (incomingRemaining <= 0)
                break;

            processingAccepted = false;
            for (chunkAdvanceSize = processChunkSize;; chunkAdvanceSize *= 2) {
                if (chunkAdvanceSize >= incomingRemaining) {
                    incomingEnd = incoming.end();
                    chunkAdvanceSize = incomingRemaining;
                    effectiveAdvance = advanceTime;
                    processingAccepted = canProcess(incomingBegin, incomingEnd, advanceTime);
                    break;
                }
                incomingEnd = incomingBegin + chunkAdvanceSize;

                effectiveAdvance = FP::undefined();
                if (finishData)
                    break;

                if (canProcess(incomingBegin, incomingEnd, advanceTime)) {
                    processingAccepted = true;
                    break;
                }
            }
        }

        if (finishData) {
            endAllChains();

            outputMux.finished.connect(std::bind(&SmoothingEngine::outputMuxFinished, this));
            lock.lock();
            if (state == Terminated)
                state = FinalizeTerminated;
            else
                state = Finalizing;
        } else {
            if (incomingBegin == incoming.end()) {
                incoming.clear();
            } else {
                incoming.erase(incoming.begin(), incomingBegin);
            }
        }
    }
}

void SmoothingEngine::outputMuxFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (state != Finalizing && state != FinalizeTerminated)
        return;
    request.notify_all();
}

std::unique_ptr<
        SmoothingEngine::DispatchTarget> SmoothingEngine::createDefaultDispatch(const SequenceName &)
{
    return std::unique_ptr<DispatchTarget>(new DispatchDefaultUnderlay);
}

std::unique_ptr<
        SmoothingEngine::DispatchTarget> SmoothingEngine::createMetaDispatch(const Data::SequenceName &unit,
                                                                             DispatchTarget *underlay)
{
    auto meta = new DispatchMetadata(this, unit, createOutputIngress());
    auto defaultUnderlay = static_cast<DispatchDefaultUnderlay *>(underlay);
    defaultUnderlay->addForwardTarget(meta);
    meta->setBreaks(getMetadataBreaks(unit));
    meta->overlaySegmenter(defaultUnderlay->getSegmenter());
    return std::unique_ptr<SmoothingEngine::DispatchTarget>(meta);
}

std::unique_ptr<
        SmoothingEngine::DispatchTarget> SmoothingEngine::createDataDispatch(const Data::SequenceName &unit,
                                                                             DispatchTarget *underlay,
                                                                             DispatchTarget *meta)
{
    Q_UNUSED(meta);
    auto data = new DispatchData(this, unit);
    auto defaultUnderlay = static_cast<DispatchDefaultUnderlay *>(underlay);
    defaultUnderlay->addForwardTarget(data);
    data->overlaySegmenter(defaultUnderlay->getSegmenter());
    return std::unique_ptr<SmoothingEngine::DispatchTarget>(data);
}

SmoothingEngine::DispatchMetadata *SmoothingEngine::getUnitMetadataDispatch(const SequenceName &unit)
{
    Q_ASSERT(!unit.isMeta());
    SequenceName metaUnit(unit.toMeta());
    metaUnit.removeFlavor(SequenceName::flavor_cover);
    metaUnit.removeFlavor(SequenceName::flavor_stats);
    metaUnit.removeFlavor(SequenceName::flavor_end);
    DispatchTarget *dtBase = dispatchTarget(metaUnit);
    Q_ASSERT(dtBase->getType() == SmoothingEngine::Type_Metadata);
    auto d = static_cast<DispatchMetadata *>(dtBase);
    return d;
}

SmoothingEngine::DispatchTarget::DispatchTarget() : explictTimeAdvance(false)
{ }

SmoothingEngine::DispatchTarget::~DispatchTarget() = default;

SmoothingEngine::DispatchDefaultUnderlay::DispatchDefaultUnderlay() : segmenter(), forward()
{ }

SmoothingEngine::DispatchDefaultUnderlay::~DispatchDefaultUnderlay() = default;

void SmoothingEngine::DispatchDefaultUnderlay::incomingData(Data::SequenceValue &&value)
{
    segmenter.add(value);
    for (auto f : forward) {
        f->incomingData(SequenceValue(value));
    }
}

void SmoothingEngine::DispatchDefaultUnderlay::incomingAdvance(double time)
{ segmenter.completedAdvance(time); }

void SmoothingEngine::DispatchDefaultUnderlay::endData()
{ }

double SmoothingEngine::DispatchDefaultUnderlay::nextPossibleTransition() const
{ return segmenter.nextTransitionTime(); }

int SmoothingEngine::DispatchDefaultUnderlay::getType() const
{ return SmoothingEngine::Type_DefaultUnderlay; }

void SmoothingEngine::DispatchDefaultUnderlay::addForwardTarget(DispatchTarget *target)
{ forward.push_back(target); }

void SmoothingEngine::DispatchDefaultUnderlay::removeForwardTarget(DispatchTarget *target)
{
    for (auto check = forward.begin(); check != forward.end();) {
        if (*check == target) {
            check = forward.erase(check);
        } else {
            --check;
        }
    }
}

void SmoothingEngine::DispatchDefaultUnderlay::serialize(QDataStream &stream) const
{ stream << segmenter; }

void SmoothingEngine::DispatchDefaultUnderlay::deserialize(QDataStream &stream)
{ stream >> segmenter; }

SmoothingEngine::DispatchBypass::DispatchBypass(Data::SinkMultiplexer::Sink *t)
        : bypassTarget(t)
{ }

SmoothingEngine::DispatchBypass::~DispatchBypass() = default;

void SmoothingEngine::DispatchBypass::incomingData(Data::SequenceValue &&value)
{ bypassTarget->incomingData(std::move(value)); }

void SmoothingEngine::DispatchBypass::incomingAdvance(double)
{ }

void SmoothingEngine::DispatchBypass::endData()
{ }

double SmoothingEngine::DispatchBypass::nextPossibleTransition() const
{ return FP::undefined(); }

int SmoothingEngine::DispatchBypass::getType() const
{ return SmoothingEngine::Type_Bypass; }

void SmoothingEngine::DispatchBypass::serialize(QDataStream &) const
{ }

void SmoothingEngine::DispatchBypass::deserialize(QDataStream &)
{ }

SmoothingEngine::DispatchDiscard::DispatchDiscard() = default;

SmoothingEngine::DispatchDiscard::~DispatchDiscard() = default;

void SmoothingEngine::DispatchDiscard::incomingData(Data::SequenceValue &&)
{ }

void SmoothingEngine::DispatchDiscard::incomingAdvance(double)
{ }

void SmoothingEngine::DispatchDiscard::endData()
{ }

double SmoothingEngine::DispatchDiscard::nextPossibleTransition() const
{ return FP::undefined(); }

int SmoothingEngine::DispatchDiscard::getType() const
{ return SmoothingEngine::Type_Discard; }

void SmoothingEngine::DispatchDiscard::serialize(QDataStream &) const
{ }

void SmoothingEngine::DispatchDiscard::deserialize(QDataStream &)
{ }


SmoothingEngine::DispatchMetadata::DispatchMetadata(SmoothingEngine *engine,
                                                    const SequenceName &setUnit,
                                                    SinkMultiplexer::Sink *target)
        : parent(engine), unit(setUnit), startTimeLimit(FP::undefined()),
          smoothingEnable(true),
          chainType(SmootherChainCore::General), chainParameters(Variant::Read::empty()),
          outputTarget(target),
          segmenter()
{
    /* So we get an advance call at every time advance, so we can do a chain
     * rebuild check there instead of at every incoming value */
    explictTimeAdvance = true;
}

SmoothingEngine::DispatchMetadata::~DispatchMetadata() = default;

void SmoothingEngine::DispatchMetadata::rebuildChain()
{
    DispatchTarget *dtBase = parent->dispatchTarget(unit.fromMeta());
    Q_ASSERT(dtBase->getType() == SmoothingEngine::Type_Value);
    auto d = static_cast<DispatchData *>(dtBase);
    /* This will force a rebuild the next time it sees a value */
    d->chainMode = DispatchData::Undefined;
}

void SmoothingEngine::DispatchMetadata::rebuildChainIfNeeded(const Variant::Read &value)
{
    auto smoothing = value.metadata("Smoothing");
    if (!smoothing.exists()) {
        switch (value.getType()) {
        case Variant::Type::Array:
        case Variant::Type::Matrix:
            smoothing = value.metadata("Children").metadata("Smoothing");
            break;
        default:
            break;
        }
    }
    auto mode = smoothing.hash("Mode").toString();

    SmootherChainCore::Type previousType = chainType;
    auto previousParameters = chainParameters;
    chainParameters = smoothing.hash("Parameters");
    chainParameters.detachFromRoot();

    if (mode.empty())
        chainType = SmootherChainCore::General;
    else if (Util::equal_insensitive(mode, "Difference"))
        chainType = SmootherChainCore::Difference;
    else if (Util::equal_insensitive(mode, "DifferenceInitial"))
        chainType = SmootherChainCore::DifferenceInitial;
    else if (Util::equal_insensitive(mode, "Vector2D", "Vector"))
        chainType = SmootherChainCore::Vector2D;
    else if (Util::equal_insensitive(mode, "Vector3D"))
        chainType = SmootherChainCore::Vector3D;
    else if (Util::equal_insensitive(mode, "BeersLawAbsorption", "BeersLaw"))
        chainType = SmootherChainCore::BeersLawAbsorption;
    else if (Util::equal_insensitive(mode, "BeersLawAbsorptionInitial", "BeersLawInitial"))
        chainType = SmootherChainCore::BeersLawAbsorptionInitial;
    else if (Util::equal_insensitive(mode, "Dewpoint", "TD"))
        chainType = SmootherChainCore::Dewpoint;
    else if (Util::equal_insensitive(mode, "RH", "RelativeHumidity"))
        chainType = SmootherChainCore::RH;
    else if (Util::equal_insensitive(mode, "RHExtrapolate", "RelativeHumidityExtrapolate"))
        chainType = SmootherChainCore::RHExtrapolate;
    else if (Util::equal_insensitive(mode, "Bypass", "None")) {
        chainType = SmootherChainCore::General;
        if (smoothingEnable) {
            smoothingEnable = false;
            rebuildChain();
        }
        return;
    } else
        chainType = SmootherChainCore::General;

    if (parent->handlerMode(unit.fromMeta(), chainType) == SmootherChainCore::Unhandled) {
        chainType = SmootherChainCore::General;
    }

    if (!smoothingEnable) {
        smoothingEnable = true;
        rebuildChain();
        return;
    }

    if (chainType != previousType) {
        rebuildChain();
        return;
    }

    switch (chainType) {
    case SmootherChainCore::General:
    case SmootherChainCore::Difference:
    case SmootherChainCore::DifferenceInitial:
        break;

    case SmootherChainCore::Vector2D:
        if (chainParameters.hash("Direction").toString() !=
                previousParameters.hash("Direction").toString() ||
                chainParameters.hash("Magnitude").toString() !=
                        previousParameters.hash("Magnitude").toString()) {
            rebuildChain();
            return;
        }
        break;

    case SmootherChainCore::Vector3D:
        if (chainParameters.hash("Azimuth").toString() !=
                previousParameters.hash("Azimuth").toString() ||
                chainParameters.hash("Elevation").toString() !=
                        previousParameters.hash("Elevation").toString() ||
                chainParameters.hash("Magnitude").toString() !=
                        previousParameters.hash("Magnitude").toString()) {
            rebuildChain();
            return;
        }
        break;

    case SmootherChainCore::BeersLawAbsorption:
    case SmootherChainCore::BeersLawAbsorptionInitial:
        if (chainParameters.hash("L").toString() != previousParameters.hash("L").toString()) {
            rebuildChain();
            return;
        }
        if (chainParameters.hash("Transmittance").exists() &&
                (!previousParameters.hash("Transmittance").exists() ||
                        chainParameters.hash("Transmittance").toString() !=
                                previousParameters.hash("Transmittance").toString())) {
            rebuildChain();
            return;
        }
        if (!chainParameters.hash("Transmittance").exists() &&
                previousParameters.hash("Transmittance").exists()) {
            rebuildChain();
            return;
        }
        if (chainParameters.hash("I").exists() && (!previousParameters.hash("I").exists() ||
                        chainParameters.hash("I").toString() !=
                                previousParameters.hash("I").toString())) {
            rebuildChain();
            return;
        }
        if (!chainParameters.hash("I").exists() && previousParameters.hash("I").exists()) {
            rebuildChain();
            return;
        }
        break;

    case SmootherChainCore::Dewpoint:
        if (chainParameters.hash("T").toString() != previousParameters.hash("T").toString() ||
                chainParameters.hash("RH").toString() != previousParameters.hash("RH").toString() ||
                chainParameters.hash("AlwaysWater").toBool() !=
                        previousParameters.hash("AlwaysWater").toBool()) {
            rebuildChain();
            return;
        }
        break;

    case SmootherChainCore::RH:
        if (chainParameters.hash("T").toString() != previousParameters.hash("T").toString() ||
                chainParameters.hash("AlwaysWater").toBool() !=
                        previousParameters.hash("AlwaysWater").toBool()) {
            rebuildChain();
            return;
        }
        if (chainParameters.hash("Dewpoint").exists() &&
                (!previousParameters.hash("Dewpoint").exists() ||
                        chainParameters.hash("Dewpoint").toString() !=
                                previousParameters.hash("Dewpoint").toString())) {
            rebuildChain();
            return;
        }
        if (!chainParameters.hash("Dewpoint").exists() &&
                previousParameters.hash("Dewpoint").exists()) {
            rebuildChain();
            return;
        }
        if (chainParameters.hash("TD").exists() && (!previousParameters.hash("TD").exists() ||
                        chainParameters.hash("TD").toString() !=
                                previousParameters.hash("TD").toString())) {
            rebuildChain();
            return;
        }
        if (!chainParameters.hash("TD").exists() && previousParameters.hash("TD").exists()) {
            rebuildChain();
            return;
        }
        break;

    case SmootherChainCore::RHExtrapolate:
        if (chainParameters.hash("TIn").toString() != previousParameters.hash("Tin").toString() ||
                chainParameters.hash("Tout").toString() !=
                        previousParameters.hash("Tout").toString() ||
                chainParameters.hash("RHin").toString() !=
                        previousParameters.hash("RHin").toString() ||
                chainParameters.hash("AlwaysWater").toBool() !=
                        previousParameters.hash("AlwaysWater").toBool()) {
            rebuildChain();
            return;
        }
        break;
    }
}

bool SmoothingEngine::DispatchMetadata::predictSegmentBounds(double &start, double &end)
{
    if (smoothingEnable) {
        parent->predictMetadataBounds(&start, &end);
        if (FP::defined(start) && FP::defined(startTimeLimit) && start < startTimeLimit)
            start = startTimeLimit;
    }
    startTimeLimit = start;

    if (!FP::defined(start) || !FP::defined(end))
        return true;
    return start < end;
}

void SmoothingEngine::DispatchMetadata::incomingSegment(ValueSegment &&segment)
{
    rebuildChainIfNeeded(segment.read());

    auto root = std::move(segment.root());
    double start = segment.getStart();
    double end = segment.getEnd();
    {
        auto metadata = root.write();
        parent->transformMetadata(unit, start, metadata);
    }
    if (!predictSegmentBounds(start, end))
        return;
    outputTarget->incomingData(SequenceValue({unit, start, end}, std::move(root)));
}

void SmoothingEngine::DispatchMetadata::incomingData(SequenceValue &&value)
{
    for (auto &add : segmenter.add(std::move(value))) {
        incomingSegment(std::move(add));
    }
}

void SmoothingEngine::DispatchMetadata::advancePredicted(double time)
{
    if (smoothingEnable) {
        parent->predictMetadataBounds(&time, nullptr);
        if (!FP::defined(startTimeLimit) || time > startTimeLimit) {
            startTimeLimit = time;
        } else {
            time = startTimeLimit;
        }
    } else {
        startTimeLimit = time;
    }

    outputTarget->incomingAdvance(time);
}

void SmoothingEngine::DispatchMetadata::incomingAdvance(double time)
{
    for (auto &add : segmenter.advance(time)) {
        incomingSegment(std::move(add));
    }
    advancePredicted(time);
}

void SmoothingEngine::DispatchMetadata::endData()
{
    for (auto &add : segmenter.finish()) {
        incomingSegment(std::move(add));
    }
    outputTarget->endData();
}

double SmoothingEngine::DispatchMetadata::nextPossibleTransition() const
{ return segmenter.nextTransitionTime(); }

int SmoothingEngine::DispatchMetadata::getType() const
{ return SmoothingEngine::Type_Metadata; }

void SmoothingEngine::DispatchMetadata::serialize(QDataStream &stream) const
{
    stream << segmenter << smoothingEnable << startTimeLimit << static_cast<quint8>(chainType)
           << chainParameters;
}

void SmoothingEngine::DispatchMetadata::deserialize(QDataStream &stream)
{
    stream >> segmenter >> smoothingEnable >> startTimeLimit;
    {
        quint8 i8 = 0;
        stream >> i8;
        chainType = static_cast<SmootherChainCore::Type>(i8);
    }
    stream >> chainParameters;
}


SmoothingEngine::DispatchData::DispatchData(SmoothingEngine *engine, const SequenceName &setUnit)
        : parent(engine),
          unit(setUnit),
          segmenter(),
          ended(false),
          lastSegments(),
          targetsBasic(),
          targetsFlags(),
          targetsGeneral(),
          targetsArray(),
          chainMode(Undefined),
          matrixSize(), directBypass(nullptr),
          pendingBypassDisable(false), initialTimeHoldback(nullptr)
{
    /* Create a byass so that we reserve our time in the multiplexer, this
     * will generally be destroyed as soon as we see a value but that might
     * not be until after the multiplexer advances. */
    directBypass = parent->createOutputSegmented();
}

SmoothingEngine::DispatchData::~DispatchData() = default;

void SmoothingEngine::DispatchData::destroyChain()
{
    auto chainFind = parent->primaryChains.find(unit);
    if (chainFind == parent->primaryChains.end())
        return;

    auto chain = chainFind->second;
    parent->primaryChains.erase(chainFind);

    auto dtBase = parent->dispatchTarget(unit);
    Q_ASSERT(dtBase->getType() == SmoothingEngine::Type_Value);
    chain->unregisterUnit(static_cast<DispatchData *>(dtBase), unit);

    while (!targetsArray.empty() && targetsArray.back().empty()) {
        targetsArray.pop_back();
    }

    bool orphanedChain = true;
    for (const auto &check : parent->primaryChains) {
        if (check.second != chain)
            continue;
        orphanedChain = false;
        break;
    }
    if (orphanedChain) {
        for (const auto &rm : chain->remainingReferencedUnits()) {
            auto check = parent->dispatch.find(rm);
            if (check == parent->dispatch.end()) {
                /* Catch if the dispatch hasn't been merged into the main
                 * yet */
                check = parent->dispatchNew.find(rm);
                if (check == parent->dispatchNew.end())
                    continue;
            }
            Q_ASSERT(check->second->getType() == SmoothingEngine::Type_Value);
            auto dd = static_cast<DispatchData *>(check->second.get());
            /* Can happen on final-value chain conversion, so just ignore
             * the duplicate end */
            if (dd->ended)
                continue;
            chain->unregisterUnit(dd, rm);
        }

        Q_ASSERT(chain->checkCompleted());
    }

    /* If a primary chain already existed for this unit then reap the chain
     * list in case it's now completed entirely. */
    parent->reapFinishedChains();
}

bool SmoothingEngine::DispatchData::checkMultiplePrimary(SmoothingEngine::DispatchMetadata *meta)
{
    if (!meta)
        return false;
    if (!meta->smoothingEnable)
        return false;

    switch (meta->chainType) {
    case SmootherChainCore::General:
    case SmootherChainCore::Dewpoint:
    case SmootherChainCore::RH:
    case SmootherChainCore::RHExtrapolate:
    case SmootherChainCore::BeersLawAbsorption:
    case SmootherChainCore::BeersLawAbsorptionInitial:
        /* Single primary input, so don't need to check for an alternate */
        break;

    case SmootherChainCore::Difference:
    case SmootherChainCore::DifferenceInitial: {
        /* No metadata for the end flavor, so always combine with the other
         * if it was registered first */
        SequenceName u = unit;
        if (u.hasFlavor(SequenceName::flavor_end)) {
            u.removeFlavor(SequenceName::flavor_end);
        } else {
            u.addFlavor(SequenceName::flavor_end);
        }

        auto check = parent->primaryChains.find(u);
        if (check != parent->primaryChains.end()) {
            parent->installPrimaryChain(unit, check->second);
            return true;
        }
        break;
    }

    case SmootherChainCore::Vector2D: {
        SequenceName u(unit);
        u.setVariable(meta->chainParameters.hash("Direction").toString());
        if (u != unit) {
            SmoothingEngine::DispatchMetadata *otherMeta = parent->getUnitMetadataDispatch(u);
            DispatchTarget *otherDBase = parent->dispatchTarget(u);
            Q_ASSERT(otherDBase->getType() == SmoothingEngine::Type_Value);
            auto otherD = static_cast<DispatchData *>(otherDBase);
            if (otherMeta->chainType == SmootherChainCore::Vector2D &&
                    otherD->chainMode == chainMode &&
                    otherMeta->chainParameters.hash("Direction").toString() ==
                            meta->chainParameters.hash("Direction").toString() &&
                    otherMeta->chainParameters.hash("Magnitude").toString() ==
                            meta->chainParameters.hash("Magnitude").toString()) {
                auto check = parent->primaryChains.find(u);
                if (check != parent->primaryChains.end()) {
                    parent->installPrimaryChain(unit, check->second);
                    return true;
                }
            }
        }

        u.setVariable(meta->chainParameters.hash("Magnitude").toString());
        if (u != unit) {
            SmoothingEngine::DispatchMetadata *otherMeta = parent->getUnitMetadataDispatch(u);
            DispatchTarget *otherDBase = parent->dispatchTarget(u);
            Q_ASSERT(otherDBase->getType() == SmoothingEngine::Type_Value);
            auto otherD = static_cast<DispatchData *>(otherDBase);
            if (otherMeta->chainType == SmootherChainCore::Vector2D &&
                    otherD->chainMode == chainMode &&
                    otherMeta->chainParameters.hash("Direction").toString() ==
                            meta->chainParameters.hash("Direction").toString() &&
                    otherMeta->chainParameters.hash("Magnitude").toString() ==
                            meta->chainParameters.hash("Magnitude").toString()) {
                auto check = parent->primaryChains.find(u);
                if (check != parent->primaryChains.end()) {
                    parent->installPrimaryChain(unit, check->second);
                    return true;
                }
            }
        }

        break;
    }

    case SmootherChainCore::Vector3D: {
        SequenceName u(unit);
        u.setVariable(meta->chainParameters.hash("Azimuth").toString());
        if (u != unit) {
            SmoothingEngine::DispatchMetadata *otherMeta = parent->getUnitMetadataDispatch(u);
            DispatchTarget *otherDBase = parent->dispatchTarget(u);
            Q_ASSERT(otherDBase->getType() == SmoothingEngine::Type_Value);
            auto otherD = static_cast<DispatchData *>(otherDBase);
            if (otherMeta->chainType == SmootherChainCore::Vector3D &&
                    otherD->chainMode == chainMode &&
                    otherMeta->chainParameters.hash("Azimuth").toString() ==
                            meta->chainParameters.hash("Azimuth").toString() &&
                    otherMeta->chainParameters.hash("Elevation").toString() ==
                            meta->chainParameters.hash("Elevation").toString() &&
                    otherMeta->chainParameters.hash("Magnitude").toString() ==
                            meta->chainParameters.hash("Magnitude").toString()) {
                auto check = parent->primaryChains.find(u);
                if (check != parent->primaryChains.end()) {
                    parent->installPrimaryChain(unit, check->second);
                    return true;
                }
            }
        }

        u.setVariable(meta->chainParameters.hash("Elevation").toString());
        if (u != unit) {
            SmoothingEngine::DispatchMetadata *otherMeta = parent->getUnitMetadataDispatch(u);
            DispatchTarget *otherDBase = parent->dispatchTarget(u);
            Q_ASSERT(otherDBase->getType() == SmoothingEngine::Type_Value);
            auto otherD = static_cast<DispatchData *>(otherDBase);
            if (otherMeta->chainType == SmootherChainCore::Vector3D &&
                    otherD->chainMode == chainMode &&
                    otherMeta->chainParameters.hash("Azimuth").toString() ==
                            meta->chainParameters.hash("Azimuth").toString() &&
                    otherMeta->chainParameters.hash("Elevation").toString() ==
                            meta->chainParameters.hash("Elevation").toString() &&
                    otherMeta->chainParameters.hash("Magnitude").toString() ==
                            meta->chainParameters.hash("Magnitude").toString()) {
                auto check = parent->primaryChains.find(u);
                if (check != parent->primaryChains.end()) {
                    parent->installPrimaryChain(unit, check->second);
                    return true;
                }
            }
        }

        u.setVariable(meta->chainParameters.hash("Magnitude").toString());
        if (u != unit) {
            SmoothingEngine::DispatchMetadata *otherMeta = parent->getUnitMetadataDispatch(u);
            DispatchTarget *otherDBase = parent->dispatchTarget(u);
            Q_ASSERT(otherDBase->getType() == SmoothingEngine::Type_Value);
            auto otherD = static_cast<DispatchData *>(otherDBase);
            if (otherMeta->chainType == SmootherChainCore::Vector3D &&
                    otherD->chainMode == chainMode &&
                    otherMeta->chainParameters.hash("Azimuth").toString() ==
                            meta->chainParameters.hash("Azimuth").toString() &&
                    otherMeta->chainParameters.hash("Elevation").toString() ==
                            meta->chainParameters.hash("Elevation").toString() &&
                    otherMeta->chainParameters.hash("Magnitude").toString() ==
                            meta->chainParameters.hash("Magnitude").toString()) {
                auto check = parent->primaryChains.find(u);
                if (check != parent->primaryChains.end()) {
                    parent->installPrimaryChain(unit, check->second);
                    return true;
                }
            }
        }

        break;
    }

    }
    return false;
}

bool SmoothingEngine::DispatchData::discardOutput(const SequenceName &unit)
{ return parent->alwaysBypassOutput(unit); }

bool SmoothingEngine::DispatchData::disableChain()
{ return false; }

void SmoothingEngine::DispatchData::setupInterface(SmoothingEngine::EngineInterface *interface,
                                                   SmoothingEngine::DispatchMetadata *meta,
                                                   SmootherChainCore::Type &effectiveType)
{
    if (!meta) {
        effectiveType = SmootherChainCore::General;
        interface->options = Variant::Read::empty();
    } else {
        effectiveType = meta->chainType;
        interface->options = meta->chainParameters;
    }

    switch (effectiveType) {
    case SmootherChainCore::General: {
        interface->baseUnits.push_back(unit);
        interface->addNewInput(unit);
        SequenceName u(getOutputUnit());
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        break;
    }

    case SmootherChainCore::Difference: {
        SequenceName u(unit);
        u.removeFlavor(SequenceName::flavor_end);
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);
        u.addFlavor(SequenceName::flavor_end);
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);
        u = getOutputUnit().withoutFlavor(SequenceName::flavor_end);
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        u = getOutputUnit().withFlavor(SequenceName::flavor_end);
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        break;
    }

    case SmootherChainCore::DifferenceInitial: {
        SequenceName u(unit);
        u.removeFlavor(SequenceName::flavor_end);
        interface->baseUnits.push_back(u);
        interface->addNewInput(u);
        u.addFlavor(SequenceName::flavor_end);
        interface->baseUnits.push_back(u);

        u = getOutputUnit().withoutFlavor(SequenceName::flavor_end);
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        u = getOutputUnit().withFlavor(SequenceName::flavor_end);
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        break;
    }

    case SmootherChainCore::Vector2D: {
        SequenceName u(unit);
        u.setVariable(interface->options.hash("Direction").toString());
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);
        u.setVariable(interface->options.hash("Magnitude").toString());
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);

        u = getOutputUnit();
        u.setVariable(interface->options.hash("Direction").toString());
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        u.setVariable(interface->options.hash("Magnitude").toString());
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        break;
    }

    case SmootherChainCore::Vector3D: {
        SequenceName u(unit);
        u.setVariable(interface->options.hash("Azimuth").toString());
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);
        u.setVariable(interface->options.hash("Elevation").toString());
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);
        u.setVariable(interface->options.hash("Magnitude").toString());
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);

        u = getOutputUnit();
        u.setVariable(interface->options.hash("Azimuth").toString());
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        u.setVariable(interface->options.hash("Elevation").toString());
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        u.setVariable(interface->options.hash("Magnitude").toString());
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        break;
    }

    case SmootherChainCore::BeersLawAbsorption: {
        interface->baseUnits.push_back(unit);

        SequenceName u(unit);
        u.setVariable(interface->options.hash("L").toString());
        interface->addNewInput(u.withoutFlavor(SequenceName::flavor_end));
        interface->addNewInput(u.withFlavor(SequenceName::flavor_end));
        if (interface->options.hash("Transmittance").exists())
            u.setVariable(interface->options.hash("Transmittance").toString());
        else
            u.setVariable(interface->options.hash("I").toString());
        interface->addNewInput(u.withoutFlavor(SequenceName::flavor_end));
        interface->addNewInput(u.withFlavor(SequenceName::flavor_end));

        u = getOutputUnit();
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        break;
    }

    case SmootherChainCore::BeersLawAbsorptionInitial: {
        interface->baseUnits.push_back(unit);

        SequenceName u(unit);
        u.setVariable(interface->options.hash("L").toString());
        interface->addNewInput(u);
        if (interface->options.hash("Transmittance").exists())
            u.setVariable(interface->options.hash("Transmittance").toString());
        else
            u.setVariable(interface->options.hash("I").toString());
        interface->addNewInput(u);

        u = getOutputUnit();
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        break;
    }

    case SmootherChainCore::Dewpoint: {
        interface->baseUnits.push_back(unit);

        SequenceName u(unit);
        u.setVariable(interface->options.hash("T").toString());
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);
        u.setVariable(interface->options.hash("RH").toString());
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);

        u = getOutputUnit();
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        break;
    }

    case SmootherChainCore::RH: {
        interface->baseUnits.push_back(unit);

        SequenceName u(unit);
        u.setVariable(interface->options.hash("T").toString());
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);
        if (interface->options.hash("Dewpoint").exists())
            u.setVariable(interface->options.hash("Dewpoint").toString());
        else
            u.setVariable(interface->options.hash("TD").toString());
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);

        u = getOutputUnit();
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        break;
    }

    case SmootherChainCore::RHExtrapolate: {
        interface->baseUnits.push_back(unit);

        SequenceName u(unit);
        u.setVariable(interface->options.hash("Tin").toString());
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);
        u.setVariable(interface->options.hash("RHin").toString());
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);
        u.setVariable(interface->options.hash("Tout").toString());
        interface->addNewInput(u);
        interface->baseUnits.push_back(u);

        u = getOutputUnit();
        if (discardOutput(u)) {
            interface->addNewDiscardOutput(u);
        } else {
            interface->addNewOutput(u);
        }
        break;
    }
    }
}

void SmoothingEngine::DispatchData::convertToBasic(double time)
{
    if (chainMode == Basic)
        return;
    SinkMultiplexer::Sink *timePlaceholder = parent->createOutputPlaceholder();
    do {
        destroyChain();
        chainMode = Basic;

        if (parent->alwaysBypassOutput(unit)) {
            pendingBypassDisable = false;
            if (!directBypass) {
                directBypass = parent->createDataIngress(unit);
            }
            break;
        }

        if (parent->generatedOutput(unit) || disableChain()) {
            pendingBypassDisable = true;
            break;
        }
        if (unit.hasFlavor(SequenceName::flavor_cover) ||
                unit.hasFlavor(SequenceName::flavor_stats)) {
            pendingBypassDisable = false;
            if (!directBypass) {
                directBypass = parent->createDataIngress(unit);
            }
            break;
        }
        pendingBypassDisable = true;

        SmoothingEngine::DispatchMetadata *meta = parent->getUnitMetadataDispatch(unit);
        if (checkMultiplePrimary(meta))
            break;

        if (!meta->smoothingEnable) {
            SequenceName u(getOutputUnit());
            if (discardOutput(u))
                break;

            auto chain = new SmoothingEngine::ChainGeneralBypass;
            parent->installPrimaryChain(unit, chain);
            parent->chains.emplace_back(chain);

            auto reconstruct =
                    new SmoothingEngine::GeneralReconstruct(u, parent->createDataIngress(unit));
            targetsGeneral.push_back(reconstruct);
            chain->node.reset(reconstruct);
            chain->unit = unit;
            break;
        }

        auto interface = parent->createEngineInterface();

        auto chain = new SmoothingEngine::ChainBasic;
        parent->installPrimaryChain(unit, chain);
        parent->chains.emplace_back(chain);

        interface->target = &chain->data;
        if (interface->auxiliary)
            chain->data.auxiliary = std::move(interface->auxiliary);
        interface->discardTime = time;
        setupInterface(interface.get(), meta, chain->type);

        parent->constructChain(unit, interface.get(), chain->type);
    } while (false);
    timePlaceholder->endData();
}

void SmoothingEngine::DispatchData::convertToFlags(double time)
{
    if (chainMode == Flags)
        return;
    SinkMultiplexer::Sink *timePlaceholder = parent->createOutputPlaceholder();
    do {
        destroyChain();
        chainMode = Flags;

        if (parent->alwaysBypassOutput(unit)) {
            pendingBypassDisable = false;
            if (!directBypass) {
                directBypass = parent->createDataIngress(unit);
            }
            break;
        }

        if (parent->generatedOutput(unit) || disableChain()) {
            pendingBypassDisable = true;
            break;
        }
        if (unit.hasFlavor(SequenceName::flavor_cover) ||
                unit.hasFlavor(SequenceName::flavor_stats)) {
            pendingBypassDisable = false;
            if (!directBypass) {
                directBypass = parent->createDataIngress(unit);
            }
            break;
        }
        pendingBypassDisable = true;

        SmoothingEngine::DispatchMetadata *meta = parent->getUnitMetadataDispatch(unit);

        if (!meta->smoothingEnable || !parent->handleFlags(unit)) {
            SequenceName u(getOutputUnit());
            if (discardOutput(u))
                break;

            auto chain = new SmoothingEngine::ChainGeneralBypass;
            parent->installPrimaryChain(unit, chain);
            parent->chains.emplace_back(chain);

            auto reconstruct =
                    new SmoothingEngine::GeneralReconstruct(u, parent->createDataIngress(unit));
            targetsGeneral.push_back(reconstruct);
            chain->node.reset(reconstruct);
            chain->unit = unit;
            break;
        }

        auto chain = new SmoothingEngine::ChainFlags;
        parent->installPrimaryChain(unit, chain);
        parent->chains.emplace_back(chain);

        auto interface = parent->createEngineInterface();
        interface->target = &chain->data;
        if (interface->auxiliary)
            chain->data.auxiliary = std::move(interface->auxiliary);
        interface->discardTime = time;
        interface->addNewFlagsInput(unit);
        interface->addNewFlagsOutput(getOutputUnit());
        interface->baseUnits.push_back(unit);
        interface->options = meta->chainParameters;

        parent->constructFlagsChain(unit, interface.get());
    } while (false);
    timePlaceholder->endData();
}

void SmoothingEngine::DispatchData::convertToArray(double time, std::size_t size)
{
    if (chainMode == Array && targetsArray.size() == size)
        return;
    SinkMultiplexer::Sink *timePlaceholder = parent->createOutputPlaceholder();
    do {
        destroyChain();
        chainMode = Array;

        if (parent->alwaysBypassOutput(unit)) {
            pendingBypassDisable = false;
            if (!directBypass) {
                directBypass = parent->createDataIngress(unit);
            }
            break;
        }

        if (parent->generatedOutput(unit) || disableChain()) {
            pendingBypassDisable = true;
            break;
        }
        if (unit.hasFlavor(SequenceName::flavor_cover) ||
                unit.hasFlavor(SequenceName::flavor_stats)) {
            pendingBypassDisable = false;
            if (!directBypass) {
                directBypass = parent->createDataIngress(unit);
            }
            break;
        }
        pendingBypassDisable = true;

        SmoothingEngine::DispatchMetadata *meta = parent->getUnitMetadataDispatch(unit);
        if (checkMultiplePrimary(meta))
            break;

        if (!meta->smoothingEnable) {
            SequenceName u(getOutputUnit());
            if (discardOutput(u))
                break;

            auto chain = new SmoothingEngine::ChainGeneralBypass;
            parent->installPrimaryChain(unit, chain);
            parent->chains.emplace_back(chain);

            auto reconstruct =
                    new SmoothingEngine::GeneralReconstruct(u, parent->createDataIngress(unit));
            targetsGeneral.push_back(reconstruct);
            chain->node.reset(reconstruct);
            chain->unit = unit;
            break;
        }

        auto chain = new SmoothingEngine::ChainArray;
        parent->installPrimaryChain(unit, chain);
        parent->chains.emplace_back(chain);
        chain->indices.resize(size);
        chain->data.size = size;

        for (std::size_t index = 0; index < size; index++) {
            auto interface = parent->createEngineInterface();
            interface->target = &chain->indices[index];
            if (interface->auxiliary)
                chain->indices[index].auxiliary = std::move(interface->auxiliary);
            interface->discardTime = time;
            interface->targetArray = &chain->data;
            interface->arrayIndex = index;
            setupInterface(interface.get(), meta, chain->type);

            parent->constructChain(unit, interface.get(), chain->type);
        }
    } while (false);
    timePlaceholder->endData();
}

void SmoothingEngine::DispatchData::convertToMatrix(double time,
                                                    const Variant::PathElement::MatrixIndex &size)
{
    if (chainMode == Matrix && matrixSize == size)
        return;
    SinkMultiplexer::Sink *timePlaceholder = parent->createOutputPlaceholder();
    do {
        destroyChain();
        chainMode = Matrix;
        matrixSize = size;

        if (parent->alwaysBypassOutput(unit)) {
            pendingBypassDisable = false;
            if (!directBypass) {
                directBypass = parent->createDataIngress(unit);
            }
            break;
        }

        if (parent->generatedOutput(unit) || disableChain()) {
            pendingBypassDisable = true;
            break;
        }
        if (unit.hasFlavor(SequenceName::flavor_cover) ||
                unit.hasFlavor(SequenceName::flavor_stats)) {
            pendingBypassDisable = false;
            if (!directBypass) {
                directBypass = parent->createDataIngress(unit);
            }
            break;
        }
        pendingBypassDisable = true;

        SmoothingEngine::DispatchMetadata *meta = parent->getUnitMetadataDispatch(unit);
        if (checkMultiplePrimary(meta))
            break;

        std::size_t totalSize = 1;
        for (auto add : size) {
            totalSize *= add;
        }

        if (!meta->smoothingEnable || size.empty() || totalSize <= 0) {
            SequenceName u(getOutputUnit());
            if (discardOutput(u))
                break;

            auto chain = new SmoothingEngine::ChainGeneralBypass;
            parent->installPrimaryChain(unit, chain);
            parent->chains.emplace_back(chain);

            auto reconstruct =
                    new SmoothingEngine::GeneralReconstruct(u, parent->createDataIngress(unit));
            targetsGeneral.push_back(reconstruct);
            chain->node.reset(reconstruct);
            chain->unit = unit;
            break;
        }


        auto chain = new SmoothingEngine::ChainMatrix;
        parent->installPrimaryChain(unit, chain);
        parent->chains.emplace_back(chain);
        chain->indices.resize(static_cast<std::size_t>(totalSize));
        chain->data.size = size;

        for (std::size_t index = 0; index < totalSize; index++) {
            auto interface = parent->createEngineInterface();
            interface->target = &chain->indices[index];
            if (interface->auxiliary)
                chain->indices[index].auxiliary = std::move(interface->auxiliary);
            interface->discardTime = time;
            interface->targetMatrix = &chain->data;
            interface->arrayIndex = index;
            setupInterface(interface.get(), meta, chain->type);

            parent->constructChain(unit, interface.get(), chain->type);
        }
    } while (false);
    timePlaceholder->endData();
}

void SmoothingEngine::DispatchData::convertToGeneral(double time)
{
    Q_UNUSED(time);
    if (chainMode == General)
        return;
    SinkMultiplexer::Sink *timePlaceholder = parent->createOutputPlaceholder();
    do {
        destroyChain();
        chainMode = General;

        if (parent->alwaysBypassOutput(unit)) {
            pendingBypassDisable = false;
            if (!directBypass) {
                directBypass = parent->createDataIngress(unit);
            }
            break;
        }

        if (parent->generatedOutput(unit) || disableChain()) {
            pendingBypassDisable = true;
            break;
        }
        if (unit.hasFlavor(SequenceName::flavor_cover) ||
                unit.hasFlavor(SequenceName::flavor_stats)) {
            pendingBypassDisable = false;
            if (!directBypass) {
                directBypass = parent->createDataIngress(unit);
            }
            break;
        }
        pendingBypassDisable = true;

        SequenceName u(getOutputUnit());
        if (discardOutput(u))
            break;

        auto chain = new SmoothingEngine::ChainGeneralBypass;
        parent->installPrimaryChain(unit, chain);
        parent->chains.emplace_back(chain);

        auto reconstruct =
                new SmoothingEngine::GeneralReconstruct(u, parent->createDataIngress(unit));
        targetsGeneral.push_back(reconstruct);
        chain->node.reset(reconstruct);
        chain->unit = unit;
    } while (false);
    timePlaceholder->endData();
}

void SmoothingEngine::DispatchData::handleSegments(ValueSegment::Transfer &&result)
{
    for (auto &add : result) {
        auto value = add.read();
        double start = add.getStart();
        double end = add.getEnd();

        switch (value.getType()) {
        case Variant::Type::Empty:
            /* If the chain is already defined then just send it
             * missing values */
            if (chainMode != Undefined) {
                for (auto target : targetsGeneral) {
                    target->incomingData(start, end, add.root());
                }
                for (auto target : targetsBasic) {
                    target->incomingData(start, end, FP::undefined());
                }
                for (auto target : targetsFlags) {
                    target->incomingAdvance(start);
                }
                for (const auto &index : targetsArray) {
                    for (auto target : index) {
                        target->incomingData(start, end, FP::undefined());
                    }
                }
                break;
            }
            /* Fall through */
        case Variant::Type::Real: {
            convertToBasic(start);

            double v = value.toDouble();
            for (auto target : targetsBasic) {
                target->incomingData(start, end, v);
            }

            for (auto target : targetsGeneral) {
                target->incomingData(start, end, add.root());
            }
            for (auto target : targetsFlags) {
                target->incomingAdvance(start);
            }

            for (const auto &index : targetsArray) {
                for (auto target : index) {
                    target->incomingAdvance(start);
                }
            }
            break;
        }

        case Variant::Type::Array: {
            auto values = value.toArray();
            convertToArray(start, values.size());

            auto addValue = values.cbegin();
            auto endValues = values.cend();
            auto index = targetsArray.cbegin();
            auto endIndex = targetsArray.cend();
            double v;
            for (; index != endIndex && addValue != endValues; ++index, ++addValue) {
                v = (*addValue).toReal();
                for (auto target : *index) {
                    target->incomingData(start, end, v);
                }
            }
            for (; index != endIndex; ++index) {
                for (auto target : *index) {
                    target->incomingAdvance(start);
                }
            }

            for (auto target : targetsGeneral) {
                target->incomingData(start, end, add.root());
            }
            for (auto target : targetsBasic) {
                target->incomingAdvance(start);
            }
            for (auto target : targetsFlags) {
                target->incomingAdvance(start);
            }
            break;
        }

        case Variant::Type::Matrix: {
            auto values = value.toMatrix();
            convertToMatrix(start, values.shape());

            auto addValue = values.cbegin();
            auto endValues = values.cend();
            auto index = targetsArray.begin();
            auto endIndex = targetsArray.end();
            double v;
            for (; index != endIndex && addValue != endValues; ++index, ++addValue) {
                v = addValue.value().toReal();
                for (auto target : *index) {
                    target->incomingData(start, end, v);
                }
            }
            for (; index != endIndex; ++index) {
                for (auto target : *index) {
                    target->incomingAdvance(start);
                }
            }

            for (auto target : targetsGeneral) {
                target->incomingData(start, end, add.root());
            }
            for (auto target : targetsBasic) {
                target->incomingAdvance(start);
            }
            for (auto target : targetsFlags) {
                target->incomingAdvance(start);
            }
            break;
        }

        case Variant::Type::Flags: {
            convertToFlags(start);

            const auto &v = value.toFlags();
            for (auto target : targetsFlags) {
                target->incomingData(start, end, v);
            }

            for (auto target : targetsGeneral) {
                target->incomingData(start, end, add.root());
            }

            for (auto target : targetsBasic) {
                target->incomingAdvance(start);
            }
            for (const auto &index : targetsArray) {
                for (auto target : index) {
                    target->incomingAdvance(start);
                }
            }
            break;
        }

        default:
            convertToGeneral(start);

            for (auto target : targetsGeneral) {
                target->incomingData(start, end, add.root());
            }
            for (auto target : targetsBasic) {
                target->incomingAdvance(start);
            }
            for (auto target : targetsFlags) {
                target->incomingAdvance(start);
            }
            for (const auto &index : targetsArray) {
                for (auto target : index) {
                    target->incomingAdvance(start);
                }
            }
            break;
        }

        if (directBypass && !pendingBypassDisable) {
            directBypass->emplaceData(SequenceIdentity(unit, start, end), add.root());
        }
    }
    if (!result.empty()) {
        lastSegments = std::move(result);
        if (initialTimeHoldback) {
            initialTimeHoldback->endData();
            initialTimeHoldback = nullptr;
        }
    }
}

void SmoothingEngine::DispatchData::addTarget(double discardTime, SmootherChainTarget *target)
{
    for (const auto &add : lastSegments) {
        if (FP::defined(discardTime) &&
                (!FP::defined(add.getStart()) || add.getStart() < discardTime))
            continue;
        target->incomingData(add.getStart(), add.getEnd(), add.getValue().toDouble());
    }
    if (ended) {
        target->endData();
        return;
    }
    targetsBasic.push_back(target);
}

void SmoothingEngine::DispatchData::addTarget(double discardTime, SmootherChainTargetFlags *target)
{
    for (const auto &add : lastSegments) {
        if (add.getValue().getType() != Variant::Type::Flags)
            continue;
        if (FP::defined(discardTime) &&
                (!FP::defined(add.getStart()) || add.getStart() < discardTime))
            continue;
        target->incomingData(add.getStart(), add.getEnd(), add.getValue().toFlags());
    }
    if (ended) {
        target->endData();
        return;
    }
    targetsFlags.push_back(target);
}

void SmoothingEngine::DispatchData::addTarget(double discardTime,
                                              SmootherChainTargetGeneral *target)
{
    for (const auto &add : lastSegments) {
        if (FP::defined(discardTime) &&
                (!FP::defined(add.getStart()) || add.getStart() < discardTime))
            continue;
        target->incomingData(add.getStart(), add.getEnd(), Variant::Root(add.root()));
    }
    if (ended) {
        target->endData();
        return;
    }
    targetsGeneral.push_back(target);
}

void SmoothingEngine::DispatchData::addTarget(double discardTime,
                                              SmootherChainTarget *target, std::size_t arrayIndex)
{
    for (const auto &add : lastSegments) {
        if (FP::defined(discardTime) &&
                (!FP::defined(add.getStart()) || add.getStart() < discardTime))
            continue;

        auto r = add.read();
        switch (r.getType()) {
        case Variant::Type::Array: {
            auto ar = r.toArray();
            if (arrayIndex >= ar.size())
                break;
            target->incomingData(add.getStart(), add.getEnd(), ar[arrayIndex].toReal());
            break;
        }
        case Variant::Type::Matrix: {
            auto am = r.toMatrix();
            if (arrayIndex >= am.size())
                break;
            target->incomingData(add.getStart(), add.getEnd(), am[arrayIndex].toReal());
            break;
        }
        default:
            break;
        }
        break;
    }
    if (ended) {
        target->endData();
        return;
    }
    if (targetsArray.size() <= static_cast<std::size_t>(arrayIndex))
        targetsArray.resize(static_cast<std::size_t>(arrayIndex) + 1);
    targetsArray[arrayIndex].push_back(target);
}

void SmoothingEngine::DispatchData::incomingData(SequenceValue &&value)
{
    /* As soon as we see a value, create a placeholder so we grab the main holdback time
     * until the actual segment is done. */
    if (chainMode == Undefined && !initialTimeHoldback) {
        initialTimeHoldback = parent->createOutputPlaceholder();
    }

    handleSegments(segmenter.add(std::move(value)));
}

void SmoothingEngine::DispatchData::advanceAll(double time)
{
    for (auto target : targetsBasic) {
        target->incomingAdvance(time);
    }
    for (auto target : targetsFlags) {
        target->incomingAdvance(time);
    }
    for (auto target : targetsGeneral) {
        target->incomingAdvance(time);
    }
    for (const auto &index : targetsArray) {
        for (auto target : index) {
            target->incomingAdvance(time);
        }
    }
    if (directBypass) {
        directBypass->incomingAdvance(time);
    }
}

void SmoothingEngine::DispatchData::incomingAdvance(double time)
{
    bool haveMore = false;
    handleSegments(segmenter.completedAdvance(time, &haveMore));
    if (!haveMore)
        advanceAll(time);
    if (pendingBypassDisable) {
        if (directBypass) {
            directBypass->endData();
            directBypass = nullptr;
        }
        pendingBypassDisable = false;
    }
}

void SmoothingEngine::DispatchData::endAll()
{
    ended = true;

    for (auto target : targetsBasic) {
        target->endData();
    }
    targetsBasic.clear();

    for (auto target : targetsFlags) {
        target->endData();
    }
    targetsFlags.clear();

    for (auto target : targetsGeneral) {
        target->endData();
    }
    targetsGeneral.clear();

    for (const auto &index : targetsArray) {
        for (auto target : index) {
            target->endData();
        }
    }
    targetsArray.clear();

    if (directBypass) {
        directBypass->endData();
        directBypass = nullptr;
    }
    if (initialTimeHoldback) {
        initialTimeHoldback->endData();
        initialTimeHoldback = nullptr;
    }
}

void SmoothingEngine::DispatchData::endData()
{
    handleSegments(segmenter.finish());
    endAll();
}

double SmoothingEngine::DispatchData::nextPossibleTransition() const
{ return segmenter.nextTransitionTime(); }

int SmoothingEngine::DispatchData::getType() const
{ return SmoothingEngine::Type_Value; }

SequenceName SmoothingEngine::DispatchData::getOutputUnit()
{ return unit; }

void SmoothingEngine::DispatchData::serialize(QDataStream &stream) const
{
    stream << ended << segmenter << static_cast<quint8>(chainMode)
           << ((bool) (pendingBypassDisable || !directBypass));
}

void SmoothingEngine::DispatchData::deserialize(QDataStream &stream)
{
    stream >> ended >> segmenter;
    {
        quint8 i8 = 0;
        stream >> i8;
        chainMode = static_cast<ChainMode>(i8);
    }
    stream >> pendingBypassDisable;
}


SmoothingEngine::BasicReconstruct::BasicReconstruct(const SequenceName &u,
                                                    SinkMultiplexer::Sink *in) : unit(u),
                                                                                 ingress(in),
                                                                                 ended(false)
{ }

SmoothingEngine::BasicReconstruct::~BasicReconstruct() = default;

bool SmoothingEngine::BasicReconstruct::finished()
{ return ended; }

void SmoothingEngine::BasicReconstruct::serialize(QDataStream &stream) const
{ Q_UNUSED(stream); }

void SmoothingEngine::BasicReconstruct::incomingData(double start, double end, double value)
{ ingress->incomingData(SequenceValue({unit, start, end}, Variant::Root(value))); }

void SmoothingEngine::BasicReconstruct::incomingAdvance(double time)
{ ingress->incomingAdvance(time); }

void SmoothingEngine::BasicReconstruct::endData()
{
    ingress->endData();
    ended = true;
}

SmoothingEngine::FlagsReconstruct::FlagsReconstruct(const SequenceName &u,
                                                    SinkMultiplexer::Sink *in) : unit(u),
                                                                                 ingress(in),
                                                                                 ended(false)
{ }

SmoothingEngine::FlagsReconstruct::~FlagsReconstruct() = default;

bool SmoothingEngine::FlagsReconstruct::finished()
{ return ended; }

void SmoothingEngine::FlagsReconstruct::serialize(QDataStream &stream) const
{ Q_UNUSED(stream); }

void SmoothingEngine::FlagsReconstruct::incomingData(double start,
                                                     double end, const Data::Variant::Flags &value)
{ ingress->incomingData(SequenceValue({unit, start, end}, Variant::Root(value))); }

void SmoothingEngine::FlagsReconstruct::incomingAdvance(double time)
{ ingress->incomingAdvance(time); }

void SmoothingEngine::FlagsReconstruct::endData()
{
    ingress->endData();
    ended = true;
}

SmoothingEngine::GeneralReconstruct::GeneralReconstruct(const SequenceName &u,
                                                        SinkMultiplexer::Sink *in)
        : unit(u), ingress(in), ended(false)
{ }

SmoothingEngine::GeneralReconstruct::~GeneralReconstruct() = default;

bool SmoothingEngine::GeneralReconstruct::finished()
{ return ended; }

void SmoothingEngine::GeneralReconstruct::serialize(QDataStream &stream) const
{ Q_UNUSED(stream); }

void SmoothingEngine::GeneralReconstruct::incomingData(double start,
                                                       double end,
                                                       const Variant::Root &value)
{ ingress->incomingData(SequenceValue(unit, value, start, end)); }

void SmoothingEngine::GeneralReconstruct::incomingAdvance(double time)
{ ingress->incomingAdvance(time); }

void SmoothingEngine::GeneralReconstruct::endData()
{
    ingress->endData();
    ended = true;
}


SmoothingEngine::EngineInterface::EngineInterface(SmoothingEngine *engine) : parent(engine),
                                                                             target(nullptr),
                                                                             arrayIndex(
                                                                                     static_cast<std::size_t>(-1)),
                                                                             targetArray(nullptr),
                                                                             targetMatrix(nullptr),
                                                                             discardTime(
                                                                                     FP::undefined()),
                                                                             auxiliary()
{ }

SmoothingEngine::EngineInterface::~EngineInterface() = default;

void SmoothingEngine::EngineInterface::addChainNode(SmootherChainNode *add)
{ target->nodes.emplace_back(add); }

SmootherChainTarget *SmoothingEngine::EngineInterface::getOutput(int index)
{ return outputs[index]; }

SmootherChainTargetFlags *SmoothingEngine::EngineInterface::getFlagsOutput(int index)
{ return outputsFlags[index]; }

SmootherChainTargetGeneral *SmoothingEngine::EngineInterface::getGeneralOutput(int index)
{ return outputsGeneral[index]; }

void SmoothingEngine::EngineInterface::addInputTarget(int index, SmootherChainTarget *target)
{
    auto dtBase = parent->dispatchTarget(this->target->inputs[index].unit);
    Q_ASSERT(dtBase->getType() == SmoothingEngine::Type_Value);
    auto d = static_cast<DispatchData *>(dtBase);
    if (arrayIndex == static_cast<std::size_t>(-1)) {
        d->addTarget(discardTime, target);
    } else {
        d->addTarget(discardTime, target, arrayIndex);
    }
    if (d->ended)
        return;
    this->target->inputs[index].targets.push_back(target);
}

void SmoothingEngine::EngineInterface::addFlagsInputTarget(int index,
                                                           SmootherChainTargetFlags *target)
{
    auto dtBase = parent->dispatchTarget(this->target->inputsFlags[index].unit);
    Q_ASSERT(dtBase->getType() == SmoothingEngine::Type_Value);
    auto d = static_cast<DispatchData *>(dtBase);
    if (arrayIndex == static_cast<std::size_t>(-1)) {
        d->addTarget(discardTime, target);
    } else {
        /* Currently unsupported */
        target->endData();
        return;
    }
    if (d->ended)
        return;
    this->target->inputsFlags[index].targets.push_back(target);
}

void SmoothingEngine::EngineInterface::addGeneralInputTarget(int index,
                                                             SmootherChainTargetGeneral *target)
{
    auto dtBase = parent->dispatchTarget(this->target->inputsGeneral[index].unit);
    Q_ASSERT(dtBase->getType() == SmoothingEngine::Type_Value);
    auto d = static_cast<DispatchData *>(dtBase);
    if (arrayIndex == static_cast<std::size_t>(-1)) {
        d->addTarget(discardTime, target);
    } else {
        /* Currently unsupported */
        target->endData();
        return;
    }
    if (d->ended)
        return;
    this->target->inputsGeneral[index].targets.push_back(target);
}

SequenceName SmoothingEngine::EngineInterface::getBaseUnit(int index)
{
    if (index < 0 || static_cast<std::size_t>(index) >= baseUnits.size())
        return {};
    return baseUnits[static_cast<std::size_t>(index)];
}

SmootherChainTarget *SmoothingEngine::EngineInterface::addNewOutput(const SequenceName &unit)
{
    target->outputs.push_back(unit);

    if (arrayIndex == static_cast<std::size_t>(-1)) {
        auto *rc = new BasicReconstruct(unit, parent->createDataIngress(unit));
        outputs.push_back(rc);
        target->nodes.emplace_back(rc);
    } else if (targetArray) {
        auto check = targetArray->outputsBasic.find(unit);
        if (check == targetArray->outputsBasic.end()) {
            auto add = new SmoothingEngineArrayMergeBasic(targetArray->size, unit,
                                                          parent->createDataIngress(unit));
            targetArray->sharedNodes.emplace_back(add);
            check = targetArray->outputsBasic.emplace(unit, add).first;
        }
        outputs.push_back(check->second->getTarget(arrayIndex));
    } else if (targetMatrix) {
        auto check = targetMatrix->outputsBasic.find(unit);
        if (check == targetMatrix->outputsBasic.end()) {
            auto add = new SmoothingEngineMatrixMergeBasic(targetMatrix->size, unit,
                                                           parent->createDataIngress(unit));
            targetMatrix->sharedNodes.emplace_back(add);
            check = targetMatrix->outputsBasic.emplace(unit, add).first;
        }
        outputs.push_back(check->second->getTarget(arrayIndex));
    } else {
        Q_ASSERT(false);
    }

    return outputs.back();
}


namespace {
class DiscardOutput : public SmootherChainTarget, public SmootherChainNode {
    bool ended;
public:
    DiscardOutput() : ended(false)
    { }

    virtual ~DiscardOutput() = default;

    virtual bool finished()
    { return ended; }

    virtual void serialize(QDataStream &) const
    { }

    virtual void incomingData(double, double, double)
    { }

    virtual void incomingAdvance(double)
    { }

    virtual void endData()
    { ended = true; }
};

class DiscardFlags : public SmootherChainTargetFlags, public SmootherChainNode {
    bool ended;
public:
    DiscardFlags() : ended(false)
    { }

    virtual ~DiscardFlags() = default;

    virtual bool finished()
    { return ended; }

    virtual void serialize(QDataStream &) const
    { }

    virtual void incomingData(double, double, const Data::Variant::Flags &)
    { }

    virtual void incomingAdvance(double)
    { }

    virtual void endData()
    { ended = true; }
};

class DiscardGeneral : public SmootherChainTargetGeneral, public SmootherChainNode {
    bool ended;
public:
    DiscardGeneral() : ended(false)
    { }

    virtual ~DiscardGeneral() = default;

    virtual bool finished()
    { return ended; }

    virtual void serialize(QDataStream &) const
    { }

    virtual void incomingData(double, double, const Data::Variant::Root &)
    { }

    virtual void incomingAdvance(double)
    { }

    virtual void endData()
    { ended = true; }
};
}

SmootherChainTarget *SmoothingEngine::EngineInterface::addNewDiscardOutput(const SequenceName &unit)
{
    target->outputs.emplace_back(unit);
    auto d = new DiscardOutput;
    outputs.push_back(d);
    target->nodes.emplace_back(d);
    return d;
}

SmootherChainTargetFlags *SmoothingEngine::EngineInterface::addNewDiscardFlagsOutput(const SequenceName &unit)
{
    target->outputsFlags.emplace_back(unit);
    auto d = new DiscardFlags;
    outputsFlags.push_back(d);
    target->nodes.emplace_back(d);
    return d;
}

SmootherChainTargetGeneral *SmoothingEngine::EngineInterface::addNewDiscardGeneralOutput(const SequenceName &unit)
{
    target->outputsGeneral.emplace_back(unit);
    auto d = new DiscardGeneral;
    outputsGeneral.push_back(d);
    target->nodes.emplace_back(d);
    return d;
}

SmootherChainTargetFlags *SmoothingEngine::EngineInterface::addNewFlagsOutput(const SequenceName &unit)
{
    target->outputsFlags.emplace_back(unit);

    if (arrayIndex == static_cast<std::size_t>(-1)) {
        auto rc = new FlagsReconstruct(unit, parent->createDataIngress(unit));
        outputsFlags.push_back(rc);
        target->nodes.emplace_back(rc);
    } else if (targetArray) {
        auto check = targetArray->outputsFlags.find(unit);
        if (check == targetArray->outputsFlags.end()) {
            auto add = new SmoothingEngineArrayMergeFlags(targetArray->size, unit,
                                                          parent->createDataIngress(unit));
            check = targetArray->outputsFlags.emplace(unit, add).first;
            targetArray->sharedNodes.emplace_back(add);
        }
        outputsFlags.push_back(check->second->getTarget(arrayIndex));
    } else if (targetMatrix) {
        auto check = targetMatrix->outputsFlags.find(unit);
        if (check == targetMatrix->outputsFlags.end()) {
            auto add = new SmoothingEngineMatrixMergeFlags(targetMatrix->size, unit,
                                                           parent->createDataIngress(unit));
            check = targetMatrix->outputsFlags.emplace(unit, add).first;
            targetMatrix->sharedNodes.emplace_back(add);
        }
        outputsFlags.push_back(check->second->getTarget(arrayIndex));
    } else {
        Q_ASSERT(false);
    }

    return outputsFlags.back();
}

SmootherChainTargetGeneral *SmoothingEngine::EngineInterface::addNewGeneralOutput(const SequenceName &unit)
{
    target->outputsGeneral.emplace_back(unit);

    if (arrayIndex == static_cast<std::size_t>(-1)) {
        auto rc = new GeneralReconstruct(unit, parent->createDataIngress(unit));
        outputsGeneral.push_back(rc);
        target->nodes.emplace_back(rc);
    } else if (targetArray) {
        auto check = targetArray->outputsGeneral.find(unit);
        if (check == targetArray->outputsGeneral.end()) {
            auto add = new SmoothingEngineArrayMergeGeneral(targetArray->size, unit,
                                                            parent->createDataIngress(unit));
            check = targetArray->outputsGeneral.emplace(unit, add).first;
            targetArray->sharedNodes.emplace_back(add);
        }
        outputsGeneral.push_back(check->second->getTarget(arrayIndex));
    } else if (targetMatrix) {
        auto check = targetMatrix->outputsGeneral.find(unit);
        if (check == targetMatrix->outputsGeneral.end()) {
            auto add = new SmoothingEngineMatrixMergeGeneral(targetMatrix->size, unit,
                                                             parent->createDataIngress(unit));
            check = targetMatrix->outputsGeneral.emplace(unit, add).first;
            targetMatrix->sharedNodes.emplace_back(add);
        }
        outputsGeneral.push_back(check->second->getTarget(arrayIndex));
    } else {
        Q_ASSERT(false);
    }

    return outputsGeneral.back();
}

SinkMultiplexer::Sink *SmoothingEngine::EngineInterface::addNewFinalOutput()
{ return parent->createOutputIngress(); }

#ifndef NDEBUG

template<typename ListType>
static bool inputsContains(const ListType &inputs, const SequenceName &unit)
{
    for (auto check : inputs) {
        if (check.unit == unit)
            return true;
    }
    return false;
}

#endif

void SmoothingEngine::EngineInterface::addNewInput(const SequenceName &unit,
                                                   SmootherChainTarget *target)
{
#ifndef NDEBUG
    if (inputsContains(this->target->inputs, unit)) {
        qCWarning(log_smoothing_smoothingengine) << "Unable to provide duplicate input" << unit;
    }
#endif
    this->target->inputs.emplace_back(unit);
    if (!target)
        return;
    addInputTarget(static_cast<int>(this->target->inputs.size() - 1), target);
}

void SmoothingEngine::EngineInterface::addNewFlagsInput(const SequenceName &unit,
                                                        SmootherChainTargetFlags *target)
{
#ifndef NDEBUG
    if (inputsContains(this->target->inputsFlags, unit)) {
        qCWarning(log_smoothing_smoothingengine) << "Unable to provide duplicate flags" << unit;
    }
#endif
    this->target->inputsFlags.emplace_back(unit);
    if (!target)
        return;
    addFlagsInputTarget(static_cast<int>(this->target->inputsFlags.size() - 1), target);
}

void SmoothingEngine::EngineInterface::addNewGeneralInput(const SequenceName &unit,
                                                          SmootherChainTargetGeneral *target)
{
#ifndef NDEBUG
    if (inputsContains(this->target->inputsGeneral, unit)) {
        qCWarning(log_smoothing_smoothingengine) << "Unable to provide duplicate general input"
                                                 << unit;
    }
#endif
    this->target->inputsGeneral.emplace_back(unit);
    if (!target)
        return;
    addGeneralInputTarget(static_cast<int>(this->target->inputsGeneral.size() - 1), target);
}

Variant::Read SmoothingEngine::EngineInterface::getOptions()
{ return options; }


SmoothingEngine::Chain::~Chain() = default;

SmoothingEngine::AuxiliaryChainController::AuxiliaryChainController() = default;

SmoothingEngine::AuxiliaryChainController::~AuxiliaryChainController() = default;

void SmoothingEngine::EngineInterface::setChainAuxiliary(AuxiliaryChainController *aux)
{
    if (target) {
        target->auxiliary.reset(aux);
    } else {
        auxiliary.reset(aux);
    }
}

enum ChainType {
    Chain_GeneralBypass = 0, Chain_Basic, Chain_Flags, Chain_Array, Chain_Matrix,
};

void SmoothingEngine::Chain::unreferenceAll(DispatchData *dispatch, const SequenceName &unit,
                                            ChainData &data,
                                            int arrayIndex)
{
    {
        auto op = std::find_if(data.inputs.begin(), data.inputs.end(),
                               [&unit](const ChainData::Input &check) {
                                   return unit == check.unit;
                               });
        if (op != data.inputs.end()) {
            for (auto toEnd : op->targets) {
                toEnd->endData();
                if (dispatch) {
                    eraseFromList(dispatch->targetsBasic, toEnd);
                    if (arrayIndex >= 0 &&
                            arrayIndex < static_cast<int>(dispatch->targetsArray.size())) {
                        eraseFromList(dispatch->targetsArray[arrayIndex], toEnd);
                    }
                }
            }
            op->targets.clear();
        }
    }

    {
        auto op = std::find_if(data.inputsFlags.begin(), data.inputsFlags.end(),
                               [&unit](const ChainData::InputFlags &check) {
                                   return unit == check.unit;
                               });
        if (op != data.inputsFlags.end()) {
            for (auto toEnd : op->targets) {
                toEnd->endData();
                if (dispatch) {
                    eraseFromList(dispatch->targetsFlags, toEnd);
                }
            }
            op->targets.clear();
        }
    }

    {
        auto op = std::find_if(data.inputsGeneral.begin(), data.inputsGeneral.end(),
                               [&unit](const ChainData::InputGeneral &check) {
                                   return unit == check.unit;
                               });
        if (op != data.inputsGeneral.end()) {
            for (auto toEnd : op->targets) {
                toEnd->endData();
                if (dispatch) {
                    eraseFromList(dispatch->targetsGeneral, toEnd);
                }
            }
            op->targets.clear();
        }
    }
}

Data::SequenceName::Set SmoothingEngine::Chain::definedInputs(const ChainData &data)
{
    Data::SequenceName::Set result;
    for (const auto &add : data.inputs) {
        result.insert(add.unit);
    }
    for (const auto &add : data.inputsFlags) {
        result.insert(add.unit);
    }
    for (const auto &add : data.inputsGeneral) {
        result.insert(add.unit);
    }
    return result;
}

void SmoothingEngine::Chain::sendTerminate(const std::vector<
        std::unique_ptr<SmootherChainNode>> &nodes)
{
    for (const auto &sig : nodes) {
        sig->signalTerminate();
    }
}

bool SmoothingEngine::Chain::isCompleted(const std::vector<
        std::unique_ptr<SmootherChainNode>> &nodes)
{
    for (const auto &sig : nodes) {
        if (!sig->finished())
            return false;
    }
    return true;
}

void SmoothingEngine::Chain::pauseChain(const std::vector<
        std::unique_ptr<SmootherChainNode>> &nodes)
{
    for (const auto &sig : nodes) {
        sig->pause();
    }
}

void SmoothingEngine::Chain::resumeChain(const std::vector<
        std::unique_ptr<SmootherChainNode>> &nodes)
{
    for (const auto &sig : nodes) {
        sig->resume();
    }
}

void SmoothingEngine::Chain::cleanupChain(const std::vector<
        std::unique_ptr<SmootherChainNode>> &nodes)
{
    for (const auto &sig : nodes) {
        sig->cleanup();
    }
}

void SmoothingEngine::Chain::serializeChain(const ChainData &data, QDataStream &stream)
{
    Serialize::container(stream, data.inputs, [&stream](const ChainData::Input &in) {
        stream << in.unit;
    });
    Serialize::container(stream, data.inputsFlags, [&stream](const ChainData::InputFlags &in) {
        stream << in.unit;
    });
    Serialize::container(stream, data.inputsGeneral, [&stream](const ChainData::InputGeneral &in) {
        stream << in.unit;
    });
    stream << data.outputs << data.outputsFlags << data.outputsGeneral;
    for (const auto &sig : data.nodes) {
        sig->serialize(stream);
    }
}

#ifndef NDEBUG

template<typename ListType>
static bool checkDeserializeList(const ListType &list, const std::vector<SequenceName> &expected)
{
    if (list.size() != expected.size())
        return false;
    return std::equal(expected.begin(), expected.end(), list.begin(),
                      [](const SequenceName &a, typename ListType::const_reference &b) {
                          return a == b.unit;
                      });
}

#endif

void SmoothingEngine::Chain::deserializeChain(ChainData &data,
                                              SmoothingEngine::EngineInterface *interface,
                                              QDataStream &stream)
{
    interface->target = &data;
    if (interface->auxiliary)
        data.auxiliary = std::move(interface->auxiliary);
    interface->discardTime = FP::undefined();

    std::vector<SequenceName> units;

    stream >> units;
    for (const auto &add : units) {
        interface->addNewInput(add);
    }
#ifndef NDEBUG
    Q_ASSERT(checkDeserializeList(data.inputs, units));
#endif

    stream >> units;
    for (const auto &add : units) {
        interface->addNewFlagsInput(add);
    }
#ifndef NDEBUG
    Q_ASSERT(checkDeserializeList(data.inputsFlags, units));
#endif

    stream >> units;
    for (const auto &add : units) {
        interface->addNewGeneralInput(add);
    }
#ifndef NDEBUG
    Q_ASSERT(checkDeserializeList(data.inputsGeneral, units));
#endif

    stream >> units;
    for (const auto &add : units) {
        interface->addNewOutput(add);
    }
    Q_ASSERT(data.outputs == units);

    stream >> units;
    for (const auto &add : units) {
        interface->addNewFlagsOutput(add);
    }
    Q_ASSERT(data.outputsFlags == units);

    stream >> units;
    for (const auto &add : units) {
        interface->addNewGeneralOutput(add);
    }
    Q_ASSERT(data.outputsGeneral == units);
}

SmoothingEngine::Chain *SmoothingEngine::Chain::deserialize(SmoothingEngine *engine,
                                                            QDataStream &stream)
{
    quint8 i8;
    stream >> i8;
    switch (static_cast<ChainType>(i8)) {
    case Chain_GeneralBypass: {
        auto chain = new ChainGeneralBypass;
        chain->restore(engine, stream);
        return chain;
    }

    case Chain_Basic: {
        auto chain = new ChainBasic;
        chain->restore(engine, stream);
        return chain;
    }

    case Chain_Flags: {
        auto chain = new ChainFlags;
        chain->restore(engine, stream);
        return chain;
    }

    case Chain_Array: {
        auto chain = new ChainArray;
        chain->restore(engine, stream);
        return chain;
    }

    case Chain_Matrix: {
        auto chain = new ChainMatrix;
        chain->restore(engine, stream);
        return chain;
    }
    }
    Q_ASSERT(false);
    return nullptr;
}

SmoothingEngine::ChainGeneralBypass::~ChainGeneralBypass() = default;

void SmoothingEngine::ChainGeneralBypass::unregisterUnit(DispatchData *dispatch,
                                                         const SequenceName &unit)
{
    if (this->unit != unit)
        return;
    node->endData();
    eraseFromList(dispatch->targetsGeneral, static_cast<SmootherChainTargetGeneral *>(node.get()));
}

Data::SequenceName::Set SmoothingEngine::ChainGeneralBypass::remainingReferencedUnits()
{ return Data::SequenceName::Set(); }

bool SmoothingEngine::ChainGeneralBypass::checkCompleted()
{ return node->finished(); }

void SmoothingEngine::ChainGeneralBypass::signalTerminate()
{ node->signalTerminate(); }

void SmoothingEngine::ChainGeneralBypass::pause()
{ }

void SmoothingEngine::ChainGeneralBypass::resume()
{ }

void SmoothingEngine::ChainGeneralBypass::cleanup()
{ }

void SmoothingEngine::ChainGeneralBypass::serialize(QDataStream &stream) const
{
    stream << (quint8) Chain_GeneralBypass;
    stream << unit;
}

void SmoothingEngine::ChainGeneralBypass::restore(SmoothingEngine *engine, QDataStream &stream)
{
    stream >> unit;
    DispatchTarget *dtBase = engine->dispatchTarget(unit);
    Q_ASSERT(dtBase->getType() == SmoothingEngine::Type_Value);
    auto d = static_cast<DispatchData *>(dtBase);
    node.reset(new SmoothingEngine::GeneralReconstruct(d->getOutputUnit(),
                                                       engine->createDataIngress(unit)));
    d->targetsGeneral.push_back(node.get());
}

SmoothingEngine::ChainBasic::~ChainBasic() = default;

void SmoothingEngine::ChainBasic::unregisterUnit(DispatchData *dispatch, const SequenceName &unit)
{ unreferenceAll(dispatch, unit, data); }

Data::SequenceName::Set SmoothingEngine::ChainBasic::remainingReferencedUnits()
{ return definedInputs(data); }

bool SmoothingEngine::ChainBasic::checkCompleted()
{ return isCompleted(data.nodes); }

void SmoothingEngine::ChainBasic::signalTerminate()
{ sendTerminate(data.nodes); }

void SmoothingEngine::ChainBasic::pause()
{ pauseChain(data.nodes); }

void SmoothingEngine::ChainBasic::resume()
{ resumeChain(data.nodes); }

void SmoothingEngine::ChainBasic::cleanup()
{ cleanupChain(data.nodes); }

void SmoothingEngine::ChainBasic::serialize(QDataStream &stream) const
{
    stream << (quint8) Chain_Basic << (quint8) type;
    serializeChain(data, stream);
}

void SmoothingEngine::ChainBasic::restore(SmoothingEngine *engine, QDataStream &stream)
{
    quint8 i8;
    stream >> i8;
    type = (SmootherChainCore::Type) i8;

    auto interface = engine->createEngineInterface();
    deserializeChain(data, interface.get(), stream);

    std::vector<SequenceName> names;
    for (const auto &add : data.inputs) {
        names.push_back(add.unit);
    }
    for (const auto &add : data.inputsFlags) {
        names.push_back(add.unit);
    }
    for (const auto &add : data.inputsGeneral) {
        names.push_back(add.unit);
    }

    engine->deserializeChain(names, stream, interface.get(), type);
}


SmoothingEngine::ChainFlags::~ChainFlags() = default;

void SmoothingEngine::ChainFlags::unregisterUnit(DispatchData *dispatch, const SequenceName &unit)
{ unreferenceAll(dispatch, unit, data); }

Data::SequenceName::Set SmoothingEngine::ChainFlags::remainingReferencedUnits()
{ return definedInputs(data); }

bool SmoothingEngine::ChainFlags::checkCompleted()
{ return isCompleted(data.nodes); }

void SmoothingEngine::ChainFlags::signalTerminate()
{ sendTerminate(data.nodes); }

void SmoothingEngine::ChainFlags::pause()
{ pauseChain(data.nodes); }

void SmoothingEngine::ChainFlags::resume()
{ resumeChain(data.nodes); }

void SmoothingEngine::ChainFlags::cleanup()
{ cleanupChain(data.nodes); }

void SmoothingEngine::ChainFlags::serialize(QDataStream &stream) const
{
    stream << (quint8) Chain_Flags;
    serializeChain(data, stream);
}

void SmoothingEngine::ChainFlags::restore(SmoothingEngine *engine, QDataStream &stream)
{
    auto interface = engine->createEngineInterface();
    deserializeChain(data, interface.get(), stream);

    std::vector<SequenceName> names;
    for (const auto &add : data.inputs) {
        names.push_back(add.unit);
    }
    for (const auto &add : data.inputsFlags) {
        names.push_back(add.unit);
    }
    for (const auto &add : data.inputsGeneral) {
        names.push_back(add.unit);
    }

    engine->deserializeFlagsChain(names, stream, interface.get());
}


SmoothingEngine::ChainArray::~ChainArray() = default;

void SmoothingEngine::ChainArray::unregisterUnit(DispatchData *dispatch, const SequenceName &unit)
{
    int arrayIndex = 0;
    for (auto &idx : indices) {
        unreferenceAll(dispatch, unit, idx, arrayIndex);
        ++arrayIndex;
    }
}

Data::SequenceName::Set SmoothingEngine::ChainArray::remainingReferencedUnits()
{
    Data::SequenceName::Set result;
    for (const auto &idx : indices) {
        for (const auto &add : definedInputs(idx)) {
            result.insert(add);
        }
    }
    return result;
}

bool SmoothingEngine::ChainArray::checkCompleted()
{
    if (!isCompleted(data.sharedNodes))
        return false;
    for (const auto &idx : indices) {
        if (!isCompleted(idx.nodes))
            return false;
    }
    return true;
}

void SmoothingEngine::ChainArray::signalTerminate()
{
    for (const auto &idx : indices) {
        sendTerminate(idx.nodes);
    }
    sendTerminate(data.sharedNodes);
}

void SmoothingEngine::ChainArray::pause()
{
    for (const auto &idx : indices) {
        pauseChain(idx.nodes);
    }
    pauseChain(data.sharedNodes);
}

void SmoothingEngine::ChainArray::resume()
{
    for (const auto &idx : indices) {
        resumeChain(idx.nodes);
    }
    resumeChain(data.sharedNodes);
}

void SmoothingEngine::ChainArray::cleanup()
{
    for (const auto &idx : indices) {
        cleanupChain(idx.nodes);
    }
    cleanupChain(data.sharedNodes);
}

template<typename HashType>
static void serializeArrayHash(const HashType &h, QDataStream &stream)
{
    stream << static_cast<quint32>(h.size());
    for (const auto &s : h) {
        stream << s.first;
        s.second->serialize(stream);
    }
}

void SmoothingEngine::ChainArray::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Chain_Array) << static_cast<quint8>(type);

    serializeArrayHash(data.outputsBasic, stream);
    serializeArrayHash(data.outputsFlags, stream);
    serializeArrayHash(data.outputsGeneral, stream);

    stream << static_cast<quint32>(indices.size());
    for (const auto &idx : indices) {
        serializeChain(idx, stream);
    }
}

void SmoothingEngine::ChainArray::restore(SmoothingEngine *engine, QDataStream &stream)
{
    {
        quint8 i8;
        stream >> i8;
        type = static_cast<SmootherChainCore::Type>(i8);
    }
    quint32 n;

    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        SequenceName unit;
        stream >> unit;
        auto add =
                new SmoothingEngineArrayMergeBasic(stream, unit, engine->createDataIngress(unit));
        data.outputsBasic.emplace(unit, add);
        data.sharedNodes.emplace_back(add);
    }

    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        SequenceName unit;
        stream >> unit;
        auto add =
                new SmoothingEngineArrayMergeFlags(stream, unit, engine->createDataIngress(unit));
        data.outputsFlags.emplace(unit, add);
        data.sharedNodes.emplace_back(add);
    }

    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        SequenceName unit;
        stream >> unit;
        auto add =
                new SmoothingEngineArrayMergeGeneral(stream, unit, engine->createDataIngress(unit));
        data.outputsGeneral.emplace(unit, add);
        data.sharedNodes.emplace_back(add);
    }

    stream >> n;
    indices.resize(n);
    data.size = static_cast<int>(n);
    for (quint32 i = 0; i < n; i++) {
        auto interface = engine->createEngineInterface();
        interface->targetArray = &data;
        interface->arrayIndex = static_cast<std::size_t>(i);
        deserializeChain(indices[i], interface.get(), stream);

        std::vector<SequenceName> names;
        for (const auto &add : indices[i].inputs) {
            names.push_back(add.unit);
        }
        for (const auto &add : indices[i].inputsFlags) {
            names.push_back(add.unit);
        }
        for (const auto &add : indices[i].inputsGeneral) {
            names.push_back(add.unit);
        }

        engine->deserializeChain(names, stream, interface.get(), type);
    }
}


SmoothingEngine::ChainMatrix::~ChainMatrix() = default;

void SmoothingEngine::ChainMatrix::unregisterUnit(DispatchData *dispatch, const SequenceName &unit)
{
    int matrixIndex = 0;
    for (auto &idx : indices) {
        unreferenceAll(dispatch, unit, idx, matrixIndex);
        ++matrixIndex;
    }
}

Data::SequenceName::Set SmoothingEngine::ChainMatrix::remainingReferencedUnits()
{
    Data::SequenceName::Set result;
    for (const auto &idx : indices) {
        for (const auto &add : definedInputs(idx)) {
            result.insert(add);
        }
    }
    return result;
}

bool SmoothingEngine::ChainMatrix::checkCompleted()
{
    if (!isCompleted(data.sharedNodes))
        return false;
    for (const auto &idx : indices) {
        if (!isCompleted(idx.nodes))
            return false;
    }
    return true;
}

void SmoothingEngine::ChainMatrix::signalTerminate()
{
    for (const auto &idx : indices) {
        sendTerminate(idx.nodes);
    }
    sendTerminate(data.sharedNodes);
}

void SmoothingEngine::ChainMatrix::pause()
{
    for (const auto &idx : indices) {
        pauseChain(idx.nodes);
    }
    pauseChain(data.sharedNodes);
}

void SmoothingEngine::ChainMatrix::resume()
{
    for (const auto &idx : indices) {
        resumeChain(idx.nodes);
    }
    resumeChain(data.sharedNodes);
}

void SmoothingEngine::ChainMatrix::cleanup()
{
    for (const auto &idx : indices) {
        cleanupChain(idx.nodes);
    }
    cleanupChain(data.sharedNodes);
}

void SmoothingEngine::ChainMatrix::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Chain_Matrix) << static_cast<quint8>(type);

    serializeArrayHash(data.outputsBasic, stream);
    serializeArrayHash(data.outputsFlags, stream);
    serializeArrayHash(data.outputsGeneral, stream);

    Q_ASSERT(data.size.size() <= 255);
    stream << static_cast<quint8>(data.size.size());
    for (auto add : data.size) {
        stream << static_cast<quint32>(add);
    }

    stream << static_cast<quint32>(indices.size());
    for (const auto &idx : indices) {
        serializeChain(idx, stream);
    }
}

void SmoothingEngine::ChainMatrix::restore(SmoothingEngine *engine, QDataStream &stream)
{
    {
        quint8 i8;
        stream >> i8;
        type = static_cast<SmootherChainCore::Type>(i8);
    }
    quint32 n;

    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        SequenceName unit;
        stream >> unit;
        auto add =
                new SmoothingEngineMatrixMergeBasic(stream, unit, engine->createDataIngress(unit));
        data.outputsBasic.emplace(unit, add);
        data.sharedNodes.emplace_back(add);
    }

    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        SequenceName unit;
        stream >> unit;
        auto add =
                new SmoothingEngineMatrixMergeFlags(stream, unit, engine->createDataIngress(unit));
        data.outputsFlags.emplace(unit, add);
        data.sharedNodes.emplace_back(add);
    }

    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        SequenceName unit;
        stream >> unit;
        auto add = new SmoothingEngineMatrixMergeGeneral(stream, unit,
                                                         engine->createDataIngress(unit));
        data.outputsGeneral.emplace(unit, add);
        data.sharedNodes.emplace_back(add);
    }

    quint8 nDims;
    stream >> nDims;
    data.size.clear();
    for (quint8 i = 0; i < nDims; i++) {
        quint32 add;
        stream >> add;
        data.size.emplace_back(add);
    }

    stream >> n;
    indices.resize(n);
    for (quint32 i = 0; i < n; i++) {
        auto interface = engine->createEngineInterface();
        interface->targetMatrix = &data;
        interface->arrayIndex = static_cast<std::size_t>(i);
        deserializeChain(indices[i], interface.get(), stream);

        std::vector<SequenceName> names;
        for (const auto &add : indices[i].inputs) {
            names.push_back(add.unit);
        }
        for (const auto &add : indices[i].inputsFlags) {
            names.push_back(add.unit);
        }
        for (const auto &add : indices[i].inputsGeneral) {
            names.push_back(add.unit);
        }

        engine->deserializeChain(names, stream, interface.get(), type);
    }
}


namespace {
template<class CoreType>
class SmoothingEngineDigitalFilterTimeConstant : public SmoothingEngine {
    DynamicTimeInterval *timeConstant;
    DynamicTimeInterval *gap;
    bool undefinedReset;
    QByteArray serializeRawData;
public:
    SmoothingEngineDigitalFilterTimeConstant(DynamicTimeInterval *tc, DynamicTimeInterval *setGap,
                                             bool resetUndefined,
                                             const QByteArray &srd) : SmoothingEngine(
            new CoreType(tc, setGap, resetUndefined)),
                                                                      timeConstant(tc),
                                                                      gap(setGap),
                                                                      undefinedReset(
                                                                              resetUndefined),
                                                                      serializeRawData(srd)
    { }

    SmoothingEngineDigitalFilterTimeConstant() : SmoothingEngine(nullptr),
                                                 timeConstant(nullptr),
                                                 gap(nullptr),
                                                 undefinedReset(false)
    { }

    virtual ~SmoothingEngineDigitalFilterTimeConstant() = default;

    virtual void deserialize(QDataStream &stream)
    {
        stream >> timeConstant >> gap >> undefinedReset;
        changeCore(new CoreType(timeConstant, gap, undefinedReset));
        SmoothingEngine::deserialize(stream);
    }

    virtual void serialize(QDataStream &stream)
    {
        pauseProcessing();
        if (!serializeRawData.isEmpty()) {
            stream.writeRawData(serializeRawData.data(), serializeRawData.length());
        }
        stream << timeConstant << gap << undefinedReset;
        serializeInternal(stream);
        resumeProcessing();
    }
};
}

SmoothingEngine *SmoothingEngine::createSinglePoleLowPassDigitalFilter(DynamicTimeInterval *tc,
                                                                       DynamicTimeInterval *setGap,
                                                                       bool resetUndefined,
                                                                       const QByteArray &serializeRawData)
{
    return new SmoothingEngineDigitalFilterTimeConstant<SmootherChainCoreSinglePoleLowPass>(tc,
                                                                                            setGap,
                                                                                            resetUndefined,
                                                                                            serializeRawData);
}

SmoothingEngine *SmoothingEngine::deserializeSinglePoleLowPassDigitalFilter(QDataStream &stream)
{
    auto engine = new SmoothingEngineDigitalFilterTimeConstant<SmootherChainCoreSinglePoleLowPass>;
    engine->deserialize(stream);
    return engine;
}

SmoothingEngine *SmoothingEngine::createFourPoleLowPassDigitalFilter(DynamicTimeInterval *tc,
                                                                     DynamicTimeInterval *setGap,
                                                                     bool resetUndefined,
                                                                     const QByteArray &serializeRawData)
{
    return new SmoothingEngineDigitalFilterTimeConstant<SmootherChainCoreFourPoleLowPass>(tc,
                                                                                          setGap,
                                                                                          resetUndefined,
                                                                                          serializeRawData);
}

SmoothingEngine *SmoothingEngine::deserializeFourPoleLowPassDigitalFilter(QDataStream &stream)
{
    auto engine = new SmoothingEngineDigitalFilterTimeConstant<SmootherChainCoreFourPoleLowPass>;
    engine->deserialize(stream);
    return engine;
}

namespace {
class SmoothingEngineDigitalFilterGeneral : public SmoothingEngine {
    QVector<double> a;
    QVector<double> b;
    DynamicTimeInterval *gap;
    bool undefinedReset;
    QByteArray serializeRawData;
public:
    SmoothingEngineDigitalFilterGeneral(const QVector<double> &setA,
                                        const QVector<double> &setB, DynamicTimeInterval *setGap,
                                        bool resetUndefined,
                                        QByteArray srd) : SmoothingEngine(
            new SmootherChainCoreGeneralDigitalFilter(setA, setB, setGap, resetUndefined)),
                                                          a(setA),
                                                          b(setB),
                                                          gap(setGap),
                                                          undefinedReset(resetUndefined),
                                                          serializeRawData(std::move(srd))
    { }

    SmoothingEngineDigitalFilterGeneral() : SmoothingEngine(nullptr),
                                            a(),
                                            b(), gap(nullptr),
                                            undefinedReset(false)
    { }

    virtual ~SmoothingEngineDigitalFilterGeneral() = default;

    virtual void deserialize(QDataStream &stream)
    {
        stream >> a >> b >> gap >> undefinedReset;
        changeCore(new SmootherChainCoreGeneralDigitalFilter(a, b, gap, undefinedReset));
        SmoothingEngine::deserialize(stream);
    }

    virtual void serialize(QDataStream &stream)
    {
        pauseProcessing();
        if (!serializeRawData.isEmpty()) {
            stream.writeRawData(serializeRawData.data(), serializeRawData.length());
        }
        stream << a << b << gap << undefinedReset;
        serializeInternal(stream);
        resumeProcessing();
    }
};
}

SmoothingEngine *SmoothingEngine::createGeneralDigitalFilter(const QVector<double> &setA,
                                                             const QVector<double> &setB,
                                                             DynamicTimeInterval *setGap,
                                                             bool resetUndefined,
                                                             const QByteArray &serializeRawData)
{
    return new SmoothingEngineDigitalFilterGeneral(setA, setB, setGap, resetUndefined,
                                                   serializeRawData);
}

SmoothingEngine *SmoothingEngine::deserializeGeneralDigitalFilter(QDataStream &stream)
{
    SmoothingEngineDigitalFilterGeneral *engine = new SmoothingEngineDigitalFilterGeneral;
    engine->deserialize(stream);
    return engine;
}

namespace {
class SmoothingEngineDigitalFilterWrapper : public SmoothingEngine {
    DigitalFilter *filter;
    QByteArray serializeRawData;
public:
    SmoothingEngineDigitalFilterWrapper(DigitalFilter *wrapFilter, QByteArray srd)
            : SmoothingEngine(new SmootherChainCoreDigitalFilterWrapper(wrapFilter)),
              filter(wrapFilter), serializeRawData(std::move(srd))
    { }

    SmoothingEngineDigitalFilterWrapper() : SmoothingEngine(nullptr), filter(nullptr)
    { }

    virtual ~SmoothingEngineDigitalFilterWrapper() = default;

    virtual void deserialize(QDataStream &stream)
    {
        stream >> filter;
        changeCore(new SmootherChainCoreDigitalFilterWrapper(filter));
        SmoothingEngine::deserialize(stream);
    }

    virtual void serialize(QDataStream &stream)
    {
        pauseProcessing();
        if (!serializeRawData.isEmpty()) {
            stream.writeRawData(serializeRawData.data(), serializeRawData.length());
        }
        stream << filter;
        serializeInternal(stream);
        resumeProcessing();
    }
};
}

SmoothingEngine *SmoothingEngine::createDigitalFilterWrapper(DigitalFilter *wrapFilter,
                                                             const QByteArray &serializeRawData)
{
    return new SmoothingEngineDigitalFilterWrapper(wrapFilter, serializeRawData);
}

SmoothingEngine *SmoothingEngine::deserializeDigitalFilterWrapper(QDataStream &stream)
{
    auto engine = new SmoothingEngineDigitalFilterWrapper;
    engine->deserialize(stream);
    return engine;
}

namespace {
class SmoothingEngineTukey3RSSH : public SmoothingEngine {
    DynamicTimeInterval *gap;
    bool smoothOverUndefined;
    QByteArray serializeRawData;
public:
    SmoothingEngineTukey3RSSH(DynamicTimeInterval *setGap,
                              bool setSmoothUndefined,
                              QByteArray srd) : SmoothingEngine(
            new SmootherChainCoreTukey3RSSH(setGap, setSmoothUndefined), false),
                                                gap(setGap),
                                                smoothOverUndefined(setSmoothUndefined),
                                                serializeRawData(std::move(srd))
    { }

    SmoothingEngineTukey3RSSH() : SmoothingEngine(nullptr), gap(nullptr), smoothOverUndefined(false)
    { }

    virtual ~SmoothingEngineTukey3RSSH() = default;

    virtual void deserialize(QDataStream &stream)
    {
        stream >> gap >> smoothOverUndefined;
        changeCore(new SmootherChainCoreTukey3RSSH(gap, smoothOverUndefined));
        SmoothingEngine::deserialize(stream);
    }

    virtual void serialize(QDataStream &stream)
    {
        pauseProcessing();
        if (!serializeRawData.isEmpty()) {
            stream.writeRawData(serializeRawData.data(), serializeRawData.length());
        }
        stream << gap << smoothOverUndefined;
        serializeInternal(stream);
        resumeProcessing();
    }
};
}

SmoothingEngine *SmoothingEngine::createTukey3RSSH(DynamicTimeInterval *setGap,
                                                   bool setSmoothUndefined,
                                                   const QByteArray &serializeRawData)
{
    return new SmoothingEngineTukey3RSSH(setGap, setSmoothUndefined, serializeRawData);
}

SmoothingEngine *SmoothingEngine::deserializeTukey3RSSH(QDataStream &stream)
{
    auto engine = new SmoothingEngineTukey3RSSH;
    engine->deserialize(stream);
    return engine;
}

namespace {
class SmoothingEngineFourier : public SmoothingEngine {
    DynamicTimeInterval *low;
    DynamicTimeInterval *high;
    DynamicTimeInterval *gap;
    bool smoothOverUndefined;
    QByteArray serializeRawData;
public:
    SmoothingEngineFourier(DynamicTimeInterval *setLow,
                           DynamicTimeInterval *setHigh,
                           DynamicTimeInterval *setGap,
                           bool setSmoothUndefined,
                           QByteArray srd)
            : SmoothingEngine(
            new SmootherChainCoreFourier(setLow, setHigh, setGap, setSmoothUndefined), false),
              low(setLow),
              high(setHigh),
              gap(setGap),
              smoothOverUndefined(setSmoothUndefined), serializeRawData(std::move(srd))
    { }

    SmoothingEngineFourier() : SmoothingEngine(nullptr), low(nullptr), high(nullptr), gap(nullptr),
                               smoothOverUndefined(false)
    { }

    virtual ~SmoothingEngineFourier() = default;

    virtual void deserialize(QDataStream &stream)
    {
        stream >> low >> high >> gap >> smoothOverUndefined;
        changeCore(new SmootherChainCoreFourier(low, high, gap, smoothOverUndefined));
        SmoothingEngine::deserialize(stream);
    }

    virtual void serialize(QDataStream &stream)
    {
        pauseProcessing();
        if (!serializeRawData.isEmpty()) {
            stream.writeRawData(serializeRawData.data(), serializeRawData.length());
        }
        stream << low << high << gap << smoothOverUndefined;
        serializeInternal(stream);
        resumeProcessing();
    }
};
}

SmoothingEngine *SmoothingEngine::createFourier(DynamicTimeInterval *setLow,
                                                DynamicTimeInterval *setHigh,
                                                DynamicTimeInterval *setGap,
                                                bool setSmoothUndefined,
                                                const QByteArray &serializeRawData)
{
    return new SmoothingEngineFourier(setLow, setHigh, setGap, setSmoothUndefined,
                                      serializeRawData);
}

SmoothingEngine *SmoothingEngine::deserializeFourier(QDataStream &stream)
{
    auto engine = new SmoothingEngineFourier;
    engine->deserialize(stream);
    return engine;
}

}
}
