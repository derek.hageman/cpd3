/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGDIGITALFILTER_H
#define CPD3SMOOTHINGDIGITALFILTER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDataStream>
#include <QDebug>

#include "smoothing/smoothing.hxx"
#include "core/timeutils.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/variant/root.hxx"

namespace CPD3 {
namespace Smoothing {

class DigitalFilter;

CPD3SMOOTHING_EXPORT QDataStream &operator<<(QDataStream &stream, const DigitalFilter *filter);

CPD3SMOOTHING_EXPORT QDataStream &operator>>(QDataStream &stream, DigitalFilter *&filter);

CPD3SMOOTHING_EXPORT QDebug operator<<(QDebug stream, const DigitalFilter *filter);

/**
 * The general implementation for a digital filter.
 */
class CPD3SMOOTHING_EXPORT DigitalFilter {
public:
    virtual ~DigitalFilter();

    /**
     * Apply the digital filter to the given value at a known (non-infinite)
     * time.
     * 
     * @param time  the time of the value
     * @param value the value to add
     * @return      the smoothed value
     */
    virtual double apply(double time, double value) = 0;

    /**
     * Force the filter to reset.
     */
    virtual void reset() = 0;

    /**
     * Test if the filter is smoothing.  That is, test if all the points it
     * needs have been filled.
     * 
     * @return true if the filter is currently smoothing
     */
    virtual bool smoothing() const = 0;

    /**
     * Get the start time needed to fully spin up the baseline given an
     * initial start time.
     * 
     * @param initial   the initial start time
     * @return          the time to begin so that the filter is running by the initial time
     */
    virtual double spinupTime(double initial) const;

    /**
     * Create a polymorphic copy of the digital filter.
     * 
     * @return a copy of the filter
     */
    virtual DigitalFilter *clone() const = 0;

    /**
     * Describe the state of the digital filter into a data value
     * 
     * @return a value describing the state
     */
    virtual Data::Variant::Root describeState() const = 0;

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator<<(QDataStream &stream, const DigitalFilter *filter);

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator>>(QDataStream &stream, DigitalFilter *&filter);

    friend CPD3SMOOTHING_EXPORT QDebug operator<<(QDebug stream, const DigitalFilter *filter);

protected:
    /**
     * Serialize the filter into the given data stream.
     * 
     * @param stream the target serialization stream
     */
    virtual void writeSerial(QDataStream &stream) const = 0;

    /**
     * Output the filter to a log stream.
     * 
     * @param stream the target stream
     */
    virtual void printLog(QDebug &stream) const = 0;
};

class DigitalFilterSinglePoleLowPass;

CPD3SMOOTHING_EXPORT QDataStream
        &operator<<(QDataStream &stream, const DigitalFilterSinglePoleLowPass &filter);

CPD3SMOOTHING_EXPORT QDataStream
        &operator>>(QDataStream &stream, DigitalFilterSinglePoleLowPass &filter);

CPD3SMOOTHING_EXPORT QDebug operator<<(QDebug stream, const DigitalFilterSinglePoleLowPass &filter);

/**
 * A single pole low pass digital filter implementation.
 */
class CPD3SMOOTHING_EXPORT DigitalFilterSinglePoleLowPass : public virtual DigitalFilter {
    Data::DynamicTimeInterval *gap;
    Data::DynamicTimeInterval *timeConstantInterval;
    double previousTC;
    double a;
    double b;
    double previousValue;
    double previousTime;
    double previousInterval;
    bool resetOnUndefined;

public:
    /**
     * Create a new single pole low pass filter.  This takes ownership of the
     * time constant specification and gap.
     * 
     * @param tc                time time constant specification
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     */
    DigitalFilterSinglePoleLowPass(Data::DynamicTimeInterval *tc,
                                   Data::DynamicTimeInterval *setGap = NULL,
                                   bool resetUndefined = true);

    /**
     * Create a single pole low pass filter with the given time constant
     * and gap.
     * 
     * @param count         the number of units in the time constant
     * @param unit          the unit of the time constant
     * @param setGap        the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     */
    DigitalFilterSinglePoleLowPass(int count = 3,
                                   Time::LogicalTimeUnit unit = Time::Minute,
                                   Data::DynamicTimeInterval *setGap = NULL,
                                   bool resetUndefined = true);

    /**
     * Create a new single pole low pass filter from saved state.
     * 
     * @param stream            the stream to read state from
     * @param outputTarget      the output target for smoothed values
     */
    DigitalFilterSinglePoleLowPass(QDataStream &stream);

    DigitalFilterSinglePoleLowPass(const DigitalFilterSinglePoleLowPass &other);

    DigitalFilterSinglePoleLowPass &operator=(const DigitalFilterSinglePoleLowPass &other);

    virtual ~DigitalFilterSinglePoleLowPass();

    virtual double apply(double time, double value);

    virtual void reset();

    virtual bool smoothing() const;

    virtual double spinupTime(double initial) const;

    virtual DigitalFilter *clone() const;

    virtual Data::Variant::Root describeState() const;

    /**
     * Save the filter state to the given stream.  This does not write the
     * identifier needed to de-serialize it into a pointer.
     * 
     * @param stream the target stream
     */
    virtual void serialize(QDataStream &stream) const;

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator<<(QDataStream &stream, const DigitalFilterSinglePoleLowPass &filter);

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator>>(QDataStream &stream, DigitalFilterSinglePoleLowPass &filter);

    friend CPD3SMOOTHING_EXPORT QDebug operator<<(QDebug stream,
                                                  const DigitalFilterSinglePoleLowPass &filter);

protected:
    virtual void writeSerial(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

class DigitalFilterGeneral;

CPD3SMOOTHING_EXPORT QDataStream
        &operator<<(QDataStream &stream, const DigitalFilterGeneral &filter);

CPD3SMOOTHING_EXPORT QDataStream &operator>>(QDataStream &stream, DigitalFilterGeneral &filter);

CPD3SMOOTHING_EXPORT QDebug operator<<(QDebug stream, const DigitalFilterGeneral &filter);

/**
 * A generalized digital filter.  This processes data based on a set of
 * "a" and "b" coefficients.
 */
class CPD3SMOOTHING_EXPORT DigitalFilterGeneral : public virtual DigitalFilter {
    Data::DynamicTimeInterval *gap;
    QVector<double> previousInputs;
    QVector<double> previousOutputs;
    double previousTime;
    double previousInterval;
    bool resetOnUndefined;

    double resetFinish(double time, double value);

public:
    /**
     * Create a new general low pass filter.  This takes ownership of the gap
     * specification.
     * 
     * @param setA              the "a" coefficients
     * @param setB              the "b" coefficients
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     */
    DigitalFilterGeneral(const QVector<double> &setA,
                         const QVector<double> &setB, Data::DynamicTimeInterval *setGap = NULL,
                         bool resetUndefined = true);

    /**
     * Create a new single pole low pass filter from saved state.
     * 
     * @param stream            the stream to read state from
     */
    DigitalFilterGeneral(QDataStream &stream);

    virtual ~DigitalFilterGeneral();

    DigitalFilterGeneral(const DigitalFilterGeneral &other);

    DigitalFilterGeneral &operator=(const DigitalFilterGeneral &other);

    virtual double apply(double time, double value);

    virtual void reset();

    virtual bool smoothing() const;

    virtual DigitalFilter *clone() const;

    virtual Data::Variant::Root describeState() const;

    /**
     * Save the filter state to the given stream.  This does not write the
     * identifier needed to de-serialize it into a pointer.
     * 
     * @param stream the target stream
     */
    virtual void serialize(QDataStream &stream) const;

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator<<(QDataStream &stream, const DigitalFilterGeneral &filter);

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator>>(QDataStream &stream, DigitalFilterGeneral &filter);

    friend CPD3SMOOTHING_EXPORT QDebug operator<<(QDebug stream,
                                                  const DigitalFilterGeneral &filter);

protected:
    /** The "a" coefficients. */
    QVector<double> a;
    /** The "b" coefficients. */
    QVector<double> b;

    virtual void writeSerial(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;

    /** 
     * Get the time of the last valid value.
     * @return the time of the last valid value
     */
    inline double getPreviousTime() const
    { return previousTime; }

    /** 
     * Get the interval between the last valid values
     * @return the interval between the last valid values
     */
    inline double getPreviousInterval() const
    { return previousInterval; }
};

class DigitalFilterFourPoleLowPass;

CPD3SMOOTHING_EXPORT QDataStream
        &operator<<(QDataStream &stream, const DigitalFilterFourPoleLowPass &filter);

CPD3SMOOTHING_EXPORT QDataStream
        &operator>>(QDataStream &stream, DigitalFilterFourPoleLowPass &filter);

CPD3SMOOTHING_EXPORT QDebug operator<<(QDebug stream, const DigitalFilterFourPoleLowPass &filter);

/**
 * A four pole low pass digital filter.
 */
class CPD3SMOOTHING_EXPORT DigitalFilterFourPoleLowPass : public DigitalFilterGeneral {
    Data::DynamicTimeInterval *timeConstantInterval;
    double previousTC;

public:
    /**
     * Create a new single pole low pass filter.  This takes ownership of the
     * time constant specification and gap.
     * 
     * @param tc                time time constant specification
     * @param setGap            the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     */
    DigitalFilterFourPoleLowPass(Data::DynamicTimeInterval *tc,
                                 Data::DynamicTimeInterval *setGap = NULL,
                                 bool resetUndefined = true);

    /**
     * Create a new four pole low pass filter from saved state.
     * 
     * @param stream            the stream to read state from
     */
    DigitalFilterFourPoleLowPass(QDataStream &stream);

    /**
     * Create a four pole low pass filter with the given time constant
     * and gap.
     * 
     * @param count         the number of units in the time constant
     * @param unit          the unit of the time constant
     * @param setGap        the maximum time to allow gaps or NULL for none
     * @param resetUndefined    reset the filter on any undefined value
     */
    DigitalFilterFourPoleLowPass(int count = 3,
                                 Time::LogicalTimeUnit unit = Time::Minute,
                                 Data::DynamicTimeInterval *setGap = NULL,
                                 bool resetUndefined = true);

    virtual ~DigitalFilterFourPoleLowPass();

    DigitalFilterFourPoleLowPass(const DigitalFilterFourPoleLowPass &other);

    DigitalFilterFourPoleLowPass &operator=(const DigitalFilterFourPoleLowPass &other);

    virtual double apply(double time, double value);

    virtual double spinupTime(double initial) const;

    virtual DigitalFilter *clone() const;

    virtual Data::Variant::Root describeState() const;

    /**
     * Save the filter state to the given stream.  This does not write the
     * identifier needed to de-serialize it into a pointer.
     * 
     * @param stream the target stream
     */
    virtual void serialize(QDataStream &stream) const;

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator<<(QDataStream &stream, const DigitalFilterFourPoleLowPass &filter);

    friend CPD3SMOOTHING_EXPORT QDataStream
            &operator>>(QDataStream &stream, DigitalFilterFourPoleLowPass &filter);

    friend CPD3SMOOTHING_EXPORT QDebug operator<<(QDebug stream,
                                                  const DigitalFilterFourPoleLowPass &filter);

protected:
    virtual void writeSerial(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

}
}

#endif
