/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QDebugStateSaver>

#include "smoothing/baseline.hxx"
#include "algorithms/statistics.hxx"
#include "core/util.hxx"

using namespace CPD3::Data;
using namespace CPD3::Algorithms;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/baseline.hxx
 * Provides routines to handle generating a baseline value from incoming
 * values and testing against that baseline for stability. */


enum BaselineType {
    BASELINE_NULL = 0, BASELINE_INVALID, BASELINE_SINGLEPOINT, BASELINE_LATEST,
    BASELINE_FOREVER,
    BASELINE_FIXEDTIME,
    BASELINE_DIGITALFILTER,
    BASELINE_COMPOSITE,
};


BaselineInvalid::BaselineInvalid()
{ }

BaselineInvalid::~BaselineInvalid()
{ }

void BaselineInvalid::add(double value, double start, double end)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    Q_UNUSED(value);
}

void BaselineInvalid::reset()
{ }

bool BaselineInvalid::ready() const
{ return false; }

bool BaselineInvalid::stable() const
{ return false; }

bool BaselineInvalid::spike() const
{ return false; }

double BaselineInvalid::value() const
{ return FP::undefined(); }

Variant::Root BaselineInvalid::describeState() const
{
    Variant::Root result;
    result["Type"].setString("Invalid");
    return result;
}

BaselineSmoother *BaselineInvalid::clone() const
{ return new BaselineInvalid; }

BaselineInvalid::BaselineInvalid(QDataStream &stream)
{ Q_UNUSED(stream); }

void BaselineInvalid::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) BASELINE_INVALID;
    stream << t;
}

void BaselineInvalid::printLog(QDebug &stream) const
{ stream << "BaselineInvalid()"; }


BaselineSinglePoint::BaselineSinglePoint() : latestValue(FP::undefined())
{ }

BaselineSinglePoint::BaselineSinglePoint(const BaselineSinglePoint &other)
        : BaselineSmoother(), latestValue(other.latestValue)
{ }

BaselineSinglePoint::~BaselineSinglePoint()
{ }

void BaselineSinglePoint::add(double value, double start, double end)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    if (!FP::defined(value))
        return;
    latestValue = value;
}

void BaselineSinglePoint::reset()
{ latestValue = FP::undefined(); }

bool BaselineSinglePoint::ready() const
{ return FP::defined(latestValue); }

bool BaselineSinglePoint::stable() const
{ return FP::defined(latestValue); }

bool BaselineSinglePoint::spike() const
{ return false; }

double BaselineSinglePoint::value() const
{ return latestValue; }

Variant::Root BaselineSinglePoint::describeState() const
{
    Variant::Root result;
    result["Type"].setString("SinglePoint");
    result["Value"].setDouble(latestValue);
    return result;
}

BaselineSmoother *BaselineSinglePoint::clone() const
{ return new BaselineSinglePoint(*this); }

BaselineSinglePoint::BaselineSinglePoint(QDataStream &stream)
{ stream >> latestValue; }

void BaselineSinglePoint::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) BASELINE_SINGLEPOINT;
    stream << t << latestValue;
}

void BaselineSinglePoint::printLog(QDebug &stream) const
{ stream << "BaselineSinglePoint(" << latestValue << ')'; }


BaselineLatest::BaselineLatest() : latestValue(FP::undefined())
{ }

BaselineLatest::BaselineLatest(const BaselineLatest &other)
        : BaselineSmoother(), latestValue(other.latestValue)
{ }

BaselineLatest::~BaselineLatest()
{ }

void BaselineLatest::add(double value, double start, double end)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    latestValue = value;
}

void BaselineLatest::reset()
{ latestValue = FP::undefined(); }

bool BaselineLatest::ready() const
{ return FP::defined(latestValue); }

bool BaselineLatest::stable() const
{ return FP::defined(latestValue); }

bool BaselineLatest::spike() const
{ return false; }

double BaselineLatest::value() const
{ return latestValue; }

Variant::Root BaselineLatest::describeState() const
{
    Variant::Root result;
    result["Type"].setString("Latest");
    result["Value"].setDouble(latestValue);
    return result;
}

BaselineSmoother *BaselineLatest::clone() const
{ return new BaselineLatest(*this); }

BaselineLatest::BaselineLatest(QDataStream &stream)
{ stream >> latestValue; }

void BaselineLatest::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) BASELINE_LATEST;
    stream << t << latestValue;
}

void BaselineLatest::printLog(QDebug &stream) const
{ stream << "BaselineLatest(" << latestValue << ')'; }


BaselineForever::BaselineForever() : sum(0), count(0)
{ }

BaselineForever::BaselineForever(const BaselineForever &other)
        : BaselineSmoother(), sum(other.sum), count(other.count)
{ }

BaselineForever::~BaselineForever()
{ }

void BaselineForever::add(double value, double start, double end)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    if (!FP::defined(value))
        return;
    sum += value;
    count++;
}

void BaselineForever::reset()
{
    sum = 0;
    count = 0;
}

bool BaselineForever::ready() const
{ return count > 0; }

bool BaselineForever::stable() const
{ return count > 0; }

bool BaselineForever::spike() const
{ return false; }

double BaselineForever::value() const
{
    if (count <= 0)
        return FP::undefined();
    return sum / (double) count;
}

Variant::Root BaselineForever::describeState() const
{
    Variant::Root result;
    result["Type"].setString("Forever");
    result["Count"].setInt64(count);
    if (count > 0)
        result["Mean"].setDouble(sum / (double) count);
    return result;
}

BaselineSmoother *BaselineForever::clone() const
{ return new BaselineForever(*this); }

BaselineForever::BaselineForever(QDataStream &stream)
{
    stream >> sum;
    quint32 i;
    stream >> i;
    count = (int) i;
}

void BaselineForever::serialize(QDataStream &stream) const
{ stream << ((quint8) BASELINE_FOREVER) << sum << (quint32) count; }

void BaselineForever::printLog(QDebug &stream) const
{ stream << "BaselineForever(" << count << "," << sum << ')'; }


/**
 * Create a new fixed time smoother.
 * 
 * @param setRequireTime    the minimum time required to be ready
 * @param setMaximumTime    the maximum amount of time considered
 * @param setStableRSD      the maximum RSD to consider stable
 * @param setSpikeBand      the band to consider spikes (or negative for only positive spikes)
 * @param setWaitTime       the time to wait after a reset before adding any values
 */
BaselineFixedTime::BaselineFixedTime(double setRequireTime,
                                     double setMaximumTime,
                                     double setStableRSD,
                                     double setSpikeBand,
                                     double setWaitTime)
        : BaselineSmoother(),
          values(),
          startTimes(),
          haveFullTimes(false),
          firstTime(FP::undefined()),
          requireTime(setRequireTime),
          maximumTime(setMaximumTime),
          stableRSD(setStableRSD),
          spikeBand(setSpikeBand),
          waitTime(setWaitTime)
{
    Q_ASSERT(FP::defined(requireTime) && requireTime > 0.0);
    Q_ASSERT(FP::defined(maximumTime) && maximumTime >= requireTime);
    Q_ASSERT(!FP::defined(stableRSD) || stableRSD >= 0.0);
    Q_ASSERT(!FP::defined(waitTime) || waitTime >= 0.0);
}

BaselineFixedTime::BaselineFixedTime(const BaselineFixedTime &other)
        : BaselineSmoother(),
          values(other.values),
          startTimes(other.startTimes),
          haveFullTimes(other.haveFullTimes),
          firstTime(other.firstTime),
          requireTime(other.requireTime),
          maximumTime(other.maximumTime),
          stableRSD(other.stableRSD),
          spikeBand(other.spikeBand),
          waitTime(other.waitTime)
{ }

BaselineFixedTime::~BaselineFixedTime()
{ }

void BaselineFixedTime::add(double value, double start, double end)
{
    Q_UNUSED(end);
    if (!FP::defined(value) || !FP::defined(start))
        return;
    if (!FP::defined(firstTime))
        firstTime = start;
    if (FP::defined(waitTime)) {
        if ((start - firstTime) < waitTime)
            return;
    }
    while (!startTimes.isEmpty() && startTimes.first() < start - maximumTime) {
        startTimes.pop_front();
        values.pop_front();
        haveFullTimes = true;
    }
    startTimes.append(start);
    values.append(value);
    if (!haveFullTimes && startTimes.size() >= 2) {
        if (startTimes.last() - startTimes.first() >= requireTime)
            haveFullTimes = true;
    }
}

void BaselineFixedTime::reset()
{
    startTimes.clear();
    values.clear();
    haveFullTimes = false;
    firstTime = FP::undefined();
}

bool BaselineFixedTime::ready() const
{ return haveFullTimes; }

bool BaselineFixedTime::stable() const
{
    if (!FP::defined(stableRSD)) return true;
    if (!haveFullTimes) return false;
    if (values.size() < 3) return false;
    double mean, sd;
    Statistics::meanSDDefined(values, mean, sd);
    if (!FP::defined(mean) || !FP::defined(sd)) return false;
    if (mean == 0.0)
        return (sd <= stableRSD);
    return (fabs(sd / mean) <= stableRSD);
}

bool BaselineFixedTime::spike() const
{
    if (!FP::defined(spikeBand)) return false;
    if (!haveFullTimes) return false;
    if (values.size() < 2) return false;

    double mean = Statistics::meanDefined(values);
    return !BaselineSmoother::inRelativeBand(mean, values.last(), spikeBand);
}

double BaselineFixedTime::value() const
{ return Statistics::mean(values); }

double BaselineFixedTime::stabilityFactor() const
{
    double mean, sd;
    Statistics::meanSDDefined(values, mean, sd);
    if (mean == 0.0) return sd;
    return sd / mean;
}

double BaselineFixedTime::spinupTime(double initial) const
{
    if (!FP::defined(initial))
        return initial;
    initial -= requireTime;
    if (FP::defined(waitTime))
        initial -= waitTime;
    return initial;
}

Variant::Root BaselineFixedTime::describeState() const
{
    Variant::Root result;
    result["Type"].setString("FixedTime");
    result["Ready"].setBool(haveFullTimes);
    result["FirstTime"].setDouble(firstTime);
    result["StableRSD"].setDouble(stableRSD);
    result["SpikeBand"].setDouble(spikeBand);
    result["WaitTime"].setDouble(waitTime);

    double mean, sd;
    Statistics::meanSD(values, mean, sd);
    result["Mean"].setDouble(mean);
    result["StandardDeviation"].setDouble(sd);

    for (int i = 0, max = values.size(); i < max; i++) {
        result["Values"].array(i).setDouble(values.at(i));
        result["Times"].array(i).setDouble(startTimes.at(i));
    }
    return result;
}

BaselineSmoother *BaselineFixedTime::clone() const
{ return new BaselineFixedTime(*this); }

BaselineFixedTime::BaselineFixedTime(QDataStream &stream)
{
    stream >> values >> startTimes >> haveFullTimes >> firstTime;
    stream >> requireTime >> stableRSD >> spikeBand >> waitTime;
}

void BaselineFixedTime::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) BASELINE_FIXEDTIME;
    stream << t;
    stream << values << startTimes << haveFullTimes << firstTime;
    stream << requireTime << stableRSD << spikeBand << waitTime;
}

void BaselineFixedTime::printLog(QDebug &stream) const
{
    double mean, sd;
    Statistics::meanSD(values, mean, sd);
    stream << "BaselineFixedTime(" << maximumTime << ",Mean=" << mean << ",SD=" << sd << ')';
}


/**
 * Create a new digital filter smoother.
 * 
 * @param setFilter     the digital filter, this takes ownership
 * @param setStableBand the relative band to detect stability
 * @param setSpikeBand  the relative band to detect spikes
 */
BaselineDigitalFilter::BaselineDigitalFilter(DigitalFilter *setFilter, double setStableBand,
                                             double setSpikeBand)
        : BaselineSmoother(),
          filter(setFilter),
          spikeBand(setSpikeBand),
          stableBand(setStableBand),
          previousInput(FP::undefined()),
          previousOutput(FP::undefined()),
          wasSmoothed(false)
{
    Q_ASSERT(!FP::defined(stableBand) || spikeBand > 0.0);
    Q_ASSERT(!FP::defined(spikeBand) || spikeBand > 0.0);
}

BaselineDigitalFilter::BaselineDigitalFilter(const BaselineDigitalFilter &other)
        : BaselineSmoother(),
          filter(other.filter == NULL ? NULL : other.filter->clone()),
          spikeBand(other.spikeBand),
          stableBand(other.stableBand),
          previousInput(other.previousInput),
          previousOutput(other.previousOutput),
          wasSmoothed(other.wasSmoothed)
{ }

BaselineDigitalFilter::~BaselineDigitalFilter()
{
    if (filter != NULL)
        delete filter;
}

void BaselineDigitalFilter::add(double value, double start, double end)
{
    Q_UNUSED(end);
    if (!FP::defined(start))
        return;
    if (!FP::defined(value)) {
        filter->apply(start, value);
        return;
    }
    wasSmoothed = filter->smoothing();
    previousInput = value;
    previousOutput = filter->apply(start, value);
}

void BaselineDigitalFilter::reset()
{
    previousInput = FP::undefined();
    previousOutput = FP::undefined();
    filter->reset();
}

bool BaselineDigitalFilter::ready() const
{ return wasSmoothed; }

bool BaselineDigitalFilter::stable() const
{
    if (!FP::defined(stableBand)) return true;
    if (!wasSmoothed) return false;
    return BaselineSmoother::inRelativeBand(previousOutput, previousInput, stableBand);
}

bool BaselineDigitalFilter::spike() const
{
    if (!FP::defined(spikeBand)) return false;
    if (!wasSmoothed) return false;
    return !BaselineSmoother::inRelativeBand(previousOutput, previousInput, spikeBand);
}

double BaselineDigitalFilter::value() const
{ return previousOutput; }

double BaselineDigitalFilter::stabilityFactor() const
{
    if (!wasSmoothed || !FP::defined(previousInput) || !FP::defined(previousOutput))
        return FP::undefined();
    double nm = qMax(fabs(previousInput), fabs(previousOutput));
    if (nm == 0.0)
        return 0.0;
    return fabs(previousInput - previousOutput) / nm;
}

double BaselineDigitalFilter::spinupTime(double initial) const
{ return filter->spinupTime(initial); }

Variant::Root BaselineDigitalFilter::describeState() const
{
    Variant::Root result = filter->describeState();
    result["LastWasSmoothed"].setBool(wasSmoothed);
    result["LastInput"].setDouble(previousInput);
    result["LastOutput"].setDouble(previousOutput);
    result["StableBand"].setDouble(stableBand);
    result["SpikeBand"].setDouble(spikeBand);
    return result;
}

BaselineSmoother *BaselineDigitalFilter::clone() const
{ return new BaselineDigitalFilter(*this); }

BaselineDigitalFilter::BaselineDigitalFilter(QDataStream &stream)
{
    stream >> filter >> spikeBand >> stableBand >> previousInput >> previousOutput >> wasSmoothed;
}

void BaselineDigitalFilter::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) BASELINE_DIGITALFILTER;
    stream << t;
    stream << filter << spikeBand << stableBand << previousInput << previousOutput << wasSmoothed;
}

void BaselineDigitalFilter::printLog(QDebug &stream) const
{
    stream << "BaselineDigitalFilter(" << filter << ",Input=" << previousInput << ",Output="
           << previousOutput << ')';
}


BaselineComposite::BaselineComposite() : smoothers(), allSmoothers(), validTarget(false)
{ }

BaselineComposite::BaselineComposite(const BaselineComposite &other)
        : BaselineSmoother(),
          smoothers(other.smoothers),
          allSmoothers(other.allSmoothers),
          validTarget(other.validTarget)
{ }

BaselineComposite::~BaselineComposite()
{ }

BaselineComposite::Entry::Entry() : smoother(NULL), start(FP::undefined()), end(FP::undefined())
{ }

BaselineComposite::Entry::~Entry()
{ if (smoother != NULL) delete smoother; }

BaselineComposite::Entry::Entry(BaselineSmoother *setSmoother, double setStart, double setEnd)
        : smoother(setSmoother), start(setStart), end(setEnd)
{ }

BaselineComposite::Entry::Entry(const Entry &other) : smoother(NULL),
                                                      start(other.start),
                                                      end(other.end)
{
    if (other.smoother != NULL) smoother = other.smoother->clone();
}

BaselineComposite::Entry &BaselineComposite::Entry::operator=(const Entry &other)
{
    if (&other == this) return *this;
    if (smoother != NULL) delete smoother;
    if (other.smoother != NULL) smoother = other.smoother->clone(); else smoother = NULL;
    start = other.start;
    end = other.end;
    return *this;
}

BaselineComposite::Entry::Entry(const Entry &other, double setStart, double setEnd) : smoother(
        NULL), start(setStart), end(setEnd)
{
    if (other.smoother != NULL) smoother = other.smoother->clone();
}

BaselineComposite::Entry::Entry(const Entry &under,
                                const Entry &over,
                                double setStart,
                                double setEnd) : smoother(NULL), start(setStart), end(setEnd)
{
    Q_UNUSED(under);
    if (over.smoother != NULL) smoother = over.smoother->clone();
}

/**
 * Add and overlay a given smoother to the composite.  This takes ownership
 * of the smoother.
 * 
 * @param smoother  the smoother to add
 * @param start     the start time to apply at
 * @param end       the end time to apply until
 */
void BaselineComposite::overlay(BaselineSmoother *smoother, double start, double end)
{
    Q_ASSERT(smoother != NULL);
    Range::overlayFragmenting(allSmoothers, Entry(smoother, start, end));
    smoothers = allSmoothers;
    validTarget = false;
}

template<class DigitalFilterCreateType>
static BaselineDigitalFilter *createDigitalFilterWrapper(const Variant::Read &config,
                                                         const Variant::Read &defaultConfig)
{
    DynamicTimeInterval *tc;
    if (config["TimeConstant"].exists()) {
        DynamicTimeInterval::Variable icr;
        icr.set(config["TimeConstant"]);
        tc = DynamicTimeInterval::Variable::reduceConfigurationSegments(icr, FP::undefined(),
                                                                        FP::undefined());
    } else if (defaultConfig["TimeConstant"].exists()) {
        DynamicTimeInterval::Variable icr;
        icr.set(defaultConfig["TimeConstant"]);
        tc = DynamicTimeInterval::Variable::reduceConfigurationSegments(icr, FP::undefined(),
                                                                        FP::undefined());
    } else {
        tc = new DynamicTimeInterval::Constant(Time::Minute, 3, false);
    }

    DynamicTimeInterval *gap = NULL;
    if (config["Gap"].exists()) {
        DynamicTimeInterval::Variable icr;
        icr.set(config["Gap"]);
        gap = DynamicTimeInterval::Variable::reduceConfigurationSegments(icr, FP::undefined(),
                                                                         FP::undefined());
    } else if (defaultConfig["Gap"].exists()) {
        DynamicTimeInterval::Variable icr;
        icr.set(defaultConfig["Gap"]);
        gap = DynamicTimeInterval::Variable::reduceConfigurationSegments(icr, FP::undefined(),
                                                                         FP::undefined());
    }

    bool resetUndefined;
    if (config["ResetOnUndefined"].exists()) {
        resetUndefined = config["ResetOnUndefined"].toBool();
    } else if (defaultConfig["ResetOnUndefined"].exists()) {
        resetUndefined = defaultConfig["ResetOnUndefined"].toBool();
    } else {
        resetUndefined = false;
    }

    double stableBand;
    if (config["StableBand"].getType() == Variant::Type::Real)
        stableBand = config["StableBand"].toDouble();
    else
        stableBand = defaultConfig["StableBand"].toDouble();

    double spikeBand;
    if (config["SpikeBand"].getType() == Variant::Type::Real)
        spikeBand = config["SpikeBand"].toDouble();
    else
        spikeBand = defaultConfig["SpikeBand"].toDouble();

    return new BaselineDigitalFilter(new DigitalFilterCreateType(tc, gap, resetUndefined),
                                     stableBand, spikeBand);
}

void BaselineComposite::overlay(const Variant::Read &config, const Variant::Read &defaultConfig,
                                double start,
                                double end)
{
    if (!config.exists())
        return;
    const auto &type = config["Type"].toString();

    if (Util::equal_insensitive(type, "singlepoint")) {
        overlay(new BaselineSinglePoint, start, end);
    } else if (Util::equal_insensitive(type, "latest")) {
        overlay(new BaselineLatest, start, end);
    } else if (Util::equal_insensitive(type, "disable")) {
        overlay(new BaselineInvalid, start, end);
    } else if (Util::equal_insensitive(type, "forever", "always")) {
        overlay(new BaselineForever, start, end);
    } else if (Util::equal_insensitive(type, "singlepole", "singlepolelowpass", "1p", "1plp")) {
        overlay(createDigitalFilterWrapper<DigitalFilterSinglePoleLowPass>(config, defaultConfig),
                start, end);
    } else if (Util::equal_insensitive(type, "fourpole", "fourpolelowpass", "4p", "4plp")) {
        overlay(createDigitalFilterWrapper<DigitalFilterFourPoleLowPass>(config, defaultConfig),
                start, end);
    } else {
        double maximumTime = config["Time"].toDouble();
        double requireTime = config["MinimumTime"].toDouble();
        if (!FP::defined(requireTime) || requireTime <= 0.0)
            requireTime = maximumTime;
        if (!FP::defined(maximumTime) || maximumTime <= 0.0)
            maximumTime = defaultConfig["Time"].toDouble();
        if (!FP::defined(maximumTime) || maximumTime <= 0.0)
            maximumTime = FP::undefined();
        if (!FP::defined(requireTime) || requireTime <= 0.0)
            requireTime = defaultConfig["MinimumTime"].toDouble();
        if (!FP::defined(requireTime) || requireTime <= 0.0)
            requireTime = maximumTime;
        if (!FP::defined(maximumTime) || maximumTime <= 0.0) {
            if (!FP::defined(requireTime) || requireTime <= 0.0)
                maximumTime = 60.0;
            else
                maximumTime = requireTime;
        }
        if (!FP::defined(requireTime) || requireTime <= 0.0)
            requireTime = maximumTime;
        if (requireTime > maximumTime)
            maximumTime = requireTime;

        double stableRSD;
        if (config["RSD"].getType() == Variant::Type::Real)
            stableRSD = config["RSD"].toDouble();
        else
            stableRSD = defaultConfig["RSD"].toDouble();
        if (stableRSD < 0.0)
            stableRSD = FP::undefined();

        double spikeBand;
        if (config["Band"].getType() == Variant::Type::Real)
            spikeBand = config["Band"].toDouble();
        else
            spikeBand = defaultConfig["Band"].toDouble();
        if (spikeBand == 0.0)
            spikeBand = FP::undefined();

        double discardTime = config["DiscardTime"].toDouble();
        if (!FP::defined(discardTime) || discardTime <= 0.0)
            discardTime = defaultConfig["DiscardTime"].toDouble();
        if (!FP::defined(discardTime) || discardTime <= 0.0)
            discardTime = FP::undefined();

        overlay(new BaselineFixedTime(requireTime, maximumTime, stableRSD, spikeBand, discardTime),
                start, end);
    }
}

BaselineSmoother *BaselineComposite::reduce(const BaselineComposite &in, double start, double end)
{
    QList<Entry>::const_iterator check = Range::findIntersecting(in.allSmoothers, start, end);
    if (check == in.allSmoothers.constEnd())
        return new BaselineInvalid;
    if (Range::compareStart(check->getStart(), start) <= 0 &&
            Range::compareEnd(check->getEnd(), end) >= 0) {
        if (check->smoother == NULL)
            return new BaselineInvalid;
        return check->smoother->clone();
    }
    return in.clone();
}

/**
 * Clear all smoothers in effect.
 */
void BaselineComposite::clear()
{
    allSmoothers.clear();
    smoothers.clear();
    validTarget = false;
}

void BaselineComposite::add(double value, double start, double end)
{
    if (!Range::intersectShift(smoothers, start, end)) {
        validTarget = false;
        return;
    }
    validTarget = true;
    smoothers.first().smoother->add(value, start, end);
}

void BaselineComposite::reset()
{
    smoothers = allSmoothers;
    for (QList<Entry>::iterator it = allSmoothers.begin(), end = allSmoothers.end();
            it != end;
            ++it) {
        it->smoother->reset();
    }
}

bool BaselineComposite::ready() const
{
    if (!validTarget) return false;
    if (smoothers.isEmpty()) return false;
    return smoothers.first().smoother->ready();
}

bool BaselineComposite::stable() const
{
    if (!validTarget) return false;
    if (smoothers.isEmpty()) return false;
    return smoothers.first().smoother->stable();
}

bool BaselineComposite::spike() const
{
    if (!validTarget) return false;
    if (smoothers.isEmpty()) return false;
    return smoothers.first().smoother->spike();
}

double BaselineComposite::value() const
{
    if (!validTarget) return FP::undefined();
    if (smoothers.isEmpty()) return FP::undefined();
    return smoothers.first().smoother->value();
}

double BaselineComposite::stabilityFactor() const
{
    if (!validTarget) return FP::undefined();
    if (smoothers.isEmpty()) return FP::undefined();
    return smoothers.first().smoother->stabilityFactor();
}

Variant::Root BaselineComposite::describeState() const
{
    if (!validTarget || smoothers.isEmpty()) {
        Variant::Root result;
        result["Type"].setString("CompositeInvalid");
        return result;
    }
    return smoothers.first().smoother->describeState();
}

double BaselineComposite::spinupTime(double initial) const
{
    if (!FP::defined(initial))
        return initial;
    QList<Entry>::const_iterator target = Range::findIntersecting(allSmoothers, initial);
    if (target == allSmoothers.end())
        return initial;
    return target->smoother->spinupTime(initial);
}

BaselineSmoother *BaselineComposite::clone() const
{ return new BaselineComposite(*this); }

BaselineComposite::BaselineComposite(QDataStream &stream)
{
    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Entry e;
        stream >> e.start >> e.end >> e.smoother;
        allSmoothers.append(e);
    }
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Entry e;
        stream >> e.start >> e.end >> e.smoother;
        smoothers.append(e);
    }
    stream >> validTarget;
}

void BaselineComposite::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) BASELINE_COMPOSITE;
    stream << t;
    stream << (quint32) allSmoothers.size();
    for (QList<Entry>::const_iterator it = allSmoothers.constBegin(), end = allSmoothers.constEnd();
            it != end;
            ++it) {
        stream << it->start << it->end << it->smoother;
    }
    stream << (quint32) smoothers.size();
    for (QList<Entry>::const_iterator it = smoothers.constBegin(), end = smoothers.constEnd();
            it != end;
            ++it) {
        stream << it->start << it->end << it->smoother;
    }
    stream << validTarget;
}

void BaselineComposite::printLog(QDebug &stream) const
{
    stream << "BaselineComposite(";
    if (!smoothers.isEmpty())
        stream << smoothers.first().smoother;
    stream << ")";
}


/**
 * Test if a value is in the relative band for a given smoothed value.  A "band"
 * is a fraction of the total value that is symmetric with respect to aproaching
 * zero.  For example, and band of 1.0 extends from half the smoothed value
 * to twice it and a band of 2.0 extends from one third to three times.
 * Specifying a negative band allows anything smaller than the band to pass.
 * 
 * @param smoothed  the input smoothed value
 * @param check     the value to check
 * @param band      the band factor
 * @return          true if the check value is within the band
 */
bool BaselineSmoother::inRelativeBand(double smoothed, double check, double band)
{
    if (!FP::defined(band))
        return true;
    if (!FP::defined(check) || !FP::defined(smoothed))
        return false;

    if (check > 0.0 && smoothed < 0.0)
        return false;
    if (check < 0.0) {
        if (smoothed > 0.0)
            return false;
        check = fabs(check);
        smoothed = fabs(smoothed);
    }
    if (band <= 0.0) {
        if (check > smoothed * (1.0 - band))
            return false;
    } else {
        if (check < smoothed / (1.0 + band))
            return false;
        if (check > smoothed * (1.0 + band))
            return false;
    }
    return true;
}

/**
 * Create a new baseline smoother from the given configuration.
 * 
 * @param defaultConfig the default configuration to use when none is specified
 * @param config    the configuration
 * @param unit      the unit within the configuration
 * @param path      the path within the configuration
 * @param start     the start time to trim to
 * @param end       the end time to trim to
 * @return          a new smoother owned by the caller
 */
BaselineSmoother *BaselineSmoother::fromConfiguration(const Variant::Read &defaultConfig,
                                                      SequenceSegment::Transfer &config,
                                                      const SequenceName &unit,
                                                      const QString &path,
                                                      double start,
                                                      double end)
{
    BaselineComposite bc;
    bc.overlay(defaultConfig, Variant::Root(), start, end);
    if (config.empty())
        return BaselineComposite::reduce(bc, start, end);

    auto i = Range::findLowerBound(config.begin(), config.end(), start);
    if (i == config.end()) {
        --i;
        if (Range::compareStartEnd(start, i->getEnd()) < 0) {
            bc.overlay(i->getValue(unit).getPath(path), defaultConfig, i->getStart(), i->getEnd());
        }
    } else if (i != config.begin() || Range::compareStartEnd(i->getStart(), end) < 0) {
        if (Range::compareStartEnd(start, i->getEnd()) >= 0)
            ++i;
        for (; i != config.end() && Range::compareStartEnd(i->getStart(), end) < 0; ++i) {

            auto configValue = i->getValue(unit).getPath(path);
            double st = i->getStart();
            if (configValue.exists()) {
                for (; i + 1 != config.end() &&
                        Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                        Range::compareStartEnd((i + 1)->getStart(), i->getEnd()) == 0 &&
                        (i + 1)->getValue(unit).getPath(path) == configValue; ++i) { }
            } else {
                for (; i + 1 != config.end() &&
                        Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                        !(i + 1)->getValue(unit).getPath(path).exists(); ++i) { }
            }
            bc.overlay(configValue, defaultConfig, st, i->getEnd());
        }
    }
    return BaselineComposite::reduce(bc, start, end);
}

/**
 * Create a new baseline smoother from the given configuration.
 * 
 * @param defaultConfig the default configuration to use when none is specified
 * @param config    the configuration
 * @param path      the path within the configuration
 * @param start     the start time to trim to
 * @param end       the end time to trim to
 * @return          a new smoother owned by the caller
 */
BaselineSmoother *BaselineSmoother::fromConfiguration(const Variant::Read &defaultConfig,
                                                      const ValueSegment::Transfer &config,
                                                      const QString &path,
                                                      double start,
                                                      double end)
{
    BaselineComposite bc;
    bc.overlay(defaultConfig, Variant::Root(), start, end);
    if (config.empty())
        return BaselineComposite::reduce(bc, start, end);

    auto i = Range::findLowerBound(config.begin(), config.end(), start);
    if (i == config.end()) {
        --i;
        if (Range::compareStartEnd(start, i->getEnd()) < 0) {
            bc.overlay(i->value().getPath(path), defaultConfig, i->getStart(), i->getEnd());
        }
    } else if (i != config.begin() || Range::compareStartEnd(i->getStart(), end) < 0) {
        if (Range::compareStartEnd(start, i->getEnd()) >= 0)
            ++i;
        for (; i != config.end() && Range::compareStartEnd(i->getStart(), end) < 0; ++i) {

            auto configValue = i->value().getPath(path);
            double st = i->getStart();
            if (configValue.exists()) {
                for (; i + 1 != config.end() &&
                        Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                        Range::compareStartEnd((i + 1)->getStart(), i->getEnd()) == 0 &&
                        (i + 1)->value().getPath(path) == configValue; ++i) { }
            } else {
                for (; i + 1 != config.end() &&
                        Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                        !(i + 1)->value().getPath(path).exists(); ++i) { }
            }
            bc.overlay(configValue, defaultConfig, st, i->getEnd());
        }
    }
    return BaselineComposite::reduce(bc, start, end);
}

BaselineSmootherOption::BaselineSmootherOption(const QString &argumentName,
                                               const QString &displayName,
                                               const QString &description,
                                               const QString &defaultBehavior,
                                               int sortPriority) : ComponentOptionBase(argumentName,
                                                                                       displayName,
                                                                                       description,
                                                                                       defaultBehavior,
                                                                                       sortPriority),
                                                                   smoother(),
                                                                   stability(true),
                                                                   spike(true)
{
}

BaselineSmootherOption::BaselineSmootherOption(const BaselineSmootherOption &other)
        : Data::ComponentOptionValueParsable(), ComponentOptionBase(other), smoother(other.smoother)
{ }

void BaselineSmootherOption::reset()
{
    optionSet = false;
    this->smoother.clear();
}

ComponentOptionBase *BaselineSmootherOption::clone() const
{ return new BaselineSmootherOption(*this); }

BaselineSmoother *BaselineSmootherOption::getSmoother() const
{
    return BaselineComposite::reduce(smoother, FP::undefined(), FP::undefined());
}

/**
 * Overlay the given smoother on to the option.  This takes ownership
 * of the smoother.
 * 
 * @param smoother  the smoother to add
 * @param start     the start time the smoother is effective
 * @param end       the end time the smoother is effective
 */
void BaselineSmootherOption::overlay(BaselineSmoother *smoother, double start, double end)
{
    this->smoother.overlay(smoother, start, end);
    optionSet = true;
}

/**
 * Set the default smoother to use.
 * 
 * @param smoother  the smoother
 */
void BaselineSmootherOption::setDefault(BaselineSmoother *smoother)
{
    this->smoother.overlay(smoother, FP::undefined(), FP::undefined());
}

/**
 * Clear the current smoother.
 */
void BaselineSmootherOption::clear()
{ this->smoother.clear(); }

/**
 * Set if this option is used for stability detection.
 * 
 * @param s true to enable detection
 */
void BaselineSmootherOption::setStabilityDetection(bool s)
{ stability = s; }

/**
 * Set if this option is used for spike detection.
 * 
 * @param s true to enable detection
 */
void BaselineSmootherOption::setSpikeDetection(bool s)
{ spike = s; }

/**
 * Test if this option is used for stability detection.
 * 
 * @return true if the option is used for detection
 */
bool BaselineSmootherOption::getStabilityDetection() const
{ return stability; }

/**
 * Test if this option is used for spike detection.
 * 
 * @return true if the option is used for detection
 */
bool BaselineSmootherOption::getSpikeDetection() const
{ return spike; }

bool BaselineSmootherOption::parseFromValue(const Data::Variant::Read &value)
{
    optionSet = true;
    if (value.getType() == Variant::Type::Array) {
        for (auto v : value.toArray()) {
            this->smoother
                .overlay(v, Variant::Root(), v.hash("Start").toDouble(), v.hash("End").toDouble());
        }
    } else {
        this->smoother.overlay(value, Variant::Root(), FP::undefined(), FP::undefined());
    }
    return true;
}


BaselineSmoother::~BaselineSmoother()
{ }

void BaselineSmoother::printLog(QDebug &stream) const
{ stream << "BaselineSmoother(Unknown)"; }

Variant::Root BaselineSmoother::describeState() const
{ return Variant::Root(); }

double BaselineSmoother::stabilityFactor() const
{ return FP::undefined(); }

double BaselineSmoother::spinupTime(double initial) const
{ return initial; }

QDataStream &operator<<(QDataStream &stream, const BaselineSmoother *baseline)
{
    if (baseline == NULL) {
        quint8 t = (quint8) BASELINE_NULL;
        stream << t;
        return stream;
    }
    baseline->serialize(stream);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, BaselineSmoother *&baseline)
{
    quint8 tRaw;
    stream >> tRaw;
    switch ((BaselineType) tRaw) {
    case BASELINE_NULL:
        baseline = NULL;
        return stream;
    case BASELINE_INVALID:
        baseline = new BaselineInvalid(stream);
        return stream;
    case BASELINE_SINGLEPOINT:
        baseline = new BaselineSinglePoint(stream);
        return stream;
    case BASELINE_LATEST:
        baseline = new BaselineLatest(stream);
        return stream;
    case BASELINE_FOREVER:
        baseline = new BaselineForever(stream);
        return stream;
    case BASELINE_FIXEDTIME:
        baseline = new BaselineFixedTime(stream);
        return stream;
    case BASELINE_DIGITALFILTER:
        baseline = new BaselineDigitalFilter(stream);
        return stream;
    case BASELINE_COMPOSITE:
        baseline = new BaselineComposite(stream);
        return stream;
    }
    Q_ASSERT(false);
    baseline = NULL;
    return stream;
}

QDebug operator<<(QDebug stream, const BaselineSmoother *baseline)
{
    QDebugStateSaver saver(stream);
    stream << static_cast<const void *>(baseline) << ':';
    if (baseline == NULL) {
        stream << "BaselineSmoother(NULL)";
    } else {
        baseline->printLog(stream);
    }
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const std::unique_ptr<BaselineSmoother> &baseline)
{
    stream << baseline.get();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, std::unique_ptr<BaselineSmoother> &baseline)
{
    BaselineSmoother *temp = nullptr;
    stream >> temp;
    baseline.reset(temp);
    return stream;
}

QDebug operator<<(QDebug stream, const std::unique_ptr<BaselineSmoother> &baseline)
{
    stream << baseline.get();
    return stream;
}

}
}
