/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3SMOOTHINGFIXEDTIME_H
#define CPD3SMOOTHINGFIXEDTIME_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QDataStream>
#include <QLoggingCategory>

#include "smoothing/smoothing.hxx"
#include "smoothing/smoothingchain.hxx"
#include "smoothing/binnedchain.hxx"
#include "smoothing/smoothingengine.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "datacore/stream.hxx"
#include "core/qtcompat.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_smoothing_fixedtime)

namespace CPD3 {
namespace Smoothing {

/**
 * The base implementation for the fixed time binning.  This takes segments
 * of non-overlapping data and combined them into fixed time length bins.
 * <br>
 * This does not keep copies of the bin data, so it may be a type that
 * cannot be duplicated.
 * 
 * @param BinData   the data type used as bin data
 */
template<typename BinData>
class FixedTimeBinner {
    Data::DynamicTimeInterval *interval;
    Data::DynamicTimeInterval *gap;
    bool binEmpty;
    double effectiveBinStart;
    double binStart;
    double binEnd;
    double lastValueEnd;
    bool haveHadTime;

public:
    /**
     * Construct a binner with the given interval and gap.  This
     * takes ownership of the interval and gap.
     * 
     * @param i     the interval to use, or NULL
     * @param g     the gap interval to use, or NULL
     */
    FixedTimeBinner(Data::DynamicTimeInterval *i, Data::DynamicTimeInterval *g) : interval(i),
                                                                                  gap(g),
                                                                                  binEmpty(
                                                                                              true),
                                                                                  effectiveBinStart(
                                                                                              FP::undefined()),
                                                                                  binStart(
                                                                                              FP::undefined()),
                                                                                  binEnd(FP::undefined()),
                                                                                  lastValueEnd(
                                                                                              FP::undefined()),
                                                                                  haveHadTime(
                                                                                              false)
    { }

    /**
     * Construct a binner with an infinite interval and no gap specification.
     */
    FixedTimeBinner() : interval(NULL),
                        gap(NULL),
                        binEmpty(true),
                        effectiveBinStart(FP::undefined()),
                        binStart(FP::undefined()),
                        binEnd(FP::undefined()),
                        lastValueEnd(FP::undefined()),
                        haveHadTime(false)
    { }

    virtual ~FixedTimeBinner()
    {
        if (interval != NULL)
            delete interval;
        if (gap != NULL)
            delete gap;
    }

    /**
     * Save the binner to the given stream.
     * 
     * @param stream    the stream target
     */
    void writeSerial(QDataStream &stream) const
    {
        stream << interval << gap << binEmpty << effectiveBinStart << binStart << binEnd
               << lastValueEnd << haveHadTime;
    }

    /**
     * Restore the binner from the given stream.
     * 
     * @param stream    the stream source
     */
    void readSerial(QDataStream &stream)
    {
        if (gap != NULL)
            delete gap;
        if (interval != NULL)
            delete interval;
        stream >> interval >> gap >> binEmpty >> effectiveBinStart >> binStart >> binEnd
               >> lastValueEnd >> haveHadTime;
    }

    /**
     * Using the given interval range, round the given start time to the
     * start of the bin it would generate.
     * 
     * @param range     the range to round or NULL for none
     * @param start     the start time to round or NULL for none
     * @param end       the end time to round
     */
    static void applyRounding(const Data::DynamicTimeInterval *range, double *start, double *end)
    {
        if (start != NULL && FP::defined(*start)) {
            *start = range->roundConst(*start, *start);

            if (end != NULL && FP::defined(*end)) {
                double checkEnd = *start;
                for (int t = 0;; t++) {
                    if (!FP::defined(checkEnd))
                        break;
                    if (t > 1000000) {
                        /* Give up */
                        *end = range->roundConst(*start, *end, true);
                        return;
                    }
                    if (checkEnd >= *end) {
                        *end = checkEnd;
                        return;
                    }
                    checkEnd = range->applyConst(*start, checkEnd);
                }
            }
            return;
        }
        if (end != NULL) {
            *end = range->roundConst(*end, *end, true);
        }
    }

protected:
    /**
     * Set the interval specification.  This takes ownership.
     * 
     * @param i     the interval to use, or NULL
     */
    void setInterval(Data::DynamicTimeInterval *i)
    {
        if (interval != NULL)
            delete interval;
        interval = i;
    }

    /**
     * Set the gap specification.  This takes ownership.
     * 
     * @param g     the gap interval to use, or NULL
     */
    void setGap(Data::DynamicTimeInterval *g)
    {
        if (gap != NULL)
            delete gap;
        gap = g;
    }

    /**
     * Add an incoming segment of data to the binner.  This may cause
     * the emission of output bins with processBin(double, double).
     * 
     * @param start     the start time of the segment
     * @param end       the end time of the segment
     * @param data      the data to add
     */
    void incomingSegment(double start, double end, const BinData &data)
    {
        if (!FP::defined(binEnd)) {
            /* If this isn't the first value then we don't need to care */
            if (haveHadTime) {
                /* Implied since only the first could possibly be undefined */
                Q_ASSERT(FP::defined(start));

                /* Check for a gap break */
                if (gap != NULL) {
                    double gapCheck = gap->apply(start, lastValueEnd);
                    if (FP::defined(gapCheck) && start > gapCheck) {
                        if (!binEmpty) {
                            processBin(effectiveBinStart, lastValueEnd);
                        }
                        advanceBin(start);
                        effectiveBinStart = start;
                    }
                }
                lastValueEnd = end;

                addToBin(start, end, data);
                binEmpty = false;
                return;
            }
            haveHadTime = true;

            lastValueEnd = end;
            if (interval == NULL) {
                effectiveBinStart = binStart = start;
                binEnd = FP::undefined();
                addToBin(start, end, data);
                binEmpty = false;
                advanceBin(binStart);
                return;
            }

            if (!FP::defined(end)) {
                /* No end time (so this is also the last possible value */
                if (!FP::defined(start)) {
                    /* No start either, so just a pass through no matter what */
                    effectiveBinStart = binStart = FP::undefined();
                    binEnd = FP::undefined();
                } else {
                    /* Round the start down */
                    effectiveBinStart = binStart = interval->round(start, start);
                    binEnd = FP::undefined();
                }

                addToBin(start, end, data);
                binEmpty = false;
                return;
            }

            if (!FP::defined(start)) {
                /* No start time, so we have a single valued bin before 
                 * everything and if it doesn't end during the first bin
                 * then that's the only trace of it. */
                effectiveBinStart = binStart = interval->round(end, end);
                binEnd = interval->apply(end, binStart);
                if (FP::defined(binStart) && end > binStart) {
                    /* Emit the "before" bin that started at minus infinity */
                    addToBin(start, binStart, data);
                    processBin(start, binStart);
                    addToBin(binStart, end, data);
                    binEmpty = false;
                } else {
                    addToBin(start, end, data);
                    processBin(start, binStart);
                    binEmpty = true;
                }

                return;
            }
            binStart = interval->round(start, start);
            binEnd = interval->apply(start, binStart);
            for (int t = 0;; t++) {
                if (!FP::defined(binStart)) {
                    binStart = start;
                    break;
                }

                if (!FP::defined(binEnd) || end <= binEnd)
                    break;

                /* Give up if this is something bizarre and just calculate from
                 * the end (correct if it's aligned, but probably not if it
                 * isn't). */
                if (t > 1000) {
                    qCDebug(log_smoothing_fixedtime) << "Initialize bin rounding diverged on"
                                                     << Logging::range(start, end);
                    binStart = interval->round(start, end);
                    binEnd = interval->apply(start, binStart);
                    break;
                }

                binStart = binEnd;
                binEnd = interval->apply(start, binStart);
            }
            effectiveBinStart = binStart;

            /* Starts before the start of the current bin, so
             * it may need a partial segment before. */
            if (start < binStart) {
                /* Figure out the rounded start time */
                double realStart = 0;
                for (int t = 1;; t++) {
                    realStart = interval->apply(start, binStart, false, -t);
                    if (!FP::defined(realStart)) {
                        realStart = start;
                        break;
                    }
                    if (realStart <= start)
                        break;

                    /* Again give up if this isn't making sense */
                    if (t > 1000) {
                        qCDebug(log_smoothing_fixedtime) << "Initialize bin rounding diverged on"
                                                         << Logging::range(start, end);
                        realStart = start;
                        break;
                    }
                }

                addToBin(start, binStart, data);
                processBin(realStart, binStart);

                /* If it ends before the bin entirely, the just
                 * dump it, otherwise re-adjust the duration */
                if (end <= binStart) {
                    binEmpty = true;
                } else {
                    addToBin(binStart, end, data);
                    binEmpty = false;

                    /* Covers the whole bin, so dump it anyway */
                    if (FP::defined(binEnd) && end == binEnd) {
                        processBin(binStart, binEnd);
                        binEmpty = true;
                    }
                }

                return;
            }

            addToBin(start, end, data);

            /* Covers the whole bin exactly, so just dump it now */
            if (FP::defined(binEnd) && end == binEnd) {
                processBin(binStart, binEnd);
                binEmpty = true;
            } else {
                binEmpty = false;
                advanceBin(effectiveBinStart);
            }

            return;
        }

        /* Should always be caught above */
        Q_ASSERT(interval != NULL);

        /* Undefined start should always be first and so handled above */
        Q_ASSERT(FP::defined(start));

        /* Check for a gap: it's only a gap if the incoming value still
         * goes in the existing bin, but has too much time since the
         * previous one.  Note that "goes into" here includes exactly ending
         * this bin, to handle the aligned bin gap problem. */
        if (gap != NULL) {
            double gapCheck = gap->apply(start, lastValueEnd);
            if (FP::defined(gapCheck) && start > gapCheck) {
                if (!binEmpty) {
                    processBin(effectiveBinStart, lastValueEnd);
                    binEmpty = true;
                }
                if (start < binEnd) {
                    advanceBin(start);
                    effectiveBinStart = start;
                }
            }
        }
        lastValueEnd = end;

        /* An endless value, so this must be the final one */
        if (!FP::defined(end)) {
            /* This completes the existing bin without adding to it */
            if (start >= binEnd) {
                if (!binEmpty) {
                    processBin(effectiveBinStart, binEnd);
                } else {
                    advanceBin(binEnd);
                }

                /* Advance bins */
                double originalBinStart = binStart;
                double originalBinEnd = binEnd;
                for (int t = 2;; t++) {
                    binStart = binEnd;
                    binEnd = interval->apply(start, originalBinStart, false, t);
                    if (!FP::defined(binEnd) || start < binEnd)
                        break;

                    /* Give up */
                    if (t > 1000) {
                        qCDebug(log_smoothing_fixedtime)
                            << "Bin advance overflow, using simple rounding.  Original "
                            << Logging::range(originalBinStart, originalBinEnd) << "adding"
                            << Logging::range(start, end);
                        binStart = interval->round(start, start);
                        break;
                    }

                    advanceBin(binEnd);
                }
                if (!FP::defined(binStart) || binStart < originalBinStart) {
                    binStart = originalBinEnd;
                }
                effectiveBinStart = binStart;

                addToBin(start, end, data);
            } else {
                /* Otherwise we need a partial segment */
                addToBin(start, binEnd, data);
                processBin(effectiveBinStart, binEnd);
                effectiveBinStart = binStart = binEnd;

                addToBin(binStart, end, data);
            }

            binEnd = FP::undefined();
            binEmpty = false;
            return;
        }

        /* Starts after the end of the current bin, so emit the current
         * one if it exists and advance. */
        if (start >= binEnd) {
            if (!binEmpty) {
                processBin(effectiveBinStart, binEnd);
            } else {
                advanceBin(binEnd);
            }

            addToBin(start, end, data);
            binEmpty = false;

            /* Advance bins: first find the start then the end of the
             * bins this one spans. */
            double originalBinStart = binStart;
            double originalBinEnd = binEnd;
            for (int t = 2;; t++) {
                binStart = binEnd;
                binEnd = interval->apply(start, originalBinStart, false, t);
                if (!FP::defined(binEnd) || start < binEnd) {
                    /* Now find the bin this one ends in, keeping the start */
                    for (;;) {
                        if (!FP::defined(binEnd)) {
                            if (!FP::defined(binStart) || binStart < originalBinEnd)
                                binStart = originalBinEnd;
                            effectiveBinStart = binStart;
                            return;
                        }
                        if (end <= binEnd)
                            break;
                        binEnd = interval->apply(start, originalBinStart, false, ++t);

                        /* Give up */
                        if (t > 1000) {
                            qCDebug(log_smoothing_fixedtime)
                                << "Bin advance overflow, using simple rounding.  Original "
                                << Logging::range(originalBinStart, originalBinEnd) << "adding"
                                << Logging::range(start, end);
                            binEnd = interval->round(start, end, true);
                            break;
                        }
                    }
                    break;
                }

                /* Give up */
                if (t > 1000) {
                    qCDebug(log_smoothing_fixedtime)
                        << "Bin advance overflow, using simple rounding.  Original "
                        << Logging::range(originalBinStart, originalBinEnd) << "adding"
                        << Logging::range(start, end);
                    binEnd = interval->round(start, end, true);
                    binStart = interval->apply(start, binEnd, false, -1);
                    break;
                }
            }
            if (!FP::defined(binStart) || binStart < originalBinEnd) {
                effectiveBinStart = binStart = originalBinEnd;
                binEnd = FP::undefined();
                return;
            }

            /* Finishes it exactly, so done */
            if (end == binEnd) {
                processBin(binStart, binEnd);
                binEmpty = true;
                effectiveBinStart = binStart = binEnd;
                binEnd = interval->apply(start, binStart);
                return;
            }

            effectiveBinStart = binStart;

            return;
        }

        /* If this is ends before the end of the current bin, 
         * then just add it. */
        if (end < binEnd) {
            addToBin(start, end, data);
            binEmpty = false;
            return;
        }

        /* Ends at exactly the bin, so this is a simple advance */
        if (end == binEnd) {
            addToBin(start, end, data);
            processBin(effectiveBinStart, binEnd);
            binEmpty = true;
            effectiveBinStart = binStart = binEnd;
            binEnd = interval->apply(start, binStart);
            return;
        }

        /* Exceeds the end, so this requires partial emission */
        addToBin(start, binEnd, data);
        processBin(effectiveBinStart, binEnd);

        /* Advance the bin until we find the end that encloses the end
         * of this segment */
        double originalEnd = binEnd;
        for (int t = 2;; t++) {
            binEnd = interval->apply(start, binStart, false, t);
            if (!FP::defined(binEnd)) {
                effectiveBinStart = binStart = originalEnd;
                addToBin(binStart, end, data);
                binEmpty = false;
                return;
            }
            if (end <= binEnd)
                break;

            /* Give up */
            if (t > 1000) {
                qCDebug(log_smoothing_fixedtime)
                    << "Bin advance overflow, using simple rounding.  Original"
                    << Logging::range(binStart, originalEnd) << "adding"
                    << Logging::range(start, end);
                binEnd = interval->round(start, end, true);
                break;
            }
        }
        effectiveBinStart = binStart = originalEnd;

        addToBin(binStart, end, data);

        /* Finishes it exactly, so done */
        if (end == binEnd) {
            processBin(effectiveBinStart, binEnd);
            binEmpty = true;
            effectiveBinStart = binStart = binEnd;
            binEnd = interval->apply(start, binStart);
            return;
        }

        binEmpty = false;
    }

    /**
     * Advance the binner to the given time.
     * 
     * @param time      the time to advance to
     */
    void incomingSegmentAdvance(double time)
    {
        /* Can't do gap checking, since we can't know if this is the final
         * advance in the data stream (e.x. advance -> end), which would
         * cause gap handling to give invalid results.  The exception is if
         * this advance ends a non-empty bin, then we can know the limits of
         * that bin gap-wise. */

        /* Have to have complex handling for the case where we've never
         * seen data so we can keep the stream moving. */
        if (!FP::defined(binEnd)) {
            /* Final bin, so we're done */
            if (haveHadTime)
                return;
            /* No interval so we can advance, but don't grab the time
             * until we have actual data */
            if (interval == NULL) {
                advanceBin(time);
                return;
            }

            double advanceTime = interval->round(time, time);
            if (!FP::defined(advanceTime))
                return;
            advanceBin(advanceTime);
            return;
        }

        /* Should always be caught above */
        Q_ASSERT(interval != NULL);

        if (time < binEnd)
            return;

        /* Ends the bin, so advance */
        if (!binEmpty) {
            if (gap != NULL) {
                double gapCheck = gap->apply(time, lastValueEnd);
                if (FP::defined(gapCheck) && time > gapCheck) {
                    processBin(effectiveBinStart, lastValueEnd);
                    binEmpty = true;
                }
            }
            if (!binEmpty) {
                processBin(effectiveBinStart, binEnd);
                binEmpty = true;
            }
        } else {
            advanceBin(binEnd);
        }

        /* Advance the bin until we find one that contains the time. */
        double originalStart = binStart;
        double originalEnd = binEnd;
        int t;
        for (t = 2;; t++) {
            binStart = binEnd;
            binEnd = interval->apply(time, originalStart, false, t);
            if (!FP::defined(binEnd) || time <= binEnd)
                break;

            /* Give up */
            if (t > 1000) {
                binStart = interval->round(time, time);
                binEnd = interval->apply(time, binStart);
                break;
            }
        }
        if (!FP::defined(binStart) || binStart < originalEnd) {
            binStart = originalEnd;
            binEnd = FP::undefined();
        }
        /* If we've move up multiple bins, advance to the new start (we can skip it if
         * it's a single one, since the segment has already been advanced) */
        if (t > 2) {
            advanceBin(binStart);
        }
        effectiveBinStart = binStart;
    }

    /**
     * Notify the binner that all incoming segments have been received.  This
     * will cause the emission of the final bins, if any.
     */
    void incomingSegmentsCompleted()
    {
        if (haveHadTime && !binEmpty) {
            if (!FP::defined(binEnd))
                processBin(effectiveBinStart, lastValueEnd);
            else
                processBin(effectiveBinStart, binEnd);
        }
        completedBinning();
    }

    /**
     * Add the data to the end of the current bin.
     * 
     * @param start     the start time of the data
     * @param end       the end time of the data
     * @param data      the data being added
     */
    virtual void addToBin(double start, double end, const BinData &data) = 0;

    /**
     * Called after all bin data has been emitted
     */
    virtual void completedBinning() = 0;

    /**
     * Called when a bin is completed.  This should also clear the contents
     * of the bin data.  This will only be called when the bin is not empty.
     * 
     * @param start     the start time of the bin
     * @param end       the end time of the bin
     */
    virtual void processBin(double start, double end) = 0;

    /**
     * Called when the binning time has been advanced without data
     * being processed.
     * 
     * @param time      the next possible bin time
     */
    virtual void advanceBin(double time) = 0;
};

/**
 * A smoother chain element for calculating fixed time binning.  This allows
 * the subclass to just implement a processor that is given a list of
 * values to process.
 * 
 * @param N the number of inputs
 */
template<std::size_t N>
class SmootherChainInputFixedTimeBinning
        : public SmootherChainNode,
          protected SmootherChainInputSegmentProcessor<N>,
          protected FixedTimeBinner<double const *> {

    typedef FixedTimeBinner<double const *> Binner;
    typedef SmootherChainInputSegmentProcessor<N> Processor;

    /* Don't need a mutex for this, since it's only written once we no
     * longer care. */
    bool segmentsFinished;

    inline void streamDone(std::size_t stream)
    {
        if (Processor::segmentEnd(stream)) {
            Binner::incomingSegmentsCompleted();
            segmentsFinished = true;
        }
    }

    class TargetN : public SmootherChainTarget {
    public:
        SmootherChainInputFixedTimeBinning *node;
        std::size_t index;

        TargetN()
        { }

        virtual ~TargetN()
        { }

        virtual void incomingData(double start, double end, double value)
        { node->segmentValue(index, start, end, value); }

        virtual void incomingAdvance(double time)
        { node->segmentAdvance(index, time); }

        virtual void endData()
        { node->streamDone(index); }
    };

    friend class TargetN;

    TargetN targets[N];

    QVector<double> buffer[N];
    QVector<double> durations;

public:
    /**
     * Create a new binning object.
     * 
     * @param interval      the interval to use, the binner takes ownership
     * @param gap           the maximum gap to use, the binner takes ownership
     */
    SmootherChainInputFixedTimeBinning(Data::DynamicTimeInterval *interval,
                                       Data::DynamicTimeInterval *gap) : Binner(interval, gap),
                                                                           buffer(),
                                                                           durations()
    {
        for (std::size_t i = 0; i < N; i++) {
            targets[i].node = this;
            targets[i].index = i;
        }
    }

    /**
     * Create a new binning object with no interval or gap specification.
     */
    SmootherChainInputFixedTimeBinning() : Binner(), buffer(), durations()
    {
        for (std::size_t i = 0; i < N; i++) {
            targets[i].node = this;
            targets[i].index = i;
        }
    }

    /**
     * Create a new binning object, deserializing the state from the given
     * stream.
     * 
     * @param stream        the stream to read state from
     */
    SmootherChainInputFixedTimeBinning(QDataStream &stream) : Binner(NULL, NULL),
                                                              buffer(),
                                                              durations()
    {
        Processor::readSerial(stream);
        Binner::readSerial(stream);
        stream >> durations;
        for (std::size_t i = 0; i < N; i++) {
            stream >> buffer[i];
        }

        for (std::size_t i = 0; i < N; i++) {
            targets[i].node = this;
            targets[i].index = i;
        }
    }

    virtual ~SmootherChainInputFixedTimeBinning()
    { }

    virtual bool finished()
    { return segmentsFinished; }

    virtual void pause()
    { Processor::freezeState(); }

    virtual void resume()
    { Processor::unfreezeState(); }

    virtual void serialize(QDataStream &stream) const
    {
        Processor::writeSerial(stream);
        Binner::writeSerial(stream);
        stream << durations;
        for (std::size_t i = 0; i < N; i++) {
            stream << buffer[i];
        }
    }

protected:
    virtual void segmentDone(double start, double end, const double values[N])
    { Binner::incomingSegment(start, end, values); }

    virtual void allStreamsAdvanced(double time)
    { Binner::incomingSegmentAdvance(time); }

    virtual void addToBin(double start, double end, const double *const &data)
    {
        for (std::size_t i = 0; i < N; i++) {
            buffer[i].append(data[i]);
        }
        if (FP::defined(start) && FP::defined(end))
            durations.append(end - start);
        else
            durations.append(FP::undefined());
    }

    virtual void processBin(double start, double end)
    {
        processBin(start, end, buffer, durations);
        for (std::size_t i = 0; i < N; i++) {
            buffer[i].clear();
        }
        durations.clear();
    }


    /**
     * Get the a target for the given input index.  The returned pointer
     * is owned by this object and should not be deleted.
     * 
     * @param index the input index
     * @return a chain target
     */
    inline SmootherChainTarget *getTarget(std::size_t index)
    {
        Q_ASSERT(index >= 0 && index < N);
        return &targets[index];
    }

    /**
     * Process the data for a given bin.
     * 
     * @param start     the start time of the bin
     * @param end       the end time of the bin
     * @param values    the vectors of values in the bin
     * @param durations the durations of the values added to the bin
     */
    virtual void processBin(double start,
                            double end,
                            const QVector<double> values[N],
                            const QVector<double> &durations) = 0;
};


/**
 * Conventional averaging for fixed time bins.  This takes as input
 * a stream of values and their coverage factors and produces the same
 * as output (averaged to the fixed time interval) as well as a statistics
 * variable for the averaged time.
 */
class CPD3SMOOTHING_EXPORT SmootherChainFixedTimeConventional
        : public SmootherChainBinnedConventional<SmootherChainInputFixedTimeBinning> {
public:
    /**
     * Create a conventional averager with the given parameters.
     * 
     * @param coverRequired the minimum coverage required or NULL, this takes ownership
     * @param interval      the interval to use or NULL for all data
     * @param gap           the maximum time to allow before breaking on a gap or NULL for never
     * @param outputAverage the output target for the average or NULL
     * @param outputCover   the output target for the cover factor or NULL
     * @param outputStats   the output target for the statistics variable or NULL
     */
    SmootherChainFixedTimeConventional(Data::DynamicDouble *coverRequired,
                                       Data::DynamicTimeInterval *interval,
                                       Data::DynamicTimeInterval *gap,
                                       SmootherChainTarget *outputAverage,
                                       SmootherChainTarget *outputCover = NULL,
                                       SmootherChainTargetGeneral *outputStats = NULL);

    /**
     * Restore the averager from the serialized state in the stream.
     * 
     * @param stream        the stream to restore from
     * @param outputAverage the output target for the average or NULL
     * @param outputCover   the output target for the cover factor or NULL
     * @param outputStats   the output target for the statistics variable or NULL
     */
    SmootherChainFixedTimeConventional(QDataStream &stream,
                                       SmootherChainTarget *outputAverage,
                                       SmootherChainTarget *outputCover = NULL,
                                       SmootherChainTargetGeneral *outputStats = NULL);
};

/**
 * Vector statistics generating for time bins.  This takes as input the streams
 * of unaveraged vector magnitude values, their coverage factors, and the
 * averaged vector magnitude (calculated with the normal component breakdown
 * then average routine).  It produces as output the un-vectored magnitude
 * average and coverage as well statistics about the magnitude (including a 
 * "stability factor" that is the ratio of the un-vectored average to the 
 * vectored one).
 */
class CPD3SMOOTHING_EXPORT SmootherChainFixedTimeVectorStatistics
        : public SmootherChainBinnedVectorStatistics<SmootherChainInputFixedTimeBinning> {
public:
    /**
     * Create a vector averager with the given parameters.
     * 
     * @param coverRequired the minimum coverage required or NULL, this takes ownership
     * @param interval      the interval to use or NULL for all data
     * @param gap           the maximum time to allow before breaking on a gap or NULL for never
     * @param outputAverage the output target for the average or NULL
     * @param outputCover   the output target for the cover factor or NULL
     * @param outputStats   the output target for the statistics variable or NULL
     */
    SmootherChainFixedTimeVectorStatistics(Data::DynamicDouble *coverRequired,
                                           Data::DynamicTimeInterval *interval,
                                           Data::DynamicTimeInterval *gap,
                                           SmootherChainTarget *outputAverage,
                                           SmootherChainTarget *outputCover = NULL,
                                           SmootherChainTargetGeneral *outputStats = NULL);

    /**
     * Restore the averager from the serialized state in the stream.
     * 
     * @param stream        the stream to restore from
     * @param outputAverage the output target for the average or NULL
     * @param outputCover   the output target for the cover factor or NULL
     * @param outputStats   the output target for the statistics variable or NULL
     */
    SmootherChainFixedTimeVectorStatistics(QDataStream &stream,
                                           SmootherChainTarget *outputAverage,
                                           SmootherChainTarget *outputCover = NULL,
                                           SmootherChainTargetGeneral *outputStats = NULL);
};


/**
 * Start and end value selection for fixed time bins.  This takes as input
 * a stream of values at the start and end of intervals (e.x. the normal values
 * and those with the "end" flavor).  It produces a value stream for the
 * start and end as well as a coverage figure.  This is often fed into a
 * combining chain element (e.x. SmootherChainBeersLawAbsorption).
 */
class CPD3SMOOTHING_EXPORT SmootherChainFixedTimeDifference : public SmootherChainBinnedDifference<
        SmootherChainInputFixedTimeBinning> {
public:
    /**
     * Create a difference averager with the given parameters.
     * 
     * @param interval      the interval to use or NULL for all data
     * @param gap           the maximum time to allow before breaking on a gap or NULL for never
     * @param outputStart   the output target for the value at the start or NULL
     * @param outputEnd     the output target for the value at the end or NULL
     * @param outputCover   the output target for the coverage or NULL
     */
    SmootherChainFixedTimeDifference(Data::DynamicTimeInterval *interval,
                                     Data::DynamicTimeInterval *gap,
                                     SmootherChainTarget *outputStart,
                                     SmootherChainTarget *outputEnd = NULL,
                                     SmootherChainTarget *outputCover = NULL);

    /**
     * Restore the averager from the serialized state in the stream.
     * 
     * @param stream        the stream to restore from
     * @param outputStart   the output target for the value at the start or NULL
     * @param outputEnd     the output target for the value at the end or NULL
     * @param outputCover   the output target for the coverage or NULL
     */
    SmootherChainFixedTimeDifference(QDataStream &stream,
                                     SmootherChainTarget *outputStart,
                                     SmootherChainTarget *outputEnd = NULL,
                                     SmootherChainTarget *outputCover = NULL);
};

/**
 * A fixed time binner for flags handling.  This simply combined all flags
 * in the bin together.
 */
class CPD3SMOOTHING_EXPORT SmootherChainFixedTimeFlags : public SmootherChainBinnedFlags<
        FixedTimeBinner> {
public:
    /**
     * Create a new fixed time binning flags averager.
     * 
     * @param interval          the interval to use or NULL for all data
     * @param gap               the maximum time to allow before breaking on a gap or NULL for never
     * @param outputFlags       the output target for the averaged flags
     * @param outputCover       the output target for the coverage or NULL
     */
    SmootherChainFixedTimeFlags(Data::DynamicTimeInterval *interval, Data::DynamicTimeInterval *gap,
                                SmootherChainTargetFlags *outputFlags,
                                SmootherChainTarget *outputCover = NULL);

    /**
     * Restore a flags averager from the given state.
     * 
     * @param stream            the stream to restore from
     * @param outputFlags       the output target for the averaged flags
     * @param outputCover       the output target for the coverage or NULL
     */
    SmootherChainFixedTimeFlags(QDataStream &stream,
                                SmootherChainTargetFlags *outputFlags,
                                SmootherChainTarget *outputCover = NULL);
};


/**
 * A smoother chain core for fixed time binned data.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreFixedTime : public SmootherChainCoreBinned {
    Data::DynamicDouble *requiredCover;
    Data::DynamicTimeInterval *interval;
    Data::DynamicTimeInterval *gap;

    Data::DynamicDouble *childCover() const;

    Data::DynamicTimeInterval *childInterval() const;

    Data::DynamicTimeInterval *childGap() const;

public:
    /**
     * Create a new core.  This takes ownership of the interval, gap, and
     * coverage.
     * 
     * @param setInterval   the interval to smooth
     * @param setGap        the maximum gap to allow or NULL for none
     * @param setRequiredCover      the minimum cover to require or NULL for none
     * @param setEnableStatistics   enable generation of statistics
     */
    SmootherChainCoreFixedTime(Data::DynamicTimeInterval *setInterval,
                               Data::DynamicTimeInterval *setGap = NULL,
                               Data::DynamicDouble *setRequiredCover = NULL,
                               bool setEnableStatistics = true);

    virtual ~SmootherChainCoreFixedTime();

    virtual Data::Variant::Root getProcessingMetadata(double time) const;

    virtual QSet<double> getProcessingMetadataBreaks() const;

    virtual bool differencePreserving() const;

    virtual void createConventional(SmootherChainCoreEngineInterface *engine,
                                    SmootherChainTarget *&inputValue,
                                    SmootherChainTarget *&inputCover,
                                    SmootherChainTarget *outputAverage,
                                    SmootherChainTarget *outputCover = NULL,
                                    SmootherChainTargetGeneral *outputStats = NULL);

    virtual void deserializeConventional(QDataStream &stream,
                                         SmootherChainCoreEngineInterface *engine,
                                         SmootherChainTarget *&inputValue,
                                         SmootherChainTarget *&inputCover,
                                         SmootherChainTarget *outputAverage,
                                         SmootherChainTarget *outputCover = NULL,
                                         SmootherChainTargetGeneral *outputStats = NULL);

    virtual void createVectorStatistics(SmootherChainCoreEngineInterface *engine,
                                        SmootherChainTarget *&inputMagnitude,
                                        SmootherChainTarget *&inputCover,
                                        SmootherChainTarget *&inputVectoredMagnitude,
                                        SmootherChainTarget *outputAverage = NULL,
                                        SmootherChainTarget *outputCover = NULL,
                                        SmootherChainTargetGeneral *outputStats = NULL);

    virtual void deserializeVectorStatistics(QDataStream &stream,
                                             SmootherChainCoreEngineInterface *engine,
                                             SmootherChainTarget *&inputMagnitude,
                                             SmootherChainTarget *&inputCover,
                                             SmootherChainTarget *&inputVectoredMagnitude,
                                             SmootherChainTarget *outputAverage = NULL,
                                             SmootherChainTarget *outputCover = NULL,
                                             SmootherChainTargetGeneral *outputStats = NULL);

    virtual void createDifference(SmootherChainCoreEngineInterface *engine,
                                  bool alwaysBreak,
                                  SmootherChainTarget *&inputStart,
                                  SmootherChainTarget *&inputEnd,
                                  SmootherChainTarget *outputStart,
                                  SmootherChainTarget *outputEnd,
                                  SmootherChainTarget *outputCover = NULL);

    virtual void deserializeDifference(QDataStream &stream,
                                       SmootherChainCoreEngineInterface *engine,
                                       bool alwaysBreak,
                                       SmootherChainTarget *&inputStart,
                                       SmootherChainTarget *&inputEnd,
                                       SmootherChainTarget *outputStart,
                                       SmootherChainTarget *outputEnd,
                                       SmootherChainTarget *outputCover = NULL);

    virtual void createFlags(SmootherChainCoreEngineInterface *engine,
                             SmootherChainTargetFlags *&inputFlags,
                             SmootherChainTarget *&inputCover,
                             SmootherChainTargetFlags *outputFlags,
                             SmootherChainTarget *outputCover = NULL);

    virtual void deserializeFlags(QDataStream &stream,
                                  SmootherChainCoreEngineInterface *engine,
                                  SmootherChainTargetFlags *&inputFlags,
                                  SmootherChainTarget *&inputCover,
                                  SmootherChainTargetFlags *outputFlags,
                                  SmootherChainTarget *outputCover = NULL);
};

/**
 * A smoother chain core for fixed time binned data that always preserved
 * smoothability of inputs.  This means it breaks on any gaps detected but
 * it also preserves the smoothing state of differential components.
 */
class CPD3SMOOTHING_EXPORT SmootherChainCoreFixedTimePreserving
        : public SmootherChainCoreFixedTime {
public:
    /**
     * Create a new core.  This takes ownership of the interval and gap.
     * 
     * @param setInterval           the interval to smooth
     * @param setRequiredCover      the minimum cover to require or NULL for none
     * @param setEnableStatistics   enable generation of statistics
     */
    SmootherChainCoreFixedTimePreserving(Data::DynamicTimeInterval *setInterval,
                                         Data::DynamicDouble *setRequiredCover = NULL,
                                         bool setEnableStatistics = true);

    virtual ~SmootherChainCoreFixedTimePreserving();

    virtual bool differencePreserving() const;
};

/**
 * The implementation for the engine that controls a fixed time smoother.
 */
class CPD3SMOOTHING_EXPORT SmoothingEngineFixedTime : public SmoothingEngine {
    Data::DynamicTimeInterval *interval;
    Data::DynamicTimeInterval *gap;
    Data::DynamicDouble *requiredCover;
    bool gapPreserving;
    bool enableStatistics;

    Data::SequenceName::Set registeredInputs;
public:
    /**
     * Create a new fixed time smoothing engine.
     * 
     * @param setInterval   the interval to average over
     * @param setGap        the maximum gap allowed (no effect if preserving gaps)
     * @param setRequiredCover      the minimum coverage allowed in an average
     * @param setGapPreserving      if set then the engine preserves gaps and difference measurements
     * @param setEnableStatistics   enable generation of statistics
     */
    SmoothingEngineFixedTime(Data::DynamicTimeInterval *setInterval,
                             Data::DynamicTimeInterval *setGap = NULL,
                             Data::DynamicDouble *setRequiredCover = NULL,
                             bool setGapPreserving = false,
                             bool setEnableStatistics = true);

    virtual ~SmoothingEngineFixedTime();

    /**
     * Register the inputs the engine is expected to see.
     * 
     * @param inputs    the expected inputs
     */
    void registerExpectedInputs(const QSet<Data::SequenceName> &inputs);

    void deserialize(QDataStream &stream) override;

    void serialize(QDataStream &stream) override;


    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

protected:
    void startProcessing(IncomingBuffer::iterator incomingBegin,
                         IncomingBuffer::iterator incomingEnd,
                         double &advanceTime,
                         double &holdbackTime) override;

    void predictMetadataBounds(double *start, double *end) override;
};

}
}

#endif
