/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "smoothing/tukeychain.hxx"
#include "smoothing/tukey.hxx"
#include "core/environment.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Smoothing {

/** @file smoothing/tukeychain.hxx
 * Smoothing chain implementations that wrap Tukey style smoothers.
 */

Tukey3RSSHChainNode::Tukey3RSSHChainNode(Data::DynamicTimeInterval *setGap,
                                         bool smoothOverUndefined,
                                         SmootherChainTarget *outputTarget) : BlockSmootherChain(
        setGap, outputTarget)
{
    if (smoothOverUndefined) {
        setUndefinedMode(BlockSmootherChain::Remove);
    } else {
        setUndefinedMode(BlockSmootherChain::DumpBuffer);
    }
}

Tukey3RSSHChainNode::Tukey3RSSHChainNode(QDataStream &stream, SmootherChainTarget *outputTarget)
        : BlockSmootherChain(stream, outputTarget)
{ }

Tukey3RSSHChainNode::~Tukey3RSSHChainNode()
{ }


Tukey3RSSHChainNode::Handler3RSSH::Handler3RSSH()
{ }

Tukey3RSSHChainNode::Handler3RSSH::~Handler3RSSH()
{ }

bool Tukey3RSSHChainNode::Handler3RSSH::process(BlockSmootherChain::Value *&buffer, size_t &size)
{
    double *temp = (double *) malloc(size * sizeof(double));
    Q_ASSERT(temp);
    double *tempEnd = temp + size;
    BlockSmootherChain::Value *bufferEnd = buffer + size;

    /* 3R */
    bool inTemp = Tukey::median3R(buffer, bufferEnd, temp, tempEnd, false);

    /* S and implicit 3R */
    if (inTemp) {
        Tukey::split(temp, tempEnd, buffer);
        inTemp = Tukey::median3R(buffer, bufferEnd, temp, tempEnd, false);
    } else {
        Tukey::split(buffer, bufferEnd, temp);
        inTemp = !Tukey::median3R(temp, tempEnd, buffer, bufferEnd, false);
    }

    /* S and implicit 3R */
    if (inTemp) {
        Tukey::split(temp, tempEnd, buffer);
        inTemp = Tukey::median3R(buffer, bufferEnd, temp, tempEnd, false);
    } else {
        Tukey::split(buffer, bufferEnd, temp);
        inTemp = !Tukey::median3R(temp, tempEnd, buffer, bufferEnd, false);
    }

    /* H */
    if (inTemp) {
        Tukey::hanning(temp, tempEnd, buffer);
    } else {
        Tukey::hanning(buffer, bufferEnd, temp);
        BlockSmootherChain::Value *target = buffer;
        for (double *source = temp; source != tempEnd; ++source, ++target) {
            target->value = *source;
        }
    }

    free(temp);
    return false;
}

BlockSmootherChain::Handler *Tukey3RSSHChainNode::createHandler(double start, double end)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    return new Handler3RSSH;
}

SmootherChainCoreTukey3RSSH::SmootherChainCoreTukey3RSSH(Data::DynamicTimeInterval *setGap,
                                                         bool setSmoothUndefined) : gap(setGap),
                                                                                    smoothOverUndefined(
                                                                                            setSmoothUndefined)
{ }

SmootherChainCoreTukey3RSSH::~SmootherChainCoreTukey3RSSH()
{
    if (gap != NULL)
        delete gap;
}

void SmootherChainCoreTukey3RSSH::createSmoother(SmootherChainCoreEngineInterface *engine,
                                                 SmootherChainTarget *&input,
                                                 SmootherChainTarget *output)
{
    Tukey3RSSHChainNode *node =
            new Tukey3RSSHChainNode(gap == NULL ? NULL : gap->clone(), smoothOverUndefined, output);
    engine->addChainNode(node);
    input = node->getTarget();
}

void SmootherChainCoreTukey3RSSH::deserializeSmoother(QDataStream &stream,
                                                      SmootherChainCoreEngineInterface *engine,
                                                      SmootherChainTarget *&input,
                                                      SmootherChainTarget *output)
{
    Tukey3RSSHChainNode *node = new Tukey3RSSHChainNode(stream, output);
    engine->addChainNode(node);
    input = node->getTarget();
}

Variant::Root SmootherChainCoreTukey3RSSH::getProcessingMetadata(double time) const
{
    Variant::Root meta;
    meta.write().hash("Type").setString("3RSSH");
    if (gap != NULL) {
        meta.write().hash("Parameters").hash("Gap").setString(gap->describe(time));
    }
    meta.write().hash("Parameters").hash("SmoothOverUndefined").setBool(smoothOverUndefined);
    return meta;
}

QSet<double> SmootherChainCoreTukey3RSSH::getProcessingMetadataBreaks() const
{
    if (gap == NULL)
        return QSet<double>();
    return gap->getChangedPoints();
}

}
}
