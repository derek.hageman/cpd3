/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QMainWindow>
#include <QProgressBar>
#include <QLabel>
#include <QAction>
#include <QTabBar>
#include <QVBoxLayout>
#include <QFrame>
#include <QScrollArea>
#include <QTimer>
#include <QGridLayout>
#include <QPushButton>
#include <QUndoStack>
#include <QUndoView>
#include <QWidgetAction>
#include <QTimer>

#include "guicore/timeboundselection.hxx"
#include "guicore/dynamictimeinterval.hxx"
#include "guicore/actionprogress.hxx"
#include "core/actioncomponent.hxx"
#include "graphing/display.hxx"

class CPD3GUI;

class InterfaceHandler;

class GenericHandler;

class DisplayHandler;

class GUIDisplayInterface;

class RealtimeDisplay;

/**
 * The main window for the GUI interface.
 */
class GUIMainWindow : public QMainWindow {
Q_OBJECT

    friend class ReplotOperation;

    class ReplotOperation;

    typedef std::shared_ptr<ReplotOperation> ReplotOperationPointer;

    class ReplotOperation {
        enum {
            Fixed, Realtime,
        } mode;

        CPD3GUI *gui;
        GUIMainWindow *parent;

        double start;
        double end;

        CPD3::Time::LogicalTimeUnit unit;
        int count;
        bool align;
    public:
        ReplotOperation();

        ReplotOperation(double s, double e, CPD3GUI *g, GUIMainWindow *p);

        ReplotOperation(CPD3::Time::LogicalTimeUnit u, int c, bool a, CPD3GUI *g, GUIMainWindow *p);

        ReplotOperation(const ReplotOperation &other);

        ReplotOperation &operator=(const ReplotOperation &other);

        void apply(const ReplotOperationPointer &operation);

        QString undoText() const;

        bool mergable(const ReplotOperation &other) const;
    };

    friend class HistoryViewList;

    class HistoryViewList : public QUndoView {
        GUIMainWindow *win;
    public:
        HistoryViewList(GUIMainWindow *w, QWidget *parent = 0);

        virtual ~HistoryViewList();

    protected:
        virtual void showEvent(QShowEvent *event);
    };

    friend class HistoryViewAction;

    class HistoryViewAction : public QWidgetAction {
        GUIMainWindow *win;
    public:
        HistoryViewAction(GUIMainWindow *w, QObject *parent);

        virtual ~HistoryViewAction();

    protected:
        virtual QWidget *createWidget(QWidget *parent);
    };

    friend class UndoReplot;

    class UndoReplot : public QUndoCommand {
        CPD3GUI *gui;
        GUIMainWindow *parent;
        ReplotOperationPointer operation;
        ReplotOperationPointer previousReplot;
        CPD3::Graphing::DisplayZoomCommand resetZoom;
    public:
        UndoReplot(const ReplotOperationPointer &op, CPD3GUI *g, GUIMainWindow *p);

        virtual ~UndoReplot();

        virtual int id() const;

        virtual bool mergeWith(const QUndoCommand *command);

        virtual void undo();

        virtual void redo();
    };


    CPD3GUI *gui;
    bool inRealtimeMode;
    ReplotOperationPointer lastActivatedReplot;
    ReplotOperationPointer initialReplot;

    QProgressBar *progressBar;
    QPushButton *realtimeMessagesPending;
    QLabel *realtimeStatus;
    QFrame *realtimeSeparator;
    QLabel *realtimeSystemFlags;
    QLabel *statusLabel;
    CPD3::GUI::ActionProgressStatusBar *progress;

    QToolBar *timeToolBar;

    CPD3::GUI::TimeBoundSelection *timeRangeSelect;
    QAction *timeRangeSelectAction;
    CPD3::GUI::TimeIntervalSelection *timeIntervalSelect;
    QAction *timeIntervalSelectAction;

    QAction *realtimeAction;
    QAction *backAction;
    QAction *forwardAction;
    QMenu *historyMenuBase;
    QAction *historyAction;

    QTabBar *displayTabBar;
    QVBoxLayout *displayLayout;
    QList<GUIDisplayInterface *> displays;
    QList<GUIDisplayInterface *> displayedTabs;
    GUIDisplayInterface *displayCurrent;
    GUIDisplayInterface *displayPrior;
    bool haveFocusedStateDisplay;

    QWidget *messageInterfaceContainer;
    QScrollArea *messageArea;
    QWidget *messageContainer;
    QGridLayout *messageLayout;
    QList<QList<QWidget *> > displayedMessages;
    QTimer *messageHideTimer;

    QUndoStack historyStack;

    std::vector<std::shared_ptr<CPD3::Graphing::DisplayHighlightController> > highlightControllers;

    struct HighlightData {
        double start;
        double end;
        double min;
        double max;
        CPD3::Data::SequenceMatch::Composite selection;
    };
    QHash<CPD3::Graphing::DisplayHighlightController::HighlightType, QList<HighlightData> >
            activeHighlights;

    void showDisplay(GUIDisplayInterface *interface);

    void addWindow(GUIDisplayInterface *add);

    void highlightApply(const std::vector<
            std::shared_ptr<CPD3::Graphing::DisplayHighlightController> > &target);

    void highlightRebuild();

public:
    GUIMainWindow(CPD3GUI *g);

    virtual ~GUIMainWindow();

    virtual QSize sizeHint() const;

    /**
     * Set the status text for the realtime label.
     * 
     * @param text the status text
     */
    void setRealtimeStatusString(const QString &text = QString(),
                                 const QString &systemFlags = QString());

    /**
     * Save all display state.  This can be used before a mode switch to
     * ensure that displays are re-created correctly.
     */
    void writeOutState();

    /**
     * Add the associated window for a handler.
     * 
     * @param interface the handler
     */
    void addAcquisitionInterface(InterfaceHandler *interface);

    /**
     * Remove the associated window for a handler.
     * 
     * @param interface the handler
     */
    void removeAcquisitionInterface(InterfaceHandler *interface);

    /**
     * Remove all acquisition interfaces.
     */
    void removeAllAcquisitionInterfaces();

    /**
     * Show the window associated with a handler.
     * 
     * @param interface the handler
     */
    void showAcquisitionInterface(InterfaceHandler *interface);


    /**
     * Add the associated window for a handler.
     * 
     * @param handler   the handler
     */
    void addGenericHandler(GenericHandler *handler);

    /**
     * Remove all generic handlers.
     */
    void removeAllGenericHandlers();

    /**
     * Show the window associated with a handler.
     * 
     * @param handler   the handler
     */
    void showGenericHandler(GenericHandler *handler);


    /**
     * Add the associated window for a handler.  This will take ownership
     * of the handler.
     * 
     * @param handler   the handler
     */
    void addDisplayHandler(DisplayHandler *handler);

    /**
     * Remove the associated window for a handler and return ownership
     * of it to the caller.
     * 
     * @param interface the handler
     */
    void removeDisplayHandler(DisplayHandler *handler);

    /**
     * Remove all display handlers.
     */
    void removeAllDisplayHandlers();

    /**
     * Show the window associated with a handler.
     * 
     * @param handler   the handler
     */
    void showDisplayHandler(DisplayHandler *handler);

    /**
     * Get the currently selected display handler.
     *
     * @return  the currently selected display handler
     */
    DisplayHandler *getSelectedDisplayHandler() const;

    /**
     * Get all handler interfaces currently displayed.
     * 
     * @return  the handler interfaces
     */
    QList<GUIDisplayInterface *> getHandlerInterfaces() const;

    /**
     * Reset all highlights.
     */
    void highlightReset();

    /**
     * Add a highlight selection.
     * 
     * @param type      the type of the selection
     * @param start     the start time of the highlight or undefined for -infinity
     * @param end       the end time of the highlight or undefined for +infinity
     * @param min       the minimum value of the highlight or undefined for -infinity
     * @param max       the maximum value time of the highlight or undefined for +infinity
     * @param selection the selection of variables the highlight applies to
     */
    void highlightAdd(CPD3::Graphing::DisplayHighlightController::HighlightType type,
                      double start,
                      double end,
                      double min = CPD3::FP::undefined(),
                      double max = CPD3::FP::undefined(),
                      const CPD3::Data::SequenceMatch::Composite &selection = CPD3::Data::SequenceMatch::Composite(
                              CPD3::Data::SequenceMatch::Element::SpecialMatch::All));

    /**
     * Reset all highlights of the given type.
     * 
     * @param type      the type of selection
     */
    void highlightReset(CPD3::Graphing::DisplayHighlightController::HighlightType type);

    /**
     * Add a highlight for a mentor edit.
     * 
     * @param start     the start time of the highlight or undefined for -infinity
     * @param end       the end time of the highlight or undefined for +infinity
     * @param min       the minimum value of the highlight or undefined for -infinity
     * @param max       the maximum value time of the highlight or undefined for +infinity
     * @param selection the selection of variables the highlight applies to
     */
    void highlightAddMentorEdit(double start,
                                double end,
                                double min = CPD3::FP::undefined(),
                                double max = CPD3::FP::undefined(),
                                const CPD3::Data::SequenceMatch::Composite &selection = CPD3::Data::SequenceMatch::Composite(
                                        CPD3::Data::SequenceMatch::Element::SpecialMatch::All));

    /**
     * Reset all mentor edit highlights.
     */
    void highlightResetMentorEdit();

    /**
     * Add a highlight for the event log.
     * 
     * @param start     the start time of the highlight or undefined for -infinity
     * @param end       the end time of the highlight or undefined for +infinity
     * @param min       the minimum value of the highlight or undefined for -infinity
     * @param max       the maximum value time of the highlight or undefined for +infinity
     * @param selection the selection of variables the highlight applies to
     */
    void highlightAddEventLog(double start,
                              double end,
                              double min = CPD3::FP::undefined(),
                              double max = CPD3::FP::undefined(),
                              const CPD3::Data::SequenceMatch::Composite &selection = CPD3::Data::SequenceMatch::Composite(
                                      CPD3::Data::SequenceMatch::Element::SpecialMatch::All));

    /**
     * Reset all mentor edit highlights.
     */
    void highlightResetEventLog();

    /**
     * Attach the given source for feedback information.
     *
     * @param source    the feedback source
     */
    void feedbackAttach(CPD3::ActionFeedback::Source &source);

protected:
    virtual void showEvent(QShowEvent *event);

    virtual void closeEvent(QCloseEvent *event);

public slots:

    /**
     * Rebuild all tabs and visiblity.
     */
    void rebuildTabs();

    /**
     * Display the currently active display in fullscreen mode.
     */
    void displayActiveInFullscreen();

    /**
     * Activate the previously selected window.
     */
    void activatePreviousSelection();

    /**
     * Activate the tab after the currently selected one.
     */
    void activateNext();

    /**
     * Activate the tab before the currently selected one.
     */
    void activatePrevious();

    /**
     * Toggle the legend for all graphs.
     * 
     * @param enable    true to enable the legends
     * @param display   the display to set or NULL for all
     */
    void legendToggle(bool enable, DisplayHandler *display = NULL);

    /**
     * Clear the entire zoom and time history.  This should generally
     * be paired with a display regeneration.
     */
    void clearHistory();

    /**
     * Switch to realtime mode with the given settings.
     * 
     * @param unit  the realtime window unit
     * @param count the realtime unit count
     * @param align set if aligning
     */
    void setRealtime(CPD3::Time::LogicalTimeUnit unit, int count, bool align);

    /**
     * Switch to fixed time window mode with the given selected interval.
     * 
     * @param start the start time
     * @param end   the end time
     */
    void setFixedTime(double start, double end);

    /**
     * Update the bounds if we're in realtime (that is they're not actually
     * directly visible).  This is used to keep them up to date while
     * the times are updated in realtime mode.
     * 
     * @param start the start time
     * @param end   the end time
     */
    void updateBoundsIfInRealtime(double start, double end);

    /**
     * Set if the realtime window toggle is enabled.
     * 
     * @param enabled   true if the toggle is enabled
     */
    void setRealtimeToggleEnabled(bool enabled);

    /**
     * A message to the realtime display.
     * 
     * @param message   the message to add
     */
    void addRealtimeMessage(const QString &messsage);

    /**
     * Start the progress tracking bar.
     */
    void start();

    /**
     * Stop the progress tracking bar.
     */
    void finished();

    /**
     * Set if the realtime bar is displayed.
     * 
     * @param enabled   set if the bar is enabled
     */
    void realtimeToggled(bool enabled);

    /**
     * Indicate that the back button was pressed.
     */
    void backPressed();

    /**
     * Indicate that the forward button was pressed.
     */
    void forwardPressed();

    /**
     * Reset the display to the start of the stack.
     */
    void backToStart();

    /**
     * Indicate that the replot button was pressed.
     */
    void replotPressed();

    /**
     * Handle an incoming display zoom command.  This just adds it to the
     * undo stack.
     * 
     * @param command   the incoming zoom command
     */
    void incomingZoom(CPD3::Graphing::DisplayZoomCommand command);

    /**
     * Handle an incoming global zoom command.  This will inject all
     * displays into the zoom command before adding it to the undo stack.
     * 
     * @param command   the incoming zoom command
     */
    void incomingGlobalZoom(CPD3::Graphing::DisplayZoomCommand command);

    /**
     * Clear the current focus.
     */
    void unfocusInitial();

    /**
     * Called when the global escape hotkey is pressed.
     */
    void escapePressed();

private slots:

    void updateHistoryEnabled();

    void tabSelected(int index, bool changeIndex = false);

    void messageAreaDelayedAdd();

    void messageAreaDelayedRemove();

    void realtimeMessagesShow();

    void dismissMessage();

    void dismissAllMessages();
};

/**
 * A common interface used to display tab contents in the main window.
 */
class GUIDisplayInterface : public QObject {
Q_OBJECT

    CPD3GUI *gui;
    GUIMainWindow *parent;
public:
    GUIDisplayInterface(CPD3GUI *g, GUIMainWindow *p);

    virtual ~GUIDisplayInterface();

    /**
     * Get the widget used to display the contents.
     * 
     * @return the display widget
     */
    virtual QWidget *widget() const = 0;

    /**
     * Get the title used to display the interface.
     * 
     * @return the title
     */
    virtual QString getTitle() const = 0;

    /**
     * Get the state save/restore ID of the interface.
     */
    virtual QString getStateID() const = 0;

    /**
     * Get the display sort priority.  Displays without a priority will
     * sort to the end.
     * 
     * @return the sort priority
     */
    virtual qint64 getSortPriority() const = 0;

    /**
     * Try to show the contents of the interface in fullscreen.
     * 
     * @return true when it was shown
     */
    virtual bool showFullscreen();

    /**
     * Clear any focusing the display has.
     */
    virtual void releaseFocus();
};

class GUIDisplayAcquisitionInterface : public GUIDisplayInterface {
Q_OBJECT

    InterfaceHandler *interface;
    RealtimeDisplay *display;
public:
    GUIDisplayAcquisitionInterface(InterfaceHandler *i, CPD3GUI *g, GUIMainWindow *p);

    virtual ~GUIDisplayAcquisitionInterface();

    virtual QWidget *widget() const;

    virtual QString getTitle() const;

    virtual QString getStateID() const;

    virtual qint64 getSortPriority() const;

    bool matchesInterface(const InterfaceHandler *i) const;

    inline InterfaceHandler *getInterface() const
    { return interface; }
};

class GUIGenericHandlerInterface : public GUIDisplayInterface {
Q_OBJECT

    GenericHandler *handler;
    RealtimeDisplay *display;
public:
    GUIGenericHandlerInterface(GenericHandler *h, CPD3GUI *g, GUIMainWindow *p);

    virtual ~GUIGenericHandlerInterface();

    virtual QWidget *widget() const;

    virtual QString getTitle() const;

    virtual QString getStateID() const;

    virtual qint64 getSortPriority() const;

    bool matchesHandler(const GenericHandler *h) const;

    inline GenericHandler *getHandler() const
    { return handler; }
};

class GUIDisplayHandlerInterface : public GUIDisplayInterface {
Q_OBJECT

    DisplayHandler *handler;
public:
    GUIDisplayHandlerInterface(DisplayHandler *h, CPD3GUI *g, GUIMainWindow *p);

    virtual ~GUIDisplayHandlerInterface();

    virtual QWidget *widget() const;

    virtual QString getTitle() const;

    virtual QString getStateID() const;

    virtual qint64 getSortPriority() const;

    bool matchesDisplay(const DisplayHandler *d) const;

    inline DisplayHandler *getHandler() const
    { return handler; }

    virtual bool showFullscreen();

    virtual void releaseFocus();

    std::vector<std::shared_ptr<
            CPD3::Graphing::DisplayHighlightController>> getHighlightControllers() const;
};

Q_DECLARE_METATYPE(GUIDisplayInterface *);

#endif
