/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef EVENTLOG_H
#define EVENTLOG_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QDialog>
#include <QRunnable>
#include <QComboBox>
#include <QLineEdit>
#include <QTableView>

#include "datacore/stream.hxx"

class GUIMainWindow;

class EventLogTableModel;

class EventLogDisplay : public QDialog {
Q_OBJECT

    GUIMainWindow *mainWindow;

    friend class EventLogTableModel;

    EventLogTableModel *model;
    QTableView *table;

    QComboBox *filterSelection;
    QWidget *manualBox;
    QLineEdit *keyFilter;
    QLineEdit *sourceFilter;
    QLineEdit *componentFilter;
    QLineEdit *authorFilter;
    QLineEdit *textFilter;

public:
    EventLogDisplay(GUIMainWindow *win = 0, Qt::WindowFlags f = 0);

    ~EventLogDisplay();

    void load(const CPD3::Data::SequenceName::Component &station, double start, double end);

private slots:

    void selectedChanged();

    void filterUpdated();

    void itemContextMenu(QPoint pos);

protected:
    virtual void hideEvent(QHideEvent *event);
};

#endif
