/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QProgressDialog>
#include <QMessageBox>

#include "datacore/archive/access.hxx"
#include "configurationeditor.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::GUI::Data;

ConfigurationEditorDialog::ConfigurationEditorDialog(QWidget *parent) : QDialog(parent)
{
    setWindowTitle(tr("CPD3 Configuration"));

    auto layout = new QVBoxLayout(this);
    setLayout(layout);

    editor = new DataEditor(this);
    layout->addWidget(editor, 1);

    editor->setArchive("configuration");
    editor->setVariable("displays");
    editor->setSelectionFlags(DataEditor::SelectEverything);

    auto buttons = new QDialogButtonBox(this);
    layout->addWidget(buttons);
    acceptButton = buttons->addButton(QDialogButtonBox::Ok);
    buttons->addButton(QDialogButtonBox::Cancel);
    connect(buttons, &QDialogButtonBox::accepted, this, &ConfigurationEditorDialog::applyChanges);
    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

ConfigurationEditorDialog::~ConfigurationEditorDialog() = default;

void ConfigurationEditorDialog::modifyEditor(const std::function<void()> &call)
{
    editor->disconnect(this);
    call();
    connect(editor, &DataEditor::dataModified, this, &ConfigurationEditorDialog::updateChanged);
    connect(editor, &DataEditor::selectionChanged, this, &ConfigurationEditorDialog::reloadData);
}

void ConfigurationEditorDialog::load(const SequenceName::Component &station,
                                     double start,
                                     double end)
{
    editor->setStation(station);
    editor->setBounds(start, end);
    acceptButton->setEnabled(false);

    struct Context {
        ConfigurationEditorDialog *dialog;
        std::shared_ptr<Archive::Access> access;
        std::shared_ptr<Archive::Access::ReadLock> lock;

        Context(ConfigurationEditorDialog *dlg) : dialog(dlg),
                                                  access(std::make_shared<Archive::Access>()),
                                                  lock(std::make_shared<Archive::Access::ReadLock>(
                                                          *access))
        { }

        ~Context()
        {
            lock.reset();
            access.reset();
        }
    };

    auto context = std::make_shared<Context>(this);
    Threading::pollFuture(this, context->access->availableSelectedFuture(),
                          [context](const Archive::Access::SelectedComponents &result) {
                              context->dialog->availableDataReady(result);
                          });

    reloadData();
}

void ConfigurationEditorDialog::availableDataReady(const Archive::Access::SelectedComponents &available)
{
    availableData = available;

    modifyEditor([this]() {
        editor->setAvailableStations(availableData.stations);
        editor->setAvailableArchives(availableData.archives);
        editor->setAvailableVariables(availableData.variables);
        editor->setAvailableFlavors(availableData.flavors);
    });
}

void ConfigurationEditorDialog::updateChanged()
{
    acceptButton->setEnabled(!editor->isPristine());
}

void ConfigurationEditorDialog::reloadData()
{
    if (!editor->isPristine()) {
        QMessageBox box(this);
        box.setText(tr("The data have been modified."));
        box.setInformativeText(tr("Do you want to save your changes and reload?"));
        box.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        box.setDefaultButton(QMessageBox::Save);
        int sel = box.exec();
        switch (sel) {
        case QMessageBox::Save:
            saveChanges();
            break;
        case QMessageBox::Discard:
            editor->setData(SequenceValue::Transfer());
            acceptButton->setEnabled(false);
            break;
        case QMessageBox::Cancel:
            return;
        default:
            break;
        }
    }

    struct Result {
        CPD3::Data::SequenceValue::Transfer values;
    };

    struct Context {
        ConfigurationEditorDialog *dialog;
        std::shared_ptr<Archive::Access> access;
        std::shared_ptr<Archive::Access::ReadLock> lock;

        Context(ConfigurationEditorDialog *dlg) : dialog(dlg),
                                                  access(std::make_shared<Archive::Access>()),
                                                  lock(std::make_shared<Archive::Access::ReadLock>(
                                                          *access))
        { }

        ~Context()
        {
            lock.reset();
            access.reset();
        }
    };

    CPD3::Data::Archive::Selection selection
            (editor->getStart(), editor->getEnd(), {editor->getStation()}, {editor->getArchive()},
             {editor->getVariable()});

    auto context = std::make_shared<Context>(this);
    std::future<Result> loadResult = std::async(std::launch::async, [context, selection] {
        Result result;
        result.values = context->access->readSynchronous(selection);
        return result;
    });

    QProgressDialog waitDialog(tr("Loading Data"), QString(), 0, 0, this);
    waitDialog.setWindowModality(Qt::ApplicationModal);
    waitDialog.setCancelButton(0);

    Threading::pollFunctor(&waitDialog, [&loadResult, &waitDialog]() {
        if (loadResult.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
            return true;
        waitDialog.close();
        return false;
    });

    if (loadResult.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
        waitDialog.exec();

    loadResult.wait();

    const auto &result = loadResult.get();
    editor->setData(result.values);
}

void ConfigurationEditorDialog::saveChanges()
{
    struct Context {
        ConfigurationEditorDialog *dialog;
        std::shared_ptr<Archive::Access> access;

        SequenceValue::Transfer toModify;
        SequenceValue::Transfer toRemove;

        Context(ConfigurationEditorDialog *dlg) : dialog(dlg),
                                                  access(std::make_shared<Archive::Access>())
        {
            toModify = dialog->editor->getNew();
            toRemove = dialog->editor->getRemoved();
        }

        ~Context() = default;
    };

    auto context = std::make_shared<Context>(this);
    std::future<void> applyResult = std::async(std::launch::async, [context] {
        context->access->writeSynchronous(context->toModify, context->toRemove);
        context->toModify.clear();
        context->toRemove.clear();
    });

    QProgressDialog waitDialog(tr("Saving Data"), QString(), 0, 0, this);
    waitDialog.setWindowModality(Qt::ApplicationModal);
    waitDialog.setCancelButton(0);

    Threading::pollFunctor(&waitDialog, [&applyResult, &waitDialog]() {
        if (applyResult.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
            return true;
        waitDialog.close();
        return false;
    });

    if (applyResult.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
        waitDialog.exec();

    applyResult.wait();

    editor->setData(SequenceValue::Transfer());
    acceptButton->setEnabled(false);
}

void ConfigurationEditorDialog::applyChanges()
{
    if (!editor->isPristine())
        saveChanges();
    return accept();
}