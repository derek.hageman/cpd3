/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3GUI_CONFIGURATIONEDITOR_HXX
#define CPD3GUI_CONFIGURATIONEDITOR_HXX

#include "core/first.hxx"

#include <functional>
#include <QDialog>

#include "datacore/archive/access.hxx"
#include "guidata/dataeditor.hxx"

class ConfigurationEditorDialog : public QDialog {
Q_OBJECT

    CPD3::Data::Archive::Access::SelectedComponents availableData;

    CPD3::GUI::Data::DataEditor *editor;
    QWidget *acceptButton;

    void saveChanges();

    void applyChanges();

    void updateChanged();

    void reloadData();

    void availableDataReady(const CPD3::Data::Archive::Access::SelectedComponents &available);

    void modifyEditor(const std::function<void()> &call);

public:
    ConfigurationEditorDialog(QWidget *parent = nullptr);

    virtual ~ConfigurationEditorDialog();

    void load(const CPD3::Data::SequenceName::Component &station, double start, double end);
};

#endif //CPD3GUI_CONFIGURATIONEDITOR_HXX
