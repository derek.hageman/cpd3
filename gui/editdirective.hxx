/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef EDITDIRECTIVE_H
#define EDITDIRECTIVE_H

#include "core/first.hxx"

#include <memory>
#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QAction>
#include <QStandardItemModel>
#include <QTableView>
#include <QDialog>
#include <QRunnable>
#include <QHash>
#include <QSet>

#include "editing/directivecontainer.hxx"
#include "guicore/timeboundselection.hxx"
#include "guidata/editors/editdirective.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"

class EditDirectiveTableModel;

class EditDirectiveDisplay;

class GUIMainWindow;

typedef std::shared_ptr<CPD3::Editing::DirectiveContainer> DirectivePointer;

Q_DECLARE_METATYPE(DirectivePointer);

class EditDirectiveSingleEditor : public QDialog {
Q_OBJECT

    EditDirectiveDisplay *display;

    DirectivePointer directive;

    double originalStart;
    double originalEnd;
    CPD3::Data::Variant::Root originalValue;

    CPD3::GUI::TimeBoundSelection *bounds;
    CPD3::GUI::Data::EditDirectiveEditor *parameters;

public:
    EditDirectiveSingleEditor(EditDirectiveDisplay *parent, Qt::WindowFlags f = 0);

    /**
     * Configure the editor for a given directive.
     * 
     * @param directive the directive to edit
     */
    void configure(const DirectivePointer &directive);

    /**
     * Set the time selection range.  This changes the times of active
     * directive.
     * 
     * @param start the start time
     * @param end   the end time
     */
    void setSelectedTimes(double start, double end);

    /**
     * Set the available stations.
     * 
     * @param set   the stations
     */
    void setAvailableStations(const CPD3::Data::SequenceName::ComponentSet &set);

    /**
     * Set the available archives.
     * 
     * @param set   the archives
     */
    void setAvailableArchives(const CPD3::Data::SequenceName::ComponentSet &set);

    /**
     * Set the available variables.
     * 
     * @param set   the variables
     */
    void setAvailableVariables(const CPD3::Data::SequenceName::ComponentSet &set);

    /**
     * Set the available flavors.
     * 
     * @param set   the flavors
     */
    void setAvailableFlavors(const CPD3::Data::SequenceName::ComponentSet &set);

signals:

    /**
     * Emitted when the directive has changed.
     * 
     * @param directive the updated directive
     */
    void changed(const DirectivePointer &directive);

    /**
     * Emitted when the directive is reverted due to dialog rejection.
     * 
     * @param directive the updated directive
     */
    void reverted(const DirectivePointer &directive);

public slots:

    virtual void reject();

private slots:

    void changeBounds(double start, double end);

    void changeParameters();
};

class EditDirectiveDisplay : public QDialog {
Q_OBJECT

    GUIMainWindow *mainWindow;

    EditDirectiveSingleEditor *editor;

    friend class EditDirectiveSingleEditor;

    double selectedStart;
    double selectedEnd;

    double loadedStart;
    double loadedEnd;
    CPD3::Data::SequenceName::Component loadedStation;
    QString loadedProfile;

    friend class EditDirectiveTableModel;

    EditDirectiveTableModel *model;
    QTableView *table;

    QPushButton *duplicateButton;
    QPushButton *removeButton;
    QPushButton *modifyButton;

    QAction *displaySystemInternal;
    QAction *displayAllProfiles;
    QAction *displayDeleted;

    struct LoadContext {
        EditDirectiveDisplay *dialog;
        std::shared_ptr<CPD3::Data::Archive::Access> access;
        std::shared_ptr<CPD3::Data::Archive::Access::ReadLock> lock;
        std::mutex mutex;
        bool aborted;

        LoadContext(EditDirectiveDisplay *dlg);

        ~LoadContext();

        void abort();

        bool isAborted();

        CPD3::Threading::Signal<int> progress;
    };

    std::weak_ptr<LoadContext> activeLoad;

    CPD3::Data::Archive::Access::SelectedComponents available;

    void showEditor
            (const DirectivePointer &directive, bool removeOnCancel = false, bool isNew = false);

    void writeChanges();

    int closing();

    int deletedSelected();

public:
    EditDirectiveDisplay(GUIMainWindow *win = 0, Qt::WindowFlags f = 0);

    ~EditDirectiveDisplay();

    void load(const CPD3::Data::SequenceName::Component &station,
              const QString &profile,
              double start,
              double end);

    bool mainWindowClosing();

public slots:

    /**
     * Set the time selection range.  This is the range used for new
     * directives and is propagated to the editor dialog.
     * 
     * @param start the start time
     * @param end   the end time
     * @return      true if the selection was consumed
     */
    bool setSelectedTimes(double start, double end);

protected:
    virtual void closeEvent(QCloseEvent *event);

private slots:

    void directiveUpdated(const DirectivePointer &pointer);

    void directiveRejected(const DirectivePointer &directive);

    void saveChanges();

    void confirmReject();

    void addDirective();

    void removeDirective();

    void modifyDirective();

    void duplicateDirective();

    void displayUpdated();

    void highlightSelection();

    void refocusDialog();

    void selectedChanged();
};

#endif
