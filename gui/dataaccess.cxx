/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "dataaccess.hxx"
#include "gui.hxx"
#include "core/threading.hxx"

using namespace CPD3;
using namespace CPD3::Data;

DataAccess::DataAccess(CPD3GUI &gui) : gui(gui),
                                       archiveBackend(*this),
                                       remoteReadSink(*this),
                                       mutex(),
                                       stopInProgress(false),
                                       remoteCachePurgeTime(FP::undefined()),
                                       remoteReadState(RemoteRead_None)
{ }

CPD3::Data::ProcessingTapChain::ArchiveBackend *DataAccess::getArchiveBackend()
{ return &archiveBackend; }


DataAccess::RemoteReadResult DataAccess::readFromRemote(const std::shared_ptr<
        DataAccess::ActiveReadContext> &context)
{
    std::unique_lock<std::mutex> lock(mutex);
    if (stopInProgress) {
        DataAccess::RemoteReadResult result;
        result.ended = true;
        return result;
    }

    remoteReadUpdated.wait(lock, [this] { return remoteReadState == RemoteRead_None; });
    Q_ASSERT(remoteReadState == RemoteRead_None);

    remoteReadSelections = context->getRemoteReadSelections();
    remoteReadState = RemoteRead_Request;

    bool haveRemote = false;
    bool haveOngoing = false;
    Threading::runQueuedFunctor(&gui, [this, &haveOngoing, &haveRemote] {
        std::unique_lock<std::mutex> ul(mutex);
        haveRemote = gui.haveRemoteData();
        haveOngoing = gui.isRealtime();

        if (remoteReadState != RemoteRead_Request)
            return;
        if (!haveRemote || allInRemoteCache()) {
            remoteReadState = RemoteRead_Completed;
            ul.unlock();
            remoteReadUpdated.notify_all();
            return;
        }

        remoteReadState = RemoteRead_Active;
        ul.unlock();
        gui.startRemoteRead(&remoteReadSink, remoteReadSelections);
    });

    remoteReadUpdated.wait(lock, [this] { return remoteReadState == RemoteRead_Completed; });
    Q_ASSERT(remoteReadState == RemoteRead_Completed);
    remoteReadState = RemoteRead_None;

    DataAccess::RemoteReadResult result;
    if (stopInProgress) {
        result.ended = true;
        return result;
    }

    if (haveRemote)
        result.data = satisfyFromRemoteCache(context->getFurthestTime());
    if (haveRemote && haveOngoing) {
        result.ended = false;
        connectedRealtime.emplace_back(context);

        /* Request a realtime resend, so the latest values get sent to the new connection
         * (specifically metadata and presistent values).  We could keep a second cache
         * of anything that would presist and satisfy from that, but this is simplier. */
        Threading::runQueuedFunctor(&gui, [this] {
            gui.resendRealtime();
        });
    } else {
        result.ended = true;
    }

    /* If it's received archive data, then we know we can drop anything before the
     * end of the archive data from the cache */
    if (Range::compareStart(remoteCachePurgeTime, context->getFurthestTime()) > 0)
        remoteCachePurgeTime = context->getFurthestTime();

    lock.unlock();
    remoteReadUpdated.notify_all();
    return result;
}

void CPD3GUI::startRemoteRead(StreamSink *sink, const Archive::Selection::List &selection)
{
    if (!acquisitionConnection) {
        sink->endData();
        return;
    }
    Q_ASSERT(this->thread() == QThread::currentThread());
    acquisitionConnection->remoteDataRead(sink, selection);
}

void CPD3GUI::resendRealtime()
{
    if (!acquisitionConnection)
        return;
    Q_ASSERT(this->thread() == QThread::currentThread());
    acquisitionConnection->realtimeResend();
}

static std::size_t selectionMatchMix(std::size_t h, const Archive::Selection::Match &add)
{
    if (add.empty())
        return h;
    std::unordered_set<SequenceName::Component> u(add.begin(), add.end());
    u.clear();
    std::vector<SequenceName::Component> sorted(u.cbegin(), u.cend());
    std::sort(sorted.begin(), sorted.end());
    for (const auto &v : sorted) {
        h = INTEGER::mix(h, std::hash<SequenceName::Component>()(v));
    }
    return h;
}

std::size_t DataAccess::SelectionHash::operator()(const Archive::Selection &selection) const
{
    std::size_t h = 0;
    if (FP::defined(selection.getStart()))
        h = std::hash<double>()(selection.getStart());
    if (FP::defined(selection.getEnd()))
        h = INTEGER::mix(h, std::hash<double>()(selection.getEnd()));
    h = selectionMatchMix(h, selection.stations);
    h = selectionMatchMix(h, selection.archives);
    h = selectionMatchMix(h, selection.variables);
    h = selectionMatchMix(h, selection.hasFlavors);
    h = selectionMatchMix(h, selection.lacksFlavors);
    h = selectionMatchMix(h, selection.exactFlavors);
    return h;
}

static bool selectionMatchEqual(const Archive::Selection::Match &a,
                                const Archive::Selection::Match &b)
{
    return std::unordered_set<SequenceName::Component>(a.begin(), a.end()) ==
            std::unordered_set<SequenceName::Component>(b.begin(), b.end());
}

bool DataAccess::SelectionEqual::operator()(const Archive::Selection &a,
                                            const Archive::Selection &b) const
{
    if (!FP::equal(a.getStart(), b.getStart()))
        return false;
    if (!FP::equal(a.getEnd(), b.getEnd()))
        return false;
    if (!selectionMatchEqual(a.stations, b.stations))
        return false;
    if (!selectionMatchEqual(a.archives, b.archives))
        return false;
    if (!selectionMatchEqual(a.variables, b.variables))
        return false;
    if (!selectionMatchEqual(a.stations, b.stations))
        return false;
    if (!selectionMatchEqual(a.hasFlavors, b.hasFlavors))
        return false;
    if (!selectionMatchEqual(a.lacksFlavors, b.lacksFlavors))
        return false;
    if (!selectionMatchEqual(a.exactFlavors, b.exactFlavors))
        return false;
    return true;
}

bool DataAccess::allInRemoteCache() const
{
    for (const auto &check : remoteReadSelections) {
        if (remoteCacheContents.count(check) == 0)
            return false;
    }
    return true;
}

SequenceValue::Transfer DataAccess::satisfyFromRemoteCache(double beginTime) const
{
    std::unique_ptr<SequenceMatch::Basic>
            match(SequenceMatch::Basic::compile(remoteReadSelections, beginTime));

    SequenceValue::Transfer result;
    for (const auto &v : remoteCache) {
        if (Range::compareStart(v.first.getStart(), beginTime) <= 0)
            continue;
        if (!match->matches(v.first.getName(), v.first.getStart(), v.first.getEnd()))
            continue;
        result.emplace_back(v.first, v.second);
    }
    return result;
}

SequenceName::Set DataAccess::cachedMatches(const SequenceMatch::Composite &selection)
{
    if (!selection.valid())
        return {};

    SequenceName::Set result;
    std::lock_guard<std::mutex> lock(mutex);
    for (const auto &v : remoteCache) {
        auto add = v.first.getName();
        add.clearFlavors();
        add.clearMeta();
        if (add.isDefaultStation() && !gui.getStation().empty())
            add.setStation(gui.getStation());
        if (!selection.matches(add))
            continue;
        result.insert(std::move(add));
    }
    return result;
}

void DataAccess::releaseAccess(const std::shared_ptr<DataAccess::ActiveReadContext> &context)
{
    std::lock_guard<std::mutex> lock(mutex);
    for (auto rt = connectedRealtime.begin(); rt != connectedRealtime.end();) {
        auto l = rt->lock();
        if (!l) {
            rt = connectedRealtime.erase(rt);
            continue;
        }
        if (l.get() == context.get()) {
            rt = connectedRealtime.erase(rt);
            continue;
        }
        ++rt;
    }
}

std::vector<std::shared_ptr<DataAccess::ActiveReadContext>> DataAccess::lockRealtime()
{
    std::vector<std::shared_ptr<DataAccess::ActiveReadContext>> result;
    std::lock_guard<std::mutex> lock(mutex);
    for (auto rt = connectedRealtime.begin(); rt != connectedRealtime.end();) {
        auto l = rt->lock();
        if (!l) {
            rt = connectedRealtime.erase(rt);
            continue;
        }
        result.emplace_back(std::move(l));
        ++rt;
    }
    return result;
}

void DataAccess::stop()
{
    std::lock_guard<std::mutex> lock(mutex);
    stopInProgress = true;
    /* This is handled by the GUI sending the abort to the remote reader */
    /*if (remoteReadState != RemoteRead_None) {
        remoteReadState = RemoteRead_Completed;
        remoteReadUpdated.notify_all();
    }*/
    for (const auto &s : connectedRealtime) {
        auto l = s.lock();
        if (!l)
            continue;
        l->stop();
    }
    connectedRealtime.clear();

    {
        auto actx = accessContext.lock();
        if (actx) {
            actx->access.signalTerminate();
        }
    }
    accessContext.reset();
}

void DataAccess::restart()
{
    std::lock_guard<std::mutex> lock(mutex);
    stopInProgress = false;
}


DataAccess::RemoteReadSink::RemoteReadSink(DataAccess &access) : access(access)
{ }

DataAccess::RemoteReadSink::~RemoteReadSink() = default;

void DataAccess::RemoteReadSink::incomingData(const SequenceValue::Transfer &values)
{
    std::lock_guard<std::mutex> lock(access.mutex);
    for (const auto &add : values) {
        Util::insert_or_assign(access.remoteCache, add, add.root());
    }
}

void DataAccess::RemoteReadSink::incomingData(SequenceValue::Transfer &&values)
{
    std::lock_guard<std::mutex> lock(access.mutex);
    for (auto &add : values) {
        Util::insert_or_assign(access.remoteCache, std::move(add.getIdentity()),
                               std::move(add.root()));
    }
}

void DataAccess::RemoteReadSink::incomingData(const SequenceValue &value)
{
    std::lock_guard<std::mutex> lock(access.mutex);
    Util::insert_or_assign(access.remoteCache, value, value.root());
}

void DataAccess::RemoteReadSink::incomingData(SequenceValue &&value)
{
    std::lock_guard<std::mutex> lock(access.mutex);
    Util::insert_or_assign(access.remoteCache, std::move(value.getIdentity()),
                           std::move(value.root()));
}

void DataAccess::RemoteReadSink::endData()
{
    std::lock_guard<std::mutex> lock(access.mutex);
    if (access.remoteReadState != RemoteRead_Active)
        return;
    access.remoteReadState = RemoteRead_Completed;
    access.remoteReadUpdated.notify_all();
}


static bool retainRealtime(const SequenceValue &value)
{
    static const SequenceName::Component archiveRaw = "raw";
    static const SequenceName::Component archiveRawMeta = "raw_meta";
    const auto &check = value.getArchive();
    return check == archiveRaw || check == archiveRawMeta;
}

void DataAccess::incomingRealtime(const SequenceValue &value)
{
    sendToRealtimeConnected(value);
    if (!retainRealtime(value))
        return;
    std::lock_guard<std::mutex> lock(mutex);
    Util::insert_or_assign(remoteCache, value, value.root());
}

void DataAccess::incomingRealtime(SequenceValue &&value)
{
    sendToRealtimeConnected(value);
    if (!retainRealtime(value))
        return;
    std::lock_guard<std::mutex> lock(mutex);
    Util::insert_or_assign(remoteCache, std::move(value.getIdentity()), std::move(value.root()));
}

void DataAccess::incomingRealtime(const CPD3::Data::SequenceValue::Transfer &values)
{
    sendToRealtimeConnected(values);
    std::lock_guard<std::mutex> lock(mutex);
    for (const auto &add : values) {
        if (!retainRealtime(add))
            continue;
        Util::insert_or_assign(remoteCache, add, add.root());
    }
}

void DataAccess::incomingRealtime(CPD3::Data::SequenceValue::Transfer &&values)
{
    sendToRealtimeConnected(values);
    std::lock_guard<std::mutex> lock(mutex);
    for (auto &add : values) {
        if (!retainRealtime(add))
            continue;
        Util::insert_or_assign(remoteCache, std::move(add.getIdentity()), std::move(add.root()));
    }
}

void DataAccess::sendToRealtimeConnected(const CPD3::Data::SequenceValue &value)
{
    for (const auto &target : lockRealtime()) {
        target->incomingRealtime(value);
    }
}

void DataAccess::sendToRealtimeConnected(const CPD3::Data::SequenceValue::Transfer &values)
{
    for (const auto &target : lockRealtime()) {
        target->incomingRealtime(values);
    }
}

void DataAccess::trimCached(double retainBegin)
{
    if (!FP::defined(retainBegin))
        return;

    SequenceIdentity purgeLimit;
    purgeLimit.setStart(retainBegin);

    std::lock_guard<std::mutex> lock(mutex);

    /* Ignore the trim if we're still loading data (since it's being loaded into the cache) */
    if (remoteReadState != RemoteRead_None)
        return;

    remoteCache.erase(remoteCache.begin(), remoteCache.lower_bound(purgeLimit));

    /* Remote any content indicators that intersect the time before the trim, since
     * we no longer contain their full contents */
    for (auto check = remoteCacheContents.begin(); check != remoteCacheContents.end();) {
        if (!FP::defined(check->getEnd()) || check->getEnd() > retainBegin) {
            check = remoteCacheContents.erase(check);
            continue;
        }
        ++check;
    }
}

void DataAccess::purgeCache()
{
    std::lock_guard<std::mutex> lock(mutex);
    remoteCache.clear();
    remoteCacheContents.clear();
}


DataAccess::ArchiveBackend::ArchiveBackend(DataAccess &access) : access(access)
{ }

DataAccess::ArchiveBackend::~ArchiveBackend() = default;

void DataAccess::ArchiveBackend::issueArchiveRead(const Archive::Selection::List &selections,
                                                  StreamSink *sink)
{ ActiveReadContext::launch(access, selections, sink); }


DataAccess::ActiveReadContext::ActiveReadContext(std::shared_ptr<AccessContext> &&actx,
                                                 const Archive::Selection::List &selections,
                                                 CPD3::Data::StreamSink *sink) : access(
        std::move(actx)),
                                                                                 selections(
                                                                                         selections),
                                                                                 sink(sink),
                                                                                 furthestTime(
                                                                                         FP::undefined()),
                                                                                 state(Running),
                                                                                 archiveReadSink(
                                                                                         *this)
{
    archiveReader = access->access.readStream(selections, &archiveReadSink);
}

DataAccess::AccessContext::AccessContext(DataAccess &data) : data(data), access()
{ }

void DataAccess::ActiveReadContext::launch(DataAccess &access,
                                           const Archive::Selection::List &selections,
                                           StreamSink *sink)
{
    std::shared_ptr<AccessContext> actx;
    {
        std::lock_guard<std::mutex> lock(access.mutex);
        if (access.stopInProgress) {
            sink->endData();
            return;
        }
        actx = access.accessContext.lock();
        if (!actx) {
            actx = std::make_shared<AccessContext>(access);
            access.accessContext = actx;
        }
    }
    std::thread(ActiveReadContext::threadRun,
                std::make_shared<ActiveReadContext>(std::move(actx), selections, sink)).detach();
}

void DataAccess::ActiveReadContext::threadRun(const std::shared_ptr<
        DataAccess::ActiveReadContext> &context)
{
    DataAccess &access = context->access->data;
    context->archiveReader->wait();
    context->archiveReader.reset();
    context->access.reset();

    {
        double limit = context->getFurthestTime();
        if (FP::defined(limit)) {
            /* Remove any selections that have been satisfied entirely from the local,
             * and adjust the remaining ones to start at the end of the local */
            for (auto sel = context->selections.begin(); sel != context->selections.end();) {
                if (FP::defined(sel->getEnd()) && sel->getEnd() <= limit) {
                    sel = context->selections.erase(sel);
                    continue;
                }
                sel->setStart(limit);
                ++sel;
            }
        }

        if (context->selections.empty()) {
            context->sink->endData();
            std::lock_guard<std::mutex> lock(context->mutex);
            context->state = Finished;
            context->externalUpdate.notify_all();
            return;
        }

        context->dataFilter = SequenceMatch::Basic::compile(context->selections, limit);
    }

    {
        auto remote = access.readFromRemote(context);

        if (!remote.data.empty()) {
            context->furthestTime = remote.data.back().getStart();
            context->sink->incomingData(std::move(remote.data));
        }

        if (remote.ended) {
            context->sink->endData();
            std::lock_guard<std::mutex> lock(context->mutex);
            context->state = Finished;
            context->externalUpdate.notify_all();
            return;
        }
    }

    return context->realtimeRun();
}

void DataAccess::ActiveReadContext::realtimeRun()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        switch (state) {
        case Running:
            break;
        case Stop_Requested:
            lock.unlock();
            sink->endData();
            lock.lock();
            state = Finished;
            externalUpdate.notify_all();
            return;
        case Finished:
            Q_ASSERT(false);
            break;
        }

        if (!pendingRealtime.empty()) {
            SequenceValue::Transfer processRealtime = std::move(pendingRealtime);
            pendingRealtime.clear();
            lock.unlock();

            sink->incomingData(std::move(processRealtime));

            lock.lock();
            continue;
        }

        externalUpdate.wait(lock);
    }
}

void DataAccess::ActiveReadContext::processRealtimeValue(const SequenceValue &value)
{
    if (Range::compareStart(furthestTime, value.getStart()) > 0)
        return;

    {
        auto check = dataPrefilterResult.find(value.getName());
        if (check == dataPrefilterResult.end()) {
            check = dataPrefilterResult.emplace(value.getName(),
                                                dataFilter->matches(value.getName())).first;
        }
        if (!check->second)
            return;
    }

    if (!dataFilter->matches(value))
        return;
    furthestTime = value.getStart();
    pendingRealtime.emplace_back(value);
}

void DataAccess::ActiveReadContext::incomingRealtime(const SequenceValue &value)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        processRealtimeValue(value);
        if (pendingRealtime.empty())
            return;
    }
    externalUpdate.notify_all();
}

void DataAccess::ActiveReadContext::incomingRealtime(const SequenceValue::Transfer &values)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        for (const auto &add : values) {
            processRealtimeValue(add);
        }
        if (pendingRealtime.empty())
            return;
    }
    externalUpdate.notify_all();
}

void DataAccess::ActiveReadContext::stop()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        switch (state) {
        case Running:
            state = Stop_Requested;
            externalUpdate.notify_all();
            break;
        case Stop_Requested:
            externalUpdate.wait(lock);
            break;
        case Finished:
            return;
        }
    }
}

Archive::Selection::List DataAccess::ActiveReadContext::getRemoteReadSelections() const
{ return selections; }

double DataAccess::ActiveReadContext::getFurthestTime() const
{ return furthestTime; }


DataAccess::ActiveReadContext::ArchiveReadSink::ArchiveReadSink(ActiveReadContext &context)
        : context(context)
{ }

DataAccess::ActiveReadContext::ArchiveReadSink::~ArchiveReadSink() = default;

void DataAccess::ActiveReadContext::ArchiveReadSink::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    context.furthestTime = values.back().getStart();
    context.sink->incomingData(values);
}

void DataAccess::ActiveReadContext::ArchiveReadSink::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    context.furthestTime = values.back().getStart();
    context.sink->incomingData(std::move(values));
}

void DataAccess::ActiveReadContext::ArchiveReadSink::incomingData(const CPD3::Data::SequenceValue &value)
{
    context.furthestTime = value.getStart();
    context.sink->incomingData(value);
}

void DataAccess::ActiveReadContext::ArchiveReadSink::incomingData(CPD3::Data::SequenceValue &&value)
{
    context.furthestTime = value.getStart();
    context.sink->incomingData(std::move(value));
}

void DataAccess::ActiveReadContext::ArchiveReadSink::endData()
{
    /* The thread waits for the end of the archive read operation, so we can just
     * ignore this entirely */
}
