/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QStringList>

#include "core/number.hxx"

#include "interfacehandler.hxx"
#include "gui.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

HandlerCommon::HandlerCommon(CPD3GUI *p) : QObject(p),
                                           parent(p),
                                           display(),
                                           displayUpdateTimer(this),
                                           displayModeList(),
                                           hadInstantDisplay(false),
                                           hadBoxcarDisplay(false),
                                           hadAverageDisplay(false),
                                           displayMode(Instant),
                                           displayPage(0),
                                           layoutInstant(),
                                           layoutBoxcar(),
                                           layoutAverage()
{
    displayUpdateTimer.setSingleShot(true);
    displayUpdateTimer.setInterval(
            parent->getSettings()->value("display/repaintinterval", 1000).toInt());
    connect(&displayUpdateTimer, SIGNAL(timeout()), this, SLOT(performDisplayUpdate()));
}

HandlerCommon::~HandlerCommon()
{ }

bool HandlerCommon::hasVisibleWindow() const
{
    return layoutInstant.pages() != 0 || layoutBoxcar.pages() != 0 || layoutAverage.pages() != 0;
}

void HandlerCommon::updateRealtime()
{
    bool discardCache = layoutInstant.update();
    discardCache = layoutBoxcar.update() || discardCache;
    discardCache = layoutAverage.update() || discardCache;

    if (discardCache) {
        if (!display.isNull())
            display->forceUpdate();
        emit displayUpdated();
    }
}

void HandlerCommon::validateDisplayRanges(int modeDirection, int pageDirection)
{
    bool haveInstantDisplay = (layoutInstant.pages() != 0);
    bool haveBoxcarDisplay = (layoutBoxcar.pages() != 0);
    bool haveAverageDisplay = (layoutAverage.pages() != 0);
    if (haveInstantDisplay != hadInstantDisplay ||
            haveBoxcarDisplay != hadBoxcarDisplay ||
            haveAverageDisplay != hadAverageDisplay) {
        hadInstantDisplay = haveInstantDisplay;
        hadBoxcarDisplay = haveBoxcarDisplay;
        hadAverageDisplay = haveAverageDisplay;

        QStringList availableModes;
        if (haveInstantDisplay)
            availableModes.append(tr("Instant"));
        if (hadBoxcarDisplay)
            availableModes.append(tr("Boxcar"));
        if (hadAverageDisplay)
            availableModes.append(tr("Average"));
        if (displayModeList != availableModes) {
            displayModeList = availableModes;

            if (!display.isNull()) {
                display->setDisplaySelectionModes(displayModeList);
            }
        }
    }

    switch (displayMode) {
    case Instant:
        if (layoutInstant.pages() == 0) {
            if (modeDirection >= 0) {
                if (layoutBoxcar.pages() > 0) {
                    displayMode = Boxcar;
                    validateDisplayRanges(0, pageDirection);

                    if (!display.isNull())
                        display->forceUpdate();
                    return;
                } else if (layoutAverage.pages() > 0) {
                    displayMode = Average;
                    validateDisplayRanges(0, pageDirection);
                    if (!display.isNull())
                        display->forceUpdate();
                    return;
                } else {
                    displayPage = 0;
                    if (!display.isNull()) {
                        display->setPages(0);
                        display->forceUpdate();
                    }
                    return;
                }
            } else {
                if (layoutAverage.pages() > 0) {
                    displayMode = Average;
                    validateDisplayRanges(0, pageDirection);
                    if (!display.isNull())
                        display->forceUpdate();
                    return;
                } else if (layoutBoxcar.pages() > 0) {
                    displayMode = Boxcar;
                    validateDisplayRanges(0, pageDirection);
                    if (!display.isNull())
                        display->forceUpdate();
                    return;
                } else {
                    displayPage = 0;
                    if (!display.isNull()) {
                        display->setPages(0);
                        display->forceUpdate();
                    }
                    return;
                }
            }
        } else if (displayPage < 0 || displayPage >= layoutInstant.pages()) {
            if (pageDirection >= 0)
                displayPage = 0;
            else
                displayPage = layoutInstant.pages() - 1;
        }
        if (!display.isNull()) {
            display->setPages(layoutInstant.pages());
            display->setSelectedPage(displayPage + 1);
            display->setSelectedDisplay(0);
        }
        break;
    case Boxcar:
        if (layoutBoxcar.pages() == 0) {
            if (modeDirection >= 0) {
                if (layoutAverage.pages() > 0) {
                    displayMode = Average;
                    validateDisplayRanges(0, pageDirection);
                    if (!display.isNull())
                        display->forceUpdate();
                    return;
                } else if (layoutInstant.pages() > 0) {
                    displayMode = Instant;
                    validateDisplayRanges(0, pageDirection);
                    if (!display.isNull())
                        display->forceUpdate();
                    return;
                } else {
                    displayPage = 0;
                    if (!display.isNull()) {
                        display->setPages(0);
                        display->forceUpdate();
                    }
                    return;
                }
            } else {
                if (layoutInstant.pages() > 0) {
                    displayMode = Instant;
                    validateDisplayRanges(0, pageDirection);
                    if (!display.isNull())
                        display->forceUpdate();
                    return;
                } else if (layoutAverage.pages() > 0) {
                    displayMode = Average;
                    validateDisplayRanges(0, pageDirection);
                    if (!display.isNull())
                        display->forceUpdate();
                    return;
                } else {
                    displayPage = 0;
                    if (!display.isNull()) {
                        display->setPages(0);
                        display->forceUpdate();
                    }
                    return;
                }
            }
        } else if (displayPage < 0 || displayPage >= layoutBoxcar.pages()) {
            if (pageDirection >= 0)
                displayPage = 0;
            else
                displayPage = layoutBoxcar.pages() - 1;
        }
        if (!display.isNull()) {
            display->setPages(layoutBoxcar.pages());
            display->setSelectedPage(displayPage + 1);
            if (hadInstantDisplay) {
                display->setSelectedDisplay(1);
            } else {
                display->setSelectedDisplay(0);
            }
        }
        break;
    case Average:
        if (layoutAverage.pages() == 0) {
            if (modeDirection >= 0) {
                if (layoutInstant.pages() > 0) {
                    displayMode = Instant;
                    validateDisplayRanges(0, pageDirection);
                    if (!display.isNull())
                        display->forceUpdate();
                    return;
                } else if (layoutBoxcar.pages() > 0) {
                    displayMode = Boxcar;
                    validateDisplayRanges(0, pageDirection);
                    if (!display.isNull())
                        display->forceUpdate();
                    return;
                } else {
                    displayPage = 0;
                    if (!display.isNull()) {
                        display->setPages(0);
                        display->forceUpdate();
                    }
                    return;
                }
            } else {
                if (layoutBoxcar.pages() > 0) {
                    displayMode = Boxcar;
                    validateDisplayRanges(0, pageDirection);
                    if (!display.isNull())
                        display->forceUpdate();
                    return;
                } else if (layoutInstant.pages() > 0) {
                    displayMode = Instant;
                    validateDisplayRanges(0, pageDirection);
                    if (!display.isNull())
                        display->forceUpdate();
                    return;
                } else {
                    displayPage = 0;
                    if (!display.isNull()) {
                        display->setPages(0);
                        display->forceUpdate();
                    }
                    return;
                }
            }
        } else if (displayPage < 0 || displayPage >= layoutAverage.pages()) {
            if (pageDirection >= 0)
                displayPage = 0;
            else
                displayPage = layoutAverage.pages() - 1;
        }
        if (!display.isNull()) {
            display->setPages(layoutAverage.pages());
            display->setSelectedPage(displayPage + 1);
            if (hadInstantDisplay) {
                if (hadBoxcarDisplay) {
                    display->setSelectedDisplay(2);
                } else {
                    display->setSelectedDisplay(1);
                }
            } else if (hadBoxcarDisplay) {
                display->setSelectedDisplay(1);
            } else {
                display->setSelectedDisplay(0);
            }
        }
        break;
    }
}

void HandlerCommon::updateDisplayContents()
{
    validateDisplayRanges();

    switch (displayMode) {
    case Instant:
        if (displayPage < 0 || displayPage >= layoutInstant.pages()) {
            display->forceUpdate();
            return;
        }
        display->update(layoutInstant.begin(displayPage), layoutInstant.end(displayPage),
                        criticalText());
        break;
    case Boxcar:
        if (displayPage < 0 || displayPage >= layoutBoxcar.pages()) {
            display->forceUpdate();
            return;
        }
        display->update(layoutBoxcar.begin(displayPage), layoutBoxcar.end(displayPage),
                        criticalText());
        break;
    case Average:
        if (displayPage < 0 || displayPage >= layoutAverage.pages()) {
            display->forceUpdate();
            return;
        }
        display->update(layoutAverage.begin(displayPage), layoutAverage.end(displayPage),
                        criticalText());
        break;
    }
}

void HandlerCommon::performDisplayUpdate()
{
    updateRealtime();

    if (!display.isNull()) {
        updateDisplayContents();
        QMetaObject::invokeMethod(display.data(), "update", Qt::QueuedConnection);
    }
}

RealtimeDisplay *HandlerCommon::displayWidget()
{
    if (!display.isNull())
        return display.data();
    RealtimeDisplay *result = new RealtimeDisplay;

    connect(result, SIGNAL(displaySelectionChanged(int)), this, SLOT(displaySelectionChanged(int)));
    connect(result, SIGNAL(pageSelectionChanged(int)), this, SLOT(pageSelectionChanged(int)));

    display = QPointer<RealtimeDisplay>(result);
    updateRealtime();
    updateDisplayContents();
    return result;
}

void HandlerCommon::displaySelectionChanged(int index)
{
    /* Use the cached "had" variables since they reflect the last list 
     * update. */
    if (index == 0) {
        if (hadInstantDisplay) {
            if (displayMode != Instant) {
                displayMode = Instant;
                if (!display.isNull())
                    display->forceUpdate();
                performDisplayUpdate();
                emit displayUpdated();
            }
            return;
        }
        if (hadBoxcarDisplay) {
            if (displayMode != Boxcar) {
                displayMode = Boxcar;
                if (!display.isNull())
                    display->forceUpdate();
                performDisplayUpdate();
                emit displayUpdated();
            }
            return;
        }
        if (hadAverageDisplay) {
            if (displayMode != Average) {
                displayMode = Average;
                if (!display.isNull())
                    display->forceUpdate();
                performDisplayUpdate();
                emit displayUpdated();
            }
            return;
        }
    } else if (index == 1) {
        if (hadInstantDisplay && hadBoxcarDisplay) {
            if (displayMode != Boxcar) {
                displayMode = Boxcar;
                if (!display.isNull())
                    display->forceUpdate();
                performDisplayUpdate();
                emit displayUpdated();
            }
            return;
        }
        if (!hadInstantDisplay && hadBoxcarDisplay && hadAverageDisplay) {
            if (displayMode != Average) {
                displayMode = Average;
                if (!display.isNull())
                    display->forceUpdate();
                performDisplayUpdate();
                emit displayUpdated();
            }
            return;
        }
    } else if (index == 2) {
        if (hadInstantDisplay && hadBoxcarDisplay && hadAverageDisplay) {
            if (displayMode != Average) {
                displayMode = Average;
                if (!display.isNull())
                    display->forceUpdate();
                performDisplayUpdate();
                emit displayUpdated();
            }
            return;
        }
    }
}

void HandlerCommon::pageSelectionChanged(int index)
{
    --index;
    if (index == displayPage)
        return;
    int direction = (index > displayPage ? 1 : -1);
    displayPage = index;
    updateRealtime();
    validateDisplayRanges(0, direction);

    if (!display.isNull())
        display->forceUpdate();
    performDisplayUpdate();
    emit displayUpdated();
}

void HandlerCommon::triggerUpdate()
{
    if (!displayUpdateTimer.isActive()) {
        QMetaObject::invokeMethod(this, "performDisplayUpdate", Qt::QueuedConnection);
        displayUpdateTimer.start();
    }
}

QString HandlerCommon::criticalText() const
{ return QString(); }
