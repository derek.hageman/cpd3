/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef REALTIMEDISPLAY_H
#define REALTIMEDISPLAY_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QWidget>
#include <QGridLayout>
#include <QComboBox>
#include <QSpinBox>
#include <QLabel>

#include "acquisition/realtimelayout.hxx"

class RealtimeDisplay : public QWidget {
Q_OBJECT

    QGridLayout *baseLayout;
    QWidget *layoutWidget;
    QList<QWidget *> elements;
    QComboBox *displaySelection;
    QSpinBox *pageSelection;
    QLabel *criticalLabel;
public:
    RealtimeDisplay(QWidget *parent = 0, Qt::WindowFlags f = 0);

    /**
     * Discard the widget cache and regenerate the display on the next update.
     */
    void forceUpdate();

    /**
     * Update the display.
     * 
     * @param begin     the start of the layout
     * @param end       the end of the layout
     * @param overrideCritical a critical message to display over all contents
     */
    void update(const CPD3::Acquisition::RealtimeLayout::iterator &begin,
                const CPD3::Acquisition::RealtimeLayout::iterator &end,
                const QString &overrideCritical = QString());

    /**
     * Set the display selection modes to show.  Set to empty to hide the
     * display selection list.
     * 
     * @param model the model
     */
    void setDisplaySelectionModes(const QStringList &modes);

    /**
     * Set the index selected in the display list.
     * 
     * @param index the index to select
     */
    void setSelectedDisplay(int index);

    /**
     * Set the number of pages to select.  Set to < 1 to hide the page
     * selection widget.
     * 
     * @param n the number of pages
     */
    void setPages(int n);

    /**
     * Set the selected page.
     * 
     * @param page the page selected
     */
    void setSelectedPage(int page);

signals:

    /**
     * Emitted when the display selection is changed.
     * 
     * @param index the index in the model
     */
    void displaySelectionChanged(int index);

    /**
     * Emitted when the page selection is changed.
     * 
     * @param index the page index
     */
    void pageSelectionChanged(int index);

    /**
     * Emitted when the internal layout has been rebuilt.
     */
    void layoutRebuilt();

public slots:

    void updateGeometry();

protected:
    void showEvent(QShowEvent *event);
};

#endif
