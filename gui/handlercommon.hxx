/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef HANDLERCOMMON_H
#define HANDLERCOMMON_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QWidget>
#include <QPointer>
#include <QTimer>
#include <QStringListModel>

#include "datacore/stream.hxx"
#include "acquisition/realtimelayout.hxx"

#include "realtimedisplay.hxx"

class CPD3GUI;

/**
 * The common base for the realtime handlers.  This implements the basic
 * display functionality (instant, boxcar, and average displays with
 * page selection).
 */
class HandlerCommon : public QObject {
Q_OBJECT

    CPD3GUI *parent;

    QPointer<RealtimeDisplay> display;
    QTimer displayUpdateTimer;
    QStringList displayModeList;
    bool hadInstantDisplay;
    bool hadBoxcarDisplay;
    bool hadAverageDisplay;
    enum {
        Instant, Boxcar, Average
    } displayMode;
    int displayPage;

    void updateDisplayContents();

    void validateDisplayRanges(int modeDirection = 0, int pageDirection = 0);

protected:
    /**
     * The realtime layout.  Child classes must update this as needed.
     */
    CPD3::Acquisition::RealtimeLayout layoutInstant;
    /**
     * The boxcar layout.  Child classes must update this as needed.
     */
    CPD3::Acquisition::RealtimeLayout layoutBoxcar;
    /**
     * The average layout.  Child classes must update this as needed.
     */
    CPD3::Acquisition::RealtimeLayout layoutAverage;

    /**
     * Trigger an update.  This may differ the actual update if there
     * has been one recently.  This is normally used on incoming data to
     * update the display when needed.
     */
    void triggerUpdate();

    /**
     * Get the text to display in the realtime critical slot.  This normally
     * returns an empty string (do not display anything).
     * 
     * @return the critical override text
     */
    virtual QString criticalText() const;

public:
    /**
     * Create the handler.
     * 
     * @param parent    the parent GUI
     */
    HandlerCommon(CPD3GUI *parent);

    virtual ~HandlerCommon();

    /**
     * Check if the interface has a visible window.
     * 
     * @return true if there is a window to display
     */
    bool hasVisibleWindow() const;

    /**
     * Update the realtime layouts.
     */
    void updateRealtime();

    /**
     * Get the display widget for the handler.  Ownership is expected to be
     * transferred to another object.
     * 
     * @return the display widget
     */
    RealtimeDisplay *displayWidget();

signals:

    /**
     * Emitted when the basic elements of the display have updated.  This can
     * be used to check if the display still exists.
     */
    void displayUpdated();

private slots:

    void performDisplayUpdate();

    void displaySelectionChanged(int index);

    void pageSelectionChanged(int index);
};

Q_DECLARE_METATYPE(HandlerCommon *);

#endif
