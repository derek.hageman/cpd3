/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <utility>
#include <functional>

#include <QEventLoop>
#include <QMessageBox>

#include "core/number.hxx"
#include "core/range.hxx"
#include "core/timeutils.hxx"
#include "core/waitutils.hxx"
#include "core/threadpool.hxx"
#include "core/memory.hxx"
#include "graphing/displaylayout.hxx"
#include "graphing/displayenable.hxx"

#include "displayhandler.hxx"
#include "gui.hxx"
#include "mainwindow.hxx"
#include "editdirective.hxx"
#include "dataaccess.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::GUI;
using namespace CPD3::Graphing;

DisplayHandler::DisplayHandler(const ValueSegment::Transfer &config,
                               std::string name,
                               std::unique_ptr<CPD3::Graphing::Display> &&setDisplay,
                               CPD3GUI *setGUI,
                               QWidget *parent) : DisplayWidget(std::move(setDisplay), parent),
                                                  gui(setGUI),
                                                  display(getDisplay()),
                                                  name(std::move(name)),
                                                  lastPlottedStart(FP::undefined()),
                                                  lastPlottedEnd(FP::undefined()),
                                                  repaintTimer(this),
                                                  baseInterval(gui->getSettings()
                                                                  ->value("display/repaintinterval",
                                                                          1000)
                                                                  .toInt()),
                                                  thresholdInterval((int) (baseInterval * 0.1))
{

    repaintTimer.setInterval(baseInterval);
    repaintTimer.setSingleShot(false);
    connect(&repaintTimer, SIGNAL(timeout()), this, SLOT(update()));
    repaintTimer.start();

    invisibleTimer.setInterval(baseInterval * 2);
    invisibleTimer.setSingleShot(false);
    connect(&invisibleTimer, SIGNAL(timeout()), this, SLOT(invisibleUpdate()));
    invisibleTimer.start();

    std::vector<std::reference_wrapper<const Variant::Root>> mergeConfig;
    for (const auto &it : config) {
        mergeConfig.emplace_back(it.root());
    }
    auto merged = Variant::Root::overlay(mergeConfig.begin(), mergeConfig.end());

    windowTitle = merged["WindowTitle"].toDisplayString();
    if (windowTitle.isEmpty())
        windowTitle = QString::fromStdString(name);

    sortPriority = merged["SortPriority"].toInt64();

    setContaminationRemoved(merged["RemoveContamination"].toBool());

    connect(this, SIGNAL(saveChanges(CPD3::Data::ValueSegment::Transfer)), this,
            SLOT(handleSave(CPD3::Data::ValueSegment::Transfer)));

    connect(display, &CPD3::Graphing::Display::outputsChanged, this,
            &DisplayHandler::resetLegendShow);
}

DisplayHandler::~DisplayHandler() = default;

QString DisplayHandler::getUserStateName() const
{
    QString result = QString::fromStdString(name);
    result.replace(QRegExp("\\s/"), "_");
    return result;
}

QSize DisplayHandler::sizeHint() const
{ return DisplayWidget::sizeHint().expandedTo(QSize(640, 480)); }

void DisplayHandler::paintEvent(QPaintEvent *event)
{
    ElapsedTimer timeTaken;
    timeTaken.start();

    gui->recalculateBounds();
    if (!FP::equal(gui->getStart(), lastPlottedStart) ||
            !FP::equal(gui->getEnd(), lastPlottedEnd)) {
        lastPlottedStart = gui->getStart();
        lastPlottedEnd = gui->getEnd();
        display->setVisibleTimeRange(lastPlottedStart, lastPlottedEnd);
    }

    DisplayWidget::paintEvent(event);

    int elapsed = (int) timeTaken.elapsed();
    if (elapsed > thresholdInterval) {
        int adjustedInterval = baseInterval + elapsed;
        if (abs(repaintTimer.interval() - adjustedInterval) > 10) {
            repaintTimer.setInterval(adjustedInterval);
            invisibleTimer.setInterval(adjustedInterval * 2);
        }
    } else {
        if (repaintTimer.interval() != baseInterval) {
            repaintTimer.setInterval(baseInterval);
            invisibleTimer.setInterval(baseInterval * 2);
        }
    }
    invisibleTimer.start();
}

void DisplayHandler::invisibleUpdate()
{
    ElapsedTimer timeTaken;
    timeTaken.start();

    gui->recalculateBounds();
    if (!FP::equal(gui->getStart(), lastPlottedStart) ||
            !FP::equal(gui->getEnd(), lastPlottedEnd)) {
        lastPlottedStart = gui->getStart();
        lastPlottedEnd = gui->getEnd();
        display->setVisibleTimeRange(lastPlottedStart, lastPlottedEnd);
    }


    int elapsed = (int) timeTaken.elapsed();
    if (elapsed > thresholdInterval * 2) {
        int adjustedInterval = baseInterval * 2 + elapsed;
        if (abs(invisibleTimer.interval() - adjustedInterval) > 10) {
            invisibleTimer.setInterval(adjustedInterval);
        }
    } else {
        if (invisibleTimer.interval() != baseInterval * 2) {
            invisibleTimer.setInterval(baseInterval * 2);
        }
    }
}

void DisplayHandler::keyPressEvent(QKeyEvent *event)
{
    DisplayWidget::keyPressEvent(event);
    gui->interactionDeferTrigger();
}

void DisplayHandler::keyReleaseEvent(QKeyEvent *event)
{
    DisplayWidget::keyReleaseEvent(event);
    gui->interactionDeferTrigger();
}

void DisplayHandler::enterEvent(QEvent *event)
{
    DisplayWidget::enterEvent(event);
    gui->interactionDeferTrigger();
}

void DisplayHandler::leaveEvent(QEvent *event)
{
    DisplayWidget::leaveEvent(event);
    gui->interactionDeferTrigger();
}

void DisplayHandler::mouseMoveEvent(QMouseEvent *event)
{
    DisplayWidget::mouseMoveEvent(event);
    gui->interactionDeferTrigger();
}

void DisplayHandler::mousePressEvent(QMouseEvent *event)
{
    DisplayWidget::mousePressEvent(event);
    gui->interactionDeferTrigger();
}

void DisplayHandler::mouseReleaseEvent(QMouseEvent *event)
{
    DisplayWidget::mouseReleaseEvent(event);
    gui->interactionDeferTrigger();
}

void DisplayHandler::injectGlobalZoom(DisplayZoomCommand &command)
{
    for (const auto &axis : display->zoomAxes()) {
        command.addCombining(axis);
    }
}

void DisplayHandler::injectResetZoom(DisplayZoomCommand &command)
{
    for (const auto &axis : display->zoomAxes()) {
        command.add(axis, FP::undefined(), FP::undefined());
    }
}

void DisplayHandler::applyDataTrim(double start, double end)
{
    display->trimData(start, end);
}

void DisplayHandler::resetLegendShow()
{
    gui->resetLegendShow(this);
}

void DisplayHandler::handleSave(CPD3::Data::ValueSegment::Transfer overlay)
{
    if (overlay.empty())
        return;
    {
        QMessageBox box(this);
        box.setText(tr("Display changes save requested."));
        box.setInformativeText(
                tr("Do you want to save your changes to the station configuration?"));
        box.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
        box.setDefaultButton(QMessageBox::Save);
        int sel = box.exec();
        switch (sel) {
        case QMessageBox::Save:
            break;
        default:
            return;
        }
    }

    struct Context {
        DisplayHandler *handler;
        std::shared_ptr<Archive::Access> access;

        CPD3::Data::ValueSegment::Transfer overlay;

        Context(DisplayHandler *handler, CPD3::Data::ValueSegment::Transfer &&overlay) : handler(
                handler), access(std::make_shared<Archive::Access>()), overlay(std::move(overlay))
        { }

        ~Context() = default;
    };

    auto context = std::make_shared<Context>(this, std::move(overlay));
    std::future<void> applyResult = std::async(std::launch::async, [context] {
        context->handler
               ->gui
               ->saveDisplayOverlay(*context->access, context->handler, context->overlay);
    });

    QProgressDialog waitDialog(tr("Saving Changes"), QString(), 0, 0, this);
    waitDialog.setWindowModality(Qt::ApplicationModal);
    waitDialog.setCancelButton(nullptr);

    Threading::pollFunctor(&waitDialog, [&applyResult, &waitDialog]() {
        if (applyResult.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
            return true;
        waitDialog.close();
        return false;
    });

    if (applyResult.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
        waitDialog.exec();

    applyResult.wait();
}


void CPD3GUI::removeNamedDisplay(const std::string &name)
{
    for (auto remove = displayHandlers.begin(); remove != displayHandlers.end();) {
        if ((*remove)->getName() != name) {
            ++remove;
            continue;
        }

        if (mainWindow)
            mainWindow->removeDisplayHandler(remove->get());
        remove->release()->deleteLater();
        remove = displayHandlers.erase(remove);
    }
}

void CPD3GUI::removeAllDisplays()
{
    if (mainWindow)
        mainWindow->removeAllDisplayHandlers();
    for (auto &handler : displayHandlers) {
        handler.release()->deleteLater();
    }
    displayHandlers.clear();
    activeDisplayConfiguration.clear();
}

void CPD3GUI::addSingleDisplay(const std::string &name, const ValueSegment::Transfer &config)
{
    DisplayDefaults defaults;
    defaults.setSettings(getSettings());
    if (!getStation().empty())
        defaults.setStation(getStation());
    if (!getArchive().empty())
        defaults.setArchive(getArchive());
    defaults.setArchiveRealtime("rt_boxcar");

    std::unique_ptr<CPD3::Graphing::DisplayLayout> display(new DisplayLayout(defaults, this));
    display->initialize(ValueSegment::withPath(config, "Layout"), defaults);

    DisplayHandler *handler = new DisplayHandler(config, name, std::move(display), this);

    DisplayDynamicContext dynamicContext(baseDisplayContext);
    dynamicContext.setInteractive(true);
    handler->setContext(dynamicContext);

    displayHandlers.emplace_back(handler);
    Q_ASSERT(mainWindow);
    mainWindow->addDisplayHandler(handler);

    connect(handler, SIGNAL(internalZoom(CPD3::Graphing::DisplayZoomCommand)), mainWindow.get(),
            SLOT(incomingZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(handler, SIGNAL(visibleZoom(CPD3::Graphing::DisplayZoomCommand)), mainWindow.get(),
            SLOT(incomingZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(handler, SIGNAL(globalZoom(CPD3::Graphing::DisplayZoomCommand)), mainWindow.get(),
            SLOT(incomingGlobalZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(handler, SIGNAL(timeRangeSelected(double, double)), this,
            SLOT(displayTimeRangeSelected(double, double)));
    connect(handler, SIGNAL(requestChainReload()), &replotPendingIssue, SLOT(start()));
}

void CPD3GUI::addNamedDisplay(const std::string &name, const ValueSegment::Transfer &config)
{
    ValueSegment::Transfer running;
    for (const auto &seg : config) {
        if (!running.empty() &&
                !DisplayLayout::mergable(running.back().value().hash("Layout"),
                                         seg.value().hash("Layout"))) {
            addSingleDisplay(name, running);
            running.clear();
        }
        running.emplace_back(seg);
    }
    if (!running.empty())
        addSingleDisplay(name, running);
}

namespace {
class GUIDisplayEnable : public DisplayEnable {
    DataAccess &access;
public:
    GUIDisplayEnable(DataAccess &access,
                     double start = FP::undefined(),
                     double end = FP::undefined(),
                     const SequenceName::Component &station = {},
                     const SequenceName::Component &archive = {},
                     const SequenceName::Component &variable = {}) : DisplayEnable(start, end,
                                                                                   station, archive,
                                                                                   variable),
                                                                     access(access)
    { }

    virtual ~GUIDisplayEnable() = default;

protected:
    CPD3::Data::SequenceName::Set evaluateSelection(const CPD3::Data::SequenceMatch::Composite &selection) override
    {
        auto result = DisplayEnable::evaluateSelection(selection);
        Util::merge(access.cachedMatches(selection), result);
        return result;
    }
};
}


CPD3GUI::DisplayConfigureContext::DisplayConfigureContext() : mutex(), terminated(false)
{ }

bool CPD3GUI::DisplayConfigureContext::isTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

void CPD3GUI::DisplayConfigureContext::terminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    feedback.disconnect();
}

void CPD3GUI::createDisplays()
{
    if (auto ctx = displayConfigureContext.lock()) {
        ctx->terminate();
        ctx->feedback.disconnect();
        displayConfigureContext.reset();
    }

    interactionDeferTrigger();

    auto context = std::make_shared<DisplayConfigureContext>();
    displayConfigureContext = context;

    if (mainWindow) {
        mainWindow->feedbackAttach(context->feedback);
        mainWindow->start();
    }

    auto displayEnable = std::make_shared<GUIDisplayEnable>(dataAccess, getStart(),
                                                            isRealtime() ? FP::undefined()
                                                                         : getEnd(), getStation(),
                                                            getArchive());

    struct TestResult {
        std::string key;
        bool passed;
        ValueSegment segment;
    };
    auto evaluate = std::make_shared<std::vector<std::future<TestResult>>>();

    {
        auto basicConfig = Range::findIntersecting(configuration, getStart());
        if (basicConfig != configuration.cend()) {
            ValueSegment::Transfer::const_iterator endBasicConfig;
            if (FP::defined(getEnd()) && !isRealtime()) {
                endBasicConfig = Range::upperBound(configuration, getEnd());
            } else {
                endBasicConfig = configuration.cend();
            }
            for (; basicConfig != endBasicConfig; ++basicConfig) {
                for (auto child : basicConfig->value().hash("Displays").toHash()) {
                    if (!child.second.hash("Layout").exists())
                        continue;

                    TestResult result;
                    result.key = child.first;
                    result.passed = false;
                    result.segment = ValueSegment(basicConfig->getStart(), basicConfig->getEnd(),
                                                  Variant::Root(child.second));

                    evaluate->emplace_back(
                            std::async(std::launch::deferred, [result, displayEnable] {
                                TestResult r = result;
                                if (!displayEnable->enabled(
                                        result.segment.getValue().hash("Enable")))
                                    return r;

                                r.passed = true;
                                return r;
                            }));
                }
            }
        }
    }

    Memory::release_unused();

    Threading::pollFuture(this, std::async(std::launch::async, [=]() {
        std::unordered_map<std::string, ValueSegment::Transfer> displayConfiguration;

        context->feedback
               .emitStage(tr("Configuring displays"),
                          tr("Data are being read from the archive to determine what displays should be displayed."),
                          true);

        size_t total = evaluate->size();
        if (total != 0) {
            std::size_t n = 0;

            for (auto &e : *evaluate) {
                if (context->isTerminated())
                    return std::unordered_map<std::string, ValueSegment::Transfer>();

                auto result = e.get();
                if (result.passed) {
                    displayConfiguration[result.key].push_back(std::move(result.segment));
                }

                ++n;
                context->feedback
                       .emitProgressFraction(static_cast<double>(n) / static_cast<double>(total));
            }
        }

        context->feedback.emitProgressFraction(1.0);

        return displayConfiguration;
    }), [this, context](std::unordered_map<std::string,
                                           ValueSegment::Transfer> displayConfiguration) {
        if (context->isTerminated())
            return;
        context->terminate();

        for (const auto &displayData : displayConfiguration) {
            const auto &active = activeDisplayConfiguration[displayData.first];
            const auto &incoming = displayData.second;
            if (active.size() == incoming.size()) {
                if (std::equal(active.cbegin(), active.cend(), incoming.cbegin(),
                               [](const ValueSegment &a, const ValueSegment &b) {
                                   return FP::equal(a.getStart(), b.getStart()) &&
                                           FP::equal(a.getEnd(), b.getEnd()) &&
                                           a.getValue() == b.getValue();
                               }))
                    continue;
            }
            removeNamedDisplay(displayData.first);
            if (displayData.second.empty())
                continue;
            addNamedDisplay(displayData.first, displayData.second);
        }

        for (const auto &oldDisplay : activeDisplayConfiguration) {
            auto check = displayConfiguration.find(oldDisplay.first);
            if (check == displayConfiguration.end()) {
                removeNamedDisplay(oldDisplay.first);
                continue;
            }
            if (check->second.empty()) {
                removeNamedDisplay(oldDisplay.first);
                continue;
            }
        }

        activeDisplayConfiguration = std::move(displayConfiguration);
        Memory::release_unused();

        if (displayHandlers.empty()) {
            if (mainWindow) {
                mainWindow->finished();
            }
            return;
        }

        Q_ASSERT(!activeChain);
        activeChain.reset(new DisplayWidgetTapChain);

        {
            Archive::Selection::Match stations;
            if (!getStation().empty())
                stations.emplace_back(getStation());

            Archive::Selection::Match archives;
            if (!getArchive().empty())
                archives.emplace_back(getArchive());
            if (isRealtime() && archives.empty()) {
                archives.emplace_back("rt_boxcar");
                archives.emplace_back("rt_instant");
            }

            activeChain->setDefaultSelection(getStart(), isRealtime() ? FP::undefined() : getEnd(),
                                             stations, archives);

            activeChain->setBackend(dataAccess.getArchiveBackend());

            QObject::connect(activeChain.get(), SIGNAL(finished()), this, SLOT(reapActiveChain()),
                             Qt::QueuedConnection);
        }

        for (auto &handler : displayHandlers) {
            handler->registerChain(activeChain.get());

            QObject::connect(activeChain.get(), SIGNAL(finished()), handler.get(), SLOT(update()),
                             Qt::QueuedConnection);
        }

        if (mainWindow) {
            /* Don't start the progress if we're in realtime mode, since we don't have
             * a reliable way of figuring out when it's done */
            if (!isRealtime()) {
                mainWindow->start();
                mainWindow->feedbackAttach(activeChain->feedback);
            } else {
                mainWindow->finished();
            }
        }

        activeChain->start();
    });
}

void CPD3GUI::saveDisplayOverlay(Archive::Access &access,
                                 DisplayHandler *handler,
                                 const CPD3::Data::ValueSegment::Transfer &overlay)
{
    if (overlay.empty())
        return;
    auto name = handler->getName();
    if (name.empty())
        return;

    static constexpr int savePriority = 1000000;

    Archive::Selection selection
            (overlay.front().getStart(), overlay.back().getEnd(), {station}, {"configuration"},
             {"displays"});
    selection.includeMetaArchive = false;
    selection.includeDefaultStation = false;

    SequenceName overlayName(station, "configuration", "displays");

    double clipStart = FP::undefined();
    double clipEnd = FP::undefined();
    {
        auto check = activeDisplayConfiguration.find(name);
        if (check != activeDisplayConfiguration.end() && !check->second.empty()) {
            clipStart = check->second.front().getStart();
            clipEnd = check->second.back().getEnd();
        }
    }

    for (;;) {
        Archive::Access::WriteLock lock(access);

        ValueSegment::Stream reader;
        std::unordered_set<SequenceIdentity> toRemove;
        {
            StreamSink::Iterator incoming;
            access.readStream(selection, &incoming)->detach();
            for (;;) {
                auto add = incoming.all();
                if (add.empty())
                    break;
                for (auto &v : add) {
                    if (v.getPriority() != savePriority)
                        continue;
                    if (v.getStation() != station)
                        continue;
                    if (v.getArchive() != "configuration")
                        continue;
                    if (v.getVariable() != "displays")
                        continue;
                    toRemove.insert(v.getIdentity());
                    reader.overlay(std::move(v));
                }
            }
        }

        SequenceValue::Transfer toModify;
        for (const auto &v : overlay) {
            Variant::Root root;
            root.write()
                .hash("Profiles")
                .hash(profile)
                .hash(mode)
                .hash("Displays")
                .hash(name)
                .hash("Layout")
                .set(v.value());
            SequenceValue add
                    (SequenceIdentity(overlayName, v.getStart(), v.getEnd(), savePriority + 1),
                     std::move(root));

            if (Range::compareStart(add.getStart(), clipStart) < 0)
                add.setStart(clipStart);
            if (Range::compareEnd(add.getEnd(), clipEnd) > 0)
                add.setEnd(clipEnd);
            if (Range::compareStartEnd(add.getStart(), add.getEnd()) >= 0)
                continue;

            for (auto &seg : reader.add(add)) {
                SequenceValue mod
                        (SequenceIdentity(overlayName, seg.getStart(), seg.getEnd(), savePriority),
                         std::move(seg.root()));
                toRemove.erase(mod.getIdentity());
                toModify.emplace_back(std::move(mod));
            }
        }
        for (auto &seg : reader.finish()) {
            SequenceValue mod
                    (SequenceIdentity(overlayName, seg.getStart(), seg.getEnd(), savePriority),
                     std::move(seg.root()));
            toRemove.erase(mod.getIdentity());
            toModify.emplace_back(std::move(mod));
        }

        SequenceIdentity::Transfer remainingRemove;
        Util::append(toRemove, remainingRemove);

        access.writeSynchronous(toModify, remainingRemove);

        if (lock.commit())
            break;
    }
}

void CPD3GUI::recalculateBounds()
{
    if (!isRealtime())
        return;
    displayBounds.end = Time::time();
    if (realtimeWindowAlign) {
        displayBounds.end = Time::floor(displayBounds.end, realtimeWindowUnit, realtimeWindowCount);
    }
    displayBounds.start = Time::logical(displayBounds.end, realtimeWindowUnit, realtimeWindowCount,
                                        realtimeWindowAlign, false, -1);

    if (mainWindow) {
        mainWindow->updateBoundsIfInRealtime(getStart(), getEnd());
    }
}

void CPD3GUI::applyReplot()
{
    trimDataTimer.stop();
    realtimeDisplayReloadTimer.stop();
    realtimeForceReplotTimer.stop();

    if (isRealtime()) {
        boundsUpdateTimer.setInterval(
                getSettings()->value("display/repaintinterval", 1000).toInt());
        boundsUpdateTimer.start();

        recalculateBounds();

        if (mainWindow) {
            mainWindow->setRealtime(realtimeWindowUnit, realtimeWindowCount, realtimeWindowAlign);
        }
    } else {
        boundsUpdateTimer.stop();

        if (mainWindow) {
            mainWindow->setFixedTime(getStart(), getEnd());
        }
    }

    dataAccess.stop();

    if (activeChain) {
        if (!activeChain->isFinished()) {
            activeChain->signalTerminate();
            for (;;) {
                Q_ASSERT(activeChain);
                QEventLoop loop;
                QObject::connect(activeChain.get(), &QThread::finished, &loop, &QEventLoop::quit);
                if (activeChain->isFinished())
                    break;
                loop.exec();
                if (!activeChain)
                    break;
            }
        }
        if (activeChain) {
            activeChain->wait();
            activeChain.reset();
        }
    }

    {
        QEventLoop loop;
        loop.processEvents();
    }

    dataAccess.restart();

    updateViewMenuVisiblity();

    Memory::release_unused();
    createDisplays();
}

void CPD3GUI::reapActiveChain()
{
    if (!activeChain)
        return;
    if (!activeChain->isFinished())
        return;
    activeChain->wait();
    activeChain.reset();

    mainWindow->finished();
}

void CPD3GUI::queueFixedReplot(double start, double end)
{
    boundsUpdateTimer.stop();
    displayBounds.start = start;
    displayBounds.end = end;
    inRealtimePlotMode = false;
    replotPendingIssue.start();
}

void CPD3GUI::queueRealtimeReplot(Time::LogicalTimeUnit unit, int count, bool align)
{
    boundsUpdateTimer.stop();
    realtimeWindowUnit = unit;
    realtimeWindowCount = count;
    realtimeWindowAlign = align;
    inRealtimePlotMode = true;
    replotPendingIssue.start();
}

void CPD3GUI::injectGlobalZoom(DisplayZoomCommand &command)
{
    for (const auto &handler : displayHandlers) {
        handler->injectGlobalZoom(command);
    }
}

void CPD3GUI::injectResetZoom(DisplayZoomCommand &command)
{
    for (const auto &handler : displayHandlers) {
        handler->injectResetZoom(command);
    }
}

void CPD3GUI::displayTimeRangeSelected(double start, double end)
{
    if (editDirectiveDialog) {
        if (editDirectiveDialog->setSelectedTimes(start, end))
            return;
    }
    if (mainWindow)
        mainWindow->setFixedTime(start, end);
}

void CPD3GUI::interactionDeferTrigger()
{
    trimDataTimer.start();
    realtimeDisplayReloadTimer.start();
}

void CPD3GUI::applyRealtimeReload()
{
    if (!mainWindow)
        return;
    if (!activeChain)
        return;
    if (!isRealtime())
        return;

    applyReplot();
}
