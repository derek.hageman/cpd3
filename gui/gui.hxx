/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef GUI_H
#define GUI_H

#include "core/first.hxx"

#include <memory>
#include <mutex>
#include <unordered_map>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QMainWindow>
#include <QProgressBar>
#include <QLabel>
#include <QSettings>
#include <QMenuBar>
#include <QAction>
#include <QMenu>
#include <QShortcut>
#include <QActionGroup>
#include <QTimer>

#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "datacore/processingtapchain.hxx"
#include "datacore/archive/access.hxx"
#include "core/timeutils.hxx"
#include "core/actioncomponent.hxx"
#include "core/threading.hxx"
#include "acquisition/acquisitionnetwork.hxx"
#include "acquisition/commandmanager.hxx"
#include "guidata/realtimemessagelog.hxx"
#include "graphing/display.hxx"
#include "graphing/displaywidget.hxx"
#include "dataaccess.hxx"


class GUIMainWindow;

class InterfaceHandler;

class GenericHandler;

class DisplayHandler;

class EditDirectiveDisplay;

class EventLogDisplay;

class GUIChainDialog;

class ConfigurationEditorDialog;

class CPD3GUI : public QObject {
Q_OBJECT

public:
    enum InvokedMode {
        Invoked_Standard, Invoked_Realtime, Invoked_Editing,
    };
private:

    std::unique_ptr<QSettings> settings;

    InvokedMode invokedMode;
    QString profile;
    QString mode;
    CPD3::Data::SequenceName::Component station;
    CPD3::Data::SequenceName::Component archive;

    CPD3::Time::Bounds displayBounds;
    CPD3::Time::Bounds initialBounds;
    CPD3::Time::LogicalTimeUnit realtimeWindowUnit;
    int realtimeWindowCount;
    bool realtimeWindowAlign;
    bool inRealtimePlotMode;
    QTimer boundsUpdateTimer;
    QTimer replotPendingIssue;
    QTimer trimDataTimer;
    QTimer realtimeDisplayReloadTimer;
    QTimer realtimeForceReplotTimer;

    CPD3::Graphing::DisplayDynamicContext baseDisplayContext;

    bool doRestoreState;

    DataAccess dataAccess;
    std::unique_ptr<CPD3::Graphing::DisplayWidgetTapChain> activeChain;

    CPD3::Data::ValueSegment::Transfer baseConfiguration;
    CPD3::Data::ValueSegment::Transfer configuration;
    std::unique_ptr<CPD3::Acquisition::AcquisitionNetworkClient> acquisitionConnection;
    bool haveAcquisitionConnection;
    std::unique_ptr<CPD3::Acquisition::AcquisitionTrayCommandHandler> acquisitionCommandHandler;

    CPD3::Acquisition::CommandManager commandManager;
    std::unordered_map<QString, std::unique_ptr<InterfaceHandler>> acquisitionInterfaces;
    CPD3::Data::Variant::Read acquisitionGenericConfiguration;
    std::vector<std::unique_ptr<GenericHandler>> acquisitionGeneric;

    class AcquisitionRealtimeIngress : public CPD3::Data::StreamSink {
        CPD3GUI &gui;
    public:
        AcquisitionRealtimeIngress(CPD3GUI &gui);

        virtual ~AcquisitionRealtimeIngress();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        void endData() override;
    };

    friend class AcquisitionRealtimeIngress;

    AcquisitionRealtimeIngress acquisitionRealtimeIngress;
    std::mutex acquisitionRealtimeMutex;
    CPD3::Data::SequenceValue::Transfer acquisitionRealtimePending;

    class DisplayConfigureContext {
        std::mutex mutex;
        bool terminated;
    public:
        DisplayConfigureContext();

        bool isTerminated();

        void terminate();

        CPD3::ActionFeedback::Source feedback;
    };

    std::weak_ptr<DisplayConfigureContext> displayConfigureContext;

    std::unordered_map<std::string, CPD3::Data::ValueSegment::Transfer> activeDisplayConfiguration;
    std::vector<std::unique_ptr<DisplayHandler>> displayHandlers;

    std::unique_ptr<GUIMainWindow> mainWindow;
    std::unique_ptr<CPD3::GUI::Data::RealtimeMessageLog> messageLogDialog;
    std::unique_ptr<EditDirectiveDisplay> editDirectiveDialog;
    std::unique_ptr<EventLogDisplay> eventLogDialog;
    std::unique_ptr<GUIChainDialog> viewDataDialog;
    std::unique_ptr<GUIChainDialog> exportDataDialog;
    std::unique_ptr<ConfigurationEditorDialog> configurationEditorDialog;

    std::unique_ptr<QMenuBar> mainMenu;

    QMenu *fileMenu;
    QAction *showMentorEditsAction;
    QAction *enterFullScreenAction;
    QAction *fileMenuSeparator1;
    QAction *showViewDataAction;
    QAction *showViewEventLogAction;
    QAction *fileMenuSeparator2;
    QAction *saveDisplayAction;
    QAction *printDisplayAction;
    QAction *exportDataAction;
    QAction *fileMenuSeparator3;

    QMenu *editMenu;
    QAction *showPreferencesAction;
    QAction *showDisplaySettingsAction;
    QAction *showAcquisitionConfigurationAction;
    QAction *showSystemConfigurationAction;

    QMenu *displayMenu;
    QActionGroup *displayMenuGroup;

    QMenu *actionMenu;

    QMenu *viewMenu;
    QAction *viewBack;
    QAction *viewNext;
    QAction *viewPrevious;
    QAction *viewResetAll;
    QAction *viewInitial;
    QAction *viewReplot;
    QAction *viewLegendToggle;
    QAction *viewMenuSeparator1;
    QList<QAction *> viewSelection;

    QMenu *acquisitionMenu;
    QList<QAction *> acquisitionMenuTopLevel;
    QAction *messageLogEntryAction;
    QAction *systemStatusMenuAction;
    QAction *setContaminationAction;
    QAction *clearContaminationAction;
    QAction *overrideContaminationAction;
    QAction *setBypassAction;
    QAction *clearBypassAction;
    QAction *overrideBypassAction;
    QAction *setAveragingAction;
    QAction *showAutoprobeStatusAction;
    QAction *restartSystemAction;

    enum AcquisitionInterfaceMenuReservedEntry {
        AcquisitionInterface_ShowWindow = 0,
    };
    enum {
        AcquisitionInterface_ReservedTotal = 1
    };
    QHash<QString, QList<QAction *> > acquisitionMenuInterfaceActions;

    static inline QAction *getAcquisitionInterfaceReserved(const QList<QAction *> &a,
                                                           AcquisitionInterfaceMenuReservedEntry re)
    {
        int idx = (int) re;
        if (idx < 0)
            idx = a.size() + idx;
        return a.at(idx);
    }

    enum AcquisitionGenericMenuReservedEntry {
        AcquisitionGeneric_ShowWindow = 0,
    };
    enum {
        AcquisitionGeneric_ReservedTotal = 1
    };
    QList<QList<QAction *> > acquisitionMenuGenericActions;

    static inline QAction *getAcquisitionGenericReserved(const QList<QAction *> &a,
                                                         AcquisitionGenericMenuReservedEntry re)
    {
        int idx = (int) re;
        if (idx < 0)
            idx = a.size() + idx;
        return a.at(idx);
    }

    QMenu *helpMenu;
    QAction *showHelpAction;
    QAction *showWebsiteAction;
    QAction *showBugReportAction;
    QAction *showDebugAction;
    QAction *helpMenuSeparator1;
    QAction *showAboutAction;

    QShortcut *shortcutBack;
    QShortcut *shortcutNext;
    QShortcut *shortcutPrevious;
    QShortcut *shortcutResetAll;
    QShortcut *shortcutReplot;
    QShortcut *shortcutLegendToggle;


    void determineInvokedMode(QString &programName, QStringList &arguments);

    void determineStation(CPD3::Data::Archive::Access &access, QStringList &arguments);

    void handleProfileAlias(CPD3::Data::Archive::Access &access);

    void determineMode(CPD3::Data::Archive::Access &access, QStringList &arguments);

    CPD3::Data::Variant::Read getRealtimeGenericConfiguration();

    void updateRealtime();

    void initializeConfiguration();

    void loadConfiguration();

    void updateDisplayMenu();

    void updateActionMenu();

    void updateMenuVisiblity();

    void acquisitionRealtimeUpdated();

    void populateInterfaceMenu(InterfaceHandler *interface,
                               QMenu *target,
                               QList<QAction *> &existing);

    void populateGenericMenu(GenericHandler *generic, QMenu *target, QList<QAction *> &existing);

    void reloadConfiguration(bool reread = false);


    void removeNamedDisplay(const std::string &name);

    void removeAllDisplays();

    void addSingleDisplay(const std::string &name,
                          const CPD3::Data::ValueSegment::Transfer &config);

    void addNamedDisplay(const std::string &name, const CPD3::Data::ValueSegment::Transfer &config);

    void createDisplays();

    void changeDisplayMode(const QString &selectedMode);

    void acquisitionTrayCommand(CPD3::Acquisition::AcquisitionTrayCommandHandler::Command command);

    void acquisitionInformationUpdated(const std::string &name);

    void acquisitionStateUpdated(const std::string &name);

    void acquisitionConnectionState(bool connected);

    void acquisitionRealtimeEvent(const CPD3::Data::Variant::Root &event);

public:
    /**
     * Create the base GUI holder.
     */
    CPD3GUI();

    ~CPD3GUI();

    /**
     * Parse command line arguments.
     * 
     * @param arguments the arguments
     * @return          zero if startup should continue, -1 on error or 1 on success but exit
     */
    int parseArguments(QStringList arguments);

    /**
     * Start the GUI.
     */
    void startup();

    /**
     * Return the effective station.
     * 
     * @return the station
     */
    inline const CPD3::Data::SequenceName::Component &getStation() const
    { return station; }

    /**
     * Return the effective default archive
     * 
     * @return the archive
     */
    inline const CPD3::Data::SequenceName::Component &getArchive() const
    { return archive; }

    /**
     * Get the current display start.
     * 
     * @return the start time
     */
    inline double getStart() const
    { return displayBounds.getStart(); }

    /**
     * Get the current display end.
     * 
     * @return the end time
     */
    inline double getEnd() const
    { return displayBounds.getEnd(); }

    /**
     * Test if there are remote data possible (there is an acquisition connection)
     *
     * @return  true if remote data are possible
     */
    inline bool haveRemoteData() const
    { return acquisitionConnection.get() != nullptr; }

    /**
     * Test if the mode is currently realtime (ongoing data).
     * 
     * @return true if data is ongoing
     */
    inline bool isRealtime() const
    { return inRealtimePlotMode; }

    /**
     * Returns true if state should be restored.
     * 
     * @return true to restore state
     */
    inline bool restoreState() const
    { return doRestoreState; }

    /**
     * Get the application settings object.
     * 
     * @return the settings object
     */
    inline QSettings *getSettings() const
    { return settings.get(); }

    /**
     * Get the command manager reference.
     *
     * @return the command manager
     */
    inline CPD3::Acquisition::CommandManager &getCommandManager()
    { return commandManager; }

    /**
     * Get the base path to store state information in the settings
     * object.
     * 
     * @param id    the ID name
     * @return      the base path
     */
    QString statePath(const QString &id) const;

    /**
     * Queue a replot of all data to the given bounds.  This will be executed
     * at the next event service, and will override any previously queued
     * replots.  This allows for the undo stack to (potentially) issue
     * multiple replots as part of a large jump without forcing multiple
     * chain creation.
     * 
     * @param start     the start time
     * @param end       the end time
     */
    void queueFixedReplot(double start, double end);

    /**
     * Queue a replot of all data to given realtime internval.  This will be 
     * executed at the next event service, and will override any previously 
     * queued replots.  This allows for the undo stack to (potentially) issue
     * multiple replots as part of a large jump without forcing multiple
     * chain creation.
     * 
     * @param unit      the realtime window unit
     * @param count     the realtime unit count
     * @param align     set if aligning
     */
    void queueRealtimeReplot(CPD3::Time::LogicalTimeUnit unit, int count, bool align);

    /**
     * Inject all displays into the given global zoom command.  The result
     * will change the command so it applies to all displays.
     * 
     * @param command   the command to add to
     */
    void injectGlobalZoom(CPD3::Graphing::DisplayZoomCommand &command);

    /**
     * Inject a zoom reset into all displays.  This is usually used to
     * generate a sub-command for a replot action.
     * 
     * @param command   the command to add to
     */
    void injectResetZoom(CPD3::Graphing::DisplayZoomCommand &command);

    /**
     * Reset the legend show state for a display.
     * 
     * @param handler   the handler
     */
    void resetLegendShow(DisplayHandler *handler);

    /**
     * Test if the main window should close (i.e. prompt for any save changes, etc).
     *
     * @return true if the main window shold close
     */
    bool allowMainWindowClose();

    /**
     * Start a remote read of data.
     *
     * @param sink      the target sink
     * @param selection the selections
     */
    void startRemoteRead(CPD3::Data::StreamSink *sink,
                         const CPD3::Data::Archive::Selection::List &selection);

    /**
     * Resend all realtime values.
     */
    void resendRealtime();

    /**
     * Save the given overlay of a display handler.
     *
     * @param access    the archive access to save with
     * @param handler   the handler to save
     * @param overlay   the overlay on the handler
     */
    void saveDisplayOverlay(CPD3::Data::Archive::Access &access,
                            DisplayHandler *handler,
                            const CPD3::Data::ValueSegment::Transfer &overlay);

public slots:

    /**
     * Recalculate the bounds if needed.  This does nothing for fixed time
     * data but will update realtime bounds to end at the current time.
     */
    void recalculateBounds();

    /**
     * Inform the GUI that a time range has been selected by a display.
     * This is used to forward the selection to the appropriate place (usually
     * just the main window to update the bounds).
     */
    void displayTimeRangeSelected(double start, double end);

    /**
     * Defer the next data trim and realtime reloads.  This is usually used as 
     * a response to user interaction so that we don't discard data and 
     * allow for quick zooming and to prevent reloading while the user
     * is still interacting with the display.
     */
    void interactionDeferTrigger();

    /**
     * Write out any global state for the GUI.
     */
    void writeOutState();

signals:

    void quitRequested();

    void serviceAcquisitionRealtime();

private slots:

    void updateViewMenu();

    void updateViewMenuVisiblity();

    void updateAcquisitionMenu();

    void updateAcquisitionMenuVisibility();

    void updateAcquisitionSystemStatus();

    void createGenericDisplays();

    void processAcquisitionRealtime();

    void showMentorEdits();

    void enterFullScreen();

    void showViewData();

    void showViewEventLog();

    void saveDisplay();

    void printDisplay();

    void exportData();

    void showPreferences();

    void showDisplaySettings();

    void showAcquisitionConfiguration();

    void showSystemConfiguration();

    void setContamination();

    void clearContamination();

    void overrideContamination();

    void setBypass();

    void clearBypass();

    void overrideBypass();

    void setAveragingTime();

    void showMessageLogEntry();

    void showAcquisitionRestart();

    void showAcquisitionAutoprobeStatus();

    void showDisplayWindow();

    void showInterfaceWindow();

    void showGenericWindow();

    void issueAcquisitionCommand();

    void showHelp();

    void showWebsite();

    void showBugReport();

    void showDebug();

    void showAbout();

    void performAction(QAction *act);

    void displayModeSelected(QAction *act);

    void replotInitial();

    void applyReplot();

    void applyDataTrim();

    void applyRealtimeReload();

    void reapActiveChain();
};


Q_DECLARE_METATYPE(QMenu *);

#endif
