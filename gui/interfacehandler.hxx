/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef INTERFACEHANDER_H
#define INTERFACEHANDER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QWidget>

#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"
#include "acquisition/realtimelayout.hxx"
#include "acquisition/acquisitioncomponent.hxx"

#include "realtimedisplay.hxx"
#include "handlercommon.hxx"

class CPD3GUI;

class InterfaceHandler : public HandlerCommon {
Q_OBJECT

    CPD3GUI *parent;
    QString name;
    CPD3::Data::Variant::Read information;
    CPD3::Data::Variant::Flags systemFlags;
    CPD3::Data::Variant::Flags bypassFlags;
    CPD3::Acquisition::AcquisitionInterface::GeneralStatus status;
public:
    /**
     * Create the interface handler.
     * 
     * @param name      the name of the interface (not the display name, the acquisition name, e.x. "A11")
     * @param parent    the parent GUI
     */
    InterfaceHandler(const QString &name, CPD3GUI *parent);

    /**
     * Update the interface information.
     * 
     * @param info the updated information
     */
    void updateInformation(const CPD3::Data::Variant::Read &info);

    /**
     * Update the interface state.
     * 
     * @param state the updated state
     */
    void updateState(const CPD3::Data::Variant::Read &state);

    /**
     * Get the interface name.
     * 
     * @return the acquisition interface name
     */
    inline QString getName() const
    { return name; }

    /**
     * Get the interface menu text.
     * 
     * @return  the menu text
     */
    inline QString getMenuText() const
    { return information.hash("MenuEntry").toDisplayString(); }

    /**
     * Get the interface menu hotkey.
     * 
     * @return the hotkey
     */
    inline QString getMenuHotkey() const
    { return information.hash("MenuCharacter").toDisplayString(); }

    /**
     * Get the display window title.
     * 
     * @return the window title
     */
    inline QString getWindowTitle() const
    { return information.hash("WindowTitle").toDisplayString(); }

    /**
     * Get the system flags for the interface.
     * 
     * @return the system flags
     */
    inline const CPD3::Data::Variant::Flags &getSystemFlags() const
    { return systemFlags; }

    /**
     * Get the bypass flags for the interface.
     * 
     * @return the bypass flags
     */
    inline const CPD3::Data::Variant::Flags &getBypassFlags() const
    { return bypassFlags; }

    /**
     * Get the general status of the interface.
     * 
     * @return the status
     */
    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getStatus() const
    { return status; }

    /**
     * Test if the interface is hidden in the acquisition menu.
     * 
     * @return  true if the interface is hidden
     */
    bool hideInAcquisitionMenu() const;

    /**
     * Process incoming realtime values.
     * 
     * @param data  the incoming data
     */
    void incomingRealtime(const CPD3::Data::SequenceValue::Transfer &data);

    /**
     * Get the display sort priority.
     * 
     * @return the display sort priority
     */
    qint64 getDisplaySortPriority() const;

protected:
    virtual QString criticalText() const;
};

Q_DECLARE_METATYPE(InterfaceHandler *);

#endif
