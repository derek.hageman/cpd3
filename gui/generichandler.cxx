/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTimer>

#include "core/number.hxx"

#include "generichandler.hxx"
#include "gui.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

GenericHandler::InputSelection GenericHandler::generateInputSelection(const SequenceName::Component &defaultArchive,
                                                                      const SequenceName::Component &defaultVariable,
                                                                      const Variant::Read &config)
{
    InputSelection result;

    result.selection =
            SequenceMatch::OrderedLookup(config, {}, {QString::fromStdString(defaultArchive)},
                                         {QString::fromStdString(defaultVariable)});
    result.selection.registerExpected(parent->getStation(), defaultArchive, defaultVariable);
    result.selection.registerExpected(parent->getStation(), defaultArchive);
    result.selection.registerExpected(parent->getStation());
    result.selection.registerExpected();
    result.metadata.write().set(config.hash("Metadata"));

    return result;
}

void GenericHandler::generateInputSelections(std::vector<InputSelection> &target,
                                             const SequenceName::Component &defaultArchive,
                                             const Variant::Read &config)
{
    auto children = config.toChildren();
    for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
        target.emplace_back(generateInputSelection(defaultArchive, add.stringKey(), add.value()));
    }
}

void GenericHandler::registerAllExisting(QSet<SequenceName> &seen,
                                         std::vector<InputSelection> &selections,
                                         RealtimeLayout &layout)
{
    for (auto &check : selections) {
        for (const auto &unit : check.selection.knownInputs()) {
            if (seen.contains(unit))
                continue;
            seen.insert(unit);
            if (unit.isMeta())
                continue;

            layout.incomingData(SequenceValue(SequenceIdentity(unit.toMeta()), check.metadata));
        }
    }
}

GenericHandler::GenericHandler(const Variant::Read &config, int uid, CPD3GUI *p) : HandlerCommon(p),
                                                                                   parent(p),
                                                                                   id(uid),
                                                                                   configuration(
                                                                                           config),
                                                                                   menuText(
                                                                                           configuration
                                                                                                   .read()
                                                                                                   .hash("MenuEntry")
                                                                                                   .toDisplayString()),
                                                                                   windowTitle(
                                                                                           configuration
                                                                                                   .read()
                                                                                                   .hash("WindowTitle")
                                                                                                   .toDisplayString()),
                                                                                   displaySortPriority(
                                                                                           configuration
                                                                                                   .read()
                                                                                                   .hash("SortPriority")
                                                                                                   .toInt64()),
                                                                                   showOnUpdate(
                                                                                           configuration
                                                                                                   .read()
                                                                                                   .hash("ShowOnUpdate")),
                                                                                   showOnUpdateRequire(
                                                                                           configuration
                                                                                                   .read()
                                                                                                   .hash("ShowOnUpdate")
                                                                                                   .hash("Require")),
                                                                                   showOnUpdateExclude(
                                                                                           configuration
                                                                                                   .read()
                                                                                                   .hash("ShowOnUpdate")
                                                                                                   .hash("Exclude")),
                                                                                   inputSelectionsInstant(),
                                                                                   inputSelectionsBoxcar(),
                                                                                   inputSelectionsAverage(),
                                                                                   seenInstant(),
                                                                                   seenBoxcar(),
                                                                                   seenAverage()
{

    generateInputSelections(inputSelectionsInstant, "rt_instant",
                            configuration.read().hash("Window").hash("Instant"));
    generateInputSelections(inputSelectionsBoxcar, "rt_boxcar",
                            configuration.read().hash("Window").hash("Boxcar"));
    generateInputSelections(inputSelectionsAverage, "raw",
                            configuration.read().hash("Window").hash("Average"));

    registerAllExisting(seenInstant, inputSelectionsInstant, layoutInstant);
    registerAllExisting(seenBoxcar, inputSelectionsBoxcar, layoutBoxcar);
    registerAllExisting(seenAverage, inputSelectionsAverage, layoutAverage);

    parent->getCommandManager().updateManual(getName(), configuration.read().hash("Commands"));
}

bool GenericHandler::hideInAcquisitionMenu() const
{ return configuration.read().hash("MenuHide").toBool(); }

QString GenericHandler::getName() const
{ return QString("__GENERIC_%1").arg(id); }

qint64 GenericHandler::getDisplaySortPriority() const
{ return displaySortPriority; }

QString GenericHandler::getUserStateName() const
{
    QString name(windowTitle);
    name.replace(QRegExp("\\s/"), "_");
    return name;
}

void GenericHandler::handleSelectionUnit(const SequenceName &unit,
                                         QSet<SequenceName> &seen,
                                         std::vector<InputSelection> &selections,
                                         RealtimeLayout &layout)
{
    if (seen.contains(unit))
        return;
    seen.insert(unit);
    if (unit.isMeta())
        return;

    for (auto &check : selections) {
        if (!check.selection.registerInput(unit))
            continue;

        layout.incomingData(SequenceValue(SequenceIdentity(unit.toMeta()), check.metadata));
        break;
    }
}

static bool showValueCheck(const Variant::Root &input, const Variant::Root &check)
{ return Variant::Root::overlay(input, check).read() == input.read(); }

static bool shouldShow(const Variant::Root &input,
                       const Variant::Root &require,
                       const Variant::Root &exclude)
{
    if (require.read().exists() && !showValueCheck(input, require))
        return false;
    if (exclude.read().exists() && showValueCheck(input, exclude))
        return false;
    return true;
}

void GenericHandler::incomingRealtime(const SequenceValue::Transfer &data)
{
    for (const auto &value : data) {
        handleSelectionUnit(value.getUnit(), seenInstant, inputSelectionsInstant, layoutInstant);
        handleSelectionUnit(value.getUnit(), seenBoxcar, inputSelectionsBoxcar, layoutBoxcar);
        handleSelectionUnit(value.getUnit(), seenAverage, inputSelectionsAverage, layoutAverage);

        if (!value.getUnit().isMeta()) {
            layoutInstant.incomingData(value);
            layoutAverage.incomingData(value);
            layoutBoxcar.incomingData(value);
        }

        if (showOnUpdate.matches(value.getUnit()) &&
                shouldShow(value.root(), showOnUpdateRequire, showOnUpdateExclude)) {
            emit requestShow();
        }
    }

    triggerUpdate();
}
