/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QMenu>
#include <QHeaderView>
#include <QSettings>
#include <QItemEditorFactory>
#include <QMessageBox>
#include <QProgressDialog>
#include <QFormLayout>

#include "core/number.hxx"
#include "core/timeutils.hxx"
#include "core/range.hxx"
#include "core/threading.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/archive/access.hxx"
#include "guicore/guiformat.hxx"
#include "guidata/valueeditor.hxx"

#include "eventlog.hxx"
#include "mainwindow.hxx"

using namespace CPD3;
using namespace CPD3::GUI;
using namespace CPD3::Data;

enum {
    Column_Time = 0, Column_Key, Column_Author, Column_Text,

    Column_TOTAL,
};

enum {
    Filter_User = 0, Filter_SystemAndUser, Filter_All, Filter_Manual,
};

namespace {
enum DisplayType {
    AcquisitionUser, AcquisitionSystem, AcquisitionSystemRealtime, Other,
};

struct DisplayData {
    double time;
    QString key;
    Variant::Root event;
    DisplayType type;

    QString authorText() const
    {
        switch (type) {
        case AcquisitionUser:
            break;
        case AcquisitionSystem:
        case AcquisitionSystemRealtime:
            return event["Source"].toDisplayString();
        case Other:
            return event["Information/By"].toDisplayString();
        }
        return event["Author"].toQString();
    }
};
}

class EventLogTableModel : public QAbstractItemModel {
    friend class EventLogDisplay;

    EventLogDisplay *display;

    QSettings settings;

    SequenceValue::Transfer allEvents;
    std::vector<DisplayData> displayedEvents;

    DisplayData buildData(const SequenceValue &event) const
    {
        DisplayData data;
        data.time = event.getStart();
        data.key = event.getVariableQString();
        data.event = event.root();
        if (data.key == "acquisition") {
            if (data.event["Source"].toString() == "EXTERNAL") {
                data.type = AcquisitionUser;
            } else {
                if (data.event["ShowRealtime"].toBool())
                    data.type = AcquisitionSystemRealtime;
                else
                    data.type = AcquisitionSystem;
            }
        } else {
            data.type = Other;
        }
        return data;
    }

    static bool passesFilter(const QLineEdit *filter, const QString &text)
    {
        QString filterText(filter->text());
        if (filterText.isEmpty())
            return true;
        QRegExp re(filterText, Qt::CaseInsensitive);
        if (!re.isValid())
            return true;
        return re.indexIn(text) >= 0;
    }

    bool shouldDisplay(const SequenceValue &event) const
    {
        int row = display->filterSelection->currentIndex();
        if (row >= 0) {
            switch (display->filterSelection->itemData(row).toInt()) {
            case Filter_User:
                if (event.getVariable() != "acquisition")
                    return false;
                if (event.read().hash("Source").toString() != "EXTERNAL")
                    return false;
                return true;
            case Filter_SystemAndUser:
                if (event.getVariable() != "acquisition")
                    return false;
                return true;
            case Filter_All:
                return true;
            case Filter_Manual:
                break;
            }
        }

        if (!passesFilter(display->keyFilter, event.getVariableQString()))
            return false;
        if (!passesFilter(display->sourceFilter, event.read().hash("Source").toDisplayString()))
            return false;
        if (!passesFilter(display->componentFilter,
                          event.read().hash("Component").toDisplayString())) {
            if (!passesFilter(display->componentFilter,
                              event.read().hash("Information").hash("By").toDisplayString()))
                return false;
        }
        if (!passesFilter(display->authorFilter, event.read().hash("Author").toQString()))
            return false;
        if (!passesFilter(display->textFilter, event.read().hash("Text").toDisplayString()))
            return false;

        return true;
    }

    void buildDisplayed()
    {
        displayedEvents.clear();
        for (const auto &add : allEvents) {
            if (!shouldDisplay(add))
                continue;
            displayedEvents.emplace_back(buildData(add));
        }
    }

public:
    EventLogTableModel(EventLogDisplay *parent) : QAbstractItemModel(parent),
                                                  display(parent),
                                                  settings(CPD3GUI_ORGANIZATION,
                                                           CPD3GUI_APPLICATION)
    { }

    virtual ~EventLogTableModel()
    { }

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const
    {
        Q_UNUSED(parent);
        return createIndex(row, column);
    }

    virtual QModelIndex parent(const QModelIndex &index) const
    {
        Q_UNUSED(index);
        return QModelIndex();
    }

    virtual int rowCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return static_cast<int>(displayedEvents.size());
    }

    virtual int columnCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return Column_TOTAL;
    }

    virtual QVariant data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();
        if (index.row() >= static_cast<int>(displayedEvents.size()))
            return QVariant();
        const auto &data = displayedEvents[index.row()];

        switch (role) {
        case Qt::ForegroundRole:
            switch (data.type) {
            case AcquisitionUser:
                return QVariant();
            case AcquisitionSystem:
                return QBrush(Qt::darkYellow);
            case AcquisitionSystemRealtime:
                return QBrush(Qt::darkRed);
            case Other:
                return QBrush(Qt::lightGray);
            }
            return QVariant();

        case Qt::ToolTipRole:
            switch (data.type) {
            case AcquisitionUser:
                return tr("User message log entry.\n");
            case AcquisitionSystem:
            case AcquisitionSystemRealtime:
                return tr("Acquisition system event.\nSource: %1\nComponent %2",
                          "system tooltip").arg(data.event["Source"].toDisplayString(),
                                                data.event["Component"].toDisplayString());
            case Other:
                return tr("Processing event.\nBy: %1\nAt: %2\nEnvironment: %3",
                          "other tooltip").arg(data.event["Information/By"].toDisplayString(),
                                               GUITime::formatTime(
                                                       data.event["Information/At"].toDouble(),
                                                       &settings, true),
                                               data.event["Information/Environment"].toDisplayString());
            }
            return QVariant();

        case Qt::DisplayRole:
            switch (index.column()) {
            case Column_Time:
                return GUITime::formatTime(data.time, &settings, true);
            case Column_Key:
                return data.key;
            case Column_Author:
                return data.authorText();
            case Column_Text:
                return data.event["Text"].toDisplayString();
            }
            break;
        }
        return QVariant();
    }

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const
    {
        if (role != Qt::DisplayRole)
            return QVariant();
        if (orientation == Qt::Vertical)
            return QVariant(section + 1);

        switch (section) {
        case Column_Time:
            return tr("Time");
        case Column_Key:
            return tr("Type");
        case Column_Author:
            return tr("Author");
        case Column_Text:
            return tr("Text");
        default:
            break;
        }
        return QVariant();
    }

    static inline bool compareTimeAscending(const DisplayData &a, const DisplayData &b)
    { return Range::compareStart(a.time, b.time) < 0; }

    static inline bool compareTimeDescending(const DisplayData &a, const DisplayData &b)
    { return Range::compareStart(a.time, b.time) > 0; }

    static inline bool compareKeyAscending(const DisplayData &a, const DisplayData &b)
    { return a.key < b.key; }

    static inline bool compareKeyDescending(const DisplayData &a, const DisplayData &b)
    { return a.key > b.key; }

    static inline bool compareAuthorAscending(const DisplayData &a, const DisplayData &b)
    { return a.authorText() < b.authorText(); }

    static inline bool compareAuthorDescending(const DisplayData &a, const DisplayData &b)
    { return a.authorText() > b.authorText(); }

    static inline bool compareTextAscending(const DisplayData &a, const DisplayData &b)
    {
        return a.event["Text"].toDisplayString() < b.event["Text"].toDisplayString();
    }

    static inline bool compareTextDescending(const DisplayData &a, const DisplayData &b)
    {
        return a.event["Text"].toDisplayString() > b.event["Text"].toDisplayString();
    }

    virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder)
    {
        beginResetModel();
        switch (column) {
        case Column_Time:
            std::stable_sort(displayedEvents.begin(), displayedEvents.end(),
                             order == Qt::AscendingOrder ? compareTimeAscending
                                                         : compareTimeDescending);
            break;
        case Column_Key:
            std::stable_sort(displayedEvents.begin(), displayedEvents.end(),
                             order == Qt::AscendingOrder ? compareKeyAscending
                                                         : compareKeyDescending);
            break;
        case Column_Author:
            std::stable_sort(displayedEvents.begin(), displayedEvents.end(),
                             order == Qt::AscendingOrder ? compareAuthorAscending
                                                         : compareAuthorDescending);
            break;
        case Column_Text:
            std::stable_sort(displayedEvents.begin(), displayedEvents.end(),
                             order == Qt::AscendingOrder ? compareTextAscending
                                                         : compareTextDescending);
            break;
        }
        endResetModel();
    }

    void updateEvents(const SequenceValue::Transfer &input)
    {
        beginResetModel();
        allEvents = input;
        displayedEvents.clear();
        buildDisplayed();
        endResetModel();
    }

    void displayFilterChanged()
    {
        beginResetModel();
        buildDisplayed();
        endResetModel();
    }
};

EventLogDisplay::EventLogDisplay(GUIMainWindow *win, Qt::WindowFlags f) : QDialog(win, f),
                                                                          mainWindow(win)
{
    QVBoxLayout *topLayout = new QVBoxLayout;
    setLayout(topLayout);

    setWindowTitle(tr("Event log"));

    model = new EventLogTableModel(this);
    table = new QTableView(this);
    topLayout->addWidget(table);
    table->setModel(model);

    table->setToolTip(tr("The table of all events."));
    table->setStatusTip(tr("System Events"));
    table->setWhatsThis(tr("This table shows the selected system events."));

    table->verticalHeader()->hide();
    table->setSelectionMode(QAbstractItemView::ExtendedSelection);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->setSortingEnabled(true);
    table->setWordWrap(true);
    table->setTextElideMode(Qt::ElideNone);
    connect(table->selectionModel(), SIGNAL(selectionChanged(
                                                    const QItemSelection &, const QItemSelection &)),
            this, SLOT(selectedChanged()));

    table->horizontalHeader()->setSectionResizeMode(Column_Time, QHeaderView::ResizeToContents);
    table->horizontalHeader()->setSectionResizeMode(Column_Key, QHeaderView::ResizeToContents);
    table->horizontalHeader()->setSectionResizeMode(Column_Author, QHeaderView::ResizeToContents);

    table->horizontalHeader()->setStretchLastSection(true);
    table->horizontalHeader()->setSectionResizeMode(Column_Text, QHeaderView::Stretch);

    table->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    table->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(table, &QTableView::customContextMenuRequested, this,
            &EventLogDisplay::itemContextMenu);

    table->setMinimumWidth(900);

    filterSelection = new QComboBox(this);
    topLayout->addWidget(filterSelection);
    filterSelection->setToolTip(tr("Select the event filtering."));
    filterSelection->setStatusTip(tr("Event filter"));
    filterSelection->setWhatsThis(
            tr("This allows you to select the type of filtering applied to the visible events."));
    filterSelection->addItem(tr("Display only user entered messages"), Filter_User);
    filterSelection->addItem(tr("Display acquisition system and user entered messages"),
                             Filter_SystemAndUser);
    filterSelection->addItem(tr("Display all events"), Filter_All);
    filterSelection->addItem(tr("Manually enter the event filtering conditions"), Filter_Manual);
    connect(filterSelection, SIGNAL(currentIndexChanged(int)), this, SLOT(filterUpdated()));

    manualBox = new QWidget(this);
    topLayout->addWidget(manualBox);
    QFormLayout *manualLayout = new QFormLayout;
    manualBox->setLayout(manualLayout);

    textFilter = new QLineEdit(manualBox);
    manualLayout->addRow(tr("&Text:"), textFilter);
    textFilter->setToolTip(tr("The regular expression selecting the text of events to display."));
    textFilter->setStatusTip(tr("Text filter"));
    textFilter->setWhatsThis(
            tr("This is a regular expression that the text of events are matched against.  If left empty then any text is accepted."));
    connect(textFilter, SIGNAL(textEdited(
                                       const QString &)), this, SLOT(filterUpdated()));

    authorFilter = new QLineEdit(manualBox);
    manualLayout->addRow(tr("&Author:"), authorFilter);
    authorFilter->setToolTip(tr("The regular expression selecting the authors to display."));
    authorFilter->setStatusTip(tr("Author filter"));
    authorFilter->setWhatsThis(
            tr("This is a regular expression that the author of events are matched against.  If left empty then any author is accepted."));
    connect(authorFilter, SIGNAL(textEdited(
                                         const QString &)), this, SLOT(filterUpdated()));

    keyFilter = new QLineEdit(manualBox);
    manualLayout->addRow(tr("T&ype:"), keyFilter);
    keyFilter->setToolTip(tr("The regular expression selecting the types to display."));
    keyFilter->setStatusTip(tr("Type filter"));
    keyFilter->setWhatsThis(
            tr("This is a regular expression that the types of events are matched against.  If left empty then any key is accepted."));
    connect(keyFilter, SIGNAL(textEdited(
                                      const QString &)), this, SLOT(filterUpdated()));

    sourceFilter = new QLineEdit(manualBox);
    manualLayout->addRow(tr("&Source:"), sourceFilter);
    sourceFilter->setToolTip(tr("The regular expression selecting the sources to display."));
    sourceFilter->setStatusTip(tr("Source filter"));
    sourceFilter->setWhatsThis(
            tr("This is a regular expression that the source of events are matched against.  If left empty then any source is accepted."));
    connect(sourceFilter, SIGNAL(textEdited(
                                         const QString &)), this, SLOT(filterUpdated()));

    componentFilter = new QLineEdit(manualBox);
    manualLayout->addRow(tr("&Component:"), componentFilter);
    componentFilter->setToolTip(tr("The regular expression selecting the components to display."));
    componentFilter->setStatusTip(tr("Component filter"));
    componentFilter->setWhatsThis(
            tr("This is a regular expression that the component origin of events are matched against.  If left empty then any component is accepted."));
    connect(componentFilter, SIGNAL(textEdited(
                                            const QString &)), this, SLOT(filterUpdated()));

    filterUpdated();
}

EventLogDisplay::~EventLogDisplay()
{ }

void EventLogDisplay::load(const SequenceName::Component &station, double start, double end)
{
    QProgressDialog progress(tr("Loading event log."), tr("Cancel"), 0, 1000, this,
                             (Qt::WindowFlags) (Qt::ApplicationModal |
                                     Qt::CustomizeWindowHint |
                                     Qt::WindowTitleHint));
    progress.setValue(0);

    struct Context {
        EventLogDisplay *dialog;
        std::shared_ptr<CPD3::Data::Archive::Access> access;
        std::shared_ptr<CPD3::Data::Archive::Access::ReadLock> lock;
        std::mutex mutex;
        bool aborted;

        Context(EventLogDisplay *dlg) : dialog(dlg),
                                        access(std::make_shared<Archive::Access>()),
                                        lock(std::make_shared<Archive::Access::ReadLock>(*access)),
                                        mutex(),
                                        aborted(false)
        { }

        ~Context()
        {
            lock.reset();
            access.reset();
        }

        void abort()
        {
            {
                std::lock_guard<std::mutex> lg(mutex);
                aborted = true;
            }
            access->signalTerminate();
            progress.disconnect();
        }

        bool isAborted()
        {
            std::lock_guard<std::mutex> lg(mutex);
            return aborted;
        }

        CPD3::Threading::Signal<int> progress;
    };
    std::weak_ptr<Context> activeLoad;

    {
        auto context = std::make_shared<Context>(this);
        activeLoad = context;

        Threading::pollFuture(this, std::async(std::launch::async, [=]() {
            SequenceValue::Transfer result;

            StreamSink::Iterator data;
            auto read = context->access
                               ->readStream(Archive::Selection(start, end, {station}, {"events"}),
                                            &data);
            while (data.hasNext()) {
                if (context->isAborted()) {
                    data.abort();
                    read->signalTerminate();
                    break;
                }
                auto value = data.next();

                double checkStart = value.getStart();
                if (!FP::defined(checkStart) ||
                        !FP::defined(start) ||
                        !FP::defined(end) ||
                        checkStart < start) {
                    context->progress(0);
                } else if (checkStart >= end) {
                    context->progress(999);
                } else {
                    int pv = (int) ((checkStart - start) / (end - start));
                    if (pv < 0)
                        pv = 0;
                    else if (pv > 999)
                        pv = 999;
                    context->progress(pv);
                }

                result.emplace_back(std::move(value));
            }
            read->wait();

            return result;
        }), [context, &progress](const SequenceValue::Transfer &result) {
            if (context->isAborted())
                return;
            context->dialog->model->updateEvents(result);
            progress.reset();
        });
    }

    /* Safe because the complete signal cannot be delivered before we re-enter the
     * event loop, so even if the load has finished, the reset() is still
     * pending */
    progress.exec();

    if (progress.wasCanceled()) {
        if (auto l = activeLoad.lock()) {
            l->abort();
        }
        activeLoad.reset();
        hide();
        return;
    }

}

void EventLogDisplay::selectedChanged()
{
    if (mainWindow == NULL)
        return;

    mainWindow->highlightResetEventLog();
    QModelIndexList selected(table->selectionModel()->selectedRows());
    for (QList<QModelIndex>::const_iterator index = selected.constBegin(),
            end = selected.constEnd(); index != end; ++index) {
        int row = index->row();
        if (row < 0 || row >= static_cast<int>(model->displayedEvents.size()))
            continue;
        DisplayData data(model->displayedEvents[row]);
        mainWindow->highlightAddEventLog(data.time, data.time);
    }
}

void EventLogDisplay::filterUpdated()
{
    int row = filterSelection->currentIndex();
    if (row >= 0) {
        switch (filterSelection->itemData(row).toInt()) {
        case Filter_User:
            manualBox->hide();
            keyFilter->setText("acquisition");
            sourceFilter->setText("EXTERNAL");
            componentFilter->setText(QString());
            authorFilter->setText(QString());
            textFilter->setText(QString());
            table->setColumnHidden(Column_Key, true);
            break;
        case Filter_SystemAndUser:
            manualBox->hide();
            keyFilter->setText("acquisition");
            sourceFilter->setText(QString());
            componentFilter->setText(QString());
            authorFilter->setText(QString());
            textFilter->setText(QString());
            table->setColumnHidden(Column_Key, true);
            break;
        case Filter_All:
            manualBox->hide();
            keyFilter->setText(QString());
            sourceFilter->setText(QString());
            componentFilter->setText(QString());
            authorFilter->setText(QString());
            textFilter->setText(QString());
            table->setColumnHidden(Column_Key, false);
            break;
        case Filter_Manual:
            manualBox->show();
            table->setColumnHidden(Column_Key, false);
            break;
        }
    } else {
        manualBox->hide();
    }

    model->displayFilterChanged();
}

void EventLogDisplay::itemContextMenu(QPoint pos)
{
    auto index = table->indexAt(pos);
    if (!index.isValid())
        return;

    auto menu = new QMenu(this);

    {
        auto act = new QAction(tr("Show &Details", "Context|Details"), menu);
        menu->addAction(act);
        act->setToolTip(tr("Display full details of the event."));
        act->setStatusTip(tr("Show event details"));
        act->setWhatsThis(tr("Show a pop up display of the full details of the event."));
        QObject::connect(act, &QAction::triggered, this, [this, index] {
            if (index.row() >= static_cast<int>(model->displayedEvents.size()))
                return;

            QDialog details(this);
            details.setWindowModality(Qt::WindowModal);
            details.setWindowTitle(tr("Event Details"));
            details.setMinimumSize(QSize(640, 480));

            auto layout = new QVBoxLayout(&details);
            details.setLayout(layout);

            auto editor = new CPD3::GUI::Data::ValueEditor(&details);
            editor->setReadOnly(true);
            editor->setValue(model->displayedEvents[index.row()].event.write());
            layout->addWidget(editor);

            details.exec();
        });
    }

    menu->popup(table->viewport()->mapToGlobal(pos));
}

void EventLogDisplay::hideEvent(QHideEvent *event)
{
    table->clearSelection();
    if (mainWindow != NULL)
        mainWindow->highlightResetEventLog();
    QDialog::hideEvent(event);
}
