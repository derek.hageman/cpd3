/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef DISPLAYHANDLER_H
#define DISPLAYHANDLER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QString>
#include <QSize>
#include <QTimer>

#include "datacore/segment.hxx"
#include "graphing/display.hxx"
#include "graphing/displaywidget.hxx"

class CPD3GUI;

/**
 * A wrapper and handler for general graphing displays.
 */
class DisplayHandler : public CPD3::Graphing::DisplayWidget {
Q_OBJECT

    CPD3GUI *gui;
    CPD3::Graphing::Display *display;
    std::string name;

    QString windowTitle;
    qint64 sortPriority;

    double lastPlottedStart;
    double lastPlottedEnd;

    QTimer repaintTimer;
    int baseInterval;
    int thresholdInterval;

    QTimer invisibleTimer;
public:
    DisplayHandler(const CPD3::Data::ValueSegment::Transfer &config,
                   std::string name,
                   std::unique_ptr<CPD3::Graphing::Display> &&setDisplay,
                   CPD3GUI *setGUI,
                   QWidget *parent = 0);

    virtual ~DisplayHandler();

    /**
     * Get the internal configuration name of the display.
     * 
     * @return the internal name
     */
    inline const std::string &getName() const
    { return name; }

    /**
     * Get the display window title.
     * 
     * @return the window title
     */
    inline QString getWindowTitle() const
    { return windowTitle; }

    /**
     * Get the name to be associated with this handler's user state.
     * 
     * @return a string to associate with the state name
     */
    QString getUserStateName() const;

    /**
     * Get the display sort priority.
     * 
     * @return the display sort priority
     */
    inline qint64 getDisplaySortPriority() const
    { return sortPriority; }

    /**
     * Inject this display into the given global zoom command.
     * 
     * @param command   the command to add this display to
     */
    void injectGlobalZoom(CPD3::Graphing::DisplayZoomCommand &command);

    /**
     * Inject this display into the given global zoom reset command.
     * 
     * @param command   the command to add this display to
     */
    void injectResetZoom(CPD3::Graphing::DisplayZoomCommand &command);

    /**
     * Trim the stored data to the given range.  This just calls
     * trimData(double, double) on the base display.
     * 
     * @param start     the start of data to retain
     * @param end       the end of data to retain
     */
    void applyDataTrim(double start, double end);

    virtual QSize sizeHint() const;

protected:
    virtual void paintEvent(QPaintEvent *event);

    virtual void keyPressEvent(QKeyEvent *event);

    virtual void keyReleaseEvent(QKeyEvent *event);

    virtual void enterEvent(QEvent *event);

    virtual void leaveEvent(QEvent *event);

    virtual void mouseMoveEvent(QMouseEvent *event);

    virtual void mousePressEvent(QMouseEvent *event);

    virtual void mouseReleaseEvent(QMouseEvent *event);

private slots:

    void handleSave(CPD3::Data::ValueSegment::Transfer overlay);

    void resetLegendShow();

    void invisibleUpdate();
};

Q_DECLARE_METATYPE(DisplayHandler *);

#endif
