/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef GENERICHANDER_H
#define GENERICHANDER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QSet>
#include <QList>

#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "acquisition/realtimelayout.hxx"

#include "realtimedisplay.hxx"
#include "handlercommon.hxx"

class CPD3GUI;

class GenericHandler : public HandlerCommon {
Q_OBJECT

    CPD3GUI *parent;
    int id;
    CPD3::Data::Variant::Root configuration;

    QString menuText;
    QString windowTitle;
    qint64 displaySortPriority;
    CPD3::Data::SequenceMatch::Composite showOnUpdate;
    CPD3::Data::Variant::Root showOnUpdateRequire;
    CPD3::Data::Variant::Root showOnUpdateExclude;

    struct InputSelection {
        CPD3::Data::SequenceMatch::OrderedLookup selection;
        CPD3::Data::Variant::Root metadata;
    };
    std::vector<InputSelection> inputSelectionsInstant;
    std::vector<InputSelection> inputSelectionsBoxcar;
    std::vector<InputSelection> inputSelectionsAverage;

    QSet<CPD3::Data::SequenceName> seenInstant;
    QSet<CPD3::Data::SequenceName> seenBoxcar;
    QSet<CPD3::Data::SequenceName> seenAverage;

    InputSelection generateInputSelection(const CPD3::Data::SequenceName::Component &defaultArchive,
                                          const CPD3::Data::SequenceName::Component &defaultVariable,
                                          const CPD3::Data::Variant::Read &config);

    void generateInputSelections(std::vector<InputSelection> &target,
                                 const CPD3::Data::SequenceName::Component &defaultArchive,
                                 const CPD3::Data::Variant::Read &config);

    void registerAllExisting(QSet<CPD3::Data::SequenceName> &seen,
                             std::vector<InputSelection> &selections,
                             CPD3::Acquisition::RealtimeLayout &layout);

    void handleSelectionUnit(const CPD3::Data::SequenceName &unit,
                             QSet<CPD3::Data::SequenceName> &seen,
                             std::vector<InputSelection> &selections,
                             CPD3::Acquisition::RealtimeLayout &layout);

public:
    /**
     * Create the generic handler.
     * 
     * @param config    the configuration
     * @param id        the ID
     * @param parent    the parent GUI
     */
    GenericHandler(const CPD3::Data::Variant::Read &config, int id, CPD3GUI *parent);

    /**
     * Get the ID set at creation.
     * 
     * @return the ID
     */
    inline int getID() const
    { return id; }

    /**
     * Get the display window title.
     * 
     * @return the window title
     */
    inline QString getWindowTitle() const
    { return windowTitle; }

    /**
     * Get the acquisition menu text.
     * 
     * @return  the menu text
     */
    inline QString getMenuText() const
    { return menuText; }

    /**
     * Test if the handler is hidden in the acquisition menu.
     * 
     * @return  true if the interface is hidden
     */
    bool hideInAcquisitionMenu() const;

    /**
     * Get the unique name for the handler.
     * 
     * @return the unique name
     */
    QString getName() const;

    /**
     * Process incoming realtime values.
     * 
     * @param data  the incoming data
     */
    void incomingRealtime(const CPD3::Data::SequenceValue::Transfer &data);

    /**
     * Get the name to be associated with this handler's user state.
     * 
     * @return a string to associate with the state name
     */
    QString getUserStateName() const;

    /**
     * Get the display sort priority.
     * 
     * @return the display sort priority
     */
    qint64 getDisplaySortPriority() const;

signals:

    /**
     * Emitted when the handler requests the showing of its window.  This
     * is used to implement the "ShowOnUpdate" flag.
     */
    void requestShow();
};

Q_DECLARE_METATYPE(GenericHandler *);

#endif
