/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef DATAACCESS_HXX
#define DATAACCESS_HXX

#include "core/first.hxx"

#include <memory>
#include <unordered_set>
#include <map>
#include <mutex>
#include <condition_variable>

#include "datacore/processingtapchain.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/sequencematch.hxx"

class CPD3GUI;

/**
 * The central interface for data access via the GUI.
 * <br>
 * This handles the process of reading from the local archive, then reading from
 * the remote one and finally connecting to ongoing realtime, if required.
 */
class DataAccess {
    CPD3GUI &gui;

    class ArchiveReadSink;

    struct AccessContext {
        DataAccess &data;
        CPD3::Data::Archive::Access access;

        AccessContext(DataAccess &data);
    };

    class ActiveReadContext {
        std::shared_ptr<AccessContext> access;
        CPD3::Data::Archive::Selection::List selections;
        CPD3::Data::StreamSink *sink;

        double furthestTime;
        enum {
            Running, Stop_Requested, Finished
        } state;

        class ArchiveReadSink final : public CPD3::Data::StreamSink {
            ActiveReadContext &context;
        public:
            ArchiveReadSink(ActiveReadContext &context);

            virtual ~ArchiveReadSink();

            void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

            void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

            void incomingData(const CPD3::Data::SequenceValue &value) override;

            void incomingData(CPD3::Data::SequenceValue &&value) override;

            void endData() override;
        };

        friend class ArchiveReadSink;

        ArchiveReadSink archiveReadSink;
        CPD3::Data::Archive::Access::StreamHandle archiveReader;

        std::mutex mutex;
        std::condition_variable externalUpdate;
        std::unique_ptr<CPD3::Data::SequenceMatch::Basic> dataFilter;
        CPD3::Data::SequenceName::Map<bool> dataPrefilterResult;
        CPD3::Data::SequenceValue::Transfer pendingRealtime;

        static void threadRun(const std::shared_ptr<ActiveReadContext> &context);

        void realtimeRun();

        void processRealtimeValue(const CPD3::Data::SequenceValue &value);

    public:
        ActiveReadContext(std::shared_ptr<AccessContext> &&access,
                          const CPD3::Data::Archive::Selection::List &selections,
                          CPD3::Data::StreamSink *sink);

        static void launch(DataAccess &access,
                           const CPD3::Data::Archive::Selection::List &selections,
                           CPD3::Data::StreamSink *sink);

        void incomingRealtime(const CPD3::Data::SequenceValue &value);

        void incomingRealtime(const CPD3::Data::SequenceValue::Transfer &values);

        void stop();

        CPD3::Data::Archive::Selection::List getRemoteReadSelections() const;

        double getFurthestTime() const;
    };

    friend class ActiveReadContext;

    class ArchiveBackend final : public CPD3::Data::ProcessingTapChain::ArchiveBackend {
        DataAccess &access;
    public:
        ArchiveBackend(DataAccess &access);

        virtual ~ArchiveBackend();

        virtual void issueArchiveRead(const CPD3::Data::Archive::Selection::List &selections,
                                      CPD3::Data::StreamSink *sink);
    };

    ArchiveBackend archiveBackend;

    class RemoteReadSink final : public CPD3::Data::StreamSink {
        DataAccess &access;
    public:
        RemoteReadSink(DataAccess &access);

        virtual ~RemoteReadSink();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        void endData() override;
    };

    friend class RemoteReadSink;

    RemoteReadSink remoteReadSink;

    std::mutex mutex;
    bool stopInProgress;
    double remoteCachePurgeTime;

    enum {
        RemoteRead_None, RemoteRead_Request, RemoteRead_Active, RemoteRead_Completed,
    } remoteReadState;
    std::condition_variable remoteReadUpdated;
    CPD3::Data::Archive::Selection::List remoteReadSelections;

    std::weak_ptr<AccessContext> accessContext;
    std::vector<std::weak_ptr<ActiveReadContext>> connectedRealtime;

    std::vector<std::shared_ptr<ActiveReadContext>> lockRealtime();

    std::map<CPD3::Data::SequenceIdentity, CPD3::Data::Variant::Root,
             CPD3::Data::SequenceIdentity::OrderTime> remoteCache;

    struct SelectionHash {
        std::size_t operator()(const CPD3::Data::Archive::Selection &selection) const;
    };

    struct SelectionEqual {
        bool operator()(const CPD3::Data::Archive::Selection &a,
                        const CPD3::Data::Archive::Selection &b) const;
    };

    std::unordered_set<CPD3::Data::Archive::Selection, SelectionHash, SelectionEqual>
            remoteCacheContents;

    struct RemoteReadResult {
        CPD3::Data::SequenceValue::Transfer data;
        bool ended;

        RemoteReadResult() = default;

        RemoteReadResult(RemoteReadResult &&other) = default;

        RemoteReadResult &operator=(RemoteReadResult &&other) = default;
    };

    RemoteReadResult readFromRemote(const std::shared_ptr<ActiveReadContext> &context);

    bool allInRemoteCache() const;

    CPD3::Data::SequenceValue::Transfer satisfyFromRemoteCache(double beginTime) const;

    void releaseAccess(const std::shared_ptr<ActiveReadContext> &context);

    void sendToRealtimeConnected(const CPD3::Data::SequenceValue &value);

    void sendToRealtimeConnected(const CPD3::Data::SequenceValue::Transfer &values);

public:
    /**
     * Create the accessor.
     *
     * @param gui   the GUI in use (for remote access)
     */
    DataAccess(CPD3GUI &gui);

    /**
     * Get the archive backend for attachment.
     *
     * @return  the archive access backend
     */
    CPD3::Data::ProcessingTapChain::ArchiveBackend *getArchiveBackend();


    /**
     * Handle an incoming realtime value.
     *
     * @param value the value
     */
    void incomingRealtime(const CPD3::Data::SequenceValue &value);

    /**
     * Handle an incoming realtime value.
     *
     * @param value the value
     */
    void incomingRealtime(CPD3::Data::SequenceValue &&value);

    /**
     * Handle incoming realtime values.
     *
     * @param values    the values
     */
    void incomingRealtime(const CPD3::Data::SequenceValue::Transfer &values);

    /**
     * Handle incoming realtime values.
     *
     * @param values    the values
     */
    void incomingRealtime(CPD3::Data::SequenceValue::Transfer &&values);


    /**
     * Stop the access, shutting down all active realtime reads.
     */
    void stop();

    /**
     * Restart the access.
     */
    void restart();

    /**
     * Trim the data currently retained in the cache to begin at the
     * given time.
     *
     * @param retainBegin       the beginning of data to retain
     */
    void trimCached(double retainBegin);

    /**
     * Purge the entire acquisition reader cache.
     */
    void purgeCache();

    /**
     * Find all matching names from the remote cache.
     *
     * @param selection the selection to match
     */
    CPD3::Data::SequenceName::Set cachedMatches(const CPD3::Data::SequenceMatch::Composite &selection);
};

#endif //DATAACCESS_HXX
