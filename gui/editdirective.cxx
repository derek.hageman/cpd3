/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QMenu>
#include <QHeaderView>
#include <QStyledItemDelegate>
#include <QSettings>
#include <QItemEditorFactory>
#include <QMessageBox>
#include <QProgressDialog>
#include <QLoggingCategory>

#include "core/number.hxx"
#include "core/timeutils.hxx"
#include "core/timeparse.hxx"
#include "core/range.hxx"
#include "core/threadpool.hxx"
#include "core/util.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "guicore/guiformat.hxx"
#include "datacore/archive/access.hxx"

#include "editdirective.hxx"
#include "mainwindow.hxx"


Q_LOGGING_CATEGORY(log_gui_editdirective, "cpd3.gui.editdirective", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::GUI;
using namespace CPD3::GUI::Data;
using namespace CPD3::Data;
using namespace CPD3::Editing;

enum {
    Column_Limits = 0, Column_Profile, Column_Author, Column_Action, Column_Summary, Column_Comment,

    Column_TOTAL,
};

namespace {
struct DisplayData {
    DirectivePointer directive;

    bool isSystemInternal;
    bool isOtherProfile;
    bool isDeleted;
};

class LimitsDelegate : public QStyledItemDelegate {
public:
    LimitsDelegate(QObject *parent = 0) : QStyledItemDelegate(parent)
    { }

    virtual ~LimitsDelegate()
    { }

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {

        QWidget *result =
                QItemEditorFactory::defaultFactory()->createEditor(static_cast<QVariant::Type>(
                                                                           index.data(Qt::EditRole)
                                                                                .userType()),
                                                                   parent);

        QFont font(option.font);
        font.setFamily("Monospace");
        font.setStyleHint(QFont::TypeWriter);
        result->setFont(font);

        return result;
    }
};

}

static bool addTriggerInequality(GUIMainWindow *window,
                                 const DirectivePointer &directive,
                                 const Variant::Read &left,
                                 const Variant::Read &right,
                                 bool asLess)
{
    double lhs = FP::undefined();
    switch (left.getType()) {
    case Variant::Type::Integer: {
        qint64 i = left.toInt64();
        if (!INTEGER::defined(i)) {
            lhs = FP::undefined();
        } else {
            lhs = (double) i;
        }
        break;
    }
    case Variant::Type::Real:
        lhs = left.toDouble();
        break;
    case Variant::Type::String:
        switch (right.getType()) {
        case Variant::Type::Integer:
        case Variant::Type::Real:
            return addTriggerInequality(window, directive, right, left, !asLess);
        default: {
            const auto &type = right.hash("Type").toString();
            if (Util::equal_insensitive(type, "constant")) {
                return addTriggerInequality(window, directive, right, left, !asLess);
            }
            break;
        }
        }
        return false;
    case Variant::Type::Array:
        return false;
    default: {
        const auto &type = left.hash("Type").toString();
        if (Util::equal_insensitive(type, "constant")) {
            lhs = left.hash("Value").toDouble();
        } else {
            switch (right.getType()) {
            case Variant::Type::Integer:
            case Variant::Type::Real:
                return addTriggerInequality(window, directive, right, left, !asLess);
            default: {
                const auto &rtype = right.hash("Type").toString();
                if (Util::equal_insensitive(rtype, "constant")) {
                    return addTriggerInequality(window, directive, right, left, !asLess);
                }
                break;
            }
            }
            return false;
        }
        break;
    }
    }

    if (!FP::defined(lhs))
        return false;

    switch (right.getType()) {
    case Variant::Type::Integer:
    case Variant::Type::Real:
        return false;
    case Variant::Type::Array: {
        bool result = false;
        for (auto add : right.toArray()) {
            result = addTriggerInequality(window, directive, left, add, asLess) || result;
        }
        return result;
    }
    default:
        break;
    }

    const auto &type = right.hash("Type").toString();
    if (Util::equal_insensitive(type, "constant", "sin", "cos", "log", "ln", "log10", "exp", "abs",
                                "absolute", "absolutevalue", "poly", "polynomial", "cal",
                                "calibration", "polyinvert", "polynomialinvert", "invertcal",
                                "invertcalibration", "sd", "standarddeviation", "quantile",
                                "minimum", "min", "maximum", "max", "slope", "length", "duration",
                                "elapsed", "average", "smoothed", "sum", "add", "difference",
                                "subtract", "product", "multiply", "power")) {
        /* Don't support transformation functions */
        return false;
    } else if (Util::equal_insensitive(type, "mean", "median")) {
        /* These don't change the value, so just pass them through */
        return addTriggerInequality(window, directive, left, right.hash("Input"), asLess);
    } else if (Util::equal_insensitive(type, "first", "firstvalid", "valid")) {
        bool result = false;
        for (auto add : right.hash("Inputs").toArray()) {
            result = addTriggerInequality(window, directive, left, add, asLess) || result;
        }
        return result;
    }

    if (asLess) {
        window->highlightAddMentorEdit(directive->getStart(), directive->getEnd(), lhs,
                                       FP::undefined(),
                                       SequenceMatch::Composite(right.hash("Value")));
    } else {
        window->highlightAddMentorEdit(directive->getStart(), directive->getEnd(), FP::undefined(),
                                       lhs, SequenceMatch::Composite(right.hash("Value")));
    }
    return true;
}

static bool addTriggerRange(GUIMainWindow *window,
                            const DirectivePointer &directive,
                            const Variant::Read &left,
                            const Variant::Read &middle,
                            const Variant::Read &right,
                            bool invert)
{
    double lhs = FP::undefined();
    switch (left.getType()) {
    case Variant::Type::Integer: {
        qint64 i = left.toInt64();
        if (!INTEGER::defined(i)) {
            lhs = FP::undefined();
        } else {
            lhs = (double) i;
        }
        break;
    }
    case Variant::Type::Real:
        lhs = left.toDouble();
        break;
    default: {
        const auto &type = left.hash("Type").toString();
        if (Util::equal_insensitive(type, "constant")) {
            lhs = left.hash("Value").toDouble();
        } else {
            return false;
        }
        break;
    }
    }
    if (!FP::defined(lhs))
        return false;

    double rhs = FP::undefined();
    switch (right.getType()) {
    case Variant::Type::Integer: {
        qint64 i = right.toInt64();
        if (!INTEGER::defined(i)) {
            rhs = FP::undefined();
        } else {
            rhs = (double) i;
        }
        break;
    }
    case Variant::Type::Real:
        rhs = right.toDouble();
        break;
    default: {
        const auto &type = right.hash("Type").toString();
        if (Util::equal_insensitive(type, "constant")) {
            rhs = right.hash("Value").toDouble();
        } else {
            return false;
        }
        break;
    }
    }
    if (!FP::defined(rhs))
        return false;

    switch (middle.getType()) {
    case Variant::Type::Integer:
    case Variant::Type::Real:
        return false;
    case Variant::Type::Array: {
        bool result = false;
        for (auto add : middle.toArray()) {
            result = addTriggerRange(window, directive, left, add, right, invert) || result;
        }
        return result;
    }
    default:
        break;
    }

    const auto &type = middle.hash("Type").toString();
    if (Util::equal_insensitive(type, "constant", "sin", "cos", "log", "ln", "log10", "exp", "abs",
                                "absolute", "absolutevalue", "poly", "polynomial", "cal",
                                "calibration", "polyinvert", "polynomialinvert", "invertcal",
                                "invertcalibration", "sd", "standarddeviation", "quantile",
                                "minimum", "min", "maximum", "max", "slope", "length", "duration",
                                "elapsed", "average", "smoothed", "sum", "add", "difference",
                                "subtract", "product", "multiply", "power")) {
        /* Don't support transformation functions */
        return false;
    } else if (Util::equal_insensitive(type, "mean", "median")) {
        /* These don't change the value, so just pass them through */
        return addTriggerInequality(window, directive, left, middle.hash("Input"), invert);
    } else if (Util::equal_insensitive(type, "first", "firstvalid", "valid")) {
        bool result = false;
        for (auto add : middle.hash("Inputs").toArray()) {
            result = addTriggerRange(window, directive, left, add, right, invert) || result;
        }
        return result;
    }

    if (lhs > rhs) {
        invert = !invert;
        qSwap(lhs, rhs);
    }

    if (invert) {
        window->highlightAddMentorEdit(directive->getStart(), directive->getEnd(), FP::undefined(),
                                       lhs, SequenceMatch::Composite(middle.hash("Value")));
        window->highlightAddMentorEdit(directive->getStart(), directive->getEnd(), rhs,
                                       FP::undefined(),
                                       SequenceMatch::Composite(middle.hash("Value")));
    } else {
        window->highlightAddMentorEdit(directive->getStart(), directive->getEnd(), lhs, rhs,
                                       SequenceMatch::Composite(right.hash("Value")));
    }
    return true;
}

static bool isInverted(const Variant::Read &config)
{
    if (config.hash("Invert").exists())
        return config.hash("Invert").toBool();
    else if (config.hash("Inverted").exists())
        return config.hash("Inverted").toBool();
    return false;
}

static bool addTriggerHighlight(GUIMainWindow *window,
                                const DirectivePointer &directive,
                                const Variant::Read &config)
{
    switch (config.getType()) {
    case Variant::Type::Empty:
    case Variant::Type::Boolean:
        return false;
    case Variant::Type::Array: {
        bool result = false;
        for (auto add : config.toArray()) {
            result = addTriggerHighlight(window, directive, add) || result;
        }
        return result;
    }
    default:
        break;
    }

    const auto &type = config.hash("Type").toString();
    if (Util::equal_insensitive(type, "or", "any", "and", "all")) {
        bool result = false;
        for (auto add : config.hash("Components").toArray()) {
            result = addTriggerHighlight(window, directive, add) || result;
        }
        return result;
    } else if (Util::equal_insensitive(type, "never", "disable")) {
        return false;
    } else if (Util::equal_insensitive(type, "periodic", "moment", "instant")) {
        /* Maybe make this display something neat? */
        return false;
    } else if (Util::equal_insensitive(type, "periodicrange", "timerange")) {
        /* Maybe make this display something neat? */
        return false;
    } else if (Util::equal_insensitive(type, "less", "lessthan", "lessequal", "lessthanorequal")) {
        return addTriggerInequality(window, directive, config.hash("Left"), config.hash("Right"),
                                    !isInverted(config));
    } else if (Util::equal_insensitive(type, "greater", "greaterthan", "greaterequal",
                                       "greaterthanorequal")) {
        return addTriggerInequality(window, directive, config.hash("Left"), config.hash("Right"),
                                    isInverted(config));
    } else if (Util::equal_insensitive(type, "equal", "equalto", "notequal", "notequalto")) {
        /* Ignored for now */
        return false;
    } else if (Util::equal_insensitive(type, "range", "insiderange", "modularrange",
                                       "insidemodularrange")) {
        return addTriggerRange(window, directive, config.hash("Start"), config.hash("Value"),
                               config.hash("End"), isInverted(config));
    } else if (Util::equal_insensitive(type, "outsiderange", "outsidemodularrange")) {
        return addTriggerRange(window, directive, config.hash("Start"), config.hash("Value"),
                               config.hash("End"), !isInverted(config));
    } else if (Util::equal_insensitive(type, "hasflag", "hasflags", "flag", "flags", "lacksflag",
                                       "lacksflags", "exists", "script")) {
        /* Not supported in the highlight shaping */
        return false;
    }

    return false;
}

static void addDirectiveHighlight(GUIMainWindow *window, const DirectivePointer &directive)
{
    const auto
            &type = directive->getValue().hash("Parameters").hash("Action").hash("Type").toString();

    bool haveAdded = false;
    if (Util::equal_insensitive(type, "contaminate", "contam", "uncontaminate", "clearcontam",
                                "unit", "setunit", "unitreplace", "translate", "duplicate",
                                "duplicatetranslate", "scriptdemultiplexer", "demultiplexer",
                                "NOOP", "None")) {
        haveAdded = addTriggerHighlight(window, directive,
                                        directive->getValue().hash("Parameters").hash("Trigger")) ||
                haveAdded;
        if (!haveAdded) {
            window->highlightAddMentorEdit(directive->getStart(), directive->getEnd());
        }
        return;
    } else if (Util::equal_insensitive(type, "flowcorrection", "flowcalibration", "spot",
                                       "spotsize", "multispot", "multispotsize", "clapspot")) {
        QString instrument(directive->getValue()
                                    .hash("Parameters")
                                    .hash("Action").hash("Instrument").toQString());
        if (!instrument.isEmpty()) {
            window->highlightAddMentorEdit(directive->getStart(), directive->getEnd(),
                                           FP::undefined(), FP::undefined(),
                                           SequenceMatch::Composite(
                                                   SequenceMatch::Element::variable(
                                                           QString::fromLatin1(".*_%1").arg(
                                                                   instrument))));
            haveAdded = true;
        }
    }

    haveAdded = addTriggerHighlight(window, directive,
                                    directive->getValue().hash("Parameters").hash("Trigger")) ||
            haveAdded;

    if (!haveAdded) {
        auto selConfig = directive->getValue().hash("Parameters").hash("Action").hash("Selection");
        if (selConfig.exists()) {
            window->highlightAddMentorEdit(directive->getStart(), directive->getEnd(),
                                           FP::undefined(), FP::undefined(),
                                           SequenceMatch::Composite(selConfig));
            haveAdded = true;
        }
    }

    if (!haveAdded) {
        window->highlightAddMentorEdit(directive->getStart(), directive->getEnd());
    }
}

class EditDirectiveTableModel : public QAbstractItemModel {
    friend class EditDirectiveDisplay;

    EditDirectiveDisplay *display;

    QSettings settings;

    QList<DirectivePointer> allDirectives;
    QList<DisplayData> displayedDirectives;

    QString buildLimitsString(double start, double end, bool includeLabels = true) const
    {
        QString display;
        QString label(tr("Start: ", "time bound start label"));
        if (includeLabels)
            display.append(label);
        else
            display.append(QString(' ').repeated(label.length()));
        display.append(GUITime::formatTime(start, &settings, true));
        label = tr("  End: ", "time bound end label");
        if (includeLabels)
            display.append(label);
        else
            display.append(QString(' ').repeated(label.length()));
        display.append(GUITime::formatTime(end, &settings, true));
        return display;
    }

    DisplayData buildData(const DirectivePointer &directive) const
    {
        DisplayData data;
        data.directive = directive;
        data.isSystemInternal = directive->getValue().hash("SystemInternal").toBool();
        data.isOtherProfile = directive->getProfile() != display->loadedProfile;
        data.isDeleted = !directive->isActive();
        return data;
    }

    bool shouldDisplay(const DirectivePointer &directive) const
    {
        if (!display->displayDeleted->isChecked() && !directive->isActive())
            return false;
        if (!display->displaySystemInternal->isChecked() &&
                directive->getValue().hash("SystemInternal").toBool())
            return false;
        if (!display->displayAllProfiles->isChecked() &&
                directive->getProfile() != display->loadedProfile)
            return false;
        return true;
    }

    void buildDisplayed()
    {
        displayedDirectives.clear();
        for (QList<DirectivePointer>::const_iterator add = allDirectives.constBegin(),
                endAdd = allDirectives.constEnd(); add != endAdd; ++add) {
            if (!shouldDisplay(*add))
                continue;
            displayedDirectives.append(buildData(*add));
        }
    }

public:
    EditDirectiveTableModel(EditDirectiveDisplay *parent) : QAbstractItemModel(parent),
                                                            display(parent),
                                                            settings(CPD3GUI_ORGANIZATION,
                                                                     CPD3GUI_APPLICATION)
    { }

    virtual ~EditDirectiveTableModel()
    { }

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const
    {
        Q_UNUSED(parent);
        return createIndex(row, column);
    }

    virtual QModelIndex parent(const QModelIndex &index) const
    {
        Q_UNUSED(index);
        return QModelIndex();
    }

    virtual int rowCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return displayedDirectives.size();
    }

    virtual int columnCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return Column_TOTAL;
    }

    virtual QVariant data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();
        if (index.row() >= displayedDirectives.size())
            return QVariant();
        DisplayData data(displayedDirectives.at(index.row()));

        switch (role) {
        case Qt::ForegroundRole:
            if (data.isDeleted) {
                return QBrush(Qt::darkRed);
            } else if (data.isSystemInternal) {
                return QBrush(Qt::darkYellow);
            } else if (data.isOtherProfile && index.column() == Column_Profile) {
                return QBrush(Qt::lightGray);
            }
            return QVariant();

        case Qt::ToolTipRole:
            if (data.isDeleted) {
                return tr("This edit directive has been deleted.");
            } else if (data.isSystemInternal) {
                return tr("This edit directive is an internal system edit.");
            } else if (data.isOtherProfile) {
                return tr("This edit directive belongs to another editing profile.");
            }
            return QVariant();

        case Qt::DisplayRole:
            switch (index.column()) {
            case Column_Limits:
                return buildLimitsString(data.directive->getStart(), data.directive->getEnd());
            case Column_Profile:
                return data.directive->getProfile();
            case Column_Author:
                return data.directive->getValue().hash("Author").toQString();
            case Column_Action: {
                const auto &type = data.directive
                                       ->getValue()
                                       .hash("Parameters")
                                       .hash("Action")
                                       .hash("Type")
                                       .toString();
                if (Util::equal_insensitive(type, "remove")) {
                    return tr("Remove");
                } else if (Util::equal_insensitive(type, "poly", "polynomial", "cal", "calibration",
                                                   "multipoly", "multipolynomial", "multical",
                                                   "multicalibration")) {
                    return tr("Calibration");
                } else if (Util::equal_insensitive(type, "polyinvert", "polynomialinvert",
                                                   "invertcal"
                                                   "invertcalibration")) {
                    return tr("Inverse calibration");
                } else if (Util::equal_insensitive(type, "recalibrate")) {
                    return tr("Re-calibration");
                } else if (Util::equal_insensitive(type, "wrap", "modular", "modulus")) {
                    return tr("Wrap");
                } else if (Util::equal_insensitive(type, "overlay", "set")) {
                    return tr("Value change");
                } else if (Util::equal_insensitive(type, "meta", "metadata", "overlaymeta",
                                                   "overlaymetadata")) {
                    return tr("Metadata");
                } else if (Util::equal_insensitive(type, "serial", "setserial")) {
                    return tr("Serial number");
                } else if (Util::equal_insensitive(type, "flag", "addflag", "addflags",
                                                   "removeflag", "removeflags")) {
                    return tr("Flags");
                } else if (Util::equal_insensitive(type, "contaminate", "contam")) {
                    return tr("Contaminate");
                } else if (Util::equal_insensitive(type, "uncontaminate", "clearcontam")) {
                    return tr("Un-contaminate");
                } else if (Util::equal_insensitive(type, "flowcorrection", "flowcalibration")) {
                    return tr("Flow");
                } else if (Util::equal_insensitive(type, "spot", "spotsize", "multispot",
                                                   "multispotsize", "clapspot")) {
                    return tr("Spot");
                } else if (Util::equal_insensitive(type, "unit", "setunit", "unitreplace",
                                                   "translate")) {
                    return tr("Identifier change");
                } else if (Util::equal_insensitive(type, "duplicate", "duplicatetranslate")) {
                    return tr("Duplication");
                } else if (Util::equal_insensitive(type, "flavors")) {
                    return tr("Flavor change");
                } else if (Util::equal_insensitive(type, "setcut", "cut")) {
                    return tr("Cut size");
                } else if (Util::equal_insensitive(type, "arithmetic", "math", "function")) {
                    return tr("Arithmetic");
                } else if (Util::equal_insensitive(type, "scriptvalue", "scriptvalues",
                                                   "scriptsegment", "scriptsegments", "script",
                                                   "scriptdemultiplexer", "demultiplexer",
                                                   "scriptgeneralvalue", "scriptgeneralvalues",
                                                   "scriptgeneralsegment",
                                                   "scriptgeneralsegments")) {
                    return tr("Script");
                } else if (Util::equal_insensitive(type, "NOOP", "None")) {
                    return tr("None");
                } else if (Util::equal_insensitive(type, "AbnormalDataEpisode")) {
                    return tr("Abnormal");
                }
                return tr("Invalidate");
            }
            case Column_Summary: {
                const auto &type = data.directive
                                       ->getValue()
                                       .hash("Parameters")
                                       .hash("Action")
                                       .hash("Type")
                                       .toString();
                if (Util::equal_insensitive(type, "contaminate", "contam", "uncontaminate",
                                            "clearcontam", "unit", "setunit", "unitreplace",
                                            "translate", "duplicate", "duplicatetranslate",
                                            "scriptdemultiplexer", "demultiplexer", "NOOP", "None",
                                            "AbnormalDataEpisode")) {
                    return QString();
                } else if (Util::equal_insensitive(type, "flowcorrection", "flowcalibration",
                                                   "spot", "spotsize", "multispot", "multispotsize",
                                                   "clapspot")) {
                    QString instrument(data.directive
                                           ->getValue()
                                           .hash("Parameters")
                                           .hash("Action").hash("Instrument").toQString());
                    if (!instrument.isEmpty())
                        return instrument;
                    return QString();
                } else if (Util::equal_insensitive(type, "serial", "setserial")) {
                    QString instrument(data.directive
                                           ->getValue()
                                           .hash("Parameters")
                                           .hash("Action").hash("Instrument").toQString());
                    if (!instrument.isEmpty())
                        return instrument;
                }

                auto selConfig = data.directive
                                     ->getValue()
                                     .hash("Parameters")
                                     .hash("Action")
                                     .hash("Selection");
                if (!selConfig.exists())
                    return QVariant();
                SequenceMatch::Composite selection(selConfig);
                SequenceName::ComponentSet matched;
                for (const auto &var : display->available.variables) {
                    if (!selection.matches(SequenceName(display->loadedStation, "raw", var)))
                        continue;
                    auto suffix = Util::suffix(var, '_');
                    if (suffix.empty())
                        continue;
                    matched.insert(std::move(suffix));
                }
                auto sorted = Util::to_qstringlist(matched);
                std::sort(sorted.begin(), sorted.end());
                return sorted.join(" ");
            }
            case Column_Comment:
                return data.directive->getValue().hash("Comment").toQString();
            default:
                break;
            }
            break;

        case Qt::EditRole:
            switch (index.column()) {
            case Column_Limits:
                return buildLimitsString(data.directive->getStart(), data.directive->getEnd(),
                                         false);
            case Column_Profile:
                return data.directive->getProfile();
            case Column_Author:
                return data.directive->getValue().hash("Author").toQString();
            case Column_Action:
                return QVariant();
            case Column_Summary:
                return QVariant();
            case Column_Comment:
                return data.directive->getValue().hash("Comment").toQString();
            default:
                break;
            }
            break;

        case Qt::FontRole:
            switch (index.column()) {
            case Column_Limits: {
                QFont font;
                font.setFamily("Monospace");
                font.setStyleHint(QFont::TypeWriter);
                /* font.setBold(true); */
                return font;
            }
            default:
                break;
            }
            break;
        default:
            break;
        }
        return QVariant();
    }

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const
    {
        if (role != Qt::DisplayRole)
            return QVariant();
        if (orientation == Qt::Vertical)
            return QVariant(section + 1);

        switch (section) {
        case Column_Limits:
            return tr("Limits");
        case Column_Profile:
            return tr("Profile");
        case Column_Author:
            return tr("Author");
        case Column_Action:
            return tr("Action");
        case Column_Summary:
            return tr("Summary");
        case Column_Comment:
            return tr("Comment");
        default:
            break;
        }
        return QVariant();
    }

    virtual bool setData(const QModelIndex &index, const QVariant &value, int role)
    {
        if (role != Qt::EditRole)
            return false;

        if (index.row() >= displayedDirectives.size())
            return false;
        DisplayData data(displayedDirectives.at(index.row()));

        switch (index.column()) {
        case Column_Limits: {
            QString str(value.toString().trimmed());
            if (!str.isEmpty()) {
                QStringList list(str.split(' ', QString::SkipEmptyParts));

                TimeParseNOOPListHandler handler;
                try {
                    Time::Bounds bounds = TimeParse::parseListBounds(list, &handler, false, true);
                    data.directive->setStart(bounds.start);
                    data.directive->setEnd(bounds.end);
                } catch (TimeParsingException tpe) {
                    return false;
                }
            } else {
                return false;
            }
            emit dataChanged(index, index);
            return true;
        }
        case Column_Profile: {
            QString str(value.toString().trimmed().toLower());
            if (str.isEmpty())
                return false;
            if (str != data.directive->getProfile()) {
                data.directive->setProfile(str);
                displayedDirectives[index.row()].isOtherProfile = str != display->loadedProfile;
                emit dataChanged(this->index(index.row(), 0),
                                 this->index(index.row(), Column_TOTAL - 1));
            }
            return true;
        }
        case Column_Author: {
            QString str(value.toString().trimmed());
            if (str.isEmpty())
                return false;
            data.directive->getValue().hash("Author").setString(str);
            emit dataChanged(index, index);
            return true;
        }
        case Column_Comment: {
            QString str(value.toString());
            if (str.isEmpty())
                return false;
            data.directive->getValue().hash("Comment").setString(str);
            emit dataChanged(index, index);
            return true;
        }
        default:
            break;
        }

        return false;
    }

    virtual Qt::ItemFlags flags(const QModelIndex &index) const
    {
        if (!index.isValid())
            return 0;

        switch (index.column()) {
        case Column_Limits:
        case Column_Profile:
        case Column_Author:
        case Column_Comment:
            return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
        case Column_Action:
        case Column_Summary:
            return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
        default:
            break;
        }

        return 0;
    }

    virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        Q_ASSERT(row + count <= displayedDirectives.size());

        beginRemoveRows(parent, row, row + count - 1);
        for (int i = 0; i < count; i++) {
            /* If it was never actually created just remove it without
             * any record of existence */
            if (!displayedDirectives[row].directive->getOriginal().isValid()) {
                for (QList<DirectivePointer>::iterator check = allDirectives.begin();
                        check != allDirectives.end();) {
                    if (*check != displayedDirectives[row].directive) {
                        ++check;
                        continue;
                    }
                    check = allDirectives.erase(check);
                }

            } else {
                displayedDirectives[row].directive->getValue().hash("Disabled").setBool(true);
            }
            displayedDirectives.removeAt(row);
        }
        endRemoveRows();

        return true;
    }

    void undeleteRow(int row)
    {
        Q_ASSERT(row >= 0 && row < displayedDirectives.size());

        displayedDirectives[row].directive->getValue().hash("Disabled").setBool(false);
        displayedDirectives[row].isDeleted = false;

        emit dataChanged(index(row, 0), index(row, Column_TOTAL - 1));
    }

    virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        QList<DirectivePointer> pending;
        for (int i = 0; i < count; i++) {
            pending.append(std::make_shared<DirectiveContainer>(
                    SequenceValue(SequenceName(display->loadedStation, "edits", {}),
                                  Variant::Root(), display->selectedStart, display->selectedEnd)));
            pending.last()->setProfile(display->loadedProfile);
        }
        allDirectives.append(pending);

        beginInsertRows(parent, row, row + count - 1);
        for (int i = 0; i < count; i++, row++) {
            displayedDirectives.insert(row, buildData(pending.at(i)));
        }
        endInsertRows();

        return true;
    }

    bool insertRows(int row, int count, const DirectiveContainer &original)
    {
        QList<DirectivePointer> pending;
        for (int i = 0; i < count; i++) {
            pending.append(std::make_shared<DirectiveContainer>(
                    SequenceName(display->loadedStation, "edits",
                                 display->loadedProfile.toStdString()),
                    original));
        }
        allDirectives.append(pending);

        beginInsertRows(QModelIndex(), row, row + count - 1);
        for (int i = 0; i < count; i++, row++) {
            displayedDirectives.insert(row, buildData(pending.at(i)));
        }
        endInsertRows();

        return true;
    }

    static inline bool compareLimitsAscending(const DisplayData &a, const DisplayData &b)
    {
        return Range::compareStart(a.directive->getStart(), b.directive->getStart()) < 0;
    }

    static inline bool compareLimitsDescending(const DisplayData &a, const DisplayData &b)
    {
        return Range::compareStart(a.directive->getStart(), b.directive->getStart()) > 0;
    }

    static inline bool compareProfileAscending(const DisplayData &a, const DisplayData &b)
    {
        return a.directive->getProfile() < b.directive->getProfile();
    }

    static inline bool compareProfileDescending(const DisplayData &a, const DisplayData &b)
    {
        return a.directive->getProfile() > b.directive->getProfile();
    }

    static inline bool compareAuthorAscending(const DisplayData &a, const DisplayData &b)
    {
        return a.directive->getValue()["Author"].toString() <
                b.directive->getValue()["Author"].toString();
    }

    static inline bool compareAuthorDescending(const DisplayData &a, const DisplayData &b)
    {
        return a.directive->getValue()["Author"].toString() >
                b.directive->getValue()["Author"].toString();
    }

    static inline bool compareCommentAscending(const DisplayData &a, const DisplayData &b)
    {
        return a.directive->getValue()["Comment"].toString() <
                b.directive->getValue()["Comment"].toString();
    }

    static inline bool compareCommentDescending(const DisplayData &a, const DisplayData &b)
    {
        return a.directive->getValue()["Comment"].toString() >
                b.directive->getValue()["Comment"].toString();
    }

    static inline bool compareActionAscending(const DisplayData &a, const DisplayData &b)
    {
        return a.directive->getValue()["Parameters/Action/Type"].toString() <
                b.directive->getValue()["Parameters/Action/Type"].toString();
    }

    static inline bool compareActionDescending(const DisplayData &a, const DisplayData &b)
    {
        return a.directive->getValue()["Parameters/Action/Type"].toString() >
                b.directive->getValue()["Parameters/Action/Type"].toString();
    }

    virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder)
    {
        beginResetModel();
        switch (column) {
        case Column_Limits:
            std::stable_sort(displayedDirectives.begin(), displayedDirectives.end(),
                        order == Qt::AscendingOrder ? compareLimitsAscending
                                                    : compareLimitsDescending);
            break;
        case Column_Profile:
            std::stable_sort(displayedDirectives.begin(), displayedDirectives.end(),
                        order == Qt::AscendingOrder ? compareProfileAscending
                                                    : compareProfileDescending);
            break;
        case Column_Author:
            std::stable_sort(displayedDirectives.begin(), displayedDirectives.end(),
                        order == Qt::AscendingOrder ? compareAuthorAscending
                                                    : compareAuthorDescending);
            break;
        case Column_Action:
            std::stable_sort(displayedDirectives.begin(), displayedDirectives.end(),
                        order == Qt::AscendingOrder ? compareActionAscending
                                                    : compareActionDescending);
            break;
        case Column_Summary:
            break;
        case Column_Comment:
            std::stable_sort(displayedDirectives.begin(), displayedDirectives.end(),
                        order == Qt::AscendingOrder ? compareCommentAscending
                                                    : compareCommentDescending);
            break;
        }
        endResetModel();
    }

    void updateDirectives(const QList<DirectiveContainer> &input)
    {
        beginResetModel();
        allDirectives.clear();
        displayedDirectives.clear();

        for (QList<DirectiveContainer>::const_iterator add = input.constBegin(),
                endAdd = input.constEnd(); add != endAdd; ++add) {
            allDirectives.append(DirectivePointer(new DirectiveContainer(*add)));
        }

        buildDisplayed();
        endResetModel();
    }

    void displayFilterChanged()
    {
        beginResetModel();
        buildDisplayed();
        endResetModel();
    }

    void availableChanged()
    {
        if (displayedDirectives.isEmpty())
            return;
        emit dataChanged(index(0, Column_Summary),
                         index(displayedDirectives.size() - 1, Column_Summary));
    }

    void directiveUpdated(const DirectivePointer &directive)
    {
        for (int row = 0, max = displayedDirectives.size(); row < max; row++) {
            if (displayedDirectives.at(row).directive == directive) {
                emit dataChanged(index(row, 0), index(row, Column_TOTAL - 1));
                break;
            }
        }
    }
};

EditDirectiveDisplay::EditDirectiveDisplay(GUIMainWindow *win, Qt::WindowFlags f) : QDialog(win, f),
                                                                                    mainWindow(win),
                                                                                    editor(NULL),
                                                                                    selectedStart(
                                                                                            FP::undefined()),
                                                                                    selectedEnd(
                                                                                            FP::undefined()),
                                                                                    loadedStart(
                                                                                            FP::undefined()),
                                                                                    loadedEnd(
                                                                                            FP::undefined())
{
    QVBoxLayout *topLayout = new QVBoxLayout;
    setLayout(topLayout);

    setWindowTitle(tr("Edit Directives"));

    QDialogButtonBox *buttonBox = new QDialogButtonBox(this);
    topLayout->addWidget(buttonBox);

    buttonBox->addButton(QDialogButtonBox::Save);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(saveChanges()));

    buttonBox->addButton(QDialogButtonBox::Cancel);
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(confirmReject()));

    QPushButton *button = buttonBox->addButton(tr("&Add"), QDialogButtonBox::ActionRole);
    button->setToolTip(tr("Add a new edit directive."));
    button->setStatusTip(tr("Add edit directive"));
    button->setWhatsThis(
            tr("This adds a new edit directive initialized to the whole selected range."));
    connect(button, SIGNAL(clicked(bool)), this, SLOT(addDirective()));

    removeButton = buttonBox->addButton(tr("&Remove"), QDialogButtonBox::ActionRole);
    removeButton->setEnabled(false);
    removeButton->setToolTip(tr("Remove the selected edit directive(s)."));
    removeButton->setStatusTip(tr("Remove selected"));
    removeButton->setWhatsThis(
            tr("This removes the selected edit directive(s).  Once removed they will no longer be in effect."));
    connect(removeButton, SIGNAL(clicked(bool)), this, SLOT(removeDirective()));

    modifyButton = buttonBox->addButton(tr("&Modify"), QDialogButtonBox::ActionRole);
    modifyButton->setEnabled(false);
    modifyButton->setToolTip(tr("Modify the parameters of the selected edit directive."));
    modifyButton->setStatusTip(tr("Modify selected"));
    modifyButton->setWhatsThis(
            tr("This opens the detailed editor for the selected edit directive.  Only usable with a single edit directive selected."));
    connect(modifyButton, SIGNAL(clicked(bool)), this, SLOT(modifyDirective()));

    duplicateButton = buttonBox->addButton(tr("D&uplicate"), QDialogButtonBox::ActionRole);
    duplicateButton->setEnabled(false);
    duplicateButton->setToolTip(tr("Duplicate selected edit directive."));
    duplicateButton->setStatusTip(tr("Duplicate selected"));
    duplicateButton->setWhatsThis(
            tr("This creates a copy of the selected edit directive and opens the detailed editor on the copy.  Only usable with a single edit directive selected."));
    connect(duplicateButton, SIGNAL(clicked(bool)), this, SLOT(duplicateDirective()));

    button = buttonBox->addButton(tr("&Display"), QDialogButtonBox::ActionRole);
    button->setToolTip(tr("Table display settings."));
    button->setStatusTip(tr("Table display"));
    button->setWhatsThis(
            tr("This button displays a menu that allows the edit directives shown in the table to be changed."));

    QMenu *menu = new QMenu(button);
    button->setMenu(menu);

    displaySystemInternal = menu->addAction(tr("Display &system"));
    displaySystemInternal->setToolTip(tr("Enable the display of internal system edit directives."));
    displaySystemInternal->setStatusTip(tr("Display system edits"));
    displaySystemInternal->setWhatsThis(
            tr("When enabled, edit directives flagged as system internal are shown in the list.  They are normally not shown."));
    displaySystemInternal->setCheckable(true);
    displaySystemInternal->setChecked(false);
    connect(displaySystemInternal, SIGNAL(toggled(bool)), this, SLOT(displayUpdated()));

    displayAllProfiles = menu->addAction(tr("Display all &profiles"));
    displayAllProfiles->setToolTip(
            tr("Enable the display of edits not originating from the main profile."));
    displayAllProfiles->setStatusTip(tr("Display all edit profiles"));
    displayAllProfiles->setWhatsThis(
            tr("When enabled, edit directives from all profiles are shown.  Normally only edits from the current profile are visible."));
    displayAllProfiles->setCheckable(true);
    displayAllProfiles->setChecked(false);
    connect(displayAllProfiles, SIGNAL(toggled(bool)), this, SLOT(displayUpdated()));

    displayDeleted = menu->addAction(tr("Display &deleted"));
    displayDeleted->setToolTip(tr("Enable the display of edits that have been deleted."));
    displayDeleted->setStatusTip(tr("Display deleted edits"));
    displayDeleted->setWhatsThis(
            tr("When enabled, edit directives that have been deleted are shown in the table."));
    displayDeleted->setCheckable(true);
    displayDeleted->setChecked(false);
    connect(displayDeleted, SIGNAL(toggled(bool)), this, SLOT(displayUpdated()));


    model = new EditDirectiveTableModel(this);
    table = new QTableView(this);
    topLayout->addWidget(table);
    table->setModel(model);

    table->setToolTip(tr("The table of all edit directives."));
    table->setStatusTip(tr("Edit directives"));
    table->setWhatsThis(tr("This table represents all visible edit directives."));

    /* table->verticalHeader()->hide(); */
    table->setSelectionMode(QAbstractItemView::ExtendedSelection);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->setSortingEnabled(true);
    connect(table->selectionModel(), SIGNAL(selectionChanged(
                                                    const QItemSelection &, const QItemSelection &)),
            this, SLOT(selectedChanged()));

    table->horizontalHeader()->setSectionResizeMode(Column_Limits, QHeaderView::ResizeToContents);
    table->horizontalHeader()->setSectionResizeMode(Column_Profile, QHeaderView::ResizeToContents);
    table->horizontalHeader()->setSectionResizeMode(Column_Author, QHeaderView::ResizeToContents);
    table->horizontalHeader()->setSectionResizeMode(Column_Action, QHeaderView::ResizeToContents);
    table->horizontalHeader()->setSectionResizeMode(Column_Summary, QHeaderView::ResizeToContents);

    table->horizontalHeader()->setStretchLastSection(true);
    table->horizontalHeader()->setSectionResizeMode(Column_Comment, QHeaderView::Stretch);

    table->setItemDelegateForColumn(Column_Limits, new LimitsDelegate(table));

    table->setMinimumWidth(900);


    displayUpdated();
}

EditDirectiveDisplay::~EditDirectiveDisplay()
{
    if (auto l = activeLoad.lock()) {
        l->abort();
    }
}

static void mergeEditingSelections(Archive::Access &access,
                                   Archive::Access::SelectedComponents &result,
                                   double loadStart,
                                   double loadEnd, const SequenceName::Component &station,
                                   const QString &profile)
{
    auto config = ValueSegment::Stream::read(
            Archive::Selection(loadStart, loadEnd, {station}, {"configuration"}, {"editing"}),
            &access);

    /* This doesn't handle filter input requests, but that would require
     * instantiating the whole chain, so we'll just avoid that. */

    Archive::Selection::List inputSelections;
    SequenceName::ComponentSet additionalVariables;
    for (const auto &segment : config) {
        auto cfg = segment.getValue().hash("Profiles").hash(profile);
        Util::append(SequenceMatch::Composite(cfg["Output/Data"]).toArchiveSelections({station},
                                                                                      {"raw"}),
                     inputSelections);
        Util::append(
                SequenceMatch::Composite(cfg["Input/Data"]).toArchiveSelections({station}, {"raw"}),
                inputSelections);
        Util::append(SequenceMatch::Composite(cfg["Display/Data"]).toArchiveSelections({station},
                                                                                       {"raw"}),
                     inputSelections);
        for (const auto &add : cfg["Display/AdditionalVariables"].toFlags()) {
            additionalVariables.insert(add);
        }
    }
    for (auto &sel : inputSelections) {
        sel.start = loadStart;
        sel.end = loadEnd;
    }
    if (inputSelections.empty()) {
        result.variables = access.availableVariables();
        result.flavors = access.availableFlavors();
    } else {
        auto selected = access.availableSelected(inputSelections);
        result.variables = std::move(selected.variables);
        if (result.variables.empty()) {
            result.variables = access.availableVariables();
        }
        result.flavors = std::move(selected.flavors);
        if (result.flavors.empty()) {
            result.flavors = access.availableFlavors();
        }
    }
    Util::merge(std::move(additionalVariables), result.variables);
}

EditDirectiveDisplay::LoadContext::LoadContext(EditDirectiveDisplay *dlg) : dialog(dlg),
                                                                            access(std::make_shared<
                                                                                    Archive::Access>()),
                                                                            lock(std::make_shared<
                                                                                    Archive::Access::ReadLock>(
                                                                                    *access)),
                                                                            mutex(),
                                                                            aborted(false)
{ }

EditDirectiveDisplay::LoadContext::~LoadContext()
{
    lock.reset();
    access.reset();
}

void EditDirectiveDisplay::LoadContext::abort()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        aborted = true;
    }
    access->signalTerminate();
    progress.disconnect();
}

bool EditDirectiveDisplay::LoadContext::isAborted()
{
    std::lock_guard<std::mutex> lock(mutex);
    return aborted;
}

void EditDirectiveDisplay::load(const SequenceName::Component &station,
                                const QString &profile,
                                double start,
                                double end)
{
    loadedStation = station;
    loadedProfile = profile;
    loadedStart = start;
    loadedEnd = end;

    if (!FP::defined(selectedStart))
        selectedStart = start;
    if (!FP::defined(selectedEnd))
        selectedEnd = end;

    if (auto l = activeLoad.lock()) {
        l->abort();
    }
    activeLoad.reset();


    QProgressDialog progress(tr("Loading edit directives."), tr("Cancel"), 0, 1000, this, 0);
    progress.setWindowModality(Qt::ApplicationModal);
    progress.setValue(0);

    {
        auto context = std::make_shared<LoadContext>(this);
        activeLoad = context;
        context->progress.connect(&progress, [&progress](int v) {
            progress.setValue(v);
        }, true);

        Threading::pollFuture(this, std::async(std::launch::async, [=]() {
            Archive::Access::SelectedComponents result;
            result.stations = context->access->availableStations();
            if (context->isAborted())
                return result;
            result.archives = context->access->availableArchives();
            if (context->isAborted())
                return result;
            result.archives.insert("avg");
            result.archives.insert("cont");

            mergeEditingSelections(*context->access, result, start, end, station, profile);

            return result;
        }), [context](const Archive::Access::SelectedComponents &result) {
            if (context->isAborted())
                return;
            if (!context->dialog)
                return;

            context->dialog->available = result;

            context->dialog->model->availableChanged();

            if (context->dialog->editor) {
                context->dialog->editor->setAvailableStations(result.stations);
                context->dialog->editor->setAvailableArchives(result.archives);
                context->dialog->editor->setAvailableVariables(result.variables);
                context->dialog->editor->setAvailableFlavors(result.flavors);
            }
        });


        Threading::pollFuture(this, std::async(std::launch::async, [=]() {
            QList<DirectiveContainer> result;

            StreamSink::Iterator data;
            auto read = context->access
                               ->readStream(Archive::Selection(start, end, {station},
                                                               {"edits"}).withMetaArchive(false),
                                            &data);
            while (data.hasNext()) {
                if (context->isAborted()) {
                    data.abort();
                    read->signalTerminate();
                    break;
                }
                auto value = data.next();

                double checkStart = value.getStart();
                if (!FP::defined(checkStart) ||
                        !FP::defined(start) ||
                        !FP::defined(end) ||
                        checkStart < start) {
                    context->progress(0);
                } else if (checkStart >= end) {
                    context->progress(999);
                } else {
                    int pv = (int) ((checkStart - start) / (end - start));
                    if (pv < 0)
                        pv = 0;
                    else if (pv > 999)
                        pv = 999;
                    context->progress(pv);
                }

                result.push_back(DirectiveContainer(value));
            }
            read->wait();

            return result;
        }), [context, &progress](const QList<DirectiveContainer> &result) {
            if (context->isAborted())
                return;
            context->dialog->model->updateDirectives(result);
            progress.reset();
        });
    }

    /* Safe because the complete signal cannot be delivered before we re-enter the
     * event loop, so even if the load has finished, the reset() is still
     * pending */
    progress.exec();

    if (progress.wasCanceled()) {
        if (auto l = activeLoad.lock()) {
            l->abort();
        }
        activeLoad.reset();
        hide();
        return;
    }
}

static void allocatePriority(SequenceValue &value,
                             const std::unordered_map<SequenceIdentity, SequenceValue> &updates,
                             const SequenceValue::Transfer &inserts,
                             Archive::Access &access)
{
    bool hit = false;
    std::int_fast32_t maxHit = 0;
    std::int_fast32_t minHit = 0;
    {
        double loadStart = value.getStart();
        double loadEnd;
        if (!FP::defined(loadStart))
            loadEnd = 1.0;
        else
            loadEnd = loadStart + 1.0;

        StreamSink::Iterator data;
        access.readStream(Archive::Selection(value.getUnit(), loadStart, loadEnd), &data)->detach();
        while (data.hasNext()) {
            auto check = data.next();

            if (!FP::equal(check.getStart(), value.getStart()) ||
                    !FP::equal(check.getEnd(), value.getEnd()))
                continue;
            hit = true;

            maxHit = std::max<std::int_fast32_t>(maxHit, check.getPriority());
            minHit = std::min<std::int_fast32_t>(minHit, check.getPriority());
        }
    }

    for (const auto &check : inserts) {
        if (&check == &value)
            continue;
        if (check.getUnit() != value.getUnit())
            continue;
        if (!FP::equal(check.getStart(), value.getStart()) ||
                !FP::equal(check.getEnd(), value.getEnd()))
            continue;
        hit = true;

        maxHit = std::max<std::int_fast32_t>(maxHit, check.getPriority());
        minHit = std::min<std::int_fast32_t>(minHit, check.getPriority());
    }

    for (const auto &check : updates) {
        if (&(check.second) == &value)
            continue;
        if (check.second.getUnit() != value.getUnit())
            continue;
        if (!FP::equal(check.second.getStart(), value.getStart()) ||
                !FP::equal(check.second.getEnd(), value.getEnd()))
            continue;
        hit = true;

        maxHit = std::max<std::int_fast32_t>(maxHit, check.second.getPriority());
        minHit = std::min<std::int_fast32_t>(minHit, check.second.getPriority());
    }

    if (!hit)
        return;

    if (maxHit < static_cast<std::int_fast32_t>(2147483647L)) {
        value.setPriority(maxHit + 1);
        return;
    }
    if (minHit > static_cast<std::int_fast32_t>(-2147483647L)) {
        value.setPriority(minHit - 1);
        return;
    }

    qCWarning(log_gui_editdirective) << "Exhausted edit priorities trying to write" << value;

    double loadStart = value.getStart();
    double loadEnd;
    if (!FP::defined(loadStart))
        loadEnd = 1.0;
    else
        loadEnd = loadStart + 1.0;

    /* Stupid, but this should never happen either */

    for (qint32 p = 1; p != 0; p++) {
        hit = false;

        for (const auto &check : inserts) {
            if (&check == &value)
                continue;
            if (check.getUnit() != value.getUnit())
                continue;
            if (!FP::equal(check.getStart(), value.getStart()) ||
                    !FP::equal(check.getEnd(), value.getEnd()))
                continue;
            if (check.getPriority() != p)
                continue;
            hit = true;
            break;
        }
        if (hit)
            continue;

        for (const auto &check : updates) {
            if (&(check.second) == &value)
                continue;
            if (check.second.getUnit() != value.getUnit())
                continue;
            if (!FP::equal(check.second.getStart(), value.getStart()) ||
                    !FP::equal(check.second.getEnd(), value.getEnd()))
                continue;
            if (check.second.getPriority() != p)
                continue;
            hit = true;
            break;
        }
        if (hit)
            continue;

        StreamSink::Iterator data;
        access.readStream(Archive::Selection(value.getUnit(), loadStart, loadEnd), &data)->detach();
        while (data.hasNext()) {
            auto check = data.next();

            if (!FP::equal(check.getStart(), value.getStart()) ||
                    !FP::equal(check.getEnd(), value.getEnd()))
                continue;
            if (check.getPriority() != p)
                continue;
            hit = true;
            break;
        }
        if (hit)
            continue;

        value.setPriority(p);
        return;
    }

    qFatal("No possible priority for directive");
}

void EditDirectiveDisplay::writeChanges()
{
    std::unordered_map<SequenceIdentity, SequenceValue> updates;
    SequenceValue::Transfer inserts;

    for (QList<DirectivePointer>::const_iterator directive = model->allDirectives.constBegin(),
            end = model->allDirectives.constEnd(); directive != end; ++directive) {
        bool changed = false;
        auto result = (*directive)->commit(&changed);
        if (!changed)
            continue;

        auto original = (*directive)->getOriginal();
        if (!original.isValid()) {
            if (result.getVariable().empty())
                result.setVariable(loadedProfile.toStdString());
            Q_ASSERT(result.isValid());
            inserts.emplace_back(std::move(result));
        } else {
            Q_ASSERT(result.isValid());
            updates.emplace(std::move(original), std::move(result));
        }
    }

    /* So we know we've got to reload and to make sure we're not keeping
     * references to values that we might change */
    model->updateDirectives(QList<DirectiveContainer>());

    if (updates.empty() && inserts.empty())
        return;

    QProgressDialog
            progress(tr("Writing edit directives to the database."), tr("Cancel"), 0, 0, this, 0);
    progress.setWindowModality(Qt::ApplicationModal);
    progress.setValue(0);

    Threading::Signal<> complete;
    complete.connect(&progress, "reset");

    std::mutex mutex;
    bool aborted = false;
    auto thread = std::thread([&] {
        Archive::Access access;
        for (;;) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                if (aborted)
                    break;
            }

            Archive::Access::WriteLock lock(access);
            SequenceValue::Transfer modify;
            SequenceIdentity::Transfer remove;

            for (auto &u : updates) {
                if (u.first != u.second) {
                    allocatePriority(u.second, updates, inserts, access);
                    modify.push_back(u.second);
                    remove.push_back(u.first);
                } else {
                    u.second.setPriority(u.first.getPriority());
                    modify.push_back(u.second);
                }
            }
            for (auto &i : inserts) {
                allocatePriority(i, updates, inserts, access);
                modify.push_back(i);
            }

            access.writeSynchronous(modify, remove);

            if (lock.commit())
                break;

            for (auto &u : updates) {
                u.second.setPriority(0);
            }
            for (auto &i : inserts) {
                i.setPriority(0);
            }
        }
        complete();
    });

    /* Safe because the complete signal cannot be delivered before we re-enter the
     * event loop, so even if the thread has already exited, the reset() is still
     * pending */
    progress.exec();

    if (progress.wasCanceled()) {
        std::lock_guard<std::mutex> lock(mutex);
        aborted = true;
    }
    thread.join();
}

void EditDirectiveDisplay::saveChanges()
{
    if (mainWindow != NULL)
        mainWindow->highlightResetMentorEdit();
    writeChanges();
    accept();
}

int EditDirectiveDisplay::closing()
{
    bool changed = false;
    for (QList<DirectivePointer>::const_iterator directive = model->allDirectives.constBegin(),
            end = model->allDirectives.constEnd(); directive != end; ++directive) {
        if ((*directive)->changed()) {
            changed = true;
            break;
        }
    }

    if (changed) {
        QMessageBox box;
        box.setText(tr("The edit directives have been modified."));
        box.setInformativeText(tr("Do you want to save your changes?"));
        box.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        box.setDefaultButton(QMessageBox::Save);
        int sel = box.exec();
        switch (sel) {
        case QMessageBox::Save:
            writeChanges();
            return -1;
        case QMessageBox::Discard:
            /* So we don't prompt again */
            model->allDirectives.clear();
            break;
        case QMessageBox::Cancel:
            return 1;
        default:
            break;
        }
    }

    return 0;
}

bool EditDirectiveDisplay::mainWindowClosing()
{
    switch (closing()) {
    default:
        break;
    case 1:
        return false;
    case -1:
        if (mainWindow != NULL)
            mainWindow->highlightResetMentorEdit();
        emit accepted();
        setVisible(false);
        return true;
    }

    if (mainWindow != NULL)
        mainWindow->highlightResetMentorEdit();
    setVisible(false);
    return true;
}

void EditDirectiveDisplay::closeEvent(QCloseEvent *event)
{
    switch (closing()) {
    default:
        break;
    case 1:
        event->ignore();
        return;
    case -1:
        if (mainWindow != NULL)
            mainWindow->highlightResetMentorEdit();
        emit accepted();
        return;
    }

    if (mainWindow != NULL)
        mainWindow->highlightResetMentorEdit();
    QDialog::closeEvent(event);
}

void EditDirectiveDisplay::confirmReject()
{
    switch (closing()) {
    default:
        break;
    case 1:
        return;
    case -1:
        if (mainWindow != NULL)
            mainWindow->highlightResetMentorEdit();
        accept();
        return;
    }
    if (mainWindow != NULL)
        mainWindow->highlightResetMentorEdit();
    reject();
}

bool EditDirectiveDisplay::setSelectedTimes(double start, double end)
{
    selectedStart = start;
    selectedEnd = end;
    if (editor != NULL) {
        editor->setSelectedTimes(start, end);
        if (editor->isVisible())
            return true;
    }
    return false;
}

void EditDirectiveDisplay::showEditor(const DirectivePointer &directive,
                                      bool removeOnCancel,
                                      bool isNew)
{
    if (editor == NULL) {
        editor = new EditDirectiveSingleEditor(this);
        connect(editor, SIGNAL(changed(
                                       const DirectivePointer &)), this, SLOT(directiveUpdated(
                                                                                      const DirectivePointer &)));
        connect(editor, SIGNAL(accepted()), this, SLOT(highlightSelection()));
        connect(editor, SIGNAL(rejected()), this, SLOT(highlightSelection()));

        /* OSX workaround since the editing dialog likes to make this go behind the main window */
        connect(editor, SIGNAL(accepted()), this, SLOT(refocusDialog()));
        connect(editor, SIGNAL(rejected()), this, SLOT(refocusDialog()));

        editor->setAvailableStations(available.stations);
        editor->setAvailableArchives(available.archives);
        editor->setAvailableVariables(available.variables);
        editor->setAvailableFlavors(available.flavors);
    }
    disconnect(editor, SIGNAL(reverted(
                                      const DirectivePointer &)), this, SLOT(directiveRejected(
                                                                                     const DirectivePointer &)));
    if (removeOnCancel) {
        connect(editor, SIGNAL(reverted(
                                       const DirectivePointer &)), this, SLOT(directiveRejected(
                                                                                      const DirectivePointer &)));
    }
    editor->configure(directive);
    editor->show();
}

void EditDirectiveDisplay::directiveUpdated(const DirectivePointer &directive)
{
    model->directiveUpdated(directive);

    /* So that the bounds get updated if edited by user */
    highlightSelection();
}

void EditDirectiveDisplay::directiveRejected(const DirectivePointer &directive)
{
    for (int row = model->displayedDirectives.size() - 1; row >= 0; --row) {
        if (model->displayedDirectives.at(row).directive != directive)
            continue;
        model->removeRows(row, 1);
    }
}

void EditDirectiveDisplay::addDirective()
{
    int row = 0;
    model->insertRows(row, 1);
    table->selectionModel()
         ->select(model->index(row, 0),
                  QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
    showEditor(model->displayedDirectives.at(row).directive, true, true);
}

void EditDirectiveDisplay::removeDirective()
{
    switch (deletedSelected()) {
    default: {
        QList<int> rows;
        QList<QModelIndex> selectedRows(table->selectionModel()->selectedRows());
        for (QList<QModelIndex>::const_iterator index = selectedRows.constBegin(),
                end = selectedRows.constEnd(); index != end; ++index) {
            rows.append(index->row());
        }
        std::sort(rows.begin(), rows.end());
        for (int i = rows.size() - 1; i >= 0; i--) {
            model->removeRows(rows.at(i), 1);
        }
        break;
    }
    case 1: {
        QList<QModelIndex> selectedRows(table->selectionModel()->selectedRows());
        for (QList<QModelIndex>::const_iterator index = selectedRows.constBegin(),
                end = selectedRows.constEnd(); index != end; ++index) {
            int row = index->row();
            if (row < 0 || row >= model->displayedDirectives.size())
                continue;
            model->undeleteRow(row);
        }
        selectedChanged();
        break;
    }
    }
}

void EditDirectiveDisplay::modifyDirective()
{
    QModelIndexList selected(table->selectionModel()->selectedRows());
    if (selected.size() != 1)
        return;
    showEditor(model->displayedDirectives.at(selected.first().row()).directive);
}

void EditDirectiveDisplay::duplicateDirective()
{
    QModelIndexList selected(table->selectionModel()->selectedRows());
    if (selected.size() != 1)
        return;
    DirectivePointer original(model->displayedDirectives.at(selected.first().row()).directive);

    int row = 0;
    model->insertRows(row, 1, *original);
    table->selectionModel()
         ->select(model->index(row, 0),
                  QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);

    showEditor(model->displayedDirectives.at(row).directive, true);
}

void EditDirectiveDisplay::displayUpdated()
{
    model->displayFilterChanged();
    table->setColumnHidden(Column_Profile, !displayAllProfiles->isChecked());
}

int EditDirectiveDisplay::deletedSelected()
{
    bool hadDeleted = false;
    bool hadNormal = false;
    QList<QModelIndex> selectedRows(table->selectionModel()->selectedRows());
    for (QList<QModelIndex>::const_iterator index = selectedRows.constBegin(),
            end = selectedRows.constEnd(); index != end; ++index) {
        int row = index->row();
        if (row < 0 || row >= model->displayedDirectives.size())
            continue;
        if (model->displayedDirectives.at(row).isDeleted) {
            if (hadNormal)
                return -1;
            hadDeleted = true;
        } else {
            if (hadDeleted)
                return -1;
            hadNormal = true;
        }
    }
    return hadDeleted ? 1 : 0;
}

void EditDirectiveDisplay::highlightSelection()
{
    if (mainWindow == NULL)
        return;

    mainWindow->highlightResetMentorEdit();
    QModelIndexList selected(table->selectionModel()->selectedRows());
    for (QList<QModelIndex>::const_iterator index = selected.constBegin(),
            end = selected.constEnd(); index != end; ++index) {
        int row = index->row();
        if (row < 0 || row >= model->displayedDirectives.size())
            continue;
        addDirectiveHighlight(mainWindow, model->displayedDirectives.at(row).directive);
    }
}

void EditDirectiveDisplay::refocusDialog()
{
    raise();
    activateWindow();
}

void EditDirectiveDisplay::selectedChanged()
{
    QModelIndexList selected(table->selectionModel()->selectedRows());
    modifyButton->setEnabled(selected.size() == 1);
    duplicateButton->setEnabled(selected.size() == 1);

    switch (deletedSelected()) {
    case 0:
        removeButton->setEnabled(selected.size() > 0);
        removeButton->setText(tr("&Remove"));
        break;
    case 1:
        removeButton->setEnabled(selected.size() > 0);
        removeButton->setText(tr("&Un-delete"));
        break;
    default:
        removeButton->setEnabled(false);
        removeButton->setText(tr("&Remove"));
        break;
    }

    highlightSelection();
}


EditDirectiveSingleEditor::EditDirectiveSingleEditor(EditDirectiveDisplay *parent,
                                                     Qt::WindowFlags f) : QDialog(parent, f),
                                                                          display(parent),
                                                                          originalStart(
                                                                                  FP::undefined()),
                                                                          originalEnd(
                                                                                  FP::undefined()),
                                                                          originalValue()
{
    QVBoxLayout *topLayout = new QVBoxLayout;
    setLayout(topLayout);

    setWindowTitle(tr("Edit Directive Parameters"));

    bounds = new TimeBoundSelection(this);
    bounds->setAllowUndefinedStart(true);
    bounds->setAllowUndefinedEnd(true);
    topLayout->addWidget(bounds);
    connect(bounds, SIGNAL(boundsEdited(double, double)), this, SLOT(changeBounds(double, double)));

    parameters = new EditDirectiveEditor(this);
    topLayout->addWidget(parameters);
    connect(parameters, SIGNAL(changed()), this, SLOT(changeParameters()));

    QDialogButtonBox *buttonBox = new QDialogButtonBox(this);
    topLayout->addWidget(buttonBox);
    buttonBox->addButton(QDialogButtonBox::Ok);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    buttonBox->addButton(QDialogButtonBox::Cancel);
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}

void EditDirectiveSingleEditor::reject()
{
    directive->setStart(originalStart);
    directive->setEnd(originalEnd);
    directive->getValue().set(originalValue);
    emit changed(directive);
    emit reverted(directive);
    QDialog::reject();
}

void EditDirectiveSingleEditor::configure(const DirectivePointer &directive)
{
    this->directive = directive;
    originalStart = directive->getStart();
    originalEnd = directive->getEnd();
    originalValue = Variant::Root(directive->getValue());

    bounds->setBounds(originalStart, originalEnd);

    parameters->disconnect(this);
    parameters->configure(directive->getValue());
    connect(parameters, SIGNAL(changed()), this, SLOT(changeParameters()));
}

void EditDirectiveSingleEditor::changeBounds(double start, double end)
{
    directive->setStart(start);
    directive->setEnd(end);
    emit changed(directive);
}

void EditDirectiveSingleEditor::changeParameters()
{
    emit changed(directive);
}

void EditDirectiveSingleEditor::setSelectedTimes(double start, double end)
{
    if (!isVisible())
        return;
    directive->setStart(start);
    directive->setEnd(end);
    bounds->setBounds(start, end);
    emit changed(directive);
}

void EditDirectiveSingleEditor::setAvailableStations(const CPD3::Data::SequenceName::ComponentSet &set)
{
    parameters->setAvailableStations(set);
}

void EditDirectiveSingleEditor::setAvailableArchives(const CPD3::Data::SequenceName::ComponentSet &set)
{
    parameters->setAvailableArchives(set);
}

void EditDirectiveSingleEditor::setAvailableVariables(const CPD3::Data::SequenceName::ComponentSet &set)
{
    parameters->setAvailableVariables(set);
}

void EditDirectiveSingleEditor::setAvailableFlavors(const CPD3::Data::SequenceName::ComponentSet &set)
{
    parameters->setAvailableFlavors(set);
}
