/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef GUICHAIN_H
#define GUICHAIN_H

#include "core/first.hxx"

#include <unordered_map>
#include <future>
#include <mutex>
#include <condition_variable>
#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QDialog>
#include <QButtonGroup>
#include <QVBoxLayout>
#include <QComboBox>
#include <QScrollArea>
#include <QLabel>
#include <QBuffer>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QMainWindow>
#include <QProgressBar>

#include "core/actioncomponent.hxx"
#include "core/component.hxx"
#include "datacore/streampipeline.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/archive/access.hxx"
#include "guidata/optionseditor.hxx"
#include "guidata/variableselect.hxx"
#include "guicore/timeboundselection.hxx"
#include "guicore/actionprogress.hxx"
#include "graphing/display.hxx"
#include "graphing/displaywidget.hxx"

struct GUIChainComponentLoadResult {
    std::unordered_map<QString, CPD3::ComponentLoader::Information> input;
    std::unordered_map<QString, CPD3::ComponentLoader::Information> filter;
    std::unordered_map<QString, CPD3::ComponentLoader::Information> output;

    GUIChainComponentLoadResult() = default;

    GUIChainComponentLoadResult(const GUIChainComponentLoadResult &) = default;

    GUIChainComponentLoadResult &operator=(const GUIChainComponentLoadResult &) = default;

    GUIChainComponentLoadResult(GUIChainComponentLoadResult &&) = default;

    GUIChainComponentLoadResult &operator=(GUIChainComponentLoadResult &&) = default;
};

class GUIChainDialog;

class GUIChainFilter : public QWidget {
Q_OBJECT

    GUIChainDialog *parent;
    CPD3::Data::ProcessingStageComponent *component;

    QPushButton *selectButton;
    QPushButton *moveUpButton;
    QPushButton *moveDownButton;
    QLabel *description;
    CPD3::GUI::Data::OptionEditor *options;

public:
    GUIChainFilter(CPD3::Data::ProcessingStageComponent *component,
                   const CPD3::ComponentLoader::Information &info,
                   GUIChainDialog *parent);

    void dataAvailable(const CPD3::Data::Archive::Access::SelectedComponents &available);

    inline bool isActive() const
    { return selectButton->isChecked(); }

    void setActive()
    { selectButton->setChecked(true); }

    void updateActive(int index, bool first, bool last);

    bool addToPipeline(CPD3::Data::StreamPipeline *chain);

    bool addToPipeline(CPD3::Data::StreamPipeline *chain,
                       double start,
                       double end,
                       CPD3::Data::SequenceName::Set &inputs);

private slots:

    void moveUp();

    void moveDown();

    void remove();
};

class GUIChainDialog : public QDialog {
Q_OBJECT

    CPD3::Data::SequenceName::Component station;
    CPD3::Data::SequenceName::Component archive;
    double start;
    double end;

    CPD3::Data::Archive::Access::SelectedComponents availableData;
    std::shared_future<GUIChainComponentLoadResult> availableComponents;

    enum {
        Input_Archive, Input_File, Input_Component, Input_Multiplexer
    };

    enum {
        Output_DataToFile, Output_Component, Output_Tabular, Output_Display,
    };

    QList<GUIChainFilter *> filters;

    friend class GUIChainFilter;

    QPushButton *inputSelectButton;
    QScrollArea *inputSelectPane;
    QComboBox *inputSelectType;

    QPushButton *outputSelectButton;
    QScrollArea *outputSelectPane;
    QComboBox *outputSelectType;

    QPushButton *addFilter;

    QButtonGroup *stagesGroup;
    QVBoxLayout *stagesLayout;

    struct ArchiveInput {
        QWidget *pane;
        CPD3::GUI::TimeBoundSelection *bounds;
        CPD3::GUI::Data::VariableSelect *selection;
        QAction *clip;
        QAction *addRequested;
        QAction *enableDefault;
        QAction *enableMeta;
        QAction *setModified;
    };
    ArchiveInput *archiveInput;

    struct FileInput {
        QWidget *pane;
        QLineEdit *target;
    };
    FileInput *fileInput;

    struct ComponentInput {
        QWidget *pane;
        QComboBox *select;
        QLabel *description;
        QWidget *basicPane;
        QWidget *filePane;
        QLineEdit *fileTarget;
        QWidget *timePane;
        QLabel *timeRequired;
        QCheckBox *timeOptional;
        CPD3::GUI::TimeBoundSelection *time;
        QWidget *singleStationPane;
        QComboBox *singleStation;
        QWidget *multipleStationPane;
        QMenu *multipleStationMenu;
        QAction *multipleStationSeparator;
        QActionGroup *multipleStations;
        CPD3::GUI::Data::OptionEditor *options;
    };
    ComponentInput *componentInput;

    struct MultiplexerInput {
        QWidget *pane;
        QVBoxLayout *layout;
        struct Input {
            GUIChainDialog *dialog;
            QWidget *pane;
            QPushButton *modify;
            QPushButton *remove;
        };
        QList<Input> inputs;
    };
    MultiplexerInput *multiplexerInput;

    struct TabularOutput {
        QWidget *pane;
    };
    TabularOutput *tabularOutput;

    struct DisplayOutput {
        QWidget *pane;

        QComboBox *type;
        CPD3::GUI::Data::OptionEditor *options;
    };
    DisplayOutput *displayOutput;

    struct FileOutput {
        QWidget *pane;
        QLineEdit *target;
    };
    FileOutput *fileOutput;

    struct ComponentOutput {
        QWidget *pane;
        QComboBox *select;
        QLabel *description;
        QWidget *filePane;
        QCheckBox *fileEnable;
        QLineEdit *fileTarget;
        QPushButton *fileSave;
        CPD3::GUI::Data::OptionEditor *options;
    };
    ComponentOutput *componentOutput;

    void setLoaded(const CPD3::Data::SequenceName::Component &station,
                   const CPD3::Data::SequenceName::Component &archive,
                   double start,
                   double end);

    void waitForComponents();

    bool configurePipeline(CPD3::Data::StreamPipeline *target,
                           QList<CPD3::Data::StreamPipeline *> &chains,
                           QList<CPD3::Data::SinkMultiplexer *> &multiplexers,
                           int &acknowledgedWarnings);

    void availableDataReady(const CPD3::Data::Archive::Access::SelectedComponents &available);

public:
    enum Mode {
        Mode_Export, Mode_ViewTabular, Mode_MultiplexerInput,
    };

    GUIChainDialog(Mode mode = Mode_Export, QWidget *parent = 0, Qt::WindowFlags flags = 0);

    virtual ~GUIChainDialog();

    virtual QSize sizeHint() const;

    void load(const CPD3::Data::SequenceName::Component &station,
              const CPD3::Data::SequenceName::Component &archive,
              double start,
              double end);

public slots:

    void updateSelected();

private slots:

    void availableComponentsReady();

    void completeDialog();

    void newFilter();

    void archiveShowSetModified();

    void inputFileShowSelection();

    void inputComponentSelected();

    void inputComponentFileShowSelection();

    void inputComponentTimeEnableChanged();

    void inputComponentAddStation();

    void inputMultiplexerAdd();

    void inputMultiplexerModify();

    void inputMultiplexerRemove();

    void outputFileShowSelection();

    void outputComponentSelected();

    void outputComponentFileShowSelection();

    void outputComponentFileEnableChanged();

    void outputDisplayTypeChanged();

    void inputTypeChanged();

    void outputTypeChanged();
};

class GUIChainOutputWorker : public QObject {
    GUIChainDialog *parent;

    QList<CPD3::Data::StreamPipeline *> chains;
    QList<CPD3::Data::SinkMultiplexer *> multiplexers;
    double start;
    double end;

    void abortAll();

    bool execProgress(CPD3::ActionFeedback::Source *source = nullptr);

public:
    GUIChainOutputWorker(GUIChainDialog *parent);

    virtual ~GUIChainOutputWorker();

    void setup(double start,
               double end, const QList<CPD3::Data::StreamPipeline *> &chains,
               const QList<CPD3::Data::SinkMultiplexer *> &multiplexers);

    bool execTabular();

    bool execOutputFile(const QString &fileName);

    bool execGeneral(std::unique_ptr<CPD3::Data::ExternalSink> &&output);

    bool execBuffer(std::unique_ptr<CPD3::Data::ExternalSink> &&output, QBuffer *buffer);

    bool execDisplay(CPD3::Graphing::Display *d,
                     const CPD3::Data::Variant::Read &config,
                     double start,
                     double end,
                     const CPD3::Data::SequenceName::Component &station,
                     const CPD3::Data::SequenceName::Component &archive);
};

class GUIChainSegmentOutputBuffer : public QObject, public CPD3::Data::StreamSink {
Q_OBJECT

    std::mutex mutex;
    std::condition_variable cond;
    bool started;
    bool finished;

    CPD3::Data::SequenceSegment::Stream reader;
public:
    CPD3::Data::SequenceSegment::Transfer segments;
    QSet<CPD3::Data::SequenceName> units;

    GUIChainSegmentOutputBuffer();

    virtual ~GUIChainSegmentOutputBuffer();

    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

    void incomingData(const CPD3::Data::SequenceValue &value) override;

    void incomingData(CPD3::Data::SequenceValue &&value) override;

    virtual void endData();

    void wait();

    CPD3::ActionFeedback::Source feedback;
};

class GUIChainComponentSelectionDialog : public QDialog {
Q_OBJECT

    std::unordered_map<QString, CPD3::ComponentLoader::Information> components;

    QComboBox *componentSelect;
    QLabel *descriptionLabel;
public:
    GUIChainComponentSelectionDialog(const std::unordered_map<QString,
                                                              CPD3::ComponentLoader::Information> &components,
                                     QWidget *parent = 0);

    QString getSelectedComponent() const;

private slots:

    void updateSelected();
};

class GUIChainBufferDialog : public QDialog {
Q_OBJECT

    QPlainTextEdit *text;
public:
    GUIChainBufferDialog(const QByteArray &buffer, QWidget *parent = 0);

    virtual ~GUIChainBufferDialog();

    virtual QSize sizeHint() const;

private slots:

    void save();
};

class GUIChainDisplayWindow : public QMainWindow {
Q_OBJECT

    double start;
    double end;
    CPD3::Data::SequenceName::Component station;
    CPD3::Data::SequenceName::Component archive;

    QSettings settings;

    CPD3::Data::Variant::Root baselineConfiguration;

    CPD3::Graphing::DisplayWidget *displayWidget;
    QTimer *repaintTimer;
    CPD3::GUI::ActionProgressStatusBar *progress;

    CPD3::Data::StreamPipeline *chain;
    CPD3::Graphing::DisplayWidgetTapChain *filterChain;
public:
    GUIChainDisplayWindow(CPD3::Graphing::Display *disp,
                          CPD3::Data::StreamPipeline *c,
                          const CPD3::Data::Variant::Read &config,
                          double s,
                          double e,
                          const CPD3::Data::SequenceName::Component &stn,
                          const CPD3::Data::SequenceName::Component &arc);

    virtual ~GUIChainDisplayWindow();

    virtual QSize sizeHint() const;

    void attachToChain();

public slots:

    void finished();

private slots:

    void chainReload();

    void saveChanges(CPD3::Data::ValueSegment::Transfer overlay);
};

#endif
