/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QStatusBar>
#include <QToolBar>
#include <QApplication>
#include <QStyle>
#include <QTimer>
#include <QCloseEvent>
#include <QVBoxLayout>
#include <QScrollBar>
#include <QPushButton>
#include <QUndoCommand>
#include <QUndoView>
#include <QToolButton>
#include <QWidgetAction>

#include "core/number.hxx"
#include "guicore/timeboundselection.hxx"
#include "guicore/dynamictimeinterval.hxx"
#include "guicore/guiformat.hxx"
#include "graphing/trace2d.hxx"
#include "graphing/bin2d.hxx"

#include "mainwindow.hxx"
#include "gui.hxx"
#include "interfacehandler.hxx"
#include "generichandler.hxx"
#include "displayhandler.hxx"

using namespace CPD3;
using namespace CPD3::GUI;
using namespace CPD3::Graphing;

GUIMainWindow::GUIMainWindow(CPD3GUI *g) : gui(g),
                                           inRealtimeMode(false),
                                           lastActivatedReplot(),
                                           initialReplot()
{
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    realtimeSystemFlags = new QLabel;
    realtimeSystemFlags->hide();
    statusBar()->addPermanentWidget(realtimeSystemFlags);

    realtimeSeparator = new QFrame;
    realtimeSeparator->setFrameStyle(QFrame::VLine);
    realtimeSeparator->hide();
    statusBar()->addPermanentWidget(realtimeSeparator);

    realtimeStatus = new QLabel;
    realtimeStatus->hide();
    statusBar()->addPermanentWidget(realtimeStatus);

    realtimeMessagesPending = new QPushButton;
    realtimeMessagesPending->hide();
    realtimeMessagesPending->setIcon(
            QApplication::style()->standardIcon(QStyle::SP_MessageBoxWarning, 0, statusBar()));
    realtimeMessagesPending->setFlat(true);
    realtimeMessagesPending->setStyleSheet("QPushButton { border-style: none }");
    realtimeMessagesPending->setFocusPolicy(Qt::NoFocus);
    realtimeMessagesPending->setToolTip(
            tr("This indicates that there are pending realtime messages."));
    realtimeMessagesPending->setStatusTip(tr("Realtime messages pending"));
    realtimeMessagesPending->setWhatsThis(
            tr("This icon indicates that there are realtime messages that have not been acknowledged.  Clicking on the icon will toggle the list of outstanding messages."));
    connect(realtimeMessagesPending, SIGNAL(clicked()), this, SLOT(realtimeMessagesShow()));
    statusBar()->addPermanentWidget(realtimeMessagesPending);

    progressBar = new QProgressBar(statusBar());
    progressBar->setMaximum(0);
    progressBar->setMinimum(0);
    progressBar->reset();
    progressBar->setValue(1);
    progressBar->setMinimumWidth(qMax(200, progressBar->minimumWidth()));
    progressBar->setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred));
    progressBar->hide();
    statusBar()->addPermanentWidget(progressBar);

    realtimeMessagesPending->setMaximumSize(progressBar->sizeHint().height(),
                                            progressBar->sizeHint().height());
    realtimeMessagesPending->setIconSize(
            QSize(progressBar->sizeHint().height() - 4, progressBar->sizeHint().height() - 4));

    statusLabel = new QLabel(statusBar());
    statusBar()->addWidget(statusLabel, 1);

    progress = new ActionProgressStatusBar(progressBar, statusLabel, this, gui->getSettings());

    timeToolBar = addToolBar(tr("View Selection"));
    timeToolBar->setContextMenuPolicy(Qt::PreventContextMenu);
    timeToolBar->setObjectName("timeToolBar");
    timeToolBar->setAllowedAreas(Qt::TopToolBarArea | Qt::BottomToolBarArea);

    timeRangeSelect = new TimeBoundSelection(timeToolBar);
    timeRangeSelect->setToolTip(tr("The time range of data selected."));
    timeRangeSelect->setStatusTip(tr("Set time range"));
    timeRangeSelect->setWhatsThis(
            tr("This is the start and end time of data selected.  When data is replotted the plots will change to this range of data."));
    timeRangeSelect->setDisableWindowFocus(true);
    timeRangeSelectAction = timeToolBar->addWidget(timeRangeSelect);
    timeRangeSelect->setSizePolicy(
            QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed, QSizePolicy::LineEdit));
    connect(timeRangeSelect, SIGNAL(boundsEdited(double, double)), gui,
            SLOT(interactionDeferTrigger()));

    timeIntervalSelect = new TimeIntervalSelection(timeToolBar);
    timeIntervalSelect->setToolTip(tr("The amount of time displayed."));
    timeIntervalSelect->setStatusTip(tr("The amount of time shown"));
    timeIntervalSelect->setWhatsThis(
            tr("This is the amount of time displayed.  In realtime mode this is how far back from the current time data is shown."));
    timeIntervalSelect->setDisableWindowFocus(true);
    timeIntervalSelectAction = timeToolBar->addWidget(timeIntervalSelect);
    timeIntervalSelect->setSizePolicy(
            QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed, QSizePolicy::LineEdit));
    timeIntervalSelectAction->setVisible(false);
    connect(timeIntervalSelect, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)),
            gui, SLOT(interactionDeferTrigger()));

    realtimeAction =
            new QAction(QApplication::style()->standardIcon(QStyle::SP_MediaPlay, 0, timeToolBar),
                        tr("Re&altime"), timeToolBar);
    realtimeAction->setToolTip(tr("Enable realtime mode."));
    realtimeAction->setStatusTip(tr("Realtime mode"));
    realtimeAction->setWhatsThis(
            tr("This sets the display mode to realtime.  While in realtime the start time is the current time and the end time is a selected interval back in time from it."));
    realtimeAction->setCheckable(true);
    connect(realtimeAction, SIGNAL(toggled(bool)), this, SLOT(realtimeToggled(bool)));
    timeToolBar->addAction(realtimeAction);
    realtimeAction->setVisible(false);

    QAction *act = new QAction(
            QApplication::style()->standardIcon(QStyle::SP_BrowserReload, 0, timeToolBar),
            tr("&Replot"), timeToolBar);
    act->setShortcuts(QKeySequence::Refresh);
    act->setToolTip(tr("Replot visible data and apply the time selection."));
    act->setStatusTip(tr("Replot data"));
    act->setWhatsThis(
            tr("This replots data with the selected time range.  Data is reloaded from disk and plots are cleared and updated with the time range selected."));
    connect(act, SIGNAL(triggered()), this, SLOT(replotPressed()));
    timeToolBar->addAction(act);

    timeToolBar->addSeparator();

    backAction =
            new QAction(QApplication::style()->standardIcon(QStyle::SP_ArrowBack, 0, timeToolBar),
                        tr("&Back"), timeToolBar);
    backAction->setShortcuts(QKeySequence::Back);
    backAction->setToolTip(tr("Undo the most recent time change or zoom."));
    backAction->setStatusTip(tr("Undo the last time change or zoom"));
    backAction->setWhatsThis(
            tr("This moves one step backwards in the history of time changes or zooms."));
    backAction->setEnabled(false);
    connect(backAction, SIGNAL(triggered()), this, SLOT(backPressed()));
    connect(&historyStack, SIGNAL(canUndoChanged(bool)), backAction, SLOT(setEnabled(bool)));
    timeToolBar->addAction(backAction);

    forwardAction = new QAction(
            QApplication::style()->standardIcon(QStyle::SP_ArrowForward, 0, timeToolBar),
            tr("&Forward"), timeToolBar);
    forwardAction->setShortcuts(QKeySequence::Forward);
    forwardAction->setToolTip(tr("Re-apply the next time change or zoom."));
    forwardAction->setStatusTip(tr("Re-apply the next time change or zoom"));
    forwardAction->setWhatsThis(
            tr("This moves one step forward in the history of time changes or zooms."));
    forwardAction->setEnabled(false);
    connect(forwardAction, SIGNAL(triggered()), this, SLOT(forwardPressed()));
    connect(&historyStack, SIGNAL(canRedoChanged(bool)), forwardAction, SLOT(setEnabled(bool)));
    timeToolBar->addAction(forwardAction);

    QToolButton *toolButton = new QToolButton(timeToolBar);
    historyMenuBase = new QMenu(toolButton);
    act = new HistoryViewAction(this, historyMenuBase);
    act->setToolTip(tr("Select a history item."));
    act->setStatusTip(tr("Select history item"));
    act->setWhatsThis(tr("Select and apply an item in the history of actions taken."));
    historyMenuBase->addAction(act);
    toolButton->setMenu(historyMenuBase);
    toolButton->setPopupMode(QToolButton::InstantPopup);
    toolButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_ArrowDown, 0, timeToolBar));
    toolButton->setText(tr("&History"));
    connect(&historyStack, SIGNAL(indexChanged(int)), historyMenuBase, SLOT(hide()));

    historyAction = timeToolBar->addWidget(toolButton);
#ifndef Q_OS_MAC
    historyAction->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_H));
#endif
    historyAction->setToolTip(tr("Show the time change and zoom history."));
    historyAction->setStatusTip(tr("Show the time change and zoom history"));
    historyAction->setWhatsThis(
            tr("This displays the history of time changes and zooms.  Selecting one will go to that state."));
    historyAction->setEnabled(false);
    connect(&historyStack, SIGNAL(canUndoChanged(bool)), this, SLOT(updateHistoryEnabled()));
    connect(&historyStack, SIGNAL(canRedoChanged(bool)), this, SLOT(updateHistoryEnabled()));


    QWidget *central = new QWidget(this);
    setCentralWidget(central);
    QVBoxLayout *layout = new QVBoxLayout(central);
    central->setLayout(layout);

    displayTabBar = new QTabBar(central);
    displayTabBar->setUsesScrollButtons(true);
    displayTabBar->setDocumentMode(true);
#ifdef Q_OS_OSX
    displayTabBar->setStyleSheet("QTabBar::tab { background-color: white; color: black; border: 2px solid #C4C4C3;  border-bottom-color: #C2C7CB; border-top-left-radius: 4px; border-top-right-radius: 4px; min-width: 8ex; padding: 2px; } "
                                 "QTabBar::tab::selected { background-color: lightgrey; color: black; border-color: #9B9B9B; } ");
#endif
    layout->addWidget(displayTabBar);
    {
        QWidget *displayArea = new QWidget(central);
        layout->addWidget(displayArea);
        displayLayout = new QVBoxLayout(displayArea);
        displayArea->setLayout(displayLayout);

        QSizePolicy policy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        policy.setHorizontalStretch(1);
        policy.setVerticalStretch(1);
        displayArea->setSizePolicy(policy);

        displayLayout->setContentsMargins(0, 0, 0, 0);
    }
    displayCurrent = NULL;
    displayPrior = NULL;
    haveFocusedStateDisplay = false;
    connect(displayTabBar, SIGNAL(currentChanged(int)), this, SLOT(tabSelected(int)));

    messageInterfaceContainer = new QWidget(central);
    layout->addWidget(messageInterfaceContainer);

    QGridLayout *outerLayout = new QGridLayout(messageInterfaceContainer);
    messageInterfaceContainer->setLayout(outerLayout);
    {
        QMargins margins(outerLayout->contentsMargins());
        margins.setTop(1);
        margins.setBottom(5);
        outerLayout->setContentsMargins(margins);
    }

    messageArea = new QScrollArea(messageInterfaceContainer);
    messageArea->setMaximumHeight(200);
    messageContainer = new QWidget(messageArea);
    messageArea->setAlignment(Qt::AlignBottom | Qt::AlignLeft);
    messageLayout = new QGridLayout(messageContainer);
    messageLayout->setColumnStretch(1, 1);
    messageContainer->setLayout(messageLayout);
    outerLayout->addWidget(messageArea, 0, 0, 2, 2);
    messageArea->setWidget(messageContainer);

    messageLayout->setContentsMargins(0, 0, 0, 0);
    messageHideTimer = new QTimer(this);
    messageHideTimer->setSingleShot(true);
    messageHideTimer->setInterval(30000);
    connect(messageHideTimer, SIGNAL(timeout()), messageInterfaceContainer, SLOT(hide()));

    QPushButton *button = new QPushButton(
            QApplication::style()->standardIcon(QStyle::SP_DialogCloseButton, 0,
                                                messageInterfaceContainer), tr("Dismiss All"),
            messageInterfaceContainer);
    button->setToolTip(tr("Dismiss all messages."));
    button->setStatusTip(tr("Dismiss realtime messages"));
    button->setWhatsThis(
            tr("This will dismiss all realtime messages.  The message display will be hidden afterwords."));
    {
        int baseSize = QFontMetrics(QFont()).height();
        button->setIconSize(QSize(baseSize, baseSize));
    }
    connect(button, SIGNAL(clicked()), this, SLOT(dismissAllMessages()));
    outerLayout->addWidget(button, 1, 0, 1, 1, Qt::AlignRight | Qt::AlignTop);

    outerLayout->setRowMinimumHeight(0, 3);
    outerLayout->setColumnMinimumWidth(1, 20);
    outerLayout->setRowStretch(1, 1);
    outerLayout->setColumnStretch(0, 1);

    messageInterfaceContainer->hide();

    if (gui->restoreState()) {
        restoreGeometry(
                gui->getSettings()->value(gui->statePath("mainWindow/geometry")).toByteArray());
        restoreState(
                gui->getSettings()->value(gui->statePath("mainWindow/windowState")).toByteArray());
        updateGeometry();
    }
}

GUIMainWindow::~GUIMainWindow()
{
    for (; ;) {
        QLayoutItem *remove = displayLayout->takeAt(0);
        if (!remove)
            break;
    }
    qDeleteAll(displays);

    /* Explicitly delete it, so it can't process any more queued signals (since that
     * references our settings) */
    delete progress;
}

void GUIMainWindow::writeOutState()
{
    gui->getSettings()->setValue(gui->statePath("mainWindow/geometry"), saveGeometry());
    gui->getSettings()->setValue(gui->statePath("mainWindow/windowState"), saveState());

    if (displayCurrent != NULL) {
        gui->getSettings()
           ->setValue(gui->statePath("mainWindow/activeDisplay"), displayCurrent->getStateID());
    } else {
        gui->getSettings()->remove(gui->statePath("mainWindow/activeDisplay"));
    }
}

void GUIMainWindow::closeEvent(QCloseEvent *event)
{
    if (!gui->allowMainWindowClose()) {
        event->ignore();
        return;
    }

    writeOutState();
    gui->writeOutState();
    QMainWindow::closeEvent(event);
}

void GUIMainWindow::showEvent(QShowEvent *event)
{
    QMainWindow::showEvent(event);
    QTimer::singleShot(100, this, SLOT(unfocusInitial()));
}

void GUIMainWindow::unfocusInitial()
{
    QWidget *focused = QApplication::focusWidget();
    if (!focused)
        return;
    focused->clearFocus();
}

void GUIMainWindow::escapePressed()
{
    unfocusInitial();
    for (QList<GUIDisplayInterface *>::const_iterator d = displays.constBegin(),
            end = displays.constEnd(); d != end; ++d) {
        (*d)->releaseFocus();
    }
}


void GUIMainWindow::setRealtime(Time::LogicalTimeUnit unit, int count, bool align)
{
    timeIntervalSelect->setInterval(unit, count, align);
    timeRangeSelectAction->setVisible(false);
    timeIntervalSelectAction->setVisible(true);
    realtimeAction->setChecked(true);
    connect(realtimeAction, SIGNAL(toggled(bool)), this, SLOT(realtimeToggled(bool)));


    if (!initialReplot) {
        initialReplot.reset(new ReplotOperation(unit, count, align, gui, this));
        lastActivatedReplot = initialReplot;
    }
}

void GUIMainWindow::setFixedTime(double start, double end)
{
    timeRangeSelect->setBounds(start, end);
    timeRangeSelectAction->setVisible(true);
    timeIntervalSelectAction->setVisible(false);
    realtimeAction->setChecked(false);

    if (!initialReplot) {
        initialReplot.reset(new ReplotOperation(start, end, gui, this));
        lastActivatedReplot = initialReplot;
    }
}

void GUIMainWindow::updateBoundsIfInRealtime(double start, double end)
{
    if (!realtimeAction->isChecked())
        return;
    timeRangeSelect->setBounds(start, end);
}

void GUIMainWindow::setRealtimeToggleEnabled(bool enabled)
{
    realtimeAction->setVisible(enabled);
}

void GUIMainWindow::setRealtimeStatusString(const QString &text, const QString &systemFlags)
{
    if (text.isEmpty()) {
        realtimeStatus->hide();
    } else {
        realtimeStatus->setText(text);
        realtimeStatus->show();
    }
    if (systemFlags.isEmpty()) {
        realtimeSystemFlags->hide();
    } else {
        realtimeSystemFlags->setText(systemFlags);
        realtimeSystemFlags->show();
    }

    realtimeSeparator->setVisible(realtimeSystemFlags->isVisible() && realtimeStatus->isVisible());
}

QSize GUIMainWindow::sizeHint() const
{ return QSize(1025, 768); }

void GUIMainWindow::realtimeMessagesShow()
{
    if (displayedMessages.isEmpty())
        return;

    messageInterfaceContainer->setVisible(!messageInterfaceContainer->isVisible());
    messageHideTimer->stop();
}

void GUIMainWindow::messageAreaDelayedRemove()
{
    messageArea->widget()->adjustSize();
    messageArea->widget()->updateGeometry();
}

void GUIMainWindow::dismissMessage()
{
    gui->interactionDeferTrigger();

    int index = sender()->property("listIndex").toInt();
    if (index < 0 || index >= displayedMessages.size())
        return;

    QList<QWidget *> messages(displayedMessages.at(index));
    for (QList<QWidget *>::const_iterator remove = messages.constBegin(), end = messages.constEnd();
            remove != end;
            ++remove) {
        messageLayout->removeWidget(*remove);
        (*remove)->hide();
        (*remove)->deleteLater();
    }
    displayedMessages.removeAt(index);

    QMetaObject::invokeMethod(this, "messageAreaDelayedRemove", Qt::QueuedConnection);

    if (displayedMessages.isEmpty()) {
        messageHideTimer->stop();
        messageInterfaceContainer->hide();
        realtimeMessagesPending->hide();
        return;
    }

    for (int max = displayedMessages.size(); index < max; index++) {
        for (int col = 0, maxCol = displayedMessages.at(index).size(); col < maxCol; ++col) {
            QWidget *widget = displayedMessages.at(index).at(col);
            widget->setProperty("listIndex", index);

            messageLayout->removeWidget(widget);
            messageLayout->addWidget(widget, index, col);
        }
    }
}

void GUIMainWindow::dismissAllMessages()
{
    gui->interactionDeferTrigger();

    for (QList<QList<QWidget *> >::const_iterator messageList = displayedMessages.constBegin(),
            endML = displayedMessages.constEnd(); messageList != endML; ++messageList) {
        for (QList<QWidget *>::const_iterator remove = messageList->constBegin(),
                endR = messageList->constEnd(); remove != endR; ++remove) {
            messageLayout->removeWidget(*remove);
            (*remove)->hide();
            (*remove)->deleteLater();
        }
    }
    displayedMessages.clear();

    messageHideTimer->stop();
    messageInterfaceContainer->hide();
    realtimeMessagesPending->hide();
}

void GUIMainWindow::messageAreaDelayedAdd()
{
    messageArea->widget()->adjustSize();
    messageArea->widget()->updateGeometry();
    messageArea->verticalScrollBar()->setValue(messageArea->verticalScrollBar()->maximum());
}

void GUIMainWindow::addRealtimeMessage(const QString &messsage)
{
    QLabel *label = new QLabel(messsage, messageContainer);
    messageLayout->addWidget(label, displayedMessages.size(), 1);

    QPushButton *dismissButton = new QPushButton(
            QApplication::style()->standardIcon(QStyle::SP_DialogCloseButton, 0, messageContainer),
            QString(), messageContainer);
    dismissButton->setIconSize(
            QSize(label->sizeHint().height() - 4, label->sizeHint().height() - 4));
    dismissButton->setMaximumSize(QSize(label->sizeHint().height(), label->sizeHint().height()));
    messageLayout->addWidget(dismissButton, displayedMessages.size(), 0);

    dismissButton->setProperty("listIndex", displayedMessages.size());
    connect(dismissButton, SIGNAL(clicked()), this, SLOT(dismissMessage()));

    displayedMessages.append(QList<QWidget *>() << dismissButton << label);

    QMetaObject::invokeMethod(this, "messageAreaDelayedAdd", Qt::QueuedConnection);
    messageInterfaceContainer->show();
    realtimeMessagesPending->show();
    messageHideTimer->start();
}

void GUIMainWindow::start()
{
    progress->setTimeBounds(gui->getStart(), gui->getEnd());
    progress->initialize();
}

void GUIMainWindow::finished()
{
    progress->finished();
    gui->interactionDeferTrigger();
}

void GUIMainWindow::feedbackAttach(ActionFeedback::Source &source)
{ progress->attach(source); }

void GUIMainWindow::realtimeToggled(bool enabled)
{
    gui->interactionDeferTrigger();
    if (enabled == inRealtimeMode)
        return;
    inRealtimeMode = enabled;
    timeRangeSelectAction->setVisible(!enabled);
    timeIntervalSelectAction->setVisible(enabled);
}

void GUIMainWindow::displayActiveInFullscreen()
{
    if (displayCurrent == NULL)
        return;
    displayCurrent->showFullscreen();
}


GUIMainWindow::ReplotOperation::ReplotOperation() : mode(Fixed),
                                                    gui(NULL),
                                                    parent(NULL),
                                                    start(FP::undefined()),
                                                    end(FP::undefined()),
                                                    unit(Time::Day),
                                                    count(1),
                                                    align(false)
{ }

GUIMainWindow::ReplotOperation::ReplotOperation(double s, double e, CPD3GUI *g, GUIMainWindow *p)
        : mode(Fixed),
          gui(g),
          parent(p),
          start(s),
          end(e), unit(Time::Minute), count(0), align(false)
{ }

GUIMainWindow::ReplotOperation::ReplotOperation(Time::LogicalTimeUnit u,
                                                int c,
                                                bool a,
                                                CPD3GUI *g,
                                                GUIMainWindow *p) : mode(Realtime),
                                                                    gui(g),
                                                                    parent(p),
                                                                    start(FP::undefined()),
                                                                    end(FP::undefined()),
                                                                    unit(u),
                                                                    count(c),
                                                                    align(a)
{ }

GUIMainWindow::ReplotOperation::ReplotOperation(const ReplotOperation &other) : mode(other.mode),
                                                                                gui(other.gui),
                                                                                parent(other.parent),
                                                                                start(other.start),
                                                                                end(other.end),
                                                                                unit(other.unit),
                                                                                count(other.count),
                                                                                align(other.align)
{ }

GUIMainWindow::ReplotOperation &GUIMainWindow::ReplotOperation::operator=(const ReplotOperation &other)
{
    if (&other == this)
        return *this;
    mode = other.mode;
    gui = other.gui;
    parent = other.parent;
    start = other.start;
    end = other.end;
    unit = other.unit;
    count = other.count;
    align = other.align;
    return *this;
}

void GUIMainWindow::ReplotOperation::apply(const ReplotOperationPointer &operation)
{
    parent->lastActivatedReplot = operation;

    parent->historyMenuBase->hide();

    switch (mode) {
    case Fixed:
        gui->queueFixedReplot(start, end);
        return;
    case Realtime:
        gui->queueRealtimeReplot(unit, count, align);
        return;
    }
    Q_ASSERT(false);
}

QString GUIMainWindow::ReplotOperation::undoText() const
{
    switch (mode) {
    case Fixed:
        return GUIMainWindow::tr("Replot: %1").arg(
                GUITime::formatRange(start, end, gui->getSettings()));
    case Realtime:
        return GUIMainWindow::tr("Replot: %1").arg(Time::describeOffset(unit, count, align));
    }
    Q_ASSERT(false);
    return QString();
}

bool GUIMainWindow::ReplotOperation::mergable(const ReplotOperation &other) const
{
    if (mode != other.mode)
        return false;
    switch (mode) {
    case Fixed:
        return FP::equal(start, other.start) && FP::equal(end, other.end);
    case Realtime:
        return unit == other.unit && count == other.count && align == other.align;
    }
    Q_ASSERT(false);
    return false;
}


GUIMainWindow::HistoryViewList::HistoryViewList(GUIMainWindow *w, QWidget *parent) : QUndoView(
        &w->historyStack, parent), win(w)
{
    if (win->initialReplot != NULL)
        setEmptyLabel(win->initialReplot->undoText());
}

GUIMainWindow::HistoryViewList::~HistoryViewList()
{ }

void GUIMainWindow::HistoryViewList::showEvent(QShowEvent *event)
{
    if (win->initialReplot != NULL)
        setEmptyLabel(win->initialReplot->undoText());
    win->unfocusInitial();
    win->gui->interactionDeferTrigger();
    QUndoView::showEvent(event);
}


GUIMainWindow::HistoryViewAction::HistoryViewAction(GUIMainWindow *w, QObject *parent)
        : QWidgetAction(parent), win(w)
{ }

GUIMainWindow::HistoryViewAction::~HistoryViewAction()
{ }

QWidget *GUIMainWindow::HistoryViewAction::createWidget(QWidget *parent)
{
    return new GUIMainWindow::HistoryViewList(win, parent);
}


GUIMainWindow::UndoReplot::UndoReplot(const ReplotOperationPointer &op,
                                      CPD3GUI *g,
                                      GUIMainWindow *p)
        : gui(g), parent(p), operation(op), previousReplot(p->lastActivatedReplot), resetZoom()
{
    setText(operation->undoText());
    gui->injectResetZoom(resetZoom);
}

GUIMainWindow::UndoReplot::~UndoReplot()
{ }

int    GUIMainWindow::UndoReplot::id() const
{ return 1; }

bool GUIMainWindow::UndoReplot::mergeWith(const QUndoCommand *command)
{
    if (id() != command->id())
        return false;
    const UndoReplot *other = static_cast<const UndoReplot *>(command);
    return operation->mergable(*(other->operation));
}

void GUIMainWindow::UndoReplot::undo()
{
    if (previousReplot)
        previousReplot->apply(previousReplot);
    resetZoom.undo();
}

void GUIMainWindow::UndoReplot::redo()
{
    resetZoom.redo();
    operation->apply(operation);
}

void GUIMainWindow::replotPressed()
{
    gui->interactionDeferTrigger();

    ReplotOperationPointer replot;
    if (inRealtimeMode) {
        replot.reset(
                new ReplotOperation(timeIntervalSelect->getUnit(), timeIntervalSelect->getCount(),
                                    timeIntervalSelect->getAlign(), gui, this));
    } else {
        replot.reset(
                new ReplotOperation(timeRangeSelect->getStart(), timeRangeSelect->getEnd(), gui,
                                    this));
    }

    if (!initialReplot) {
        initialReplot = replot;
        historyStack.clear();
        return;
    }

    if (historyStack.isClean() && initialReplot->mergable(*replot)) {
        initialReplot->apply(initialReplot);
        return;
    }

    historyStack.push(new UndoReplot(replot, gui, this));
}

void GUIMainWindow::backPressed()
{
    unfocusInitial();
    gui->interactionDeferTrigger();
    historyStack.undo();
}

void GUIMainWindow::forwardPressed()
{
    unfocusInitial();
    gui->interactionDeferTrigger();
    historyStack.redo();
}

void GUIMainWindow::backToStart()
{
    unfocusInitial();
    unfocusInitial();
    gui->interactionDeferTrigger();
    while (historyStack.canUndo()) {
        historyStack.undo();
    }
}

void GUIMainWindow::clearHistory()
{
    if (initialReplot && initialReplot != lastActivatedReplot) {
        initialReplot.reset(new ReplotOperation(*lastActivatedReplot));
    }
    historyStack.clear();
}

void GUIMainWindow::updateHistoryEnabled()
{
    historyAction->setEnabled(historyStack.canUndo() || historyStack.canRedo());
}

void GUIMainWindow::incomingZoom(CPD3::Graphing::DisplayZoomCommand command)
{
    gui->interactionDeferTrigger();
    historyStack.push(new CPD3::Graphing::DisplayZoomCommand(command));
}

void GUIMainWindow::incomingGlobalZoom(CPD3::Graphing::DisplayZoomCommand command)
{
    gui->interactionDeferTrigger();
    gui->injectGlobalZoom(command);
    historyStack.push(new CPD3::Graphing::DisplayZoomCommand(command));
}


GUIDisplayInterface::GUIDisplayInterface(CPD3GUI *g, GUIMainWindow *p) : gui(g), parent(p)
{ }

GUIDisplayInterface::~GUIDisplayInterface()
{ }

bool GUIDisplayInterface::showFullscreen()
{ return false; }

void GUIDisplayInterface::releaseFocus()
{ }

static bool displaySortCompare(const GUIDisplayInterface *a, const GUIDisplayInterface *b)
{
    {
        qint64 priorityA = a->getSortPriority();
        qint64 priorityB = b->getSortPriority();
        if (INTEGER::defined(priorityA) && INTEGER::defined(priorityB)) {
            if (priorityA != priorityB)
                return priorityA < priorityB;
        } else if (!INTEGER::defined(priorityA)) {
            return false;
        } else if (!INTEGER::defined(priorityB)) {
            return true;
        }
    }

    const GUIDisplayHandlerInterface *dispA = qobject_cast<const GUIDisplayHandlerInterface *>(a);
    if (dispA) {
        {
            const GUIDisplayHandlerInterface
                    *dispB = qobject_cast<const GUIDisplayHandlerInterface *>(b);
            if (dispB) {
                return dispA->getTitle().compare(dispB->getTitle(), Qt::CaseInsensitive) < 0;
            }
        }

        /* Static displays always sort before the dynamic ones */
        return true;
    }

    const GUIDisplayAcquisitionInterface
            *acqA = qobject_cast<const GUIDisplayAcquisitionInterface *>(a);
    if (acqA) {
        {
            const GUIDisplayHandlerInterface
                    *dispB = qobject_cast<const GUIDisplayHandlerInterface *>(b);
            if (dispB) {
                return false;
            }
        }

        {
            const GUIDisplayAcquisitionInterface
                    *acqB = qobject_cast<const GUIDisplayAcquisitionInterface *>(b);
            if (acqB) {
                return acqA->getTitle().compare(acqB->getTitle(), Qt::CaseInsensitive) < 0;
            }
        }

        {
            const GUIGenericHandlerInterface
                    *genB = qobject_cast<const GUIGenericHandlerInterface *>(b);
            if (genB) {
                return acqA->getTitle().compare(genB->getTitle(), Qt::CaseInsensitive) < 0;
            }
        }
    }

    const GUIGenericHandlerInterface *genA = qobject_cast<const GUIGenericHandlerInterface *>(a);
    if (genA) {
        {
            const GUIDisplayHandlerInterface
                    *dispB = qobject_cast<const GUIDisplayHandlerInterface *>(b);
            if (dispB) {
                return false;
            }
        }

        {
            const GUIGenericHandlerInterface
                    *genB = qobject_cast<const GUIGenericHandlerInterface *>(b);
            if (genB) {
                return genA->getTitle().compare(genB->getTitle(), Qt::CaseInsensitive) < 0;
            }
        }

        {
            const GUIDisplayAcquisitionInterface
                    *acqB = qobject_cast<const GUIDisplayAcquisitionInterface *>(b);
            if (acqB) {
                return genA->getTitle().compare(acqB->getTitle(), Qt::CaseInsensitive) < 0;
            }
        }
    }

    /* Unknown, so "equal" */
    return false;
}

void GUIMainWindow::showDisplay(GUIDisplayInterface *interface)
{
    for (; ;) {
        QLayoutItem *remove = displayLayout->takeAt(0);
        if (!remove)
            break;
        QWidget *widget = remove->widget();
        if (!widget)
            continue;
        widget->hide();
    }

    if (interface == NULL)
        return;
    QWidget *add = interface->widget();
    if (!add)
        return;

    displayLayout->addWidget(add);
    add->show();
}

void GUIMainWindow::rebuildTabs()
{
    displayTabBar->disconnect(this);

    for (int i = displayTabBar->count() - 1; i >= 0; i--) {
        displayTabBar->removeTab(i);
    }
    displayedTabs.clear();

    int focusTab = -1;
    for (QList<GUIDisplayInterface *>::const_iterator add = displays.constBegin(),
            end = displays.constEnd(); add != end; ++add) {
        QWidget *w = (*add)->widget();
        if (w == NULL)
            continue;

        bool isStateFocus = gui->restoreState() &&
                gui->getSettings()->value(gui->statePath("mainWindow/activeDisplay")).toString() ==
                        (*add)->getStateID();
        bool focusThisDisplay = !haveFocusedStateDisplay && isStateFocus;
        if (haveFocusedStateDisplay && displayCurrent == *add)
            focusThisDisplay = true;

        int idx = displayedTabs.size();
        displayedTabs.append(*add);
        displayTabBar->addTab((*add)->getTitle());

        if (focusThisDisplay && isStateFocus)
            haveFocusedStateDisplay = true;

        if (focusThisDisplay) {
            focusTab = idx;
            if (displayCurrent != *add) {
                displayPrior = displayCurrent;
                displayCurrent = *add;
            }
        }

        if (focusTab == -1 && displayCurrent == *add)
            focusTab = idx;
    }

    if (focusTab >= 0) {
        displayCurrent = displayedTabs.at(focusTab);
        showDisplay(displayCurrent);
        displayTabBar->setCurrentIndex(focusTab);
    } else if (displayTabBar->count() > 0) {
        displayCurrent = displayedTabs.at(0);
        showDisplay(displayCurrent);
        displayTabBar->setCurrentIndex(0);
    } else {
        showDisplay(NULL);
    }

    connect(displayTabBar, SIGNAL(currentChanged(int)), this, SLOT(tabSelected(int)));
}

void GUIMainWindow::tabSelected(int idx, bool changeIndex)
{
    if (idx < 0 || idx >= displayedTabs.size())
        return;
    GUIDisplayInterface *interface = displayedTabs.at(idx);

    showDisplay(interface);

    displayPrior = displayCurrent;
    displayCurrent = interface;

    if (changeIndex) {
        displayTabBar->disconnect(this);
        displayTabBar->setCurrentIndex(idx);
        connect(displayTabBar, SIGNAL(currentChanged(int)), this, SLOT(tabSelected(int)));
    }
}

void GUIMainWindow::addWindow(GUIDisplayInterface *add)
{
    /* Just added to the end, don't need to resort */
    if (displayedTabs.size() == 0 || !displaySortCompare(add, displays.last())) {
        bool isStateFocus = gui->restoreState() &&
                gui->getSettings()->value(gui->statePath("mainWindow/activeDisplay")).toString() ==
                        add->getStateID();
        bool focusThisDisplay = !haveFocusedStateDisplay && isStateFocus;
        if (displayTabBar->count() == 0)
            focusThisDisplay = true;
        displays.append(add);

        QWidget *w = add->widget();
        if (w == NULL)
            return;

        if (focusThisDisplay && isStateFocus)
            haveFocusedStateDisplay = true;

        int idx = displayedTabs.size();
        displayTabBar->disconnect(this);
        displayedTabs.append(add);
        displayTabBar->addTab(add->getTitle());
        if (focusThisDisplay) {
            displayTabBar->setCurrentIndex(idx);
            showDisplay(add);
            displayPrior = displayCurrent;
            displayCurrent = add;
        }
        connect(displayTabBar, SIGNAL(currentChanged(int)), this, SLOT(tabSelected(int)));
        return;
    }

    displays.append(add);
    std::stable_sort(displays.begin(), displays.end(), displaySortCompare);

    QWidget *w = add->widget();
    if (w == NULL)
        return;
    rebuildTabs();
}


QList<GUIDisplayInterface *> GUIMainWindow::getHandlerInterfaces() const
{
    return displays;
}

void GUIMainWindow::activatePreviousSelection()
{
    if (displayPrior == NULL || displayPrior == displayCurrent || displayTabBar->count() < 2)
        return;
    int idx = displayedTabs.indexOf(displayPrior);
    if (idx < 0)
        return;
    tabSelected(idx, true);
}

void GUIMainWindow::activateNext()
{
    if (displayCurrent == NULL || displayTabBar->count() < 2)
        return;
    int idx = displayedTabs.indexOf(displayCurrent);
    if (idx < 0)
        return;
    tabSelected((idx + 1) % displayTabBar->count(), true);
}

void GUIMainWindow::activatePrevious()
{
    if (displayCurrent == NULL || displayTabBar->count() < 2)
        return;
    int idx = displayedTabs.indexOf(displayCurrent);
    if (idx < 0)
        return;
    --idx;
    if (idx < 0)
        idx = displayTabBar->count() - 1;
    tabSelected(idx, true);
}

void GUIMainWindow::legendToggle(bool enable, DisplayHandler *display)
{
    for (QList<GUIDisplayInterface *>::const_iterator in = displays.constBegin(),
            endD = displays.constEnd(); in != endD; ++in) {
        const GUIDisplayHandlerInterface
                *check = qobject_cast<const GUIDisplayHandlerInterface *>(*in);
        if (!check)
            continue;
        if (display != NULL && !check->matchesDisplay(display))
            continue;
        for (const auto &out : check->getHandler()->getOutputs()) {
            if (auto traceOut = qobject_cast<TraceOutput2D *>(out.get())) {
                traceOut->setLegendAlwaysHide(!enable);
            }

            if (auto binOut = qobject_cast<BinOutput2D *>(out.get())) {
                binOut->setLegendAlwaysHide(!enable);
            }
        }
    }
}

void CPD3GUI::resetLegendShow(DisplayHandler *handler)
{
    if (!mainWindow)
        return;
    if (!viewLegendToggle)
        return;
    mainWindow->legendToggle(viewLegendToggle->isChecked(), handler);
}

GUIDisplayAcquisitionInterface::GUIDisplayAcquisitionInterface(InterfaceHandler *i,
                                                               CPD3GUI *g,
                                                               GUIMainWindow *p)
        : GUIDisplayInterface(g, p), interface(i), display(NULL)
{
    connect(interface, SIGNAL(displayUpdated()), p, SLOT(rebuildTabs()));

    display = interface->displayWidget();
    display->hide();

    /* Connect the update() directly so it gets called first and the updated
     * size is available for the updateSizes() */
    connect(display, SIGNAL(layoutRebuilt()), display, SLOT(update()));
    connect(display, SIGNAL(layoutRebuilt()), display, SLOT(updateGeometry()),
            Qt::QueuedConnection);
}

GUIDisplayAcquisitionInterface::~GUIDisplayAcquisitionInterface()
{
    display->deleteLater();
}

QString GUIDisplayAcquisitionInterface::getTitle() const
{ return interface->getWindowTitle(); }

QWidget *GUIDisplayAcquisitionInterface::widget() const
{
    if (!interface->hasVisibleWindow())
        return NULL;
    return display;
}

QString GUIDisplayAcquisitionInterface::getStateID() const
{ return QString("acq-%1").arg(interface->getName()); }

qint64 GUIDisplayAcquisitionInterface::getSortPriority() const
{ return interface->getDisplaySortPriority(); }

bool GUIDisplayAcquisitionInterface::matchesInterface(const InterfaceHandler *i) const
{ return i == interface; }

void GUIMainWindow::addAcquisitionInterface(InterfaceHandler *interface)
{
    addWindow(new GUIDisplayAcquisitionInterface(interface, gui, this));
}

void GUIMainWindow::removeAcquisitionInterface(InterfaceHandler *interface)
{
    for (QList<GUIDisplayInterface *>::iterator remove = displays.begin();
            remove != displays.end();) {
        const GUIDisplayAcquisitionInterface
                *check = qobject_cast<const GUIDisplayAcquisitionInterface *>(*remove);
        if (!check) {
            ++remove;
            continue;
        }
        if (!check->matchesInterface(interface)) {
            ++remove;
            continue;
        }
        if (check == displayCurrent)
            displayCurrent = NULL;
        if (check == displayPrior)
            displayPrior = NULL;
        (*remove)->deleteLater();
        remove = displays.erase(remove);
    }
    rebuildTabs();
}

void GUIMainWindow::removeAllAcquisitionInterfaces()
{
    for (QList<GUIDisplayInterface *>::iterator remove = displays.begin();
            remove != displays.end();) {
        const GUIDisplayAcquisitionInterface
                *check = qobject_cast<const GUIDisplayAcquisitionInterface *>(*remove);
        if (!check) {
            ++remove;
            continue;
        }
        if (check == displayCurrent)
            displayCurrent = NULL;
        if (check == displayPrior)
            displayPrior = NULL;
        (*remove)->deleteLater();
        remove = displays.erase(remove);
    }
    rebuildTabs();
}

void GUIMainWindow::showAcquisitionInterface(InterfaceHandler *interface)
{
    for (QList<GUIDisplayInterface *>::const_iterator d = displays.constBegin(),
            end = displays.constEnd(); d != end; ++d) {
        const GUIDisplayAcquisitionInterface
                *check = qobject_cast<const GUIDisplayAcquisitionInterface *>(*d);
        if (!check)
            continue;
        if (!check->matchesInterface(interface))
            continue;
        int idx = displayedTabs.indexOf(*d);
        if (idx < 0)
            continue;
        tabSelected(idx, true);
        break;
    }
}


GUIGenericHandlerInterface::GUIGenericHandlerInterface(GenericHandler *h,
                                                       CPD3GUI *g,
                                                       GUIMainWindow *p) : GUIDisplayInterface(g,
                                                                                               p),
                                                                           handler(h)
{
    connect(handler, SIGNAL(displayUpdated()), p, SLOT(rebuildTabs()));

    display = handler->displayWidget();
    display->hide();

    /* Connect the update() directly so it gets called first and the updated
     * size is available for the updateSizes() */
    connect(display, SIGNAL(layoutRebuilt()), display, SLOT(update()));
    connect(display, SIGNAL(layoutRebuilt()), display, SLOT(updateGeometry()),
            Qt::QueuedConnection);
}

GUIGenericHandlerInterface::~GUIGenericHandlerInterface()
{
    display->deleteLater();
}

QString GUIGenericHandlerInterface::getStateID() const
{ return QString("gen-%1").arg(handler->getUserStateName()); }

QWidget *GUIGenericHandlerInterface::widget() const
{
    if (!handler->hasVisibleWindow())
        return NULL;
    return display;
}

QString GUIGenericHandlerInterface::getTitle() const
{ return handler->getWindowTitle(); }

qint64 GUIGenericHandlerInterface::getSortPriority() const
{ return handler->getDisplaySortPriority(); }

bool GUIGenericHandlerInterface::matchesHandler(const GenericHandler *h) const
{ return h == handler; }


void GUIMainWindow::addGenericHandler(GenericHandler *handler)
{
    addWindow(new GUIGenericHandlerInterface(handler, gui, this));
}

void GUIMainWindow::removeAllGenericHandlers()
{
    for (QList<GUIDisplayInterface *>::iterator remove = displays.begin();
            remove != displays.end();) {
        const GUIGenericHandlerInterface
                *check = qobject_cast<const GUIGenericHandlerInterface *>(*remove);
        if (!check) {
            ++remove;
            continue;
        }
        if (check == displayCurrent)
            displayCurrent = NULL;
        if (check == displayPrior)
            displayPrior = NULL;
        (*remove)->deleteLater();
        remove = displays.erase(remove);
    }
    rebuildTabs();
}

void GUIMainWindow::showGenericHandler(GenericHandler *handler)
{
    for (QList<GUIDisplayInterface *>::const_iterator d = displays.constBegin(),
            end = displays.constEnd(); d != end; ++d) {
        const GUIGenericHandlerInterface
                *check = qobject_cast<const GUIGenericHandlerInterface *>(*d);
        if (!check)
            continue;
        if (!check->matchesHandler(handler))
            continue;
        int idx = displayedTabs.indexOf(*d);
        if (idx < 0)
            continue;
        tabSelected(idx, true);
        break;
    }
}


GUIDisplayHandlerInterface::GUIDisplayHandlerInterface(DisplayHandler *h,
                                                       CPD3GUI *g,
                                                       GUIMainWindow *p) : GUIDisplayInterface(g,
                                                                                               p),
                                                                           handler(h)
{
    handler->hide();
}

GUIDisplayHandlerInterface::~GUIDisplayHandlerInterface()
{
    handler->deleteLater();
}

QString GUIDisplayHandlerInterface::getStateID() const
{ return QString("dsp-%1").arg(handler->getUserStateName()); }

QWidget *GUIDisplayHandlerInterface::widget() const
{ return handler; }

QString GUIDisplayHandlerInterface::getTitle() const
{ return handler->getWindowTitle(); }

qint64 GUIDisplayHandlerInterface::getSortPriority() const
{ return handler->getDisplaySortPriority(); }

bool GUIDisplayHandlerInterface::matchesDisplay(const DisplayHandler *d) const
{ return d == handler; }

bool GUIDisplayHandlerInterface::showFullscreen()
{
    handler->displayFullScreen();
    return true;
}

void GUIDisplayHandlerInterface::releaseFocus()
{ handler->releaseFocus(); }

std::vector<std::shared_ptr<CPD3::Graphing::DisplayHighlightController>>
GUIDisplayHandlerInterface::getHighlightControllers() const
{ return handler->getHighlightControllers(); }


void GUIMainWindow::addDisplayHandler(DisplayHandler *handler)
{
    addWindow(new GUIDisplayHandlerInterface(handler, gui, this));
    auto add = handler->getHighlightControllers();
    highlightApply(add);
    Util::append(std::move(add), highlightControllers);
}

void GUIMainWindow::removeDisplayHandler(DisplayHandler *handler)
{
    highlightControllers.clear();
    for (QList<GUIDisplayInterface *>::iterator remove = displays.begin();
            remove != displays.end();) {
        const GUIDisplayHandlerInterface
                *check = qobject_cast<const GUIDisplayHandlerInterface *>(*remove);
        if (!check) {
            ++remove;
            continue;
        }
        if (!check->matchesDisplay(handler)) {
            Util::append(check->getHighlightControllers(), highlightControllers);
            ++remove;
            continue;
        }
        if (check == displayCurrent)
            displayCurrent = NULL;
        if (check == displayPrior)
            displayPrior = NULL;
        (*remove)->deleteLater();
        remove = displays.erase(remove);
    }
    rebuildTabs();
    highlightRebuild();
}

void GUIMainWindow::removeAllDisplayHandlers()
{
    haveFocusedStateDisplay = false;
    highlightControllers.clear();
    for (QList<GUIDisplayInterface *>::iterator remove = displays.begin();
            remove != displays.end();) {
        const GUIDisplayHandlerInterface
                *check = qobject_cast<const GUIDisplayHandlerInterface *>(*remove);
        if (!check) {
            ++remove;
            continue;
        }
        if (check == displayCurrent)
            displayCurrent = NULL;
        if (check == displayPrior)
            displayPrior = NULL;
        (*remove)->deleteLater();
        remove = displays.erase(remove);
    }
    rebuildTabs();
}

void GUIMainWindow::showDisplayHandler(DisplayHandler *handler)
{
    for (QList<GUIDisplayInterface *>::const_iterator d = displays.constBegin(),
            end = displays.constEnd(); d != end; ++d) {
        const GUIDisplayHandlerInterface
                *check = qobject_cast<const GUIDisplayHandlerInterface *>(*d);
        if (!check)
            continue;
        if (!check->matchesDisplay(handler))
            continue;
        int idx = displayedTabs.indexOf(*d);
        if (idx < 0)
            continue;
        tabSelected(idx, true);
        break;
    }
}

DisplayHandler *GUIMainWindow::getSelectedDisplayHandler() const
{
    int idx = displayTabBar->currentIndex();
    if (idx < 0 || idx >= displayedTabs.size())
        return nullptr;
    auto check = dynamic_cast<GUIDisplayHandlerInterface *>(displayedTabs[idx]);
    if (!check)
        return nullptr;
    return check->getHandler();
}

void GUIMainWindow::highlightReset()
{
    for (const auto &c : highlightControllers) {
        c->clear();
    }
    activeHighlights.clear();
}

void GUIMainWindow::highlightApply(const std::vector<
        std::shared_ptr<CPD3::Graphing::DisplayHighlightController>> &target)
{
    for (const auto &c : target) {
        for (QHash<CPD3::Graphing::DisplayHighlightController::HighlightType,
                   QList<HighlightData> >::const_iterator add = activeHighlights.constBegin(),
                endAdd = activeHighlights.constEnd(); add != endAdd; ++add) {
            for (QList<HighlightData>::const_iterator data = add.value().constBegin(),
                    endD = add.value().constEnd(); data != endD; ++data) {
                c->add(add.key(), data->start, data->end, data->min, data->max, data->selection);
            }
        }
    }
}

void GUIMainWindow::highlightRebuild()
{
    for (const auto &c : highlightControllers) {
        c->clear();
    }
    highlightApply(highlightControllers);
}

void GUIMainWindow::highlightAdd(CPD3::Graphing::DisplayHighlightController::HighlightType type,
                                 double start,
                                 double end,
                                 double min,
                                 double max,
                                 const CPD3::Data::SequenceMatch::Composite &selection)
{
    for (const auto &c : highlightControllers) {
        c->add(type, start, end, min, max, selection);
    }
    HighlightData add;
    add.start = start;
    add.end = end;
    add.min = min;
    add.max = max;
    add.selection = selection;
    activeHighlights[type].append(add);
}

void GUIMainWindow::highlightReset(CPD3::Graphing::DisplayHighlightController::HighlightType type)
{
    QHash<CPD3::Graphing::DisplayHighlightController::HighlightType,
          QList<HighlightData> >::iterator check = activeHighlights.find(type);
    if (check == activeHighlights.end())
        return;
    activeHighlights.erase(check);
    highlightRebuild();
}

void GUIMainWindow::highlightAddMentorEdit(double start,
                                           double end,
                                           double min,
                                           double max,
                                           const CPD3::Data::SequenceMatch::Composite &selection)
{
    highlightAdd(CPD3::Graphing::DisplayHighlightController::MentorEdit, start, end, min, max,
                 selection);
}

void GUIMainWindow::highlightResetMentorEdit()
{
    highlightReset(CPD3::Graphing::DisplayHighlightController::MentorEdit);
}

void GUIMainWindow::highlightAddEventLog(double start,
                                         double end,
                                         double min,
                                         double max,
                                         const CPD3::Data::SequenceMatch::Composite &selection)
{
    highlightAdd(CPD3::Graphing::DisplayHighlightController::EventLog, start, end, min, max,
                 selection);
}

void GUIMainWindow::highlightResetEventLog()
{
    highlightReset(CPD3::Graphing::DisplayHighlightController::EventLog);
}
