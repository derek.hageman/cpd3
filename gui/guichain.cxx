/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QDialogButtonBox>
#include <QProgressDialog>
#include <QtConcurrentRun>
#include <QApplication>
#include <QTableWidget>
#include <QHeaderView>
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <QStatusBar>
#include <QLoggingCategory>

#include "core/number.hxx"
#include "core/waitutils.hxx"
#include "core/util.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/processingstage.hxx"
#include "guicore/guiformat.hxx"
#include "guicore/actionprogress.hxx"
#include "guicore/timesingleselection.hxx"
#include "graphing/displaycomponent.hxx"
#include "io/drivers/file.hxx"

#include "guichain.hxx"


Q_LOGGING_CATEGORY(log_gui_chain, "cpd3.gui.chain", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::GUI;
using namespace CPD3::GUI::Data;
using namespace CPD3::Graphing;


GUIChainFilter::GUIChainFilter(ProcessingStageComponent *cmp,
                               const ComponentLoader::Information &info,
                               GUIChainDialog *p) : QWidget(p), parent(p), component(cmp)
{
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    QWidget *topBar = new QWidget(this);
    QHBoxLayout *topBarLayout = new QHBoxLayout(topBar);
    topBar->setLayout(topBarLayout);
    topBarLayout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(topBar);

    selectButton = new QPushButton(info.title(), this);
    {
        QSizePolicy policy(selectButton->sizePolicy());
        policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
        selectButton->setSizePolicy(policy);
    }
    selectButton->setToolTip(tr("Configure the processing stage"));
    selectButton->setStatusTip(tr("Processing configuration"));
    selectButton->setWhatsThis(
            tr("Press this button to show the configuration for this processing stage."));
    topBarLayout->addWidget(selectButton, 1);
    selectButton->setCheckable(true);
    parent->stagesGroup->addButton(selectButton);
    connect(selectButton, SIGNAL(toggled(bool)), parent, SLOT(updateSelected()));

    moveUpButton =
            new QPushButton(QApplication::style()->standardIcon(QStyle::SP_ArrowUp), QString(),
                            topBar);
    {
        QSizePolicy policy(moveUpButton->sizePolicy());
        policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
        moveUpButton->setSizePolicy(policy);
    }
    moveUpButton->setContentsMargins(0, 0, 0, 0);
    moveUpButton->setToolTip(tr("Move this component earlier in processing"));
    moveUpButton->setStatusTip(tr("Move earlier"));
    moveUpButton->setWhatsThis(
            tr("Use this button to move the component one stage earlier in the processing stages."));
    connect(moveUpButton, SIGNAL(clicked(bool)), this, SLOT(moveUp()));
    topBarLayout->addWidget(moveUpButton);

    moveDownButton =
            new QPushButton(QApplication::style()->standardIcon(QStyle::SP_ArrowDown), QString(),
                            topBar);
    {
        QSizePolicy policy(moveDownButton->sizePolicy());
        policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
        moveDownButton->setSizePolicy(policy);
    }
    moveDownButton->setContentsMargins(0, 0, 0, 0);
    moveDownButton->setToolTip(tr("Move this component later in processing"));
    moveDownButton->setStatusTip(tr("Move later"));
    moveDownButton->setWhatsThis(
            tr("Use this button to move the component one stage later in the processing stages."));
    connect(moveDownButton, SIGNAL(clicked(bool)), this, SLOT(moveDown()));
    topBarLayout->addWidget(moveDownButton);

    QPushButton *removeButton =
            new QPushButton(QApplication::style()->standardIcon(QStyle::SP_TrashIcon), QString(),
                            topBar);
    {
        QSizePolicy policy(removeButton->sizePolicy());
        policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
        removeButton->setSizePolicy(policy);
    }
    removeButton->setContentsMargins(0, 0, 0, 0);
    removeButton->setToolTip(tr("Remove this processing stage"));
    removeButton->setStatusTip(tr("Remove stage"));
    removeButton->setWhatsThis(
            tr("Use this button to remove the stage from the processing sequence."));
    connect(removeButton, SIGNAL(clicked(bool)), this, SLOT(remove()));
    topBarLayout->addWidget(removeButton);

    description = new QLabel(info.description(), this);
    description->setWordWrap(true);
    layout->addWidget(description);

    options = new OptionEditor(this);
    layout->addWidget(options, 1);
    options->setOptions(component->getOptions());
}

void GUIChainFilter::dataAvailable(const Archive::Access::SelectedComponents &available)
{
    options->setAvailableStations(available.stations);
    options->setAvailableArchives(available.archives);
    options->setAvailableVariables(available.variables);
    options->setAvailableFlavors(available.flavors);
}

bool GUIChainFilter::addToPipeline(StreamPipeline *chain)
{
    std::unique_ptr<ProcessingStage>
            filter(component->createGeneralFilterDynamic(options->getOptions()));

    if (!chain->addProcessingStage(std::move(filter), component))
        return false;

    return true;
}

bool GUIChainFilter::addToPipeline(StreamPipeline *chain,
                                   double start,
                                   double end,
                                   SequenceName::Set &inputs)
{
    QList<SequenceName> createInputs;
    Util::append(inputs, createInputs);
    std::unique_ptr<ProcessingStage> filter
            (component->createGeneralFilterPredefined(options->getOptions(), start, end,
                                                      createInputs));

    Util::merge(filter->predictedOutputs(), inputs);

    if (!chain->addProcessingStage(std::move(filter), component))
        return false;

    return true;
}

void GUIChainFilter::updateActive(int index, bool first, bool last)
{
    if (selectButton->isChecked()) {
        description->setVisible(true);
        options->setVisible(true);
        parent->stagesLayout->setStretch(index, 1);
    } else {
        description->setVisible(false);
        options->setVisible(false);
        parent->stagesLayout->setStretch(index, 0);
    }
    moveUpButton->setEnabled(!first);
    moveDownButton->setEnabled(!last);
}

void GUIChainFilter::moveUp()
{
    int index = parent->filters.indexOf(this);
    if (index < 1)
        return;

    using std::swap;
    swap(parent->filters[index], parent->filters[index - 1]);

    if (parent->inputSelectPane != NULL)
        ++index;

    parent->stagesLayout->removeWidget(this);
    parent->stagesLayout->insertWidget(index - 1, this);
    parent->updateSelected();
}

void GUIChainFilter::moveDown()
{
    int index = parent->filters.indexOf(this);
    if (index < 0 || index >= parent->filters.size() - 1)
        return;

    using std::swap;
    swap(parent->filters[index], parent->filters[index + 1]);

    if (parent->inputSelectPane != NULL)
        ++index;

    parent->stagesLayout->removeWidget(this);
    parent->stagesLayout->insertWidget(index + 1, this);
    parent->updateSelected();
}

void GUIChainFilter::remove()
{
    int index = parent->filters.indexOf(this);
    if (index < 0)
        return;

    parent->filters.removeAt(index);
    parent->stagesLayout->removeWidget(this);
    hide();
    deleteLater();

    parent->stagesGroup->removeButton(selectButton);
    if (selectButton->isChecked()) {
        parent->stagesGroup->buttons().at(0)->setChecked(true);
    } else {
        parent->updateSelected();
    }
}

GUIChainDialog::GUIChainDialog(Mode mode, QWidget *parent, Qt::WindowFlags flags) : QDialog(parent,
                                                                                            flags),
                                                                                    station(),
                                                                                    archive(),
                                                                                    start(FP::undefined()),
                                                                                    end(FP::undefined()),
                                                                                    availableComponents(),
                                                                                    filters(),
                                                                                    inputSelectButton(
                                                                                            NULL),
                                                                                    inputSelectPane(
                                                                                            NULL),
                                                                                    inputSelectType(
                                                                                            NULL),
                                                                                    outputSelectButton(
                                                                                            NULL),
                                                                                    outputSelectPane(
                                                                                            NULL),
                                                                                    outputSelectType(
                                                                                            NULL),
                                                                                    addFilter(NULL),
                                                                                    stagesGroup(
                                                                                            NULL),
                                                                                    stagesLayout(
                                                                                            NULL),
                                                                                    archiveInput(
                                                                                            NULL),
                                                                                    fileInput(NULL),
                                                                                    componentInput(
                                                                                            NULL),
                                                                                    multiplexerInput(
                                                                                            NULL),
                                                                                    tabularOutput(
                                                                                            NULL),
                                                                                    displayOutput(
                                                                                            NULL),
                                                                                    fileOutput(
                                                                                            NULL),
                                                                                    componentOutput(
                                                                                            NULL)
{
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    setLayout(mainLayout);

    QScrollArea *mainScroll = new QScrollArea(this);
    mainScroll->setFrameShape(QFrame::NoFrame);
    mainScroll->setWidgetResizable(true);
    mainLayout->addWidget(mainScroll, 1);

    QWidget *container = new QWidget(mainScroll);
    mainScroll->setWidget(container);
    stagesLayout = new QVBoxLayout(container);
    container->setLayout(stagesLayout);
    stagesLayout->setContentsMargins(0, 0, 0, 0);
    stagesGroup = new QButtonGroup(this);

    {
        container = new QWidget(mainScroll);
        stagesLayout->addWidget(container);
        QVBoxLayout *layout = new QVBoxLayout(container);
        container->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        inputSelectButton = new QPushButton(tr("Input"), container);
        inputSelectButton->setToolTip(tr("Select the input to get data from"));
        inputSelectButton->setStatusTip(tr("Data input"));
        inputSelectButton->setWhatsThis(
                tr("Press this button to show the input selection information.  This is used to select the origin of the data to be processed."));
        layout->addWidget(inputSelectButton);
        inputSelectButton->setCheckable(true);
        inputSelectButton->setChecked(true);
        stagesGroup->addButton(inputSelectButton);
        connect(inputSelectButton, SIGNAL(toggled(bool)), this, SLOT(updateSelected()));

        inputSelectPane = new QScrollArea(container);
        inputSelectPane->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        inputSelectPane->setWidgetResizable(true);
        container = new QWidget(inputSelectPane);
        inputSelectPane->setWidget(container);
        layout->addWidget(inputSelectPane, 1);

        layout = new QVBoxLayout(container);
        container->setLayout(layout);

        inputSelectType = new QComboBox(container);
        inputSelectType->setToolTip(tr("Select the origin of data"));
        inputSelectType->setStatusTip(tr("Data input type"));
        inputSelectType->setWhatsThis(
                tr("This selects the general general source that data are obtained with."));
        layout->addWidget(inputSelectType, 0);

        {
            inputSelectType->addItem(tr("Read data from the archive"), Input_Archive);

            archiveInput = new ArchiveInput;

            archiveInput->pane = new QWidget(container);
            layout->addWidget(archiveInput->pane, 1);
            QGridLayout *pane = new QGridLayout(archiveInput->pane);
            pane->setContentsMargins(0, 0, 0, 0);
            archiveInput->pane->setLayout(pane);
            pane->setColumnStretch(0, 1);
            pane->setRowStretch(1, 1);

            archiveInput->bounds = new TimeBoundSelection(archiveInput->pane);
            archiveInput->bounds->setToolTip(tr("The time range of data loaded"));
            archiveInput->bounds->setStatusTip(tr("Archive time range"));
            archiveInput->bounds
                        ->setWhatsThis(
                                tr("This sets the time range of data to be loaded from the archive."));
            archiveInput->bounds->setAllowUndefinedStart(true);
            archiveInput->bounds->setAllowUndefinedEnd(true);
            {
                QSizePolicy policy(archiveInput->bounds->sizePolicy());
                policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
                archiveInput->bounds->setSizePolicy(policy);
            }
            pane->addWidget(archiveInput->bounds, 0, 0, 1, 1);

            QPushButton *advanced =
                    new QPushButton(QApplication::style()->standardIcon(QStyle::SP_DriveNetIcon),
                                    QString(), archiveInput->pane);
            advanced->setToolTip(tr("Show advanced options"));
            advanced->setStatusTip(tr("Advanced archive options"));
            advanced->setWhatsThis(
                    tr("Press this button to show the available advanced options for archive access."));
            advanced->setContentsMargins(0, 0, 0, 0);
            {
                QSizePolicy policy(advanced->sizePolicy());
                policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
                advanced->setSizePolicy(policy);
            }
            pane->addWidget(advanced, 0, 1, 1, 1);

            QMenu *advancedMenu = new QMenu(advanced);
            advanced->setMenu(advancedMenu);

            archiveInput->clip = new QAction(tr("&Clip Data"), advancedMenu);
            advancedMenu->addAction(archiveInput->clip);
            archiveInput->clip->setCheckable(true);
            archiveInput->clip->setChecked(true);
            archiveInput->clip->setToolTip(tr("Clip data to the specified bounds"));
            archiveInput->clip->setStatusTip(tr("Data clipping"));
            archiveInput->clip
                        ->setWhatsThis(
                                tr("When enabled, data are clipped to the bounds specified even if the underlying values have start or end times that exceed them.  Any data that intersect the time range are loaded regardless."));

            archiveInput->addRequested = new QAction(tr("&Add Requested"), advancedMenu);
            advancedMenu->addAction(archiveInput->addRequested);
            archiveInput->addRequested->setCheckable(true);
            archiveInput->addRequested->setChecked(true);
            archiveInput->addRequested->setToolTip(tr("Automatically add requested inputs"));
            archiveInput->addRequested->setStatusTip(tr("Add requested inputs"));
            archiveInput->addRequested
                        ->setWhatsThis(
                                tr("When enabled, any inputs requested by the processing chain are automatically added to the processing."));

            archiveInput->enableDefault = new QAction(tr("Enable &Default Station"), advancedMenu);
            advancedMenu->addAction(archiveInput->enableDefault);
            archiveInput->enableDefault->setCheckable(true);
            archiveInput->enableDefault->setChecked(true);
            archiveInput->enableDefault->setToolTip(tr("Load data from the default station"));
            archiveInput->enableDefault->setStatusTip(tr("Default station"));
            archiveInput->enableDefault
                        ->setWhatsThis(
                                tr("When enabled, the default global station provides an underlay of data when available."));

            archiveInput->enableMeta = new QAction(tr("Enable Automatic &Metadata"), advancedMenu);
            advancedMenu->addAction(archiveInput->enableMeta);
            archiveInput->enableMeta->setCheckable(true);
            archiveInput->enableMeta->setChecked(true);
            archiveInput->enableMeta->setToolTip(tr("Load metadata for all normal data requested"));
            archiveInput->enableMeta->setStatusTip(tr("Enable metadata"));
            archiveInput->enableMeta
                        ->setWhatsThis(
                                tr("When enabled, metadata are automatically included for all requested data."));

            archiveInput->setModified = new QAction(tr("Set Modified &Time"), advancedMenu);
            advancedMenu->addAction(archiveInput->setModified);
            archiveInput->setModified->setToolTip(tr("Set the minim"));
            archiveInput->setModified->setStatusTip(tr("Enable metadata"));
            archiveInput->setModified
                        ->setWhatsThis(
                                tr("When enabled, metadata are automatically included for all requested data."));
            archiveInput->setModified->setProperty("time", FP::undefined());
            connect(archiveInput->setModified, SIGNAL(triggered(bool)), this,
                    SLOT(archiveShowSetModified()));

            archiveInput->selection = new VariableSelect(archiveInput->pane);
            pane->addWidget(archiveInput->selection, 1, 0, 1, -1);
        }

        if (mode != Mode_ViewTabular) {
            inputSelectType->addItem(tr("Read data from a file"), Input_File);

            fileInput = new FileInput;

            fileInput->pane = new QWidget(container);
            layout->addWidget(fileInput->pane, 1);
            QGridLayout *pane = new QGridLayout(fileInput->pane);
            pane->setContentsMargins(0, 0, 0, 0);
            fileInput->pane->setLayout(pane);
            pane->setColumnStretch(1, 1);
            pane->setRowStretch(2, 1);

            QLabel *label = new QLabel(
                    tr("Data will be read to the specified file in native CPD3 format.  If you need data converted from another format you must select a conversion component to ingest it."),
                    fileInput->pane);
            label->setWordWrap(true);
            pane->addWidget(label, 0, 0, 1, -1);

            label = new QLabel(tr("&File:"), fileInput->pane);
            pane->addWidget(label, 1, 0, 1, 1);

            fileInput->target = new QLineEdit(fileInput->pane);
            pane->addWidget(fileInput->target, 1, 1, 1, 1);
            label->setBuddy(fileInput->target);
            fileInput->target->setToolTip(tr("The filename that data will be read from"));
            fileInput->target->setStatusTip(tr("Input file"));
            fileInput->target
                     ->setWhatsThis(
                             tr("This sets the input file name that data will be read from."));

            QPushButton *button = new QPushButton(
                    QApplication::style()->standardIcon(QStyle::SP_DialogSaveButton), QString(),
                    fileInput->pane);
            button->setContentsMargins(0, 0, 0, 0);
            pane->addWidget(button, 1, 2, 1, 1);
            button->setToolTip(tr("Select input file"));
            button->setStatusTip(tr("Input file selection"));
            button->setWhatsThis(tr("Press this button to select the file to read data from."));
            connect(button, SIGNAL(clicked(bool)), this, SLOT(inputFileShowSelection()));

            pane->addWidget(new QWidget(fileInput->pane), 2, 1, 1, -1);
        }

        if (mode != Mode_ViewTabular) {
            inputSelectType->addItem(tr("Convert or process data"), Input_Component);

            componentInput = new ComponentInput;

            componentInput->pane = new QWidget(container);
            layout->addWidget(componentInput->pane, 1);
            QVBoxLayout *pane = new QVBoxLayout(componentInput->pane);
            pane->setContentsMargins(0, 0, 0, 0);
            componentInput->pane->setLayout(pane);

            componentInput->select = new QComboBox(fileInput->pane);
            pane->addWidget(componentInput->select);
            componentInput->select->setToolTip(tr("Select the processing component"));
            componentInput->select->setStatusTip(tr("Select component"));
            componentInput->select
                          ->setWhatsThis(
                                  tr("This is the list of available components for input generation."));
            connect(componentInput->select, SIGNAL(currentIndexChanged(int)), this,
                    SLOT(inputComponentSelected()));

            componentInput->description = new QLabel(fileInput->pane);
            pane->addWidget(componentInput->description);
            componentInput->description->setWordWrap(true);

            componentInput->basicPane = new QWidget(fileInput->pane);
            pane->addWidget(componentInput->basicPane);
            componentInput->basicPane->setVisible(false);
            QVBoxLayout *basicLayout = new QVBoxLayout(componentInput->basicPane);
            componentInput->basicPane->setLayout(basicLayout);
            basicLayout->setContentsMargins(0, 0, 0, 0);


            componentInput->filePane = new QWidget(componentInput->basicPane);
            basicLayout->addWidget(componentInput->filePane);
            QHBoxLayout *line = new QHBoxLayout(componentInput->filePane);
            componentInput->filePane->setLayout(line);
            line->setContentsMargins(0, 0, 0, 0);

            QLabel *label = new QLabel(tr("&File:"), componentInput->filePane);
            line->addWidget(label);

            componentInput->fileTarget = new QLineEdit(componentInput->filePane);
            line->addWidget(componentInput->fileTarget, 1);
            label->setBuddy(componentInput->fileTarget);
            componentInput->fileTarget->setToolTip(tr("The filename that data will be read from"));
            componentInput->fileTarget->setStatusTip(tr("Input file"));
            componentInput->fileTarget
                          ->setWhatsThis(
                                  tr("This sets the input file name that data will be read from."));

            QPushButton *button = new QPushButton(
                    QApplication::style()->standardIcon(QStyle::SP_DialogOpenButton), QString(),
                    componentInput->filePane);
            button->setContentsMargins(0, 0, 0, 0);
            line->addWidget(button);
            button->setToolTip(tr("Select input file"));
            button->setStatusTip(tr("Input file selection"));
            button->setWhatsThis(tr("Press this button to select the file to read data from."));
            connect(button, SIGNAL(clicked(bool)), this, SLOT(inputComponentFileShowSelection()));


            componentInput->timePane = new QWidget(fileInput->pane);
            pane->addWidget(componentInput->timePane);
            componentInput->timePane->setVisible(false);

            line = new QHBoxLayout(componentInput->timePane);
            componentInput->timePane->setLayout(line);
            line->setContentsMargins(0, 0, 0, 0);

            componentInput->timeRequired = new QLabel(tr("&Time:"), componentInput->timePane);
            line->addWidget(componentInput->timeRequired);

            componentInput->timeOptional = new QCheckBox(tr("Time:"), componentInput->timePane);
            line->addWidget(componentInput->timeOptional);
            componentInput->timeOptional->setVisible(false);
            componentInput->timeOptional->setChecked(true);
            componentInput->timeOptional->setToolTip(tr("Enable time range"));
            componentInput->timeOptional->setStatusTip(tr("Enable time"));
            componentInput->timeOptional
                          ->setWhatsThis(
                                  tr("When enabled, a time range is passed to the processor."));
            connect(componentInput->timeOptional, SIGNAL(toggled(bool)), this,
                    SLOT(inputComponentTimeEnableChanged()));

            componentInput->time = new TimeBoundSelection(componentInput->timePane);
            line->addWidget(componentInput->time, 1);
            componentInput->timeRequired->setBuddy(componentInput->time);
            componentInput->time->setToolTip(tr("The time range passed to the component"));
            componentInput->time->setStatusTip(tr("Component time range"));
            componentInput->time
                          ->setWhatsThis(
                                  tr("This sets the time range that is requested for data to be generated."));
            componentInput->time->setAllowUndefinedStart(true);
            componentInput->time->setAllowUndefinedEnd(true);
            {
                QSizePolicy policy(componentInput->time->sizePolicy());
                policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
                componentInput->time->setSizePolicy(policy);
            }


            componentInput->singleStationPane = new QWidget(fileInput->pane);
            pane->addWidget(componentInput->singleStationPane);
            componentInput->singleStationPane->setVisible(false);

            line = new QHBoxLayout(componentInput->singleStationPane);
            componentInput->singleStationPane->setLayout(line);
            line->setContentsMargins(0, 0, 0, 0);

            label = new QLabel(tr("&Station:"), componentInput->singleStationPane);
            line->addWidget(label);

            componentInput->singleStation = new QComboBox(componentInput->singleStationPane);
            line->addWidget(componentInput->singleStation, 1);
            label->setBuddy(componentInput->singleStation);
            componentInput->singleStation->setInsertPolicy(QComboBox::NoInsert);
            componentInput->singleStation->setEditable(true);
            componentInput->singleStation->setToolTip(tr("The selected GAW station"));
            componentInput->singleStation->setStatusTip(tr("Component station"));
            componentInput->singleStation
                          ->setWhatsThis(
                                  tr("This is the GAW station code the input component will operate on."));
            {
                QSizePolicy policy(componentInput->singleStation->sizePolicy());
                policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
                componentInput->singleStation->setSizePolicy(policy);
            }


            componentInput->multipleStationPane = new QWidget(fileInput->pane);
            pane->addWidget(componentInput->multipleStationPane);
            componentInput->multipleStationPane->setVisible(false);

            line = new QHBoxLayout(componentInput->multipleStationPane);
            componentInput->multipleStationPane->setLayout(line);
            line->setContentsMargins(0, 0, 0, 0);

            button = new QPushButton(tr("Stations"), componentInput->multipleStationPane);
            line->addWidget(button, 1);
            button->setToolTip(tr("The selected GAW stations"));
            button->setStatusTip(tr("Component station"));
            button->setWhatsThis(
                    tr("Use this button to select the GAW stations the component will operate on."));
            componentInput->multipleStationMenu = new QMenu(button);
            button->setMenu(componentInput->multipleStationMenu);

            componentInput->multipleStations =
                    new QActionGroup(componentInput->multipleStationMenu);
            componentInput->multipleStations->setExclusive(false);

            componentInput->multipleStationSeparator =
                    componentInput->multipleStationMenu->addSeparator();
            componentInput->multipleStationSeparator->setVisible(false);

            QAction *action = componentInput->multipleStationMenu->addAction(tr("&Add Station"));
            action->setToolTip(tr("Add another station"));
            action->setStatusTip(tr("Add a station"));
            action->setWhatsThis(tr("Use this to add a station not currently listed."));
            connect(action, SIGNAL(triggered(bool)), this, SLOT(inputComponentAddStation()));

            componentInput->options = new OptionEditor(componentInput->pane);
            pane->addWidget(componentInput->options, 1);
        }

        if (mode != Mode_ViewTabular) {
            inputSelectType->addItem(tr("Multiplex other data sources"), Input_Multiplexer);

            multiplexerInput = new MultiplexerInput;

            multiplexerInput->pane = new QWidget(container);
            layout->addWidget(multiplexerInput->pane, 1);
            multiplexerInput->layout = new QVBoxLayout(multiplexerInput->pane);
            multiplexerInput->layout->setContentsMargins(0, 0, 0, 0);
            multiplexerInput->pane->setLayout(multiplexerInput->layout);

            QPushButton
                    *button = new QPushButton(tr("&Add Multiplexed Input"), multiplexerInput->pane);
            multiplexerInput->layout->addWidget(button);
            button->setToolTip(tr("Add a new input stream"));
            button->setStatusTip(tr("Add an Input Stream"));
            button->setWhatsThis(
                    tr("Press this button to add another input to be multiplexed together."));
            connect(button, SIGNAL(clicked(bool)), this, SLOT(inputMultiplexerAdd()));

            multiplexerInput->layout->addStretch(1);
        }

        if (inputSelectType->count() < 2)
            inputSelectType->setVisible(false);
        connect(inputSelectType, SIGNAL(currentIndexChanged(int)), this, SLOT(inputTypeChanged()));
    }

    if (mode != Mode_ViewTabular) {
        addFilter = new QPushButton(tr("Add Processing Stage"), mainScroll);
        stagesLayout->addWidget(addFilter);
        connect(addFilter, SIGNAL(clicked()), this, SLOT(newFilter()));
    }

    if (mode != Mode_MultiplexerInput) {
        container = new QWidget(mainScroll);
        stagesLayout->addWidget(container);
        QVBoxLayout *layout = new QVBoxLayout(container);
        container->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        outputSelectButton = new QPushButton(tr("Output"), container);
        outputSelectButton->setToolTip(tr("Select the output of data"));
        outputSelectButton->setStatusTip(tr("Data output"));
        outputSelectButton->setWhatsThis(
                tr("Press this button to show the output selection information.  This is used to set what is done with the data after processing."));
        layout->addWidget(outputSelectButton);
        outputSelectButton->setCheckable(true);
        stagesGroup->addButton(outputSelectButton);
        connect(outputSelectButton, SIGNAL(toggled(bool)), this, SLOT(updateSelected()));

        outputSelectPane = new QScrollArea(container);
        outputSelectPane->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        outputSelectPane->setWidgetResizable(true);
        container = new QWidget(outputSelectPane);
        outputSelectPane->setWidget(container);
        layout->addWidget(outputSelectPane, 1);

        layout = new QVBoxLayout(container);
        container->setLayout(layout);

        outputSelectType = new QComboBox(container);
        outputSelectType->setToolTip(tr("Select the conversion of data"));
        outputSelectType->setStatusTip(tr("Data output type"));
        outputSelectType->setWhatsThis(
                tr("This selects the general conversion and export that is performed on data after processing."));
        layout->addWidget(outputSelectType, 0);

        {
            outputSelectType->addItem(tr("Tabular display"), Output_Tabular);

            tabularOutput = new TabularOutput;

            tabularOutput->pane = new QWidget(container);
            layout->addWidget(tabularOutput->pane, 1);
            QVBoxLayout *pane = new QVBoxLayout(tabularOutput->pane);
            pane->setContentsMargins(0, 0, 0, 0);
            tabularOutput->pane->setLayout(pane);

            QLabel *label = new QLabel(
                    tr("Data will be shown in a new window with a table representing values at their start times."),
                    tabularOutput->pane);
            label->setWordWrap(true);
            pane->addWidget(label);

            pane->addStretch(1);
        }

        if (mode != Mode_ViewTabular) {
            outputSelectType->addItem(tr("Graphical display"), Output_Display);

            displayOutput = new DisplayOutput;

            displayOutput->pane = new QWidget(container);
            layout->addWidget(displayOutput->pane, 1);
            QVBoxLayout *pane = new QVBoxLayout(displayOutput->pane);

            displayOutput->type = new QComboBox(displayOutput->pane);
            pane->addWidget(displayOutput->type);
            displayOutput->type->setToolTip(tr("Select the graphical display type"));
            displayOutput->type->setStatusTip(tr("Display type"));
            displayOutput->type
                         ->setWhatsThis(
                                 tr("This selects the general type of graphical display created from the input data."));
            displayOutput->type->addItem(tr("Timeseries"), DisplayComponent::Display_TimeSeries2D);
            displayOutput->type->addItem(tr("Density Plot"), DisplayComponent::Display_Density2D);
            displayOutput->type->addItem(tr("Scatter Plot"), DisplayComponent::Display_Scatter2D);
            displayOutput->type->addItem(tr("Cycle Plot"), DisplayComponent::Display_Cycle2D);
            displayOutput->type->addItem(tr("CDF"), DisplayComponent::Display_CDF);
            displayOutput->type->addItem(tr("PDF"), DisplayComponent::Display_PDF);
            displayOutput->type->addItem(tr("Allan Plot"), DisplayComponent::Display_Allan);
            displayOutput->type->addItem(tr("Generic External"), DisplayComponent::Display_Layout);
            connect(displayOutput->type, SIGNAL(currentIndexChanged(int)), this,
                    SLOT(outputDisplayTypeChanged()));

            displayOutput->options = new OptionEditor(displayOutput->pane);
            pane->addWidget(displayOutput->options, 1);

            outputDisplayTypeChanged();
        }

        if (mode != Mode_ViewTabular) {
            outputSelectType->addItem(tr("Write data to a file"), Output_DataToFile);

            fileOutput = new FileOutput;

            fileOutput->pane = new QWidget(container);
            layout->addWidget(fileOutput->pane, 1);
            QGridLayout *pane = new QGridLayout(fileOutput->pane);
            pane->setContentsMargins(0, 0, 0, 0);
            fileOutput->pane->setLayout(pane);
            pane->setColumnStretch(1, 1);
            pane->setRowStretch(2, 1);

            QLabel *label = new QLabel(
                    tr("Data will be written to the specified file in native CPD3 format.  This format is only suitable for later processing with CPD3, it cannot be directly read by other programs.  To export data in a format usable by other programs, change the output mode to use a conversion method."),
                    fileOutput->pane);
            label->setWordWrap(true);
            pane->addWidget(label, 0, 0, 1, -1);

            label = new QLabel(tr("&File:"), fileOutput->pane);
            pane->addWidget(label, 1, 0, 1, 1);

            fileOutput->target = new QLineEdit(fileOutput->pane);
            pane->addWidget(fileOutput->target, 1, 1, 1, 1);
            label->setBuddy(fileOutput->target);
            fileOutput->target->setToolTip(tr("The filename that data will be written to"));
            fileOutput->target->setStatusTip(tr("Output file"));
            fileOutput->target
                      ->setWhatsThis(
                              tr("This sets the output file name that data will be written to."));

            QPushButton *button = new QPushButton(
                    QApplication::style()->standardIcon(QStyle::SP_DialogSaveButton), QString(),
                    fileOutput->pane);
            button->setContentsMargins(0, 0, 0, 0);
            pane->addWidget(button, 1, 2, 1, 1);
            button->setToolTip(tr("Select output file"));
            button->setStatusTip(tr("Output file selection"));
            button->setWhatsThis(tr("Press this button to select the file to write data to."));
            connect(button, SIGNAL(clicked(bool)), this, SLOT(outputFileShowSelection()));

            pane->addWidget(new QWidget(fileOutput->pane), 2, 1, 1, -1);
        }

        if (mode != Mode_ViewTabular) {
            outputSelectType->addItem(tr("Convert or process data"), Output_Component);

            componentOutput = new ComponentOutput;

            componentOutput->pane = new QWidget(container);
            layout->addWidget(componentOutput->pane, 1);
            QVBoxLayout *pane = new QVBoxLayout(componentOutput->pane);
            pane->setContentsMargins(0, 0, 0, 0);
            componentOutput->pane->setLayout(pane);

            componentOutput->select = new QComboBox(fileOutput->pane);
            pane->addWidget(componentOutput->select);
            componentOutput->select->setToolTip(tr("Select the processing component"));
            componentOutput->select->setStatusTip(tr("Select component"));
            componentOutput->select
                           ->setWhatsThis(
                                   tr("This is the list of available components for output processing."));
            connect(componentOutput->select, SIGNAL(currentIndexChanged(int)), this,
                    SLOT(outputComponentSelected()));

            componentOutput->description = new QLabel(fileOutput->pane);
            pane->addWidget(componentOutput->description);
            componentOutput->description->setWordWrap(true);

            componentOutput->filePane = new QWidget(fileOutput->pane);
            pane->addWidget(componentOutput->filePane);
            QHBoxLayout *line = new QHBoxLayout(componentOutput->filePane);
            componentOutput->filePane->setLayout(line);
            line->setContentsMargins(0, 0, 0, 0);
            componentOutput->filePane->setVisible(false);

            componentOutput->fileEnable = new QCheckBox(tr("&File:"), componentOutput->filePane);
            line->addWidget(componentOutput->fileEnable);
            componentOutput->fileEnable
                           ->setToolTip(tr("When selected output will be written to a file"));
            componentOutput->fileEnable->setStatusTip(tr("Output to file"));
            componentOutput->fileEnable
                           ->setWhatsThis(
                                   tr("When selected, the output will be written to the specified file.  When disabled, the output is displayed in a new window."));
            componentOutput->fileEnable->setChecked(false);
            connect(componentOutput->fileEnable, SIGNAL(toggled(bool)), this,
                    SLOT(outputComponentFileEnableChanged()));

            componentOutput->fileTarget = new QLineEdit(componentOutput->filePane);
            line->addWidget(componentOutput->fileTarget, 1);
            componentOutput->fileTarget
                           ->setToolTip(tr("The filename that the output will be written to"));
            componentOutput->fileTarget->setStatusTip(tr("Output file"));
            componentOutput->fileTarget
                           ->setWhatsThis(
                                   tr("This sets the output file name that data will be read from."));
            componentOutput->fileTarget->setEnabled(false);

            componentOutput->fileSave = new QPushButton(
                    QApplication::style()->standardIcon(QStyle::SP_DialogSaveButton), QString(),
                    componentOutput->filePane);
            componentOutput->fileSave->setContentsMargins(0, 0, 0, 0);
            line->addWidget(componentOutput->fileSave);
            componentOutput->fileSave->setToolTip(tr("Select output file"));
            componentOutput->fileSave->setStatusTip(tr("Output file selection"));
            componentOutput->fileSave
                           ->setWhatsThis(
                                   tr("Press this button to select the file to read data from."));
            componentOutput->fileSave->setEnabled(false);
            connect(componentOutput->fileSave, SIGNAL(clicked(bool)), this,
                    SLOT(outputComponentFileShowSelection()));

            componentOutput->options = new OptionEditor(componentOutput->pane);
            pane->addWidget(componentOutput->options, 1);
        }

        if (outputSelectType->count() < 2)
            outputSelectType->setVisible(false);
        connect(outputSelectType, SIGNAL(currentIndexChanged(int)), this,
                SLOT(outputTypeChanged()));
    }

    if (inputSelectButton != NULL && addFilter == NULL && outputSelectButton == NULL) {
        inputSelectButton->setVisible(false);
    } else if (outputSelectButton != NULL && addFilter == NULL && inputSelectButton == NULL) {
        outputSelectButton->setVisible(false);
    } else if (mode == Mode_ViewTabular) {
        if (inputSelectButton != NULL)
            inputSelectButton->setVisible(false);
        if (outputSelectButton != NULL)
            outputSelectButton->setVisible(false);
    }

    QDialogButtonBox *buttons = new QDialogButtonBox(this);
    buttons->addButton(QDialogButtonBox::Ok);
    if (mode != Mode_MultiplexerInput)
        buttons->addButton(QDialogButtonBox::Cancel);
    mainLayout->addWidget(buttons);
    connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));
    connect(buttons, SIGNAL(accepted()), this, SLOT(completeDialog()));

    availableComponents = std::async(std::launch::async, [] {
        GUIChainComponentLoadResult result;
        for (auto &add : ComponentLoader::list("gui")) {
            if (add.second.interface("externalconverter") ||
                    add.second.interface("externalsource")) {
                result.input.emplace(add.first, std::move(add.second));
            } else if (add.second.interface("externalsink")) {
                result.output.emplace(add.first, std::move(add.second));
            } else if (add.second.interface("processingstage")) {
                result.filter.emplace(add.first, std::move(add.second));
            }
        }
        return result;
    });
    Threading::pollFunctor(this, [this]() {
        if (availableComponents.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
            return true;
        QMetaObject::invokeMethod(this, "availableComponentsReady", Qt::QueuedConnection);
        return false;
    });

    inputTypeChanged();
    outputTypeChanged();
    updateSelected();
}

GUIChainDialog::~GUIChainDialog()
{
    if (archiveInput != NULL)
        delete archiveInput;
    if (fileInput != NULL)
        delete fileInput;
    if (componentInput != NULL)
        delete componentInput;
    if (multiplexerInput != NULL)
        delete multiplexerInput;
    if (tabularOutput != NULL)
        delete tabularOutput;
    if (displayOutput != NULL)
        delete displayOutput;
    if (fileOutput != NULL)
        delete fileOutput;
    if (componentOutput != NULL)
        delete componentOutput;
}

QSize GUIChainDialog::sizeHint() const
{ return QSize(800, 600); }

void GUIChainDialog::setLoaded(const SequenceName::Component &station,
                               const SequenceName::Component &archive,
                               double start,
                               double end)
{
    if (!FP::equal(start, this->start) || !FP::equal(end, this->end)) {
        if (archiveInput != NULL) {
            archiveInput->bounds->setBounds(start, end);
        }
        if (componentInput != NULL) {
            componentInput->time->setBounds(start, end);
        }
    }

    this->station = station;
    this->archive = archive;
    this->start = start;
    this->end = end;
}

void GUIChainDialog::load(const SequenceName::Component &station,
                          const SequenceName::Component &archive,
                          double start,
                          double end)
{
    setLoaded(station, archive, start, end);

    struct Context {
        GUIChainDialog *dialog;
        std::shared_ptr<Archive::Access> access;
        std::shared_ptr<Archive::Access::ReadLock> lock;

        Context(GUIChainDialog *dlg) : dialog(dlg),
                                       access(std::make_shared<Archive::Access>()),
                                       lock(std::make_shared<Archive::Access::ReadLock>(*access))
        { }

        ~Context()
        {
            lock.reset();
            access.reset();
        }
    };

    auto context = std::make_shared<Context>(this);

    Archive::Selection selection(start, end);
    if (!station.empty())
        selection.stations.push_back(station);
    if (!archive.empty())
        selection.archives.push_back(archive);

    Threading::pollFuture(this, context->access->availableSelectedFuture(selection),
                          [context](const Archive::Access::SelectedComponents &result) {
                              context->dialog->availableDataReady(result);
                          });
}

void GUIChainDialog::availableDataReady(const Archive::Access::SelectedComponents &available)
{
    availableData = available;

    for (QList<GUIChainFilter *>::const_iterator filter = filters.constBegin(),
            end = filters.constEnd(); filter != end; ++filter) {
        (*filter)->dataAvailable(available);
    }

    if (archiveInput != NULL) {
        archiveInput->selection->setAvailableStations(available.stations);
        archiveInput->selection->setAvailableArchives(available.archives);
        archiveInput->selection->setAvailableVariables(available.variables);
        archiveInput->selection->setAvailableFlavors(available.flavors);
    }

    if (componentInput != NULL) {
        {
            QString selected(componentInput->singleStation->currentText());
            componentInput->singleStation->clear();
            for (const auto &station : available.stations) {
                auto display = QString::fromStdString(station);
                componentInput->singleStation->addItem(display.toUpper());
                if (selected.toLower() == display.toLower()) {
                    componentInput->singleStation
                                  ->setCurrentIndex(componentInput->singleStation->count() - 1);
                    selected.clear();
                }
            }
            if (!selected.isEmpty())
                componentInput->singleStation->lineEdit()->setText(selected);
        }

        {
            SequenceName::ComponentSet selected;
            QList<QAction *> actions(componentInput->multipleStations->actions());
            for (auto action : actions) {
                if (!action->isChecked())
                    continue;
                selected.insert(action->text().toLower().toStdString());
                componentInput->multipleStations->removeAction(action);
                componentInput->multipleStationMenu->removeAction(action);
                action->deleteLater();
            }

            componentInput->multipleStationSeparator
                          ->setVisible(!selected.empty() || !available.stations.empty());

            for (const auto &station : available.stations) {
                auto displayStation = QString::fromStdString(station);
                QAction *action =
                        componentInput->multipleStations->addAction(displayStation.toUpper());
                action->setCheckable(true);
                if (selected.erase(station) != 0) {
                    action->setChecked(true);
                } else {
                    action->setChecked(false);
                }

                componentInput->multipleStationMenu
                              ->insertAction(componentInput->multipleStationSeparator, action);
            }

            auto sorted = Util::to_qstringlist(selected);
            std::sort(sorted.begin(), sorted.end());
            for (const auto &station : sorted) {
                QAction *action = componentInput->multipleStations->addAction(station.toUpper());
                action->setCheckable(true);
                action->setChecked(true);
                componentInput->multipleStationMenu
                              ->insertAction(componentInput->multipleStationSeparator, action);
            }
        }

        componentInput->options->setAvailableStations(available.stations);
        componentInput->options->setAvailableArchives(available.archives);
        componentInput->options->setAvailableVariables(available.variables);
        componentInput->options->setAvailableFlavors(available.flavors);
    }

    if (displayOutput != NULL) {
        displayOutput->options->setAvailableStations(available.stations);
        displayOutput->options->setAvailableArchives(available.archives);
        displayOutput->options->setAvailableVariables(available.variables);
        displayOutput->options->setAvailableFlavors(available.flavors);
    }

    if (componentOutput != NULL) {
        componentOutput->options->setAvailableStations(available.stations);
        componentOutput->options->setAvailableArchives(available.archives);
        componentOutput->options->setAvailableVariables(available.variables);
        componentOutput->options->setAvailableFlavors(available.flavors);
    }

    if (multiplexerInput != NULL) {
        for (const auto &in : multiplexerInput->inputs) {
            in.dialog->availableDataReady(available);
        }
    }
}

void GUIChainDialog::availableComponentsReady()
{
    if (availableComponents.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
        return;

    if (componentInput) {
        componentInput->select->clear();
        std::vector<ComponentLoader::Information> sorted;
        for (const auto &add : availableComponents.get().input) {
            sorted.emplace_back(add.second);
        }
        std::sort(sorted.begin(), sorted.end(), ComponentLoader::Information::DisplayOrder());
        for (const auto &add : sorted) {
            componentInput->select
                          ->addItem(tr("%1 - %2", "component format").arg(add.componentName(),
                                                                          add.title()),
                                    add.componentName());
        }
    }

    if (componentOutput) {
        componentOutput->select->clear();
        std::vector<ComponentLoader::Information> sorted;
        for (const auto &add : availableComponents.get().output) {
            sorted.emplace_back(add.second);
        }
        std::sort(sorted.begin(), sorted.end(), ComponentLoader::Information::DisplayOrder());
        for (const auto &add : sorted) {
            componentOutput->select
                           ->addItem(tr("%1 - %2", "component format").arg(add.componentName(),
                                                                           add.title()),
                                     add.componentName());
        }
    }
}

void GUIChainDialog::updateSelected()
{
    int index = 0;
    if (inputSelectPane != NULL) {
        if (inputSelectButton->isChecked()) {
            inputSelectPane->setVisible(true);
            stagesLayout->setStretch(index, 1);
        } else {
            inputSelectPane->setVisible(false);
            stagesLayout->setStretch(index, 0);
        }
        ++index;
    }

    for (int i = 0, max = filters.size(); i < max; i++) {
        filters.at(i)->updateActive(index, i == 0, i == max - 1);
        ++index;
    }

    if (addFilter != NULL) {
        ++index;
    }

    if (outputSelectButton != NULL) {
        if (outputSelectButton->isChecked()) {
            outputSelectPane->setVisible(true);
            stagesLayout->setStretch(index, 1);
        } else {
            outputSelectPane->setVisible(false);
            stagesLayout->setStretch(index, 0);
        }
        ++index;
    }
}

void GUIChainDialog::waitForComponents()
{
    if (availableComponents.wait_for(std::chrono::seconds(0)) != std::future_status::timeout)
        return;

    QProgressDialog waitDialog(tr("Loading Components"), QString(), 0, 0, this);
    waitDialog.setWindowModality(Qt::ApplicationModal);
    waitDialog.setCancelButton(0);

    Threading::pollFunctor(&waitDialog, [this, &waitDialog]() {
        if (availableComponents.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
            return true;
        waitDialog.close();
        return false;
    });

    if (availableComponents.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
        waitDialog.exec();

    availableComponents.wait();
}

void GUIChainDialog::newFilter()
{
    waitForComponents();

    GUIChainComponentSelectionDialog dialog(availableComponents.get().filter);
    dialog.setWindowModality(Qt::ApplicationModal);
    dialog.setWindowTitle(tr("Add Processing Stage"));
    if (!dialog.exec())
        return;

    auto info = ComponentLoader::information(dialog.getSelectedComponent());
    ProcessingStageComponent *component = qobject_cast<ProcessingStageComponent *>(info.second);
    if (!component)
        return;

    GUIChainFilter *filter = new GUIChainFilter(component, info.first, this);
    int index = 0;
    if (inputSelectPane != NULL)
        ++index;
    index += filters.size();
    filters.append(filter);

    filter->dataAvailable(availableData);

    stagesLayout->insertWidget(index, filter);
    filter->setActive();
}

void GUIChainDialog::archiveShowSetModified()
{
    if (archiveInput == NULL)
        return;
    double time = archiveInput->setModified->property("time").toDouble();

    QDialog dialog(this);
    dialog.setWindowModality(Qt::ApplicationModal);
    dialog.setWindowTitle(tr("Set Archive Minimum Modified"));
    QGridLayout *layout = new QGridLayout(&dialog);
    layout->setColumnStretch(1, 1);
    layout->setRowStretch(1, 1);

    QLabel *label = new QLabel(tr("Data &modified after:"), &dialog);
    layout->addWidget(label, 0, 0);

    TimeSingleSelection *editor = new TimeSingleSelection(&dialog);
    editor->setAllowUndefined(true);
    editor->setTime(archiveInput->setModified->property("time").toDouble());
    layout->addWidget(editor, 0, 1);
    label->setBuddy(editor);

    layout->addWidget(new QWidget(&dialog), 1, 0, 1, -1);

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons, 2, 0, 1, -1);

    connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

    if (!dialog.exec())
        return;

    time = editor->getTime();
    archiveInput->setModified->setProperty("time", time);

    if (!FP::defined(time)) {
        archiveInput->setModified->setText(tr("Set Modified &Time"));
    } else {
        archiveInput->setModified
                    ->setText(tr("Set Modified &Time (%1)").arg(GUITime::formatTime(time)));
    }
}

void GUIChainDialog::inputFileShowSelection()
{
    if (fileInput == NULL)
        return;

    QStringList filters;
    filters << tr("CPD3 data (*.c3d)");
    filters << tr("CPD3 binary data (*.c3r)");
    filters << tr("CPD3 XML data (*.xml)");
    filters << tr("Any files (*)");

    QString file(QFileDialog::getOpenFileName(this, QString(), fileInput->target->text(),
                                              filters.join(";;")));
    if (file.isEmpty())
        return;

    fileInput->target->setText(file);
}

void GUIChainDialog::inputComponentSelected()
{
    if (componentInput == NULL)
        return;
    if (availableComponents.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
        return;

    int idx = componentInput->select->currentIndex();
    if (idx < 0 || idx >= componentInput->select->count())
        return;
    QString componentName(componentInput->select->itemData(idx).toString());
    componentInput->description
                  ->setText(availableComponents.get().input.at(componentName).description());

    QObject *component = ComponentLoader::create(componentName);
    if (!component)
        return;

    ExternalConverterComponent *basic = qobject_cast<ExternalConverterComponent *>(component);
    if (basic) {
        componentInput->basicPane->setVisible(true);
        componentInput->timePane->setVisible(false);
        componentInput->filePane->setVisible(basic->requiresInputDevice());
        componentInput->options->setOptions(basic->getOptions());
        return;
    }

    ExternalSourceComponent *external = qobject_cast<ExternalSourceComponent *>(component);
    if (external) {
        componentInput->basicPane->setVisible(false);
        componentInput->timePane->setVisible(true);

        if (external->ingressRequiresTime()) {
            componentInput->timeOptional->setChecked(true);
            componentInput->timeOptional->setVisible(false);
            componentInput->timeRequired->setVisible(true);
        } else {
            componentInput->timeOptional->setVisible(true);
            componentInput->timeRequired->setVisible(false);
        }

        if (external->ingressRequireStations() == 1 && external->ingressAllowStations() == 1) {
            componentInput->singleStationPane->setVisible(true);
            componentInput->multipleStationPane->setVisible(false);
        } else if (external->ingressAllowStations() > 0) {
            componentInput->singleStationPane->setVisible(false);
            componentInput->multipleStationPane->setVisible(true);
        } else {
            componentInput->singleStationPane->setVisible(false);
            componentInput->multipleStationPane->setVisible(false);
        }

        componentInput->options->setOptions(external->getOptions());
        return;
    }

    componentInput->basicPane->setVisible(false);
    componentInput->timePane->setVisible(false);
    componentInput->singleStationPane->setVisible(false);
    componentInput->multipleStationPane->setVisible(false);
    componentInput->options->setOptions(ComponentOptions());

    qCDebug(log_gui_chain) << "Unknown component type for" << componentName;
}

void GUIChainDialog::inputComponentFileShowSelection()
{
    if (componentInput == NULL)
        return;

    QString file(QFileDialog::getOpenFileName(this, QString(), componentInput->fileTarget->text()));
    if (file.isEmpty())
        return;

    componentInput->fileTarget->setText(file);
}

void GUIChainDialog::inputComponentTimeEnableChanged()
{
    if (componentInput == NULL)
        return;

    componentInput->time->setEnabled(componentInput->timeOptional->isChecked());
}

void GUIChainDialog::inputComponentAddStation()
{
    if (componentInput == NULL)
        return;

    QString station(QInputDialog::getText(this, tr("Add Station"), tr("Station")));
    station = station.trimmed().toUpper();
    if (station.isEmpty())
        return;
    QList<QAction *> actions(componentInput->multipleStations->actions());
    for (QList<QAction *>::const_iterator action = actions.constBegin(), end = actions.constEnd();
            action != end;
            ++action) {
        if ((*action)->text() == station) {
            (*action)->setChecked(true);
            return;
        }
    }

    componentInput->multipleStationSeparator->setVisible(true);

    QAction *action = componentInput->multipleStations->addAction(station);
    action->setCheckable(true);
    action->setChecked(true);
    componentInput->multipleStationMenu
                  ->insertAction(componentInput->multipleStationSeparator, action);
}

void GUIChainDialog::inputMultiplexerAdd()
{
    if (multiplexerInput == NULL)
        return;

    GUIChainDialog *mux = new GUIChainDialog(Mode_MultiplexerInput, this);
    mux->setWindowModality(Qt::ApplicationModal);
    mux->setWindowTitle(tr("Configure Multiplexed Input"));

    mux->setLoaded(station, archive, start, end);
    mux->availableData = availableData;

    if (!mux->exec()) {
        mux->deleteLater();
        return;
    }

    MultiplexerInput::Input input;
    input.dialog = mux;
    input.pane = new QWidget(multiplexerInput->pane);
    QHBoxLayout *line = new QHBoxLayout(input.pane);
    line->setContentsMargins(0, 0, 0, 0);

    input.modify = new QPushButton(tr("Edit Input %1").arg(multiplexerInput->inputs.size() + 1),
                                   input.pane);
    {
        QSizePolicy policy(input.modify->sizePolicy());
        policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
        input.modify->setSizePolicy(policy);
    }
    line->addWidget(input.modify, 1);
    input.modify->setToolTip(tr("Modify the multiplexed input"));
    input.modify->setStatusTip(tr("Modify Input"));
    input.modify
         ->setWhatsThis(tr("Press this button to re-open the configuration dialog for the input."));
    input.modify->setProperty("multiplexerIndex", multiplexerInput->inputs.size());
    connect(input.modify, SIGNAL(clicked(bool)), this, SLOT(inputMultiplexerModify()));

    input.remove =
            new QPushButton(QApplication::style()->standardIcon(QStyle::SP_TrashIcon), QString(),
                            input.pane);
    line->addWidget(input.remove);
    {
        QSizePolicy policy(input.remove->sizePolicy());
        policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
        input.remove->setSizePolicy(policy);
    }
    input.remove->setContentsMargins(0, 0, 0, 0);
    input.remove->setToolTip(tr("Remove the multiplexed input"));
    input.remove->setStatusTip(tr("Remove Input"));
    input.remove->setWhatsThis(tr("Press this button to remove the selected multiplexed input."));
    input.remove->setProperty("multiplexerIndex", multiplexerInput->inputs.size());
    connect(input.remove, SIGNAL(clicked(bool)), this, SLOT(inputMultiplexerRemove()));

    multiplexerInput->inputs.append(input);

    multiplexerInput->layout->insertWidget(multiplexerInput->layout->count() - 2, input.pane);
}

void GUIChainDialog::inputMultiplexerModify()
{
    if (multiplexerInput == NULL)
        return;
    int index = sender()->property("multiplexerIndex").toInt();
    if (index < 0 || index >= multiplexerInput->inputs.size())
        return;
    multiplexerInput->inputs[index].dialog->exec();
}

void GUIChainDialog::inputMultiplexerRemove()
{
    if (multiplexerInput == NULL)
        return;
    int index = sender()->property("multiplexerIndex").toInt();
    if (index < 0 || index >= multiplexerInput->inputs.size())
        return;

    multiplexerInput->layout->removeWidget(multiplexerInput->inputs[index].pane);
    multiplexerInput->inputs[index].pane->deleteLater();
    multiplexerInput->inputs.removeAt(index);

    index = 0;
    for (int max = multiplexerInput->inputs.size(); index < max; index++) {
        multiplexerInput->inputs[index].modify->setText(tr("Edit Input %1").arg(index + 1));
        multiplexerInput->inputs[index].modify->setProperty("multiplexerIndex", index);

        multiplexerInput->inputs[index].remove->setProperty("multiplexerIndex", index);
    }
}

void GUIChainDialog::outputFileShowSelection()
{
    if (fileOutput == NULL)
        return;

    QStringList filters;
    filters << tr("CPD3 data (*.c3d)");
    filters << tr("CPD3 binary data (*.c3r)");
    filters << tr("CPD3 XML data (*.xml)");
    filters << tr("CPD3 JSON data (*.json)");
    filters << tr("Any files (*)");

    QString selectedFilter;
    QString file(QFileDialog::getSaveFileName(this, QString(), fileOutput->target->text(),
                                              filters.join(";;"), &selectedFilter));
    if (file.isEmpty())
        return;

    QFileInfo info(file);
    if (info.completeSuffix().isEmpty() && !info.exists()) {
        if (selectedFilter == tr("CPD3 data (*.c3d)")) {
            file.append(tr(".c3d", "default extension"));
        } else if (selectedFilter == tr("CPD3 binary data (*.c3r)")) {
            file.append(tr(".c3r", "default extension"));
        } else if (selectedFilter == tr("CPD3 XML data (*.xml)")) {
            file.append(tr(".xml", "default extension"));
        } else if (selectedFilter == tr("CPD3 XML data (*.json)")) {
            file.append(tr(".json", "default extension"));
        }
    }

    fileOutput->target->setText(file);
}

void GUIChainDialog::outputComponentSelected()
{
    if (componentOutput == NULL)
        return;
    if (availableComponents.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
        return;

    int idx = componentOutput->select->currentIndex();
    if (idx < 0 || idx >= componentOutput->select->count())
        return;
    QString componentName(componentOutput->select->itemData(idx).toString());
    componentOutput->description
                   ->setText(availableComponents.get().output.at(componentName).description());

    QObject *component = ComponentLoader::create(componentName);
    if (!component)
        return;

    ExternalSinkComponent *basic = qobject_cast<ExternalSinkComponent *>(component);
    if (basic) {
        componentOutput->filePane->setVisible(basic->requiresOutputDevice());
        componentOutput->options->setOptions(basic->getOptions());
        return;
    }

    componentOutput->filePane->setVisible(false);
}

void GUIChainDialog::outputComponentFileShowSelection()
{
    if (componentOutput == NULL)
        return;

    QStringList filters;
    filters << tr("Text files (*.txt *.text)");
    filters << tr("Any files (*)");

    QString selectedFilter;
    QString file(QFileDialog::getSaveFileName(this, QString(), QString(), filters.join(";;"),
                                              &selectedFilter));
    if (file.isEmpty())
        return;

    QFileInfo info(file);
    if (info.completeSuffix().isEmpty() && !info.exists()) {
        if (selectedFilter == tr("Text files (*.txt *.text)")) {
            file.append(tr(".txt", "default extension"));
        }
    }

    componentOutput->fileTarget->setText(file);
}

void GUIChainDialog::outputComponentFileEnableChanged()
{
    if (componentOutput == NULL)
        return;

    bool enable = componentOutput->fileEnable->isChecked();
    componentOutput->fileTarget->setEnabled(enable);
    componentOutput->fileSave->setEnabled(enable);
}

void GUIChainDialog::outputDisplayTypeChanged()
{
    if (displayOutput == NULL)
        return;
    int idx = displayOutput->type->currentIndex();
    if (idx < 0 || idx >= displayOutput->type->count())
        return;
    idx = displayOutput->type->itemData(idx).toInt();
    displayOutput->options
                 ->setOptions(DisplayComponent::options((DisplayComponent::DisplayType) idx));
}

namespace {
enum {
    AckFatal_General = 0x1,
    AckWarning_GlobalSelection = 0x2,
    AckWarning_NoSelection = 0x4,
    AckWarning_NoMultiplexerInputs = 0x8,
    AckWarning_BadInputFile = 0x10,
    AckWarning_BadStation = 0x20,
};
}

bool GUIChainDialog::configurePipeline(StreamPipeline *target, QList<StreamPipeline *> &chains,
                                       QList<SinkMultiplexer *> &multiplexers,
                                       int &acknowledgedWarnings)
{
    int inputType = -1;
    if (inputSelectType != NULL) {
        inputType = inputSelectType->currentIndex();
        if (inputType >= 0 && inputType < inputSelectType->count())
            inputType = inputSelectType->itemData(inputType).toInt();
        else
            inputType = -1;
    }

    bool fixedInputs = false;
    double fixedStart = FP::undefined();
    double fixedEnd = FP::undefined();
    SequenceName::Set fixedUnits;

    switch (inputType) {
    case Input_Archive: {
        Q_ASSERT(archiveInput != NULL);

        fixedInputs = true;
        fixedStart = archiveInput->bounds->getStart();
        fixedEnd = archiveInput->bounds->getEnd();

        VariableSelect::SequenceMatchDefaults variableDefaults;
        if (!archive.empty())
            variableDefaults.setArchiveFanout(QStringList(QString::fromStdString(archive)));
        Variant::Write config = Variant::Write::empty();
        archiveInput->selection->writeSequenceMatch(config, variableDefaults);
        SequenceMatch::OrderedLookup variable(config);
        variable.registerExpected(station, archive);
        Util::merge(variable.knownInputs(), fixedUnits);

        break;
    }
    case Input_File:
        break;
    case Input_Component:
        break;
    case Input_Multiplexer:
        break;
    default:
        break;
    }

    if (fixedInputs) {
        for (QList<GUIChainFilter *>::const_iterator f = filters.constBegin(),
                end = filters.constEnd(); f != end; ++f) {
            if (!(*f)->addToPipeline(target, fixedStart, fixedEnd, fixedUnits))
                return false;
        }
    } else {
        for (QList<GUIChainFilter *>::const_iterator f = filters.constBegin(),
                end = filters.constEnd(); f != end; ++f) {
            if (!(*f)->addToPipeline(target))
                return false;
        }
    }

    switch (inputType) {
    case Input_Archive: {
        Q_ASSERT(archiveInput != NULL);

        VariableSelect::ArchiveDefaults defaults;
        defaults.setStation(station);
        defaults.setArchive(archive);

        Archive::Selection::List selections;

        if (archiveInput->addRequested->isChecked()) {
            for (const auto &name : target->requestedInputs()) {
                selections.emplace_back(name, fixedStart, fixedEnd);
            }
        }

        if (!selections.empty())
            defaults.setEmptySelectAll(false);

        Util::append(archiveInput->selection->getArchiveSelection(defaults, fixedStart, fixedEnd),
                     selections);

        for (auto &sel : selections) {
            sel.includeDefaultStation = archiveInput->enableDefault->isChecked();
            sel.includeMetaArchive = archiveInput->enableMeta->isChecked();
            sel.modifiedAfter = archiveInput->setModified->property("time").toDouble();
        }

        if (selections.empty()) {
            if (!(acknowledgedWarnings & AckWarning_NoSelection)) {
                acknowledgedWarnings |= AckWarning_NoSelection;
                QMessageBox::critical(this, tr("No Data selected"),
                                      tr("No data selected for processing."), QMessageBox::Ok);
            }
            return false;
        }

        bool hasGlobalSelection = false;
        for (const auto &sel : selections) {
            if (sel.stations.empty())
                hasGlobalSelection = true;
            if (sel.archives.empty())
                hasGlobalSelection = true;
            if (sel.variables.empty())
                hasGlobalSelection = true;
        }
        if (hasGlobalSelection && !(acknowledgedWarnings & AckWarning_GlobalSelection)) {
            acknowledgedWarnings |= AckWarning_GlobalSelection;
            if (QMessageBox::warning(this, tr("Global Selection"),
                                     tr("You have selected all available data in one or more parts (station, archive or variable).  This will result in a large amount of processing.  Do you want to proceed anyway?"),
                                     QMessageBox::Yes | QMessageBox::No, QMessageBox::No) !=
                    QMessageBox::Yes)
                return false;
        }

        if (!target->setInputArchive(selections,
                                     (archiveInput->clip->isChecked() ? Time::Bounds(fixedStart,
                                                                                     fixedEnd)
                                                                      : Time::Bounds()))) {
            if (!(acknowledgedWarnings & AckFatal_General)) {
                acknowledgedWarnings |= AckFatal_General;
                QMessageBox::critical(this, tr("Archive Input Error"),
                                      tr("Error setting archive input: %1").arg(
                                              target->getInputError()), QMessageBox::Ok);
            }
            return false;
        }
        break;
    }
    case Input_File: {
        Q_ASSERT(fileInput != NULL);
        QString name(fileInput->target->text());
        if (name.isEmpty()) {
            if (!(acknowledgedWarnings & AckWarning_BadInputFile)) {
                acknowledgedWarnings |= AckWarning_BadInputFile;
                QMessageBox::critical(this, tr("No Input File"),
                                      tr("You must specify an input file."), QMessageBox::Ok);
            }
            return false;
        }
        if (!target->setInputFile(name)) {
            if (!(acknowledgedWarnings & AckFatal_General)) {
                acknowledgedWarnings |= AckFatal_General;
                QMessageBox::critical(this, tr("File Input Error"),
                                      tr("Error setting file input: %1").arg(
                                              target->getInputError()), QMessageBox::Ok);
            }
            return false;
        }
        break;
    }
    case Input_Component: {
        Q_ASSERT(componentInput != NULL);

        int idx = componentInput->select->currentIndex();
        if (idx < 0 || idx >= componentInput->select->count())
            return false;
        QString componentName(componentInput->select->itemData(idx).toString());
        QObject *component = ComponentLoader::create(componentName);
        if (!component)
            return false;

        std::unique_ptr<ExternalConverter> input;
        QString fileName;

        ExternalConverterComponent *basic = qobject_cast<ExternalConverterComponent *>(component);
        if (basic) {
            if (basic->requiresInputDevice()) {
                fileName = componentInput->fileTarget->text();
                if (fileName.isEmpty()) {
                    if (!(acknowledgedWarnings & AckWarning_BadInputFile)) {
                        acknowledgedWarnings |= AckWarning_BadInputFile;
                        QMessageBox::critical(this, tr("No Input File"),
                                              tr("You must specify an input file."),
                                              QMessageBox::Ok);
                    }
                    return false;
                }
            }
            input.reset(basic->createDataIngress(componentInput->options->getOptions()));
        }

        ExternalSourceComponent *external = qobject_cast<ExternalSourceComponent *>(component);
        if (external && !input) {
            std::vector<SequenceName::Component> stations;

            if (external->ingressRequireStations() == 1 && external->ingressAllowStations() == 1) {
                QString add(componentInput->singleStation->currentText().trimmed().toLower());
                if (add.isEmpty()) {
                    if (!(acknowledgedWarnings & AckWarning_BadStation)) {
                        acknowledgedWarnings |= AckWarning_BadStation;
                        QMessageBox::critical(this, tr("No Input Station"),
                                              tr("You must specify a valid station."),
                                              QMessageBox::Ok);
                    }
                    return false;
                }
                stations.emplace_back(add.toStdString());
            } else if (external->ingressAllowStations() > 0) {
                int max = external->ingressAllowStations();
                for (auto action : componentInput->multipleStations->actions()) {
                    if (!action->isChecked())
                        continue;
                    stations.emplace_back(action->text().toLower().toStdString());
                    if (static_cast<int>(station.size()) >= max)
                        break;
                }
                if (static_cast<int>(stations.size()) < external->ingressRequireStations()) {
                    if (!(acknowledgedWarnings & AckWarning_BadStation)) {
                        acknowledgedWarnings |= AckWarning_BadStation;
                        QMessageBox::critical(this, tr("Insufficient Input Stations"),
                                              tr("You must select at least %n station(s).", "",
                                                 external->ingressRequireStations()),
                                              QMessageBox::Ok);
                    }
                    return false;
                }
            }

            if (!external->ingressRequiresTime() && !componentInput->timeOptional->isChecked()) {
                input.reset(external->createExternalIngress(componentInput->options->getOptions(),
                                                            stations));
            } else {
                input.reset(external->createExternalIngress(componentInput->options->getOptions(),
                                                            componentInput->time->getStart(),
                                                            componentInput->time->getEnd(),
                                                            stations));
            }
        }

        if (!input)
            return false;

        if (!fileName.isEmpty()) {
            if (!target->setInputGeneral(std::move(input), fileName)) {
                if (!(acknowledgedWarnings & AckFatal_General)) {
                    acknowledgedWarnings |= AckFatal_General;
                    QMessageBox::critical(this, tr("Input Error"),
                                          tr("Error setting input: %1").arg(
                                                  target->getInputError()), QMessageBox::Ok);
                }
                return false;
            }
        } else {
            if (!target->setInputGeneral(std::move(input))) {
                if (!(acknowledgedWarnings & AckFatal_General)) {
                    acknowledgedWarnings |= AckFatal_General;
                    QMessageBox::critical(this, tr("Input Error"),
                                          tr("Error setting input: %1").arg(
                                                  target->getInputError()), QMessageBox::Ok);
                }
                return false;
            }
        }

        break;
    }
    case Input_Multiplexer: {
        Q_ASSERT(multiplexerInput != NULL);

        if (multiplexerInput->inputs.isEmpty()) {
            if (!(acknowledgedWarnings & AckWarning_NoMultiplexerInputs)) {
                acknowledgedWarnings |= AckWarning_NoMultiplexerInputs;
                QMessageBox::critical(this, tr("No Multiplexer Inputs"),
                                      tr("You must specify one or more multiplexer inputs."),
                                      QMessageBox::Ok);
            }
            return false;
        }

        SinkMultiplexer *mux = new SinkMultiplexer;
        multiplexers.append(mux);
        for (QList<MultiplexerInput::Input>::const_iterator
                in = multiplexerInput->inputs.constBegin(),
                end = multiplexerInput->inputs.constEnd(); in != end; ++in) {
            chains.append(new StreamPipeline(false, false, false));
            chains.last()->setOutputIngress(mux->createSink());
            if (!in->dialog
                   ->configurePipeline(chains.last(), chains, multiplexers, acknowledgedWarnings))
                return false;
        }
        mux->sinkCreationComplete();
        StreamSink *in = target->setInputExternal();
        if (!in && !(acknowledgedWarnings & AckFatal_General)) {
            acknowledgedWarnings |= AckFatal_General;
            QMessageBox::critical(this, tr("Multiplexer Input Error"),
                                  tr("Error setting input: %1").arg(target->getInputError()),
                                  QMessageBox::Ok);
        }
        mux->setEgress(in);
        break;
    }
    default:
        break;
    }

    return true;
}


GUIChainSegmentOutputBuffer::GUIChainSegmentOutputBuffer()
        : mutex(), cond(), started(false), finished(false), reader(), segments()
{ }

GUIChainSegmentOutputBuffer::~GUIChainSegmentOutputBuffer()
{ }

void GUIChainSegmentOutputBuffer::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;

    if (!started) {
        started = true;
        feedback.emitStage(tr("Processing Data"),
                           tr("Incoming data are being processed into individual segments for output."),
                           true);
    }

    {
        std::lock_guard<std::mutex> lock(mutex);
        for (const auto &add : values) {
            if (!add.getUnit().isDefaultStation()) {
                SequenceName unit = add.getUnit();
                unit.clearMeta();
                unit.removeFlavor(SequenceName::flavor_cover);
                unit.removeFlavor(SequenceName::flavor_stats);
                unit.removeFlavor(SequenceName::flavor_end);
                units |= unit;
            }

            Util::append(reader.add(add), segments);
        }
    }

    if (FP::defined(values.back().getStart())) {
        feedback.emitProgressTime(values.back().getStart());
    }
}

void GUIChainSegmentOutputBuffer::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;

    if (!started) {
        started = true;
        feedback.emitStage(tr("Processing Data"),
                           tr("Incoming data are being processed into individual segments for output."),
                           true);
    }

    double lastTime = values.back().getStart();
    {
        std::lock_guard<std::mutex> lock(mutex);
        for (auto &add : values) {
            if (!add.getUnit().isDefaultStation()) {
                SequenceName unit = add.getUnit();
                unit.clearMeta();
                unit.removeFlavor(SequenceName::flavor_cover);
                unit.removeFlavor(SequenceName::flavor_stats);
                unit.removeFlavor(SequenceName::flavor_end);
                units |= unit;
            }

            Util::append(reader.add(std::move(add)), segments);
        }
    }

    if (FP::defined(lastTime)) {
        feedback.emitProgressTime(lastTime);
    }
}

void GUIChainSegmentOutputBuffer::incomingData(const SequenceValue &value)
{
    if (!started) {
        started = true;
        feedback.emitStage(tr("Processing Data"),
                           tr("Incoming data are being processed into individual segments for output."),
                           true);
    }

    {
        std::lock_guard<std::mutex> lock(mutex);
        if (!value.getUnit().isDefaultStation()) {
            SequenceName unit = value.getUnit();
            unit.clearMeta();
            unit.removeFlavor(SequenceName::flavor_cover);
            unit.removeFlavor(SequenceName::flavor_stats);
            unit.removeFlavor(SequenceName::flavor_end);
            units |= unit;
        }

        Util::append(reader.add(value), segments);
    }

    if (FP::defined(value.getStart())) {
        feedback.emitProgressTime(value.getStart());
    }
}

void GUIChainSegmentOutputBuffer::incomingData(SequenceValue &&value)
{
    if (!started) {
        started = true;
        feedback.emitStage(tr("Processing Data"),
                           tr("Incoming data are being processed into individual segments for output."),
                           true);
    }

    double lastTime = value.getStart();
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (!value.getUnit().isDefaultStation()) {
            SequenceName unit = value.getUnit();
            unit.clearMeta();
            unit.removeFlavor(SequenceName::flavor_cover);
            unit.removeFlavor(SequenceName::flavor_stats);
            unit.removeFlavor(SequenceName::flavor_end);
            units |= unit;
        }

        Util::append(reader.add(std::move(value)), segments);
    }

    if (FP::defined(lastTime)) {
        feedback.emitProgressTime(lastTime);
    }
}

void GUIChainSegmentOutputBuffer::endData()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        finished = true;
        Util::append(reader.finish(), segments);
    }
    cond.notify_all();
}

void GUIChainSegmentOutputBuffer::wait()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (finished)
            break;
        cond.wait(lock);
    }
}

GUIChainOutputWorker::GUIChainOutputWorker(GUIChainDialog *p) : QObject(p),
                                                                parent(p),
                                                                chains(),
                                                                multiplexers(),
                                                                start(FP::undefined()),
                                                                end(FP::undefined())
{ }

GUIChainOutputWorker::~GUIChainOutputWorker()
{
    abortAll();
    qDeleteAll(chains);
    qDeleteAll(multiplexers);
}

void GUIChainOutputWorker::abortAll()
{
    for (QList<StreamPipeline *>::const_iterator c = chains.constBegin(), end = chains.constEnd();
            c != end;
            ++c) {
        (*c)->signalTerminate();
    }
    for (QList<SinkMultiplexer *>::const_iterator m = multiplexers.constBegin(),
            end = multiplexers.constEnd(); m != end; ++m) {
        (*m)->signalTerminate();
    }
    for (QList<StreamPipeline *>::const_iterator c = chains.constBegin(), end = chains.constEnd();
            c != end;
            ++c) {
        (*c)->waitInEventLoop();
    }
    for (QList<SinkMultiplexer *>::const_iterator m = multiplexers.constBegin(),
            end = multiplexers.constEnd(); m != end; ++m) {
        (*m)->wait();
    }
}

void GUIChainOutputWorker::setup(double start, double end, const QList<StreamPipeline *> &chains,
                                 const QList<SinkMultiplexer *> &multiplexers)
{
    this->chains = chains;
    this->multiplexers = multiplexers;
    this->start = start;
    this->end = end;
}

bool GUIChainOutputWorker::execProgress(CPD3::ActionFeedback::Source *source)
{
    ActionProgressDialog progress(parent);

    progress.setWindowModality(Qt::ApplicationModal);
    progress.setWindowTitle(tr("Data Processing"));
    progress.setTimeBounds(start, end);
    if (source)
        progress.attach(*source);
    else
        progress.attach(chains.at(0)->feedback);
    connect(&progress, &ActionProgressDialog::canceled,
            std::bind(&StreamPipeline::signalTerminate, chains.front()));
    chains.front()
          ->finished
          .connect(&progress, std::bind(&ActionProgressDialog::accept, &progress), true);

    for (QList<StreamPipeline *>::const_iterator c = chains.constBegin(), end = chains.constEnd();
            c != end;
            ++c) {
        (*c)->start();
    }
    for (QList<SinkMultiplexer *>::const_iterator m = multiplexers.constBegin(),
            end = multiplexers.constEnd(); m != end; ++m) {
        (*m)->start();
    }

    progress.exec();
    if (progress.wasCanceled() || progress.result() == QDialog::Rejected) {
        abortAll();
        return false;
    }

    for (QList<StreamPipeline *>::const_iterator c = chains.constBegin(), end = chains.constEnd();
            c != end;
            ++c) {
        (*c)->waitInEventLoop();
    }
    for (QList<SinkMultiplexer *>::const_iterator m = multiplexers.constBegin(),
            end = multiplexers.constEnd(); m != end; ++m) {
        (*m)->wait();
    }
    qDeleteAll(chains);
    chains.clear();
    qDeleteAll(multiplexers);
    multiplexers.clear();

    return true;
}

namespace {
class TabularModel : public QAbstractItemModel {
    QWidget *dialog;

    QSettings settings;
    QList<SequenceName> units;
    SequenceSegment::Transfer *segments;

    bool showStation;
    bool showArchive;
    bool showVariable;
    bool showFlavors;

    static QString formatValue(const Variant::Read &value, const Variant::Read &meta)
    {
        if (!value.exists())
            return QString();

        QString format(meta.metadata("Format").toDisplayString());
        switch (value.getType()) {
        case Variant::Type::Real:
            return NumberFormat(format).apply(value.toDouble(), QChar());
        case Variant::Type::Integer:
            return NumberFormat(format).apply(value.toInt64(), QChar());
        case Variant::Type::String:
            return value.toDisplayString();
        case Variant::Type::Flags: {
            qint64 bits = 0;
            for (const auto &flag : value.toFlags()) {
                qint64 add = meta.metadataSingleFlag(flag).hash("Bits").toInt64();
                if (!INTEGER::defined(add))
                    continue;
                bits |= add;
            }
            return NumberFormat(format).apply(bits, QChar());
        }
        default:
            break;
        }

        return QString();
    }

    static void appendWithSpace(QString &target,
                                const Variant::Read &v,
                                const QString &prefix = QString())
    {
        if (!v.exists())
            return;
        QString add;
        switch (v.getType()) {
        case Variant::Type::Integer:
            add = QString::number(v.toInt64());
            break;
        case Variant::Type::Real:
            add = QString::number(v.toDouble());
            break;
        default:
            add = v.toDisplayString();
            break;
        }

        if (add.isEmpty())
            return;
        if (!target.isEmpty())
            target.append(' ');
        if (!prefix.isEmpty())
            target.append(prefix);
        target.append(add);
    }

public:
    TabularModel(const QList<SequenceName> &u, const SequenceSegment::Transfer &s,
                 QWidget *parent = 0) : QAbstractItemModel(parent),
                                        dialog(parent),
                                        settings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION),
                                        units(u),
                                        segments(new SequenceSegment::Transfer(s)),
                                        showStation(false),
                                        showArchive(false),
                                        showVariable(true),
                                        showFlavors(false)
    {
        SequenceName::ComponentSet uniqueStations;
        SequenceName::ComponentSet uniqueArchives;
        SequenceName::Map<SequenceName::Flavors> flavorsLookup;
        for (QList<SequenceName>::const_iterator unit = units.constBegin(), end = units.constEnd();
                unit != end;
                ++unit) {
            uniqueStations.insert(unit->getStation());
            uniqueArchives.insert(unit->getArchive());

            auto checkFlavors = unit->getFlavors();
            SequenceName checkUnit(unit->withoutAllFlavors());
            auto check = flavorsLookup.find(checkUnit);
            if (check != flavorsLookup.end() && check->second != checkFlavors) {
                showFlavors = true;
            }
            flavorsLookup.emplace(checkUnit, checkFlavors);
        }
        if (uniqueStations.size() > 1)
            showStation = true;
        if (uniqueArchives.size() > 1)
            showArchive = true;
    }

    virtual ~TabularModel()
    { }

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const
    {
        Q_UNUSED(parent);
        return createIndex(row, column);
    }

    virtual QModelIndex parent(const QModelIndex &index) const
    {
        Q_UNUSED(index);
        return QModelIndex();
    }

    virtual int rowCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return segments->size();
    }

    virtual int columnCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return units.size();
    }

    virtual QVariant data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();
        if (index.row() >= static_cast<int>(segments->size()))
            return QVariant();
        if (index.column() >= units.size())
            return QVariant();

        const auto &unit = units.at(index.column());

        switch (role) {
        case Qt::BackgroundRole: {
            if (!(*segments)[index.row()].getValue(unit).exists()) {
                return QApplication::palette(dialog).brush(QPalette::Disabled, QPalette::Window);
            }
            return QVariant();
        }

        case Qt::ToolTipRole: {
            auto value = (*segments)[index.row()].getValue(unit);
            auto meta = (*segments)[index.row()].getValue(unit.toMeta());

            QString description(meta.metadata("Description").toDisplayString());
            QString suffix;
            appendWithSpace(suffix, meta.metadata("Source").hash("Manufacturer"));
            appendWithSpace(suffix, meta.metadata("Source").hash("Model"));
            appendWithSpace(suffix, meta.metadata("Source").hash("SerialNumber"), QString('#'));

            double wl = meta.metadata("Wavelength").toDouble();
            if (FP::defined(wl)) {
                if (!suffix.isEmpty()) {
                    suffix = tr("%1 at %2 nm", "wavelength suffix").arg(suffix).arg(wl);
                } else {
                    suffix = tr("%2 nm", "wavelength suffix").arg(wl);
                }
            }

            if (!description.isEmpty() && !suffix.isEmpty()) {
                description = tr("%1 (%2)", "description format").arg(description, suffix);
            } else if (!suffix.isEmpty()) {
                description = suffix;
            }

            QString unitName;
            if (!unitName.isEmpty())
                unitName.append(':');
            unitName.append(unit.getStationQString().toUpper());

            if (!unitName.isEmpty())
                unitName.append(':');
            unitName.append(unit.getArchiveQString().toUpper());

            if (!unitName.isEmpty())
                unitName.append(':');
            unitName.append(unit.getVariableQString());

            {
                auto sorted = Util::to_qstringlist(unit.getFlavors());
                if (!sorted.isEmpty()) {
                    if (!unitName.isEmpty())
                        unitName.append(':');
                    std::sort(sorted.begin(), sorted.end());
                    unitName.append(sorted.join(":").toUpper());
                }
            }


            QString tip(tr("%1 %2<br>%3<br>%4", "tooltip format").arg(unitName,
                                                                      formatValue(value, meta),
                                                                      description,
                                                                      GUITime::formatRange(
                                                                              (*segments)[index.row()]
                                                                                      .getStart(),
                                                                              (*segments)[index.row()]
                                                                                      .getEnd(),
                                                                              &settings, true)));

            switch (value.getType()) {
            case Variant::Type::Flags:
                for (const auto &flag : value.toFlags()) {
                    QString add
                            (meta.metadataSingleFlag(flag).hash("Description").toDisplayString());
                    if (add.isEmpty())
                        continue;
                    tip.append(tr("<br>%1", "flag format").arg(add));
                }
                break;
            default:
                break;
            }

            return tip;
        }

        case Qt::DisplayRole: {
            auto value = (*segments)[index.row()].getValue(unit);
            auto meta = (*segments)[index.row()].getValue(unit.toMeta());
            return formatValue(value, meta);
        }

        case Qt::FontRole: {
            QFont font;
            font.setFamily("Monospace");
            font.setStyleHint(QFont::TypeWriter);
            /* font.setBold(true); */
            return font;
        }

        case Qt::TextAlignmentRole:
            return Qt::AlignRight;

        default:
            break;

        }
        return QVariant();
    }

    virtual QVariant headerData(int selection, Qt::Orientation orientation, int role) const
    {
        if (role != Qt::DisplayRole)
            return QVariant();

        if (orientation == Qt::Vertical) {
            if (selection < 0 || selection >= static_cast<int>(segments->size()))
                return QVariant();
            return GUITime::formatTime((*segments)[selection].getStart(), &settings, true);
        }

        if (selection < 0 || selection >= units.size())
            return QVariant();
        const auto &unit = units.at(selection);

        QString display;
        if (showStation) {
            if (!display.isEmpty())
                display.append(':');
            display.append(unit.getStationQString().toUpper());
        }
        if (showArchive) {
            if (!display.isEmpty())
                display.append(':');
            display.append(unit.getArchiveQString().toUpper());
        }
        if (showVariable) {
            if (!display.isEmpty())
                display.append(':');
            display.append(unit.getVariableQString());
        }
        if (showFlavors) {
            if (!display.isEmpty())
                display.append(':');
            auto sorted = Util::to_qstringlist(unit.getFlavors());
            std::sort(sorted.begin(), sorted.end());
            display.append(sorted.join(":").toUpper());
        }

        return display;
    }
};

class ResizingTableView : public QTableView {
public:
    ResizingTableView(QWidget *parent = 0) : QTableView(parent)
    { }

    virtual ~ResizingTableView()
    { }

    virtual QSize sizeHint() const
    {
        QHeaderView *vHeader = verticalHeader();
        QHeaderView *hHeader = horizontalHeader();

        int doubleFrame = 2 * frameWidth();

        int w = hHeader->length() + vHeader->width() + doubleFrame;
        int h = vHeader->length() + hHeader->height() + doubleFrame;

        return QSize(qMin(700, w), qMin(500, h));
    }
};

}

bool GUIChainOutputWorker::execTabular()
{
    GUIChainSegmentOutputBuffer buffer;
    if (!chains.at(0)->setOutputIngress(&buffer))
        return false;

    if (!execProgress(&buffer.feedback))
        return false;
    buffer.wait();

    QDialog *display = new QDialog(parent);
    display->setAttribute(Qt::WA_DeleteOnClose);
    QVBoxLayout *layout = new QVBoxLayout(display);
    display->setLayout(layout);

    display->setWindowTitle(tr("Tabular Data"));

    QList<SequenceName> units(buffer.units.values());
    std::sort(units.begin(), units.end(), SequenceName::OrderDisplayCaching());

    TabularModel *model = new TabularModel(units, buffer.segments, display);
    QTableView *table = new ResizingTableView(display);
    layout->addWidget(table, 1);
    table->setModel(model);

    table->setToolTip(tr("A table of the selected data"));
    table->setStatusTip(tr("Data output"));
    table->setWhatsThis(
            tr("This table shows a display of the values of data at the given start times."));

    for (int i = 0, max = units.size(); i < max; i++) {
        table->horizontalHeader()->setSectionResizeMode(i, QHeaderView::ResizeToContents);
    }
    table->resizeColumnsToContents();
    table->adjustSize();

    QDialogButtonBox
            *buttons = new QDialogButtonBox(QDialogButtonBox::Close, Qt::Horizontal, display);
    layout->addWidget(buttons);
    connect(buttons, SIGNAL(accepted()), display, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), display, SLOT(reject()));

    display->show();
    deleteLater();
    return true;
}

bool GUIChainOutputWorker::execOutputFile(const QString &fileName)
{
    StandardDataOutput::OutputType mode = StandardDataOutput::OutputType::Direct;
    if (fileName.endsWith(tr(".c3r", "raw extension"), Qt::CaseInsensitive))
        mode = StandardDataOutput::OutputType::Raw;
    else if (fileName.endsWith(tr(".xml", "xml extension"), Qt::CaseInsensitive))
        mode = StandardDataOutput::OutputType::XML;
    else if (fileName.endsWith(tr(".json", "json extension"), Qt::CaseInsensitive))
        mode = StandardDataOutput::OutputType::JSON;
    if (!chains.at(0)->setOutputFile(fileName, mode))
        return false;

    if (!execProgress())
        return false;

    deleteLater();
    return true;
}

bool GUIChainOutputWorker::execGeneral(std::unique_ptr<CPD3::Data::ExternalSink> &&output)
{
    if (!chains.at(0)->setOutputGeneral(std::move(output)))
        return false;

    if (!execProgress())
        return false;

    deleteLater();
    return true;
}

bool GUIChainOutputWorker::execBuffer(std::unique_ptr<CPD3::Data::ExternalSink> &&output,
                                      QBuffer *buffer)
{
    if (!chains.at(0)->setOutputGeneral(std::move(output)))
        return false;

    if (!execProgress())
        return false;

    GUIChainBufferDialog *display = new GUIChainBufferDialog(buffer->buffer(), parent);
    display->setAttribute(Qt::WA_DeleteOnClose);

    display->show();
    deleteLater();
    return true;
}

bool GUIChainOutputWorker::execDisplay(CPD3::Graphing::Display *d,
                                       const Variant::Read &config,
                                       double start,
                                       double end,
                                       const SequenceName::Component &station,
                                       const SequenceName::Component &archive)
{
    GUIChainDisplayWindow *display =
            new GUIChainDisplayWindow(d, chains.at(0), config, start, end, station, archive);
    setParent(display);
    display->attachToChain();

    display->setAttribute(Qt::WA_DeleteOnClose);
    display->show();
    display->raise();

    for (QList<StreamPipeline *>::const_iterator c = chains.constBegin(), endC = chains.constEnd();
            c != endC;
            ++c) {
        (*c)->start();
    }
    for (QList<SinkMultiplexer *>::const_iterator m = multiplexers.constBegin(),
            endM = multiplexers.constEnd(); m != endM; ++m) {
        (*m)->start();
    }
    return true;
}

void GUIChainDialog::completeDialog()
{
    if (outputSelectButton == NULL) {
        accept();
        return;
    }

    double effectiveStart = start;
    double effectiveEnd = end;
    QList<StreamPipeline *> chains;
    QList<SinkMultiplexer *> multiplexers;

    chains.append(new StreamPipeline(false, false, false));
    int acknowledgedWarnings = 0;
    if (!configurePipeline(chains.at(0), chains, multiplexers, acknowledgedWarnings)) {
        qDeleteAll(chains);
        qDeleteAll(multiplexers);
        /* Leave it open, since this may be the result of a warning
         * failure */
        return;
    }

    {
        int index = -1;
        if (inputSelectType != NULL) {
            index = inputSelectType->currentIndex();
            if (index >= 0 && index < inputSelectType->count())
                index = inputSelectType->itemData(index).toInt();
            else
                index = -1;
        }
        switch (index) {
        case Input_Archive:
            Q_ASSERT(archiveInput != NULL);
            effectiveStart = archiveInput->bounds->getStart();
            effectiveEnd = archiveInput->bounds->getEnd();
            break;
        case Input_File:
            break;
        case Input_Component:
            break;
        case Input_Multiplexer:
            break;
        default:
            break;
        }
    }

    GUIChainOutputWorker *worker = new GUIChainOutputWorker(this);
    worker->setup(effectiveStart, effectiveEnd, chains, multiplexers);

    {
        int index = Output_Tabular;
        if (outputSelectType != NULL) {
            index = outputSelectType->currentIndex();
            if (index >= 0 && index < outputSelectType->count())
                index = outputSelectType->itemData(index).toInt();
            else
                index = -1;
        }
        switch (index) {
        case Output_DataToFile: {
            Q_ASSERT(fileOutput != NULL);
            QString outputFile(fileOutput->target->text());
            if (outputFile.isEmpty()) {
                QMessageBox::critical(this, tr("No Output File"),
                                      tr("You must select a file to write to."), QMessageBox::Ok);
                break;
            }

            if (worker->execOutputFile(outputFile))
                accept();
            else
                reject();
            return;
        }
        case Output_Component: {
            Q_ASSERT(componentOutput != NULL);

            int idx = componentOutput->select->currentIndex();
            if (idx < 0 || idx >= componentOutput->select->count())
                break;
            QString componentName(componentOutput->select->itemData(idx).toString());
            QObject *component = ComponentLoader::create(componentName);
            if (!component)
                break;

            std::unique_ptr<ExternalSink> output;

            ExternalSinkComponent *basic = qobject_cast<ExternalSinkComponent *>(component);
            if (basic) {
                if (basic->requiresOutputDevice()) {
                    if (!componentOutput->fileEnable->isChecked()) {
                        QBuffer *buffer = new QBuffer(worker);
                        if (!buffer->open(QIODevice::WriteOnly))
                            break;

                        output.reset(basic->createDataEgress(buffer, componentOutput->options
                                                                                    ->getOptions()));
                        if (!output)
                            break;
                        if (worker->execBuffer(std::move(output), buffer))
                            accept();
                        else
                            reject();
                        return;
                    } else {
                        QString outputFile(componentOutput->fileTarget->text());
                        if (outputFile.isEmpty()) {
                            QMessageBox::critical(this, tr("No Output File"),
                                                  tr("You must select a file to write to."),
                                                  QMessageBox::Ok);
                            break;
                        }

                        auto handle = IO::Access::file(outputFile,
                                                       IO::File::Mode::writeOnly().textMode()
                                                                                  .bufferedMode());
                        if (!handle) {
                            QMessageBox::critical(this, tr("Output File Error"),
                                                  tr("Error opening output file"), QMessageBox::Ok);
                            break;
                        }
                        auto stream = handle->stream();
                        if (!stream) {
                            QMessageBox::critical(this, tr("Output File Error"),
                                                  tr("Error opening output stream"),
                                                  QMessageBox::Ok);
                            break;
                        }

                        output.reset(basic->createDataSink(std::move(stream),
                                                           componentOutput->options->getOptions()));
                    }
                } else {
                    output.reset(basic->createDataSink({}, componentOutput->options->getOptions()));
                }
            }

            if (!output)
                break;

            if (worker->execGeneral(std::move(output)))
                accept();
            else
                reject();
            return;
        }
        case Output_Tabular:
            if (worker->execTabular())
                accept();
            else
                reject();
            return;
        case Output_Display: {
            Q_ASSERT(displayOutput != NULL);

            int idx = displayOutput->type->currentIndex();
            if (idx < 0 || idx >= displayOutput->type->count())
                break;
            idx = displayOutput->type->itemData(idx).toInt();

            DisplayDefaults defaults;
            if (!station.empty())
                defaults.setStation(station);
            if (!archive.empty())
                defaults.setArchive(archive);
            defaults.setArchiveRealtime("rt_boxcar");

            CPD3::Graphing::Display *d =
                    DisplayComponent::create((DisplayComponent::DisplayType) idx,
                                             displayOutput->options->getOptions(), defaults);
            if (d == NULL)
                break;

            if (worker->execDisplay(d, DisplayComponent::baselineConfiguration(
                    (DisplayComponent::DisplayType) idx, displayOutput->options->getOptions()),
                                    start, end, station, archive))
                accept();
            else
                reject();
            return;
        }
        default:
            break;
        }
    }

    reject();
    delete worker;
}

void GUIChainDialog::inputTypeChanged()
{
    if (inputSelectType == NULL)
        return;
    int index = inputSelectType->currentIndex();
    if (index < 0 || index >= inputSelectType->count())
        return;
    switch (inputSelectType->itemData(index).toInt()) {
    case Input_Archive:
        if (archiveInput != NULL)
            archiveInput->pane->setVisible(true);
        if (fileInput != NULL)
            fileInput->pane->setVisible(false);
        if (componentInput != NULL)
            componentInput->pane->setVisible(false);
        if (multiplexerInput != NULL)
            multiplexerInput->pane->setVisible(false);
        break;
    case Input_File:
        if (archiveInput != NULL)
            archiveInput->pane->setVisible(false);
        if (fileInput != NULL)
            fileInput->pane->setVisible(true);
        if (componentInput != NULL)
            componentInput->pane->setVisible(false);
        if (multiplexerInput != NULL)
            multiplexerInput->pane->setVisible(false);
        break;
    case Input_Component:
        if (archiveInput != NULL)
            archiveInput->pane->setVisible(false);
        if (fileInput != NULL)
            fileInput->pane->setVisible(false);
        if (componentInput != NULL)
            componentInput->pane->setVisible(true);
        if (multiplexerInput != NULL)
            multiplexerInput->pane->setVisible(false);
        waitForComponents();
        break;
    case Input_Multiplexer:
        if (archiveInput != NULL)
            archiveInput->pane->setVisible(false);
        if (fileInput != NULL)
            fileInput->pane->setVisible(false);
        if (componentInput != NULL)
            componentInput->pane->setVisible(false);
        if (multiplexerInput != NULL)
            multiplexerInput->pane->setVisible(true);
        break;
    default:
        break;
    }
}

void GUIChainDialog::outputTypeChanged()
{
    if (outputSelectType == NULL)
        return;
    int index = outputSelectType->currentIndex();
    if (index < 0 || index >= outputSelectType->count())
        return;
    switch (outputSelectType->itemData(index).toInt()) {
    case Output_DataToFile:
        if (tabularOutput != NULL)
            tabularOutput->pane->setVisible(false);
        if (fileOutput != NULL)
            fileOutput->pane->setVisible(true);
        if (displayOutput != NULL)
            displayOutput->pane->setVisible(false);
        if (componentOutput != NULL)
            componentOutput->pane->setVisible(false);
        break;
    case Output_Component:
        if (tabularOutput != NULL)
            tabularOutput->pane->setVisible(false);
        if (fileOutput != NULL)
            fileOutput->pane->setVisible(false);
        if (displayOutput != NULL)
            displayOutput->pane->setVisible(false);
        if (componentOutput != NULL)
            componentOutput->pane->setVisible(true);
        waitForComponents();
        break;
    case Output_Tabular:
        if (tabularOutput != NULL)
            tabularOutput->pane->setVisible(true);
        if (fileOutput != NULL)
            fileOutput->pane->setVisible(false);
        if (displayOutput != NULL)
            displayOutput->pane->setVisible(false);
        if (componentOutput != NULL)
            componentOutput->pane->setVisible(false);
        break;
    case Output_Display:
        if (tabularOutput != NULL)
            tabularOutput->pane->setVisible(false);
        if (fileOutput != NULL)
            fileOutput->pane->setVisible(false);
        if (displayOutput != NULL)
            displayOutput->pane->setVisible(true);
        if (componentOutput != NULL)
            componentOutput->pane->setVisible(false);
        break;
    default:
        break;
    }
}


GUIChainComponentSelectionDialog::GUIChainComponentSelectionDialog(const std::unordered_map<QString,
                                                                                            CPD3::ComponentLoader::Information> &c,
                                                                   QWidget *parent) : QDialog(
        parent), components(c)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);

    componentSelect = new QComboBox(this);
    layout->addWidget(componentSelect);
    componentSelect->setToolTip(tr("Select the processing component"));
    componentSelect->setStatusTip(tr("Select component"));
    componentSelect->setWhatsThis(tr("This is the list of available components for selection."));

    {
        std::vector<ComponentLoader::Information> sorted;
        for (const auto &add : components) {
            sorted.emplace_back(add.second);
        }
        std::sort(sorted.begin(), sorted.end(), ComponentLoader::Information::DisplayOrder());
        for (const auto &add : sorted) {
            componentSelect->addItem(
                    tr("%1 - %2", "component format").arg(add.componentName(), add.title()),
                    add.componentName());
        }
    }

    connect(componentSelect, SIGNAL(currentIndexChanged(int)), this, SLOT(updateSelected()));

    descriptionLabel = new QLabel(this);
    layout->addWidget(descriptionLabel, 1);
    descriptionLabel->setWordWrap(true);

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 this);
    layout->addWidget(buttons);

    connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));

    updateSelected();
}

void GUIChainComponentSelectionDialog::updateSelected()
{
    descriptionLabel->setText(components[getSelectedComponent()].description());
}

QString GUIChainComponentSelectionDialog::getSelectedComponent() const
{
    int idx = componentSelect->currentIndex();
    if (idx < 0 || idx >= componentSelect->count())
        return QString();
    return componentSelect->itemData(idx).toString();
}

GUIChainBufferDialog::GUIChainBufferDialog(const QByteArray &buffer, QWidget *parent) : QDialog(
        parent)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);

    text = new QPlainTextEdit(this);
    layout->addWidget(text, 1);
    text->setReadOnly(true);
    text->setWordWrapMode(QTextOption::NoWrap);
    text->setPlainText(QString::fromUtf8(buffer));
    {
        QFont f(text->font());
        f.setFamily("Monospace");
        f.setStyleHint(QFont::TypeWriter);
        text->setFont(f);
    }

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Save | QDialogButtonBox::Close, Qt::Horizontal,
                                 this);
    layout->addWidget(buttons);

    connect(buttons, SIGNAL(accepted()), this, SLOT(save()));
    connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));
}

GUIChainBufferDialog::~GUIChainBufferDialog()
{ }

void GUIChainBufferDialog::save()
{
    QStringList filters;
    filters << tr("Text files (*.txt *.text)");
    filters << tr("Any files (*)");

    QString selectedFilter;
    QString name(QFileDialog::getSaveFileName(this, QString(), QString(), filters.join(";;"),
                                              &selectedFilter));
    if (name.isEmpty())
        return;

    QFileInfo info(name);
    if (info.completeSuffix().isEmpty() && !info.exists()) {
        if (selectedFilter == tr("Text files (*.txt *.text)")) {
            name.append(tr(".txt", "default extension"));
        }
    }

    QFile target(name);
    if (!target.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)) {
        QMessageBox::critical(this, tr("Output File Error"),
                              tr("Error opening output file: %1").arg(target.errorString()),
                              QMessageBox::Ok);
        return;
    }
    target.write(text->toPlainText().toUtf8());
    target.close();
}

QSize GUIChainBufferDialog::sizeHint() const
{ return QSize(800, 600); }


namespace {
class TimerBackoffDisplayWidget : public CPD3::Graphing::DisplayWidget {
    QTimer *repaintTimer;
    int baseInterval;
    int thresholdInterval;
public:
    TimerBackoffDisplayWidget(std::unique_ptr<CPD3::Graphing::Display> &&setDisplay,
                              QWidget *parent,
                              QTimer *timer) : DisplayWidget(std::move(setDisplay), parent),
                                               repaintTimer(timer),
                                               baseInterval(timer->interval()),
                                               thresholdInterval((int) (timer->interval() * 0.1))
    { }

protected:
    virtual void paintEvent(QPaintEvent *event)
    {
        ElapsedTimer timeTaken;
        timeTaken.start();
        DisplayWidget::paintEvent(event);
        int elapsed = (int) timeTaken.elapsed();
        if (elapsed > thresholdInterval) {
            int adjustedInterval = baseInterval + elapsed;
            if (abs(repaintTimer->interval() - adjustedInterval) > 10) {
                repaintTimer->setInterval(adjustedInterval);
            }
        } else {
            if (repaintTimer->interval() != baseInterval) {
                repaintTimer->setInterval(baseInterval);
            }
        }
    }
};
}

GUIChainDisplayWindow::GUIChainDisplayWindow(CPD3::Graphing::Display *disp,
                                             StreamPipeline *c,
                                             const CPD3::Data::Variant::Read &config,
                                             double s,
                                             double e,
                                             const SequenceName::Component &stn,
                                             const SequenceName::Component &arc)
        : QMainWindow(),
          start(s),
          end(e),
          station(stn),
          archive(arc),
          settings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION),
          baselineConfiguration(config),
          displayWidget(NULL),
          progress(NULL),
          chain(c),
          filterChain(NULL)
{

    setWindowTitle(tr("CPX3 Exported Data"));

    repaintTimer = new QTimer(this);
    repaintTimer->setInterval(settings.value("display/repaintinterval", 1000).toInt());
    repaintTimer->setSingleShot(false);

    displayWidget =
            new TimerBackoffDisplayWidget(std::unique_ptr<CPD3::Graphing::Display>(disp), this,
                                          repaintTimer);
    displayWidget->setResetSelectsAll(true);
    connect(displayWidget, SIGNAL(internalZoom(CPD3::Graphing::DisplayZoomCommand)), displayWidget,
            SLOT(applyZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(displayWidget, SIGNAL(visibleZoom(CPD3::Graphing::DisplayZoomCommand)), displayWidget,
            SLOT(applyZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(displayWidget, SIGNAL(globalZoom(CPD3::Graphing::DisplayZoomCommand)), displayWidget,
            SLOT(applyZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(displayWidget, SIGNAL(timeRangeSelected(double, double)), displayWidget,
            SLOT(setVisibleTimeRange(double, double)), Qt::QueuedConnection);
    setCentralWidget(displayWidget);
    connect(repaintTimer, SIGNAL(timeout()), displayWidget, SLOT(update()));

    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    QProgressBar *progressBar = new QProgressBar(statusBar());
    progressBar->setMaximum(0);
    progressBar->setMinimum(0);
    progressBar->reset();
    progressBar->setValue(1);
    statusBar()->addPermanentWidget(progressBar);
    QLabel *statusLabel = new QLabel(tr("Initializing"), statusBar());
    statusBar()->addWidget(statusLabel);

    progress = new ActionProgressStatusBar(progressBar, statusLabel, this, &settings);
    progress->setTimeBounds(start, end);
    progress->attach(chain->feedback);

    chain->finished.connect(this, std::bind(&GUIChainDisplayWindow::finished, this), true);

    connect(displayWidget, SIGNAL(requestChainReload()), this, SLOT(chainReload()));
    connect(displayWidget, SIGNAL(saveChanges(CPD3::Data::ValueSegment::Transfer)), this,
            SLOT(saveChanges(CPD3::Data::ValueSegment::Transfer)));

    progress->initialize();
    progress->setTimeBounds(start, end);
}

GUIChainDisplayWindow::~GUIChainDisplayWindow()
{
    chain->signalTerminate();
    chain->waitInEventLoop();
    if (filterChain != NULL) {
        filterChain->signalTerminate();
        filterChain->wait();
        delete filterChain;
    }

    progress->finished();

    QCoreApplication::processEvents();

    /* Explicitly delete it, so it can't process any more queued signals (since that
     * references our settings */
    delete progress;
}

void GUIChainDisplayWindow::attachToChain()
{
    if (filterChain != NULL)
        filterChain->deleteLater();

    Archive::Selection::Match stations;
    if (!station.empty())
        stations.emplace_back(station);
    Archive::Selection::Match archives;
    if (!archive.empty())
        archives.emplace_back(archive);

    filterChain = new DisplayWidgetTapChain;
    filterChain->setDefaultSelection(start, end, stations, archives);
    displayWidget->registerChain(filterChain);
    if (!chain->setOutputFilterChain(filterChain, true))
        return;
    progress->initialize();
    progress->setTimeBounds(start, end);
    repaintTimer->start();
}

void GUIChainDisplayWindow::chainReload()
{
    chain->waitInEventLoop();

    if (filterChain != NULL) {
        filterChain->signalTerminate();
        filterChain->wait();
        filterChain->deleteLater();
        filterChain = NULL;
    }

    attachToChain();
    chain->start();
}

void GUIChainDisplayWindow::saveChanges(ValueSegment::Transfer overlay)
{
    DisplayWidget::saveConfiguration(baselineConfiguration, std::move(overlay), this);
}

QSize GUIChainDisplayWindow::sizeHint() const
{ return QSize(801, 600); }

void GUIChainDisplayWindow::finished()
{
    progress->finished();
    displayWidget->update();
}
