/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QWidget>
#include <QLabel>
#include <QGroupBox>
#include <QGridLayout>
#include <QStringListModel>
#include <QTimer>

#include "core/number.hxx"

#include "realtimedisplay.hxx"

using namespace CPD3;
using namespace CPD3::Acquisition;

RealtimeDisplay::RealtimeDisplay(QWidget *parent, Qt::WindowFlags f) : QWidget(parent, f),
                                                                       baseLayout(new QGridLayout(
                                                                               this)),
                                                                       layoutWidget(NULL),
                                                                       elements(),
                                                                       displaySelection(NULL),
                                                                       pageSelection(NULL),
                                                                       criticalLabel(NULL)
{
    setLayout(baseLayout);

    displaySelection = new QComboBox(this);
    displaySelection->setToolTip(tr("Select the display mode."));
    displaySelection->setStatusTip(tr("Display mode"));
    displaySelection->setWhatsThis(tr("This allows you to change the displayed data."));
    baseLayout->addWidget(displaySelection, 1, 0, 1, 1, Qt::AlignBottom);
    displaySelection->hide();
    connect(displaySelection, SIGNAL(currentIndexChanged(int)), this,
            SIGNAL(displaySelectionChanged(int)));

    pageSelection = new QSpinBox(this);
    pageSelection->setToolTip(tr("Select the page."));
    pageSelection->setStatusTip(tr("Page"));
    pageSelection->setWhatsThis(tr("This you to select the displayed page of data."));
    pageSelection->setWrapping(true);
    pageSelection->setRange(1, 1);
    baseLayout->addWidget(pageSelection, 1, 1, 1, 1, Qt::AlignBottom);
    pageSelection->hide();
    connect(pageSelection, SIGNAL(valueChanged(int)), this, SIGNAL(pageSelectionChanged(int)));

    criticalLabel = new QLabel(this);
    baseLayout->addWidget(criticalLabel, 0, 0, 1, 2, Qt::AlignHCenter | Qt::AlignVCenter);
    criticalLabel->hide();

    baseLayout->setRowStretch(0, 0);
    baseLayout->setRowStretch(1, 1);
}

void RealtimeDisplay::forceUpdate()
{
    if (layoutWidget != NULL) {
        layoutWidget->deleteLater();
        layoutWidget = NULL;
    }
    elements.clear();
}


void RealtimeDisplay::setDisplaySelectionModes(const QStringList &modes)
{
    if (!modes.isEmpty()) {
        displaySelection->setModel(new QStringListModel(modes, displaySelection));
        displaySelection->show();
        if (!displaySelection->isVisible())
            updateGeometry();
    } else {
        displaySelection->hide();
        if (displaySelection->isVisible())
            updateGeometry();
    }
}

void RealtimeDisplay::setSelectedDisplay(int index)
{
    displaySelection->setCurrentIndex(index);
}

void RealtimeDisplay::setPages(int n)
{
    if (n <= 1) {
        pageSelection->hide();
        if (pageSelection->isVisible())
            updateGeometry();
    } else {
        pageSelection->setRange(1, n);
        pageSelection->show();
        if (!pageSelection->isVisible())
            updateGeometry();
    }
}

void RealtimeDisplay::setSelectedPage(int page)
{
    pageSelection->setValue(page);
}

void RealtimeDisplay::update(const RealtimeLayout::iterator &begin,
                             const RealtimeLayout::iterator &end,
                             const QString &overrideCritical)
{
    if (layoutWidget != NULL) {
        if (overrideCritical.isEmpty()) {
            criticalLabel->hide();
            layoutWidget->setStyleSheet(QString());
        } else {
            layoutWidget->setStyleSheet("QLabel { color : rgba(0,0,0,0) }");
            criticalLabel->setText(overrideCritical);
            criticalLabel->show();
        }

        for (RealtimeLayout::iterator update = begin; update != end; ++update) {
            int index = update.uid();
            if (index < 0 || index >= elements.size())
                continue;
            QWidget *base = elements.at(index);
            if (base == NULL)
                continue;

            switch (update.type()) {
            case RealtimeLayout::GroupBox:
            case RealtimeLayout::FinalGroupBox:
                break;
            case RealtimeLayout::LeftJustifiedLabel:
            case RealtimeLayout::CenteredLabel:
            case RealtimeLayout::RightJustifiedLabel:
            case RealtimeLayout::ValueText:
            case RealtimeLayout::ValueLine: {
                QLabel *target = qobject_cast<QLabel *>(base);
                if (target) {
                    QString text(update.text());
                    text.replace('&', "&&");
                    target->setText(text);
                }
                break;
            }
            }
        }
        return;
    }

    layoutWidget = new QWidget(this);
    baseLayout->addWidget(layoutWidget, 0, 0, 1, 2);
    QVBoxLayout *topLayout = new QVBoxLayout(layoutWidget);
    layoutWidget->setLayout(topLayout);

    if (overrideCritical.isEmpty()) {
        criticalLabel->hide();
        layoutWidget->setStyleSheet(QString());
    } else {
        layoutWidget->setStyleSheet("QLabel { color : rgba(0,0,0,0) }");
        criticalLabel->setText(overrideCritical);
        criticalLabel->show();
    }

    QGridLayout *layout = NULL;
    QWidget *activeParent = NULL;
    QGroupBox *activeBox = NULL;
    QFont labelFont("Serif");
    QFont valueFont("Monospace");
    for (RealtimeLayout::iterator update = begin; update != end; ++update) {
        QLabel *label = NULL;
        switch (update.type()) {
        case RealtimeLayout::GroupBox: {
            bool hadActive = false;
            if (activeBox != NULL) {
                activeBox->setFlat(false);
                hadActive = true;
            }

            activeBox = new QGroupBox(layoutWidget);
            topLayout->addWidget(activeBox);
            layout = new QGridLayout(activeBox);
            activeBox->setLayout(layout);
            activeBox->setFlat(!hadActive);
            activeBox->setTitle(update.text());
            activeBox->setAlignment(Qt::AlignHCenter);
            activeParent = activeBox;
            break;
        }
        case RealtimeLayout::FinalGroupBox: {
            if (activeBox != NULL) {
                activeBox->setFlat(false);

                activeBox = new QGroupBox(layoutWidget);
                topLayout->addWidget(activeBox);
                layout = new QGridLayout(activeBox);
                activeBox->setLayout(layout);
                activeBox->setFlat(false);
                activeBox->setTitle(update.text());
                activeBox->setAlignment(Qt::AlignHCenter);
                activeParent = activeBox;
            } else if (!update.text().isEmpty()) {
                activeBox = new QGroupBox(layoutWidget);
                topLayout->addWidget(activeBox);
                layout = new QGridLayout(activeBox);
                activeBox->setLayout(layout);
                activeBox->setFlat(false);
                activeBox->setTitle(update.text());
                activeBox->setAlignment(Qt::AlignHCenter);
                activeParent = activeBox;
            } else {
                activeParent = new QWidget(layoutWidget);
                topLayout->addWidget(activeParent);
                layout = new QGridLayout(activeParent);
                activeParent->setLayout(layout);
            }
            break;
        }
        case RealtimeLayout::LeftJustifiedLabel: {
            label = new QLabel(update.text(), activeParent);
            label->setTextInteractionFlags(Qt::NoTextInteraction);
            label->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
            label->setFont(labelFont);
            layout->addWidget(label, update.row(), update.column(), 1, 1,
                              Qt::AlignVCenter | Qt::AlignLeft);
            break;
        }
        case RealtimeLayout::CenteredLabel: {
            label = new QLabel(update.text(), activeParent);
            label->setTextInteractionFlags(Qt::NoTextInteraction);
            label->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
            label->setFont(labelFont);
            layout->addWidget(label, update.row(), update.column(), 1, 1,
                              Qt::AlignVCenter | Qt::AlignHCenter);
            break;
        }
        case RealtimeLayout::RightJustifiedLabel: {
            label = new QLabel(update.text(), activeParent);
            label->setTextInteractionFlags(Qt::NoTextInteraction);
            label->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
            label->setFont(labelFont);
            layout->addWidget(label, update.row(), update.column(), 1, 1,
                              Qt::AlignVCenter | Qt::AlignRight);
            break;
        }
        case RealtimeLayout::ValueText: {
            label = new QLabel(update.text(), activeParent);
            label->setTextInteractionFlags(Qt::NoTextInteraction);
            label->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
            label->setFont(valueFont);
            layout->addWidget(label, update.row(), update.column(), 1, 1,
                              Qt::AlignVCenter | Qt::AlignLeft);
            break;
        }
        case RealtimeLayout::ValueLine: {
            label = new QLabel(update.text(), activeParent);
            label->setTextInteractionFlags(Qt::NoTextInteraction);
            label->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
            label->setFont(valueFont);
            layout->addWidget(label, update.row(), update.column(), 1, -1,
                              Qt::AlignVCenter | Qt::AlignLeft);
            break;
        }
        }

        if (label != NULL) {
            int targetIndex = update.uid();
            while (targetIndex >= elements.size()) {
                elements.append(NULL);
            }
            elements[targetIndex] = label;
        }
    }

    emit layoutRebuilt();
}

void RealtimeDisplay::updateGeometry()
{
    QWidget::updateGeometry();
}

void RealtimeDisplay::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);
    QTimer::singleShot(100, this, SLOT(updateGeometry()));
}
