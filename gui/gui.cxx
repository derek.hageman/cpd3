/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <future>
#include <QTranslator>
#include <QLibraryInfo>
#include <QApplication>
#include <QMessageBox>
#include <QIcon>
#include <QLocale>
#include <QDir>
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QDesktopServices>
#include <QUrl>
#include <QPlainTextEdit>
#include <QSvgWidget>
#include <QScrollBar>
#include <QComboBox>
#include <QtConcurrentRun>
#include <QLoggingCategory>
#include <QGlobalStatic>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QHeaderView>

#include "core/abort.hxx"
#include "core/component.hxx"
#include "core/timeparse.hxx"
#include "core/range.hxx"
#include "core/environment.hxx"
#include "core/threadpool.hxx"
#include "core/memory.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/segment.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/variant/composite.hxx"
#include "clicore/argumentparser.hxx"
#include "clicore/terminaloutput.hxx"
#include "guidata/realtimemessagelog.hxx"
#include "guidata/realtimecommand.hxx"
#include "guidata/variableselect.hxx"
#include "guidata/optionseditor.hxx"
#include "guidata/wizards/initialsetup.hxx"
#include "guidata/wizards/displaysetup.hxx"
#include "guicore/guiformat.hxx"
#include "guicore/actionprogress.hxx"

#include "gui.hxx"
#include "mainwindow.hxx"
#include "interfacehandler.hxx"
#include "generichandler.hxx"
#include "displayhandler.hxx"
#include "editdirective.hxx"
#include "eventlog.hxx"
#include "guichain.hxx"
#include "configurationeditor.hxx"


Q_LOGGING_CATEGORY(log_gui_main, "cpd3.gui.main", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::CLI;
using namespace CPD3::Acquisition;
using namespace CPD3::GUI;
using namespace CPD3::GUI::Data;

CPD3GUI::CPD3GUI() : settings(new QSettings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION)),
                     invokedMode(Invoked_Standard),
                     profile(),
                     mode(),
                     station(),
                     archive(),
                     displayBounds(FP::undefined(), FP::undefined()),
                     initialBounds(FP::undefined(), FP::undefined()),
                     realtimeWindowUnit(Time::Day),
                     realtimeWindowCount(1),
                     realtimeWindowAlign(false),
                     inRealtimePlotMode(false),
                     boundsUpdateTimer(this),
                     replotPendingIssue(this),
                     trimDataTimer(this),
                     realtimeDisplayReloadTimer(this),
                     realtimeForceReplotTimer(this),
                     baseDisplayContext(),
                     doRestoreState(true),
                     dataAccess(*this),
                     activeChain(),
                     baseConfiguration(),
                     configuration(),
                     acquisitionConnection(),
                     haveAcquisitionConnection(false),
                     acquisitionCommandHandler(),
                     commandManager(),
                     acquisitionInterfaces(),
                     acquisitionGenericConfiguration(),
                     acquisitionGeneric(),
                     acquisitionRealtimeIngress(*this),
                     mainWindow(),
                     messageLogDialog(),
                     editDirectiveDialog(),
                     eventLogDialog(),
                     viewDataDialog(),
                     exportDataDialog(),
                     configurationEditorDialog(),
                     mainMenu()
{

    displayBounds.end = Time::floor(Time::time(), Time::Day);
    displayBounds.start = Time::logical(displayBounds.end, Time::Day, -7, true);

    baseDisplayContext.setSettings(settings.get());

    displayMenuGroup = nullptr;

    setContaminationAction = nullptr;
    clearContaminationAction = nullptr;
    overrideContaminationAction = nullptr;
    setBypassAction = nullptr;
    clearBypassAction = nullptr;
    overrideBypassAction = nullptr;
    setAveragingAction = nullptr;

    replotPendingIssue.setSingleShot(true);
    replotPendingIssue.setInterval(0);
    connect(&replotPendingIssue, SIGNAL(timeout()), this, SLOT(applyReplot()));

    connect(&boundsUpdateTimer, SIGNAL(timeout()), this, SLOT(recalculateBounds()));

    trimDataTimer.setSingleShot(false);
    trimDataTimer.setInterval(15 * 60 * 1000);
    connect(&trimDataTimer, SIGNAL(timeout()), this, SLOT(applyDataTrim()));


    realtimeDisplayReloadTimer.setSingleShot(false);
    realtimeDisplayReloadTimer.setInterval(3600 * 1000);
    connect(&realtimeDisplayReloadTimer, SIGNAL(timeout()), this, SLOT(applyRealtimeReload()));

    realtimeForceReplotTimer.setSingleShot(true);
    realtimeForceReplotTimer.setInterval(10 * 1000);
    connect(&realtimeForceReplotTimer, SIGNAL(timeout()), this, SLOT(applyReplot()));

    connect(this, SIGNAL(serviceAcquisitionRealtime()), this, SLOT(processAcquisitionRealtime()),
            Qt::QueuedConnection);
}

CPD3GUI::~CPD3GUI()
{
    dataAccess.stop();

    if (auto ctx = displayConfigureContext.lock()) {
        ctx->terminate();
        displayConfigureContext.reset();
    }

    if (activeChain) {
        activeChain->signalTerminate();
        activeChain->wait();
        activeChain.reset();
    }

    if (mainWindow)
        mainWindow->removeAllDisplayHandlers();
    acquisitionCommandHandler.reset();

    if (acquisitionConnection) {
        acquisitionConnection.reset();
    }

    acquisitionInterfaces.clear();
    acquisitionGeneric.clear();
    displayHandlers.clear();

    /* Immediate deletes, so we know we're off the thread pool before we destroy
     * the actual handling object (the thread pool might be destroyed before the
     * deleteLater() calls are evalulated). */
#ifdef Q_OS_MAC
    mainMenu.reset();
#else
    mainMenu.release();
#endif
    messageLogDialog.reset();
    editDirectiveDialog.reset();
    configurationEditorDialog.reset();
    viewDataDialog.reset();
    exportDataDialog.reset();
    eventLogDialog.reset();
    mainWindow.reset();

    settings.reset();
}

namespace {
class Parser : public ArgumentParser {
    QString programName;
    CPD3GUI::InvokedMode invokedMode;
public:
    Parser(const QString &pn, CPD3GUI::InvokedMode im) : programName(pn), invokedMode(im)
    { }

    virtual ~Parser()
    { }

protected:
    virtual QList<BareWordHelp> getBareWordHelp()
    {
        switch (invokedMode) {
        case CPD3GUI::Invoked_Standard:
            return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                         tr("The station to show"),
                                                         tr("Inferred from the current directory"),
                                                         tr("The \"station\" bare word argument is used to specify the station displayed.  "
                                                            "This is the three digit station identification code.  For example "
                                                            "\"brw\"."))
                                         << BareWordHelp(tr("times", "times argument name"),
                                                         tr("The time range of data to display"),
                                                         QString(),
                                                         TimeParse::describeListBoundsUsage(false,
                                                                                            true,
                                                                                            Time::Week))
                                         << BareWordHelp(tr("mode", "profile argument name"),
                                                         tr("The display mode"), tr("\"raw\" mode"),
                                                         tr("The \"mode\" bare word argument is used to specify the initial mode that "
                                                            "is shown.  The mode determines which set of displays are shown and generally "
                                                            "the origin of the data (raw data, passed data, etc).  Under most "
                                                            "circumstances the modes \"raw\", \"editing\", \"clean\", and \"avg\" are "
                                                            "available."));
        case CPD3GUI::Invoked_Editing:
            return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                         tr("The station to show"),
                                                         tr("Inferred from the current directory"),
                                                         tr("The \"station\" bare word argument is used to specify the station displayed.  "
                                                            "This is the three digit station identification code.  For example "
                                                            "\"brw\"."))
                                         << BareWordHelp(tr("times", "times argument name"),
                                                         tr("The time range of data to display"),
                                                         QString(),
                                                         TimeParse::describeListBoundsUsage(false,
                                                                                            true,
                                                                                            Time::Week));
        case CPD3GUI::Invoked_Realtime:
            return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                         tr("The station to show"),
                                                         tr("Inferred from the current directory"),
                                                         tr("The \"station\" bare word argument is used to specify the station displayed.  "
                                                            "This is the three digit station identification code.  For example "
                                                            "\"brw\"."))
                                         << BareWordHelp(tr("duration", "druation argument name"),
                                                         tr("The amount of data backwards in time to show"),
                                                         tr("One week"),
                                                         TimeParse::describeDurationUsage(false,
                                                                                          false,
                                                                                          Time::Week));
        }
        Q_ASSERT(false);
        return QList<BareWordHelp>();
    }

    virtual QString getBareWordCommandLine()
    {
        switch (invokedMode) {
        case CPD3GUI::Invoked_Standard:
            return tr("[station] [times] [mode]", "bare word arguments");
        case CPD3GUI::Invoked_Editing:
            return tr("[station] [times]", "bare word arguments");
        case CPD3GUI::Invoked_Realtime:
            return tr("[station] [duration]", "bare word arguments");
        }
        Q_ASSERT(false);
        return QString();
    }

    virtual QString getProgramName()
    { return programName; }

    virtual QString getDescription()
    {
        switch (invokedMode) {
        case CPD3GUI::Invoked_Standard:
            return tr("This is the main graphical display for CPD3.  This shows various plots of "
                      "available data.  By default it will display the last week of raw data "
                      "starting at the previous midnight UTC.  The display mode can be switched to "
                      "display data that has been passed by the station mentor or to a view that "
                      "allows for comparison of data so that the mentor can make appropriate edits.");
        case CPD3GUI::Invoked_Realtime:
            return tr("This is a graphical display of realtime data.  It will show text based "
                      "displays of the various instruments connected to the acquisition system "
                      "as well as plots of their recent data.  By default the last day of data will "
                      "be shown on the plots.  It also allows for control of the acquisition "
                      "system.");
        case CPD3GUI::Invoked_Editing:
            return tr("This is the main graphical display for CPD3.  This shows various plots of "
                      "available data.  By default it will display the last week of data in "
                      "comparison mode so the station mentor can make edits to the data.  The "
                      "display mode can be switched to display data that has been passed by the "
                      "mentor or to display the full set of raw data.");
        }
        Q_ASSERT(false);
        return QString();
    }

    virtual QString getExamples(const QList<ComponentExample> &examples)
    {
        Q_UNUSED(examples);
        switch (invokedMode) {
        case CPD3GUI::Invoked_Standard:
            return tr("\nExamples:\n"
                      "General usage:\n\n"
                      "    %1 brw 2010:1 2010:2\n\n"
                      "This displays data for the station \"brw\" from 2010-01-01 to "
                      "2010-01-02.\n\n"
                      "Default one week of data:\n\n"
                      "    %1 sgp\n\n"
                      "This displays the last seven days of data for the station \"sgp\" starting "
                      "at the previous midnight UTC.\n\n"
                      "Explicit mode:\n\n"
                      "    %1 thd 2012w3 clean\n\n"
                      "This displays the third week of 2012 (2012-01-16 to 2012-01-23) using the "
                      "\"clean\" display mode.  That is, this will show only data that has been "
                      "passed by the station mentor.").arg(programName);
        case CPD3GUI::Invoked_Editing:
            return tr("\nExamples:\n"
                      "General usage:\n\n"
                      "    %1 brw 2010:1 2010:2\n\n"
                      "This displays data for the station \"brw\" from 2010-01-01 to "
                      "2010-01-02.\n\n"
                      "Default one week of data:\n\n"
                      "    %1 sgp\n\n"
                      "This displays the last seven days of data for the station \"sgp\" starting "
                      "at the previous midnight UTC.").arg(programName);
        case CPD3GUI::Invoked_Realtime:
            return tr("\nExamples:\n"
                      "General usage:\n\n"
                      "    %1 2h\n\n"
                      "This displays the last two hours of data for the current station.").arg(
                    programName);
        }
        Q_ASSERT(false);
        return QString();
    }

    virtual QHash<QString, QString> getDocumentationTypeID()
    {
        QHash<QString, QString> result;
        result.insert("type", "gui");
        return result;
    };
};
}

static QString getInvokedName(const QString &argument)
{
    QFileInfo file(argument);
    QString result(file.fileName());
    if (result.endsWith(".exe", Qt::CaseInsensitive))
        result.chop(4);
    if (!result.isEmpty())
        return result;
    return argument;
}

int CPD3GUI::parseArguments(QStringList arguments)
{
    Archive::Access access;
    Archive::Access::ReadLock lock(access);

    QString programName(tr("cpx3", "program name"));
    if (!arguments.isEmpty())
        programName = getInvokedName(arguments.takeFirst());
    determineInvokedMode(programName, arguments);

    ComponentOptions options;
    options.add("ignore-state", new ComponentOptionBoolean(tr("ignore-state", "name"),
                                                           tr("Ignore saved window state"),
                                                           tr("If set then the saved window state is discarded."),
                                                           QString()));

    if (profile.isEmpty()) {
        profile = QString::fromStdString(SequenceName::impliedProfile(&access));
        options.add("profile",
                    new ComponentOptionSingleString(tr("profile", "name"), tr("Display profile"),
                                                    tr("This is the name of the display profile to use.  The "
                                                       "display profile determines what graphs and components are "
                                                       "shown in the main window.  If not specified it will default "
                                                       "to the \"%1\" profile.  Valid profiles include \"aerosol\", "
                                                       "\"ozone\", \"wx\", and \"met\".  "
                                                       "Consult the station configuration for available profiles.  "
                                                       "This option is not case sensitive.").arg(
                                                            profile),
                                                    tr("%1", "profile default").arg(profile)));
    }

    Parser parser(programName, invokedMode);
    try {
        parser.parse(arguments, options);
    } catch (ArgumentParsingException ape) {
        if (ape.isError()) {
            if (!ape.getText().isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(ape.getText(), true);
#else
                QMessageBox::critical(0, tr("Invalid Usage"), ape.getText());
#endif
            }
            QCoreApplication::exit(1);
            return -1;
        } else {
            if (!ape.getText().isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(ape.getText());
#else
                QMessageBox::information(0, tr("Help"), ape.getText());
#endif
            }
            QCoreApplication::exit(0);
        }
        return 1;
    }

    determineStation(access, arguments);
    if (station.empty()) {
#ifndef NO_CONSOLE
        TerminalOutput::simpleWordWrap(
                tr("Unable to determine station.  This can be the result of an invalid station "
                   "being specified, or the specified station not existing."), true);
#else
        QMessageBox::critical(0, tr("Invalid Usage"), tr(
"Unable to determine station.  This can be the result of an invalid station "
"being specified, or the specified station not existing."));
#endif
        return -1;
    }

    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get();
    }
    if (profile.isEmpty()) {
        profile = "aerosol";
    } else {
        profile = profile.toLower();
    }
    handleProfileAlias(access);

    determineMode(access, arguments);
    if (mode.isEmpty()) {
#ifndef NO_CONSOLE
        TerminalOutput::simpleWordWrap(
                tr("Unrecognized or unsupported profile and mode combination.  This can occur "
                   "because the profile and/or mode does not exist or is not supported on the "
                   "requested station."), true);
#else
        QMessageBox::critical(0, tr("Invalid Usage"), tr(
"Unrecognized or unsupported profile and mode combination.  This can occur "
"because the profile and/or mode does not exist or is not supported on the "
"requested station."));
#endif
        return -1;
    }

    switch (invokedMode) {
    case Invoked_Standard:
    case Invoked_Editing:
        if (!arguments.isEmpty()) {
            try {
                TimeParseNOOPListHandler handler;
                displayBounds =
                        TimeParse::parseListBounds(arguments, &handler, displayBounds, false, false,
                                                   Time::Week);
            } catch (TimeParsingException tpe) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(tr("Error parsing time: %1\n"
                                                  "This may be the result of other argument being invalid and being interpreted "
                                                  "as a time argument.  Ensure that both the time specification and the "
                                                  "other arguments listed are valid.  The first argument of the \"time\" was "
                                                  "\"%2\" and the last one was \"%3\".").arg(
                        tpe.getDescription(), arguments.first(), arguments.last()), true);
#else
                QMessageBox::critical(0, tr("Invalid Usage"), tr(
"Error parsing time: %1\n"
"This may be the result of other argument being invalid and being interpreted "
"as a time argument.  Ensure that both the time specification and the "
"other arguments listed are valid.  The first argument of the \"time\" was "
"\"%2\" and the last one was \"%3\".").arg(
                    tpe.getDescription(), arguments.first(), arguments.last()));
#endif
                return -1;
            }
        }
        initialBounds = displayBounds;
        break;

    case Invoked_Realtime:
        if (!arguments.isEmpty()) {
            try {
                TimeParse::parseOffset(arguments.join(" "), &realtimeWindowUnit,
                                       &realtimeWindowCount, &realtimeWindowAlign, false, false);
            } catch (TimeParsingException tpe) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(tr("Error parsing duration: %1\n"
                                                  "This may be the result of other argument being invalid and being interpreted "
                                                  "as a duration argument.  Ensure that both the duration specification and the "
                                                  "other arguments listed are valid.  The first argument of the \"duration\" was "
                                                  "\"%2\" and the last one was \"%3\".").arg(
                        tpe.getDescription(), arguments.first(), arguments.last()), true);
#else
                QMessageBox::critical(0, tr("Invalid Usage"), tr(
"Error parsing duration: %1\n"
"This may be the result of other argument being invalid and being interpreted "
"as a duration argument.  Ensure that both the duration specification and the "
"other arguments listed are valid.  The first argument of the \"duration\" was "
"\"%2\" and the last one was \"%3\".").arg(
                    tpe.getDescription(), arguments.first(), arguments.last()));
#endif
                return -1;
            }
        }
        break;
    }


    doRestoreState = !qobject_cast<ComponentOptionBoolean *>(options.get("ignore-state"))->get();

    return 0;
}

void CPD3GUI::determineInvokedMode(QString &programName, QStringList &arguments)
{
    if (programName.startsWith(tr("da.edit", "program name"), Qt::CaseInsensitive)) {
        QString check(programName.mid(tr("da.edit", "program name").length() + 1));
        if (!check.isEmpty()) {
            profile = check;
            profile.replace('.', "_");
        }
        invokedMode = Invoked_Editing;
        return;
    }
    if (programName.startsWith(tr("cpd3.realtime", "program name"), Qt::CaseInsensitive)) {
        QString check(programName.mid(tr("cpd3.realtime", "program name").length() + 1));
        if (!check.isEmpty()) {
            profile = check;
            profile.replace('.', "_");
        }
        invokedMode = Invoked_Realtime;
        return;
    }
    if (programName.startsWith(tr("cpx3.", "program name"), Qt::CaseInsensitive)) {
        QString check(programName.mid(tr("cpx3.", "program name").length()));
        if (!check.isEmpty()) {
            profile = check;
            profile.replace('.', "_");
            invokedMode = Invoked_Standard;
            return;
        }
    }
    if (programName.startsWith(tr("cpx.", "program name"), Qt::CaseInsensitive)) {
        QString check(programName.mid(tr("cpx.", "program name").length()));
        if (!check.isEmpty()) {
            profile = check;
            profile.replace('.', "_");
            invokedMode = Invoked_Standard;
            return;
        }
    }
    if (programName.startsWith(tr("da.gui", "program name"), Qt::CaseInsensitive)) {
        QString check(programName.mid(tr("da.gui", "program name").length() + 1));
        if (!check.isEmpty()) {
            profile = check;
            profile.replace('.', "_");
        }
        invokedMode = Invoked_Standard;
        return;
    }

    if (!arguments.isEmpty() && arguments.at(0).startsWith(tr("--mode=", "mode switch"))) {
        QString check(arguments.takeFirst().mid(tr("--mode=", "mode switch").length()).toLower());
        if (check == tr("editing", "editing mode") || check == tr("edit", "editing mode")) {
            programName = tr("da.edit", "program name");
            invokedMode = Invoked_Editing;
            return;
        } else if (check == tr("realtime", "realtime mode") || check == tr("rt", "realtime mode")) {
            programName = tr("cpd3.realtime", "program name");
            invokedMode = Invoked_Realtime;
            return;
        }
    }
}


void CPD3GUI::determineStation(Archive::Access &access, QStringList &arguments)
{
    if (!arguments.isEmpty()) {
        auto stations = access.availableStations({arguments.front().toStdString()});
        if (stations.size() == 1) {
            arguments.removeFirst();
            station = *(stations.begin());
            baseDisplayContext.setStation(station);
            return;
        }
    }

    auto check = SequenceName::impliedStation(&access);
    if (!check.empty()) {
        station = check;
        baseDisplayContext.setStation(station);
    }
}

void CPD3GUI::handleProfileAlias(Archive::Access &access)
{
    double now = Time::time();
    auto segments = ValueSegment::Stream::read(
            Archive::Selection(now, FP::undefined(), {station}, {"configuration"}, {"displays"}),
            &access);
    if (segments.empty() || Range::compareStart(segments[0].getStart(), now) > 0)
        return;
    QString check(segments.front().value().hash("ProfileAlias").hash(profile).toQString());
    if (check.isEmpty())
        return;
    profile = check.toLower();
}

void CPD3GUI::determineMode(Archive::Access &access, QStringList &arguments)
{
    auto segments = ValueSegment::Stream::read(
            Archive::Selection(FP::undefined(), FP::undefined(), {station}, {"configuration"},
                               {"displays"}), &access);

    if (invokedMode == Invoked_Standard && !arguments.isEmpty()) {
        QString check(arguments.last().toLower());
        double now = Time::time();
        if (!segments.empty() && Range::compareStart(segments.front().getStart(), now) <= 0) {
            QString check2(segments.front().value().hash("ModeAlias").hash(check).toQString());
            if (!check2.isEmpty())
                check = check2.toLower();
        }
        for (const auto &seg : segments) {
            if (!seg.value().hash("Profiles").hash(profile).hash(check).exists()) {
                continue;
            }
            arguments.removeLast();
            mode = check;
            return;
        }
    }

    QString check;
    switch (invokedMode) {
    case Invoked_Standard:
        check = "raw";
        break;
    case Invoked_Realtime:
        check = "realtime";
        break;
    case Invoked_Editing:
        check = "editing";
        break;
    }
    if (!check.isEmpty()) {
        for (const auto &seg : segments) {
            if (!seg.value().hash("Profiles").hash(profile).hash(check).exists()) {
                continue;
            }
            mode = check;
            return;
        }
    }
}

QString CPD3GUI::statePath(const QString &id) const
{
    QString result("maingui/");
    result.append(QString::fromStdString(station));
    result.append('/');
    result.append(profile);
    switch (invokedMode) {
    case Invoked_Standard:
        result.append("/standard/");
        break;
    case Invoked_Realtime:
        result.append("/realtime/");
        break;
    case Invoked_Editing:
        result.append("/editing/");
        break;
    }
    result.append(id);
    return result;
}

void CPD3GUI::initializeConfiguration()
{
    configuration.clear();
    archive = {};
    for (const auto &add : baseConfiguration) {
        Variant::Root cfg(add.value().hash("Profiles").hash(profile).hash(mode));
        if (cfg["Archive"].exists())
            archive = cfg["Archive"].toString();
        configuration.emplace_back(add.getStart(), add.getEnd(), std::move(cfg));
    }
}

void CPD3GUI::loadConfiguration()
{
    baseConfiguration = ValueSegment::Stream::read(
            Archive::Selection(FP::undefined(), FP::undefined(), {station}, {"configuration"},
                               {"displays"}));

    initializeConfiguration();
}

bool CPD3GUI::allowMainWindowClose()
{
    if (editDirectiveDialog) {
        if (editDirectiveDialog->isVisible()) {
            if (!editDirectiveDialog->mainWindowClosing())
                return false;
        }
    }

    return true;
}

Variant::Read CPD3GUI::getRealtimeGenericConfiguration()
{
    auto configCheck = Range::findIntersecting(configuration, getStart(), getEnd());

    Variant::Read genericDisplays = Variant::Read::empty();
    if (configCheck != configuration.end())
        genericDisplays = configCheck->value().hash("RealtimeWindows");

    if (genericDisplays.exists()) {
        if (genericDisplays.getType() == Variant::Type::Hash) {
            if (!genericDisplays.toHash().empty())
                return genericDisplays;
        } else if (genericDisplays.getType() == Variant::Type::Array) {
            if (!genericDisplays.toArray().empty())
                return genericDisplays;
        }
    }

    if (configCheck != configuration.end()) {
        if (configCheck->value().hash("RealtimeHideStandard").toBool())
            return Variant::Read::empty();
    }

    auto standardConfig = ValueSegment::Stream::read(
            Archive::Selection(getStart(), getEnd(), {station}, {"configuration"}, {"realtime"}));
    if (standardConfig.empty())
        return Variant::Read::empty();

    return standardConfig.back().value().hash("Windows");
}

void CPD3GUI::createGenericDisplays()
{
    for (auto add : acquisitionGenericConfiguration.toChildren()) {
        GenericHandler *handler = new GenericHandler(add, acquisitionGeneric.size(), this);
        acquisitionGeneric.emplace_back(handler);
        if (mainWindow)
            mainWindow->addGenericHandler(handler);
    }
}

void CPD3GUI::updateRealtime()
{
    auto configCheck = Range::findIntersecting(configuration, getStart(), getEnd());
    Variant::Read realtime = Variant::Read::empty();
    if (configCheck != configuration.cend())
        realtime = configCheck->value().hash("Realtime");

    if ((!realtime.exists() ||
            (realtime.getType() == Variant::Type::Boolean && !realtime.toBool())) &&
            invokedMode != Invoked_Realtime) {
        dataAccess.purgeCache();

        if (mainWindow) {
            mainWindow->setRealtimeToggleEnabled(false);
            if (acquisitionConnection) {
                mainWindow->setFixedTime(getStart(), getEnd());
            }
        }

        acquisitionCommandHandler.reset();
        acquisitionConnection.reset();

        acquisitionMenuTopLevel.clear();
        acquisitionMenuInterfaceActions.clear();
        acquisitionMenuGenericActions.clear();
        haveAcquisitionConnection = false;
        if (mainWindow) {
            mainWindow->removeAllAcquisitionInterfaces();
            mainWindow->removeAllGenericHandlers();
        }
        acquisitionInterfaces.clear();
        acquisitionGeneric.clear();
        commandManager.reset(true);
        updateAcquisitionSystemStatus();

        return;
    }

    if (!acquisitionCommandHandler) {
        acquisitionCommandHandler.reset(new AcquisitionTrayCommandHandler);
        acquisitionCommandHandler->commandReceived
                                 .connect(this, std::bind(&CPD3GUI::acquisitionTrayCommand, this,
                                                          std::placeholders::_1), true);
    }

    if (mainWindow) {
        mainWindow->setRealtimeToggleEnabled(true);
        if (!acquisitionConnection) {
            mainWindow->setRealtime(realtimeWindowUnit, realtimeWindowCount, realtimeWindowAlign);
        }
    }

    if (acquisitionConnection) {
        if (acquisitionConnection->getConfiguration() != realtime) {
            acquisitionConnection.reset();

            if (messageLogDialog) {
                messageLogDialog.release()->deleteLater();
            }
            acquisitionMenuTopLevel.clear();
            acquisitionMenuInterfaceActions.clear();
            acquisitionMenuGenericActions.clear();
        }
    }

    auto genericDisplays = getRealtimeGenericConfiguration();

    if (!acquisitionConnection) {
        dataAccess.purgeCache();

        if (mainWindow) {
            mainWindow->removeAllAcquisitionInterfaces();
            mainWindow->removeAllGenericHandlers();
        }
        acquisitionInterfaces.clear();
        acquisitionGeneric.clear();
        commandManager.reset(true);

        QByteArray checkOverride(qgetenv("CPD3REALTIMEOVERRIDE"));
        if (!checkOverride.isEmpty())
            realtime = Variant::Root(QString(checkOverride)).read();

        haveAcquisitionConnection = false;
        acquisitionConnection.reset(new AcquisitionNetworkClient(realtime));
        acquisitionConnection->setRealtimeEgress(&acquisitionRealtimeIngress);
        acquisitionConnection->interfaceInformationUpdated
                             .connect(this, std::bind(&CPD3GUI::acquisitionInformationUpdated, this,
                                                      std::placeholders::_1), true);
        acquisitionConnection->interfaceStateUpdated
                             .connect(this, std::bind(&CPD3GUI::acquisitionStateUpdated, this,
                                                      std::placeholders::_1), true);
        acquisitionConnection->interfaceInformationUpdated
                             .connect(this,
                                      std::bind(&CPD3GUI::updateAcquisitionSystemStatus, this),
                                      true);
        acquisitionConnection->interfaceStateUpdated
                             .connect(this,
                                      std::bind(&CPD3GUI::updateAcquisitionSystemStatus, this),
                                      true);
        acquisitionConnection->connectionState
                             .connect(this, std::bind(&CPD3GUI::acquisitionConnectionState, this,
                                                      std::placeholders::_1), true);
        acquisitionConnection->realtimeEvent
                             .connect(this, std::bind(&CPD3GUI::acquisitionRealtimeEvent, this,
                                                      std::placeholders::_1), true);

        acquisitionGenericConfiguration = genericDisplays;
        createGenericDisplays();

        acquisitionConnection->start();
        updateAcquisitionSystemStatus();
    } else {
        if (genericDisplays != acquisitionGenericConfiguration) {
            if (mainWindow)
                mainWindow->removeAllGenericHandlers();
            acquisitionGeneric.clear();

            acquisitionGenericConfiguration = genericDisplays;
            createGenericDisplays();
        }
    }
}

void CPD3GUI::startup()
{
    loadConfiguration();
    updateRealtime();

    mainWindow.reset(new GUIMainWindow(this));
    mainWindow->setWindowTitle(
            tr("CPD3 %1", "station title").arg(QString::fromStdString(station).toUpper()));

    if (!acquisitionConnection) {
        inRealtimePlotMode = false;
        mainWindow->setFixedTime(getStart(), getEnd());
        mainWindow->setRealtimeToggleEnabled(false);
    } else {
        inRealtimePlotMode = true;
        recalculateBounds();
        mainWindow->setRealtime(realtimeWindowUnit, realtimeWindowCount, realtimeWindowAlign);
        mainWindow->setRealtimeToggleEnabled(true);
    }

    /* Add any existing handlers to the window */
    for (const auto &add : acquisitionGeneric) {
        mainWindow->addGenericHandler(add.get());
    }
    for (const auto &add : acquisitionInterfaces) {
        mainWindow->addAcquisitionInterface(add.second.get());
    }

#ifdef Q_OS_MAC
    mainMenu.reset(new QMenuBar(0));
#else
    mainMenu.reset(mainWindow->menuBar());
#endif

    QAction *act;

    fileMenu = mainMenu->addMenu(tr("&File", "Menu|File"));

    showMentorEditsAction = new QAction(tr("Mentor &Edits", "Menu|File|MentorEdits"), fileMenu);
    showMentorEditsAction->setShortcut(tr("CTRL-E", "Menu|File|MentorEdits"));
    showMentorEditsAction->setToolTip(tr("Display the mentor edits manager."));
    showMentorEditsAction->setStatusTip(tr("Modify mentor edits"));
    showMentorEditsAction->setWhatsThis(
            tr("This displays the manager for mentor edits.  Mentor edits are modifications to the data applied during processing to correct for system faults or anomalous data."));
    connect(showMentorEditsAction, SIGNAL(triggered()), this, SLOT(showMentorEdits()));
    fileMenu->addAction(showMentorEditsAction);

    enterFullScreenAction = new QAction(tr("&Fullscreen", "Menu|File|Fullscreen"), fileMenu);
    enterFullScreenAction->setShortcut(tr("CTRL-F", "Menu|File|Fullscreen"));
    enterFullScreenAction->setToolTip(tr("Show the current display in full screen mode."));
    enterFullScreenAction->setStatusTip(tr("Show the current display in full screen"));
    enterFullScreenAction->setWhatsThis(
            tr("This will display the currently active pane to be displayed in full screen mode.  Pressing escape will exit full screen mode."));
    connect(enterFullScreenAction, SIGNAL(triggered()), this, SLOT(enterFullScreen()));
    fileMenu->addAction(enterFullScreenAction);

    fileMenuSeparator1 = fileMenu->addSeparator();

    showViewDataAction = new QAction(tr("View &Data", "Menu|File|ViewData"), fileMenu);
    showViewDataAction->setToolTip(tr("Open the data viewer."));
    showViewDataAction->setStatusTip(tr("Open the data viewer"));
    showViewDataAction->setWhatsThis(
            tr("This will open a simple viewer that allows you to see a basic table of data values."));
    connect(showViewDataAction, SIGNAL(triggered()), this, SLOT(showViewData()));
    fileMenu->addAction(showViewDataAction);

    showViewEventLogAction = new QAction(tr("View Event &Log", "Menu|File|ViewEventLog"), fileMenu);
    showViewEventLogAction->setToolTip(tr("Open the event log viewer."));
    showViewEventLogAction->setStatusTip(tr("Open the event log viewer"));
    showViewEventLogAction->setWhatsThis(
            tr("This will open a dialog that allows you to view the event log.  The event log contains both user entered messages and system state information logged automatically."));
    connect(showViewEventLogAction, SIGNAL(triggered()), this, SLOT(showViewEventLog()));
    fileMenu->addAction(showViewEventLogAction);

    fileMenuSeparator2 = fileMenu->addSeparator();

    saveDisplayAction = new QAction(tr("&Save Image", "Menu|File|SaveImage"), fileMenu);
    saveDisplayAction->setShortcuts(QKeySequence::Save);
    saveDisplayAction->setToolTip(tr("Save an image of the current display."));
    saveDisplayAction->setStatusTip(tr("Save an image of the current display"));
    saveDisplayAction->setWhatsThis(
            tr("This will save an image of the currently selected display to an external file."));
    connect(saveDisplayAction, SIGNAL(triggered()), this, SLOT(saveDisplay()));
    fileMenu->addAction(saveDisplayAction);

    printDisplayAction = new QAction(tr("&Print", "Menu|File|Print"), fileMenu);
    printDisplayAction->setShortcuts(QKeySequence::Print);
    printDisplayAction->setToolTip(tr("Print the current display."));
    printDisplayAction->setStatusTip(tr("Print the current display"));
    printDisplayAction->setWhatsThis(
            tr("This allows you to print an image of the currently selected display."));
    connect(printDisplayAction, SIGNAL(triggered()), this, SLOT(printDisplay()));
    fileMenu->addAction(printDisplayAction);

    exportDataAction = new QAction(tr("E&xport Data", "Menu|File|ExpotData"), fileMenu);
    exportDataAction->setToolTip(tr("Export data to a file."));
    exportDataAction->setStatusTip(tr("Export data to a file"));
    exportDataAction->setWhatsThis(
            tr("This will open a dialog that allows you to export simple data to a file you select."));
    connect(exportDataAction, SIGNAL(triggered()), this, SLOT(exportData()));
    fileMenu->addAction(exportDataAction);

#ifdef Q_OS_MAC
    act = new QAction("quit", fileMenu);
    fileMenuSeparator3 = NULL;
#else
    fileMenuSeparator3 = fileMenu->addSeparator();
    act = new QAction(tr("&Quit", "Menu|File|Quit"), fileMenu);
#endif
    act->setMenuRole(QAction::QuitRole);
    act->setShortcuts(QKeySequence::Quit);
    act->setToolTip(tr("Quit the application."));
    act->setStatusTip(tr("Quit"));
    act->setWhatsThis(tr("Close the display application."));
    connect(act, SIGNAL(triggered()), mainWindow.get(), SLOT(close()));
    //connect(act, SIGNAL(triggered()), this, SIGNAL(quitRequested()));
    fileMenu->addAction(act);


    editMenu = mainMenu->addMenu(tr("&Edit", "Menu|Edit"));

#ifdef Q_OS_MAC
    showPreferencesAction = new QAction("preferences", editMenu);
#else
    showPreferencesAction = new QAction(tr("&Preferences", "Menu|Edit|Preferences"), editMenu);
#endif
    showPreferencesAction->setShortcuts(QKeySequence::Preferences);
    showPreferencesAction->setMenuRole(QAction::PreferencesRole);
    showPreferencesAction->setToolTip(tr("Show user preferences."));
    showPreferencesAction->setStatusTip(tr("Show user preferences"));
    showPreferencesAction->setWhatsThis(
            tr("This allows you to configure user specific preferences.  These do no affect the system configuration but allow customization of things like time display formats."));
    connect(showPreferencesAction, SIGNAL(triggered()), this, SLOT(showPreferences()));
    editMenu->addAction(showPreferencesAction);

    showDisplaySettingsAction =
            new QAction(tr("&Display Settings", "Menu|Edit|DisplaySettings"), editMenu);
    showDisplaySettingsAction->setToolTip(tr("Show the global display settings dialog."));
    showDisplaySettingsAction->setStatusTip(tr("Show display settings"));
    showDisplaySettingsAction->setWhatsThis(
            tr("This allows you to perform advanced customization of the displays."));
    connect(showDisplaySettingsAction, SIGNAL(triggered()), this, SLOT(showDisplaySettings()));
    editMenu->addAction(showDisplaySettingsAction);

    showAcquisitionConfigurationAction =
            new QAction(tr("&Acquisition Configuration", "Menu|Edit|AcquisitionConfiguration"),
                        editMenu);
    showAcquisitionConfigurationAction->setToolTip(
            tr("Show the configuration for the acquisition system."));
    showAcquisitionConfigurationAction->setStatusTip(tr("Acquisition system configuration"));
    showAcquisitionConfigurationAction->setWhatsThis(
            tr("This will display the configuration dialog for the acquisition system.  Changes will not take effect until the acquisition system is restarted."));
    connect(showAcquisitionConfigurationAction, SIGNAL(triggered()), this,
            SLOT(showAcquisitionConfiguration()));
    editMenu->addAction(showAcquisitionConfigurationAction);

    showSystemConfigurationAction =
            new QAction(tr("&System Configuration", "Menu|Edit|SystemConfiguration"), editMenu);
    showSystemConfigurationAction->setToolTip(tr("Show the main configuration editor."));
    showSystemConfigurationAction->setStatusTip(tr("System configuration"));
    showSystemConfigurationAction->setWhatsThis(
            tr("This will display a general editor for system configuration."));
    connect(showSystemConfigurationAction, SIGNAL(triggered()), this,
            SLOT(showSystemConfiguration()));
    editMenu->addAction(showSystemConfigurationAction);

    displayMenu = mainMenu->addMenu(tr("&Display", "Menu|Display"));
    displayMenu->menuAction()->setVisible(false);

    actionMenu = mainMenu->addMenu(tr("&Operations", "Menu|Action"));
    actionMenu->menuAction()->setVisible(false);
    connect(actionMenu, SIGNAL(triggered(QAction * )), this, SLOT(performAction(QAction * )));

    viewMenu = mainMenu->addMenu(tr("&View", "Menu|Action"));
    viewMenu->menuAction()->setVisible(true);

    viewBack = new QAction(tr("&Back", "Menu|View|Back"), viewMenu);
    viewBack->setToolTip(tr("Return to the previously selected display."));
    viewBack->setStatusTip(tr("Back"));
    viewBack->setWhatsThis(
            tr("This will return the activate display to the one previously selected."));
    connect(viewBack, SIGNAL(triggered()), mainWindow.get(), SLOT(activatePreviousSelection()));
    viewMenu->addAction(viewBack);

    viewNext = new QAction(tr("&Next", "Menu|View|Next"), viewMenu);
    viewNext->setToolTip(tr("Advance to the next display in order."));
    viewNext->setStatusTip(tr("Next"));
    viewNext->setWhatsThis(
            tr("This will advance the selected display to the next one in sorted order."));
    connect(viewNext, SIGNAL(triggered()), mainWindow.get(), SLOT(activateNext()));
    viewMenu->addAction(viewNext);

    viewPrevious = new QAction(tr("&Previous", "Menu|View|Previous"), viewMenu);
    viewPrevious->setToolTip(tr("Advance to the previous display in order."));
    viewPrevious->setStatusTip(tr("Previous"));
    viewPrevious->setWhatsThis(
            tr("This will advance the selected display to the previous one in sorted order."));
    connect(viewPrevious, SIGNAL(triggered()), mainWindow.get(), SLOT(activatePrevious()));
    viewMenu->addAction(viewPrevious);

    viewResetAll = new QAction(tr("Reset &All", "Menu|View|ResetAll"), viewMenu);
    viewResetAll->setToolTip(tr("Reset the displayed data to the initial state."));
    viewResetAll->setStatusTip(tr("Reset"));
    viewResetAll->setWhatsThis(
            tr("This resets the display to the initial state, effectively undoing all zooms and replots."));
    connect(viewResetAll, SIGNAL(triggered()), mainWindow.get(), SLOT(backToStart()));
    viewMenu->addAction(viewResetAll);

    viewInitial = new QAction(tr("&Initial Times", "Menu|View|Initial"), viewMenu);
    viewInitial->setToolTip(tr("Restore the visible time range to the initial start on."));
    viewInitial->setStatusTip(tr("Initial times"));
    viewInitial->setWhatsThis(
            tr("This replots the data with the time range selected on initial startup."));
    connect(viewInitial, SIGNAL(triggered()), this, SLOT(replotInitial()));
    viewMenu->addAction(viewInitial);

    viewReplot = new QAction(tr("&Replot", "Menu|View|Replot"), viewMenu);
    viewReplot->setToolTip(tr("Replot visible data and apply the time selection."));
    viewReplot->setStatusTip(tr("Replot data"));
    viewReplot->setWhatsThis(
            tr("This replots data with the selected time range.  Data is reloaded from disk and plots are cleared and updated with the time range selected."));
    connect(viewReplot, SIGNAL(triggered()), mainWindow.get(), SLOT(replotPressed()));
    viewMenu->addAction(viewReplot);

    viewLegendToggle = new QAction(tr("&Legends", "Menu|View|Legend"), viewMenu);
    viewLegendToggle->setToolTip(tr("Toggle the display of all legends."));
    viewLegendToggle->setStatusTip(tr("Legend"));
    viewLegendToggle->setWhatsThis(
            tr("This will toggle the display status of all legends on all graphs."));
    viewLegendToggle->setCheckable(true);
    viewLegendToggle->setChecked(true);
    connect(viewLegendToggle, SIGNAL(toggled(bool)), mainWindow.get(), SLOT(legendToggle(bool)));
    if (restoreState()) {
        viewLegendToggle->setChecked(settings->value(statePath("view/showLegends"), true).toBool());
    }
    viewMenu->addAction(viewLegendToggle);

    viewMenuSeparator1 = viewMenu->addSeparator();

    acquisitionMenu = mainMenu->addMenu(tr("&Acquisition", "Menu|Acquisition"));
    acquisitionMenu->menuAction()->setVisible(false);
    connect(acquisitionMenu, SIGNAL(aboutToShow()), this, SLOT(updateAcquisitionMenu()));
    updateAcquisitionMenu();

    helpMenu = mainMenu->addMenu(tr("&Help", "Menu|Help"));

    showHelpAction = new QAction(tr("&Help", "Menu|Help|Help"), helpMenu);
    showHelpAction->setShortcuts(QKeySequence::HelpContents);
    showHelpAction->setToolTip(tr("Show the help."));
    showHelpAction->setStatusTip(tr("Show the help"));
    showHelpAction->setWhatsThis(tr("This will open a web browser to display the local help."));
    connect(showHelpAction, SIGNAL(triggered()), this, SLOT(showHelp()));
    helpMenu->addAction(showHelpAction);

    showWebsiteAction = new QAction(tr("&Website", "Menu|Help|Website"), helpMenu);
    showWebsiteAction->setToolTip(tr("Open the website."));
    showWebsiteAction->setStatusTip(tr("Open the website"));
    showWebsiteAction->setWhatsThis(tr("This will open a web browser to the online help."));
    connect(showWebsiteAction, SIGNAL(triggered()), this, SLOT(showWebsite()));
    helpMenu->addAction(showWebsiteAction);

    showBugReportAction = new QAction(tr("Report &Bug", "Menu|Help|BugReport"), helpMenu);
    showBugReportAction->setToolTip(tr("Open a bug report dialog."));
    showBugReportAction->setStatusTip(tr("Report a bug"));
    showBugReportAction->setWhatsThis(
            tr("This will open a dialog that allows you to submit a bug report or feature request."));
    connect(showBugReportAction, SIGNAL(triggered()), this, SLOT(showBugReport()));
    helpMenu->addAction(showBugReportAction);

    showDebugAction = new QAction(tr("Show &Debug", "Menu|Help|Debug"), helpMenu);
    showDebugAction->setToolTip(tr("Open the debug log."));
    showDebugAction->setStatusTip(tr("Show the debug log"));
    showDebugAction->setWhatsThis(tr("This will display the contents of the debug log."));
    connect(showDebugAction, SIGNAL(triggered()), this, SLOT(showDebug()));
    helpMenu->addAction(showDebugAction);

#ifdef Q_OS_MAC
    showAboutAction = new QAction("about", helpMenu);
    helpMenuSeparator1 = NULL;
#else
    helpMenuSeparator1 = helpMenu->addSeparator();
    showAboutAction = new QAction(tr("&About", "Menu|Help|About"), helpMenu);
#endif
    showAboutAction->setMenuRole(QAction::AboutRole);
    showAboutAction->setToolTip(tr("About CPD3."));
    showAboutAction->setStatusTip(tr("About CPD3"));
    showAboutAction->setWhatsThis(tr("This displays information about the application."));
    connect(showAboutAction, SIGNAL(triggered()), this, SLOT(showAbout()));
    helpMenu->addAction(showAboutAction);

    shortcutBack = new QShortcut(QKeySequence(Qt::Key_B), mainWindow.get(),
                                 SLOT(activatePreviousSelection()));
    shortcutBack->setWhatsThis(tr("MReturn to the previously selected display."));

    shortcutNext = new QShortcut(QKeySequence(Qt::Key_N), mainWindow.get(), SLOT(activateNext()));
    shortcutNext->setWhatsThis(tr("Move the display to the next logical one."));

    shortcutPrevious =
            new QShortcut(QKeySequence(Qt::Key_P), mainWindow.get(), SLOT(activatePrevious()));
    shortcutPrevious->setWhatsThis(tr("Move the display to the previous logical one."));

    shortcutResetAll =
            new QShortcut(QKeySequence(Qt::Key_A), mainWindow.get(), SLOT(backToStart()));
    shortcutResetAll->setWhatsThis(tr("This resets the displayed data to the initial state."));

    shortcutReplot =
            new QShortcut(QKeySequence(Qt::Key_R), mainWindow.get(), SLOT(replotPressed()));
    shortcutReplot->setWhatsThis(
            tr("This replots data with the selected time range.  Data is reloaded from disk and plots are cleared and updated with the time range selected."));

    shortcutLegendToggle = new QShortcut(QKeySequence(Qt::Key_L), mainWindow.get());
    connect(shortcutLegendToggle, SIGNAL(activated()), viewLegendToggle, SLOT(toggle()));
    shortcutLegendToggle->setWhatsThis(tr("Move the display to the previous logical one."));

    QShortcut *escape =
            new QShortcut(QKeySequence(Qt::Key_Escape), mainWindow.get(), SLOT(escapePressed()));
    escape->setWhatsThis(tr("Clear the current keyboard focus."));

    updateMenuVisiblity();
    applyReplot();

    mainWindow->show();
}

void CPD3GUI::updateMenuVisiblity()
{
    auto configCheck(Range::findIntersecting(configuration, getStart(), getEnd()));
    Variant::Read effective = Variant::Read::empty();
    if (configCheck != configuration.cend())
        effective = configCheck->value().hash("Menu");

    showMentorEditsAction->setVisible(!effective["HideMentorEdits"].toBool());
    enterFullScreenAction->setVisible(!effective["HideFullScreen"].toBool());
    if (fileMenuSeparator1 != NULL) {
        fileMenuSeparator1->setVisible(
                showMentorEditsAction->isVisible() || enterFullScreenAction->isVisible());
    }
    showViewDataAction->setVisible(!effective["HideViewData"].toBool());
    showViewEventLogAction->setVisible(!effective["HideEventLog"].toBool());
    if (fileMenuSeparator2 != NULL) {
        fileMenuSeparator2->setVisible(
                showViewDataAction->isVisible() || showViewEventLogAction->isVisible());
    }
    saveDisplayAction->setVisible(!effective["HideSaveDisplay"].toBool());
    printDisplayAction->setVisible(!effective["HidePrintDisplay"].toBool());
    exportDataAction->setVisible(!effective["HideExportData"].toBool());
    if (fileMenuSeparator3 != NULL) {
        fileMenuSeparator3->setVisible(saveDisplayAction->isVisible() ||
                                               printDisplayAction->isVisible() ||
                                               exportDataAction->isVisible());
    }
#ifdef Q_OS_MAC
    fileMenu->menuAction()->setVisible(showMentorEditsAction->isVisible() ||
        enterFullScreenAction->isVisible() || showViewDataAction->isVisible() ||
        showViewEventLogAction->isVisible() || saveDisplayAction->isVisible() ||
        saveDisplayAction->isVisible() || printDisplayAction->isVisible() ||
        exportDataAction->isVisible());
#endif

    showPreferencesAction->setVisible(!effective["HidePreferences"].toBool());
    showDisplaySettingsAction->setVisible(!effective["HideDisplaySettings"].toBool());
    if (effective["HideAcquisitionConfiguration"].exists()) {
        showAcquisitionConfigurationAction->setVisible(
                !effective["HideAcquisitionConfiguration"].toBool());
    } else {
        showAcquisitionConfigurationAction->setVisible(acquisitionConnection.get() != nullptr);
    }
    showSystemConfigurationAction->setVisible(!effective["HideSystemConfiguration"].toBool());
    editMenu->menuAction()
            ->setVisible(showPreferencesAction->isVisible() ||
                                 showDisplaySettingsAction->isVisible() ||
                                 showAcquisitionConfigurationAction->isVisible() ||
                                 showSystemConfigurationAction->isVisible());

    showHelpAction->setVisible(!effective["HideHelp"].toBool());
    showWebsiteAction->setVisible(!effective["HideWebsite"].toBool());
    //showBugReportAction->setVisible(!effective["HideBugReport"].toBool());
    showBugReportAction->setVisible(false);
    showDebugAction->setVisible(!effective["HideDebug"].toBool());
    if (helpMenuSeparator1 != NULL) {
        helpMenuSeparator1->setVisible(showHelpAction->isVisible() ||
                                               showWebsiteAction->isVisible() ||
                                               showBugReportAction->isVisible() ||
                                               showDebugAction->isVisible());
    }
    showAboutAction->setVisible(!effective["HideAbout"].toBool());
    helpMenu->menuAction()
            ->setVisible(showHelpAction->isVisible() ||
                                 showWebsiteAction->isVisible() ||
                                 showBugReportAction->isVisible() ||
                                 showDebugAction->isVisible()
#ifdef Q_OS_MAC
                    || showAboutAction->isVisible()
#endif
            );

    shortcutBack->setEnabled(!effective["DisableBackShortcut"].toBool());
    shortcutNext->setEnabled(!effective["DisableAdvanceShortcut"].toBool());
    shortcutPrevious->setEnabled(!effective["DisableAdvanceShortcut"].toBool());
    shortcutReplot->setEnabled(!effective["DisableReplotShortcut"].toBool());
    shortcutLegendToggle->setEnabled(!effective["DisableLegendToggleShortcut"].toBool());


    updateDisplayMenu();
    updateActionMenu();
    if (effective["HideDisplayMenu"].toBool())
        displayMenu->menuAction()->setVisible(false);

    viewBack->setVisible(!effective["HideViewBack"].toBool());
    viewNext->setVisible(!effective["HideViewNext"].toBool());
    viewPrevious->setVisible(!effective["HideViewPrevious"].toBool());
    viewResetAll->setVisible(!effective["HideViewResetAll"].toBool());
    viewReplot->setVisible(!effective["HideViewReplot"].toBool());
    viewInitial->setVisible(!effective["HideViewInitial"].toBool() &&
                                    FP::defined(initialBounds.getStart()) &&
                                    FP::defined(initialBounds.getEnd()));
    viewLegendToggle->setVisible(!effective["HideViewLegends"].toBool());
    updateViewMenuVisiblity();

    updateAcquisitionMenuVisibility();
}

void CPD3GUI::writeOutState()
{
    if (viewLegendToggle != NULL) {
        settings->setValue(statePath("view/showLegends"), viewLegendToggle->isChecked());
    }
}

void CPD3GUI::updateDisplayMenu()
{
    struct DisplayMenuSortItem {
        QString mode;
        QString name;
        qint64 sortPriority;
        Variant::Read config;

        bool operator<(const DisplayMenuSortItem &other) const
        {
            if (!INTEGER::defined(sortPriority)) {
                if (INTEGER::defined(other.sortPriority))
                    return false;
            } else if (!INTEGER::defined(other.sortPriority)) {
                return true;
            } else if (sortPriority != other.sortPriority) {
                return sortPriority < other.sortPriority;
            }
            return name < other.name;
        }
    };

    std::unordered_map<std::string, DisplayMenuSortItem> displayMenuContents;
    for (const auto &add : baseConfiguration) {
        for (auto child : add.value().hash("Profiles").hash(profile).toHash()) {
            if (child.first.empty())
                continue;
            if (child.second.getType() != Variant::Type::Hash)
                continue;
            if (child.second.hash("UserHide").toBool())
                continue;
            DisplayMenuSortItem item;
            item.mode = QString::fromStdString(child.first);
            item.name = child.second.hash("Name").toDisplayString();
            if (item.name.isEmpty())
                item.name = item.mode;
            item.sortPriority = child.second.hash("SortPriority").toInt64();
            item.config = child.second;
            displayMenuContents.emplace(child.first, std::move(item));
        }
    }

    if (displayMenuGroup != NULL)
        displayMenuGroup->deleteLater();

    displayMenu->clear();

    displayMenuGroup = new QActionGroup(displayMenu);
    displayMenuGroup->setExclusive(true);
    {
        std::vector<DisplayMenuSortItem> sorted;
        for (auto &add : displayMenuContents) {
            sorted.emplace_back(std::move(add.second));
        }
        std::sort(sorted.begin(), sorted.end());
        bool haveSelectedCurrentMode = false;
        for (const auto &add : sorted) {
            QAction *act = new QAction(add.name, displayMenuGroup);
            act->setToolTip(add.config.hash("ToolTip").toDisplayString());
            act->setStatusTip(add.config.hash("StatusTip").toDisplayString());
            act->setWhatsThis(add.config.hash("WhatsThis").toDisplayString());
            act->setCheckable(true);
            displayMenu->addAction(act);
            displayMenuGroup->addAction(act);
            if (add.mode == mode) {
                act->setChecked(true);
                haveSelectedCurrentMode = true;
            } else {
                act->setChecked(false);
            }
            act->setProperty("displayModeSelected", add.mode);
        }
        if (sorted.empty() || (sorted.size() == 1 && haveSelectedCurrentMode)) {
            displayMenu->menuAction()->setVisible(false);
        } else {
            displayMenu->menuAction()->setVisible(true);
        }
    }
    connect(displayMenuGroup, SIGNAL(triggered(QAction * )), this,
            SLOT(displayModeSelected(QAction * )));
}

void CPD3GUI::updateViewMenu()
{
    for (QList<QAction *>::const_iterator act = viewSelection.constBegin(),
            end = viewSelection.constEnd(); act != end; ++act) {
        viewMenu->removeAction(*act);
        (*act)->deleteLater();
    }
    viewSelection.clear();

    if (mainWindow)
        return;

    QList<GUIDisplayInterface *> interfaces(mainWindow->getHandlerInterfaces());
    for (QList<GUIDisplayInterface *>::const_iterator win = interfaces.constBegin(),
            end = interfaces.constEnd(); win != end; ++win) {
        const GUIDisplayHandlerInterface
                *disp = qobject_cast<const GUIDisplayHandlerInterface *>(*win);
        if (!disp)
            continue;
        QAction *act = new QAction(disp->getTitle(), viewMenu);
        act->setToolTip(tr("Show the display."));
        act->setStatusTip(tr("Show the associated display"));
        act->setWhatsThis(tr("This will show and bring to the front the data display."));
        act->setProperty("display", QVariant::fromValue<DisplayHandler *>(disp->getHandler()));
        viewMenu->addAction(act);
        viewSelection.append(act);

        connect(act, SIGNAL(triggered()), this, SLOT(showDisplayWindow()));
    }

    viewMenuSeparator1->setVisible((viewBack->isVisible() ||
            viewNext->isVisible() ||
            viewPrevious->isVisible() ||
            viewResetAll->isVisible() ||
            viewReplot->isVisible() ||
            viewInitial->isVisible() ||
            viewLegendToggle->isVisible()) && !viewSelection.isEmpty());
}

void CPD3GUI::updateViewMenuVisiblity()
{
    auto configCheck = Range::findIntersecting(configuration, getStart(), getEnd());
    Variant::Read effective = Variant::Read::empty();
    if (configCheck != configuration.cend())
        effective = configCheck->value().hash("Menu");

    updateViewMenu();

    viewMenu->menuAction()
            ->setVisible(!effective["HideViewMenu"].toBool() ||
                                 (viewBack->isVisible() ||
                                         viewNext->isVisible() ||
                                         viewPrevious->isVisible() ||
                                         viewResetAll->isVisible() ||
                                         viewInitial->isVisible() ||
                                         viewReplot->isVisible() ||
                                         viewLegendToggle->isVisible() ||
                                         !viewSelection.isEmpty()));
}

void CPD3GUI::updateActionMenu()
{
    struct ActionMenuSortItem {
        QString name;
        qint64 sortPriority;
        Variant::Read config;

        bool operator<(const ActionMenuSortItem &other) const
        {
            if (!INTEGER::defined(sortPriority)) {
                if (INTEGER::defined(other.sortPriority))
                    return false;
            } else if (!INTEGER::defined(other.sortPriority)) {
                return true;
            } else if (sortPriority != other.sortPriority) {
                return sortPriority < other.sortPriority;
            }
            return name < other.name;
        }
    };

    auto configCheck = Range::findIntersecting(configuration, getStart(), getEnd());
    Variant::Read effective = Variant::Read::empty();
    if (configCheck != configuration.cend())
        effective = configCheck->value().hash("Menu");

    std::vector<ActionMenuSortItem> sorted;
    for (auto child : effective.hash("Actions").toHash()) {
        if (child.first.empty())
            continue;
        if (child.second.getType() != Variant::Type::Hash)
            continue;
        if (child.second.hash("Hide").toBool())
            continue;
        ActionMenuSortItem item;
        item.name = child.second.hash("Name").toDisplayString();
        if (item.name.isEmpty())
            item.name = QString::fromStdString(child.first);
        item.sortPriority = child.second.hash("SortPriority").toInt64();
        item.config = child.second;
        sorted.emplace_back(std::move(item));
    }

    bool enableUser = !effective["HideUserAction"].toBool();

    actionMenu->clear();
    if (sorted.empty() && !enableUser) {
        actionMenu->menuAction()->setVisible(false);
        return;
    }

    std::sort(sorted.begin(), sorted.end());
    for (const auto &add : sorted) {
        QAction *act = new QAction(add.name, actionMenu);
        act->setToolTip(add.config.hash("ToolTip").toDisplayString());
        act->setStatusTip(add.config.hash("StatusTip").toDisplayString());
        act->setWhatsThis(add.config.hash("WhatsThis").toDisplayString());
        act->setProperty("actionConfig", QVariant::fromValue<Variant::Read>(add.config));
        actionMenu->addAction(act);
    }

    if (enableUser) {
        if (!sorted.empty())
            actionMenu->addSeparator();

        QAction *act = new QAction(tr("Manual Selection", "Menu|Action|Manual"), actionMenu);
        act->setToolTip(tr("Perform a manual operation"));
        act->setStatusTip(tr("Manual operation"));
        act->setWhatsThis(
                tr("This allows you to perform a manual operation by specifying all parameters to the action."));

        Variant::Root config;
        config["ComponentPrompt"].setBool(true);
        config["TimePrompt"].setBool(true);
        config["OptionsPrompt"].setBool(true);
        act->setProperty("actionConfig", QVariant::fromValue<Variant::Read>(config.read()));
        actionMenu->addAction(act);
    }

    actionMenu->menuAction()->setVisible(!effective["HideActionMenu"].toBool());
}

void CPD3GUI::acquisitionInformationUpdated(const std::string &name)
{
    if (!acquisitionConnection)
        return;

    auto info = acquisitionConnection->getInterfaceInformation(name);

    if (info.read().exists()) {
        commandManager.updateCommands(QString::fromStdString(name), info["Commands"]);

        auto check = acquisitionInterfaces.find(QString::fromStdString(name));
        InterfaceHandler *interface;
        if (check == acquisitionInterfaces.end()) {
            interface = new InterfaceHandler(QString::fromStdString(name), this);
            acquisitionInterfaces.emplace(QString::fromStdString(name),
                                          std::unique_ptr<InterfaceHandler>(interface));

            /* Force a menu rebuild */
            acquisitionMenuTopLevel.clear();
            acquisitionMenuInterfaceActions.remove(QString::fromStdString(name));

            if (mainWindow && !info["MenuHide"].toBool())
                mainWindow->addAcquisitionInterface(interface);

            /* Ensure the display is reloaded in a timely fashion so new plots
             * are created */
            realtimeForceReplotTimer.start();
        } else {
            interface = check->second.get();
        }
        interface->updateInformation(info);
    } else {
        commandManager.updateCommands(QString::fromStdString(name), Variant::Root());
        auto check = acquisitionInterfaces.find(QString::fromStdString(name));
        if (check != acquisitionInterfaces.end()) {
            if (mainWindow)
                mainWindow->removeAcquisitionInterface(check->second.get());
            acquisitionInterfaces.erase(check);

            /* Force a menu rebuild */
            acquisitionMenuTopLevel.clear();
            acquisitionMenuInterfaceActions.remove(QString::fromStdString(name));
        }
    }


    updateAcquisitionMenu();
}

void CPD3GUI::acquisitionStateUpdated(const std::string &name)
{
    if (!acquisitionConnection)
        return;

    auto target = acquisitionInterfaces.find(QString::fromStdString(name));
    if (target == acquisitionInterfaces.end())
        return;
    target->second->updateState(acquisitionConnection->getInterfaceState(name));

    /* Because contamination and such affect this.  This also calls the
     * global state update, so the status bar gets updated too. */
    updateAcquisitionMenuVisibility();
}

void CPD3GUI::updateAcquisitionMenu()
{
    class AcquisitionMenuTopLevelSortItem {
    public:
        QString name;
        qint64 sortPriority;

        enum {
            Interface, AggregateCommand, Generic,
        } type;
        InterfaceHandler *interface;
        GenericHandler *generic;
        Variant::Read commandData;

        AcquisitionMenuTopLevelSortItem(const QString &n) : name(n),
                                                            sortPriority(0),
                                                            type(Interface),
                                                            interface(NULL),
                                                            generic(NULL),
                                                            commandData()
        { }

        bool operator<(const AcquisitionMenuTopLevelSortItem &other) const
        {
            if (!INTEGER::defined(sortPriority)) {
                if (INTEGER::defined(other.sortPriority))
                    return false;
            } else if (!INTEGER::defined(other.sortPriority)) {
                return true;
            } else if (sortPriority != other.sortPriority) {
                return sortPriority < other.sortPriority;
            }
            return name.compare(other.name, Qt::CaseInsensitive) < 0;
        }

        QString getMenuText() const
        {
            QString text;
            switch (type) {
            case Interface: {
                text = interface->getMenuText();
                if (text.isEmpty())
                    text = interface->getName();
                QString hotkey(interface->getMenuHotkey());
                if (!hotkey.isEmpty()) {
                    int idx = text.indexOf(hotkey, 0, Qt::CaseInsensitive);
                    if (idx >= 0) {
                        text.insert(idx, '&');
                    }
                }
                break;
            }
            case AggregateCommand:
                text = commandData.hash("DisplayName").toDisplayString();
                if (text.isEmpty())
                    text = commandData.hash("Command").toQString();
                break;
            case Generic: {
                text = generic->getMenuText();
                break;
            }
            }
            if (text.isEmpty())
                text = CPD3GUI::tr("Unnamed");
            return text;
        }
    };

    if (!acquisitionConnection) {
        acquisitionMenu->menuAction()->setVisible(false);

        acquisitionMenuTopLevel.clear();
        acquisitionMenuInterfaceActions.clear();
        acquisitionMenuGenericActions.clear();
        acquisitionMenu->clear();
        return;
    }

    std::vector<AcquisitionMenuTopLevelSortItem> topLevelMenu;

    auto configCheck = Range::findIntersecting(configuration, getStart(), getEnd());
    Variant::Read effective = Variant::Read::empty();
    if (configCheck != configuration.cend())
        effective = configCheck->value().hash("Menu");

    for (const auto &add : acquisitionGeneric) {
        if (add->hideInAcquisitionMenu())
            continue;

        AcquisitionMenuTopLevelSortItem se(add->getMenuText());
        se.type = AcquisitionMenuTopLevelSortItem::Generic;
        se.generic = add.get();
        topLevelMenu.emplace_back(std::move(se));
    }
    for (const auto &add : acquisitionInterfaces) {
        if (add.second->hideInAcquisitionMenu())
            continue;
        if (effective.hash("Acquisition").hash(add.first).hash("MenuHide").toBool())
            continue;

        AcquisitionMenuTopLevelSortItem se(add.second->getMenuText());
        se.type = AcquisitionMenuTopLevelSortItem::Interface;
        se.interface = add.second.get();
        se.commandData = commandManager.getCommands(add.second->getName());
        topLevelMenu.emplace_back(std::move(se));
    }
    for (const auto &add : commandManager.getAggregateCommands()) {
        if (!add["GlobalMenu"].toBool())
            continue;
        if (effective.hash("Aggregate").hash(add["Command"].toString()).hash("Hide").toBool())
            continue;

        AcquisitionMenuTopLevelSortItem se(add["DisplayName"].toDisplayString());
        se.name.replace('&', QString());
        se.type = AcquisitionMenuTopLevelSortItem::AggregateCommand;
        se.commandData = add;
        topLevelMenu.emplace_back(std::move(se));
    }
    std::stable_sort(topLevelMenu.begin(), topLevelMenu.end());

    /* Always update initially (menu just created) */
    bool needTopLevelUpdate = acquisitionMenu->isEmpty();
    if (acquisitionMenuTopLevel.size() == static_cast<int>(topLevelMenu.size())) {
        for (std::size_t i = 0, max = topLevelMenu.size(); i < max && !needTopLevelUpdate; i++) {
            if (topLevelMenu[i].getMenuText() != acquisitionMenuTopLevel.at(i)->text()) {
                needTopLevelUpdate = true;
                break;
            }
            switch (topLevelMenu[i].type) {
            case AcquisitionMenuTopLevelSortItem::AggregateCommand:
                if (acquisitionMenuTopLevel.at(i)->property("commandData").value<Variant::Read>() !=
                        topLevelMenu[i].commandData) {
                    needTopLevelUpdate = true;
                    break;
                }
                break;
            case AcquisitionMenuTopLevelSortItem::Interface:
                if (acquisitionMenuTopLevel.at(i)
                                           ->property("interface")
                                           .value<InterfaceHandler *>() !=
                        topLevelMenu[i].interface) {
                    needTopLevelUpdate = true;
                    break;
                }
                break;
            case AcquisitionMenuTopLevelSortItem::Generic:
                if (acquisitionMenuTopLevel.at(i)
                                           ->property("genericHandler")
                                           .value<GenericHandler *>() != topLevelMenu[i].generic) {
                    needTopLevelUpdate = true;
                    break;
                }
                break;
            }
        }
    } else {
        needTopLevelUpdate = true;
    }

    if (needTopLevelUpdate) {
        acquisitionMenuInterfaceActions.clear();
        acquisitionMenuGenericActions.clear();
        acquisitionMenuTopLevel.clear();
        acquisitionMenu->clear();

        messageLogEntryAction =
                new QAction(tr("Message Log Entry", "Menu|Acquisition|MessageLogEntry"),
                            acquisitionMenu);
        messageLogEntryAction->setToolTip(tr("Show the message log entry dialog."));
        messageLogEntryAction->setStatusTip(tr("Add user message log entry"));
        messageLogEntryAction->setWhatsThis(
                tr("This allows you to add a user entry to the message log.  This is a general text message associated with the acquisition data at the time it is entered."));
        messageLogEntryAction->setShortcut(tr("CTRL-Enter", "Menu|Acquisition|MessageLogEntry"));
        connect(messageLogEntryAction, SIGNAL(triggered()), this, SLOT(showMessageLogEntry()));
        acquisitionMenu->addAction(messageLogEntryAction);

        QMenu *systemStatusMenu = new QMenu(tr("System Status"), acquisitionMenu);
        acquisitionMenu->addMenu(systemStatusMenu);
        systemStatusMenuAction = systemStatusMenu->menuAction();

        setContaminationAction = new QAction(tr("Set &Contamination"), systemStatusMenu);
        setContaminationAction->setToolTip(tr("Enable the user contamination flag."));
        setContaminationAction->setStatusTip(tr("Enable user contamination"));
        setContaminationAction->setWhatsThis(
                tr("Enable the user contamination flag.  This will cause data to be flagged as contamination and excluded from longer run averaging."));
        setContaminationAction->setVisible(false);
        connect(setContaminationAction, SIGNAL(triggered()), this, SLOT(setContamination()));
        systemStatusMenu->addAction(setContaminationAction);

        clearContaminationAction = new QAction(tr("Clear &Contamination"), systemStatusMenu);
        clearContaminationAction->setToolTip(tr("Disable the user contamination flag."));
        clearContaminationAction->setStatusTip(tr("Disable user contamination"));
        clearContaminationAction->setWhatsThis(
                tr("Disable the user contamination flag.  This will end the period of time that will be excluded from longer averaging."));
        clearContaminationAction->setVisible(false);
        connect(clearContaminationAction, SIGNAL(triggered()), this, SLOT(clearContamination()));
        systemStatusMenu->addAction(clearContaminationAction);

        overrideContaminationAction = new QAction(tr("&Override Contamination"), systemStatusMenu);
        overrideContaminationAction->setToolTip(tr("Disable all contamination flags."));
        overrideContaminationAction->setStatusTip(tr("Disable all contamination flags"));
        overrideContaminationAction->setWhatsThis(
                tr("This will remove all contamination flags from the system; even those set automatically by internal components."));
        overrideContaminationAction->setVisible(false);
        connect(overrideContaminationAction, SIGNAL(triggered()), this,
                SLOT(overrideContamination()));
        systemStatusMenu->addAction(overrideContaminationAction);

        setBypassAction = new QAction(tr("Set &Bypass"), systemStatusMenu);
        setBypassAction->setToolTip(tr("Enable the user bypass flag."));
        setBypassAction->setStatusTip(tr("Enable user bypass"));
        setBypassAction->setWhatsThis(
                tr("Enable the user bypass flag.  This disables averaging and causes the system to enter bypass mode."));
        setBypassAction->setVisible(false);
        connect(setBypassAction, SIGNAL(triggered()), this, SLOT(setBypass()));
        systemStatusMenu->addAction(setBypassAction);

        clearBypassAction = new QAction(tr("Clear &Bypass"), systemStatusMenu);
        clearBypassAction->setToolTip(tr("Disable the user bypass flag."));
        clearBypassAction->setStatusTip(tr("Disable user bypass"));
        clearBypassAction->setWhatsThis(
                tr("Disable the user bypass flag.  This will re-enable averaging and cause the system to exit bypass mode if no other bypasses are in effect."));
        clearBypassAction->setVisible(false);
        connect(clearBypassAction, SIGNAL(triggered()), this, SLOT(clearBypass()));
        systemStatusMenu->addAction(clearBypassAction);

        overrideBypassAction = new QAction(tr("O&verride Bypass"), systemStatusMenu);
        overrideBypassAction->setToolTip(tr("Disable all bypass flags."));
        overrideBypassAction->setStatusTip(tr("Disable all bypass flags"));
        overrideBypassAction->setWhatsThis(
                tr("This will remove all bypass flags from the system; even those set automatically by internal components.  This may result in undesirable system behavior."));
        overrideBypassAction->setVisible(false);
        connect(overrideBypassAction, SIGNAL(triggered()), this, SLOT(overrideBypass()));
        systemStatusMenu->addAction(overrideBypassAction);

        setAveragingAction = new QAction(tr("Change &Averaging Time"), systemStatusMenu);
        setAveragingAction->setToolTip(tr("Change the acquisition averaging time."));
        setAveragingAction->setStatusTip(tr("Change the averaging time"));
        setAveragingAction->setWhatsThis(
                tr("This allows you to change the averaging time the acquisition system is using."));
        connect(setAveragingAction, SIGNAL(triggered()), this, SLOT(setAveragingTime()));
        systemStatusMenu->addAction(setAveragingAction);

        showAutoprobeStatusAction = new QAction(
                tr("Show Auto&detection Status", "Menu|Acquisition|ShowAutoprobeStatus"),
                acquisitionMenu);
        showAutoprobeStatusAction->setToolTip(
                tr("Display the status of acquisition autodetection."));
        showAutoprobeStatusAction->setStatusTip(tr("Show autodetection status"));
        showAutoprobeStatusAction->setWhatsThis(
                tr("This will display a window with the status of the acquisition autodetection details."));
        connect(showAutoprobeStatusAction, SIGNAL(triggered()), this,
                SLOT(showAcquisitionAutoprobeStatus()));
        systemStatusMenu->addAction(showAutoprobeStatusAction);

        restartSystemAction = new QAction(tr("&Restart System", "Menu|Acquisition|RestartSystem"),
                                          acquisitionMenu);
        restartSystemAction->setToolTip(
                tr("Restart the acquisition system and reload its configuration."));
        restartSystemAction->setStatusTip(tr("Restart the acquisition system"));
        restartSystemAction->setWhatsThis(
                tr("This will restart the acquisition system.  Data collection will be momentarily interrupted and all configuration changes will be reloaded."));
        connect(restartSystemAction, SIGNAL(triggered()), this, SLOT(showAcquisitionRestart()));
        systemStatusMenu->addAction(restartSystemAction);

        for (const auto &add : topLevelMenu) {
            switch (add.type) {
            case AcquisitionMenuTopLevelSortItem::Interface: {
                QMenu *interfaceMenu = new QMenu(add.getMenuText(), acquisitionMenu);
                QAction *act = interfaceMenu->menuAction();
                act->setProperty("baseMenu", QVariant::fromValue<QMenu *>(interfaceMenu));
                act->setProperty("interface",
                                 QVariant::fromValue<InterfaceHandler *>(add.interface));
                acquisitionMenuTopLevel.append(act);
                acquisitionMenu->addMenu(interfaceMenu);
                break;
            }
            case AcquisitionMenuTopLevelSortItem::AggregateCommand: {
                QAction *act = new QAction(add.getMenuText(), acquisitionMenu);
                act->setProperty("commandData",
                                 QVariant::fromValue<Variant::Read>(add.commandData));
                act->setToolTip(add.commandData["ToolTip"].toDisplayString());
                act->setStatusTip(add.commandData["StatusTip"].toDisplayString());
                act->setWhatsThis(add.commandData["WhatsThis"].toDisplayString());
                connect(act, SIGNAL(triggered()), this, SLOT(issueAcquisitionCommand()));

                QString shortcut(add.commandData["Shortcut"].toDisplayString());
                if (!shortcut.isEmpty())
                    act->setShortcut(shortcut);

                acquisitionMenuTopLevel.append(act);
                acquisitionMenu->addAction(act);
                break;
            }
            case AcquisitionMenuTopLevelSortItem::Generic: {
                QMenu *genericMenu = new QMenu(add.getMenuText(), acquisitionMenu);
                QAction *act = genericMenu->menuAction();
                act->setProperty("baseMenu", QVariant::fromValue<QMenu *>(genericMenu));
                act->setProperty("genericHandler",
                                 QVariant::fromValue<GenericHandler *>(add.generic));
                acquisitionMenuTopLevel.append(act);
                acquisitionMenu->addMenu(genericMenu);
                break;
            }
            }
        }
    }

    for (auto act : acquisitionMenuTopLevel) {
        QVariant vMenu = act->property("baseMenu");
        if (vMenu.isNull())
            continue;
        QMenu *menu = vMenu.value<QMenu *>();
        if (!menu)
            continue;
        QVariant vTarget = act->property("interface");
        if (!vTarget.isNull()) {
            InterfaceHandler *interface = vTarget.value<InterfaceHandler *>();
            Q_ASSERT(interface);
            populateInterfaceMenu(interface, menu,
                                  acquisitionMenuInterfaceActions[interface->getName()]);
        }
        vTarget = act->property("genericHandler");
        if (!vTarget.isNull()) {
            GenericHandler *generic = vTarget.value<GenericHandler *>();
            Q_ASSERT(generic);
            while (acquisitionMenuGenericActions.size() <= generic->getID()) {
                acquisitionMenuGenericActions.append(QList<QAction *>());
            }
            populateGenericMenu(generic, menu, acquisitionMenuGenericActions[generic->getID()]);
        }
    }

    updateAcquisitionMenuVisibility();
}

namespace {
class AcquisitionCommandMenuSortItem {
public:
    QString name;
    qint64 sortPriority;
    qint64 displayPriority;
    Variant::Read commandData;

    bool operator<(const AcquisitionCommandMenuSortItem &other) const
    {
        if (!INTEGER::defined(sortPriority)) {
            if (INTEGER::defined(other.sortPriority))
                return false;
        } else if (!INTEGER::defined(other.sortPriority)) {
            return true;
        } else if (sortPriority != other.sortPriority) {
            return sortPriority < other.sortPriority;
        }
        if (!INTEGER::defined(displayPriority)) {
            if (INTEGER::defined(other.displayPriority))
                return false;
        } else if (!INTEGER::defined(other.displayPriority)) {
            return true;
        } else if (displayPriority != other.displayPriority) {
            return displayPriority < other.displayPriority;
        }
        return name.compare(other.name, Qt::CaseInsensitive) < 0;
    }

    QString getMenuText() const
    {
        QString text(commandData.hash("DisplayName").toDisplayString());
        if (text.isEmpty())
            text = commandData.hash("Command").toQString();
        if (text.isEmpty())
            text = CPD3GUI::tr("Unnamed");
        return text;
    }
};
}

static std::vector<AcquisitionCommandMenuSortItem> generateCommandList(InterfaceHandler *interface,
                                                                       const Variant::Read &effective,
                                                                       CommandManager &commandManager)
{
    std::vector<Variant::Read> commands;

    for (auto cmd : commandManager.getCommands(interface->getName()).read().toHash()) {
        if (cmd.first.empty())
            continue;
        Variant::Write add = Variant::Root(cmd.second).write();
        if (!add.hash("Targets").exists())
            add.hash("Targets").toArray().after_back() = interface->getName();
        if (!add.hash("Command").exists())
            add.hash("Command").setString(cmd.first);
        if (effective.hash("Interface")
                     .hash(interface->getName())
                     .hash(add.hash("Command").toString())
                     .hash("Hide")
                     .toBool())
            continue;
        commands.emplace_back(std::move(add));
    }

    for (const auto &cmd : commandManager.getAggregateCommands()) {
        if (effective.hash("Aggregate").hash(cmd["Command"].toString()).hash("Hide").toBool())
            continue;
        bool hit = false;
        for (auto check : cmd["Targets"].toArray()) {
            if (check.toQString() != interface->getName())
                continue;
            hit = true;
            break;
        }
        if (!hit)
            continue;
        commands.emplace_back(cmd.read());
    }

    std::vector<AcquisitionCommandMenuSortItem> sorted;

    for (auto &cmd : commands) {
        AcquisitionCommandMenuSortItem add;
        add.name = cmd.hash("DisplayName").toDisplayString();
        add.name.replace('&', QString());
        add.sortPriority = 0;
        qint64 i = cmd.hash("SortPriority").toInt64();
        if (INTEGER::defined(i))
            add.sortPriority = i;
        add.displayPriority = 0;
        i = cmd.hash("DisplayPriority").toInt64();
        if (INTEGER::defined(i))
            add.displayPriority = i;
        add.commandData = std::move(cmd);
        sorted.emplace_back(std::move(add));
    }
    std::stable_sort(sorted.begin(), sorted.end());

    return sorted;
}

void CPD3GUI::populateInterfaceMenu(InterfaceHandler *interface,
                                    QMenu *target,
                                    QList<QAction *> &existing)
{
    auto configCheck = Range::findIntersecting(configuration, getStart(), getEnd());
    Variant::Read effective = Variant::Read::empty();
    if (configCheck != configuration.cend())
        effective = configCheck->value().hash("Menu");

    auto sorted = generateCommandList(interface, effective, commandManager);

    if (static_cast<int>(sorted.size()) + AcquisitionInterface_ReservedTotal == existing.size()) {
        bool allMatched = true;
        for (std::size_t i = 0, max = sorted.size(); i < max; ++i) {
            if (sorted[i].getMenuText() != existing.at(i + 1)->text()) {
                allMatched = false;
                break;
            }
            if (existing.at(i)->property("commandData").value<Variant::Read>() !=
                    sorted[i].commandData) {
                allMatched = false;
                break;
            }
        }
        if (allMatched)
            return;
    }

    target->clear();
    existing.clear();

    QAction *act;

    /* Order must match AcquisitionInterfaceMenuReservedEntry enum */

    act = new QAction(tr("Show Display"), target);
    act->setToolTip(tr("Show the realtime data display."));
    act->setStatusTip(tr("Show the associated display"));
    act->setWhatsThis(
            tr("This will show and bring to the front the data display for the selected realtime interface."));
    act->setProperty("interface", QVariant::fromValue<InterfaceHandler *>(interface));
    connect(act, SIGNAL(triggered()), this, SLOT(showInterfaceWindow()));
    target->addAction(act);
    existing.append(act);

    for (const auto &add : sorted) {
        act = new QAction(add.getMenuText(), acquisitionMenu);
        act->setProperty("commandData", QVariant::fromValue<Variant::Read>(add.commandData));
        act->setToolTip(add.commandData["ToolTip"].toDisplayString());
        act->setStatusTip(add.commandData["StatusTip"].toDisplayString());
        act->setWhatsThis(add.commandData["WhatsThis"].toDisplayString());
        connect(act, SIGNAL(triggered()), this, SLOT(issueAcquisitionCommand()));

        QString shortcut(add.commandData["Shortcut"].toDisplayString());
        if (!shortcut.isEmpty())
            act->setShortcut(shortcut);

        target->addAction(act);
        existing.append(act);
    }
}

static std::vector<AcquisitionCommandMenuSortItem> generateCommandList(GenericHandler *generic,
                                                                       CommandManager &commandManager)
{
    std::vector<Variant::Read> commands;

    for (auto cmd : commandManager.getManual(generic->getName()).read().toHash()) {
        if (cmd.first.empty())
            continue;
        Variant::Write add = Variant::Root(cmd.second).write();
        if (!add.hash("Command").exists())
            add.hash("Command").setString(cmd.first);
        commands.emplace_back(std::move(add));
    }

    std::vector<AcquisitionCommandMenuSortItem> sorted;

    for (auto &cmd : commands) {
        AcquisitionCommandMenuSortItem add;
        add.name = cmd.hash("DisplayName").toDisplayString();
        add.name.replace('&', QString());
        add.sortPriority = 0;
        qint64 i = cmd.hash("SortPriority").toInt64();
        if (INTEGER::defined(i))
            add.sortPriority = i;
        add.displayPriority = 0;
        i = cmd.hash("DisplayPriority").toInt64();
        if (INTEGER::defined(i))
            add.displayPriority = i;
        add.commandData = std::move(cmd);
        sorted.emplace_back(std::move(add));
    }
    std::stable_sort(sorted.begin(), sorted.end());

    return sorted;
}

void CPD3GUI::populateGenericMenu(GenericHandler *generic,
                                  QMenu *target,
                                  QList<QAction *> &existing)
{
    auto sorted = generateCommandList(generic, commandManager);

    if (static_cast<int>(sorted.size()) + AcquisitionGeneric_ReservedTotal == existing.size()) {
        bool allMatched = true;
        for (std::size_t i = 0, max = sorted.size(); i < max; ++i) {
            if (sorted[i].getMenuText() != existing.at(i + 1)->text()) {
                allMatched = false;
                break;
            }
            if (existing.at(i)->property("commandData").value<Variant::Read>() !=
                    sorted[i].commandData) {
                allMatched = false;
                break;
            }
        }
        if (allMatched)
            return;
    }

    target->clear();
    existing.clear();

    QAction *act;

    /* Order must match AcquisitionGenericMenuReservedEntry enum */

    act = new QAction(tr("Show Display"), target);
    act->setToolTip(tr("Show the realtime data display."));
    act->setStatusTip(tr("Show the associated display"));
    act->setWhatsThis(
            tr("This will show and bring to the front the data display for the selected realtime interface."));
    act->setProperty("genericHandler", QVariant::fromValue<GenericHandler *>(generic));
    connect(act, SIGNAL(triggered()), this, SLOT(showGenericWindow()));
    target->addAction(act);
    existing.append(act);

    for (const auto &add : sorted) {
        act = new QAction(add.getMenuText(), acquisitionMenu);
        act->setProperty("commandData", QVariant::fromValue<Variant::Read>(add.commandData));
        act->setToolTip(add.commandData["ToolTip"].toDisplayString());
        act->setStatusTip(add.commandData["StatusTip"].toDisplayString());
        act->setWhatsThis(add.commandData["WhatsThis"].toDisplayString());
        connect(act, SIGNAL(triggered()), this, SLOT(issueAcquisitionCommand()));

        QString shortcut(add.commandData["Shortcut"].toDisplayString());
        if (!shortcut.isEmpty())
            act->setShortcut(shortcut);

        target->addAction(act);
        existing.append(act);
    }
}

void CPD3GUI::updateAcquisitionMenuVisibility()
{
    if (!acquisitionConnection) {
        acquisitionMenu->menuAction()->setVisible(false);
        return;
    }

    auto configCheck = Range::findIntersecting(configuration, getStart(), getEnd());
    Variant::Read effective = Variant::Read::empty();
    if (configCheck != configuration.cend())
        effective = configCheck->value().hash("Menu");

    if (effective["HideAcquisitionMenu"].toBool()) {
        acquisitionMenu->menuAction()->setVisible(false);
        return;
    }

    /* Update these so the page counts and window visibility are current */
    for (const auto &target : acquisitionInterfaces) {
        target.second->updateRealtime();
    }
    for (const auto &target : acquisitionGeneric) {
        target->updateRealtime();
    }

    messageLogEntryAction->setVisible(!effective["HideMessageLogEntry"].toBool());
    setAveragingAction->setVisible(!effective["HideAveragingTime"].toBool());
    restartSystemAction->setVisible(!effective["HideAcquisitionRestart"].toBool());
    showAutoprobeStatusAction->setVisible(!effective["HideAutoprobeStatus"].toBool());
    updateAcquisitionSystemStatus();
    systemStatusMenuAction->setVisible(messageLogEntryAction->isVisible() ||
                                               setAveragingAction->isVisible() ||
                                               restartSystemAction->isVisible() ||
                                               showAutoprobeStatusAction->isVisible() ||
                                               setContaminationAction->isVisible() ||
                                               clearContaminationAction->isVisible() ||
                                               overrideContaminationAction->isVisible() ||
                                               setBypassAction->isVisible() ||
                                               clearBypassAction->isVisible() ||
                                               overrideBypassAction->isVisible());

    bool haveAnyVisibleInterfaceMenu = false;

    for (QHash<QString, QList<QAction *> >::const_iterator
            actionData = acquisitionMenuInterfaceActions.constBegin(),
            endActionData = acquisitionMenuInterfaceActions.constEnd();
            actionData != endActionData;
            ++actionData) {
        auto checkInterface = acquisitionInterfaces.find(actionData.key());
        if (checkInterface == acquisitionInterfaces.end())
            continue;
        auto interface = checkInterface->second.get();

        if (actionData.value().isEmpty())
            continue;
        QAction *showWindowAction = getAcquisitionInterfaceReserved(actionData.value(),
                                                                    AcquisitionInterface_ShowWindow);
        showWindowAction->setVisible(interface->hasVisibleWindow());

        QAction *menuAction = NULL;
        for (QList<QAction *>::const_iterator check = acquisitionMenuTopLevel.constBegin(),
                endCheck = acquisitionMenuTopLevel.constEnd(); check != endCheck; ++check) {
            QVariant vTarget = (*check)->property("interface");
            if (vTarget.isNull())
                continue;
            if (vTarget.value<InterfaceHandler *>() == interface) {
                menuAction = *check;
                break;
            }
        }

        if (actionData.value().size() > AcquisitionInterface_ReservedTotal ||
                showWindowAction->isVisible()) {
            haveAnyVisibleInterfaceMenu = true;
            if (menuAction != NULL)
                menuAction->setVisible(true);
        } else {
            if (menuAction != NULL)
                menuAction->setVisible(false);
        }
    }

    for (int idx = 0, max = acquisitionMenuGenericActions.size(); idx < max; ++idx) {
        if (idx >= static_cast<int>(acquisitionGeneric.size()))
            continue;
        if (idx >= acquisitionMenuGenericActions.size() ||
                acquisitionMenuGenericActions.at(idx).isEmpty())
            continue;
        GenericHandler *generic = acquisitionGeneric[idx].get();
        Q_ASSERT(generic);

        QAction *showWindowAction =
                getAcquisitionGenericReserved(acquisitionMenuGenericActions.at(idx),
                                              AcquisitionGeneric_ShowWindow);
        showWindowAction->setVisible(generic->hasVisibleWindow());

        QAction *menuAction = nullptr;
        for (QList<QAction *>::const_iterator check = acquisitionMenuTopLevel.constBegin(),
                endCheck = acquisitionMenuTopLevel.constEnd(); check != endCheck; ++check) {
            QVariant vTarget = (*check)->property("genericHandler");
            if (vTarget.isNull())
                continue;
            if (vTarget.value<GenericHandler *>() == generic) {
                menuAction = *check;
                break;
            }
        }

        if (acquisitionMenuGenericActions.at(idx).size() > AcquisitionGeneric_ReservedTotal ||
                showWindowAction->isVisible()) {
            haveAnyVisibleInterfaceMenu = true;
            if (menuAction)
                menuAction->setVisible(true);
        } else {
            if (menuAction)
                menuAction->setVisible(false);
        }
    }

    acquisitionMenu->menuAction()
                   ->setVisible(haveAnyVisibleInterfaceMenu ||
                                        messageLogEntryAction->isVisible() ||
                                        systemStatusMenuAction->isVisible());
}

namespace {
struct StatusLineEntry {
    QString literal;
    QString addText;

    bool operator<(const StatusLineEntry &other) const
    { return literal < other.literal; }
};
}

void CPD3GUI::updateAcquisitionSystemStatus()
{
    auto configCheck = Range::findIntersecting(configuration, getStart(), getEnd());
    Variant::Read menuConfiguration = Variant::Read::empty();
    if (configCheck != configuration.cend())
        menuConfiguration = configCheck->value().hash("Menu");

    bool isContaminated = false;
    bool isUserContaminated = false;
    bool isBypassed = false;
    bool isUserBypassed = false;

    QList<StatusLineEntry> statusLineSort;
    for (const auto &target : acquisitionInterfaces) {
        const auto &systemFlags = target.second->getSystemFlags();
        isContaminated = isContaminated || Variant::Composite::isContaminationFlag(systemFlags);
        isUserContaminated = isUserContaminated || systemFlags.count("Contaminated") != 0;

        const auto &bypassFlags = target.second->getBypassFlags();
        isBypassed = isBypassed || !bypassFlags.empty();
        isUserBypassed = isUserBypassed || bypassFlags.count("Bypass") != 0;

        StatusLineEntry add;
        add.literal = target.second->getMenuHotkey();
        switch (target.second->getStatus()) {
        case CPD3::Acquisition::AcquisitionInterface::GeneralStatus::NoCommunications:
            add.addText =
                    tr("<font color='red'>%1</font>", "no comms status text").arg(add.literal);
            break;
        case CPD3::Acquisition::AcquisitionInterface::GeneralStatus::Disabled:
            add.addText =
                    tr("<font color='yellow'>%1</font>", "disabled status text").arg(add.literal);
            break;
        case CPD3::Acquisition::AcquisitionInterface::GeneralStatus::Normal:
            add.addText = tr("%1", "normal status text").arg(add.literal);
            break;
        }
        statusLineSort.append(add);
    }

    if (menuConfiguration["HideContamination"].toBool()) {
        if (setContaminationAction)
            setContaminationAction->setVisible(false);
        if (clearContaminationAction)
            clearContaminationAction->setVisible(false);
        if (overrideContaminationAction)
            overrideContaminationAction->setVisible(false);
    } else {
        if (setContaminationAction)
            setContaminationAction->setVisible(!isUserContaminated);
        if (clearContaminationAction)
            clearContaminationAction->setVisible(isUserContaminated);
        if (overrideContaminationAction)
            overrideContaminationAction->setVisible(isContaminated);
    }
    if (menuConfiguration["HideBypass"].toBool()) {
        if (setBypassAction)
            setBypassAction->setVisible(false);
        if (clearBypassAction)
            clearBypassAction->setVisible(false);
        if (overrideBypassAction)
            overrideBypassAction->setVisible(false);
    } else {
        if (setBypassAction)
            setBypassAction->setVisible(!isUserBypassed);
        if (clearBypassAction)
            clearBypassAction->setVisible(isUserBypassed);
        if (overrideBypassAction)
            overrideBypassAction->setVisible(isBypassed);
    }

    if (!mainWindow)
        return;
    if (!acquisitionConnection) {
        mainWindow->setRealtimeStatusString();
        return;
    }

    if (!haveAcquisitionConnection) {
        mainWindow->setRealtimeStatusString(
                tr("<font color='red'>NO CONNECTION</font>", "no connection status text"));
        return;
    }

    std::sort(statusLineSort.begin(), statusLineSort.end());

    QString statusLineText;
    for (QList<StatusLineEntry>::const_iterator add = statusLineSort.constBegin(),
            endAdd = statusLineSort.constEnd(); add != endAdd; ++add) {
        statusLineText.append(add->addText);
    }

    if (statusLineText.isEmpty()) {
        statusLineText =
                tr("<font color='orange'>NO INTERFACES</font>", "no interfaces status text");
    }

    QString systemFlags;
    if (isContaminated && isBypassed) {
        systemFlags =
                tr("<font color='orange'>Contaminated</font> and <font color='red'>Bypassed</font>",
                   "contaminated and bypassed flags");
    } else if (isContaminated) {
        systemFlags = tr("<font color='orange'>Contaminated</font>", "contaminated flags");
    } else if (isBypassed) {
        systemFlags = tr("<font color='red'>Bypassed</font>", "bypassed flags");
    }

    mainWindow->setRealtimeStatusString(statusLineText, systemFlags);
}

void CPD3GUI::acquisitionConnectionState(bool connected)
{
    haveAcquisitionConnection = connected;

    if (!connected) {
        if (mainWindow) {
            mainWindow->removeAllAcquisitionInterfaces();
        }
        acquisitionInterfaces.clear();
        commandManager.reset();
    }

    updateAcquisitionSystemStatus();
}

void CPD3GUI::acquisitionRealtimeEvent(const CPD3::Data::Variant::Root &event)
{
    if (!mainWindow)
        return;
    QString text = event.read().hash("Text").toDisplayString();
    if (text.isEmpty())
        return;
    mainWindow->addRealtimeMessage(tr("%1 %2: %3", "realtime event format").arg(
            GUITime::formatTime(event.read().hash("Time").toDouble(), settings.get()),
            event.read().hash("Source").toDisplayString(), text));
}

CPD3GUI::AcquisitionRealtimeIngress::AcquisitionRealtimeIngress(CPD3GUI &gui) : gui(gui)
{ }

CPD3GUI::AcquisitionRealtimeIngress::~AcquisitionRealtimeIngress() = default;

void CPD3GUI::AcquisitionRealtimeIngress::incomingData(const SequenceValue::Transfer &values)
{
    gui.dataAccess.incomingRealtime(values);

    bool doService = false;
    {
        std::lock_guard<std::mutex> lock(gui.acquisitionRealtimeMutex);
        doService = gui.acquisitionRealtimePending.empty();
        Util::append(values, gui.acquisitionRealtimePending);
    }
    if (doService)
        gui.acquisitionRealtimeUpdated();
}

void CPD3GUI::AcquisitionRealtimeIngress::incomingData(SequenceValue::Transfer &&values)
{
    gui.dataAccess.incomingRealtime(values);

    bool doService = false;
    {
        std::lock_guard<std::mutex> lock(gui.acquisitionRealtimeMutex);
        doService = gui.acquisitionRealtimePending.empty();
        Util::append(std::move(values), gui.acquisitionRealtimePending);
    }
    if (doService)
        gui.acquisitionRealtimeUpdated();
}

void CPD3GUI::AcquisitionRealtimeIngress::incomingData(const SequenceValue &value)
{
    gui.dataAccess.incomingRealtime(value);

    bool doService;
    {
        std::lock_guard<std::mutex> lock(gui.acquisitionRealtimeMutex);
        doService = gui.acquisitionRealtimePending.empty();
        gui.acquisitionRealtimePending.emplace_back(value);
    }
    if (doService)
        gui.acquisitionRealtimeUpdated();
}

void CPD3GUI::AcquisitionRealtimeIngress::incomingData(SequenceValue &&value)
{
    gui.dataAccess.incomingRealtime(value);

    bool doService;
    {
        std::lock_guard<std::mutex> lock(gui.acquisitionRealtimeMutex);
        doService = gui.acquisitionRealtimePending.empty();
        gui.acquisitionRealtimePending.emplace_back(std::move(value));
    }
    if (doService)
        gui.acquisitionRealtimeUpdated();
}

void CPD3GUI::AcquisitionRealtimeIngress::endData()
{ }

void CPD3GUI::acquisitionRealtimeUpdated()
{ emit serviceAcquisitionRealtime(); }

void CPD3GUI::processAcquisitionRealtime()
{
    acquisitionRealtimeMutex.lock();
    SequenceValue::Transfer values(acquisitionRealtimePending);
    acquisitionRealtimePending = SequenceValue::Transfer();
    acquisitionRealtimeMutex.unlock();

    if (values.empty())
        return;

    commandManager.incomingData(values);

    for (const auto &target : acquisitionInterfaces) {
        target.second->incomingRealtime(values);
    }
    for (const auto &target : acquisitionGeneric) {
        target->incomingRealtime(values);
    }
}

void CPD3GUI::acquisitionTrayCommand(AcquisitionTrayCommandHandler::Command command)
{
    switch (command) {
    case AcquisitionTrayCommandHandler::Command::ShowRealtime: {
        bool haveRealtime = false;
        for (const auto &add : configuration) {
            auto check = add.value().hash("Profiles").hash(profile).hash("realtime");
            if (check.getType() == Variant::Type::Hash) {
                haveRealtime = true;
                break;
            }
        }
        if (!haveRealtime)
            return;
        changeDisplayMode("realtime");
        break;
    }
    }
}

void CPD3GUI::applyDataTrim()
{
    double start = getStart();
    double end = getEnd();
    if (FP::defined(start)) {
        start -= 15 * 60;
        dataAccess.trimCached(start);
    }
    if (FP::defined(end))
        end += 15 * 60;

    for (const auto &handler : displayHandlers) {
        handler->applyDataTrim(start, end);
    }
    Memory::release_unused();
}

void CPD3GUI::showMentorEdits()
{
    if (!editDirectiveDialog) {
        editDirectiveDialog.reset(new EditDirectiveDisplay(mainWindow.get()));
        connect(editDirectiveDialog.get(), SIGNAL(accepted()), this, SLOT(applyReplot()));
        editDirectiveDialog->setSelectedTimes(getStart(), getEnd());
    }
    editDirectiveDialog->show();
    editDirectiveDialog->load(station, profile, getStart(), getEnd());
}

void CPD3GUI::enterFullScreen()
{
    if (!mainWindow)
        return;
    mainWindow->displayActiveInFullscreen();
}

void CPD3GUI::showViewData()
{
    if (!viewDataDialog) {
        viewDataDialog.reset(
                new GUIChainDialog(GUIChainDialog::Mode_ViewTabular, mainWindow.get()));
    }
    viewDataDialog->show();
    viewDataDialog->load(station, archive, getStart(), getEnd());
}

void CPD3GUI::showViewEventLog()
{
    if (!eventLogDialog) {
        eventLogDialog.reset(new EventLogDisplay(mainWindow.get()));
    }
    eventLogDialog->show();
    eventLogDialog->load(station, getStart(), getEnd());
}

void CPD3GUI::saveDisplay()
{
    if (!mainWindow)
        return;
    auto handler = mainWindow->getSelectedDisplayHandler();
    if (!handler)
        return;
    handler->promptExportImage();
}

void CPD3GUI::printDisplay()
{
    if (!mainWindow)
        return;
    auto handler = mainWindow->getSelectedDisplayHandler();
    if (!handler)
        return;
    handler->promptPrint();
}

void CPD3GUI::exportData()
{
    if (!exportDataDialog) {
        exportDataDialog.reset(new GUIChainDialog(GUIChainDialog::Mode_Export, mainWindow.get()));
    }
    exportDataDialog->show();
    exportDataDialog->load(station, archive, getStart(), getEnd());
}

void CPD3GUI::showPreferences()
{
    QDialog dialog(mainWindow.get());
    dialog.setWindowTitle(tr("Preferences"));

    QVBoxLayout *layout = new QVBoxLayout;
    dialog.setLayout(layout);
    layout->addWidget(new QLabel("Time display mode:", &dialog));

    QComboBox *timeDisplay = new QComboBox(&dialog);
    layout->addWidget(timeDisplay);
    timeDisplay->setToolTip(tr("The formatting type used when displaying times."));
    timeDisplay->setStatusTip(tr("Time format"));
    timeDisplay->setWhatsThis(
            tr("This sets the general formatting mode used when displaying time.  This controls both the time axes of graphs as well as the general default mode in time input boxes."));
    timeDisplay->addItem(tr("Year and DOY"), QVariant("yeardoy"));
    timeDisplay->addItem(tr("Date and time (ISO8601)"), QVariant("datetime"));

    QString selectedTimeDisplay(settings->value("time/displayformat").toString().toLower());
    if (selectedTimeDisplay == "datetime") {
        timeDisplay->setCurrentIndex(1);
    } else {
        timeDisplay->setCurrentIndex(0);
    }

    layout->addStretch(1);
    QDialogButtonBox *buttons = new QDialogButtonBox(&dialog);
    layout->addWidget(buttons);
    buttons->addButton(QDialogButtonBox::Ok);
    buttons->addButton(QDialogButtonBox::Cancel);
    connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

    dialog.exec();
    if (dialog.result() != QDialog::Accepted)
        return;

    settings->setValue("time/displayformat",
                       timeDisplay->itemData(timeDisplay->currentIndex()).toString());

    applyReplot();
}

void CPD3GUI::showDisplaySettings()
{
    DisplaySetup dialog(station, profile, mode, mainWindow.get());
    dialog.setWindowModality(Qt::ApplicationModal);
    if (dialog.exec() != QDialog::Accepted)
        return;

    if (mainWindow) {
        mainWindow->writeOutState();
        mainWindow->clearHistory();
    }
    reloadConfiguration();
}

void CPD3GUI::showAcquisitionConfiguration()
{
    InitialSetup dialog(InitialSetup::Mode_Acquisition, mainWindow.get());
    dialog.setWindowModality(Qt::ApplicationModal);
    dialog.exec();
}

void CPD3GUI::showSystemConfiguration()
{
    if (!configurationEditorDialog) {
        configurationEditorDialog.reset(new ConfigurationEditorDialog(mainWindow.get()));
    }
    configurationEditorDialog->show();
    configurationEditorDialog->load(station, getStart(), getEnd());
}

void CPD3GUI::issueAcquisitionCommand()
{
    if (!acquisitionConnection)
        return;
    auto commandData = sender()->property("commandData").value<Variant::Read>();
    if (!commandData.exists())
        return;

    RealtimeCommand commandDialog(commandData, acquisitionConnection.get(), mainWindow.get());
    connect(&commandDialog, &RealtimeCommand::issueCommandCompleted,
            [this](const QString &target, const CPD3::Data::Variant::Root &command) {
                if (!acquisitionConnection)
                    return;
                acquisitionConnection->issueCommand(target.toStdString(), command);
            });
    commandDialog.exec();
}

void CPD3GUI::showInterfaceWindow()
{
    if (!mainWindow)
        return;
    QVariant vInterface = sender()->property("interface");
    if (vInterface.isNull())
        return;
    InterfaceHandler *interface = vInterface.value<InterfaceHandler *>();
    Q_ASSERT(interface != NULL);
    mainWindow->showAcquisitionInterface(interface);
}

void CPD3GUI::showDisplayWindow()
{
    if (!mainWindow)
        return;
    QVariant vDisplay = sender()->property("display");
    if (vDisplay.isNull())
        return;
    DisplayHandler *display = vDisplay.value<DisplayHandler *>();
    Q_ASSERT(display);
    mainWindow->showDisplayHandler(display);
}

void CPD3GUI::showGenericWindow()
{
    if (!mainWindow)
        return;
    QVariant vGeneric = sender()->property("genericHandler");
    if (vGeneric.isNull())
        return;
    GenericHandler *generic = vGeneric.value<GenericHandler *>();
    Q_ASSERT(generic != NULL);
    mainWindow->showGenericHandler(generic);
}

void CPD3GUI::setContamination()
{
    if (!acquisitionConnection)
        return;
    if (QMessageBox::question(mainWindow.get(), tr("Acquisition Contamination"),
                              tr("Really set the user contamination flag?  This will cause data to be excluded "
                                 "from longer averaging until the contamination flag is cleared."),
                              QMessageBox::Yes | QMessageBox::No, QMessageBox::No) !=
            QMessageBox::Yes)
        return;
    if (!acquisitionConnection)
        return;
    acquisitionConnection->systemFlagSet("Contaminated");
}

void CPD3GUI::clearContamination()
{
    if (!acquisitionConnection)
        return;
    acquisitionConnection->systemFlagClear("Contaminated");
    QMessageBox::information(mainWindow.get(), tr("Acquisition Contamination"),
                             tr("The user contamination flag has been cleared.  If there are no other "
                                "outstanding contamination flags data will now be included in longer "
                                "averaging."));
}

void CPD3GUI::overrideContamination()
{
    if (!acquisitionConnection)
        return;
    if (QMessageBox::warning(mainWindow.get(), tr("Acquisition Contamination"),
                             tr("Really clear all contamination flags?  This will clear all outstanding "
                                "flags for contamination; even those set by internal components.  This may "
                                "result in undesired data being included in longer averaging."),
                             QMessageBox::Yes | QMessageBox::No, QMessageBox::No) !=
            QMessageBox::Yes)
        return;
    if (!acquisitionConnection)
        return;
    acquisitionConnection->systemFlagClearAll();
}

void CPD3GUI::setBypass()
{
    if (!acquisitionConnection)
        return;
    if (QMessageBox::question(mainWindow.get(), tr("Acquisition Bypass"),
                              tr("Really bypass the acquisition system?  This will cause data to be excluded "
                                 "from all averaging until the bypass is removed.  It will also switch the "
                                 "physical state of the system into bypass mode if possible."),
                              QMessageBox::Yes | QMessageBox::No, QMessageBox::No) !=
            QMessageBox::Yes)
        return;
    if (!acquisitionConnection)
        return;
    acquisitionConnection->bypassFlagSet();
}

void CPD3GUI::clearBypass()
{
    if (!acquisitionConnection)
        return;
    acquisitionConnection->bypassFlagClear();
    QMessageBox::information(mainWindow.get(), tr("Acquisition Bypass"),
                             tr("The user bypass flag has been cleared.  If there are no other outstanding "
                                "bypass flags then the system will transition back to normal operating mode "
                                "and data will resume being averaged."));
}

void CPD3GUI::overrideBypass()
{
    if (!acquisitionConnection)
        return;
    if (QMessageBox::warning(mainWindow.get(), tr("Acquisition Bypass"),
                             tr("Really clear all bypass flags?  This will clear all bypass flags and force "
                                "the system to return to normal operating mode.  This may result in undesired "
                                "operation if it clears bypass flags set by internal components."),
                             QMessageBox::Yes | QMessageBox::No, QMessageBox::No) !=
            QMessageBox::Yes)
        return;
    if (!acquisitionConnection)
        return;
    acquisitionConnection->bypassFlagClearAll();
}

void CPD3GUI::setAveragingTime()
{
    if (!acquisitionConnection)
        return;
    QDialog dlg(mainWindow.get());
    dlg.setModal(true);
    dlg.setWindowTitle(tr("Set Averaging Time"));
    QVBoxLayout *topLayout = new QVBoxLayout;
    dlg.setLayout(topLayout);

    TimeIntervalSelection *selection = new TimeIntervalSelection(&dlg);
    selection->setAllowZero(true);
    selection->setToolTip(tr("The data averaging interval."));
    selection->setStatusTip(tr("Averaging interval"));
    selection->setWhatsThis(
            tr("This is the interval over which realtime data are averaged before being placed into the archive.  When set to zero data are output at the instrument native frequency."));
    selection->setInterval(Time::Minute, 1, true);

    QDialogButtonBox *buttons = new QDialogButtonBox(&dlg);
    topLayout->addWidget(buttons);
    connect(buttons, SIGNAL(accepted()), &dlg, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), &dlg, SLOT(reject()));
    buttons->addButton(QDialogButtonBox::Ok);
    buttons->addButton(QDialogButtonBox::Cancel);

    dlg.exec();
    if (dlg.result() != QDialog::Accepted)
        return;
    if (!acquisitionConnection)
        return;
    acquisitionConnection->setAveragingTime(selection->getUnit(), selection->getCount(),
                                            selection->getAlign());
}

void CPD3GUI::showMessageLogEntry()
{
    if (!acquisitionConnection)
        return;
    if (!messageLogDialog) {
        messageLogDialog.reset(new RealtimeMessageLog(mainWindow.get()));
        connect(messageLogDialog.get(), &RealtimeMessageLog::messageLogEvent,
                [this](const CPD3::Data::Variant::Root &message) {
                    if (!acquisitionConnection)
                        return;
                    acquisitionConnection->messageLogEvent(message);
                });
    }
    messageLogDialog->show();
}

void CPD3GUI::showAcquisitionRestart()
{
    if (!acquisitionConnection)
        return;
    if (QMessageBox::question(mainWindow.get(), tr("Restart Acquisition System"),
                              tr("Really restart the acquisition system?  This will momentarily interrupt data "
                                 "collection but will cause the configuration to be reloaded with the most "
                                 "recent changes."), QMessageBox::Yes | QMessageBox::No,
                              QMessageBox::No) != QMessageBox::Yes)
        return;
    if (!acquisitionConnection)
        return;
    acquisitionConnection->restartRequested();
}

void CPD3GUI::showAcquisitionAutoprobeStatus()
{
    if (!acquisitionConnection)
        return;

    QDialog dlg(mainWindow.get());
    dlg.setWindowTitle(tr("Autodetection Status"));
    dlg.setMinimumSize(QSize(640, 480));
    QVBoxLayout *layout = new QVBoxLayout(&dlg);

    auto activeTable = new QTableWidget(&dlg);
    layout->addWidget(activeTable);

    activeTable->setToolTip(tr("This currently active autodetection interfaces."));
    activeTable->setStatusTip(tr("Autodetection status"));
    activeTable->setWhatsThis(tr("This is the list of currently active autodetection interfaces."));
    activeTable->setColumnCount(4);
    activeTable->setSelectionMode(QAbstractItemView::NoSelection);
    activeTable->verticalHeader()->hide();
    activeTable->setWordWrap(true);
    connect(activeTable->horizontalHeader(), SIGNAL(sectionResized(int, int, int)), activeTable,
            SLOT(resizeRowsToContents()));

    QTableWidgetItem *item = new QTableWidgetItem(tr("Interface"));
    item->setData(Qt::ToolTipRole, tr("The interface being inspected."));
    item->setData(Qt::WhatsThisRole,
                  tr("This is the interface that is currently being autodetected."));
    activeTable->setHorizontalHeaderItem(0, item);

    item = new QTableWidgetItem(tr("In Progress"));
    item->setData(Qt::ToolTipRole, tr("The currently active component attempting detection."));
    item->setData(Qt::WhatsThisRole,
                  tr("This is the identity of the component currently interrogating on the interface."));
    activeTable->setHorizontalHeaderItem(1, item);

    item = new QTableWidgetItem(tr("Attempt"));
    item->setData(Qt::ToolTipRole, tr("The current attempt at autodetection of the component."));
    item->setData(Qt::WhatsThisRole,
                  tr("This is the current attempt count of the same component on this interface."));
    activeTable->setHorizontalHeaderItem(2, item);

    item = new QTableWidgetItem(tr("Remaining"));
    item->setData(Qt::ToolTipRole, tr("The number of other components waiting for an attempt."));
    item->setData(Qt::WhatsThisRole,
                  tr("This is the number of other components also waiting for attempts on the interface."));
    activeTable->setHorizontalHeaderItem(3, item);

    activeTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    activeTable->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    activeTable->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    activeTable->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);

    QLabel *inactiveText = new QLabel(tr("Autodetection complete"), &dlg);
    layout->addWidget(inactiveText);
    inactiveText->setVisible(false);

    std::function<void()> updateTable = [&]() {
        if (!acquisitionConnection) {
            activeTable->setVisible(false);
            inactiveText->setVisible(true);
            return;
        }

        auto state = acquisitionConnection->getAutoprobeState();
        auto interfaces = state["Interfaces"].toArray();
        if (!state["Active"].toBoolean() || interfaces.empty()) {
            activeTable->setVisible(false);
            inactiveText->setVisible(true);
            return;
        }

        /* Clear removes the header too */
        for (int i = activeTable->rowCount() - 1; i >= 0; i--) {
            activeTable->removeRow(i);
        }

        for (auto info : interfaces) {
            int row = activeTable->rowCount();
            activeTable->insertRow(row);

            QString itemData = info["Name"].toDisplayString();
            QTableWidgetItem *item = new QTableWidgetItem(itemData);
            item->setFlags(Qt::ItemIsEnabled);
            activeTable->setItem(row, 0, item);

            itemData = info["Current/Instrument"].toDisplayString();
            if (itemData.isEmpty()) {
                itemData = info["Current/Component"].toQString();
            } else {
                itemData = tr("%1 (%2)", "autoprobe interface").arg(itemData,
                                                                    info["Current/Component"].toQString());
            }
            item = new QTableWidgetItem(itemData);
            item->setFlags(Qt::ItemIsEnabled);
            activeTable->setItem(row, 1, item);

            auto i = info["Current/Try"].toInt64();
            if (!INTEGER::defined(i))
                i = 0;
            auto max = info["Current/MaximumTries"].toInteger();
            if (!INTEGER::defined(i))
                max = i;
            itemData = tr("%1/%2", "interface attempt").arg(i).arg(max);
            item = new QTableWidgetItem(itemData);
            item->setFlags(Qt::ItemIsEnabled);
            activeTable->setItem(row, 2, item);

            itemData = tr("%1", "others remaining").arg(info["Pending"].toArray().size());
            item = new QTableWidgetItem(itemData);
            item->setFlags(Qt::ItemIsEnabled);
            activeTable->setItem(row, 3, item);
        }
    };

    QDialogButtonBox
            *buttonBox = new QDialogButtonBox(QDialogButtonBox::Close, Qt::Horizontal, &dlg);
    connect(buttonBox, SIGNAL(accepted()), &dlg, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), &dlg, SLOT(reject()));
    layout->addWidget(buttonBox);

    updateTable();
    acquisitionConnection->autoprobeStateUpdated.connect(&dlg, updateTable, true);

    dlg.exec();
}

void CPD3GUI::showHelp()
{
    QDesktopServices::openUrl(QUrl("ftp://aftp.cmdl.noaa.gov/aerosol/doc/software/Using_CPX3.pdf",
                                   QUrl::TolerantMode));
}

void CPD3GUI::showWebsite()
{
    QDesktopServices::openUrl(QUrl("http://esrl.noaa.gov/gmd/aero/software/aerosols/cpd3faq.html",
                                   QUrl::TolerantMode));
}

void CPD3GUI::showBugReport()
{
    QDesktopServices::openUrl(
            QUrl("mailto:Derek.Hageman@noaa.gov?subject=CPD3%02Bug%02Report", QUrl::TolerantMode));
}

void CPD3GUI::reloadConfiguration(bool reread)
{
    removeAllDisplays();
    if (reread) {
        loadConfiguration();
    } else {
        initializeConfiguration();
    }
    updateViewMenuVisiblity();
    updateRealtime();

    if (!acquisitionConnection) {
        inRealtimePlotMode = false;

        if (mainWindow) {
            mainWindow->setFixedTime(getStart(), getEnd());
        }
    } else {
        inRealtimePlotMode = true;
        recalculateBounds();

        if (mainWindow) {
            mainWindow->setRealtime(realtimeWindowUnit, realtimeWindowCount, realtimeWindowAlign);
        }
    }
    applyReplot();

    updateActionMenu();
}

void CPD3GUI::changeDisplayMode(const QString &selectedMode)
{
    if (selectedMode.isEmpty() || selectedMode == mode)
        return;

    if (mainWindow) {
        mainWindow->writeOutState();
        mainWindow->clearHistory();
    }

    mode = selectedMode;
    reloadConfiguration();
}

void CPD3GUI::displayModeSelected(QAction *act)
{
    changeDisplayMode(act->property("displayModeSelected").toString());
}

void CPD3GUI::replotInitial()
{
    if (!FP::defined(initialBounds.getStart()) || !FP::defined(initialBounds.getEnd()))
        return;

    displayBounds = initialBounds;

    if (mainWindow) {
        mainWindow->setFixedTime(getStart(), getEnd());
        mainWindow->replotPressed();
    }
}

void CPD3GUI::performAction(QAction *act)
{
    auto actionConfig = act->property("actionConfig").value<Variant::Read>();

    QString componentName(actionConfig["Component/Name"].toQString());
    if (componentName.isEmpty() && actionConfig["ComponentPrompt"].toBool()) {
        auto loader = std::async(std::launch::async, [] {
            std::unordered_map<QString, ComponentLoader::Information> result;
            for (auto &add : ComponentLoader::list("gui")) {
                if (!add.second.interface("action") && !add.second.interface("actiontime"))
                    continue;

                result.emplace(add.first, std::move(add.second));
            }
            return result;
        });

        if (loader.wait_for(std::chrono::seconds(0)) == std::future_status::timeout) {
            QProgressDialog waitDialog(tr("Loading Components"), QString(), 0, 0, mainWindow.get());
            waitDialog.setWindowModality(Qt::ApplicationModal);
            waitDialog.setCancelButton(0);

            Threading::pollFunctor(&waitDialog, [&loader, &waitDialog]() {
                if (loader.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
                    return true;
                waitDialog.close();
                return false;
            });

            if (loader.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
                waitDialog.exec();
        }

        GUIChainComponentSelectionDialog dialog(loader.get(), mainWindow.get());
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Select Operation"));
        if (!dialog.exec())
            return;

        componentName = dialog.getSelectedComponent();
    }
    if (componentName.isEmpty())
        return;

    QObject *component = ComponentLoader::create(componentName);
    if (!component) {
        qCWarning(log_gui_main) << "Can't load component" << componentName;
        return;
    }

    double effectiveStart = displayBounds.start;
    double effectiveEnd = displayBounds.end;
    if (actionConfig["Start"].exists())
        effectiveStart = actionConfig["Start"].toDouble();
    if (actionConfig["End"].exists())
        effectiveEnd = actionConfig["End"].toDouble();
    bool disableTimes = false;

    ActionComponent *actionComponent = nullptr;
    ActionComponentTime *actionComponentTime = nullptr;
    ExternalSinkComponent *egressComponent = nullptr;
    ComponentOptions options;
    std::vector<std::string> stations;
    Archive::Selection::List selections;
    bool promptTime = false;
    bool allowDisableTime = true;
    bool promptData = false;
    if ((actionComponent = qobject_cast<ActionComponent *>(component))) {
        options = actionComponent->getOptions();
        if (actionConfig["Stations"].exists()) {
            stations.clear();
            for (const auto &add : actionConfig["Stations"].toChildren().keys()) {
                stations.emplace_back(add);
            }
        } else if (actionComponent->actionRequireStations() > 1) {
            qCWarning(log_gui_main) << "Component" << componentName << "requires multiple stations";
            return;
        } else if (actionComponent->actionAllowStations() > 0) {
            stations.emplace_back(station);
        }
    } else if ((actionComponentTime = qobject_cast<ActionComponentTime *>(component))) {
        options = actionComponentTime->getOptions();
        if (actionConfig["Stations"].exists()) {
            stations.clear();
            for (const auto &add : actionConfig["Stations"].toChildren().keys()) {
                stations.emplace_back(add);
            }
        } else if (actionComponentTime->actionRequireStations() > 1) {
            qCWarning(log_gui_main) << "Component" << componentName << "requires multiple stations";
            return;
        } else if (actionComponentTime->actionAllowStations() > 0) {
            stations.emplace_back(station);
        }

        allowDisableTime = true;
        if (actionConfig["TimeDisable"].exists())
            allowDisableTime = actionConfig["TimeDisable"].toBool();
        if (actionComponentTime->actionRequiresTime())
            allowDisableTime = false;

        promptTime = true;
        if (actionConfig["TimePrompt"].exists())
            promptTime = actionConfig["TimePrompt"].toBool();
        if (actionConfig["NoTime"].toBool())
            promptTime = false;
        if (actionConfig["NoTime"].toBool())
            allowDisableTime = false;

        if (!actionComponentTime->actionAcceptsUndefinedBounds()) {
            if (!FP::defined(effectiveStart)) {
                if (FP::defined(getStart()))
                    effectiveStart = getStart();
            }
            if (!FP::defined(effectiveEnd)) {
                if (FP::defined(getEnd()))
                    effectiveEnd = getEnd();
            }
            if (!FP::defined(effectiveStart) ||
                    !FP::defined(effectiveEnd) ||
                    effectiveStart > effectiveEnd) {
                effectiveStart = getStart();
                effectiveEnd = getEnd();
            }
            if (!FP::defined(effectiveStart) ||
                    !FP::defined(effectiveEnd) ||
                    effectiveStart > effectiveEnd) {
                return;
            }
        }
    } else if ((egressComponent = qobject_cast<ExternalSinkComponent *>(component))) {
        options = egressComponent->getOptions();

        promptTime = true;
        if (actionConfig["TimePrompt"].exists())
            promptTime = actionConfig["TimePrompt"].toBool();

        promptData = true;
        if (actionConfig["DataPrompt"].exists())
            promptData = actionConfig["DataPrompt"].toBool();
        if (!actionConfig["Input"].exists()) {
            promptData = true;
        } else {
            selections = (SequenceMatch::Composite(actionConfig["Input"])).toArchiveSelections(
                    (!station.empty() ? Archive::Selection::Match{station}
                                      : Archive::Selection::Match{}),
                    (!archive.empty() ? Archive::Selection::Match{archive}
                                      : Archive::Selection::Match{}));
            for (auto &mod : selections) {
                mod.start = effectiveStart;
                mod.end = effectiveEnd;
            }
        }
    }

    bool promptOptions = true;
    if (actionConfig["OptionsPrompt"].exists())
        promptOptions = actionConfig["OptionsPrompt"].toBool();
    if (options.getAll().isEmpty())
        promptOptions = false;

    Variant::Root optionsConfig(actionConfig["Component/Options"]);
    if (!optionsConfig["profile"].exists())
        optionsConfig["profile"].setString(profile);
    if (!optionsConfig["station"].exists())
        optionsConfig["station"].setString(station);
    ValueOptionParse::parse(options, optionsConfig);

    if (promptTime || promptOptions || promptData) {
        QDialog dialog(mainWindow.get());
        dialog.setWindowTitle(tr("Configure Operation"));

        QVBoxLayout *layout = new QVBoxLayout;
        dialog.setLayout(layout);

        TimeBoundSelection *times = 0;
        QCheckBox *timesEnabled = 0;
        if (promptTime) {
            if (allowDisableTime) {
                timesEnabled = new QCheckBox(tr("Operation time range:"), &dialog);
                layout->addWidget(timesEnabled);
                timesEnabled->setChecked(true);
                timesEnabled->setToolTip(tr("Enable the operation time range."));
                timesEnabled->setStatusTip(tr("Enable operation times"));
                timesEnabled->setWhatsThis(
                        tr("This controls if the operation is passed a specific time range."));
            } else {
                layout->addWidget(new QLabel(tr("Operation time range:"), &dialog));
            }

            times = new TimeBoundSelection(&dialog);
            layout->addWidget(times);
            times->setToolTip(tr("The time range to operate on."));
            times->setStatusTip(tr("Set time range"));
            times->setWhatsThis(tr("This is the time range the operation will be performed on."));
            if (actionComponentTime && actionComponentTime->actionAcceptsUndefinedBounds()) {
                times->setAllowUndefinedStart(true);
                times->setAllowUndefinedEnd(true);
            } else {
                times->setAllowUndefinedStart(false);
                times->setAllowUndefinedEnd(false);
            }
            times->setBounds(effectiveStart, effectiveEnd);
            if (timesEnabled) {
                connect(timesEnabled, SIGNAL(toggled(bool)), times, SLOT(setEnabled(bool)));
            }
        }

        VariableSelect *data = 0;
        if (promptData) {
            layout->addWidget(new QLabel("Operation time range:", &dialog));

            data = new VariableSelect(&dialog);
            layout->addWidget(data);
            data->setToolTip(tr("The data to operate on."));
            data->setStatusTip(tr("Set selected data"));
            data->setWhatsThis(tr("This is the data that the operation will be performed on."));

            VariableSelect::ArchiveDefaults def;
            def.setStation(station);
            def.setArchive(archive);
            data->configureFromArchiveSelection(selections);
        }

        if (promptOptions) {
            OptionEditor *editor = new OptionEditor(&dialog);
            layout->addWidget(editor, 1);
            editor->setOptions(options);
            editor->configureAvailable(Archive::Selection(effectiveStart, effectiveEnd,
                                                          (!station.empty()
                                                           ? Archive::Selection::Match{station}
                                                           : Archive::Selection::Match{}),
                                                          (!archive.empty()
                                                           ? Archive::Selection::Match{archive}
                                                           : Archive::Selection::Match{})));
        } else if (!promptData) {
            layout->addStretch(1);
        }

        QDialogButtonBox *buttons = new QDialogButtonBox(&dialog);
        layout->addWidget(buttons);
        buttons->addButton(QDialogButtonBox::Ok);
        buttons->addButton(QDialogButtonBox::Cancel);
        connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
        connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

        dialog.exec();
        if (dialog.result() != QDialog::Accepted)
            return;

        if (times) {
            effectiveStart = times->getStart();
            effectiveEnd = times->getEnd();
        }
        if (timesEnabled) {
            disableTimes = !timesEnabled->isChecked();
        }
        if (data) {
            VariableSelect::ArchiveDefaults def;
            def.setStation(station);
            def.setArchive(archive);
            selections = data->getArchiveSelection(def, effectiveStart, effectiveEnd);
        }
    }

    QString continuePrompt;
    if (actionComponent) {
        if (actionComponent->actionRequireStations() > static_cast<int>(stations.size())) {
            qCWarning(log_gui_main) << "Component" << componentName << "requires"
                                    << actionComponent->actionRequireStations()
                                    << "station(s) but only" << stations.size() << "are defined";
            return;
        }
        if (actionComponent->actionAllowStations() < static_cast<int>(stations.size())) {
            qCWarning(log_gui_main) << "Component" << componentName << "only allows"
                                    << actionComponent->actionAllowStations() << "station(s) but"
                                    << stations.size() << "are defined";
            return;
        }
        continuePrompt = actionComponent->promptActionContinue(options, stations);
    } else if (actionComponentTime) {
        if (actionComponentTime->actionRequireStations() > static_cast<int>(stations.size())) {
            qCWarning(log_gui_main) << "Component" << componentName << "requires"
                                    << actionComponentTime->actionRequireStations()
                                    << "station(s) but only" << stations.size() << "are defined";
            return;
        }
        if (actionComponentTime->actionAllowStations() < static_cast<int>(stations.size())) {
            qCWarning(log_gui_main) << "Component " << componentName << "only allows"
                                    << actionComponentTime->actionAllowStations()
                                    << "station(s) but" << stations.size() << "are defined";
            return;
        }
        if (!actionConfig["NoTime"].toBool() && !disableTimes) {
            if (!actionComponentTime->actionAcceptsUndefinedBounds()) {
                if (!FP::defined(effectiveStart) ||
                        !FP::defined(effectiveEnd) ||
                        effectiveStart > effectiveEnd) {
                    qCWarning(log_gui_main) << "Component" << componentName
                                            << "does not accept undefined time ranges";
                    return;
                }
            } else {
                if (FP::defined(effectiveStart) &&
                        FP::defined(effectiveEnd) &&
                        effectiveStart > effectiveEnd) {
                    effectiveStart = FP::undefined();
                    effectiveEnd = FP::undefined();
                }
            }

            continuePrompt = actionComponentTime->promptTimeActionContinue(options, effectiveStart,
                                                                           effectiveEnd, stations);
        } else {
            continuePrompt = actionComponentTime->promptTimeActionContinue(options, stations);
        }
    }
    if (!continuePrompt.isEmpty() && !actionConfig["IgnoreContinue"].toBool()) {
        int ret = QMessageBox::question(mainWindow.get(), tr("Confirm Operation"), continuePrompt,
                                        QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
        if (ret != QMessageBox::Yes)
            return;
    }

    CPD3Action *action = NULL;
    std::unique_ptr<ExternalSink> output;
    QBuffer buffer;
    if (actionComponent) {
        action = actionComponent->createAction(options, stations);
    } else if (actionComponentTime) {
        if (!actionConfig["NoTime"].toBool() && !disableTimes) {
            action = actionComponentTime->createTimeAction(options, effectiveStart, effectiveEnd,
                                                           stations);
        } else {
            action = actionComponentTime->createTimeAction(options, stations);
        }
    } else if (egressComponent) {
        buffer.open(QIODevice::WriteOnly);
        output.reset(egressComponent->createDataEgress(&buffer, options));
    }

    if (action != NULL) {
        ActionProgressDialog dialog(mainWindow.get(), 0, settings.get());
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Operation in Progress"));
        dialog.setTimeBounds(effectiveStart, effectiveEnd);
        dialog.attach(action->feedback);

        connect(&dialog, SIGNAL(canceled()), action, SLOT(signalTerminate()), Qt::DirectConnection);
        connect(action, SIGNAL(finished()), &dialog, SLOT(accept()), Qt::QueuedConnection);

        action->start();
        dialog.exec();
        if (dialog.wasCanceled() || dialog.result() == QDialog::Rejected)
            action->signalTerminate();
        action->wait();

        delete action;
    } else if (output) {
        ActionProgressDialog dialog(mainWindow.get(), 0, settings.get());
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Operation in Progress"));
        dialog.setTimeBounds(effectiveStart, effectiveEnd);

        StreamPipeline chain(false, false, false);
        dialog.attach(chain.feedback);
        chain.setInputArchive(selections);
        chain.setOutputGeneral(std::move(output));

        connect(&dialog, &ActionProgressDialog::canceled,
                std::bind(&StreamPipeline::signalTerminate, &chain));
        chain.finished.connect(&dialog, std::bind(&ActionProgressDialog::accept, &dialog), true);

        chain.start();
        dialog.exec();
        if (dialog.wasCanceled() || dialog.result() == QDialog::Rejected)
            chain.signalTerminate();
        chain.waitInEventLoop();

        GUIChainBufferDialog *display = new GUIChainBufferDialog(buffer.data(), mainWindow.get());
        display->setAttribute(Qt::WA_DeleteOnClose);
        display->show();
    } else {
        return;
    }

    if (actionConfig["TriggerReload"].toBool()) {
        if (mainWindow) {
            mainWindow->writeOutState();
            mainWindow->clearHistory();
        }
        reloadConfiguration(true);
    } else if (actionConfig["TriggerReplot"].toBool()) {
        applyReplot();
    }
}

namespace {
class LogoSvg : public QSvgWidget {
public:
    LogoSvg(const QString &file, QWidget *parent) : QSvgWidget(file, parent)
    {
        QSizePolicy policy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        policy.setHeightForWidth(true);
        policy.setHorizontalStretch(1);
        policy.setVerticalStretch(1);
        setSizePolicy(policy);
    }

    virtual ~LogoSvg()
    { }

    virtual QSize sizeHint() const
    { return QSize(64, 64); }

    virtual int heightForWidth(int w) const
    { return w; }
};
}

void CPD3GUI::showAbout()
{
    QDialog dlg(mainWindow.get());
    dlg.setWindowTitle(tr("About CPD3"));
    QVBoxLayout *layout = new QVBoxLayout(&dlg);

    QSvgWidget *logo = new LogoSvg(":/icon.svg", &dlg);
    layout->addWidget(logo);

    QLabel *label = new QLabel(tr("CPD3 %1<br>%2<br>\xC2\xA9 2020 University of Colorado").arg(
            Environment::revision(), Environment::architecture()), &dlg);
    label->setAlignment(Qt::AlignHCenter);
    layout->addWidget(label, Qt::AlignHCenter | Qt::AlignTop);

    QDialogButtonBox
            *buttonBox = new QDialogButtonBox(QDialogButtonBox::Close, Qt::Horizontal, &dlg);
    connect(buttonBox, SIGNAL(accepted()), &dlg, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), &dlg, SLOT(reject()));
    layout->addWidget(buttonBox);

    dlg.exec();
}


namespace {
struct DebugData {
    std::mutex mutex;
    QString contents;
};
}
Q_GLOBAL_STATIC(DebugData, debugData)

void guiMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{

#ifndef NO_CONSOLE
    switch (type) {
    case QtDebugMsg:
        if (context.file && context.line) {
            fprintf(stderr, "%s %s %s:%d D(%d): %s\n",
                    QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ")
                                                   .toLatin1()
                                                   .constData(), context.category, context.file,
                    context.line, static_cast<int>(QCoreApplication::applicationPid()),
                    msg.toUtf8().constData());
        } else {
            fprintf(stderr, "%s %s D(%d): %s\n",
                    QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ")
                                                   .toLatin1()
                                                   .constData(), context.category,
                    static_cast<int>(QCoreApplication::applicationPid()), msg.toUtf8().constData());
        }
        break;
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
    case QtInfoMsg:
        if (context.file && context.line) {
            fprintf(stderr, "%s %s %s:%d I(%d): %s\n",
                    QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ")
                                                   .toLatin1()
                                                   .constData(), context.category, context.file,
                    context.line, static_cast<int>(QCoreApplication::applicationPid()),
                    msg.toUtf8().constData());
        } else {
            fprintf(stderr, "%s %s I(%d): %s\n",
                    QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ")
                                                   .toLatin1()
                                                   .constData(), context.category,
                    static_cast<int>(QCoreApplication::applicationPid()), msg.toUtf8().constData());
        }
        break;
#endif
    case QtWarningMsg:
        if (context.file && context.line) {
            fprintf(stderr, "%s %s %s:%d W(%d): %s\n",
                    QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ")
                                                   .toLatin1()
                                                   .constData(), context.category, context.file,
                    context.line, static_cast<int>(QCoreApplication::applicationPid()),
                    msg.toUtf8().constData());
        } else {
            fprintf(stderr, "%s %s W(%d): %s\n",
                    QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ")
                                                   .toLatin1()
                                                   .constData(), context.category,
                    static_cast<int>(QCoreApplication::applicationPid()), msg.toUtf8().constData());
        }
        break;
    case QtCriticalMsg:
        if (context.file && context.line) {
            fprintf(stderr, "%s %s %s:%d C(%d): %s\n",
                    QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ")
                                                   .toLatin1()
                                                   .constData(), context.category, context.file,
                    context.line, static_cast<int>(QCoreApplication::applicationPid()),
                    msg.toUtf8().constData());
        } else {
            fprintf(stderr, "%s %s C(%d): %s\n",
                    QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ")
                                                   .toLatin1()
                                                   .constData(), context.category,
                    static_cast<int>(QCoreApplication::applicationPid()), msg.toUtf8().constData());
        }
        break;
    case QtFatalMsg:
        if (context.file && context.line) {
            fprintf(stderr, "%s %s %s:%d F(%d): %s\n",
                    QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ")
                                                   .toLatin1()
                                                   .constData(), context.category, context.file,
                    context.line, static_cast<int>(QCoreApplication::applicationPid()),
                    msg.toUtf8().constData());
        } else {
            fprintf(stderr, "%s %s F(%d): %s\n",
                    QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ")
                                                   .toLatin1()
                                                   .constData(), context.category,
                    static_cast<int>(QCoreApplication::applicationPid()), msg.toUtf8().constData());
        }
        abort();
        break;
    }
#else
    switch (type) {
    case QtDebugMsg:
        break;
    case QtWarningMsg:
        break;
    case QtCriticalMsg:
        QMessageBox::critical(NULL, "Critical", msg);
        break;
    case QtFatalMsg:
        QMessageBox::critical(NULL, "Fatal", msg);
        abort();
        break;
    }
#endif

    auto d = debugData();
    if (!d)
        return;

    std::lock_guard<std::mutex> lock(d->mutex);

    QDateTime currentTime = QDateTime::currentDateTimeUtc();
    d->contents.append(currentTime.toString("yyyy-MM-ddThh:mm:ss.zzzZ"));
    d->contents.append(' ');
    d->contents.append(context.category);
    d->contents.append(' ');
    if (context.function) {
        d->contents.append(context.function);
        d->contents.append(' ');
    }
    if (context.file && context.line) {
        d->contents.append(context.file);
        d->contents.append(':');
        d->contents.append(QString::number(context.line));
        d->contents.append(' ');
    }

    switch (type) {
    case QtDebugMsg:
        d->contents.append('D');
        break;
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
    case QtInfoMsg:
        d->contents.append('I');
        break;
#endif
    case QtWarningMsg:
        d->contents.append('W');
        break;
    case QtCriticalMsg:
        d->contents.append('C');
        break;
    case QtFatalMsg:
        d->contents.append('F');
        break;
    }

    d->contents.append(": ");
    d->contents.append(msg);
    d->contents.append('\n');

    int sz = d->contents.size();
    if (sz > 16777216)
        d->contents.remove(0, sz - 16777216);
}

static void displayDebugFilter(QLoggingCategory *category)
{
    if (qstrncmp(category->categoryName(), "cpd3.", 4) == 0) {
        category->setEnabled(QtDebugMsg, true);
        category->setEnabled(QtWarningMsg, true);
        category->setEnabled(QtCriticalMsg, true);
        category->setEnabled(QtFatalMsg, true);
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
        category->setEnabled(QtInfoMsg, true);
#endif
    }
}

void CPD3GUI::showDebug()
{
    QString contents;
    {
        auto d = debugData();
        if (!d)
            return;
        std::lock_guard<std::mutex> lock(d->mutex);
        contents = d->contents;
    }

    QDialog dlg(mainWindow.get());
    dlg.setWindowTitle(tr("Debug Messages"));
    dlg.setMinimumSize(QSize(800, 600));
    QVBoxLayout *layout = new QVBoxLayout(&dlg);
    QPlainTextEdit *text = new QPlainTextEdit(contents, &dlg);
    text->setReadOnly(true);
    text->setLineWrapMode(QPlainTextEdit::NoWrap);
    text->setFont(QFont("Monospace"));
    layout->addWidget(text);

    QDialogButtonBox
            *buttonBox = new QDialogButtonBox(QDialogButtonBox::Close, Qt::Horizontal, &dlg);
    connect(buttonBox, SIGNAL(accepted()), &dlg, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), &dlg, SLOT(reject()));
    layout->addWidget(buttonBox);

    static bool installedFilter = false;
    if (!installedFilter) {
        auto enable = buttonBox->addButton(tr("Set Filter"), QDialogButtonBox::ActionRole);
        connect(enable, &QAbstractButton::clicked, &dlg, [&dlg, enable] {
            enable->hide();
            installedFilter = true;
            QLoggingCategory::installFilter(displayDebugFilter);
        });
    }

    text->verticalScrollBar()->setValue(text->verticalScrollBar()->maximum());
    dlg.exec();
}

#if defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 1

#include <tbb/scalable_allocator.h>

static void allocatorRelease()
{
    ::free(nullptr);
#ifdef HAVE_TBB_COMMAND
    scalable_allocation_command(TBBMALLOC_CLEAN_ALL_BUFFERS, nullptr);
#endif
}

static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }

#elif defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 2
#include <gperftools/malloc_extension.h>
static void allocatorRelease()
{ MallocExtension::instance()->ReleaseFreeMemory(); }
static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }
#else
static void installAllocator()
{ }
#endif

int main(int argc, char **argv)
{
    installAllocator();
    if (::qgetenv("CPD3_LOG").isEmpty()) {
        qInstallMessageHandler(guiMessageOutput);
    }

    QApplication app(argc, argv);
    Q_INIT_RESOURCE(gui);

    qRegisterMetaType<QMenu *>("QMenu *");
    qRegisterMetaType<InterfaceHandler *>("InterfaceHandler *");
    qRegisterMetaType<GenericHandler *>("GenericHandler *");
    qRegisterMetaType<CPD3::Data::Variant::Read>("CPD3::Data::Variant::Read");
    qRegisterMetaType<CPD3::Data::Variant::Write>("CPD3::Data::Variant::Write");
    qRegisterMetaType<CPD3::Data::Variant::Root>("CPD3::Data::Variant::Root");

    app.setQuitOnLastWindowClosed(true);
    app.setWindowIcon(QIcon(":/icon.svg"));

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator guiTranslator;
    guiTranslator.load("gui_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&guiTranslator);
    QTranslator acquisitionTranslator;
    acquisitionTranslator.load("acquisition_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&acquisitionTranslator);

    CPD3GUI *gui = new CPD3GUI;
    if (int rc = gui->parseArguments(app.arguments())) {
        delete gui;
        if (rc == 1)
            rc = 0;
        else
            rc = 1;
        return rc;
    }

    QObject::connect(gui, SIGNAL(quitRequested()), &app, SLOT(quit()));

    /* Disabled since it probably makes more sense to just do an ungraceful
     * abort when the GUI is forcibly closed */
#if 0
    Abort::installAbortHandler(true);
    AbortPoller abortPoller(&app);
    QObject::connect(&abortPoller, SIGNAL(aborted()), &app, SLOT(quit()));    
    abortPoller.start();
#endif

    gui->startup();
    int rc = app.exec();
    delete gui;

    ThreadPool::system()->wait();

    return rc;
}
