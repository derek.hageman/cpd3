/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QStringList>

#include "core/number.hxx"
#include "core/util.hxx"

#include "interfacehandler.hxx"
#include "gui.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

InterfaceHandler::InterfaceHandler(const QString &n, CPD3GUI *p) : HandlerCommon(p),
                                                                   parent(p),
                                                                   name(n),
                                                                   information(),
                                                                   systemFlags(),
                                                                   bypassFlags(),
                                                                   status(AcquisitionInterface::GeneralStatus::Normal)
{ }

void InterfaceHandler::updateInformation(const Variant::Read &i)
{
    information = i;
    information.detachFromRoot();
    emit displayUpdated();
}

void InterfaceHandler::updateState(const CPD3::Data::Variant::Read &state)
{
    systemFlags = state.hash("SystemFlags").toFlags();
    bypassFlags = state.hash("BypassFlags").toFlags();

    const auto &statusString = state.hash("Status").toString();
    AcquisitionInterface::GeneralStatus previousStatus(status);
    if (Util::equal_insensitive(statusString, "NoCommunications")) {
        status = AcquisitionInterface::GeneralStatus::NoCommunications;
    } else if (Util::equal_insensitive(statusString, "Disabled")) {
        status = AcquisitionInterface::GeneralStatus::Disabled;
    } else {
        status = AcquisitionInterface::GeneralStatus::Normal;
    }
    if (previousStatus != status)
        triggerUpdate();
}

QString InterfaceHandler::criticalText() const
{
    switch (status) {
    case AcquisitionInterface::GeneralStatus::NoCommunications:
        return tr("<font color='red' size='4'><b>No Communications</b></font>");
    case AcquisitionInterface::GeneralStatus::Disabled:
    case AcquisitionInterface::GeneralStatus::Normal:
        return QString();
    }
    Q_ASSERT(false);
    return QString();
}

bool InterfaceHandler::hideInAcquisitionMenu() const
{ return information.hash("MenuHide").toBool(); }

qint64 InterfaceHandler::getDisplaySortPriority() const
{ return INTEGER::undefined(); }

static const SequenceName::Component archiveInstantMeta = "rt_instant_meta";
static const SequenceName::Component archiveBoxcarMeta = "rt_boxcar_meta";

void InterfaceHandler::incomingRealtime(const SequenceValue::Transfer &data)
{
    for (const auto &value : data) {
        if (value.getUnit().isMeta()) {
            if (value.getValue().metadata("Source").hash("Name").toQString() != name) {
                /* Explicitly discontinue these in case the variable source
                 * changes */
                layoutInstant.incomingData(
                        SequenceValue(value.getUnit(), Variant::Root(), value.getStart(),
                                      value.getEnd()));
                layoutAverage.incomingData(
                        SequenceValue(value.getUnit(), Variant::Root(), value.getStart(),
                                      value.getEnd()));
                layoutBoxcar.incomingData(
                        SequenceValue(value.getUnit(), Variant::Root(), value.getStart(),
                                      value.getEnd()));
                continue;
            }
            const auto &archive = value.getArchive();
            if (archive == archiveInstantMeta) {
                layoutInstant.incomingData(value);
                continue;
            } else if (archive == archiveBoxcarMeta) {
                layoutBoxcar.incomingData(value);
                continue;
            } else {
                layoutAverage.incomingData(value);
                continue;
            }
            continue;
        }

        layoutInstant.incomingData(value);
        layoutAverage.incomingData(value);
        layoutBoxcar.incomingData(value);
    }

    triggerUpdate();
}
