/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3_TRANSFERPACKAGE_HXX
#define CPD3_TRANSFERPACKAGE_HXX

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <deque>
#include <map>

#include "transfer.hxx"
#include "core/actioncomponent.hxx"
#include "core/threading.hxx"
#include "io/access.hxx"
#include "io/process.hxx"
#include "datacore/variant/root.hxx"

class QSslCertificate;

namespace CPD3 {
namespace Transfer {

/**
 * The handler for creating transfer packages.  Stages are added in the order of a
 * pipeline of processing.  So the last added stage is applied right before final
 * output.
 */
class CPD3TRANSFER_EXPORT TransferPack {
    class Stage;

    std::vector<std::unique_ptr<Stage>> stages;

    Threading::Signal<> terminateRequested;
    std::mutex mutex;
    bool terminated;

public:
    TransferPack();

    virtual ~TransferPack();

    /**
     * Add a stage that invokes an external program for processing.
     *
     * @param configuration the configuration of the external program
     */
    void invokeStage(const Data::Variant::Read &configuration);

    /**
     * Add a stage that encrypts the data
     *
     * @param from      the encryption key or certificate
     * @param usePrivate    if set then encrypt using the origin as a private key
     * @param loadExternal  set to allow loading of externally stored keys
     * @param password      the password for the key
     */
    void encryptStage(const Data::Variant::Read &from,
                      bool usePrivate = false,
                      bool loadExternal = true,
                      const std::string &password = {});

    /**
     * Add a stage that add signature to the data.
     *
     * @param from      the signing key
     * @param includeCertificate include the signing certificate if possible
     * @param loadExternal  set to allow loading of externally stored keys
     * @param password      the password for the key
     */
    void signStage(const Data::Variant::Read &from,
                   bool includeCertificate = true,
                   bool loadExternal = true,
                   const std::string &password = {});

    /**
     * Add a stage that adds multiple signatures to the data.
     *
     * @param from      the signing keys
     * @param includeCertificate include the signing certificate if possible
     * @param loadExternal  set to allow loading of externally stored keys
     * @param password      the password for the key
     */
    void signStage(const std::vector<Data::Variant::Read> &from,
                   bool includeCertificate = true,
                   bool loadExternal = true,
                   const std::string &password = {});

    /**
     * Add a stage that compresses the data.
     */
    void compressStage();

    /**
     * Add stage that adds a checksum (usually a hashsum) to the data.
     */
    void checksumStage();

    CPD3::ActionFeedback::Source feedback;

    /**
     * Perform the packaging.
     *
     * @param input     the input source
     * @param output    the output target
     * @return          true on success
     */
    bool exec(const IO::Access::Handle &input, const IO::Access::Handle &output);

    /**
     * Request termination of the packaging.
     */
    void signalTerminate();

protected:
    /**
     * Apply substitutions to strings being used.
     * <br>
     * By default this just returns the input.
     *
     * @param input the input
     * @return      the input with substitutions applied
     */
    virtual QString applySubstitutions(const QString &input) const;
};


/**
 * The handler for unpacking transfer packages.
 */
class CPD3TRANSFER_EXPORT TransferUnpack {
    Threading::Signal<> terminateRequested;
    std::mutex mutex;
    bool terminated;

    struct Requirement {
        enum class Type : int {
            End, Checksum, Compression, Encryption, Signature
        } type;
        bool optional;
        int flags;

        Requirement(Type type, bool optional, int flags = 0);
    };

    std::deque<Requirement> requirements;

    std::multimap<int, IO::Process::Spawn> invoke;

    QString error;

    class Stage;

public:
    TransferUnpack();

    virtual ~TransferUnpack();

    /**
     * Invoke the given program at the given depth in the processing chain.  Zero or
     * positive depths invoke before that index stage (so depth zero invokes
     * before any processing).
     *
     * @param configuration the configuration to process
     * @param depth         the depth to invoke at
     */
    void invokeAt(const Data::Variant::Read &configuration, int depth = 0);

    /**
     * Set the end of requirements.  That is, require processing to be completed at this
     * stage.
     *
     * @param optional  allow processing to continue even if this requirement doesn't match
     */
    void requireEnd(bool optional = false);

    /**
     * Require a valid checksum on the remainder of the container.
     *
     * @param optional  allow processing to continue even if this requirement doesn't match
     */
    void requireChecksum(bool optional = false);

    /**
     * Require the remainder of the container to be compressed.
     *
     * @param optional  allow processing to continue even if this requirement doesn't match
     */
    void requireCompression(bool optional = false);

    enum {
        /** No additional flags; accept any form of encryption. */
        Encryption_Any = 0x00,

        /** Require the encryption to have been done with a certificate. */
        Encryption_Certificate = 0x01,

        /** Require the encryption to have been done with a private key. */
        Encryption_Key = 0x02,

        /** Require the certificate ID to be included. That is, disallow an implicit
         * certificate identification.  */
        Encryption_RequireIdentification = 0x04,

        /** The default flags if none are given */
        Encryption_Default = Encryption_Certificate,
    };

    /**
     * Require the remainder of the container to be encrypted.
     *
     * @param flags     the encrypt requirement flags
     * @param optional  allow processing to continue even if this requirement doesn't match
     */
    void requireEncryption(int flags = Encryption_Default, bool optional = false);

    enum {
        /** No additional flags; accept any form of signature. */
        Signature_Any = 0x00,

        /** Require the signature to include the certificate it corresponds to. */
        Signature_IncludeCertificate = 0x01,

        /** Require the signature to include the ID if the certificate used to verify it.  That
         * is, disallow an implied certificate identification. */
        Signature_RequireIdentification = 0x02,

        /** Only accept a single signing certificate. */
        Signature_SingleOnly = 0x04,

        /** The default flags if none are given */
        Signature_Default = Signature_Any,
    };

    /**
     * Require the remainder of the container to be signed.
     *
     * @param flags     the requirement flags
     * @param optional  allow processing to continue even if this requirement doesn't match
     */
    void requireSignature(int flags = Signature_Default, bool optional = false);


    CPD3::ActionFeedback::Source feedback;

    /**
     * Perform the packaging.
     *
     * @param input     the input source
     * @param output    the output target
     * @return          true on success
     */
    bool exec(const IO::Access::Handle &input, const IO::Access::Handle &output);

    /**
     * Request termination of the packaging.
     */
    void signalTerminate();

    /**
     * Get a description of the failure.
     *
     * @return  an error description
     */
    QString errorString();

protected:

    /**
     * Apply substitutions to strings being used.
     * <br>
     * By default this just returns the input.
     *
     * @param input the input
     * @return      the input with substitutions applied
     */
    virtual QString applySubstitutions(const QString &input) const;

    /**
     * Get the certificate associated with the given identifier (SHA512 hash).
     * <br>
     * The default implementation always returns undefined.
     *
     * @param id            the certificate identifier
     * @param decryption    true if the certificate is used for decryption (instead of signature checking)
     * @param depth         the depth of unpacking, starting from zero
     * @return              the certificate data or undefined if it wasn't found
     */
    virtual Data::Variant::Read getCertificate(const QByteArray &id, bool decryption, int depth);

    /**
     * Get the key associated with the given certificate identifier (SHA512 hash).  Keys are
     * only used for decryption.
     * <br>
     * The default implementation always returns undefined.
     *
     * @param id    the certificate identifier
     * @param depth         the depth of unpacking, starting from zero
     * @return      the key data or undefined if it wasn't found
     */
    virtual Data::Variant::Read getKeyForCertificate(const QByteArray &id, int depth);

    /**
     * Check if the given signing certificates are authorized to continue unpacking.  This can
     * also set any internal authorization parameters reflecting that certificate's
     * permissions.
     * <br>
     * The default implementation always returns true.
     *
     * @param ids   the certificate identifiers
     * @param depth         the depth of unpacking, starting from zero
     * @return      false to halt processing
     */
    virtual bool signatureAuthorized(const QList<QByteArray> &ids, int depth);

    /**
     * Check if the given signing certificate is considered valid.  This is normally used
     * to check the certificate against an authority before even checking the signature.
     * <br>
     * The default implementation always returns true.
     *
     * @parma certificate   the certificate
     * @param depth         the depth of unpacking, starting from zero
     */
    virtual bool signingCertificateValid(const QSslCertificate &certificate, int depth);

    /**
     * Check if unpacking should continue at the current depth.  This is called after
     * the current stage has completed successfully but before advancing to the next one.  This
     * can be used to abort processing if the steps don't match (e.x. a signature isn't
     * present).
     * <br>
     * The default implementation always returns true.
     *
     * @param depth     the depth of unpacking, starting from zero
     * @return          false to halt processing
     */
    virtual bool continueUnpacking(int depth);
};

}
}

#endif //CPD3_PACKAGE_HXX
