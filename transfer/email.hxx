/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3TRANSFEREMAIL_H
#define CPD3TRANSFEREMAIL_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QByteArray>
#include <QStringList>
#include <QHash>
#include <QList>

#include "transfer/transfer.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Transfer {

class SMTPSend;

/**
 * This interface allows the construction of email text suitable for passing
 * off to SMTP.
 */
class CPD3TRANSFER_EXPORT EmailBuilder {
public:
    /**
     * The escape mode used for characters outside 7-bit ASCII.
     */
    enum EscapeMode {
        /** Use standard UTF8 characters. */
                Escape_UTF8, /** Output HTML escapes. */
                Escape_HTML,
    };
private:
    QHash<QString, QString> headers;
    EscapeMode escapeMode;
    QString messageBody;
    QString bodyMimeType;

    QStringList to;
    QStringList cc;
    QStringList bcc;
    QStringList from;

    struct Attachment {
        QByteArray data;
        QString fileName;
        QString contentID;
        QString mimeType;
    };
    QList<Attachment> attachments;

    bool anyContentsContainBoundary(const QByteArray &boundary) const;

public:
    EmailBuilder();

    /**
     * Set the non-standard character escape mode.  The default is UTF8
     * characters.
     * 
     * @param mode      the escape mode
     */
    void setEscapeMode(EscapeMode mode);

    /**
     * Set a header in the message.
     * 
     * @param key       the header key
     * @param value     the header value
     */
    void setHeader(const QString &key, const QString &value);

    /**
     * Set the time of the message to the given (UTC) time.
     * 
     * @param time      the time of the message or undefined for the current time
     */
    void setTime(double time = FP::undefined());

    /**
     * Set the message subject line.
     * 
     * @param subject   the subject
     */
    void setSubject(const QString &subject);

    /**
     * Add a "To" recipient.
     * 
     * @param address   the address specification
     */
    void addTo(const QString &address);

    /**
     * Add a "CC:" recipient.
     * 
     * @param address   the address specification
     */
    void addCC(const QString &address);

    /**
     * Add a "BCC:" recipient.  This does not add anything to the headers,
     * but can be used to control the interface with a SMTP sender.
     * 
     * @param address   the address specification
     */
    void addBCC(const QString &address);

    /**
     * Add a "From" address.
     * 
     * @param address   the address specification
     */
    void addFrom(const QString &address);

    /**
     * Set the message body.
     * 
     * @param body  the body
     */
    void setBody(const QString &body, const QString &mimeType = "text/plain");

    /**
     * Add an attachment to the message.
     * 
     * @param data      the attachment data
     * @param fileName  the file name of the attachment
     * @param mimeType  the MIME type of the file
     * @param contentID the contentID of the attachment
     */
    void addAttachment(const QByteArray &data,
                       const QString &fileName,
                       const QString &mimeType = "application/octet-stream",
                       const QString &contentID = QString());

    /**
     * Construct the output message.
     * 
     * @return  the message
     */
    QByteArray construct() const;

    /**
     * Queue the message for sending on the given sender.
     */
    void queue(SMTPSend *target) const;
};

}
}

#endif
