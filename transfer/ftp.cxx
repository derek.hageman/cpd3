/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cctype>
#include <QFile>
#include <QSslSocket>
#include <QThread>
#include <QEventLoop>
#include <QTimer>
#include <QLoggingCategory>
#include <QRegularExpression>

#include "transfer/ftp.hxx"
#include "algorithms/cryptography.hxx"
#include "core/util.hxx"
#include "io/drivers/qio.hxx"


Q_LOGGING_CATEGORY(log_transfer_ftp, "cpd3.transfer.ftp", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Transfer {

FTPConnection::File::File() : remoteSize(0), remoteModified(FP::undefined())
{ }

FTPConnection::File::~File() = default;

FTPConnection::File::File(std::string remoteName,
                          std::string remoteDirectory,
                          std::uint_fast64_t remoteSize,
                          double remoteModified) : remoteName(std::move(remoteName)),
                                                   remoteDirectory(std::move(remoteDirectory)),
                                                   remoteSize(remoteSize),
                                                   remoteModified(remoteModified)
{ }

FTPConnection::File::File(const File &other) = default;

FTPConnection::File &FTPConnection::File::operator=(const File &other) = default;

FTPConnection::File::File(File &&other) = default;

FTPConnection::File &FTPConnection::File::operator=(File &&other) = default;

std::string FTPConnection::File::filePath() const
{
    auto result = remoteDirectory;
    if (!result.empty())
        result.push_back('/');
    result += remoteName;
    return result;
}


FTPConnection::FTPConnection(std::string host,
                             std::uint16_t port,
                             std::string user,
                             std::string password) : host(std::move(host)),
                                                     user(std::move(user)),
                                                     password(std::move(password)),
                                                     socketConfig(port),
                                                     transferMode(TransferMode::Passive),
                                                     commandTimeout(30.0),
                                                     transferTimeout(30.0),
                                                     transferTotalTimeout(FP::undefined()),
                                                     keepaliveInterval(60.0),
                                                     resumeRetries(3),
                                                     relaxedIncoming(false),
                                                     resumeDownload(true),
                                                     resumeUpload(false),
                                                     closeOnDataFailure(true),
                                                     allowKeepaliveQueueing(true),
                                                     uploadRateLimit(0),
                                                     terminated(false),
                                                     commandConnectionEnded(false)
{ }

FTPConnection::~FTPConnection() = default;

void FTPConnection::setSSL(const Data::Variant::Read &ssl)
{
    if (!ssl.exists()) {
        socketConfig.tls = {};
        return;
    }
    if (commandConnection) {
        qCWarning(log_transfer_ftp)
            << "Changing FTP SSL configuration when the connection is open has undefined results";
    }
    Data::Variant::Root sslConfig(ssl);
    socketConfig.tls = [sslConfig](QSslSocket &socket) {
        if (!Algorithms::Cryptography::setupSocket(&socket, sslConfig)) {
            qCDebug(log_transfer_ftp) << "Failed to setup TLS on socket";
            return false;
        }
        return true;
    };
}

void FTPConnection::setPassive()
{ transferMode = TransferMode::Passive; }

void FTPConnection::setActive()
{ transferMode = TransferMode::Active; }

void FTPConnection::setPassiveThenActive()
{ transferMode = TransferMode::PassiveThenActive; }

void FTPConnection::setRelaxedIncoming(bool enable)
{ relaxedIncoming = enable; }

void FTPConnection::setResumeDownload(bool enable)
{ resumeDownload = enable; }

void FTPConnection::setResumeUpload(bool enable)
{ resumeUpload = enable; }

void FTPConnection::setCloseOnDataFailure(bool enable)
{ closeOnDataFailure = enable; }

void FTPConnection::setTimeouts(double command, double transfer, double total)
{
    commandTimeout = command;
    transferTimeout = transfer;
    transferTotalTimeout = total;
}

void FTPConnection::setKeepalive(double interval, bool queueing)
{
    keepaliveInterval = interval;
    allowKeepaliveQueueing = queueing;
}

void FTPConnection::setResumeRetries(int total)
{
    if (total < 0)
        total = 0;
    resumeRetries = total;
}

void FTPConnection::setUploadRateLimit(std::int_fast64_t bytesPerSecond)
{
    if (bytesPerSecond <= 0) {
        uploadRateLimit = 0;
        return;
    }
    uploadRateLimit = bytesPerSecond;
}

static double toTimeout(const CPD3::Data::Variant::Read &value, double def)
{
    {
        double d = value.toReal();
        if (FP::defined(d))
            return d;
    }
    auto i = value.toInteger();
    if (!INTEGER::defined(i))
        return def;
    return static_cast<double>(i);
}

void FTPConnection::configure(const CPD3::Data::Variant::Read &configuration)
{
    setSSL(configuration.hash("SSL"));
    const auto &active = configuration.hash("ActiveMode").toString();
    if (Util::equal_insensitive(active, "both"))
        setPassiveThenActive();
    else if (Util::equal_insensitive(active, "active", "activeonly"))
        setActive();
    else
        setPassive();
    setRelaxedIncoming(configuration.hash("RelaxedIncoming").toBool());
    bool resume = true;
    if (configuration.hash("Resume").hash("Download").exists())
        resume = configuration.hash("Resume").hash("Download").toBoolean();
    setResumeDownload(resume);
    resume = true;
    if (configuration.hash("Resume").hash("Upload").exists())
        resume = configuration.hash("Resume").hash("Upload").toBoolean();
    setResumeUpload(resume);
    setTimeouts(toTimeout(configuration.hash("Timeout").hash("Command"), commandTimeout),
                toTimeout(configuration.hash("Timeout").hash("Transfer"), transferTimeout),
                toTimeout(configuration.hash("Timeout").hash("Total"), transferTotalTimeout));
    setKeepalive(toTimeout(configuration.hash("KeepAlive"), keepaliveInterval),
            !configuration.hash("ImmediateKeepalive").toBoolean());

    qint64 n = configuration.hash("Resume").hash("Retries").toInt64();
    if (INTEGER::defined(n)) {
        setResumeRetries((int) n);
    }
    n = configuration.hash("UploadRateLimit").toInt64();
    if (!INTEGER::defined(n)) {
        double f = configuration.hash("UploadRateLimit").toDouble();
        if (FP::defined(f)) {
            if (f > 0.0) {
                n = static_cast<std::int_fast64_t>(std::ceil(f));
            } else {
                n = -1;
            }
        }
    }
    if (INTEGER::defined(n)) {
        setUploadRateLimit(n);
    }
}


FTPConnection::Clock::time_point FTPConnection::nextCommandTimeout() const
{
    if (!FP::defined(commandTimeout) || commandTimeout <= 0.0)
        return Clock::now() + std::chrono::seconds(3600);
    return Clock::now() +
            std::chrono::milliseconds(std::max<std::size_t>(1, commandTimeout * 1000.0));
}

FTPConnection::Clock::time_point FTPConnection::totalTransferTimeout() const
{
    if (!FP::defined(transferTotalTimeout) || transferTotalTimeout <= 0.0)
        return Clock::now() + std::chrono::seconds(86400);
    return Clock::now() +
            std::chrono::milliseconds(std::max<std::size_t>(1, transferTotalTimeout * 1000.0));
}

FTPConnection::Clock::time_point FTPConnection::dataConnectionEstablishTimeout(const Clock::time_point &absolute) const
{
    auto timeout = absolute;
    if (FP::defined(commandTimeout) && commandTimeout > 0.0) {
        auto next = Clock::now() +
                std::chrono::milliseconds(std::max<std::size_t>(1, commandTimeout * 1000.0));
        if (next < timeout)
            timeout = next;
    }
    if (FP::defined(transferTotalTimeout) && transferTotalTimeout > 0.0) {
        auto next = Clock::now() +
                std::chrono::milliseconds(
                        std::max<std::size_t>(1, transferTotalTimeout / 2.0 * 1000.0));
        if (next < timeout)
            timeout = next;
    }
    return timeout;
}

std::string FTPConnection::nextCommandLine(const Clock::time_point &timeout)
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        Util::ByteArray::LineIterator it(commandData);
        auto raw = it.next(commandConnectionEnded);
        if (!raw.empty()) {
            auto line = raw.toString();
            commandData.pop_front(it.getCurrentOffset());
            return line;
        }
        if (timeout <= Clock::now())
            break;
        if (terminated)
            break;
        if (commandConnectionEnded)
            break;
        notify.wait_until(lock, timeout);
    }
    if (commandConnectionEnded) {
        qCDebug(log_transfer_ftp) << "Connection closed without an available response";
    } else if (!terminated) {
        qCDebug(log_transfer_ftp) << "Timeout waiting for response";
    }
    return {};
}

int FTPConnection::nextCommandCode(const Clock::time_point &timeout)
{
    for (;;) {
        auto line = nextCommandLine(timeout);
        if (line.empty())
            return 0;
        if (line.length() < 3)
            return 0;
        if (line.length() > 3 && line[3] == '-')
            continue;
        if (line.length() > 3) {
            if (line[3] != ' ') {
                qCDebug(log_transfer_ftp) << "Invalid response code" << line;
                return 0;
            }
            line.resize(3);
        }

        try {
            return std::stoi(line);
        } catch (std::exception &) {
            qCDebug(log_transfer_ftp) << "Invalid response code" << line;
            return 0;
        }
    }
}

std::pair<int, std::string> FTPConnection::nextCommandResponse(const Clock::time_point &timeout)
{
    std::string data;
    for (;;) {
        auto line = nextCommandLine(timeout);
        if (line.empty())
            return {0, std::move(data)};
        if (line.length() < 3)
            return {0, std::move(data)};

        if (line.length() > 3) {
            std::copy(line.begin() + 4, line.end(), std::back_inserter(data));
            if (line[3] == '-')
                continue;
            if (line[3] != ' ') {
                qCDebug(log_transfer_ftp) << "Invalid response code" << line;
                return {0, std::move(data)};
            }

            line.resize(3);
        }

        try {
            return {std::stoi(line), std::move(data)};
        } catch (std::exception &) {
            qCDebug(log_transfer_ftp) << "Invalid response code" << line;
            return {0, std::move(data)};
        }
    }
}

bool FTPConnection::simpleCommand(const std::string &command)
{
    if (!commandConnection)
        return false;
    commandConnection->write(command + "\r\n");
    int rc = nextCommandCode(nextCommandTimeout());
    if (rc == 0)
        return false;
    if (rc / 100 != 2) {
        error = "Command failed with code: " + std::to_string(rc);
        qCDebug(log_transfer_ftp) << "Command" << command << "failed with code" << rc;
        return false;
    }
    return true;
}

bool FTPConnection::connectToServer()
{
    if (commandConnection) {
        error = "Already connected";
        qCDebug(log_transfer_ftp) << "Already connected to" << host;
        return false;
    }

    dataServer.reset();
    dataConnection.reset();
    incomingActiveDataConnections.clear();
    commandData.clear();
    commandConnectionEnded = false;

    qCDebug(log_transfer_ftp) << "Starting FTP connection to" << host;

    commandConnection = IO::Socket::TCP::connect(host, socketConfig, false);
    if (!commandConnection) {
        error = "Connection failed";
        qCDebug(log_transfer_ftp) << "Connection to" << host << "failed";
        return false;
    }
    commandConnection->read.connect([this](const Util::ByteArray &data) {
        {
            std::lock_guard<std::mutex> lock(this->mutex);
            commandData += data;
        }
        this->notify.notify_all();
    });
    commandConnection->ended.connect([this]() {
        {
            std::lock_guard<std::mutex> lock(this->mutex);
            commandConnectionEnded = true;
        }
        this->notify.notify_all();
    });
    commandConnection->start();

    {
        auto timeout = nextCommandTimeout();
        for (;;) {
            auto line = nextCommandLine(timeout);
            if (line.empty()) {
                error = "Handshake failed";
                qCDebug(log_transfer_ftp) << "Handshake failed with" << host;
                commandConnection.reset();
                commandConnectionEnded = true;
                return false;
            }
            if (Util::starts_with(line, "220-"))
                continue;
            /* Allow handshake responses that have lines without a number in them */
            if (!std::isdigit(line[0]))
                continue;
            if (Util::starts_with(line, "220"))
                break;

            error = "Invalid handshake: " + line;
            qCDebug(log_transfer_ftp) << "Handshake failed with" << host << ":" << line;
            commandConnection.reset();
            commandConnectionEnded = true;
            return false;
        }
    }

    if (!user.empty()) {
        commandConnection->write("USER " + user + "\r\n");
        int rc = nextCommandCode(nextCommandTimeout());
        if (!rc) {
            error = "User command failed";
            qCDebug(log_transfer_ftp) << "User command on" << host << "failed";
            commandConnection.reset();
            commandConnectionEnded = true;
            return false;
        }
        if (rc / 100 == 3 && !password.empty()) {
            commandConnection->write("PASS " + password + "\r\n");
            rc = nextCommandCode(nextCommandTimeout());
            if (!rc) {
                error = "Password command failed";
                qCDebug(log_transfer_ftp) << "Password command on" << host << "failed";
                commandConnection.reset();
                commandConnectionEnded = true;
                return false;
            }
        }
        if (rc / 100 != 2) {
            error = "Login rejected";
            qCDebug(log_transfer_ftp) << "Login to" << host << "rejected with code" << rc;
            commandConnection.reset();
            commandConnectionEnded = true;
            return false;
        }
    }

    if (!simpleCommand("TYPE I")) {
        error = "Binary data set failed";
        qCDebug(log_transfer_ftp) << "TYPE command failed";
        commandConnection.reset();
        commandConnectionEnded = true;
        return false;
    }

    qCDebug(log_transfer_ftp) << "Connected to" << host;

    return true;
}

void FTPConnection::disconnectFromServer()
{
    if (commandConnection) {
        commandConnection->write("QUIT\r\n");
        commandConnection.reset();
        qCDebug(log_transfer_ftp) << "Closed connection to" << host;
    }
    dataServer.reset();
    dataConnection.reset();
    incomingActiveDataConnections.clear();
    passiveConnectionHost.clear();
    commandConnectionEnded = true;
    commandData.clear();
}

bool FTPConnection::mkdir(const std::string &path)
{ return simpleCommand("MKD " + path); }

bool FTPConnection::prepareDataConnection(bool polarity)
{
    dataServer.reset();
    dataConnection.reset();
    incomingActiveDataConnections.clear();
    passiveConnectionHost.clear();

    if (!polarity) {
        switch (transferMode) {
        case TransferMode::Active:
        case TransferMode::ActiveThenPassive:
            return prepareActiveConnection();
        case TransferMode::Passive:
        case TransferMode::PassiveThenActive:
            return preparePassiveConnection();
        }
    } else {
        switch (transferMode) {
        case TransferMode::Active:
        case TransferMode::PassiveThenActive:
            return prepareActiveConnection();
        case TransferMode::Passive:
        case TransferMode::ActiveThenPassive:
            return preparePassiveConnection();
        }
    }

    return false;
}

bool FTPConnection::establishDataConnection(const Clock::time_point &timeout)
{
    if (!dataServer) {
        if (passiveConnectionHost.empty())
            return false;
        dataConnection =
                IO::Socket::TCP::connect(passiveConnectionHost, passiveConnectionConfig, false);
        return dataConnection.get() != nullptr;
    }

    {
        std::unique_lock<std::mutex> lock(mutex);
        while (Clock::now() < timeout) {
            if (!incomingActiveDataConnections.empty()) {
                dataConnection = std::move(incomingActiveDataConnections.front());
                if (dataConnection)
                    return true;
            }
            notify.wait_until(lock, timeout,
                              [this] { return !incomingActiveDataConnections.empty(); });
        }
    }

    error = "Timeout waiting for active connection";
    qCDebug(log_transfer_ftp) << "Timeout waiting for an incoming active connection";
    return false;
}

std::pair<std::string, std::uint16_t> FTPConnection::negotiationEPSV()
{
    if (!commandConnection)
        return {};

    commandConnection->write("EPSV\r\n");
    auto resp = nextCommandResponse(nextCommandTimeout());
    if (resp.first != 229) {
        error = "EPSV command rejected";
        return {};
    }
    if (resp.second.empty()) {
        error = "EPSV response empty";
        return {};
    }

    auto iBegin = std::find(resp.second.begin(), resp.second.end(), '(');
    if (iBegin == resp.second.end()) {
        error = "Malformed EPSV response: " + resp.second;
        qCDebug(log_transfer_ftp) << "Malformed EPSV response:" << resp.second;
        return {};
    }
    auto iEnd = std::find(iBegin + 1, resp.second.end(), ')');
    if (iEnd == resp.second.end()) {
        error = "Malformed EPSV response: " + resp.second;
        qCDebug(log_transfer_ftp) << "Malformed EPSV response:" << resp.second;
        return {};
    }

    if (iEnd - iBegin < 4) {
        error = "Malformed EPSV response: " + resp.second;
        qCDebug(log_transfer_ftp) << "Malformed EPSV response:" << resp.second;
        return {};
    }

    auto iPortBegin = iBegin + 4;
    auto iPortEnd = std::find(iPortBegin, iEnd, *(iBegin + 1));
    if (iPortEnd == iEnd) {
        error = "Malformed EPSV response: " + resp.second;
        qCDebug(log_transfer_ftp) << "Malformed EPSV response:" << resp.second;
        return {};
    }

    try {
        auto port = std::stoi(std::string(iPortBegin, iPortEnd), nullptr, 10);
        if (port <= 0 || port > 65535) {
            error = "Invalid EPSV port: " + std::to_string(port);
            qCDebug(log_transfer_ftp) << "Invalid EPSV port:" << resp.second;
            return {};
        }

        qCDebug(log_transfer_ftp) << "Extend passive mode negotiated on port" << port;
        return {host, port};
    } catch (std::exception &) {
        error = "Malformed EPSV response: " + resp.second;
        qCDebug(log_transfer_ftp) << "Malformed EPSV response:" << resp.second;
        return {};
    }
    return {};
}

std::pair<std::string, std::uint16_t> FTPConnection::negotiationPASV()
{
    if (!commandConnection)
        return {};

    commandConnection->write("PASV\r\n");
    auto resp = nextCommandResponse(nextCommandTimeout());
    if (resp.first != 227) {
        error = "PASV command rejected";
        return {};
    }
    if (resp.second.empty()) {
        error = "PASV response empty";
        return {};
    }

    auto iBegin = std::find(resp.second.begin(), resp.second.end(), '(');
    if (iBegin == resp.second.end()) {
        error = "Malformed PASV response: " + resp.second;
        qCDebug(log_transfer_ftp) << "Malformed PASV response:" << resp.second;
        return {};
    }
    auto iEnd = std::find(iBegin + 1, resp.second.end(), ')');
    if (iEnd == resp.second.end()) {
        error = "Malformed PASV response: " + resp.second;
        qCDebug(log_transfer_ftp) << "Malformed PASV response:" << resp.second;
        return {};
    }

    auto fields = Util::split_string(std::string(iBegin + 1, iEnd), ',', true);
    if (fields.size() != 6) {
        error = "Malformed PASV response: " + resp.second;
        qCDebug(log_transfer_ftp) << "Malformed PASV response:" << resp.second;
        return {};
    }

    try {
        std::string pasvHost;
        std::uint16_t pasvPort = 0;

        auto i = std::stoi(fields[0]);
        if (i < 0 || i > 255) {
            error = "Invalid PASV IP[0] field: " + std::to_string(i);
            qCDebug(log_transfer_ftp) << "Invalid PASV IP field:" << resp.second;
            return {};
        }
        pasvHost += std::to_string(i);
        pasvHost += '.';

        i = std::stoi(fields[1]);
        if (i < 0 || i > 255) {
            error = "Invalid PASV IP[1] field: " + std::to_string(i);
            qCDebug(log_transfer_ftp) << "Invalid PASV IP field:" << resp.second;
            return {};
        }
        pasvHost += std::to_string(i);
        pasvHost += '.';

        i = std::stoi(fields[2]);
        if (i < 0 || i > 255) {
            error = "Invalid PASV IP[2] field: " + std::to_string(i);
            qCDebug(log_transfer_ftp) << "Invalid PASV IP field:" << resp.second;
            return {};
        }
        pasvHost += std::to_string(i);
        pasvHost += '.';

        i = std::stoi(fields[3]);
        if (i < 0 || i > 255) {
            error = "Invalid PASV IP[3] field: " + std::to_string(i);
            qCDebug(log_transfer_ftp) << "Invalid PASV IP field:" << resp.second;
            return {};
        }
        pasvHost += std::to_string(i);

        i = std::stoi(fields[4]);
        if (i < 0 || i > 255) {
            error = "Invalid PASV port[0] field: " + std::to_string(i);
            qCDebug(log_transfer_ftp) << "Invalid PASV port field:" << resp.second;
            return {};
        }
        pasvPort = static_cast<std::uint16_t>(i) << 8U;

        i = std::stoi(fields[5]);
        if (i < 0 || i > 255) {
            error = "Invalid PASV port[1] field: " + std::to_string(i);
            qCDebug(log_transfer_ftp) << "Invalid PASV port field:" << resp.second;
            return {};
        }
        pasvPort |= static_cast<std::uint16_t>(i);

        qCDebug(log_transfer_ftp) << "Passive mode negotiated to" << pasvHost << ":" << pasvPort;
        return {std::move(pasvHost), pasvPort};
    } catch (std::exception &) {
        error = "Malformed PASV response: " + resp.second;
        qCDebug(log_transfer_ftp) << "Malformed PASV response:" << resp.second;
        return {};
    }
    return {};
}

bool FTPConnection::preparePassiveConnection()
{
    qCDebug(log_transfer_ftp) << "Starting passive mode negotiation";

    {
        auto epsv = negotiationEPSV();
        if (!epsv.first.empty() && epsv.second) {
            passiveConnectionHost = epsv.first;
            passiveConnectionConfig.port = epsv.second;
            return true;
        }
    }

    {
        auto pasv = negotiationPASV();
        if (!pasv.first.empty() && pasv.second) {
            passiveConnectionHost = pasv.first;
            passiveConnectionConfig.port = pasv.second;
            return true;
        }
    }

    return false;
}

void FTPConnection::acceptIncomingActiveConnection(std::unique_ptr<IO::Socket::Connection> &&conn)
{
    auto incomingTCP = dynamic_cast<IO::Socket::TCP::Connection *>(conn.get());
    auto commandTCP = dynamic_cast<IO::Socket::TCP::Connection *>(commandConnection.get());
    if (!relaxedIncoming && incomingTCP) {
        if (incomingTCP->peerPort() != getPort() - 1)
            return;
        if (commandTCP) {
            if (incomingTCP->peerAddress() != commandTCP->peerAddress())
                return;
        }
    }
    {
        std::lock_guard<std::mutex> lock(mutex);
        incomingActiveDataConnections.emplace_back(std::move(conn));
    }
    notify.notify_all();
}

bool FTPConnection::prepareActiveConnection()
{
    auto serverSocketConfig = socketConfig;
    serverSocketConfig.port = serverSocketConfig.port_any;
    if (auto cmd = dynamic_cast<IO::Socket::TCP::Connection *>(commandConnection.get())) {
        dataServer.reset(new IO::Socket::TCP::Server(
                std::bind(&FTPConnection::acceptIncomingActiveConnection, this,
                          std::placeholders::_1), cmd->localAddress(),
                std::move(serverSocketConfig)));
    } else {
        dataServer.reset(new IO::Socket::TCP::Server(
                std::bind(&FTPConnection::acceptIncomingActiveConnection, this,
                          std::placeholders::_1), std::move(serverSocketConfig)));
    }
    if (!dataServer->startListening()) {
        error = "Active server start failed";
        qCDebug(log_transfer_ftp) << "Failed start active server";
        return false;
    }

    std::uint16_t port = dataServer->localPort();

    auto addresses = dataServer->localAddress();
    {
        QHostAddress selected;
        for (const auto &addr : addresses) {
            if (addr.isLoopback())
                continue;
#if QT_VERSION >= QT_VERSION_CHECK(5, 11, 0)
            if (!addr.isGlobal())
                continue;
#endif
#ifndef QT_NO_IPV4
            if (addr == QHostAddress(QHostAddress::AnyIPv4))
                continue;
#endif
#ifndef QT_NO_IPV6
            if (addr == QHostAddress(QHostAddress::AnyIPv6))
                continue;
#endif
            if (addr == QHostAddress(QHostAddress::Any))
                continue;
            selected = addr;
            break;
        }
        if (!selected.isNull()) {
            std::string command = "EPRT |";
            switch (selected.protocol()) {
            case QAbstractSocket::IPv6Protocol:
                command += "2|";
                break;
            default:
                command += "1|";
                break;
            }
            command += selected.toString().toStdString();
            command += "|";
            command += std::to_string(port);
            command += "|";
            if (simpleCommand(command)) {
                qCDebug(log_transfer_ftp) << "Extended active mode negotiation on" << selected
                                          << "port" << port;
                return true;
            }
        }
    }

#ifndef QT_NO_IPV4
    {
        quint32 ip = 0;
        for (const auto &addr : addresses) {
            if (addr.isLoopback())
                continue;
#if QT_VERSION >= QT_VERSION_CHECK(5, 11, 0)
            if (!addr.isGlobal())
                continue;
#endif
            if (addr == QHostAddress(QHostAddress::AnyIPv4))
                continue;
            if (addr == QHostAddress(QHostAddress::Any))
                continue;

            bool ok = false;
            ip = addr.toIPv4Address(&ok);
            if (ok)
                break;
            ip = 0;
        }
        if (ip) {
            std::string command = "PORT ";
            command += std::to_string((ip >> 24U) & 0xFFU);
            command += ",";
            command += std::to_string((ip >> 16U) & 0xFFU);
            command += ",";
            command += std::to_string((ip >> 8U) & 0xFFU);
            command += ",";
            command += std::to_string((ip >> 0U) & 0xFFU);
            command += ",";
            command += std::to_string((port >> 8U) & 0xFFU);
            command += ",";
            command += std::to_string((port >> 0U) & 0xFFU);
            if (simpleCommand(command)) {
                qCDebug(log_transfer_ftp) << "Active mode negotiation on port" << port;
                return true;
            }
        }
    }
#endif

    {
        QHostAddress selected;
        for (const auto &addr : addresses) {
#ifndef QT_NO_IPV4
            if (addr == QHostAddress(QHostAddress::AnyIPv4))
                continue;
#endif
#ifndef QT_NO_IPV6
            if (addr == QHostAddress(QHostAddress::AnyIPv6))
                continue;
#endif
            if (addr == QHostAddress(QHostAddress::Any))
                continue;
            selected = addr;
            break;
        }
        if (!selected.isNull()) {
            std::string command = "EPRT |";
            switch (selected.protocol()) {
            case QAbstractSocket::IPv6Protocol:
                command += "2|";
                break;
            default:
                command += "1|";
                break;
            }
            command += selected.toString().toStdString();
            command += "|";
            command += std::to_string(port);
            command += "|";
            if (simpleCommand(command)) {
                qCDebug(log_transfer_ftp) << "Extended active mode negotiation on" << selected
                                          << "port" << port;
                return true;
            }
        }
    }

#ifndef QT_NO_IPV4
    {
        quint32 ip = 0;
        for (const auto &addr : addresses) {
            if (addr == QHostAddress(QHostAddress::AnyIPv4))
                continue;
            if (addr == QHostAddress(QHostAddress::Any))
                continue;

            bool ok = false;
            ip = addr.toIPv4Address(&ok);
            if (ok)
                break;
            ip = 0;
        }
        if (ip) {
            std::string command = "PORT ";
            command += std::to_string((ip >> 24U) & 0xFFU);
            command += ",";
            command += std::to_string((ip >> 16U) & 0xFFU);
            command += ",";
            command += std::to_string((ip >> 8U) & 0xFFU);
            command += ",";
            command += std::to_string((ip >> 0U) & 0xFFU);
            command += ",";
            command += std::to_string((port >> 8U) & 0xFFU);
            command += ",";
            command += std::to_string((port >> 0U) & 0xFFU);
            if (simpleCommand(command)) {
                qCDebug(log_transfer_ftp) << "Active mode negotiation on port" << port;
                return true;
            }
        }
    }
#endif

    error = "No available active address";
    qCDebug(log_transfer_ftp) << "No available address to accept active connection on";
    return false;
}

void FTPConnection::startKeepalive()
{
    keepaliveState = KeepaliveState::Disabled;
    if (!FP::defined(keepaliveInterval) || keepaliveInterval <= 0.0)
        return;
    keepaliveState = KeepaliveState::WaitingToSend;
    keepaliveTimeout = Clock::now() +
            std::chrono::milliseconds(std::max<std::size_t>(1, keepaliveInterval * 1000.0));
    queuedKeepalives = 0;
}

bool FTPConnection::stepKeepalive(std::unique_lock<std::mutex> &lock)
{
    switch (keepaliveState) {
    case KeepaliveState::Disabled:
    case KeepaliveState::Failed:
        return true;
    case KeepaliveState::WaitingToSend:
        if (keepaliveTimeout > Clock::now())
            return true;

        if (!commandConnection) {
            keepaliveState = KeepaliveState::Failed;
            return false;
        }
        keepaliveState = KeepaliveState::WaitingForResponse;
        keepaliveTimeout = Clock::now() +
                std::chrono::milliseconds(std::max<std::size_t>(1, std::max(
                        keepaliveInterval * 2 * 1000.0, commandTimeout * 1000.0)));
        queuedKeepalives++;
        lock.unlock();
        commandConnection->write("NOOP\r\n");
        lock.lock();
        return false;
    case KeepaliveState::WaitingForResponse:
        break;
    }

    Util::ByteArray::LineIterator it(commandData);
    while (auto raw = it.next(commandConnectionEnded)) {
        std::string line = raw.toString();

        if (line.empty())
            continue;
        if (line.length() < 3)
            continue;
        if (line.length() > 3 && line[3] == '-')
            continue;
        if (line.length() > 3) {
            if (line[3] != ' ') {
                qCDebug(log_transfer_ftp) << "Invalid NOOP response code" << line;
                continue;
            }
            line.resize(3);
        }

        try {
            std::stoi(line);
        } catch (std::exception &) {
            qCDebug(log_transfer_ftp) << "Invalid NOOP keepalive response" << raw;
            keepaliveState = KeepaliveState::Failed;
            commandData.pop_front(it.getCurrentOffset());
            return false;
        }

        --queuedKeepalives;
        if (!allowKeepaliveQueueing || queuedKeepalives <= 0) {
            commandData.pop_front(it.getCurrentOffset());

            keepaliveState = KeepaliveState::WaitingToSend;
            keepaliveTimeout = Clock::now() +
                    std::chrono::milliseconds(std::max<std::size_t>(1, keepaliveInterval * 1000.0));
            return false;
        }
    }
    commandData.pop_front(it.getCurrentOffset());

    if (commandConnectionEnded || keepaliveTimeout <= Clock::now()) {
        if (!commandConnectionEnded && allowKeepaliveQueueing && queuedKeepalives > 0) {
            keepaliveState = KeepaliveState::WaitingToSend;
            keepaliveTimeout = Clock::now();
            return false;
        }
        error = "Keepalive timeout";
        qCDebug(log_transfer_ftp) << "Timeout waiting for keepalive response";
        keepaliveState = KeepaliveState::Failed;
        return false;
    }
    return true;
}

void FTPConnection::flushKeepalive(std::unique_lock<std::mutex> &lock,
                                   const Clock::time_point &timeout)
{
    switch (keepaliveState) {
    case KeepaliveState::Disabled:
    case KeepaliveState::Failed:
    case KeepaliveState::WaitingToSend:
        if (allowKeepaliveQueueing && queuedKeepalives > 0)
            break;
        return;
    case KeepaliveState::WaitingForResponse:
        break;
    }

    auto maximumWait = timeout;
    bool firstResponse = true;

    for (;;) {
        if (Clock::now() <= maximumWait)
            return;

        Util::ByteArray::LineIterator it(commandData);
        while (auto line = it.next(commandConnectionEnded)) {
            if (line.empty())
                continue;
            if (line.size() < 3)
                continue;
            if (line.size() > 3 && line[3] == '-')
                continue;

            --queuedKeepalives;
            if (!allowKeepaliveQueueing || queuedKeepalives <= 0) {
                commandData.pop_front(it.getCurrentOffset());
                keepaliveState = KeepaliveState::Disabled;
                return;
            } else if (firstResponse) {
                firstResponse = false;
                auto lastResponse = Clock::now() + std::chrono::seconds(2);
                if (lastResponse < maximumWait)
                    maximumWait = lastResponse;
            }
        }

        commandData.pop_front(it.getCurrentOffset());
        notify.wait_until(lock, timeout);
    }
}

bool FTPConnection::downloadData(IO::Generic::Writable &target,
                                 const Clock::time_point &timeout,
                                 std::uint_fast64_t expectedSize,
                                 std::uint_fast64_t cutoffSize)
{
    if (!dataConnection || !commandConnection)
        return false;
    bool transferEnded = false;

    auto connection = std::move(dataConnection);

    Clock::time_point lastByteReceived = Clock::now();
    bool byteStalled = false;

    Threading::Receiver rx;
    connection->read.connect(rx, [&](const Util::ByteArray &data) {
        target.write(data);

        byteCounter += data.size();
        if (expectedSize != static_cast<std::uint_fast64_t>(-1)) {
            progress(byteCounter, expectedSize);
        }

        {
            std::lock_guard<std::mutex> lock(mutex);
            lastByteReceived = Clock::now();
        }

        if (cutoffSize > 0 && byteCounter > cutoffSize) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                if (transferEnded)
                    return;
                transferEnded = true;
                notify.notify_all();
            }
            qCDebug(log_transfer_ftp) << "Cutting off download after" << byteCounter << "bytes";
        }
    });

    connection->ended.connect([this, &transferEnded]() {
        std::lock_guard<std::mutex> lock(mutex);
        transferEnded = true;
        notify.notify_all();
    });

    if (auto writeStream = dynamic_cast<IO::Generic::Stream *>(&target)) {
        writeStream->writeStall.connect(rx, [&](bool stalled) {
            connection->readStall(stalled);
            {
                std::lock_guard<std::mutex> lock(mutex);
                if (byteStalled && !stalled)
                    lastByteReceived = Clock::now();
                byteStalled = stalled;
            }
        });
    }

    connection->start();
    {
        startKeepalive();

        std::unique_lock<std::mutex> lock(mutex);
        while (Clock::now() < timeout) {
            if (transferEnded) {
                flushKeepalive(lock, timeout);
                return true;
            }
            if (terminated)
                return false;

            if (keepaliveState == KeepaliveState::Failed)
                break;
            if (!stepKeepalive(lock))
                continue;

            auto wakeTime = timeout;
            if (keepaliveState != KeepaliveState::Disabled && keepaliveTimeout < wakeTime) {
                wakeTime = keepaliveTimeout;
            }
            if (!byteStalled && FP::defined(transferTimeout) && transferTimeout > 0.0) {
                auto expireTime = lastByteReceived +
                        std::chrono::milliseconds(
                                std::max<std::size_t>(1, transferTimeout * 1000.0));
                if (expireTime <= Clock::now())
                    break;
                if (expireTime < wakeTime)
                    wakeTime = expireTime;
            }
            notify.wait_until(lock, wakeTime);
        }
    }
    connection.reset();

    if (keepaliveState == KeepaliveState::Failed) {
        error = "Keepalive failure";
        qCDebug(log_transfer_ftp) << "Download transfer aborted due to keepalive failure";
        return false;
    }

    error = "Download timeout";
    qCDebug(log_transfer_ftp) << "Timeout waiting for download transfer to complete";
    return false;
}

bool FTPConnection::uploadData(std::unique_ptr<IO::Generic::Stream> &&source,
                               const Clock::time_point &timeout,
                               std::uint_fast64_t expectedSize)
{
    if (!dataConnection || !commandConnection)
        return false;

    auto connection = std::move(dataConnection);

    bool transferEnded = false;
    std::uint_fast64_t bytesThisSecond = 0;
    bool rateStalled = false;
    bool connectionStalled = false;
    bool anyStalled = false;

    Threading::Receiver rx;
    source->read.connect(rx, [&](const Util::ByteArray &data) {
        connection->write(data);

        byteCounter += data.size();
        if (expectedSize != static_cast<std::uint_fast64_t>(-1)) {
            progress(byteCounter, expectedSize);
        }

        std::unique_lock<std::mutex> lock(mutex);
        bytesThisSecond += data.size();
        if (uploadRateLimit > 0 && bytesThisSecond > uploadRateLimit) {
            if (!rateStalled) {
                rateStalled = true;
                anyStalled = true;
                lock.unlock();
                source->readStall(true);
            }
        }
    });
    source->ended.connect(rx, [&]() {
        {
            std::lock_guard<std::mutex> lock(mutex);
            transferEnded = true;
        }
        notify.notify_all();
    });

    connection->writeStall.connect(rx, [&](bool stalled) {
        std::unique_lock<std::mutex> lock(mutex);
        if (stalled) {
            if (connectionStalled)
                return;
            connectionStalled = true;
            anyStalled = true;
            lock.unlock();
            source->readStall(true);
        } else {
            connectionStalled = false;
            lock.unlock();
            notify.notify_all();
        }
    });

    connection->start();
    source->start();

    {
        startKeepalive();

        Clock::time_point rateResetTime = Clock::now() + std::chrono::seconds(1);

        std::unique_lock<std::mutex> lock(mutex);
        while (Clock::now() < timeout) {
            if (transferEnded) {
                flushKeepalive(lock, timeout);
                lock.unlock();
                source.reset();
                connection.reset();
                return true;
            }
            if (terminated)
                return false;

            if (keepaliveState == KeepaliveState::Failed)
                break;
            if (!stepKeepalive(lock))
                continue;

            auto wakeTime = timeout;
            if (keepaliveState != KeepaliveState::Disabled && keepaliveTimeout < wakeTime) {
                wakeTime = keepaliveTimeout;
            }
            if (uploadRateLimit > 0) {
                auto now = Clock::now();
                if (rateResetTime <= now) {
                    bytesThisSecond = 0;
                    rateStalled = false;
                    rateResetTime = now + std::chrono::seconds(1);
                }
                if (rateResetTime < wakeTime)
                    wakeTime = rateResetTime;
            }

            bool wasStalled = anyStalled;
            anyStalled = rateStalled || connectionStalled;
            if (wasStalled && !anyStalled) {
                /* No longer stalled, so release it */
                lock.unlock();
                source->readStall(false);
                lock.lock();
                anyStalled = rateStalled || connectionStalled;
                if (anyStalled) {
                    /* Something stalled it again while we had the lock released, so make sure
                     * it's still stalled (in case our stall release got ordered after the stall
                     * acquire) */
                    lock.unlock();
                    source->readStall(true);
                    lock.lock();
                }
                continue;
            }

            notify.wait_until(lock, wakeTime);
        }
    }
    source.reset();
    connection.reset();

    if (keepaliveState == KeepaliveState::Failed) {
        error = "Keepalive failure";
        qCDebug(log_transfer_ftp) << "Upload transfer aborted due to keepalive failure";
        return false;
    }

    error = "Upload timeout";
    qCDebug(log_transfer_ftp) << "Timeout waiting for upload transfer to complete";
    return false;
}

std::vector<FTPConnection::File> FTPConnection::list(const std::string &path,
                                                     bool directories,
                                                     bool *ok)
{
    qCDebug(log_transfer_ftp) << "Starting list for" << path;

    if (ok)
        *ok = false;
    int tryLimit = resumeRetries + 1;
    switch (transferMode) {
    case TransferMode::PassiveThenActive:
    case TransferMode::ActiveThenPassive:
        tryLimit *= 2;
        break;
    case TransferMode::Passive:
    case TransferMode::Active:
        break;
    }

    auto absoluteTimeout = totalTransferTimeout();

    byteCounter = 0;
    bool dataConnectionPolarity = false;
    bool useNLST = false;
    bool haveTriedNLST = false;
    bool haveTriedLIST = false;
    for (int t = 0; t < tryLimit; ++t) {
        if (absoluteTimeout < Clock::now()) {
            error = "Maximum transfer timeout exceeded";
            qCDebug(log_transfer_ftp) << "Maximum transfer timeout exceeded during list";
            return {};
        }

        if (!commandConnection) {
            if (!connectToServer())
                continue;
        }

        if (!prepareDataConnection(dataConnectionPolarity)) {
            if (closeOnDataFailure) {
                disconnectFromServer();
            }
            dataConnectionPolarity = !dataConnectionPolarity;
            continue;
        }

        if (useNLST) {
            commandConnection->write("NLST " + path + "\r\n");
        } else {
            commandConnection->write("LIST " + path + "\r\n");
        }
        if (!establishDataConnection(dataConnectionEstablishTimeout(absoluteTimeout))) {
            if (closeOnDataFailure) {
                disconnectFromServer();
            }
            dataConnectionPolarity = !dataConnectionPolarity;
            continue;
        }

        int rc = nextCommandCode(nextCommandTimeout());
        if (!rc) {
            commandConnection.reset();
            commandConnectionEnded = true;
            continue;
        }

        if (rc / 100 == 5) {
            qCDebug(log_transfer_ftp) << "000000" << rc;
            if (useNLST) {
                useNLST = false;
                haveTriedNLST = true;
                if (!haveTriedLIST) {
                    ++tryLimit;
                }
            } else {
                useNLST = true;
                haveTriedLIST = true;
                if (!haveTriedNLST) {
                    ++tryLimit;
                }
            }
            continue;
        }
        if (rc / 100 != 1) {
            error = "Invalid response to list command: " + std::to_string(rc);
            qCDebug(log_transfer_ftp) << "Invalid response to list command" << rc;
            commandConnection.reset();
            commandConnectionEnded = true;
            dataConnectionPolarity = !dataConnectionPolarity;
            continue;
        }

        Util::ByteArray listData;
        {
            IO::Generic::Stream::ByteArray target(listData);
            if (!downloadData(target, absoluteTimeout, -1, 32 * 1024 * 1024)) {
                if (closeOnDataFailure) {
                    disconnectFromServer();
                }
                dataConnectionPolarity = !dataConnectionPolarity;
                continue;
            }

            rc = nextCommandCode(absoluteTimeout);
            if (rc / 100 != 2) {
                continue;
            }
        }

        if (useNLST) {
            std::vector<File> result;
            if (interpretNLST(listData, directories, result)) {
                if (ok)
                    *ok = true;
                qCDebug(log_transfer_ftp) << "NLST received" << byteCounter << "bytes and"
                                          << result.size() << "entries";
                return result;
            }

            useNLST = false;
            haveTriedNLST = true;
            if (!haveTriedLIST) {
                ++tryLimit;
            }
        } else {
            std::vector<File> result;
            if (interpretLIST(listData, directories, path, result)) {
                if (ok)
                    *ok = true;
                qCDebug(log_transfer_ftp) << "LIST received" << byteCounter << "bytes and"
                                          << result.size() << "entries";
                return result;
            }

            useNLST = true;
            haveTriedLIST = true;
            if (!haveTriedNLST) {
                ++tryLimit;
            }
        }
    }

    return {};
}

bool FTPConnection::interpretNLST(Util::ByteArray &raw, bool directories, std::vector<File> &result)
{
    if (!commandConnection)
        return false;

    Util::ByteArray::LineIterator it(raw);
    while (auto rawLine = it.next(true)) {
        auto fullName = rawLine.toString();
        if (fullName.empty())
            continue;

        std::string fileName;
        std::string pathName;
        {
            auto filenameBegin = fullName.begin();
            auto pathnameEnd = fullName.begin();
            for (;;) {
                auto next = std::find(pathnameEnd + 1, fullName.end(), '/');
                if (next == fullName.end())
                    break;

                filenameBegin = next + 1;
                pathnameEnd = next;
            }
            fileName = std::string(filenameBegin, fullName.end());
            pathName = std::string(fullName.begin(), pathnameEnd);
        }

        std::uint_fast64_t size = 0;
        double mtime = FP::undefined();
        if (!directories) {
            commandConnection->write("MDTM " + fullName + "\r\n");
            auto resp = nextCommandResponse(nextCommandTimeout());
            if (!resp.first)
                return false;
            if (resp.first / 100 == 5) {
                /* Probably a directory */
                continue;
            }
            if (resp.first / 100 != 2) {
                error = "Invalid modified time response " +
                        std::to_string(resp.first) +
                        ": " +
                        resp.second;
                qCDebug(log_transfer_ftp) << "Invalid MDTM response code " << resp.first << ":"
                                          << resp.second;
                return false;
            }

            mtime = parseResponseTime(resp.second);

            commandConnection->write("SIZE " + fullName + "\r\n");
            resp = nextCommandResponse(nextCommandTimeout());
            if (resp.first / 100 == 5) {
                /* Probably a directory */
                continue;
            }
            if (resp.first / 100 != 2) {
                error = "Invalid size response " + std::to_string(resp.first) + ": " + resp.second;
                qCDebug(log_transfer_ftp) << "Invalid SIZE response code " << resp.first << ":"
                                          << resp.second;
                return false;
            }

            try {
                size = std::stoull(resp.second);
            } catch (std::exception &) {
                size = 0;
            }
        } else {
            commandConnection->write("MDTM " + fullName + "\r\n");
            int rc = nextCommandCode(nextCommandTimeout());
            if (!rc)
                return false;
            /* 550 should be directories */
            if (rc / 100 != 5)
                continue;
        }

        result.emplace_back(File(std::move(fileName), std::move(pathName), size, mtime));
    }

    return true;
}

double FTPConnection::parseResponseTime(const std::string &response)
{
    if (response.size() < 14) {
        qCDebug(log_transfer_ftp) << "Modified time too short:" << response;
        error = "Invalid modified time: " + response;
        return FP::undefined();
    }

    int year;
    try {
        year = std::stoi(std::string(response.begin(), response.begin() + 4));
        if (year < 1900 || year > 2999) {
            qCDebug(log_transfer_ftp) << "Modified time year out of range:" << response;
            error = "Invalid modified time year: " + response;
            return FP::undefined();
        }
    } catch (std::exception &) {
        qCDebug(log_transfer_ftp) << "Modified time year invalid:" << response;
        error = "Invalid modified time year: " + response;
        return FP::undefined();
    }

    int month;
    try {
        month = std::stoi(std::string(response.begin() + 4, response.begin() + 6));
        if (month < 1 || month > 12) {
            qCDebug(log_transfer_ftp) << "Modified time month out of range:" << response;
            error = "Invalid modified time month: " + response;
            return FP::undefined();
        }
    } catch (std::exception &) {
        qCDebug(log_transfer_ftp) << "Modified time month invalid:" << response;
        error = "Invalid modified time month: " + response;
        return FP::undefined();
    }

    int day;
    try {
        day = std::stoi(std::string(response.begin() + 6, response.begin() + 8));
        if (day < 1 || day > 31) {
            qCDebug(log_transfer_ftp) << "Modified time day out of range:" << response;
            error = "Invalid modified time day: " + response;
            return FP::undefined();
        }
    } catch (std::exception &) {
        qCDebug(log_transfer_ftp) << "Modified time day invalid:" << response;
        error = "Invalid modified time day: " + response;
        return FP::undefined();
    }

    int hour;
    try {
        hour = std::stoi(std::string(response.begin() + 8, response.begin() + 10));
        if (hour < 0 || hour > 23) {
            qCDebug(log_transfer_ftp) << "Modified time hour out of range:" << response;
            error = "Invalid modified time hour: " + response;
            return FP::undefined();
        }
    } catch (std::exception &) {
        qCDebug(log_transfer_ftp) << "Modified time hour invalid:" << response;
        error = "Invalid modified time hour: " + response;
        return FP::undefined();
    }

    int minute;
    try {
        minute = std::stoi(std::string(response.begin() + 10, response.begin() + 12));
        if (minute < 0 || minute > 59) {
            qCDebug(log_transfer_ftp) << "Modified time minute out of range:" << response;
            error = "Invalid modified time minute: " + response;
            return FP::undefined();
        }
    } catch (std::exception &) {
        qCDebug(log_transfer_ftp) << "Modified time minute invalid:" << response;
        error = "Invalid modified time minute: " + response;
        return FP::undefined();
    }

    int second;
    try {
        second = std::stoi(std::string(response.begin() + 12, response.begin() + 14));
        if (second < 0 || second > 60) {
            qCDebug(log_transfer_ftp) << "Modified time second out of range:" << response;
            error = "Invalid modified time second: " + response;
            return FP::undefined();
        }
    } catch (std::exception &) {
        qCDebug(log_transfer_ftp) << "Modified time second invalid:" << response;
        error = "Invalid modified time second: " + response;
        return FP::undefined();
    }

    double fsec = 0;
    if (response.size() > 14) {
        try {
            std::string fraction = "1." + std::string(response.begin() + 14, response.end());
            fsec = std::stod(fraction);
            if (fsec < 0.0 || fsec >= 1.0)
                fsec = 0;
        } catch (std::exception &) {
            fsec = 0;
        }
    }

    QDateTime dt(QDate(year, month, day), QTime(hour, minute, second), Qt::UTC);
    if (!dt.isValid()) {
        qCDebug(log_transfer_ftp) << "Modified time date invalid:" << response;
        error = "Invalid modified date: " + response;
        return FP::undefined();
    }

    return Time::fromDateTime(dt) + fsec;
}


/*
 * From QFtp.  Copyright (C) 2012 Digia Plc and/or its subsidiary(-ies).
 */
static double parseUnixTime(QString time)
{
    QStringList formats;
    formats << QLatin1String("MMM dd  yyyy") << QLatin1String("MMM dd hh:mm")
            << QLatin1String("MMM  d  yyyy") << QLatin1String("MMM  d hh:mm")
            << QLatin1String("MMM  d yyyy") << QLatin1String("MMM dd yyyy");

    time[0] = time[0].toUpper();

    QDateTime dateTime;
    int n = 0;
    do {
        dateTime = QLocale::c().toDateTime(time, formats.at(n++));
    } while (n < formats.size() && (!dateTime.isValid()));

    if (n == 2 || n == 4) {
        dateTime.setDate(
                QDate(QDate::currentDate().year(), dateTime.date().month(), dateTime.date().day()));

        static const int futureTolerance = 86400;
        if (dateTime.secsTo(QDateTime::currentDateTime()) < -futureTolerance) {
            QDate d = dateTime.date();
            d.setDate(d.year() - 1, d.month(), d.day());
            dateTime.setDate(d);
        }
    }

    return Time::fromDateTime(dateTime.toUTC());
}

bool FTPConnection::interpretLIST(Util::ByteArray &raw,
                                  bool directories,
                                  const std::string &pathName,
                                  std::vector<File> &result)
{
    if (!commandConnection)
        return false;

    QRegularExpression reDOS
            (R"(^\s*(\d+-\d+-\d+\s*\d+\s*:\s*\d+\s*[AP]M)\s+((?:<DIR>)|(?:\d+))\s*(\S.+)$)",
             QRegularExpression::CaseInsensitiveOption);
    QRegularExpression reUnix
            (R"(^([ld-])[a-zA-Z-]{9}\s+\d+\s+\S+\s+\S+\s+(\d+)\s+(\S+\s+\S+\s+\S+)\s+(\S.+)$)",
             QRegularExpression::CaseInsensitiveOption);

    Util::ByteArray::LineIterator it(raw);
    bool anyLines = false;
    bool anyValidLines = false;
    while (auto rawLine = it.next(true)) {
        QString line = rawLine.toQString();
        QString fileName;
        double mtime = FP::undefined();
        std::uint_fast64_t size = 0;

        anyLines = true;

        bool isSymlink = false;
        auto r = reDOS.match(line, 0, QRegularExpression::NormalMatch,
                             QRegularExpression::AnchoredMatchOption);
        if (r.hasMatch() && r.capturedLength() == line.length()) {
            QDateTime
                    dt(QLocale::c().toDateTime(r.captured(1), QLatin1String("MM-dd-yy  hh:mmAP")));
            if (dt.isValid()) {
                if (dt.date().year() < 1971) {
                    dt.setDate(QDate(dt.date().year() + 100, dt.date().month(), dt.date().day()));
                }
                mtime = Time::fromDateTime(dt.toUTC());
            }

            if (r.captured(2).compare("<DIR>", Qt::CaseInsensitive) == 0) {
                if (!directories)
                    continue;
            } else {
                if (directories)
                    continue;
                bool ok = false;
                size = r.captured(2).toULongLong(&ok);
                if (!ok)
                    continue;
            }
            fileName = r.captured(3);
        } else {
            r = reUnix.match(line, 0, QRegularExpression::NormalMatch,
                             QRegularExpression::AnchoredMatchOption);
            if (r.hasMatch() && r.capturedLength() == line.length()) {
                fileName = r.captured(4);

                if (r.captured(1) == "-") {
                    if (directories)
                        continue;
                } else if (r.captured(1) == "l") {
                    /* Always accept symlinks */

                    int idx = fileName.indexOf(" ->");
                    if (idx != -1)
                        fileName.resize(idx);

                    isSymlink = true;
                } else {
                    if (!directories)
                        continue;
                }
                bool ok = false;
                size = r.captured(2).toULongLong(&ok);
                if (!ok)
                    continue;

                mtime = parseUnixTime(r.captured(3));
            } else {
                qCDebug(log_transfer_ftp) << "Unknown LIST response line format:" << line;
                error = "Unrecognized list format: " + line.toStdString();
                continue;
            }
        }

        if (fileName.isEmpty())
            continue;

        anyValidLines = true;

        /* Try to read the exact date, since the timestamping is unreliable at best */
        if (!directories || isSymlink) {
            QString fullName = QString::fromStdString(pathName);
            if (!fullName.isEmpty() && !fullName.endsWith('/'))
                fullName += '/';
            fullName += fileName;

            commandConnection->write("MDTM " + fullName.toStdString() + "\r\n");
            auto resp = nextCommandResponse(nextCommandTimeout());
            if (!resp.first)
                return false;
            if (resp.first / 100 == 2) {
                /* Got a time and it's a symlink, so it's probably not a directory */
                if (isSymlink && directories)
                    continue;
                double checkTime = parseResponseTime(resp.second);
                if (FP::defined(checkTime))
                    mtime = checkTime;
            } else if (resp.first / 100 == 5) {
                /* If we got a 500 error and it's a symlink then it probably
                     * points to a directory */
                if (isSymlink && !directories)
                    continue;
            }
        }

        if (!directories && isSymlink) {
            QString fullName = QString::fromStdString(pathName);
            if (!fullName.isEmpty() && !fullName.endsWith('/'))
                fullName += '/';
            fullName += fileName;

            commandConnection->write("SIZE " + fullName.toStdString() + "\r\n");
            auto resp = nextCommandResponse(nextCommandTimeout());
            if (!resp.first)
                return false;

            if (resp.first / 100 == 2) {
                try {
                    size = std::stoull(resp.second);
                } catch (std::exception &) {
                }
            }
        }

        result.emplace_back(File(fileName.toStdString(), pathName, size, mtime));
    }

    if (!anyLines)
        return true;
    return anyValidLines;
}


bool FTPConnection::get(const std::string &source, IO::Generic::Writable &target)
{
    int tryLimit = resumeRetries + 1;
    switch (transferMode) {
    case TransferMode::PassiveThenActive:
    case TransferMode::ActiveThenPassive:
        tryLimit *= 2;
        break;
    case TransferMode::Passive:
    case TransferMode::Active:
        break;
    }

    auto absoluteTimeout = totalTransferTimeout();

    byteCounter = 0;
    std::uint_fast64_t expectedSize = 0;
    bool dataConnectionPolarity = false;
    bool haveSize = false;
    for (int t = 0; t < tryLimit; ++t) {
        if (absoluteTimeout < Clock::now()) {
            error = "Maximum transfer timeout exceeded";
            qCDebug(log_transfer_ftp) << "Maximum transfer timeout exceeded during get";
            return false;
        }

        if (!commandConnection) {
            if (!connectToServer())
                continue;
        }

        if (!haveSize) {
            commandConnection->write("SIZE " + source + "\r\n");
            auto resp = nextCommandResponse(nextCommandTimeout());
            if (resp.first) {
                haveSize = true;
                if (resp.first / 100 == 2) {
                    try {
                        expectedSize = std::stoull(resp.second);
                    } catch (std::exception &) {
                        expectedSize = 0;
                    }
                }
            }
        }

        if (!prepareDataConnection(dataConnectionPolarity)) {
            if (closeOnDataFailure) {
                disconnectFromServer();
            }
            dataConnectionPolarity = !dataConnectionPolarity;
            continue;
        }

        bool needToResetDownload = byteCounter != 0;
        if (needToResetDownload && resumeDownload) {
            commandConnection->write("REST " + std::to_string(byteCounter) + "\r\n");
            int rc = nextCommandCode(nextCommandTimeout());
            if (rc / 100 == 3) {
                needToResetDownload = false;
                qCDebug(log_transfer_ftp) << "Resuming download of" << source << "at byte"
                                          << byteCounter;
            }
        }
        if (needToResetDownload) {
            auto seekable = dynamic_cast<IO::Generic::Block *>(&target);
            if (!seekable) {
                error = "Unable to reset stream only destination";
                qCDebug(log_transfer_ftp) << "The target needs to be reset but it is not seekable";
                return false;
            }
            seekable->seek(0);
            byteCounter = 0;
            qCDebug(log_transfer_ftp) << "Restarting download of" << source << "from the beginning";
        }

        commandConnection->write("RETR " + source + "\r\n");
        if (!establishDataConnection(dataConnectionEstablishTimeout(absoluteTimeout))) {
            if (closeOnDataFailure) {
                disconnectFromServer();
            }
            dataConnectionPolarity = !dataConnectionPolarity;
            continue;
        }

        int rc = nextCommandCode(nextCommandTimeout());
        if (!rc) {
            commandConnection.reset();
            commandConnectionEnded = true;
            continue;
        }

        if (rc / 100 != 1) {
            error = "Invalid response to RETR: " + std::to_string(rc);
            qCDebug(log_transfer_ftp) << "Invalid response to RETR" << rc;
            commandConnection.reset();
            commandConnectionEnded = true;
            dataConnectionPolarity = !dataConnectionPolarity;
            continue;
        }

        if (!downloadData(target, absoluteTimeout, expectedSize)) {
            if (closeOnDataFailure) {
                disconnectFromServer();
            }
            dataConnectionPolarity = !dataConnectionPolarity;
            continue;
        }

        rc = nextCommandCode(absoluteTimeout);
        if (rc / 100 != 2) {
            continue;
        }

        return true;
    }

    return false;
}

bool FTPConnection::get(const std::string &source, IO::Generic::Backing &target)
{
    {
        auto block = target.block();
        if (block)
            return get(source, *block);
    }
    auto stream = target.stream();
    if (!stream)
        return false;
    return get(source, *stream);
}

bool FTPConnection::get(const std::string &source, Util::ByteArray &target)
{
    IO::Generic::Block::ByteArray wrapped(target);
    return get(source, wrapped);
}

bool FTPConnection::get(const std::string &source, QByteArray &target)
{
    Util::ByteArray temp;
    if (!get(source, temp))
        return false;
    target = temp.toQByteArray();
    return true;
}

bool FTPConnection::get(const std::string &source, QIODevice *target)
{
    /* Needs to be run somewhere with an event loop, and we don't run one */
    Q_ASSERT(target->thread() != QThread::currentThread());

    bool useStream = target->isSequential();
    auto handle = IO::Access::qio(target);
    if (!handle)
        return false;

    if (useStream) {
        auto stream = handle->stream();
        if (!stream)
            return false;
        return get(source, *stream);
    }

    auto block = handle->block();
    if (!block)
        return false;
    return get(source, *block);
}


bool FTPConnection::put(const std::string &target,
                        const std::function<std::unique_ptr<
                                IO::Generic::Stream>(std::uint_fast64_t offset)> &createSource,
                        std::uint_fast64_t expectedSize)
{
    int tryLimit = resumeRetries + 1;
    switch (transferMode) {
    case TransferMode::PassiveThenActive:
    case TransferMode::ActiveThenPassive:
        tryLimit *= 2;
        break;
    case TransferMode::Passive:
    case TransferMode::Active:
        break;
    }

    auto absoluteTimeout = totalTransferTimeout();

    byteCounter = 0;
    bool dataConnectionPolarity = false;
    for (int t = 0; t < tryLimit; ++t) {
        if (absoluteTimeout < Clock::now()) {
            error = "Maximum transfer timeout exceeded";
            qCDebug(log_transfer_ftp) << "Maximum transfer timeout exceeded during get";
            return false;
        }

        if (!commandConnection) {
            if (!connectToServer())
                continue;
        }

        if (!prepareDataConnection(dataConnectionPolarity)) {
            if (closeOnDataFailure) {
                disconnectFromServer();
            }
            dataConnectionPolarity = !dataConnectionPolarity;
            continue;
        }

        std::unique_ptr<IO::Generic::Stream> source;
        auto resumeByte = byteCounter;
        byteCounter = 0;
        if (resumeByte != 0 && resumeUpload) {
            commandConnection->write("SIZE " + target + "\r\n");
            auto resp = nextCommandResponse(nextCommandTimeout());
            if (resp.first / 100 == 2) {
                try {
                    resumeByte = std::stoull(resp.second);
                } catch (std::exception &) {
                    resumeByte = 0;
                }
            }

            source = createSource(resumeByte);
            if (source && resumeByte != 0) {
                commandConnection->write("REST " + std::to_string(resumeByte) + "\r\n");
                int rc = nextCommandCode(nextCommandTimeout());
                if (rc / 100 == 3) {
                    byteCounter = resumeByte;
                    qCDebug(log_transfer_ftp) << "Resuming upload of" << target << "at byte"
                                              << byteCounter;
                } else {
                    source.reset();
                }
            }
        }
        if (!source) {
            source = createSource(byteCounter);
        }
        if (!source) {
            error = "Unable to position data source";
            qCDebug(log_transfer_ftp) << "Unable to create a data source at position"
                                      << byteCounter;
            return false;
        }

        commandConnection->write("STOR " + target + "\r\n");
        if (!establishDataConnection(dataConnectionEstablishTimeout(absoluteTimeout))) {
            if (closeOnDataFailure) {
                disconnectFromServer();
            }
            dataConnectionPolarity = !dataConnectionPolarity;
            continue;
        }

        int rc = nextCommandCode(nextCommandTimeout());
        if (!rc) {
            commandConnection.reset();
            commandConnectionEnded = true;
            continue;
        }

        if (rc / 100 != 1) {
            error = "Invalid response to STOR: " + std::to_string(rc);
            qCDebug(log_transfer_ftp) << "Invalid response to STOR" << rc;
            commandConnection.reset();
            commandConnectionEnded = true;
            dataConnectionPolarity = !dataConnectionPolarity;
            continue;
        }

        if (!uploadData(std::move(source), absoluteTimeout, expectedSize)) {
            if (closeOnDataFailure) {
                disconnectFromServer();
            }
            dataConnectionPolarity = !dataConnectionPolarity;
            continue;
        }

        rc = nextCommandCode(absoluteTimeout);
        if (rc / 100 != 2) {
            continue;
        }

        return true;
    }

    return false;
}

bool FTPConnection::put(const std::string &target,
                        std::unique_ptr<IO::Generic::Stream> &&source,
                        std::uint_fast64_t expectedSize)
{
    return put(target, [&source](std::uint_fast64_t offset) {
        if (offset != 0)
            return std::unique_ptr<IO::Generic::Stream>();
        return std::move(source);
    }, expectedSize);
}

std::size_t FTPConnection::streamChunkSize() const
{
    std::size_t chunkSize = 65536;
    if (uploadRateLimit > 0) {
        auto limit = std::max<std::size_t>(1024, uploadRateLimit / 2);
        if (chunkSize > limit)
            chunkSize = uploadRateLimit / 2;
    }
    return chunkSize;
}

bool FTPConnection::put(const std::string &target,
                        IO::Generic::Block &source,
                        std::uint_fast64_t expectedSize)
{
    return put(target, [this, &source](std::uint_fast64_t offset) {
        source.seek(offset);
        return std::unique_ptr<IO::Generic::Stream>(
                new IO::Generic::Block::StreamReader(source, streamChunkSize()));
    }, expectedSize);
}

bool FTPConnection::put(const std::string &target,
                        IO::Generic::Backing &source,
                        std::uint_fast64_t expectedSize)
{
    if (expectedSize == static_cast<std::uint_fast64_t>(-1)) {
        expectedSize = 0;
        if (auto file = dynamic_cast<IO::File::Backing *>(&source)) {
            expectedSize = file->size();
        }
    }

    auto block = source.block();
    if (block) {
        return put(target, [this, &block](std::uint_fast64_t offset) {
            block->seek(offset);
            return std::unique_ptr<IO::Generic::Stream>(
                    new IO::Generic::Block::StreamReader(*block, streamChunkSize()));
        }, expectedSize);
    }

    auto stream = source.stream();
    if (!stream)
        return false;
    return put(target, [&stream](std::uint_fast64_t offset) {
        if (offset != 0)
            return std::unique_ptr<IO::Generic::Stream>();
        return std::move(stream);
    }, expectedSize);
}

bool FTPConnection::put(const std::string &target, const Util::ByteArray &source)
{
    return put(target, [&source](std::uint_fast64_t offset) {
        return std::unique_ptr<IO::Generic::Stream>(
                new IO::Generic::Stream::ByteView(source.mid(offset)));
    }, source.size());
}

bool FTPConnection::put(const std::string &target, const QByteArray &source)
{
    Util::ByteView wrapped(source);
    return put(target, [&wrapped](std::uint_fast64_t offset) {
        return std::unique_ptr<IO::Generic::Stream>(
                new IO::Generic::Stream::ByteView(wrapped.mid(offset)));
    }, source.size());
}

bool FTPConnection::put(const std::string &target,
                        QIODevice *source,
                        std::uint_fast64_t expectedSize)
{
    /* Needs to be run somewhere with an event loop, and we don't run one */
    Q_ASSERT(source->thread() != QThread::currentThread());

    if (expectedSize == static_cast<std::uint_fast64_t>(-1)) {
        expectedSize = 0;
        {
            auto n = source->size();
            if (n > 0) {
                expectedSize = n;
            }
        }
    }

    bool useStream = source->isSequential();
    auto handle = IO::Access::qio(source);
    if (!handle)
        return false;

    if (useStream) {
        auto stream = handle->stream();
        if (!stream)
            return false;
        return put(target, [&stream](std::uint_fast64_t offset) {
            if (offset != 0)
                return std::unique_ptr<IO::Generic::Stream>();
            return std::move(stream);
        }, expectedSize);
    }

    auto block = handle->block();
    if (!block)
        return false;

    return put(target, [this, &block](std::uint_fast64_t offset) {
        block->seek(offset);
        return std::unique_ptr<IO::Generic::Stream>(
                new IO::Generic::Block::StreamReader(*block, streamChunkSize()));
    }, expectedSize);
}

void FTPConnection::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    notify.notify_all();
}

}
}
