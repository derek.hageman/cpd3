/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QDateTime>

#include "transfer/email.hxx"
#include "core/timeutils.hxx"
#include "transfer/smtp.hxx"

namespace CPD3 {
namespace Transfer {

/** @file transfer/email.hxx
 * Email building routines.
 */



EmailBuilder::EmailBuilder()
        : headers(),
          escapeMode(Escape_UTF8),
          messageBody(),
          bodyMimeType("text/plain"),
          to(),
          cc(),
          bcc(),
          from(),
          attachments()
{
    setTime();
}

void EmailBuilder::setEscapeMode(EscapeMode mode)
{ escapeMode = mode; }

void EmailBuilder::setHeader(const QString &key, const QString &value)
{ headers.insert(key, value); }

void EmailBuilder::setTime(double time)
{
    if (!FP::defined(time))
        time = Time::time();
    setHeader("Date", Time::toDateTime(time, true).toString("ddd, d MMM yyyy hh:mm:ss '-0000'"));
}

void EmailBuilder::setSubject(const QString &subject)
{ setHeader("Subject", subject); }

void EmailBuilder::addTo(const QString &address)
{ to.append(address); }

void EmailBuilder::addCC(const QString &address)
{ cc.append(address); }

void EmailBuilder::addBCC(const QString &address)
{ bcc.append(address); }

void EmailBuilder::addFrom(const QString &address)
{ from.append(address); }

void EmailBuilder::setBody(const QString &body, const QString &mimeType)
{
    messageBody = body;
    bodyMimeType = mimeType;
    if (bodyMimeType.isEmpty())
        bodyMimeType = "text/plain";
}

void EmailBuilder::addAttachment(const QByteArray &data,
                                 const QString &fileName,
                                 const QString &mimeType,
                                 const QString &contentID)
{
    Attachment add;
    add.data = data;
    add.fileName = fileName;
    add.contentID = contentID;
    add.mimeType = mimeType;
    attachments.append(add);
}

void EmailBuilder::queue(SMTPSend *target) const
{
    if (to.isEmpty() && cc.isEmpty() && bcc.isEmpty())
        return;
    QByteArray contents(construct());
    if (contents.isEmpty())
        return;
    if (!from.isEmpty())
        target->setFrom(from.first().toStdString(), false);
    target->addMessage(QStringList(to) << cc << bcc, contents, false);
}

bool EmailBuilder::anyContentsContainBoundary(const QByteArray &boundary) const
{
    if (messageBody.contains(QString::fromLatin1(boundary)))
        return true;
    for (QList<Attachment>::const_iterator check = attachments.constBegin(),
            endCheck = attachments.constEnd(); check != endCheck; ++check) {
        if (check->data.contains(boundary))
            return true;
    }
    return false;
}

static void insertListHeader(QHash<QByteArray, QByteArray> &effectiveHeaders,
                             const QByteArray &target,
                             const QStringList &contents)
{
    if (contents.isEmpty())
        return;
    effectiveHeaders.insert(target, contents.join(", ").toUtf8());
}

static void takeOutputHeader(QByteArray &output,
                             QHash<QByteArray, QByteArray> &effectiveHeaders,
                             const QByteArray &key)
{
    QByteArray value(effectiveHeaders.take(key));
    if (value.isEmpty())
        return;
    output.append(key);
    output.append(": ");
    output.append(value);
    output.append("\r\n");
}

static void appendBase64(QByteArray &output, const QByteArray &base64)
{
    output.reserve(output.size() + base64.size() + ((int) (base64.size() / 70)) * 2);

    const char *ptr = base64.constData();
    int remaining = base64.size();
    while (remaining > 0) {
        int take = qMin(70, remaining);
        int offset = output.size();
        output.resize(offset + take);
        memcpy(output.data() + offset, ptr, take);
        ptr += take;
        remaining -= take;

        if (remaining > 0)
            output.append("\r\n");
    }
}

QByteArray EmailBuilder::construct() const
{
    if (messageBody.isEmpty() && attachments.isEmpty() && !headers.contains("Subject"))
        return QByteArray();

    QHash<QByteArray, QByteArray> effectiveHeaders;
    for (QHash<QString, QString>::const_iterator add = headers.constBegin(),
            endAdd = headers.constEnd(); add != endAdd; ++add) {
        effectiveHeaders.insert(add.key().toUtf8(), add.value().toUtf8());
    }
    insertListHeader(effectiveHeaders, QByteArray("To"), to);
    insertListHeader(effectiveHeaders, QByteArray("CC"), cc);
    insertListHeader(effectiveHeaders, QByteArray("From"), from);

    effectiveHeaders.remove(QByteArray("MIME-Version"));
    effectiveHeaders.remove(QByteArray("Content-Type"));

    QByteArray output;
    takeOutputHeader(output, effectiveHeaders, QByteArray("From"));
    takeOutputHeader(output, effectiveHeaders, QByteArray("To"));
    takeOutputHeader(output, effectiveHeaders, QByteArray("CC"));
    for (QHash<QByteArray, QByteArray>::const_iterator header = effectiveHeaders.constBegin(),
            endHeaders = effectiveHeaders.constEnd(); header != endHeaders; ++header) {
        if (header.value().isEmpty())
            continue;
        output.append(header.key());
        output.append(": ");
        output.append(header.value());
        output.append("\r\n");
    }

    QByteArray boundaryMarker;
    for (int ittr = 0; ittr < 1000; ++ittr) {
        boundaryMarker = "ZzCPD3-";
        boundaryMarker.append(Random::string());
        if (!anyContentsContainBoundary(boundaryMarker))
            break;
    }

    bool usingMultipart = !attachments.isEmpty();
    output.append("MIME-Version: 1.0\r\n");
    if (!usingMultipart) {
        output.append("Content-Type: ");
        output.append(bodyMimeType.toLatin1());
        output.append("\r\n");
    } else {
        output.append("Content-Type: multipart/related; boundary=\"");
        output.append(boundaryMarker);
        output.append("\"\r\n");
    }
    boundaryMarker.prepend("\r\n--");
    boundaryMarker.append("\r\n");

    QByteArray encodedBody;
    bool bodyIs7Bit = true;
    if (!messageBody.isEmpty()) {
        encodedBody.reserve(messageBody.size());

        int lineLength = 0;
        for (QString::const_iterator msgChar = messageBody.constBegin(),
                endMsg = messageBody.constEnd(); msgChar != endMsg; ++msgChar) {
            /* RFC 821 limits line length to 1000 characters but some MTAs
             * limit it more */
            if (lineLength > 800) {
                int checkIndex = encodedBody.size() - 1;
                for (const char *inspect = encodedBody.constData() + checkIndex;
                        lineLength > 0;
                        --lineLength, --inspect) {
                    Q_ASSERT(checkIndex >= 0);
                    if (*inspect != ' ')
                        continue;
                    encodedBody.replace(checkIndex, 1, " \r\n");
                    lineLength = 0;
                    break;
                }
                if (lineLength <= 0) {
                    encodedBody.append("\r\n");
                    lineLength = 0;
                }
            }

            quint32 uc = msgChar->unicode();

            /* Fits in 7bit ASCII, so no translation needed */
            if (uc <= 0x7F) {
                if (uc == '\r' || uc == '\n')
                    lineLength = 0;
                else
                    ++lineLength;

                encodedBody.append((char) uc);
                continue;
            }

            switch (escapeMode) {
            case Escape_UTF8: {
                QByteArray add(QString(*msgChar).toUtf8());
                bodyIs7Bit = false;
                encodedBody.append(add);
                lineLength += add.length();
                break;
            }
            case Escape_HTML: {
                QByteArray add("&#x");
                add.append(QByteArray::number(uc, 16).toUpper());
                add.append(';');
                encodedBody.append(add);
                lineLength += add.length();
                break;
            }
            }
        }
    }

    if (!encodedBody.isEmpty()) {
        if (usingMultipart) {
            output.append(boundaryMarker);
            output.append("Content-Type: ");
            output.append(bodyMimeType.toLatin1());
            output.append("\r\n");
        }

        if (bodyIs7Bit) {
            output.append("Content-Transfer-Encoding: 7bit\r\n");
        } else {
            output.append("Content-Transfer-Encoding: base64\r\n");
        }

        output.append("\r\n");
        if (bodyIs7Bit) {
            output.append(encodedBody);
        } else {
            appendBase64(output, encodedBody.toBase64());
        }
    } else {
        if (!usingMultipart) {
            output.append("\r\n");
        }
    }

    for (QList<Attachment>::const_iterator add = attachments.constBegin(),
            endAdd = attachments.constEnd(); add != endAdd; ++add) {
        Q_ASSERT(usingMultipart);

        output.append(boundaryMarker);
        if (!add->mimeType.isEmpty()) {
            output.append("Content-Type: ");
            output.append(add->mimeType.toLatin1());
            output.append("\r\n");
        }
        if (!add->contentID.isEmpty()) {
            output.append("Content-ID: ");
            output.append(add->contentID.toLatin1());
            output.append("\r\n");
        }
        if (!add->fileName.isEmpty()) {
            output.append("Content-Disposition: attachment; filename=\"");
            output.append(add->fileName.toLatin1());
            output.append("\";\r\n");
        }
        output.append("Content-Transfer-Encoding: base64\r\n\r\n");

        appendBase64(output, add->data.toBase64());
    }

    /* Final boundary marker */
    if (usingMultipart) {
        output.append(boundaryMarker);
        output.remove(output.size() - 2, 2);
        output.append("--\r\n");
    }

    return output;
}


}
}
