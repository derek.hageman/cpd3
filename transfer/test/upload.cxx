/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <memory>
#include <QObject>
#include <QTest>
#include <QCryptographicHash>
#include <QHostInfo>
#include <QFile>
#include <QTcpServer>
#include <QTcpSocket>
#include <QLoggingCategory>
#include <QTemporaryFile>

#include "transfer/upload.hxx"
#include "core/number.hxx"
#include "core/waitutils.hxx"
#include "core/threadpool.hxx"
#include "core/qtcompat.hxx"

Q_LOGGING_CATEGORY(log_test, "cpd3.test")

using namespace CPD3;
using namespace CPD3::Transfer;
using namespace CPD3::Data;

class TestUploader : public FileUploader {
    std::unique_ptr<int> index;
    QString temporaryFile;
    QString inputFile;
public:
    TestUploader(const Variant::Read &config, QFile *file) : FileUploader(config),
                                                             index(new int(0))
    {
        addUpload(IO::Access::file(file->fileName(), IO::File::Mode::readOnly()));
    }

    TestUploader(const Variant::Read &config, const QByteArray &data) : FileUploader(config),
                                                                        index(new int(0))
    {
        addUpload(IO::Access::buffer(Util::ByteArray(data))->stream());
    }

    TestUploader(const Variant::Read &config, const QList<QByteArray> &data) : FileUploader(config),
                                                                               index(new int(0))
    {
        for (const auto &add : data) {
            addUpload(IO::Access::buffer(Util::ByteArray(add)));
        }
    }

protected:
    virtual QString applySubstitutions(const QString &input) const
    {
        QString result(input);
        result.replace("TEMP", temporaryFile);
        result.replace("INPUT", inputFile);
        if (result.contains("INDEX")) {
            (*index)++;
            result.replace("INDEX", QString::number(*index));
        }
        return result;
    }

    virtual void pushTemporaryOutput(const QFileInfo &fileName)
    {
        temporaryFile = fileName.absoluteFilePath();
    }

    virtual void popTemporaryOutput()
    {
        temporaryFile = QString();
    }

    virtual void pushInputFile(const QFileInfo &file)
    {
        inputFile = file.absoluteFilePath();
    }

    virtual void popInputFile()
    {
        inputFile = QString();
    }
};

class TestFTPManager : public QRunnable {
    int port;

    std::mutex mutex;

    QString waitForNextCommand(QTcpSocket *socket)
    {
        ElapsedTimer to;
        to.start();
        QString result;
        for (; ;) {
            int remaining = 30000 - to.elapsed();
            if (remaining <= 0) {
                qCDebug(log_test) << "Command connection timeout";
                return QString();
            }
            QEventLoop el;
            QTimer timer;
            QObject::connect(&timer, SIGNAL(timeout()), &el, SLOT(quit()), Qt::QueuedConnection);
            QObject::connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), &el,
                             SLOT(quit()), Qt::QueuedConnection);
            QObject::connect(socket, SIGNAL(readyRead()), &el, SLOT(quit()), Qt::QueuedConnection);
            if (socket->bytesAvailable() <= 0) {
                if (socket->state() != QAbstractSocket::ConnectedState) {
                    qCDebug(log_test) << "Command connection closed:" <<
                                      socket->errorString();
                    break;
                }
                timer.start(remaining);
                el.exec();
                continue;
            }

            char c = 0;
            if (!socket->getChar(&c))
                return QString();
            if (c == '\n' || c == '\r') {
                if (!result.isEmpty())
                    return result;
                continue;
            }
            result.append(c);
        }
        return QString();
    }

    bool writeData(QTcpSocket *socket, const QByteArray &data)
    {
        ElapsedTimer to;
        to.start();

        int offset = 0;
        while (offset < data.size()) {
            int remaining = 30000 - to.elapsed();
            if (remaining <= 0)
                return false;
            qint64 n = socket->write(data.constData() + offset, data.size() - offset);
            if (n < 0)
                return false;
            offset += (int) n;
        }
        socket->flush();

        while (socket->bytesToWrite() > 0) {
            if (socket->state() == QAbstractSocket::UnconnectedState)
                break;
            int remaining = 30000 - to.elapsed();
            if (remaining <= 0)
                return false;
            socket->waitForBytesWritten(remaining);
        }

        return true;
    }

    QByteArray readAllData(QTcpSocket *socket)
    {
        ElapsedTimer to;
        to.start();
        QByteArray result;
        static const int blockSize = 65536;
        for (; ;) {
            int remaining = 30000 - to.elapsed();
            if (remaining <= 0)
                return result;
            int original = result.size();
            result.resize(original + blockSize);
            qint64 n = socket->read(result.data() + original, blockSize);
            if (n > 0) {
                result.resize(original + (int) n);
                continue;
            }
            result.resize(original);
            if (socket->bytesAvailable() > 0)
                continue;

            if (socket->state() == QAbstractSocket::UnconnectedState)
                return result;
            socket->waitForReadyRead(remaining);
        }
        return result;
    }

    QTcpSocket *commandSocket;
    QString dataHost;
    quint16 dataPort;

    QTcpSocket *makeDataConnection(QTcpServer &server)
    {
        if (dataHost.isEmpty()) {
            qCDebug(log_test) << "Waiting for data connection";

            if (!server.hasPendingConnections() && !server.waitForNewConnection(30000)) {
                qCDebug(log_test) << "No connection received";
                writeData(commandSocket, QByteArray("425 Connection failed"));
                return NULL;
            }
            QTcpSocket *dataSocket = server.nextPendingConnection();
            if (!dataSocket) {
                qCDebug(log_test) << "Connection failed";
                writeData(commandSocket, QByteArray("425 Connection failed"));
                return NULL;
            }
            if (!dataSocket->waitForConnected(30000)) {
                qCDebug(log_test) << "Connection failed:" << dataSocket->errorString();
                dataSocket->deleteLater();
                writeData(commandSocket, QByteArray("425 Connection failed ") +
                        dataSocket->errorString().toUtf8() +
                        "\n");
                return NULL;
            }
            qCDebug(log_test) << "Data connection opened";

            return dataSocket;
        } else {
            /* This doesn't currently work because the FTP client wants
             * a connection from the server port - 1, which we can't
             * bind explicitly via Qt's API. */

            qCDebug(log_test) << "Initiating data connection to" <<
                              dataHost <<
                              ":" <<
                              dataPort;

            QTcpSocket *dataSocket = new QTcpSocket(&server);
            dataSocket->connectToHost(dataHost, dataPort);
            if (!dataSocket->waitForConnected(30000)) {
                writeData(commandSocket, QByteArray("425 Connection failed ") +
                        dataSocket->errorString().toUtf8() +
                        "\n");
                return NULL;
            }
            qCDebug(log_test) << "Data connection opened";

            return dataSocket;
        }
    }

    bool receiveData(QByteArray &target, QTcpServer &server)
    {
        QTcpSocket *dataSocket = makeDataConnection(server);
        if (dataSocket == NULL)
            return false;

        target = readAllData(dataSocket);
        qCDebug(log_test) << "Received" << target.size() << "bytes";

        dataSocket->disconnectFromHost();
        dataSocket->deleteLater();
        return true;
    }

    bool sendData(const QByteArray &source, QTcpServer &server)
    {
        QTcpSocket *dataSocket = makeDataConnection(server);
        if (dataSocket == NULL)
            return false;

        if (!writeData(dataSocket, source)) {
            writeData(commandSocket,
                      QByteArray("425 Write failed ") + dataSocket->errorString().toUtf8() + "\n");
            return false;
        }
        qCDebug(log_test) << "Transmitted" << source.size() << "bytes";

        dataSocket->disconnectFromHost();
        if (dataSocket->state() != QAbstractSocket::UnconnectedState)
            dataSocket->waitForDisconnected(30000);
        dataSocket->deleteLater();
        return true;
    }

public:
    QSet<QString> directories;
    QString receivedName;
    QByteArray receivedData;
    bool haltOnReceive;

    TestFTPManager() : port(0)
    {
        setAutoDelete(false);
        haltOnReceive = false;
    }

    virtual void run()
    {
        qRegisterMetaType<QAbstractSocket::SocketState>("QAbstractSocket::SocketState");

        QTcpServer server;

        if (!server.listen(QHostAddress::LocalHost)) {
            std::lock_guard<std::mutex> lock(mutex);
            port = -1;
            return;
        }

        {
            std::lock_guard<std::mutex> lock(mutex);
            port = server.serverPort();
        }

        qCDebug(log_test) << "Server listening on port" << port;

        if (!server.waitForNewConnection(30000)) {
            std::lock_guard<std::mutex> lock(mutex);
            port = -1;
            return;
        }

        commandSocket = server.nextPendingConnection();
        if (!commandSocket) {
            std::lock_guard<std::mutex> lock(mutex);
            port = -1;
            return;
        }
        if (!commandSocket->waitForConnected(30000)) {
            std::lock_guard<std::mutex> lock(mutex);
            port = -1;
            return;
        }

        writeData(commandSocket, QByteArray("220 OK\n"));
        qCDebug(log_test) << "Command connection accepted";

        dataHost = QString();
        dataPort = 0;

        ElapsedTimer to;
        to.start();
        while (to.elapsed() < 30000) {
            QString command(waitForNextCommand(commandSocket));
            if (command.isEmpty())
                break;
            qCDebug(log_test) << "Received command" << command;

            if (command.startsWith("USER ", Qt::CaseInsensitive)) {
                writeData(commandSocket, QByteArray("331 OK\n"));
                continue;
            } else if (command.startsWith("PASS ", Qt::CaseInsensitive)) {
                writeData(commandSocket, QByteArray("230 OK\n"));
                continue;
            } else if (command.startsWith("TYPE ", Qt::CaseInsensitive)) {
                writeData(commandSocket, QByteArray("200 OK\n"));
                continue;
            } else if (command.startsWith("ALLO ", Qt::CaseInsensitive)) {
                writeData(commandSocket, QByteArray("200 OK\n"));
                continue;
            } else if (command.startsWith("HELP", Qt::CaseInsensitive)) {
                writeData(commandSocket, QByteArray("200 OK\n"));
                continue;
            } else if (command.startsWith("PORT ", Qt::CaseInsensitive)) {
                QString data(command.mid(5));
                data.replace('(', QString());
                data.replace(')', QString());
                QStringList fields(data.split(','));
                if (fields.size() < 6) {
                    writeData(commandSocket, QByteArray("500 Insufficient parameters\n"));
                    continue;
                }
                dataPort = fields.at(5).toInt();
                dataPort += fields.at(4).toInt() * 256;
                dataHost = QStringList(fields.mid(0, 4)).join(".");
                writeData(commandSocket, QByteArray("200 OK\n"));
                continue;
            } else if (command.startsWith("PASV", Qt::CaseInsensitive)) {
                dataHost.clear();
                dataPort = 0;
                writeData(commandSocket, QByteArray("227 Entering Passive Mode (127,0,0,1,") +
                        QByteArray::number(port / 256) +
                        "," +
                        QByteArray::number(port % 256) +
                        ")\n");
                continue;
            } else if (command.startsWith("MKD ", Qt::CaseInsensitive)) {
                QString name(command.mid(4));
                QString parent(name.section('/', 0, -2));
                bool valid = parent.isEmpty();
                for (QSet<QString>::const_iterator dir = directories.constBegin(),
                        end = directories.constEnd(); dir != end; ++dir) {
                    if (dir->startsWith(name)) {
                        valid = false;
                        break;
                    }
                    if (*dir == parent)
                        valid = true;
                }
                if (!valid) {
                    writeData(commandSocket, QByteArray("550 Create directory operation failed\n"));
                    qCDebug(log_test) << "Directory" << name << "NOT created";
                } else {
                    writeData(commandSocket,
                              QByteArray("257 \"") + name.toUtf8() + "\" directory created\n");
                    directories.insert(name);
                    qCDebug(log_test) << "Directory" << name << "created";
                }
                continue;
            } else if (command.startsWith("STOR ", Qt::CaseInsensitive)) {
                QString name(command.mid(5));

                qCDebug(log_test) << "Receiving" << name;

                writeData(commandSocket,
                          QByteArray("150 Opening connection for ") + name.toUtf8() + "\n");

                QByteArray data;
                if (!receiveData(data, server))
                    continue;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    receivedName = name;
                    receivedData = data;
                }
                writeData(commandSocket, QByteArray("226 Completed\n"));

                if (haltOnReceive)
                    break;
                continue;
            } else if (command.startsWith("QUIT", Qt::CaseInsensitive)) {
                writeData(commandSocket, QByteArray("221 Goodbye.\n"));
                commandSocket->disconnectFromHost();
                break;
            } else if (command.startsWith("NOOP", Qt::CaseInsensitive)) {
                writeData(commandSocket, QByteArray("200 OK\n"));
                break;
            }

            writeData(commandSocket, QByteArray("502 Not implemented\n"));
        }

        if (commandSocket)
            commandSocket->close();
        server.close();

        qCDebug(log_test) << "Server completing";
        std::lock_guard<std::mutex> lock(mutex);
        port = -2;
    }

    int waitForPort()
    {
        for (; ;) {
            QThread::yieldCurrentThread();
            std::lock_guard<std::mutex> lock(mutex);
            if (port == 0)
                continue;
            if (port < 0)
                return -1;
            return port;
        }
    }

    void waitForCompleted()
    {
        for (; ;) {
            QThread::yieldCurrentThread();
            std::lock_guard<std::mutex> lock(mutex);
            if (port < 0)
                return;
        }
    }
};

class TestUpload : public QObject {
Q_OBJECT

    QString tmpPath;
    QDir tmpDir;

    void recursiveRemove(const QDir &base)
    {
        if (base.path().isEmpty() || base == QDir::current())
            return;
        QFileInfoList list(base.entryInfoList(QDir::Files | QDir::NoDotAndDotDot));
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            if (!QFile::remove(rm->absoluteFilePath())) {
                qDebug() << "Failed to remove file" << rm->absoluteFilePath();
                continue;
            }
        }
        list = base.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            QDir subDir(base);
            if (!subDir.cd(rm->fileName())) {
                qDebug() << "Failed to change directory" << rm->absoluteFilePath();
                continue;
            }
            recursiveRemove(subDir);
            base.rmdir(rm->fileName());
        }
    }

    bool populateTemp(const QString &name)
    {
        QFile file(name);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate))
            return false;
        file.write(name.toUtf8());
        file.close();
        return true;
    }

    static QByteArray takeLine(QByteArray &buffer)
    {
        int idx = buffer.indexOf('\n');
        if (idx < 0)
            return buffer;
        QByteArray result(buffer.mid(0, idx));
        buffer = buffer.mid(idx + 1);
        return result;
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        tmpDir = QDir(QDir::tempPath());
        tmpPath = "CPD3Upload-";
        tmpPath.append(Random::string());
        tmpDir.mkdir(tmpPath);
        QVERIFY(tmpDir.cd(tmpPath));
    }

    void cleanupTestCase()
    {
        recursiveRemove(tmpDir);
        tmpDir.cdUp();
        tmpDir.rmdir(tmpPath);
    }

    void init()
    {
        recursiveRemove(tmpDir);
    }

#ifdef Q_OS_UNIX

    void actions()
    {
        QTemporaryFile temp1;
        QVERIFY(temp1.open());
        QTemporaryFile temp2;
        QVERIFY(temp2.open());

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "Command";
        config["Actions/Program"] = "bash";
        config["Actions/Arguments/#0"] = "-c";
        config["Actions/Arguments/#1"] = "cat INPUTTEMP >> '" +
                temp2.fileName() +
                "'; echo RunINPUTTEMP >> '" +
                temp2.fileName() +
                "'";

        QVERIFY(populateTemp(temp1.fileName()));

        {
            QFile file(temp1.fileName());
            TestUploader upload(config, &file);
            QVERIFY(upload.exec());
            QByteArray check(temp2.readAll());

            QCOMPARE(takeLine(check),
                     temp1.fileName().toUtf8() + "Run" + temp1.fileName().toUtf8());
            QVERIFY(check.isEmpty());
        }

        temp2.resize(0);
        {
            QFile file(temp1.fileName());
            QVERIFY(file.open(QIODevice::ReadOnly));
            TestUploader upload(config, &file);
            QVERIFY(upload.exec());
            QByteArray check(temp2.readAll());

            QVERIFY(takeLine(check).startsWith(temp1.fileName().toUtf8() + "Run"));
            QVERIFY(check.isEmpty());
        }

        temp2.resize(0);
        {
            QByteArray data(100, 'A');
            TestUploader upload(config, data);
            QVERIFY(upload.exec());
            QByteArray check(temp2.readAll());

            QVERIFY(takeLine(check).startsWith(data + "Run"));
            QVERIFY(check.isEmpty());
        }
    }

#endif

    void copy()
    {
        QTemporaryFile temp1;
        QVERIFY(temp1.open());
        QTemporaryFile temp2;
        QVERIFY(temp2.open());

        QString targetName(temp2.fileName());

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "Copy";
        config["Local/Target"] = targetName;

        QVERIFY(populateTemp(temp1.fileName()));

        {
            QFile file(temp1.fileName());
            TestUploader upload(config, &file);
            QVERIFY(upload.exec());
            QFile input(targetName);
            QVERIFY(input.open(QIODevice::ReadOnly));
            QCOMPARE(input.readAll(), temp1.fileName().toUtf8());
        }

        QFile::resize(targetName, 0);
        {
            QFile file(temp1.fileName());
            QVERIFY(file.open(QIODevice::ReadOnly));
            TestUploader upload(config, &file);
            QVERIFY(upload.exec());
            QFile input(targetName);
            QVERIFY(input.open(QIODevice::ReadOnly));
            QCOMPARE(input.readAll(), temp1.fileName().toUtf8());
        }

        QFile::resize(targetName, 0);
        {
            QByteArray data(100, 'A');
            TestUploader upload(config, data);
            QVERIFY(upload.exec());
            QFile input(targetName);
            QVERIFY(input.open(QIODevice::ReadOnly));
            QCOMPARE(input.readAll(), data);
        }

        targetName = tmpPath + "/foobar/baz";
        QVERIFY(!QFileInfo(tmpPath + "/foobar/baz").exists());
        QVERIFY(!QFileInfo(tmpPath + "/foobar").exists());
        config["Local/Target"] = targetName;
        {
            QFile file(temp1.fileName());
            TestUploader upload(config, &file);
            QVERIFY(upload.exec());
            QFile input(targetName);
            QVERIFY(input.open(QIODevice::ReadOnly));
            QCOMPARE(input.readAll(), temp1.fileName().toUtf8());
        }
    }

#ifdef Q_OS_UNIX

    void email()
    {
        QTemporaryFile temp1;
        QVERIFY(temp1.open());
        QTemporaryFile temp2;
        QVERIFY(temp2.open());

        QString scriptFile(tmpDir.absoluteFilePath("execscript.sh"));
        {
            QFile runProg(scriptFile);
            QVERIFY(runProg.open(QIODevice::WriteOnly | QIODevice::Truncate));
            runProg.setPermissions(QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner);
            runProg.resize(0);
            runProg.write("#!/bin/sh\nexec cat - >> " + temp2.fileName().toUtf8());
            runProg.close();
        }

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "EMail";
        config["Email/Relay/Program"] = scriptFile;
        config["Email/To"] = "Foo@localhost.localdomain";
        config["Email/From"] = "Bar@localhost.localdomain";
        config["Email/CC"] = "Baz@localhost.localdomain";
        config["Email/Attachment"] = "INPUT.gz";
        config["Email/Subject"] = "Email test";
        config["Email/Body"] = "Email body";

        QVERIFY(populateTemp(temp1.fileName()));

        {
            QFile file(temp1.fileName());
            TestUploader upload(config, &file);
            QVERIFY(upload.exec());

            QByteArray check(temp2.readAll());
            QVERIFY(check.contains("To: Foo@localhost.localdomain"));
            QVERIFY(check.contains("From: Bar@localhost.localdomain"));
            QVERIFY(check.contains("CC: Baz@localhost.localdomain"));
            QVERIFY(check.contains("Subject: Email test"));
            QVERIFY(check.contains("Email body"));
            QVERIFY(check.contains("filename=\"" + temp1.fileName().toUtf8() + ".gz\""));
            check = check.replace('\r', QByteArray());
            check = check.replace('\n', QByteArray());
            QVERIFY(check.contains(temp1.fileName().toUtf8().toBase64()));
        }

        temp2.resize(0);
        {
            QFile file(temp1.fileName());
            QVERIFY(file.open(QIODevice::ReadOnly));
            TestUploader upload(config, temp1.fileName().toUtf8());
            QVERIFY(upload.exec());

            QByteArray check(temp2.readAll());
            QVERIFY(check.contains("To: Foo@localhost.localdomain"));
            QVERIFY(check.contains("From: Bar@localhost.localdomain"));
            QVERIFY(check.contains("CC: Baz@localhost.localdomain"));
            QVERIFY(check.contains("Subject: Email test"));
            QVERIFY(check.contains("Email body"));
            QVERIFY(check.contains("filename=\".gz\""));
            check = check.replace('\r', QByteArray());
            check = check.replace('\n', QByteArray());
            QVERIFY(check.contains(temp1.fileName().toUtf8().toBase64()));
        }

        temp2.resize(0);
        {
            QByteArray data(20, 'A');
            TestUploader upload(config, data);
            QVERIFY(upload.exec());

            QByteArray check(temp2.readAll());
            QVERIFY(check.contains("To: Foo@localhost.localdomain"));
            QVERIFY(check.contains("From: Bar@localhost.localdomain"));
            QVERIFY(check.contains("CC: Baz@localhost.localdomain"));
            QVERIFY(check.contains("Subject: Email test"));
            QVERIFY(check.contains("Email body"));
            QVERIFY(check.contains("filename=\".gz\""));
            check = check.replace('\r', QByteArray());
            check = check.replace('\n', QByteArray());
            QVERIFY(check.contains(data.toBase64()));
        }

        config["Email/Separate"] = true;
        {
            QFile file(temp1.fileName());
            TestUploader upload(config, &file);
            QVERIFY(upload.exec());

            QByteArray check(temp2.readAll());
            QVERIFY(check.contains("To: Foo@localhost.localdomain"));
            QVERIFY(check.contains("From: Bar@localhost.localdomain"));
            QVERIFY(check.contains("CC: Baz@localhost.localdomain"));
            QVERIFY(check.contains("Subject: Email test"));
            QVERIFY(check.contains("Email body"));
            QVERIFY(check.contains("filename=\"" + temp1.fileName().toUtf8() + ".gz\""));
            check = check.replace('\r', QByteArray());
            check = check.replace('\n', QByteArray());
            QVERIFY(check.contains(temp1.fileName().toUtf8().toBase64()));
        }

        temp2.resize(0);
        {
            QFile file(temp1.fileName());
            QVERIFY(file.open(QIODevice::ReadOnly));
            TestUploader upload(config, temp1.fileName().toUtf8());
            QVERIFY(upload.exec());

            QByteArray check(temp2.readAll());
            QVERIFY(check.contains("To: Foo@localhost.localdomain"));
            QVERIFY(check.contains("From: Bar@localhost.localdomain"));
            QVERIFY(check.contains("CC: Baz@localhost.localdomain"));
            QVERIFY(check.contains("Subject: Email test"));
            QVERIFY(check.contains("Email body"));
            QVERIFY(check.contains("filename=\".gz\""));
            check = check.replace('\r', QByteArray());
            check = check.replace('\n', QByteArray());
            QVERIFY(check.contains(temp1.fileName().toUtf8().toBase64()));
        }

        temp2.resize(0);
        {
            QByteArray data(20, 'A');
            TestUploader upload(config, data);
            QVERIFY(upload.exec());

            QByteArray check(temp2.readAll());
            QVERIFY(check.contains("To: Foo@localhost.localdomain"));
            QVERIFY(check.contains("From: Bar@localhost.localdomain"));
            QVERIFY(check.contains("CC: Baz@localhost.localdomain"));
            QVERIFY(check.contains("Subject: Email test"));
            QVERIFY(check.contains("Email body"));
            QVERIFY(check.contains("filename=\".gz\""));
            check = check.replace('\r', QByteArray());
            check = check.replace('\n', QByteArray());
            QVERIFY(check.contains(data.toBase64()));
        }

        config["Email/Separate"] = false;
        config["Email/Attachment"] = "INDEX.gz";
        temp2.resize(0);
        {
            QByteArray data1(10, 'A');
            QByteArray data2(20, 'B');
            TestUploader upload(config, QList<QByteArray>() << data1 << data2);
            QVERIFY(upload.exec());

            QByteArray check(temp2.readAll());
            QVERIFY(check.contains("To: Foo@localhost.localdomain"));
            QVERIFY(check.contains("From: Bar@localhost.localdomain"));
            QVERIFY(check.contains("CC: Baz@localhost.localdomain"));
            QVERIFY(check.contains("Subject: Email test"));
            QVERIFY(check.contains("Email body"));
            QVERIFY(check.contains("filename=\"1.gz\""));
            QVERIFY(check.contains("filename=\"2.gz\""));
            check = check.replace('\r', QByteArray());
            check = check.replace('\n', QByteArray());
            QVERIFY(check.contains(data1.toBase64()));
            QVERIFY(check.contains(data2.toBase64()));
        }

        config["Email/Separate"] = true;
        temp2.resize(0);
        {
            QByteArray data1(10, 'A');
            QByteArray data2(20, 'B');
            TestUploader upload(config, QList<QByteArray>() << data1 << data2);
            QVERIFY(upload.exec());

            QByteArray check(temp2.readAll());
            QVERIFY(check.contains("To: Foo@localhost.localdomain"));
            QVERIFY(check.contains("From: Bar@localhost.localdomain"));
            QVERIFY(check.contains("CC: Baz@localhost.localdomain"));
            QVERIFY(check.contains("Subject: Email test"));
            QVERIFY(check.contains("Email body"));
            QVERIFY(check.contains("filename=\"1.gz\""));
            QVERIFY(check.contains("filename=\"2.gz\""));
            check = check.replace('\r', QByteArray());
            check = check.replace('\n', QByteArray());
            QVERIFY(check.contains(data1.toBase64()));
            QVERIFY(check.contains(data2.toBase64()));

            int idx = check.indexOf("Email body");
            QVERIFY(idx > 0);
            QVERIFY(check.indexOf("Email body", idx + 1) > 0);
        }
    }

#endif

    void ftpFile()
    {
        QTemporaryFile temp1;
        QVERIFY(temp1.open());

        TestFTPManager manager;
        manager.directories.insert("path");
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "FTP";
        config["FTP/Hostname"] = "127.0.0.1";
        config["FTP/Port"] = port;
        config["FTP/FTPActive"] = false;
        config["FTP/Target"] = "path/to/file";

        QVERIFY(populateTemp(temp1.fileName()));

        {
            QFile file(temp1.fileName());
            TestUploader upload(config, &file);
            QVERIFY(upload.exec());
        }

        manager.waitForCompleted();

        QCOMPARE(manager.receivedName, QString("path/to/file"));
        QCOMPARE(manager.receivedData, temp1.fileName().toUtf8());

        QSet<QString> dirs;
        dirs.insert("path");
        dirs.insert("path/to");
        QCOMPARE(manager.directories, dirs);
    }

    void ftpDevice()
    {
        QTemporaryFile temp1;
        QVERIFY(temp1.open());

        TestFTPManager manager;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "FTP";
        config["FTP/Hostname"] = "127.0.0.1";
        config["FTP/Port"] = port;
        config["FTP/FTPActive"] = false;
        config["FTP/Target"] = "targetfile";

        QVERIFY(populateTemp(temp1.fileName()));

        {
            QFile file(temp1.fileName());
            QVERIFY(file.open(QIODevice::ReadOnly));
            TestUploader upload(config, file.readAll());
            QVERIFY(upload.exec());
        }

        manager.waitForCompleted();

        QCOMPARE(manager.receivedName, QString("targetfile"));
        QCOMPARE(manager.receivedData, temp1.fileName().toUtf8());
        QCOMPARE(manager.directories.size(), 0);
    }

    void ftpRawData()
    {
        TestFTPManager manager;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "FTP";
        config["FTP/Hostname"] = "127.0.0.1";
        config["FTP/Port"] = port;
        config["FTP/FTPActive"] = false;
        config["FTP/Target"] = "targetfile";

        QByteArray data(100, 'A');

        {
            TestUploader upload(config, data);
            QVERIFY(upload.exec());
        }

        manager.waitForCompleted();

        QCOMPARE(manager.receivedName, QString("targetfile"));
        QCOMPARE(manager.receivedData, data);
    }

    void urlFile()
    {
        QTemporaryFile temp1;
        QVERIFY(temp1.open());

        TestFTPManager manager;
        manager.haltOnReceive = true;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "URL";
        config["URL/Target"] = QString("ftp://anonymous@127.0.0.1:%1/path/to/file").arg(port);
        config["URL/PUT"] = true;

        QVERIFY(populateTemp(temp1.fileName()));

        {
            QFile file(temp1.fileName());
            TestUploader upload(config, &file);
            QVERIFY(upload.exec());
        }

        manager.waitForCompleted();

        QCOMPARE(manager.receivedName, QString("/path/to/file"));
        QCOMPARE(manager.receivedData, temp1.fileName().toUtf8());
    }

    void urlDevice()
    {
        QTemporaryFile temp1;
        QVERIFY(temp1.open());

        TestFTPManager manager;
        manager.haltOnReceive = true;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "URL";
        config["URL/Target"] = QString("ftp://anonymous@127.0.0.1:%1/targetfile").arg(port);
        config["URL/PUT"] = true;

        QVERIFY(populateTemp(temp1.fileName()));

        {
            QFile file(temp1.fileName());
            QVERIFY(file.open(QIODevice::ReadOnly));
            TestUploader upload(config, file.readAll());
            QVERIFY(upload.exec());
        }

        manager.waitForCompleted();

        QCOMPARE(manager.receivedName, QString("/targetfile"));
        QCOMPARE(manager.receivedData, temp1.fileName().toUtf8());
        QCOMPARE(manager.directories.size(), 0);
    }

    void urlRawData()
    {
        TestFTPManager manager;
        manager.haltOnReceive = true;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "URL";
        config["URL/Target"] = QString("ftp://anonymous@127.0.0.1:%1/targetfile").arg(port);
        config["URL/PUT"] = true;

        QByteArray data(100, 'A');

        {
            TestUploader upload(config, data);
            QVERIFY(upload.exec());
        }

        manager.waitForCompleted();

        QCOMPARE(manager.receivedName, QString("/targetfile"));
        QCOMPARE(manager.receivedData, data);
    }
};

QTEST_MAIN(TestUpload)

#include "upload.moc"
