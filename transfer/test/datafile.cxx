/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <algorithm>
#include <QObject>
#include <QBuffer>
#include <QTest>

#include "transfer/datafile.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Transfer;
using namespace CPD3::Data;

class TestDataFile : public QObject {
Q_OBJECT

    static void addAll(DataPack &writer,
                       const ArchiveValue::Transfer &values,
                       const ArchiveErasure::Transfer &deleted,
                       int flushStep = -1,
                       bool polarity = false)
    {
        auto currentValue = values.begin();
        auto currentDeleted = deleted.begin();
        auto endValues = values.end();
        auto endDeleted = deleted.end();

        int index = 0;
        while (currentValue != endValues || currentDeleted != endDeleted) {
            if (currentValue != endValues) {
                if (currentDeleted == endDeleted ||
                        Range::compareStart(currentValue->getStart(), currentDeleted->getStart()) <
                                (polarity ? 0 : 1)) {
                    writer.incomingValue(*currentValue);
                    ++currentValue;
                } else {
                    writer.incomingErasure(*currentDeleted);
                    ++currentDeleted;
                }
            } else {
                writer.incomingErasure(*currentDeleted);
                ++currentDeleted;
            }

            if (flushStep > 0) {
                if (index == 0) {
                    writer.flushData();
                }

                index = (index + 1) % flushStep;
            }
        }
    }

    template<typename T>
    static bool checkSortEqual(const T &a, const T &b, bool checkModified = false)
    {
        if (!FP::equal(a.getStart(), b.getStart()))
            return false;
        if (!FP::equal(a.getEnd(), b.getEnd()))
            return false;
        if (a.getPriority() != b.getPriority())
            return false;
        if (a.getUnit() != b.getUnit())
            return false;
        if (checkModified && !FP::equal(a.getModified(), b.getModified()))
            return false;
        return true;
    }

    static int checkSortValueCompare(const ArchiveValue &a, const ArchiveValue &b)
    {
        qint64 ia = a.getValue().toInt64();
        qint64 ib = b.getValue().toInt64();
        if (!INTEGER::defined(ia))
            return INTEGER::defined(ib) ? -1 : 0;
        else if (!INTEGER::defined(ib))
            return 1;
        if (ia == ib)
            return 0;
        return ia < ib ? -1 : 1;
    }

    static int checkSortValueCompare(const ArchiveErasure &a, const ArchiveErasure &b)
    {
        Q_UNUSED(a);
        Q_UNUSED(b);
        return 0;
    }

    template<typename T>
    static bool checkSortCompare(const T &a, const T &b)
    {
        {
            int cr = Range::compareStart(a.getStart(), b.getStart());
            if (cr != 0)
                return cr < 0;
        }
        {
            int cr = Range::compareEnd(a.getEnd(), b.getEnd());
            if (cr != 0)
                return cr < 0;
        }

        {
            int pa = a.getPriority();
            int pb = b.getPriority();
            if (pa != pb)
                return pa < pb;
        }

        {
            const SequenceName &ua = a.getName();
            const SequenceName &ub = b.getName();
            if (ua != ub)
                return SequenceName::OrderLogical()(ua, ub);
        }

        {
            int cr = checkSortValueCompare(a, b);
            if (cr != 0)
                return cr < 0;
        }

        return Range::compareStart(a.getModified(), b.getModified()) < 0;
    }

    static bool compare(const ArchiveValue::Transfer &result,
                        const ArchiveValue::Transfer &expected,
                        bool checkModified = false)
    {
        if (result.size() != expected.size()) {
            qDebug() << "Value list size mismatch";
            return false;
        }
        for (auto r = result.begin(), e = expected.begin(), endR = result.end();
                r != endR;
                ++r, ++e) {
            if (!checkSortEqual(*r, *e, checkModified)) {
                qDebug() << "Value mismatch:" << *r << "vs" << *e;
                return false;
            }
            if (r->getValue() != e->getValue()) {
                qDebug() << "Value mismatch:" << *r << "vs" << *e;
                return false;
            }
        }
        return true;
    }

    static bool compare(const ArchiveErasure::Transfer &result,
                        const ArchiveErasure::Transfer &expected,
                        bool checkModified = false)
    {
        if (result.size() != expected.size()) {
            qDebug() << "Deleted list size mismatch";
            return false;
        }
        for (auto r = result.begin(), e = expected.begin(), endR = result.end();
                r != endR;
                ++r, ++e) {
            if (!checkSortEqual(*r, *e, checkModified)) {
                qDebug() << "Deleted mismatch:" << *r << "vs" << *e;
                return false;
            }
        }
        return true;
    }

    template<typename T>
    static bool checkAscending(const T &list)
    {
        double prior = FP::undefined();
        for (const auto &i : list) {
            double now = i.getStart();
            if (Range::compareStart(prior, now) > 0)
                return false;
            prior = now;
        }
        return true;
    }

    static ArchiveValue AV(const SequenceName &name,
                           int index,
                           double start,
                           double end,
                           int priority = 0,
                           double modified = FP::undefined())
    {
        return ArchiveValue(name, Variant::Root(index), start, end, priority, modified, false);
    }

    static ArchiveErasure AE(const SequenceName &name,
                             double start,
                             double end,
                             int priority = 0,
                             double modified = FP::undefined())
    {
        return ArchiveErasure(name, start, end, priority, modified);
    }


private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void empty()
    {
        QBuffer buffer;
        QVERIFY(buffer.open(QIODevice::ReadWrite));

        DataPack pack(&buffer);
        pack.endData();
        pack.start();
        Threading::pollInEventLoop([&] { return !pack.isFinished(); });
        pack.wait();

        QCOMPARE((int) buffer.size(), 0);
    }

    void basic()
    {
        QFETCH(ArchiveValue::Transfer, values);
        QFETCH(ArchiveErasure::Transfer, erasure);

        QVERIFY(checkAscending(values));
        QVERIFY(checkAscending(erasure));

        std::sort(values.begin(), values.end(), checkSortCompare<ArchiveValue>);
        std::sort(erasure.begin(), erasure.end(), checkSortCompare<ArchiveErasure>);

        QVERIFY(!values.empty() || !erasure.empty());

        {
            auto buffer = IO::Access::buffer();
            {
                DataPack pack(buffer->stream(), false);
                pack.start();
                addAll(pack, values, erasure);
                pack.endData();
                Threading::pollInEventLoop([&] { return !pack.isFinished(); });
                pack.wait();
            }

            ErasureSink::Buffer output;
            QVERIFY(DataUnpack().exec(buffer, &output));

            auto aresult = output.archiveTake();
            auto eresult = output.erasureTake();
            QVERIFY(checkAscending(aresult));
            QVERIFY(checkAscending(eresult));

            std::sort(aresult.begin(), aresult.end(), checkSortCompare<ArchiveValue>);
            std::sort(eresult.begin(), eresult.end(), checkSortCompare<ArchiveErasure>);
            QVERIFY(compare(aresult, values, false));
            QVERIFY(compare(eresult, erasure, false));
        }

        {
            auto buffer = IO::Access::buffer();
            {
                DataPack pack(buffer->stream(), true);
                pack.start();
                addAll(pack, values, erasure);
                pack.endData();
                Threading::pollInEventLoop([&] { return !pack.isFinished(); });
                pack.wait();
            }

            ErasureSink::Buffer output;
            QVERIFY(DataUnpack().exec(buffer, &output));

            auto aresult = output.archiveTake();
            auto eresult = output.erasureTake();
            QVERIFY(checkAscending(aresult));
            QVERIFY(checkAscending(eresult));

            std::sort(aresult.begin(), aresult.end(), checkSortCompare<ArchiveValue>);
            std::sort(eresult.begin(), eresult.end(), checkSortCompare<ArchiveErasure>);
            QVERIFY(compare(aresult, values, true));
            QVERIFY(compare(eresult, erasure, true));
        }

        {
            auto buffer = IO::Access::buffer();
            {
                DataPack pack(buffer->stream(), false);
                pack.start();
                addAll(pack, values, erasure, 1);
                pack.endData();
                Threading::pollInEventLoop([&] { return !pack.isFinished(); });
                pack.wait();
            }


            ErasureSink::Buffer output;
            QVERIFY(DataUnpack().exec(buffer, &output));

            auto aresult = output.archiveTake();
            auto eresult = output.erasureTake();
            QVERIFY(checkAscending(aresult));
            QVERIFY(checkAscending(eresult));

            std::sort(aresult.begin(), aresult.end(), checkSortCompare<ArchiveValue>);
            std::sort(eresult.begin(), eresult.end(), checkSortCompare<ArchiveErasure>);
            QVERIFY(compare(aresult, values, false));
            QVERIFY(compare(eresult, erasure, false));
        }

        {
            auto buffer = IO::Access::buffer();
            {
                DataPack pack(buffer->stream(), false);
                pack.start();
                addAll(pack, values, erasure, 1, true);
                pack.endData();
                Threading::pollInEventLoop([&] { return !pack.isFinished(); });
                pack.wait();
            }

            ErasureSink::Buffer output;
            QVERIFY(DataUnpack().exec(buffer, &output));

            auto aresult = output.archiveTake();
            auto eresult = output.erasureTake();
            QVERIFY(checkAscending(aresult));
            QVERIFY(checkAscending(eresult));

            std::sort(aresult.begin(), aresult.end(), checkSortCompare<ArchiveValue>);
            std::sort(eresult.begin(), eresult.end(), checkSortCompare<ArchiveErasure>);
            QVERIFY(compare(aresult, values, false));
            QVERIFY(compare(eresult, erasure, false));
        }

        if (values.size() > 1) {
            auto buffer = IO::Access::buffer();
            {
                DataPack pack(buffer->stream(), false);
                pack.start();
                addAll(pack, values, erasure, static_cast<int>(values.size()) / 2);
                pack.endData();
                Threading::pollInEventLoop([&] { return !pack.isFinished(); });
                pack.wait();
            }

            ErasureSink::Buffer output;
            QVERIFY(DataUnpack().exec(buffer, &output));

            auto aresult = output.archiveTake();
            auto eresult = output.erasureTake();
            QVERIFY(checkAscending(aresult));
            QVERIFY(checkAscending(eresult));

            std::sort(aresult.begin(), aresult.end(), checkSortCompare<ArchiveValue>);
            std::sort(eresult.begin(), eresult.end(), checkSortCompare<ArchiveErasure>);
            QVERIFY(compare(aresult, values, false));
            QVERIFY(compare(eresult, erasure, false));
        }
    }

    void basic_data()
    {
        QTest::addColumn<ArchiveValue::Transfer>("values");
        QTest::addColumn<ArchiveErasure::Transfer>("erasure");

        SequenceName u1("bnd", "raw", "BsG_S11");
        SequenceName u2("bnd", "raw", "BsB_S11");
        SequenceName u3("bnd", "clean", "BsG_S11");
        SequenceName u4("alt", "raw", "BsG_S11");
        SequenceName u5("alt", "clean", "BsB_S11");

        QTest::newRow("Single value")
                << (ArchiveValue::Transfer{AV(u1, 1, 1000.0, 2000.0, 1, 100.0)})
                << (ArchiveErasure::Transfer());
        QTest::newRow("Single deleted") << (ArchiveValue::Transfer()) << (ArchiveErasure::Transfer{
                ArchiveErasure(u1, 1000.0, 2000.0, 1, 100.0)});
        QTest::newRow("Single both") << (ArchiveValue::Transfer{AV(u1, 1, 1000.0, 2000.0)})
                                     << (ArchiveErasure::Transfer{AE(u1, 1000.0, 2000.0)});
        QTest::newRow("Single value before") << (ArchiveValue::Transfer{AV(u1, 1, 1000.0, 2000.0)})
                                             << (ArchiveErasure::Transfer{AE(u1, 1001.0, 2000.0)});
        QTest::newRow("Single deleted before")
                << (ArchiveValue::Transfer{AV(u1, 1, 1001.0, 2000.0)})
                << (ArchiveErasure::Transfer{AE(u1, 1000.0, 2000.0)});

        QTest::newRow("Multiple value")
                << (ArchiveValue::Transfer{AV(u1, 1, 1000.0, 2000.0), AV(u2, 2, 1000.0, 2000.0),
                                           AV(u3, 3, 1000.0, 2000.0), AV(u4, 4, 1000.0, 2000.0),
                                           AV(u5, 5, 1000.0, 2000.0)})
                << (ArchiveErasure::Transfer());
        QTest::newRow("Multiple value alternate")
                << (ArchiveValue::Transfer{AV(u1, 1, 1000.0, 2000.0), AV(u5, 2, 1000.0, 2000.0),
                                           AV(u3, 3, 1000.0, 2000.0), AV(u4, 4, 1000.0, 2000.0),
                                           AV(u2, 5, 1000.0, 2000.0)})
                << (ArchiveErasure::Transfer());
        QTest::newRow("Multiple deleted") << (ArchiveValue::Transfer())
                                          << (ArchiveErasure::Transfer{AE(u1, 1000.0, 2000.0),
                                                                       AE(u2, 1000.0, 2000.0),
                                                                       AE(u3, 1000.0, 2000.0),
                                                                       AE(u4, 1000.0, 2000.0),
                                                                       AE(u5, 1000.0, 2000.0)});
        QTest::newRow("Multiple deleted alternate") << (ArchiveValue::Transfer())
                                                    << (ArchiveErasure::Transfer{
                                                            AE(u1, 1000.0, 2000.0),
                                                            AE(u5, 1000.0, 2000.0),
                                                            AE(u3, 1000.0, 2000.0),
                                                            AE(u4, 1000.0, 2000.0),
                                                            AE(u2, 1000.0, 2000.0)});
        QTest::newRow("Multiple both")
                << (ArchiveValue::Transfer{AV(u1, 1, 1000.0, 2000.0), AV(u2, 2, 1000.0, 2000.0),
                                           AV(u3, 3, 1000.0, 2000.0), AV(u4, 4, 1000.0, 2000.0),
                                           AV(u5, 5, 1000.0, 2000.0)})
                << (ArchiveErasure::Transfer{AE(u1, 1000.0, 2000.0), AE(u5, 1000.0, 2000.0),
                                             AE(u3, 1000.0, 2000.0), AE(u4, 1000.0, 2000.0),
                                             AE(u2, 1000.0, 2000.0)});
        QTest::newRow("Multiple both alternate")
                << (ArchiveValue::Transfer{AV(u1, 1, 1000.0, 2000.0), AV(u5, 2, 1000.0, 2000.0),
                                           AV(u3, 3, 1000.0, 2000.0), AV(u4, 4, 1000.0, 2000.0),
                                           AV(u2, 5, 1000.0, 2000.0)})
                << (ArchiveErasure::Transfer{AE(u1, 1000.0, 2000.0), AE(u5, 1000.0, 2000.0),
                                             AE(u3, 1000.0, 2000.0), AE(u4, 1000.0, 2000.0),
                                             AE(u2, 1000.0, 2000.0)});

        QTest::newRow("Ordered value")
                << (ArchiveValue::Transfer{AV(u1, 1, 1000.0, 2000.0), AV(u1, 2, 1000.0, 2000.0),
                                           AV(u1, 3, 1001.0, 2000.0), AV(u2, 4, 1001.0, 2001.0),
                                           AV(u1, 5, 1003.0, 2000.0)})
                << (ArchiveErasure::Transfer());
        QTest::newRow("Ordered deleted") << (ArchiveValue::Transfer())
                                         << (ArchiveErasure::Transfer{AE(u1, 1000.0, 2000.0),
                                                                      AE(u1, 1000.0, 2000.0),
                                                                      AE(u1, 1001.0, 2000.0),
                                                                      AE(u2, 1001.0, 2001.0),
                                                                      AE(u1, 1003.0, 2000.0)});
        QTest::newRow("Ordered both")
                << (ArchiveValue::Transfer{AV(u1, 1, 1000.0, 2000.0), AV(u1, 2, 1000.0, 2000.0),
                                           AV(u1, 3, 1001.0, 2000.0), AV(u2, 4, 1001.0, 2001.0),
                                           AV(u1, 5, 1003.0, 2000.0)})
                << (ArchiveErasure::Transfer{AE(u1, 1000.0, 2000.0), AE(u1, 1000.0, 2000.0),
                                             AE(u1, 1001.0, 2000.0), AE(u2, 1001.0, 2001.0),
                                             AE(u1, 1003.0, 2000.0)});

        {
            ArchiveValue::Transfer overflowValuesStation;
            ArchiveErasure::Transfer overflowDeletedStation;

            ArchiveValue::Transfer overflowValuesArchive;
            ArchiveErasure::Transfer overflowDeletedArchive;

            ArchiveValue::Transfer overflowValuesVariable;
            ArchiveErasure::Transfer overflowDeletedVariable;

            ArchiveValue::Transfer overflowValuesFlavors;
            ArchiveErasure::Transfer overflowDeletedFlavors;

            ArchiveValue::Transfer overflowValuesAll;
            ArchiveErasure::Transfer overflowDeletedAll;

            ArchiveValue::Transfer overflowValuesAdvancing;
            ArchiveErasure::Transfer overflowDeletedAdvancing;

            for (int i = 0; i < 0xFFFF * 2 + 1; i++) {
                SequenceName stationUnit("s" + std::to_string(i), "a", "v");
                SequenceName archiveUnit("stn", "variable" + std::to_string(i), "v");
                SequenceName variableUnit("station", "archive", "arc" + std::to_string(i));
                SequenceName flavorUnit("s", "a", "v", {"fl" + std::to_string(i)});

                SequenceName allUnit("s" + std::to_string(i % 732), "a" + std::to_string(i % 1023),
                                     "v%1" + std::to_string(i % 517),
                                     {"f%1" + std::to_string(i % 40)});

                overflowValuesStation.push_back(AV(stationUnit, i, 1000.0, 2000.0));
                overflowDeletedStation.push_back(AE(stationUnit, 1000.0, 2000.0));

                overflowValuesArchive.push_back(AV(archiveUnit, i, 1000.0, 2000.0));
                overflowDeletedArchive.push_back(AE(archiveUnit, 1000.0, 2000.0));

                overflowValuesVariable.push_back(AV(variableUnit, i, 1000.0, 2000.0));
                overflowDeletedVariable.push_back(AE(variableUnit, 1000.0, 2000.0));

                overflowValuesFlavors.push_back(AV(flavorUnit, i, 1000.0, 2000.0));
                overflowDeletedFlavors.push_back(AE(flavorUnit, 1000.0, 2000.0));

                overflowValuesAll.push_back(AV(allUnit, i, 1000.0, 2000.0));
                overflowDeletedAll.push_back(AE(allUnit, 1000.0, 2000.0));

                double begin = 1000.0 + (double) (i / 100);
                overflowValuesAdvancing.push_back(AV(allUnit, i, begin, begin + 10.0));
                overflowDeletedAdvancing.push_back(AE(allUnit, begin, begin + 10.0));
            }

            QTest::newRow("Overflow station values") << overflowValuesStation
                                                     << (ArchiveErasure::Transfer());
            QTest::newRow("Overflow station deleted") << (ArchiveValue::Transfer())
                                                      << overflowDeletedStation;
            QTest::newRow("Overflow station both") << overflowValuesStation
                                                   << overflowDeletedStation;

            QTest::newRow("Overflow archive values") << overflowValuesArchive
                                                     << (ArchiveErasure::Transfer());
            QTest::newRow("Overflow archive deleted") << (ArchiveValue::Transfer())
                                                      << overflowDeletedArchive;
            QTest::newRow("Overflow archive both") << overflowValuesArchive
                                                   << overflowDeletedArchive;

            QTest::newRow("Overflow variable values") << overflowValuesVariable
                                                      << (ArchiveErasure::Transfer());
            QTest::newRow("Overflow variable deleted") << (ArchiveValue::Transfer())
                                                       << overflowDeletedVariable;
            QTest::newRow("Overflow variable both") << overflowValuesVariable
                                                    << overflowDeletedVariable;

            QTest::newRow("Overflow flavors values") << overflowValuesFlavors
                                                     << (ArchiveErasure::Transfer());
            QTest::newRow("Overflow flavors deleted") << (ArchiveValue::Transfer())
                                                      << overflowDeletedFlavors;
            QTest::newRow("Overflow flavors both") << overflowValuesFlavors
                                                   << overflowDeletedFlavors;

            QTest::newRow("Overflow flavors values") << overflowValuesFlavors
                                                     << (ArchiveErasure::Transfer());
            QTest::newRow("Overflow flavors deleted") << (ArchiveValue::Transfer())
                                                      << overflowDeletedFlavors;
            QTest::newRow("Overflow flavors both") << overflowValuesFlavors
                                                   << overflowDeletedFlavors;

            QTest::newRow("Overflow all values") << overflowValuesAll
                                                 << (ArchiveErasure::Transfer());
            QTest::newRow("Overflow all deleted") << (ArchiveValue::Transfer())
                                                  << overflowDeletedAll;
            QTest::newRow("Overflow all both") << overflowValuesAll << overflowDeletedAll;

            QTest::newRow("Overflow advancing values") << overflowValuesAdvancing
                                                       << (ArchiveErasure::Transfer());
            QTest::newRow("Overflow advancing deleted") << (ArchiveValue::Transfer())
                                                        << overflowDeletedAdvancing;
            QTest::newRow("Overflow advancing both") << overflowValuesAdvancing
                                                     << overflowDeletedAdvancing;
        }

        {
            ArchiveValue::Transfer largeValues;
            ArchiveErasure::Transfer largeDeleted;

            for (int i = 0; i < 0xFFFF + 3; i++) {
                largeValues.emplace_back(AV(u1, i, 1000.0, 2000.0));
                largeValues.emplace_back(AV(u1, -i, 1000.0, 2001.0));
                largeDeleted.emplace_back(AE(u1, 1000.0, 2000.0));
                largeDeleted.emplace_back(AE(u1, 1000.0, 2001.0));
            }
            for (int i = 0; i < 0xFFFF + 3; i++) {
                largeValues.emplace_back(AV(u1, i, 1001.0, 2000.0));
                largeValues.emplace_back(AV(u1, -i, 1001.0, 2001.0));
                largeDeleted.emplace_back(AE(u1, 1001.0, 2000.0));
                largeDeleted.emplace_back(AE(u1, 1001.0, 2001.0));
            }

            QTest::newRow("Large values") << largeValues << (ArchiveErasure::Transfer());
            QTest::newRow("Large deleted") << (ArchiveValue::Transfer()) << largeDeleted;
            QTest::newRow("Large both") << largeValues << largeDeleted;
        }
    }

    void legacyFormat()
    {
        SequenceName u1("bnd", "raw", "BsG_S11");
        ArchiveValue::Transfer values
                {ArchiveValue(u1, Variant::Root(Variant::Flags{"Flag"}), 1000.0, 1000.0, 0,
                              FP::undefined(), false)};
        ArchiveErasure::Transfer erasure{AE(u1, 1000.0, 2000.0)};

        auto buffer = IO::Access::buffer();
        {
            DataPack pack(buffer->stream(), false, true);
            pack.start();
            addAll(pack, values, erasure);
            pack.endData();
            Threading::pollInEventLoop([&] { return !pack.isFinished(); });
            pack.wait();
        }

        ErasureSink::Buffer output;
        QVERIFY(DataUnpack().exec(buffer, &output));

        auto carchive = output.archiveTake();
        auto cerasure = output.erasureTake();
        QVERIFY(checkAscending(carchive));
        QVERIFY(checkAscending(cerasure));

        QVERIFY(compare(carchive, values, false));
        QVERIFY(compare(cerasure, erasure, false));
    }
};

QTEST_MAIN(TestDataFile)

#include "datafile.moc"