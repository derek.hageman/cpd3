/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QtEndian>
#include <QBuffer>
#include <QSslCertificate>
#include <QTest>
#include <QTemporaryFile>

#include "transfer/package.hxx"
#include "algorithms/cryptography.hxx"
#include "core/qtcompat.hxx"
#include "io/drivers/file.hxx"


using namespace CPD3;
using namespace CPD3::Transfer;
using namespace CPD3::Data;
using namespace CPD3::Algorithms;

static const char *cert1Data = "-----BEGIN CERTIFICATE-----\n"
                               "MIICbTCCAdYCCQDZ6mj3BI+kCTANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
                               "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
                               "DU5PQUEvR01EL0VTUkwxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
                               "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1MzkwNloXDTM5MTIxMjE1MzkwNlow\n"
                               "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
                               "ZGVyMRYwFAYDVQQKDA1OT0FBL0dNRC9FU1JMMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
                               "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
                               "jQAwgYkCgYEAn3nIKzjLLEY8KG6+hUvrkjDkl9VU8HFKnp8wA2RsMta/UvKG4ZIY\n"
                               "bNVsKSiAGm1G0i6Mtftw8quyihoxuy/hElH/XJChU5lersdrhalocHXoipNx8BeW\n"
                               "T8sXuJOrh+IoDKqVI4I6Sz42qAlqBU7bNm40TE8sBPAOefyTGavC3e0CAwEAATAN\n"
                               "BgkqhkiG9w0BAQUFAAOBgQBU2hljZZpJJ+zK6eePUpdELQGPGHJEbzfReeSBhRau\n"
                               "iDj4jEa7IE3yrMv8NqC3Q+6lEVuJg0NInvDYsNYLPjMSPS+jKldrmrJvg+TFrp1w\n"
                               "Qr3JoznNxwcwuDyrKjOVux6jo2aype3Zayo5VUizJX9Zo+xLe2MBVR1D0MXmoq8L\n"
                               "zA==\n"
                               "-----END CERTIFICATE-----";
static const char *key1Data = "-----BEGIN RSA PRIVATE KEY-----\n"
                              "MIICXAIBAAKBgQCfecgrOMssRjwobr6FS+uSMOSX1VTwcUqenzADZGwy1r9S8obh\n"
                              "khhs1WwpKIAabUbSLoy1+3Dyq7KKGjG7L+ESUf9ckKFTmV6ux2uFqWhwdeiKk3Hw\n"
                              "F5ZPyxe4k6uH4igMqpUjgjpLPjaoCWoFTts2bjRMTywE8A55/JMZq8Ld7QIDAQAB\n"
                              "AoGBAIQOd0f7PpsKCfS9R7zfklG7dP+Z4z07wzu4vCyC8uniVAoe1LxjmyA8VtV6\n"
                              "OSIpDTUs4M4tSWlZ7n1XlYjY6/lNt3xsy5BBQMlWVI8BV/yXnWRCxkQGBvXYTUKe\n"
                              "7LRsUcqyGpAPeuStO/V8GEM5H272yv5S4413D09C9cAhROMtAkEA07j2lnnSl/WR\n"
                              "0NFEOT9+nbh6Z+yRTzybC86TBhLCN3bY4VvqSWi1PdcU3oiz81dcaAPTksdx18/Y\n"
                              "cSfz295ewwJBAMDTq22Dt2HMPQymxNUn9/IdIPU34/fEqSHS3e9hAgrp2gDVeNHR\n"
                              "ZEzMkW00rLgdukkW51PV9hVIhYXdxYOjZY8CQFsf/cnwLurGf/b/SrzVDjr1/oEi\n"
                              "Obx/2j+vrmnrwvm6Rkhglir4TSGLo+jPr5vpmtUN6I8BFoeLZp31UyjrwZ8CQC3W\n"
                              "Y2ruI7qozV5jimjNToCMchg4yAVPB5GVydIsskqb2onWNRlTeE9VVcCrA9/kmTLk\n"
                              "serY8t2OVsdCt8AaKHsCQFMEhJCpXaeLnrzToSv0uqRYjcT+r3ZjSghhSepN0ZA+\n"
                              "PPj1CMmq9JrQaQAv3rT3hCbDF38TPhKX64OYzbuvdxA=\n"
                              "-----END RSA PRIVATE KEY-----\n";
static const char *cert2Data = "-----BEGIN CERTIFICATE-----\n"
                               "MIICbTCCAdYCCQCr9EVbER/M4TANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
                               "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
                               "DU5PQUEvRVNSTC9HTUQxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
                               "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1NDIxNloXDTM5MTIxMjE1NDIxNlow\n"
                               "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
                               "ZGVyMRYwFAYDVQQKDA1OT0FBL0VTUkwvR01EMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
                               "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
                               "jQAwgYkCgYEAqa4sVcN1M4mWVqTqZuG2VikkOTpYrHYgnD4jFKDfi5FTFE9OQ5oW\n"
                               "xhrBdoEtiWLEPmPSQCtJWCbD0d/YQ1ITxOF0OSaAa4c6PkdeiDmfull8EGmuaTFo\n"
                               "ikcFS0r2SVtvIy3eFEAg9GBF4iCMp2AvUo2eqNCLt4CdRi6LQs2xkmUCAwEAATAN\n"
                               "BgkqhkiG9w0BAQUFAAOBgQCEvlrqC1zChgkFMq8Oi7YRQOdx7ikzDodey/uu0r/W\n"
                               "DxCnpA+F2f3okHiR5+LfYqQ5fvCYnmZd7w69tvdpui8swa15aXWaxLVD9/woyM2y\n"
                               "Ofce/9pw6UgoYLkh9KsW5+2EP2Sn44lWcU5Jf8U/p8cDdjNIDzDG4vw7UFWiRakm\n"
                               "Tw==\n"
                               "-----END CERTIFICATE-----\n";
static const char *key2Data = "-----BEGIN RSA PRIVATE KEY-----\n"
                              "MIICWwIBAAKBgQCprixVw3UziZZWpOpm4bZWKSQ5OlisdiCcPiMUoN+LkVMUT05D\n"
                              "mhbGGsF2gS2JYsQ+Y9JAK0lYJsPR39hDUhPE4XQ5JoBrhzo+R16IOZ+6WXwQaa5p\n"
                              "MWiKRwVLSvZJW28jLd4UQCD0YEXiIIynYC9SjZ6o0Iu3gJ1GLotCzbGSZQIDAQAB\n"
                              "AoGAUzE2Q4ZlfDNFJo4M7wxTXcMmI3jb6RKxwmkkwgRuFfvWg+quMK7n45FSsUt8\n"
                              "jBOErCI8/4E5oKLA97GMUtV3IxAXT/hxQRe6qZS7PEUW4k6Op7evX1hSCfI/IAB/\n"
                              "AS6OqUR1U4+AFArhPS3CA+vuY4I01mpWpvI3dfvq29oUW4ECQQDQ4ItViA/CjWiq\n"
                              "peW2+cptQ8QqkNGscjsUTZyp3aNtunC1mP/HRKlKLnuhY6G+LQWbzDv7BSJ9UNO4\n"
                              "Ro1y90jJAkEAz/Xdjqx/xZntCleuxvMACWdhpMBqfdgs7+maLDXbtc9G43ygbqIz\n"
                              "Wj99k9lu0L3iHdRlYicwdfFFBbvd4jsmvQJATOF5J4AvHNLjpXvuc0y5n0IEIA6x\n"
                              "viFFcZGnijZUAv1OouivrG6vSOiXBK4hSFhV6iRgJ2KacTmg1ADT627tUQJAK4hl\n"
                              "W9OCX8QMGekm/iCqNk284/cfk75oEcTN8ElJ9/Iu/bn9/4rWwyKdUBDpIKtPJT1s\n"
                              "B7L6cwYRk9Sy6wPE5QJAFmjRUr3GypvuC4haazaOMyIt/FEbC+kUs9kq6JZsiBuM\n"
                              "LbZ02ko9IkhfOiUIewV6+Xb2j+1u/Itc737M6RZ+3Q==\n"
                              "-----END RSA PRIVATE KEY-----";

class TestFileUnpacker : public TransferUnpack {
public:
    QSet<QByteArray> acceptedSignatures;
    QHash<QByteArray, Data::Variant::Read> certificateLookup;
    QHash<QByteArray, Data::Variant::Read> keyLookup;

    QSet<QByteArray> returnedSignatures;
    QSet<QByteArray> returnedCertificates;
    QSet<QByteArray> returnedSignatureCertificates;
    QSet<QByteArray> returnedKeys;

    TestFileUnpacker() = default;

    virtual ~TestFileUnpacker() = default;

protected:
    Data::Variant::Read getCertificate(const QByteArray &id, bool decryption, int) override
    {
        if (decryption)
            returnedCertificates.insert(id);
        else
            returnedSignatureCertificates.insert(id);
        return certificateLookup.value(id, Data::Variant::Read::empty());
    }

    Data::Variant::Read getKeyForCertificate(const QByteArray &id, int) override
    {
        returnedKeys.insert(id);
        return keyLookup.value(id, Data::Variant::Read::empty());
    }

    bool signatureAuthorized(const QList<QByteArray> &ids, int) override
    {
        for (QList<QByteArray>::const_iterator check = ids.constBegin(), end = ids.constEnd();
                check != end;
                ++check) {
            returnedSignatures.insert(*check);
            if (!acceptedSignatures.contains(*check))
                return false;
        }
        return true;
    }
};

class TestPackage : public QObject {
Q_OBJECT

    static bool writeData(const QByteArray &data, QIODevice *target)
    {
        const char *ptr = data.constData();
        qint64 remaining = data.size();

        while (remaining > 0) {
            qint64 n = target->write(ptr, remaining);
            if (n < 0) {
                qDebug() << "Write failure:" << target->errorString();
                return false;
            }
            Q_ASSERT(n <= remaining);
            remaining -= n;
            ptr += n;
        }
        return true;
    }

    static QByteArray makeCompressibleData()
    {
        QByteArray result;

        QDataStream stream(&result, QIODevice::WriteOnly);
        while (result.size() < 1048576) {
            stream << (quint32) (result.size() ^ 0xDEADBEEF);
            stream << sin(result.size() / 10.0);
            stream << (quint8) 1;
        }

        return result;
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }


    void basicPackageFormat()
    {
        TransferPack packer;
        packer.checksumStage();

        Util::ByteArray inputData("Test data");
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));

        auto outputData = outputFile->stream()->readAll();
        QCOMPARE((int) outputData.size(), 4 + 1 + 1 + 64 + 1 + (int) inputData.size());

        const uchar *checkData = outputData.data<const uchar *>();
        QCOMPARE(qFromBigEndian<quint32>(checkData), 0xC4D34A76);
        checkData += 4;

        /* Checksum stage */
        QCOMPARE((int) (*checkData), 1);
        checkData += 1;

        /* Checksum type */
        QCOMPARE((int) (*checkData), 0);
        checkData += 1;

        /* Checksum contents */
        QCOMPARE(QByteArray::fromRawData((const char *) (checkData), 64),
                 Cryptography::sha512(QByteArray(1, (char) 0) + inputData.toQByteArrayRef()));
        checkData += 64;

        /* Inner data stage */
        QCOMPARE((int) (*checkData), 0);
        checkData += 1;

        /* Inner data contents */
        QCOMPARE(QByteArray((const char *) (checkData), inputData.size()),
                 inputData.toQByteArrayRef());
    }

    void noop()
    {
        TransferPack packer;

        Util::ByteArray inputData("Test data");
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(outputData.toQByteArrayRef().contains("Test data"));
        }

        TestFileUnpacker unpacker;
        auto recoveredFile = IO::Access::buffer();
        QVERIFY(unpacker.exec(outputFile, recoveredFile));
        {
            auto outputData = recoveredFile->stream()->readAll();
            QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
        }
    }

    void checksum()
    {
        TransferPack packer;
        packer.checksumStage();

        Util::ByteArray inputData("Test data");
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(outputData.toQByteArrayRef().contains("Test data"));
        }

        {
            TestFileUnpacker unpacker;
            auto recoveredFile = IO::Access::buffer();
            QVERIFY(unpacker.exec(outputFile, recoveredFile));
            {
                auto outputData = recoveredFile->stream()->readAll();
                QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
            }
        }


        {
            auto corruptData = outputFile->block();
            {
                Util::ByteArray temp;
                corruptData->read(temp);
            }
            corruptData->move(-1);
            corruptData->write(Util::ByteArray::filled(0));
        }
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(outputData.toQByteArrayRef().contains("Test dat"));
            QVERIFY(!outputData.toQByteArrayRef().contains("Test data"));
        }

        {
            TestFileUnpacker unpacker;
            auto recoveredFile = IO::Access::buffer();
            QVERIFY(!unpacker.exec(outputFile, recoveredFile));
        }
    }

    void compression()
    {
        TransferPack packer;
        packer.compressStage();

        Util::ByteArray inputData(makeCompressibleData());
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(outputData.size() > 256U);
            QVERIFY(outputData.size() < inputData.size());
        }


        TestFileUnpacker unpacker;
        auto recoveredFile = IO::Access::buffer();
        QVERIFY(unpacker.exec(outputFile, recoveredFile));
        {
            auto outputData = recoveredFile->stream()->readAll();
            QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
        }
    }

    void encryptPublic()
    {
        TransferPack packer;
        Variant::Write config = Variant::Write::empty();
        config["Certificate"].setString(cert1Data);
        config["Key"].setString(key1Data);
        packer.encryptStage(config, false);

        Util::ByteArray inputData("Test data");
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(!outputData.toQByteArrayRef().contains("Test data"));
        }

        {
            TransferPack packer2;
            Variant::Write config2 = Variant::Write::empty();
            config2["Certificate"].setString(cert2Data);
            config2["Key"].setString(key2Data);
            packer2.encryptStage(config2, false);

            auto outputFile2 = IO::Access::buffer();
            QVERIFY(packer2.exec(inputFile, outputFile2));
            {
                auto outputData = outputFile->stream()->readAll();
                auto outputData2 = outputFile2->stream()->readAll();
                QVERIFY(!outputData2.toQByteArrayRef().contains("Test data"));
                QCOMPARE((int) outputData2.size(), (int) outputData.size());
                QVERIFY(outputData2.toQByteArrayRef() != outputData.toQByteArrayRef());
            }
        }

        QByteArray cert1Sig(Cryptography::sha512(QSslCertificate(cert1Data)));

        TestFileUnpacker unpacker;
        unpacker.keyLookup.insert(cert1Sig, Variant::Root(key1Data));

        auto recoveredFile = IO::Access::buffer();
        QVERIFY(unpacker.exec(outputFile, recoveredFile));
        {
            auto outputData = recoveredFile->stream()->readAll();
            QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
        }

        QCOMPARE(unpacker.returnedKeys.size(), 1);
        QVERIFY(unpacker.returnedKeys.contains(cert1Sig));
    }

    void encryptPrivate()
    {
        TransferPack packer;
        Variant::Write config = Variant::Write::empty();
        config["Certificate"].setString(cert1Data);
        config["Key"].setString(key1Data);
        packer.encryptStage(config, true);

        Util::ByteArray inputData("Test data");
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(!outputData.toQByteArrayRef().contains("Test data"));
        }

        {
            TransferPack packer2;
            Variant::Write config2 = Variant::Write::empty();
            config2["Certificate"].setString(cert2Data);
            config2["Key"].setString(key2Data);
            packer2.encryptStage(config2, true);

            auto outputFile2 = IO::Access::buffer();
            QVERIFY(packer2.exec(inputFile, outputFile2));
            {
                auto outputData = outputFile->stream()->readAll();
                auto outputData2 = outputFile2->stream()->readAll();
                QVERIFY(!outputData2.toQByteArrayRef().contains("Test data"));
                QCOMPARE((int) outputData2.size(), (int) outputData.size());
                QVERIFY(outputData2.toQByteArrayRef() != outputData.toQByteArrayRef());
            }
        }

        QByteArray cert1Sig(Cryptography::sha512(QSslCertificate(cert1Data)));

        TestFileUnpacker unpacker;
        unpacker.certificateLookup.insert(cert1Sig, Variant::Root(cert1Data));

        auto recoveredFile = IO::Access::buffer();
        QVERIFY(unpacker.exec(outputFile, recoveredFile));
        {
            auto outputData = recoveredFile->stream()->readAll();
            QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
        }

        QCOMPARE(unpacker.returnedCertificates.size(), 1);
        QVERIFY(unpacker.returnedCertificates.contains(cert1Sig));
    }

    void signIncluded()
    {
        TransferPack packer;
        Variant::Write config = Variant::Write::empty();
        config["Certificate"].setString(cert1Data);
        config["Key"].setString(key1Data);
        packer.signStage(config, true);

        Util::ByteArray inputData("Test data");
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(outputData.toQByteArrayRef().contains("Test data"));
            QVERIFY(outputData.toQByteArrayRef().contains(QSslCertificate(cert1Data).toDer()));
            QVERIFY(!outputData.toQByteArrayRef().contains(QSslCertificate(cert2Data).toDer()));
        }

        QByteArray cert1Sig(Cryptography::sha512(QSslCertificate(cert1Data)));

        {
            TestFileUnpacker unpacker;
            unpacker.acceptedSignatures.insert(cert1Sig);

            auto recoveredFile = IO::Access::buffer();
            QVERIFY(unpacker.exec(outputFile, recoveredFile));
            {
                auto outputData = recoveredFile->stream()->readAll();
                QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
            }

            QCOMPARE(unpacker.returnedSignatures.size(), 1);
            QVERIFY(unpacker.returnedSignatures.contains(cert1Sig));
            QCOMPARE(unpacker.returnedSignatureCertificates.size(), 0);
        }

        {
            auto corruptData = outputFile->block();
            {
                Util::ByteArray temp;
                corruptData->read(temp);
            }
            corruptData->move(-1);
            corruptData->write(Util::ByteArray::filled(0));
        }
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(outputData.toQByteArrayRef().contains("Test dat"));
            QVERIFY(!outputData.toQByteArrayRef().contains("Test data"));
        }

        {
            TestFileUnpacker unpacker;
            unpacker.acceptedSignatures.insert(cert1Sig);
            auto recoveredFile = IO::Access::buffer();
            QVERIFY(!unpacker.exec(outputFile, recoveredFile));
        }
    }

    void signImplied()
    {
        TransferPack packer;
        Variant::Write config = Variant::Write::empty();
        config["Certificate"].setString(cert1Data);
        config["Key"].setString(key1Data);
        packer.signStage(config, false);

        Util::ByteArray inputData("Test data");
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(outputData.toQByteArrayRef().contains("Test data"));
            QVERIFY(outputData.toQByteArrayRef()
                              .contains(Cryptography::sha512(QSslCertificate(cert1Data).toDer())));
            QVERIFY(!outputData.toQByteArrayRef()
                               .contains(Cryptography::sha512(QSslCertificate(cert2Data).toDer())));
        }

        QByteArray cert1Sig(Cryptography::sha512(QSslCertificate(cert1Data)));

        {
            TestFileUnpacker unpacker;
            unpacker.certificateLookup.insert(cert1Sig, Variant::Root(cert1Data));
            unpacker.acceptedSignatures.insert(cert1Sig);

            auto recoveredFile = IO::Access::buffer();
            QVERIFY(unpacker.exec(outputFile, recoveredFile));
            {
                auto outputData = recoveredFile->stream()->readAll();
                QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
            }

            QCOMPARE(unpacker.returnedSignatures.size(), 1);
            QVERIFY(unpacker.returnedSignatures.contains(cert1Sig));
            QCOMPARE(unpacker.returnedSignatureCertificates.size(), 1);
            QVERIFY(unpacker.returnedSignatureCertificates.contains(cert1Sig));
        }

        {
            auto corruptData = outputFile->block();
            {
                Util::ByteArray temp;
                corruptData->read(temp);
            }
            corruptData->move(-1);
            corruptData->write(Util::ByteArray::filled(0));
        }
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(outputData.toQByteArrayRef().contains("Test dat"));
            QVERIFY(!outputData.toQByteArrayRef().contains("Test data"));
        }

        {
            TestFileUnpacker unpacker;
            unpacker.acceptedSignatures.insert(cert1Sig);
            auto recoveredFile = IO::Access::buffer();
            QVERIFY(!unpacker.exec(outputFile, recoveredFile));
        }
    }

    void signMultiple()
    {
        TransferPack packer;
        Variant::Write config1 = Variant::Write::empty();
        config1["Certificate"].setString(cert1Data);
        config1["Key"].setString(key1Data);
        Variant::Write config2 = Variant::Write::empty();
        config2["Certificate"].setString(cert2Data);
        config2["Key"].setString(key2Data);
        packer.signStage(std::vector<Variant::Read>{config1, config2}, true);

        Util::ByteArray inputData("Test data");
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(outputData.toQByteArrayRef().contains("Test data"));
            QVERIFY(outputData.toQByteArrayRef().contains(QSslCertificate(cert1Data).toDer()));
            QVERIFY(outputData.toQByteArrayRef().contains(QSslCertificate(cert2Data).toDer()));
        }

        QByteArray cert1Sig(Cryptography::sha512(QSslCertificate(cert1Data)));
        QByteArray cert2Sig(Cryptography::sha512(QSslCertificate(cert2Data)));

        {
            TestFileUnpacker unpacker;
            unpacker.acceptedSignatures.insert(cert1Sig);
            unpacker.acceptedSignatures.insert(cert2Sig);

            auto recoveredFile = IO::Access::buffer();
            QVERIFY(unpacker.exec(outputFile, recoveredFile));
            {
                auto outputData = recoveredFile->stream()->readAll();
                QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
            }

            QCOMPARE(unpacker.returnedSignatures.size(), 2);
            QVERIFY(unpacker.returnedSignatures.contains(cert1Sig));
            QVERIFY(unpacker.returnedSignatures.contains(cert2Sig));
        }

        {
            TestFileUnpacker unpacker;
            unpacker.acceptedSignatures.insert(cert1Sig);

            auto recoveredFile = IO::Access::buffer();
            QVERIFY(!unpacker.exec(outputFile, recoveredFile));
        }
        {
            TestFileUnpacker unpacker;
            unpacker.acceptedSignatures.insert(cert2Sig);

            auto recoveredFile = IO::Access::buffer();
            QVERIFY(!unpacker.exec(outputFile, recoveredFile));
        }

        {
            auto corruptData = outputFile->block();
            {
                Util::ByteArray temp;
                corruptData->read(temp);
            }
            corruptData->move(-1);
            corruptData->write(Util::ByteArray::filled(0));
        }
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(outputData.toQByteArrayRef().contains("Test dat"));
            QVERIFY(!outputData.toQByteArrayRef().contains("Test data"));
        }

        {
            TestFileUnpacker unpacker;
            unpacker.acceptedSignatures.insert(cert1Sig);
            unpacker.acceptedSignatures.insert(cert2Sig);
            auto recoveredFile = IO::Access::buffer();
            QVERIFY(!unpacker.exec(outputFile, recoveredFile));
        }
    }

    void pipelineToStream()
    {
        TransferPack packer;
        packer.compressStage();
        packer.checksumStage();

        Util::ByteArray inputData(makeCompressibleData());
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));


        TestFileUnpacker unpacker;
        auto recoveredFile = IO::Access::buffer();
        QVERIFY(unpacker.exec(outputFile, recoveredFile));
        {
            auto outputData = recoveredFile->stream()->readAll();
            QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
        }
    }

    void pipelineMultipleToStream()
    {
        TransferPack packer;
        packer.compressStage();
        packer.compressStage();
        packer.checksumStage();

        Util::ByteArray inputData(makeCompressibleData());
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));


        TestFileUnpacker unpacker;
        auto recoveredFile = IO::Access::buffer();
        QVERIFY(unpacker.exec(outputFile, recoveredFile));
        {
            auto outputData = recoveredFile->stream()->readAll();
            QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
        }
    }

    void pipelineToSeek()
    {
        TransferPack packer;
        packer.compressStage();
        Variant::Write config = Variant::Write::empty();
        config["Certificate"].setString(cert1Data);
        config["Key"].setString(key1Data);
        packer.signStage(config, true);

        Util::ByteArray inputData(makeCompressibleData());
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));


        TestFileUnpacker unpacker;
        unpacker.acceptedSignatures.insert(Cryptography::sha512(QSslCertificate(cert1Data)));
        auto recoveredFile = IO::Access::buffer();
        QVERIFY(unpacker.exec(outputFile, recoveredFile));
        {
            auto outputData = recoveredFile->stream()->readAll();
            QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
        }
    }

    void streamToPipeline()
    {
        TransferPack packer;
        packer.checksumStage();
        packer.compressStage();

        Util::ByteArray inputData(makeCompressibleData());
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));


        TestFileUnpacker unpacker;
        auto recoveredFile = IO::Access::buffer();
        QVERIFY(unpacker.exec(outputFile, recoveredFile));
        {
            auto outputData = recoveredFile->stream()->readAll();
            QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
        }
    }

    void seekToPipeline()
    {
        TransferPack packer;
        Variant::Write config = Variant::Write::empty();
        config["Certificate"].setString(cert1Data);
        config["Key"].setString(key1Data);
        packer.signStage(config, true);
        packer.compressStage();

        Util::ByteArray inputData(makeCompressibleData());
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));


        TestFileUnpacker unpacker;
        unpacker.acceptedSignatures.insert(Cryptography::sha512(QSslCertificate(cert1Data)));
        auto recoveredFile = IO::Access::buffer();
        QVERIFY(unpacker.exec(outputFile, recoveredFile));
        {
            auto outputData = recoveredFile->stream()->readAll();
            QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
        }
    }

    void seekToStream()
    {
        TransferPack packer;
        Variant::Write config = Variant::Write::empty();
        config["Certificate"].setString(cert1Data);
        config["Key"].setString(key1Data);
        packer.signStage(config, true);
        packer.checksumStage();

        Util::ByteArray inputData(makeCompressibleData());
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));


        TestFileUnpacker unpacker;
        unpacker.acceptedSignatures.insert(Cryptography::sha512(QSslCertificate(cert1Data)));
        auto recoveredFile = IO::Access::buffer();
        QVERIFY(unpacker.exec(outputFile, recoveredFile));
        {
            auto outputData = recoveredFile->stream()->readAll();
            QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
        }
    }

    void streamToSeek()
    {
        TransferPack packer;
        packer.checksumStage();
        Variant::Write config = Variant::Write::empty();
        config["Certificate"].setString(cert1Data);
        config["Key"].setString(key1Data);
        packer.signStage(config, true);

        Util::ByteArray inputData(makeCompressibleData());
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));


        TestFileUnpacker unpacker;
        unpacker.acceptedSignatures.insert(Cryptography::sha512(QSslCertificate(cert1Data)));
        auto recoveredFile = IO::Access::buffer();
        QVERIFY(unpacker.exec(outputFile, recoveredFile));
        {
            auto outputData = recoveredFile->stream()->readAll();
            QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
        }
    }

#ifdef Q_OS_UNIX

    void invokePipeline()
    {
        TransferPack packer;
        Variant::Write config = Variant::Write::empty();
        config["Program"] = "/bin/sh";
        config["Arguments/#0"] = "-c";
        config["Arguments/#1"] = "cat -; /bin/echo -n ' Tail'";
        packer.invokeStage(config);

        Util::ByteArray inputData("Test data");
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));

        inputData += " Tail";
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(outputData.toQByteArrayRef().contains(inputData.toQByteArrayRef()));
        }

        {
            TestFileUnpacker unpacker;
            auto recoveredFile = IO::Access::buffer();
            QVERIFY(unpacker.exec(outputFile, recoveredFile));
            {
                auto outputData = recoveredFile->stream()->readAll();
                QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
            }
        }

        QTemporaryFile tapEndFile;
        QVERIFY(tapEndFile.open());
        QTemporaryFile tapStartFile;
        QVERIFY(tapStartFile.open());

        {
            TestFileUnpacker unpacker;

            config.setEmpty();
            config["Program"] = "tee";
            config["Arguments/#0"] = tapEndFile.fileName();
            unpacker.invokeAt(config, std::numeric_limits<int>::max());

            config.setEmpty();
            config["Program"] = "tee";
            config["Arguments/#0"] = tapStartFile.fileName();
            unpacker.invokeAt(config);

            auto recoveredFile = IO::Access::buffer();
            QVERIFY(unpacker.exec(outputFile, recoveredFile));
            {
                auto outputData = recoveredFile->stream()->readAll();
                QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
            }
        }

        {
            QCOMPARE(tapEndFile.readAll(), inputData.toQByteArrayRef());
        }

        {
            TestFileUnpacker unpacker2;
            auto recoveredFile = IO::Access::buffer();
            QVERIFY(unpacker2.exec(IO::Access::file(tapStartFile.fileName()), recoveredFile));
            {
                auto outputData = recoveredFile->stream()->readAll();
                QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
            }
        }
    }

#endif

    void standardUpload()
    {
        TransferPack packer;
        packer.compressStage();
        Variant::Write config = Variant::Write::empty();
        config["Certificate"].setString(cert1Data);
        config["Key"].setString(key1Data);
        packer.signStage(config);
        packer.checksumStage();

        Util::ByteArray inputData(makeCompressibleData());
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(outputData.size() < inputData.size());
        }

        QByteArray cert1Sig(Cryptography::sha512(QSslCertificate(cert1Data)));

        TestFileUnpacker unpacker;
        unpacker.certificateLookup.insert(cert1Sig, Variant::Root(cert1Data));
        unpacker.acceptedSignatures.insert(cert1Sig);

        unpacker.requireChecksum();
        unpacker.requireSignature(TransferUnpack::Signature_SingleOnly);
        unpacker.requireEncryption(TransferUnpack::Encryption_Default, true);
        unpacker.requireCompression();
        unpacker.requireEnd();

        auto recoveredFile = IO::Access::buffer();
        QVERIFY(unpacker.exec(outputFile, recoveredFile));
        {
            auto outputData = recoveredFile->stream()->readAll();
            QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
        }

        QCOMPARE(unpacker.returnedSignatures.size(), 1);
        QVERIFY(unpacker.returnedSignatures.contains(cert1Sig));
        QCOMPARE(unpacker.returnedSignatureCertificates.size(), 0);
    }

    void standardSynchronize()
    {
        TransferPack packer;
        packer.compressStage();
        Variant::Write config1 = Variant::Write::empty();
        config1["Certificate"].setString(cert1Data);
        config1["Key"].setString(key1Data);
        packer.encryptStage(config1);
        Variant::Write config2 = Variant::Write::empty();
        config2["Certificate"].setString(cert2Data);
        config2["Key"].setString(key2Data);
        packer.signStage(config2);
        packer.checksumStage();

        Util::ByteArray inputData(makeCompressibleData());
        auto inputFile = IO::Access::buffer(inputData);
        auto outputFile = IO::Access::buffer();
        QVERIFY(packer.exec(inputFile, outputFile));
        {
            auto outputData = outputFile->stream()->readAll();
            QVERIFY(outputData.size() < inputData.size());

            QVERIFY(!outputData.toQByteArrayRef().contains(QSslCertificate(cert1Data).toDer()));
            QVERIFY(outputData.toQByteArrayRef().contains(QSslCertificate(cert2Data).toDer()));
            QVERIFY(outputData.toQByteArrayRef()
                              .contains(Cryptography::sha512(QSslCertificate(cert1Data).toDer())));
            QVERIFY(!outputData.toQByteArrayRef()
                               .contains(Cryptography::sha512(QSslCertificate(cert2Data).toDer())));
        }

        QByteArray cert1Sig(Cryptography::sha512(QSslCertificate(cert1Data)));
        QByteArray cert2Sig(Cryptography::sha512(QSslCertificate(cert2Data)));

        TestFileUnpacker unpacker;
        unpacker.keyLookup.insert(cert1Sig, Variant::Root(key1Data));
        unpacker.acceptedSignatures.insert(cert2Sig);

        unpacker.requireChecksum();
        unpacker.requireSignature(TransferUnpack::Signature_SingleOnly |
                                          TransferUnpack::Signature_RequireIdentification);
        unpacker.requireEncryption();
        unpacker.requireCompression();
        unpacker.requireEnd();

        auto recoveredFile = IO::Access::buffer();
        QVERIFY(unpacker.exec(outputFile, recoveredFile));
        {
            auto outputData = recoveredFile->stream()->readAll();
            QCOMPARE(outputData.toQByteArrayRef(), inputData.toQByteArrayRef());
        }

        QCOMPARE(unpacker.returnedSignatures.size(), 1);
        QVERIFY(unpacker.returnedSignatures.contains(cert2Sig));
        QCOMPARE(unpacker.returnedSignatureCertificates.size(), 0);

        QCOMPARE(unpacker.returnedKeys.size(), 1);
        QVERIFY(unpacker.returnedKeys.contains(cert1Sig));
    }
};

QTEST_MAIN(TestPackage)

#include "package.moc"
