/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QObject>
#include <QTest>
#include <QtAlgorithms>

#include "transfer/download.hxx"
#include "core/textsubstitution.hxx"
#include "core/qtcompat.hxx"

//#define TEST_NETWORK

using namespace CPD3;
using namespace CPD3::Transfer;
using namespace CPD3::Data;

class TestDownloader : public FileDownloader {
    QString temporaryFile;
public:
    TestDownloader() : FileDownloader()
    { }

    virtual ~TestDownloader()
    { }

protected:
    void pushTemporaryOutput(const QFileInfo &fileName) override
    {
        temporaryFile = fileName.absoluteFilePath();
    }

    void popTemporaryOutput() override
    {
        temporaryFile = QString();
    }

    QString applySubstitutions(const QString &input) const override
    {
        TextSubstitutionStack subs;
        subs.setFile("TEMP", temporaryFile);
        return subs.apply(input);
    }

    class MatchReplace : public TextSubstitution {
        QList<TimeCapture> &captures;
    public:
        MatchReplace(QList<TimeCapture> &c) : captures(c)
        { }

        virtual ~MatchReplace()
        { }

    protected:
        QString substitution(const QStringList &elements) const override
        { return FileDownloader::getTimeCapture(elements, captures); }
    };

    QString applyMatching(const QString &input, QList<TimeCapture> &captures) const override
    {
        MatchReplace subs(captures);
        return subs.apply(input);
    }
};

class TestDownload : public QObject {
Q_OBJECT

    QString tmpPath;
    QDir tmpDir;

    void recursiveRemove(const QDir &base)
    {
        if (base.path().isEmpty() || base == QDir::current())
            return;
        QFileInfoList list(base.entryInfoList(QDir::Files | QDir::NoDotAndDotDot));
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            if (!QFile::remove(rm->absoluteFilePath())) {
                qDebug() << "Failed to remove file" << rm->absoluteFilePath();
                continue;
            }
        }
        list = base.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            QDir subDir(base);
            if (!subDir.cd(rm->fileName())) {
                qDebug() << "Failed to change directory" << rm->absoluteFilePath();
                continue;
            }
            recursiveRemove(subDir);
            base.rmdir(rm->fileName());
        }
    }

    static bool writeData(const QString &target, const QByteArray &data)
    {
        QFile output(target);
        if (!output.open(QIODevice::WriteOnly))
            return false;

        const char *ptr = data.constData();
        qint64 remaining = data.size();
        while (remaining > 0) {
            qint64 n = output.write(ptr, remaining);
            if (n < 0)
                return false;
            Q_ASSERT(n <= remaining);
            ptr += n;
            remaining -= n;
        }

        output.flush();
        output.close();
        return true;
    }

    static QByteArray readAll(const QFileInfo &info)
    {
        QFile file(info.filePath());
        if (!file.open(QIODevice::ReadOnly))
            return QByteArray();
        return file.readAll();
    }

    static void sleepUntilModifiedChanged()
    {
        QTest::qSleep(1250);
    }

    static bool sortFilesByName(const DownloadFile *a, const DownloadFile *b)
    {
        return a->fileName() < b->fileName();
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        tmpDir = QDir(QDir::tempPath());
        tmpPath = "CPD3Download-";
        tmpPath.append(Random::string());
        tmpDir.mkdir(tmpPath);
        QVERIFY(tmpDir.cd(tmpPath));
    }

    void cleanupTestCase()
    {
        recursiveRemove(tmpDir);
        tmpDir.cdUp();
        tmpDir.rmdir(tmpPath);
    }

    void init()
    {
        recursiveRemove(tmpDir);
    }

    void cleanup()
    {
        recursiveRemove(tmpDir);
    }

    void localWritable()
    {
        tmpDir.mkdir("files");
        QDir fileDir(tmpDir.absolutePath() + "/files");

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "Local";
        config["Writable"] = true;
        config["Path"] = fileDir.path();
        config["Match"] = "FILE_${TIME}\\.dat";
        config["MinimumAge"].setInt64(0);

        {
            TestDownloader dl;
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();

            QCOMPARE((int)files.size(), 0);
        }

        QByteArray state;
        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto  files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 0);
        }

        QVERIFY(writeData(fileDir.filePath("INVALID.dat"), "INVALID"));
        QVERIFY(writeData(fileDir.filePath("FILE_20160101T000000.dat"), "File data 1"));
        double wtime = Time::time();

        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000000.dat"));
            QCOMPARE(file->getFileTime(), 1451606400.0);
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QVERIFY(file->writable());
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 1"));
        }

        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000000.dat"));
            QCOMPARE(file->getFileTime(), 1451606400.0);
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 1"));
        }

        fileDir.remove("FILE_20160101T000000.dat");

        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 0);
        }
    }

    void localReadonly()
    {
        tmpDir.mkdir("files");
        QDir fileDir(tmpDir.absolutePath() + "/files");

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "Local";
        config["Writable"] = false;
        config["Path"] = fileDir.path();
        config["Match"] = "FILE_${TIME}\\.dat";
        config["MinimumAge"].setInt64(0);

        {
            TestDownloader dl;
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();

            QCOMPARE((int)files.size(), 0);
        }

        double current = Time::time();
        double prior = FP::undefined();
        QByteArray state;
        {
            TestDownloader dl;
            dl.restoreState(state);
            dl.setupExecutionBounds(prior, current);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 0);
        }

        sleepUntilModifiedChanged();
        QVERIFY(writeData(fileDir.filePath("FILE_20160101T000000.dat"), "File data 1"));
        double wtime = Time::time();

        prior = current;
        current = Time::time();
        {
            TestDownloader dl;
            dl.setupExecutionBounds(prior, current);
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000000.dat"));
            QCOMPARE(file->getFileTime(), 1451606400.0);
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 1"));
            QVERIFY(!file->writable());
        }

        prior = current;
        current = Time::time();
        {
            TestDownloader dl;
            dl.setupExecutionBounds(prior, current);
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 0);
        }

        sleepUntilModifiedChanged();
        QVERIFY(writeData(fileDir.filePath("FILE_20160101T000001.dat"), "File data 2"));
        wtime = Time::time();

        prior = current;
        current = Time::time();
        {
            TestDownloader dl;
            dl.setupExecutionBounds(prior, current);
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000001.dat"));
            QCOMPARE(file->getFileTime(), 1451606401.0);
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 2"));
        }

        prior = current;
        current = Time::time();
        {
            TestDownloader dl;
            dl.setupExecutionBounds(prior, current);
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 0);
        }

        current = Time::time();
        prior = FP::undefined();
        {
            TestDownloader dl;
            dl.setupExecutionBounds(prior, current);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 2);
        }

        prior = current;
        current = Time::time();
        {
            TestDownloader dl;
            dl.setupExecutionBounds(prior, current);
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 0);
        }
    }

    void localWalk()
    {
        tmpDir.mkdir("files1");
        tmpDir.mkdir("files2");
        tmpDir.mkdir("miss");
        QDir fDir1(tmpDir.absolutePath() + "/files1");
        QDir fDir2(tmpDir.absolutePath() + "/files2");
        QDir missDir(tmpDir.absolutePath() + "/miss");

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "Local";
        config["Writable"] = true;
        config["Path"] = tmpDir.absolutePath();
        config["Match"] = "FILE_${TIME}\\.dat";
        config["MatchPath/#0"] = "files\\d";
        config["MinimumAge"].setInt64(0);

        {
            TestDownloader dl;
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();

            QCOMPARE((int)files.size(), 0);
        }

        QByteArray state;
        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 0);
        }

        QVERIFY(writeData(missDir.filePath("FILE_20160101T000000.dat"), "MISS"));
        QVERIFY(writeData(fDir1.filePath("FILE_20160101T000000.dat"), "File data 1"));
        double wtime = Time::time();

        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000000.dat"));
            QCOMPARE(file->getFileTime(), 1451606400.0);
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 1"));
        }

        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000000.dat"));
            QCOMPARE(file->getFileTime(), 1451606400.0);
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 1"));
        }

        fDir1.remove("FILE_20160101T000000.dat");
        missDir.remove("FILE_20160101T000000.dat");

        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 0);
        }

        QVERIFY(writeData(fDir2.filePath("FILE_20160101T000001.dat"), "File data 2"));
        wtime = Time::time();

        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000001.dat"));
            QCOMPARE(file->getFileTime(), 1451606401.0);
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 2"));
        }

        config["MatchPath"].remove();
        config["Recursive"] = true;

        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000001.dat"));
            QCOMPARE(file->getFileTime(), 1451606401.0);
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 2"));
        }
    }

    void localSingle()
    {
        QString fileName(tmpDir.filePath("FILE_20160101T000000.dat"));

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "Single";
        config["Source"] = "Local";
        config["File"] = fileName;
        config["Writable"] = true;

        {
            TestDownloader dl;
            QVERIFY(!dl.exec(config));
            auto files = dl.takeFiles();
            QCOMPARE((int)files.size(), 0);
        }

        QByteArray state;
        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(!dl.exec(config));
            auto files = dl.takeFiles();
            QCOMPARE((int)files.size(), 0);
        }

        QVERIFY(writeData(fileName, "File data 1"));
        double wtime = Time::time();

        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000000.dat"));
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QVERIFY(file->writable());
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 1"));
        }

        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000000.dat"));
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QVERIFY(file->writable());
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 1"));
        }

        config["Match"] = ".*FILE_${TIME}\\.dat";
        {
            TestDownloader dl;
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000000.dat"));
            QCOMPARE(file->getFileTime(), 1451606400.0);
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QVERIFY(file->writable());
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 1"));
        }

        {
            TestDownloader dl;
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000000.dat"));
            QCOMPARE(file->getFileTime(), 1451606400.0);
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QVERIFY(file->writable());
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 1"));
        }
    }

    void localList()
    {
        QString listName(tmpDir.filePath("LIST.txt"));
        QString fileName1(tmpDir.filePath("FILE_20160101T000000.dat"));
        QString fileName2(tmpDir.filePath("FILE_20160101T000001.dat"));

        Variant::Write config = Variant::Write::empty();
        config["Type"] = "List";
        config["Source"] = "Local";
        config["File"] = listName;
        config["Fetch/Source"] = "Local";
        config["Fetch/Writable"] = true;
        config["MinimumAge"].setInt64(0);
        config["HonorModified"] = true;

        {
            TestDownloader dl;
            QVERIFY(!dl.exec(config));
            auto files = dl.takeFiles();
            QCOMPARE((int)files.size(), 0);
        }

        QByteArray state;
        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(!dl.exec(config));
            auto files = dl.takeFiles();
            QCOMPARE((int)files.size(), 0);
        }

        QVERIFY(writeData(listName, fileName1.toUtf8()));
        QVERIFY(writeData(fileName1, "File data 1"));
        double wtime = Time::time();

        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000000.dat"));
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QVERIFY(file->writable());
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 1"));
        }

        {
            TestDownloader dl;
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000000.dat"));
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QVERIFY(file->writable());
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 1"));
        }

        config["Match"] = ".*FILE_${TIME}\\.dat";
        {
            TestDownloader dl;
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000000.dat"));
            QCOMPARE(file->getFileTime(), 1451606400.0);
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QVERIFY(file->writable());
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 1"));
        }

        double current = Time::time();
        double prior = FP::undefined();
        {
            TestDownloader dl;
            dl.setupExecutionBounds(prior, current);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000000.dat"));
            QCOMPARE(file->getFileTime(), 1451606400.0);
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QVERIFY(file->writable());
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 1"));
        }

        sleepUntilModifiedChanged();
        QVERIFY(writeData(listName, fileName1.toUtf8() + "\n" + fileName2.toUtf8()));
        QVERIFY(writeData(fileName2, "File data 2"));

        prior = current;
        current = Time::time();
        {
            TestDownloader dl;
            dl.setupExecutionBounds(prior, current);
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("FILE_20160101T000001.dat"));
            QCOMPARE(file->getFileTime(), 1451606401.0);
            QVERIFY(fabs(file->getModifiedTime() - wtime) <= 60.0);
            QVERIFY(file->writable());
            QCOMPARE(readAll(file->fileInfo()), QByteArray("File data 2"));
        }
    }

#ifdef TEST_NETWORK

    void ftpSingle()
    {
        Variant::Write config = Variant::Write::empty();
        config["Type"] = "FTP";
        config["Hostname"] = "aftp.cmdl.noaa.gov";
        config["Path"] = "/aerosol";
        config["Match"] = "README";
        config["MinimumAge"].setInt64(0);

        {
            TestDownloader dl;
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("README"));
            QVERIFY(FP::defined(file->getModifiedTime()));
            QVERIFY(!file->writable());
            QVERIFY(readAll(file->fileInfo()).contains("Aerosols group"));
        }

        double current = Time::time();
        double prior = FP::undefined();
        QByteArray state;
        {
            TestDownloader dl;
            dl.restoreState(state);
            dl.setupExecutionBounds(prior, current);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QCOMPARE(file->fileName(), QString("README"));
            QVERIFY(FP::defined(file->getModifiedTime()));
            QVERIFY(!file->writable());
            QVERIFY(readAll(file->fileInfo()).contains("Aerosols group"));
        }

        prior = current;
        current = Time::time();
        {
            TestDownloader dl;
            dl.setupExecutionBounds(prior, current);
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 0);
        }
    }

    void ftpWalk()
    {
        Variant::Write config = Variant::Write::empty();
        config["Type"] = "FTP";
        config["Hostname"] = "aftp.cmdl.noaa.gov";
        config["Path"] = "/aerosol";
        config["Match"] = "README(?:\\.txt)?";
        config["MatchPath/#0"] = "smo";
        config["MatchPath/#1"] = "(archive)|(minute)";
        config["MinimumAge"].setInt64(0);

        {
            TestDownloader dl;
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();

            QCOMPARE((int)files.size(), 2);
            std::sort(files.begin(), files.end(), sortFilesByName);
            auto file = std::move(files.front());
            files.erase(files.begin());
            QCOMPARE(file->fileName(), QString("README"));
            QVERIFY(FP::defined(file->getModifiedTime()));
            QVERIFY(!file->writable());
            QVERIFY(readAll(file->fileInfo()).contains("Aerosols group"));

            file = std::move(files.front());
            files.erase(files.begin());
            QCOMPARE(file->fileName(), QString("README.txt"));
            QVERIFY(FP::defined(file->getModifiedTime()));
            QVERIFY(!file->writable());
            QVERIFY(readAll(file->fileInfo()).startsWith("The highest frequency"));
        }

        double current = Time::time();
        double prior = FP::undefined();
        QByteArray state;
        {
            TestDownloader dl;
            dl.restoreState(state);
            dl.setupExecutionBounds(prior, current);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 2);
            std::sort(files.begin(), files.end(), sortFilesByName);
            auto file = std::move(files.front());
            files.erase(files.begin());
            QCOMPARE(file->fileName(), QString("README"));
            QVERIFY(FP::defined(file->getModifiedTime()));
            QVERIFY(!file->writable());
            QVERIFY(readAll(file->fileInfo()).contains("Aerosols group"));

            file = std::move(files.front());
            files.erase(files.begin());
            QCOMPARE(file->fileName(), QString("README.txt"));
            QVERIFY(FP::defined(file->getModifiedTime()));
            QVERIFY(!file->writable());
            QVERIFY(readAll(file->fileInfo()).startsWith("The highest frequency"));
        }

        prior = current;
        current = Time::time();
        {
            TestDownloader dl;
            dl.setupExecutionBounds(prior, current);
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 0);
        }
    }

    void httpGET()
    {
        Variant::Write config = Variant::Write::empty();
        config["Type"] = "Single";
        config["Source"] = "GET";
        config["File"] = "http://www.esrl.noaa.gov/gmd/aero/index.html";

        {
            TestDownloader dl;
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QVERIFY(!file->writable());
            QVERIFY(readAll(file->fileInfo()).contains("Aerosols Group"));
        }

        double current = Time::time();
        double prior = FP::undefined();
        QByteArray state;
        {
            TestDownloader dl;
            dl.restoreState(state);
            dl.setupExecutionBounds(prior, current);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QVERIFY(!file->writable());
            QVERIFY(readAll(file->fileInfo()).contains("Aerosols Group"));
        }

        prior = current;
        current = Time::time();
        {
            TestDownloader dl;
            dl.setupExecutionBounds(prior, current);
            dl.restoreState(state);
            QVERIFY(dl.exec(config));
            auto files = dl.takeFiles();
            state = dl.saveState();

            QCOMPARE((int)files.size(), 1);
            auto &file = files.front();
            QVERIFY(!file->writable());
            QVERIFY(readAll(file->fileInfo()).contains("Aerosols Group"));
        }
    }
#endif
};

QTEST_MAIN(TestDownload)

#include "download.moc"
