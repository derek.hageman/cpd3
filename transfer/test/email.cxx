/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "transfer/email.hxx"
#include "transfer/smtp.hxx"

using namespace CPD3::Transfer;

static const char
        *pngData = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAIAAAD8GO2jAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAL"
        "EwAACxMBAJqcGAAAAAd0SU1FB90FCBIXER+x0bQAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRo"
        "IEdJTVBXgQ4XAAAH60lEQVRIx6VWWWyc1RU+5y7/OjOe8cx4iYMd24FAEqLEYUmzFAQF8lC1lRqq"
        "tmoLLQ+lQn2AShSVh6K2AiEBUlFBpWJREaKtoG0KFAIGEgIiiV0gTkgcOyGYOE68zIzjWf/l3nv6"
        "YJRYSUBRe17v1fed7yzfvUhEcL4gopmZ6blyuXtJtxCCiCYmJp5+5YHLV6y+btV3CoWZex+6o609"
        "f+/PHlFa1cJyR0uXbduMsdMIYRjW6lUxj3V07JPurp7Tx0T06hsv//6Fu6WwvrHxlo19mweG3n19"
        "6OnFXR09+Vtc1334id8ePDxUDttHPjmw/qpriFo552elaNu2bduCiLZu++v2E3+6sfenmzduEUIA"
        "wJ7BXU/0321YHKjohXcee2Hn45bLbY8zZI1qVCgUduzalsw6xVJp63tPdS7qSWcyqWTqvJXgN2y+"
        "7p+f/qZWqR+e/ECi37toBSL2v/PqB4d2Wg43GphgiWYLAaXDW1vaWJy478F7IiwDEjIan/hs285/"
        "hHXNQORzLefqYIMjb9bnYsYxVI19J94qlKaJ6MTUsfpcHAfaaCJDKjTSYalEk+s7CdF8sjgGBIyj"
        "MaQiM1su/aX/0d89c/vQwcFzFbADxwZUoFVkVGzKtdlTlZIxplqrxKEJqlrHBhlqRWhEyk8vtvu2"
        "bvsbACBHFRvOUdjM8YXlciODw5MfnUsgavyEjgwgqMiUytODh95O+zmLu4JZTJCTEMjQGHJ9J9/U"
        "MTx0eO+BAcvljKGwmHQYF0xYzHbkko7e3q5l5+nBpRtzXLA4NCo0KPWU/ni0vr1v5boWt7ccT4Zx"
        "XcfEOHpJmUm0Y82fLI0rCi2PWy4XFvfSsjmd27Du2puu/u6q7o2WtM5WkMzblelQOjwOjI4pqOmY"
        "wnSm6Y5bf/DOrrWP/f1XQVhhHKI4GisM9Sxa9f1v3zo2NdzSmrNsORdOV4LZ5T19N6z8UVuuAxHP"
        "U6L5OjYqsYqNikzCYZ6V8FjWdd3rN900dHDPS+8+6ySkVqZWbRyq7W9Z1XXXDx9IJpKciyAIJovj"
        "2XRLczp3XnQAYF7KaetJBxUdBzpq6LihPZnK2G2IKKX82vpvMmTpNpsxjEPdnunetPJb+VyL63qW"
        "ZaVSqUu6V2Qz+S9CBwBx7bLvcWMvTR4Z2D0QiFJzc667eU1rugsAEHHF8pVXXL7hs+JBxephXVut"
        "qY6Wzi+BO9dv+PN/fOWSRX2rl1xzcfsaMtDTvmJDz5ZsOj9/AxHXXvbVSjHcPfh+UFUcrOvWfT2R"
        "SF4IdLFYfLz/LjxtdlrrSqUspfQ8fz5HYwwiEtHo6Mhtv75JaxJg3XbzL265+faFpnZuRFH09vuv"
        "HQhfmmNj4szAcu77CSnlwizmgaYKk05S6JiCavTWnq1Xrty0/LIV80ksLJdSanp6qlqrvn9k63F/"
        "h3FUXIvFQmZjjFJq3u+01vPGgoiGNGNokACgYU2/NPDkzpGMYaEt/FZ7aa6pgyMvlKbfO/piSYx4"
        "aWkUYRn8JhsAP7friYnjw0f2E9CSi5Z2d/YAwEIpHW2dpLiX5ss25LXSI6d2qikdBwYRokATARki"
        "A8JiybwdB1rHZCd4VNeWJwQR9e/49/bKQ+VSPQ5hjdp0c+aeXHN+obKuzq7l3VeoxUdtn6mIeRms"
        "FiPGlZexEKBaisozYRRoLjGoKMtlRFA8Ftq+8JokC8NwJjpq+Zxxlm51HZ5wLOfsWRZi/cobq6Vg"
        "9mSjOF6fPd6IAx0HZvZ4ozYbGU2ZdiezyKmfiqulcG4qDGs6mbeNptJEQ0zPTFmUjBpKuqxaCrUP"
        "tn02gdb6SLCTC14crxlNyMD2BACEdRUHWlgsauiooQkgahgijQyCY7GTlH7GElLIRi0sFmqMo5uS"
        "E/o/w2Mfrrr46oUEJ0+e+PjIoJeW+W6/UggrhTCoBlwwxjGoRsJiiGg0IUJYV4BQKZC0WdTQjTku"
        "stncyLG9DRW7KWk01ePycOP1S6M1lnXGFz3PD2bk7Mkyl8z2eLrdZQyqs3FU1yoyAKiV/nxaERBR"
        "xyYONRBwqflXNl7Zv+9ZN8OSWQcIjUJOVm/T1Qn/zLq6rmvqcu/obgIDAEFFRXWtQm05PL8kwQQK"
        "wQAhrGtEiBqaCABQxcYY4t6Gw4m8yHX5zbkm17OzuWw20bY0vS7hn3nEEbGn85KDwwfHxo/o2MSh"
        "UZERkkmXN8qKDCEAE+j4ggvmJIQxAAA6JjIg2npTjLh0hJR2W+oiQe5FVl8mlT2rz77v3/nj+375"
        "8GfHpkalyxlDFZtTk4HRZHuCiIwiAGACW3sTjCMAGE1hTeEfdv/Ed5KxjrLu4k6vL2/1tme7FjZg"
        "4Z4fGh2+/6mfj5/8VGtjNAECIkiHx4FGBNsTXavTdkIwBmFdM44qNHjg8IeRiqrsxNLmq/KZtnP/"
        "HWdxHP306J9ffmTfyODMzLRSGgCExbQiabOOy5Je2iJNyMDyBRkKqwqJ6LShXojRE1EQBPsOfPTi"
        "G8/sHx2oBLOOL7hkXCAgOj63fRGHulFWXKKbkvhFf9MLodn+Xv+T/7p/tjqFDC2Xa2Xqc7G0ORco"
        "Xe6nZTJn/48Epzd8x7tvPvjcndJXuSW+tFkU6KCskIGOiQn0MxZqrb/89fjyCMPw0efu2z/7muVx"
        "LpmwmFZmbjJAxGTeJkOsWCrA/xGWZV2/dgsgIEMypJWpFiInITIdLhcIAP8FkYMeKFMn2B0AAAAA"
        "SUVORK5CYII=";

class TestEmail : public QObject {
Q_OBJECT

private slots:

    void basic()
    {
        EmailBuilder builder;

        builder.setSubject("Test Message");
        builder.addTo("\"Derek Hageman\" <Derek.Hageman@noaa.gov>");

        builder.setEscapeMode(EmailBuilder::Escape_HTML);
        builder.setBody(QString::fromUtf8(QByteArray("<html>"
                                                             "Some message data: \xCE\xA3!<br><br>"
                                                             "<img src=\"cid:img1@cmdl.noaa.gov\">"
                                                             "</html>")), "text/html");
        builder.addAttachment(QByteArray::fromBase64(QByteArray(pngData)), "attached.png",
                              "image/png", "<img1@cmdl.noaa.gov>");

        QByteArray constructed(builder.construct());
        QVERIFY(constructed.contains("Derek.Hageman@noaa.gov"));
        QVERIFY(constructed.contains("Some message data"));

#if 0
        SMTPSend sender;
        sender.setFrom(QString());
        builder.queue(&sender);
        sender.waitCompleted();
#endif
    }
};

QTEST_MAIN(TestEmail)

#include "email.moc"
