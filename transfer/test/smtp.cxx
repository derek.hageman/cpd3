/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QTemporaryFile>

#include "transfer/smtp.hxx"
#include "core/qtcompat.hxx"

//#define TEST_SENDING
//#define TEST_RELAY
#define MESSAGE_RECIPIENT   "Derek.Hageman@noaa.gov"

#ifdef TEST_SENDING

#ifdef TEST_RELAY
#define RELAY_HOSTNAME      "smtp.gmail.com"
#define RELAY_PORT          465
#define RELAY_USERNAME      "XXX@noaa.gov"
#define RELAY_PASSWORD      "XXX"
#define RELAY_USETLS        true
#endif

#endif

using namespace CPD3;
using namespace CPD3::Transfer;
using namespace CPD3::Data;

class TestSMTP : public QObject {
Q_OBJECT

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void program()
    {
        Variant::Write relay = Variant::Write::empty();
        relay["Program"].setString("/bin/true");
        //relay["Program"].setString("tee /tmp/emailout");
        SMTPSend send(relay);

        send.setFrom();
        send.addMessage(QStringList() << MESSAGE_RECIPIENT,
                        QByteArray("Date: Mon, 6 May 2013 12:00:00 -0600\r\n"
                                   "Subject: Test\r\n"
                                   "\r\n"
                                   "Program Message"));

        send.start();
        send.wait();
    }

#ifdef TEST_SENDING
    void direct() {
        SMTPSend send;
        
        send.setFrom();
        send.addMessage(QStringList() << MESSAGE_RECIPIENT,
            QByteArray(
            "Date: Mon, 6 May 2013 12:00:00 -0600\r\n"
            "Subject: Test\r\n"
            "MIME-Version: 1.0\r\n"
            "Content-Type: text/html\r\n"
            "\r\n"
            "<html><body>\r\n"
            "Stuff<br>Things\r\n"
            "</body></html>"));

        send.start();
        send.wait();
    }
#endif

#ifdef RELAY_HOSTNAME
    void relay() {
        Variant::Write relay = Variant::Write::empty();
        relay["Hostname"].setString(RELAY_HOSTNAME);
#ifdef RELAY_PORT
        relay["Port"].setInt64(RELAY_PORT);
#endif
#ifdef RELAY_USERNAME
        relay["Username"].setString(RELAY_USERNAME);
#endif
#ifdef RELAY_PASSWORD
        relay["Password"].setString(RELAY_PASSWORD);
#endif
#ifdef RELAY_USETLS
        relay["TLS"].setBool(RELAY_USETLS);
#endif
        SMTPSend send(relay);

        send.setFrom(RELAY_USERNAME);
        send.addMessage(QStringList() << MESSAGE_RECIPIENT,
            QByteArray(
            "Date: Mon, 6 May 2013 12:00:00 -0600\r\n"
            "Subject: Test\r\n"
            "\r\n"
            "Relayed Message"));

        send.start();
        send.wait();
    }

#ifdef RELAY_PASSWORD
    void relayPasswordFile() {
        Variant::Write relay = Variant::Write::empty();
        relay["Hostname"].setString(RELAY_HOSTNAME);
#ifdef RELAY_PORT
        relay["Port"].setInt64(RELAY_PORT);
#endif
#ifdef RELAY_USERNAME
        relay["Username"].setString(RELAY_USERNAME);
#endif
#ifdef RELAY_USETLS
        relay["TLS"].setBool(RELAY_USETLS);
#endif

        QTemporaryFile file;
        QVERIFY(file.open());
        file.write(RELAY_PASSWORD);
        file.close();

        relay["PasswordFile"].setString(file.fileName());

        SMTPSend send(relay);

        send.setFrom(RELAY_USERNAME);
        send.addMessage(QStringList() << MESSAGE_RECIPIENT,
            QByteArray(
            "To: " MESSAGE_RECIPIENT "\r\n"
            "Date: Mon, 6 May 2013 12:00:00 -0600\r\n"
            "Subject: Test\r\n"
            "\r\n"
            "Relayed File Message"));

        send.start();
        send.wait();
    }
#endif

#endif
};

QTEST_MAIN(TestSMTP)

#include "smtp.moc"
