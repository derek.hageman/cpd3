/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QHostInfo>
#include <QTcpServer>
#include <QTcpSocket>
#include <QLoggingCategory>
#include <QTimer>

#include "transfer/ftp.hxx"
#include "core/number.hxx"
#include "core/waitutils.hxx"
#include "core/threadpool.hxx"
#include "core/qtcompat.hxx"
#include "core/timeutils.hxx"

Q_LOGGING_CATEGORY(log_test, "cpd3.test")

using namespace CPD3;
using namespace CPD3::Transfer;
using namespace CPD3::Data;

class TestFTPManager : public QRunnable {
    int port;

    std::mutex mutex;

    QString waitForNextCommand(QTcpSocket *socket)
    {
        ElapsedTimer to;
        to.start();
        QString result;
        for (;;) {
            int remaining = 30000 - to.elapsed();
            if (remaining <= 0) {
                qCDebug(log_test) << "Command connection timeout";
                return QString();
            }
            QEventLoop el;
            QTimer timer;
            QObject::connect(&timer, SIGNAL(timeout()), &el, SLOT(quit()), Qt::QueuedConnection);
            QObject::connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), &el,
                             SLOT(quit()), Qt::QueuedConnection);
            QObject::connect(socket, SIGNAL(readyRead()), &el, SLOT(quit()), Qt::QueuedConnection);
            if (socket->bytesAvailable() <= 0) {
                if (socket->state() != QAbstractSocket::ConnectedState) {
                    qCDebug(log_test) << "Command connection closed:" << socket->errorString();
                    break;
                }
                timer.start(remaining);
                el.exec();
                continue;
            }

            char c = 0;
            if (!socket->getChar(&c))
                return QString();
            if (c == '\n' || c == '\r') {
                if (!result.isEmpty())
                    return result;
                continue;
            }
            result.append(c);
        }
        return QString();
    }

    bool writeData(QTcpSocket *socket, const QByteArray &data)
    {
        ElapsedTimer to;
        to.start();

        int offset = 0;
        while (offset < data.size()) {
            int remaining = 30000 - to.elapsed();
            if (remaining <= 0)
                return false;
            qint64 n = socket->write(data.constData() + offset, data.size() - offset);
            if (n < 0)
                return false;
            offset += (int) n;
        }
        socket->flush();

        while (socket->bytesToWrite() > 0) {
            if (socket->state() == QAbstractSocket::UnconnectedState)
                break;
            int remaining = 30000 - to.elapsed();
            if (remaining <= 0)
                return false;
            socket->waitForBytesWritten(remaining);
        }

        return true;
    }

    QByteArray readAllData(QTcpSocket *socket)
    {
        ElapsedTimer to;
        to.start();
        QByteArray result;
        static const int blockSize = 65536;
        for (;;) {
            int remaining = 30000 - to.elapsed();
            if (remaining <= 0)
                return result;
            int original = result.size();
            result.resize(original + blockSize);
            qint64 n = socket->read(result.data() + original, blockSize);
            if (n > 0) {
                result.resize(original + (int) n);
                continue;
            }
            result.resize(original);
            if (socket->bytesAvailable() > 0)
                continue;

            if (socket->state() == QAbstractSocket::UnconnectedState)
                return result;
            socket->waitForReadyRead(remaining);
        }
        return result;
    }

    QTcpSocket *commandSocket;
    QString dataHost;
    quint16 dataPort;

    QTcpSocket *makeDataConnection(QTcpServer &server)
    {
        if (dataHost.isEmpty()) {
            qCDebug(log_test) << "Waiting for data connection on port" << server.serverPort();

            if (!server.hasPendingConnections() && !server.waitForNewConnection(30000)) {
                writeData(commandSocket, QByteArray("425 Connection failed"));
                qCDebug(log_test) << "No data connection received";
                return NULL;
            }
            QTcpSocket *dataSocket = server.nextPendingConnection();
            if (!dataSocket) {
                writeData(commandSocket, QByteArray("425 Connection failed"));
                qCDebug(log_test) << "No data connection received";
                return NULL;
            }
            if (!dataSocket->waitForConnected(30000)) {
                writeData(commandSocket, QByteArray("425 Connection failed ") +
                        dataSocket->errorString().toUtf8() +
                        "\n");
                qCDebug(log_test) << "Data connection failed:" << dataSocket->errorString();
                dataSocket->deleteLater();
                return NULL;
            }
            qCDebug(log_test) << "Data connection opened";

            return dataSocket;
        } else {
            /* This doesn't currently work because the FTP client wants
             * a connection from the server port - 1, which we can't
             * bind explicitly via Qt's API. */

            qCDebug(log_test) << "Initiating data connection to" << dataHost << ":" << dataPort;

            QTcpSocket *dataSocket = new QTcpSocket(&server);
            dataSocket->connectToHost(dataHost, dataPort);
            if (!dataSocket->waitForConnected(30000)) {
                writeData(commandSocket, QByteArray("425 Connection failed ") +
                        dataSocket->errorString().toUtf8() +
                        "\n");
                qCDebug(log_test) << "Data connection failed:" << dataSocket->errorString();
                dataSocket->deleteLater();
                return NULL;
            }
            qCDebug(log_test) << "Data connection opened";

            return dataSocket;
        }
    }

    bool receiveData(QByteArray &target, QTcpServer &server)
    {
        QTcpSocket *dataSocket = makeDataConnection(server);
        if (dataSocket == NULL)
            return false;

        target = readAllData(dataSocket);
        qCDebug(log_test) << "Received" << target.size() << "bytes";

        dataSocket->disconnectFromHost();
        dataSocket->deleteLater();
        return true;
    }

    bool sendData(const QByteArray &source, QTcpServer &server)
    {
        QTcpSocket *dataSocket = makeDataConnection(server);
        if (dataSocket == NULL)
            return false;

        if (!writeData(dataSocket, source)) {
            writeData(commandSocket,
                      QByteArray("425 Write failed ") + dataSocket->errorString().toUtf8() + "\n");
            return false;
        }
        qCDebug(log_test) << "Transmitted" << source.size() << "bytes";

        dataSocket->disconnectFromHost();
        if (dataSocket->state() != QAbstractSocket::UnconnectedState)
            dataSocket->waitForDisconnected(30000);
        dataSocket->deleteLater();
        return true;
    }

public:
    QSet<QString> directories;
    QHash<QString, QByteArray> receivedData;
    QHash<QString, QByteArray> dataToSend;
    QHash<QString, QByteArray> listData;
    QHash<QString, QByteArray> nlstData;
    QHash<QString, double> mdtmData;
    QHash<QString, qint64> sizeData;
    bool disableEPSV;
    bool disablePASV;
    bool disableEPRT;
    bool disablePORT;
    bool interruptTransfer;
    QByteArray connectionString;

    TestFTPManager() : port(0)
    {
        setAutoDelete(false);

        disableEPSV = false;
        disablePASV = false;
        disableEPRT = false;
        disablePORT = false;
        interruptTransfer = false;

        connectionString = "220 OK\n";
    }

    virtual void run()
    {
        QTcpServer server;

        if (!server.listen(QHostAddress("127.0.0.1"))) {
            std::lock_guard<std::mutex> lock(mutex);
            port = -1;
            return;
        }

        {
            std::lock_guard<std::mutex> lock(mutex);
            port = server.serverPort();
        }

        qCDebug(log_test) << "Server listening on port" << port;

        if (!server.waitForNewConnection(30000)) {
            std::lock_guard<std::mutex> lock(mutex);
            port = -1;
            return;
        }

        commandSocket = server.nextPendingConnection();
        if (!commandSocket) {
            std::lock_guard<std::mutex> lock(mutex);
            port = -1;
            return;
        }
        if (!commandSocket->waitForConnected(30000)) {
            std::lock_guard<std::mutex> lock(mutex);
            port = -1;
            return;
        }

        writeData(commandSocket, connectionString);
        qCDebug(log_test) << "Command connection accepted";

        dataPort = 0;
        dataHost = QString();
        int offset = 0;

        ElapsedTimer to;
        to.start();
        while (to.elapsed() < 30000) {
            QString command(waitForNextCommand(commandSocket));
            if (command.isEmpty())
                break;
            qCDebug(log_test) << "Received command" << command;

            if (command.startsWith("USER ", Qt::CaseInsensitive)) {
                writeData(commandSocket, QByteArray("331 OK\n"));
                continue;
            } else if (command.startsWith("PASS ", Qt::CaseInsensitive)) {
                writeData(commandSocket, QByteArray("230 OK\n"));
                continue;
            } else if (command.startsWith("TYPE ", Qt::CaseInsensitive)) {
                writeData(commandSocket, QByteArray("200 OK\n"));
                continue;
            } else if (!disablePORT && command.startsWith("PORT ", Qt::CaseInsensitive)) {
                QString data(command.mid(5));
                data.replace('(', QString());
                data.replace(')', QString());
                QStringList fields(data.split(','));
                if (fields.size() < 6) {
                    writeData(commandSocket, QByteArray("500 Insufficient parameters\n"));
                    continue;
                }
                dataPort = fields.at(5).toInt();
                dataPort += fields.at(4).toInt() * 256;
                dataHost = QStringList(fields.mid(0, 4)).join(".");
                writeData(commandSocket, QByteArray("200 OK\n"));
                continue;
            } else if (!disablePASV && command.startsWith("PASV", Qt::CaseInsensitive)) {
                dataHost.clear();
                dataPort = 0;
                writeData(commandSocket, QByteArray("227 Entering Passive Mode (127,0,0,1,") +
                        QByteArray::number(port / 256) +
                        "," +
                        QByteArray::number(port % 256) +
                        ")\n");
                continue;
            } else if (!disableEPRT && command.startsWith("EPRT ", Qt::CaseInsensitive)) {
                QString data(command.mid(5));
                QChar delim(data.at(0));
                data = data.mid(1);
                QStringList fields(data.split(delim));
                if (fields.size() < 3) {
                    writeData(commandSocket, QByteArray("500 Insufficient parameters\n"));
                    continue;
                }
                dataPort = fields.at(2).toInt();
                dataHost = fields.at(1);
                writeData(commandSocket, QByteArray("200 OK\n"));
                continue;
            } else if (!disableEPSV && command.startsWith("EPSV", Qt::CaseInsensitive)) {
                dataHost.clear();
                dataPort = 0;
                writeData(commandSocket, QByteArray("229 Entering Extended Passive Mode (|||") +
                        QByteArray::number(port) +
                        "|)\n");
                continue;
            } else if (command.startsWith("MDTM ", Qt::CaseInsensitive)) {
                QString name(command.mid(5));
                double mtime = mdtmData.value(name, FP::undefined());
                if (!FP::defined(mtime)) {
                    writeData(commandSocket,
                              QByteArray("550 Could not get file modification time.\n"));
                    continue;
                }
                QDateTime dt(Time::toDateTime(mtime));
                writeData(commandSocket, QByteArray("213 ") +
                        QByteArray::number(dt.date().year()).rightJustified(4, '0') +
                        QByteArray::number(dt.date().month()).rightJustified(2, '0') +
                        QByteArray::number(dt.date().day()).rightJustified(2, '0') +
                        QByteArray::number(dt.time().hour()).rightJustified(2, '0') +
                        QByteArray::number(dt.time().minute()).rightJustified(2, '0') +
                        QByteArray::number(dt.time().second()).rightJustified(2, '0') +
                        "\n");
                continue;
            } else if (command.startsWith("SIZE ", Qt::CaseInsensitive)) {
                QString name(command.mid(5));
                qint64 size = sizeData.value(name, -1);
                if (size < 0) {
                    writeData(commandSocket, QByteArray("550 Could not get file size.\n"));
                    continue;
                }
                writeData(commandSocket, QByteArray("213 ") + QByteArray::number(size) + "\n");
                continue;
            } else if (command.startsWith("MKD ", Qt::CaseInsensitive)) {
                QString name(command.mid(4));
                QString parent(name.section('/', 0, -2));
                bool valid = parent.isEmpty();
                for (QSet<QString>::const_iterator dir = directories.constBegin(),
                        end = directories.constEnd(); dir != end; ++dir) {
                    if (dir->startsWith(name)) {
                        valid = false;
                        break;
                    }
                    if (*dir == parent)
                        valid = true;
                }
                if (!valid) {
                    writeData(commandSocket, QByteArray("550 Create directory operation failed\n"));
                    qCDebug(log_test) << "Directory" << name << "NOT created";
                } else {
                    writeData(commandSocket,
                              QByteArray("257 \"") + name.toUtf8() + "\" directory created\n");
                    directories.insert(name);
                    qCDebug(log_test) << "Directory" << name << "created";
                }
                continue;
            } else if (command.startsWith("REST ", Qt::CaseInsensitive)) {
                bool ok = false;
                int value = command.mid(5).toInt(&ok, 10);
                if (!ok) {
                    writeData(commandSocket, QByteArray("500 Invalid offset\n"));
                    continue;
                }
                offset = value;
                writeData(commandSocket, QByteArray("350 Restarting at " +
                                                            QByteArray::number(offset) +
                                                            ". Send STORE or RETRIEVE\n"));
                continue;
            } else if (command.startsWith("STOR ", Qt::CaseInsensitive)) {
                QString name(command.mid(5));

                qCDebug(log_test) << "Receiving" << name;

                writeData(commandSocket,
                          QByteArray("150 Opening connection for ") + name.toUtf8() + "\n");

                QByteArray data;
                if (!receiveData(data, server))
                    continue;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    receivedData[name] = receivedData[name].mid(0, offset);
                    receivedData[name].append(data);
                }
                offset = 0;

                if (interruptTransfer) {
                    interruptTransfer = false;
                    writeData(commandSocket, QByteArray("426 Transfer aborted.\n"));
                } else {
                    writeData(commandSocket, QByteArray("226 Completed\n"));
                }
                continue;
            } else if (command.startsWith("RETR ", Qt::CaseInsensitive)) {
                QString name(command.mid(5));
                std::lock_guard<std::mutex> lock(mutex);
                if (!dataToSend.contains(name)) {
                    writeData(commandSocket, QByteArray("550 File does not exist\n"));
                    continue;
                }

                qCDebug(log_test) << "Sending" << name;

                writeData(commandSocket,
                          QByteArray("150 Opening connection for ") + name.toUtf8() + "\n");
                QByteArray toSend(dataToSend[name].mid(offset));
                QByteArray okResult(QByteArray("226 Completed\n"));
                if (interruptTransfer) {
                    interruptTransfer = false;
                    toSend = toSend.mid(0, toSend.length() / 2 + 1);
                    okResult = QByteArray("426 Transfer aborted\n");
                }
                if (!sendData(toSend, server))
                    continue;
                offset = 0;
                writeData(commandSocket, okResult);
                continue;
            } else if (command.startsWith("LIST ", Qt::CaseInsensitive)) {
                QString name(command.mid(5));

                qCDebug(log_test) << "Listing" << name;

                std::lock_guard<std::mutex> lock(mutex);
                if (!listData.contains(name)) {
                    writeData(commandSocket, QByteArray("550 Directory does not exist\n"));
                    continue;
                }
                writeData(commandSocket, QByteArray("150 Here comes the directory listing.\n"));
                if (!sendData(listData[name], server))
                    continue;

                writeData(commandSocket, QByteArray("226 Directory send OK.\n"));
                continue;
            } else if (command.startsWith("NLST ", Qt::CaseInsensitive)) {
                QString name(command.mid(5));

                qCDebug(log_test) << "Listing" << name;

                std::lock_guard<std::mutex> lock(mutex);
                if (!nlstData.contains(name)) {
                    writeData(commandSocket, QByteArray("550 Directory does not exist\n"));
                    continue;
                }
                writeData(commandSocket, QByteArray("150 Here comes the directory listing.\n"));
                if (!sendData(nlstData[name], server))
                    continue;

                writeData(commandSocket, QByteArray("226 Directory send OK.\n"));
                continue;
            } else if (command.startsWith("QUIT", Qt::CaseInsensitive)) {
                writeData(commandSocket, QByteArray("221 Goodbye.\n"));
                commandSocket->disconnectFromHost();
                break;
            } else if (command.startsWith("NOOP", Qt::CaseInsensitive)) {
                writeData(commandSocket, QByteArray("200 OK\n"));
                break;
            }

            writeData(commandSocket, QByteArray("502 Not implemented\n"));
        }

        if (commandSocket)
            commandSocket->close();
        server.close();

        qCDebug(log_test) << "Server completing";
        std::lock_guard<std::mutex> lock(mutex);
        port = -2;
    }

    int waitForPort()
    {
        for (;;) {
            QThread::yieldCurrentThread();
            std::lock_guard<std::mutex> lock(mutex);
            if (port == 0)
                continue;
            if (port < 0)
                return -1;
            return port;
        }
    }

    void waitForCompleted()
    {
        for (;;) {
            QThread::yieldCurrentThread();
            std::lock_guard<std::mutex> lock(mutex);
            if (port < 0)
                return;
        }
    }
};

class TestFTP : public QObject {
Q_OBJECT

    static bool ftpFileNameSort(const FTPConnection::File &a, const FTPConnection::File &b)
    {
        return a.filePath() < b.filePath();
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void uploadPassive()
    {
        QByteArray data("Some data that was transferred");

        TestFTPManager manager;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["ActiveMode"] = "passive";
        config["Resume/Retries"] = 0;

        FTPConnection connection("localhost", port);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        QVERIFY(connection.put("/path/to/file", data));
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE(manager.receivedData["/path/to/file"], data);
    }

    void uploadActive()
    {
        QByteArray data("Some data that was transferred");

        TestFTPManager manager;
        manager.connectionString = QByteArray("220-Line1\n220\n");
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["ActiveMode"] = "active";
        config["RelaxedIncoming"] = true;
        config["Resume/Retries"] = 0;

        FTPConnection connection("localhost", port);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        QVERIFY(connection.put("/path/to/file", data));
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE(manager.receivedData["/path/to/file"], data);
    }

    void uploadPassiveLegacy()
    {
        QByteArray data("Some data that was transferred");

        TestFTPManager manager;
        manager.connectionString = QByteArray("220-Line1\nLine2\n220 OK\n");
        manager.disableEPSV = true;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["ActiveMode"] = "passive";
        config["Resume/Retries"] = 0;

        FTPConnection connection("localhost", port);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        QVERIFY(connection.put("/path/to/file", data));
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE(manager.receivedData["/path/to/file"], data);
    }

    void uploadActiveLegacy()
    {
        QByteArray data("Some data that was transferred");

        TestFTPManager manager;
        manager.connectionString = QByteArray("220-Line1\n220-Line2\n220-\n220 \n");
        manager.disableEPRT = true;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["ActiveMode"] = "active";
        config["RelaxedIncoming"] = true;
        config["Resume/Retries"] = 0;

        FTPConnection connection("localhost", port);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        QVERIFY(connection.put("/path/to/file", data));
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE(manager.receivedData["/path/to/file"], data);
    }

    void uploadActiveFallback()
    {
        QByteArray data("Some data that was transferred");

        TestFTPManager manager;
        manager.connectionString = QByteArray("220-Line1\r\n220-Line2\r\n220-\r\n220 \r\n");
        manager.disableEPSV = true;
        manager.disablePASV = true;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["ActiveMode"] = "both";
        config["RelaxedIncoming"] = true;
        config["Resume/Retries"] = 0;

        FTPConnection connection("localhost", port);
        connection.setCloseOnDataFailure(false);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        QVERIFY(connection.put("/path/to/file", data));
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE(manager.receivedData["/path/to/file"], data);
    }

    void downloadPassive()
    {
        QByteArray data("Some data that was transferred");

        TestFTPManager manager;
        manager.dataToSend["/path/to/file"] = data;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["ActiveMode"] = "passive";
        config["Resume/Retries"] = 0;

        FTPConnection connection("localhost", port);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        QByteArray rx;
        QVERIFY(connection.get("/path/to/file", rx));
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE(rx, data);
    }

    void downloadActive()
    {
        QByteArray data("Some data that was transferred");

        TestFTPManager manager;
        manager.dataToSend["/path/to/file"] = data;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["ActiveMode"] = "active";
        config["RelaxedIncoming"] = true;
        config["Resume/Retries"] = 0;

        FTPConnection connection("localhost", port);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        QByteArray rx;
        QVERIFY(connection.get("/path/to/file", rx));
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE(rx, data);
    }

    void downloadActiveFallback()
    {
        QByteArray data("Some data that was transferred");

        TestFTPManager manager;
        manager.disableEPSV = true;
        manager.disablePASV = true;
        manager.dataToSend["/path/to/file"] = data;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["ActiveMode"] = "both";
        config["RelaxedIncoming"] = true;
        config["Resume/Retries"] = 0;

        FTPConnection connection("localhost", port);
        connection.setCloseOnDataFailure(false);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        QByteArray rx;
        QVERIFY(connection.get("/path/to/file", rx));
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE(rx, data);
    }

    void downloadResume()
    {
        QByteArray data("Some data that was transferred");

        TestFTPManager manager;
        manager.interruptTransfer = true;
        manager.dataToSend["/path/to/file"] = data;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["Resume/Retries"] = 3;
        config["Resume/Download"] = true;

        FTPConnection connection("localhost", port);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        QByteArray rx;
        QVERIFY(connection.get("/path/to/file", rx));
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE(rx, data);
    }

    void uploadResume()
    {
        QByteArray data("Some data that was transferred");

        TestFTPManager manager;
        manager.interruptTransfer = true;
        manager.sizeData["/path/to/file"] = data.size() / 2 + 1;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["ActiveMode"] = "passive";
        config["Resume/Retries"] = 3;
        config["Resume/Upload"] = true;

        FTPConnection connection("localhost", port);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        QVERIFY(connection.put("/path/to/file", data));
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE(manager.receivedData["/path/to/file"], data);
    }

    void createDirectories()
    {
        TestFTPManager manager;
        manager.directories.insert("/path");
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["ActiveMode"] = "passive";
        config["Resume/Retries"] = 0;

        FTPConnection connection("localhost", port);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        QVERIFY(!connection.mkdir("/path"));
        QVERIFY(!connection.mkdir("/path/to/file"));
        QVERIFY(connection.mkdir("/path/to"));
        QVERIFY(connection.mkdir("/path/to/file"));
        QVERIFY(connection.mkdir("/foo"));
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE(manager.directories,
                 QSet<QString>() << "/path" << "/path/to" << "/path/to/file" << "/foo");
    }

    void unixList()
    {
        TestFTPManager manager;
        manager.listData["/path"] = "-rw-rw-r-- 1 hageman users   389 Feb 23 14:28 CMakeLists.txt\n"
                                    "drwxr-xr-x 2 hageman users 18432 Jul  6  2015 bin\n"
                                    "-rw-rw-r-- 1 hageman users 13284 Jan  6 11:16 chunkfile.cxx\n"
                                    "lrwxrwxrwx 1 hageman users     2 Feb 22 10:46 da.tap -> upload.cxx\n"
                                    "-rw-rw-r-- 1 hageman users  5406 Jan  6 10:27 email.cxx\n"
                                    "-rw-rw-r-- 1 hageman users 28531 Jul  7  2015 ftp.cxx\n"
                                    "lrwxrwxrwx 1 hageman users     2 Feb 22 10:46 i386 -> bin\n"
                                    "-rw-rw-r-- 1 hageman users  4316 Jan  6 10:27 smtp.cxx\n"
                                    "-rw-rw-r-- 1 hageman users 30212 Feb 24 12:04 upload.cxx";
        manager.listData["/empty"] = "";
        manager.mdtmData["/path/CMakeLists.txt"] = 1420070400;
        manager.mdtmData["/path/chunkfile.cxx"] = 1420070400;
        manager.mdtmData["/path/email.cxx"] = 1420243200;
        manager.mdtmData["/path/smtp.cxx"] = 1420329600;
        manager.mdtmData["/path/upload.cxx"] = 1420419723;
        manager.mdtmData["/path/da.tap"] = 1420419723;
        manager.sizeData["/path/da.tap"] = 1234;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["ActiveMode"] = "passive";
        config["Resume/Retries"] = 0;

        FTPConnection connection("localhost", port);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        bool ok = false;
        auto files = connection.list("/path", false, &ok);
        QVERIFY(ok);
        ok = false;
        auto directories = connection.list("/path", true, &ok);
        QVERIFY(ok);
        ok = false;
        QVERIFY(connection.list("/empty", true, &ok).empty());
        QVERIFY(ok);
        ok = false;
        QVERIFY(connection.list("/empty", false, &ok).empty());
        QVERIFY(ok);
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE((int) files.size(), 7);
        QCOMPARE((int) directories.size(), 2);

        std::sort(files.begin(), files.end(), ftpFileNameSort);
        QCOMPARE(files[0].filePath(), std::string("/path/CMakeLists.txt"));
        QCOMPARE(files[0].fileName(), std::string("CMakeLists.txt"));
        QCOMPARE(files[0].dir(), std::string("/path"));
        QCOMPARE(files[0].modified(), 1420070400.0);
        QCOMPARE((int) files[0].size(), 389);
        QCOMPARE(files[1].filePath(), std::string("/path/chunkfile.cxx"));
        QCOMPARE(files[1].modified(), 1420070400.0);
        QCOMPARE((int) files[1].size(), 13284);
        QCOMPARE(files[2].filePath(), std::string("/path/da.tap"));
        QCOMPARE(files[2].modified(), 1420419723.0);
        QCOMPARE((int) files[2].size(), 1234);
        QCOMPARE(files[3].filePath(), std::string("/path/email.cxx"));
        QCOMPARE(files[3].modified(), 1420243200.0);
        QCOMPARE((int) files[3].size(), 5406);
        QCOMPARE(files[4].filePath(), std::string("/path/ftp.cxx"));
        QVERIFY(fabs(files[4].modified() - 1436227200.0) < 86400.0);
        QCOMPARE((int) files[4].size(), 28531);
        QCOMPARE(files[5].filePath(), std::string("/path/smtp.cxx"));
        QCOMPARE(files[5].modified(), 1420329600.0);
        QCOMPARE((int) files[5].size(), 4316);
        QCOMPARE(files[6].filePath(), std::string("/path/upload.cxx"));
        QCOMPARE(files[6].modified(), 1420419723.0);
        QCOMPARE((int) files[6].size(), 30212);

        std::sort(directories.begin(), directories.end(), ftpFileNameSort);
        QCOMPARE(directories[0].filePath(), std::string("/path/bin"));
        QCOMPARE(directories[0].fileName(), std::string("bin"));
        QCOMPARE(directories[0].dir(), std::string("/path"));
        QCOMPARE(directories[1].filePath(), std::string("/path/i386"));
        QCOMPARE(directories[1].fileName(), std::string("i386"));
        QCOMPARE(directories[1].dir(), std::string("/path"));
    }

    void dosList()
    {
        TestFTPManager manager;
        manager.listData["/path"] = "01-16-02  11:14AM       <DIR>          epsgroup\n"
                                    "06-05-03  03:19PM                 1973 aaaa.txt\n"
                                    "06-05-03  03:19PM                 1972 bbbb.txt";
        manager.mdtmData["/path/aaaa.txt"] = 1420070400;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["ActiveMode"] = "passive";
        config["Resume/Retries"] = 0;

        FTPConnection connection("localhost", port);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        bool ok = false;
        auto files = connection.list("/path", false, &ok);
        QVERIFY(ok);
        ok = false;
        auto directories = connection.list("/path", true, &ok);
        QVERIFY(ok);
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE((int) files.size(), 2);
        QCOMPARE((int) directories.size(), 1);

        std::sort(files.begin(), files.end(), ftpFileNameSort);
        QCOMPARE(files[0].filePath(), std::string("/path/aaaa.txt"));
        QCOMPARE(files[0].fileName(), std::string("aaaa.txt"));
        QCOMPARE(files[0].dir(), std::string("/path"));
        QCOMPARE(files[0].modified(), 1420070400.0);
        QCOMPARE((int) files[0].size(), 1973);
        QCOMPARE(files[1].filePath(), std::string("/path/bbbb.txt"));
        QVERIFY(fabs(files[1].modified() - 1054837140.0) < 86400.0);
        QCOMPARE((int) files[1].size(), 1972);

        std::sort(directories.begin(), directories.end(), ftpFileNameSort);
        QCOMPARE(directories[0].filePath(), std::string("/path/epsgroup"));
        QCOMPARE(directories[0].fileName(), std::string("epsgroup"));
        QCOMPARE(directories[0].dir(), std::string("/path"));
    }

    void fallbackList()
    {
        TestFTPManager manager;
        manager.listData["/path"] = "blarg\nfoo";
        manager.nlstData["/path"] = "/path/file1\n"
                                    "/path/file2\n"
                                    "/path/dir1\n"
                                    "/path/file3";
        manager.mdtmData["/path/file1"] = 1420070400;
        manager.mdtmData["/path/file2"] = 1420419723;
        manager.mdtmData["/path/file3"] = 1436227200;
        manager.sizeData["/path/file1"] = 123;
        manager.sizeData["/path/file2"] = 456;
        manager.sizeData["/path/file3"] = 7890;
        ThreadPool::system()->run(&manager);

        int port = manager.waitForPort();
        if (port < 0) {
            QFAIL("Test server failure");
        }

        Variant::Write config = Variant::Write::empty();
        config["ActiveMode"] = "passive";
        config["Resume/Retries"] = 0;

        FTPConnection connection("localhost", port);
        connection.configure(config);
        QVERIFY(connection.connectToServer());
        bool ok = false;
        auto files = connection.list("/path", false, &ok);
        QVERIFY(ok);
        ok = false;
        auto directories = connection.list("/path", true, &ok);
        QVERIFY(ok);
        connection.disconnectFromServer();
        manager.waitForCompleted();

        QCOMPARE((int) files.size(), 3);
        QCOMPARE((int) directories.size(), 1);

        std::sort(files.begin(), files.end(), ftpFileNameSort);
        QCOMPARE(files[0].filePath(), std::string("/path/file1"));
        QCOMPARE(files[0].fileName(), std::string("file1"));
        QCOMPARE(files[0].dir(), std::string("/path"));
        QCOMPARE(files[0].modified(), 1420070400.0);
        QCOMPARE((int) files[0].size(), 123);
        QCOMPARE(files[1].filePath(), std::string("/path/file2"));
        QCOMPARE(files[1].modified(), 1420419723.0);
        QCOMPARE((int) files[1].size(), 456);
        QCOMPARE(files[2].filePath(), std::string("/path/file3"));
        QCOMPARE(files[2].modified(), 1436227200.0);
        QCOMPARE((int) files[2].size(), 7890);

        std::sort(directories.begin(), directories.end(), ftpFileNameSort);
        QCOMPARE(directories[0].filePath(), std::string("/path/dir1"));
        QCOMPARE(directories[0].fileName(), std::string("dir1"));
        QCOMPARE(directories[0].dir(), std::string("/path"));
    }

#if 0

    void realWorldDownload()
    {
        FTPConnection connection("aftp.cmdl.noaa.gov");
        QVERIFY(connection.connectToServer());
        QByteArray data;
        QVERIFY(connection.get("/aerosol/README.txt", data));
        connection.disconnectFromServer();

        QVERIFY(data.size() > 0);
    }

    void realWorldList()
    {
        FTPConnection connection("ftp.etl.noaa.gov");
        QVERIFY(connection.connectToServer());

        bool ok = false;
        auto directories =
                connection.list("/psd3/arctic/tiksi/surface_properties/fluxtower/towermet/products",
                                true, &ok);
        QVERIFY(ok);
        ok = false;
        for (const auto &check : directories) {
            if (check.fileName() == "2018") {
                ok = true;
                break;
            }
        }
        QVERIFY(ok);

        auto files = connection.list(
                "/psd3/arctic/tiksi/surface_properties/fluxtower/towermet/products/2018/daily_meteorological/",
                false, &ok);
        QVERIFY(ok);
        ok = false;
        for (const auto &check : files) {
            if (check.fileName() == "tikmeteorologicaltwr.b1.20180127.000000.txt") {
                ok = true;
                break;
            }
        }
        QVERIFY(ok);
    }

    void realWorldUpload()
    {
        QByteArray dataToUpload("Some data\nBeing uploaded\n");

        FTPConnection connection("awftp.cmdl.noaa.gov");
        QVERIFY(connection.connectToServer());
        QVERIFY(connection.put("/incoming/aer/nil/cpd3.test.1", dataToUpload));
        connection.disconnectFromServer();
    }

    void realWorldUploadThrottle()
    {
        QByteArray dataToUpload;
        dataToUpload.fill('A', 300 * 1024);

        IO::Generic::Block::ByteView block(dataToUpload);
        std::unique_ptr<IO::Generic::Block::StreamReader>
                stream(new IO::Generic::Block::StreamReader(block, 1024));

        FTPConnection connection("awftp.cmdl.noaa.gov");
        connection.setUploadRateLimit(60000);
        QVERIFY(connection.connectToServer());
        QVERIFY(connection.put("/incoming/aer/nil/cpd3.test.2", std::move(stream),
                               dataToUpload.size()));
        connection.disconnectFromServer();
    }

#endif
};

QTEST_MAIN(TestFTP)

#include "ftp.moc"
