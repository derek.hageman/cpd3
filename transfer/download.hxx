/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3TRANSFERDOWNLOAD_H
#define CPD3TRANSFERDOWNLOAD_H

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <vector>
#include <deque>
#include <QRegularExpression>
#include <QString>
#include <QStringList>

#include "transfer/transfer.hxx"
#include "transfer/ftp.hxx"
#include "core/textsubstitution.hxx"
#include "core/actioncomponent.hxx"
#include "io/drivers/url.hxx"

namespace CPD3 {
namespace Transfer {

class FileDownloader;

/**
 * A single file downloaded or found locally.
 */
class CPD3TRANSFER_EXPORT DownloadFile {
    double fileTime;
    QString pattern;

    friend class FileDownloader;

protected:
    DownloadFile();

    /**
     * Acquire the file for local access.  For remote files, this should download the file into
     * a temporary local one.  This is called before the file leaves control of the download
     * manager.
     *
     * @return  true if the file was successfully made locally available
     */
    virtual bool acquire() = 0;

    /**
     * Abort acquisition of the file.
     */
    virtual void abortAcquire();

    ActionFeedback::Source feedback;

public:
    virtual ~DownloadFile();

    /**
     * Get the file information for the downloaded file.  This can be either the actual
     * source if the file existed originally locally, or a temporary file representing
     * the downloaded data.
     *
     * @return  the local file information
     */
    virtual QFileInfo fileInfo() const = 0;

    /**
     * Test if the file is locally writable.
     *
     * @return      true if the local file is writable
     */
    virtual bool writable() const = 0;

    /**
     * Get the file name component of the downloaded file.  This may not reflect the
     * actual local name for files that aren't locally accessible.
     *
     * @return  the file name
     */
    virtual QString fileName() const;

    /**
     * Get the ID of the file, used to identify it for modified time equality checking.
     *
     * @return the ID of the file
     */
    virtual Util::ByteArray id() const;

    /**
     * Get the long form description of the file.  For example, the absolute path of
     * local files.
     *
     * @return  the description of the file
     */
    virtual QString description() const;

    /**
     * Get the modification time of the file, if available.
     *
     * @return      the modification time or undefined if not available.
     */
    virtual double getModifiedTime() const;

    /**
     * Get the time associated with the file.  This is usually a time derived from the
     * file name itself.
     *
     * @return      the file time
     */
    virtual double getFileTime() const;

    /**
     * Get the pattern the file matched.
     *
     * @return the pattern
     */
    virtual QString getPattern() const;

    /**
     * Get the URL backing of the file, if any.
     *
     * @return  the URL backing or null if not a URL
     */
    virtual IO::URL::Backing *getURL() const;
};

/**
 * This is the common interface to handle downloading of files from external
 * targets.
 */
class CPD3TRANSFER_EXPORT FileDownloader {
    friend class DownloadFile;

    std::shared_ptr<IO::URL::Context> urlContext;
    std::shared_ptr<FTPConnection> ftpConnection;

    struct TimeState {
        double latestTime;
        std::unordered_set<Util::ByteArray> equalToLatest;

        bool referenced;
        double nextLatestTime;
        std::unordered_set<Util::ByteArray> nextEqualToLatest;

        TimeState();

        void updateNext(DownloadFile &file);
    };

    std::unordered_map<Util::ByteArray, TimeState> timeState;

    double lastTimeExecuted;
    double currentExecution;
    double latestFileModification;

    bool terminated;

    std::vector<std::unique_ptr<DownloadFile>> downloadedFiles;

    bool findLocalDirectories;

    void configureRequest(IO::URL::Backing &request, const Data::Variant::Read &settings);

    std::unique_ptr<DownloadFile> createProcess(const Data::Variant::Read &settings);

public:
    /**
     * The elements the file downloader understands during time capture.
     */
    enum class TimeCapture {
        Ignore,
        Generic,
        Year,
        Month,
        Day,
        Hour,
        Minute,
        Second,
        Millisecond,
        ShortYear,
        NamedMonth,
        ShortMonth,
        DOY,
        HourMinuteInteger
    };

    /**
     * Get the default pattern substitution for the given key for time capture.
     * This provides sensible time capturing pattern matches.
     *
     * @param key       the matching key
     * @param captures  the captured patterns
     * @return          the pattern substitution or empty if none matched
     */
    static QString getTimeCapture(const QStringList &elements, QList<TimeCapture> &captures);

    /**
     * A context for time extraction from a matched regular expression.
     */
    class CPD3TRANSFER_EXPORT TimeExtractContext {
        int extractedYear;
        int extractedMonth;
        int extractedDay;
        int extractedHour;
        int extractedMinute;
        int extractedSecond;
        int extractedMillisecond;
        double extractedDOY;
        QStringList extractedGeneric;
    public:
        TimeExtractContext();

        /**
         * Integrate a regular expression and its captures into the time context.
         *
         * @param expression    the expression to integrate
         * @param captures      the captures in the expression
         */
        void integrate(const QStringList &result, const QList<TimeCapture> &captures);

        void integrate(const QRegularExpressionMatch &expression,
                       const QList<TimeCapture> &captures);

        /**
         * Perform the time extraction.
         *
         * @param reference     the reference time to use
         * @return              the extracted time, or undefined on failure
         */
        double extract(double reference = FP::undefined()) const;
    };

private:

    struct MatchElement {
        QRegularExpression pattern;
        QRegularExpressionMatch match;
        QString original;
        QList<TimeCapture> capture;

        inline MatchElement(const QString &pattern, const QString &original, bool caseSensitive = false)
                : pattern(pattern, caseSensitive ? QRegularExpression::NoPatternOption
                                              : QRegularExpression::CaseInsensitiveOption),
                  original(original),
                  capture()
        { }

        bool apply(const QString &str);
    };

    static double convertTime(const std::deque<MatchElement> &matched,
                              double reference = FP::undefined());

    Util::ByteArray getTimeContext(const Data::Variant::Read &config,
                              const Util::ByteArray &defaultContext = Util::ByteArray::filled(0));

    bool filterFiles(std::vector<std::unique_ptr<DownloadFile>> &files,
                     const Data::Variant::Read &config,
                     const Util::ByteArray &timeContext = Util::ByteArray::filled(0),
                     const std::deque<MatchElement> &matched = {});

    MatchElement buildMatch(const QString &result, bool caseSensitive = false) const;

    MatchElement buildMatch(const Data::Variant::Read &config) const;

    std::deque<MatchElement> buildMatchList(const Data::Variant::Read &config) const;

    bool listFTP(std::vector<std::unique_ptr<DownloadFile>> &output,
                 const std::shared_ptr<FTPConnection> &connection,
                 const Data::Variant::Read &config,
                 const std::string &path,
                 const std::string &rootPath,
                 const std::deque<MatchElement> &matched);

    bool walkFTP(std::vector<std::unique_ptr<DownloadFile>> &output,
                 const std::shared_ptr<FTPConnection> &connection,
                 const Data::Variant::Read &config,
                 const std::string &path,
                 const std::string &rootPath,
                 std::deque<MatchElement> remaining,
                 const std::deque<MatchElement> &matched = {});

    bool execFTP(const Data::Variant::Read &config);

    bool listLocal(std::vector<std::unique_ptr<DownloadFile>> &output,
                   const Data::Variant::Read &config,
                   const QDir &dir,
                   const QDir &rootDir,
                   const std::deque<MatchElement> &matched);

    bool walkLocal(std::vector<std::unique_ptr<DownloadFile>> &output,
                   const Data::Variant::Read &config,
                   const QDir &dir,
                   const QDir &rootDir,
                   std::deque<MatchElement> remaining,
                   const std::deque<MatchElement> &matched = {});

    bool execLocal(const Data::Variant::Read &config);

    bool execSingle(const Data::Variant::Read &config);

    bool execList(const Data::Variant::Read &config);

public:
    FileDownloader();

    virtual ~FileDownloader();

    FileDownloader(const FileDownloader &) = delete;

    FileDownloader &operator=(const FileDownloader &) = delete;

    /**
     * Set the time bounds this execution of the downloader represents.  This is normally
     * used in conjunction with a CPD3::Database::RunLock lock to correctly handle modified
     * time tracking.
     *
     * @param lastTimeExecuted  the start time of the last execution
     * @param currentExecution  the start time of the current execution
     */
    void setupExecutionBounds(double lastTimeExecuted, double currentExecution);

    /**
     * Restore the internal state from the given data.
     *
     * @param state     the state data contents
     */
    void restoreState(const QByteArray &state);

    /**
     * Save the internal state to an output data storage.
     *
     * @param pruneUnused   if set, then remove any parts of the state that where not referenced during downloading
     * @return      the state data contents
     */
    QByteArray saveState(bool pruneUnused = false) const;

    /**
     * Take all files downloaded so far and acknowledge them as accepted with respect to
     * modified time tracking.
     *
     * @return      the files downloaded so far
     */
    std::vector<std::unique_ptr<DownloadFile>> takeFiles();

    /**
     * Set the downloader to only locate local directories instead of actually trying
     * to get files in those directories.
     */
    inline void setFindLocalDirectories(bool enable = true)
    { this->findLocalDirectories = enable; }

    CPD3::ActionFeedback::Source feedback;

protected:

    /**
     * Test if the downloader has been terminated.
     *
     * @return  true if the downloader is terminated
     */
    bool testTerminated();

    /**
     * Apply substitutions to strings being used.
     * <br>
     * By default this just returns the input.
     *
     * @param input the input
     * @return      the input with substitutions applied
     */
    virtual QString applySubstitutions(const QString &input) const;

    /**
     * Apply substitutions to strings being used during matching capture.  This should
     * result in a regular expression pattern.
     * <br>
     * By default this returns the regular expression escaped result of
     * applySubstitutions(const QString &)
     *
     * @param input     the input
     * @param captures  any
     * @return          the input with substitutions applied
     */
    virtual QString applyMatching(const QString &input, QList<TimeCapture> &captures) const;

    /**
     * Called when a temporary file is created for output.
     * <br>
     * By default this does nothing.
     *
     * @param fileName  the file name created
     */
    virtual void pushTemporaryOutput(const QFileInfo &fileName);

    /**
     * Called when a temporary file is removed.
     * <br>
     * By default this does nothing.
     */
    virtual void popTemporaryOutput();

    /**
     * Called during filtering for local files.
     * <br>
     * By default this does nothing.
     *
     * @param dir       the current directory
     * @param rootDir   the search directory root
     */
    virtual void pushFilterDirectory(const QDir &dir, const QDir &rootDir);

    /**
     * Called during filtering for remote files.
     * <br>
     * By default this does nothing.
     *
     * @param path      the current path
     * @param rootPath  the search path root
     */
    virtual void pushFilterRemote(const std::string &path, const std::string &rootPath);

    /**
     * Called when a filter path is stepped out of.
     * <br>
     * By default this does nothing.
     */
    virtual void popFilter();

    /**
     * Create a request for a single file fetch based on the given configuration.
     *
     * @param config    the configuration to retrieve
     * @return          a file to download
     */
    virtual std::unique_ptr<DownloadFile> createSingleFetch(const Data::Variant::Read &config);

    /**
     * Create a request for to fetch a list of files based on the given configuration.
     *
     * @param config    the configuration to retrieve
     * @return          a file to download
     */
    virtual std::unique_ptr<DownloadFile> createListFetch(const Data::Variant::Read &config);

    /**
     * Convert a response from createListFetch(const Data::Variant::Read &) to a list of individual
     * files to download.
     *
     * @param config    the configuration
     * @param response  the successfully downloaded list
     * @param files     the output files list
     * @return          true on success
     */
    virtual bool convertListResponse(const Data::Variant::Read &config,
                                     DownloadFile &response,
                                     std::vector<std::unique_ptr<DownloadFile>> &files);

    /**
     * Emitted when a terminate request has been received.
     */
    Threading::Signal<> terminateRequested;

public slots:

    /**
     * Perform the download.
     *
     * @return  true on success
     */
    bool exec(const Data::Variant::Read &config);

    /**
     * Request termination of the downloader.
     */
    void signalTerminate();
};

}
}

#endif //CPD3TRANSFERDOWNLOAD_H
