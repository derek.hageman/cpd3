/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef HAVE_DNSQUERY
#include <windows.h>
#include <windns.h>
#endif

#ifdef HAVE_RES_QUERY

#ifdef Q_OS_OSX
#define BIND_8_COMPAT
#endif

#include <netinet/in.h>
#include <arpa/nameser.h>
#include <resolv.h>

#endif

#include <cstring>
#include <QtEndian>
#include <QLoggingCategory>
#include <QFile>
#include <QRegularExpression>

#include "transfer/smtp.hxx"
#include "core/environment.hxx"
#include "io/drivers/file.hxx"
#include "io/drivers/tcp.hxx"
#include "io/drivers/udp.hxx"
#include "io/process.hxx"


Q_LOGGING_CATEGORY(log_transfer_smtp, "cpd3.transfer.smtp", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace Transfer {


SMTPSend::SMTPSend(const Variant::Read &relay) : relayMode(RelayMode::Direct),
                                                 relayPort(25),
                                                 relayTLS(false),
                                                 allSendsOk(true),
                                                 anySentOk(false)
{
    const auto &fc = relay.hash("From").toString();
    if (!fc.empty())
        setFrom(fc);
    else
        setFrom({});

    relayName = relay.hash("Hostname").toString();
    if (!relayName.empty()) {
        relayMode = RelayMode::Relay;
        auto check = relay.hash("Port").toInteger();
        if (INTEGER::defined(check) && check > 0 && check < 65536)
            relayPort = static_cast<std::uint16_t>(check);
        relayTLS = relay.hash("TLS").toBool();
        relayUsername = relay.hash("Username").toString();
        relayPassword = relay.hash("Password").toString();
        if (relayPassword.empty()) {
            QString fileName(relay.hash("PasswordFile").toQString());
            if (!fileName.isEmpty() && QFile::exists(fileName)) {
                if (auto file = IO::Access::file(fileName, IO::File::Mode::readOnly())->block()) {
                    QByteArray contents;
                    file->read(contents);
                    relayPassword = contents.toStdString();
                } else {
                    qCDebug(log_transfer_smtp) << "Failed to open password file" << fileName;
                }
            }
        }
        return;
    }

    relayName = relay.hash("Program").toString();
    if (!relayName.empty()) {
        relayMode = RelayMode::LocalProgram;
        return;
    }
}

SMTPSend::~SMTPSend()
{
    signalTerminate();
    if (reaper.joinable()) {
        reaper.join();
    }
}

class SMTPSend::Sender {
public:
    Sender() = default;

    virtual ~Sender() = default;

    virtual bool isComplete() = 0;

    virtual void signalTerminate()
    { }
};

void SMTPSend::sendToProgram(Util::ByteArray &&message)
{
    class Execute final : public Sender {
        SMTPSend &parent;

        std::shared_ptr<IO::Process::Instance> process;
        bool completed;
    public:
        Execute(SMTPSend &parent, const std::string &program, Util::ByteArray &&message) : parent(
                parent), completed(false)
        {
            qCDebug(log_transfer_smtp) << "Starting sender program:" << program;

            IO::Process::Spawn spawn(program);
#ifndef NDEBUG
            spawn.forward();
#else
            spawn.discard();
#endif
            spawn.inputStream = IO::Process::Spawn::StreamMode::Capture;

            process = spawn.create();
            if (!process)
                return;

            process->exited.connect([this](int rc, bool normal) {
                if (!normal) {
                    qCDebug(log_transfer_smtp) << "Email send did not exit normally";
                } else if (rc != 0) {
                    qCDebug(log_transfer_smtp) << "Email send exited with code" << rc;
                } else {
                    qCDebug(log_transfer_smtp) << "Email sent to process successfully";
                }
                std::lock_guard<std::mutex> lock(this->parent.mutex);
                completed = true;
                if (rc != 0 || !normal)
                    this->parent.allSendsOk = false;
                else
                    this->parent.anySentOk = true;
                this->parent.notify.notify_all();
            });

            if (!process->start()) {
                process.reset();
                return;
            }

            {
                auto stream = process->inputStream();
                stream->write(std::move(message));
                stream->close();
            }
        }

        virtual ~Execute()
        {
            if (process) {
                process->terminate();
                process->wait(10.0);
                process->kill();
            }
        }

        bool isComplete() override
        {
            if (!process)
                return true;
            return completed;
        }

        void signalTerminate() override
        {
            if (!process)
                return;
            qCDebug(log_transfer_smtp) << "Terminating sending process";
            process->terminate();
        }
    };
    senders.emplace_back(new Execute(*this, relayName, std::move(message)));
}

namespace {
class SMTPStream final {
    std::mutex &mutex;
    std::condition_variable &notify;
    bool &terminated;

    std::unique_ptr<IO::Socket::TCP::Connection> connection;
    Util::ByteArray incomingData;
    bool connectionEnded;

    using Clock = std::chrono::steady_clock;
public:
    SMTPStream(const std::string &host,
               const IO::Socket::TCP::Configuration &config,
               std::mutex &mutex,
               std::condition_variable &notify,
               bool &terminated) : mutex(mutex),
                                   notify(notify),
                                   terminated(terminated),
                                   connectionEnded(false)
    {
        connection = IO::Socket::TCP::connect(host, config, false);
        if (!connection)
            return;

        connection->read.connect([this](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(this->mutex);
                incomingData += data;
            }
            this->notify.notify_all();
        });
        connection->ended.connect([this]() {
            {
                std::lock_guard<std::mutex> lock(this->mutex);
                connectionEnded = true;
            }
            this->notify.notify_all();
        });
        connection->start();
    }

    ~SMTPStream()
    {
        connection.reset();
    }

    std::string exchange(const std::string &command)
    {
        if (!connection)
            return {};
        connection->write(command);
        return nextLine();
    }

    std::string nextLine()
    {
        if (!connection)
            return {};
        auto timeout = Clock::now() + std::chrono::seconds(30);
        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            Util::ByteArray::LineIterator it(incomingData);
            auto raw = it.next(connectionEnded);
            if (!raw.empty()) {
                std::string line(raw.data<const char *>(), raw.size());
                incomingData.pop_front(it.getCurrentOffset());
                return line;
            }
            if (timeout <= Clock::now())
                break;
            if (terminated)
                break;
            if (connectionEnded)
                break;
            notify.wait_until(lock, timeout);
        }
        if (connectionEnded) {
            qCDebug(log_transfer_smtp) << "Connection closed without an available response";
        } else if (!terminated) {
            qCDebug(log_transfer_smtp) << "Timeout waiting for response";
        }
        return {};
    }

    void write(const Util::ByteView &data)
    {
        if (!connection)
            return;
        connection->write(data);
    }
};

enum : std::uint32_t {
    SMTP_Capability_Auth_PLAIN = 0x01U,
    SMTP_Capability_Auth_LOGIN = 0x02U,
    SMTP_Capability_Auth_CRAMMD5 = 0x04U,
};

}

static bool doMessageSend(SMTPStream &stream,
                          const std::string &from,
                          const std::vector<std::string> &recipients,
                          const Util::ByteArray &message)
{
    {
        auto response = stream.exchange("MAIL FROM:<" + from + ">\r\n");
        if (!Util::starts_with(response, "250")) {
            qCDebug(log_transfer_smtp) << "Invalid response to MAIL FROM:" << response;
            return false;
        }
    }
    for (const auto &r : recipients) {
        auto response = stream.exchange("RCPT TO:<" + r + ">\r\n");
        if (!Util::starts_with(response, "250")) {
            qCDebug(log_transfer_smtp) << "Invalid response to RCPT TO:" << response;
            return false;
        }
    }

    {
        auto response = stream.exchange("DATA\r\n");
        if (!Util::starts_with(response, "354")) {
            qCDebug(log_transfer_smtp) << "Invalid response to DATA:" << response;
            return false;
        }
    }

    for (const auto &lineData : message.split_lines()) {
        if (!lineData.empty() && lineData.front() == '.') {
            stream.write(Util::ByteView("."));
        }
        stream.write(lineData);
        stream.write(Util::ByteView("\r\n"));
    }

    {
        auto response = stream.exchange(".\r\n");
        if (!Util::starts_with(response, "250")) {
            qCDebug(log_transfer_smtp) << "Invalid response to message body:" << response;
            return false;
        }
    }

    qCDebug(log_transfer_smtp) << "Message send of" << message.size() << "body byte(s) to"
                               << recipients.size() << "recipient(s) completed";
    return true;
}

static bool doHandshake(SMTPStream &stream, std::uint32_t &capabilities)
{
    {
        auto response = stream.nextLine();
        if (!Util::starts_with(response, "220")) {
            qCDebug(log_transfer_smtp) << "Invalid initial handshake response:" << response;
            return false;
        }
    }

    auto line = stream.exchange("EHLO " + QHostInfo::localHostName().toStdString() + "\r\n");
    if (line.empty()) {
        /* No response to ESMTP, so try plain */
        line = stream.exchange("HELO " + QHostInfo::localHostName().toStdString() + "\r\n");
    }

    if (Util::starts_with(line, "250-")) {
        /* Ignore the first handshake line and advance to the capabilities */
        line = stream.nextLine();

        while (!line.empty()) {
            if (!Util::starts_with(line, "250"))
                break;
            if (line.length() < 4)
                break;

            auto caps = QString::fromStdString(line);
            caps = caps.mid(4).toUpper();
            if (caps.startsWith("AUTH")) {
                for (const auto &type : caps.split(' ')) {
                    if (type == "PLAIN") {
                        capabilities |= SMTP_Capability_Auth_PLAIN;
                    } else if (type == "LOGIN") {
                        capabilities |= SMTP_Capability_Auth_LOGIN;
                    } else if (type == "CRAM-MD5") {
                        capabilities |= SMTP_Capability_Auth_CRAMMD5;
                    }
                }
            }

            if (Util::starts_with(line, "250 "))
                break;
            line = stream.nextLine();
        }
    } else if (!Util::starts_with(line, "250 ")) {
        qCDebug(log_transfer_smtp) << "Invalid EHLO/HELO response response:" << line;
        return false;
    }

    return true;
}

static bool doClose(SMTPStream &stream)
{
    stream.write(Util::ByteView("QUIT\r\n"));
    return true;
}

static bool doAuthPLAIN(SMTPStream &stream,
                        const std::string &username,
                        const std::string &password)
{
    Util::ByteArray packed;
    packed += username;
    packed.push_back(0);
    packed += username;
    packed.push_back(0);
    packed += password;

    auto command = "AUTH PLAIN " + packed.toQByteArrayRef().toBase64() + "\r\n";
    auto response = stream.exchange(command.toStdString());
    if (Util::starts_with(response, "535")) {
        qCDebug(log_transfer_smtp) << "AUTH PLAIN rejected:" << response;
        return false;
    } else if (!Util::starts_with(response, "235")) {
        qCDebug(log_transfer_smtp) << "Invalid AUTH PLAIN response:" << response;
        return false;
    }

    return true;
}

static bool doAuthLogin(SMTPStream &stream,
                        const std::string &username,
                        const std::string &password)
{
    auto response = stream.exchange("AUTH LOGIN\r\n");
    if (!Util::starts_with(response, "334") || response.size() < 4) {
        qCDebug(log_transfer_smtp) << "Invalid response to AUTH LOGIN:" << response;
        return false;
    }

    QByteArray present;
    {
        auto query = QByteArray::fromStdString(response);
        query = query.mid(4);
        query = QByteArray::fromBase64(query).toLower();
        if (query.startsWith("pass")) {
            present = QByteArray::fromStdString(password);
        } else {
            present = QByteArray::fromStdString(username);
        }
    }
    present = present.toBase64() + "\r\n";
    response = stream.exchange(present.toStdString());
    if (!Util::starts_with(response, "334") || response.size() < 4) {
        qCDebug(log_transfer_smtp) << "Invalid response to first AUTH LOGIN line:" << response;
        return false;
    }

    {
        auto query = QByteArray::fromStdString(response);
        query = query.mid(4);
        query = QByteArray::fromBase64(query).toLower();
        if (query.startsWith("pass")) {
            present = QByteArray::fromStdString(password);
        } else {
            present = QByteArray::fromStdString(username);
        }
    }
    present = present.toBase64() + "\r\n";
    response = stream.exchange(present.toStdString());

    if (Util::starts_with(response, "535")) {
        qCDebug(log_transfer_smtp) << "AUTH LOGIN rejected:" << response;
        return false;
    } else if (!Util::starts_with(response, "235")) {
        qCDebug(log_transfer_smtp) << "Invalid AUTH LOGIN response:" << response;
        return false;
    }

    return true;
}

static bool doAuthCRAMMD5(SMTPStream &stream,
                          const std::string &username,
                          const std::string &password)
{
    auto response = stream.exchange("AUTH CRAM-MD5\r\n");
    if (!Util::starts_with(response, "334") || response.size() < 4) {
        qCDebug(log_transfer_smtp) << "Invalid response to AUTH CRAM-MD5:" << response;
        return false;
    }

    auto challenge = QByteArray::fromStdString(response);
    challenge = challenge.mid(4);

    QCryptographicHash hasher(QCryptographicHash::Md5);
    QByteArray hashed;
    QByteArray secret;

    secret = QByteArray(64, 0);
    std::memcpy(secret.data(), password.data(), std::min<int>(password.length(), 64));
    for (auto ptr = secret.data(), end = secret.data() + secret.length(); ptr != end; ++ptr) {
        *ptr ^= 0x36;
    }
    secret += challenge;
    hasher.reset();
    hasher.addData(secret);
    hashed = hasher.result();

    secret = QByteArray(64, 0);
    std::memcpy(secret.data(), password.data(), std::min<int>(password.length(), 64));
    for (auto ptr = secret.data(), end = secret.data() + secret.length(); ptr != end; ++ptr) {
        *ptr ^= 0x5C;
    }
    secret += hashed;
    hasher.reset();
    hasher.addData(secret);
    hashed = hasher.result();


    auto present = QByteArray::fromStdString(username) + " " + hashed.toHex();
    present = present.toBase64() + "\r\n";

    response = stream.exchange(present.toStdString());
    if (Util::starts_with(response, "535")) {
        qCDebug(log_transfer_smtp) << "AUTH CRAM-MD5 rejected:" << response;
        return false;
    } else if (!Util::starts_with(response, "235")) {
        qCDebug(log_transfer_smtp) << "Invalid AUTH CRAM-MD5 response:" << response;
        return false;
    }
    return true;
}

static bool doAvailableAuth(SMTPStream &stream,
                            const std::string &username,
                            const std::string &password,
                            std::uint32_t capabilities)
{
    if (username.empty() || password.empty())
        return true;

    bool authFailed = false;
    if (capabilities & SMTP_Capability_Auth_CRAMMD5) {
        if (doAuthCRAMMD5(stream, username, password))
            return true;
        authFailed = true;
    }
    if (capabilities & SMTP_Capability_Auth_LOGIN) {
        if (doAuthLogin(stream, username, password))
            return true;
        authFailed = true;
    }
    if (capabilities & SMTP_Capability_Auth_PLAIN) {
        if (doAuthPLAIN(stream, username, password))
            return true;
        authFailed = true;
    }
    if (authFailed)
        return false;

    qCDebug(log_transfer_smtp)
        << "Authentication requested, but no mechanisms available so continuing without it";
    return true;
}

void SMTPSend::sendToRelay(std::vector<std::string> &&recipients, Util::ByteArray &&message)
{
    class Execute final : public Sender {
        SMTPSend &parent;
        std::vector<std::string> recipients;
        Util::ByteArray message;

        std::thread thread;
        bool terminated;
        bool completed;

        void run()
        {
            IO::Socket::TCP::Configuration config(parent.relayPort);
            if (parent.relayTLS) {
                config.tls = [](QSslSocket &) {
                    return true;
                };
            }
            SMTPStream stream(parent.relayName, config, parent.mutex, parent.notify, terminated);

            std::uint32_t capabilities = 0;
            if (!doHandshake(stream, capabilities))
                return sendFailed();
            if (!doAvailableAuth(stream, parent.relayUsername, parent.relayPassword, capabilities))
                return sendFailed();
            if (!doMessageSend(stream, parent.from, recipients, message))
                return sendFailed();
            doClose(stream);
            return sendSucceeded();
        }

        void sendFailed()
        {
            qCDebug(log_transfer_smtp) << "Relay send failed";
            {
                std::lock_guard<std::mutex> lock(parent.mutex);
                completed = true;
                parent.allSendsOk = false;
            }
            parent.notify.notify_all();
        }

        void sendSucceeded()
        {
            qCDebug(log_transfer_smtp) << "Relay send succeeded";
            {
                std::lock_guard<std::mutex> lock(parent.mutex);
                completed = true;
                parent.anySentOk = true;
            }
            parent.notify.notify_all();
        }

    public:
        Execute(SMTPSend &parent, std::vector<std::string> &&recipients, Util::ByteArray &&message)
                : parent(parent),
                  recipients(std::move(recipients)),
                  message(std::move(message)),
                  terminated(false),
                  completed(false)
        {
            thread = std::thread(std::bind(&Execute::run, this));
        }

        virtual ~Execute()
        { thread.join(); }

        bool isComplete() override
        { return completed; }

        void signalTerminate() override
        {
            if (completed)
                return;
            qCDebug(log_transfer_smtp) << "Terminating relay send";
            terminated = true;
        }
    };
    senders.emplace_back(new Execute(*this, std::move(recipients), std::move(message)));
}

void SMTPSend::sendDirect(const std::string &host,
                          std::vector<std::string> &&recipients,
                          const Util::ByteArray &message)
{
    class Execute final : public Sender {
        SMTPSend &parent;
        std::string host;
        std::vector<std::string> recipients;
        Util::ByteArray message;

        std::thread thread;
        bool terminated;
        bool completed;

#if !defined(HAVE_DNSQUERY)

        Util::ByteArray queryMX(const std::string &host)
        {
#if defined(HAVE_RES_QUERY)
            Util::ByteArray response;
            response.resize(4096);
            auto rc = res_query(host.c_str(), C_IN, T_MX, response.data<unsigned char *>(),
                                response.size());
            if (rc <= 0)
                return {};
            response.resize(rc);
            return response;
#else
            auto socket =
                    IO::UDP::Configuration().listen(IO::UDP::Configuration::AddressFamily::Any)
                                            .create();
            if (!socket)
                return {};

            Util::ByteArray query;
            {
                query.resize(12, 0);

                auto target = query.data<uchar *>();

                auto queryID = Random::integer32() >> 16;
                qToBigEndian<quint16>(queryID, target);
                target += 2;

                /* Standard query with recursion */
                qToBigEndian<quint16>(0x0100, target);
                target += 2;

                /* One query entry */
                qToBigEndian<quint16>(1, target);

                /* All remaining count fields are already zeroed */

                for (const auto &part : Util::split_string(host, '.', true)) {
                    if (part.empty() || part.size() > 255)
                        continue;
                    query.push_back(part.size());
                    query.push_back(part.data(), part.size());
                }

                auto offset = query.size();
                query.resize(offset + 5);
                target = query.data<uchar *>(offset);

                /* Terminating root label */
                *target = 0;
                ++target;

                /* Type (MX) */
                qToBigEndian<quint16>(0x000F, target);
                target += 2;

                /* Class (IN) */
                qToBigEndian<quint16>(0x0001, target);
            }

            Util::ByteArray response;
            socket->received.connect([this, &response](const IO::UDP::Packet &packet) {
                {
                    std::lock_guard<std::mutex> lock(parent.mutex);
                    if (!response.empty())
                        return;
                    response = packet.data;
                }
                parent.notify.notify_all();
            });

            /*
             * This is a fallback path, so don't worry too much about getting the right DNS
             * server
             */
            socket->sendTo(std::move(query), "8.8.8.8", 53);

            {
                std::unique_lock<std::mutex> lock(parent.mutex);
                parent.notify.wait_for(lock, std::chrono::seconds(10), [this, &response] {
                    if (terminated)
                        return true;
                    return !response.empty();
                });
            }
            socket.reset();
            return response;
#endif
        }

        static bool skipDNSLabel(const uchar *&data, std::size_t &remaining)
        {
            for (;;) {
                if (remaining <= 0)
                    return false;
                auto skip = *(reinterpret_cast<const quint8 *>(data));
                remaining--;
                data++;

                /* A pointer, so this terminates it */
                if ((skip & 0xC0U) == 0xC0U) {
                    remaining--;
                    data++;
                    return true;
                }

                if (skip == 0)
                    return true;

                remaining -= skip;
                data += skip;
            }
        }

        static std::vector<std::string> parseMX(const Util::ByteArray &response)
        {
            if (response.size() <= 12)
                return {};
            auto data = response.data<const uchar *>();

            /* Skip ID */
            data += 2;

            auto flags = qFromBigEndian<quint16>(data);
            data += 2;
            if (!(flags & 0x8000U))
                return {};

            auto qcount = qFromBigEndian<quint16>(data);
            data += 2;
            auto acount = qFromBigEndian<quint16>(data);
            if (acount == 0)
                return {};

            data = response.data<const uchar *>(12);
            auto remaining = response.size() - 12;
            for (; qcount != 0; --qcount) {
                if (!skipDNSLabel(data, remaining))
                    return {};
                if (remaining <= 4)
                    return {};
                remaining -= 4;
                data += 4;
            }

            std::vector<std::string> result;
            for (; acount != 0; --acount) {
                if (!skipDNSLabel(data, remaining))
                    break;
                if (remaining <= 8)
                    break;
                remaining -= 8;

                auto type = qFromBigEndian<quint16>(data);
                data += 2;
                auto cls = qFromBigEndian<quint16>(data);
                data += 2;
                /* Skip TTL */
                data += 4;
                auto rlength = qFromBigEndian<quint16>(data);
                data += 2;

                auto decodePtr = data;
                auto decodeLength = remaining;
                remaining -= rlength;
                data += rlength;
                if (type != 0x000F || cls != 0x0001 || rlength <= 3 || remaining < 0)
                    continue;

                /* Skip preference */
                decodePtr += 2;
                decodeLength -= 2;

                std::string targetHost;
                for (std::size_t ittr = 0U; ittr < 64U; ++ittr) {
                    if (decodeLength <= 0)
                        break;
                    auto len = *(reinterpret_cast<const quint8 *>(decodePtr));
                    if (len == 0)
                        break;

                    /* This is a pointer, so move to that location */
                    if ((len & 0xC0U) == 0xC0U) {
                        if (decodeLength < 2)
                            break;
                        len = qFromBigEndian<quint16>(decodePtr);
                        len &= ~0xC000U;
                        decodePtr = response.data<const uchar *>(len);
                        decodeLength = response.size() - len;
                        continue;
                    }

                    if (len > decodeLength)
                        break;
                    decodePtr++;
                    decodeLength--;

                    if (!targetHost.empty())
                        targetHost.push_back('.');

                    targetHost += std::string(reinterpret_cast<const char *>(decodePtr), len);
                    decodePtr += len;
                    decodeLength -= len;
                }

                if (targetHost.empty())
                    continue;
                result.emplace_back(std::move(targetHost));
            }
            return result;
        }

#endif

        std::vector<std::string> resolveMX(const std::string &host)
        {
#if defined(HAVE_DNSQUERY)
            ::PDNS_RECORD first = nullptr;
            auto code =
                    ::DnsQuery_UTF8(host.c_str(), DNS_TYPE_MX, DNS_QUERY_STANDARD, nullptr, &first,
                                    nullptr);
            if (code != ERROR_SUCCESS) {
                if (first)
                    ::DnsRecordListFree(first, DnsFreeRecordList);
                return {};
            }

            std::vector<std::string> result;
            for (auto rec = first; rec; rec = rec->pNext) {
                if (rec->wType != DNS_TYPE_MX)
                    continue;
                if (rec->Flags.S.Section != DNSREC_ANSWER)
                    continue;
#if defined(UNICODE)
                result.emplace_back(QString::fromWCharArray(
                        reinterpret_cast<const wchar_t *>(rec->Data.MX.pNameExchange)).
                        toStdString());
#else
                result.emplace_back(reinterpret_cast<const char *>(rec->Data.MX.pNameExchange));
#endif
            }

            ::DnsRecordListFree(first, DnsFreeRecordList);
            return result;
#else
            return parseMX(queryMX(host));
#endif
        }

        void run()
        {
            for (const auto &connectHost : resolveMX(host)) {
                SMTPStream stream(connectHost, {25}, parent.mutex, parent.notify, terminated);

                std::uint32_t capabilities = 0;
                if (!doHandshake(stream, capabilities))
                    continue;
                if (!doMessageSend(stream, parent.from, recipients, message))
                    return sendFailed();
                doClose(stream);
                return sendSucceeded();
            }
            return sendFailed();
        }

        void sendFailed()
        {
            qCDebug(log_transfer_smtp) << "Direct send failed";
            {
                std::lock_guard<std::mutex> lock(parent.mutex);
                completed = true;
                parent.allSendsOk = false;
            }
            parent.notify.notify_all();
        }

        void sendSucceeded()
        {
            qCDebug(log_transfer_smtp) << "Direct send succeeded";
            {
                std::lock_guard<std::mutex> lock(parent.mutex);
                completed = true;
                parent.anySentOk = true;
            }
            parent.notify.notify_all();
        }

    public:
        Execute(SMTPSend &parent,
                std::string host,
                std::vector<std::string> &&recipients,
                Util::ByteArray message) : parent(parent),
                                           host(std::move(host)),
                                           recipients(std::move(recipients)),
                                           message(std::move(message)),
                                           terminated(false),
                                           completed(false)
        {
            thread = std::thread(std::bind(&Execute::run, this));
        }

        virtual ~Execute()
        { thread.join(); }

        bool isComplete() override
        { return completed; }

        void signalTerminate() override
        {
            if (completed)
                return;
            qCDebug(log_transfer_smtp) << "Terminating direct send";
            terminated = true;
        }
    };
    senders.emplace_back(new Execute(*this, host, std::move(recipients), message));
}

void SMTPSend::start()
{
    Q_ASSERT(senders.empty());
    if (reaper.joinable())
        reaper.join();

    if (messages.empty()) {
        qCDebug(log_transfer_smtp) << "Send completed because there are no messages to send";
        finished();
        return;
    }

    QRegularExpression extractAddress("<([^>]+)>");
    for (auto &msg : messages) {
        if (msg.insertToHeader) {
            std::string toHeader;
            for (auto &r : msg.recipients) {
                if (!toHeader.empty())
                    toHeader += ", ";
                toHeader += r;

                auto match = extractAddress.match(QString::fromStdString(r));
                if (match.hasMatch()) {
                    r = match.captured(1).toStdString();
                }
            }
            msg.message -= "To: " + toHeader + "\r\n";
        }
        if (!fromHeader.empty())
            msg.message -= fromHeader;
    }

    switch (relayMode) {
    case RelayMode::Direct: {
        for (auto &msg : messages) {
            if (msg.recipients.empty())
                continue;
            std::unordered_map<std::string, std::vector<std::string>> toHosts;
            for (auto &r : msg.recipients) {
                auto parts = Util::halves(r, '@');
                if (parts.second.empty())
                    continue;
                toHosts[parts.second].emplace_back(std::move(r));
            }
            for (auto &host : toHosts) {
                sendDirect(host.first, std::move(host.second), msg.message);
            }
        }
        break;
    }
    case RelayMode::Relay: {
        for (auto &msg : messages) {
            if (msg.recipients.empty())
                continue;
            sendToRelay(std::move(msg.recipients), std::move(msg.message));
        }
        break;
    }
    case RelayMode::LocalProgram: {
        for (auto &msg : messages) {
            if (msg.recipients.empty())
                continue;
            sendToProgram(std::move(msg.message));
        }
        break;
    }
    }
    messages.clear();

    if (senders.empty()) {
        qCDebug(log_transfer_smtp) << "Send completed because no senders where created";
        finished();
        return;
    }

    reaper = std::thread([this] {
        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            if (senders.empty()) {
                qCDebug(log_transfer_smtp) << "All senders complete";
                lock.unlock();
                finished();
                return;
            }
            for (auto s = senders.begin(); s != senders.end();) {
                if (!(*s)->isComplete()) {
                    ++s;
                    continue;
                }
                auto del = s->release();
                senders.erase(s);
                lock.unlock();
                delete del;
                notify.notify_all();
                break;
            }
            if (!lock) {
                lock.lock();
                continue;
            }
            notify.wait(lock);
        }
    });
}

void SMTPSend::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        for (const auto &s : senders) {
            s->signalTerminate();
        }
    }
    notify.notify_all();
}

void SMTPSend::setFrom(const std::string &address, bool insert)
{
    if (address.empty()) {
        if (!insert) {
            from.clear();
            fromHeader.clear();
            return;
        }

        auto localUser = Environment::user();
        localUser.replace(QRegularExpression("\\s+"), QString());
        localUser += "@";
        localUser += QHostInfo::localHostName();

        from = localUser.toStdString();
        fromHeader = "From: ";
        fromHeader += from;
        fromHeader += "\r\n";
        return;
    }

    QRegularExpression extractAddress("<([^>]+)>");
    auto match = extractAddress.match(QString::fromStdString(address));
    if (match.hasMatch()) {
        from = match.captured(1).toStdString();
    } else {
        from = address;
    }
    if (insert) {
        fromHeader = "From: ";
        fromHeader += from;
        fromHeader += "\r\n";
    } else {
        fromHeader.clear();
    }
}

void SMTPSend::addMessage(const std::vector<std::string> &recipients,
                          const Util::ByteArray &message,
                          bool insertToHeader)
{
    QueuedMessage add;
    add.recipients = recipients;
    add.message = message;
    add.insertToHeader = insertToHeader;
    messages.emplace_back(std::move(add));
}

void SMTPSend::addMessage(const QStringList &recipients,
                          const QByteArray &message,
                          bool insertToHeader)
{
    QueuedMessage add;
    for (const auto &r : recipients) {
        add.recipients.emplace_back(r.toStdString());
    }
    add.message = message;
    add.insertToHeader = insertToHeader;
    messages.emplace_back(std::move(add));
}

bool SMTPSend::wait(double timeout)
{ return Threading::waitForTimeout(timeout, mutex, notify, [this] { return senders.empty(); }); }

bool SMTPSend::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return senders.empty();
}

bool SMTPSend::allMessagesSent() const
{
    Q_ASSERT(senders.empty());
    return allSendsOk;
}

bool SMTPSend::anyMessageSent() const
{
    Q_ASSERT(senders.empty());
    return allSendsOk;
}

SMTPSend::QueuedMessage::QueuedMessage() : insertToHeader(false)
{ }

}
}
