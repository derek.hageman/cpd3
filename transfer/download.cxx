/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QDataStream>
#include <QLoggingCategory>
#include <QUrl>

#include "transfer/download.hxx"
#include "algorithms/cryptography.hxx"
#include "core/process.hxx"
#include "core/timeparse.hxx"
#include "datacore/variant/composite.hxx"
#include "io/drivers/file.hxx"
#include "io/process.hxx"


Q_LOGGING_CATEGORY(log_transfer_download, "cpd3.transfer.download", QtWarningMsg)


using namespace CPD3::Data;

namespace CPD3 {
namespace Transfer {

static QString passwordOrPasswordFile(const Variant::Read &config, const QString &def = QString())
{
    QString password(config.hash("Password").toQString());
    if (password.isEmpty()) {
        QString fileName(config.hash("PasswordFile").toQString());
        if (!fileName.isEmpty() && QFile::exists(fileName)) {
            if (auto file = IO::Access::file(fileName, IO::File::Mode::readOnly())->block()) {
                QByteArray contents;
                file->read(contents);
                password = QString::fromUtf8(contents);
            } else {
                qCDebug(log_transfer_download) << "Failed to open password file" << fileName;
            }
        }
    }
    if (password.isEmpty())
        return def;
    return password;
}

FileDownloader::FileDownloader() : lastTimeExecuted(FP::undefined()),
                                   currentExecution(FP::undefined()),
                                   latestFileModification(FP::undefined()),
                                   terminated(false),
                                   downloadedFiles(),
                                   findLocalDirectories(false)
{ }

FileDownloader::~FileDownloader() = default;

void FileDownloader::signalTerminate()
{
    terminated = true;
    terminateRequested();
}

bool FileDownloader::testTerminated()
{
    return terminated;
}

static const quint8 downloadStateVersion = 1;

FileDownloader::TimeState::TimeState() : latestTime(FP::undefined()),
                                         referenced(false),
                                         nextLatestTime(FP::undefined())
{ }


void FileDownloader::restoreState(const QByteArray &state)
{
    timeState.clear();

    QDataStream stream(state);
    quint8 version = 0;
    stream >> version;
    if (version != downloadStateVersion)
        return;

    quint32 n = 0;
    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        QByteArray key;
        stream >> key;

        TimeState value;
        stream >> value.latestTime;
        stream >> value.equalToLatest;

        value.nextLatestTime = value.latestTime;
        value.nextEqualToLatest = value.equalToLatest;

        timeState.emplace(Util::ByteArray(key), std::move(value));
    }
}

QByteArray FileDownloader::saveState(bool pruneUnused) const
{
    QByteArray result;
    {
        QDataStream stream(&result, QIODevice::WriteOnly);
        stream << downloadStateVersion;

        quint32 n = static_cast<quint32>(timeState.size());
        if (pruneUnused) {
            for (const auto &add : timeState) {
                if (!add.second.referenced)
                    n--;
            }
        }

        stream << n;
        for (const auto &add : timeState) {
            if (pruneUnused && !add.second.referenced)
                continue;

            stream << add.first.toQByteArrayRef();

            stream << add.second.latestTime;
            stream << add.second.equalToLatest;
        }
    }
    return result;
}

void FileDownloader::setupExecutionBounds(double lastTimeExecuted, double currentExecution)
{
    this->lastTimeExecuted = lastTimeExecuted;
    this->currentExecution = currentExecution;
}

void FileDownloader::TimeState::updateNext(DownloadFile &file)
{
    double modified = file.getModifiedTime();

    if (!FP::defined(modified))
        return;
    if (FP::defined(nextLatestTime) && nextLatestTime > modified)
        return;

    if (!FP::defined(nextLatestTime) || modified != nextLatestTime) {
        nextLatestTime = modified;
        nextEqualToLatest.clear();
    }

    nextEqualToLatest.insert(Util::ByteArray(file.id()));
}

std::vector<std::unique_ptr<DownloadFile>> FileDownloader::takeFiles()
{
    std::vector<std::unique_ptr<DownloadFile>> result
            (std::make_move_iterator(downloadedFiles.begin()),
             std::make_move_iterator(downloadedFiles.end()));
    downloadedFiles.clear();

    for (auto &add : timeState) {
        add.second.latestTime = add.second.nextLatestTime;
        add.second.equalToLatest = add.second.nextEqualToLatest;
    }

    return std::move(result);
}


DownloadFile::DownloadFile() : fileTime(FP::undefined())
{ }

DownloadFile::~DownloadFile() = default;

QString DownloadFile::description() const
{ return fileName(); }

QString DownloadFile::fileName() const
{ return QString(); }

double DownloadFile::getModifiedTime() const
{ return FP::undefined(); }

Util::ByteArray DownloadFile::id() const
{ return {}; }

double DownloadFile::getFileTime() const
{ return fileTime; }

QString DownloadFile::getPattern() const
{ return pattern; }

IO::URL::Backing *DownloadFile::getURL() const
{ return nullptr; }

void DownloadFile::abortAcquire()
{ }


namespace {
class DownloadLocalFile final : public DownloadFile {
    QFileInfo localFile;
    double modifiedTime;
    bool isWritable;

    static double localModifiedTime(const QFileInfo &info)
    {
        double modifiedTime = FP::undefined();
        QDateTime fmod(info.lastModified());
        if (fmod.isValid()) {
            modifiedTime = Time::fromDateTime(fmod.toUTC(), true);
        }
        return modifiedTime;
    }

public:
    DownloadLocalFile(const QString &path, bool isWritable) : localFile(path),
                                                              modifiedTime(
                                                                      localModifiedTime(localFile)),
                                                              isWritable(isWritable)
    { }

    DownloadLocalFile(QFileInfo info, bool isWritable) : localFile(std::move(info)),
                                                         modifiedTime(localModifiedTime(localFile)),
                                                         isWritable(isWritable)
    { }

    virtual ~DownloadLocalFile() = default;

    QString fileName() const override
    { return localFile.fileName(); }

    Util::ByteArray id() const override
    {
        auto result = Util::ByteArray::filled(1);
        result += localFile.canonicalFilePath().toUtf8();
        return result;
    }

    QString description() const override
    { return localFile.absoluteFilePath(); }

    double getModifiedTime() const override
    { return modifiedTime; }

    QFileInfo fileInfo() const override
    { return localFile; }

    bool writable() const override
    { return isWritable; }

protected:

    bool acquire() override
    {
        if (!IO::Access::file(localFile, IO::File::Mode::readOnly())->block()) {
            qCDebug(log_transfer_download) << "Failed to open local file"
                                           << localFile.absoluteFilePath();
            return false;
        }
        return true;
    }
};

class DownloadURLFile : public DownloadFile {
    std::shared_ptr<IO::URL::Backing> request;
    double modifiedTime;
    std::shared_ptr<IO::File::Backing> localFile;

    std::mutex mutex;
    std::condition_variable cv;
    bool aborted;
public:
    explicit DownloadURLFile(std::shared_ptr<IO::URL::Backing> request,
                             double modifiedTime = FP::undefined()) : request(std::move(request)),
                                                                      modifiedTime(FP::undefined()),
                                                                      aborted(false)
    { }

    virtual ~DownloadURLFile() = default;

    QString fileName() const override
    {
        QString result(request->url().path());
        while (result.endsWith('/')) {
            result.chop(1);
        }
        if (result.isEmpty())
            return request->url().host();
        return result.section('/', -1);
    }

    Util::ByteArray id() const override
    { return Util::ByteArray(request->url().toString(QUrl::RemovePassword).toUtf8()); }

    QString description() const override
    { return request->url().toString(QUrl::RemovePassword); }

    double getModifiedTime() const override
    { return modifiedTime; }

    QFileInfo fileInfo() const override
    {
        if (!localFile)
            return {};
        return QFileInfo(QString::fromStdString(localFile->filename()));
    }

    bool writable() const override
    { return false; }

    IO::URL::Backing *getURL() const override
    { return request.get(); }

    void abortAcquire() override
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            aborted = true;
        }
        cv.notify_all();
    }

protected:

    bool acquire() override
    {
        localFile = IO::Access::temporaryFile();
        if (!localFile) {
            qCDebug(log_transfer_download) << "Failed to create temporary file for URL download"
                                           << description();
            return false;
        }
        auto outputStream = localFile->stream();
        if (!outputStream) {
            qCDebug(log_transfer_download) << "Failed to open temporary file for URL download"
                                           << description();
            return false;
        }
        auto downloadStream = request->stream();
        if (!downloadStream) {
            qCDebug(log_transfer_download) << "Failed to create URL download stream"
                                           << description();
            return false;
        }

        bool isEnded = false;
        Threading::Receiver rx;
        static_cast<IO::URL::Stream *>(downloadStream.get())->receiveProgress
                                                            .connect(rx, [this](double fraction) {
                                                                feedback.emitProgressFraction(
                                                                        fraction);
                                                            });

        downloadStream->read.connect(rx, [&outputStream](const Util::ByteArray &data) {
            outputStream->write(data);
        });
        downloadStream->ended.connect(rx, [this, &isEnded]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                isEnded = true;
            }
            cv.notify_all();
        });
        outputStream->writeStall.connect(rx, [&downloadStream](bool stalled) {
            downloadStream->readStall(stalled);
        });

        downloadStream->start();

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [this, &isEnded] { return isEnded || aborted; });
            if (aborted) {
                return false;
            }
        }
        return true;
    }
};

class DownloadURLFileGET final : public DownloadURLFile {
public:
    explicit DownloadURLFileGET(std::shared_ptr<IO::URL::Backing> request,
                                double modifiedTime = FP::undefined()) : DownloadURLFile(
            std::move(request), modifiedTime)
    { }

    virtual ~DownloadURLFileGET() = default;

    Util::ByteArray id() const override
    {
        auto result = Util::ByteArray::filled(3);
        result += DownloadURLFile::id();
        return result;
    }
};

class DownloadURLFilePOST final : public DownloadURLFile {
public:
    explicit DownloadURLFilePOST(std::shared_ptr<IO::URL::Backing> request,
                                 double modifiedTime = FP::undefined()) : DownloadURLFile(
            std::move(request), modifiedTime)
    { }

    virtual ~DownloadURLFilePOST() = default;

    Util::ByteArray id() const override
    {
        auto result = Util::ByteArray::filled(4);
        result += DownloadURLFile::id();
        return result;
    }
};

class DownloadURLFilePUT final : public DownloadURLFile {
public:
    explicit DownloadURLFilePUT(std::shared_ptr<IO::URL::Backing> request,
                                double modifiedTime = FP::undefined()) : DownloadURLFile(
            std::move(request), modifiedTime)
    { }

    virtual ~DownloadURLFilePUT() = default;

    Util::ByteArray id() const override
    {
        auto result = Util::ByteArray::filled(5);
        result += DownloadURLFile::id();
        return result;
    }
};

class DownloadProcessOutput : public DownloadFile {
    IO::Process::Spawn spawn;
    std::shared_ptr<IO::File::Backing> localFile;

    std::mutex mutex;
    bool aborted;
    bool ignoreExitStatus;
    std::int64_t requiredExitCode;

    Threading::Signal<> abortProcess;
public:
    explicit DownloadProcessOutput(IO::Process::Spawn spawn,
                                   const Variant::Read &settings = {},
                                   std::shared_ptr<IO::File::Backing> localFile = {}) : spawn(
            std::move(spawn)),
                                                                                        localFile(
                                                                                                std::move(
                                                                                                        localFile)),
                                                                                        aborted(false),
                                                                                        ignoreExitStatus(
                                                                                                settings["Exit/IgnoreStatus"]
                                                                                                        .toBoolean()),
                                                                                        requiredExitCode(
                                                                                                settings["Exit/RequireCode"]
                                                                                                        .toInteger())
    { }

    virtual ~DownloadProcessOutput() = default;

    QString description() const override
    {
        std::string desc = spawn.command;
        for (const auto &add : spawn.arguments) {
            desc += " ";
            desc += add;
        }
        return QString::fromStdString(desc);
    }

    QFileInfo fileInfo() const override
    {
        if (!localFile)
            return {};
        return QFileInfo(QString::fromStdString(localFile->filename()));
    }

    bool writable() const override
    { return false; }

    void abortAcquire() override
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            aborted = true;
        }
        abortProcess();
    }

protected:
    bool acquire() override
    {
        if (!localFile)
            return false;

        auto proc = spawn.create();
        if (!proc) {
            qCDebug(log_transfer_download) << "Failed to create process";
            return false;
        }

        Threading::Receiver rx;
        abortProcess.connect(rx, std::bind(&IO::Process::Instance::terminate, proc.get()));

        IO::Process::Instance::ExitMonitor exitOk(*proc);
        if (!proc->start()) {
            qCDebug(log_transfer_download) << "Failed to start process";
            return false;
        }
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (aborted)
                return false;
        }
        if (!proc->wait()) {
            qCDebug(log_transfer_download) << "Process wait failed";
            return false;
        }

        if (!ignoreExitStatus && !exitOk.isNormal()) {
            qCDebug(log_transfer_download) << "Process exited with abnormal state";
            return false;
        }

        if (INTEGER::defined(requiredExitCode) && exitOk.getExitCode() != requiredExitCode) {
            qCDebug(log_transfer_download) << "Process exited with code" << exitOk.getExitCode();
            return false;
        }

        return true;
    }
};

class DownloadFTPFile final : public DownloadFile {
    FTPConnection::File remoteFile;
    std::weak_ptr<FTPConnection> connection;
    std::shared_ptr<IO::File::Backing> localFile;
    std::string remoteHost;
public:
    explicit DownloadFTPFile(FTPConnection::File file,
                             const std::shared_ptr<FTPConnection> &connection = {}) : remoteFile(
            std::move(file)), connection(connection)
    {
        if (connection) {
            remoteHost = connection->getHost();
        }
    }

    virtual ~DownloadFTPFile() = default;

    QString fileName() const override
    { return QString::fromStdString(remoteFile.fileName()); }

    QString description() const override
    {
        return QString("ftp://%1/%2").arg(QString::fromStdString(remoteHost),
                                          QString::fromStdString(remoteFile.filePath()));
    }

    double getModifiedTime() const override
    { return remoteFile.modified(); }

    QFileInfo fileInfo() const override
    {
        if (!localFile)
            return {};
        return QFileInfo(QString::fromStdString(localFile->filename()));
    }

    Util::ByteArray id() const override
    {
        auto result = Util::ByteArray::filled(2);
        result += remoteHost;
        result.push_back(1);
        result += remoteFile.filePath();
        return result;
    }

    bool writable() const override
    { return false; }

protected:
    bool acquire() override
    {
        auto ftp = connection.lock();
        if (!ftp)
            return false;

        localFile = IO::Access::temporaryFile();
        if (!localFile) {
            qCDebug(log_transfer_download) << "Failed to create temporary file for FTP download"
                                           << description();
            return false;
        }

        Threading::Receiver rx;
        ftp->progress.connect(rx, [this](std::uint_fast64_t done, std::uint_fast64_t total) {
            if (!total)
                return;
            feedback.progressFraction(static_cast<double>(done) / static_cast<double>(total));
        });

        return ftp->get(remoteFile, *localFile);
    }
};

}

void FileDownloader::configureRequest(IO::URL::Backing &request, const Variant::Read &settings)
{
    if (settings["SSL"].exists()) {
        QSslConfiguration ssl;
        Algorithms::Cryptography::configureSSL(ssl, settings["SSL"]);
        request.tls(ssl);
    }

    for (auto header : settings["Headers"].toHash()) {
        request.header(applySubstitutions(QString::fromStdString(header.first)).toUtf8(),
                       applySubstitutions(header.second.toQString()).toUtf8());
    }
}

std::unique_ptr<DownloadFile> FileDownloader::createProcess(const Data::Variant::Read &settings)
{
    auto localFile = IO::Access::temporaryFile();
    if (!localFile) {
        qCDebug(log_transfer_download) << "Failed to create temporary file for process output";
    }

    auto program = settings["Program"].toQString();
    QStringList arguments;
    if (settings["Arguments"].getType() == Variant::Type::String) {
        arguments = settings["Arguments"].toQString().split(' ', QString::SkipEmptyParts);
    } else {
        for (auto add : settings["Arguments"].toArray()) {
            arguments.append(add.toQString());
        }
    }

    if (localFile && !settings["Literal"].toBool()) {
        pushTemporaryOutput(QFileInfo(QString::fromStdString(localFile->filename())));
        program = applySubstitutions(program);
        for (auto &arg : arguments) {
            arg = applySubstitutions(arg);
        }
        popTemporaryOutput();
    }

    IO::Process::Spawn spawn(program, arguments);
    spawn.inputStream = IO::Process::Spawn::StreamMode::Discard;
    spawn.errorStream = IO::Process::Spawn::StreamMode::Forward;
    if (settings["CaptureOutput"].toBool()) {
        spawn.outputStream = IO::Process::Spawn::StreamMode::File;
        spawn.outputFile = localFile->filename();
    } else {
        spawn.outputStream = IO::Process::Spawn::StreamMode::Forward;
    }

    return std::unique_ptr<DownloadFile>(
            new DownloadProcessOutput(std::move(spawn), settings, std::move(localFile)));
}

static QUrl toURL(const QString &input)
{
    return QUrl(QUrl::fromUserInput(input));
}

static IO::Access::Handle toUploadData(const Variant::Read &settings)
{
    QByteArray data;
    switch (settings["Data"].getType()) {
    case Variant::Type::Bytes:
        data = settings["Data"].toBinary();
        break;
    default:
        data = QByteArray::fromStdString(settings["Data"].toString());
        break;
    }
    return IO::Access::buffer(Util::ByteArray(data));
}

std::unique_ptr<DownloadFile> FileDownloader::createSingleFetch(const Variant::Read &config)
{
    const auto &type = config["Source"].toString();
    if (Util::equal_insensitive(type, "local", "file")) {
        return std::unique_ptr<DownloadFile>(
                new DownloadLocalFile(applySubstitutions(config["File"].toQString()),
                                      config["Writable"].toBool()));
    } else if (Util::equal_insensitive(type, "post")) {
        if (!urlContext)
            urlContext = std::make_shared<IO::URL::Context>();

        return std::unique_ptr<DownloadFile>(new DownloadURLFilePOST(
                std::static_pointer_cast<IO::URL::Backing>(
                        IO::Access::urlPOST(toURL(applySubstitutions(config["File"].toQString())),
                                            toUploadData(config), urlContext))));
    } else if (Util::equal_insensitive(type, "put")) {
        if (!urlContext)
            urlContext = std::make_shared<IO::URL::Context>();

        return std::unique_ptr<DownloadFile>(new DownloadURLFilePUT(
                std::static_pointer_cast<IO::URL::Backing>(
                        IO::Access::urlPUT(toURL(applySubstitutions(config["File"].toQString())),
                                           toUploadData(config), urlContext))));
    } else if (Util::equal_insensitive(type, "process")) {
        return createProcess(config);
    }

    if (!urlContext)
        urlContext = std::make_shared<IO::URL::Context>();
    return std::unique_ptr<DownloadFile>(new DownloadURLFileGET(
            std::static_pointer_cast<IO::URL::Backing>(
                    IO::Access::urlGET(toURL(applySubstitutions(config["File"].toQString())),
                                       urlContext))));
}

std::unique_ptr<DownloadFile> FileDownloader::createListFetch(const Variant::Read &config)
{ return createSingleFetch(config); }

bool FileDownloader::convertListResponse(const Data::Variant::Read &config,
                                         DownloadFile &response,
                                         std::vector<std::unique_ptr<DownloadFile>> &files)
{
    auto file = IO::Access::file(response.fileInfo(), IO::File::Mode::readOnly())->block();
    if (!file) {
        feedback.emitFailure(QObject::tr("List open failure"));
        return false;
    }

    const auto &type = config["Fetch/Source"].toString();

    while (auto line = file->readLine()) {
        auto name = line.toQString();
        if (name.isEmpty())
            continue;

        if (Util::equal_insensitive(type, "local", "file")) {
            files.emplace_back(new DownloadLocalFile(name, config["Fetch/Writable"].toBool()));
        } else if (Util::equal_insensitive(type, "post")) {
            if (!urlContext)
                urlContext = std::make_shared<IO::URL::Context>();

            files.emplace_back(new DownloadURLFilePOST(std::static_pointer_cast<IO::URL::Backing>(
                    IO::Access::urlPOST(toURL(name), toUploadData(config["Fetch"]), urlContext))));
        } else if (Util::equal_insensitive(type, "put")) {
            if (!urlContext)
                urlContext = std::make_shared<IO::URL::Context>();

            files.emplace_back(new DownloadURLFilePUT(std::static_pointer_cast<IO::URL::Backing>(
                    IO::Access::urlPUT(toURL(name), toUploadData(config["Fetch"]), urlContext))));
        } else if (Util::equal_insensitive(type, "process")) {
            auto localFile = IO::Access::temporaryFile();
            if (!localFile) {
                qCDebug(log_transfer_download)
                    << "Failed to create temporary file for process output";
            }

            IO::Process::Spawn spawn;
            if (config["Fetch/Program"].exists()) {
                auto program = config["Fetch/Program"].toQString();
                QStringList arguments;
                if (config["Fetch/Arguments"].getType() == Variant::Type::String) {
                    arguments = config["Fetch/Arguments"].toQString()
                                                         .split(' ', QString::SkipEmptyParts);
                } else {
                    for (auto add : config["Fetch/Arguments"].toArray()) {
                        arguments.append(add.toQString());
                    }
                }
                arguments.push_back(name);

                if (localFile &&
                        config["Fetch/Literal"].exists() &&
                        !config["Fetch/Literal"].toBool()) {
                    pushTemporaryOutput(QFileInfo(QString::fromStdString(localFile->filename())));
                    program = applySubstitutions(program);
                    for (auto &arg : arguments) {
                        arg = applySubstitutions(arg);
                    }
                    popTemporaryOutput();
                }

                spawn = IO::Process::Spawn(program, arguments);
            } else {
                if (localFile &&
                        config["Fetch/Literal"].exists() &&
                        !config["Fetch/Literal"].toBool()) {
                    pushTemporaryOutput(QFileInfo(QString::fromStdString(localFile->filename())));
                    name = applySubstitutions(name);
                    popTemporaryOutput();
                }

                spawn = IO::Process::Spawn::shell(name);
            }

            spawn.inputStream = IO::Process::Spawn::StreamMode::Discard;
            spawn.errorStream = IO::Process::Spawn::StreamMode::Forward;
            if (config["Fetch/CaptureOutput"].toBool()) {
                spawn.outputStream = IO::Process::Spawn::StreamMode::File;
                spawn.outputFile = localFile->filename();
            } else {
                spawn.outputStream = IO::Process::Spawn::StreamMode::Forward;
            }

            files.emplace_back(new DownloadProcessOutput(std::move(spawn), config["Fetch"],
                                                         std::move(localFile)));
        } else {
            if (!urlContext)
                urlContext = std::make_shared<IO::URL::Context>();

            files.emplace_back(new DownloadURLFileGET(std::static_pointer_cast<IO::URL::Backing>(
                    IO::Access::urlGET(toURL(name), urlContext))));
        }
    }

    return true;
}

static void setCaptureIntegerIfValid(const QString &cap, int &output, int min = -1, int max = -1)
{
    if (output != -1)
        return;
    bool ok = false;
    int result = cap.toInt(&ok);
    if (!ok)
        return;
    if (min != -1 && result < min)
        return;
    if (max != -1 && result > max)
        return;
    output = result;
}

static void setCaptureDoubleIfValid(const QString &cap,
                                    double &output,
                                    double min = -1.0,
                                    double max = -1.0)
{
    if (output != -1.0)
        return;
    bool ok = false;
    double result = cap.toDouble(&ok);
    if (!ok)
        return;
    if (min != -1.0 && result < min)
        return;
    if (max != -1.0 && result > max)
        return;
    output = result;
}

static double walkOffsetBackwards(double origin,
                                  int millisecond,
                                  int second = -1,
                                  int minute = -1,
                                  int hour = -1,
                                  int day = -1)
{
    if (!FP::defined(origin))
        return FP::undefined();
    if (millisecond != -1)
        origin += (double) millisecond / 1000.0;
    if (second != -1)
        origin += (double) second;
    if (minute != -1)
        origin += (double) (minute * 60);
    if (hour != -1)
        origin += (double) (hour * 3600);
    if (day != -1)
        origin += (double) (day * 86400);
    return origin;
}

FileDownloader::TimeExtractContext::TimeExtractContext() : extractedYear(-1),
                                                           extractedMonth(-1),
                                                           extractedDay(-1),
                                                           extractedHour(-1),
                                                           extractedMinute(-1),
                                                           extractedSecond(-1),
                                                           extractedMillisecond(-1),
                                                           extractedDOY(-1.0),
                                                           extractedGeneric()
{ }

void FileDownloader::TimeExtractContext::integrate(const QStringList &result,
                                                   const QList<TimeCapture> &captures)
{
    if (result.empty())
        return;
    for (int i = 0, max = std::min<int>(result.size() - 1, captures.size()); i < max; i++) {
        const auto &cap = result[i + 1];
        if (cap.isEmpty())
            continue;

        switch (captures[i]) {
        case TimeCapture::Ignore:
            break;
        case TimeCapture::Generic:
            extractedGeneric.append(cap);
            break;
        case TimeCapture::Year:
            setCaptureIntegerIfValid(cap, extractedYear, 1970, 2999);
            break;
        case TimeCapture::Month:
            setCaptureIntegerIfValid(cap, extractedMonth, 1, 12);
            break;
        case TimeCapture::Day:
            setCaptureIntegerIfValid(cap, extractedDay, 1, 31);
            break;
        case TimeCapture::Hour:
            setCaptureIntegerIfValid(cap, extractedHour, 0, 23);
            break;
        case TimeCapture::Minute:
            setCaptureIntegerIfValid(cap, extractedMinute, 0, 59);
            break;
        case TimeCapture::Second:
            setCaptureIntegerIfValid(cap, extractedSecond, 0, 60);
            break;
        case TimeCapture::Millisecond:
            setCaptureIntegerIfValid(cap, extractedMillisecond, 0, 999);
            break;
        case TimeCapture::DOY:
            setCaptureDoubleIfValid(cap, extractedDOY, 1, 366);
            break;
        case TimeCapture::ShortYear:
            if (extractedYear == -1) {
                bool ok = false;
                int v = cap.toInt(&ok);
                if (ok && v >= 0 && v <= 99) {
                    if (v >= 70) {
                        v += 1900;
                    } else {
                        v += 2000;
                    }
                    extractedYear = v;
                }
            }
            break;
        case TimeCapture::NamedMonth:
            if (extractedMonth == -1) {
                if (cap.compare("January", Qt::CaseInsensitive) == 0)
                    extractedMonth = 1;
                else if (cap.compare("February", Qt::CaseInsensitive) == 0)
                    extractedMonth = 2;
                else if (cap.compare("March", Qt::CaseInsensitive) == 0)
                    extractedMonth = 3;
                else if (cap.compare("April", Qt::CaseInsensitive) == 0)
                    extractedMonth = 4;
                else if (cap.compare("May", Qt::CaseInsensitive) == 0)
                    extractedMonth = 5;
                else if (cap.compare("June", Qt::CaseInsensitive) == 0)
                    extractedMonth = 6;
                else if (cap.compare("July", Qt::CaseInsensitive) == 0)
                    extractedMonth = 7;
                else if (cap.compare("August", Qt::CaseInsensitive) == 0)
                    extractedMonth = 8;
                else if (cap.compare("September", Qt::CaseInsensitive) == 0)
                    extractedMonth = 9;
                else if (cap.compare("October", Qt::CaseInsensitive) == 0)
                    extractedMonth = 10;
                else if (cap.compare("November", Qt::CaseInsensitive) == 0)
                    extractedMonth = 11;
                else if (cap.compare("December", Qt::CaseInsensitive) == 0)
                    extractedMonth = 12;
            }
            break;
        case TimeCapture::ShortMonth:
            if (extractedMonth == -1) {
                if (cap.startsWith("JAN", Qt::CaseInsensitive))
                    extractedMonth = 1;
                else if (cap.startsWith("FEB", Qt::CaseInsensitive))
                    extractedMonth = 2;
                else if (cap.startsWith("MAR", Qt::CaseInsensitive))
                    extractedMonth = 3;
                else if (cap.startsWith("APR", Qt::CaseInsensitive))
                    extractedMonth = 4;
                else if (cap.startsWith("MAY", Qt::CaseInsensitive))
                    extractedMonth = 5;
                else if (cap.startsWith("JUN", Qt::CaseInsensitive))
                    extractedMonth = 6;
                else if (cap.startsWith("JUL", Qt::CaseInsensitive))
                    extractedMonth = 7;
                else if (cap.startsWith("AUG", Qt::CaseInsensitive))
                    extractedMonth = 8;
                else if (cap.startsWith("SEP", Qt::CaseInsensitive))
                    extractedMonth = 9;
                else if (cap.startsWith("OCT", Qt::CaseInsensitive))
                    extractedMonth = 10;
                else if (cap.startsWith("NOV", Qt::CaseInsensitive))
                    extractedMonth = 11;
                else if (cap.startsWith("DEC", Qt::CaseInsensitive))
                    extractedMonth = 12;
            }
            break;
        case TimeCapture::HourMinuteInteger: {
            bool ok = false;
            int v = cap.toInt(&ok, 10);
            if (ok && v >= 0 && v <= 2359) {
                int minute = v % 100;
                if (minute >= 0 && minute <= 59) {
                    if (extractedMinute == -1) {
                        extractedMinute = minute;
                    }
                    if (extractedHour == -1) {
                        extractedHour = v / 100;
                    }
                }
            }
            break;
        }
        }
    }
}

void FileDownloader::TimeExtractContext::integrate(const QRegularExpressionMatch &expression,
                                                   const QList<TimeCapture> &captures)
{ return integrate(expression.capturedTexts(), captures); }

double FileDownloader::TimeExtractContext::extract(double reference) const
{
    if (!extractedGeneric.isEmpty()) {
        try {
            return TimeParse::parseListSingle(extractedGeneric);
        } catch (const TimeParsingException &) {
        }
    }

    if (extractedDOY != -1) {
        if (extractedYear != -1) {
            double out = Time::fromDateTime(
                    QDateTime(QDate(extractedYear, 1, 1), QTime(0, 0, 0), Qt::UTC));
            out += (extractedDOY - 1.0) * 86400.0;
            return out;
        }
        if (FP::defined(reference)) {
            int year = (Time::toDateTime(reference)).date().year();
            double out = Time::fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
            out += (extractedDOY - 1.0) * 86400.0;
            return out;
        }
    }

    if (extractedYear == -1 || extractedMonth == -1 || extractedDay == -1) {
        return walkOffsetBackwards(reference, extractedMillisecond, extractedSecond,
                                   extractedMinute, extractedHour, extractedDay);
    }

    if (extractedHour != -1 && extractedMinute != -1 && extractedSecond != -1) {
        QDateTime dt(QDate(extractedYear, extractedMonth, extractedDay),
                     QTime(extractedHour, extractedMinute, extractedSecond), Qt::UTC);
        if (!dt.isValid())
            return FP::undefined();
        double result = Time::fromDateTime(dt);
        if (FP::defined(result) && extractedMillisecond != -1)
            result += static_cast<double>(extractedMillisecond) / 1000.0;
        return result;
    }

    QDateTime dt(QDate(extractedYear, extractedMonth, extractedDay), QTime(0, 0, 0), Qt::UTC);
    if (dt.isValid())
        reference = Time::fromDateTime(dt);
    return walkOffsetBackwards(reference, extractedMillisecond, extractedSecond, extractedMinute,
                               extractedHour);
}


double FileDownloader::convertTime(const std::deque<MatchElement> &matched, double reference)
{
    TimeExtractContext context;
    for (auto m = matched.rbegin(), endM = matched.rend(); m != endM; ++m) {
        context.integrate(m->match, m->capture);
    }
    return context.extract(reference);
}

QString FileDownloader::getTimeCapture(const QStringList &elements, QList<TimeCapture> &captures)
{
    int index = 0;
    if (index >= elements.size())
        return QString();
    QString type(elements.at(index++).toLower());

    if (type == "anytime" || type == "isotime" || type == "time") {
        captures.append(TimeCapture::Year);
        captures.append(TimeCapture::Month);
        captures.append(TimeCapture::Day);
        captures.append(TimeCapture::Hour);
        captures.append(TimeCapture::Minute);
        captures.append(TimeCapture::Second);
        captures.append(TimeCapture::Millisecond);
        return R"((\d{4})-?(\d{2})-?(\d{2})[T ]?(\d{2}):?(\d{2}):?(\d{2})(?:\.\d+)?Z?)";
    } else if (type == "isostrict") {
        captures.append(TimeCapture::Year);
        captures.append(TimeCapture::Month);
        captures.append(TimeCapture::Day);
        captures.append(TimeCapture::Hour);
        captures.append(TimeCapture::Minute);
        captures.append(TimeCapture::Second);
        captures.append(TimeCapture::Millisecond);
        return R"((\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(?:\.\d+)?Z)";
    } else if (type == "year") {
        captures.append(TimeCapture::Year);
        return R"((\d{4}))";
    } else if (type == "shortyear") {
        captures.append(TimeCapture::ShortYear);
        return R"((\d{2}))";
    } else if (type == "month") {
        captures.append(TimeCapture::Month);
        return R"((\d{2}))";
    } else if (type == "relaxedmonth") {
        captures.append(TimeCapture::Month);
        return R"((\d{1,2}))";
    } else if (type == "day") {
        captures.append(TimeCapture::Day);
        return R"((\d{2}))";
    } else if (type == "relaxedday") {
        captures.append(TimeCapture::Day);
        return R"((\d{1,2}))";
    } else if (type == "hour") {
        captures.append(TimeCapture::Hour);
        return R"((\d{2}))";
    } else if (type == "relaxedhour") {
        captures.append(TimeCapture::Hour);
        return R"((\d{1,2}))";
    } else if (type == "minute") {
        captures.append(TimeCapture::Minute);
        return R"((\d{2}))";
    } else if (type == "relaxedminute") {
        captures.append(TimeCapture::Minute);
        return R"((\d{1,2}))";
    } else if (type == "second") {
        captures.append(TimeCapture::Second);
        return R"((\d{2}))";
    } else if (type == "relaxedsecond") {
        captures.append(TimeCapture::Second);
        return R"((\d{1,2}))";
    } else if (type == "millisecond" || type == "msec") {
        captures.append(TimeCapture::Millisecond);
        return R"((\d{3}))";
    } else if (type == "relaxedmillisecond") {
        captures.append(TimeCapture::Millisecond);
        return R"((\d{1,3}))";
    } else if (type == "namedmonth") {
        captures.append(TimeCapture::NamedMonth);
        return R"(((?:[jJ][aA][nN][uU][aA][rR][yY])|(?:[fF][eE][bB][rR][uU][aA][rR][yY])|(?:[mM][aA][rR][cC][hH])|(?:[aA][pP][rR][iI][lL])|(?:[mM][aA][yY])|(?:[jJ][uU][nN][eE])|(?:[jJ][uU][lL][yY])|(?:[aA][uU][gG][uU][sS][tT])|(?:[sS][eE][pP][tT][eE][mM][bB][eE][rR])|(?:[oO][cC][tT][oO][bB][eE][rR])|(?:[nN][oO][vV][eE][mM][bB][eE][rR])|(?:[dD][eE][cC][eE][mM][bB][eE][rR])))";
    } else if (type == "shortmonth") {
        captures.append(TimeCapture::ShortMonth);
        return R"(((?:[jJ][aA][nN])|(?:[fF][eE][bB])|(?:[mM][aA][rR])|(?:[aA][pP][rR])|(?:[mM][aA][yY])|(?:[jJ][uU][nN])|(?:[jJ][uU][lL])|(?:[aA][uU][gG])|(?:[sS][eE][pP][tT])|(?:[oO][cC][tT])|(?:[nN][oO][vV])|(?:[dD][eE][cC])))";
    } else if (type == "doy") {
        captures.append(TimeCapture::DOY);
        return R"((\d{1,3}(?:\.\d*)?))";
    } else if (type == "integerdoy") {
        captures.append(TimeCapture::DOY);
        return R"((\d{1,3}))";
    } else if (type == "integerhhmm" || type == "hhmminteger") {
        captures.append(TimeCapture::HourMinuteInteger);
        return R"((\d{1,4}))";
    } else if (type == "cap" || type == "capture" || type == "parse") {
        QString pattern = R"(\d+)";
        if (index < elements.size())
            pattern = elements[index++];

        if (!pattern.isEmpty()) {
            captures.append(TimeCapture::Generic);
            return "(" + pattern + ")";
        }
    }

    return {};
}

QString FileDownloader::applySubstitutions(const QString &input) const
{ return input; }

QString FileDownloader::applyMatching(const QString &input, QList<TimeCapture> &) const
{ return QRegularExpression::escape(applySubstitutions(input)); }

void FileDownloader::pushTemporaryOutput(const QFileInfo &)
{ }

void FileDownloader::popTemporaryOutput()
{ }

void FileDownloader::pushFilterDirectory(const QDir &, const QDir &)
{ }

void FileDownloader::pushFilterRemote(const std::string &, const std::string &)
{ }

void FileDownloader::popFilter()
{ }

bool FileDownloader::execSingle(const Variant::Read &config)
{
    auto add = createSingleFetch(config);
    if (!add)
        return true;
    std::vector<std::unique_ptr<DownloadFile>> files;
    files.emplace_back(std::move(add));

    if (!filterFiles(files, config, getTimeContext(config)))
        return false;

    if (testTerminated())
        return false;

    feedback.emitStage(QObject::tr("Acquire file"),
                       QObject::tr("The system is acquiring the file."), true);

    for (auto &file : files) {
        if (testTerminated())
            return false;

        if (!file->acquire()) {
            feedback.emitFailure();
            return false;
        }
    }

    std::move(std::make_move_iterator(files.begin()), std::make_move_iterator(files.end()),
              Util::back_emplacer(downloadedFiles));
    return true;
}

bool FileDownloader::execList(const Variant::Read &config)
{
    auto list = createListFetch(config);
    if (testTerminated())
        return false;

    feedback.emitStage(QObject::tr("List files"),
                       QObject::tr("The system is fetching the file list."), true);

    if (!list || !list->acquire()) {
        feedback.emitFailure();
        return false;
    }

    feedback.emitStage(QObject::tr("Process list"), QObject::tr(
            "The system is processing the file list and determining files to acquire."), false);
    std::vector<std::unique_ptr<DownloadFile>> files;
    if (!convertListResponse(config, *list, files))
        return false;
    list.reset();

    if (!filterFiles(files, config, getTimeContext(config)))
        return false;

    if (files.empty())
        return true;

    for (auto &file : files) {
        if (testTerminated())
            return false;

        feedback.emitStage(QObject::tr("Acquire %1").arg(file->fileName()),
                           QObject::tr("The system is acquiring the file %1.").arg(
                                   file->description()), true);

        if (!file->acquire()) {
            feedback.emitFailure();
            return false;
        }
    }

    std::move(std::make_move_iterator(files.begin()), std::make_move_iterator(files.end()),
              Util::back_emplacer(downloadedFiles));
    return true;
}

FileDownloader::MatchElement FileDownloader::buildMatch(const QString &result,
                                                        bool caseSensitive) const
{
    QList<TimeCapture> capture;
    FileDownloader::MatchElement me(applyMatching(result, capture), result, caseSensitive);
    me.capture = capture;
    return me;
}

FileDownloader::MatchElement FileDownloader::buildMatch(const Data::Variant::Read &config) const
{
    switch (config.getType()) {
    case Variant::Type::Hash:
        return buildMatch(config.hash("Pattern").toQString(),
                          config.hash("CaseSensitive").toBool());
    default:
        return buildMatch(config.toQString());
    }
}

bool FileDownloader::MatchElement::apply(const QString &str)
{
    match = pattern.match(str, 0, QRegularExpression::NormalMatch,
                          QRegularExpression::AnchoredMatchOption);
    if (!match.hasMatch())
        return false;
    return match.capturedLength() == str.length();
}

std::deque<
        FileDownloader::MatchElement> FileDownloader::buildMatchList(const Data::Variant::Read &config) const
{
    std::deque<MatchElement> result;
    switch (config.getType()) {
    case Variant::Type::String: {
        MatchElement me(buildMatch(config.toQString()));
        if (!me.pattern.isValid() || me.pattern.pattern().isEmpty())
            break;
        result.emplace_back(std::move(me));
        break;
    }
    case Variant::Type::Hash:
        result.emplace_back(buildMatch(config));
        break;
    default: {
        for (auto v : config.toArray()) {
            MatchElement me(buildMatch(v));
            if (!me.pattern.isValid() || me.pattern.pattern().isEmpty())
                continue;
            result.emplace_back(std::move(me));
        }
        break;
    }
    }
    return result;
}

Util::ByteArray FileDownloader::getTimeContext(const Data::Variant::Read &config,
                                               const Util::ByteArray &defaultContext)
{
    if (config["TimeContext"].exists()) {
        return Util::ByteArray(applySubstitutions(config["TimeContext"].toQString()).toUtf8());
    }
    return defaultContext;
}

bool FileDownloader::filterFiles(std::vector<std::unique_ptr<DownloadFile>> &files,
                                 const Data::Variant::Read &config,
                                 const Util::ByteArray &timeContext,
                                 const std::deque<MatchElement> &matched)
{
    TimeState &state = this->timeState[timeContext];
    state.referenced = true;

    double firstModifiedTime =
            Variant::Composite::offsetTimeInterval(config["MinimumAge"], lastTimeExecuted, false,
                                                   Time::Minute, 5);
    double firstInspectTime =
            Variant::Composite::offsetTimeInterval(config["MinimumAge"], currentExecution, false,
                                                   Time::Minute, 5);

    auto acceptPatterns = buildMatchList(config["Match"]);

    for (auto file = files.begin(); file != files.end();) {
        double modifiedTime = (*file)->getModifiedTime();
        bool writable = (*file)->writable();

        double modifiedAfter = FP::undefined();
        if (!writable || config["HonorModified"].toBool()) {
            modifiedAfter = firstModifiedTime;
        }
        if (!config["PureModifiedTracking"].toBool() && FP::defined(state.latestTime)) {
            if (FP::defined(modifiedAfter) && modifiedAfter > state.latestTime)
                modifiedAfter = state.latestTime;
        }

        if (!FP::defined(modifiedTime)) {
            if (!writable || FP::defined(firstInspectTime)) {
                if (config["RequireModified"].toBool()) {
                    file = files.erase(file);
                    continue;
                }
            }
        } else {
            if (FP::defined(firstInspectTime) && modifiedTime > firstInspectTime) {
                file = files.erase(file);
                continue;
            }

            if (!FP::defined(latestFileModification) || modifiedTime > latestFileModification)
                latestFileModification = modifiedTime;

            if (FP::defined(modifiedAfter) && modifiedTime <= modifiedAfter) {
                if (modifiedTime != modifiedAfter ||
                        state.equalToLatest.count((*file)->id()) != 0) {
                    file = files.erase(file);
                    continue;
                }
            }
        }

        auto finalMatched = matched;

        if (!acceptPatterns.empty()) {
            QString fileName((*file)->fileName());
            auto matchPattern = acceptPatterns.begin();
            auto endMatchPatterns = acceptPatterns.end();
            for (; matchPattern != endMatchPatterns; ++matchPattern) {
                if (!matchPattern->apply(fileName))
                    continue;
                break;
            }
            if (matchPattern == endMatchPatterns) {
                file = files.erase(file);
                continue;
            }

            finalMatched.emplace_back(*matchPattern);
            (*file)->pattern = matchPattern->original;
        }

        (*file)->fileTime = convertTime(finalMatched, (*file)->getModifiedTime());
        state.updateNext(**file);
        ++file;
    }

    return true;
}

bool FileDownloader::listFTP(std::vector<std::unique_ptr<DownloadFile>> &output,
                             const std::shared_ptr<FTPConnection> &connection,
                             const Data::Variant::Read &config,
                             const std::string &path,
                             const std::string &rootPath,
                             const std::deque<MatchElement> &matched)
{
    if (testTerminated())
        return false;

    {
        bool ok = false;
        auto files = connection->list(path, false, &ok);
        if (!ok) {
            qCDebug(log_transfer_download) << "FTP file list failed:" << connection->errorString();
            return false;
        }
        std::vector<std::unique_ptr<DownloadFile>> add;
        for (auto &file : files) {
            add.emplace_back(new DownloadFTPFile(std::move(file), connection));
        }

        auto defaultContext = Util::ByteArray::filled(0);
        defaultContext.push_back(2);
        defaultContext += connection->getHost();
        defaultContext.push_back(0);
        defaultContext += connection->getUser();
        defaultContext.push_back(0);
        defaultContext += path;

        pushFilterRemote(path, rootPath);
        Util::ByteArray timeContext = getTimeContext(config, defaultContext);
        popFilter();

        if (!filterFiles(add, config, timeContext, matched))
            return false;

        std::move(std::make_move_iterator(add.begin()), std::make_move_iterator(add.end()),
                  Util::back_emplacer(output));
    }

    if (config["Recursive"].toBool()) {
        bool ok = false;
        auto dirs = connection->list(path, true, &ok);
        if (!ok) {
            qCDebug(log_transfer_download) << "FTP directory list failed:"
                                           << connection->errorString();
            return false;
        }
        for (const auto &file : dirs) {
            if (!listFTP(output, connection, config, file.filePath(), rootPath, matched))
                return false;
        }
    }

    return true;
}

bool FileDownloader::walkFTP(std::vector<std::unique_ptr<DownloadFile>> &output,
                             const std::shared_ptr<FTPConnection> &connection,
                             const Data::Variant::Read &config,
                             const std::string &path,
                             const std::string &rootPath,
                             std::deque<MatchElement> remaining,
                             const std::deque<MatchElement> &matched)
{
    if (testTerminated())
        return false;

    if (remaining.empty())
        return listFTP(output, connection, config, path, rootPath, matched);
    MatchElement me(remaining.front());
    remaining.pop_front();

    bool ok = false;
    auto dirs = connection->list(path, true, &ok);
    if (!ok) {
        qCDebug(log_transfer_download) << "FTP directory list failed:" << connection->errorString();
        return false;
    }
    for (const auto &file : dirs) {
        if (!me.apply(QString::fromStdString(file.fileName())))
            continue;
        auto newMatched = matched;
        newMatched.emplace_back(me);
        if (!walkFTP(output, connection, config, file.filePath(), rootPath, remaining, newMatched))
            return false;
    }
    return true;
}

bool FileDownloader::execFTP(const Variant::Read &config)
{
    auto matchPath = buildMatchList(config["MatchPath"]);
    QString path(applySubstitutions(config["Path"].toQString()));

    QString hostname(applySubstitutions(config["Hostname"].toQString()));
    if (hostname.isEmpty()) {
        qCDebug(log_transfer_download) << "Invalid FTP host name";
        return false;
    }
    qint64 port = config["Port"].toInt64();
    if (!INTEGER::defined(port) || port <= 0 || port > 65535)
        port = 21;

    QString username("anonymous");
    if (config["Username"].exists())
        username = applySubstitutions(config["Username"].toQString());
    QString password(passwordOrPasswordFile(config, "cpd3"));

    auto connection = std::make_shared<FTPConnection>(hostname.toStdString(),
                                                      static_cast<std::uint16_t>(port),
                                                      username.toStdString(),
                                                      password.toStdString());
    connection->configure(config);

    Threading::Receiver rx;
    terminateRequested.connect(rx, std::bind(&FTPConnection::signalTerminate, connection.get()));

    feedback.emitStage(QObject::tr("Connecting to %1").arg(hostname),
                       QObject::tr("The system is initating a FTP connection to %1.").arg(hostname),
                       false);

    if (!connection->connectToServer()) {
        qCDebug(log_transfer_download) << "Failed to connect to FTP server:"
                                       << connection->errorString();
        feedback.emitFailure();
        return false;
    }
    if (testTerminated())
        return false;

    feedback.emitStage(QObject::tr("List files"), QObject::tr(
            "The system is inspecting local directories, looking for new files."), false);

    std::vector<std::unique_ptr<DownloadFile>> files;
    if (!walkFTP(files, connection, config, path.toStdString(), path.toStdString(), matchPath)) {
        feedback.emitFailure();
        return false;
    }

    for (auto &file : files) {
        if (testTerminated())
            return false;

        feedback.emitStage(QObject::tr("Download %1").arg(file->fileName()),
                           QObject::tr("The system is downloading the file %1.").arg(
                                   file->description()), true);
        file->feedback.forward(feedback);

        if (!file->acquire()) {
            feedback.emitFailure(QString::fromStdString(connection->errorString()));
            connection->disconnectFromServer();
            return false;
        }
    }

    connection->disconnectFromServer();

    std::move(std::make_move_iterator(files.begin()), std::make_move_iterator(files.end()),
              Util::back_emplacer(downloadedFiles));
    return true;
}

bool FileDownloader::listLocal(std::vector<std::unique_ptr<DownloadFile>> &output,
                               const Data::Variant::Read &config,
                               const QDir &dir,
                               const QDir &rootDir,
                               const std::deque<MatchElement> &matched)
{
    if (testTerminated())
        return false;
    bool writable = true;
    if (config["Writable"].exists()) {
        writable = config["Writable"].toBool();
    } else {
        QDir test(dir);
        QString last(dir.dirName());
        if (test.cdUp()) {
            writable = !test.entryList(QStringList(last),
                                       QDir::Dirs | QDir::Writable | QDir::NoDotAndDotDot)
                            .isEmpty();
        }
    }

    QDir::Filters filters = QDir::NoDotAndDotDot;
    if (!config["IgnoreReadableFlag"].toBoolean())
        filters |= QDir::Readable;

    if (findLocalDirectories) {
        output.emplace_back(new DownloadLocalFile(dir.path(), writable));
    } else {
        std::vector<std::unique_ptr<DownloadFile>> add;
        for (const auto &file : dir.entryInfoList(QDir::Files | filters)) {
            add.emplace_back(new DownloadLocalFile(file, writable));
        }

        auto defaultContext = Util::ByteArray::filled(0);
        defaultContext.push_back(1);
        defaultContext += rootDir.canonicalPath().toUtf8();

        pushFilterDirectory(dir, rootDir);
        Util::ByteArray timeContext = getTimeContext(config, defaultContext);
        popFilter();

        if (!filterFiles(add, config, timeContext, matched))
            return false;

        std::move(std::make_move_iterator(add.begin()), std::make_move_iterator(add.end()),
                  Util::back_emplacer(output));
    }

    if (config["Recursive"].toBool()) {
        for (const auto &file : dir.entryInfoList(QDir::Dirs | filters)) {
            QDir subdir(dir);
            if (!subdir.cd(file.fileName())) {
                qCDebug(log_transfer_download) << "Unable to get sub directory"
                                               << file.absoluteFilePath();
                continue;
            }
            if (!listLocal(output, config, subdir, rootDir, matched))
                return false;
        }
    }

    return true;
}

bool FileDownloader::walkLocal(std::vector<std::unique_ptr<DownloadFile>> &output,
                               const Data::Variant::Read &config,
                               const QDir &dir,
                               const QDir &rootDir,
                               std::deque<MatchElement> remaining,
                               const std::deque<MatchElement> &matched)
{
    if (testTerminated())
        return false;

    if (remaining.empty())
        return listLocal(output, config, dir, rootDir, matched);
    MatchElement me(remaining.front());
    remaining.pop_front();

    QDir::Filters filters = QDir::Dirs | QDir::NoDotAndDotDot;
    if (!config["IgnoreReadableFlag"].toBoolean())
        filters |= QDir::Readable;
    for (const auto &file : dir.entryInfoList(filters)) {
        if (!me.apply(file.fileName()))
            continue;
        QDir subdir = dir;
        if (!subdir.cd(file.fileName())) {
            qCDebug(log_transfer_download) << "Unable to get sub-directory"
                                           << file.canonicalFilePath();
            continue;
        }
        auto newMatched = matched;
        newMatched.emplace_back(me);
        if (!walkLocal(output, config, subdir, rootDir, remaining, newMatched))
            return false;
    }
    return true;
}

bool FileDownloader::execLocal(const Variant::Read &config)
{
    auto matchPath = buildMatchList(config["MatchPath"]);
    QString path(applySubstitutions(config["Path"].toQString()));

    feedback.emitStage(QObject::tr("List files"), QObject::tr(
            "The system is inspecting local directories, looking for new files."), false);

    std::vector<std::unique_ptr<DownloadFile>> files;
    if (!walkLocal(files, config, QDir(path), QDir(path), matchPath)) {
        feedback.emitFailure(QObject::tr("Directory walk failed"));
        return false;
    }

    if (!findLocalDirectories) {
        for (auto &file : files) {
            if (!file->acquire()) {
                feedback.emitFailure(QObject::tr("Failed to open %1").arg(file->fileName()));
                return false;
            }
        }
    }

    std::move(std::make_move_iterator(files.begin()), std::make_move_iterator(files.end()),
              Util::back_emplacer(downloadedFiles));
    return true;
}

bool FileDownloader::exec(const Variant::Read &config)
{
    if (testTerminated())
        return false;

    switch (config.getType()) {
    case Variant::Type::Matrix:
    case Variant::Type::Array: {
        for (auto add : config.toArray()) {
            if (!exec(add))
                return false;
        }
        return true;
    }
    case Variant::Type::String: {
        Variant::Write add;
        add.detachFromRoot();
        add.hash("Path").setString(config.toString());
        return exec(add);
    }
    default:
        return true;
    case Variant::Type::Hash:
        break;
    }

    const auto &type = config["Type"].toString();
    if (Util::equal_insensitive(type, "single")) {
        if (findLocalDirectories)
            return true;
        return execSingle(config);
    } else if (Util::equal_insensitive(type, "list")) {
        if (findLocalDirectories)
            return true;
        return execList(config);
    } else if (Util::equal_insensitive(type, "ftp")) {
        if (findLocalDirectories)
            return true;
        return execFTP(config);
    }

    return execLocal(config);
}


}
}
