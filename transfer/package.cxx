/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QLoggingCategory>
#include <QCryptographicHash>
#include <QSslCertificate>
#include <QSslKey>

#include "package.hxx"
#include "io/drivers/file.hxx"
#include "core/compress_lz4.hxx"
#include "algorithms/cryptography.hxx"


Q_LOGGING_CATEGORY(log_transfer_package, "cpd3.transfer.package", QtWarningMsg)

using namespace CPD3::Algorithms;

static constexpr quint32 fileMagicStart = 0xC4D34A76;
static constexpr quint32 maximumCertificateSize = 2 * 1024 * 1024;

namespace {
enum class StageType : std::uint8_t {
    /* Innermost designator, this is used to terminate processing */
    Inner = 0,

    Checksum, Compression, Encrypt, Sign,
};

enum class ChecksumType : std::uint8_t {
    SHA512 = 0,
};

enum class CompressionType : std::uint8_t {
    LZ4 = 0,
};

enum class EncryptType : std::uint8_t {
    Public = 0, Private
};

enum class SignType : std::uint8_t {
    CertificateIncluded = 0, CertificateImplied,
};

}

namespace CPD3 {
namespace Transfer {

class TransferPack::Stage {
    TransferPack &parent;
public:
    explicit Stage(TransferPack &parent) : parent(parent),
                                           terminateRequested(parent.terminateRequested)
    { }

    virtual ~Stage() = default;

    class Seekable;

    class Stream;

    class Pipeline;

protected:
    Threading::Signal<> terminateRequested;

    bool isTerminated() const
    {
        std::lock_guard<std::mutex> lock(parent.mutex);
        return parent.terminated;
    }
};

class TransferPack::Stage::Seekable : public TransferPack::Stage {
public:
    explicit Seekable(TransferPack &parent) : Stage(parent)
    { }

    virtual ~Seekable() = default;

    virtual bool execute(std::unique_ptr<IO::Generic::Block> &&input,
                         std::unique_ptr<IO::Generic::Stream> &&output) = 0;

    bool copyData(IO::Generic::Block &input, IO::Generic::Writable &output)
    {
        static constexpr std::size_t chunkSize = 65536;
        while (!input.readFinished()) {
            if (isTerminated())
                return false;
            Util::ByteArray buffer;
            input.read(buffer, chunkSize);
            if (buffer.empty())
                break;
            output.write(std::move(buffer));
        }
        return true;
    }

    class IOOutput;
};

class TransferPack::Stage::Stream : public TransferPack::Stage {
public:
    explicit Stream(TransferPack &parent) : Stage(parent)
    { }

    virtual ~Stream() = default;

    virtual bool execute(std::unique_ptr<IO::Generic::Stream> &&input,
                         std::unique_ptr<IO::Generic::Block> &&output) = 0;
};

class TransferPack::Stage::Pipeline : public TransferPack::Stage {
public:
    explicit Pipeline(TransferPack &parent) : Stage(parent)
    { }

    virtual ~Pipeline() = default;

    virtual bool start(std::unique_ptr<IO::Generic::Stream> &&input,
                       std::unique_ptr<IO::Generic::Stream> &&output) = 0;

    virtual bool wait() = 0;

    class AddHeader;
};

class TransferPack::Stage::Pipeline::AddHeader : public TransferPack::Stage::Pipeline {
    std::mutex mutex;
    std::condition_variable notify;
    bool inputEnded;

    std::unique_ptr<IO::Generic::Stream> output;
    std::unique_ptr<IO::Generic::Stream> input;
public:
    explicit AddHeader(TransferPack &parent) : Pipeline(parent), inputEnded(false)
    { }

    virtual ~AddHeader()
    {
        if (input) {
            input->read.disconnect();
            input->ended.disconnect();
        }
        if (output) {
            output->writeStall.disconnect();
        }
    }

    bool start(std::unique_ptr<IO::Generic::Stream> &&in,
               std::unique_ptr<IO::Generic::Stream> &&out) override
    {
        input = std::move(in);
        output = std::move(out);

        input->read.connect([this](const Util::ByteArray &data) {
            if (!output)
                return;
            output->write(data);
        });
        input->ended.connect([this] {
            if (output) {
                /* This assumes the input caller doesn't have a lock held while calling this,
                 * since the disconnect call could block if a write stall set is in progress.
                 * However, that should be a safe assumption, since anything handling a
                 * read stall request shouldn't make assumptions about the context it's called
                 * from. */
                output->writeStall.disconnect();
            }
            output.reset();
            {
                std::lock_guard<std::mutex> lock(mutex);
                inputEnded = true;
            }
            notify.notify_all();
        });
        output->writeStall.connect([this](bool isStalled) {
            input->readStall(isStalled);
        });

        input->start();
        return true;
    }

    bool wait() override
    {
        std::unique_lock<std::mutex> lock(mutex);
        notify.wait(lock, [this] { return inputEnded; });
        return true;
    }
};

void TransferPack::checksumStage()
{
    class ChecksumStage : public Stage::Stream {
    public:
        explicit ChecksumStage(TransferPack &parent) : Stream(parent)
        { }

        virtual ~ChecksumStage() = default;

        bool execute(std::unique_ptr<IO::Generic::Stream> &&input,
                     std::unique_ptr<IO::Generic::Block> &&output) override
        {
            QCryptographicHash hash(QCryptographicHash::Sha512);

            static constexpr std::size_t checksumSize = 64;

            std::size_t headerPosition;
            {
                Util::ByteArray header;
                header.push_back(static_cast<std::uint8_t>(StageType::Checksum));
                header.push_back(static_cast<std::uint8_t>(ChecksumType::SHA512));
                headerPosition = header.size();
                header.resize(header.size() + checksumSize);

                headerPosition += output->tell();
                output->write(std::move(header));
            }

            std::mutex mutex;
            std::condition_variable cv;
            bool ended = false;
            Threading::Receiver rx;

            input->read.connect(rx, [&](const Util::ByteArray &data) {
                hash.addData(data.toQByteArrayRef());
                output->write(data);
            });
            input->ended.connect(rx, [&]() {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    ended = true;
                }
                cv.notify_all();
            });
            terminateRequested.connect(rx, [&]() {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    ended = true;
                }
                cv.notify_all();
            });

            if (isTerminated())
                return false;

            input->start();
            {
                std::unique_lock<std::mutex> lock(mutex);
                cv.wait(lock, [&] { return ended; });
            }

            rx.disconnect();

            output->seek(headerPosition);
            {
                auto contents = hash.result();
                contents.resize(checksumSize);
                output->write(std::move(contents));
            }
            return true;
        }
    };
    stages.emplace_back(new ChecksumStage(*this));
}

void TransferPack::compressStage()
{
    class CompressStage : public Stage::Pipeline {
        std::mutex mutex;
        std::condition_variable notify;
        bool ended;
        bool outputStalled;
        Threading::PoolRun pool;
        std::thread outputThread;

        Util::ByteArray buffer;

        std::unique_ptr<IO::Generic::Stream> input;
        std::unique_ptr<IO::Generic::Stream> output;

        struct Result {
            bool ready;
            Util::ByteArray contents;

            Result() : ready(false)
            { }
        };

        std::deque<std::unique_ptr<Result>> results;

        Threading::Receiver rx;

        void processCompression(const Util::ByteArray &in, Result *out)
        {
            if (!in.empty()) {
                CompressorLZ4HC compressor;
                compressor.compress(in, out->contents);
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                out->ready = true;
            }
            notify.notify_all();
        }

        std::vector<std::unique_ptr<Result>> waitForOutput(bool &wasStalled)
        {
            std::unique_lock<std::mutex> lock(mutex);
            for (;;) {
                bool isStalled = results.size() > 8 || outputStalled;
                if (isStalled != wasStalled) {
                    wasStalled = isStalled;
                    lock.unlock();
                    input->readStall(isStalled);
                    lock.lock();
                    continue;
                }

                std::vector<std::unique_ptr<Result>> resultsReady;
                while (!results.empty() && results.front()->ready) {
                    resultsReady.emplace_back(std::move(results.front()));
                    results.pop_front();
                }
                if (!resultsReady.empty())
                    return std::move(resultsReady);
                if (ended && results.empty())
                    return {};
                notify.wait(lock);
            }
        }

        void flushResults()
        {
            bool wasStalled = false;
            for (;;) {
                auto resultsReady = waitForOutput(wasStalled);
                if (resultsReady.empty())
                    break;

                Q_ASSERT(output.get() != nullptr);
                for (auto &r : resultsReady) {
                    if (r->contents.empty())
                        continue;
                    {
                        Util::ByteArray header;
                        header.appendNumber<quint32>(r->contents.size());
                        output->write(std::move(header));
                    }
                    output->write(std::move(r->contents));
                }
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                ended = true;
            }
            notify.notify_all();

            output.reset();
            input.reset();
        }

        void flushBuffer(bool final = false)
        {
            static constexpr std::size_t minimumChunk = 1 * 1024 * 1024;
            static constexpr std::size_t maximumChunk = 64 * 1024 * 1024;
            while (final || buffer.size() >= minimumChunk) {
                if (buffer.empty())
                    break;

                results.emplace_back(new Result);
                if (buffer.size() < maximumChunk) {
                    pool.run(std::bind(&CompressStage::processCompression, this, buffer,
                                       results.back().get()));
                    buffer.clear();
                } else {
                    Util::ByteArray chunk(buffer.mid(0, maximumChunk));
                    buffer.pop_front(maximumChunk);
                    pool.run(std::bind(&CompressStage::processCompression, this, chunk,
                                       results.back().get()));
                }
            }
        }

    public:
        explicit CompressStage(TransferPack &parent) : Pipeline(parent),
                                                       ended(false),
                                                       outputStalled(false)
        { }

        virtual ~CompressStage()
        {
            rx.disconnect();
            {
                std::lock_guard<std::mutex> lock(mutex);
                ended = true;
            }
            notify.notify_all();
            pool.wait();
            if (outputThread.joinable())
                outputThread.join();
            input.reset();
            output.reset();
        }

        bool start(std::unique_ptr<IO::Generic::Stream> &&in,
                   std::unique_ptr<IO::Generic::Stream> &&out) override
        {
            input = std::move(in);
            output = std::move(out);

            terminateRequested.connect(rx, [this]() {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    ended = true;
                }
                notify.notify_all();
            });
            if (isTerminated())
                return false;

            {
                Util::ByteArray header;
                header.push_back(static_cast<std::uint8_t>(StageType::Compression));
                header.push_back(static_cast<std::uint8_t>(CompressionType::LZ4));
                output->write(std::move(header));
            }

            input->read.connect(rx, [this](const Util::ByteArray &data) {
                std::lock_guard<std::mutex> lock(mutex);
                if (ended)
                    return;
                buffer += data;
                flushBuffer();
            });
            input->ended.connect(rx, [this]() {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    if (ended)
                        return;
                    flushBuffer(true);
                    ended = true;
                }
                notify.notify_all();
            });
            output->writeStall.connect(rx, [this](bool enable) {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    outputStalled = enable;
                }
                notify.notify_all();
            });

            input->start();

            outputThread = std::thread(std::bind(&CompressStage::flushResults, this));
            return true;
        }

        bool wait() override
        {
            {
                std::unique_lock<std::mutex> lock(mutex);
                notify.wait(lock, [this] { return ended; });
            }
            pool.wait();
            if (outputThread.joinable())
                outputThread.join();
            return true;
        }
    };
    stages.emplace_back(new CompressStage(*this));
}

void TransferPack::invokeStage(const Data::Variant::Read &configuration)
{
    IO::Process::Spawn spawn;

    if (!configuration["Arguments"].exists()) {
        spawn = IO::Process::Spawn::shell(applySubstitutions(configuration["Program"].toQString()));
    } else {
        spawn.command = applySubstitutions(configuration["Program"].toQString()).toStdString();
        for (auto add : configuration["Arguments"].toArray()) {
            spawn.arguments.emplace_back(applySubstitutions(add.toQString()).toStdString());
        }
    }

    class InvokeStage : public Stage::Pipeline {
        IO::Process::Spawn spawn;
        std::shared_ptr<IO::Process::Instance> process;
        std::unique_ptr<IO::Generic::Stream> input;
        std::unique_ptr<IO::Process::Stream> toProcess;
        std::unique_ptr<IO::Generic::Stream> output;
        std::unique_ptr<IO::Process::Stream> fromProcess;
        Threading::Receiver rx;
    public:
        InvokeStage(TransferPack &parent, IO::Process::Spawn spawn) : Pipeline(parent),
                                                                      spawn(std::move(spawn))
        {
            spawn.capture();
            spawn.outputStream = IO::Process::Spawn::StreamMode::Forward;
        }

        virtual ~InvokeStage()
        {
            rx.disconnect();
            if (process) {
                process->terminate();
            }
        }

        bool start(std::unique_ptr<IO::Generic::Stream> &&in,
                   std::unique_ptr<IO::Generic::Stream> &&out) override
        {
            process = spawn.create();
            if (!process)
                return false;

            input = std::move(in);
            output = std::move(out);
            toProcess = process->inputStream();
            fromProcess = process->outputStream();

            input->read.connect(rx, [this](const Util::ByteArray &data) {
                toProcess->write(data);
            });
            input->ended.connect(rx, [this]() {
                toProcess->close();
                toProcess.reset();
            });
            toProcess->writeStall.connect(rx, [this](bool stalled) {
                input->readStall(stalled);
            });

            fromProcess->read.connect(rx, [this](const Util::ByteArray &data) {
                output->write(data);
            });
            fromProcess->ended.connect(rx, [this]() {
                output.reset();
            });
            output->writeStall.connect(rx, [this](bool stalled) {
                fromProcess->readStall(stalled);
            });
            fromProcess->start();

            if (!process->start())
                return false;

            input->start();

            terminateRequested.connect(rx, [this] {
                process->terminate();
            });
            if (isTerminated())
                return false;

            return true;
        }

        bool wait() override
        {
            process->wait();
            return true;
        }
    };

    stages.emplace_back(new InvokeStage(*this, std::move(spawn)));
}

void TransferPack::encryptStage(const Data::Variant::Read &from,
                                bool usePrivate,
                                bool loadExternal,
                                const std::string &password)
{
    class EncryptStage : public Stage::Pipeline {
        Data::Variant::Root from;
        bool usePrivate;
        bool loadExternal;
        std::string password;

        std::unique_ptr<IO::Generic::Stream> input;
        std::unique_ptr<IO::Generic::Stream> output;
        std::thread thread;
        bool isOk;
    public:
        EncryptStage(TransferPack &parent,
                     const Data::Variant::Read &from,
                     bool usePrivate,
                     bool loadExternal,
                     const std::string &password) : Pipeline(parent),
                                                    from(from),
                                                    usePrivate(usePrivate),
                                                    loadExternal(loadExternal),
                                                    password(password),
                                                    isOk(false)
        { }

        virtual ~EncryptStage()
        {
            if (thread.joinable())
                thread.join();
        }

        bool start(std::unique_ptr<IO::Generic::Stream> &&in,
                   std::unique_ptr<IO::Generic::Stream> &&out) override
        {
            input = std::move(in);
            output = std::move(out);

            static constexpr std::size_t fingerprintSize = 64;

            auto cert = Cryptography::getCertificate(from, loadExternal);
            bool encryptWithPrivate = true;

            Util::ByteArray header;
            header.push_back(static_cast<std::uint8_t>(StageType::Encrypt));
            header.push_back(0);

            if (cert.isNull()) {
                header.resize(header.size() + fingerprintSize, 0);
            } else {
                auto fingerprint = Cryptography::sha512(cert);
                if (fingerprint.size() != 64) {
                    qCDebug(log_transfer_package)
                        << "Encryption certificate SHA512 calculation error";
                    return false;
                }
                header += std::move(fingerprint);
                encryptWithPrivate = usePrivate;
            }

            if (usePrivate) {
                header[1] = static_cast<std::uint8_t>(EncryptType::Private);
            } else {
                header[1] = static_cast<std::uint8_t>(EncryptType::Public);
            }
            output->write(std::move(header));

            thread = std::thread([this, encryptWithPrivate] {
                isOk = Cryptography::encrypt(from, std::move(input), *output, encryptWithPrivate,
                                             loadExternal, password);
                output.reset();
            });
            return true;
        }

        bool wait() override
        {
            if (thread.joinable())
                thread.join();
            return isOk;
        }

    };

    checksumStage();
    stages.emplace_back(new EncryptStage(*this, from, usePrivate, loadExternal, password));
}

void TransferPack::signStage(const std::vector<Data::Variant::Read> &from,
                             bool includeCertificate,
                             bool loadExternal,
                             const std::string &password)
{
    class IdentifierStage : public Stage::Pipeline::AddHeader {
        std::vector<Data::Variant::Root> from;
        bool includeCertificate;
        bool loadExternal;
        std::string password;
    public:
        IdentifierStage(TransferPack &parent,
                        const std::vector<Data::Variant::Read> &from,
                        bool includeCertificate,
                        bool loadExternal,
                        const std::string &password) : AddHeader(parent),
                                                       includeCertificate(includeCertificate),
                                                       loadExternal(loadExternal),
                                                       password(password)
        {
            for (const auto &f : from) {
                this->from.emplace_back(f);
            }
        }

        virtual ~IdentifierStage() = default;

        bool start(std::unique_ptr<IO::Generic::Stream> &&input,
                   std::unique_ptr<IO::Generic::Stream> &&output) override
        {
            if (from.empty()) {
                qCDebug(log_transfer_package) << "No certificates specified";
                return false;
            }
            if (from.size() > 65536) {
                qCDebug(log_transfer_package) << "Too many signing certificates";
                return false;
            }

            for (const auto &add : from) {
                auto cert = Cryptography::getCertificate(add, loadExternal);
                Util::ByteArray header;
                if (cert.isNull()) {
                    if (includeCertificate) {
                        header.appendNumber<quint32>(0);
                    } else {
                        header.resize(64, 0);
                    }
                } else {
                    if (includeCertificate) {
                        auto data = cert.toDer();
                        quint32 size = data.size();
                        if (size == 0) {
                            qCDebug(log_transfer_package) << "Zero certificate data";
                            return false;
                        }
                        if (size > maximumCertificateSize) {
                            qCDebug(log_transfer_package) << "Certificate too large";
                            return false;
                        }
                        header.appendNumber<quint32>(size);
                        header += std::move(data);
                    } else {
                        auto certHash = Cryptography::sha512(cert);
                        if (certHash.size() != 64) {
                            qCDebug(log_transfer_package)
                                << "Signature certificate SHA512 calculation error";
                            return false;
                        }
                        header += std::move(certHash);
                    }
                }
                output->write(std::move(header));
            }

            return AddHeader::start(std::move(input), std::move(output));
        }
    };

    stages.emplace_back(
            new IdentifierStage(*this, from, includeCertificate, loadExternal, password));

    class SignStage : public Stage::Seekable {
        std::vector<Data::Variant::Root> from;
        bool includeCertificate;
        bool loadExternal;
        std::string password;

        Util::ByteArray getHash(IO::Generic::Block &input)
        {
            static constexpr std::size_t chunkSize = 65536;
            QByteArray buffer;
            QCryptographicHash hash(QCryptographicHash::Sha512);
            while (!input.readFinished()) {
                if (isTerminated())
                    return {};
                buffer.clear();
                input.read(buffer, chunkSize);
                hash.addData(buffer);
            }
            return Util::ByteArray(hash.result());
        }

    public:
        SignStage(TransferPack &parent,
                  const std::vector<Data::Variant::Read> &from,
                  bool includeCertificate,
                  bool loadExternal,
                  const std::string &password) : Seekable(parent),
                                                 includeCertificate(includeCertificate),
                                                 loadExternal(loadExternal),
                                                 password(password)
        {
            for (const auto &f : from) {
                this->from.emplace_back(f);
            }
        }

        virtual ~SignStage() = default;

        bool execute(std::unique_ptr<IO::Generic::Block> &&input,
                     std::unique_ptr<IO::Generic::Stream> &&output) override
        {
            if (from.empty()) {
                qCDebug(log_transfer_package) << "No certificates specified";
                return false;
            }
            if (from.size() > 65536U) {
                qCDebug(log_transfer_package) << "Too many signing certificates";
                return false;
            }

            {
                Util::ByteArray header;
                header.push_back(static_cast<std::uint8_t>(StageType::Sign));
                if (includeCertificate)
                    header.push_back(static_cast<std::uint8_t>(SignType::CertificateIncluded));
                else
                    header.push_back(static_cast<std::uint8_t>(SignType::CertificateImplied));
                header.appendNumber<quint16>(from.size() - 1);
                output->write(std::move(header));
            }

            input->seek(0);

            auto hash = getHash(*input);
            if (hash.empty())
                return false;

            for (const auto &add : from) {
                auto signature = Cryptography::signHash(add, hash, loadExternal, password);
                if (signature.empty()) {
                    qCDebug(log_transfer_package) << "Error signing data";
                    return false;
                }

                auto size = signature.size();
                if (size == 0 || size > 65536) {
                    qCDebug(log_transfer_package) << "Signature too large";
                    return false;
                }
                --size;

                Util::ByteArray header;
                header.appendNumber<quint16>(size);
                header += std::move(signature);
                output->write(std::move(header));
            }

            input->seek(0);
            return copyData(*input, *output);
        }

    };

    stages.emplace_back(new SignStage(*this, from, includeCertificate, loadExternal, password));
}

void TransferPack::signStage(const Data::Variant::Read &from,
                             bool includeCertificate,
                             bool loadExternal,
                             const std::string &password)
{
    return signStage(std::vector<Data::Variant::Read>{from}, includeCertificate, loadExternal,
                     password);
}

bool TransferPack::exec(const IO::Access::Handle &input, const IO::Access::Handle &output)
{
    if (stages.empty()) {
        qCWarning(log_transfer_package) << "Unable to reuse packer";
        return false;
    }

    struct State {
        std::shared_ptr<IO::File::Backing> temporaryInput;
        std::shared_ptr<IO::File::Backing> temporaryOutput;
        std::vector<std::unique_ptr<Stage::Pipeline>> pipeline;

        std::unique_ptr<IO::Generic::Stream> pipelineInputStream;
        std::unique_ptr<IO::Generic::Stream> pipelineOutputStream;
        std::unique_ptr<IO::Generic::Block> pipelineInputBlock;

        std::unique_ptr<Stage::Seekable> stageInput;
        std::unique_ptr<IO::Generic::Block> stageInputBlock;
        std::unique_ptr<IO::Generic::Stream> stageInputResult;
        std::thread stageInputThread;
        bool stageInputStatus;

        ~State()
        {
            if (stageInputThread.joinable())
                stageInputThread.join();
        }

        bool startPipeline()
        {
            std::unique_ptr<IO::Generic::Stream>
                    pipelineStageInput = std::move(pipelineInputStream);
            pipelineInputStream.reset();

            stageInputStatus = true;
            if (stageInput) {
                stageInputThread = std::thread([this] {
                    stageInputStatus = stageInput->execute(std::move(stageInputBlock),
                                                           std::move(stageInputResult));
                    stageInput.reset();
                    stageInputBlock.reset();
                    stageInputResult.reset();
                });
            }

            for (auto stage = pipeline.begin(), endStage = pipeline.end();
                    stage != endStage;
                    ++stage) {
                Q_ASSERT(pipelineStageInput.get() != nullptr);
                if (stage + 1 == endStage) {
                    return (*stage)->start(std::move(pipelineStageInput),
                                           std::move(pipelineOutputStream));
                }

                auto pipe = IO::Access::pipe();
                if (!(*stage)->start(std::move(pipelineStageInput), std::move(pipe.first)))
                    return false;
                pipelineStageInput = std::move(pipe.second);
            }

            Q_ASSERT(pipelineStageInput.get() == nullptr);
            return true;
        }

        bool completePipeline()
        {
            if (stageInputThread.joinable())
                stageInputThread.join();
            if (!stageInputStatus)
                return false;
            for (auto &stage : pipeline) {
                if (!stage->wait()) {
                    pipeline.clear();
                    pipelineInputBlock.reset();
                    return false;
                }
                stage.reset();
            }
            pipeline.clear();
            pipelineInputBlock.reset();
            return true;
        }

        static void writeHeader(IO::Generic::Writable &target)
        {
            Util::ByteArray header;
            header.resize(4);
            qToBigEndian<quint32>(fileMagicStart, header.data<uchar *>());
            target.write(std::move(header));
        }

        static bool slurpInputStream(std::unique_ptr<IO::Generic::Stream> &&input,
                                     IO::Generic::Writable &output)
        {
            std::mutex mutex;
            std::condition_variable cv;
            bool ended = false;

            input->read.connect([&](const Util::ByteArray &data) {
                output.write(data);
            });
            input->ended.connect([&]() {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    ended = true;
                }
                cv.notify_all();
            });

            input->start();
            {
                std::unique_lock<std::mutex> lock(mutex);
                cv.wait(lock, [&] { return ended; });
            }
            input.reset();
            return true;
        }

        void attachPipelineInput(const IO::Access::Handle &input)
        {
            pipelineInputStream = input->stream();
            if (!pipelineInputStream) {
                pipelineInputBlock = input->block();
                if (!pipelineInputBlock)
                    return;
                pipelineInputStream = std::unique_ptr<IO::Generic::Stream>(
                        new IO::Generic::Block::StreamReader(*pipelineInputBlock));
            }
        }

        std::unique_ptr<IO::Generic::Block> finalToBlock(const IO::Access::Handle &output)
        {
            auto direct = output->block();
            if (direct) {
                temporaryOutput.reset();
                writeHeader(*direct);
                return std::move(direct);
            }

            temporaryOutput = IO::Access::temporaryFile();
            if (!temporaryOutput) {
                qCDebug(log_transfer_package) << "Temporary output creation failed";
                return {};
            }

            auto block = temporaryOutput->block();
            if (!block) {
                qCDebug(log_transfer_package) << "Temporary output block access failed";
                return {};
            }
            writeHeader(*block);
            return std::move(block);
        }

        std::unique_ptr<IO::Generic::Block> nextToBlock()
        {
            temporaryOutput = IO::Access::temporaryFile();
            if (!temporaryOutput) {
                qCDebug(log_transfer_package) << "Temporary output creation failed";
                return {};
            }
            return temporaryOutput->block();
        }

        std::unique_ptr<IO::Generic::Stream> finalToStream(const IO::Access::Handle &output)
        {
            auto direct = output->stream();
            if (direct) {
                temporaryOutput.reset();
                writeHeader(*direct);
                return std::move(direct);
            }

            temporaryOutput = IO::Access::temporaryFile();
            if (!temporaryOutput) {
                qCDebug(log_transfer_package) << "Temporary output creation failed";
                return {};
            }
            auto stream = temporaryOutput->stream();
            if (!stream) {
                qCDebug(log_transfer_package) << "Temporary output stream access failed";
                return {};
            }
            writeHeader(*stream);
            return std::move(stream);
        }

        bool pipelineToFutureBlock(const IO::Access::Handle &input)
        {
            if (pipeline.empty())
                return true;
            temporaryOutput = IO::Access::temporaryFile();
            if (!temporaryOutput) {
                qCDebug(log_transfer_package) << "Temporary output creation failed";
                return false;
            }
            pipelineOutputStream = temporaryOutput->stream();
            if (!pipelineOutputStream)
                return false;

            if (pipelineInputStream)
                return true;

            if (stageInput) {
                auto pipe = IO::Access::pipe();
                stageInputResult = std::move(pipe.first);
                pipelineInputStream = std::move(pipe.second);
            } else if (temporaryInput) {
                pipelineInputStream = temporaryInput->stream();
            } else {
                attachPipelineInput(input);
            }
            if (!pipelineInputStream)
                return false;

            return true;
        }

        bool finalFromPipeline(const IO::Access::Handle &input, const IO::Access::Handle &output)
        {
            temporaryOutput.reset();

            if (stageInput) {
                auto pipe = IO::Access::pipe();
                stageInputResult = std::move(pipe.first);
                pipelineInputStream = std::move(pipe.second);
            } else if (temporaryInput) {
                pipelineInputStream = temporaryInput->stream();
            } else {
                attachPipelineInput(input);
            }
            if (!pipelineInputStream)
                return false;

            pipelineOutputStream = output->stream();
            if (!pipelineOutputStream) {
                temporaryOutput = IO::Access::temporaryFile();
                if (!temporaryOutput) {
                    qCDebug(log_transfer_package) << "Final temporary output creation failed";
                    return false;
                }
                pipelineOutputStream = temporaryOutput->stream();
            }
            if (!pipelineOutputStream)
                return false;

            writeHeader(*pipelineOutputStream);
            return true;
        }

        bool finish(const IO::Access::Handle &output)
        {
            pipeline.clear();
            pipelineInputBlock.reset();
            temporaryInput.reset();
            if (!temporaryOutput)
                return true;

            auto source = temporaryOutput->stream();
            if (!source)
                return false;

            if (auto block = output->block()) {
                return slurpInputStream(std::move(source), *block);
            } else if (auto stream = output->stream()) {
                return slurpInputStream(std::move(source), *stream);
            }

            qCDebug(log_transfer_package) << "Unable to create output target";
            return false;
        }

        bool advance()
        {
            pipeline.clear();
            pipelineInputStream.reset();
            pipelineOutputStream.reset();
            pipelineInputBlock.reset();
            if (stageInputThread.joinable())
                stageInputThread.join();
            stageInput.reset();
            stageInputResult.reset();
            stageInputBlock.reset();
            temporaryInput = std::move(temporaryOutput);
            return true;
        }

        bool advance(const IO::Access::Handle &input, std::unique_ptr<Stage::Seekable> &&stage)
        {
            if (!advance())
                return false;
            if (temporaryInput) {
                stageInputBlock = temporaryInput->block();
                if (!stageInputBlock)
                    return false;
                stageInput = std::move(stage);
                return true;
            }

            stageInputBlock = input->block();
            if (!stageInputBlock) {
                auto stream = input->stream();
                if (!stream) {
                    qCDebug(log_transfer_package) << "No available stage input";
                    return false;
                }

                temporaryInput = IO::Access::temporaryFile();
                if (!temporaryInput) {
                    qCDebug(log_transfer_package) << "Temporary input creation failed";
                    return false;
                }
                stageInputBlock = temporaryInput->block();
                if (!stageInputBlock)
                    return false;

                if (!slurpInputStream(std::move(stream), *stageInputBlock))
                    return false;
                stageInputBlock->seek(0);
            }

            stageInput = std::move(stage);
            return true;
        }

        std::unique_ptr<IO::Generic::Stream> resultToStream(const IO::Access::Handle &input)
        {
            if (stageInput) {
                auto pipe = IO::Access::pipe();
                stageInputResult = std::move(pipe.first);
                if (pipeline.empty())
                    return std::move(pipe.second);

                pipelineInputStream = std::move(pipe.second);

                pipe = IO::Access::pipe();
                pipelineOutputStream = std::move(pipe.first);
                return std::move(pipe.second);
            } else if (temporaryInput) {
                if (pipeline.empty())
                    return temporaryInput->stream();

                pipelineInputStream = temporaryInput->stream();
                if (!pipelineInputStream)
                    return {};

                auto pipe = IO::Access::pipe();
                pipelineOutputStream = std::move(pipe.first);
                return std::move(pipe.second);
            }

            auto stream = input->stream();
            if (!stream) {
                pipelineInputBlock = input->block();
                if (!pipelineInputBlock) {
                    qCDebug(log_transfer_package) << "No stream input possible";
                    return {};
                }
                stream = std::unique_ptr<IO::Generic::Stream>(
                        new IO::Generic::Block::StreamReader(*pipelineInputBlock));
            }
            if (pipeline.empty())
                return std::move(stream);

            pipelineInputStream = std::move(stream);

            auto pipe = IO::Access::pipe();
            pipelineOutputStream = std::move(pipe.first);
            return std::move(pipe.second);
        }

        std::unique_ptr<IO::Generic::Block> resultToBlock(const IO::Access::Handle &input)
        {
            if (stageInput) {
                if (pipeline.empty()) {
                    temporaryOutput = IO::Access::temporaryFile();
                    if (!temporaryOutput) {
                        qCDebug(log_transfer_package) << "Result temporary output creation failed";
                        return {};
                    }

                    stageInputResult = temporaryOutput->stream();
                    if (!stageInputResult)
                        return {};
                    return temporaryOutput->block();
                }

                auto pipe = IO::Access::pipe();
                stageInputResult = std::move(pipe.first);
                pipelineInputStream = std::move(pipe.second);
            } else if (temporaryInput) {
                if (pipeline.empty())
                    return temporaryInput->block();

                pipelineInputStream = temporaryInput->stream();
            } else {
                if (pipeline.empty()) {
                    auto block = input->block();
                    if (block)
                        return std::move(block);
                    auto stream = input->stream();
                    if (!stream)
                        return {};
                    temporaryInput = IO::Access::temporaryFile();
                    block = temporaryInput->block();
                    if (!block)
                        return {};
                    if (!slurpInputStream(std::move(stream), *block))
                        return {};
                    block->seek(0);
                    return std::move(block);
                }

                attachPipelineInput(input);
            }
            if (!pipelineInputStream)
                return {};

            temporaryOutput = IO::Access::temporaryFile();
            if (!temporaryOutput) {
                qCDebug(log_transfer_package) << "Result temporary output creation failed";
                return {};
            }
            pipelineOutputStream = temporaryOutput->stream();
            if (!pipelineOutputStream)
                return {};

            return temporaryOutput->block();
        }
    };

    State state;
    for (;;) {
        auto baseStage = std::move(stages.front());
        stages.erase(stages.begin());

        if (dynamic_cast<Stage::Pipeline *>(baseStage.get())) {
            state.pipeline.emplace_back(static_cast<Stage::Pipeline *>(baseStage.release()));
            if (stages.empty()) {
                if (!state.finalFromPipeline(input, output))
                    return false;
                if (!state.startPipeline())
                    return false;
                if (!state.completePipeline())
                    return false;
                return state.finish(output);
            }
        } else if (auto stage = dynamic_cast<Stage::Stream *>(baseStage.get())) {
            if (stages.empty()) {
                {
                    auto outputBlock = state.finalToBlock(output);
                    if (!outputBlock)
                        return false;
                    auto stageInputStream = state.resultToStream(input);
                    if (!stageInputStream)
                        return false;

                    if (!state.startPipeline())
                        return false;
                    if (!stage->execute(std::move(stageInputStream), std::move(outputBlock)))
                        return false;
                }
                baseStage.reset();
                if (!state.completePipeline())
                    return false;
                return state.finish(output);
            }

            {
                auto outputBlock = state.nextToBlock();
                if (!outputBlock)
                    return false;
                auto stageInputStream = state.resultToStream(input);
                if (!stageInputStream)
                    return false;
                if (!state.startPipeline())
                    return false;
                if (!stage->execute(std::move(stageInputStream), std::move(outputBlock)))
                    return false;
            }
            baseStage.reset();
            if (!state.completePipeline())
                return false;
            if (!state.advance())
                return false;
        } else if (auto stage = dynamic_cast<Stage::Seekable *>(baseStage.get())) {
            if (stages.empty()) {
                auto stageInputBlock = state.resultToBlock(input);
                if (!stageInputBlock)
                    return false;
                if (!state.startPipeline())
                    return false;
                if (!state.completePipeline())
                    return false;

                auto outputStream = state.finalToStream(output);
                if (!outputStream)
                    return false;
                if (!stage->execute(std::move(stageInputBlock), std::move(outputStream)))
                    return false;
                stageInputBlock.reset();
                outputStream.reset();
                baseStage.reset();
                return state.finish(output);
            }

            if (!state.pipelineToFutureBlock(input))
                return false;
            if (!state.startPipeline())
                return false;
            if (!state.completePipeline())
                return false;

            if (!state.advance(input, std::unique_ptr<Stage::Seekable>(
                    static_cast<Stage::Seekable *>(baseStage.release()))))
                return false;
        } else {
            Q_ASSERT(false);
            return false;
        }
    }
}

void TransferPack::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    terminateRequested();
}

QString TransferPack::applySubstitutions(const QString &input) const
{ return input; }

TransferPack::TransferPack() : terminated(false)
{
    class FinalStage : public Stage::Pipeline::AddHeader {
    public:
        explicit FinalStage(TransferPack &parent) : AddHeader(parent)
        { }

        virtual ~FinalStage() = default;

        bool start(std::unique_ptr<IO::Generic::Stream> &&input,
                   std::unique_ptr<IO::Generic::Stream> &&output) override
        {
            {
                Util::ByteArray header;
                header.push_back(static_cast<std::uint8_t>(StageType::Inner));
                output->write(std::move(header));
            }
            return AddHeader::start(std::move(input), std::move(output));
        }
    };
    stages.emplace_back(new FinalStage(*this));
}

TransferPack::~TransferPack() = default;


TransferUnpack::TransferUnpack() : terminated(false)
{ }

TransferUnpack::~TransferUnpack() = default;

void TransferUnpack::invokeAt(const Data::Variant::Read &configuration, int depth)
{
    IO::Process::Spawn spawn;

    if (!configuration["Arguments"].exists()) {
        spawn = IO::Process::Spawn::shell(applySubstitutions(configuration["Program"].toQString()));
    } else {
        spawn.command = applySubstitutions(configuration["Program"].toQString()).toStdString();
        for (auto add : configuration["Arguments"].toArray()) {
            spawn.arguments.emplace_back(applySubstitutions(add.toQString()).toStdString());
        }
    }

    spawn.inputStream = IO::Process::Spawn::StreamMode::Capture;
    spawn.outputStream = IO::Process::Spawn::StreamMode::Capture;
    spawn.errorStream = IO::Process::Spawn::StreamMode::Forward;

    invoke.emplace(depth, std::move(spawn));
}

TransferUnpack::Requirement::Requirement(Type type, bool optional, int flags) : type(type),
                                                                                optional(optional),
                                                                                flags(flags)
{ }

void TransferUnpack::requireEnd(bool optional)
{ requirements.emplace_back(Requirement::Type::End, optional); }

void TransferUnpack::requireChecksum(bool optional)
{ requirements.emplace_back(Requirement::Type::Checksum, optional); }

void TransferUnpack::requireCompression(bool optional)
{ requirements.emplace_back(Requirement::Type::Compression, optional); }

void TransferUnpack::requireEncryption(int flags, bool optional)
{
    requirements.emplace_back(Requirement::Type::Encryption, optional, flags);
    requireChecksum(true);
}

void TransferUnpack::requireSignature(int flags, bool optional)
{ requirements.emplace_back(Requirement::Type::Signature, optional, flags); }

class TransferUnpack::Stage {
protected:
    TransferUnpack &parent;
    CPD3::ActionFeedback::Source &feedback;

    Threading::Signal<> terminateRequested;

    bool isTerminated() const
    {
        std::lock_guard<std::mutex> lock(parent.mutex);
        return parent.terminated;
    }

    bool fail(const QString &message) const
    {
        std::lock_guard<std::mutex> lock(parent.mutex);
        parent.error = message;
        return false;
    }

public:
    explicit Stage(TransferUnpack &parent) : parent(parent),
                                             feedback(parent.feedback),
                                             terminateRequested(parent.terminateRequested)
    { }

    virtual ~Stage() = default;

    class Header;

    class Process;
};

class TransferUnpack::Stage::Header : public Stage {
public:
    explicit Header(TransferUnpack &parent) : Stage(parent)
    { }

    virtual ~Header() = default;

    virtual bool exec(IO::Generic::Block &input, int depth) = 0;
};

class TransferUnpack::Stage::Process : public Stage {
public:
    explicit Process(TransferUnpack &parent) : Stage(parent)
    { }

    virtual ~Process() = default;

    virtual bool exec(IO::Generic::Block &input, IO::Generic::Block &output, int depth) = 0;
};

static bool isAllZero(const Util::ByteView &data)
{
    for (auto c : data) {
        if (c != 0)
            return false;
    }
    return true;
}

bool TransferUnpack::exec(const IO::Access::Handle &input, const IO::Access::Handle &output)
{
    class ChecksumStage : public Stage::Header {
    public:
        explicit ChecksumStage(TransferUnpack &parent) : Header(parent)
        { }

        virtual ~ChecksumStage() = default;

        bool exec(IO::Generic::Block &input, int) override
        {
            feedback.emitState(QObject::tr("Verifying integrity"));

            Util::ByteArray header;
            input.read(header, 65);
            if (header.size() != 65) {
                qCDebug(log_transfer_package) << "Checksum header too short";
                fail(QObject::tr("Unable to read checksum header"));
                return false;
            }
            auto headerEnd = input.tell();

            switch (static_cast<ChecksumType>(header.front())) {
            case ChecksumType::SHA512:
                break;
            default:
                qCDebug(log_transfer_package) << "Unrecognized checksum type:" << header.front();
                fail(QObject::tr("Invalid checksum type"));
                return false;
            }

            while (!parent.requirements.empty()) {
                auto require = std::move(parent.requirements.front());
                parent.requirements.pop_front();
                if (require.type == Requirement::Type::Checksum)
                    break;
                if (require.optional)
                    continue;

                qCDebug(log_transfer_package) << "Invalid compression requirement (expected"
                                              << static_cast<int>(require.type) << ")";
                fail(QObject::tr("Compression requirement failed"));
                return false;
            }

            std::unique_ptr<IO::Generic::Stream>
                    reader(new IO::Generic::Block::StreamReader(input));
            Threading::Receiver rx;
            {
                auto ended = reader->ended;
                terminateRequested.connect(rx, [ended] {
                    ended();
                });
            }
            if (isTerminated())
                return false;
            auto dataHash = Cryptography::sha512(std::move(reader));
            rx.disconnect();

            if (dataHash != header.mid(1)) {
                qCDebug(log_transfer_package) << "Checksum mismatch";
                fail(QObject::tr("Checksum mismatch"));
                return false;
            }

            input.seek(headerEnd);
            return true;
        }
    };

    class DecompressionStage : public Stage::Process {

        std::mutex mutex;
        std::condition_variable notify;
        Threading::PoolRun pool;
        bool terminated;

        struct Result {
            bool ready;
            Util::ByteArray contents;

            explicit Result(Util::ByteArray &&contents) : ready(false),
                                                          contents(std::move(contents))
            { }
        };

        std::deque<std::unique_ptr<Result>> results;

        Threading::Receiver rx;

        bool startChunk(IO::Generic::Block &input)
        {
            static constexpr quint32 maximumSize = 512 * 1024 * 1024;

            Util::ByteArray data;
            input.read(data, 4);
            if (data.size() == 0) {
                /* End of file, which should get detected on the next iteration */
                return true;
            }
            if (data.size() != 4) {
                qCDebug(log_transfer_package) << "Unable to read compression chunk header";
                fail(QObject::tr("Error in compression stream"));
                return false;
            }

            auto size = data.readNumber<quint32>();
            if (size <= 0 || size > maximumSize) {
                qCDebug(log_transfer_package) << "Invalid compression chunk size";
                fail(QObject::tr("Error in compression stream"));
                return false;
            }

            data.clear();
            input.read(data, size);
            if (data.size() != size) {
                qCDebug(log_transfer_package) << "Error reading compression chunk";
                fail(QObject::tr("Error in compression stream"));
                return false;
            }

            results.emplace_back(new Result(std::move(data)));
            auto result = results.back().get();

            pool.run([this, result]() {
                {
                    Util::ByteArray data;
                    DecompressorLZ4 decompressor;
                    decompressor.decompress(result->contents, data);
                    result->contents = std::move(data);
                }
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    result->ready = true;
                }
                notify.notify_all();
            });

            return true;
        }

    public:
        explicit DecompressionStage(TransferUnpack &parent) : Process(parent), terminated(false)
        { }

        virtual ~DecompressionStage()
        {
            {
                std::lock_guard<std::mutex> lock(mutex);
                terminated = true;
            }
            notify.notify_all();

            rx.disconnect();
            pool.wait();
        }

        bool exec(IO::Generic::Block &input, IO::Generic::Block &output, int) override
        {
            feedback.emitState(QObject::tr("Decompressing"));

            {
                Util::ByteArray header;
                input.read(header, 1);
                if (header.size() != 1) {
                    qCDebug(log_transfer_package) << "Compression header too short";
                    fail(QObject::tr("Unable to read compression header"));
                    return false;
                }

                switch (static_cast<CompressionType>(header.front())) {
                case CompressionType::LZ4:
                    break;
                default:
                    qCDebug(log_transfer_package) << "Unrecognized compression type:"
                                                  << header.front();
                    fail(QObject::tr("Invalid compression type"));
                    return false;
                }
            }

            while (!parent.requirements.empty()) {
                auto require = std::move(parent.requirements.front());
                parent.requirements.pop_front();
                if (require.type == Requirement::Type::Compression)
                    break;
                if (require.optional)
                    continue;

                qCDebug(log_transfer_package) << "Invalid compression requirement (expected"
                                              << static_cast<int>(require.type) << ")";
                fail(QObject::tr("Compression requirement failed"));
                return false;
            }

            terminateRequested.connect(rx, [this] {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    terminated = true;
                }
                notify.notify_all();
            });
            if (isTerminated())
                return false;

            while (!input.readFinished()) {
                if (results.size() < 8) {
                    if (!startChunk(input))
                        return false;
                    continue;;
                }

                std::unique_ptr<Result> process;
                {
                    std::unique_lock<std::mutex> lock(mutex);
                    if (terminated)
                        return false;
                    if (!results.front()->ready) {
                        notify.wait(lock);
                        continue;
                    }
                    process = std::move(results.front());
                    results.pop_front();
                }
                output.write(std::move(process->contents));
            }

            while (!results.empty()) {
                std::unique_ptr<Result> process;
                {
                    std::unique_lock<std::mutex> lock(mutex);
                    if (terminated)
                        return false;
                    if (!results.front()->ready) {
                        notify.wait(lock);
                        continue;
                    }
                    process = std::move(results.front());
                    results.pop_front();
                }

                output.write(std::move(process->contents));
            }

            return true;
        }
    };

    class DecryptStage : public Stage::Process {
    public:
        explicit DecryptStage(TransferUnpack &parent) : Process(parent)
        { }

        virtual ~DecryptStage() = default;

        bool exec(IO::Generic::Block &input, IO::Generic::Block &output, int depth) override
        {
            feedback.emitState(QObject::tr("Decrypting"));

            bool usePrivate = false;
            Data::Variant::Read key;
            {
                Util::ByteArray header;
                input.read(header, 65);
                if (header.size() != 65) {
                    qCDebug(log_transfer_package) << "Encryption header too short";
                    fail(QObject::tr("Unable to read encryption header"));
                    return false;
                }

                switch (static_cast<EncryptType>(header.front())) {
                case EncryptType::Private:
                    usePrivate = false;
                    break;
                case EncryptType::Public:
                    usePrivate = true;
                    break;
                default:
                    qCDebug(log_transfer_package) << "Unrecognized encryption type:"
                                                  << header.front();
                    fail(QObject::tr("Invalid encryption type"));
                    return false;
                }

                while (!parent.requirements.empty()) {
                    auto require = std::move(parent.requirements.front());
                    parent.requirements.pop_front();
                    if (require.type != Requirement::Type::Encryption) {
                        if (require.optional)
                            continue;
                        qCDebug(log_transfer_package) << "Invalid encryption requirement (expected"
                                                      << static_cast<int>(require.type) << ")";
                        fail(QObject::tr("Encryption requirement failed"));
                        return false;
                    }
                    if (require.flags & Encryption_Certificate) {
                        if (!usePrivate) {
                            qCDebug(log_transfer_package)
                                << "Encryption requirement needs a certificate";
                            fail(QObject::tr("Encryption requirement failed"));
                            return false;
                        }
                    } else if (require.flags & Encryption_Key) {
                        if (usePrivate) {
                            qCDebug(log_transfer_package) << "Encryption requirement needs a key";
                            fail(QObject::tr("Encryption requirement failed"));
                            return false;
                        }
                    }

                    if (require.flags & Encryption_RequireIdentification) {
                        if (isAllZero(header.mid(1))) {
                            qCDebug(log_transfer_package)
                                << "Encryption requirement needs an explicit identifier";
                            fail(QObject::tr("Encryption requirement failed"));
                            return false;
                        }
                    }
                    break;
                }

                if (usePrivate) {
                    key = parent.getKeyForCertificate(header.mid(1).toQByteArray(), depth - 1);
                } else {
                    key = parent.getCertificate(header.mid(1).toQByteArray(), true, depth - 1);
                }

                if (!key.exists()) {
                    qCDebug(log_transfer_package) << "No decryption key available for:"
                                                  << header.mid(1).toQByteArrayRef().toHex();
                    fail(QObject::tr("No key available"));
                    return false;
                }
            }

            std::unique_ptr<IO::Generic::Stream>
                    reader(new IO::Generic::Block::StreamReader(input));
            Threading::Receiver rx;
            {
                auto ended = reader->ended;
                terminateRequested.connect(rx, [ended] {
                    ended();
                });
            }
            if (isTerminated())
                return false;

            if (!Cryptography::decrypt(key, std::move(reader), output, usePrivate, true))
                return false;

            rx.disconnect();
            return true;
        }
    };

    class SignStage : public Stage::Header {

        std::vector<Util::ByteArray> readSignatures(IO::Generic::Block &input, std::size_t count)
        {
            std::vector<Util::ByteArray> signatures;

            for (std::size_t i = 0; i < count; i++) {
                Util::ByteArray header;
                input.read(header, 2);
                if (header.size() != 2) {
                    qCDebug(log_transfer_package) << "Signature" << i << "header too short";
                    fail(QObject::tr("Unable to read signature header"));
                    return {};
                }

                std::size_t signatureSize = header.readNumber<quint16>() + 1;
                signatures.emplace_back();
                input.read(signatures.back(), signatureSize);
                if (signatures.back().size() != signatureSize) {
                    qCDebug(log_transfer_package) << "Signature" << i << "too short";
                    fail(QObject::tr("Unable to read signature header"));
                    return {};
                }
            }

            return signatures;
        }

        bool readSigningCertificates(IO::Generic::Block &input,
                                     std::size_t count,
                                     int depth,
                                     std::vector<Data::Variant::Root> &certificates,
                                     std::vector<Util::ByteArray> &ids)
        {
            for (std::size_t i = 0; i < count; i++) {
                Util::ByteArray data;
                input.read(data, 4);
                if (data.size() != 4) {
                    qCDebug(log_transfer_package) << "Certificate" << i << "header too short";
                    fail(QObject::tr("Unable to read signature header"));
                    return false;
                }

                std::size_t dataSize = data.readNumber<quint32>();
                if (dataSize <= 0 || dataSize > maximumCertificateSize) {
                    qCDebug(log_transfer_package) << "Certificate" << i << "header too large at"
                                                  << dataSize << "bytes";
                    fail(QObject::tr("Unable to read signature header"));
                    return false;
                }

                data.clear();
                input.read(data, dataSize);
                if (data.size() != dataSize) {
                    qCDebug(log_transfer_package) << "Certificate" << i << "data too short";
                    fail(QObject::tr("Unable to read signature header"));
                    return false;
                }

                QSslCertificate cert(data.toQByteArrayRef(), QSsl::Der);
                if (cert.isNull()) {
                    qCDebug(log_transfer_package) << "Certificate" << i << "is corrupted";
                    fail(QObject::tr("Corrupted signature certificate"));
                    return false;
                }

                if (!parent.signingCertificateValid(cert, depth - 1)) {
                    qCDebug(log_transfer_package) << "Certificate" << i << "rejected as invalid";
                    fail(QObject::tr("Invalid certificate"));
                    return false;
                }

                certificates.emplace_back(Data::Variant::Bytes(data.toQByteArray()));

                data = Cryptography::sha512(cert);
                if (data.size() != 64) {
                    qCDebug(log_transfer_package)
                        << "Signature certificate SHA512 calculation error";
                    fail(QObject::tr("SHA512 calculation error"));
                    return false;
                }
                ids.emplace_back(std::move(data));
            }

            return true;
        }

        bool readSigningIDs(IO::Generic::Block &input,
                            std::size_t count,
                            int depth,
                            std::vector<Data::Variant::Root> &certificates,
                            std::vector<Util::ByteArray> &ids)
        {
            for (std::size_t i = 0; i < count; i++) {
                Util::ByteArray data;
                input.read(data, 64);
                if (data.size() != 64) {
                    qCDebug(log_transfer_package) << "Certificate" << i << "identifier too short";
                    fail(QObject::tr("Unable to read signature header"));
                    return false;
                }

                auto cert = parent.getCertificate(data.toQByteArray(), false, depth - 1);
                if (!cert.exists()) {
                    qCDebug(log_transfer_package) << "Certificate" << i << "not available";
                    fail(QObject::tr("No signature data"));
                    return false;
                }

                certificates.emplace_back(Data::Variant::Root(cert));
                ids.emplace_back(std::move(data));
            }

            return true;
        }

        bool checkSignatures(const Util::ByteView &dataHash,
                             const std::vector<Util::ByteArray> &signatures,
                             const std::vector<Data::Variant::Root> &certificates)
        {
            Q_ASSERT(signatures.size() == certificates.size());
            for (std::size_t i = 0, max = signatures.size(); i < max; i++) {
                if (!Cryptography::checkHashSignature(certificates[i], signatures[i], dataHash,
                                                      true)) {
                    qCDebug(log_transfer_package) << "Signature failure for certificate" << i;
                    fail(QObject::tr("Signature mismatch"));
                    return false;
                }
            }
            return true;
        }

    public:
        explicit SignStage(TransferUnpack &parent) : Header(parent)
        { }

        virtual ~SignStage() = default;

        bool exec(IO::Generic::Block &input, int depth) override
        {
            feedback.emitState(QObject::tr("Verifying signatures"));

            bool includeCertificate = false;
            std::size_t signatureCount = 0;
            {
                Util::ByteArray header;
                input.read(header, 3);
                if (header.size() != 3) {
                    qCDebug(log_transfer_package) << "Signature global header too short";
                    fail(QObject::tr("Unable to read signature header"));
                    return false;
                }

                switch (static_cast<SignType>(header.front())) {
                case SignType::CertificateIncluded:
                    includeCertificate = true;
                    break;
                case SignType::CertificateImplied:
                    includeCertificate = false;
                    break;
                default:
                    qCDebug(log_transfer_package) << "Unrecognized signature type:"
                                                  << header.front();
                    fail(QObject::tr("Invalid signature type"));
                    return false;
                }

                signatureCount = header.readNumber<quint16>(1) + 1;
            }
            Q_ASSERT(signatureCount > 0);

            auto signatures = readSignatures(input, signatureCount);
            if (signatures.size() != signatureCount)
                return false;
            std::size_t signedDataBegin = input.tell();

            std::vector<Data::Variant::Root> certificates;
            std::vector<Util::ByteArray> ids;
            if (includeCertificate) {
                if (!readSigningCertificates(input, signatureCount, depth, certificates, ids))
                    return false;
            } else {
                if (!readSigningIDs(input, signatureCount, depth, certificates, ids))
                    return false;
            }
            if (certificates.size() != signatureCount)
                return false;
            std::size_t interiorDataBegin = input.tell();

            while (!parent.requirements.empty()) {
                auto require = std::move(parent.requirements.front());
                parent.requirements.pop_front();
                if (require.type != Requirement::Type::Signature) {
                    if (require.optional)
                        continue;
                    qCDebug(log_transfer_package) << "Invalid signature requirement (expected"
                                                  << static_cast<int>(require.type) << ")";
                    fail(QObject::tr("Signature requirement failed"));
                    return false;
                }
                if (require.flags & Signature_IncludeCertificate) {
                    if (!includeCertificate) {
                        qCDebug(log_transfer_package)
                            << "Signature requirement needs a certificate";
                        fail(QObject::tr("Signature requirement failed"));
                        return false;
                    }
                } else if (require.flags & Signature_RequireIdentification) {
                    for (const auto &check : signatures) {
                        if (isAllZero(check)) {
                            qCDebug(log_transfer_package)
                                << "Signature requirement needs explicit identification";
                            fail(QObject::tr("Signature requirement failed"));
                            return false;
                        }
                    }
                }

                if (require.flags & Signature_SingleOnly) {
                    if (signatures.size() != 1) {
                        qCDebug(log_transfer_package) << "Only a single signature is acceptable";
                        fail(QObject::tr("Signature requirement failed"));
                        return false;
                    }
                }
                break;
            }

            input.seek(signedDataBegin);
            std::unique_ptr<IO::Generic::Stream>
                    reader(new IO::Generic::Block::StreamReader(input));
            Threading::Receiver rx;
            {
                auto ended = reader->ended;
                terminateRequested.connect(rx, [ended] {
                    ended();
                });
            }
            if (isTerminated())
                return false;
            auto dataHash = Cryptography::sha512(std::move(reader));
            rx.disconnect();
            if (dataHash.empty()) {
                qCDebug(log_transfer_package) << "Error calculating signature data hash";
                fail(QObject::tr("SHA512 calculation error"));
                return false;
            }

            if (!checkSignatures(dataHash, signatures, certificates))
                return false;

            {
                QList<QByteArray> check;
                for (const auto &data : ids) {
                    check.append(data.toQByteArray());
                }
                if (!parent.signatureAuthorized(check, depth - 1)) {
                    qCDebug(log_transfer_package) << "Signature authorization failure";
                    fail(QObject::tr("Unauthorized signature"));
                    return false;
                }
            }

            input.seek(interiorDataBegin);
            return true;
        }
    };

    struct State {
        TransferUnpack &parent;

        std::shared_ptr<IO::File::Backing> temporaryInput;
        std::shared_ptr<IO::File::Backing> temporaryOutput;
        std::unique_ptr<IO::Generic::Block> stageInput;
        std::unique_ptr<IO::Generic::Block> stageOutput;

        std::unique_ptr<Stage> stage;

        State(TransferUnpack &parent) : parent(parent)
        { }

        bool slurpInputStream(std::unique_ptr<IO::Generic::Stream> &&input,
                              IO::Generic::Writable &output)
        {
            std::condition_variable cv;
            bool ended = false;
            Threading::Receiver rx;

            input->read.connect(rx, [&](const Util::ByteArray &data) {
                output.write(data);
            });
            input->ended.connect(rx, [&]() {
                {
                    std::lock_guard<std::mutex> lock(parent.mutex);
                    ended = true;
                }
                cv.notify_all();
            });
            parent.terminateRequested.connect(rx, [&]() {
                cv.notify_all();
            });

            input->start();
            {
                std::unique_lock<std::mutex> lock(parent.mutex);
                cv.wait(lock, [&] { return ended || parent.terminated; });
            }
            input.reset();
            return ended;
        }

        bool fail(const QString &message)
        {
            parent.error = message;
            return false;
        }

        bool begin(const IO::Access::Handle &input)
        {
            stageInput = input->block();

            if (!stageInput) {
                auto stream = input->stream();
                if (!stream) {
                    qCDebug(log_transfer_package) << "Unable to acquire base input";
                    parent.error = QObject::tr("No input available");
                    return false;
                }
                temporaryInput = IO::Access::temporaryFile();
                if (!temporaryInput) {
                    qCDebug(log_transfer_package) << "Unable to acquire temporary input";
                    fail(QObject::tr("Temporary input creation failed"));
                    return false;
                }
                stageInput = temporaryInput->block();
                if (!stageInput) {
                    qCDebug(log_transfer_package) << "Unable to create temporary input block";
                    fail(QObject::tr("Temporary input creation failed"));
                    return false;
                }
                if (!slurpInputStream(std::move(stream), *stageInput)) {
                    qCDebug(log_transfer_package) << "Error reading input";
                    fail(QObject::tr("Input read failed"));
                    return false;
                }
            }

            stageInput->seek(0);
            return true;
        }

        bool executeInvoke(std::multimap<int, IO::Process::Spawn>::const_iterator begin,
                           std::multimap<int, IO::Process::Spawn>::const_iterator end)
        {
            if (begin == end)
                return true;

            Q_ASSERT(stageInput.get() != nullptr);
            temporaryInput = IO::Access::temporaryFile();
            if (!temporaryInput) {
                qCDebug(log_transfer_package) << "Unable to acquire temporary input";
                fail(QObject::tr("Temporary input creation failed"));
                return false;
            }
            {
                auto writeBlock = temporaryInput->block();
                if (!writeBlock) {
                    qCDebug(log_transfer_package) << "Unable to create temporary input block";
                    fail(QObject::tr("Temporary file creation failed"));
                    return false;
                }
                if (!slurpInputStream(std::unique_ptr<IO::Generic::Stream>(
                        new IO::Generic::Block::StreamReader(*stageInput)), *writeBlock)) {
                    qCDebug(log_transfer_package) << "Error reading input";
                    fail(QObject::tr("Input read failed"));
                    return false;
                }
            }
            stageInput.reset();

            temporaryOutput = IO::Access::temporaryFile();
            if (!temporaryOutput) {
                qCDebug(log_transfer_package) << "Unable to acquire temporary output";
                fail(QObject::tr("Temporary output creation failed"));
                return false;
            }

            std::vector<IO::Process::Spawn> pipelineSpawn;
            for (; begin != end; ++begin) {
                pipelineSpawn.emplace_back(begin->second);
            }

            Q_ASSERT(temporaryInput.get() != nullptr);
            pipelineSpawn.front().inputStream = IO::Process::Spawn::StreamMode::File;
            pipelineSpawn.front().inputFile = temporaryInput->filename();

            Q_ASSERT(temporaryOutput.get() != nullptr);
            pipelineSpawn.back().outputStream = IO::Process::Spawn::StreamMode::File;
            pipelineSpawn.back().outputFile = temporaryOutput->filename();

            {
                auto pipeline = IO::Process::Spawn::pipeline(std::move(pipelineSpawn));
                if (pipeline.processes.empty()) {
                    fail(QObject::tr("Pipeline creation failed"));
                    return false;
                }

                Threading::Receiver rx;
                parent.terminateRequested.connect(rx, [&] { pipeline.terminate(); });

                {
                    std::lock_guard<std::mutex> lock(parent.mutex);
                    if (parent.terminated)
                        return false;
                }

                if (!pipeline.start() || !pipeline.wait()) {
                    fail(QObject::tr("Pipeline execution failed"));
                    return false;
                }
            }

            stageInput.reset();
            stageOutput.reset();
            temporaryInput = std::move(temporaryOutput);
            temporaryOutput.reset();

            stageInput = temporaryInput->block();
            if (!stageInput) {
                qCDebug(log_transfer_package) << "Unable to create pipeline output input";
                fail(QObject::tr("Temporary input creation failed"));
                return false;
            }

            return true;
        }

        bool executeStage(int depth)
        {
            Q_ASSERT(stage.get() != nullptr);
            Q_ASSERT(stageInput.get() != nullptr);

            if (auto execute = dynamic_cast<Stage::Header *>(stage.get())) {
                return execute->exec(*stageInput, depth);
            } else if (auto execute = dynamic_cast<Stage::Process *>(stage.get())) {
                temporaryOutput = IO::Access::temporaryFile();
                if (!temporaryOutput) {
                    qCDebug(log_transfer_package) << "Unable to acquire temporary output";
                    parent.error = QObject::tr("Temporary output creation failed");
                    return false;
                }
                stageOutput = temporaryOutput->block();
                if (!stageOutput) {
                    qCDebug(log_transfer_package) << "Unable to create temporary output block";
                    parent.error = QObject::tr("Temporary output creation failed");
                    return false;
                }
                return execute->exec(*stageInput, *stageOutput, depth);
            } else {
                Q_ASSERT(false);
                return false;
            }
        }

        bool advanceStage()
        {
            stage.reset();

            if (stageOutput) {
                stageInput = std::move(stageOutput);
                stageInput->seek(0);
            }
            if (temporaryOutput)
                temporaryInput = std::move(temporaryOutput);

            Util::ByteArray data;
            stageInput->read(data, 1);
            if (data.size() != 1) {
                qCDebug(log_transfer_package) << "Stage has no header available";
                parent.error = QObject::tr("No stage header");
                return false;
            }

            switch (static_cast<StageType>(data.front())) {
            case StageType::Inner:
                while (!parent.requirements.empty()) {
                    auto require = std::move(parent.requirements.front());
                    parent.requirements.pop_front();
                    if (require.type == Requirement::Type::End)
                        break;
                    if (require.optional)
                        continue;
                    qCDebug(log_transfer_package) << "Invalid end requirement (expected"
                                                  << static_cast<int>(require.type) << ")";
                    {
                        std::lock_guard<std::mutex> lock(parent.mutex);
                        parent.error = QObject::tr("Final requirement failed");
                    }
                    return false;
                }

                /* No further processing, so leave the stage unset */
                return true;
            case StageType::Checksum:
                stage.reset(new ChecksumStage(parent));
                break;
            case StageType::Compression:
                stage.reset(new DecompressionStage(parent));
                break;
            case StageType::Encrypt:
                stage.reset(new DecryptStage(parent));
                break;
            case StageType::Sign:
                stage.reset(new SignStage(parent));
                break;
            default:
                qCDebug(log_transfer_package) << "Invalid stage type:" << data.front();
                parent.error = QObject::tr("Invalid stage type");
                return false;
            }

            return true;
        }

        bool finish(const IO::Access::Handle &output)
        {
            std::unique_ptr<IO::Generic::Stream>
                    inputStream(new IO::Generic::Block::StreamReader(*stageInput));
            if (auto stream = output->stream()) {
                if (!slurpInputStream(std::move(inputStream), *stream)) {
                    qCDebug(log_transfer_package) << "Error writing output block";
                    parent.error = QObject::tr("Error writing output");
                    return false;
                }
            } else if (auto block = output->block()) {
                if (!slurpInputStream(std::move(inputStream), *block)) {
                    qCDebug(log_transfer_package) << "Error writing output block";
                    parent.error = QObject::tr("Error writing output");
                    return false;
                }
            } else {
                qCDebug(log_transfer_package) << "Output unavailable";
                parent.error = QObject::tr("Error creating output");
                return false;
            }
            inputStream.reset();
            stageInput.reset();

            return true;
        }
    };
    State state(*this);

    if (!state.begin(input))
        return false;

    class InitialHeader : public Stage::Header {
    public:
        explicit InitialHeader(TransferUnpack &parent) : Header(parent)
        { }

        virtual ~InitialHeader() = default;

        bool exec(IO::Generic::Block &input, int)
        {
            Util::ByteArray data;
            input.read(data, 4);
            if (data.size() != 4 ||
                    qFromBigEndian<quint32>(data.data<uchar *>()) != fileMagicStart) {
                qCDebug(log_transfer_package) << "Magic number header mismatch";
                fail(QObject::tr("Invalid header"));
                return false;
            }
            return true;
        }
    };
    state.stage.reset(new InitialHeader(*this));

    feedback.emitStage(QObject::tr("Unpacking"),
                       QObject::tr("The system is unpacking the input data."), false);

    auto priorInvoke = invoke.begin();
    for (int depth = 0;; ++depth) {
        {
            auto next = invoke.upper_bound(depth);
            if (!state.executeInvoke(priorInvoke, next))
                return false;
            priorInvoke = next;
        }

        if (!state.executeStage(depth))
            return false;

        if (depth > 0 && !continueUnpacking(depth - 1))
            return false;

        if (!state.advanceStage())
            return false;
        if (!state.stage)
            break;
    }

    if (!state.executeInvoke(priorInvoke, invoke.end()))
        return false;

    feedback.emitState(QObject::tr("Finalizing"));

    return state.finish(output);
}

void TransferUnpack::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;

    }
    terminateRequested();
}

QString TransferUnpack::errorString()
{
    std::lock_guard<std::mutex> lock(mutex);
    return error;
}

QString TransferUnpack::applySubstitutions(const QString &input) const
{ return input; }

Data::Variant::Read TransferUnpack::getCertificate(const QByteArray &, bool, int)
{ return Data::Variant::Read::empty(); }

Data::Variant::Read TransferUnpack::getKeyForCertificate(const QByteArray &id, int depth)
{ return Data::Variant::Read::empty(); }

bool TransferUnpack::signatureAuthorized(const QList<QByteArray> &, int)
{ return true; }

bool TransferUnpack::signingCertificateValid(const QSslCertificate &, int)
{ return true; }

bool TransferUnpack::continueUnpacking(int)
{ return true; }

}
}

