/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3TRANSFERUPLOAD_H
#define CPD3TRANSFERUPLOAD_H

#include "core/first.hxx"

#include <mutex>
#include <QFileInfo>

#include "transfer.hxx"
#include "smtp.hxx"
#include "email.hxx"
#include "datacore/variant/root.hxx"
#include "core/actioncomponent.hxx"
#include "io/access.hxx"
#include "io/drivers/url.hxx"
#include "io/drivers/file.hxx"

namespace CPD3 {
namespace Transfer {

/**
 * This is the common interface to handle uploading of files to external
 * targets.
 */
class CPD3TRANSFER_EXPORT FileUploader {
    Data::Variant::Read config;

    struct UploadInput final {
        IO::Access::Handle source;
        std::unique_ptr<IO::Generic::Stream> stream;

        UploadInput();

        ~UploadInput();

        UploadInput(const UploadInput &) = delete;

        UploadInput(UploadInput &&) = default;

        explicit UploadInput(IO::Access::Handle handle);

        explicit UploadInput(std::unique_ptr<IO::Generic::Stream> &&stream);
    };
    std::vector<UploadInput> inputs;

    std::unique_ptr<SMTPSend> smtpSend;
    std::shared_ptr<IO::URL::Context> urlContext;

    std::mutex mutex;
    bool terminated;

    Threading::Signal<> terminateRequested;

    bool testTerminated();

    bool invokeCommand(const QString &program,
                       const QStringList &arguments,
                       double timeout = FP::undefined());

    bool executeActions(const Data::Variant::Read &settings);

    bool configureEmail(const Data::Variant::Read &settings, EmailBuilder &builder);

    bool attachEmailFile(const Data::Variant::Read &settings,
                         EmailBuilder &builder,
                         const QByteArray &data,
                         const QString &attachmentName);

    bool sendEmail(const Data::Variant::Read &settings,
                   const QByteArray &data,
                   const QString &attachmentName);

    QString getPassword(const Data::Variant::Read &settings, const QString &def = {});

    bool uploadFTP(const Data::Variant::Read &settings, UploadInput &source);

    bool uploadURL(const Data::Variant::Read &settings, UploadInput &source);

    void streamCopy(std::unique_ptr<IO::Generic::Stream> &&source, IO::Generic::Writable &target);

    std::shared_ptr<IO::File::Backing> toTemporaryFile(std::unique_ptr<
            IO::Generic::Stream> &&source);

    bool uploadCommand(const Data::Variant::Read &settings, UploadInput &source);

    bool uploadSFTP(const Data::Variant::Read &settings, UploadInput &source);

public:
    /**
     * Create an uploader.
     * 
     * @param config        the configuration
     */
    FileUploader(const Data::Variant::Read &config0);

    /**
     * Add a source to upload.
     *
     * @param source        the source of data
     */
    void addUpload(IO::Access::Handle source);

    void addUpload(std::unique_ptr<IO::Generic::Stream> &&source);

    virtual ~FileUploader();

    /**
     * Move a local from from one location to another.
     *
     * @param from      the original file
     * @param to        the new file
     * @param overwrite overwrite the target if it exists
     * @return          true on success
     */
    static bool moveLocalFile(const QFileInfo &from, const QFileInfo &to, bool overwrite = true);

    /**
     * Copy a local from from one location to another.
     *
     * @param from      the original file
     * @param to        the new file
     * @param overwrite overwrite the target if it exists
     * @return          true on success
     */
    static bool copyLocalFile(const QFileInfo &from, const QFileInfo &to, bool overwrite = true);

    /**
     * Perform the upload.
     *
     * @return  true on success
     */
    bool exec();

    /**
     * Request termination of the uploader.
     */
    void signalTerminate();

    CPD3::ActionFeedback::Source feedback;

protected:

    /**
     * Apply substitutions to strings being used.
     * <br>
     * By default this just returns the input.
     *
     * @param input the input
     * @return      the input with substitutions applied
     */
    virtual QString applySubstitutions(const QString &input) const;

    /**
     * Called when a temporary file is created for output.
     * <br>
     * By default this does nothing.
     *
     * @param file  the file created
     */
    virtual void pushTemporaryOutput(const QFileInfo &file);

    /**
     * Called when a temporary file is removed.
     * <br>
     * By default this does nothing.
     */
    virtual void popTemporaryOutput();

    /**
     * Called when starting actions on an input file.
     * <br>
     * By default this does nothing.
     *
     * @param file      the file being processed
     */
    virtual void pushInputFile(const QFileInfo &file);

    /**
     * Called when action on an input file are completed.
     * <br>
     * By default this does nothing.
     */
    virtual void popInputFile();

    /**
     * Get the MIME type associated with a file name.
     * 
     * @param fileName      the file name
     * @return              the mime type
     */
    virtual QString getMIMEType(const QString &fileName) const;
};

}
}

#endif
