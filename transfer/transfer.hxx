/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3TRANSFER_H
#define CPD3TRANSFER_H

#include <QtCore/QtGlobal>

#if defined(cpd3transfer_EXPORTS)
#   define CPD3TRANSFER_EXPORT Q_DECL_EXPORT
#else
#   define CPD3TRANSFER_EXPORT Q_DECL_IMPORT
#endif

#endif
