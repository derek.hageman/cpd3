/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3_TRANSFERDATAFILE_HXX
#define CPD3_TRANSFERDATAFILE_HXX

#include "core/first.hxx"

#include <vector>
#include <mutex>
#include <condition_variable>
#include <unordered_map>
#include <cstdint>
#include <QIODevice>
#include <QDataStream>
#include <QLoggingCategory>

#include "transfer/transfer.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/stream.hxx"
#include "core/actioncomponent.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_transfer_datafile)

namespace CPD3 {
namespace Transfer {

/**
 * The data package creator.  This accepts values and deleted erasures and generates
 * a data file package.
 */
class CPD3TRANSFER_EXPORT DataPack : public Data::ExternalSink, public virtual Data::ErasureSink {
    std::unique_ptr<IO::Generic::Stream> target;
    bool includeModified;
    bool useLegacySerialization;

    std::mutex mutex;
    std::condition_variable external;
    std::condition_variable internal;
    Data::ArchiveValue::Transfer pendingValues;
    Data::ArchiveErasure::Transfer pendingErasure;
    bool writeAll;
    bool ended;
    bool processingActive;
    bool terminated;
    bool threadComplete;

    bool shouldWritePending() const;

    template<typename T>
    class CircularSerializer {
        std::unordered_map<T, quint16> idLookup;
        std::vector<T> buffer;
        std::vector<std::uint_fast8_t> referenced;

        quint16 pendingBegin;
        quint16 pendingEnd;
    public:
        CircularSerializer() : idLookup(), buffer(), referenced(), pendingBegin(0), pendingEnd(0)
        { }

        virtual ~CircularSerializer() = default;

        bool hasPending() const
        { return pendingBegin != pendingEnd; }

        bool canAcceptNew() const
        {
            quint16 next = static_cast<quint16>(pendingEnd + 1);
            if (next == pendingBegin)
                return false;
            if (next >= referenced.size())
                return true;
            return !referenced[next];
        }

        void flushPending(QDataStream &target)
        {
            quint32 total;
            if (pendingEnd >= pendingBegin) {
                total = static_cast<quint32>(pendingEnd) - static_cast<quint32>(pendingBegin);
            } else {
                total = static_cast<quint32>(0xFFFF) -
                        static_cast<quint32>(pendingBegin) +
                        static_cast<quint32>(pendingEnd) +
                        static_cast<quint32>(1);
            }
            Q_ASSERT(total > 0);

            target << static_cast<quint16>(total - 1);
            for (; pendingBegin != pendingEnd; ++pendingBegin) {
                serialize(target, buffer[pendingBegin]);
            }
        }

        void resetReferenced()
        {
            referenced.clear();
        }

        quint16 lookup(const T &input)
        {
            auto check = idLookup.find(input);
            if (check != idLookup.end()) {
                quint16 result = check->second;
                if (result >= referenced.size())
                    referenced.resize(result + 1, 0);
                referenced[result] = 1;
                return result;
            }

            Q_ASSERT(canAcceptNew());

            prepareLookup(input);

            quint16 result = pendingEnd;
            if (result < buffer.size()) {
                check = idLookup.find(buffer[result]);
                Q_ASSERT(check != idLookup.end());
                idLookup.erase(check);
                buffer[pendingEnd] = input;
            } else {
                Q_ASSERT(result == buffer.size());
                buffer.push_back(input);
            }
            idLookup.emplace(input, result);
            pendingEnd++;

            if (result >= referenced.size())
                referenced.resize(result + 1, 0);
            referenced[result] = 1;
            return result;
        }

    protected:
        virtual void prepareLookup(const T &)
        { }

        virtual void serialize(QDataStream &stream, const T &add) = 0;
    };

    class StringSerializer : public CircularSerializer<CPD3::Data::SequenceName::Component> {
    public:
        StringSerializer();

        virtual ~StringSerializer();

    protected:
        void serialize(QDataStream &stream,
                       const CPD3::Data::SequenceName::Component &add) override;
    };

    class UnitSerializer : public CircularSerializer<Data::SequenceName> {
        CircularSerializer<CPD3::Data::SequenceName::Component> &stations;
        CircularSerializer<CPD3::Data::SequenceName::Component> &archives;
        CircularSerializer<CPD3::Data::SequenceName::Component> &variables;
        CircularSerializer<CPD3::Data::SequenceName::Component> &flavors;
    public:
        UnitSerializer(CircularSerializer<CPD3::Data::SequenceName::Component> &stations,
                       CircularSerializer<CPD3::Data::SequenceName::Component> &archives,
                       CircularSerializer<CPD3::Data::SequenceName::Component> &variables,
                       CircularSerializer<CPD3::Data::SequenceName::Component> &flavors);

        virtual ~UnitSerializer();

    protected:
        void prepareLookup(const Data::SequenceName &add) override;

        void serialize(QDataStream &stream, const Data::SequenceName &add) override;
    };

    StringSerializer stations;
    StringSerializer archives;
    StringSerializer variables;
    StringSerializer flavors;
    UnitSerializer units;

    std::thread thread;

    bool needToFlushLookup() const;

    struct DataPackageWriteKey {
        double end;
        qint32 priority;

        inline bool operator==(const DataPackageWriteKey &other) const
        {
            return priority == other.priority && FP::equal(end, other.end);
        }

        inline DataPackageWriteKey() : end(FP::undefined()), priority(0)
        { }

        DataPackageWriteKey(const DataPackageWriteKey &) = default;

        DataPackageWriteKey &operator=(const DataPackageWriteKey &) = default;

        DataPackageWriteKey(DataPackageWriteKey &&) = default;

        DataPackageWriteKey &operator=(DataPackageWriteKey &&) = default;

        inline DataPackageWriteKey(const Data::ArchiveValue &value) : end(value.getEnd()),
                                                                      priority(
                                                                              (qint32) value.getPriority())
        { }

        inline DataPackageWriteKey(const Data::ArchiveErasure &value) : end(value.getEnd()),
                                                                        priority(
                                                                                (qint32) value.getPriority())
        { }

        struct hash {
            inline std::size_t operator()(const DataPackageWriteKey &key) const
            {
                std::size_t h = std::hash<qint32>()(key.priority);
                if (FP::defined(key.end)) {
                    h = INTEGER::mix(h, std::hash<double>()(key.end));
                }
                return h;
            }
        };
    };

    struct DataPackageWriteValue {
        quint16 id;
        double modified;
        Data::Variant::Root value;

        inline DataPackageWriteValue() : id(0), modified(FP::undefined()), value()
        { }

        inline DataPackageWriteValue(quint16 i, const Data::ArchiveValue &value) : id(i),
                                                                                   modified(
                                                                                           value.getModified()),
                                                                                   value(value.root())
        { }

        inline DataPackageWriteValue(quint16 i, Data::ArchiveValue &&value) : id(i),
                                                                              modified(
                                                                                      value.getModified()),
                                                                              value(std::move(
                                                                                      value.root()))
        { }

        DataPackageWriteValue(const DataPackageWriteValue &) = default;

        DataPackageWriteValue &operator=(const DataPackageWriteValue &) = default;

        DataPackageWriteValue(DataPackageWriteValue &&) = default;

        DataPackageWriteValue &operator=(DataPackageWriteValue &&) = default;
    };

    struct DataPackageWriteErasure {
        quint16 id;
        double modified;

        inline DataPackageWriteErasure() : id(0), modified(FP::undefined())
        { }

        inline DataPackageWriteErasure(quint16 i, const Data::ArchiveErasure &value) : id(i),
                                                                                       modified(
                                                                                               value.getModified())
        { }

        DataPackageWriteErasure(const DataPackageWriteErasure &) = default;

        DataPackageWriteErasure &operator=(const DataPackageWriteErasure &) = default;

        DataPackageWriteErasure(DataPackageWriteErasure &&) = default;

        DataPackageWriteErasure &operator=(DataPackageWriteErasure &&) = default;
    };

    typedef std::vector<DataPackageWriteValue> ValueWriteList;
    typedef std::unordered_map<DataPackageWriteKey, ValueWriteList, DataPackageWriteKey::hash>
            ValueWriteBreakdown;
    typedef std::pair<double, ValueWriteBreakdown> ValueWriteTime;
    typedef std::vector<ValueWriteTime> ValueWriteQueued;

    typedef std::vector<DataPackageWriteErasure> DeletedWriteList;
    typedef std::unordered_map<DataPackageWriteKey, DeletedWriteList, DataPackageWriteKey::hash>
            DeletedWriteBreakdown;
    typedef std::vector<DeletedWriteBreakdown> DeletedWriteQueued;

    void flushTime(QDataStream &stream,
                   double startTime,
                   const ValueWriteBreakdown &values,
                   const DeletedWriteBreakdown &deleted) const;

    void flushAllPending(QDataStream &stream,
                         ValueWriteQueued &values,
                         DeletedWriteQueued &deleted);

    void enqueueValue(ValueWriteBreakdown &queued, Data::ArchiveValue &&add);

    void enqueueDeleted(DeletedWriteBreakdown &queued, Data::ArchiveErasure &&add);

    void stall(std::unique_lock<std::mutex> &lock);

    void run();

public:
    /**
     * Create the packager.
     *
     * @param target            the target output device
     * @param includeModified   include modification times
     * @param useLegacySerialization use legacy variant serialization format
     * @param parent            the parent object
     */
    DataPack(std::unique_ptr<IO::Generic::Stream> &&target,
             bool includeModified = true,
             bool useLegacySerialization = false);

    /**
     * Create the packager.
     *
     * @param target            the target output device
     * @param includeModified   include modification times
     * @param useLegacySerialization use legacy variant serialization format
     * @param parent            the parent object
     */
    DataPack(QIODevice *target, bool includeModified = true, bool useLegacySerialization = false);

    virtual ~DataPack();

    void incomingValue(const Data::ArchiveValue &value) override;

    void incomingValue(Data::ArchiveValue &&value) override;

    /* @see incomingValue(const Data::ArchiveValue &) */
    void incomingData(const Data::SequenceValue &value) override;

    /* @see incomingValue(const Data::SequenceValue &) */
    void incomingData(Data::SequenceValue &&value) override;


    void incomingErasure(const Data::ArchiveErasure &erasure) override;

    void incomingErasure(Data::ArchiveErasure &&erasure) override;

    void endData() override;

    void endStream() override;

    /**
     * Add incoming archive values
     *
     * @param value the values
     */
    void incomingValue(const Data::ArchiveValue::Transfer &value);

    /** @see incomingValue(const Data::ArchiveValue::Transfer &) */
    void incomingValue(Data::ArchiveValue::Transfer &&value);

    /* @see incomingValue(const Data::ArchiveValue::Transfer &) */
    void incomingData(const Data::SequenceValue::Transfer &value) override;

    /* @see incomingValue(const Data::SequenceValue::Transfer &) */
    void incomingData(Data::SequenceValue::Transfer &&value) override;

    /**
     * Add incoming erasure values
     *
     * @param erasure   the values
     */
    void incomingErasure(const Data::ArchiveErasure::Transfer &erasure);

    /** @see incomingErasure(const Data::ArchiveErasure::Transfer &) */
    void incomingErasure(Data::ArchiveErasure::Transfer &&erasure);

    void flushData() override;

    void signalTerminate() override;

    void start() override;

    bool isFinished() override;

    bool wait(double timeout = CPD3::FP::undefined()) override;
};


/**
 * The data package unpacker.  This reads from an input device and generates a stream
 * of output values and erasures.
 */
class CPD3TRANSFER_EXPORT DataUnpack {
    QString error;

    std::mutex mutex;
    std::condition_variable cond;
    bool terminated;

    template<typename T>
    class CircularDeserializer {
        std::vector<T> buffer;
        quint16 nextRead;

    public:
        CircularDeserializer() : buffer(), nextRead(0)
        { }

        virtual ~CircularDeserializer()
        { }

        const T &lookup(quint16 id) const
        {
            if (id >= buffer.size()) {
                qCWarning(log_transfer_datafile) << "Invalid lookup ID" << id;
                static const T invalid;
                return invalid;
            }
            return buffer[id];
        }

        void readPending(QDataStream &stream)
        {
            quint16 n = 0;
            stream >> n;

            for (quint32 i = 0; i <= n; i++) {
                if (nextRead >= buffer.size()) {
                    Q_ASSERT(nextRead == buffer.size());
                    buffer.push_back(deserialize(stream));
                } else {
                    buffer[nextRead] = deserialize(stream);
                }
                ++nextRead;
            }
        }

        void clear()
        {
            buffer.clear();
            nextRead = 0;
        }

    protected:

        virtual T deserialize(QDataStream &stream) = 0;
    };

    class StringDeserializer : public CircularDeserializer<CPD3::Data::SequenceName::Component> {
    public:
        StringDeserializer();

        virtual ~StringDeserializer();

    protected:
        CPD3::Data::SequenceName::Component deserialize(QDataStream &stream) override;
    };

    class UnitDeserializer : public CircularDeserializer<Data::SequenceName> {
        CircularDeserializer<CPD3::Data::SequenceName::Component> &stations;
        CircularDeserializer<CPD3::Data::SequenceName::Component> &archives;
        CircularDeserializer<CPD3::Data::SequenceName::Component> &variables;
        CircularDeserializer<CPD3::Data::SequenceName::Component> &flavors;
    public:
        UnitDeserializer(CircularDeserializer<CPD3::Data::SequenceName::Component> &stations,
                         CircularDeserializer<CPD3::Data::SequenceName::Component> &archives,
                         CircularDeserializer<CPD3::Data::SequenceName::Component> &variables,
                         CircularDeserializer<CPD3::Data::SequenceName::Component> &flavors);

        virtual ~UnitDeserializer();

    protected:
        Data::SequenceName deserialize(QDataStream &stream) override;
    };

    StringDeserializer stations;
    StringDeserializer archives;
    StringDeserializer variables;
    StringDeserializer flavors;
    UnitDeserializer units;

    bool processingContinue(QDataStream &stream);

    void deserializeValues(Data::ErasureSink *sink,
                           QDataStream &stream,
                           bool includeModified,
                           int count,
                           double startTime,
                           double endTime,
                           int priority);

    void deserializeDeleted(Data::ErasureSink *sink,
                            QDataStream &stream,
                            bool includeModified,
                            int count,
                            double startTime,
                            double endTime,
                            int priority);

    bool execInner(QIODevice *input, Data::ErasureSink *sink);

public:
    DataUnpack();

    virtual ~DataUnpack();

    /**
     * Get a string describing the cause of the last failure.
     *
     * @return a description of the last failure
     */
    inline QString errorString() const
    { return error; }

    CPD3::ActionFeedback::Source feedback;

    /**
     * Run the unpacking on the given input device.
     *
     * @param input             the device to read from
     * @param sink              the output sink
     * @return                  true on success
     */
    bool exec(const IO::Access::Handle &input, Data::ErasureSink *sink);

    bool exec(std::unique_ptr<IO::Generic::Stream> &&input, Data::ErasureSink *sink);

    bool exec(std::unique_ptr<IO::Generic::Block> &&input, Data::ErasureSink *sink);

    /**
     * Request termination of the unpacking.
     */
    void signalTerminate();

protected:
    /**
     * Test if the unpacker has been terminated.
     *
     * @return  true if the unpacking is terminated
     */
    bool testTerminated();
};

}
}

#endif //CPD3_TRANSFERDATAFILE_HXX
