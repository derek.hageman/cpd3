/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QtEndian>
#include <QLoggingCategory>

#include "core/waitutils.hxx"
#include "core/threadpool.hxx"
#include "transfer/datafile.hxx"
#include "datacore/variant/serialization.hxx"
#include "io/drivers/qio.hxx"

using namespace CPD3::Data;


Q_LOGGING_CATEGORY(log_transfer_datafile, "cpd3.transfer.datafile", QtWarningMsg)

namespace CPD3 {
namespace Transfer {

/** @file transfer/package.hxx
 * The interface to handle packaging and unpacking of CPD3 data for transfer to remote
 * systems.
 */

static const int initialBufferSize = 1024 * 1024;

static const quint32 packageMagic = 0xC4D3DA4A;

namespace {
enum {
    /*
     * Data package contains modified times.
     */
            DataPackageType_WithModified_v1 = 0,

    /*
     * Data package without modified times.
     */
            DataPackageType_WithoutModified_v1,

    /*
     * Data package contains modified times, using the new serialization format.
     */
            DataPackageType_WithModified_v2,

    /*
     * Data package without modified times, using the new serialization format.
     */
            DataPackageType_WithoutModified_v2
};

/*
 * Base serialization:
 * <U8 count-1 or 0xFF for typed><double startTime><double endTime><I32 priority>
 *      (<U16 name>,[<double modified>],<Value>)x(count)
 */

enum {
    /*
     * Values continue at the previously set start time:
     * <double endTime><I32 priority><U16 count-1>
     *      (<U16 unit>,[<double modified>],<Value>)x(count)
     */
            Type_ValuesContinue = 0,

    /*
     * Erasure definition begins (no prior start time):
     * <double startTime><double endTime><I32 priority><U8 count-1>
     *      (<U16 unit>,[<double modified>])x(count)
     */
            Type_Erasure,

    /*
     * Erasure definitions continue at the previously set start time:
     * <double endTime><I32 priority><U16 count-1>
     *      (<U16 unit>,[<double modified>])x(count)
     */
            Type_ErasureContinue,

    /*
     * Station definitions follow:
     * <U16 count-1>(<U8 length>,<UTF8 characters>)x(count)
     */
            Type_Stations,

    /*
     * Archive definitions follow:
     * <U16 count-1>(<U8 length>,<UTF8 characters>)x(count)
     */
            Type_Archives,

    /*
     * Variable definitions follow:
     * <U16 count-1>(<U8 length>,<UTF8 characters>)x(count)
     */
            Type_Variables,

    /*
     * Flavor definitions follow:
     * <U16 count-1>(<U8 length>,<UTF8 characters>)x(count)
     */
            Type_Flavors,

    /*
     * Unit definitions follow:
     * <U16 count-1>(<U16 station>,<U16 archive>,<U16 variable>,<U16 flavors>)x(count)
     */
            Type_Units,
};
}

DataPack::DataPack(std::unique_ptr<IO::Generic::Stream> &&t,
                   bool includeModified,
                   bool useLegacySerialization) : target(std::move(t)),
                                                  includeModified(includeModified),
                                                  useLegacySerialization(useLegacySerialization),
                                                  writeAll(false),
                                                  ended(false),
                                                  processingActive(false),
                                                  terminated(false),
                                                  threadComplete(false),
                                                  units(stations, archives, variables, flavors)
{ }

DataPack::DataPack(QIODevice *device, bool includeModified, bool useLegacySerialization) : DataPack(
        IO::Access::qio(device)->stream(), includeModified,
        useLegacySerialization)
{
    Q_ASSERT(device != nullptr);
}

DataPack::~DataPack()
{
    signalTerminate();
    if (thread.joinable()) {
        thread.join();
    }
}

void DataPack::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    external.notify_all();
    internal.notify_all();
}

void DataPack::start()
{ thread = std::thread(&DataPack::run, this); }

bool DataPack::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return threadComplete;
}

bool DataPack::wait(double timeout)
{ return Threading::waitForTimeout(timeout, mutex, external, [this] { return threadComplete; }); }

void DataPack::stall(std::unique_lock<std::mutex> &lock)
{
    while (processingActive &&
            ((pendingValues.size() + pendingErasure.size()) > ErasureSink::stallThreshold)) {
        if (terminated)
            return;
        external.wait(lock);
    }
}

void DataPack::endData()
{ endStream(); }

void DataPack::endStream()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        ended = true;
    }
    internal.notify_all();
}

void DataPack::incomingValue(const ArchiveValue &value)
{
    bool signalStep = false;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!ended);
        pendingValues.emplace_back(value);
        signalStep = shouldWritePending();
    }
    if (signalStep) {
        internal.notify_all();
    }
}

void DataPack::incomingValue(ArchiveValue &&value)
{
    bool signalStep = false;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!ended);
        pendingValues.emplace_back(std::move(value));
        signalStep = shouldWritePending();
    }
    if (signalStep) {
        internal.notify_all();
    }
}

void DataPack::incomingData(const SequenceValue &value)
{
    bool signalStep = false;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!ended);
        pendingValues.emplace_back(value);
        signalStep = shouldWritePending();
    }
    if (signalStep) {
        internal.notify_all();
    }
}

void DataPack::incomingData(SequenceValue &&value)
{
    bool signalStep = false;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!ended);
        pendingValues.emplace_back(std::move(value));
        signalStep = shouldWritePending();
    }
    if (signalStep) {
        internal.notify_all();
    }
}

void DataPack::incomingErasure(const ArchiveErasure &erasure)
{
    bool signalStep = false;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!ended);
        pendingErasure.emplace_back(erasure);
        signalStep = shouldWritePending();
    }
    if (signalStep) {
        internal.notify_all();
    }
}

void DataPack::incomingErasure(ArchiveErasure &&erasure)
{
    bool signalStep = false;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!ended);
        pendingErasure.emplace_back(std::move(erasure));
        signalStep = shouldWritePending();
    }
    if (signalStep) {
        internal.notify_all();
    }
}

void DataPack::incomingValue(const ArchiveValue::Transfer &values)
{
    if (values.empty())
        return;
    bool signalStep = false;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!ended);
        Util::append(values, pendingValues);
        signalStep = shouldWritePending();
    }
    if (signalStep) {
        internal.notify_all();
    }
}

void DataPack::incomingValue(ArchiveValue::Transfer &&values)
{
    if (values.empty())
        return;
    bool signalStep = false;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!ended);
        Util::append(std::move(values), pendingValues);
        signalStep = shouldWritePending();
    }
    if (signalStep) {
        internal.notify_all();
    }
}

void DataPack::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    bool signalStep = false;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!ended);
        std::copy(values.begin(), values.end(), Util::back_emplacer(pendingValues));
        signalStep = shouldWritePending();
    }
    if (signalStep) {
        internal.notify_all();
    }
}

void DataPack::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    bool signalStep = false;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!ended);
        std::move(values.begin(), values.end(), Util::back_emplacer(pendingValues));
        signalStep = shouldWritePending();
    }
    if (signalStep) {
        internal.notify_all();
    }
}

void DataPack::incomingErasure(const ArchiveErasure::Transfer &erasures)
{
    if (erasures.empty())
        return;
    bool signalStep = false;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!ended);
        Util::append(erasures, pendingErasure);
        signalStep = shouldWritePending();
    }
    if (signalStep) {
        internal.notify_all();
    }
}

void DataPack::incomingErasure(ArchiveErasure::Transfer &&erasures)
{
    if (erasures.empty())
        return;
    bool signalStep = false;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);

        Q_ASSERT(!ended);
        Util::append(std::move(erasures), pendingErasure);
        signalStep = shouldWritePending();
    }
    if (signalStep) {
        internal.notify_all();
    }
}

void DataPack::flushData()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        writeAll = true;
    }
    internal.notify_all();
}

bool DataPack::shouldWritePending() const
{
    int pvs = pendingValues.size();
    int pds = pendingErasure.size();
    if (pvs == 0 && pds == 0)
        return false;
    return (pvs + pds) <= 0 || pvs + pds > initialBufferSize;
}

bool DataPack::needToFlushLookup() const
{
    return !stations.canAcceptNew() ||
            !archives.canAcceptNew() ||
            !variables.canAcceptNew() ||
            !flavors.canAcceptNew() ||
            !units.canAcceptNew();
}

void DataPack::run()
{
    auto qstream = IO::Generic::Backing::qioStream(std::move(target));
    if (!qstream->open(QIODevice::WriteOnly)) {
        {
            std::unique_lock<std::mutex> lock(mutex);
            threadComplete = true;
        }
        external.notify_all();
        finished();
        return;
    }

    bool firstStep = true;

    for (;;) {
        ArchiveValue::Transfer valuesToWrite;
        ArchiveErasure::Transfer erasureToWrite;
        bool packageEnded = false;
        {
            std::unique_lock<std::mutex> lock(mutex);
            for (;;) {
                if (terminated) {
                    threadComplete = true;
                    processingActive = false;
                    lock.unlock();
                    external.notify_all();
                    finished();
                    return;
                }
                if (!writeAll && !ended && !shouldWritePending()) {
                    if (processingActive) {
                        processingActive = false;
                        external.notify_all();
                    }
                    internal.wait(lock);
                    continue;
                }
                writeAll = false;
                processingActive = true;
                valuesToWrite = std::move(pendingValues);
                pendingValues.clear();
                erasureToWrite = std::move(pendingErasure);
                pendingErasure.clear();
                packageEnded = ended;
                break;
            }
        }

        if (valuesToWrite.empty() && erasureToWrite.empty()) {
            if (packageEnded) {
                qstream->close();
                qstream.reset();
                {
                    std::unique_lock<std::mutex> lock(mutex);
                    processingActive = false;
                    threadComplete = true;
                }
                external.notify_all();
                finished();
                return;
            }
            {
                std::unique_lock<std::mutex> lock(mutex);
                processingActive = false;
            }
            external.notify_all();
            continue;
        }

        QDataStream stream(qstream.get());
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);

        if (firstStep) {
            {
                QByteArray buffer(4, static_cast<char>(0));
                qToBigEndian<quint32>(packageMagic, reinterpret_cast<uchar *>(buffer.data()));
                stream.writeRawData(buffer.constData(), 4);
            }
            quint8 type = 0xFF;
            if (useLegacySerialization) {
                if (includeModified)
                    type = DataPackageType_WithModified_v1;
                else
                    type = DataPackageType_WithoutModified_v1;
            } else {
                if (includeModified)
                    type = DataPackageType_WithModified_v2;
                else
                    type = DataPackageType_WithoutModified_v2;
            }
            stream.writeRawData(reinterpret_cast<const char *>(&type), 1);
            firstStep = false;
        }

        auto nextValue = valuesToWrite.begin();
        auto nextErasure = erasureToWrite.begin();
        auto endValues = valuesToWrite.end();
        auto endErasure = erasureToWrite.end();

        double currentStart = FP::undefined();
        ValueWriteQueued queuedValues;
        DeletedWriteQueued queuedDeleted;
        queuedValues.emplace_back(currentStart, ValueWriteBreakdown());
        queuedDeleted.emplace_back();

        while (nextValue != endValues && nextErasure != endErasure) {
            if (needToFlushLookup())
                flushAllPending(stream, queuedValues, queuedDeleted);

            double valueStart = nextValue->getStart();
            double deletedStart = nextErasure->getStart();

            if (!FP::defined(currentStart)) {
                if (!FP::defined(valueStart)) {
                    Q_ASSERT(queuedValues.size() == 1);
                    Q_ASSERT(queuedDeleted.size() == 1);
                    Q_ASSERT(!FP::defined(queuedValues.front().first));
                    enqueueValue(queuedValues.front().second, std::move(*nextValue));
                    ++nextValue;
                } else if (!FP::defined(deletedStart)) {
                    Q_ASSERT(!FP::defined(currentStart));
                    Q_ASSERT(queuedValues.size() == 1);
                    Q_ASSERT(queuedDeleted.size() == 1);
                    Q_ASSERT(!FP::defined(queuedValues.front().first));
                    enqueueDeleted(queuedDeleted.front(), std::move(*nextErasure));
                    ++nextErasure;
                } else if (deletedStart < valueStart) {
                    currentStart = deletedStart;
                    queuedValues.emplace_back(currentStart, ValueWriteBreakdown());
                    queuedDeleted.emplace_back();

                    Q_ASSERT(!queuedDeleted.empty());
                    Q_ASSERT(queuedValues.back().first == currentStart);
                    enqueueDeleted(queuedDeleted.back(), std::move(*nextErasure));
                    ++nextErasure;
                } else {
                    currentStart = valueStart;
                    queuedValues.emplace_back(currentStart, ValueWriteBreakdown());
                    queuedDeleted.emplace_back();

                    Q_ASSERT(!queuedValues.empty());
                    Q_ASSERT(queuedValues.back().first == currentStart);
                    enqueueValue(queuedValues.back().second, std::move(*nextValue));
                    ++nextValue;
                }
                continue;
            }

            Q_ASSERT(FP::defined(currentStart));
            Q_ASSERT(FP::defined(valueStart));
            Q_ASSERT(FP::defined(deletedStart));
            Q_ASSERT(valueStart >= currentStart);
            Q_ASSERT(deletedStart >= currentStart);
            Q_ASSERT(queuedValues.size() == queuedDeleted.size());

            if (deletedStart < valueStart) {
                if (deletedStart != currentStart) {
                    currentStart = deletedStart;
                    queuedValues.emplace_back(currentStart, ValueWriteBreakdown());
                    queuedDeleted.emplace_back();
                }

                Q_ASSERT(!queuedDeleted.empty());
                Q_ASSERT(queuedValues.back().first == currentStart);
                enqueueDeleted(queuedDeleted.back(), std::move(*nextErasure));
                ++nextErasure;
            } else {
                if (valueStart != currentStart) {
                    currentStart = valueStart;
                    queuedValues.emplace_back(currentStart, ValueWriteBreakdown());
                    queuedDeleted.emplace_back();
                }

                Q_ASSERT(!queuedValues.empty());
                Q_ASSERT(queuedValues.back().first == currentStart);
                enqueueValue(queuedValues.back().second, std::move(*nextValue));
                ++nextValue;
            }
        }

        for (; nextValue != endValues; ++nextValue) {
            if (needToFlushLookup())
                flushAllPending(stream, queuedValues, queuedDeleted);

            Q_ASSERT(nextErasure == endErasure);
            Q_ASSERT(queuedValues.size() == queuedDeleted.size());

            double valueStart = nextValue->getStart();

            if (!FP::defined(currentStart)) {
                if (FP::defined(valueStart)) {
                    currentStart = valueStart;
                    queuedValues.emplace_back(currentStart, ValueWriteBreakdown());
                    queuedDeleted.emplace_back();
                }

                Q_ASSERT(!queuedValues.empty());
                enqueueValue(queuedValues.back().second, std::move(*nextValue));
                continue;
            }

            Q_ASSERT(valueStart >= currentStart);

            if (valueStart != currentStart) {
                currentStart = valueStart;
                queuedValues.emplace_back(currentStart, ValueWriteBreakdown());
                queuedDeleted.emplace_back();
            }

            Q_ASSERT(!queuedValues.empty());
            Q_ASSERT(queuedValues.back().first == currentStart);
            enqueueValue(queuedValues.back().second, std::move(*nextValue));
        }
        for (; nextErasure != endErasure; ++nextErasure) {
            if (needToFlushLookup())
                flushAllPending(stream, queuedValues, queuedDeleted);

            Q_ASSERT(nextValue == endValues);
            Q_ASSERT(queuedValues.size() == queuedDeleted.size());

            double deletedStart = nextErasure->getStart();

            if (!FP::defined(currentStart)) {
                if (FP::defined(deletedStart)) {
                    currentStart = deletedStart;
                    queuedValues.emplace_back(currentStart, ValueWriteBreakdown());
                    queuedDeleted.emplace_back();
                }

                Q_ASSERT(!queuedDeleted.empty());
                enqueueDeleted(queuedDeleted.back(), std::move(*nextErasure));
                continue;
            }

            Q_ASSERT(deletedStart >= currentStart);

            if (deletedStart != currentStart) {
                currentStart = deletedStart;
                queuedValues.emplace_back(currentStart, ValueWriteBreakdown());
                queuedDeleted.emplace_back();
            }

            Q_ASSERT(!queuedDeleted.empty());
            Q_ASSERT(queuedValues.back().first == currentStart);
            enqueueDeleted(queuedDeleted.back(), std::move(*nextErasure));
        }

        flushAllPending(stream, queuedValues, queuedDeleted);

        if (stream.status() != QDataStream::Ok) {
            qCWarning(log_transfer_datafile) << "Data writing failed:" << qstream->errorString();

            qstream->close();
            qstream.reset();
            {
                std::unique_lock<std::mutex> lock(mutex);
                processingActive = false;
                threadComplete = true;
            }
            external.notify_all();
            finished();
            return;
        }
    }
}

void DataPack::enqueueValue(ValueWriteBreakdown &queued, ArchiveValue &&add)
{
    auto id = units.lookup(add.getUnit());
    queued[DataPackageWriteKey(add)].emplace_back(id, std::move(add));
}

void DataPack::enqueueDeleted(DeletedWriteBreakdown &queued, ArchiveErasure &&add)
{
    auto id = units.lookup(add.getUnit());
    queued[DataPackageWriteKey(add)].emplace_back(id, std::move(add));
}

void DataPack::flushAllPending(QDataStream &stream,
                               ValueWriteQueued &values,
                               DeletedWriteQueued &deleted)
{
    Q_ASSERT(!values.empty());
    Q_ASSERT(values.size() == deleted.size());

    if (stations.hasPending()) {
        stream << static_cast<quint8>(0xFF) << static_cast<quint8>(Type_Stations);
        stations.flushPending(stream);
    }
    if (archives.hasPending()) {
        stream << static_cast<quint8>(0xFF) << static_cast<quint8>(Type_Archives);
        archives.flushPending(stream);
    }
    if (variables.hasPending()) {
        stream << static_cast<quint8>(0xFF) << static_cast<quint8>(Type_Variables);
        variables.flushPending(stream);
    }
    if (flavors.hasPending()) {
        stream << static_cast<quint8>(0xFF) << static_cast<quint8>(Type_Flavors);
        flavors.flushPending(stream);
    }
    if (units.hasPending()) {
        stream << static_cast<quint8>(0xFF) << static_cast<quint8>(Type_Units);
        units.flushPending(stream);
    }

    {
        auto valueTime = values.cbegin();
        auto deletedTime = deleted.cbegin();
        for (auto valueEnd = values.cend(); valueTime != valueEnd; ++valueTime, ++deletedTime) {
            flushTime(stream, valueTime->first, valueTime->second, *deletedTime);
        }
    }

    stations.resetReferenced();
    archives.resetReferenced();
    variables.resetReferenced();
    flavors.resetReferenced();
    units.resetReferenced();

    double finalTime = values.back().first;
    values.clear();
    deleted.clear();
    values.emplace_back(finalTime, ValueWriteBreakdown());
    deleted.emplace_back();
}

void DataPack::flushTime(QDataStream &stream,
                         double startTime,
                         const ValueWriteBreakdown &values,
                         const DeletedWriteBreakdown &deleted) const
{
    bool first = true;
    for (const auto &key : values) {
        std::size_t totalFragment = key.second.size();
        std::size_t offset = 0;

        if (first) {
            first = false;
            std::size_t n = std::min(totalFragment - offset, static_cast<std::size_t>(0xFF));
            Q_ASSERT((n - 1) >= 0 && (n - 1) < 0xFF);
            stream << static_cast<quint8>(n - 1) << startTime << key.first.end
                   << key.first.priority;

            for (auto add = key.second.cbegin() + offset, endAdd = add + n; add != endAdd; ++add) {
                stream << add->id;
                if (includeModified)
                    stream << add->modified;
                if (!useLegacySerialization) {
                    stream << add->value;
                } else {
                    Variant::Serialization::serializeLegacy(stream, add->value);
                }
            }
            offset += n;
        }

        while (offset < totalFragment) {
            std::size_t n = std::min(totalFragment - offset, static_cast<std::size_t>(0xFFFF + 1));
            Q_ASSERT((n - 1) >= 0 && (n - 1) <= 0xFFFF);
            stream << static_cast<quint8>(0xFF) << static_cast<quint8>(Type_ValuesContinue)
                   << key.first.end << key.first.priority << static_cast<quint16>(n - 1);

            for (auto add = key.second.cbegin() + offset, endAdd = add + n; add != endAdd; ++add) {
                stream << add->id;
                if (includeModified)
                    stream << add->modified;
                if (!useLegacySerialization) {
                    stream << add->value;
                } else {
                    Variant::Serialization::serializeLegacy(stream, add->value);
                }
            }
            offset += n;
        }
    }

    for (const auto &key : deleted) {
        std::size_t totalFragment = key.second.size();
        std::size_t offset = 0;

        if (first) {
            first = false;
            std::size_t n = std::min(totalFragment - offset, static_cast<std::size_t>(0xFF + 1));
            Q_ASSERT((n - 1) >= 0 && (n - 1) <= 0xFF);
            stream << static_cast<quint8>(0xFF) << static_cast<quint8>(Type_Erasure) << startTime
                   << key.first.end << key.first.priority << static_cast<quint8>(n - 1);

            for (auto add = key.second.cbegin() + offset, endAdd = add + n; add != endAdd; ++add) {
                stream << add->id;
                if (includeModified)
                    stream << add->modified;
            }
            offset += n;
        }

        while (offset < totalFragment) {
            std::size_t n = std::min(totalFragment - offset, static_cast<std::size_t>(0xFFFF + 1));
            Q_ASSERT((n - 1) >= 0 && (n - 1) <= 0xFFFF);
            stream << static_cast<quint8>(0xFF) << static_cast<quint8>(Type_ErasureContinue)
                   << key.first.end << key.first.priority << static_cast<quint16>(n - 1);

            for (auto add = key.second.cbegin() + offset, endAdd = add + n; add != endAdd; ++add) {
                stream << add->id;
                if (includeModified)
                    stream << add->modified;
            }
            offset += n;
        }
    }
}


DataPack::StringSerializer::StringSerializer() = default;

DataPack::StringSerializer::~StringSerializer() = default;

void DataPack::StringSerializer::serialize(QDataStream &stream, const SequenceName::Component &add)
{
    std::size_t size = add.size();
    if (size > 255) {
        qCWarning(log_transfer_datafile) << "Truncating long string:" << add;
        size = 255;
    }
    stream << static_cast<quint8>(size);
    stream.writeRawData(add.data(), static_cast<int>(size));
}


DataPack::UnitSerializer::UnitSerializer(CircularSerializer<SequenceName::Component> &stations,
                                         CircularSerializer<SequenceName::Component> &archives,
                                         CircularSerializer<SequenceName::Component> &variables,
                                         CircularSerializer<SequenceName::Component> &flavors)
        : stations(stations), archives(archives), variables(variables), flavors(flavors)
{ }

DataPack::UnitSerializer::~UnitSerializer() = default;

void DataPack::UnitSerializer::serialize(QDataStream &stream, const SequenceName &add)
{
    stream << stations.lookup(add.getStation());
    stream << archives.lookup(add.getArchive());
    stream << variables.lookup(add.getVariable());
    stream << flavors.lookup(add.getFlavorsString());
}

void DataPack::UnitSerializer::prepareLookup(const Data::SequenceName &add)
{
    stations.lookup(add.getStation());
    archives.lookup(add.getArchive());
    variables.lookup(add.getVariable());
    flavors.lookup(add.getFlavorsString());
}


DataUnpack::DataUnpack()
        : error(),
          mutex(),
          cond(),
          terminated(false),
          stations(),
          archives(),
          variables(),
          flavors(),
          units(stations, archives, variables, flavors)
{ }

DataUnpack::~DataUnpack() = default;

void DataUnpack::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    cond.notify_all();
}

bool DataUnpack::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

bool DataUnpack::processingContinue(QDataStream &stream)
{
    switch (stream.status()) {
    case QDataStream::Ok:
        break;
    case QDataStream::ReadCorruptData:
        qCDebug(log_transfer_datafile) << "Stream read corrupted data";
        error = QObject::tr("Stream corruption");
        return false;
    case QDataStream::ReadPastEnd:
        qCDebug(log_transfer_datafile) << "Stream read past the end of data";
        error = QObject::tr("Premature data end");
        return false;
    default:
        qCDebug(log_transfer_datafile) << "Stream error:" << stream.device()->errorString();
        error = QObject::tr("Read error: %1").arg(stream.device()->errorString());
        return false;
    }

    std::lock_guard<std::mutex> lock(mutex);
    return !terminated;
}

bool DataUnpack::execInner(QIODevice *input, ErasureSink *sink)
{
    SinkEndGuard<ErasureSink> sinkGuard(sink);

    stations.clear();
    archives.clear();
    variables.clear();
    flavors.clear();
    units.clear();

    feedback.emitStage(QObject::tr("Extracting data"),
                       QObject::tr("The system is unpacking the data file"), !input->isSequential());

    QDataStream stream(input);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setVersion(QDataStream::Qt_4_5);

    bool includeModified = false;
    {
        QByteArray buffer(4, (char) 0);
        if (stream.readRawData(buffer.data(), 4) != 4) {
            error = QObject::tr("Error reading magic number: %1").arg(input->errorString());
            qCDebug(log_transfer_datafile) << "Error reading magic number";
            feedback.emitFailure();
            return false;
        }
        quint32 check = qFromBigEndian<quint32>((const uchar *) buffer.constData());
        if (check != packageMagic) {
            error = QObject::tr("Magic number does not match");
            qCDebug(log_transfer_datafile) << "Magic number (" << hex << check
                                           << ") does not match the expected value (" << hex
                                           << packageMagic << ")";
            feedback.emitFailure();
            return false;
        }

        quint8 type = 0xFF;
        if (stream.readRawData((char *) &type, 1) != 1) {
            error = QObject::tr("Error reading file type: %1").arg(input->errorString());
            qCDebug(log_transfer_datafile) << "Error reading type";
            feedback.emitFailure();
            return false;
        }
        switch (type) {
        case DataPackageType_WithModified_v1:
        case DataPackageType_WithModified_v2:
            includeModified = true;
            break;
        case DataPackageType_WithoutModified_v1:
        case DataPackageType_WithoutModified_v2:
            includeModified = false;
            break;
        default:
            error = QObject::tr("Invalid file type");
            qCDebug(log_transfer_datafile) << "Unrecognized file type:" << type;
            feedback.emitFailure();
            return false;
        }
    }

    double startTime = FP::undefined();
    for (;;) {
        if (!input->isSequential()) {
            qint64 total = input->size();
            qint64 completed = input->pos();
            if (total > 0) {
                feedback.emitProgressFraction((double) completed / (double) total);
            }
        }
        if (!processingContinue(stream))
            return false;

        quint8 dataCount = 0;
        stream >> dataCount;

        /* After a read, so file ends are detected correctly */
        if (input->atEnd())
            break;

        if (dataCount != 0xFF) {
            double endTime;
            qint32 priority;
            double priorStart = startTime;

            stream >> startTime >> endTime >> priority;

            if (FP::defined(priorStart) && (!FP::defined(startTime) || startTime < priorStart)) {
                error = QObject::tr("Start time out of order");
                qCDebug(log_transfer_datafile) << "Start time" << Time::toISO8601(startTime)
                                               << "is before the previous one"
                                               << Time::toISO8601(priorStart);
                feedback.emitFailure();
                return false;
            }

            deserializeValues(sink, stream, includeModified, static_cast<int>(dataCount) + 1,
                              startTime, endTime, priority);
            continue;
        }

        quint8 type = 0;
        stream >> type;

        switch (type) {
        case Type_ValuesContinue: {
            double endTime = 0;
            qint32 priority = 0;
            quint16 count = 0;
            stream >> endTime >> priority >> count;
            deserializeValues(sink, stream, includeModified, static_cast<int>(count) + 1, startTime,
                              endTime, priority);
            break;
        }

        case Type_Erasure: {
            double endTime = 0;
            qint32 priority = 0;
            quint8 count = 0;
            double priorStart = startTime;

            stream >> startTime >> endTime >> priority >> count;

            if (FP::defined(priorStart) && (!FP::defined(startTime) || startTime < priorStart)) {
                error = QObject::tr("Start time out of order");
                qCDebug(log_transfer_datafile) << "Start time" << Time::toISO8601(startTime)
                                               << "is before the previous one"
                                               << Time::toISO8601(priorStart);
                feedback.emitFailure();
                return false;
            }

            deserializeDeleted(sink, stream, includeModified, static_cast<int>(count) + 1,
                               startTime, endTime, priority);
            break;
        }

        case Type_ErasureContinue: {
            double endTime = 0;
            qint32 priority = 0;
            quint16 count = 0;
            stream >> endTime >> priority >> count;
            deserializeDeleted(sink, stream, includeModified, static_cast<int>(count) + 1,
                               startTime, endTime, priority);
            break;
        }

        case Type_Stations:
            stations.readPending(stream);
            break;

        case Type_Archives:
            archives.readPending(stream);
            break;

        case Type_Variables:
            variables.readPending(stream);
            break;

        case Type_Flavors:
            flavors.readPending(stream);
            break;

        case Type_Units:
            units.readPending(stream);
            break;

        default:
            qCDebug(log_transfer_datafile) << "Unrecognized definition type: " << type;
            error = QObject::tr("Unrecognized definition type");
            feedback.emitFailure();
            return false;
        }
    }

    sinkGuard.end();

    if (!input->isSequential()) {
        feedback.emitProgressFraction(1.0);
    }

    return true;
}

bool DataUnpack::exec(const IO::Access::Handle &input, Data::ErasureSink *sink)
{
    auto block = input->block();
    if (block)
        return exec(std::move(block), sink);
    auto stream = input->stream();
    if (!stream) {
        qCDebug(log_transfer_datafile) << "No unpacking input possible";
        error = QObject::tr("Input unavailable");
        return false;
    }
    return exec(std::move(stream), sink);
}

bool DataUnpack::exec(std::unique_ptr<IO::Generic::Stream> &&input, Data::ErasureSink *sink)
{
    auto qio = IO::Generic::Backing::qioStream(std::move(input));
    if (!qio->open(QIODevice::ReadOnly)) {
        qCDebug(log_transfer_datafile) << "Error opening device";
        error = QObject::tr("Device open failure");
        return false;
    }
    return execInner(qio.get(), sink);
}

bool DataUnpack::exec(std::unique_ptr<IO::Generic::Block> &&input, Data::ErasureSink *sink)
{
    auto qio = IO::Generic::Backing::qioBlock(std::move(input));
    if (!qio->open(QIODevice::ReadOnly)) {
        qCDebug(log_transfer_datafile) << "Error opening device";
        error = QObject::tr("Device open failure");
        return false;
    }
    return execInner(qio.get(), sink);
}

void DataUnpack::deserializeValues(ErasureSink *sink,
                                   QDataStream &stream,
                                   bool includeModified,
                                   int count,
                                   double startTime,
                                   double endTime,
                                   int priority)
{
    for (int i = 0; i < count; i++) {
        quint16 unitID = 0;
        stream >> unitID;
        double modified = FP::undefined();
        if (includeModified)
            stream >> modified;
        Variant::Root dataValue = Variant::Root::deserialize(stream);

        const auto &unit = units.lookup(unitID);
        if (!unit.isValid()) {
            qCDebug(log_transfer_datafile) << "Invalid value unit" << unitID;
            continue;
        }

        sink->emplaceValue(SequenceIdentity(unit, startTime, endTime, priority),
                           std::move(dataValue), modified);
    }
}

void DataUnpack::deserializeDeleted(ErasureSink *sink, QDataStream &stream,
                                    bool includeModified,
                                    int count,
                                    double startTime,
                                    double endTime,
                                    int priority)
{
    for (int i = 0; i < count; i++) {
        quint16 unitID = 0;
        stream >> unitID;
        double modified = FP::undefined();
        if (includeModified)
            stream >> modified;

        const auto &unit = units.lookup(unitID);
        if (!unit.isValid()) {
            qCDebug(log_transfer_datafile) << "Invalid deleted unit" << unitID;
            continue;
        }

        sink->emplaceErasure(SequenceIdentity(unit, startTime, endTime, priority), modified);
    }
}


DataUnpack::StringDeserializer::StringDeserializer() = default;

DataUnpack::StringDeserializer::~StringDeserializer() = default;

SequenceName::Component DataUnpack::StringDeserializer::deserialize(QDataStream &stream)
{
    quint8 length = 0;
    stream >> length;
    if (length == 0)
        return {};
    Util::ByteArray buffer;
    buffer.resize(length);
    if (stream.readRawData(buffer.data<char *>(), length) != static_cast<int>(buffer.size()))
        return {};
    return SequenceName::Component(buffer.data<char *>(), length);
}


DataUnpack::UnitDeserializer::UnitDeserializer(DataUnpack::CircularDeserializer<
        SequenceName::Component> &stations,
                                               DataUnpack::CircularDeserializer<
                                                       SequenceName::Component> &archives,
                                               DataUnpack::CircularDeserializer<
                                                       SequenceName::Component> &variables,
                                               DataUnpack::CircularDeserializer<
                                                       SequenceName::Component> &flavors)
        : stations(stations), archives(archives), variables(variables), flavors(flavors)
{ }

DataUnpack::UnitDeserializer::~UnitDeserializer() = default;

Data::SequenceName DataUnpack::UnitDeserializer::deserialize(QDataStream &stream)
{
    quint16 station = 0;
    quint16 archive = 0;
    quint16 variable = 0;
    quint16 flavor = 0;
    stream >> station >> archive >> variable >> flavor;

    SequenceName u(stations.lookup(station), archives.lookup(archive), variables.lookup(variable));
    u.setFlavorsString(flavors.lookup(flavor));
    return u;
}


}
}

