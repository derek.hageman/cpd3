/*
 * Copyright (c) 2020 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <sys/types.h>
#include <sys/stat.h>

#endif

#include <QLoggingCategory>
#include <QUrl>

#include "upload.hxx"
#include "ftp.hxx"
#include "io/process.hxx"
#include "algorithms/cryptography.hxx"


Q_LOGGING_CATEGORY(log_transfer_upload, "cpd3.transfer.upload", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace Transfer {


FileUploader::FileUploader(const Variant::Read &config) : config(config), mutex(), terminated(false)
{
    this->config.detachFromRoot();
}

FileUploader::~FileUploader()
{
    terminateRequested.disconnect();
    smtpSend.reset();
    urlContext.reset();
}

bool FileUploader::invokeCommand(const QString &program,
                                 const QStringList &arguments,
                                 double timeout)
{
    qCDebug(log_transfer_upload) << "Invoking:" << program << arguments;

    auto proc = IO::Process::Spawn(program, arguments).forward().create();
    if (!proc)
        return false;

    IO::Process::Instance::ExitMonitor exitOk(*proc);
    exitOk.connectTerminate(terminateRequested);
    if (testTerminated())
        return false;

    if (!proc->start() || !proc->wait(timeout))
        return false;
    return exitOk.success();
}

bool FileUploader::executeActions(const Variant::Read &settings)
{
    switch (settings.getType()) {
    case Variant::Type::String: {
        QStringList arguments(applySubstitutions(settings.toQString()).split(QRegExp("\\s+"),
                                                                             QString::SkipEmptyParts));
        if (arguments.isEmpty()) {
            qCWarning(log_transfer_upload) << "Empty command as the only action";
            return true;
        }
        QString program(arguments.takeFirst());

        feedback.emitStage(QObject::tr("Execute External Program"),
                           QObject::tr("An external program is currently being executed."), false);
        feedback.emitState(program);
        if (!invokeCommand(program, arguments)) {
            feedback.emitFailure();
            return false;
        }
        break;
    }

    case Variant::Type::Array: {
        auto children = settings.toArray();
        if (children.empty())
            return true;

        feedback.emitStage(QObject::tr("Execute External Programs"),
                           QObject::tr("An external program is currently being executed."), false);

        int nCompleted = 0;
        for (auto child: children) {
            feedback.emitState(
                    QObject::tr("%1 of %2", "program state").arg(nCompleted + 1, children.size()));

            QString program(applySubstitutions(child.hash("Program").toQString()));
            if (program.isEmpty()) {
                qCWarning(log_transfer_upload) << "Empty command in action list";
                continue;
            }

            QStringList arguments;
            for (auto add: child.hash("Arguments").toArray()) {
                arguments.append(applySubstitutions(add.toQString()));
            }

            feedback.emitState(program);
            bool status = invokeCommand(program, arguments);
            if (!status && !child.hash("AllowFailure").toBool()) {
                qCDebug(log_transfer_upload) << "Execution of" << program << "failed";
                feedback.emitFailure();
                return false;
            }
        }

        break;
    }

    case Variant::Type::Hash: {
        QString program(applySubstitutions(settings["Program"].toQString()));
        if (program.isEmpty()) {
            qCWarning(log_transfer_upload) << "Empty command in action list";
            return true;
        }

        feedback.emitStage(QObject::tr("Execute External Program"),
                           QObject::tr("An external program is currently being executed."), false);

        QStringList arguments;
        for (auto add: settings["Arguments"].toArray()) {
            arguments.append(applySubstitutions(add.toQString()));
        }

        feedback.emitState(program);
        bool status = invokeCommand(program, arguments);
        if (!status && !settings["AllowFailure"].toBool()) {
            qCDebug(log_transfer_upload) << "Execution of" << program << "failed";
            feedback.emitFailure();
            return false;
        }
    }

    default:
        break;
    }

    return true;
}

static QStringList toReceiverList(const Variant::Read &targets)
{
    switch (targets.getType()) {
    default:
        return QStringList(targets.toQString());
    case Variant::Type::Array: {
        QStringList result;
        for (auto add: targets.toArray()) {
            result.append(add.toQString());
        }
        return result;
    }
    }
    return QStringList();
}

bool FileUploader::configureEmail(const Variant::Read &settings, EmailBuilder &builder)
{
    if (settings["PlainText"].toBool()) {
        builder.setEscapeMode(EmailBuilder::Escape_UTF8);
        builder.setBody(applySubstitutions(settings["Body"].toQString()));
    } else {
        builder.setEscapeMode(EmailBuilder::Escape_HTML);
        builder.setBody(applySubstitutions(settings["Body"].toQString()), "text/html");
    }

    builder.setSubject(applySubstitutions(settings["Subject"].toQString()));

    for (const auto &e: toReceiverList(settings["To"])) {
        builder.addTo(applySubstitutions(e));
    }
    for (const auto &e: toReceiverList(settings["CC"])) {
        builder.addCC(applySubstitutions(e));
    }
    for (const auto &e: toReceiverList(settings["BCC"])) {
        builder.addBCC(applySubstitutions(e));
    }

    return true;
}

bool FileUploader::attachEmailFile(const Variant::Read &settings,
                                   EmailBuilder &builder,
                                   const QByteArray &data,
                                   const QString &attachmentName)
{

    QString mimeType(applySubstitutions(settings["MIMEType"].toQString()));
    if (mimeType.isEmpty())
        mimeType = getMIMEType(attachmentName);
    builder.addAttachment(data, attachmentName, mimeType,
                          applySubstitutions(settings["ContentID"].toQString()));

    return true;
}

bool FileUploader::sendEmail(const Variant::Read &settings,
                             const QByteArray &data,
                             const QString &attachmentName)
{
    EmailBuilder builder;
    if (!configureEmail(settings, builder))
        return false;
    if (!attachEmailFile(settings, builder, data, attachmentName))
        return false;
    builder.queue(smtpSend.get());
    return true;
}

QString FileUploader::getMIMEType(const QString &fileName) const
{
    if (fileName.endsWith(".png", Qt::CaseInsensitive))
        return "image/png";
    else if (fileName.endsWith(".jpg", Qt::CaseInsensitive))
        return "image/jpeg";
    else if (fileName.endsWith(".jpeg", Qt::CaseInsensitive))
        return "image/jpeg";
    else if (fileName.endsWith(".gz", Qt::CaseInsensitive))
        return "application/x-compressed";
    return "application/octet-stream";
}

QString FileUploader::getPassword(const Data::Variant::Read &settings, const QString &def)
{
    QString password(settings["Password"].toQString());
    if (!password.isEmpty())
        return applySubstitutions(password);
    QString fileName(applySubstitutions(settings["PasswordFile"].toQString()));
    if (!fileName.isEmpty() && QFile::exists(fileName)) {
        QFile file(fileName);
        if (file.open(QIODevice::ReadOnly)) {
            password = QString::fromUtf8(file.readAll());
            file.close();
            return applySubstitutions(password);
        } else {
            qCDebug(log_transfer_upload) << "Failed to open password file" << fileName << ":"
                                         << file.errorString();
        }
    }
    return def;
}

bool FileUploader::uploadFTP(const Variant::Read &settings, UploadInput &source)
{
    QString targetHost(settings["Hostname"].toQString());
    targetHost = applySubstitutions(targetHost);
    if (targetHost.isEmpty()) {
        qCWarning(log_transfer_upload) << "No host name specified";
        return false;
    }

    quint16 port = 21;
    {
        qint64 check = settings["Port"].toInt64();
        if (INTEGER::defined(check) && check > 0 && check < 65536)
            port = (quint16) check;
    }

    QString username("anonymous");
    if (settings["Username"].exists())
        username = settings["Username"].toQString();
    username = applySubstitutions(username);

    QString password(getPassword(settings, "cpd3"));

    QString targetFile;
    if (settings["Target"].exists())
        targetFile = settings["Target"].toQString();
    targetFile = applySubstitutions(targetFile);
    if (targetFile.isEmpty()) {
        if (auto file = dynamic_cast<IO::File::Backing *>(source.source.get())) {
            targetFile = QFileInfo(QString::fromStdString(file->filename())).fileName();
        }
    }

    feedback.emitStage(QObject::tr("Upload File"), QObject::tr(
            "The upload of the file to \"%3:%4\" is currently in progress.").arg(targetHost,
                                                                                 targetFile), true);

    FTPConnection
            ftp(targetHost.toStdString(), port, username.toStdString(), password.toStdString());
    Threading::Receiver rx;
    ftp.configure(settings);
    terminateRequested.connect(rx, std::bind(&FTPConnection::signalTerminate, &ftp));
    ftp.progress.connect(rx, [this](std::uint_fast64_t complete, std::uint_fast64_t total) {
        if (total <= 0)
            return;
        feedback.emitProgressFraction(static_cast<double>(complete) / static_cast<double>(total));
    });

    feedback.emitState(QObject::tr("Connecting"));
    if (!ftp.connectToServer()) {
        qCDebug(log_transfer_upload) << "Initial FTP connection failed to" << targetHost
                                     << ", aborting";
        feedback.emitFailure();
        return false;
    }
    if (testTerminated())
        return false;

    if (!settings["CreateDirectory"].exists() || settings["CreateDirectory"].toBool()) {
        QStringList directories(targetFile.split('/'));
        QString prefix;
        if (!directories.isEmpty() && directories.first().isEmpty()) {
            prefix = "/";
            directories.removeFirst();
        }
        if (directories.size() > 1) {
            feedback.emitState(QObject::tr("Creating directory"));
            for (QList<QString>::const_iterator dir = directories.constBegin(),
                    end = directories.constEnd() - 1; dir != end; ++dir) {
                prefix.append(*dir);

                qCDebug(log_transfer_upload) << "Creating directory" << prefix;
                ftp.mkdir(prefix.toStdString());

                prefix.append('/');

                if (testTerminated())
                    return false;
            }
        }
    }

    feedback.emitState(QObject::tr("Transferring"));

    if (source.source) {
        if (!ftp.put(targetFile.toStdString(), *source.source)) {
            qCDebug(log_transfer_upload) << "FTP upload failed:" << ftp.errorString();
            feedback.emitFailure();
            return false;
        }
    } else {
        if (!source.stream) {
            qCDebug(log_transfer_upload) << "No available file source";
            feedback.emitFailure();
            return false;
        }
        if (!ftp.put(targetFile.toStdString(), std::move(source.stream))) {
            qCDebug(log_transfer_upload) << "FTP upload failed:" << ftp.errorString();
            feedback.emitFailure();
            return false;
        }
    }
    if (testTerminated())
        return false;

    feedback.emitState(QObject::tr("Disconnecting"));
    ftp.disconnectFromServer();

    feedback.emitState(QString());
    return true;
}

bool FileUploader::uploadURL(const Variant::Read &settings, UploadInput &source)
{
    if (!urlContext)
        urlContext = std::make_shared<IO::URL::Context>();

    QUrl url(QUrl::fromUserInput(applySubstitutions(settings["Target"].toQString())));

    auto password = getPassword(settings);
    if (!password.isEmpty())
        url.setPassword(password);
    auto username = applySubstitutions(settings["Username"].toQString());
    if (!username.isEmpty())
        url.setUserName(password);

    std::shared_ptr<IO::URL::Backing> request;
    if (settings["PUT"].toBool()) {
        if (source.source) {
            request = std::static_pointer_cast<IO::URL::Backing>(
                    IO::Access::urlPUT(url, source.source, urlContext));
        } else {
            request = std::static_pointer_cast<IO::URL::Backing>(
                    IO::Access::urlPUT(url, std::move(source.stream), urlContext));
        }
    } else {
        if (source.source) {
            request = std::static_pointer_cast<IO::URL::Backing>(
                    IO::Access::urlPOST(url, source.source, urlContext));
        } else {
            request = std::static_pointer_cast<IO::URL::Backing>(
                    IO::Access::urlPOST(url, std::move(source.stream), urlContext));
        }
    }

    if (!request) {
        qCDebug(log_transfer_upload) << "Unable to create URL request";
        feedback.emitFailure();
        return false;
    }

    if (settings["SSL"].exists()) {
        QSslConfiguration ssl;
        Algorithms::Cryptography::configureSSL(ssl, settings["SSL"]);
        request->tls(ssl);
    }

    for (auto header: settings["Headers"].toHash()) {
        request->header(applySubstitutions(QString::fromStdString(header.first)).toUtf8(),
                        applySubstitutions(header.second.toQString()).toUtf8());
    }

    feedback.emitStage(QObject::tr("Upload File"), QObject::tr(
            "The upload of the file to \"%1\" is currently in progress.").arg(
            url.toString(QUrl::RemoveUserInfo)), true);

    bool ended = false;
    std::condition_variable cv;

    auto reply = request->stream();
    if (!reply) {
        qCDebug(log_transfer_upload) << "Unable start URL upload";
        feedback.emitFailure();
        return false;
    }

    Threading::Receiver rx;
    terminateRequested.connect(rx, std::bind(&IO::URL::Stream::abort,
                                             static_cast<IO::URL::Stream *>(reply.get())));
    if (testTerminated())
        return false;

    {
        double timeout = config["Timeout"].toDouble();
        if (FP::defined(timeout) && timeout > 0) {
            static_cast<IO::URL::Stream *>(reply.get())->startTimeout(timeout);
        }
    }

    reply->ended.connect([this, &ended, &cv] {
        {
            std::lock_guard<std::mutex> lock(mutex);
            ended = true;
        }
        cv.notify_all();
    });
    reply->start();
    {
        std::unique_lock<std::mutex> lock(mutex);
        cv.wait(lock, [&ended] { return ended; });
    }

    if (static_cast<IO::URL::Stream *>(reply.get())->responseError()) {
        qCDebug(log_transfer_upload) << "Error during URL operation";
        feedback.emitFailure();
        return false;
    }

    reply.reset();
    feedback.emitState(QString());
    return true;
}

void FileUploader::streamCopy(std::unique_ptr<IO::Generic::Stream> &&source,
                              IO::Generic::Writable &target)
{
    std::condition_variable cv;
    bool ended = false;
    Threading::Receiver rx;
    source->read.connect(rx, [&](const Util::ByteArray &data) { target.write(data); });
    source->ended.connect(rx, [this, &cv, &ended] {
        {
            std::lock_guard<std::mutex> lock(mutex);
            ended = true;
        }
        cv.notify_all();
    });
    terminateRequested.connect(rx, [this, &cv, &ended] {
        {
            std::lock_guard<std::mutex> lock(mutex);
            ended = true;
        }
        cv.notify_all();
    });

    source->start();
    {
        std::unique_lock<std::mutex> lock(mutex);
        cv.wait(lock, [&ended] { return ended; });
    }

    source.reset();
}

std::shared_ptr<IO::File::Backing> FileUploader::toTemporaryFile(std::unique_ptr<
        IO::Generic::Stream> &&source)
{
    auto outputFile = IO::Access::temporaryFile();
    if (!outputFile) {
        qCDebug(log_transfer_upload) << "Failed to open temporary file";
        return {};
    }
    auto target = outputFile->block();
    if (!target) {
        qCDebug(log_transfer_upload) << "Failed to open temporary target";
        return {};
    }

    feedback.emitStage(QObject::tr("Writing Temporary File"), QObject::tr(
            "The data are being written to a temporary file for external use."), false);

    streamCopy(std::move(source), *target);
    target.reset();
    if (testTerminated())
        return {};
    return outputFile;
}

bool FileUploader::uploadCommand(const Variant::Read &settings, UploadInput &source)
{
    auto commandFile = std::dynamic_pointer_cast<IO::File::Backing>(source.source);
    bool isTemporary = false;
    if (!commandFile) {
        if (source.stream) {
            commandFile = toTemporaryFile(std::move(source.stream));
            isTemporary = commandFile.get() != nullptr;
        }
        if (!commandFile) {
            if (!source.source) {
                qCDebug(log_transfer_upload) << "No available file source";
                feedback.emitFailure();
                return false;
            }
            auto stream = source.source->stream();
            if (!stream) {
                qCDebug(log_transfer_upload) << "Unable to create source stream";
                feedback.emitFailure();
                return false;
            }
            commandFile = toTemporaryFile(std::move(source.stream));
            isTemporary = commandFile.get() != nullptr;
        }
    }
    if (!commandFile) {
        qCDebug(log_transfer_upload) << "No local file for command execution";
        feedback.emitFailure();
        return false;
    }

    /* Make sure we try to open it, so automounts, etc get activated */
    auto block = commandFile->block();
    if (!block) {
        qCDebug(log_transfer_upload) << "Unable to read file for command execution";
        feedback.emitFailure();
        return false;
    }

    if (isTemporary) {
        pushTemporaryOutput(QFileInfo(QString::fromStdString(commandFile->filename())));
        bool status = executeActions(config["Actions"]);
        popTemporaryOutput();
        return status;
    } else {
        pushInputFile(QFileInfo(QString::fromStdString(commandFile->filename())));
        bool status = executeActions(config["Actions"]);
        popInputFile();
        return status;
    }
}

bool FileUploader::uploadSFTP(const Variant::Read &settings, UploadInput &source)
{
    QString targetHost(settings["Hostname"].toQString());
    targetHost = applySubstitutions(targetHost);
    if (targetHost.isEmpty()) {
        qCWarning(log_transfer_upload) << "No host name specified";
        return false;
    }

    quint16 port = 22;
    {
        qint64 check = settings["Port"].toInt64();
        if (INTEGER::defined(check) && check > 0 && check < 65536)
            port = (quint16) check;
    }

    QString username("anonymous");
    if (settings["Username"].exists())
        username = settings["Username"].toQString();
    username = applySubstitutions(username);

    QString password(getPassword(settings, {}));

    QString targetFile;
    if (settings["Target"].exists())
        targetFile = settings["Target"].toQString();
    targetFile = applySubstitutions(targetFile);
    if (targetFile.isEmpty()) {
        if (auto file = dynamic_cast<IO::File::Backing *>(source.source.get())) {
            targetFile = QFileInfo(QString::fromStdString(file->filename())).fileName();
        }
    }

    double timeout = settings["Timeout"].toDouble();

    feedback.emitStage(QObject::tr("Upload File"), QObject::tr(
            "The upload of the file to \"%3:%4\" is currently in progress.").arg(targetHost,
                                                                                 targetFile), true);

    auto commandFile = std::dynamic_pointer_cast<IO::File::Backing>(source.source);
    bool isTemporary = false;
    if (!commandFile) {
        if (source.stream) {
            commandFile = toTemporaryFile(std::move(source.stream));
            isTemporary = commandFile.get() != nullptr;
        }
        if (!commandFile) {
            if (!source.source) {
                qCDebug(log_transfer_upload) << "No available file source";
                feedback.emitFailure();
                return false;
            }
            auto stream = source.source->stream();
            if (!stream) {
                qCDebug(log_transfer_upload) << "Unable to create source stream";
                feedback.emitFailure();
                return false;
            }
            commandFile = toTemporaryFile(std::move(source.stream));
            isTemporary = commandFile.get() != nullptr;
        }
    }
    if (!commandFile) {
        qCDebug(log_transfer_upload) << "No local file for command execution";
        feedback.emitFailure();
        return false;
    }

    /* Make sure we try to open it, so automounts, etc get activated */
    auto block = commandFile->block();
    if (!block) {
        qCDebug(log_transfer_upload) << "Unable to read file for command execution";
        feedback.emitFailure();
        return false;
    }

    QString program = "sftp";
    QStringList
            arguments{"-o", "User " + username, "-o", "Port " + QString::number(port), targetHost};

    std::shared_ptr<IO::File::Backing> passwordFile;

    if (!password.isEmpty()) {
        passwordFile = IO::Access::temporaryFile();
        if (!passwordFile) {
            qCDebug(log_transfer_upload) << "Failed to open temporary password file";
            return {};
        }
        {
            auto passwordTarget = passwordFile->block();
            if (!passwordTarget) {
                qCDebug(log_transfer_upload) << "Failed to open temporary password target";
                return {};
            }
            passwordTarget->write(password);
        }

        arguments =
                QStringList{"-f" + QString::fromStdString(passwordFile->filename()), program, "-o",
                            "BatchMode no"} + arguments;
        program = "sshpass";
    } else {
        arguments = QStringList{"-o", "BatchMode yes", "-b", "-",} + arguments;
    }

    qCDebug(log_transfer_upload) << "Invoking:" << program << arguments;

    auto spawn = IO::Process::Spawn(program, arguments).forward();
    spawn.inputStream = IO::Process::Spawn::StreamMode::Capture;
    auto proc = spawn.create();
    if (!proc) {
        qCDebug(log_transfer_upload) << "Error creating process";
        feedback.emitFailure();
        return false;
    }

    IO::Process::Instance::ExitMonitor exitOk(*proc);
    exitOk.connectTerminate(terminateRequested);
    if (testTerminated()) {
        feedback.emitFailure();
        return false;
    }
    if (!proc->start()) {
        qCDebug(log_transfer_upload) << "Error starting process";
        feedback.emitFailure();
        return false;
    }

    {
        auto commandStream = proc->inputStream();
        if (!commandStream) {
            qCDebug(log_transfer_upload) << "Error creating process input stream";
            feedback.emitFailure();
            return false;
        }

        commandStream->write(QString("put ") +
                                     QString::fromStdString(commandFile->filename()) +
                                     " " +
                                     targetFile +
                                     "\n");
        commandStream->write("quit\n");
        commandStream->close();
    }

    if (!proc->wait(timeout)) {
        qCDebug(log_transfer_upload) << "Error waiting for process";
        feedback.emitFailure();
        return false;
    }

    if (!exitOk.success()) {
        qCDebug(log_transfer_upload) << "Process execution error";
        feedback.emitFailure();
        return false;
    }

    feedback.emitState(QString());
    return true;
}

static bool prepareLocalTarget(const Variant::Read &settings, const QString &targetFile)
{
    QFileInfo info(targetFile);
    /* Automounting workaround, have to try and access the directory for the
    * automount to trigger.  Otherwise we could try and create a directory within the
    * automount point, which we don't have permission to do. */
    info.dir().exists();
    info.exists();

    if (!settings["CreateDirectory"].exists() || settings["CreateDirectory"].toBool()) {
        /* Bleh, doesn't look like there's a reasonable way to tell Qt to actually
         * make writable directories... */
#ifdef Q_OS_UNIX
        mode_t mask = ::umask(S_IWGRP | S_IWOTH);
        mode_t target = mask;
        target &= ~(S_IWUSR | S_IRUSR | S_IXUSR);
        ::umask(target);
#endif

        info.dir().mkpath(".");
        QFile::Permissions perm = QFileInfo(info.dir().absolutePath()).permissions();
        perm |= QFileDevice::ReadOwner;
        perm |= QFileDevice::WriteOwner;

#ifdef Q_OS_UNIX
        ::umask(mask);
        perm |= QFileDevice::ExeOwner;
#endif

        QFileInfo(info.dir().absolutePath()).permission(perm);
    }
    return true;
}

bool FileUploader::exec()
{
    if (!executeActions(config["Before"]))
        return false;

    const auto &transferType = config["Type"].toString();

    if (Util::equal_insensitive(transferType, "external", "command")) {
        for (auto &source: inputs) {
            if (!uploadCommand(config, source))
                return false;
        }
    } else if (Util::equal_insensitive(transferType, "copy", "file", "local")) {
        std::size_t completedCount = 0;
        for (auto &source: inputs) {
            ++completedCount;

            if (auto localFile = std::dynamic_pointer_cast<IO::File::Backing>(source.source)) {
                QFileInfo sourceInfo(QString::fromStdString(localFile->filename()));
                pushInputFile(sourceInfo);
                auto targetFile = applySubstitutions(config["Local/Target"].toQString());
                if (targetFile.isEmpty()) {
                    qCWarning(log_transfer_upload) << "Copy destination file is empty";
                    popInputFile();
                    return false;
                }
                popInputFile();

                if (!prepareLocalTarget(config["Local"], targetFile))
                    return false;

                feedback.emitStage(QObject::tr("Copying Output File (%1/%2)").arg(completedCount)
                                                                             .arg(inputs.size()),
                                   QObject::tr("Output is being copied to the file \"%1\".").arg(
                                           targetFile), false);
                if (!copyLocalFile(sourceInfo, QFileInfo(targetFile))) {
                    feedback.emitFailure();
                    return false;
                }
                continue;
            }

            auto stream = std::move(source.stream);
            if (!stream && source.source) {
                stream = source.source->stream();
            }
            if (!stream) {
                qCDebug(log_transfer_upload) << "No available file source";
                feedback.emitFailure();
                return false;
            }

            auto targetFile = applySubstitutions(config["Local/Target"].toQString());
            if (targetFile.isEmpty()) {
                qCWarning(log_transfer_upload) << "Local destination file is empty";
                return false;
            }

            auto target = IO::Access::file(targetFile, IO::File::Mode::writeOnly());
            if (!target) {
                qCDebug(log_transfer_upload) << "Unable to get local target file";
                return false;
            }

            if (!prepareLocalTarget(config["Local"], targetFile))
                return false;

            auto copyTarget = target->block();
            if (!copyTarget) {
                qCDebug(log_transfer_upload) << "Open target for data copy";
                return false;
            }

            feedback.emitStage(QObject::tr("Writing Output File (%1/%2)").arg(completedCount)
                                                                         .arg(inputs.size()),
                               QObject::tr("Output is being copied to the file \"%1\".").arg(
                                       targetFile), false);

            streamCopy(std::move(stream), *copyTarget);
            if (testTerminated())
                return false;
        }
    } else if (Util::equal_insensitive(transferType, "email")) {
        smtpSend.reset(new SMTPSend(config["Email/Relay"]));
        if (config["Email/From"].exists()) {
            auto from = applySubstitutions(config["Email/From"].toQString());
            if (!from.isEmpty())
                smtpSend->setFrom(from.toStdString());
        }

        QString attachmentName;
        if (config["Email/Attachment"].exists())
            attachmentName = config["Email/Attachment"].toQString();

        if (config["Email/Separate"].toBool()) {
            std::size_t completedCount = 0;
            for (auto &source: inputs) {
                ++completedCount;

                QString name;
                if (auto localFile = std::dynamic_pointer_cast<IO::File::Backing>(source.source)) {
                    QFileInfo sourceInfo(QString::fromStdString(localFile->filename()));
                    pushInputFile(sourceInfo);
                    name = applySubstitutions(attachmentName);
                    popInputFile();
                    if (name.isEmpty() && !config["Email/Attachment"].exists())
                        name = sourceInfo.fileName();
                } else {
                    name = applySubstitutions(attachmentName);
                }

                auto stream = std::move(source.stream);
                if (!stream && source.source) {
                    stream = source.source->stream();
                }
                if (!stream) {
                    qCDebug(log_transfer_upload) << "No available file source";
                    feedback.emitFailure();
                    return false;
                }

                feedback.emitStage(
                        QObject::tr("Reading File (%1/%2)").arg(completedCount).arg(inputs.size()),
                        QObject::tr(
                                "The input file is currently being read in preparation for generating the Email."),
                        false);

                qCDebug(log_transfer_upload) << "Reading attachment" << name;
                auto data = stream->readAll();

                if (testTerminated())
                    return false;

                if (!sendEmail(config["Email"], data.toQByteArray(), name)) {
                    return false;
                }
            }
        } else {
            EmailBuilder builder;
            configureEmail(config["Email"], builder);

            std::size_t completedCount = 0;
            for (auto &source: inputs) {
                ++completedCount;

                QString name;
                if (auto localFile = std::dynamic_pointer_cast<IO::File::Backing>(source.source)) {
                    QFileInfo sourceInfo(QString::fromStdString(localFile->filename()));
                    pushInputFile(sourceInfo);
                    name = applySubstitutions(attachmentName);
                    popInputFile();
                    if (name.isEmpty() && !config["Email/Attachment"].exists())
                        name = sourceInfo.fileName();
                } else {
                    name = applySubstitutions(attachmentName);
                }

                auto stream = std::move(source.stream);
                if (!stream && source.source) {
                    stream = source.source->stream();
                }
                if (!stream) {
                    qCDebug(log_transfer_upload) << "No available file source";
                    feedback.emitFailure();
                    return false;
                }

                feedback.emitStage(
                        QObject::tr("Reading File (%1/%2)").arg(completedCount).arg(inputs.size()),
                        QObject::tr(
                                "The input file is currently being read in preparation for generating the Email."),
                        false);

                qCDebug(log_transfer_upload) << "Reading attachment" << name;
                auto data = stream->readAll();

                if (testTerminated())
                    return false;

                if (!attachEmailFile(config["Email"], builder, data.toQByteArray(), name)) {
                    return false;
                }
            }

            builder.queue(smtpSend.get());
        }

        terminateRequested.connect(std::bind(&SMTPSend::signalTerminate, smtpSend.get()));
        if (testTerminated())
            return false;

        feedback.emitStage(QObject::tr("Sending Email"),
                           QObject::tr("The Email is being sent to the recipients."), true);

        qCDebug(log_transfer_upload) << "Executing SMTP send";
        smtpSend->start();

        if (!smtpSend->wait(config["Email/Timeout"].toDouble())) {
            feedback.emitFailure();
            return false;
        }
        if (testTerminated())
            return false;

    } else if (Util::equal_insensitive(transferType, "url")) {
        for (auto &source: inputs) {
            bool isFile = false;
            if (auto localFile = std::dynamic_pointer_cast<IO::File::Backing>(source.source)) {
                QFileInfo sourceInfo(QString::fromStdString(localFile->filename()));
                pushInputFile(sourceInfo);
                isFile = true;
            }

            bool ok = uploadURL(config["URL"], source);

            if (isFile) {
                popInputFile();
            }

            if (!ok)
                return false;
        }
    } else if (Util::equal_insensitive(transferType, "sftp")) {
        for (auto &source: inputs) {
            bool isFile = false;
            if (auto localFile = std::dynamic_pointer_cast<IO::File::Backing>(source.source)) {
                QFileInfo sourceInfo(QString::fromStdString(localFile->filename()));
                pushInputFile(sourceInfo);
                isFile = true;
            }

            bool ok = uploadSFTP(config["SFTP"], source);

            if (isFile) {
                popInputFile();
            }

            if (!ok)
                return false;
        }
    } else {
        for (auto &source: inputs) {
            bool isFile = false;
            if (auto localFile = std::dynamic_pointer_cast<IO::File::Backing>(source.source)) {
                QFileInfo sourceInfo(QString::fromStdString(localFile->filename()));
                pushInputFile(sourceInfo);
                isFile = true;
            }

            bool ok = uploadFTP(config["FTP"], source);

            if (isFile) {
                popInputFile();
            }

            if (!ok)
                return false;
        }
    }

    if (!executeActions(config["After"]))
        return false;

    return true;
}

FileUploader::UploadInput::UploadInput() = default;

FileUploader::UploadInput::~UploadInput() = default;

FileUploader::UploadInput::UploadInput(IO::Access::Handle handle) : source(std::move(handle))
{ }

FileUploader::UploadInput::UploadInput(std::unique_ptr<IO::Generic::Stream> &&stream) : stream(
        std::move(stream))
{ }

void FileUploader::addUpload(IO::Access::Handle source)
{ inputs.emplace_back(std::move(source)); }

void FileUploader::addUpload(std::unique_ptr<IO::Generic::Stream> &&source)
{ inputs.emplace_back(std::move(source)); }

void FileUploader::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    terminateRequested();
}

bool FileUploader::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

QString FileUploader::applySubstitutions(const QString &input) const
{ return input; }

void FileUploader::pushTemporaryOutput(const QFileInfo &)
{ }

void FileUploader::popTemporaryOutput()
{ }

void FileUploader::pushInputFile(const QFileInfo &)
{ }

void FileUploader::popInputFile()
{ }

static QFileInfo clearTargetFile(const QFileInfo &to, bool overwrite)
{
    if (overwrite) {
        QFile::remove(to.absoluteFilePath());
        return to;
    }

    if (!to.exists())
        return to;

    for (int i = 1; i < 100000; i++) {
        QFileInfo target(to.dir(), to.fileName() + "." + QString::number(i));
        if (!target.exists())
            return target;
    }
    return to;
}

bool FileUploader::moveLocalFile(const QFileInfo &from, const QFileInfo &to, bool overwrite)
{
#if defined(Q_OS_LINUX)
    if (!overwrite &&
            IO::Process::Spawn("mv", {"-f", "--backup=numbered",
                                      QDir::toNativeSeparators(from.absoluteFilePath()),
                                      QDir::toNativeSeparators(to.absoluteFilePath())}).run())
        return true;
#endif
#if defined(Q_OS_UNIX)
    if (IO::Process::Spawn("mv",
                           {"-f", QDir::toNativeSeparators(from.absoluteFilePath()).toStdString(),
                            QDir::toNativeSeparators(clearTargetFile(to,
                                                                     overwrite).absoluteFilePath()).toStdString()})
            .run())
        return true;
#elif 0 && defined(Q_OS_WIN32)
    if (IO::Process::Spawn("cmd.exe", {"/C", "move", "/Y",
            QDir::toNativeSeparators(from.absoluteFilePath()).toStdString(),
            QDir::toNativeSeparators(clearTargetFile(to, overwrite).
            absoluteFilePath()).toStdString()}).run())
        return true;
#endif
    if (QFile::rename(from.absoluteFilePath(), clearTargetFile(to, overwrite).absoluteFilePath()))
        return true;

    QFileInfo target(clearTargetFile(to, overwrite));
    if (!QFile::copy(from.absoluteFilePath(), target.absoluteFilePath()))
        return false;

    QFile::setPermissions(target.absoluteFilePath(), from.permissions());

    return QFile::remove(from.absoluteFilePath());
}

bool FileUploader::copyLocalFile(const QFileInfo &from, const QFileInfo &to, bool overwrite)
{
#if defined(Q_OS_LINUX)
    if (!overwrite &&
            IO::Process::Spawn("cp",
                               {"-f", "--backup=numbered", "--reflink=auto", "--no-preserve=all",
                                QDir::toNativeSeparators(from.absoluteFilePath()).toStdString(),
                                QDir::toNativeSeparators(
                                        to.absoluteFilePath()).toStdString()}).run())
        return true;
#endif
#if defined(Q_OS_LINUX)
    if (IO::Process::Spawn("cp", {"-f", "--reflink=auto", "--no-preserve=all",
                                  QDir::toNativeSeparators(from.absoluteFilePath()).toStdString(),
                                  QDir::toNativeSeparators(clearTargetFile(to,
                                                                           overwrite).absoluteFilePath())
                                          .toStdString()}).run())
        return true;
#elif 0 && defined(Q_OS_WIN32)
    if (IO::Process::Spawn("cmd.exe", {"/C", "copy", "/Y",
            QDir::toNativeSeparators(from.absoluteFilePath()).toStdString(),,
            QDir::toNativeSeparators(clearTargetFile(to, overwrite).
            absoluteFilePath()).toStdString()}).run())
        return true;
#endif

    return QFile::copy(from.absoluteFilePath(), clearTargetFile(to, overwrite).absoluteFilePath());
}


}
}
