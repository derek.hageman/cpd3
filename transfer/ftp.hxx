/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3TRANSFERFTP_H
#define CPD3TRANSFERFTP_H

#include "core/first.hxx"

#include "transfer/transfer.hxx"
#include "core/threading.hxx"
#include "datacore/variant/root.hxx"
#include "io/drivers/tcp.hxx"
#include "io/drivers/file.hxx"
#include "io/access.hxx"

namespace CPD3 {
namespace Transfer {

/**
 * A connection to an FTP server used for data transfer.
 */
class CPD3TRANSFER_EXPORT FTPConnection final {
public:
    /**
     * A single file in a FTP file listing.
     */
    class CPD3TRANSFER_EXPORT File {
        std::string remoteName;
        std::string remoteDirectory;
        std::uint_fast64_t remoteSize;
        double remoteModified;

        friend class FTPConnection;

        File(std::string remoteName,
             std::string remoteDirectory,
             std::uint_fast64_t remoteSize,
             double remoteModified);

    public:
        File();

        ~File();

        File(const File &other);

        File &operator=(const File &other);

        File(File &&other);

        File &operator=(File &&other);


        /**
         * Get the file name component of the file.  This does not include the path.
         *
         * @return the file name
         */
        inline const std::string &fileName() const
        { return remoteName; }

        /**
         * Get the directory name component of the file.
         *
         * @return  the containing directory name
         */
        inline const std::string &dir() const
        { return remoteDirectory; }

        /**
         * Get the size of the file in bytes.
         *
         * @return the file size
         */
        inline std::uint_fast64_t size() const
        { return remoteSize; }

        /**
         * Get the modified time of the file.
         *
         * @return the modified time
         */
        inline double modified() const
        { return remoteModified; }

        /**
         * Get the full path to the file.  This contains both the file name and the leading path.
         *
         * @return the file name
         */
        std::string filePath() const;
    };

private:
    std::string host;
    std::string user;
    std::string password;
    IO::Socket::TCP::Configuration socketConfig;
    enum class TransferMode {
        PassiveThenActive, ActiveThenPassive, Passive, Active,
    } transferMode;
    double commandTimeout;
    double transferTimeout;
    double transferTotalTimeout;
    double keepaliveInterval;
    int resumeRetries;
    bool relaxedIncoming;
    bool resumeDownload;
    bool resumeUpload;
    bool closeOnDataFailure;
    bool allowKeepaliveQueueing;
    std::uint_fast64_t uploadRateLimit;

    std::string error;

    std::uint_fast64_t byteCounter;

    std::mutex mutex;
    std::condition_variable notify;
    bool terminated;

    std::unique_ptr<IO::Socket::Connection> commandConnection;
    Util::ByteArray commandData;
    bool commandConnectionEnded;

    std::unique_ptr<IO::Socket::Connection> dataConnection;
    std::string passiveConnectionHost;
    IO::Socket::TCP::Configuration passiveConnectionConfig;

    std::unique_ptr<IO::Socket::TCP::Server> dataServer;
    std::vector<std::unique_ptr<IO::Socket::Connection>> incomingActiveDataConnections;

    using Clock = std::chrono::steady_clock;

    Clock::time_point nextCommandTimeout() const;

    Clock::time_point totalTransferTimeout() const;

    Clock::time_point dataConnectionEstablishTimeout(const Clock::time_point &absolute) const;

    std::string nextCommandLine(const Clock::time_point &timeout);

    int nextCommandCode(const Clock::time_point &timeout);

    std::pair<int, std::string> nextCommandResponse(const Clock::time_point &timeout);

    bool simpleCommand(const std::string &command);

    bool prepareDataConnection(bool polarity = false);

    bool establishDataConnection(const Clock::time_point &timeout);

    std::pair<std::string, std::uint16_t> negotiationEPSV();

    std::pair<std::string, std::uint16_t> negotiationPASV();

    bool preparePassiveConnection();

    void acceptIncomingActiveConnection(std::unique_ptr<IO::Socket::Connection> &&conn);

    bool prepareActiveConnection();

    enum class KeepaliveState {
        Disabled, WaitingToSend, WaitingForResponse, Failed,
    };
    KeepaliveState keepaliveState;
    Clock::time_point keepaliveTimeout;
    int queuedKeepalives;

    void startKeepalive();

    bool stepKeepalive(std::unique_lock<std::mutex> &lock);

    void flushKeepalive(std::unique_lock<std::mutex> &lock, const Clock::time_point &timeout);

    bool downloadData(IO::Generic::Writable &target,
                      const Clock::time_point &timeout,
                      std::uint_fast64_t expectedSize = 0,
                      std::uint_fast64_t cutoffSize = 0);

    bool uploadData(std::unique_ptr<IO::Generic::Stream> &&source,
                    const Clock::time_point &timeout, std::uint_fast64_t expectedSize = 0);

    bool interpretNLST(Util::ByteArray &raw, bool directories, std::vector<File> &result);

    double parseResponseTime(const std::string &response);

    bool interpretLIST(Util::ByteArray &raw,
                       bool directories,
                       const std::string &pathName,
                       std::vector<File> &result);

    std::size_t streamChunkSize() const;

public:
    /**
     * Create the FTP connection.
     *
     * @param hostname      the host name of the FTP server
     * @param port          the port number to connect on
     * @param username      the user name
     * @param password      the password
     * @param parent        the parent object
     */
    explicit FTPConnection(std::string hostname,
                           std::uint16_t port = 21,
                           std::string username = "anonymous",
                           std::string password = "cpd3");

    ~FTPConnection();

    /**
     * Get the host name the connection uses.
     *
     * @return the connection host name
     */
    inline const std::string &getHost() const
    { return host; }

    /**
     * Get the user name the connection uses.
     *
     * @return the connection user name
     */
    inline const std::string &getUser() const
    { return user; }

    /**
     * Get the port the connection uses.
     *
     * @return the connection user name
     */
    inline std::uint16_t getPort() const
    { return socketConfig.port; }

    /**
     * Get the password the connection uses.
     *
     * @return the connection password
     */
    inline const std::string &getPassword() const
    { return password; }

    /**
     * Configure the SSL settings used.  When defined the connection operates in FTPS mode.
     *
     * @param ssl   the SSL configuration.
     */
    void setSSL(const CPD3::Data::Variant::Read &ssl);

    /**
     * Use passive transfers only.
     */
    void setPassive();

    /**
     * Use active transfers only.
     */
    void setActive();

    /**
     * Try passive transfers then fall back to active if they fail.
     */
    void setPassiveThenActive();

    /**
     * Set relaxed mode for incoming active connections.  When in relaxed mode the
     * server and port of the originator are not checked.
     *
     * @param enable    enable relaxed mode
     */
    void setRelaxedIncoming(bool enable = true);

    /**
     * Set the connection download resume state.  When enabled the connection will attempt to
     * restart downloads based on the number of bytes transferred so far if the
     * transfer is interrupted.
     *
     * @param enable    enable download resume
     */
    void setResumeDownload(bool enable = true);

    /**
     * Set the connection upload resume state.  When enabled the connection will attempt to
     * restart downloads based on the file size on the remote server if the transfer is
     * interrupted.
     *
     * @param enable    enable upload resume
     */
    void setResumeUpload(bool enable = true);

    /**
     * Set the connection close on data failure state.  When enabled, this causes the
     * whole connection to close when a data connection cannot be established.
     *
     * @param enable    enable close on data failure
     */
    void setCloseOnDataFailure(bool enable = true);

    /**
     * Set the connection timeouts.
     * <br>
     * Setting to zero disables the timeout.
     *
     * @param command   the time to wait for command completion
     * @param transfer  the time to wait for transfer activity
     * @param total     the time to wait for the complete data transfer
     */
    void setTimeouts(double command, double transfer, double total);

    /**
     * Set the command keepalive interval during uploads and downloads.
     * <br>
     * Setting to zero disables keepalives.
     *
     * @param interval  the interval
     * @param queing    allow queueing during transfers
     */
    void setKeepalive(double interval, bool queueing = true);

    /**
     * Set the number of times transfering is retried.
     *
     * @param total     the number of retries (zero only tries once)
     */
    void setResumeRetries(int total);

    /**
     * Set the upload rate limit in bytes/second.
     *
     * @param bytesPerSecond    the rate limit or -1 to disable
     */
    void setUploadRateLimit(std::int_fast64_t bytesPerSecond);

    /**
     * Configure the connection from a data value.
     *
     * @param configuration the configuration to use
     */
    void configure(const CPD3::Data::Variant::Read &configuration);

    /**
     * Connect to the remote server.
     *
     * @return true on success
     */
    bool connectToServer();

    /**
     * Disconnect from the remote server
     */
    void disconnectFromServer();

    /**
     * Create a directory on the server.
     *
     * @param path  the directory to create
     * @return      true on success
     */
    bool mkdir(const std::string &path);

    /**
     * List files on the remote server.
     *
     * @param path          the path to list files in
     * @param directories   list directories instead of files only
     * @param ok            if not null then set to true on success
     * @return              the files in the directory
     */
    std::vector<File> list(const std::string &path, bool directories = false, bool *ok = nullptr);

    /**
     * Upload a file to the server.
     *
     * @param target        the remote file name
     * @param source        the data to upload
     * @param expectedSize  the expected size for progress indication
     * @return              true on success
     */
    bool put(const std::string &target,
             std::unique_ptr<IO::Generic::Stream> &&source,
             std::uint_fast64_t expectedSize = 0);

    bool put(const std::string &target,
             const std::function<
                     std::unique_ptr<IO::Generic::Stream>(std::uint_fast64_t offset)> &source,
             std::uint_fast64_t expectedSize = 0);

    bool put(const std::string &target,
             IO::Generic::Block &source,
             std::uint_fast64_t expectedSize = 0);

    bool put(const std::string &target,
             IO::Generic::Backing &source,
             std::uint_fast64_t expectedSize = static_cast<std::uint_fast64_t>(-1));

    bool put(const std::string &target, const Util::ByteArray &source);

    bool put(const std::string &target, const QByteArray &source);

    bool put(const std::string &target,
             QIODevice *source,
             std::uint_fast64_t expectedSize = static_cast<std::uint_fast64_t>(-1));

    /**
     * Download a file from the server.
     *
     * @param source        the remote file name
     * @param target        the target to write data to
     * @return              true on success
     */
    bool get(const std::string &source, IO::Generic::Writable &target);

    bool get(const std::string &source, IO::Generic::Backing &target);

    bool get(const std::string &source, Util::ByteArray &target);

    bool get(const std::string &source, QByteArray &target);

    bool get(const std::string &source, QIODevice *target);

    inline bool get(const File &source, IO::Generic::Writable &target)
    { return get(source.filePath(), target); }

    inline bool get(const File &source, IO::Generic::Backing &target)
    { return get(source.filePath(), target); }

    inline bool get(const File &source, Util::ByteArray &target)
    { return get(source.filePath(), target); }

    inline bool get(const File &source, QByteArray &target)
    { return get(source.filePath(), target); }

    inline bool get(const File &source, QIODevice *target)
    { return get(source.filePath(), target); }

    /**
     * Get a string describing the cause of the last failure.
     *
     * @return a description of the last failure
     */
    inline const std::string &errorString() const
    { return error; }

    /**
     * Terminate any active transfer and start shutting down.
     */
    void signalTerminate();

    /**
     * Emitted periodically while data are being transferred.  The first argument
     * is the number of bytes completed, and the second is the total size if available
     * or 0 if not.
     */
    Threading::Signal<std::uint_fast64_t, std::uint_fast64_t> progress;
};

}
}

#endif