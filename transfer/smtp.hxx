/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3TRANSFERSMTP_H
#define CPD3TRANSFERSMTP_H

#include "core/first.hxx"

#include <mutex>

#include "transfer/transfer.hxx"
#include "core/threading.hxx"
#include "datacore/variant/root.hxx"

namespace CPD3 {
namespace Transfer {

/**
 * This is a transfer of a single SMTP message to one or more recipients.
 */
class CPD3TRANSFER_EXPORT SMTPSend final {
    struct QueuedMessage {
        std::vector<std::string> recipients;
        Util::ByteArray message;
        bool insertToHeader;

        QueuedMessage();
    };
    std::vector<QueuedMessage> messages;
    std::string from;

    Util::ByteArray fromHeader;

    enum class RelayMode {
        Direct, Relay, LocalProgram
    } relayMode;
    std::string relayName;
    std::uint16_t relayPort;
    bool relayTLS;
    std::string relayUsername;
    std::string relayPassword;

    class Sender;

    std::mutex mutex;
    std::condition_variable notify;
    std::vector<std::unique_ptr<Sender>> senders;
    bool allSendsOk;
    bool anySentOk;

    std::thread reaper;

    void sendToProgram(Util::ByteArray &&message);

    void sendToRelay(std::vector<std::string> &&recipients, Util::ByteArray &&message);

    void sendDirect(const std::string &host,
                    std::vector<std::string> &&recipients,
                    const Util::ByteArray &message);

public:
    /**
     * Create a new SMTP sending object from a relay configuration.  If the
     * relay is empty or does not specify a host name then the sending
     * object will initiate the connection to the destination addresses
     * directly.
     * 
     * @param relay         the relay configuration
     * @param parent        the parent object
     */
    SMTPSend(const Data::Variant::Read &relay = Data::Variant::Read::empty());

    ~SMTPSend();

    SMTPSend(const SMTPSend &) = delete;

    SMTPSend &operator=(const SMTPSend &) = delete;

    /**
     * Send the message(s).
     */
    void start();

    /**
     * Wait for the timeout to be completed.
     * 
     * @param time      the maximum time to wait
     * @return          true if all messages have been sent
     */
    bool wait(double timeout = FP::undefined());

    /**
     * Test if the message(s) have been sent.
     *
     * @return  true when completed
     */
    bool isFinished();

    /**
     * Test if all messages where successfully sent.  Only valid after the send operation
     * has completed.
     *
     * @return  true if all messages have been sent
     */
    bool allMessagesSent() const;

    /**
     * Test if any messages where successfully sent.  Only valid after the send operation
     * has completed.
     *
     * @return  true if any messages have been sent
     */
    bool anyMessageSent() const;

    /**
     * Set the SMTP from line.  If unset then this is calculated from the
     * local host.
     * 
     * @param from      the from address
     * @param insert    if set then the send will insert a from header at the start of all message bodies
     */
    void setFrom(const std::string &from = {}, bool insert = true);

    /**
     * Add a message for sending.
     * 
     * @param recipients        the recipients of the message
     * @param message           the message to send (including all headers, etc)
     * @param insertToHeader    insert a "To: " header with the recipients
     */
    void addMessage(const std::vector<std::string> &recipients,
                    const Util::ByteArray &message,
                    bool insertToHeader = true);

    void addMessage(const QStringList &recipients,
                    const QByteArray &message,
                    bool insertToHeader = true);

    /**
     * Abort the send.
     */
    void signalTerminate();

    /**
     * Emitted when all messages have been sent.
     */
    Threading::Signal<> finished;
};

}
}

#endif
