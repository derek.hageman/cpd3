/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <thread>
#include <QLoggingCategory>

#include "acquisition/acquisitionarchive.hxx"
#include "datacore/stream.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/util.hxx"


Q_LOGGING_CATEGORY(log_acquisition_archive, "cpd3.acquisition.archive", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace Acquisition {

static const SequenceName::Component archiveRaw = "raw";

/** @file acquisition/acquisitionarchive.hxx
 * Acquisition archive access controller.
 */





AcquisitionArchive::AcquisitionArchive(const Variant::Read &configuration) : loggingDeferWrite(
        !configuration.hash("Immediate").toBool()),
                                                                             maximumPurgeAge(
                                                                                     3600.0),
                                                                             minimumPurgePreserveSize(
                                                                                     120.0),
                                                                             inShutdown(false)
{
    if (configuration.hash("PurgeAge").exists())
        maximumPurgeAge = configuration.hash("PurgeAge").toDouble();
    if (configuration.hash("PurgePreserveDuration").exists())
        minimumPurgePreserveSize = configuration.hash("PurgePreserveDuration").toDouble();
}

AcquisitionArchive::~AcquisitionArchive()
{
    initiateShutdown();
    wait();
}

void AcquisitionArchive::backgroundThread()
{
    std::unique_lock<std::mutex> lock(mutex);
    while (!backgroundQueue.empty()) {
        std::function<void(std::unique_lock<std::mutex> &)>
                run = std::move(backgroundQueue.front());

        run(lock);
        Q_ASSERT(lock.owns_lock());

        backgroundQueue.pop_front();
    }
    completion.notify_all();
}

void AcquisitionArchive::backgroundLaunch(const std::function<
        void(std::unique_lock<std::mutex> &)> &run)
{
    if (backgroundQueue.empty())
        std::thread(std::bind(&AcquisitionArchive::backgroundThread, this)).detach();
    backgroundQueue.emplace_back(run);
}

void AcquisitionArchive::backgroundLaunch(std::function<void(std::unique_lock<std::mutex> &)> &&run)
{
    if (backgroundQueue.empty())
        std::thread(std::bind(&AcquisitionArchive::backgroundThread, this)).detach();
    backgroundQueue.emplace_back(std::move(run));
}

void AcquisitionArchive::saveState(const QString &key, const QByteArray &data)
{
    std::lock_guard<std::mutex> lock(mutex);
    pendingState[key] = data;
    backgroundLaunch(std::bind(&AcquisitionArchive::stateRun, this, std::placeholders::_1));
}

void AcquisitionArchive::saveState(QString &&key, QByteArray &&data)
{
    std::lock_guard<std::mutex> lock(mutex);
    pendingState[std::move(key)] = std::move(data);
    backgroundLaunch(std::bind(&AcquisitionArchive::stateRun, this, std::placeholders::_1));
}

void AcquisitionArchive::stateRun(std::unique_lock<std::mutex> &lock)
{
    std::unique_ptr<Database::RunLock> runLock;

    while (!pendingState.empty()) {
        auto writeState = std::move(pendingState);
        pendingState.clear();
        lock.unlock();

        if (!runLock)
            runLock.reset(new Database::RunLock);

        for (auto &w : writeState) {
            runLock->blindSet(std::move(w.first), std::move(w.second));
        }

        lock.lock();
    }
    Q_ASSERT(lock.owns_lock());
    Q_ASSERT(pendingState.empty());

    if (!runLock)
        return;

    /* Don't release it with the lock held, so any cleanup can run without a lock
     * held */
    lock.unlock();
    runLock.reset();
    lock.lock();
}

void AcquisitionArchive::reapAccess()
{
    /* Release all these outside of the lock, so anything shutdown doesn't block */
    std::unique_ptr<Data::Archive::Access> accessRelease;
    std::shared_ptr<EventWrite> eventsRelease;
    std::shared_ptr<ArchiveWrite> archiveRelease;
    std::shared_ptr<PersistentWrite> persistentRelease;
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (!access)
            return;

        if (events) {
            if (!events->wait(0.0))
                return;
            eventsRelease = std::move(events);
            events.reset();
        }
        if (archive) {
            if (!archive->wait(0.0))
                return;
            archiveRelease = std::move(archive);
            archive.reset();
        }
        if (persistent) {
            if (!persistent->wait(0.0))
                return;
            persistentRelease = std::move(persistent);
            persistent.reset();
        }

        accessRelease = std::move(access);
        access.reset();

        completion.notify_all();
    }
}

void AcquisitionArchive::acquireAccess(std::unique_lock<std::mutex> &lock)
{
    if (access)
        return;

    /* Don't create the access with the lock held, so any transaction it does
     * is unlocked */
    lock.unlock();
    std::unique_ptr<Archive::Access> a(new Archive::Access);
    lock.lock();

    if (access)
        return;
    access = std::move(a);
}

void AcquisitionArchive::incomingEvent(const Data::SequenceValue &event)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (!access) {
        backgroundLaunch([this, event](std::unique_lock<std::mutex> &l) {
            acquireAccess(l);
            if (!events) {
                events = std::make_shared<EventWrite>(*access);
                events->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
            }
            events->incomingEvent(std::move(event));
        });
        return;
    }
    if (!events) {
        events = std::make_shared<EventWrite>(*access);
        events->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
    }
    events->incomingEvent(event);
}

void AcquisitionArchive::incomingEvent(Data::SequenceValue &&event)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (!access) {
        backgroundLaunch([this, event](std::unique_lock<std::mutex> &l) {
            acquireAccess(l);
            if (!events) {
                events = std::make_shared<EventWrite>(*access);
                events->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
            }
            events->incomingEvent(std::move(event));
        });
        return;
    }
    if (!events) {
        events = std::make_shared<EventWrite>(*access);
        events->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
    }
    events->incomingEvent(std::move(event));
}

AcquisitionArchive::EventWrite::EventWrite(Archive::Access &access) : BackgroundWrite(access)
{ }

AcquisitionArchive::EventWrite::~EventWrite() = default;

void AcquisitionArchive::EventWrite::incomingEvent(const SequenceValue &event)
{
    wake([this, &event] {
        incoming.emplace_back(event);
        return true;
    });
}

void AcquisitionArchive::EventWrite::incomingEvent(SequenceValue &&event)
{
    wake([this, &event] {
        incoming.emplace_back(std::move(event));
        return true;
    });
}

bool AcquisitionArchive::EventWrite::prepare()
{
    beginTime = Time::time();

    if (incoming.empty())
        return !processing.empty();

    Util::append(std::move(incoming), processing);
    incoming.clear();
    return true;
}

Archive::Access::BackgroundWrite::Result AcquisitionArchive::EventWrite::operation(Archive::Access &access)
{
    for (auto &event : processing) {
        for (int t = 0; t < 1000; t++) {
            StreamSink::Iterator existing;
            access.readStream(Archive::Selection(event.getUnit(), event.getStart() - 0.001,
                                                 event.getStart() + 0.001).withMetaArchive(false)
                                                                          .withDefaultStation(
                                                                                  false), &existing)
                  ->detach();
            bool hit = false;
            int highestMatchingPriority = event.getPriority();
            while (existing.hasNext()) {
                auto v = existing.next();
                if (v.getUnit() != event.getUnit())
                    continue;
                if (!FP::equal(v.getStart(), event.getStart()))
                    continue;
                if (!FP::equal(v.getEnd(), event.getEnd()))
                    continue;
                highestMatchingPriority = std::max(highestMatchingPriority, v.getPriority());
                if (v.getPriority() != event.getPriority())
                    continue;
                hit = true;
            }
            if (!hit)
                break;

            event.setPriority(highestMatchingPriority + 1);
        }

        access.writeSynchronous(SequenceValue::Transfer{event});
    }
    return Again;
}

void AcquisitionArchive::EventWrite::success()
{
    double endTime = Time::time();
    for (const auto &event : processing) {
        qCDebug(log_acquisition_archive) << "Wrote event" << event.read().hash("Text").toString()
                                         << "from" << event.read().hash("Source").toString() << "in"
                                         << Logging::elapsed(endTime - beginTime);
    }
    processing.clear();
}

Data::StreamSink *AcquisitionArchive::createLoggingSink()
{ return new ArchiveSink(*this); }

AcquisitionArchive::ArchiveSink::ArchiveSink(AcquisitionArchive &archive) : archive(archive),
                                                                            ended(false)
{ }

AcquisitionArchive::ArchiveSink::~ArchiveSink()
{
    std::unique_lock<std::mutex> lock(archive.mutex);
    archive.completion.wait(lock, [this] { return ended; });
}

void AcquisitionArchive::ArchiveSink::incomingData(const SequenceValue::Transfer &values)
{ archive.incomingLogging(values); }

void AcquisitionArchive::ArchiveSink::incomingData(SequenceValue::Transfer &&values)
{ archive.incomingLogging(std::move(values)); }

void AcquisitionArchive::ArchiveSink::incomingData(const SequenceValue &value)
{ archive.incomingLogging(value); }

void AcquisitionArchive::ArchiveSink::incomingData(SequenceValue &&value)
{ archive.incomingLogging(std::move(value)); }

void AcquisitionArchive::ArchiveSink::endData()
{
    std::lock_guard<std::mutex> lock(archive.mutex);
    ended = true;
    archive.completion.notify_all();
}

void AcquisitionArchive::backgroundLoggingFlush()
{
    backgroundLaunch([this](std::unique_lock<std::mutex> &l) {
        if (deferredLogging.empty())
            return;
        acquireAccess(l);
        if (!archive) {
            archive = std::make_shared<ArchiveWrite>(*this, *access);
            archive->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
        }
        archive->incomingData(std::move(deferredLogging));
        deferredLogging.clear();
    });
}

void AcquisitionArchive::flushLogging()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (deferredLogging.empty())
        return;
    if (!access) {
        backgroundLoggingFlush();
        return;
    }
    if (!archive) {
        archive = std::make_shared<ArchiveWrite>(*this, *access);
        archive->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
    }
    archive->incomingData(std::move(deferredLogging));
    deferredLogging.clear();
}

bool AcquisitionArchive::shouldDeferLogging(std::size_t incoming) const
{
    if (!loggingDeferWrite)
        return false;
    if (inShutdown)
        return false;
    return deferredLogging.size() + incoming < 8192;
}

void AcquisitionArchive::incomingLogging(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;

    std::lock_guard<std::mutex> lock(mutex);
    if (shouldDeferLogging(values.size())) {
        Util::append(values, deferredLogging);
        return;
    }

    if (!access) {
        Util::append(values, deferredLogging);
        backgroundLoggingFlush();
        return;
    }

    if (!archive) {
        archive = std::make_shared<ArchiveWrite>(*this, *access);
        archive->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
    }
    if (!deferredLogging.empty()) {
        archive->incomingData(std::move(deferredLogging));
        deferredLogging.clear();
    }
    archive->incomingData(values);
}

void AcquisitionArchive::incomingLogging(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;

    std::lock_guard<std::mutex> lock(mutex);
    if (shouldDeferLogging(values.size())) {
        Util::append(std::move(values), deferredLogging);
        return;
    }

    if (!access) {
        Util::append(std::move(values), deferredLogging);
        backgroundLoggingFlush();
        return;
    }

    if (!archive) {
        archive = std::make_shared<ArchiveWrite>(*this, *access);
        archive->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
    }
    if (!deferredLogging.empty()) {
        archive->incomingData(std::move(deferredLogging));
        deferredLogging.clear();
    }
    archive->incomingData(std::move(values));
}

void AcquisitionArchive::incomingLogging(const SequenceValue &value)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (shouldDeferLogging(1)) {
        deferredLogging.emplace_back(value);
        return;
    }

    if (!access) {
        deferredLogging.emplace_back(value);
        backgroundLoggingFlush();
        return;
    }

    if (!archive) {
        archive = std::make_shared<ArchiveWrite>(*this, *access);
        archive->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
    }
    if (!deferredLogging.empty()) {
        archive->incomingData(std::move(deferredLogging));
        deferredLogging.clear();
    }
    archive->incomingData(value);
}

void AcquisitionArchive::incomingLogging(SequenceValue &&value)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (shouldDeferLogging(1)) {
        deferredLogging.emplace_back(std::move(value));
        return;
    }

    if (!access) {
        deferredLogging.emplace_back(std::move(value));
        backgroundLoggingFlush();
        return;
    }

    if (!archive) {
        archive = std::make_shared<ArchiveWrite>(*this, *access);
        archive->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
    }
    if (!deferredLogging.empty()) {
        archive->incomingData(std::move(deferredLogging));
        deferredLogging.clear();
    }
    archive->incomingData(std::move(value));
}

bool AcquisitionArchive::checkPurgeValue(const SequenceValue &add) const
{
    if (!add.getUnit().isMeta()) {
        if (FP::defined(minimumPurgePreserveSize)) {
            if (FP::defined(add.getStart()) &&
                    FP::defined(add.getEnd()) &&
                    (add.getEnd() - add.getStart()) < minimumPurgePreserveSize)
                return false;
        }
    }

    return true;
}

AcquisitionArchive::ArchiveWrite::ArchiveWrite(AcquisitionArchive &archive, Archive::Access &access)
        : BackgroundWrite(access), archive(archive)
{ }

AcquisitionArchive::ArchiveWrite::~ArchiveWrite()
{
    Q_ASSERT(incoming.empty());
}

void AcquisitionArchive::ArchiveWrite::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(values, incoming);
        return true;
    });
}

void AcquisitionArchive::ArchiveWrite::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(std::move(values), incoming);
        return true;
    });
}

void AcquisitionArchive::ArchiveWrite::incomingData(const SequenceValue &value)
{
    wake([this, &value] {
        incoming.emplace_back(value);
        return true;
    });
}

void AcquisitionArchive::ArchiveWrite::incomingData(SequenceValue &&value)
{
    wake([this, &value] {
        incoming.emplace_back(std::move(value));
        return true;
    });
}

bool AcquisitionArchive::ArchiveWrite::prepare()
{
    beginTime = Time::time();
    purgeRanges.clear();

    if (incoming.empty())
        return !processing.empty();

    Util::append(std::move(incoming), processing);
    incoming.clear();
    return true;
}

Archive::Access::BackgroundWrite::Result AcquisitionArchive::ArchiveWrite::operation(Archive::Access &access)
{
    purgeRanges.clear();
    firstWrittenTime = FP::undefined();

    double purgeLimit = FP::undefined();
    if (FP::defined(archive.maximumPurgeAge)) {
        purgeLimit = Time::time() - archive.maximumPurgeAge;
    }

    for (const auto &v : processing) {
        if (!FP::defined(firstWrittenTime) ||
                (FP::defined(v.getStart()) && v.getStart() < firstWrittenTime)) {
            firstWrittenTime = v.getStart();
        }

        auto purgeUnit = v.getUnit();
        auto suffix = Util::suffix(purgeUnit.getVariable(), '_');
        if (suffix.empty())
            continue;
        purgeUnit.setVariable(suffix);
        purgeUnit.clearMeta();
        purgeUnit.clearFlavors();

        Q_ASSERT(purgeUnit.getArchive() == archiveRaw);

        double startPurge = v.getStart();
        if (FP::defined(purgeLimit)) {
            if (!FP::defined(startPurge) || startPurge < purgeLimit)
                startPurge = purgeLimit;
        }

        double endPurge = FP::undefined();
        auto purgeCheck = archive.purgedInstrumentData.find(purgeUnit);
        if (purgeCheck != archive.purgedInstrumentData.end())
            endPurge = purgeCheck->second;
        if (Range::compareStartEnd(startPurge, endPurge) >= 0)
            continue;

        auto p = purgeRanges.find(purgeUnit);
        if (p == purgeRanges.end()) {
            purgeRanges.emplace(purgeUnit, Time::Bounds(startPurge, endPurge));
            qCDebug(log_acquisition_archive) << "Removing data for" << suffix << "in"
                                             << Logging::range(startPurge, endPurge)
                                             << "triggered by" << v.getUnit() << "for"
                                             << Logging::range(v.getStart(), v.getEnd());
        } else {
            Q_ASSERT(FP::equal(p->second.getEnd(), endPurge));
            if (Range::compareStart(startPurge, p->second.getStart()) < 0) {
                p->second.setStart(startPurge);
                qCDebug(log_acquisition_archive) << "Extending removal for" << suffix << "in"
                                                 << Logging::range(startPurge, p->second.getEnd())
                                                 << "triggered by" << v.getUnit() << "for"
                                                 << Logging::range(v.getStart(), v.getEnd());
            }
        }
    }

    SequenceValue::Transfer modify = processing;
    SequenceName::Map<SequenceValue::Transfer> metadata;
    for (auto v = modify.begin(); v != modify.end();) {
        if (!v->getUnit().isMeta()) {
            ++v;
            continue;
        }
        metadata[v->getUnit()].emplace_back(std::move(*v));
        v = modify.erase(v);
    }

    SequenceIdentity::Transfer remove;
    for (const auto &p : purgeRanges) {
        double purgeStart = p.second.getStart();
        double purgeEnd = p.second.getEnd();

        StreamSink::Iterator existing;
        access.readStream(
                Archive::Selection(purgeStart, purgeEnd, {p.first.getStation()}, {archiveRaw},
                                   {QString("[^_]+_%1").arg(QRegExp::escape(
                                                               p.first.getVariableQString()))
                                                       .toStdString()}).withMetaArchive(true)
                                                                       .withDefaultStation(false),
                &existing)->detach();

        qint64 totalRemoved = 0;
        qint64 totalBefore = 0;
        qint64 totalAfter = 0;
        while (existing.hasNext()) {
            auto v = existing.next();
            if (Range::compareStartEnd(v.getStart(), purgeEnd) >= 0)
                break;
            if (Range::compareStartEnd(purgeStart, v.getEnd()) >= 0)
                continue;
            if (v.getStation() != p.first.getStation())
                continue;
            {
                std::lock_guard<std::mutex> lock(archive.transactionMutex);
                if (!v.getUnit().isMeta() && archive.seenPersistent.count(v.getUnit()) != 0)
                    continue;
            }

            /* At this point, we know it intersects the purge range, so remove the existing;
             * we'll add fragments below if required */
            remove.emplace_back(v);
            ++totalRemoved;

            /* If we have an end of the purge, see if we need to modify the existing
             * and move it after the purge end */
            if (FP::defined(purgeEnd) && Range::compareEnd(v.getEnd(), purgeEnd) > 0) {
                SequenceValue dv = v;
                dv.setStart(purgeEnd);
                if (archive.checkPurgeValue(dv)) {
                    modify.emplace_back(std::move(dv));
                    ++totalAfter;
                }
            }

            /* If we have a purge start, see if we need to add a new one before the start
             * of the purge*/
            if (FP::defined(purgeStart) && Range::compareStart(v.getStart(), purgeStart) < 0) {
                SequenceValue dv = v;
                dv.setEnd(purgeStart);
                if (archive.checkPurgeValue(dv)) {
                    modify.emplace_back(std::move(dv));
                    ++totalBefore;
                }
            }
        }

        qCDebug(log_acquisition_archive) << "Purge for" << p.first.getVariable() << "removing "
                                         << totalRemoved << "value(s), with" << totalBefore
                                         << "before and" << totalAfter << "after";
    }

    if (!modify.empty() || !remove.empty())
        access.writeSynchronous(modify, remove);

    for (auto &add : metadata) {
        flattenOverlapping(access, std::move(add.second));
    }

    return Again;
}

void AcquisitionArchive::ArchiveWrite::success()
{
    for (const auto &p : purgeRanges) {
        double purgeStart = p.second.getStart();
        Q_ASSERT(archive.purgedInstrumentData.count(p.first) == 0 ||
                         Range::compareStart(purgeStart,
                                             archive.purgedInstrumentData.at(p.first)) <= 0);
        archive.purgedInstrumentData.emplace(p.first, purgeStart);
    }

    if (FP::defined(firstWrittenTime))
        archive.loggingWritten(firstWrittenTime);

    double endTime = Time::time();
    qCInfo(log_acquisition_archive) << "Wrote" << processing.size() << "logging value(s) in"
                                    << Logging::elapsed(endTime - beginTime);
    processing.clear();
}

void AcquisitionArchive::terminateLogging(double endTime)
{
    if (!FP::defined(endTime))
        return;
    Q_ASSERT(!access);

    Archive::Access access;
    double startPrepare;
    double endPrepare;
    int nRemove = 0;
    int nModify = 0;
    for (;;) {
        startPrepare = Time::time();

        Archive::Access::WriteLock lock(access);

        SequenceValue::Transfer modify;
        SequenceIdentity::Transfer remove;
        for (const auto &p : purgedInstrumentData) {
            StreamSink::Iterator existing;
            access.readStream(Archive::Selection(endTime, FP::undefined(), {p.first.getStation()},
                                                 {archiveRaw}, {QString("[^_]+_%1").arg(
                            QRegExp::escape(p.first.getVariableQString()))
                                                                                   .toStdString()}).withMetaArchive(
                                                                                                           true)
                                                                                                   .withDefaultStation(false),
                              &existing)->detach();

            while (existing.hasNext()) {
                auto v = existing.next();
                if (v.getStation() != p.first.getStation())
                    continue;
                if (Range::compareEnd(v.getEnd(), endTime) <= 0)
                    continue;

                remove.emplace_back(v);

                /* Entirely after the end time, so just remove */
                if (Range::compareStartEnd(v.getStart(), endTime) >= 0)
                    continue;

                /* Add a value with the original start and an end at the start of the purge range */
                SequenceValue dv = v;
                dv.setEnd(endTime);
                if (!checkPurgeValue(dv))
                    continue;
                modify.emplace_back(std::move(dv));
            }
        }

        endPrepare = Time::time();

        nModify = modify.size();
        nRemove = remove.size();
        access.writeSynchronous(modify, remove);

        if (lock.commit())
            break;
    }


    double endWrite = Time::time();
    qCInfo(log_acquisition_archive) << "Terminate purge ending at" << Logging::time(endTime)
                                    << "completed in" << Logging::elapsed(endWrite - startPrepare)
                                    << "with" << Logging::elapsed(endPrepare - startPrepare)
                                    << "spent in pre-write, removing" << nRemove << "value(s) with"
                                    << nModify << "remaining";
}


AcquisitionArchive::PersistentWrite::PersistentWrite(AcquisitionArchive &archive,
                                                     Archive::Access &access) : BackgroundWrite(
        access), archive(archive)
{ }

AcquisitionArchive::PersistentWrite::~PersistentWrite()
{
    Q_ASSERT(incoming.empty());
}

void AcquisitionArchive::PersistentWrite::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(values, incoming);
        return true;
    });
}

void AcquisitionArchive::PersistentWrite::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(std::move(values), incoming);
        return true;
    });
}

void AcquisitionArchive::PersistentWrite::incomingData(const SequenceValue &value)
{
    wake([this, &value] {
        incoming.emplace_back(value);
        return true;
    });
}

void AcquisitionArchive::PersistentWrite::incomingData(SequenceValue &&value)
{
    wake([this, &value] {
        incoming.emplace_back(std::move(value));
        return true;
    });
}

bool AcquisitionArchive::PersistentWrite::prepare()
{
    beginTime = Time::time();
    notifySeen.clear();

    if (incoming.empty())
        return !processing.empty();

    Util::append(std::move(incoming), processing);
    incoming.clear();
    return true;
}

Archive::Access::BackgroundWrite::Result AcquisitionArchive::PersistentWrite::operation(Archive::Access &access)
{
    auto values = processing;
    notifySeen.clear();

    SequenceName::Map<SequenceValue::Transfer> breakdown;
    SequenceValue::Transfer overflow;
    for (const auto &add : processing) {
        if (add.getUnit().getArchive() != archiveRaw) {
            overflow.emplace_back(add);
            continue;
        }

        notifySeen.emplace(add.getUnit());

        breakdown[add.getUnit()].emplace_back(add);
    }

    if (!overflow.empty())
        access.writeSynchronous(overflow, false);

    for (auto &add : breakdown) {
        flattenOverlapping(access, std::move(add.second));
    }

    return Again;
}

void AcquisitionArchive::PersistentWrite::success()
{
    {
        std::lock_guard<std::mutex> lock(archive.transactionMutex);
        std::copy(notifySeen.begin(), notifySeen.end(),
                  std::inserter(archive.seenPersistent, archive.seenPersistent.end()));
    }

    double endTime = Time::time();
    qCInfo(log_acquisition_archive) << "Wrote" << processing.size() << "persistent value(s) in"
                                    << Logging::elapsed(endTime - beginTime);
    processing.clear();
}

void AcquisitionArchive::flattenOverlapping(Archive::Access &access,
                                            SequenceValue::Transfer incoming)
{
    if (incoming.empty())
        return;

    std::sort(incoming.begin(), incoming.end(), [](const SequenceValue &a, const SequenceValue &b) {
        return Range::compareStart(a.getStart(), b.getStart()) < 0;
    });

    /* First, walk forwards and set the end times, so the incoming ones don't overlap */
    for (auto v = incoming.begin(), next = v + 1; next != incoming.end();) {
        Q_ASSERT(v->getUnit() == next->getUnit());

        v->setEnd(next->getStart());

        /* Becomes zero length, so discard it */
        if (Range::compareStartEnd(v->getStart(), v->getEnd()) >= 0) {
            v = incoming.erase(v);
            next = v + 1;
            continue;
        }

        v = next;
        ++next;
    }

    Q_ASSERT(!incoming.empty());

    const auto &first = incoming.front();

    /* Now read any archived values that intersect the replacement range (from now
     * to the end of time) */
    StreamSink::Iterator existing;
    access.readStream(Archive::Selection(first.getUnit(), first.getStart()).withMetaArchive(false)
                                                                           .withDefaultStation(
                                                                                   false),
                      &existing)->detach();

    SequenceIdentity::Transfer remove;
    /* Adjust existing values to all end at the start of the first incoming one, or
     * remove entirely if they start after it */
    while (existing.hasNext()) {
        auto ev = existing.next();

        if (ev.getUnit() != first.getUnit())
            continue;
        if (Range::compareStartEnd(first.getStart(), ev.getEnd()) > 0)
            continue;

        remove.emplace_back(ev);

        ev.setEnd(first.getStart());

        /* Becomes zero length, so discard it */
        if (Range::compareStartEnd(ev.getStart(), ev.getEnd()) >= 0)
            continue;

        incoming.emplace_back(std::move(ev));
    }

    access.writeSynchronous(incoming, remove);
}

void AcquisitionArchive::incomingPersistent(const Data::SequenceValue::Transfer &values)
{
    if (values.empty())
        return;

    std::lock_guard<std::mutex> lock(mutex);
    if (!access) {
        backgroundLaunch([this, values](std::unique_lock<std::mutex> &l) {
            acquireAccess(l);
            if (!persistent) {
                persistent = std::make_shared<PersistentWrite>(*this, *access);
                persistent->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
            }
            persistent->incomingData(std::move(values));
        });
        return;
    }

    if (!persistent) {
        persistent = std::make_shared<PersistentWrite>(*this, *access);
        persistent->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
    }
    persistent->incomingData(values);
}

void AcquisitionArchive::incomingPersistent(Data::SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;

    std::lock_guard<std::mutex> lock(mutex);
    if (!access) {
        backgroundLaunch([this, values](std::unique_lock<std::mutex> &l) {
            acquireAccess(l);
            if (!persistent) {
                persistent = std::make_shared<PersistentWrite>(*this, *access);
                persistent->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
            }
            persistent->incomingData(std::move(values));
        });
        return;
    }

    if (!persistent) {
        persistent = std::make_shared<PersistentWrite>(*this, *access);
        persistent->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
    }
    persistent->incomingData(std::move(values));
}

void AcquisitionArchive::incomingPersistent(const Data::SequenceValue &value)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (!access) {
        backgroundLaunch([this, value](std::unique_lock<std::mutex> &l) {
            acquireAccess(l);
            if (!persistent) {
                persistent = std::make_shared<PersistentWrite>(*this, *access);
                persistent->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
            }
            persistent->incomingData(std::move(value));
        });
        return;
    }

    if (!persistent) {
        persistent = std::make_shared<PersistentWrite>(*this, *access);
        persistent->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
    }
    persistent->incomingData(value);
}

void AcquisitionArchive::incomingPersistent(Data::SequenceValue &&value)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (!access) {
        backgroundLaunch([this, value](std::unique_lock<std::mutex> &l) {
            acquireAccess(l);
            if (!persistent) {
                persistent = std::make_shared<PersistentWrite>(*this, *access);
                persistent->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
            }
            persistent->incomingData(std::move(value));
        });
        return;
    }

    if (!persistent) {
        persistent = std::make_shared<PersistentWrite>(*this, *access);
        persistent->complete.connect(std::bind(&AcquisitionArchive::reapAccess, this));
    }
    persistent->incomingData(std::move(value));
}

void AcquisitionArchive::initiateShutdown()
{
    std::lock_guard<std::mutex> lock(mutex);
    inShutdown = true;
    if (!deferredLogging.empty()) {
        backgroundLoggingFlush();
    }
}

void AcquisitionArchive::wait()
{
    reapAccess();

    std::unique_lock<std::mutex> lock(mutex);
    completion.wait(lock, [this] { return !access && backgroundQueue.empty(); });
}

}
}