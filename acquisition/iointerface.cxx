/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QCoreApplication>
#include <QLoggingCategory>
#include <QDir>
#include <QFileInfo>

#include "iointerface.hxx"
#include "ioserver.hxx"
#include "io/process.hxx"

Q_LOGGING_CATEGORY(log_acquisition_iointerface, "cpd3.acquisition.iointerface", QtWarningMsg)

using namespace CPD3::Data;


namespace CPD3 {
namespace Acquisition {

IOInterface::IOInterface(std::unique_ptr<IOTarget> &&target) : terminated(false),
                                                               connectionState(
                                                                       ConnectionState::Initialize),
                                                               clientSideTarget(std::move(target)),
                                                               connectionEnded(false),
                                                               expectingPong(false)
{ }

IOInterface::~IOInterface()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    notify.notify_all();
    if (thread.joinable()) {
        thread.join();
    }
}

void IOInterface::write(const Util::ByteView &data)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        auto remaining = data;
        for (;;) {
            std::size_t packetSize = remaining.size();
            if (!packetSize)
                break;
            if (packetSize > 0xFFFFU)
                packetSize = 0xFFFFU;
            writePending.push_back(static_cast<std::uint8_t>(IOServer::PacketToServer::Data));
            writePending.appendNumber<quint16>(packetSize);
            writePending += remaining.mid(0, packetSize);
            remaining = remaining.mid(packetSize);
        }
    }
    notify.notify_all();
    anyControl(Util::ByteArray(data));
}

bool IOInterface::isEnded() const
{ return false; }

std::unique_ptr<IOTarget> IOInterface::target(bool deviceOnly)
{
    std::lock_guard<std::mutex> lock(mutex);
    return clientSideTarget->clone(deviceOnly);
}

QString IOInterface::description(bool deviceOnly)
{
    std::lock_guard<std::mutex> lock(mutex);
    return clientSideTarget->description(deviceOnly);
}

Data::Variant::Root IOInterface::configuration(bool deviceOnly)
{
    std::lock_guard<std::mutex> lock(mutex);
    return clientSideTarget->configuration(deviceOnly);
}

bool IOInterface::triggerAutoprobeWhenClosed()
{
    std::lock_guard<std::mutex> lock(mutex);
    return clientSideTarget->triggerAutoprobeWhenClosed();
}

bool IOInterface::integratedFraming()
{
    std::lock_guard<std::mutex> lock(mutex);
    return clientSideTarget->integratedFraming();
}

void IOInterface::start()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (connectionState != ConnectionState::Initialize)
            return;
        connectionState = ConnectionState::Connecting;
    }
    thread = std::thread(std::bind(&IOInterface::run, this));

    std::unique_lock<std::mutex> lock(mutex);
    notify.wait(lock, [this] { return connectionState == ConnectionState::Connected; });
}

static void runInThisProcess(const std::shared_ptr<std::unique_ptr<IOTarget>> &target)
{
    std::thread([target] {
        IOServer server(std::move(*target), true);
        server.start();
        server.wait();
    }).detach();
}

static bool runInSpawnedProcess(IO::Process::Spawn spawn, const IOTarget &target)
{
    spawn.arguments.emplace_back("--spawned");
    spawn.outputStream = IO::Process::Spawn::StreamMode::Forward;
    spawn.errorStream = IO::Process::Spawn::StreamMode::Forward;
#ifdef Q_OS_UNIX
    spawn.inputStream = IO::Process::Spawn::StreamMode::Capture;

    auto proc = spawn.create();
    if (!proc)
        return false;

    if (!proc->detach())
        return false;

    auto stream = proc->inputStream();
    stream->write(target.serialize());
    stream->close();
    return true;
#else
    spawn.inputStream = IO::Process::Spawn::StreamMode::Discard;
    spawn.arguments.emplace_back(target.serialize().toQByteArrayRef().toBase64().toStdString());
    return spawn.detach();
#endif
}

static std::string relativeServerProgram()
{
    QDir programDir(QCoreApplication::applicationDirPath());
    if (!programDir.exists())
        return {};
    {
        QFileInfo file(programDir.absoluteFilePath("cpd3_ioserver"));
        if (file.exists())
            return file.absoluteFilePath().toStdString();
    }
    {
        QFileInfo file(programDir.absoluteFilePath("cpd3_ioserver.exe"));
        if (file.exists())
            return file.absoluteFilePath().toStdString();
    }
    return {};
}

static bool spawnServer(const IOTarget &target)
{
    if (!target.spawnServer())
        return false;

    qCInfo(log_acquisition_iointerface) << "Starting instance for" << target.description();

    if (!::qgetenv("CPD3_IOINTERFACE_ONE_PROCESS").isEmpty()) {
        runInThisProcess(std::make_shared<std::unique_ptr<IOTarget>>(target.clone()));
        return true;
    }

    /* First try explicitly launching it from the same path as we're in */
    {
        auto prog = relativeServerProgram();
        if (!prog.empty()) {
            if (runInSpawnedProcess({prog, {}}, target))
                return true;
        }
    }

    /* Now try the system path */
    if (runInSpawnedProcess({"cpd3_ioserver", {}}, target))
        return true;

    runInThisProcess(std::make_shared<std::unique_ptr<IOTarget>>(target.clone()));
    return true;
}

bool IOInterface::handleNextPacket()
{
    if (processRead.empty())
        return false;

    switch (static_cast<IOServer::PacketToClient>(processRead.front())) {
    case IOServer::PacketToClient::Data: {
        if (processRead.size() < 3U)
            return false;
        auto size = processRead.readNumber<quint16>(1U);
        if (processRead.size() < 3U + size)
            return false;
        read(Util::ByteArray(processRead.mid(3U, size)));
        processRead.pop_front(3U + size);
        return true;
    }
    case IOServer::PacketToClient::Pong: {
        processRead.pop_front();
        expectingPong = false;
        return true;
    }
    case IOServer::PacketToClient::OtherControl: {
        if (processRead.size() < 3U)
            return false;
        auto size = processRead.readNumber<quint16>(1);
        if (processRead.size() < 3U + size)
            return false;
        {
            Util::ByteArray c(processRead.mid(3U, size));
            otherControl(c);
            anyControl(c);
        }
        processRead.pop_front(3U + size);
        return true;
    }
    case IOServer::PacketToClient::SetExclusiveResult:
        processRead.pop_front();
        return true;
    case IOServer::PacketToClient::TargetMergeResult:
        processRead.pop_front();
        return true;
    case IOServer::PacketToClient::ResetResult:
        processRead.pop_front();
        return true;
    case IOServer::PacketToClient::ReopenResult:
        processRead.pop_front();
        return true;
    case IOServer::PacketToClient::UpdateTarget: {
        if (processRead.size() < 5U)
            return false;
        auto size = processRead.readNumber<quint32>(1U);
        if (processRead.size() < 5U + size)
            return false;
        auto deserialized = IOTarget::deserialize(processRead.mid(5U, size));
        if (deserialized) {
            qCDebug(log_acquisition_iointerface) << "Received target update"
                                                 << deserialized->description();
            std::lock_guard<std::mutex> lock(mutex);
            clientSideTarget->merge(*deserialized);
        }
        processRead.pop_front(5U + size);
        return true;
    }
    case IOServer::PacketToClient::DeviceClosed:
        processRead.pop_front();
        deviceClosed();
        return true;
    default:
        break;
    }

    qCWarning(log_acquisition_iointerface) << "Invalid packet received from server ("
                                           << processRead.front() << ")";
    connectionEnded = true;
    notify.notify_all();
    return false;
}

void IOInterface::run()
{
    std::unique_ptr<IO::Socket::Connection> connection;
    int backoffCounter = 0;
    auto reconnectTime = Clock::now();
    auto spawnTime = reconnectTime + std::chrono::milliseconds(50);
    Clock::time_point pingTime;

    for (;;) {
        Util::ByteArray processControl;
        Util::ByteArray processWrite;
        {
            std::unique_lock<std::mutex> lock(mutex);
            for (;;) {
                if (!lock)
                    lock.lock();

                if (terminated) {
                    connectionState = ConnectionState::Shutdown;
                    notify.notify_all();
                    return;
                }

                switch (connectionState) {
                case ConnectionState::Connecting: {
                    if (reconnectTime > Clock::now()) {
                        notify.wait_until(lock, reconnectTime);
                        continue;
                    }
                    Q_ASSERT(!connection);
                    auto target = clientSideTarget->clone();
                    lock.unlock();

                    if (spawnTime < Clock::now()) {
                        auto sleepTime = Random::fp() * (backoffCounter / 20.0 + 0.25);
                        spawnTime = Clock::now() +
                                std::chrono::milliseconds(static_cast<int>(sleepTime * 1000.0));
                        if (spawnServer(*target)) {
                            std::this_thread::sleep_for(std::chrono::milliseconds(100));
                        }
                    }

                    connection = establishConnection(*target);
                    if (!connection) {
                        if (backoffCounter) {
                            auto sleepTime = Random::fp() * (backoffCounter / 10.0);
                            reconnectTime = Clock::now() +
                                    std::chrono::milliseconds(static_cast<int>(sleepTime * 1000.0));
                        }
                        backoffCounter++;
                        if (backoffCounter > 10)
                            backoffCounter = 10;
                        continue;
                    }

                    backoffCounter = 0;
                    pingTime = Clock::now() + std::chrono::seconds(10);

                    lock.lock();
                    connectionState = ConnectionState::Connected;
                    lock.unlock();
                    notify.notify_all();
                    continue;
                }
                case ConnectionState::Connected:
                    if (connectionEnded) {
                        connection.reset();
                        connectionState = ConnectionState::Connecting;
                        continue;
                    }
                    break;
                case ConnectionState::LockPending:
                    connectionState = ConnectionState::LockHeld;
                    lock.unlock();
                    notify.notify_all();
                    continue;
                case ConnectionState::LockHeld:
                    if (!controlPending.empty()) {
                        processControl = std::move(controlPending);
                        controlPending.clear();
                        lock.unlock();

                        connection->write(std::move(processControl));
                        continue;
                    }
                    notify.wait(lock);
                    continue;
                case ConnectionState::Shutdown:
                case ConnectionState::Initialize:
                    Q_ASSERT(false);
                    connectionState = ConnectionState::Connecting;
                    continue;
                }

                Q_ASSERT(connectionState == ConnectionState::Connected);

                processWrite = std::move(writePending);
                writePending.clear();

                processControl = std::move(controlPending);
                controlPending.clear();

                auto now = Clock::now();
                if (pingTime <= now) {
                    if (!expectingPong) {
                        processControl.push_back(
                                static_cast<std::uint8_t>(IOServer::PacketToServer::Ping));
                        expectingPong = true;
                        pongTimeout = now + std::chrono::seconds(30);
                    }
                    pingTime = now + std::chrono::seconds(10);
                }
                if (expectingPong && pongTimeout < now) {
                    connectionEnded = true;
                    notify.notify_all();

                    qCDebug(log_acquisition_iointerface) << "Ping timeout to"
                                                         << connection->describePeer();
                }

                /* Only process for reads if there's something new */
                if (!readPending.empty()) {
                    processRead += std::move(readPending);
                    readPending.clear();
                    break;
                }

                if (!processWrite.empty())
                    break;
                if (!processControl.empty())
                    break;

                if (connectionEnded)
                    continue;
                if (expectingPong && pongTimeout < pingTime) {
                    notify.wait_until(lock, pongTimeout);
                } else {
                    notify.wait_until(lock, pingTime);
                }
            }
        }

        if (!processControl.empty()) {
            connection->write(std::move(processControl));
        }
        if (!processWrite.empty()) {
            connection->write(std::move(processWrite));
        }

        while (handleNextPacket()) { }
    }
}

void IOInterface::requeueReadProcessing()
{
    /* Put the processing back at the front of pending, so the main processing loop
     * runs again */
    using std::swap;
    swap(processRead, readPending);
    readPending += std::move(processRead);
}

std::unique_ptr<IO::Socket::Connection> IOInterface::establishConnection(const IOTarget &target)
{
    auto connection = target.clientConnection();
    if (!connection)
        return {};

    readPending.clear();
    processRead.clear();
    controlPending.clear();
    connectionEnded = false;
    expectingPong = false;

    qCDebug(log_acquisition_iointerface) << "Starting connection to server socket for"
                                         << target.description();

    connection->read.connect([this](const Util::ByteArray &data) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            readPending += data;
        }
        notify.notify_all();
    });
    connection->ended.connect([this]() {
        {
            std::lock_guard<std::mutex> lock(mutex);
            connectionEnded = true;
        }
        notify.notify_all();
    });
    connection->start();

    auto timeout = Clock::now() + std::chrono::seconds(20);

    std::unique_lock<std::mutex> lock(mutex);
    while (timeout > Clock::now()) {
        if (terminated)
            return {};
        if (connectionEnded) {
            qCDebug(log_acquisition_iointerface) << "Hello to" << connection->describePeer()
                                                 << "closed";
            return {};
        }

        if (readPending.empty()) {
            notify.wait_until(lock, timeout);
            continue;
        }

        auto packetType = static_cast<IOServer::PacketToClient>(readPending.front());
        if (packetType == IOServer::PacketToClient::Hello) {
            readPending.pop_front();
            break;
        }

        qCDebug(log_acquisition_iointerface) << "Invalid hello from" << connection->describePeer();
        return {};
    }

    {
        Util::ByteArray packet;
        packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToServer::TargetMerge));
        {
            auto add = target.serialize();
            packet.appendNumber<quint32>(add.size());
            packet += std::move(add);
        }

        connection->write(std::move(packet));
    }

    while (timeout > Clock::now()) {
        if (terminated)
            return {};
        if (connectionEnded) {
            qCDebug(log_acquisition_iointerface) << "Handshake to" << connection->describePeer()
                                                 << "closed";
            return {};
        }

        if (readPending.empty()) {
            notify.wait_until(lock, timeout);
            continue;
        }

        processRead += std::move(readPending);
        readPending.clear();

        auto packetType = static_cast<IOServer::PacketToClient>(processRead.front());
        if (!handleNextPacket())
            continue;

        if (packetType == IOServer::PacketToClient::TargetMergeResult) {
            requeueReadProcessing();
            return std::move(connection);
        }
    }

    qCDebug(log_acquisition_iointerface) << "Handshake to" << connection->describePeer()
                                         << "timed out";
    return {};
}

class IOInterface::ConnectionLock final {
    IOInterface &interface;
    bool held;
public:
    explicit ConnectionLock(IOInterface &in) : interface(in), held(false)
    {
        {
            std::unique_lock<std::mutex> lock(interface.mutex);
            interface.notify.wait(lock, [this] {
                if (interface.terminated)
                    return true;
                return interface.connectionState == ConnectionState::Connected ||
                        interface.connectionState == ConnectionState::Shutdown;
            });
            if (interface.connectionState != ConnectionState::Connected)
                return;
            interface.connectionState = ConnectionState::LockPending;
        }
        interface.notify.notify_all();
        {
            std::unique_lock<std::mutex> lock(interface.mutex);
            interface.notify.wait(lock, [this] {
                if (interface.terminated || interface.connectionEnded)
                    return true;
                return interface.connectionState == ConnectionState::LockHeld ||
                        interface.connectionState == ConnectionState::Shutdown;
            });
            if (interface.connectionState != ConnectionState::LockHeld)
                return;
        }
        held = true;
    }

    ~ConnectionLock()
    {
        if (!held)
            return;
        {
            std::lock_guard<std::mutex> lock(interface.mutex);
            if (interface.connectionState != ConnectionState::LockHeld)
                return;
            interface.requeueReadProcessing();
            interface.connectionState = ConnectionState::Connected;
        }
        interface.notify.notify_all();
    }

    bool transceive(const Util::ByteView &data, IOServer::PacketToClient wait)
    {
        if (!held)
            return false;

        {
            std::lock_guard<std::mutex> lock(interface.mutex);
            interface.controlPending += data;
        }
        interface.notify.notify_all();

        auto timeout = Clock::now() + std::chrono::seconds(20);

        std::unique_lock<std::mutex> lock(interface.mutex);
        while (timeout > Clock::now()) {
            if (!lock)
                lock.lock();
            if (interface.connectionEnded) {
                qCDebug(log_acquisition_iointerface) << "Connection ended during control sequence";
                return false;
            }

            if (interface.readPending.empty()) {
                interface.notify.wait_until(lock, timeout);
                continue;
            }

            interface.processRead += std::move(interface.readPending);
            interface.readPending.clear();

            auto packetType = static_cast<IOServer::PacketToClient>(interface.processRead.front());
            lock.unlock();
            if (!interface.handleNextPacket())
                continue;

            if (packetType == wait)
                return true;
        }

        qCDebug(log_acquisition_iointerface) << "Connection timed out during control sequence";
        return false;
    }
};

void IOInterface::setExclusive(bool enable)
{
    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToServer::SetExclusive));
    if (enable) {
        packet.push_back(static_cast<std::uint8_t>(1));
    } else {
        packet.push_back(static_cast<std::uint8_t>(0));
    }

    ConnectionLock lock(*this);
    lock.transceive(packet, IOServer::PacketToClient::SetExclusiveResult);
}

void IOInterface::merge(const IOTarget &overlay)
{
    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToServer::TargetMerge));
    {
        auto add = overlay.serialize();
        packet.appendNumber<quint32>(add.size());
        packet += std::move(add);
    }

    ConnectionLock lock(*this);
    {
        std::lock_guard<std::mutex> mlock(mutex);
        clientSideTarget->merge(overlay);
    }
    lock.transceive(packet, IOServer::PacketToClient::TargetMergeResult);
}

void IOInterface::reset()
{
    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToServer::Reset));
    ConnectionLock lock(*this);
    lock.transceive(packet, IOServer::PacketToClient::ResetResult);
}

void IOInterface::reopen()
{
    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToServer::Reopen));
    ConnectionLock lock(*this);
    lock.transceive(packet, IOServer::PacketToClient::ReopenResult);
}

}
}
