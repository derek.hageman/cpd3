/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITIONBUSMANAGER_H
#define CPD3ACQUISITIONBUSMANAGER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QHash>

#include "acquisition/acquisition.hxx"
#include "datacore/segment.hxx"
#include "datacore/sequencematch.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * A class that provides a management interface to handle a bus of
 * instruments connected to a single external interface.
 */
class CPD3ACQUISITION_EXPORT BusManager {
public:
    class Device;

    /**
     * The interface used by devices to communicate with the manager.
     */
    class CPD3ACQUISITION_EXPORT DeviceInterface {
        friend class BusManager;

        friend class Device;

        BusManager *manager;
        Device *device;

        double timeoutTime;
        bool removeRequested;
        bool provisional;
        double provisionalRequest;
        double provisionalTime;

        DeviceInterface();

        DeviceInterface(BusManager *m);

        void initializeDevice(Device *dev);

    public:
        ~DeviceInterface();

        /**
         * Request a timeout at the given time.
         * 
         * @param time      the timeout time
         */
        void timeoutAt(double time);

        /**
         * Inform the manager that the device has a response queued and
         * so a next device lookup should return this device once it
         * moves to the top of the response queue.
         */
        void queueResponse();

        /**
         * Inform the manager that the queued response has been received, so
         * this device is no longer expect its next response.
         */
        void receivedResponse();

        /**
         * Clear the expected response queue.
         */
        void clearResponseQueue();

        /**
         * Request the device to be removed when possible.  This exists
         * because it is NOT safe to directly remove the device from
         * the backend during a callback initiated by the manager.
         */
        void removeDevice();

        /**
         * Request an exclusive lock on the bus that prevents any other locks
         * until it is released.  This will always return immediately, but
         * the lock may not be acquired until later.  The device's
         * lockAcquired(double) is called when the lock is actually acquired.
         */
        void lockBus();

        /**
         * Release the lock on the bus acquired with lockBus().
         */
        void unlockBus();

        /**
         * Set the device to provisional mode.
         * 
         * @param expireTimeout the time in seconds before the device is removed if it fails to be accepted
         */
        void setProvisional(double expireTimout);

        /**
         * Accept a provisional device.  This prevents it from being removed.  
         * This does nothing if the device is not provisional.
         */
        void setAccepted();
    };

    /**
     * A device on the bus.
     */
    class CPD3ACQUISITION_EXPORT Device {
        friend class BusManager;

        DeviceInterface *deviceInterface;

        Device();

    public:
        /**
         * Create a new device using the given interface.  The device is
         * not provisional initially.
         * 
         * @param interface     the device interface
         */
        Device(DeviceInterface *interface);

        virtual ~Device();

        /**
         * Get the device interface that the device was created with.
         * 
         * @return the device interface
         */
        inline DeviceInterface *interface() const
        { return deviceInterface; }

        /**
         * Set the device to provisional mode.
         * 
         * @param expireTimeout the time in seconds before the device is removed if it fails to be accepted
         */
        inline void setProvisional(double expireTimout)
        { interface()->setProvisional(expireTimout); }

        /**
         * Accept a provisional device.  This prevents it from being removed.  
         * This does nothing if the device is not provisional.
         */
        inline void setAccepted()
        { interface()->setAccepted(); }

        /**
         * Called when a lock on the bus has been acquired.
         * <br>
         * The default implementation does nothing.
         * 
         * @param time  the current time
         */
        virtual void lockAcquired(double time);

        /**
         * Called every advance while the device has a lock on the bus.
         * <br>
         * The default implementation does nothing.
         * 
         * @param time  the current time
         */
        virtual void lockHeld(double time);

        /**
         * Called when the timeout requested has occurred.
         * <br>
         * The default implementation simply calls 
         * interface()->receivedResponse()
         * 
         * @param time  the current time
         */
        virtual void incomingTimeout(double time);

        /**
         * Called when the manager has shifted control to this interface.
         * Control will immediately advance to the next interface unless
         * this one either locks the bus or requests a response.
         * <br>
         * The default implementation does nothing
         * 
         * @param time  the current time
         */
        virtual void busControlAcquired(double time);
    };

    friend class Backend;

    /**
     * The control interface to the bus backend.
     */
    class CPD3ACQUISITION_EXPORT Backend {
    public:
        virtual ~Backend();

        /**
         * Advance to the next device on the bus and return a pointer to it.
         * Should return NULL if the bus is empty.  Note that this cannot
         * return the same device twice in a single scan.
         * 
         * @return      the next device
         */
        virtual Device *advanceControl() = 0;

        /**
         * Remove the given device from the bus.  This is used for provisional
         * devices that have failed to be confirmed.
         * 
         * @param device    the device to remove
         */
        virtual void removeDevice(Device *device) = 0;

        /**
         * Request a timeout at the given time.
         * 
         * @param time      the timeout time
         */
        virtual void timeoutAt(double time) = 0;

        /**
         * Reset the simple iteration of the bus.
         */
        virtual void beginSimpleIteration() = 0;

        /**
         * Get the next device in simple iteration or NULL if all
         * devices have been iterated.
         * 
         * @return the next device in iteration or NULL if at the end
         */
        virtual Device *nextSimpleIteration() = 0;
    };

    /**
     * Create a new bus manager.
     * 
     * @param backend   the backend to use to access the contents of the bus
     */
    BusManager(Backend *backend);

    ~BusManager();

    /**
     * Create a new device interface.  This must immediately be passed to
     * a device being allocated.
     * 
     * @return  an interface for a new device
     */
    DeviceInterface *createDeviceInterface();

    /**
     * Advance the manager to the given time.
     * 
     * @param time  the current time
     */
    void advance(double time);

    /**
     * Get the device that last queued a response.
     * 
     * @return  the last device to queue a response
     */
    Device *nextResponder() const;

    /**
     * Get the device that currently has a lock on the bus.
     * 
     * @return  the device that currently has a lock
     */
    Device *busLocker() const;

private:
    Backend *backend;
    QList<DeviceInterface *> responseQueue;
    QList<DeviceInterface *> lockAcquireQueue;
    DeviceInterface *activeLock;

    void removeInterface(DeviceInterface *interface);

    bool advanceAction(double time, Device *&beginDevice);

    bool advanceRemoveAllRequested(Device *&beginDevice);

    void advanceProcessAll(double time);
};

}
}

#endif
