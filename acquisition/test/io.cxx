/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>
#include <QTcpServer>

#include "acquisition/iointerface.hxx"
#include "acquisition/ioserver.hxx"

//#define FIRST_SERIAL_PORT   "/dev/ttyUSB0"
//#define SECOND_SERIAL_PORT  "/dev/ttyUSB2"

using namespace CPD3;
using namespace CPD3::Acquisition;
using namespace CPD3::Data;

class TestIO : public QObject {
Q_OBJECT

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        QVERIFY(qputenv("CPD3_IOINTERFACE_ONE_PROCESS", QByteArray("1")));
    }

    void basicServer()
    {
        auto serverName = "CPD3Test-" + Random::string();

        std::mutex mutex;
        std::condition_variable cv;
        std::list<std::unique_ptr<IO::Socket::Connection>> incomingConnections;

        IO::Socket::Local::Server server([&](std::unique_ptr<IO::Socket::Connection> &&c) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                incomingConnections.emplace_back(std::move(c));
            }
            cv.notify_all();
        }, serverName);
        QVERIFY(server.startListening());

        Variant::Root config;
        config["Type"] = "QtLocalSocket";
        config["Name"] = serverName;

        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        IOServer ioserver(std::move(target), false);

        bool ioServerFinished = false;
        ioserver.finished.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioServerFinished = true;
            }
            cv.notify_all();
        });
        ioserver.start();
        QVERIFY(!ioserver.isFinished());

        target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        IOInterface iointerface(std::move(target));

        {
            auto cloned = iointerface.target();
            QVERIFY(cloned.get() != nullptr);
            auto check = IOTarget::create(config);
            QVERIFY(check.get() != nullptr);
            QVERIFY(cloned->matchesDevice(*check));
        }
        QVERIFY(!iointerface.description().isEmpty());
        {
            auto check = iointerface.configuration();
            QCOMPARE(check["Type"].toQString(), QString("QtLocalSocket"));
        }
        QVERIFY(!iointerface.triggerAutoprobeWhenClosed());
        QVERIFY(!iointerface.integratedFraming());

        Util::ByteArray ioInterfaceRead;
        Util::ByteArray ioInterfaceOtherControl;
        Util::ByteArray ioInterfaceAnyControl;
        bool ioInterfaceEnded = false;
        bool ioInterfaceClosed = false;
        iointerface.ended.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceEnded = true;
            }
            cv.notify_all();
        });
        iointerface.deviceClosed.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceClosed = true;
            }
            cv.notify_all();
        });
        iointerface.read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceRead += data;
            }
            cv.notify_all();
        });
        iointerface.otherControl.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceOtherControl += data;
            }
            cv.notify_all();
        });
        iointerface.anyControl.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceAnyControl += data;
            }
            cv.notify_all();
        });
        iointerface.start();
        QVERIFY(!iointerface.isEnded());

        std::unique_ptr<IO::Socket::Connection> serverConnection;
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !incomingConnections.empty(); });
            QVERIFY(!incomingConnections.empty());
            serverConnection = std::move(incomingConnections.front());
            incomingConnections.clear();
        }

        Util::ByteArray serverData;
        bool serverEnded = false;
        serverConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData += data;
            }
            cv.notify_all();
        });
        serverConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverEnded = true;
            }
            cv.notify_all();
        });
        serverConnection->start();

        QByteArray send = "SomeDataSent";
        iointerface.write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QCOMPARE(ioInterfaceAnyControl.toQByteArrayRef(), send);
            QVERIFY(ioInterfaceOtherControl.empty());
            QVERIFY(ioInterfaceRead.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!ioInterfaceEnded);
            QVERIFY(!ioInterfaceClosed);
            serverData.clear();
            ioInterfaceAnyControl.clear();
        }

        send = "OtherData";
        serverConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) ioInterfaceRead.size() >= send.size(); });
            QCOMPARE(ioInterfaceRead.toQByteArrayRef(), send);
            QVERIFY(ioInterfaceOtherControl.empty());
            QVERIFY(ioInterfaceAnyControl.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!ioInterfaceEnded);
            QVERIFY(!ioInterfaceClosed);
            ioInterfaceRead.clear();
        }

        iointerface.reopen();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return serverEnded; });
            QVERIFY(serverEnded);
        }
        serverConnection.reset();


        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !incomingConnections.empty(); });
            QVERIFY(!incomingConnections.empty());
            serverConnection = std::move(incomingConnections.front());
            incomingConnections.clear();
            serverEnded = false;
        }
        serverConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData += data;
            }
            cv.notify_all();
        });
        serverConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverEnded = true;
            }
            cv.notify_all();
        });
        serverConnection->start();

        iointerface.setExclusive(true);
        iointerface.setExclusive(false);

        send = "DataFromServer";
        serverConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) ioInterfaceRead.size() >= send.size(); });
            QCOMPARE(ioInterfaceRead.toQByteArrayRef(), send);
            QVERIFY(ioInterfaceOtherControl.empty());
            QVERIFY(ioInterfaceAnyControl.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!ioInterfaceEnded);
            QVERIFY(!ioInterfaceClosed);
            ioInterfaceRead.clear();
        }

        send = "ClientSentThisData";
        iointerface.write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QCOMPARE(ioInterfaceAnyControl.toQByteArrayRef(), send);
            QVERIFY(ioInterfaceOtherControl.empty());
            QVERIFY(ioInterfaceRead.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!ioInterfaceEnded);
            QVERIFY(!ioInterfaceClosed);
            serverData.clear();
            ioInterfaceAnyControl.clear();
        }


        serverConnection.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return ioInterfaceClosed; });
            QVERIFY(ioInterfaceClosed);
            QVERIFY(!ioInterfaceEnded);
            ioInterfaceClosed = false;
        }

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !incomingConnections.empty(); });
            QVERIFY(!incomingConnections.empty());
            serverConnection = std::move(incomingConnections.front());
            incomingConnections.clear();
            serverEnded = false;
        }
        serverConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData += data;
            }
            cv.notify_all();
        });
        serverConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverEnded = true;
            }
            cv.notify_all();
        });
        serverConnection->start();

        send = "ReconnectionData";
        serverConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) ioInterfaceRead.size() >= send.size(); });
            QCOMPARE(ioInterfaceRead.toQByteArrayRef(), send);
            QVERIFY(ioInterfaceOtherControl.empty());
            QVERIFY(ioInterfaceAnyControl.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!ioInterfaceEnded);
            QVERIFY(!ioInterfaceClosed);
            ioInterfaceRead.clear();
        }

        target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        IOInterface alternateInterface(std::move(target));
        alternateInterface.start();

        send = "SecondaryControl";
        alternateInterface.write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
        }
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) ioInterfaceOtherControl.size() >= send.size(); });
            QCOMPARE(ioInterfaceOtherControl.toQByteArrayRef(), send);
            QVERIFY(ioInterfaceRead.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!ioInterfaceEnded);
            QVERIFY(!ioInterfaceClosed);
            ioInterfaceOtherControl.clear();
        }
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) ioInterfaceAnyControl.size() >= send.size(); });
            QCOMPARE(ioInterfaceAnyControl.toQByteArrayRef(), send);
            QVERIFY(ioInterfaceRead.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!ioInterfaceEnded);
            QVERIFY(!ioInterfaceClosed);
            ioInterfaceAnyControl.clear();
        }
    }

    void spawnedServer()
    {
        auto serverName = "CPD3Test-" + Random::string();

        std::mutex mutex;
        std::condition_variable cv;
        std::list<std::unique_ptr<IO::Socket::Connection>> incomingConnections;

        IO::Socket::Local::Server server([&](std::unique_ptr<IO::Socket::Connection> &&c) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                incomingConnections.emplace_back(std::move(c));
            }
            cv.notify_all();
        }, serverName);
        QVERIFY(server.startListening());

        Variant::Root config;
        config["Type"] = "QtLocalSocket";
        config["Name"] = serverName;

        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        IOInterface iointerface(std::move(target));

        {
            auto cloned = iointerface.target();
            QVERIFY(cloned.get() != nullptr);
            auto check = IOTarget::create(config);
            QVERIFY(check.get() != nullptr);
            QVERIFY(cloned->matchesDevice(*check));
        }
        QVERIFY(!iointerface.description().isEmpty());
        {
            auto check = iointerface.configuration();
            QCOMPARE(check["Type"].toQString(), QString("QtLocalSocket"));
        }
        QVERIFY(!iointerface.triggerAutoprobeWhenClosed());
        QVERIFY(!iointerface.integratedFraming());

        Util::ByteArray ioInterfaceRead;
        Util::ByteArray ioInterfaceOtherControl;
        Util::ByteArray ioInterfaceAnyControl;
        bool ioInterfaceEnded = false;
        bool ioInterfaceClosed = false;
        iointerface.ended.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceEnded = true;
            }
            cv.notify_all();
        });
        iointerface.deviceClosed.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceClosed = true;
            }
            cv.notify_all();
        });
        iointerface.read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceRead += data;
            }
            cv.notify_all();
        });
        iointerface.otherControl.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceOtherControl += data;
            }
            cv.notify_all();
        });
        iointerface.anyControl.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceAnyControl += data;
            }
            cv.notify_all();
        });
        iointerface.start();
        QVERIFY(!iointerface.isEnded());

        std::unique_ptr<IO::Socket::Connection> serverConnection;
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !incomingConnections.empty(); });
            QVERIFY(!incomingConnections.empty());
            serverConnection = std::move(incomingConnections.front());
            incomingConnections.clear();
        }

        Util::ByteArray serverData;
        bool serverEnded = false;
        serverConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData += data;
            }
            cv.notify_all();
        });
        serverConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverEnded = true;
            }
            cv.notify_all();
        });
        serverConnection->start();

        QByteArray send = "SomeDataSent";
        iointerface.write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QCOMPARE(ioInterfaceAnyControl.toQByteArrayRef(), send);
            QVERIFY(ioInterfaceOtherControl.empty());
            QVERIFY(ioInterfaceRead.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!ioInterfaceEnded);
            QVERIFY(!ioInterfaceClosed);
            serverData.clear();
            ioInterfaceAnyControl.clear();
        }

        send = "OtherData";
        serverConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) ioInterfaceRead.size() >= send.size(); });
            QCOMPARE(ioInterfaceRead.toQByteArrayRef(), send);
            QVERIFY(ioInterfaceOtherControl.empty());
            QVERIFY(ioInterfaceAnyControl.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!ioInterfaceEnded);
            QVERIFY(!ioInterfaceClosed);
            ioInterfaceRead.clear();
        }
    }

    void remoteServer()
    {
        auto serverName = "CPD3Test-" + Random::string();

        quint16 port = 0;
        {
            QTcpServer svr;
            svr.listen();
            port = svr.serverPort();
        }

        std::mutex mutex;
        std::condition_variable cv;
        std::list<std::unique_ptr<IO::Socket::Connection>> incomingConnections;

        IO::Socket::Local::Server server([&](std::unique_ptr<IO::Socket::Connection> &&c) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                incomingConnections.emplace_back(std::move(c));
            }
            cv.notify_all();
        }, serverName);
        QVERIFY(server.startListening());

        Variant::Root config;
        config["Type"] = "QtLocalSocket";
        config["Name"] = serverName;
        config["DisableLocal"].setBoolean(true);
        config["External/Port"].setInteger(port);

        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        IOServer ioserver(std::move(target), false);
        ioserver.start();
        QVERIFY(!ioserver.isFinished());

        config.write().setEmpty();
        config["Type"] = "CPD3Server";
        config["Port"].setInteger(port);

        target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        IOInterface iointerface(std::move(target));

        Util::ByteArray ioInterfaceRead;
        Util::ByteArray ioInterfaceOtherControl;
        Util::ByteArray ioInterfaceAnyControl;
        bool ioInterfaceEnded = false;
        bool ioInterfaceClosed = false;
        iointerface.ended.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceEnded = true;
            }
            cv.notify_all();
        });
        iointerface.deviceClosed.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceClosed = true;
            }
            cv.notify_all();
        });
        iointerface.read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceRead += data;
            }
            cv.notify_all();
        });
        iointerface.otherControl.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceOtherControl += data;
            }
            cv.notify_all();
        });
        iointerface.anyControl.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceAnyControl += data;
            }
            cv.notify_all();
        });
        iointerface.start();
        QVERIFY(!iointerface.isEnded());

        std::unique_ptr<IO::Socket::Connection> serverConnection;
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !incomingConnections.empty(); });
            QVERIFY(!incomingConnections.empty());
            serverConnection = std::move(incomingConnections.front());
            incomingConnections.clear();
        }

        Util::ByteArray serverData;
        bool serverEnded = false;
        serverConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData += data;
            }
            cv.notify_all();
        });
        serverConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverEnded = true;
            }
            cv.notify_all();
        });
        serverConnection->start();

        QByteArray send = "SomeDataSent";
        iointerface.write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QCOMPARE(ioInterfaceAnyControl.toQByteArrayRef(), send);
            QVERIFY(ioInterfaceOtherControl.empty());
            QVERIFY(ioInterfaceRead.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!ioInterfaceEnded);
            QVERIFY(!ioInterfaceClosed);
            serverData.clear();
            ioInterfaceAnyControl.clear();
        }

        send = "OtherData";
        serverConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) ioInterfaceRead.size() >= send.size(); });
            QCOMPARE(ioInterfaceRead.toQByteArrayRef(), send);
            QVERIFY(ioInterfaceOtherControl.empty());
            QVERIFY(ioInterfaceAnyControl.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!ioInterfaceEnded);
            QVERIFY(!ioInterfaceClosed);
            ioInterfaceRead.clear();
        }
    }

#if defined(FIRST_SERIAL_PORT) && defined(SECOND_SERIAL_PORT)

    void nullModemLoopback()
    {
        std::mutex mutex;
        std::condition_variable cv;

        Variant::Root config1;
        config1["Type"] = "SerialPort";
        config1["Port"] = FIRST_SERIAL_PORT;
        config1["Baud"] = 9600;
        Variant::Root config2;
        config2["Type"] = "SerialPort";
        config2["Port"] = SECOND_SERIAL_PORT;
        config2["Baud"] = 9600;

        auto target = IOTarget::create(config1);
        QVERIFY(target.get() != nullptr);
        IOServer ioserver1(std::move(target), false);
        ioserver1.start();
        QVERIFY(!ioserver1.isFinished());

        target = IOTarget::create(config2);
        QVERIFY(target.get() != nullptr);
        IOServer ioserver2(std::move(target), false);
        ioserver2.start();
        QVERIFY(!ioserver2.isFinished());

        QTest::qSleep(500);

        target = IOTarget::create(config1);
        QVERIFY(target.get() != nullptr);
        IOInterface iointerface1(std::move(target));
        Util::ByteArray read1;
        iointerface1.read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                read1 += data;
            }
            cv.notify_all();
        });
        iointerface1.start();
        QVERIFY(!iointerface1.isEnded());

        target = IOTarget::create(config2);
        QVERIFY(target.get() != nullptr);
        IOInterface iointerface2(std::move(target));
        Util::ByteArray read2;
        iointerface2.read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                read2 += data;
            }
            cv.notify_all();
        });
        iointerface2.start();
        QVERIFY(!iointerface2.isEnded());

        QTest::qSleep(500);

        {
            std::lock_guard<std::mutex> lock(mutex);
            read1.clear();
            read2.clear();
        }

        QByteArray send = "SomeDataSent";
        iointerface1.write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) read2.size() >= send.size(); });
            QCOMPARE(read2.toQByteArrayRef(), send);
            QVERIFY(read1.empty());
            read2.clear();
        }

        send = "SecondData";
        iointerface2.write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) read1.size() >= send.size(); });
            QCOMPARE(read1.toQByteArrayRef(), send);
            QVERIFY(read2.empty());
            read1.clear();
        }

        iointerface1.reset();
        QTest::qSleep(500);
        {
            std::lock_guard<std::mutex> lock(mutex);
            read1.clear();
            read2.clear();
        }

        send = "PostResetData";
        iointerface1.write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) read2.size() >= send.size(); });
            QCOMPARE(read2.toQByteArrayRef(), send);
            QVERIFY(read1.empty());
            read2.clear();
        }

        Variant::Root reconfigure;
        config1["Type"] = "SerialPort";
        config1["Baud"] = 19200;

        target = IOTarget::create(reconfigure);
        QVERIFY(target.get() != nullptr);
        iointerface1.merge(*target);
        iointerface2.merge(*target);

        QTest::qSleep(500);
        {
            std::lock_guard<std::mutex> lock(mutex);
            read1.clear();
            read2.clear();
        }

        send = "ReconfigureData1";
        iointerface1.write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) read2.size() >= send.size(); });
            QCOMPARE(read2.toQByteArrayRef(), send);
            QVERIFY(read1.empty());
            read2.clear();
        }

        send = "ReconfigureData2";
        iointerface2.write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) read1.size() >= send.size(); });
            QCOMPARE(read1.toQByteArrayRef(), send);
            QVERIFY(read2.empty());
            read1.clear();
        }
    }

#endif

    void multiplexer()
    {
        std::mutex mutex;
        std::condition_variable cv;

        auto serverName1 = "CPD3Test-" + Random::string();
        std::list<std::unique_ptr<IO::Socket::Connection>> incomingConnections1;
        IO::Socket::Local::Server server1([&](std::unique_ptr<IO::Socket::Connection> &&c) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                incomingConnections1.emplace_back(std::move(c));
            }
            cv.notify_all();
        }, serverName1);
        QVERIFY(server1.startListening());

        auto serverName2 = "CPD3Test-" + Random::string();
        std::list<std::unique_ptr<IO::Socket::Connection>> incomingConnections2;
        IO::Socket::Local::Server server2([&](std::unique_ptr<IO::Socket::Connection> &&c) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                incomingConnections2.emplace_back(std::move(c));
            }
            cv.notify_all();
        }, serverName2);
        QVERIFY(server2.startListening());

        auto serverName3 = "CPD3Test-" + Random::string();
        std::list<std::unique_ptr<IO::Socket::Connection>> incomingConnections3;
        IO::Socket::Local::Server server3([&](std::unique_ptr<IO::Socket::Connection> &&c) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                incomingConnections3.emplace_back(std::move(c));
            }
            cv.notify_all();
        }, serverName3);
        QVERIFY(server3.startListening());

        Variant::Root config;
        config["Type"] = "Multiplexer";
        config["Targets/Input"].setFlags({"Input"});
        config["Targets/Output"].setFlags({"Output"});
        config["Targets/Echo"].setFlags({"Echo"});
        config["Interfaces/#0/Type"] = "QtLocalSocket";
        config["Interfaces/#0/Name"] = serverName1;
        config["Interfaces/#0/Channels/Input"].setFlags({"Input"});
        config["Interfaces/#1/Type"] = "QtLocalSocket";
        config["Interfaces/#1/Name"] = serverName2;
        config["Interfaces/#1/Channels/Output"].setFlags({"Output"});
        config["Interfaces/#2/Type"] = "QtLocalSocket";
        config["Interfaces/#2/Name"] = serverName3;
        config["Interfaces/#2/Channels/Input"].setFlags({"Input", "Echo"});
        config["Interfaces/#2/Channels/Output"].setFlags({"Output"});

        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        IOServer ioserver(std::move(target), false);
        ioserver.start();
        QVERIFY(!ioserver.isFinished());

        std::unique_ptr<IO::Socket::Connection> serverConnection1;
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !incomingConnections1.empty(); });
            QVERIFY(!incomingConnections1.empty());
            serverConnection1 = std::move(incomingConnections1.front());
            incomingConnections1.clear();
        }
        Util::ByteArray serverData1;
        serverConnection1->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData1 += data;
            }
            cv.notify_all();
        });
        serverConnection1->start();

        std::unique_ptr<IO::Socket::Connection> serverConnection2;
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !incomingConnections2.empty(); });
            QVERIFY(!incomingConnections2.empty());
            serverConnection2 = std::move(incomingConnections2.front());
            incomingConnections2.clear();
        }
        Util::ByteArray serverData2;
        serverConnection2->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData2 += data;
            }
            cv.notify_all();
        });
        serverConnection2->start();

        std::unique_ptr<IO::Socket::Connection> serverConnection3;
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !incomingConnections3.empty(); });
            QVERIFY(!incomingConnections3.empty());
            serverConnection3 = std::move(incomingConnections3.front());
            incomingConnections3.clear();
        }
        Util::ByteArray serverData3;
        serverConnection3->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData3 += data;
            }
            cv.notify_all();
        });
        serverConnection3->start();

        target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        IOInterface iointerface(std::move(target));

        Util::ByteArray ioInterfaceRead;
        Util::ByteArray ioInterfaceOtherControl;
        iointerface.read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceRead += data;
            }
            cv.notify_all();
        });
        iointerface.otherControl.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ioInterfaceOtherControl += data;
            }
            cv.notify_all();
        });
        iointerface.start();
        QVERIFY(!iointerface.isEnded());

        QByteArray send = "FirstData";
        serverConnection1->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) ioInterfaceRead.size() >= send.size(); });
            QCOMPARE(ioInterfaceRead.toQByteArrayRef(), send);
            QVERIFY(ioInterfaceOtherControl.empty());
            QVERIFY(serverData1.empty());
            QVERIFY(serverData2.empty());
            QVERIFY(serverData3.empty());
            ioInterfaceRead.clear();
        }

        serverConnection2->write("DISCARD");

        send = "SecondData";
        iointerface.write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData2.size() >= send.size(); });
            cv.wait(lock, [&] { return (int) serverData3.size() >= send.size(); });
            QCOMPARE(serverData2.toQByteArrayRef(), send);
            QCOMPARE(serverData3.toQByteArrayRef(), send);
            QVERIFY(ioInterfaceRead.empty());
            QVERIFY(ioInterfaceOtherControl.empty());
            QVERIFY(serverData1.empty());
            serverData2.clear();
            serverData3.clear();
        }

        send = "ThirdData";
        serverConnection3->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) ioInterfaceRead.size() >= send.size(); });
            cv.wait(lock, [&] { return (int) ioInterfaceOtherControl.size() >= send.size(); });
            QCOMPARE(ioInterfaceRead.toQByteArrayRef(), send);
            QCOMPARE(ioInterfaceOtherControl.toQByteArrayRef(), send);
            QVERIFY(serverData1.empty());
            QVERIFY(serverData2.empty());
            QVERIFY(serverData3.empty());
            ioInterfaceRead.clear();
            ioInterfaceOtherControl.clear();
        }
    }
};

QTEST_MAIN(TestIO)

#include "io.moc"
