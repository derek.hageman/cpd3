/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <mutex>
#include <QtGlobal>
#include <QTest>

#include "acquisition/lineingress.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

Q_DECLARE_METATYPE(ComponentOptions);

class DataBlock {
public:
    Util::ByteArray data;
    double time;

    DataBlock() : time(FP::undefined())
    { }

    DataBlock(Util::ByteArray d, double t) : data(std::move(d)), time(t)
    { }

    DataBlock(const char *d, double t) : data(d), time(t)
    { }

    bool operator==(const DataBlock &other) const
    { return data == other.data && FP::equal(time, other.time); }
};

Q_DECLARE_METATYPE(DataBlock);

Q_DECLARE_METATYPE(QList<DataBlock>);

Q_DECLARE_METATYPE(QList<QByteArray>);

namespace QTest {
template<>
char *toString(const QList<DataBlock> &output)
{
    QByteArray ba;
    for (int i = 0, max = output.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        if (FP::defined(output.at(i).time))
            ba += QByteArray::number(output.at(i).time, 'f', 0);
        else
            ba += "NA";
        ba += "=\"";
        ba += output.at(i).data;
        ba += "\"";
    }
    return qstrdup(ba.data());
}
}

class TestAcquisitionInterface : public AcquisitionInterface {
    bool terminated;
    std::mutex mutex;
public:
    QList<DataBlock> output;

    TestAcquisitionInterface() : terminated(false)
    { }

    virtual ~TestAcquisitionInterface()
    { }

    virtual void setControlStream(AcquisitionControlStream *controlStream)
    {
        Q_UNUSED(controlStream);
    }

    virtual void setState(AcquisitionState *state)
    {
        Q_UNUSED(state);
    }

    virtual void setRealtimeEgress(StreamSink *egress)
    {
        Q_UNUSED(egress);
    }

    virtual void setLoggingEgress(StreamSink *egress)
    {
        if (egress != NULL) {
            egress->incomingData(
                    SequenceValue({{}, "raw", "ZVar"}, Variant::Root(1.0), 100.0, 200.0));
            egress->endData();
        }
    }

    virtual void incomingData(const Util::ByteView &data, double time, IncomingDataType type)
    {
        Q_UNUSED(type);
        if (data.empty())
            return;
        output.append(DataBlock(Util::ByteArray(data), time));
    }

    virtual void signalTerminate()
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            terminated = true;
        }
        emit completed();
    }

    virtual bool isCompleted()
    {
        std::lock_guard<std::mutex> lock(mutex);
        return terminated;
    }

};

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestLineIngress : public QObject {
Q_OBJECT
private slots:

    void generalWrapper()
    {
        QFETCH(ComponentOptions, options);
        QFETCH(bool, allowUndefinedTime);
        QFETCH(QList<QByteArray>, input);
        QFETCH(QList<DataBlock>, expected);

        SequenceValue::Transfer expectedValues
                {SequenceValue(SequenceName("nil", "raw", "ZVar_X1"), Variant::Root(1.0), 100.0,
                               200.0)};

        TestAcquisitionInterface *interface = new TestAcquisitionInterface;
        LineIngressWrapper *wrapper =
                LineIngressWrapper::create(std::unique_ptr<AcquisitionInterface>(interface),
                                           options, allowUndefinedTime);
        StreamSink::Buffer ingress;

        wrapper->start();
        wrapper->setEgress(&ingress);
        for (const auto &add : input) {
            wrapper->incomingData(add);
        }
        wrapper->endData();
        wrapper->wait();

        QCOMPARE(interface->output, expected);
        delete wrapper;
        QCOMPARE(ingress.values(), expectedValues);
        ingress.reset();

        interface = new TestAcquisitionInterface;
        wrapper = LineIngressWrapper::create(std::unique_ptr<AcquisitionInterface>(interface),
                                             options, allowUndefinedTime);

        wrapper->start();
        QTest::qSleep(50);
        wrapper->setEgress(&ingress);
        QTest::qSleep(50);
        for (const auto &add : input) {
            wrapper->incomingData(add);
            QTest::qSleep(50);
        }
        wrapper->endData();
        wrapper->wait();

        QCOMPARE(interface->output, expected);
        delete wrapper;
        QCOMPARE(ingress.values(), expectedValues);
    }

    void generalWrapper_data()
    {
        QTest::addColumn<ComponentOptions>("options");
        QTest::addColumn<bool>("allowUndefinedTime");
        QTest::addColumn<QList<QByteArray> >("input");
        QTest::addColumn<QList<DataBlock> >("expected");

        ComponentOptions options;
        LineIngressWrapper::addOptions(options);

        QTest::newRow("Empty") << options << false << (QList<QByteArray>()) << (QList<DataBlock>());

        QTest::newRow("Default CSV single line") << options << false
                                                 << (QList<QByteArray>() << "2012:100,ABC")
                                                 << (QList<DataBlock>()
                                                         << DataBlock("ABC", 1333929600));
        QTest::newRow("Default CSV single line embeded") << options << false << (QList<QByteArray>()
                << "2012:100,ABC,CD") << (QList<DataBlock>() << DataBlock("ABC,CD", 1333929600));
        QTest::newRow("Default CSV") << options << false
                                     << (QList<QByteArray>() << "2012:100,ABC\n2012:101,CDE\n")
                                     << (QList<DataBlock>() << DataBlock("ABC", 1333929600)
                                                            << DataBlock("CDE", 1334016000));
        QTest::newRow("Default CSV split") << options << false
                                           << (QList<QByteArray>() << "2012:100" << ",ABC\n"
                                                                   << "2012:101,CDE\n")
                                           << (QList<DataBlock>() << DataBlock("ABC", 1333929600)
                                                                  << DataBlock("CDE", 1334016000));

        options = ComponentOptions();
        LineIngressWrapper::addOptions(options);
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("singletime");
        QTest::newRow("Single time CSV single line") << options << false
                                                     << (QList<QByteArray>() << "2012:100,ABC")
                                                     << (QList<DataBlock>()
                                                             << DataBlock("ABC", 1333929600));
        QTest::newRow("Single time CSV single line escaped") << options << false
                                                             << (QList<QByteArray>()
                                                                     << "2012:100,\"ABC,CD\",EF")
                                                             << (QList<DataBlock>()
                                                                     << DataBlock("ABC,CD",
                                                                                  1333929600));
        QTest::newRow("Single time CSV") << options << false
                                         << (QList<QByteArray>() << "2012:100,ABC\n2012:101,CDE\n")
                                         << (QList<DataBlock>() << DataBlock("ABC", 1333929600)
                                                                << DataBlock("CDE", 1334016000));
        QTest::newRow("Single time CSV split") << options << false
                                               << (QList<QByteArray>() << "2012:100" << ",ABC\n"
                                                                       << "2012:101,CDE\n")
                                               << (QList<DataBlock>()
                                                       << DataBlock("ABC", 1333929600)
                                                       << DataBlock("CDE", 1334016000));

        options = ComponentOptions();
        LineIngressWrapper::addOptions(options);
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("twotime");
        QTest::newRow("Double time CSV single line") << options << false
                                                     << (QList<QByteArray>() << "2012,100,ABC")
                                                     << (QList<DataBlock>()
                                                             << DataBlock("ABC", 1333929600));
        QTest::newRow("Double time CSV") << options << false
                                         << (QList<QByteArray>() << "2012,100,ABC\n2012,101,CDE\n")
                                         << (QList<DataBlock>() << DataBlock("ABC", 1333929600)
                                                                << DataBlock("CDE", 1334016000));
        QTest::newRow("Double time CSV split") << options << false
                                               << (QList<QByteArray>() << "2012,100" << ",ABC\n"
                                                                       << "2012,101,CDE\n")
                                               << (QList<DataBlock>()
                                                       << DataBlock("ABC", 1333929600)
                                                       << DataBlock("CDE", 1334016000));

        options = ComponentOptions();
        LineIngressWrapper::addOptions(options);
        qobject_cast<ComponentOptionSingleInteger *>(options.get("record-index"))->set(3);
        QTest::newRow("Default CSV skip field") << options << false
                                                << (QList<QByteArray>() << "2012:100,QWE,ABC")
                                                << (QList<DataBlock>()
                                                        << DataBlock("ABC", 1333929600));

        options = ComponentOptions();
        LineIngressWrapper::addOptions(options);
        qobject_cast<ComponentOptionSingleInteger *>(options.get("time-index"))->set(2);
        qobject_cast<ComponentOptionSingleInteger *>(options.get("record-index"))->set(3);
        QTest::newRow("Default CSV time field") << options << false
                                                << (QList<QByteArray>() << "QWE,2012:100,ABC")
                                                << (QList<DataBlock>()
                                                        << DataBlock("ABC", 1333929600));

        options = ComponentOptions();
        LineIngressWrapper::addOptions(options);
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("singletime");
        qobject_cast<ComponentOptionSingleInteger *>(options.get("time-index"))->set(2);
        qobject_cast<ComponentOptionSingleInteger *>(options.get("record-index"))->set(3);
        QTest::newRow("Single time CSV relocate") << options << false
                                                  << (QList<QByteArray>() << "QWE,2012:100,ABC,ZDF")
                                                  << (QList<DataBlock>()
                                                          << DataBlock("ABC", 1333929600));

        options = ComponentOptions();
        LineIngressWrapper::addOptions(options);
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("twotime");
        qobject_cast<ComponentOptionSingleInteger *>(options.get("time-index"))->set(2);
        qobject_cast<ComponentOptionSingleInteger *>(options.get("time-index2"))->set(3);
        qobject_cast<ComponentOptionSingleInteger *>(options.get("record-index"))->set(1);
        QTest::newRow("Double time CSV relocate") << options << false
                                                  << (QList<QByteArray>() << "ABC,2012,100")
                                                  << (QList<DataBlock>()
                                                          << DataBlock("ABC", 1333929600));

        options = ComponentOptions();
        LineIngressWrapper::addOptions(options);
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("cpd1");
        QTest::newRow("CPD1 raw data") << options << false
                                       << (QList<QByteArray>() << "STN,Year,DOY,Port,Data\n"
                                                               << "NIL,2012,100,/dev/ttyS0,ABC")
                                       << (QList<DataBlock>() << DataBlock("ABC", 1333929600));

        options = ComponentOptions();
        LineIngressWrapper::addOptions(options);
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("cpd2");
        QTest::newRow("CPD2 raw data") << options << false << (QList<QByteArray>()
                << "!row;colhdr;X1r,X1r;STN;EPOCH;DateTime;ZReport_X1\n"
                << "!row;mvc;X1r,X1r;ZZZ;0;9999-99-99T99:99:99Z;Z\n"
                << "X1r,NIL,1333929600,2012-04-09T00:00:00Z,ABC\n")
                                       << (QList<DataBlock>() << DataBlock("ABC", 1333929600));
    }
};

QTEST_APPLESS_MAIN(TestLineIngress)

#include "lineingress.moc"
