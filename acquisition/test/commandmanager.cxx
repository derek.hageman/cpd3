/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>

#include "acquisition/commandmanager.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/root.hxx"

using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class TestCommandManager : public QObject {
Q_OBJECT

private slots:

    void basic()
    {
        CommandManager manager;

        Variant::Write add = Variant::Write::empty();
        add.hash("Command1").hash("Exclude").array(0).hash("Type").setString("Flags");
        add.hash("Command1")
           .hash("Exclude")
           .array(0)
           .hash("Flags").setFlags({"Flag1"});
        add.hash("Command1").hash("Exclude").array(0).hash("Variable").setString("F1_X1");
        add.hash("Command2").setBool(true);
        add.hash("Command3").hash("Include").array(0).hash("Type").setString("LessThan");
        add.hash("Command3").hash("Include").array(0).hash("Value").setDouble(1.0);
        add.hash("Command3").hash("Include").array(0).hash("Variable").setString("Q1_X1");
        add.hash("AG1").hash("Aggregate").setBool(true);
        manager.updateCommands("X1", add);

        add.setEmpty();
        add.hash("Command4").setBool(true);
        add.hash("AG1").hash("Aggregate").setBool(true);
        manager.updateCommands("X2", add);

        manager.incomingData(SequenceValue({"bnd", "rt_instant", "F1_X1"},
                                           Variant::Root(Variant::Flags{"Flag1"})));
        manager.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(0.5)));

        auto check = manager.getCommands("X1").read();
        QVERIFY(!check.hash("Command1").exists());
        QVERIFY(check.hash("Command2").exists());
        QVERIFY(check.hash("Command3").exists());

        check = manager.getCommands("X2").read();
        QVERIFY(check.hash("Command4").exists());

        auto aggregated = manager.getAggregateCommands();
        QCOMPARE((int) aggregated.size(), 1);
        QCOMPARE(aggregated.at(0).read().hash("Command").toString(), std::string("AG1"));


        manager.incomingData(
                SequenceValue({"bnd", "rt_instant", "F1_X1"}, Variant::Root(Variant::Flags())));
        check = manager.getCommands("X1").read();
        QVERIFY(check.hash("Command1").exists());
        QVERIFY(check.hash("Command2").exists());
        QVERIFY(check.hash("Command3").exists());


        manager.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(1.5)));
        check = manager.getCommands("X1").read();
        QVERIFY(check.hash("Command1").exists());
        QVERIFY(check.hash("Command2").exists());
        QVERIFY(!check.hash("Command3").exists());


        manager.incomingData(SequenceValue({"bnd", "rt_instant", "F1_X1"},
                                           Variant::Root(Variant::Flags{"Flag1"})));
        check = manager.getCommands("X1").read();
        QVERIFY(!check.hash("Command1").exists());
        QVERIFY(check.hash("Command2").exists());
        QVERIFY(!check.hash("Command3").exists());
    }

    void manual()
    {
        CommandManager manager;

        Variant::Write add = Variant::Write::empty();
        add.hash("Command1").hash("Exclude").array(0).hash("Type").setString("Flags");
        add.hash("Command1")
           .hash("Exclude")
           .array(0)
           .hash("Flags").setFlags({"Flag1"});
        add.hash("Command1").hash("Exclude").array(0).hash("Variable").setString("F1_X1");
        add.hash("Command2").setBool(true);
        add.hash("Command3").hash("Include").array(0).hash("Type").setString("LessThan");
        add.hash("Command3").hash("Include").array(0).hash("Value").setDouble(1.0);
        add.hash("Command3").hash("Include").array(0).hash("Variable").setString("Q1_X1");
        add.hash("AG1").hash("Aggregate").setBool(true);
        manager.updateManual("X1", add);

        add.setEmpty();
        add.hash("Command4").setBool(true);
        add.hash("AG1").hash("Aggregate").setBool(true);
        manager.updateManual("X2", add);

        manager.incomingData(SequenceValue({"bnd", "rt_instant", "F1_X1"},
                                           Variant::Root(Variant::Flags{"Flag1"})));
        manager.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(0.5)));

        auto check = manager.getManual("X1").read();
        QVERIFY(!check.hash("Command1").exists());
        QVERIFY(check.hash("Command2").exists());
        QVERIFY(check.hash("Command3").exists());

        check = manager.getManual("X2").read();
        QVERIFY(check.hash("Command4").exists());

        auto aggregated = manager.getAggregateCommands();
        QCOMPARE((int) aggregated.size(), 1);
        QCOMPARE(aggregated.at(0).read().hash("Command").toString(), std::string("AG1"));


        manager.incomingData(
                SequenceValue({"bnd", "rt_instant", "F1_X1"}, Variant::Root(Variant::Flags())));
        check = manager.getManual("X1").read();
        QVERIFY(check.hash("Command1").exists());
        QVERIFY(check.hash("Command2").exists());
        QVERIFY(check.hash("Command3").exists());


        manager.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(1.5)));
        check = manager.getManual("X1").read();
        QVERIFY(check.hash("Command1").exists());
        QVERIFY(check.hash("Command2").exists());
        QVERIFY(!check.hash("Command3").exists());


        manager.incomingData(SequenceValue({"bnd", "rt_instant", "F1_X1"},
                                           Variant::Root(Variant::Flags{"Flag1"})));
        check = manager.getManual("X1").read();
        QVERIFY(!check.hash("Command1").exists());
        QVERIFY(check.hash("Command2").exists());
        QVERIFY(!check.hash("Command3").exists());
    }

    void combined()
    {
        CommandManager manager;

        Variant::Write add = Variant::Write::empty();
        add.hash("Command1").hash("Exclude").array(0).hash("Type").setString("Flags");
        add.hash("Command1")
           .hash("Exclude")
           .array(0)
           .hash("Flags").setFlags({"Flag1"});
        add.hash("Command1").hash("Exclude").array(0).hash("Variable").setString("F1_X1");
        add.hash("Command2").setBool(true);
        add.hash("Command3").hash("Include").array(0).hash("Type").setString("LessThan");
        add.hash("Command3").hash("Include").array(0).hash("Value").setDouble(1.0);
        add.hash("Command3").hash("Include").array(0).hash("Variable").setString("Q1_X1");
        add.hash("AG1").hash("Aggregate").setBool(true);
        manager.updateManual("X1", add);

        add.setEmpty();
        add.hash("Command4").setBool(true);
        add.hash("AG1").hash("Aggregate").setBool(true);
        manager.updateCommands("X2", add);

        manager.incomingData(SequenceValue({"bnd", "rt_instant", "F1_X1"},
                                           Variant::Root(Variant::Flags{"Flag1"})));
        manager.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(0.5)));

        auto check = manager.getManual("X1").read();
        QVERIFY(!check.hash("Command1").exists());
        QVERIFY(check.hash("Command2").exists());
        QVERIFY(check.hash("Command3").exists());

        check = manager.getCommands("X2").read();
        QVERIFY(check.hash("Command4").exists());

        auto aggregated = manager.getAggregateCommands();
        QCOMPARE((int) aggregated.size(), 1);
        QCOMPARE(aggregated.at(0).read().hash("Command").toString(), std::string("AG1"));


        manager.incomingData(
                SequenceValue({"bnd", "rt_instant", "F1_X1"}, Variant::Root(Variant::Flags())));
        check = manager.getManual("X1").read();
        QVERIFY(check.hash("Command1").exists());
        QVERIFY(check.hash("Command2").exists());
        QVERIFY(check.hash("Command3").exists());


        manager.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(1.5)));
        check = manager.getManual("X1").read();
        QVERIFY(check.hash("Command1").exists());
        QVERIFY(check.hash("Command2").exists());
        QVERIFY(!check.hash("Command3").exists());


        manager.incomingData(SequenceValue({"bnd", "rt_instant", "F1_X1"},
                                           Variant::Root(Variant::Flags{"Flag1"})));
        check = manager.getManual("X1").read();
        QVERIFY(!check.hash("Command1").exists());
        QVERIFY(check.hash("Command2").exists());
        QVERIFY(!check.hash("Command3").exists());

        QVERIFY(!manager.getManual("X2").read().exists());
        QVERIFY(!manager.getCommands("X1").read().exists());
    }
};

QTEST_APPLESS_MAIN(TestCommandManager)

#include "commandmanager.moc"
