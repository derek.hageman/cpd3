/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>

#include "acquisition/framedinstrument.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Acquisition;

class DataBlock {
public:
    Util::ByteArray data;
    double time;

    DataBlock() : time(FP::undefined())
    { }

    DataBlock(Util::ByteArray d, double t) : data(std::move(d)), time(t)
    { }

    DataBlock(const char *d, double t) : data(d), time(t)
    { }

    bool operator==(const DataBlock &other) const
    { return data == other.data && time == other.time; }
};

Q_DECLARE_METATYPE(DataBlock)

Q_DECLARE_METATYPE(QList<DataBlock>)

namespace QTest {
template<>
char *toString(const QList<DataBlock> &output)
{
    QByteArray ba;
    for (int i = 0, max = output.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        ba += QByteArray::number(output.at(i).time);
        ba += "=\"";
        ba += output.at(i).data;
        ba += "\"";
    }
    return qstrdup(ba.data());
}
}

class IncomingBlock : public DataBlock {
public:
    AcquisitionInterface::IncomingDataType type;

    IncomingBlock() : DataBlock(), type(AcquisitionInterface::IncomingDataType::Stream)
    { }

    IncomingBlock(const QByteArray &d,
                  double t,
                  AcquisitionInterface::IncomingDataType ty = AcquisitionInterface::IncomingDataType::Stream)
            : DataBlock(d, t), type(ty)
    { }
};

Q_DECLARE_METATYPE(IncomingBlock)

Q_DECLARE_METATYPE(QList<IncomingBlock>)

class InstrumentImplementation : public FramedInstrument {
public:
    InstrumentImplementation() : FramedInstrument("test")
    { }

    virtual ~InstrumentImplementation()
    { }

    QList<DataBlock> resultData;
    QList<DataBlock> resultControl;

protected:
    virtual void incomingDataFrame(const Util::ByteArray &frame, double time)
    {
        resultData.append(DataBlock(frame, time));
    }

    virtual void incomingControlFrame(const Util::ByteArray &frame, double time)
    {
        resultControl.append(DataBlock(frame, time));
    }
};

class InstrumentImplementationTiming : public InstrumentImplementation {
public:
    InstrumentImplementationTiming()
    { }

    virtual ~InstrumentImplementationTiming()
    { }

    double startDataDiscardTime;
    int startDiscardDataFrames;
    double startControlDiscardTime;
    int startDiscardControlFrames;
    double startTimeoutTime;

    double discardDataTime;
    double discardControlTime;
    double timeoutTime;

protected:
    virtual void incomingDataFrame(const Util::ByteArray &frame, double time)
    {
        InstrumentImplementation::incomingDataFrame(frame, time);
        if (FP::defined(startDataDiscardTime) || startDiscardDataFrames > 0) {
            discardData(startDataDiscardTime, startDiscardDataFrames);
            startDataDiscardTime = FP::undefined();
            startDiscardDataFrames = 0;
        }
        if (FP::defined(startTimeoutTime)) {
            timeoutAt(startTimeoutTime);
            startTimeoutTime = FP::undefined();
        }
    }

    virtual void incomingControlFrame(const Util::ByteArray &frame, double time)
    {
        InstrumentImplementation::incomingControlFrame(frame, time);
        if (FP::defined(startControlDiscardTime) || startDiscardControlFrames > 0) {
            discardControl(startControlDiscardTime, startDiscardControlFrames);
            startControlDiscardTime = FP::undefined();
            startDiscardControlFrames = 0;
        }
        if (FP::defined(startTimeoutTime)) {
            timeoutAt(startTimeoutTime);
            startTimeoutTime = FP::undefined();
        }
    }

    virtual void incomingInstrumentTimeout(double time)
    {
        timeoutTime = time;
    }

    virtual void discardDataCompleted(double time)
    {
        discardDataTime = time;
    }

    virtual void discardControlCompleted(double time)
    {
        discardControlTime = time;
    }
};

class TestFramedInstrument : public QObject {
Q_OBJECT
private slots:

    void incomingData()
    {
        QFETCH(QList<IncomingBlock>, input);
        QFETCH(QList<DataBlock>, expected);

        InstrumentImplementation *data = new InstrumentImplementation;
        InstrumentImplementation *control = new InstrumentImplementation;
        data->start();
        control->start();

        for (QList<IncomingBlock>::const_iterator b = input.constBegin(), end = input.constEnd();
                b != end;
                ++b) {
            data->incomingData(b->data, b->time, b->type);
            control->incomingControl(b->data, b->time, b->type);
        }
        if (!input.isEmpty() &&
                input.last().type == AcquisitionInterface::IncomingDataType::Stream) {
            data->incomingData(QByteArray(), input.last().time,
                               AcquisitionInterface::IncomingDataType::Final);
            control->incomingControl(QByteArray(), input.last().time,
                                     AcquisitionInterface::IncomingDataType::Final);
        }
        data->initiateShutdown();
        control->initiateShutdown();
        data->wait();
        control->wait();

        QCOMPARE(data->resultData, expected);
        QVERIFY(data->resultControl.isEmpty());
        QCOMPARE(control->resultControl, expected);
        QVERIFY(control->resultData.isEmpty());

        delete data;
        delete control;

        data = new InstrumentImplementation;
        control = new InstrumentImplementation;
        data->start();
        control->start();

        for (QList<IncomingBlock>::const_iterator b = input.constBegin(), end = input.constEnd();
                b != end;
                ++b) {
            QTest::qSleep(50);
            data->incomingData(b->data, b->time, b->type);
            control->incomingControl(b->data, b->time, b->type);
        }
        if (!input.isEmpty() &&
                input.last().type == AcquisitionInterface::IncomingDataType::Stream) {
            QTest::qSleep(50);
            data->incomingData(QByteArray(), input.last().time,
                               AcquisitionInterface::IncomingDataType::Final);
            control->incomingControl(QByteArray(), input.last().time,
                                     AcquisitionInterface::IncomingDataType::Final);
        }
        data->initiateShutdown();
        control->initiateShutdown();
        data->wait();
        control->wait();

        QCOMPARE(data->resultData, expected);
        QVERIFY(data->resultControl.isEmpty());
        QCOMPARE(control->resultControl, expected);
        QVERIFY(control->resultData.isEmpty());

        delete data;
        delete control;
    }

    void incomingData_data()
    {
        QTest::addColumn<QList<IncomingBlock> >("input");
        QTest::addColumn<QList<DataBlock> >("expected");

        QTest::newRow("Empty") << (QList<IncomingBlock>()) << (QList<DataBlock>());

        QTest::newRow("Line stream") <<
                (QList<IncomingBlock>() <<
                        IncomingBlock(QByteArray("A", 1), 1.0) <<
                        IncomingBlock(QByteArray("BC", 2), 2.0) <<
                        IncomingBlock(QByteArray("\r", 1), 3.0) <<
                        IncomingBlock(QByteArray("DEF\n", 4), 4.0) <<
                        IncomingBlock(QByteArray("\nXYZ\n", 5), 5.0) <<
                        IncomingBlock(QByteArray("GH\n\r", 4), 6.0)) <<
                (QList<DataBlock>() <<
                        DataBlock(QByteArray("ABC", 3), 3.0) <<
                        DataBlock(QByteArray("DEF", 3), 4.0) <<
                        DataBlock(QByteArray("XYZ", 3), 5.0) <<
                        DataBlock(QByteArray("GH", 2), 6.0));
        QTest::newRow("Line stream terminal")
                << (QList<IncomingBlock>() << IncomingBlock(QByteArray("A", 1), 1.0)
                                           << IncomingBlock(QByteArray("BC", 2), 2.0)
                                           << IncomingBlock(QByteArray("\r", 1), 3.0)
                                           << IncomingBlock(QByteArray("DEF\n\r", 4), 4.0)
                                           << IncomingBlock(QByteArray("GH", 2), 5.0,
                                                            AcquisitionInterface::IncomingDataType::Final))
                << (QList<DataBlock>() << DataBlock(QByteArray("ABC", 3), 3.0)
                                       << DataBlock(QByteArray("DEF", 3), 4.0)
                                       << DataBlock(QByteArray("GH", 2), 5.0));

        QTest::newRow("Complete stream") << (QList<IncomingBlock>()
                << IncomingBlock(QByteArray("ABC", 3), 1.0,
                                 AcquisitionInterface::IncomingDataType::Complete)
                << IncomingBlock(QByteArray("DEF", 3), 2.0,
                                 AcquisitionInterface::IncomingDataType::Complete)
                << IncomingBlock(QByteArray("GH", 2), 3.0,
                                 AcquisitionInterface::IncomingDataType::Complete))
                                         << (QList<DataBlock>()
                                                 << DataBlock(QByteArray("ABC", 3), 1.0)
                                                 << DataBlock(QByteArray("DEF", 3), 2.0)
                                                 << DataBlock(QByteArray("GH", 2), 3.0));

        QTest::newRow("Hybrid") <<
                (QList<IncomingBlock>() << IncomingBlock(QByteArray("A", 1), 1.0)
                                        << IncomingBlock(QByteArray("BC", 2), 2.0)
                                        << IncomingBlock(QByteArray("BLAH", 4), 3.0,
                                                         AcquisitionInterface::IncomingDataType::Complete)
                                        << IncomingBlock(QByteArray("\nDEF\n", 5), 4.0) <<
                        IncomingBlock(QByteArray("GH\n\r", 4), 5.0)) <<
                (QList<DataBlock>() <<
                        DataBlock(QByteArray("ABC", 3), 3.0) <<
                        DataBlock(QByteArray("BLAH", 4), 3.0) <<
                        DataBlock(QByteArray("DEF", 3), 4.0) <<
                        DataBlock(QByteArray("GH", 2), 5.0));
    }

    void timing()
    {
        QFETCH(double, discardTime);
        QFETCH(int, discardFrames);
        QFETCH(double, timeoutTime);
        QFETCH(QList<IncomingBlock>, input);
        QFETCH(QList<DataBlock>, expected);
        QFETCH(double, expectedDiscardTime);

        InstrumentImplementationTiming *a = new InstrumentImplementationTiming;
        InstrumentImplementationTiming *b = new InstrumentImplementationTiming;
        InstrumentImplementationTiming *c = new InstrumentImplementationTiming;

        a->startDataDiscardTime = discardTime;
        a->startDiscardDataFrames = discardFrames;
        a->startControlDiscardTime = FP::undefined();
        a->startDiscardControlFrames = 0;
        a->startTimeoutTime = timeoutTime;
        a->discardDataTime = FP::undefined();
        a->discardControlTime = FP::undefined();
        a->timeoutTime = FP::undefined();
        b->startDataDiscardTime = FP::undefined();
        b->startDiscardDataFrames = 0;
        b->startControlDiscardTime = discardTime;
        b->startDiscardControlFrames = discardFrames;
        b->startTimeoutTime = timeoutTime;
        b->discardDataTime = FP::undefined();
        b->discardControlTime = FP::undefined();
        b->timeoutTime = FP::undefined();
        c->startDataDiscardTime = discardTime;
        c->startDiscardDataFrames = discardFrames;
        c->startControlDiscardTime = discardTime;
        c->startDiscardControlFrames = discardFrames;
        c->startTimeoutTime = timeoutTime;
        c->discardDataTime = FP::undefined();
        c->discardControlTime = FP::undefined();
        c->timeoutTime = FP::undefined();

        a->start();
        b->start();
        c->start();

        for (QList<IncomingBlock>::const_iterator block = input.constBegin(),
                end = input.constEnd(); block != end; ++block) {
            a->incomingData(block->data, block->time, block->type);
            b->incomingControl(block->data, block->time, block->type);
            c->incomingData(block->data, block->time, block->type);
            c->incomingControl(block->data, block->time, block->type);
        }
        if (!input.isEmpty() &&
                input.last().type == AcquisitionInterface::IncomingDataType::Stream) {
            a->incomingData(QByteArray(), input.last().time,
                            AcquisitionInterface::IncomingDataType::Final);
            b->incomingControl(QByteArray(), input.last().time,
                               AcquisitionInterface::IncomingDataType::Final);
            c->incomingData(QByteArray(), input.last().time,
                            AcquisitionInterface::IncomingDataType::Final);
            c->incomingControl(QByteArray(), input.last().time,
                               AcquisitionInterface::IncomingDataType::Final);
        }
        bool didTimeout = false;
        if (FP::defined(timeoutTime) &&
                (input.isEmpty() ||
                        (FP::defined(input.last().time) && input.last().time < timeoutTime))) {
            a->incomingTimeout(timeoutTime);
            b->incomingTimeout(timeoutTime);
            c->incomingTimeout(timeoutTime);
            didTimeout = true;
        }

        a->initiateShutdown();
        b->initiateShutdown();
        c->initiateShutdown();
        a->wait();
        b->wait();
        c->wait();

        QCOMPARE(a->resultData, expected);
        if (FP::defined(expectedDiscardTime)) {
            QCOMPARE(a->discardDataTime, expectedDiscardTime);
        } else {
            QVERIFY(!FP::defined(a->discardDataTime));
        }
        if (didTimeout) {
            QCOMPARE(a->timeoutTime, timeoutTime);
        } else {
            QVERIFY(!FP::defined(a->timeoutTime));
        }
        QVERIFY(a->resultControl.isEmpty());
        QVERIFY(!FP::defined(a->discardControlTime));

        QCOMPARE(b->resultControl, expected);
        if (FP::defined(expectedDiscardTime)) {
            QCOMPARE(b->discardControlTime, expectedDiscardTime);
        } else {
            QVERIFY(!FP::defined(b->discardControlTime));
        }
        if (didTimeout) {
            QCOMPARE(b->timeoutTime, timeoutTime);
        } else {
            QVERIFY(!FP::defined(b->timeoutTime));
        }
        QVERIFY(b->resultData.isEmpty());
        QVERIFY(!FP::defined(b->discardDataTime));

        QCOMPARE(c->resultData, expected);
        QCOMPARE(c->resultControl, expected);
        if (FP::defined(expectedDiscardTime)) {
            QCOMPARE(c->discardDataTime, expectedDiscardTime);
            QCOMPARE(c->discardControlTime, expectedDiscardTime);
        } else {
            QVERIFY(!FP::defined(c->discardDataTime));
            QVERIFY(!FP::defined(c->discardControlTime));
        }
        if (didTimeout) {
            QCOMPARE(c->timeoutTime, timeoutTime);
        } else {
            QVERIFY(!FP::defined(c->timeoutTime));
        }

        delete a;
        delete b;
        delete c;


        a = new InstrumentImplementationTiming;
        b = new InstrumentImplementationTiming;
        c = new InstrumentImplementationTiming;

        a->startDataDiscardTime = discardTime;
        a->startDiscardDataFrames = discardFrames;
        a->startControlDiscardTime = FP::undefined();
        a->startDiscardControlFrames = 0;
        a->startTimeoutTime = timeoutTime;
        a->discardDataTime = FP::undefined();
        a->discardControlTime = FP::undefined();
        a->timeoutTime = FP::undefined();
        b->startDataDiscardTime = FP::undefined();
        b->startDiscardDataFrames = 0;
        b->startControlDiscardTime = discardTime;
        b->startDiscardControlFrames = discardFrames;
        b->startTimeoutTime = timeoutTime;
        b->discardDataTime = FP::undefined();
        b->discardControlTime = FP::undefined();
        b->timeoutTime = FP::undefined();
        c->startDataDiscardTime = discardTime;
        c->startDiscardDataFrames = discardFrames;
        c->startControlDiscardTime = discardTime;
        c->startDiscardControlFrames = discardFrames;
        c->startTimeoutTime = timeoutTime;
        c->discardDataTime = FP::undefined();
        c->discardControlTime = FP::undefined();
        c->timeoutTime = FP::undefined();

        a->start();
        b->start();
        c->start();

        for (QList<IncomingBlock>::const_iterator block = input.constBegin(),
                end = input.constEnd(); block != end; ++block) {
            QTest::qSleep(50);
            a->incomingData(block->data, block->time, block->type);
            b->incomingControl(block->data, block->time, block->type);
            c->incomingData(block->data, block->time, block->type);
            c->incomingControl(block->data, block->time, block->type);
        }
        if (!input.isEmpty() &&
                input.last().type == AcquisitionInterface::IncomingDataType::Stream) {
            QTest::qSleep(50);
            a->incomingData(QByteArray(), input.last().time,
                            AcquisitionInterface::IncomingDataType::Final);
            b->incomingControl(QByteArray(), input.last().time,
                               AcquisitionInterface::IncomingDataType::Final);
            c->incomingData(QByteArray(), input.last().time,
                            AcquisitionInterface::IncomingDataType::Final);
            c->incomingControl(QByteArray(), input.last().time,
                               AcquisitionInterface::IncomingDataType::Final);
        }
        didTimeout = false;
        if (FP::defined(timeoutTime) &&
                (input.isEmpty() ||
                        (FP::defined(input.last().time) && input.last().time < timeoutTime))) {
            QTest::qSleep(50);
            a->incomingTimeout(timeoutTime);
            b->incomingTimeout(timeoutTime);
            c->incomingTimeout(timeoutTime);
            didTimeout = true;
        }

        a->initiateShutdown();
        b->initiateShutdown();
        c->initiateShutdown();
        a->wait();
        b->wait();
        c->wait();

        QCOMPARE(a->resultData, expected);
        if (FP::defined(expectedDiscardTime)) {
            QCOMPARE(a->discardDataTime, expectedDiscardTime);
        } else {
            QVERIFY(!FP::defined(a->discardDataTime));
        }
        if (didTimeout) {
            QCOMPARE(a->timeoutTime, timeoutTime);
        } else {
            QVERIFY(!FP::defined(a->timeoutTime));
        }
        QVERIFY(a->resultControl.isEmpty());
        QVERIFY(!FP::defined(a->discardControlTime));

        QCOMPARE(b->resultControl, expected);
        if (FP::defined(expectedDiscardTime)) {
            QCOMPARE(b->discardControlTime, expectedDiscardTime);
        } else {
            QVERIFY(!FP::defined(b->discardControlTime));
        }
        if (didTimeout) {
            QCOMPARE(b->timeoutTime, timeoutTime);
        } else {
            QVERIFY(!FP::defined(b->timeoutTime));
        }
        QVERIFY(b->resultData.isEmpty());
        QVERIFY(!FP::defined(b->discardDataTime));

        QCOMPARE(c->resultData, expected);
        QCOMPARE(c->resultControl, expected);
        if (FP::defined(expectedDiscardTime)) {
            QCOMPARE(c->discardDataTime, expectedDiscardTime);
            QCOMPARE(c->discardControlTime, expectedDiscardTime);
        } else {
            QVERIFY(!FP::defined(c->discardDataTime));
            QVERIFY(!FP::defined(c->discardControlTime));
        }
        if (didTimeout) {
            QCOMPARE(c->timeoutTime, timeoutTime);
        } else {
            QVERIFY(!FP::defined(c->timeoutTime));
        }

        delete a;
        delete b;
        delete c;
    }

    void timing_data()
    {
        QTest::addColumn<double>("discardTime");
        QTest::addColumn<int>("discardFrames");
        QTest::addColumn<double>("timeoutTime");
        QTest::addColumn<QList<IncomingBlock> >("input");
        QTest::addColumn<QList<DataBlock> >("expected");
        QTest::addColumn<double>("expectedDiscardTime");

        QTest::newRow("Empty") <<
                FP::undefined() <<
                0 <<
                FP::undefined() <<
                (QList<IncomingBlock>()) <<
                (QList<DataBlock>()) <<
                FP::undefined();

        QTest::newRow("Empty Timeout") <<
                FP::undefined() <<
                0 <<
                100.0 <<
                (QList<IncomingBlock>()) <<
                (QList<DataBlock>()) <<
                FP::undefined();

        QTest::newRow("Discard time") <<
                290.0 <<
                0 <<
                100.0 <<
                (QList<IncomingBlock>() <<
                        IncomingBlock(QByteArray("1\n", 2), 50.0) <<
                        IncomingBlock(QByteArray("AB", 2), 140.0) <<
                        IncomingBlock(QByteArray("2\n", 2), 150.0) <<
                        IncomingBlock(QByteArray("3\n", 2), 250.0) <<
                        IncomingBlock(QByteArray("4\n", 2), 300.0) <<
                        IncomingBlock(QByteArray("5\n", 2), 400.0) <<
                        IncomingBlock(QByteArray("CD", 2), 410.0) <<
                        IncomingBlock(QByteArray("6\r", 2), 500.0)) <<
                (QList<DataBlock>() <<
                        DataBlock(QByteArray("1", 1), 50.0) <<
                        DataBlock(QByteArray("4", 1), 300.0) <<
                        DataBlock(QByteArray("5", 1), 400.0) <<
                        DataBlock(QByteArray("CD6", 3), 500.0)) <<
                300.0;

        QTest::newRow("Discard frames") <<
                FP::undefined() <<
                2 <<
                100.0 <<
                (QList<IncomingBlock>() <<
                        IncomingBlock(QByteArray("1\n", 2), 50.0) <<
                        IncomingBlock(QByteArray("AB", 2), 75.0) <<
                        IncomingBlock(QByteArray("2\n", 2), 150.0) <<
                        IncomingBlock(QByteArray("3\n", 2), 250.0) <<
                        IncomingBlock(QByteArray("QQ", 2), 275.0) <<
                        IncomingBlock(QByteArray("4\n", 2), 300.0) <<
                        IncomingBlock(QByteArray("CD", 2), 390.0) <<
                        IncomingBlock(QByteArray("5\n", 2), 400.0) <<
                        IncomingBlock(QByteArray("6\r", 2), 500.0)) <<
                (QList<DataBlock>() <<
                        DataBlock(QByteArray("1", 1), 50.0) <<
                        DataBlock(QByteArray("QQ4", 3), 300.0) <<
                        DataBlock(QByteArray("CD5", 3), 400.0) <<
                        DataBlock(QByteArray("6", 1), 500.0)) <<
                250.0;

        QTest::newRow("Discard time and frames") <<
                290.0 <<
                1 <<
                100.0 <<
                (QList<IncomingBlock>() <<
                        IncomingBlock(QByteArray("1\n", 2), 50.0) <<
                        IncomingBlock(QByteArray("AB", 2), 75.0) <<
                        IncomingBlock(QByteArray("2\n", 2), 150.0) <<
                        IncomingBlock(QByteArray("CD", 2), 175.0) <<
                        IncomingBlock(QByteArray("3\n", 2), 250.0) <<
                        IncomingBlock(QByteArray("EF", 2), 275.0) <<
                        IncomingBlock(QByteArray("4\n", 2), 300.0) <<
                        IncomingBlock(QByteArray("GH", 2), 350.0) <<
                        IncomingBlock(QByteArray("5\n", 2), 400.0) <<
                        IncomingBlock(QByteArray("QQ", 2), 450.0) <<
                        IncomingBlock(QByteArray("6\r", 2), 500.0)) <<
                (QList<DataBlock>() <<
                        DataBlock(QByteArray("1", 1), 50.0) <<
                        DataBlock(QByteArray("GH5", 3), 400.0) <<
                        DataBlock(QByteArray("QQ6", 3), 500.0)) <<
                300.0;

        QTest::newRow("Full timeout") <<
                FP::undefined() <<
                0 <<
                100.0 <<
                (QList<IncomingBlock>() <<
                        IncomingBlock(QByteArray("A", 1), 1.0) <<
                        IncomingBlock(QByteArray("BC", 2), 2.0) <<
                        IncomingBlock(QByteArray("\r", 1), 3.0) <<
                        IncomingBlock(QByteArray("DEF\n", 4), 4.0) <<
                        IncomingBlock(QByteArray("\nXYZ\n", 5), 5.0) <<
                        IncomingBlock(QByteArray("GH\n\r", 4), 6.0)) <<
                (QList<DataBlock>() <<
                        DataBlock(QByteArray("ABC", 3), 3.0) <<
                        DataBlock(QByteArray("DEF", 3), 4.0) <<
                        DataBlock(QByteArray("XYZ", 3), 5.0) <<
                        DataBlock(QByteArray("GH", 2), 6.0)) <<
                FP::undefined();
    }
};

QTEST_MAIN(TestFramedInstrument)

#include "framedinstrument.moc"
