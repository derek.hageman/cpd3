/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QUuid>
#include <QTest>
#include <QTemporaryFile>

#include "acquisition/acquisitionarchive.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "database/runlock.hxx"
#include "database/storage.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Acquisition;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestArchive : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    bool contains(const SequenceValue::Transfer &data, const SequenceValue &find)
    { return std::find(data.begin(), data.end(), find) != data.end(); }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void events()
    {
        AcquisitionArchive archive;

        archive.incomingEvent(
                SequenceValue(SequenceName("nil", "events", "acquisition"), Variant::Root(1.0),
                              1000.0,
                              1000.0));
        archive.incomingEvent(
                SequenceValue(SequenceName("nil", "events", "acquisition"), Variant::Root(2.0),
                              1000.0,
                              1000.0));
        archive.incomingEvent(
                SequenceValue(SequenceName("nil", "events", "acquisition"), Variant::Root(3.0),
                              1001.0,
                              1001.0));
        archive.initiateShutdown();
        archive.wait();

        auto data = Archive::Access(databaseFile).readSynchronous(Archive::Selection());
        QCOMPARE((int) data.size(), 3);
        QVERIFY(contains(data, SequenceValue(SequenceName("nil", "events", "acquisition"),
                                             Variant::Root(1.0),
                                             1000.0,
                                             1000.0, 0)));
        QVERIFY(contains(data, SequenceValue(SequenceName("nil", "events", "acquisition"),
                                             Variant::Root(2.0),
                                             1000.0,
                                             1000.0, 1)));
        QVERIFY(contains(data, SequenceValue(SequenceName("nil", "events", "acquisition"),
                                             Variant::Root(3.0),
                                             1001.0,
                                             1001.0, 0)));
    }

    void persistent()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"nil", "raw", "Ff_A11"}, Variant::Root(1.0), 1000.0,
                              FP::undefined())});

        AcquisitionArchive archive;

        archive.incomingPersistent(SequenceValue::Transfer{
                SequenceValue(SequenceName("nil", "raw", "Ff_A11"), Variant::Root(2.0), 2000.0,
                              3000),
                SequenceValue(SequenceName("nil", "raw", "Ff_A11"), Variant::Root(3.0), 3000.0,
                              FP::undefined()),
                SequenceValue(SequenceName("nil", "raw", "Fn_A11"), Variant::Root(4.0), 1000.0,
                              FP::undefined())});
        archive.initiateShutdown();
        archive.wait();

        auto data = Archive::Access(databaseFile).readSynchronous(Archive::Selection());
        QCOMPARE((int) data.size(), 4);
        QVERIFY(contains(data,
                         SequenceValue(SequenceName("nil", "raw", "Ff_A11"), Variant::Root(1.0),
                                       1000.0,
                                       2000.0)));
        QVERIFY(contains(data,
                         SequenceValue(SequenceName("nil", "raw", "Ff_A11"), Variant::Root(2.0),
                                       2000.0,
                                       3000.0)));
        QVERIFY(contains(data,
                         SequenceValue(SequenceName("nil", "raw", "Ff_A11"), Variant::Root(3.0),
                                       3000.0,
                                       FP::undefined())));
        QVERIFY(contains(data,
                         SequenceValue(SequenceName("nil", "raw", "Fn_A11"), Variant::Root(4.0),
                                       1000.0,
                                       FP::undefined())));
    }

    void state()
    {
        AcquisitionArchive archive;

        archive.saveState("key1", QByteArray("abcd"));
        archive.saveState("key2", QByteArray("efg"));
        archive.initiateShutdown();
        archive.wait();

        {
            Database::RunLock
                    rc(Database::Storage::sqlite(databaseFile.fileName().toUtf8().constData()));
            rc.acquire("key1");
            QCOMPARE(rc.get("key1"), QByteArray("abcd"));
        }
        {
            Database::RunLock
                    rc(Database::Storage::sqlite(databaseFile.fileName().toUtf8().constData()));
            rc.acquire("key2");
            QCOMPARE(rc.get("key2"), QByteArray("efg"));
        }
    }

    void loggingAdd()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"nil", "raw_meta", "BaG_A11"}, Variant::Root(1.0), 1000.0,
                              FP::undefined()),
                SequenceValue({"nil", "raw_meta", "Ba1_A11"}, Variant::Root(2.0), 1000.0,
                              FP::undefined()),
                SequenceValue({"nil", "raw", "Ba1_A12"}, Variant::Root(3.0), 1000.0,
                              FP::undefined()),
                SequenceValue({"nil", "raw", "BsG_S11"}, Variant::Root(4.0), 2000.0,
                              FP::undefined())});

        Variant::Write config = Variant::Write::empty();
        config.hash("PurgeAge").setDouble(FP::undefined());
        config.hash("PurgePreserveDuration").setDouble(FP::undefined());
        AcquisitionArchive archive(config);

        StreamSink *i = archive.createLoggingSink();
        i->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("nil", "raw", "BaG_A11"), Variant::Root(5.0), 2000.0,
                              3000)});
        i->endData();
        delete i;
        archive.flushLogging();

        QTest::qSleep(50);

        i = archive.createLoggingSink();
        i->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("nil", "raw", "BaG_A13"), Variant::Root(6.0), 1000.0,
                              3000.0),
                SequenceValue(SequenceName("nil", "raw", "BsG_S11"), Variant::Root(7.0), 2000.0,
                              3000.0),
                SequenceValue(SequenceName("nil", "raw", "BaG_A11"), Variant::Root(8.0), 3000.0,
                              4000.0)});
        i->endData();
        delete i;

        archive.initiateShutdown();
        archive.wait();

        auto data = Archive::Access(databaseFile).readSynchronous(Archive::Selection());
        QCOMPARE((int) data.size(), 7);
        QVERIFY(contains(data, SequenceValue(SequenceName("nil", "raw_meta", "BaG_A11"),
                                             Variant::Root(1.0),
                                             1000.0,
                                             2000.0)));
        QVERIFY(contains(data,
                         SequenceValue(SequenceName("nil", "raw", "BaG_A11"), Variant::Root(5.0),
                                       2000.0,
                                       3000.0)));
        QVERIFY(contains(data,
                         SequenceValue(SequenceName("nil", "raw", "BaG_A11"), Variant::Root(8.0),
                                       3000.0,
                                       4000.0)));

        QVERIFY(contains(data, SequenceValue(SequenceName("nil", "raw_meta", "Ba1_A11"),
                                             Variant::Root(2.0),
                                             1000.0,
                                             2000.0)));

        QVERIFY(contains(data,
                         SequenceValue(SequenceName("nil", "raw", "Ba1_A12"), Variant::Root(3.0),
                                       1000.0,
                                       FP::undefined())));

        QVERIFY(contains(data,
                         SequenceValue(SequenceName("nil", "raw", "BsG_S11"), Variant::Root(7.0),
                                       2000.0,
                                       3000.0)));

        QVERIFY(contains(data,
                         SequenceValue(SequenceName("nil", "raw", "BaG_A13"), Variant::Root(6.0),
                                       1000.0,
                                       3000.0)));
    }

    void loggingTerminate()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"nil", "raw", "BaG_A11"}, Variant::Root(1.0), 1000.0,
                              FP::undefined())});

        Variant::Write config = Variant::Write::empty();
        config.hash("PurgeAge").setDouble(FP::undefined());
        config.hash("PurgePreserveDuration").setDouble(FP::undefined());
        AcquisitionArchive archive(config);

        StreamSink *i = archive.createLoggingSink();
        i->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("nil", "raw", "BaG_A11"), Variant::Root(2.0), 2000.0,
                              3000)});
        i->endData();
        delete i;

        archive.initiateShutdown();
        archive.wait();
        archive.terminateLogging(2500.0);

        auto data = Archive::Access(databaseFile).readSynchronous(Archive::Selection());
        QCOMPARE((int) data.size(), 2);
        QVERIFY(contains(data,
                         SequenceValue(SequenceName("nil", "raw", "BaG_A11"), Variant::Root(1.0),
                                       1000.0,
                                       2000.0)));
        QVERIFY(contains(data,
                         SequenceValue(SequenceName("nil", "raw", "BaG_A11"), Variant::Root(2.0),
                                       2000.0,
                                       2500.0)));
    }

    void loggingPurgeStages()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"nil", "raw_meta", "BaG_A11"}, Variant::Root(1.0), 1000.0,
                              FP::undefined()),
                SequenceValue({"nil", "raw", "BaG_A11"}, Variant::Root(2.0), 1000.0, 2000.0)});

        Variant::Write config = Variant::Write::empty();
        config.hash("PurgeAge").setDouble(FP::undefined());
        config.hash("PurgePreserveDuration").setDouble(FP::undefined());
        AcquisitionArchive archive(config);

        StreamSink *i = archive.createLoggingSink();
        i->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("nil", "raw", "BaG_A11"), Variant::Root(3.0), 1200.0,
                              3000.0)});
        i->endData();
        delete i;
        archive.flushLogging();

        QTest::qSleep(50);

        i = archive.createLoggingSink();
        i->incomingData(
                SequenceValue(SequenceName("nil", "raw_meta", "BaG_A11"), Variant::Root(4.0),
                              1100.0,
                              FP::undefined()));
        i->endData();
        delete i;

        archive.initiateShutdown();
        archive.wait();

        auto data = Archive::Access(databaseFile).readSynchronous(Archive::Selection());
        QCOMPARE((int) data.size(), 4);
        QVERIFY(contains(data, SequenceValue(SequenceName("nil", "raw_meta", "BaG_A11"),
                                             Variant::Root(1.0),
                                             1000.0,
                                             1100.0)));
        QVERIFY(contains(data,
                         SequenceValue(SequenceName("nil", "raw", "BaG_A11"), Variant::Root(2.0),
                                       1000.0,
                                       1100.0)));
        QVERIFY(contains(data,
                         SequenceValue(SequenceName("nil", "raw", "BaG_A11"), Variant::Root(3.0),
                                       1200.0,
                                       3000.0)));
        QVERIFY(contains(data, SequenceValue(SequenceName("nil", "raw_meta", "BaG_A11"),
                                             Variant::Root(4.0),
                                             1100.0,
                                             FP::undefined())));
    }

    void metadataOverlap()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"nil", "raw_meta", "BaG_A11"}, Variant::Root(1.0), 1000.0,
                              FP::undefined())});

        AcquisitionArchive archive;

        StreamSink *i = archive.createLoggingSink();
        i->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("nil", "raw_meta", "BaG_A11"), Variant::Root(2.0),
                              2000.0,
                              FP::undefined()),
                SequenceValue(SequenceName("nil", "raw_meta", "BaG_A11"), Variant::Root(3.0),
                              3000.0,
                              FP::undefined())});
        i->endData();
        delete i;

        archive.initiateShutdown();
        archive.wait();

        auto data = Archive::Access(databaseFile).readSynchronous(Archive::Selection());
        QCOMPARE((int) data.size(), 3);
        QVERIFY(contains(data, SequenceValue(SequenceName("nil", "raw_meta", "BaG_A11"),
                                             Variant::Root(1.0),
                                             1000.0,
                                             2000.0)));
        QVERIFY(contains(data, SequenceValue(SequenceName("nil", "raw_meta", "BaG_A11"),
                                             Variant::Root(2.0),
                                             2000.0,
                                             3000.0)));
        QVERIFY(contains(data, SequenceValue(SequenceName("nil", "raw_meta", "BaG_A11"),
                                             Variant::Root(3.0),
                                             3000.0,
                                             FP::undefined())));
    }
};

QTEST_MAIN(TestArchive)

#include "archive.moc"
