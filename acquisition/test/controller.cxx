/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <mutex>
#include <QtGlobal>
#include <QTest>
#include <QLocalSocket>
#include <QLocalServer>
#include <QCryptographicHash>
#include <QAtomicInt>
#include <QHostInfo>
#include <QTemporaryFile>
#include <QSignalSpy>

#include "acquisition/acquisitioncontroller.hxx"
#include "acquisition/acquisitionnetwork.hxx"
#include "core/number.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Acquisition;
using namespace CPD3::Data;

class TestComponent : public AcquisitionComponent {
public:
    Variant::Write receivedCommand;
    QByteArray receivedData;
    QByteArray receivedControl;
    SequenceValue::Transfer receivedRealtime;
    QAtomicInt externalCounter;
    QAtomicInt receivedCounter;

    TestComponent() : receivedCommand(Variant::Write::empty()),
                      receivedData(),
                      receivedControl(),
                      receivedRealtime(),
                      externalCounter(0),
                      receivedCounter(0)
    { }

    virtual ~TestComponent()
    { }

    std::unique_ptr<AcquisitionInterface> createAcquisitionPassive(const ComponentOptions &,
                                                                   const std::string &) override
    {
        Q_ASSERT(false);
        return {};
    }

    std::unique_ptr<
            AcquisitionInterface> createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                           const std::string &context = std::string()) override;

    virtual QString getComponentName() const
    { return "TEST"; }

    virtual QString getComponentDisplayName() const
    { return "TEST"; }

    virtual QString getComponentDescription() const
    { return "TEST"; }

    bool hasReceived(const SequenceName &unit, const Variant::Read &value)
    {
        for (const auto &check : receivedRealtime) {
            if (check.getUnit() != unit)
                continue;
            if (value.exists() && check.getValue() != value)
                continue;
            return true;
        }
        return false;
    }
};

class TestEgress : public StreamSink::Buffer {
    std::mutex mutex;
public:
    TestEgress() = default;

    virtual ~TestEgress() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(!ended());
        StreamSink::Buffer::incomingData(values);
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(!ended());
        StreamSink::Buffer::incomingData(std::move(values));
    }

    void incomingData(const SequenceValue &value) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(!ended());
        StreamSink::Buffer::incomingData(value);
    }

    void incomingData(SequenceValue &&value) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(!ended());
        StreamSink::Buffer::incomingData(std::move(value));
    }

    virtual void endData()
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(!ended());
        StreamSink::Buffer::endData();
    }

    bool isEnded()
    {
        std::lock_guard<std::mutex> lock(mutex);
        return ended();
    }

    bool contains(const SequenceName &unit, const Variant::Read &value)
    {
        std::lock_guard<std::mutex> lock(mutex);
        return StreamSink::Buffer::contains(unit,
                                            [&](const SequenceValue &a, const SequenceName &b) {
                                                if (a.getName() != b)
                                                    return false;
                                                if (!value.exists())
                                                    return true;
                                                return a.getValue() == value;
                                            });
    }
};


class TestInterface : public AcquisitionInterface {
    ValueSegment::Transfer config;
    bool active;
    AcquisitionControlStream *control;
    AcquisitionState *state;
    StreamSink *realtimeEgress;
    StreamSink *loggingEgress;
    StreamSink *persistentEgress;

    friend class RealtimeReceiver;

    class RealtimeReceiver : public StreamSink {
        TestInterface *interface;
    public:
        explicit RealtimeReceiver(TestInterface *i) : interface(i)
        { }

        virtual ~RealtimeReceiver() = default;

        void incomingData(const SequenceValue::Transfer &values) override
        { Util::append(values, interface->cmp->receivedRealtime); }

        void endData() override
        { }
    };

    RealtimeReceiver realtimeIncoming;

    int previousTime;
    double loggingTime;
    bool emitLogMeta;
    bool emitRealtimeMeta;
    TestComponent *cmp;
public:
    TestInterface(TestComponent *c, const ValueSegment::Transfer &conf) : config(conf),
                                                                          active(false),
                                                                          control(NULL),
                                                                          state(NULL),
                                                                          realtimeEgress(NULL),
                                                                          loggingEgress(NULL),
                                                                          persistentEgress(NULL),
                                                                          realtimeIncoming(this),
                                                                          previousTime(-1),
                                                                          loggingTime(
                                                                                  FP::undefined()),
                                                                          emitLogMeta(true),
                                                                          emitRealtimeMeta(true),
                                                                          cmp(c)
    { }

    virtual ~TestInterface()
    { }

    virtual void setControlStream(AcquisitionControlStream *controlStream)
    { control = controlStream; }

    virtual void setState(AcquisitionState *s)
    {
        state = s;
        if (state != NULL) {
            state->setAveragingTime(Time::Second, 1, true);
        }
    }

    virtual void setRealtimeEgress(StreamSink *egress)
    { realtimeEgress = egress; }

    virtual void setLoggingEgress(StreamSink *egress)
    { loggingEgress = egress; }

    virtual void setPersistentEgress(StreamSink *egress)
    {
        persistentEgress = egress;
        if (persistentEgress != NULL) {
            persistentEgress->incomingData(
                    SequenceValue(SequenceName("", "raw", "Persistent"), Variant::Root("Complete"),
                                  500.0, 1000.0));
        }
    }

    virtual SequenceValue::Transfer getPersistentValues()
    {
        return SequenceValue::Transfer{
                SequenceValue({{}, "raw", "Persistent"}, Variant::Root("Ongoing"), 1000.0,
                              FP::undefined())};
    }

    virtual void incomingData(const Util::ByteView &data,
                              double time,
                              AcquisitionInterface::IncomingDataType type)
    {
        Q_UNUSED(data);
        Q_UNUSED(type);
        cmp->receivedData += data.toQByteArray();

#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
        int i = (int) cmp->receivedCounter.load();
#else
        int i = (int) cmp->receivedCounter.loadRelaxed();
#endif
        i += data.size();
        cmp->receivedCounter = i;

        if (state != NULL)
            state->requestDataWakeup(time + 0.05);
    }

    virtual void incomingControl(const Util::ByteView &data, double time, IncomingDataType type)
    {
        Q_UNUSED(data);
        Q_UNUSED(time);
        Q_UNUSED(type);
        cmp->receivedControl += data.toQByteArray();
    }

    virtual void incomingTimeout(double time)
    {
        Q_UNUSED(time);
    }

    void incomingCommand(const Variant::Read &command) override
    {
        cmp->receivedCommand = Variant::Root::overlay(Variant::Root(cmp->receivedCommand),
                                                      Variant::Root(command)).write();
    }

    virtual void incomingAdvance(double time)
    {
        int iTime = (int) time;
        if (loggingEgress != NULL && FP::defined(loggingTime)) {
            if (emitLogMeta) {
                loggingEgress->incomingData(SequenceValue(SequenceName("", "raw_meta", "Logging"),
                                                          Variant::Root("MetaLogging"), loggingTime,
                                                          FP::undefined()));
                loggingEgress->incomingData(SequenceValue::Transfer{
                        SequenceValue(SequenceName("", "raw_meta", "Persistent"),
                                      Variant::Root("MetaPersistent"), loggingTime,
                                      FP::undefined())});
                emitLogMeta = false;
            }
            if (emitRealtimeMeta && realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue::Transfer{
                        SequenceValue(SequenceName("", "raw_meta", "Realtime"),
                                      Variant::Root("MetaRealtime"), time, FP::undefined())});
                emitRealtimeMeta = false;
            }
            if (fabs(loggingTime - time) > 0.1) {
                loggingEgress->incomingData(
                        SequenceValue(SequenceName("", "raw", "Logging"), Variant::Root(0.5),
                                      loggingTime, time));
                loggingTime = time;

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(
                            SequenceValue(SequenceName("", "raw", "Realtime"), Variant::Root(1.0),
                                          time, FP::undefined()));
                }
            }
        } else {
            loggingTime = time;
        }

        if (iTime == previousTime)
            return;
        previousTime = iTime;

        if (state != NULL) {
            Data::Variant::Root output(-1.0);
            state->remap("Input", output);
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue(SequenceName("", "raw", "Remapped"), output, time,
                                      FP::undefined()));
            }

            if (iTime % 2 == 0) {
                state->setSystemFlag("BasicFlag");
                state->requestFlush(0.1);
                state->setSystemFlavors({"A"});
            } else {
                state->clearSystemFlag("BasicFlag");
                state->setSystemFlavors({"B"});

                Data::Variant::Root command;
                command.write().hash("ExtraCommand").setDouble(0.123);
                state->sendCommand("X1", command);
            }

            if (iTime % 4 == 0) {
                state->setBypassFlag("BasicBypass");
            } else {
                state->clearBypassFlag("BasicBypass");
            }

            state->requestStateSave();
            state->event(time, "Stuff happened");

            cmp->externalCounter.ref();

            state->realtimeAdvanced(time);
        }
    }

    virtual Data::Variant::Root getCommands()
    {
        Data::Variant::Root result;
        result.write()
              .hash("TestCommand")
              .hash("Include")
              .array(0)
              .hash("Variable")
              .setString("V1");
        result.write()
              .hash("TestCommand")
              .hash("Exclude")
              .array(0)
              .hash("Variable")
              .setString("V2");
        result.write()
              .hash("TestCommand")
              .hash("Parameters")
              .hash("P")
              .hash("Variable")
              .setString("V3");
        return result;
    }

    virtual SequenceMatch::Composite getExplicitGroupedVariables()
    {
        SequenceMatch::Composite sel;
        sel.append({}, {}, "Persistent");
        return sel;
    }

    virtual StreamSink *getRealtimeIngress()
    { return &realtimeIncoming; }

    virtual void serializeState(QDataStream &stream)
    {
        stream << (quint32) 0xABCDEF;
    }

    virtual void deserializeState(QDataStream &stream)
    {
        quint32 i;
        stream >> i;
        Q_ASSERT(i == (quint32) 0xABCDEF);
    }

    virtual Data::Variant::Root getSystemFlagsMetadata()
    {
        Data::Variant::Root result;
        result.write().hash("TestFlag").setString("A test flag");
        return result;
    }

    virtual void start()
    { active = true; }

    virtual void signalTerminate()
    { active = false; emit completed(); }

    virtual bool isCompleted()
    { return !active; }
};

std::unique_ptr<
        AcquisitionInterface> TestComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                      const std::string &)
{ return std::unique_ptr<AcquisitionInterface>(new TestInterface(this, config)); }

class TestController : public QObject {
Q_OBJECT

    QString uid;

    static bool waitForInterfaceConnection(QLocalServer &server)
    {
        ElapsedTimer st;
        st.start();
        while (st.elapsed() < 30000) {
            QEventLoop loop;
            connect(&server, SIGNAL(newConnection()), &loop, SLOT(quit()));
            QTimer::singleShot(1000, &loop, SLOT(quit()));
            if (server.hasPendingConnections())
                return true;
            if (server.waitForNewConnection(500))
                return true;
            loop.exec();
        }
        return false;
    }

    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        uid = Random::string();

        QVERIFY(qputenv("CPD3_IOINTERFACE_ONE_PROCESS", QByteArray("1")));

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void cleanup()
    {
        QLocalServer::removeServer(QString("CPD3ControllerTest-%1").arg(uid));
        QLocalServer::removeServer(QString("CPD3ControllerTestNetwork-%1").arg(uid));
    }

    void cleanupTestCase()
    {
        QLocalServer::removeServer(QString("CPD3ControllerTest-%1").arg(uid));
        QLocalServer::removeServer(QString("CPD3ControllerTestNetwork-%1").arg(uid));
    }

    void emptyTerminate()
    {
        QLocalServer localSocketServer;
        QLocalServer::removeServer(QString("CPD3ControllerTest-%1").arg(uid));
        QVERIFY(localSocketServer.listen(QString("CPD3ControllerTest-%1").arg(uid)));

        ValueSegment::Transfer config;
        config.emplace_back(FP::undefined(), FP::undefined(), Variant::Root());
        config.back()["Autoprobe/Additional/#0/Type"].setString("qtlocalsocket");
        config.back()["Autoprobe/Additional/#0/Name"].setString(
                QString("CPD3ControllerTest-%1").arg(uid));
        config.back()["Autoprobe/DisableDefaultInterfaces"].setBool(true);
        config.back()["Autoprobe/DisableDefaultComponents"].setBool(true);
        config.back()["Components/Neph/Name"].setString("acquire_tsi_neph3563");
        config.back()["DisableLoopback"].setBool(true);
        config.back()["DisableLocal"].setBool(true);
        AcquisitionController controller(config);

        QEventLoop checkStarted;
        controller.controllerReady
                  .connect(&checkStarted, std::bind(&QEventLoop::quit, &checkStarted), true);
        QTimer::singleShot(30000, &checkStarted, SLOT(quit()));
        controller.start();

        QVERIFY(localSocketServer.waitForNewConnection(30000));
        QLocalSocket *serverSocket = localSocketServer.nextPendingConnection();
        QVERIFY(serverSocket != NULL);
        serverSocket->open(QIODevice::ReadWrite | QIODevice::Unbuffered);
        QVERIFY(!localSocketServer.hasPendingConnections());

        checkStarted.exec();

        QTest::qSleep(500);
        controller.signalTerminate();

        QVERIFY(controller.wait(30000));
        serverSocket->close();
        delete serverSocket;
    }

    void emptyShutdown()
    {
        QLocalServer localSocketServer;
        QLocalServer::removeServer(QString("CPD3ControllerTest-%1").arg(uid));
        QVERIFY(localSocketServer.listen(QString("CPD3ControllerTest-%1").arg(uid)));

        ValueSegment::Transfer config;
        config.emplace_back(FP::undefined(), FP::undefined(), Variant::Root());
        config.back()["Autoprobe/Additional/#0/Type"].setString("qtlocalsocket");
        config.back()["Autoprobe/Additional/#0/Name"].setString(
                QString("CPD3ControllerTest-%1").arg(uid));
        config.back()["Autoprobe/DisableDefaultInterfaces"].setBool(true);
        config.back()["Autoprobe/DisableDefaultComponents"].setBool(true);
        config.back()["Components/Neph/Name"].setString("acquire_tsi_neph3563");
        config.back()["DisableLoopback"].setBool(true);
        config.back()["DisableLocal"].setBool(true);
        AcquisitionController controller(config);

        QEventLoop checkStarted;
        controller.controllerReady
                  .connect(&checkStarted, std::bind(&QEventLoop::quit, &checkStarted), true);
        QTimer::singleShot(30000, &checkStarted, SLOT(quit()));
        controller.start();

        QVERIFY(localSocketServer.waitForNewConnection(30000));
        QLocalSocket *serverSocket = localSocketServer.nextPendingConnection();
        QVERIFY(serverSocket != NULL);
        serverSocket->open(QIODevice::ReadWrite | QIODevice::Unbuffered);
        QVERIFY(!localSocketServer.hasPendingConnections());

        checkStarted.exec();

        QTest::qSleep(500);
        controller.initiateShutdown();

        QVERIFY(controller.wait(30000));
        serverSocket->close();
        delete serverSocket;
    }

    void general()
    {
        std::mutex mutex;
        std::condition_variable cv;

        QLocalServer localSocketServer;
        QLocalServer::removeServer(QString("CPD3ControllerTest-%1").arg(uid));
        QVERIFY(localSocketServer.listen(QString("CPD3ControllerTest-%1").arg(uid)));

        ValueSegment::Transfer config;
        config.emplace_back(FP::undefined(), FP::undefined(), Variant::Root());
        config.back()["Autoprobe/Disable"].setBool(true);
        config.back()["DisableLoopback"].setBool(true);
        config.back()["LocalSocket"].setString(QString("CPD3ControllerTestNetwork-%1").arg(uid));
        config.back()["EnableStatistics"].setBool(true);
        AcquisitionController controller(config, "nil");
        TestComponent component;

        Variant::Root componentConfig;
        componentConfig.write().hash("Groups").setFlags({"testgroup"});
        Variant::Composite::fromCalibration(
                componentConfig.write().hash("Remap").hash("Input").hash("Calibration"),
                Calibration(QVector<double>() << 0.5 << 1.0));
        componentConfig.write().hash("Interface").hash("Type").setString("qtlocalsocket");
        componentConfig.write()
                       .hash("Interface")
                       .hash("Name")
                       .setString(QString("CPD3ControllerTest-%1").arg(uid));
        controller.injectInterface(&component, ValueSegment::Transfer{
                ValueSegment(FP::undefined(), FP::undefined(), componentConfig)});

        QEventLoop checkStarted;
        controller.controllerReady
                  .connect(&checkStarted, std::bind(&QEventLoop::quit, &checkStarted), true);
        QTimer::singleShot(30000, &checkStarted, SLOT(quit()));
        controller.start();
        checkStarted.exec();

        QVERIFY(waitForInterfaceConnection(localSocketServer));
        QLocalSocket *serverSocket = localSocketServer.nextPendingConnection();
        QVERIFY(serverSocket != NULL);
        serverSocket->open(QIODevice::ReadWrite | QIODevice::Unbuffered);
        QVERIFY(!localSocketServer.hasPendingConnections());

        Variant::Root clientConfig;
        clientConfig["Type"].setString("LocalSocket");
        clientConfig["Name"].setString(QString("CPD3ControllerTestNetwork-%1").arg(uid));
        Variant::Root clientRealtimeEvent;
        bool clientConnected = false;
        std::unique_ptr<AcquisitionNetworkClient>
                client(new AcquisitionNetworkClient(clientConfig));
        client->realtimeEvent.connect([&](const Variant::Root &event) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientRealtimeEvent = event;
            }
            cv.notify_all();
        });
        client->connectionState.connect([&](bool connected) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientConnected = connected;
            }
            cv.notify_all();
        });

        client->start();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(30), [&] { return clientConnected; });
        }

        TestEgress clientRealtime;
        client->setRealtimeEgress(&clientRealtime);

        /* No good way to make sure the interface is ready */
        QTest::qSleep(2000);

        QCOMPARE(serverSocket->write(QByteArray("ABCD", 4)), Q_INT64_C(4));
        serverSocket->waitForBytesWritten(1000);

        QEventLoop loop;
        ElapsedTimer to;
        to.start();
        while (to.elapsed() < 30000 && (int)
#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
                component.externalCounter.load()
#else
                component.externalCounter.loadRelaxed()
#endif
                < 6) {
            QTest::qSleep(100);
            loop.processEvents(QEventLoop::AllEvents, 100);
        }
#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
        QVERIFY((int) component.externalCounter.load() >= 6);
#else
        QVERIFY((int) component.externalCounter.loadRelaxed() >= 6);
#endif
        while (to.elapsed() < 30000 && (int)
#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
                component.receivedCounter.load()
#else
                component.receivedCounter.loadRelaxed()
#endif
                < 4) {
            QTest::qSleep(100);
            loop.processEvents(QEventLoop::AllEvents, 100);
        }
#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
        QVERIFY((int) component.receivedCounter.load() >= 4);
#else
        QVERIFY((int) component.receivedCounter.loadRelaxed() >= 4);
#endif
        /* Unfortunately, there's no timing guarantees for when these
         * pass through the various smoothing layers, so have to test
         * directly like this */
        while (to.elapsed() < 30000) {
            if (clientRealtime.contains(SequenceName("nil", "raw", "Realtime_X1", {"a"}),
                                        Variant::Root(1.0)) &&
                    clientRealtime.contains(SequenceName("nil", "raw", "Realtime_X1", {"b"}),
                                            Variant::Root(1.0)) &&
                    clientRealtime.contains(SequenceName("nil", "rt_instant", "Realtime_X1", {"a"}),
                                            Variant::Root(1.0)) &&
                    clientRealtime.contains(SequenceName("nil", "rt_instant", "Realtime_X1", {"b"}),
                                            Variant::Root(1.0)) &&
                    clientRealtime.contains(SequenceName("nil", "rt_boxcar", "Realtime_X1", {"a"}),
                                            Variant::Root(1.0)) &&
                    clientRealtime.contains(SequenceName("nil", "rt_boxcar", "Realtime_X1", {"b"}),
                                            Variant::Root(1.0)))
                break;
            QTest::qSleep(100);
        }

        Variant::Root clientMessage;
        clientMessage.write().hash("Text").setString("Client message");
        client->messageLogEvent(clientMessage);

        TestEgress clientLogging;
        client->remoteDataRead(&clientLogging,
                               Archive::Selection(FP::undefined(), FP::undefined(), {"nil"},
                                                  {"raw"}, {"Logging_X1", "Realtime_X1"}, {},
                                                  {"cover", "stats"}).withMetaArchive(false));
        ElapsedTimer clientTO;
        clientTO.start();
        while (clientTO.elapsed() < 60000 && !clientLogging.isEnded()) {
            QTest::qSleep(50);
        }
        QVERIFY(clientLogging.isEnded());

        auto interfaceInfo = client->getInterfaceInformation("X1");
        QCOMPARE(interfaceInfo["Source/Name"].toString(), std::string("X1"));
        QCOMPARE(interfaceInfo["Commands/TestCommand/Include/#0/Variable"].toString(),
                 std::string("V1_X1"));
        QCOMPARE(interfaceInfo["Commands/TestCommand/Exclude/#0/Variable"].toString(),
                 std::string("V2_X1"));
        QCOMPARE(interfaceInfo["Commands/TestCommand/Parameters/P/Variable"].toString(),
                 std::string("V3_X1"));

        client.reset();

        controller.initiateShutdown();
        QVERIFY(controller.wait(30000));
        serverSocket->close();
        delete serverSocket;

        /* There's apparently sometimes a delay in the flush time to
         * data under valgrind, so this is needed in that case */
        QTest::qSleep(500);

        QCOMPARE(component.receivedData, QByteArray("ABCD", 4));
        QVERIFY(component.receivedCommand.hash("Bypass").exists());
        QVERIFY(component.receivedCommand.hash("UnBypass").exists());
        QCOMPARE(component.receivedCommand.hash("ExtraCommand").toDouble(), 0.123);

        Archive::Access access(databaseFile);

        auto result = access.readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"nil"}, {"events"},
                                   {"acquisition"}));
        QVERIFY(result.size() >= 3);
        QCOMPARE(result.at(0).read()["Text"].toString(), std::string("Stuff happened"));
        QCOMPARE(result.at(0).read()["Source"].toString(), std::string("X1"));
        bool hit = false;
        for (const auto &check : result) {
            if (check.read()["Text"].toString() != "Client message")
                continue;
            if (check.read()["Source"].toString() != "EXTERNAL")
                continue;
            hit = true;
            break;
        }
        QVERIFY(hit);


        result = access.readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"nil"}, {"raw"},
                                   {"Logging_X1"}, {"a"}, {"cover", "stats"}).withMetaArchive(
                        false));
        QVERIFY(result.size() >= 1);
        QCOMPARE(result.at(0).read().toDouble(), 0.5);
        QVERIFY((result.at(0).getEnd() - result.at(0).getStart()) > 0.8);

        result = access.readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"nil"}, {"raw"},
                                   {"Logging_X1"}, {"b"}, {"cover", "stats"}).withMetaArchive(
                        false));
        QVERIFY(result.size() >= 1);
        QCOMPARE(result.at(0).read().toDouble(), 0.5);
        QVERIFY((result.at(0).getEnd() - result.at(0).getStart()) > 0.8);

        result = access.readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"nil"}, {"raw_meta"},
                                   {"Logging_X1"}, {"a"}, {"cover", "stats"}));
        QCOMPARE((int) result.size(), 1);

        result = access.readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"nil"}, {"raw_meta"},
                                   {"Logging_X1"}, {"b"}, {"cover", "stats"}));
        QCOMPARE((int) result.size(), 1);

        result = access.readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"nil"}, {"raw"},
                                   {"Persistent_X1"}).withMetaArchive(false));
        QCOMPARE((int) result.size(), 2);
        QCOMPARE(result.at(0).getStart(), 500.0);
        QCOMPARE(result.at(0).getEnd(), 1000.0);
        QCOMPARE(result.at(0).getValue().toString(), std::string("Complete"));
        QCOMPARE(result.at(1).getStart(), 1000.0);
        QVERIFY(result.at(1).getEnd() > Time::time() + 3600.0);
        QCOMPARE(result.at(1).getValue().toString(), std::string("Ongoing"));

        result = access.readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"nil"}, {"raw_meta"},
                                   {"Persistent_X1"}));
        QCOMPARE((int) result.size(), 1);

        QVERIFY(clientRealtime.contains(SequenceName("nil", "raw", "Realtime_X1", {"a"}),
                                        Variant::Root(1.0)));
        QVERIFY(clientRealtime.contains(SequenceName("nil", "raw", "Realtime_X1", {"b"}),
                                        Variant::Root(1.0)));
        QVERIFY(clientRealtime.contains(SequenceName("nil", "rt_instant", "Realtime_X1", {"a"}),
                                        Variant::Root(1.0)));
        QVERIFY(clientRealtime.contains(SequenceName("nil", "rt_instant", "Realtime_X1", {"b"}),
                                        Variant::Root(1.0)));
        QVERIFY(clientRealtime.contains(SequenceName("nil", "rt_boxcar", "Realtime_X1", {"a"}),
                                        Variant::Root(1.0)));
        QVERIFY(clientRealtime.contains(SequenceName("nil", "rt_boxcar", "Realtime_X1", {"b"}),
                                        Variant::Root(1.0)));

        QVERIFY(clientRealtime.contains(SequenceName("nil", "raw_meta", "Realtime_X1", {"a"}),
                                        Variant::Root()));
        QVERIFY(clientRealtime.contains(SequenceName("nil", "raw_meta", "Realtime_X1", {"b"}),
                                        Variant::Root()));
        QVERIFY(clientRealtime.contains(
                SequenceName("nil", "rt_instant_meta", "Realtime_X1", {"a"}), Variant::Root()));
        QVERIFY(clientRealtime.contains(
                SequenceName("nil", "rt_instant_meta", "Realtime_X1", {"b"}), Variant::Root()));
        QVERIFY(clientRealtime.contains(SequenceName("nil", "rt_boxcar_meta", "Realtime_X1", {"a"}),
                                        Variant::Root()));
        QVERIFY(clientRealtime.contains(SequenceName("nil", "rt_boxcar_meta", "Realtime_X1", {"b"}),
                                        Variant::Root()));


        QVERIFY(clientLogging.contains(SequenceName("nil", "raw", "Realtime_X1", {"a"}),
                                       Variant::Root(1.0)));
        QVERIFY(clientLogging.contains(SequenceName("nil", "raw", "Realtime_X1", {"b"}),
                                       Variant::Root(1.0)));

        QVERIFY(clientRealtime.contains(SequenceName("nil", "rt_instant", "Remapped_X1", {"b"}),
                                        Variant::Root(-0.5)));

        QCOMPARE(clientRealtimeEvent["Text"].toString(), std::string("Stuff happened"));
        QCOMPARE(clientRealtimeEvent["Source"].toString(), std::string("X1"));

        QVERIFY(component.hasReceived(SequenceName("nil", "raw", "Realtime_X1", {"a"}),
                                      Variant::Root(1.0)));
        QVERIFY(component.hasReceived(SequenceName("nil", "rt_instant", "Realtime_X1", {"a"}),
                                      Variant::Root(1.0)));
        QVERIFY(component.hasReceived(SequenceName("nil", "rt_boxcar", "Realtime_X1", {"a"}),
                                      Variant::Root(1.0)));
    }

    void immediateMetadata()
    {
        QLocalServer localSocketServer;
        QLocalServer::removeServer(QString("CPD3ControllerTest-%1").arg(uid));
        QVERIFY(localSocketServer.listen(QString("CPD3ControllerTest-%1").arg(uid)));

        ValueSegment::Transfer config;
        config.emplace_back(FP::undefined(), FP::undefined(), Variant::Root());
        config.back().write()["Autoprobe/Disable"].setBool(true);
        config.back().write()["DisableLoopback"].setBool(true);
        AcquisitionController controller(config, "nil");
        TestComponent component;

        Variant::Root componentConfig;
        componentConfig.write().hash("Groups").setFlags({"testgroup"});
        Variant::Composite::fromCalibration(
                componentConfig.write().hash("Remap").hash("Input").hash("Calibration"),
                Calibration(QVector<double>() << 0.5 << 1.0));
        componentConfig.write().hash("Interface").hash("Type").setString("qtlocalsocket");
        componentConfig.write()
                       .hash("Interface")
                       .hash("Name")
                       .setString(QString("CPD3ControllerTest-%1").arg(uid));
        controller.injectInterface(&component, ValueSegment::Transfer{
                ValueSegment(FP::undefined(), FP::undefined(), componentConfig)});

        QEventLoop checkStarted;
        controller.controllerReady
                  .connect(&checkStarted, std::bind(&QEventLoop::quit, &checkStarted), true);
        QTimer::singleShot(30000, &checkStarted, SLOT(quit()));
        controller.start();
        checkStarted.exec();

        QVERIFY(waitForInterfaceConnection(localSocketServer));
        QLocalSocket *serverSocket = localSocketServer.nextPendingConnection();
        QVERIFY(serverSocket != NULL);
        serverSocket->open(QIODevice::ReadWrite | QIODevice::Unbuffered);
        QVERIFY(!localSocketServer.hasPendingConnections());

        /* No good way to make sure the interface is ready */
        QTest::qSleep(2000);

        QCOMPARE(serverSocket->write(QByteArray("ABCD", 4)), Q_INT64_C(4));
        serverSocket->waitForBytesWritten(1000);

        Archive::Access access(databaseFile);
        SequenceValue::Transfer result;

        ElapsedTimer to;
        to.start();
        while (to.elapsed() < 30000) {
            result = access.readSynchronous(
                    Archive::Selection(FP::undefined(), FP::undefined(), {"nil"}, {"raw_meta"},
                                       {"Persistent_X1"}));
            if (result.size() > 0)
                break;
            QTest::qSleep(500);
            controller.flushData();
        }
        QCOMPARE((int) result.size(), 1);

        controller.initiateShutdown();
        QVERIFY(controller.wait(30000));
        serverSocket->close();
        delete serverSocket;

        /* There's apparently sometimes a delay in the flush time to
         * data under valgrind, so this is needed in that case */
        QTest::qSleep(500);

        result = access.readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"nil"}, {"raw_meta"},
                                   {"Persistent_X1"}));
        QCOMPARE((int) result.size(), 1);
    }
};

QTEST_MAIN(TestController)

#include "controller.moc"
