/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>

#include "acquisition/busmanager.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Acquisition;

class TestDevice : public BusManager::Device {
public:
    double lockAcquireTime;
    double lockHeldTime;
    double timeoutTime;
    double controlTime;
    int requestResponseCount;
    int releaseResponseCount;
    bool shouldAcquireLock;
    bool shouldReleaseLock;
    bool doTimeoutUpdate;
    double setTimeout;

    TestDevice(BusManager::DeviceInterface *interface) : BusManager::Device(interface),
                                                         lockAcquireTime(FP::undefined()),
                                                         lockHeldTime(FP::undefined()),
                                                         timeoutTime(FP::undefined()),
                                                         controlTime(FP::undefined()),
                                                         requestResponseCount(0),
                                                         releaseResponseCount(0),
                                                         shouldAcquireLock(false),
                                                         shouldReleaseLock(false),
                                                         doTimeoutUpdate(false),
                                                         setTimeout(FP::undefined())
    { }

    virtual ~TestDevice()
    { }

    void applyResponses()
    {
        for (; requestResponseCount > 0; --requestResponseCount) {
            interface()->queueResponse();
        }
        for (; releaseResponseCount > 0; --releaseResponseCount) {
            interface()->receivedResponse();
        }
        if (doTimeoutUpdate) {
            doTimeoutUpdate = false;
            interface()->timeoutAt(setTimeout);
        }
    }

    virtual void lockAcquired(double time)
    {
        lockAcquireTime = time;

        applyResponses();
        if (shouldReleaseLock) {
            shouldReleaseLock = false;
            interface()->unlockBus();
        }
    }

    virtual void lockHeld(double time)
    {
        lockHeldTime = time;

        applyResponses();
        if (shouldReleaseLock) {
            shouldReleaseLock = false;
            interface()->unlockBus();
        }
    }

    virtual void incomingTimeout(double time)
    {
        timeoutTime = time;

        applyResponses();
        if (shouldAcquireLock) {
            shouldAcquireLock = false;
            interface()->lockBus();
        }
    }

    virtual void busControlAcquired(double time)
    {
        controlTime = time;

        applyResponses();
        if (shouldAcquireLock) {
            shouldAcquireLock = false;
            interface()->lockBus();
        }
    }
};

class TestBackend : public BusManager::Backend {
public:
    QList<TestDevice *> devices;
    QList<TestDevice *>::const_iterator controlIterator;
    QList<TestDevice *>::const_iterator simpleIterator;
    double timeoutTime;

    TestBackend()
            : devices(),
              controlIterator(devices.constEnd()),
              simpleIterator(devices.constEnd()),
              timeoutTime(FP::undefined())
    { }

    virtual ~TestBackend()
    { }

    virtual TestDevice *advanceControl()
    {
        if (controlIterator == devices.constEnd())
            controlIterator = devices.constBegin();
        if (controlIterator == devices.constEnd())
            return NULL;
        TestDevice *result = *controlIterator;
        ++controlIterator;
        return result;
    }

    virtual void removeDevice(BusManager::Device *device)
    {
        devices.removeAll(static_cast<TestDevice *>(device));
        controlIterator = devices.constEnd();
        simpleIterator = devices.constEnd();
    }

    virtual void timeoutAt(double time)
    {
        timeoutTime = time;
    }

    virtual void beginSimpleIteration()
    {
        simpleIterator = devices.constBegin();
    }

    virtual TestDevice *nextSimpleIteration()
    {
        if (simpleIterator == devices.constEnd())
            return NULL;
        TestDevice *result = *simpleIterator;
        ++simpleIterator;
        return result;
    }

    void resetIteration()
    {
        controlIterator = devices.constEnd();
        simpleIterator = devices.constEnd();
    }

    void addDevice(TestDevice *device)
    {
        devices.append(device);
        controlIterator = devices.constEnd();
        simpleIterator = devices.constEnd();
    }
};

class TestBusManager : public QObject {
Q_OBJECT
private slots:

    void empty()
    {
        TestBackend backend;
        BusManager bus(&backend);

        bus.advance(100.0);
        bus.advance(100.0);
        QVERIFY(bus.nextResponder() == NULL);
        QVERIFY(bus.busLocker() == NULL);
        bus.advance(100.0);
        bus.advance(110.0);
        QVERIFY(bus.nextResponder() == NULL);
        QVERIFY(bus.busLocker() == NULL);
    }

    void single()
    {
        TestBackend backend;
        BusManager bus(&backend);
        TestDevice d1(bus.createDeviceInterface());
        backend.addDevice(&d1);

        bus.advance(100.0);
        QCOMPARE(d1.controlTime, 100.0);
        QVERIFY(!FP::defined(d1.lockAcquireTime));
        QVERIFY(!FP::defined(d1.lockHeldTime));
        QVERIFY(!FP::defined(d1.timeoutTime));
        QVERIFY(!FP::defined(backend.timeoutTime));

        bus.advance(110.0);
        QCOMPARE(d1.controlTime, 110.0);
        QVERIFY(!FP::defined(d1.lockAcquireTime));
        QVERIFY(!FP::defined(d1.lockHeldTime));
        QVERIFY(!FP::defined(d1.timeoutTime));
        QVERIFY(!FP::defined(backend.timeoutTime));

        d1.requestResponseCount = 1;
        bus.advance(120.0);
        QCOMPARE(d1.controlTime, 120.0);
        QCOMPARE(d1.requestResponseCount, 0);
        QVERIFY(!FP::defined(d1.lockAcquireTime));
        QVERIFY(!FP::defined(d1.lockHeldTime));
        QVERIFY(!FP::defined(d1.timeoutTime));
        QVERIFY(!FP::defined(backend.timeoutTime));
        QVERIFY(bus.nextResponder() == &d1);

        bus.advance(130.0);
        QCOMPARE(d1.controlTime, 120.0);
        QVERIFY(!FP::defined(d1.lockAcquireTime));
        QVERIFY(!FP::defined(d1.lockHeldTime));
        QVERIFY(!FP::defined(d1.timeoutTime));
        QVERIFY(!FP::defined(backend.timeoutTime));
        QVERIFY(bus.nextResponder() == &d1);

        d1.interface()->receivedResponse();
        bus.advance(140.0);
        QCOMPARE(d1.controlTime, 140.0);
        QVERIFY(!FP::defined(d1.lockAcquireTime));
        QVERIFY(!FP::defined(d1.lockHeldTime));
        QVERIFY(!FP::defined(d1.timeoutTime));
        QVERIFY(!FP::defined(backend.timeoutTime));
        QVERIFY(bus.nextResponder() == NULL);

        d1.doTimeoutUpdate = true;
        d1.setTimeout = 160.0;
        bus.advance(150.0);
        QCOMPARE(d1.controlTime, 150.0);
        QVERIFY(!FP::defined(d1.lockAcquireTime));
        QVERIFY(!FP::defined(d1.lockHeldTime));
        QVERIFY(!FP::defined(d1.timeoutTime));
        QCOMPARE(backend.timeoutTime, 160.0);

        bus.advance(160.0);
        QCOMPARE(d1.controlTime, 160.0);
        QCOMPARE(d1.timeoutTime, 160.0);
        QVERIFY(!FP::defined(d1.lockAcquireTime));
        QVERIFY(!FP::defined(d1.lockHeldTime));
        QVERIFY(!FP::defined(backend.timeoutTime));

        d1.shouldAcquireLock = true;
        d1.shouldReleaseLock = true;
        bus.advance(170.0);
        QCOMPARE(d1.controlTime, 170.0);
        QCOMPARE(d1.lockAcquireTime, 170.0);
        QCOMPARE(d1.timeoutTime, 160.0);
        QVERIFY(!FP::defined(d1.lockHeldTime));
        QVERIFY(!FP::defined(backend.timeoutTime));

        bus.advance(180.0);
        QCOMPARE(d1.controlTime, 180.0);
        QCOMPARE(d1.lockAcquireTime, 170.0);
        QCOMPARE(d1.timeoutTime, 160.0);
        QVERIFY(!FP::defined(d1.lockHeldTime));
        QVERIFY(!FP::defined(backend.timeoutTime));

        d1.shouldAcquireLock = true;
        bus.advance(190.0);
        QCOMPARE(d1.controlTime, 190.0);
        QCOMPARE(d1.lockAcquireTime, 190.0);
        QCOMPARE(d1.lockHeldTime, 190.0);
        QCOMPARE(d1.timeoutTime, 160.0);
        QVERIFY(!FP::defined(backend.timeoutTime));

        bus.advance(200.0);
        QCOMPARE(d1.controlTime, 190.0);
        QCOMPARE(d1.lockAcquireTime, 190.0);
        QCOMPARE(d1.lockHeldTime, 200.0);
        QCOMPARE(d1.timeoutTime, 160.0);
        QVERIFY(!FP::defined(backend.timeoutTime));

        d1.shouldReleaseLock = true;
        bus.advance(210.0);
        QCOMPARE(d1.controlTime, 210.0);
        QCOMPARE(d1.lockAcquireTime, 190.0);
        QCOMPARE(d1.lockHeldTime, 210.0);
        QCOMPARE(d1.timeoutTime, 160.0);
        QVERIFY(!FP::defined(backend.timeoutTime));

        bus.advance(220.0);
        QCOMPARE(d1.controlTime, 220.0);
        QCOMPARE(d1.lockAcquireTime, 190.0);
        QCOMPARE(d1.lockHeldTime, 210.0);
        QCOMPARE(d1.timeoutTime, 160.0);
        QVERIFY(!FP::defined(backend.timeoutTime));

        d1.interface()->setProvisional(15.0);
        bus.advance(230.0);
        QCOMPARE(d1.controlTime, 230.0);
        QCOMPARE(d1.lockAcquireTime, 190.0);
        QCOMPARE(d1.lockHeldTime, 210.0);
        QCOMPARE(d1.timeoutTime, 160.0);
        QCOMPARE(backend.timeoutTime, 245.0);

        bus.advance(250.0);
        QCOMPARE(d1.controlTime, 230.0);
        QCOMPARE(d1.lockAcquireTime, 190.0);
        QCOMPARE(d1.lockHeldTime, 210.0);
        QCOMPARE(d1.timeoutTime, 160.0);
        QVERIFY(!FP::defined(backend.timeoutTime));
        QVERIFY(backend.devices.isEmpty());
    }

    void sequence()
    {
        TestBackend backend;
        BusManager bus(&backend);
        TestDevice d1(bus.createDeviceInterface());
        backend.addDevice(&d1);
        TestDevice d2(bus.createDeviceInterface());
        backend.addDevice(&d2);
        TestDevice d3(bus.createDeviceInterface());
        backend.addDevice(&d3);

        bus.advance(100.0);
        QCOMPARE(d1.controlTime, 100.0);
        QCOMPARE(d2.controlTime, 100.0);
        QCOMPARE(d3.controlTime, 100.0);

        bus.advance(110.0);
        QCOMPARE(d1.controlTime, 110.0);
        QCOMPARE(d2.controlTime, 110.0);
        QCOMPARE(d3.controlTime, 110.0);
        QVERIFY(bus.nextResponder() == NULL);

        backend.resetIteration();
        d2.requestResponseCount = 2;
        bus.advance(120.0);
        QCOMPARE(d1.controlTime, 120.0);
        QCOMPARE(d2.controlTime, 120.0);
        QCOMPARE(d3.controlTime, 110.0);
        QVERIFY(bus.nextResponder() == &d2);

        bus.advance(130.0);
        QCOMPARE(d1.controlTime, 120.0);
        QCOMPARE(d2.controlTime, 120.0);
        QCOMPARE(d3.controlTime, 110.0);
        QVERIFY(bus.nextResponder() == &d2);

        d2.interface()->receivedResponse();
        bus.advance(140.0);
        QCOMPARE(d1.controlTime, 120.0);
        QCOMPARE(d2.controlTime, 120.0);
        QCOMPARE(d3.controlTime, 110.0);
        QVERIFY(bus.nextResponder() == &d2);

        d2.interface()->receivedResponse();
        bus.advance(140.0);
        QCOMPARE(d1.controlTime, 140.0);
        QCOMPARE(d2.controlTime, 140.0);
        QCOMPARE(d3.controlTime, 140.0);
        QVERIFY(bus.nextResponder() == NULL);

        backend.resetIteration();
        d2.shouldAcquireLock = true;
        bus.advance(150.0);
        QCOMPARE(d1.controlTime, 150.0);
        QCOMPARE(d2.controlTime, 150.0);
        QCOMPARE(d3.controlTime, 140.0);
        QCOMPARE(d2.lockAcquireTime, 150.0);
        QCOMPARE(d2.lockHeldTime, 150.0);

        bus.advance(160.0);
        QCOMPARE(d1.controlTime, 150.0);
        QCOMPARE(d2.controlTime, 150.0);
        QCOMPARE(d3.controlTime, 140.0);
        QCOMPARE(d2.lockAcquireTime, 150.0);
        QCOMPARE(d2.lockHeldTime, 160.0);

        d2.interface()->unlockBus();
        bus.advance(170.0);
        QCOMPARE(d1.controlTime, 170.0);
        QCOMPARE(d2.controlTime, 170.0);
        QCOMPARE(d3.controlTime, 170.0);

        d1.interface()->timeoutAt(190.0);
        d2.interface()->timeoutAt(195.0);
        d3.interface()->timeoutAt(200.0);
        bus.advance(180.0);
        QCOMPARE(d1.controlTime, 180.0);
        QCOMPARE(d2.controlTime, 180.0);
        QCOMPARE(d3.controlTime, 180.0);
        QCOMPARE(backend.timeoutTime, 190.0);

        bus.advance(190.0);
        QCOMPARE(d1.controlTime, 190.0);
        QCOMPARE(d1.timeoutTime, 190.0);
        QCOMPARE(d2.controlTime, 190.0);
        QCOMPARE(d3.controlTime, 190.0);
        QCOMPARE(backend.timeoutTime, 195.0);

        d2.interface()->timeoutAt(FP::undefined());
        bus.advance(200.0);
        QCOMPARE(d1.controlTime, 200.0);
        QCOMPARE(d1.timeoutTime, 190.0);
        QCOMPARE(d2.controlTime, 200.0);
        QVERIFY(!FP::defined(d2.timeoutTime));
        QCOMPARE(d3.controlTime, 200.0);
        QCOMPARE(d3.timeoutTime, 200.0);
        QVERIFY(!FP::defined(backend.timeoutTime));

        backend.resetIteration();
        d1.requestResponseCount = 1;
        d2.setProvisional(15.0);
        d3.setProvisional(15.0);
        bus.advance(210.0);
        QCOMPARE(d1.controlTime, 210.0);
        QCOMPARE(d2.controlTime, 200.0);
        QCOMPARE(d3.controlTime, 200.0);
        QVERIFY(bus.nextResponder() == &d1);
        QCOMPARE(backend.timeoutTime, 225.0);

        d3.setAccepted();
        bus.advance(220.0);
        QCOMPARE(d1.controlTime, 210.0);
        QCOMPARE(d2.controlTime, 200.0);
        QCOMPARE(d3.controlTime, 200.0);
        QVERIFY(bus.nextResponder() == &d1);
        QCOMPARE(backend.timeoutTime, 225.0);

        bus.advance(230.0);
        QCOMPARE(d1.controlTime, 210.0);
        QCOMPARE(d2.controlTime, 200.0);
        QCOMPARE(d3.controlTime, 200.0);
        QVERIFY(bus.nextResponder() == &d1);
        QCOMPARE(backend.devices.size(), 2);
        QVERIFY(!FP::defined(backend.timeoutTime));

        d1.interface()->receivedResponse();
        bus.advance(240.0);
        QCOMPARE(d1.controlTime, 240.0);
        QCOMPARE(d2.controlTime, 200.0);
        QCOMPARE(d3.controlTime, 240.0);
        QVERIFY(bus.nextResponder() == NULL);

        TestDevice d4(bus.createDeviceInterface());
        backend.addDevice(&d4);

        bus.advance(250.0);
        QCOMPARE(d1.controlTime, 250.0);
        QCOMPARE(d3.controlTime, 250.0);
        QCOMPARE(d4.controlTime, 250.0);

        backend.resetIteration();
        d1.requestResponseCount = 1;
        d3.requestResponseCount = 2;
        d4.requestResponseCount = 1;
        bus.advance(260.0);
        QCOMPARE(d1.controlTime, 260.0);
        QCOMPARE(d3.controlTime, 250.0);
        QCOMPARE(d4.controlTime, 250.0);

        bus.advance(270.0);
        QCOMPARE(d1.controlTime, 260.0);
        QCOMPARE(d3.controlTime, 250.0);
        QCOMPARE(d4.controlTime, 250.0);

        d1.interface()->receivedResponse();
        bus.advance(280.0);
        QCOMPARE(d1.controlTime, 260.0);
        QCOMPARE(d3.controlTime, 280.0);
        QCOMPARE(d4.controlTime, 250.0);

        d3.interface()->receivedResponse();
        d3.interface()->receivedResponse();
        bus.advance(290.0);
        QCOMPARE(d1.controlTime, 260.0);
        QCOMPARE(d3.controlTime, 280.0);
        QCOMPARE(d4.controlTime, 290.0);

        bus.advance(300.0);
        QCOMPARE(d1.controlTime, 260.0);
        QCOMPARE(d3.controlTime, 280.0);
        QCOMPARE(d4.controlTime, 290.0);

        d4.interface()->receivedResponse();
        bus.advance(310.0);
        QCOMPARE(d1.controlTime, 310.0);
        QCOMPARE(d3.controlTime, 310.0);
        QCOMPARE(d4.controlTime, 310.0);
    }
};

QTEST_APPLESS_MAIN(TestBusManager)

#include "busmanager.moc"
