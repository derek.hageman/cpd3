/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>

#include "acquisition/realtimelayout.hxx"
#include "datacore/stream.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class TestRealtimeLayout : public QObject {
Q_OBJECT

    static bool checkExists(RealtimeLayout::iterator it,
                            const RealtimeLayout::iterator &end,
                            int row,
                            int col,
                            RealtimeLayout::LayoutItemType type,
                            const QString &text)
    {
        int hitCount = 0;
        for (; it != end; ++it) {
            if (it.type() != type)
                continue;
            if (it.row() != row)
                continue;
            if (it.column() != col)
                continue;

            if (it.text().trimmed() != text.trimmed()) {
                qDebug() << "Text mistmatch at" << row << col << "; expecting" << text << "got"
                         << it.text();
                return false;
            }
            ++hitCount;
        }
        return hitCount == 1;
    }

    static bool checkExists(const RealtimeLayout &rt,
                            int row,
                            int col,
                            RealtimeLayout::LayoutItemType type,
                            const QString &text)
    { return checkExists(rt.begin(), rt.end(), row, col, type, text); }

    static bool checkExists(int p,
                            const RealtimeLayout &rt,
                            int row,
                            int col,
                            RealtimeLayout::LayoutItemType type,
                            const QString &text)
    { return checkExists(rt.begin(p), rt.end(p), row, col, type, text); }

private slots:

    void basic()
    {
        RealtimeLayout layout(true);
        Variant::Root root;
        auto add = root.write();

        add.metadataReal("Format").setString("00.00");
        add.metadataReal("Realtime").hash("RowOrder").setInt64(0);
        add.metadataReal("Realtime").hash("GroupName").setString("V1");

        add.metadataReal("Realtime").hash("GroupColumn").setString("C1");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "T1_X1"}, root));

        add.metadataReal("Realtime").hash("GroupColumn").setString("C2");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "T2_X1"}, root));

        add.metadataReal("Realtime").hash("GroupColumn").setString("C3");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "T3_X1"}, root));

        add.metadataReal("Format").setString("00.0");
        add.metadataReal("Realtime").hash("GroupName").setString("V2");
        add.metadataReal("Realtime").hash("RowOrder").setInt64(1);
        add.metadataReal("Realtime").hash("GroupColumn").setString("C1");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "U1_X1"}, root));

        add.metadataReal("Realtime").hash("GroupColumn").setString("C3");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "U2_X1"}, root));

        add.setEmpty();
        add.metadataReal("Format").setString("000.000");
        add.metadataReal("Realtime").hash("Format").setString("0.000");
        add.metadataReal("Realtime").hash("Name").setString("Value1");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q1_X1"}, root));

        add.metadataReal("Realtime").hash("Name").setString("Value2");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q2_X1"}, root));

        add.metadataReal("Realtime").hash("Name").setString("Value3");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q3_X1"}, root));


        add.setEmpty();
        add.metadataReal("Format").setString("0.0");
        add.metadataReal("Realtime").hash("Name").setString("Line");
        add.metadataReal("Realtime").hash("FullLine").setBool(true);
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "P_X1"}, root));


        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T1_X1"}, Variant::Root(1.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T2_X1"}, Variant::Root(2.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T3_X1"}, Variant::Root(3.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "U1_X1"}, Variant::Root(1.1)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "U2_X1"}, Variant::Root(2.1)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(1.3)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q2_X1"}, Variant::Root(2.3)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q3_X1"}, Variant::Root(3.3)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "P_X1"}, Variant::Root(1.4)));

        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::CenteredLabel, "C1"));
        QVERIFY(checkExists(layout, 0, 2, RealtimeLayout::CenteredLabel, "C2"));
        QVERIFY(checkExists(layout, 0, 3, RealtimeLayout::CenteredLabel, "C3"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "V1:"));
        QVERIFY(checkExists(layout, 2, 0, RealtimeLayout::LeftJustifiedLabel, "V2:"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "1.00"));
        QVERIFY(checkExists(layout, 1, 2, RealtimeLayout::ValueText, "2.00"));
        QVERIFY(checkExists(layout, 1, 3, RealtimeLayout::ValueText, "3.00"));
        QVERIFY(checkExists(layout, 2, 1, RealtimeLayout::ValueText, "1.1"));
        QVERIFY(checkExists(layout, 2, 3, RealtimeLayout::ValueText, "2.1"));
        QVERIFY(checkExists(layout, 3, 0, RealtimeLayout::LeftJustifiedLabel, "Value1:"));
        QVERIFY(checkExists(layout, 4, 0, RealtimeLayout::LeftJustifiedLabel, "Value2:"));
        QVERIFY(checkExists(layout, 3, 2, RealtimeLayout::LeftJustifiedLabel, "Value3:"));
        QVERIFY(checkExists(layout, 3, 1, RealtimeLayout::ValueText, "1.300"));
        QVERIFY(checkExists(layout, 4, 1, RealtimeLayout::ValueText, "2.300"));
        QVERIFY(checkExists(layout, 3, 3, RealtimeLayout::ValueText, "3.300"));
        QVERIFY(checkExists(layout, 5, 0, RealtimeLayout::ValueLine, "Line: 1.4"));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::CenteredLabel, "C1"));
        QVERIFY(checkExists(layout, 0, 2, RealtimeLayout::CenteredLabel, "C2"));
        QVERIFY(checkExists(layout, 0, 3, RealtimeLayout::CenteredLabel, "C3"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "V1:"));
        QVERIFY(checkExists(layout, 2, 0, RealtimeLayout::LeftJustifiedLabel, "V2:"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "1.00"));
        QVERIFY(checkExists(layout, 1, 2, RealtimeLayout::ValueText, "2.00"));
        QVERIFY(checkExists(layout, 1, 3, RealtimeLayout::ValueText, "3.00"));
        QVERIFY(checkExists(layout, 2, 1, RealtimeLayout::ValueText, "1.1"));
        QVERIFY(checkExists(layout, 2, 3, RealtimeLayout::ValueText, "2.1"));
        QVERIFY(checkExists(layout, 3, 0, RealtimeLayout::LeftJustifiedLabel, "Value1:"));
        QVERIFY(checkExists(layout, 4, 0, RealtimeLayout::LeftJustifiedLabel, "Value2:"));
        QVERIFY(checkExists(layout, 3, 2, RealtimeLayout::LeftJustifiedLabel, "Value3:"));
        QVERIFY(checkExists(layout, 3, 1, RealtimeLayout::ValueText, "1.300"));
        QVERIFY(checkExists(layout, 4, 1, RealtimeLayout::ValueText, "2.300"));
        QVERIFY(checkExists(layout, 3, 3, RealtimeLayout::ValueText, "3.300"));
        QVERIFY(checkExists(layout, 5, 0, RealtimeLayout::ValueLine, "Line: 1.4"));


        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T1_X1"}, Variant::Root(4.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T2_X1"}, Variant::Root(5.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T3_X1"}, Variant::Root(6.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "U1_X1"}, Variant::Root(4.1)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "U2_X1"}, Variant::Root(5.1)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(4.3)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q2_X1"}, Variant::Root(5.3)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q3_X1"}, Variant::Root(6.3)));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::CenteredLabel, "C1"));
        QVERIFY(checkExists(layout, 0, 2, RealtimeLayout::CenteredLabel, "C2"));
        QVERIFY(checkExists(layout, 0, 3, RealtimeLayout::CenteredLabel, "C3"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "V1:"));
        QVERIFY(checkExists(layout, 2, 0, RealtimeLayout::LeftJustifiedLabel, "V2:"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "4.00"));
        QVERIFY(checkExists(layout, 1, 2, RealtimeLayout::ValueText, "5.00"));
        QVERIFY(checkExists(layout, 1, 3, RealtimeLayout::ValueText, "6.00"));
        QVERIFY(checkExists(layout, 2, 1, RealtimeLayout::ValueText, "4.1"));
        QVERIFY(checkExists(layout, 2, 3, RealtimeLayout::ValueText, "5.1"));
        QVERIFY(checkExists(layout, 3, 0, RealtimeLayout::LeftJustifiedLabel, "Value1:"));
        QVERIFY(checkExists(layout, 4, 0, RealtimeLayout::LeftJustifiedLabel, "Value2:"));
        QVERIFY(checkExists(layout, 3, 2, RealtimeLayout::LeftJustifiedLabel, "Value3:"));
        QVERIFY(checkExists(layout, 3, 1, RealtimeLayout::ValueText, "4.300"));
        QVERIFY(checkExists(layout, 4, 1, RealtimeLayout::ValueText, "5.300"));
        QVERIFY(checkExists(layout, 3, 3, RealtimeLayout::ValueText, "6.300"));
        QVERIFY(checkExists(layout, 5, 0, RealtimeLayout::ValueLine, "Line: 1.4"));


        add.setEmpty();
        add.metadataReal("Format").setString("0.0000");
        add.metadataReal("Realtime").hash("Format").remove();
        add.metadataReal("Realtime").hash("Name").setString("ValueA");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q1_X1"}, root));

        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::CenteredLabel, "C1"));
        QVERIFY(checkExists(layout, 0, 2, RealtimeLayout::CenteredLabel, "C2"));
        QVERIFY(checkExists(layout, 0, 3, RealtimeLayout::CenteredLabel, "C3"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "V1:"));
        QVERIFY(checkExists(layout, 2, 0, RealtimeLayout::LeftJustifiedLabel, "V2:"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "4.00"));
        QVERIFY(checkExists(layout, 1, 2, RealtimeLayout::ValueText, "5.00"));
        QVERIFY(checkExists(layout, 1, 3, RealtimeLayout::ValueText, "6.00"));
        QVERIFY(checkExists(layout, 2, 1, RealtimeLayout::ValueText, "4.1"));
        QVERIFY(checkExists(layout, 2, 3, RealtimeLayout::ValueText, "5.1"));
        QVERIFY(checkExists(layout, 3, 0, RealtimeLayout::LeftJustifiedLabel, "ValueA:"));
        QVERIFY(checkExists(layout, 4, 0, RealtimeLayout::LeftJustifiedLabel, "Value2:"));
        QVERIFY(checkExists(layout, 3, 2, RealtimeLayout::LeftJustifiedLabel, "Value3:"));
        QVERIFY(checkExists(layout, 3, 1, RealtimeLayout::ValueText, "4.3000"));
        QVERIFY(checkExists(layout, 4, 1, RealtimeLayout::ValueText, "5.300"));
        QVERIFY(checkExists(layout, 3, 3, RealtimeLayout::ValueText, "6.300"));
        QVERIFY(checkExists(layout, 5, 0, RealtimeLayout::ValueLine, "Line: 1.4"));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);
    }

    void basicFlavors()
    {
        RealtimeLayout layout(true);
        Variant::Root root;
        auto add = root.write();

        add.metadataReal("Format").setString("00.00");
        add.metadataReal("Realtime").hash("RowOrder").setInt64(0);
        add.metadataReal("Realtime").hash("GroupName").setString("V1");

        add.metadataReal("Realtime").hash("GroupColumn").setString("C1");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "T1_X1"}, root));

        add.metadataReal("Realtime").hash("GroupColumn").setString("C2");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "T2_X1"}, root));

        add.metadataReal("Realtime").hash("GroupColumn").setString("C3");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "T3_X1"}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "T3_X1", {"pm1"}}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "T3_X1", {"pm10"}}, root));

        add.metadataReal("Format").setString("00.0");
        add.metadataReal("Realtime").hash("GroupName").setString("V2");
        add.metadataReal("Realtime").hash("RowOrder").setInt64(1);
        add.metadataReal("Realtime").hash("GroupColumn").setString("C1");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "U1_X1"}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "U1_X1", {"pm1"}}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "U1_X1", {"pm10"}}, root));

        add.metadataReal("Realtime").hash("GroupColumn").setString("C3");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "U2_X1"}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "U2_X1", {"pm1"}}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "U2_X1", {"pm10"}}, root));

        add.setEmpty();
        add.metadataReal("Format").setString("000.000");
        add.metadataReal("Realtime").hash("Format").setString("0.000");
        add.metadataReal("Realtime").hash("Name").setString("Value1");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q1_X1"}, root));

        add.metadataReal("Realtime").hash("Name").setString("Value2");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q2_X1"}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q2_X1", {"pm1"}}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q2_X1", {"pm10"}}, root));

        add.metadataReal("Realtime").hash("Name").setString("Value3");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q3_X1"}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q3_X1", {"pm1"}}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q3_X1", {"pm10"}}, root));


        add.setEmpty();
        add.metadataReal("Format").setString("0.0");
        add.metadataReal("Realtime").hash("Name").setString("Line");
        add.metadataReal("Realtime").hash("FullLine").setBool(true);
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "P_X1"}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "P_X1", {"pm1"}}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "P_X1", {"pm10"}}, root));


        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T1_X1"}, Variant::Root(1.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T2_X1"}, Variant::Root(2.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T2_X1", {"pm1"}},
                                          Variant::Root(2.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T3_X1"}, Variant::Root(3.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "U1_X1", {"pm10"}},
                                          Variant::Root(1.1)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "U2_X1"}, Variant::Root(2.1)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(1.3)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q2_X1"}, Variant::Root(2.3)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q3_X1"}, Variant::Root(3.3)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "P_X1"}, Variant::Root(1.4)));

        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::CenteredLabel, "C1"));
        QVERIFY(checkExists(layout, 0, 2, RealtimeLayout::CenteredLabel, "C2"));
        QVERIFY(checkExists(layout, 0, 3, RealtimeLayout::CenteredLabel, "C3"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "V1:"));
        QVERIFY(checkExists(layout, 2, 0, RealtimeLayout::LeftJustifiedLabel, "V2:"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "1.00"));
        QVERIFY(checkExists(layout, 1, 2, RealtimeLayout::ValueText, "2.00"));
        QVERIFY(checkExists(layout, 1, 3, RealtimeLayout::ValueText, "3.00"));
        QVERIFY(checkExists(layout, 2, 1, RealtimeLayout::ValueText, "1.1"));
        QVERIFY(checkExists(layout, 2, 3, RealtimeLayout::ValueText, "2.1"));
        QVERIFY(checkExists(layout, 3, 0, RealtimeLayout::LeftJustifiedLabel, "Value1:"));
        QVERIFY(checkExists(layout, 4, 0, RealtimeLayout::LeftJustifiedLabel, "Value2:"));
        QVERIFY(checkExists(layout, 3, 2, RealtimeLayout::LeftJustifiedLabel, "Value3:"));
        QVERIFY(checkExists(layout, 3, 1, RealtimeLayout::ValueText, "1.300"));
        QVERIFY(checkExists(layout, 4, 1, RealtimeLayout::ValueText, "2.300"));
        QVERIFY(checkExists(layout, 3, 3, RealtimeLayout::ValueText, "3.300"));
        QVERIFY(checkExists(layout, 5, 0, RealtimeLayout::ValueLine, "Line: 1.4"));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::CenteredLabel, "C1"));
        QVERIFY(checkExists(layout, 0, 2, RealtimeLayout::CenteredLabel, "C2"));
        QVERIFY(checkExists(layout, 0, 3, RealtimeLayout::CenteredLabel, "C3"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "V1:"));
        QVERIFY(checkExists(layout, 2, 0, RealtimeLayout::LeftJustifiedLabel, "V2:"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "1.00"));
        QVERIFY(checkExists(layout, 1, 2, RealtimeLayout::ValueText, "2.00"));
        QVERIFY(checkExists(layout, 1, 3, RealtimeLayout::ValueText, "3.00"));
        QVERIFY(checkExists(layout, 2, 1, RealtimeLayout::ValueText, "1.1"));
        QVERIFY(checkExists(layout, 2, 3, RealtimeLayout::ValueText, "2.1"));
        QVERIFY(checkExists(layout, 3, 0, RealtimeLayout::LeftJustifiedLabel, "Value1:"));
        QVERIFY(checkExists(layout, 4, 0, RealtimeLayout::LeftJustifiedLabel, "Value2:"));
        QVERIFY(checkExists(layout, 3, 2, RealtimeLayout::LeftJustifiedLabel, "Value3:"));
        QVERIFY(checkExists(layout, 3, 1, RealtimeLayout::ValueText, "1.300"));
        QVERIFY(checkExists(layout, 4, 1, RealtimeLayout::ValueText, "2.300"));
        QVERIFY(checkExists(layout, 3, 3, RealtimeLayout::ValueText, "3.300"));
        QVERIFY(checkExists(layout, 5, 0, RealtimeLayout::ValueLine, "Line: 1.4"));

        layout.incomingData(
                SequenceValue({"bnd", "rt_instant", "T2_X1", {"pm1"}}, Variant::Root()));
        layout.incomingData(
                SequenceValue({"bnd", "rt_instant", "U1_X1", {"pm10"}}, Variant::Root()));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q3_X1"}, Variant::Root()));


        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T1_X1"}, Variant::Root(4.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T2_X1"}, Variant::Root(5.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T3_X1"}, Variant::Root(6.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "U1_X1"}, Variant::Root(4.1)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "U2_X1"}, Variant::Root(5.1)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(4.3)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q2_X1"}, Variant::Root(5.3)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q3_X1", {"pm1"}},
                                          Variant::Root(6.3)));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::CenteredLabel, "C1"));
        QVERIFY(checkExists(layout, 0, 2, RealtimeLayout::CenteredLabel, "C2"));
        QVERIFY(checkExists(layout, 0, 3, RealtimeLayout::CenteredLabel, "C3"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "V1:"));
        QVERIFY(checkExists(layout, 2, 0, RealtimeLayout::LeftJustifiedLabel, "V2:"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "4.00"));
        QVERIFY(checkExists(layout, 1, 2, RealtimeLayout::ValueText, "5.00"));
        QVERIFY(checkExists(layout, 1, 3, RealtimeLayout::ValueText, "6.00"));
        QVERIFY(checkExists(layout, 2, 1, RealtimeLayout::ValueText, "4.1"));
        QVERIFY(checkExists(layout, 2, 3, RealtimeLayout::ValueText, "5.1"));
        QVERIFY(checkExists(layout, 3, 0, RealtimeLayout::LeftJustifiedLabel, "Value1:"));
        QVERIFY(checkExists(layout, 4, 0, RealtimeLayout::LeftJustifiedLabel, "Value2:"));
        QVERIFY(checkExists(layout, 3, 2, RealtimeLayout::LeftJustifiedLabel, "Value3:"));
        QVERIFY(checkExists(layout, 3, 1, RealtimeLayout::ValueText, "4.300"));
        QVERIFY(checkExists(layout, 4, 1, RealtimeLayout::ValueText, "5.300"));
        QVERIFY(checkExists(layout, 3, 3, RealtimeLayout::ValueText, "6.300"));
        QVERIFY(checkExists(layout, 5, 0, RealtimeLayout::ValueLine, "Line: 1.4"));


        add.setEmpty();
        add.metadataReal("Format").setString("0.0000");
        add.metadataReal("Realtime").hash("Format").remove();
        add.metadataReal("Realtime").hash("Name").setString("ValueA");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q1_X1", {"pm1"}}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q1_X1"}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(4.4)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1", {"pm1"}},
                                          Variant::Root(4.4)));

        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::CenteredLabel, "C1"));
        QVERIFY(checkExists(layout, 0, 2, RealtimeLayout::CenteredLabel, "C2"));
        QVERIFY(checkExists(layout, 0, 3, RealtimeLayout::CenteredLabel, "C3"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "V1:"));
        QVERIFY(checkExists(layout, 2, 0, RealtimeLayout::LeftJustifiedLabel, "V2:"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "4.00"));
        QVERIFY(checkExists(layout, 1, 2, RealtimeLayout::ValueText, "5.00"));
        QVERIFY(checkExists(layout, 1, 3, RealtimeLayout::ValueText, "6.00"));
        QVERIFY(checkExists(layout, 2, 1, RealtimeLayout::ValueText, "4.1"));
        QVERIFY(checkExists(layout, 2, 3, RealtimeLayout::ValueText, "5.1"));
        QVERIFY(checkExists(layout, 3, 0, RealtimeLayout::LeftJustifiedLabel, "Value2:"));
        QVERIFY(checkExists(layout, 4, 0, RealtimeLayout::LeftJustifiedLabel, "ValueA:"));
        QVERIFY(checkExists(layout, 3, 2, RealtimeLayout::LeftJustifiedLabel, "Value3:"));
        QVERIFY(checkExists(layout, 3, 1, RealtimeLayout::ValueText, "5.300"));
        QVERIFY(checkExists(layout, 4, 1, RealtimeLayout::ValueText, "4.4000"));
        QVERIFY(checkExists(layout, 3, 3, RealtimeLayout::ValueText, "6.300"));
        QVERIFY(checkExists(layout, 5, 0, RealtimeLayout::ValueLine, "Line: 1.4"));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);
    }

    void columnOrder()
    {
        RealtimeLayout layout(true);
        Variant::Root root;
        auto add = root.write();

        add.metadataReal("Format").setString("00.00");
        add.metadataReal("Realtime").hash("RowOrder").setInt64(0);
        add.metadataReal("Realtime").hash("GroupName").setString("V1");

        add.metadataReal("Realtime").hash("GroupColumn").setString("C1");
        add.metadataReal("Realtime").hash("ColumnOrder").setInt64(2);
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "T1_X1"}, root));

        add.metadataReal("Realtime").hash("GroupColumn").setString("C2");
        add.metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "T2_X1"}, root));

        add.metadataReal("Realtime").hash("GroupColumn").setString("C3");
        add.metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "T3_X1"}, root));


        add.metadataReal("Format").setString("00.0");
        add.metadataReal("Realtime").hash("GroupName").setString("V2");
        add.metadataReal("Realtime").hash("RowOrder").setInt64(1);
        add.metadataReal("Realtime").hash("GroupColumn").setString("C1");
        add.metadataReal("Realtime").hash("ColumnOrder").setInt64(2);
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "U1_X1"}, root));

        add.metadataReal("Realtime").hash("GroupColumn").setString("C3");
        add.metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "U2_X1"}, root));


        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T1_X1"}, Variant::Root(1.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T2_X1"}, Variant::Root(2.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "T3_X1"}, Variant::Root(3.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "U1_X1"}, Variant::Root(1.1)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "U2_X1"}, Variant::Root(2.1)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(1.3)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q2_X1"}, Variant::Root(2.3)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q3_X1"}, Variant::Root(3.3)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "P_X1"}, Variant::Root(1.4)));

        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 3, RealtimeLayout::CenteredLabel, "C1"));
        QVERIFY(checkExists(layout, 0, 2, RealtimeLayout::CenteredLabel, "C2"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::CenteredLabel, "C3"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "V1:"));
        QVERIFY(checkExists(layout, 2, 0, RealtimeLayout::LeftJustifiedLabel, "V2:"));
        QVERIFY(checkExists(layout, 1, 3, RealtimeLayout::ValueText, "1.00"));
        QVERIFY(checkExists(layout, 1, 2, RealtimeLayout::ValueText, "2.00"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "3.00"));
        QVERIFY(checkExists(layout, 2, 3, RealtimeLayout::ValueText, "1.1"));
        QVERIFY(checkExists(layout, 2, 1, RealtimeLayout::ValueText, "2.1"));
    }

    void pages()
    {
        RealtimeLayout layout;
        Variant::Root root;
        auto add = root.write();

        add.metadataReal("Format").setString("0.0");
        add.metadataReal("Realtime").hash("Name").setString("A");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q1_X1"}, root));

        add.metadataReal("Realtime").hash("Page").setInt64(0);
        add.metadataReal("Realtime").hash("Name").setString("B");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q2_X1"}, root));

        add.metadataReal("Realtime").hash("Page").setInt64(1);
        add.metadataReal("Realtime").hash("Name").setString("C");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q3_X1"}, root));

        add.metadataReal("Realtime").hash("Page").remove();
        add.metadataReal("Realtime").hash("PageMask").setInt64(0x3);
        add.metadataReal("Realtime").hash("Name").setString("D");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q4_X1"}, root));

        add.metadataReal("Realtime").remove();
        add.metadataReal("Realtime").array(0).hash("Name").setString("P1");
        add.metadataReal("Realtime").array(1).hash("Name").setString("P2");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q5_X1"}, root));

        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(1.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q2_X1"}, Variant::Root(2.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q3_X1"}, Variant::Root(3.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q4_X1"}, Variant::Root(4.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q5_X1"}, Variant::Root(5.0)));

        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 2);

        QVERIFY(checkExists(0, layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(0, layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "A:"));
        QVERIFY(checkExists(0, layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "B:"));
        QVERIFY(checkExists(0, layout, 2, 0, RealtimeLayout::LeftJustifiedLabel, "D:"));
        QVERIFY(checkExists(0, layout, 3, 0, RealtimeLayout::LeftJustifiedLabel, "P1:"));
        QVERIFY(checkExists(0, layout, 0, 1, RealtimeLayout::ValueText, "1.0"));
        QVERIFY(checkExists(0, layout, 1, 1, RealtimeLayout::ValueText, "2.0"));
        QVERIFY(checkExists(0, layout, 2, 1, RealtimeLayout::ValueText, "4.0"));
        QVERIFY(checkExists(0, layout, 3, 1, RealtimeLayout::ValueText, "5.0"));

        QVERIFY(checkExists(1, layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(1, layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "C:"));
        QVERIFY(checkExists(1, layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "D:"));
        QVERIFY(checkExists(1, layout, 2, 0, RealtimeLayout::LeftJustifiedLabel, "P2:"));
        QVERIFY(checkExists(1, layout, 0, 1, RealtimeLayout::ValueText, "3.0"));
        QVERIFY(checkExists(1, layout, 1, 1, RealtimeLayout::ValueText, "4.0"));
        QVERIFY(checkExists(1, layout, 2, 1, RealtimeLayout::ValueText, "5.0"));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 2);
    }

    void boxes()
    {
        RealtimeLayout layout;
        Variant::Root root;
        auto add = root.write();

        add.metadataReal("Format").setString("0.0");
        add.metadataReal("Realtime").hash("Name").setString("A");
        add.metadataReal("Realtime").hash("Box").setString("Box1");
        add.metadataReal("Realtime").hash("RowOrder").setInt64(0);
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q1_X1"}, root));

        add.metadataReal("Realtime").hash("Name").setString("B");
        add.metadataReal("Realtime").hash("Box").setString("Box1");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q2_X1"}, root));

        add.metadataReal("Realtime").hash("Name").setString("D");
        add.metadataReal("Realtime").hash("Box").setString("Box1");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q4_X1"}, root));

        add.metadataReal("Realtime").hash("Name").setString("C");
        add.metadataReal("Realtime").hash("Box").setString("Box2");
        add.metadataReal("Realtime").hash("RowOrder").setInt64(1);
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q3_X1"}, root));

        add.metadataReal("Realtime").hash("Name").setString("E");
        add.metadataReal("Realtime").hash("Box").setString("Box2");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q5_X1"}, root));

        add.metadataReal("Realtime").hash("Name").setString("F");
        add.metadataReal("Realtime").hash("Box").remove();
        add.metadataReal("Realtime").hash("RowOrder").setInt64(2);
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "Q6_X1"}, root));

        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q1_X1"}, Variant::Root(1.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q2_X1"}, Variant::Root(2.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q3_X1"}, Variant::Root(3.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q4_X1"}, Variant::Root(4.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q5_X1"}, Variant::Root(5.0)));
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "Q6_X1"}, Variant::Root(6.0)));

        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        RealtimeLayout::iterator box1 = layout.begin();
        RealtimeLayout::iterator end = layout.end();
        for (; box1 != end; ++box1) {
            if (box1.type() != RealtimeLayout::GroupBox)
                continue;
            QCOMPARE(box1.text(), QString("Box1"));
            break;
        }
        QVERIFY(box1 != end);

        RealtimeLayout::iterator box2 = box1 + 1;
        for (; box2 != end; ++box2) {
            if (box2.type() != RealtimeLayout::GroupBox)
                continue;
            QCOMPARE(box2.text(), QString("Box2"));
            break;
        }
        QVERIFY(box2 != end);

        RealtimeLayout::iterator box3 = box2 + 1;
        for (; box3 != end; ++box3) {
            if (box3.type() != RealtimeLayout::FinalGroupBox)
                continue;
            QCOMPARE(box3.text(), QString(""));
            break;
        }
        QVERIFY(box3 != end);

        QVERIFY(checkExists(box1, box2, 0, 0, RealtimeLayout::LeftJustifiedLabel, "A:"));
        QVERIFY(checkExists(box1, box2, 1, 0, RealtimeLayout::LeftJustifiedLabel, "B:"));
        QVERIFY(checkExists(box1, box2, 2, 0, RealtimeLayout::LeftJustifiedLabel, "D:"));
        QVERIFY(checkExists(box1, box2, 0, 1, RealtimeLayout::ValueText, "1.0"));
        QVERIFY(checkExists(box1, box2, 1, 1, RealtimeLayout::ValueText, "2.0"));
        QVERIFY(checkExists(box1, box2, 2, 1, RealtimeLayout::ValueText, "4.0"));
        QVERIFY(checkExists(box2, box3, 0, 0, RealtimeLayout::LeftJustifiedLabel, "C:"));
        QVERIFY(checkExists(box2, box3, 1, 0, RealtimeLayout::LeftJustifiedLabel, "E:"));
        QVERIFY(checkExists(box2, box3, 0, 1, RealtimeLayout::ValueText, "3.0"));
        QVERIFY(checkExists(box2, box3, 1, 1, RealtimeLayout::ValueText, "5.0"));
        QVERIFY(checkExists(box3, end, 0, 0, RealtimeLayout::LeftJustifiedLabel, "F:"));
        QVERIFY(checkExists(box3, end, 0, 1, RealtimeLayout::ValueText, "6.0"));
    }

    void array()
    {
        RealtimeLayout layout;
        Variant::Root root;
        auto add = root.write();

        add.metadataArray("Children").metadataReal("Format").setString("0.0");
        add.metadataArray("Realtime").hash("Name").setString("Array1");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "N1_X1"}, root));

        add.metadataArray("Realtime").hash("Name").setString("Array2");
        add.metadataArray("Count").setInt64(4);
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "N2_X1"}, root));

        add.array(0).setDouble(1.0);
        add.array(1).setDouble(2.0);
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "N1_X1"}, root));

        add.array(0).setDouble(3.0);
        add.array(1).setDouble(4.0);
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "N2_X1"}, root));


        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::CenteredLabel, "Array1"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::CenteredLabel, "Array2"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::ValueText, "1.0"));
        QVERIFY(checkExists(layout, 2, 0, RealtimeLayout::ValueText, "2.0"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "3.0"));
        QVERIFY(checkExists(layout, 2, 1, RealtimeLayout::ValueText, "4.0"));
        QVERIFY(checkExists(layout, 3, 1, RealtimeLayout::ValueText, ""));
        QVERIFY(checkExists(layout, 4, 1, RealtimeLayout::ValueText, ""));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        add.array(0).setDouble(5.0);
        add.array(1).setDouble(6.0);
        add.array(2).setDouble(7.0);
        add.array(3).setDouble(8.0);
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "N2_X1"}, root));


        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::CenteredLabel, "Array1"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::CenteredLabel, "Array2"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::ValueText, "1.0"));
        QVERIFY(checkExists(layout, 2, 0, RealtimeLayout::ValueText, "2.0"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "5.0"));
        QVERIFY(checkExists(layout, 2, 1, RealtimeLayout::ValueText, "6.0"));
        QVERIFY(checkExists(layout, 3, 1, RealtimeLayout::ValueText, "7.0"));
        QVERIFY(checkExists(layout, 4, 1, RealtimeLayout::ValueText, "8.0"));
    }

    void flags()
    {
        RealtimeLayout layout;
        Variant::Root root;
        auto add = root.write();

        add.metadataSingleFlag("FlagA").hash("Bits").setInt64(0x01);
        add.metadataSingleFlag("FlagB").hash("Bits").setInt64(0x02);
        add.metadataSingleFlag("FlagC").hash("Bits").setInt64(0x04);
        add.metadataSingleFlag("FlagD").hash("Bits").setInt64(0x08);
        add.metadataFlags("Format").setString("FF");
        add.metadataFlags("Realtime").setBool(true);
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "F1_X1"}, root));

        layout.incomingData(SequenceValue({"bnd", "rt_instant", "F1_X1"},
                                          Variant::Root(Variant::Flags{"FlagA", "FlagC"})));

        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "F1:"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::ValueText, "05"));

        layout.incomingData(SequenceValue({"bnd", "rt_instant", "F1_X1"},
                                          Variant::Root(Variant::Flags{"FlagA"})));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "F1:"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::ValueText, "01"));

        layout.incomingData(SequenceValue({"bnd", "rt_instant", "F1_X1"},
                                          Variant::Root(Variant::Flags{"FlagQ"})));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "F1:"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::ValueText, "00"));
    }

    void stringTranslation()
    {
        RealtimeLayout layout;
        Variant::Root root;
        auto add = root.write();

        add.metadataString("Realtime").hash("Translation").hash("S1").setString("Text1");
        add.metadataString("Realtime").hash("Translation").hash("S2").setString("Text2");
        add.metadataString("Realtime").hash("Name").setString(QString());
        add.metadataString("Realtime").hash("FullLine").setBool(true);
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "ZF_X1"}, root));

        layout.incomingData(SequenceValue({"bnd", "rt_instant", "ZF_X1"}, Variant::Root("S1")));

        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::ValueLine, "Text1"));

        layout.incomingData(SequenceValue({"bnd", "rt_instant", "ZF_X1"}, Variant::Root("S2")));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::ValueLine, "Text2"));

        layout.incomingData(SequenceValue({"bnd", "rt_instant", "ZF_X1"}, Variant::Root("Flarg")));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::ValueLine, "Flarg"));
    }

    void booleanTranslation()
    {
        RealtimeLayout layout;
        Variant::Root root;
        auto add = root.write();

        add.metadataBoolean("Realtime").hash("Translation").hash("TRUE").setString("ON");
        add.metadataBoolean("Realtime").hash("Translation").hash("FALSE").setString("OFF");
        add.metadataBoolean("Realtime").hash("Name").setString("State");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "ZF_X1"}, root));

        layout.incomingData(SequenceValue({"bnd", "rt_instant", "ZF_X1"}, Variant::Root(true)));

        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "State:"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::ValueText, "ON"));

        layout.incomingData(SequenceValue({"bnd", "rt_instant", "ZF_X1"}, Variant::Root(false)));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "State:"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::ValueText, "OFF"));
    }

    void hashFormat()
    {
        Variant::Root hashData;
        hashData["Double1"].setDouble(123.343);
        hashData["Time1"].setDouble(1355258109);
        hashData["Nest/String"].setString("AString");
        hashData["Nest/Flag"].setFlags({"Flag1"});

        QCOMPARE(RealtimeLayout::hashFormat("Nothing", hashData), QString("Nothing"));
        QCOMPARE(RealtimeLayout::hashFormat("${TIME|Time1}", hashData),
                 QString("2012-12-11T20:35:09Z"));
        QCOMPARE(RealtimeLayout::hashFormat("Abc ${number|/Double1|000.0}", hashData),
                 QString("Abc 123.3"));
        QCOMPARE(RealtimeLayout::hashFormat("${Flags|/Nest/Flag} abc", hashData),
                 QString("Flag1 abc"));
        QCOMPARE(RealtimeLayout::hashFormat("${time|/Time1}${String|/Nest/String}", hashData),
                 QString("2012-12-11T20:35:09ZAString"));
        QCOMPARE(RealtimeLayout::hashFormat("${String|Nest/String} ${flags|Nest/Flag|,|uc}",
                                            hashData), QString("AString FLAG1"));


        RealtimeLayout layout;
        Variant::Root root;
        auto add = root.write();

        add.metadataHash("Realtime")
           .hash("HashFormat")
           .setString("${String|/String1} ${String|/String2}");
        add.metadataHash("Realtime").hash("Name").setString("State");
        layout.incomingData(SequenceValue({"bnd", "rt_instant_meta", "ZF_X1"}, root));

        add.setEmpty();
        add.hash("String1").setString("SA1");
        add.hash("String2").setString("SB1");
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "ZF_X1"}, root));

        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "State:"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::ValueText, "SA1 SB1"));

        add.hash("String1").setString("SA1");
        add.hash("String2").setString("SB2");
        layout.incomingData(SequenceValue({"bnd", "rt_instant", "ZF_X1"}, root));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::FinalGroupBox, ""));
        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "State:"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::ValueText, "SA1 SB2"));
    }

    void removeConsoleEscapes()
    {
        QFETCH(QString, input);
        QFETCH(QString, expected);
        QString result(RealtimeLayout::removeConsoleEscapes(input));
        QCOMPARE(result, expected);
    }

    void removeConsoleEscapes_data()
    {
        QTest::addColumn<QString>("input");
        QTest::addColumn<QString>("expected");

        QTest::newRow("Empty") << QString() << QString();
        QTest::newRow("No escapes") << QString("Abcd") << QString("Abcd");
        QTest::newRow("No escapes single") << QString("A") << QString("A");
        QTest::newRow("Single") << QString("&") << QString("&");
        QTest::newRow("Single literal") << QString("&&") << QString("&");
        QTest::newRow("Single character") << QString("&A") << QString("A");
        QTest::newRow("Single character leading") << QString("Blarg &A") << QString("Blarg A");
        QTest::newRow("Single character trailing") << QString("&A Foo") << QString("A Foo");
        QTest::newRow("Single terminator") << QString("1234 &") << QString("1234 &");
        QTest::newRow("Multiple") << QString("Abc&d Ef&gh") << QString("Abcd Efgh");
        QTest::newRow("Multiple literal") << QString("Abc&d && Ef&gh") << QString("Abcd & Efgh");
    }

    void sizeSwitch()
    {
        RealtimeLayout layout(true);
        Variant::Root root;
        auto add = root.write();

        add.metadataReal("Format").setString("0.00");
        add.metadataReal("Realtime").hash("Name").setString("Value");

        layout.incomingData(SequenceValue({"bnd", "rt_boxcar_meta", "T1_X1", {"pm10"}}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_boxcar_meta", "T2_X1", {"pm10"}}, root));

        layout.incomingData(
                SequenceValue({"bnd", "rt_boxcar", "T1_X1", {"pm10"}}, Variant::Root(1.0)));
        layout.incomingData(
                SequenceValue({"bnd", "rt_boxcar", "T2_X1", {"pm10"}}, Variant::Root(2.0)));

        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "Value:"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::ValueText, "1.00"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "Value:"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "2.00"));

        layout.incomingData(SequenceValue({"bnd", "rt_boxcar", "T1_X1", {"pm10"}},
                                          Variant::Root(FP::undefined())));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "Value:"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::ValueText, "    "));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "Value:"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "2.00"));

        layout.incomingData(SequenceValue({"bnd", "rt_boxcar_meta", "T1_X1", {"pm1"}}, root));
        layout.incomingData(SequenceValue({"bnd", "rt_boxcar_meta", "T2_X1", {"pm1"}}, root));

        layout.incomingData(
                SequenceValue({"bnd", "rt_boxcar", "T1_X1", {"pm1"}}, Variant::Root(1.5)));
        layout.incomingData(
                SequenceValue({"bnd", "rt_boxcar", "T2_X1", {"pm1"}}, Variant::Root(2.5)));

        QVERIFY(layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "Value:"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::ValueText, "1.50"));

        layout.incomingData(SequenceValue({"bnd", "rt_boxcar", "T2_X1", {"pm1"}}, Variant::Root()));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "Value:"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::ValueText, "1.50"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "Value:"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "2.00"));

        layout.incomingData(SequenceValue({"bnd", "rt_boxcar", "T1_X1", {"pm10"}},
                                          Variant::Root(FP::undefined())));
        layout.incomingData(SequenceValue({"bnd", "rt_boxcar", "T2_X1", {"pm10"}},
                                          Variant::Root(FP::undefined())));
        layout.incomingData(SequenceValue({"bnd", "rt_boxcar", "T1_X1", {"pm1"}}, Variant::Root()));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "Value:"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::ValueText, "    "));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "Value:"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "    "));

        layout.incomingData(
                SequenceValue({"bnd", "rt_boxcar", "T1_X1", {"pm10"}}, Variant::Root(3.0)));
        layout.incomingData(
                SequenceValue({"bnd", "rt_boxcar", "T2_X1", {"pm10"}}, Variant::Root(4.0)));

        QVERIFY(!layout.update());
        QCOMPARE(layout.pages(), 1);

        QVERIFY(checkExists(layout, 0, 0, RealtimeLayout::LeftJustifiedLabel, "Value:"));
        QVERIFY(checkExists(layout, 0, 1, RealtimeLayout::ValueText, "3.00"));
        QVERIFY(checkExists(layout, 1, 0, RealtimeLayout::LeftJustifiedLabel, "Value:"));
        QVERIFY(checkExists(layout, 1, 1, RealtimeLayout::ValueText, "4.00"));
    }
};

QTEST_APPLESS_MAIN(TestRealtimeLayout)

#include "realtimelayout.moc"
