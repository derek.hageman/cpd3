/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>
#include <QLocalSocket>
#include <QLocalServer>
#include <QCryptographicHash>
#include <QHostInfo>
#include <QTime>
#include <QtEndian>

#include "acquisition/autoprobecontroller.hxx"
#include "core/number.hxx"
#include "core/qtcompat.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Acquisition;
using namespace CPD3::Data;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    double accumulatedTime;

    double intensities[40];
    double deltaI[40];
    bool changing;
    double filterTime;
    int filterID;
    int spotNumber;
    double flow;
    double volume;
    double T;
    double T_case;

    QByteArray sn;
    QByteArray fw;
    double flowCal[4];

    enum {
        MAIN, CFG, CAL,
    } menu;

    ModelInstrument() : incoming(), outgoing(), unpolledRemaining(0)
    {
        for (int i = 0; i < 40; i++) {
            if (i % 4 == 0) {
                intensities[i] = -100.0 + i;
                deltaI[i] = (double) i * -0.005;
            } else if (i < 4 || i >= 40 - 4) {
                intensities[i] = 80000.0 + i;
                deltaI[i] = (double) i * -0.001;
            } else {
                intensities[i] = 100000.0 + i;
                deltaI[i] = (double) i * -1.5;
            }
        }

        changing = false;
        filterTime = 0.0;
        filterID = 0;
        spotNumber = 1;
        flow = 0.6;
        volume = 0.0;
        T = 30.0;
        T_case = 25.0;
        accumulatedTime = 0.0;

        sn = "10.001";
        fw = "10.100";
        flowCal[0] = 0.0;
        flowCal[1] = 1.0;
        flowCal[2] = 0.0;
        flowCal[3] = 0.0;

        menu = MAIN;
    }

    bool advance(double seconds)
    {
        for (int i = 0; i < 40; i++) {
            intensities[i] += deltaI[i] * seconds;
        }
        accumulatedTime += seconds;

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));

            switch (menu) {
            case MAIN:
                if (line == "main") {
                    outgoing.append("main\r");
                    break;
                } else if (line == "hide") {
                    unpolledRemaining = FP::undefined();
                    outgoing.append("unpolled reports disabled\r");
                    break;
                } else if (line == "show") {
                    unpolledRemaining = 0.0;
                    outgoing.append("unpolled reports enabled\r");
                    break;
                } else if (line == "cal") {
                    menu = CAL;
                    outgoing.append("calibration menu\r");
                    break;
                } else if (line == "cfg") {
                    menu = CFG;
                    outgoing.append("configuration menu\r");
                    break;
                } else if (line == "go") {
                    changing = false;
                    filterID++;
                    filterTime = 0;
                    outgoing.append("filter change end\r");
                    break;
                } else if (line == "stop") {
                    changing = true;
                    spotNumber = 0;
                    filterTime = 0;
                    volume = 0.0;
                    outgoing.append("filter change start\r");
                    break;
                } else if (line.startsWith("spot")) {
                    int idxE = -1;
                    if ((idxE = line.indexOf('=')) > 0) {
                        QByteArray value(line.mid(idxE + 1));
                        bool ok = false;
                        int priorSpot = spotNumber;
                        spotNumber = value.toInt(&ok);
                        if (!ok || spotNumber < 0 || spotNumber > 8)
                            spotNumber = 0;
                        if (spotNumber == 0 || priorSpot != spotNumber) {
                            filterTime = 0;
                            volume = 0.0;
                        }
                    }
                    outgoing.append("spot = ");
                    outgoing.append(QByteArray::number(spotNumber));
                    outgoing.append("\r");
                } else {
                    outgoing.append("?\r");
                }
                break;

            case CFG:
                if (line == "main") {
                    menu = MAIN;
                    outgoing.append("main menu\r");
                    break;
                } else if (line == "sn") {
                    outgoing.append("sn = ");
                    outgoing.append(sn);
                    outgoing.append('\r');
                    break;
                } else if (line == "fw") {
                    outgoing.append("fw = ");
                    outgoing.append(fw);
                    outgoing.append('\r');
                    break;
                } else {
                    outgoing.append("?\r");
                }
                break;

            case CAL:
                if (line == "main") {
                    menu = MAIN;
                    outgoing.append("main menu\r");
                    break;
                } else if (line == "flow") {
                    outgoing.append("flow = ");
                    for (int i = 0; i < 4; i++) {
                        if (i != 0) outgoing.append(", ");
                        outgoing.append(QByteArray::number(flowCal[i], 'E'));
                    }
                    outgoing.append('\r');
                    break;
                } else if (line.startsWith("flow")) {
                    int idxE = -1;
                    if ((idxE = line.indexOf('=')) > 0) {
                        QByteArray value(line.mid(idxE + 1));
                        QList<QByteArray> fields(value.split(','));
                        for (int i = 0, max = qMin(4, fields.length()); i < max; i++) {
                            bool ok = false;
                            flowCal[i] = fields.at(i).toDouble(&ok);
                            if (!ok) {
                                flowCal[i] = false;
                                break;
                            }
                        }
                    }
                } else {
                    outgoing.append("?\r");
                }
                break;
            }

            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        if (spotNumber != 0) {
            filterTime += seconds;
            volume += flow * seconds / 60000.0;
        }
        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;

            if (unpolledRemaining <= 0.0) {
                unpolledRemaining = 1.0;

                outgoing.append("03,");
                if (changing)
                    outgoing.append("0001,");
                else
                    outgoing.append("0000,");
                outgoing.append(
                        QByteArray::number((int) floor(filterTime), 16).rightJustified(8, '0'));
                outgoing.append(',');
                outgoing.append(QByteArray::number(filterID, 16).rightJustified(4, '0'));
                outgoing.append(',');
                outgoing.append(QByteArray::number(spotNumber, 16));
                outgoing.append(',');
                outgoing.append(QByteArray::number(flow, 'f', 2));
                outgoing.append(',');
                outgoing.append(QByteArray::number(volume, 'f', 5));
                outgoing.append(',');
                outgoing.append(QByteArray::number(T_case, 'f', 2));
                outgoing.append(',');
                outgoing.append(QByteArray::number(T, 'f', 2));

                for (int i = 0; i < 40; i++) {
                    outgoing.append(',');
                    float f = (float) intensities[i];
                    quint32 raw;
                    memcpy(&raw, &f, 4);
                    raw = qToLittleEndian<quint32>(raw);
                    outgoing.append(QByteArray::number(raw, 16).rightJustified(8, '0'));
                }

                outgoing.append("\r");
                return true;
            }
        }
        return false;
    }
};

class TestAutoprobe : public QObject {
Q_OBJECT

    static bool waitForInterfaceConnection(QLocalServer &server)
    {
        ElapsedTimer st;
        st.start();
        while (st.elapsed() < 30000) {
            QEventLoop loop;
            connect(&server, SIGNAL(newConnection()), &loop, SLOT(quit()));
            QTimer::singleShot(1000, &loop, SLOT(quit()));
            if (server.waitForNewConnection(500))
                return true;
            loop.exec();
        }
        return false;
    }

    QString uid;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        uid = Random::string();

        QVERIFY(qputenv("CPD3_IOINTERFACE_ONE_PROCESS", QByteArray("1")));
    }

    void cleanupTestCase()
    {
        QLocalServer::removeServer(QString("CPD3AutoprobeTest-%1").arg(uid));
    }

    void general()
    {
        QLocalServer localSocketServer;
        QLocalServer::removeServer(QString("CPD3AutoprobeTest-%1").arg(uid));
        QVERIFY(localSocketServer.listen(QString("CPD3AutoprobeTest-%1").arg(uid)));

        Variant::Write config = Variant::Write::empty();
        config["Type"].setString("qtlocalsocket");
        config["Name"].setString(QString("CPD3AutoprobeTest-%1").arg(uid));

        std::unique_ptr<IOInterface> interface(new IOInterface(IOTarget::create(config)));
        interface->start();

        QVERIFY(waitForInterfaceConnection(localSocketServer));
        QLocalSocket *serverSocket = localSocketServer.nextPendingConnection();
        QVERIFY(serverSocket != NULL);
        serverSocket->open(QIODevice::ReadWrite | QIODevice::Unbuffered);
        QVERIFY(!localSocketServer.hasPendingConnections());

        config.setEmpty();
        config["Components/TestA/Name"].setString("acquire_gmd_clap3w");
        config["Components/TestA/Priority"].setInt64(1);
        config["Components/TestA/Instrument"].setString("A22");
        config["Components/TestB/Name"].setString("acquire_gmd_clap3w");
        config["Components/TestB/Priority"].setInt64(2);
        config["Components/TestC/Name"].setString("acquire_gmd_clap3w");
        config["Components/TestC/Priority"].setInt64(0);
        config["Components/TestC/Match/SerialNumber"].setInt64(-1);
        config["Autoprobe/DisableDefaultConfiguration"].setBool(true);
        config["Autoprobe/DisableDefaultComponents"].setBool(true);
        AutoprobeController autoprobe(std::move(interface), ValueSegment::Transfer{
                ValueSegment(FP::undefined(), FP::undefined(), Variant::Root(config))});
        autoprobe.start();

        /* Autoprobe controller is tied to real time, and probably not
         * worth extending it just for testing. */
        ModelInstrument instrument;
        for (double time = 0; time < 60.0; time += 0.1) {
            if (localSocketServer.hasPendingConnections()) {
                serverSocket->close();
                delete serverSocket;
                QVERIFY(localSocketServer.waitForNewConnection(30000));
                serverSocket = localSocketServer.nextPendingConnection();
                QVERIFY(serverSocket != NULL);
                serverSocket->open(QIODevice::ReadWrite | QIODevice::Unbuffered);
            }

            serverSocket->waitForReadyRead(1);
            instrument.incoming += serverSocket->readAll();

            instrument.advance(0.1);
            if (!instrument.outgoing.isEmpty()) {
                serverSocket->write(instrument.outgoing);
                instrument.outgoing.clear();
                serverSocket->waitForBytesWritten(1000);
            }

            QEventLoop loop;
            autoprobe.completed.connect(&loop, std::bind(&QEventLoop::quit, &loop), true);
            QTimer::singleShot(100, &loop, SLOT(quit()));
            if (!autoprobe.isActive())
                break;
            loop.exec();
        }
        if (autoprobe.isActive()) {
            serverSocket->close();
            delete serverSocket;
            QFAIL("Autoprobe component failed");
        }

        auto result = autoprobe.takeResult();
        QVERIFY(result.get() != nullptr);
        QVERIFY(result->acquisition.get() != nullptr);
        QVERIFY(result->interface.get() != nullptr);

        QCOMPARE(result->componentName, QString("acquire_gmd_clap3w"));
        QCOMPARE(result->instrumentName, QString("A22"));

        result->acquisition->signalTerminate();
        result->acquisition->wait(30);

        result->interface.reset();

        serverSocket->close();
        delete serverSocket;
    }
};

QTEST_MAIN(TestAutoprobe)

#include "autoprobe.moc"
