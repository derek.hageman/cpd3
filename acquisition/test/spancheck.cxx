/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>

#include "acquisition/spancheck.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Algorithms;

class TestInterface : public NephelometerSpancheckController::Interface {
    enum {
        Set_Bypass = 0x0001,
        Set_FilteredAir = 0x0002,
        Set_Gas = 0x0004,
        Set_Zero = 0x0008,
        Set_Commands = 0x0010,
        Set_Starting = 0x0020,
        Set_Completed = 0x0040,
        Set_Aborted = 0x0080,
    };
    quint32 set;
    bool bypassState;
    Rayleigh::Gas gas;
    QHash<QString, Variant::Root> commands;
public:
    TestInterface()
    { reset(); }

    virtual ~TestInterface() = default;

    void reset()
    {
        set = 0;
        bypassState = false;
        gas = Rayleigh::Air;
        commands.clear();
    }

    void setBypass(bool enable) override
    {
        set |= Set_Bypass;
        bypassState = enable;
    }

    bool checkBypass(bool expected = true) const
    {
        if (!(set & Set_Bypass))
            return false;
        return bypassState == expected;
    }

    void switchToFilteredAir() override
    {
        set &= ~Set_Gas;
        set |= Set_FilteredAir;
    }

    bool checkOnFilteredAir() const
    {
        return (set & Set_FilteredAir);
    }

    void switchToGas(Rayleigh::Gas gas) override
    {
        set &= ~Set_FilteredAir;
        set |= Set_Gas;
        this->gas = gas;
    }

    bool checkOnGas(Rayleigh::Gas expected) const
    {
        if (!(set & Set_Gas))
            return false;
        return gas == expected;
    }

    void issueZero() override
    {
        set |= Set_Zero;
    }

    bool checkZero() const
    {
        return (set & Set_Zero);
    }

    void issueCommand(const QString &target, const Variant::Read &command) override
    {
        set |= Set_Commands;
        commands.insert(target, Variant::Root(command));
    }

    bool checkCommands(const QHash<QString, Variant::Root> &expected) const
    {
        if (!(set & Set_Commands))
            return false;
        if (expected.size() != commands.size())
            return false;
        for (auto e = expected.begin(); e != expected.end(); ++e) {
            auto c = commands.find(e.key());
            if (c == commands.end())
                return false;
            if (e.value().read() != c.value().read())
                return false;
        }
        return true;
    }

    bool checkGlobalCommands(const QList<Variant::Root> &expected) const
    {
        if (!(set & Set_Commands))
            return false;
        return commands.value(QString()).read() ==
                Variant::Root::overlay(expected.begin(), expected.end()).read();
    }

    void resetCommands()
    {
        commands.clear();
    }

    bool start() override
    {
        set |= Set_Starting;
        return true;
    }

    bool checkStarted() const
    {
        return (set & Set_Starting);
    }

    void completed() override
    {
        set |= Set_Completed;
    }

    bool checkCompleted() const
    {
        return (set & Set_Completed);
    }

    void aborted() override
    {
        set &= ~Set_Completed;
        set |= Set_Aborted;
    }

    bool checkAborted() const
    {
        return (set & Set_Aborted);
    }
};

class TestRealtime : public StreamSink {
    QString lastState;
public:
    TestRealtime()
    { }

    virtual ~TestRealtime()
    { }

    void incomingData(const SequenceValue::Transfer &values) override
    {
        for (const auto &check : values) {
            if (check.getVariable() != "ZSPANCHECKSTATE")
                continue;
            lastState = check.getValue().hash("Current").toQString();
        }
    }

    virtual void endData()
    { }

    bool isInState(const QString &test)
    {
        return lastState == test;
    }
};

class TestSpancheck : public QObject {
Q_OBJECT

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void basic()
    {
        NephelometerSpancheckController spancheck;
        TestInterface interface;
        TestRealtime realtime;
        spancheck.setInterface(&interface);

        double currentTime = 0.0;
        spancheck.advance(currentTime, &realtime);

        Variant::Root command;
        command["StartSpancheck"].setBool(true);
        interface.reset();
        spancheck.command(command);

        while (currentTime < 3000.0 && !realtime.isInState("GasAirFlush")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("GasAirFlush"));
        QVERIFY(interface.checkStarted());
        QVERIFY(interface.checkOnFilteredAir());
        QVERIFY(interface.checkBypass(false));

        while (currentTime < 3000.0 && !realtime.isInState("GasFlush")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("GasFlush"));
        QVERIFY(interface.checkOnGas(Rayleigh::CO2));
        QVERIFY(interface.checkBypass(true));

        while (currentTime < 3000.0 && !realtime.isInState("GasSample")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("GasSample"));
        QVERIFY(interface.checkOnGas(Rayleigh::CO2));
        QVERIFY(interface.checkBypass(true));

        while (currentTime < 3000.0 && !realtime.isInState("AirFlush")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("AirFlush"));
        QVERIFY(interface.checkOnFilteredAir());
        QVERIFY(interface.checkBypass(false));

        while (currentTime < 3000.0 && !realtime.isInState("AirSample")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("AirSample"));
        QVERIFY(interface.checkOnFilteredAir());
        QVERIFY(interface.checkBypass(false));

        while (currentTime < 3000.0 && !realtime.isInState("Inactive")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("Inactive"));
        QVERIFY(interface.checkBypass(false));
        QVERIFY(interface.checkZero());
        QVERIFY(interface.checkCompleted());
    }

    void complex()
    {
        Variant::Root startCommands;
        startCommands["SpanStart"].setBool(true);
        Variant::Root endCommands;
        endCommands["SpanEnd"].setBool(true);

        Variant::Root airActivate;
        airActivate["AirActive"].setBool(true);
        Variant::Root airDeactivate;
        airDeactivate["AirInactive"].setBool(true);

        Variant::Root sf6Activate;
        sf6Activate["SF6Active"].setBool(true);
        Variant::Root sf6Deactivate;
        sf6Deactivate["SF6Inactive"].setBool(true);

        Variant::Root config;
        config["Start/Commands"].set(startCommands);
        config["End/Commands"].set(endCommands);
        config["Air/Activate/Commands"].set(airActivate);
        config["Air/Deactivate/Commands"].set(airDeactivate);
        config["Gas/Type"].setString("SF6");
        config["Gas/Activate/Commands"].set(sf6Activate);
        config["Gas/Deactivate/Commands"].set(sf6Deactivate);

        NephelometerSpancheckController spancheck(ValueSegment::Transfer(), config);
        TestInterface interface;
        TestRealtime realtime;
        spancheck.setInterface(&interface);

        double currentTime = 0.0;
        spancheck.advance(currentTime, &realtime);

        Variant::Root command;
        command["StartSpancheck"].setBool(true);
        interface.reset();
        spancheck.command(command);

        while (currentTime < 3000.0 && !realtime.isInState("GasAirFlush")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("GasAirFlush"));
        QVERIFY(interface.checkStarted());
        QVERIFY(interface.checkOnFilteredAir());
        QVERIFY(interface.checkBypass(false));
        QVERIFY(interface.checkGlobalCommands(
                QList<Variant::Root>() << startCommands << airActivate));
        interface.resetCommands();

        while (currentTime < 3000.0 && !realtime.isInState("GasFlush")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("GasFlush"));
        QVERIFY(interface.checkOnGas(Rayleigh::SF6));
        QVERIFY(interface.checkBypass(true));
        QVERIFY(interface.checkGlobalCommands(
                QList<Variant::Root>() << airDeactivate << sf6Activate));
        interface.resetCommands();

        while (currentTime < 3000.0 && !realtime.isInState("GasSample")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("GasSample"));
        QVERIFY(interface.checkOnGas(Rayleigh::SF6));
        QVERIFY(interface.checkBypass(true));

        while (currentTime < 3000.0 && !realtime.isInState("AirFlush")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("AirFlush"));
        QVERIFY(interface.checkOnFilteredAir());
        QVERIFY(interface.checkBypass(false));
        QVERIFY(interface.checkGlobalCommands(
                QList<Variant::Root>() << sf6Deactivate << airActivate));
        interface.resetCommands();

        while (currentTime < 3000.0 && !realtime.isInState("AirSample")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("AirSample"));

        while (currentTime < 3000.0 && !realtime.isInState("Inactive")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("Inactive"));
        QVERIFY(interface.checkBypass(false));
        QVERIFY(interface.checkZero());
        QVERIFY(interface.checkCompleted());
        QVERIFY(interface.checkGlobalCommands(
                QList<Variant::Root>() << airDeactivate << endCommands));
    }

    void values()
    {
        NephelometerSpancheckController spancheck;
        TestInterface interface;
        TestRealtime realtime;
        spancheck.setInterface(&interface);
        spancheck.setWavelength(0, 550.0, "G");

        double currentTime = 0.0;
        spancheck.advance(currentTime, &realtime);

        Variant::Root command;
        command["StartSpancheck"].setBool(true);
        interface.reset();
        spancheck.command(command);

        double expectedAirRayleigh = 12.267;
        double expectedCO2Rayleigh = expectedAirRayleigh * 2.61;

        while (currentTime < 3000.0 && !realtime.isInState("GasAirFlush")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("GasAirFlush"));
        QVERIFY(interface.checkStarted());
        QVERIFY(interface.checkOnFilteredAir());
        QVERIFY(interface.checkBypass(false));

        while (currentTime < 3000.0 && !realtime.isInState("GasFlush")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("GasFlush"));
        QVERIFY(interface.checkOnGas(Rayleigh::CO2));
        QVERIFY(interface.checkBypass(true));

        while (currentTime < 3000.0 && !realtime.isInState("GasSample")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("GasSample"));
        QVERIFY(interface.checkOnGas(Rayleigh::CO2));
        QVERIFY(interface.checkBypass(true));

        /* 5 percent positive error, but with 0.5 zero offset */
        double zeroOffset = 0.5;
        double measuredGas = expectedCO2Rayleigh * 1.05 + zeroOffset - expectedAirRayleigh;
        spancheck.updateTotal(NephelometerSpancheckController::Scattering, measuredGas, 0);
        spancheck.updateTotal(NephelometerSpancheckController::MeasurementCounts, 10100.0, 0);
        spancheck.update(NephelometerSpancheckController::ReferenceCounts, 200000.0, 0);
        spancheck.update(NephelometerSpancheckController::DarkCounts, 100.0);
        spancheck.update(NephelometerSpancheckController::Temperature, 0.0);
        spancheck.update(NephelometerSpancheckController::Pressure, 1013.25);

        while (currentTime < 3000.0 && !realtime.isInState("AirFlush")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("AirFlush"));
        QVERIFY(interface.checkOnFilteredAir());
        QVERIFY(interface.checkBypass(false));

        while (currentTime < 3000.0 && !realtime.isInState("AirSample")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("AirSample"));
        QVERIFY(interface.checkOnFilteredAir());
        QVERIFY(interface.checkBypass(false));

        spancheck.updateTotal(NephelometerSpancheckController::Scattering, zeroOffset, 0);
        spancheck.updateTotal(NephelometerSpancheckController::MeasurementCounts, 5125.0, 0);
        spancheck.update(NephelometerSpancheckController::ReferenceCounts, 200000.0, 0);
        spancheck.update(NephelometerSpancheckController::DarkCounts, 125.0);
        spancheck.update(NephelometerSpancheckController::Temperature, 0.0);
        spancheck.update(NephelometerSpancheckController::Pressure, 1013.25);

        while (currentTime < 3000.0 && !realtime.isInState("Inactive")) {
            currentTime += 10.0;
            spancheck.advance(currentTime, &realtime);
        }
        QVERIFY(realtime.isInState("Inactive"));
        QVERIFY(interface.checkBypass(false));
        QVERIFY(interface.checkZero());
        QVERIFY(interface.checkCompleted());

        auto result = spancheck.results(NephelometerSpancheckController::FullResults);

        QVERIFY(fabs(result["PCT"].toDouble() - 5.0) < 0.1);
        QVERIFY(fabs(result["G/Results/#0/Air/Bsf"].toDouble() - expectedAirRayleigh) < 0.1);
        QCOMPARE(result["G/Results/#0/Air/Bs"].toDouble(), zeroOffset);
        QVERIFY(fabs(result["G/Results/#0/Gas/Bsf"].toDouble() - expectedCO2Rayleigh) < 0.1);
        QCOMPARE(result["G/Results/#0/Gas/Bs"].toDouble(), measuredGas);
        QVERIFY(fabs(result["G/Results/#0/PCT"].toDouble() - 5.0) < 0.1);
        double sensitivityFactor = (10000.0 - 5000.0) / (2.61 - 1.0);
        QVERIFY(fabs(result["G/Results/#0/Cc"].toDouble() - sensitivityFactor) < 0.1);
    }
};

QTEST_MAIN(TestSpancheck)

#include "spancheck.moc"
