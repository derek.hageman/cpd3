/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#endif

#include <QtGlobal>
#include <QTest>
#include <QTcpServer>
#include <QUdpSocket>

#include "acquisition/iotarget.hxx"
#include "io/drivers/tcp.hxx"
#include "io/drivers/localsocket.hxx"
#include "io/drivers/unixsocket.hxx"
#include "io/drivers/udp.hxx"
#include "io/drivers/file.hxx"

using namespace CPD3;
using namespace CPD3::Acquisition;
using namespace CPD3::Data;

//#define FIRST_SERIAL_PORT   "/dev/ttyUSB0"
//#define SECOND_SERIAL_PORT  "/dev/ttyUSB2"


class TestIOTarget : public QObject {
Q_OBJECT

    static std::uint16_t availableTCPPort()
    {
        QTcpServer server;
        server.listen();
        return server.serverPort();
    }

    static std::uint16_t availableUDPPort()
    {
        QUdpSocket server;
        server.bind();
        return server.localPort();
    }

private slots:

    void initTestCase()
    {
        //Logging::suppressForTesting();
    }

    void connectionBasic()
    {
        std::unique_ptr<IOTarget> target;
        {
            auto allLocal = IOTarget::enumerateLocal();
            if (!allLocal.empty()) {
                target = std::move(allLocal.front());
            }
        }
        if (!target) {
            Variant::Root config;
#ifdef Q_OS_WIN32
            config["Port"] = "COM1";
#else
            config["Port"] = "/dev/ttyS0";
#endif
            target = IOTarget::create(config);
        }
        if (!target) {
            QSKIP("No available target device");
        }

        std::mutex mutex;
        std::condition_variable cv;
        std::vector<std::unique_ptr<IO::Socket::Connection>> connections;

        auto servers = target->serverListeners([&](std::unique_ptr<IO::Socket::Connection> &&c) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                connections.emplace_back(std::move(c));
            }
            cv.notify_all();
        });
        QVERIFY(!servers.empty());

        auto client = target->clientConnection(true);
        QVERIFY(client.get() != nullptr);

        std::unique_ptr<IO::Socket::Connection> accepted;
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !connections.empty(); });
            QCOMPARE((int) connections.size(), 1);
            accepted = std::move(connections.front());
            connections.clear();
        }

        Util::ByteArray dataReceivedByClient;
        bool clientEnded = false;
        client->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                dataReceivedByClient += data;
            }
            cv.notify_all();
        });
        client->ended.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientEnded = true;
            }
            cv.notify_all();
        });
        client->start();

        Util::ByteArray dataReceivedByServer;
        bool serverEnded = false;
        accepted->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                dataReceivedByServer += data;
            }
            cv.notify_all();
        });
        accepted->ended.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverEnded = true;
            }
            cv.notify_all();
        });
        accepted->start();

        QByteArray dataSentToClient = "DataToServer";
        client->write(dataSentToClient);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock,
                    [&] { return (int) dataReceivedByServer.size() >= dataSentToClient.size(); });
            QCOMPARE(dataReceivedByServer.toQByteArrayRef(), dataSentToClient);
        }

        QByteArray dataSentToServer = "DataToClient";
        accepted->write(dataSentToServer);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock,
                    [&] { return (int) dataReceivedByClient.size() >= dataSentToServer.size(); });
            QCOMPARE(dataReceivedByClient.toQByteArrayRef(), dataSentToServer);
        }

        client.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return serverEnded; });
            QCOMPARE(dataReceivedByClient.toQByteArrayRef(), dataSentToServer);
            QCOMPARE(dataReceivedByServer.toQByteArrayRef(), dataSentToClient);
        }

        accepted.reset();
        servers.clear();
        connections.clear();
    }

#ifdef FIRST_SERIAL_PORT

    void openSerialPort()
    {
        Variant::Root config;
        config["Port"] = FIRST_SERIAL_PORT;
        auto target = IOTarget::create(config);

        QVERIFY(target.get() != nullptr);
        QVERIFY(target->isValid());
        QVERIFY(target->triggerAutoprobeWhenClosed());
        QVERIFY(target->description().startsWith(FIRST_SERIAL_PORT));
        QVERIFY(target->spawnServer());

        {
            auto serialized = target->serialize();
            auto deserialized = IOTarget::deserialize(serialized);
            QVERIFY(deserialized.get() != nullptr);
            QVERIFY(deserialized->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*deserialized));
        }

        {
            auto check = target->configuration();
            QCOMPARE(check["Port"].toQString(), QString(FIRST_SERIAL_PORT));
            check = target->configuration(true);
            QCOMPARE(check["Port"].toQString(), QString(FIRST_SERIAL_PORT));
        }

        {
            auto check = target->clone();
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
            check->merge(*target);
        }

        {
            auto check = target->clone(true);
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
        }

        auto device = target->backingDevice();
        QVERIFY(device.get() != nullptr);
        device->start();
        QVERIFY(!device->isEnded());
        device->reset();
    }

#ifdef SECOND_SERIAL_PORT

    void nullModemLoopback()
    {
        std::mutex mutex;
        std::condition_variable cv;

        Variant::Root config;
        config["Port"] = FIRST_SERIAL_PORT;
        auto firstTarget = IOTarget::create(config);
        QVERIFY(firstTarget.get() != nullptr);
        QVERIFY(firstTarget->isValid());
        QVERIFY(firstTarget->triggerAutoprobeWhenClosed());
        QVERIFY(firstTarget->description().startsWith(FIRST_SERIAL_PORT));
        QVERIFY(firstTarget->spawnServer());

        config["Type"] = "SerialPort";
        config["Port"] = SECOND_SERIAL_PORT;
        config["Baud"].setInteger(38400);
        auto secondTarget = IOTarget::create(config);
        QVERIFY(secondTarget.get() != nullptr);
        QVERIFY(secondTarget->isValid());
        QVERIFY(secondTarget->triggerAutoprobeWhenClosed());
        QVERIFY(secondTarget->description().startsWith(SECOND_SERIAL_PORT));
        QVERIFY(secondTarget->spawnServer());

        QVERIFY(!firstTarget->matchesDevice(*secondTarget));
        QVERIFY(!secondTarget->matchesDevice(*firstTarget));

        auto firstDevice = firstTarget->backingDevice();
        QVERIFY(firstDevice.get() != nullptr);
        auto secondDevice = secondTarget->backingDevice();
        QVERIFY(secondDevice.get() != nullptr);

        config["Port"].remove();
        {
            auto over = IOTarget::create(config);
            QVERIFY(over.get() != nullptr);
            firstTarget->merge(*over, firstDevice.get());
        }

        Util::ByteArray firstData;
        firstDevice->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                firstData += data;
            }
            cv.notify_all();
        });
        Util::ByteArray secondData;
        secondDevice->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                secondData += data;
            }
            cv.notify_all();
        });

        firstDevice->start();
        secondDevice->start();

        QTest::qSleep(250);
        {
            std::lock_guard<std::mutex> lock(mutex);
            firstData.clear();
            secondData.clear();
        }

        QByteArray send = "SomeDataSent";
        firstDevice->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) secondData.size() >= send.size(); });
            QCOMPARE(secondData.toQByteArrayRef(), send);
            QVERIFY(firstData.empty());
            secondData.clear();
        }

        send = "OtherDataSent";
        secondDevice->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) firstData.size() >= send.size(); });
            QCOMPARE(firstData.toQByteArrayRef(), send);
            QVERIFY(secondData.empty());
            firstData.clear();
        }

        config["Baud"].setInteger(9600);
        {
            auto over = IOTarget::create(config);
            QVERIFY(over.get() != nullptr);
            firstTarget->merge(*over, firstDevice.get());
            secondTarget->merge(*over, secondDevice.get());
        }

        QTest::qSleep(100);
        {
            std::lock_guard<std::mutex> lock(mutex);
            firstData.clear();
            secondData.clear();
        }

        QByteArray alternateSend = "datadatadata";
        secondDevice->write(send);
        firstDevice->write(alternateSend);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) firstData.size() >= send.size(); });
            QCOMPARE(firstData.toQByteArrayRef(), send);
            cv.wait(lock, [&] { return (int) secondData.size() >= alternateSend.size(); });
            QCOMPARE(secondData.toQByteArrayRef(), alternateSend);
        }
    }

#endif

#endif

    void tcpConnection()
    {
        std::mutex mutex;
        std::condition_variable cv;
        std::list<std::unique_ptr<IO::Socket::Connection>> incomingConnections;

        IO::Socket::TCP::Server server([&](std::unique_ptr<IO::Socket::Connection> &&c) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                incomingConnections.emplace_back(std::move(c));
            }
            cv.notify_all();
        });
        QVERIFY(server.startListening());
        auto port = server.localPort();
        QVERIFY(port > 0);

        Variant::Root config;
        config["Type"] = "RemoteServer";
        config["Server"] = "localhost";
        config["ServerPort"].setInteger(port);
        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        QVERIFY(target->isValid());
        QVERIFY(!target->triggerAutoprobeWhenClosed());
        QVERIFY(target->spawnServer());

        {
            auto serialized = target->serialize();
            auto deserialized = IOTarget::deserialize(serialized);
            QVERIFY(deserialized.get() != nullptr);
            QVERIFY(deserialized->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*deserialized));
        }

        {
            auto check = target->configuration();
            QCOMPARE((int) check["ServerPort"].toInteger(), (int) port);
            QCOMPARE(check["Server"].toQString(), QString("localhost"));
        }

        {
            auto check = target->clone();
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
            check->merge(*target);
        }

        auto device = target->backingDevice();
        QVERIFY(device.get() != nullptr);

        Util::ByteArray clientData;
        bool clientEnded = false;
        device->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientData += data;
            }
            cv.notify_all();
        });
        device->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientEnded = true;
            }
            cv.notify_all();
        });

        device->start();

        std::unique_ptr<IO::Socket::Connection> serverConnection;
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !incomingConnections.empty(); });
            QVERIFY(!incomingConnections.empty());
            serverConnection = std::move(incomingConnections.front());
            incomingConnections.clear();
        }

        Util::ByteArray serverData;
        bool serverEnded = false;
        serverConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData += data;
            }
            cv.notify_all();
        });
        serverConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverEnded = true;
            }
            cv.notify_all();
        });

        serverConnection->start();

        QByteArray send = "SomeDataSent";
        device->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QVERIFY(clientData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            serverData.clear();
        }

        send = "OtherDataSent";
        serverConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) clientData.size() >= send.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), send);
            QVERIFY(serverData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            clientData.clear();
        }

        QVERIFY(!device->isEnded());

        serverConnection.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return clientEnded; });
            QVERIFY(clientEnded);
        }

        QVERIFY(device->isEnded());
    }

    void localConnection()
    {
        auto serverName = "CPD3Test-" + Random::string();

        std::mutex mutex;
        std::condition_variable cv;
        std::list<std::unique_ptr<IO::Socket::Connection>> incomingConnections;

        IO::Socket::Local::Server server([&](std::unique_ptr<IO::Socket::Connection> &&c) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                incomingConnections.emplace_back(std::move(c));
            }
            cv.notify_all();
        }, serverName);
        QVERIFY(server.startListening());

        Variant::Root config;
        config["Type"] = "QtLocalSocket";
        config["Name"] = serverName;
        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        QVERIFY(target->isValid());
        QVERIFY(!target->triggerAutoprobeWhenClosed());
        QVERIFY(target->spawnServer());

        {
            auto serialized = target->serialize();
            auto deserialized = IOTarget::deserialize(serialized);
            QVERIFY(deserialized.get() != nullptr);
            QVERIFY(deserialized->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*deserialized));
        }

        {
            auto check = target->configuration();
            QCOMPARE(check["Name"].toQString(), serverName);
        }

        {
            auto check = target->clone();
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
            check->merge(*target);
        }

        auto device = target->backingDevice();
        QVERIFY(device.get() != nullptr);

        Util::ByteArray clientData;
        bool clientEnded = false;
        device->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientData += data;
            }
            cv.notify_all();
        });
        device->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientEnded = true;
            }
            cv.notify_all();
        });

        device->start();

        std::unique_ptr<IO::Socket::Connection> serverConnection;
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !incomingConnections.empty(); });
            QVERIFY(!incomingConnections.empty());
            serverConnection = std::move(incomingConnections.front());
            incomingConnections.clear();
        }

        Util::ByteArray serverData;
        bool serverEnded = false;
        serverConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData += data;
            }
            cv.notify_all();
        });
        serverConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverEnded = true;
            }
            cv.notify_all();
        });

        serverConnection->start();

        QByteArray send = "SomeDataSent";
        device->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QVERIFY(clientData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            serverData.clear();
        }

        send = "OtherDataSent";
        serverConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) clientData.size() >= send.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), send);
            QVERIFY(serverData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            clientData.clear();
        }

        QVERIFY(!device->isEnded());

        serverConnection.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return clientEnded; });
            QVERIFY(clientEnded);
        }

        QVERIFY(device->isEnded());
    }

#ifdef Q_OS_UNIX

    void unixSocketConnection()
    {
        auto serverName = "/tmp/CPD3Test-" + Random::string();

        std::mutex mutex;
        std::condition_variable cv;
        std::list<std::unique_ptr<IO::Socket::Connection>> incomingConnections;

        IO::Socket::Local::Server server([&](std::unique_ptr<IO::Socket::Connection> &&c) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                incomingConnections.emplace_back(std::move(c));
            }
            cv.notify_all();
        }, serverName);
        QVERIFY(server.startListening());

        Variant::Root config;
        config["Type"] = "LocalSocket";
        config["File"] = serverName;
        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        QVERIFY(target->isValid());
        QVERIFY(!target->triggerAutoprobeWhenClosed());
        QVERIFY(target->spawnServer());

        {
            auto serialized = target->serialize();
            auto deserialized = IOTarget::deserialize(serialized);
            QVERIFY(deserialized.get() != nullptr);
            QVERIFY(deserialized->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*deserialized));
        }

        {
            auto check = target->configuration();
            QCOMPARE(check["File"].toQString(), serverName);
        }

        {
            auto check = target->clone();
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
            check->merge(*target);
        }

        auto device = target->backingDevice();
        QVERIFY(device.get() != nullptr);

        Util::ByteArray clientData;
        bool clientEnded = false;
        device->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientData += data;
            }
            cv.notify_all();
        });
        device->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientEnded = true;
            }
            cv.notify_all();
        });

        device->start();

        std::unique_ptr<IO::Socket::Connection> serverConnection;
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !incomingConnections.empty(); });
            QVERIFY(!incomingConnections.empty());
            serverConnection = std::move(incomingConnections.front());
            incomingConnections.clear();
        }

        Util::ByteArray serverData;
        bool serverEnded = false;
        serverConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData += data;
            }
            cv.notify_all();
        });
        serverConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverEnded = true;
            }
            cv.notify_all();
        });

        serverConnection->start();

        QByteArray send = "SomeDataSent";
        device->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QVERIFY(clientData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            serverData.clear();
        }

        send = "OtherDataSent";
        serverConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) clientData.size() >= send.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), send);
            QVERIFY(serverData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            clientData.clear();
        }

        QVERIFY(!device->isEnded());

        serverConnection.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return clientEnded; });
            QVERIFY(clientEnded);
        }

        QVERIFY(device->isEnded());
    }

#endif

    void udp()
    {
        auto clientPort = availableUDPPort();

        std::mutex mutex;
        std::condition_variable cv;
        std::list<std::unique_ptr<IO::Socket::Connection>> incomingConnections;

        IO::UDP::Configuration cfg;
        cfg.listen(QHostAddress::LocalHost);
        cfg.listen(QHostAddress::LocalHostIPv6);
        cfg.loopback(clientPort);
        cfg.waitForResolution = true;
        cfg.requireAllRemote = true;

        auto serverConnection = cfg();
        QVERIFY(serverConnection.get() != nullptr);

        Util::ByteArray serverData;
        bool serverEnded = false;
        serverConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData += data;
            }
            cv.notify_all();
        });
        serverConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverEnded = true;
            }
            cv.notify_all();
        });

        serverConnection->start();

        auto serverPort = serverConnection->localPort();
        QVERIFY(serverPort > 0);

        Variant::Root config;
        config["Type"] = "UDP";
        config["ListenAddress"] = "localhost";
        config["LocalPort"].setInteger(clientPort);
        config["Server"] = "localhost";
        config["ServerPort"].setInteger(serverPort);
        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        QVERIFY(target->isValid());
        QVERIFY(!target->triggerAutoprobeWhenClosed());
        QVERIFY(target->spawnServer());

        {
            auto serialized = target->serialize();
            auto deserialized = IOTarget::deserialize(serialized);
            QVERIFY(deserialized.get() != nullptr);
            QVERIFY(deserialized->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*deserialized));
        }

        {
            auto check = target->configuration();
            QCOMPARE((int) check["ServerPort"].toInteger(), (int) serverPort);
            QCOMPARE(check["Server"].toQString(), QString("localhost"));
            QCOMPARE((int) check["LocalPort"].toInteger(), (int) clientPort);
            QCOMPARE(check["ListenAddress"].toQString(), QString("localhost"));
        }

        {
            auto check = target->configuration(true);
            QCOMPARE((int) check["ServerPort"].toInteger(), (int) serverPort);
            QCOMPARE(check["Server"].toQString(), QString("localhost"));
            QCOMPARE((int) check["LocalPort"].toInteger(), (int) clientPort);
            QCOMPARE(check["ListenAddress"].toQString(), QString("localhost"));
        }

        {
            auto check = target->clone();
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
            check->merge(*target);
        }

        {
            auto check = target->clone(true);
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
            check->merge(*target);
        }

        auto device = target->backingDevice();
        QVERIFY(device.get() != nullptr);

        Util::ByteArray clientData;
        bool clientEnded = false;
        device->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientData += data;
            }
            cv.notify_all();
        });
        device->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientEnded = true;
            }
            cv.notify_all();
        });

        device->start();

        QByteArray send = "SomeDataSent";
        device->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QVERIFY(clientData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            serverData.clear();
        }

        send = "OtherDataSent";
        serverConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) clientData.size() >= send.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), send);
            QVERIFY(serverData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            clientData.clear();
        }

        QVERIFY(!device->isEnded());
    }

    void tcpServer()
    {
        auto port = availableTCPPort();

        std::mutex mutex;
        std::condition_variable cv;

        Variant::Root config;
        config["Type"] = "TCPListen";
        config["ListenAddress"] = "localhost";
        config["LocalPort"].setInteger(port);
        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        QVERIFY(target->isValid());
        QVERIFY(!target->triggerAutoprobeWhenClosed());
        QVERIFY(target->spawnServer());

        {
            auto serialized = target->serialize();
            auto deserialized = IOTarget::deserialize(serialized);
            QVERIFY(deserialized.get() != nullptr);
            QVERIFY(deserialized->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*deserialized));
        }

        {
            auto check = target->configuration();
            QCOMPARE((int) check["LocalPort"].toInteger(), (int) port);
            QCOMPARE(check["ListenAddress"].toQString(), QString("localhost"));
        }

        {
            auto check = target->clone();
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
            check->merge(*target);
        }

        auto device = target->backingDevice();
        QVERIFY(device.get() != nullptr);

        Util::ByteArray serverData;
        bool serverEnded = false;
        device->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData += data;
            }
            cv.notify_all();
        });
        device->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverEnded = true;
            }
            cv.notify_all();
        });

        device->start();

        auto clientConnection = IO::Socket::TCP::loopback({port}, true);
        QVERIFY(clientConnection.get() != nullptr);

        Util::ByteArray clientData;
        bool clientEnded = false;
        clientConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientData += data;
            }
            cv.notify_all();
        });
        clientConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientEnded = true;
            }
            cv.notify_all();
        });

        clientConnection->start();

        QByteArray send = "SomeDataSent";
        clientConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QVERIFY(clientData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            serverData.clear();
        }

        send = "OtherDataSent";
        device->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) clientData.size() >= send.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), send);
            QVERIFY(serverData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            clientData.clear();
        }


        QVERIFY(!device->isEnded());
        QVERIFY(!clientConnection->isEnded());
        clientConnection.reset();
        {
            std::lock_guard<std::mutex> lock(mutex);
            QVERIFY(!serverEnded);
        }
        QVERIFY(!device->isEnded());

        clientConnection = IO::Socket::TCP::loopback({port}, true);
        QVERIFY(clientConnection.get() != nullptr);


        clientConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientData += data;
            }
            cv.notify_all();
        });
        clientConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientEnded = true;
            }
            cv.notify_all();
        });

        clientConnection->start();

        send = "MoreDataSent";
        clientConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QVERIFY(clientData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            serverData.clear();
        }

        send = "Final";
        device->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) clientData.size() >= send.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), send);
            QVERIFY(serverData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            clientData.clear();
        }

        device.reset();

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return clientEnded; });
            QVERIFY(clientEnded);
        }
    }

    void localServer()
    {
        auto serverName = "CPD3Test-" + Random::string();

        std::mutex mutex;
        std::condition_variable cv;

        Variant::Root config;
        config["Type"] = "QtLocalListen";
        config["Name"] = serverName;
        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        QVERIFY(target->isValid());
        QVERIFY(!target->triggerAutoprobeWhenClosed());
        QVERIFY(target->spawnServer());

        {
            auto serialized = target->serialize();
            auto deserialized = IOTarget::deserialize(serialized);
            QVERIFY(deserialized.get() != nullptr);
            QVERIFY(deserialized->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*deserialized));
        }

        {
            auto check = target->configuration();
            QCOMPARE(check["Name"].toQString(), serverName);
        }

        {
            auto check = target->clone();
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
            check->merge(*target);
        }

        auto device = target->backingDevice();
        QVERIFY(device.get() != nullptr);

        Util::ByteArray serverData;
        bool serverEnded = false;
        device->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData += data;
            }
            cv.notify_all();
        });
        device->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverEnded = true;
            }
            cv.notify_all();
        });

        device->start();

        auto clientConnection = IO::Socket::Local::connect(serverName, {}, true);
        QVERIFY(clientConnection.get() != nullptr);

        Util::ByteArray clientData;
        bool clientEnded = false;
        clientConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientData += data;
            }
            cv.notify_all();
        });
        clientConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientEnded = true;
            }
            cv.notify_all();
        });

        clientConnection->start();

        QByteArray send = "SomeDataSent";
        clientConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QVERIFY(clientData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            serverData.clear();
        }

        send = "OtherDataSent";
        device->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) clientData.size() >= send.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), send);
            QVERIFY(serverData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            clientData.clear();
        }


        QVERIFY(!device->isEnded());
        QVERIFY(!clientConnection->isEnded());
        clientConnection.reset();
        {
            std::lock_guard<std::mutex> lock(mutex);
            QVERIFY(!serverEnded);
        }
        QVERIFY(!device->isEnded());

        clientConnection = IO::Socket::Local::connect(serverName, {}, true);
        QVERIFY(clientConnection.get() != nullptr);


        clientConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientData += data;
            }
            cv.notify_all();
        });
        clientConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientEnded = true;
            }
            cv.notify_all();
        });

        clientConnection->start();

        send = "MoreDataSent";
        clientConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QVERIFY(clientData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            serverData.clear();
        }

        send = "Final";
        device->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) clientData.size() >= send.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), send);
            QVERIFY(serverData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            clientData.clear();
        }

        device.reset();

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return clientEnded; });
            QVERIFY(clientEnded);
        }
    }

#ifdef Q_OS_UNIX

    void unixSocketServer()
    {
        auto serverName = "/tmp/CPD3Test-" + Random::string();

        std::mutex mutex;
        std::condition_variable cv;

        Variant::Root config;
        config["Type"] = "LocalListen";
        config["File"] = serverName;
        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        QVERIFY(target->isValid());
        QVERIFY(!target->triggerAutoprobeWhenClosed());
        QVERIFY(target->spawnServer());

        {
            auto serialized = target->serialize();
            auto deserialized = IOTarget::deserialize(serialized);
            QVERIFY(deserialized.get() != nullptr);
            QVERIFY(deserialized->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*deserialized));
        }

        {
            auto check = target->configuration();
            QCOMPARE(check["File"].toQString(), serverName);
        }

        {
            auto check = target->clone();
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
            check->merge(*target);
        }

        auto device = target->backingDevice();
        QVERIFY(device.get() != nullptr);

        Util::ByteArray serverData;
        bool serverEnded = false;
        device->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverData += data;
            }
            cv.notify_all();
        });
        device->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverEnded = true;
            }
            cv.notify_all();
        });

        device->start();

        auto clientConnection = IO::Socket::Unix::connect(serverName.toStdString());
        QVERIFY(clientConnection.get() != nullptr);

        Util::ByteArray clientData;
        bool clientEnded = false;
        clientConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientData += data;
            }
            cv.notify_all();
        });
        clientConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientEnded = true;
            }
            cv.notify_all();
        });

        clientConnection->start();

        QByteArray send = "SomeDataSent";
        clientConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QVERIFY(clientData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            serverData.clear();
        }

        send = "OtherDataSent";
        device->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) clientData.size() >= send.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), send);
            QVERIFY(serverData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            clientData.clear();
        }


        QVERIFY(!device->isEnded());
        QVERIFY(!clientConnection->isEnded());
        clientConnection.reset();
        {
            std::lock_guard<std::mutex> lock(mutex);
            QVERIFY(!serverEnded);
        }
        QVERIFY(!device->isEnded());

        clientConnection = IO::Socket::Unix::connect(serverName.toStdString());
        QVERIFY(clientConnection.get() != nullptr);


        clientConnection->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientData += data;
            }
            cv.notify_all();
        });
        clientConnection->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientEnded = true;
            }
            cv.notify_all();
        });

        clientConnection->start();

        send = "MoreDataSent";
        clientConnection->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) serverData.size() >= send.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), send);
            QVERIFY(clientData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            serverData.clear();
        }

        send = "Final";
        device->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) clientData.size() >= send.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), send);
            QVERIFY(serverData.empty());
            QVERIFY(!serverEnded);
            QVERIFY(!clientEnded);
            clientData.clear();
        }

        device.reset();

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return clientEnded; });
            QVERIFY(clientEnded);
        }
    }

#endif

#ifdef Q_OS_UNIX

    void fifo()
    {
        struct TemporaryFifo {
            std::string name;

            TemporaryFifo() = default;

            bool create(std::string target)
            {
                if (::mkfifo(target.c_str(), 0600) != 0) {
                    return false;
                }

                name = std::move(target);
                return true;
            }

            bool create(const QString &target)
            { return create(target.toStdString()); }

            ~TemporaryFifo()
            {
                if (name.empty())
                    return;
                ::unlink(name.c_str());
            }

            std::unique_ptr<IO::Generic::Stream> read() const
            {
                IO::File::Mode mode = IO::File::Mode::readOnly();
                /* This is opened before the target, so we have to have it nonblocking */
                mode.access = IO::File::Mode::Access::NonBlocking;
                mode.create = false;
                return IO::Access::file(name, mode)->stream();
            }

            std::unique_ptr<IO::Generic::Stream> write() const
            {
                IO::File::Mode mode = IO::File::Mode::writeOnly();
                /* Use buffered so we're using the fd stream backend */
                mode.access = IO::File::Mode::Access::Buffered;
                mode.create = false;
                return IO::Access::file(name, mode)->stream();
            }
        };

        TemporaryFifo readPipe;
        QVERIFY(readPipe.create("/tmp/CPD3TestRead-" + Random::string()));

        TemporaryFifo writePipe;
        QVERIFY(writePipe.create("/tmp/CPD3TestWrite-" + Random::string()));

        std::mutex mutex;
        std::condition_variable cv;

        Variant::Root config;
        config["Type"] = "Pipe";
        config["Input"] = readPipe.name;
        config["Output"] = writePipe.name;
        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        QVERIFY(target->isValid());
        QVERIFY(!target->triggerAutoprobeWhenClosed());
        QVERIFY(target->spawnServer());

        {
            auto serialized = target->serialize();
            auto deserialized = IOTarget::deserialize(serialized);
            QVERIFY(deserialized.get() != nullptr);
            QVERIFY(deserialized->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*deserialized));
        }

        {
            auto check = target->configuration();
            QCOMPARE(check["Input"].toString(), readPipe.name);
            QCOMPARE(check["Output"].toString(), writePipe.name);
        }

        {
            auto check = target->configuration(true);
            QCOMPARE(check["Input"].toString(), readPipe.name);
            QCOMPARE(check["Output"].toString(), writePipe.name);
        }

        {
            auto check = target->clone();
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
            check->merge(*target);
        }

        {
            auto check = target->clone(true);
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
            check->merge(*target);
        }


        /*
         * Open the end we read from (the target writes to) first, since non-blocking write open
         * (the target) fails with ENXIO if there's no reader.  The other end succeeds without
         * us being connected, so we do it after the target has opened it.
         */
        auto readStream = writePipe.read();
        QVERIFY(readStream.get() != nullptr);
        auto device = target->backingDevice();
        QVERIFY(device.get() != nullptr);
        auto writeStream = readPipe.write();
        QVERIFY(writeStream.get() != nullptr);

        Util::ByteArray targetData;
        bool targetEnded = false;
        device->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                targetData += data;
            }
            cv.notify_all();
        });
        device->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                targetEnded = true;
            }
            cv.notify_all();
        });

        device->start();

        Util::ByteArray readData;
        bool readEnded = false;
        readStream->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                readData += data;
            }
            cv.notify_all();
        });
        readStream->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                readEnded = true;
            }
            cv.notify_all();
        });

        readStream->start();

        QByteArray send = "SomeDataSent";
        device->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) readData.size() >= send.size(); });
            QCOMPARE(readData.toQByteArrayRef(), send);
            QVERIFY(targetData.empty());
            QVERIFY(!targetEnded);
            QVERIFY(!readEnded);
            readData.clear();
        }

        send = "OtherDataSent";
        writeStream->write(send);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) targetData.size() >= send.size(); });
            QCOMPARE(targetData.toQByteArrayRef(), send);
            QVERIFY(readData.empty());
            QVERIFY(!targetEnded);
            QVERIFY(!readEnded);
            targetData.clear();
        }

        QVERIFY(!device->isEnded());

        readStream.reset();
        writeStream.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return targetEnded; });
            QVERIFY(targetEnded);
        }

        QVERIFY(device->isEnded());
    }

#endif

#ifdef Q_OS_UNIX

    void process()
    {
        std::mutex mutex;
        std::condition_variable cv;

        Variant::Root config;
        config["Type"] = "Command";
        config["ErrorStreamAsEcho"].setBoolean(true);
        config["Command"].setString("/bin/sh");
        config["Arguments/#0"].setString("-c");
        config["Arguments/#1"].setString(R"(
echo -n -e 'stderr data\n' 1>&2;
exec cat -
)");
        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        QVERIFY(target->isValid());
        QVERIFY(!target->triggerAutoprobeWhenClosed());
        QVERIFY(target->spawnServer());

        {
            auto serialized = target->serialize();
            auto deserialized = IOTarget::deserialize(serialized);
            QVERIFY(deserialized.get() != nullptr);
            QVERIFY(deserialized->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*deserialized));
        }

        {
            auto check = target->configuration();
            QCOMPARE(check["Command"].toString(), config["Command"].toString());
            QCOMPARE(check["Arguments"], config["Arguments"]);
        }

        {
            auto check = target->clone();
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
            check->merge(*target);
        }

        auto device = target->backingDevice();
        QVERIFY(device.get() != nullptr);

        Util::ByteArray processData;
        Util::ByteArray controlData;
        bool processEnded = false;
        device->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                processData += data;
            }
            cv.notify_all();
        });
        device->control.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                controlData += data;
            }
            cv.notify_all();
        });
        device->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                processEnded = true;
            }
            cv.notify_all();
        });

        device->start();
        QVERIFY(!device->isEnded());

        QByteArray data = "stderr data\n";
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) controlData.size() >= data.size(); });
            QCOMPARE(controlData.toQByteArrayRef(), data);
            QVERIFY(processData.empty());
            QVERIFY(!processEnded);
            controlData.clear();
        }

        data = "SomeDataSent\n";
        device->write(data);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return (int) processData.size() >= data.size(); });
            QCOMPARE(processData.toQByteArrayRef(), data);
            QVERIFY(controlData.empty());
            QVERIFY(!processEnded);
            processData.clear();
        }

        QVERIFY(!device->isEnded());

        device.reset();
    }

#endif

#if 0

    void url()
    {
        std::mutex mutex;
        std::condition_variable cv;

        Variant::Root config;
        config["Type"] = "URL";
        config["URL"].setString("https://www.esrl.noaa.gov/gmd/");
        config["Method"].setString("GET");
        auto target = IOTarget::create(config);
        QVERIFY(target.get() != nullptr);
        QVERIFY(target->isValid());
        QVERIFY(!target->triggerAutoprobeWhenClosed());
        QVERIFY(target->spawnServer());

        {
            auto serialized = target->serialize();
            auto deserialized = IOTarget::deserialize(serialized);
            QVERIFY(deserialized.get() != nullptr);
            QVERIFY(deserialized->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*deserialized));
        }

        {
            auto check = target->configuration();
            QCOMPARE(check["Type"].toString(), std::string("URL"));
            QCOMPARE(check["URL"].toString(), config["URL"].toString());
            QCOMPARE(check["Method"].toString(), config["Method"].toString());
        }

        {
            auto check = target->clone();
            QVERIFY(check.get() != nullptr);
            QVERIFY(check->matchesDevice(*target));
            QVERIFY(target->matchesDevice(*check));
            check->merge(*target);
        }

        auto device = target->backingDevice();
        QVERIFY(device.get() != nullptr);

        Util::ByteArray responseData;
        bool responseEnded = false;
        device->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                responseData += data;
            }
            cv.notify_all();
        });
        device->ended.connect([&] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                responseEnded = true;
            }
            cv.notify_all();
        });

        device->start();
        QVERIFY(!device->isEnded());

        device->write("\n");
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return responseData.size() > 64; });
            QVERIFY(responseData.toQString().contains("<html"));
            QVERIFY(!responseEnded);
            responseData.clear();
        }

        QVERIFY(!device->isEnded());

        device->write("\n");
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return responseData.size() > 64; });
            QVERIFY(responseData.toQString().contains("<html"));
            QVERIFY(!responseEnded);
            responseData.clear();
        }

        QVERIFY(!device->isEnded());

        device.reset();
    }

#endif

};

QTEST_MAIN(TestIOTarget)

#include "iotarget.moc"
