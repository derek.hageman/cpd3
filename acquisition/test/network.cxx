/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QTest>
#include <QCryptographicHash>
#include <QTemporaryFile>
#include <QTcpServer>

#include "acquisition/acquisitionnetwork.hxx"
#include "datacore/stream.hxx"
#include "core/number.hxx"
#include "core/qtcompat.hxx"
#include "core/waitutils.hxx"

using namespace CPD3;
using namespace CPD3::Acquisition;
using namespace CPD3::Data;

/* Sometimes causes weird failures due to Qt's usage of libcrypto */
//#define TEST_SSL

#ifdef TEST_SSL
static const char *cert1Data = "-----BEGIN CERTIFICATE-----\n"
                               "MIIFazCCA1OgAwIBAgIUcQWCjvR+2UjdwEiq/hfK5nRTy2gwDQYJKoZIhvcNAQEL\n"
                               "BQAwRTELMAkGA1UEBhMCVVMxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM\n"
                               "GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMjA4MjQyMjQ0NDNaFw0zMjA4\n"
                               "MjEyMjQ0NDNaMEUxCzAJBgNVBAYTAlVTMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw\n"
                               "HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggIiMA0GCSqGSIb3DQEB\n"
                               "AQUAA4ICDwAwggIKAoICAQCT0tQuQnLGh7D4IfHWvd2551Hz151BWvLa28zH7eYb\n"
                               "+OrjenNA+UF4Oeg2jOTdveHYqE3Kx1cOYcAaFoybXzTy3OCSsPqXyYIzc1MsSSCx\n"
                               "6fCg/+m2HadtmEjMQdjdfwOphpqj616ZguRCkUu9UeP8Y+VStFp9F1E2mw/G7JTu\n"
                               "ssFrN71g/G+HtMcAyiEcEfzX19Tnz4+hpY8D6O24vntegrn3GtCZXXmIL/h0UZ35\n"
                               "hTXnSNOi7gSRz6mKoKTPNvFxPA2fw3gksFYKYNLeVaM7ylfe+Hb4SajSC6jJp1Ku\n"
                               "EMrcPvE77j2QSQEFgX3it9peJ2GJ+YpOCE2Kgkc7JqyFfmlb/P4q3M74dYooxlvd\n"
                               "tkQZbAGCaI3QX+cND1F3S3YdJi87ksl7J33DnIxJwDCayKLwonb3K92zNYWi3YVK\n"
                               "70mF4kRCEXmZa5nr2mu5NcMG78jMBLmTgkkK+D2d7sW3g92yvQO/zatucTqo81Lv\n"
                               "gsq6pJ46UTHS6FJriKHprARvl3ncFtcUtVlWmCTci3MXsGH9Qm0OlnQDtStHwIpA\n"
                               "BJW/M6fARXenJr8ezVVj+nrVNMj1SGXu+Lx7MyluCbwwTDhQBEKsDC+WJln1our4\n"
                               "eng7IX7m/fEg3/JlpfcujcJYsW8dx/PzDjA212TUt8bZkoZI8y5+Oug3u8Pp763u\n"
                               "WQIDAQABo1MwUTAdBgNVHQ4EFgQURNp3BrFtSLhpTeXbIoq+YebXumYwHwYDVR0j\n"
                               "BBgwFoAURNp3BrFtSLhpTeXbIoq+YebXumYwDwYDVR0TAQH/BAUwAwEB/zANBgkq\n"
                               "hkiG9w0BAQsFAAOCAgEAhuwOAltN+WL8X+sJpI+Y3IDBHucaeBOD1+be37TjSMWo\n"
                               "DMqFcCdZSKiRK0Ww03Wecjxx83A+HU6H/rERmu4NxNiEXFtLYU8l16KFdoqp0egT\n"
                               "FWKifL/vikMYM/nMhpeR+9fylA+/AqfoNZkeAH13S7qliGVYRkz2Ci/yqMMr+Ph9\n"
                               "aorambR4SKh3O75rOrOumPStC9adWQfUNcN6tZGORZranvdZjYRo+197c8Tk9y/r\n"
                               "mZ/YbxJ46V2rIRtbt3Wq8iDUSTZqMRAcegEex7Vz9zv9A6GyqSkeki2CR+P39wgN\n"
                               "zXKHviHNnnhtNEwdDnsJKTP6cA8t7Qorxjx8eZic1gxDNF7FSZ4FYiJZL+9rWZrC\n"
                               "E7BkaOf/IS6B3bXCuIPO+no5/BSCuuTBxwpuVqr5iEjK6/Wmrtzn9g7sxo8LikfU\n"
                               "m/8OhSe4UcffMWALlw1iy5WO78JicpSa+n7wtcNSkIsCout4DjXn9AXY4YyumQAT\n"
                               "QerRcM0WH3TvPv1C87yEWQPAPzCHtDKl+ITfXF5NbezzwevryABIKta16GVabeBi\n"
                               "MbixwC7IixqB306CVhFMkba5vNMlCqrWGPBcnMdwIcgjsn4dV4TnNU7wiHd3qxpS\n"
                               "Z5GdEPBJs+SiWzFHMAz4dsfQrune67PePBbXdQfddEPNg0hOPpGzNYYMZVjE+/0=\n"
                               "-----END CERTIFICATE-----";
static const char *key1Data = "-----BEGIN PRIVATE KEY-----\n"
                              "MIIJQQIBADANBgkqhkiG9w0BAQEFAASCCSswggknAgEAAoICAQCT0tQuQnLGh7D4\n"
                              "IfHWvd2551Hz151BWvLa28zH7eYb+OrjenNA+UF4Oeg2jOTdveHYqE3Kx1cOYcAa\n"
                              "FoybXzTy3OCSsPqXyYIzc1MsSSCx6fCg/+m2HadtmEjMQdjdfwOphpqj616ZguRC\n"
                              "kUu9UeP8Y+VStFp9F1E2mw/G7JTussFrN71g/G+HtMcAyiEcEfzX19Tnz4+hpY8D\n"
                              "6O24vntegrn3GtCZXXmIL/h0UZ35hTXnSNOi7gSRz6mKoKTPNvFxPA2fw3gksFYK\n"
                              "YNLeVaM7ylfe+Hb4SajSC6jJp1KuEMrcPvE77j2QSQEFgX3it9peJ2GJ+YpOCE2K\n"
                              "gkc7JqyFfmlb/P4q3M74dYooxlvdtkQZbAGCaI3QX+cND1F3S3YdJi87ksl7J33D\n"
                              "nIxJwDCayKLwonb3K92zNYWi3YVK70mF4kRCEXmZa5nr2mu5NcMG78jMBLmTgkkK\n"
                              "+D2d7sW3g92yvQO/zatucTqo81Lvgsq6pJ46UTHS6FJriKHprARvl3ncFtcUtVlW\n"
                              "mCTci3MXsGH9Qm0OlnQDtStHwIpABJW/M6fARXenJr8ezVVj+nrVNMj1SGXu+Lx7\n"
                              "MyluCbwwTDhQBEKsDC+WJln1our4eng7IX7m/fEg3/JlpfcujcJYsW8dx/PzDjA2\n"
                              "12TUt8bZkoZI8y5+Oug3u8Pp763uWQIDAQABAoICAAQPAfEetS7G/ziC2xRbrGys\n"
                              "IZuFsB1BIWGZrxTOrvaV5iIWhLdUGKRzx5DG28pMSjkD/vXKNRVRoP/+XkAuVCJ2\n"
                              "ZsqURh8YguUFfbM9s0J7QzZybFDqHcSULayBAtKrB/dVuSV2wwdJORnsShVxvAYc\n"
                              "GonjpofgdeP/TCa4rGqk2Qtn8YDKEYha8etPQWs3QbdN51wxflaweF2hvbQos7Ov\n"
                              "uGokz/UkBEAVXBeafZFCI22dGeVzbkfwGQ4ztK61ShoDLXcVW3U7a7NXfvaoiIWt\n"
                              "262FKiQNRq+0rv7RY6NgledH6aDs/L77SCZF7MZGqL3UzDho8++KShG7W7gPquUp\n"
                              "OOy1eLRg7ppyxBf2JWP0SEbxTN8yhIQwtdwgKKk3Q3N+/fXwBUVhi/Rv9dmkLUP2\n"
                              "r7IVZNAAmgTMNGgA6PNqUPNAKlruJTQ8SFflSNe5qulkB3OmQ6uHvJu7x/z9HvvE\n"
                              "kJ/3oZiw1a6VzMpVe4MZgwGDD2GUZDpPwvxNZi/5bHM9ltWqFH+6mYzAmMNvKASg\n"
                              "Gl36BXAPTELF/xd7Dfd47zN2CvDKBQ51BCdnUCiQ76qE6f9LrCfOh5rNT/ALNKT4\n"
                              "3btBFfoN2WHskzAXD7qAoQYDi5M9xAliNKpJnWu7AXJkpTJOv3CVYNMIvJ0ml6jc\n"
                              "WI9Yst7PIXhUN79aupfxAoIBAQDG0sMnZNp+uAAoqVylWJl7NApHSB1qhWxTZ+pL\n"
                              "tvmbPLI19Qu8RWZyoH2RgOSAqCUhBaMFiyF7FxZsE3DSXw9pqptk81HK6CAjnoVS\n"
                              "dKP3/GDSjtkk4kTsK5bVavlmoKuZUAmh2HJmYMrrUSA9Uyu5NsLl9Y0Rpk8H5kyf\n"
                              "Gm1auYufcw9FuK4tcvTZ1MZEAIKQ7SoV4i1uytEeDYT5h3vpLVMOvH4RM5Urw5S/\n"
                              "3ZTrdgJACbNrL+p98HTyRGxeeMqJEt+3JLeDmFOMll95Cng6+smzSmOxOQku5apx\n"
                              "7ytR3D6s1nTsigLAAxVmXSXPEtbDKaEHrl8zw7yDdHHsPC7RAoIBAQC+VX/vD14K\n"
                              "r4CLQh1HUpqwgrdDDfCTlUyPPLNTwUcew2t64IxIFxRJr5/dOkuL6Rz5GH1ivzY3\n"
                              "XJ3bkxDwLXW59dWcy+anivbI0MhXqmWtOe8XE+a3COZr1zYV84W7dJMMsi8pI2Vi\n"
                              "0/7hv52xWAPOZnSLDBrP2oSkkpibPujWU8bi8R70joUa7v4UPh7u+q23+PvhKyMQ\n"
                              "deqVAFl1Mb9WVkenBcfWoze5sx3cF0bkKXutxN1uh9gr2Uwjch/4Yh7kSMs4lEmZ\n"
                              "51aGU/gC0EprIM3O7+o88NZxgC+GF6YgrpOlpx/pKUPBe5aO1ShBT+BO8AER1GFl\n"
                              "v72v/Ez/V/kJAoIBAEwUnmTWrN1Mn5Lvq+oBi0mf0kcQi2EViSwpWXh7newPP8px\n"
                              "6Hm0vM0kKKii/81TilGmjIk0gi1N2mCk18lIYud2R1xL14KjbJj0seOpioz8YDhy\n"
                              "PRlmFCWjUGZ+Ns1UshVKkUUDRFN0unFta77LsrF/CPliCwcz8o39TFidjjbnRUxQ\n"
                              "hQmS7+OoV5V7XBrtbwjyF+aj0+rPZVHwrm+lrn69v0imTD9c07oZbzQ0ICYx1A6Z\n"
                              "J7TecwaaGsYR9L35ztbBCCZWwHp0sZPcftAcd4FqMgCPeLJ6Ns9hRuWuNY9vjfQp\n"
                              "ZDiXXxIGnAu9nRguB0xLA7miuf9e6SYMSwOwy+ECggEAWZVod55mBuV/vQvLOAyb\n"
                              "HkUH+JmRCAWXWTuas+sejE4yQk15+VxTgjMVLU0IzbtUlbF/IoEZBYmkCvr3V/Qt\n"
                              "mu8oMXqO/4Cakv4hrZFX9eZ0sAn/51pbCZrrq/1IjmhZ5fnf1J8CUzewmZRUpmnk\n"
                              "sLrsU53I6NfS4prVFQzRDj+0NpCCn1yNLZYbJG/wo059gT/BXcOt50t4s9TMRiq6\n"
                              "AeruIqDH5DBCRDcX8MVL6ovT2H/2MNXjWxAVlAFdJs0X+R6B+Aljcvq9cNAIxVpA\n"
                              "DJgOBj5Jo5E/fYB13ck3ud4xRCCbFmUDrQd8X5HYNpVf4Ad6mWe6x+ctYq1/mBdR\n"
                              "8QKCAQA6krd3nFC3qDt1SMV1ZkPO+KX7U0HyOlXR4SOm6lrwxvmRO9A8WP04slSQ\n"
                              "N+nLSAAVBAmVzW5WNEGeKI9DiygMZkK88/zNblP+ehNypWhcZ0EvFuUS5yaT/KhJ\n"
                              "7DQEp6jTIPHqm7N/4SF7lCV6gR9WlFJoKEvjT4o6bYVNLIpy2tAB6gphwRpEdnBj\n"
                              "VLLEQ/H9kUgPtNmCnMy/zPCucDgSiqyZOBcwBTvqj90np4cnU4NFDAOzqst6sLok\n"
                              "R+md2F0bM8BjYYczybdYvVq2/GQIi3I4ikTuDk6DANppQY89J/m0yaT+Fpmm8L9a\n"
                              "aPLk7jPofbX2GWYZdOSJ2GoNuTbj\n"
                              "-----END PRIVATE KEY-----";
static const char *cert2Data = "-----BEGIN CERTIFICATE-----\n"
                               "MIIFazCCA1OgAwIBAgIUfBlD9z2l9v0/Ko1Y1Et9UMUXfgowDQYJKoZIhvcNAQEL\n"
                               "BQAwRTELMAkGA1UEBhMCVVMxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM\n"
                               "GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMjA4MjQyMjQ1NDFaFw0zMjA4\n"
                               "MjEyMjQ1NDFaMEUxCzAJBgNVBAYTAlVTMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw\n"
                               "HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggIiMA0GCSqGSIb3DQEB\n"
                               "AQUAA4ICDwAwggIKAoICAQCiokRHZYO0kpbdPqJeK0LcDrG7pfQemeeShGAZj4Gm\n"
                               "5Qqfi0VavMY5Qz3rkXNhfi2d2TJ7vAn+luZ+XK1EZ9HYnauFrJIcgRBQkFLaulHL\n"
                               "R0E9TRVqknGYYUtkppX1IJ2J5ug07QXNz0LEnsGY6CL3Goefv+TMFl2O1+PJxJ8R\n"
                               "ooR13gmHVtzRASgK4qjUkvQ2wmk3EuJmFzYfpyM5kPkrO4zJuqf0zsZKA12CBcfg\n"
                               "EKTMmpO7Aekt6j9Sh2EqTPhuS37tJXDvlHot1bw2AaiICugMx8iXtuI/46Imwh5r\n"
                               "9sXeGU1HWiSkQm0Ci4xMEyGQtwEWk3Ca4hPS3SXiDv8tEYdnUYOCINAOqWsFgXUq\n"
                               "l5lgYBHwb560ZxmqFquFDFUr0uorfDMIJVymvzMuZTptxSmOqXdnkPfaj2cmoEOl\n"
                               "8P2OPUSy0GL2KFdXbenRM5cL3i218FG+sbRKAvE8SOdPlbLiFO00C/29xgXbS+lA\n"
                               "LJpNrqWI+wbE2BtW8pIbyFtix28RItUBJH+m1RyN6HlZ4eBAGJNYsC82Ht11XigP\n"
                               "6Bi3XIIYQoy5vic7ak/Elni2Kv+/RMF08T2EUDYgoEhnr5sfDB9EQh7EU+sK+rRr\n"
                               "32GujuAPSRzGsPOfB/6CPRbYvhPHF8MN0lcuFOQ6oJ0YpA6a1IJ7zjuyIOdliBoe\n"
                               "RwIDAQABo1MwUTAdBgNVHQ4EFgQUgKr7P2bigfa5EN8jnFFwVm9MP+IwHwYDVR0j\n"
                               "BBgwFoAUgKr7P2bigfa5EN8jnFFwVm9MP+IwDwYDVR0TAQH/BAUwAwEB/zANBgkq\n"
                               "hkiG9w0BAQsFAAOCAgEAYwEfXElpKlmKSJhoHV2BiA41yJ8Qqe+W49pvUCJLouko\n"
                               "ThPxZ9hLfKtGASQnriaWWR+wee3morVTL5gxg25YZcIKAb9ivzPaEBq6NVOfnXmu\n"
                               "3fZeastWpUAf3IfFj+S0u7r39aBMLBN0AR9UA+JTRQ/rSiZfZbaKFwBrEv56kbAn\n"
                               "8PstFifkIR3RcTOsLY7lgxvAv2EwZIllSRX3ulC+ur51I4S2DDjqoOVtsdJuDuuk\n"
                               "ytLyBFAwFR+EPaSNpcHvSCoHcNG+3jRpssgK662hnatJgn41y7dOWaHVodPeOiQ0\n"
                               "Fvfv1WXlUdr5JAS7lmiuyTA7ft6lMXasS1dmkcvT89yu4awlW9u8QVOnCmHug5ml\n"
                               "UklCeHaWnJEydnxvat0gRNB07nvJj1KGoa+ufVQgu3NhO/WTnUR4T8mGwT7Ljx1y\n"
                               "2nElBItfIaarexDG7JvKAN043Ex8bMvAVIbXHEAJOZvp+SEVJbmtuTx+SWoM1iCi\n"
                               "LbZhBoSYgwg/C67CyikrdaHcXIfH4wLU+aOTlvKYDZ1PpRMHAI9QYz8n4uy/x8+0\n"
                               "lG8s2vrKogXMTE5Dg/hCtpNeqwhgsseh0WF6QfuWOEVJ10qFZgCPptybh7yBoaw9\n"
                               "PiOmBGUjXsxCkknO4Q7zJqjkplDkfZfb1I9Mt0P0N1U0zJyQeML+hb4fAVhDfxI=\n"
                               "-----END CERTIFICATE-----";
static const char *key2Data = "-----BEGIN PRIVATE KEY-----\n"
                              "MIIJQgIBADANBgkqhkiG9w0BAQEFAASCCSwwggkoAgEAAoICAQCiokRHZYO0kpbd\n"
                              "PqJeK0LcDrG7pfQemeeShGAZj4Gm5Qqfi0VavMY5Qz3rkXNhfi2d2TJ7vAn+luZ+\n"
                              "XK1EZ9HYnauFrJIcgRBQkFLaulHLR0E9TRVqknGYYUtkppX1IJ2J5ug07QXNz0LE\n"
                              "nsGY6CL3Goefv+TMFl2O1+PJxJ8RooR13gmHVtzRASgK4qjUkvQ2wmk3EuJmFzYf\n"
                              "pyM5kPkrO4zJuqf0zsZKA12CBcfgEKTMmpO7Aekt6j9Sh2EqTPhuS37tJXDvlHot\n"
                              "1bw2AaiICugMx8iXtuI/46Imwh5r9sXeGU1HWiSkQm0Ci4xMEyGQtwEWk3Ca4hPS\n"
                              "3SXiDv8tEYdnUYOCINAOqWsFgXUql5lgYBHwb560ZxmqFquFDFUr0uorfDMIJVym\n"
                              "vzMuZTptxSmOqXdnkPfaj2cmoEOl8P2OPUSy0GL2KFdXbenRM5cL3i218FG+sbRK\n"
                              "AvE8SOdPlbLiFO00C/29xgXbS+lALJpNrqWI+wbE2BtW8pIbyFtix28RItUBJH+m\n"
                              "1RyN6HlZ4eBAGJNYsC82Ht11XigP6Bi3XIIYQoy5vic7ak/Elni2Kv+/RMF08T2E\n"
                              "UDYgoEhnr5sfDB9EQh7EU+sK+rRr32GujuAPSRzGsPOfB/6CPRbYvhPHF8MN0lcu\n"
                              "FOQ6oJ0YpA6a1IJ7zjuyIOdliBoeRwIDAQABAoICADPXSrGBg+6a7Zkfvo0K+DiC\n"
                              "PJhmqX7Zq03ygVmUe40SJIU/1T87vmoBa6r8Bc31dSAEXInBomPzgQyViSutdmA4\n"
                              "vjSRkk+gum0b3DVZv/nuwDaErEd439nlZa1zRojJOT58itdYGIoGv69CNc8CbCbd\n"
                              "X48GEa4WkQMYAUXPNa9e4R9bRClOgHvlBPkXUB7Wqx8LcJN8IwvM6VVEpz9R3YMw\n"
                              "68tgAurPwLhWA0gPhKuBUq7ftSiezs/yg/XQLqJXv7cUvKRmU24jI6EZqPGELM8U\n"
                              "vuxYALKPuuYHryfwp9bdZixKX25Xpydu/yWZCwMy8/eXxjKGlBXi69PC57D7+83F\n"
                              "MzAC49icsDogN4zwbABzVhVcvtgqDyyndB3fWdRn4gXU6/3+zDnSCAaEDoxGjPIn\n"
                              "2TitYZSZWwWMsa8yy/HvMrRVEWX1PCrNv2KTwtcL12s9yauSg4StH6my6Di5x8tT\n"
                              "hGl0GfE5nGUSRF9Pld9FZydHIRJ9TsPQ6JzMlZr2lvL46rkk9Iza2plWXAp5Pw8a\n"
                              "gD+ail1GZ45D68Nl3f8tl6CjMAqw+9Oba81gSBO/fmoAz0ba2c7Xa7HgN6863yC1\n"
                              "cE2YKAtDK7fE33YEAhyWQ3TGOpXbmMmaYfoRH4T7D/8qYiB6LDxiz20v9wVuT3mb\n"
                              "4rGzSK09KcxqeAfZd5zRAoIBAQDWWWCVii6aWuGmBvssZwFOO6J+HmMXtpRJtmsA\n"
                              "u0V5h8mz1lVNd1O0OXAqpUVTDO0auVVPFc/+JLlFye5x5hXlNsnPU6BJeBkDp3Vt\n"
                              "RRKHNR0YTB2xxLAS0IKyUKYoTUf1yJ5B5FlMb+wNgrkdc0FQrzYf+k9YGsDejz0B\n"
                              "KQZ3L28p3o19RMamaRKEWDQ0cNWYDnbMKBjbzRvDFcFE2yiFoa+8sCu3ZCIZBJ0i\n"
                              "ARWy0KjEdgpeoCAEJvHy7XN+ufdA6tRhL5u13CaZaD9ky0Q30y0m0Dylwf08aBCL\n"
                              "DuS/UNsMKVOXx4VaiWglX5itMg53/lEqJ+FWyGyPb0TuJm13AoIBAQDCPFru4V5z\n"
                              "F3Z5B9YnSTRUa1EwPGSqijrVecc53ze9hAnJUtM86jolcbVAs/fsDsv3HZFmQ8op\n"
                              "DkVTNNmrY5OlpAHk4Tu37WGzLbgQ9hRXatJ+VDresKdbgUL/W3U33DhxORdwWGMz\n"
                              "NyXemwwGrlSmB9g92mQJu6bxjuxlr/GQA2Bq5ww5xYNI4VmizJylN6L8HcY/8DFz\n"
                              "ssJZDyxZcnzJ8wwfuQyR8O6P8tKVRYKpkZijW+wadFJ3YNqnbaFHySFqVQIoucIu\n"
                              "cOOueWwJ4nTrfP5X/FyewFe1RAEBAacBfWSupX3Lyqfhql7N2+EAbWMu+GcmVTrE\n"
                              "nZjudMJ2msmxAoIBAQCwCWEwDvctH1xYXT1k/wdsd7+AsnYRYIoya5U/WY26PJGj\n"
                              "l3AhGdHtnoqHC4p+pYwIuZLdS32xK/nY3fltI1hyEef11Kk8DV0Fyj6/Sc3oqu9F\n"
                              "KCXu/CjamtHrnh6H1Mg3i9alKONJylVj1ysui5xlpi/eXVVSd05UufCRBl9f/N3f\n"
                              "1f3/lj9LJdnmRQZC1zQkGCTqkDeyNKYLNs+uPArwjJRmc4zDpYH3Z/mglqF6ZLFl\n"
                              "/8eOZRVGM7HbH7YNqq7saq+XTWElHzO4I4yEvAc/jbh0OurD+yh9tr13ZvjoQoBT\n"
                              "YZFcgqj7bQPSRct+2jBVzdBcvbnr6th1iB1lNLwLAoIBAFuxVlQSvB2k49663lPp\n"
                              "cPSxPMCCohsH6kJAnNYrFAs5O814dhP5lr16clA7JTygt5TOtocKVXMQM5XWCzOn\n"
                              "bFnzlJlR4nkvbMHDQXNwV6X03a2ZYbkit76wxMn7iNh384UvqGr2rs3MqOnvU7wL\n"
                              "rBY8+c4pPLSDi4nZKKvQZT7Q+UE+FT1oilNrXn9GDGTKYPyXqbEhWJb/ulOEoDER\n"
                              "pOtI+142Y0K59ESsItEo4UffXakwicLrSsrkRBRp4osa3dVuj/hdyNdXn4QL+f6o\n"
                              "kv1gIkmy7p5auoztr3OkLgy7/z2bDkmYP6x+WwbfV9Z1zS6tQTSNY5LRuxJGuE55\n"
                              "P+ECggEAJVnMoTv7bxlqAXGKAJbufzmNr8ahI9/AEkBpMJc2ZxN0GwHl5nv/DZWN\n"
                              "6s6n2fwy6pygWlwOWGn+LL0gsJWUc5UNsQjukZyg7n/eAfl2evc2yiYR4HJub5aZ\n"
                              "JIFcnuh+Bzwy7zJrtAR/XrgyTIN0l+5UcfFhknMBZSFOv3KETFdiLfX4Zz0/I/83\n"
                              "rIIF0IHlisbDrp8gBNlCJGI2/Z+TJjxJc1KlSX75xaRvttx6uFvDg9moTfqJF8hS\n"
                              "xQWv6cansKSsUAtOYGOG4G+T94Q8XXlsbCZ2Gv8uTiEE9/NAQ2DkVGSKvJNK2q5t\n"
                              "q/WkfvYkbGOrhauIOsr+yxmWYkf86g==\n"
                              "-----END PRIVATE KEY-----";
#endif

class TestEgress : public StreamSink::Buffer {
    std::mutex mutex;
public:
    TestEgress() = default;

    virtual ~TestEgress() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(!ended());
        StreamSink::Buffer::incomingData(values);
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(!ended());
        StreamSink::Buffer::incomingData(std::move(values));
    }

    void incomingData(const SequenceValue &value) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(!ended());
        StreamSink::Buffer::incomingData(value);
    }

    void incomingData(SequenceValue &&value) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(!ended());
        StreamSink::Buffer::incomingData(std::move(value));
    }

    virtual void endData()
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(!ended());
        StreamSink::Buffer::endData();
    }

    bool isEnded()
    {
        std::lock_guard<std::mutex> lock(mutex);
        return ended();
    }

    bool contains(const SequenceName &unit, const Variant::Read &value)
    {
        std::lock_guard<std::mutex> lock(mutex);
        return StreamSink::Buffer::contains(unit,
                                            [&](const SequenceValue &a, const SequenceName &b) {
                                                if (a.getName() != b)
                                                    return false;
                                                return a.getValue() == value;
                                            });
    }
};

class TestAcquisitionNetwork : public QObject {
Q_OBJECT

    QString uid;
    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        uid = Random::string();

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void general()
    {
        QFETCH(Data::Variant::Root, serverConfig);
        QFETCH(Data::Variant::Root, clientConfig);

        {
            Archive::Access access(databaseFile);

        }
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "T_S11"}, Variant::Root(0.9), 1352216144,
                              1352216145)});

        std::mutex mutex;
        std::condition_variable cv;

        Variant::Root serverMessageLogEvent;
        std::string serverCommandReceivedName;
        Variant::Root serverCommandReceivedData;
        double serverFlushRequest = FP::undefined();
        Time::LogicalTimeUnit serverSetAveragingTimeUnit;
        int serverSetAveragingTimeCount = -1;
        bool serverSetAveragingTimeAlign = false;
        Variant::Flag serverBypassFlagSet;
        Variant::Flag serverBypassFlagClear;
        int serverBypassFlatClearAll = 0;
        Variant::Flag serverSystemFlagSet;
        Variant::Flag serverSystemFlagClear;
        int serverSystemFlatClearAll = 0;
        int serverRestartRequested = 0;

        bool clientConnected = false;
        std::string clientInterfaceInformationUpdated;
        std::string clientInterfaceStateUpdated;
        Variant::Root clientRealtimeEvent;

        AcquisitionNetworkServer server(serverConfig);
        AcquisitionNetworkClient client(clientConfig);

        server.incomingRealtime(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "U_S11"), Variant::Root(0.1),
                              Time::time(), FP::undefined())});
        server.start();
        server.messageLogEvent.connect([&](const Variant::Root &event) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverMessageLogEvent = event;
            }
            cv.notify_all();
        });
        server.commandReceived.connect([&](const std::string &name, const Variant::Root &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverCommandReceivedName = name;
                serverCommandReceivedData = data;
            }
            cv.notify_all();
        });
        server.flushRequest.connect([&](double time) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverFlushRequest = time;
            }
            cv.notify_all();
        });
        server.setAveragingTime.connect([&](Time::LogicalTimeUnit unit, int count, bool align) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverSetAveragingTimeUnit = unit;
                serverSetAveragingTimeCount = count;
                serverSetAveragingTimeAlign = align;
            }
            cv.notify_all();
        });
        server.bypassFlagSet.connect([&](const Variant::Flag &flag) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverBypassFlagSet = flag;
            }
            cv.notify_all();
        });
        server.bypassFlagClear.connect([&](const Variant::Flag &flag) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverBypassFlagClear = flag;
            }
            cv.notify_all();
        });
        server.bypassFlagClearAll.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverBypassFlatClearAll++;
            }
            cv.notify_all();
        });
        server.systemFlagSet.connect([&](const Variant::Flag &flag) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverSystemFlagSet = flag;
            }
            cv.notify_all();
        });
        server.systemFlagClear.connect([&](const Variant::Flag &flag) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverSystemFlagClear = flag;
            }
            cv.notify_all();
        });
        server.systemFlagClearAll.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverSystemFlatClearAll++;
            }
            cv.notify_all();
        });
        server.restartRequested.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serverRestartRequested++;
            }
            cv.notify_all();
        });

        server.incomingRealtime(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "U_S11"), Variant::Root(0.1),
                              Time::time(), FP::undefined())});

        client.connectionState.connect([&](bool connected) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientConnected = connected;
            }
            cv.notify_all();
        });
        client.interfaceInformationUpdated.connect([&](const std::string &name) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientInterfaceInformationUpdated = name;
            }
            cv.notify_all();
        });
        client.interfaceStateUpdated.connect([&](const std::string &name) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientInterfaceStateUpdated = name;
            }
            cv.notify_all();
        });
        client.realtimeEvent.connect([&](const Variant::Root &event) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                clientRealtimeEvent = event;
            }
            cv.notify_all();
        });

        TestEgress realtimeEgress;
        client.setRealtimeEgress(&realtimeEgress);

        client.start();

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(30), [&] { return clientConnected; });
            QVERIFY(clientConnected);
        }

        Variant::Root clientMessageLogData;
        clientMessageLogData.write().hash("Text").setString("Test message");
        client.messageLogEvent(clientMessageLogData);
        Variant::Root clientCommandData;
        clientCommandData.write().hash("Command").setString("Test command");
        client.issueCommand("CommandTarget", clientCommandData);
        client.flushRequest(1.25);
        client.setAveragingTime(Time::Minute, 2, false);
        client.bypassFlagSet("SetBFlag");
        client.bypassFlagClear("ClearBFlag");
        client.bypassFlagClearAll();
        client.systemFlagSet("SetSFlag");
        client.systemFlagClear("ClearSFlag");
        client.systemFlagClearAll();

        server.archiveFlush();
        Variant::Write serverInterfaceInformationData = Variant::Write::empty();
        serverInterfaceInformationData.hash("Source").setString("Test interface");
        server.updateInterfaceInformation("InterfaceTarget", serverInterfaceInformationData);
        Variant::Write serverInterfaceStateData = Variant::Write::empty();
        serverInterfaceStateData.hash("Status").setString("Normal");
        server.updateInterfaceState("InterfaceTarget2", serverInterfaceStateData);
        Variant::Root serverEventData;
        serverEventData.write().hash("Text").setString("Test event");
        server.incomingEvent(Variant::Root(serverEventData));

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&] { return clientRealtimeEvent.read().exists(); });
            QVERIFY(clientRealtimeEvent.read().exists());
        }
        client.restartRequested();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&] { return serverRestartRequested > 0; });
            QCOMPARE(serverRestartRequested, 1);
        }

        server.incomingRealtime(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.0), Time::time(),
                              FP::undefined()),
                SequenceValue(SequenceName("bnd", "rt_instant", "T_S11"), Variant::Root(-1.0),
                              Time::time(), FP::undefined()),
                SequenceValue(SequenceName("bnd", "rt_boxcar", "T_S11"), Variant::Root(-2.0),
                              Time::time(), FP::undefined())});

        TestEgress archiveEgress;
        client.remoteDataRead(&archiveEgress, Archive::Selection());

        ElapsedTimer to;
        to.start();
        while (to.elapsed() < 10000 && !archiveEgress.isEnded()) {
            QTest::qSleep(50);
        }
        QVERIFY(to.elapsed() < 10000);
        QVERIFY(archiveEgress.isEnded());

        to.start();
        while (to.elapsed() < 10000 &&
                (!realtimeEgress.contains(SequenceName("bnd", "raw", "T_S11"),
                                          Variant::Root(1.0)) ||
                        !realtimeEgress.contains(SequenceName("bnd", "rt_instant", "T_S11"),
                                                 Variant::Root(-1.0)) ||
                        !realtimeEgress.contains(SequenceName("bnd", "rt_boxcar", "T_S11"),
                                                 Variant::Root(-2.0)) ||
                        !realtimeEgress.contains(SequenceName("bnd", "rt_instant", "U_S11"),
                                                 Variant::Root(0.1)))) {
            QTest::qSleep(50);
        }

        auto interfaceInformationCheck = client.getInterfaceInformation("InterfaceTarget");
        auto interfaceStateCheck = client.getInterfaceState("InterfaceTarget2");

        QVERIFY(realtimeEgress.contains(SequenceName("bnd", "rt_instant", "U_S11"),
                                        Variant::Root(0.1)));
        QVERIFY(realtimeEgress.contains(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.0)));
        QVERIFY(realtimeEgress.contains(SequenceName("bnd", "rt_instant", "T_S11"),
                                        Variant::Root(-1.0)));
        QVERIFY(realtimeEgress.contains(SequenceName("bnd", "rt_boxcar", "T_S11"),
                                        Variant::Root(-2.0)));

        std::lock_guard<std::mutex> lock(mutex);

        QVERIFY(archiveEgress.ended());
        QVERIFY(archiveEgress.contains(SequenceName("bnd", "raw", "T_S11"), Variant::Root(0.9)));
        QVERIFY(archiveEgress.contains(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.0)));

        QCOMPARE(serverMessageLogEvent.read(), clientMessageLogData.read());

        QCOMPARE(serverCommandReceivedName, std::string("CommandTarget"));
        QCOMPARE(serverCommandReceivedData.read(), clientCommandData.read());

        QCOMPARE(serverFlushRequest, 1.25);

        QCOMPARE(serverSetAveragingTimeUnit, Time::Minute);
        QCOMPARE(serverSetAveragingTimeCount, 2);
        QCOMPARE(serverSetAveragingTimeAlign, false);

        QCOMPARE(serverBypassFlagSet, Variant::Flag("SetBFlag"));

        QCOMPARE(serverBypassFlagClear, Variant::Flag("ClearBFlag"));

        QCOMPARE(serverBypassFlatClearAll, 1);

        QCOMPARE(serverSystemFlagSet, Variant::Flag("SetSFlag"));

        QCOMPARE(serverSystemFlagClear, Variant::Flag("ClearSFlag"));

        QCOMPARE(serverSystemFlatClearAll, 1);

        QCOMPARE(serverRestartRequested, 1);


        QCOMPARE(clientInterfaceInformationUpdated, std::string("InterfaceTarget"));
        QCOMPARE(interfaceInformationCheck.write(), serverInterfaceInformationData);

        QCOMPARE(clientInterfaceStateUpdated, std::string("InterfaceTarget2"));
        QCOMPARE(interfaceStateCheck.write(), serverInterfaceStateData);

        QCOMPARE(clientRealtimeEvent.read(), serverEventData.read());
    }

    void general_data()
    {
        QTest::addColumn<Data::Variant::Root>("serverConfig");
        QTest::addColumn<Data::Variant::Root>("clientConfig");

        Data::Variant::Root serverConfig;
        Data::Variant::Root clientConfig;

        serverConfig.write().hash("DisableLocal").setBool(false);
        serverConfig.write().hash("DisableLoopback").setBool(true);
        serverConfig.write().hash("LocalSocket").setString(QString("CPD3NetworkTest-%1").arg(uid));
        clientConfig.write().hash("Type").setString("LocalSocket");
        clientConfig.write().hash("Name").setString(QString("CPD3NetworkTest-%1").arg(uid));
        QTest::newRow("Local") << serverConfig << clientConfig;

        {
            QLocalSocket testRunning;
            testRunning.connectToServer("CPD3Acquisition");
            if (!testRunning.waitForConnected(1000)) {
                serverConfig.write().setEmpty();
                serverConfig.write().hash("DisableLoopback").setBool(true);
                clientConfig.write().setEmpty();
                QTest::newRow("Default Local") << serverConfig << clientConfig;
            } else {
                testRunning.disconnectFromServer();
                testRunning.close();
            }
        }

#ifndef QT_NO_IPV4
        {
            QTcpSocket testRunning;
            testRunning.connectToHost("127.0.0.1", 14234);
            if (!testRunning.waitForConnected(1000)) {
                serverConfig.write().setEmpty();
                serverConfig.write().hash("DisableLoopback").setBool(false);
                serverConfig.write().hash("DisableLocal").setBool(true);
                clientConfig.write().setEmpty();
                clientConfig.write().hash("Type").setString("TCP");
                clientConfig.write().hash("Server").setString("127.0.0.1");
                QTest::newRow("Default localhost IPv4") << serverConfig << clientConfig;
            } else {
                testRunning.disconnectFromHost();
                testRunning.close();
            }
        }
#endif

#ifndef QT_NO_IPV6
        {
            QTcpSocket testRunning;
            testRunning.connectToHost("::1", 14234);
            if (!testRunning.waitForConnected(1000)) {
                serverConfig.write().setEmpty();
                serverConfig.write().hash("DisableLoopback").setBool(false);
                serverConfig.write().hash("DisableLocal").setBool(true);
                clientConfig.write().setEmpty();
                clientConfig.write().hash("Type").setString("TCP");
                clientConfig.write().hash("Server").setString("::1");
                QTest::newRow("Default localhost IPv6") << serverConfig << clientConfig;
            } else {
                testRunning.disconnectFromHost();
                testRunning.close();
            }
        }
#endif

        /* Find a free port */
        quint16 port = 1;
        {
            QTcpServer server;
            server.listen();
            port = server.serverPort();
            server.close();
        }

        serverConfig.write().setEmpty();
        clientConfig.write().setEmpty();
        serverConfig.write().hash("DisableLocal").setBool(true);
        serverConfig.write().hash("DisableLoopback").setBool(true);
        serverConfig.write().hash("Listen").hash("Port").setInt64(port);
#ifndef QT_NO_IPV4
        clientConfig.write().hash("Type").setString("RemoteServer");
        clientConfig.write().hash("Server").setString("127.0.0.1");
        clientConfig.write().hash("ServerPort").setInt64(port);
        QTest::newRow("Manual IPv4") << serverConfig << clientConfig;
#endif

#ifndef QT_NO_IPV6
        clientConfig.write().setEmpty();
        clientConfig.write().hash("Type").setString("RemoteServer");
        clientConfig.write().hash("Server").setString("::1");
        clientConfig.write().hash("ServerPort").setInt64(port);
        QTest::newRow("Manual IPv6") << serverConfig << clientConfig;
#endif

        clientConfig.write().setEmpty();
        clientConfig.write().hash("Type").setString("RemoteServer");
        clientConfig.write().hash("Server").setString("localhost");
        clientConfig.write().hash("ServerPort").setInt64(port);
        QTest::newRow("Manual DNS") << serverConfig << clientConfig;


#ifndef QT_NO_IPV4
        serverConfig.write().setEmpty();
        clientConfig.write().setEmpty();
        serverConfig.write().hash("DisableLocal").setBool(true);
        serverConfig.write().hash("DisableLoopback").setBool(true);
        serverConfig.write().hash("Listen").array(0).hash("Port").setInt64(port);
        serverConfig.write().hash("Listen").array(0).hash("ListenAddress").setString("127.0.0.1");
        clientConfig.write().hash("Type").setString("RemoteServer");
        clientConfig.write().hash("Server").setString("127.0.0.1");
        clientConfig.write().hash("ServerPort").setInt64(port);
        QTest::newRow("IPv4 only") << serverConfig << clientConfig;
#endif

#ifndef QT_NO_IPV6
        serverConfig.write().setEmpty();
        clientConfig.write().setEmpty();
        serverConfig.write().hash("DisableLocal").setBool(true);
        serverConfig.write().hash("DisableLoopback").setBool(true);
        serverConfig.write().hash("Listen").array(0).hash("Port").setInt64(port);
        serverConfig.write().hash("Listen").array(0).hash("ListenAddress").setString("::1");
        clientConfig.write().hash("Type").setString("RemoteServer");
        clientConfig.write().hash("Server").setString("::1");
        clientConfig.write().hash("ServerPort").setInt64(port);
        QTest::newRow("IPv6 only") << serverConfig << clientConfig;
#endif


#ifdef TEST_SSL
        serverConfig.write().setEmpty();
        clientConfig.write().setEmpty();
        serverConfig.write().hash("DisableLocal").setBool(true);
        serverConfig.write().hash("DisableLoopback").setBool(true);
        serverConfig.write().hash("Listen").array(0).hash("SSL").
            hash("Certificate").setString(cert1Data);
        serverConfig.write().hash("Listen").array(0).hash("SSL").
            hash("Key").setString(key1Data);
        serverConfig.write().hash("Listen").array(0).hash("SSL").
            hash("Authorized").setString(cert2Data);
        clientConfig.write().hash("Type").setString("RemoteServer");
        clientConfig.write().hash("SSL").hash("Certificate").setString(cert2Data);
        clientConfig.write().hash("SSL").hash("Key").setString(key2Data);
        
#ifndef QT_NO_IPV4
        serverConfig.write().hash("Listen").array(0).hash("Port").setInt64(port);
        serverConfig.write().hash("Listen").array(0).hash("ListenAddress").
                setString("127.0.0.1");
        clientConfig.write().hash("Server").setString("127.0.0.1");
        clientConfig.write().hash("ServerPort").setInt64(port);
        QTest::newRow("IPv4 SSL") << serverConfig << clientConfig;
#endif

#ifndef QT_NO_IPV6
        serverConfig.write().hash("Listen").array(0).hash("Port").setInt64(port);
        serverConfig.write().hash("Listen").array(0).hash("ListenAddress").
                setString("::1");
        clientConfig.write().hash("Server").setString("::1");
        clientConfig.write().hash("ServerPort").setInt64(port);
        QTest::newRow("IPv6 SSL") << serverConfig << clientConfig;
#endif

#endif
    }
};

QTEST_MAIN(TestAcquisitionNetwork)

#include "network.moc"
