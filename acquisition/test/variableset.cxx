/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QtGlobal>
#include <QTest>

#include "acquisition/acquisitionvariableset.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestVariableSet : public QObject {
Q_OBJECT

    static bool valuesEqual(const Variant::Read &a, const Variant::Read &b)
    {
        if (a.getType() != Variant::Type::Real)
            return a == b;
        if (b.getType() != Variant::Type::Real)
            return a == b;
        double va = a.toDouble();
        double vb = b.toDouble();
        if (!FP::defined(va))
            return !FP::defined(vb);
        if (!FP::defined(vb))
            return false;
        double eps = qMax(va, vb) * 1E-6;
        if (eps == 0)
            return va == vb;
        return fabs(va - vb) < eps;
    }

    static bool checkContains(SequenceValue::Transfer &list, const SequenceValue &value)
    {
        for (auto check = list.begin(), endCheck = list.end(); check != endCheck; ++check) {
            if (check->getUnit() != value.getUnit())
                continue;
            if (!FP::equal(check->getStart(), value.getStart()))
                continue;
            if (!FP::equal(check->getEnd(), value.getEnd()))
                continue;

            auto merged = Variant::Root::overlay(check->root(), value.root());
            if (!valuesEqual(merged, check->read())) {
                qDebug() << "Value overlay mismatch for" << *check << "merged:" << merged
                         << "input:" << value;
                return false;
            }

            list.erase(check);
            return true;
        }

        return false;
    }

private slots:

    void empty()
    {
        SinkMultiplexer mux;
        mux.start();
        StreamSink::Buffer outputLogging;
        mux.setEgress(&outputLogging);
        AcquisitionVariableSet vs(&mux, ValueSegment::Transfer());
        StreamSink::Buffer outputRealtime;
        vs.setRealtimeEgress(&outputRealtime);

        vs.process(100.0, SequenceValue::Transfer(), SequenceValue::Transfer());

        vs.finish();
        mux.sinkCreationComplete();
        QVERIFY(mux.wait(30));

        QVERIFY(outputLogging.values().empty());
        QVERIFY(outputRealtime.values().empty());
    }

    void bypass()
    {
        SinkMultiplexer mux;
        mux.start();
        StreamSink::Buffer outputLogging;
        mux.setEgress(&outputLogging);
        AcquisitionVariableSet vs(&mux, ValueSegment::Transfer());
        StreamSink::Buffer outputRealtime;
        vs.setRealtimeEgress(&outputRealtime);

        SequenceValue::Transfer logging;
        SequenceValue::Transfer realtime;

        logging.emplace_back(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.0), 100.0, 200.0);
        logging.emplace_back(SequenceName("bnd", "raw", "T_S11"), Variant::Root(2.0), 200.0, 300.0);
        logging.emplace_back(SequenceName("bnd", "raw", "T_S11"), Variant::Root(3.0), 300.0, 400.0);

        realtime.emplace_back(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.5), 300.0,
                              FP::undefined());

        vs.process(300.0, logging, realtime);

        vs.finish();
        mux.sinkCreationComplete();
        QVERIFY(mux.wait(30));

        QCOMPARE(outputLogging.values(), logging);
        QCOMPARE(outputRealtime.values(), realtime);
    }

    void complex()
    {
        Variant::Root config1;
        config1["Source/Combined"].setString("Top1");
        config1["Source/OverlayOnly"].setString("Overlay1");
        config1["GlobalMetadata"].metadata("Combined").setString("Top1");
        config1["GlobalMetadata"].metadata("OverlayOnly").setString("Overlay1");
        config1["VariableMetadata/T_S11"].metadata("Combined").setString("VariableTop1");
        config1["VariableMetadata/T_S11"].metadata("VariableOnly").setString("Overlay1");

        Variant::Root config2;
        config2["Source/Combined"].setString("Top2");
        config2["Source/OverlayOnly"].setString("Overlay2");
        config2["GlobalMetadata"].metadata("Combined").setString("Top2");
        config2["GlobalMetadata"].metadata("OverlayOnly").setString("Overlay2");
        config2["VariableMetadata/U_S11"].metadata("Combined").setString("VariableTop2");
        config2["VariableMetadata/U_S11"].metadata("VariableOnly").setString("Overlay2");

        Variant::Root config3;
        config3["Source/Combined"].setString("Top3");
        config3["Source/OverlayOnly"].setString("Overlay3");
        config3["GlobalMetadata"].metadata("Combined").setString("Top3");
        config3["GlobalMetadata"].metadata("OverlayOnly").setString("Overlay3");

        SinkMultiplexer mux;
        mux.start();
        StreamSink::Buffer outputLogging;
        mux.setEgress(&outputLogging);

        AcquisitionVariableSet vs(&mux, ValueSegment::Transfer{
                                          ValueSegment(FP::undefined(), 150.0, config1), ValueSegment(150.0, 300.0, config2),
                                          ValueSegment(300.0, FP::undefined(), config3)}, Variant::Root(), Variant::Root(),
                                  "S11");
        StreamSink::Buffer outputRealtime;
        vs.setRealtimeEgress(&outputRealtime);

        Variant::Root meta_T_S11_1;
        meta_T_S11_1.write().metadataReal("Source").hash("Combined").setString("BottomT1");
        meta_T_S11_1.write().metadataReal("Source").hash("UnderlayOnly").setString("BottomT1");
        meta_T_S11_1.write().metadataReal("Combined").setString("BottomT1");
        meta_T_S11_1.write().metadataReal("UnderlayOnly").setString("BottomT1");
        meta_T_S11_1.write()
                    .metadataReal("Smoothing")
                    .hash("Parameters")
                    .hash("Tin")
                    .setString("T");

        Variant::Root meta_T_S11_2;
        meta_T_S11_2.write().metadataReal("Source").hash("Combined").setString("BottomT2");
        meta_T_S11_2.write().metadataReal("Source").hash("UnderlayOnly").setString("BottomT2");
        meta_T_S11_2.write().metadataReal("Global").hash("Combined").setString("BottomT2");
        meta_T_S11_2.write().metadataReal("Global").hash("UnderlayOnly").setString("BottomT2");
        meta_T_S11_2.write()
                    .metadataReal("Smoothing")
                    .hash("Parameters")
                    .hash("Tin")
                    .setString("T");

        Variant::Root meta_U_S11_1;
        meta_U_S11_1.write().metadataReal("Source").hash("Combined").setString("Bottom1");
        meta_U_S11_1.write().metadataReal("Source").hash("UnderlayOnly").setString("BottomU1");
        meta_U_S11_1.write().metadataReal("Combined").setString("BottomU1");
        meta_U_S11_1.write().metadataReal("UnderlayOnly").setString("BottomU1");

        Variant::Root meta_U_S11_2;
        meta_U_S11_2.write().metadataReal("Source").hash("Combined").setString("BottomU2");
        meta_U_S11_2.write().metadataReal("Source").hash("UnderlayOnly").setString("BottomU2");
        meta_U_S11_2.write().metadataReal("Combined").setString("BottomU2");
        meta_U_S11_2.write().metadataReal("UnderlayOnly").setString("BottomU2");

        Variant::Root meta_F_S11_1;
        meta_F_S11_1.write().metadataFlags("Source").hash("Combined").setString("BottomF2");
        meta_F_S11_1.write().metadataFlags("Source").hash("UnderlayOnly").setString("BottomF2");
        meta_F_S11_1.write().metadataFlags("Combined").setString("BottomF2");
        meta_F_S11_1.write().metadataFlags("UnderlayOnly").setString("BottomF2");
        meta_F_S11_1.write().metadataSingleFlag("InputFlag").setString("Base");

        Variant::Root meta_flags_1;
        meta_flags_1.write().metadataSingleFlag("AddFlag1").setString("Add1");
        Variant::Root meta_flags_2;
        meta_flags_2.write().metadataSingleFlag("AddFlag2").setString("Add2");

        vs.process(10.0, SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "T_S11"), meta_T_S11_1,
                              FP::undefined(), 100.0),
                SequenceValue(SequenceName("bnd", "raw_meta", "U_S11"), meta_U_S11_1,
                              FP::undefined(), 200.0),
                SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"), meta_F_S11_1,
                              FP::undefined(), FP::undefined())}, SequenceValue::Transfer());

        vs.setPossibleSystemFlags(meta_flags_1);
        vs.setFlavors({"pm1"});
        vs.process(100.0, SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"), Variant::Root(Variant::Flags()),
                              80.0,
                              90.0),
                SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.0), 80.0, 90.0),
                SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(2.0), 85.0, 95.0),
                SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                              Variant::Root(Variant::Flags{"Z"}),
                              90.0, 130.0),
                SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(3.0), 95.0, 100.0),
                SequenceValue(SequenceName("bnd", "raw", "U_S11"), Variant::Root(4.0), 95.0, 110.0),
                SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(5.0), 100.0,
                              110.0), SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                    Variant::Root(Variant::Flags{"A"}),
                              100.0, 115.0)}, SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw_meta", "T_S11"), meta_T_S11_1, 100.0,
                              FP::undefined()),
                SequenceValue(SequenceName("bnd", "raw_meta", "U_S11"), meta_U_S11_1, 100.0,
                              FP::undefined()),
                SequenceValue(SequenceName("bnd", "raw_meta", "F1_S11"), meta_F_S11_1, 100.0,
                              FP::undefined()), SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"A"}),
                                                              100.0, FP::undefined()),
                SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(0.1), 100.0,
                              FP::undefined())});

        vs.setPossibleSystemFlags(meta_flags_2);
        vs.setSystemFlags({"Add1"});
        vs.process(110.0, SequenceValue::Transfer(), SequenceValue::Transfer());

        vs.setFlavors({"pm10"});
        vs.setFlushUntil(120.0);
        vs.process(115.0, SequenceValue::Transfer{
                           SequenceValue(SequenceName("bnd", "raw", "U_S11"), Variant::Root(1.1), 114.0,
                                         116.0),
                           SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(2.1), 115.0,
                                         120.0)},
                   SequenceValue::Transfer{SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags{"B"}), 115.0,
                                                         FP::undefined()),
                                           SequenceValue(SequenceName("bnd", "raw", "T_S11"),
                                                         Variant::Root(0.2), 115.0,
                                                         FP::undefined())});

        vs.process(120.0, SequenceValue::Transfer{
                           SequenceValue(SequenceName("bnd", "raw_meta", "T_S11"), meta_T_S11_2, 120.0,
                                         FP::undefined()),
                           SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.3), 120.0,
                                         160.0),
                           SequenceValue(SequenceName("bnd", "raw", "U_S11"), Variant::Root(2.3), 120.0,
                                         160.0)},
                   SequenceValue::Transfer{SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                         Variant::Root(Variant::Flags{"C"}), 120.0,
                                                         FP::undefined()),
                                           SequenceValue(SequenceName("bnd", "raw", "T_S11"),
                                                         Variant::Root(0.3), 120.0,
                                                         FP::undefined())});

        vs.process(200.0, SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.4), 190.0,
                              200.0),
                SequenceValue(SequenceName("bnd", "raw", "U_S11"), Variant::Root(2.4), 190.0,
                              200.0), SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                    Variant::Root(Variant::Flags{"B"}), 190.0,
                                                    200.0),
                SequenceValue(SequenceName("bnd", "raw_meta", "U_S11"), meta_U_S11_2, 200.0,
                              FP::undefined()), SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                                              Variant::Root(Variant::Flags{"C"}),
                                                              200.0, 300.0)},
                   SequenceValue::Transfer());

        vs.process(1000.0, SequenceValue::Transfer(), SequenceValue::Transfer());

        vs.finish();
        mux.sinkCreationComplete();
        QVERIFY(mux.wait(30));

        auto outputLoggingResult = outputLogging.take();
        auto outputRealtimeResult = outputRealtime.take();


        QVERIFY(checkContains(outputLoggingResult,
                              SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.0),
                                            80.0, 90.0)));
        QVERIFY(checkContains(outputLoggingResult,
                              SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(2.0),
                                            85.0, 95.0)));
        QVERIFY(checkContains(outputLoggingResult,
                              SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(3.0),
                                            95.0, 100.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw", "T_S11", {"pm1"}), Variant::Root(5.0), 100.0,
                110.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw", "T_S11", {"pm10"}), Variant::Root(1.3), 120.0,
                160.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw", "U_S11", {"pm10"}), Variant::Root(2.3), 120.0,
                160.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw", "T_S11", {"pm10"}), Variant::Root(1.4), 190.0,
                200.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw", "U_S11", {"pm10"}), Variant::Root(2.4), 190.0,
                200.0)));
        QVERIFY(checkContains(outputLoggingResult,
                              SequenceValue(SequenceName("bnd", "raw", "F1_S11"),
                                            Variant::Root(Variant::Flags()), 80.0, 90.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw", "F1_S11", {"pm1"}), Variant::Root(Variant::Flags{"A"}),
                100.0, 110.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw", "F1_S11", {"pm1"}),
                Variant::Root(Variant::Flags{"A", "Add1"}), 110.0, 115.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw", "F1_S11", {"pm10"}),
                Variant::Root(Variant::Flags{"B", "Add1"}), 190.0, 200.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw", "F1_S11", {"pm10"}),
                Variant::Root(Variant::Flags{"C", "Add1"}), 200.0, 300.0)));

        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw", "T_S11", {"pm1"}), Variant::Root(0.1), 100.0,
                FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw", "T_S11", {"pm1"}), Variant::Root(), 115.0,
                FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw", "T_S11", {"pm10"}), Variant::Root(0.3), 120.0,
                FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw", "F1_S11", {"pm1"}), Variant::Root(Variant::Flags{"A"}),
                100.0, FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw", "F1_S11", {"pm1"}), Variant::Root(), 115.0,
                FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw", "F1_S11", {"pm10"}),
                Variant::Root(Variant::Flags{"B", "Add1"}), 115.0, FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw", "F1_S11", {"pm10"}),
                Variant::Root(Variant::Flags{"C", "Add1"}), 120.0, FP::undefined())));

        for (const auto &dv : outputRealtimeResult) {
            if (!dv.getUnit().isMeta())
                continue;
            QVERIFY(dv.read().metadata("Processing").exists());
            QCOMPARE((int) dv.read().metadata("Processing").toArray().size(), 1);
        }
        for (const auto &dv : outputLoggingResult) {
            if (!dv.getUnit().isMeta())
                continue;
            QVERIFY(dv.read().metadata("Processing").exists());
            QCOMPARE((int) dv.read().metadata("Processing").toArray().size(), 1);
        }

        Variant::Root check = meta_T_S11_1;
        check.write().metadata("Source").hash("Combined").setString("Top1");
        check.write().metadata("Source").hash("OverlayOnly").setString("Overlay1");
        check.write().metadata("Combined").setString("VariableTop1");
        check.write().metadata("OverlayOnly").setString("Overlay1");
        check.write().metadata("VariableOnly").setString("Overlay1");
        check.write().metadata("Smoothing").hash("Parameters").hash("Tin").setString("T_S11");
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11"), check, FP::undefined(),
                100.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11", {"pm1"}), check,
                FP::undefined(), 100.0)));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11"), check, 100.0,
                FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11", {"pm1"}), check, 100.0,
                FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11", {"pm10"}), check, 115.0,
                FP::undefined())));
        check.write().set(meta_T_S11_2);
        check.write().metadata("Source").hash("Combined").setString("Top1");
        check.write().metadata("Source").hash("OverlayOnly").setString("Overlay1");
        check.write().metadata("Combined").setString("VariableTop1");
        check.write().metadata("OverlayOnly").setString("Overlay1");
        check.write().metadata("VariableOnly").setString("Overlay1");
        check.write().metadata("Smoothing").hash("Parameters").hash("Tin").setString("T_S11");
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11"), check, 120.0, 150.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11", {"pm1"}), check, 120.0,
                150.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11", {"pm10"}), check, 120.0,
                150.0)));
        check.write().set(meta_T_S11_2);
        check.write().metadata("Source").hash("Combined").setString("Top2");
        check.write().metadata("Source").hash("OverlayOnly").setString("Overlay2");
        check.write().metadata("Combined").setString("Top2");
        check.write().metadata("OverlayOnly").setString("Overlay2");
        check.write().metadata("Smoothing").hash("Parameters").hash("Tin").setString("T_S11");
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11"), check, 150.0, 300.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11", {"pm1"}), check, 150.0,
                300.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11", {"pm10"}), check, 150.0,
                300.0)));
        check.write().set(meta_T_S11_2);
        check.write().metadata("Source").hash("Combined").setString("Top3");
        check.write().metadata("Source").hash("OverlayOnly").setString("Overlay3");
        check.write().metadata("Combined").setString("Top3");
        check.write().metadata("OverlayOnly").setString("Overlay3");
        check.write().metadata("Smoothing").hash("Parameters").hash("Tin").setString("T_S11");
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11"), check, 300.0,
                FP::undefined())));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11", {"pm1"}), check, 300.0,
                FP::undefined())));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11", {"pm10"}), check, 300.0,
                FP::undefined())));

        check.write().set(meta_U_S11_1);
        check.write().metadata("Source").hash("Combined").setString("Top1");
        check.write().metadata("Source").hash("OverlayOnly").setString("Overlay1");
        check.write().metadata("Combined").setString("Top1");
        check.write().metadata("OverlayOnly").setString("Overlay1");
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11"), check, FP::undefined(),
                150.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11", {"pm1"}), check,
                FP::undefined(), 150.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11", {"pm10"}), check, 100.0,
                150.0)));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11"), check, 100.0,
                FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11", {"pm1"}), check, 100.0,
                FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11", {"pm10"}), check, 115.0,
                FP::undefined())));
        check.write().set(meta_U_S11_1);
        check.write().metadata("Source").hash("Combined").setString("Top2");
        check.write().metadata("Source").hash("OverlayOnly").setString("Overlay2");
        check.write().metadata("Combined").setString("VariableTop2");
        check.write().metadata("OverlayOnly").setString("Overlay2");
        check.write().metadata("VariableOnly").setString("Overlay2");
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11"), check, 150.0, 200.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11", {"pm1"}), check, 150.0,
                200.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11", {"pm10"}), check, 150.0,
                200.0)));
        check.write().set(meta_U_S11_2);
        check.write().metadata("Source").hash("Combined").setString("Top2");
        check.write().metadata("Source").hash("OverlayOnly").setString("Overlay2");
        check.write().metadata("Combined").setString("VariableTop2");
        check.write().metadata("OverlayOnly").setString("Overlay2");
        check.write().metadata("VariableOnly").setString("Overlay2");
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11"), check, 200.0, 300.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11", {"pm1"}), check, 200.0,
                300.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11", {"pm10"}), check, 200.0,
                300.0)));
        check.write().set(meta_U_S11_2);
        check.write().metadata("Source").hash("Combined").setString("Top3");
        check.write().metadata("Source").hash("OverlayOnly").setString("Overlay3");
        check.write().metadata("Combined").setString("Top3");
        check.write().metadata("OverlayOnly").setString("Overlay3");
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11"), check, 300.0,
                FP::undefined())));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11", {"pm1"}), check, 300.0,
                FP::undefined())));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "U_S11", {"pm10"}), check, 300.0,
                FP::undefined())));

        check.write().set(meta_F_S11_1);
        check.write().metadataFlags("Source").hash("Combined").setString("Top1");
        check.write().metadataFlags("Source").hash("OverlayOnly").setString("Overlay1");
        check.write().metadataFlags("Combined").setString("Top1");
        check.write().metadataFlags("OverlayOnly").setString("Overlay1");
        check.write().metadataSingleFlag("AddFlag1").setString("Add1");
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11"), check, FP::undefined(),
                150.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11", {"pm1"}), check,
                FP::undefined(), 150.0)));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11"), check, 100.0,
                FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11", {"pm1"}), check, 100.0,
                FP::undefined())));
        check.write().set(meta_F_S11_1);
        check.write().metadataFlags("Source").hash("Combined").setString("Top1");
        check.write().metadataFlags("Source").hash("OverlayOnly").setString("Overlay1");
        check.write().metadataFlags("Combined").setString("Top1");
        check.write().metadataFlags("OverlayOnly").setString("Overlay1");
        check.write().metadataSingleFlag("AddFlag2").setString("Add2");
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11"), check, 100.0, 150.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11", {"pm1"}), check, 100.0,
                150.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11", {"pm10"}), check, 100.0,
                150.0)));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11"), check, 110.0,
                FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11", {"pm1"}), check, 110.0,
                FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11", {"pm10"}), check, 115.0,
                FP::undefined())));
        check.write().set(meta_F_S11_1);
        check.write().metadataFlags("Source").hash("Combined").setString("Top2");
        check.write().metadataFlags("Source").hash("OverlayOnly").setString("Overlay2");
        check.write().metadataFlags("Combined").setString("Top2");
        check.write().metadataFlags("OverlayOnly").setString("Overlay2");
        check.write().metadataSingleFlag("AddFlag2").setString("Add2");
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11"), check, 150.0, 300.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11", {"pm1"}), check, 150.0,
                300.0)));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11", {"pm10"}), check, 150.0,
                300.0)));
        check.write().set(meta_F_S11_1);
        check.write().metadataFlags("Source").hash("Combined").setString("Top3");
        check.write().metadataFlags("Source").hash("OverlayOnly").setString("Overlay3");
        check.write().metadataFlags("Combined").setString("Top3");
        check.write().metadataFlags("OverlayOnly").setString("Overlay3");
        check.write().metadataSingleFlag("AddFlag2").setString("Add2");
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11"), check, 300.0,
                FP::undefined())));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11", {"pm1"}), check, 300.0,
                FP::undefined())));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "F1_S11", {"pm10"}), check, 300.0,
                FP::undefined())));

        QVERIFY(outputLoggingResult.empty());
        QVERIFY(outputRealtimeResult.empty());
    }

    void realtimeOverride()
    {
        SinkMultiplexer mux;
        mux.start();
        StreamSink::Buffer outputLogging;
        mux.setEgress(&outputLogging);
        AcquisitionVariableSet vs(&mux, ValueSegment::Transfer());
        StreamSink::Buffer outputRealtime;
        StreamSink::Buffer outputRealtimeDiscard;
        vs.setRealtimeEgress(&outputRealtime);
        vs.setRealtimeDiscardEgress(&outputRealtimeDiscard);

        Variant::Root meta;
        meta.write().metadataReal("Realtime").hash("Persistent").setBool(true);

        vs.process(200.0, (SequenceValue::Transfer{
                           SequenceValue(SequenceName("bnd", "raw_meta", "T_S11"), meta, 100.0,
                                         FP::undefined()),
                           SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.0), 100.0,
                                         300.0),
                           SequenceValue(SequenceName("bnd", "raw", "U_S11"), Variant::Root(1.1), 100.0,
                                         300.0)}),
                   (SequenceValue::Transfer{
                           SequenceValue(SequenceName("bnd", "raw_meta", "T_S11"), meta, 100.0,
                                         FP::undefined()),
                           SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.2),
                                         100.0,
                                         FP::undefined()),
                           SequenceValue(SequenceName("bnd", "raw", "U_S11"), Variant::Root(1.3),
                                         100.0,
                                         FP::undefined())}));

        vs.setFlavors({"pm1"});
        vs.process(250.0, SequenceValue::Transfer(), SequenceValue::Transfer());

        vs.setFlushUntil(350.0);
        vs.process(300.0, (SequenceValue::Transfer{
                           SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(2.0), 300.0,
                                         400.0),
                           SequenceValue(SequenceName("bnd", "raw", "U_S11"), Variant::Root(2.1), 300.0,
                                         400.0)}),
                   (SequenceValue::Transfer{
                           SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(2.2),
                                         300.0, FP::undefined()),
                           SequenceValue(SequenceName("bnd", "raw", "U_S11"), Variant::Root(2.3),
                                         300.0, FP::undefined())}));
        vs.process(350.0, SequenceValue::Transfer(), SequenceValue::Transfer());
        vs.process(400.0, SequenceValue::Transfer(), SequenceValue::Transfer());
        vs.process(1000.0, SequenceValue::Transfer(), SequenceValue::Transfer());

        vs.finish();
        mux.sinkCreationComplete();
        QVERIFY(mux.wait(30));

        auto outputRealtimeResult = outputRealtime.take();
        auto outputRealtimeDiscardResult = outputRealtimeDiscard.take();

        QVERIFY(checkContains(outputRealtimeResult,
                              SequenceValue(SequenceName("bnd", "raw_meta", "T_S11"), meta, 100.0,
                                            FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult,
                              SequenceValue(SequenceName("bnd", "raw_meta", "T_S11", {"pm1"}), meta,
                                            250.0, FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult,
                              SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(1.2),
                                            100.0, FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult,
                              SequenceValue(SequenceName("bnd", "raw", "U_S11"), Variant::Root(1.3),
                                            100.0, FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult,
                              SequenceValue(SequenceName("bnd", "raw", "T_S11"), Variant::Root(),
                                            250.0, FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult,
                              SequenceValue(SequenceName("bnd", "raw", "U_S11"), Variant::Root(),
                                            250.0, FP::undefined())));
        QVERIFY(checkContains(outputRealtimeResult, SequenceValue(
                SequenceName("bnd", "raw", "T_S11", {"pm1"}), Variant::Root(1.2), 250.0,
                FP::undefined())));
        QVERIFY(checkContains(outputRealtimeDiscardResult, SequenceValue(
                SequenceName("bnd", "raw", "T_S11", {"pm1"}), Variant::Root(2.2), 300.0,
                FP::undefined())));
        QVERIFY(checkContains(outputRealtimeDiscardResult, SequenceValue(
                SequenceName("bnd", "raw", "U_S11", {"pm1"}), Variant::Root(2.3), 300.0,
                FP::undefined())));

        auto outputLoggingResult = outputLogging.take();

        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11"), meta, 100.0,
                FP::undefined())));
        QVERIFY(checkContains(outputLoggingResult, SequenceValue(
                SequenceName("bnd", "raw_meta", "T_S11", {"pm1"}), meta, 100.0,
                FP::undefined())));

        QVERIFY(outputRealtimeResult.empty());
        QVERIFY(outputRealtimeDiscardResult.empty());
    }

    void flagsFlavorsGap()
    {
        SinkMultiplexer mux;
        mux.start();
        StreamSink::Buffer outputLogging;
        mux.setEgress(&outputLogging);
        AcquisitionVariableSet vs(&mux, ValueSegment::Transfer());
        StreamSink::Buffer outputRealtime;
        vs.setRealtimeEgress(&outputRealtime);

        SequenceValue::Transfer logging;
        SequenceValue::Transfer realtime;

        vs.setFlavors({"pm1"});
        vs.setFlushUntil(110.0);
        vs.process(90.0, logging, realtime);
        vs.process(95.0, logging, realtime);

        logging.emplace_back(SequenceName("bnd", "raw", "F1_S11"), Variant::Root(Variant::Flags()),
                             100.0,
                             200.0);
        vs.process(210.0, logging, realtime);
        logging.clear();

        vs.setFlavors({"pm10"});
        vs.process(220.0, logging, realtime);
        vs.process(230.0, logging, realtime);

        logging.emplace_back(SequenceName("bnd", "raw", "F1_S11"), Variant::Root(Variant::Flags()),
                             200.0,
                             300.0);
        vs.process(310.0, logging, realtime);

        logging.clear();
        logging.emplace_back(SequenceName("bnd", "raw", "F1_S11"), Variant::Root(Variant::Flags()),
                             300.0,
                             400.0);
        logging.emplace_back(SequenceName("bnd", "raw", "F1_S11"), Variant::Root(Variant::Flags()),
                             400.0, 500.0);
        vs.process(500.0, logging, realtime);
        logging.clear();

        vs.process(600.0, logging, realtime);

        vs.finish();
        mux.sinkCreationComplete();
        QVERIFY(mux.wait(30));

        logging.clear();
        logging.emplace_back(SequenceName("bnd", "raw", "F1_S11", {"pm1"}),
                             Variant::Root(Variant::Flags()), 100.0, 200.0);
        //logging.emplace_back(SequenceName("bnd", "raw", "F1_S11", {"pm10"}), Variant::Root(Variant::Flags()), 200.0, 300.0);
        logging.emplace_back(SequenceName("bnd", "raw", "F1_S11", {"pm10"}),
                             Variant::Root(Variant::Flags()), 300.0, 400.0);
        logging.emplace_back(SequenceName("bnd", "raw", "F1_S11", {"pm10"}),
                             Variant::Root(Variant::Flags()), 400.0, 500.0);

        QCOMPARE(outputLogging.values(), logging);
    }

    void metadataResend()
    {
        SinkMultiplexer mux;
        mux.start();
        StreamSink::Buffer outputLogging;
        mux.setEgress(&outputLogging);
        AcquisitionVariableSet vs(&mux, ValueSegment::Transfer());
        StreamSink::Buffer outputRealtime;
        vs.setRealtimeEgress(&outputRealtime);

        SequenceValue::Transfer logging;
        SequenceValue::Transfer realtime;

        vs.setFlavors({"pm1"});

        Variant::Root meta;
        meta.write().setType(Variant::Type::MetadataReal);

        logging.push_back(
                SequenceValue({"bnd", "raw_meta", "BsG_S11"}, meta, 100.0, FP::undefined()));
        vs.process(100.0, logging, realtime);
        logging.clear();

        vs.process(110.0, logging, realtime);

        vs.setFlavors({"pm10"});
        vs.process(120.0, logging, realtime);

        vs.process(200.0, logging, realtime, 200.0);

        vs.resendLoggingMetadata();
        vs.process(210.0, logging, realtime);

        vs.finish();
        mux.sinkCreationComplete();
        QVERIFY(mux.wait(30));

        auto result = outputLogging.take();

        QVERIFY(checkContains(result, SequenceValue(
                {"bnd", "raw_meta", "BsG_S11", SequenceName::Flavors{"pm1"}}, meta, 100.0,
                FP::undefined())));
        QVERIFY(checkContains(result, SequenceValue(
                {"bnd", "raw_meta", "BsG_S11", SequenceName::Flavors{"pm10"}}, meta, 100.0,
                FP::undefined())));
        QVERIFY(checkContains(result, SequenceValue(
                {"bnd", "raw_meta", "BsG_S11", SequenceName::Flavors{"pm1"}}, meta, 200.0,
                FP::undefined())));
        QVERIFY(checkContains(result, SequenceValue(
                {"bnd", "raw_meta", "BsG_S11", SequenceName::Flavors{"pm10"}}, meta, 200.0,
                FP::undefined())));

        QVERIFY(result.empty());
    }
};

QTEST_MAIN(TestVariableSet)

#include "variableset.moc"
