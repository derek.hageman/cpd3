/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "acquisition/acquisitionvariableset.hxx"
#include "core/environment.hxx"


Q_LOGGING_CATEGORY(log_acquisition_variableset_size, "cpd3.acquisition.variableset.size",
                   QtWarningMsg)

using namespace CPD3::Data;


namespace CPD3 {
namespace Acquisition {

/** @file acquisition/acquisitionvariableset.hxx
 * A set of grouped variables in the acquisition interface.
 */


AcquisitionVariableSet::AcquisitionVariableSet(SinkMultiplexer *loggingOutput,
                                               const ValueSegment::Transfer &config,
                                               const Variant::Read &processing,
                                               const Variant::Read &globalOverlay,
                                               const QString &name)
        : metadataGlobalState(),
          metadataVariableState(),
          variableState(),
          systemFlagsState(),
          flavorsUpdated(true),
          allFlavorSets(),
          previousFlavors(),
          activeFlavors(),
          systemFlagsUpdated(false),
          activeSystemFlags(),
          flushEndTimeUpdated(false),
          flushEndTime(FP::undefined()),
          bypassUpdated(false),
          bypassed(false),
          possibleSystemFlagsUpdated(false), possibleSystemFlags(), reinitializeMetadata(false),
          loggingMux(loggingOutput), realtimeEgress(nullptr), realtimeDiscardEgress(nullptr),
          loggingFinished(false),
          loggingLastAdvance(FP::undefined()),
          variableMetadata(),
          loggingDispatch(),
          realtimeDispatch(), metadataProcessing(processing),
          sourceName(name)
#ifndef NDEBUG
        , lastMultiplexerSizeCheck(0)
#endif
{

#ifdef MULTIPLEXER_TAG_TRACKING
    variableState.loggingIngress =
            loggingMux->createIngress(false, true, QString(":%1-VS").arg(sourceName));
    systemFlagsState.loggingIngress =
            loggingMux->createIngress(false, true, QString(":%1-SF").arg(sourceName));
#else
    variableState.loggingIngress = loggingMux->createSink();
    systemFlagsState.loggingIngress = loggingMux->createSink();
#endif

    if (globalOverlay.exists()) {
        Range::overlayFragmenting(metadataGlobalState.overlay,
                                  ValueSegment(FP::undefined(), FP::undefined(),
                                               Variant::Root(globalOverlay)));
    }
    for (const auto &segment : config) {
        if (segment.read().getPath("Source").exists()) {
            Variant::Root add;
            add.write().metadata("Source").set(segment.read().getPath("Source"));
            Range::overlayFragmenting(metadataGlobalState.overlay,
                                      ValueSegment(segment.getStart(), segment.getEnd(),
                                                   std::move(add)));
        }

        if (segment.read().getPath("GlobalMetadata").exists()) {
            Range::overlayFragmenting(metadataGlobalState.overlay,
                                      ValueSegment(segment.getStart(), segment.getEnd(),
                                                   Variant::Root(segment.read()
                                                                        .getPath(
                                                                                "GlobalMetadata"))));
        }

        auto variableConfig = segment.value().getPath("VariableMetadata");
        switch (variableConfig.getType()) {
        case Variant::Type::Hash:
            for (auto child : variableConfig.toHash()) {
                if (child.first.empty())
                    continue;
                VariableMetadataConfiguration add;
                add.selection.append(QString(), QString(), QString::fromStdString(child.first));
                add.overlay = ValueSegment(segment.getStart(), segment.getEnd(),
                                           Variant::Root(child.second));
                variableMetadata.emplace_back(std::move(add));
            }
            break;
        default:
            for (auto child : variableConfig.toChildren()) {
                VariableMetadataConfiguration add;
                add.selection = SequenceMatch::Composite(child.getPath("Variable"));
                add.overlay = ValueSegment(segment.getStart(), segment.getEnd(),
                                           Variant::Root(child.getPath("Metadata")));
                variableMetadata.emplace_back(std::move(add));
            }
            break;
        }
    }

    finalizeMetadataOverlay(metadataGlobalState);

    metadataProcessing["By"].setString("acquisition");
    metadataProcessing["At"].setDouble(Time::time());
    metadataProcessing["Environment"].setString(Environment::describe());
}

AcquisitionVariableSet::~AcquisitionVariableSet()
{
    for (auto &del : metadataVariableState) {
        if (del.second->loggingIngress) {
            Q_ASSERT(!loggingFinished);
            del.second->loggingIngress->endData();
        } else {
            Q_ASSERT(loggingFinished);
        }
    }
    metadataVariableState.clear();

    dataVariableState.clear();
    loggingDispatch.clear();
    realtimeDispatch.clear();

    if (variableState.loggingIngress)
        variableState.loggingIngress->endData();
    if (systemFlagsState.loggingIngress)
        systemFlagsState.loggingIngress->endData();
}

AcquisitionVariableSet::MetadataOverlay::MetadataOverlay()
        : overlay(), start(FP::undefined()), end(FP::undefined())
{ }

AcquisitionVariableSet::MetadataOverlay::MetadataOverlay(const MetadataOverlay &) = default;

AcquisitionVariableSet::MetadataOverlay &AcquisitionVariableSet::MetadataOverlay::operator=(const MetadataOverlay &) = default;

AcquisitionVariableSet::MetadataOverlay::MetadataOverlay(MetadataOverlay &&) = default;

AcquisitionVariableSet::MetadataOverlay &AcquisitionVariableSet::MetadataOverlay::operator=(
        MetadataOverlay &&) = default;

AcquisitionVariableSet::MetadataOverlay::MetadataOverlay(const MetadataOverlay &other,
                                                         double s,
                                                         double e) : overlay(other.overlay),
                                                                     start(s),
                                                                     end(e)
{ }

AcquisitionVariableSet::MetadataOverlay::MetadataOverlay(const ValueSegment &other,
                                                         double s,
                                                         double e) : overlay(other.value()),
                                                                     start(s),
                                                                     end(e)
{ }

AcquisitionVariableSet::MetadataOverlay::MetadataOverlay(const MetadataOverlay &under,
                                                         const ValueSegment &over,
                                                         double s, double e) : overlay(
        Variant::Root::overlay(under.overlay, over.root())), start(s), end(e)
{ }

void AcquisitionVariableSet::finalizeMetadataOverlay(MetadataStreamState &target)
{
    for (const auto &add : target.overlay) {
        target.stream.overlayInsert(add, add.start, add.end);
    }
    target.stream.overlayEnd();
}

AcquisitionVariableSet::VariableOverlay::VariableOverlay() : discard(false), flavors()
{ }

AcquisitionVariableSet::VariableOverlay::VariableOverlay(const VariableOverlay &) = default;

AcquisitionVariableSet::VariableOverlay &AcquisitionVariableSet::VariableOverlay::operator=(const VariableOverlay &) = default;

AcquisitionVariableSet::VariableOverlay::VariableOverlay(VariableOverlay &&) = default;

AcquisitionVariableSet::VariableOverlay &AcquisitionVariableSet::VariableOverlay::operator=(
        VariableOverlay &&) = default;

AcquisitionVariableSet::SystemFlagsOverlay::SystemFlagsOverlay() = default;

AcquisitionVariableSet::SystemFlagsOverlay::SystemFlagsOverlay(const SystemFlagsOverlay &) = default;

AcquisitionVariableSet::SystemFlagsOverlay &AcquisitionVariableSet::SystemFlagsOverlay::operator=(
        const SystemFlagsOverlay &) = default;

AcquisitionVariableSet::SystemFlagsOverlay::SystemFlagsOverlay(SystemFlagsOverlay &&) = default;

AcquisitionVariableSet::SystemFlagsOverlay &AcquisitionVariableSet::SystemFlagsOverlay::operator=(
        SystemFlagsOverlay &&) = default;

AcquisitionVariableSet::MetadataVariableData *AcquisitionVariableSet::lookupMetadataVariableData(
        const SequenceName &unit)
{
    Q_ASSERT(unit.isMeta());

    auto check = metadataVariableState.find(unit);
    if (check != metadataVariableState.end())
        return check->second.get();

    std::unique_ptr<MetadataVariableData> data(new MetadataVariableData);
#ifdef MULTIPLEXER_TAG_TRACKING
    data->loggingIngress = loggingMux->createIngress(false, true, QString(":%1-%2").arg(sourceName,
                                                                                        unit.getVariable()));
#else
    data->loggingIngress = loggingMux->createSink();
#endif
    data->isFlags = Util::starts_with(unit.getVariable(), "F1_");
    data->realtimePersistent = false;

    for (const auto &overlay : variableMetadata) {
        if (!overlay.selection.matches(unit))
            continue;
        Range::overlayFragmenting(data->state.overlay, overlay.overlay);
    }

    finalizeMetadataOverlay(data->state);

    return metadataVariableState.emplace(unit, std::move(data)).first->second.get();
}

AcquisitionVariableSet::VariableData *AcquisitionVariableSet::lookupVariableData(const SequenceName &unit)
{
    Q_ASSERT(!unit.isMeta());

    auto check = dataVariableState.find(unit);
    if (check != dataVariableState.end())
        return check->second.get();

    std::unique_ptr<VariableData> data(new VariableData);
    data->metadata = lookupMetadataVariableData(unit.toMeta());

    return dataVariableState.emplace(unit, std::move(data)).first->second.get();
}

AcquisitionVariableSet::DispatchBase *AcquisitionVariableSet::createDispatch(SequenceName::Map<
        std::unique_ptr<DispatchBase>> &dispatch, const SequenceName &unit)
{
    if (!unit.isMeta()) {
        if (Util::starts_with(unit.getVariable(), "F1_")) {
            return dispatch.emplace(unit, std::unique_ptr<DispatchBase>(
                                   new DispatchSystemFlags(&systemFlagsState,
                                                           lookupVariableData(unit),
                                                           lookupMetadataVariableData(
                                                                   unit.toMeta()))))
                           .first
                           ->second
                           .get();
        } else {
            return dispatch.emplace(unit, std::unique_ptr<DispatchBase>(
                                   new DispatchGeneral(&variableState, lookupVariableData(unit),
                                                       lookupMetadataVariableData(unit.toMeta()))))
                           .first
                           ->second
                           .get();
        }
    }

    return dispatch.emplace(unit, std::unique_ptr<DispatchBase>(
                           new DispatchMetadata(&metadataGlobalState,
                                                lookupMetadataVariableData(unit), this)))
                   .first
                   ->second
                   .get();
}

void AcquisitionVariableSet::applyValueOverlay(SequenceValue &under, const Variant::Root &over)
{
    if (!over.read().exists())
        return;

    auto underRead = under.read();
    if (underRead.getType() == over.read().getType() || !underRead.isMetadata()) {
        auto merged = Variant::Root::overlay(under.root(), over);
        under.setRoot(std::move(merged));
        return;
    }

    Variant::Root overData;
    auto overWrite = overData.write();

    auto underType = underRead.getType();
    overWrite.setType(underType);

    switch (underType) {
    case Variant::Type::MetadataReal:
    case Variant::Type::MetadataInteger:
    case Variant::Type::MetadataBoolean:
    case Variant::Type::MetadataString:
    case Variant::Type::MetadataBytes:
    case Variant::Type::MetadataArray:
    case Variant::Type::MetadataMatrix:
    case Variant::Type::MetadataKeyframe:
        switch (over.read().getType()) {
        case Variant::Type::Hash:
            for (auto add : over.read().toHash()) {
                overWrite.metadata(add.first).set(add.second);
            }
            break;
        default:
            for (auto add : over.read().toMetadata()) {
                overWrite.metadata(add.first).set(add.second);
            }
            break;
        }
        break;
    case Variant::Type::MetadataFlags:
        switch (over.read().getType()) {
        case Variant::Type::Hash:
            for (auto add : over.read().toHash()) {
                overWrite.metadataSingleFlag(add.first).set(add.second);
            }
            break;
        default:
            for (auto add : over.read().toMetadata()) {
                overWrite.metadataFlags(add.first).set(add.second);
            }
            break;
        }
        break;
    case Variant::Type::MetadataHash:
        switch (over.read().getType()) {
        case Variant::Type::Hash:
            for (auto add : over.read().toHash()) {
                overWrite.metadataHashChild(add.first).set(add.second);
            }
            break;
        default:
            for (auto add : over.read().toMetadata()) {
                overWrite.metadataHash(add.first).set(add.second);
            }
            break;
        }
        break;
    default:
        Q_ASSERT(false);
        break;
    }

    auto merged = Variant::Root::overlay(under.root(), overData);
    under.setRoot(std::move(merged));
}

void AcquisitionVariableSet::applyDynamicMetadata(SequenceValue &value) const
{
    Q_ASSERT(value.getUnit().isMeta());

    {
        auto target = value.write().metadata("Processing").toArray().after_back();
        target.set(metadataProcessing);
        target.hash("At").setDouble(Time::time());
    }

    auto parameters = value.write().metadata("Smoothing").hash("Parameters");
    if (parameters.getType() == Variant::Type::Hash) {
        /* This may need better logic if the smoothing parameters ever
         * require strings that aren't variable names */
        for (auto mod : parameters.toHash()) {
            if (mod.second.getType() != Variant::Type::String)
                continue;
            QString var(mod.second.toQString());
            if (var.isEmpty() || var.contains('_'))
                continue;
            var.append('_');
            var.append(sourceName);
            mod.second.setString(var);
        }
    }
}

void AcquisitionVariableSet::applyDynamicMetadata(SequenceValue::Transfer &meta) const
{
    for (auto &value : meta) {
        applyDynamicMetadata(value);
    }
}

void AcquisitionVariableSet::process(double time,
                                     const SequenceValue::Transfer &logging,
                                     const SequenceValue::Transfer &realtime,
                                     double loggingAdvanceTime)
{
    bool updateSystemFlagsVariableStream = false;
    bool updateSystemFlagsStream = false;
    bool updateVariablesStream = false;
    bool updateMetadataSystemFlagsStream = false;
    bool metadataFanoutNew = false;
    bool metadataFanoutResend = false;
    bool realtimePersistentResend = false;

    if (flavorsUpdated) {
        if (previousFlavors != activeFlavors) {
            systemFlagsState.variableOverlay.flavors = activeFlavors;
            variableState.overlay.flavors = activeFlavors;

            updateSystemFlagsVariableStream = true;
            updateVariablesStream = true;
            realtimePersistentResend = true;

            if (allFlavorSets.count(activeFlavors) == 0) {
                for (const auto &send : realtimeDispatch) {
                    send.second
                        ->realtimeFlavorsNew(send.first, activeFlavors, time, realtimeEgress);
                }
                metadataFanoutNew = true;
            }
            if (!allFlavorSets.empty()) {
                for (const auto &send : realtimeDispatch) {
                    send.second
                        ->realtimeFlavorsFlush(send.first, previousFlavors, time, realtimeEgress);
                }
            }
        }

        /* Outside the above, for the initial start up case */
        allFlavorSets.insert(activeFlavors);
    }

    if (systemFlagsUpdated) {
        systemFlagsState.flagsOverlay.flags = activeSystemFlags;
        updateSystemFlagsStream = true;
    }

    if (!bypassed) {
        if (variableState.overlay.discard) {
            if (!FP::defined(flushEndTime) || flushEndTime <= time) {
                variableState.overlay.discard = false;
                updateVariablesStream = true;
            }
        } else if (flushEndTimeUpdated) {
            if (FP::defined(flushEndTime) && flushEndTime > time) {
                variableState.overlay.discard = true;
                updateVariablesStream = true;
            }
        }
    } else if (!variableState.overlay.discard) {
        variableState.overlay.discard = true;
        updateVariablesStream = true;
    }

    if (possibleSystemFlagsUpdated) {
        updateMetadataSystemFlagsStream = true;
    }

    if (reinitializeMetadata) {
        metadataFanoutResend = true;
    }

    if (flavorsUpdated)
        previousFlavors = activeFlavors;
    flavorsUpdated = false;
    systemFlagsUpdated = false;
    flushEndTimeUpdated = false;
    possibleSystemFlagsUpdated = false;
    bypassUpdated = false;
    reinitializeMetadata = false;

    if (updateVariablesStream) {
        variableState.stream.overlayTail(variableState.overlay, time);
    }
    if (updateSystemFlagsVariableStream) {
        systemFlagsState.variableStream.overlayTail(systemFlagsState.variableOverlay, time);
    }
    if (updateSystemFlagsStream) {
        systemFlagsState.flagsStream.overlayTail(systemFlagsState.flagsOverlay, time);
    }

    variableState.stream.overlayAdvance(time);
    systemFlagsState.variableStream.overlayAdvance(time);
    systemFlagsState.flagsStream.overlayAdvance(time);

    if ((updateMetadataSystemFlagsStream || realtimePersistentResend) &&
            (realtimeEgress || realtimeDiscardEgress)) {
        SequenceName::Set normalSend;
        for (const auto &add : realtime) {
            normalSend.insert(add.getUnit());
        }
        if (updateMetadataSystemFlagsStream) {
            for (const auto &mvs : metadataVariableState) {
                MetadataVariableData *data = mvs.second.get();
                if (!data->isFlags)
                    continue;
                if (normalSend.count(mvs.first) != 0)
                    continue;
                auto target = realtimeDispatch.find(mvs.first);
                if (target == realtimeDispatch.end())
                    continue;

                target->second
                      ->realtimeResend(
                              SequenceValue(mvs.first, data->realtime, time, FP::undefined()),
                              realtimeEgress, realtimeDiscardEgress);
            }
        }
        if (realtimePersistentResend) {
            for (const auto &dvs : dataVariableState) {
                VariableData *data = dvs.second.get();
                if (!data->metadata->realtimePersistent)
                    continue;
                if (normalSend.count(dvs.first) != 0)
                    continue;
                auto target = realtimeDispatch.find(dvs.first);
                if (target == realtimeDispatch.end())
                    continue;

                target->second
                      ->realtimeResend(SequenceValue(dvs.first, data->realtimePersistentValue, time,
                                                     FP::undefined()), realtimeEgress,
                                       realtimeDiscardEgress);
            }
        }
    }

    for (const auto &add : realtime) {
        DispatchBase *target;
        {
            auto dispatch = realtimeDispatch.find(add.getUnit());
            if (dispatch == realtimeDispatch.end()) {
                target = createDispatch(realtimeDispatch, add.getUnit());
            } else {
                target = dispatch->second.get();
            }
        }

        target->realtime(add, realtimeEgress, realtimeDiscardEgress);
    }
    if (loggingFinished)
        return;

    for (const auto &add : logging) {
        DispatchBase *target;
        {
            auto dispatch = loggingDispatch.find(add.getUnit());
            if (dispatch == loggingDispatch.end()) {
                target = createDispatch(loggingDispatch, add.getUnit());
            } else {
                target = dispatch->second.get();
            }
        }

        target->logging(add);
    }

    /* Always try to advance as much as possible */
    if (FP::defined(loggingLastAdvance) &&
            FP::defined(loggingAdvanceTime) &&
            loggingAdvanceTime < loggingLastAdvance)
        loggingAdvanceTime = FP::undefined();
    if (!logging.empty() && Range::compareStart(logging.back().getStart(), loggingAdvanceTime) > 0)
        loggingAdvanceTime = logging.back().getStart();
    Q_ASSERT(!FP::defined(loggingAdvanceTime) ||
                     Range::compareStart(loggingAdvanceTime, loggingLastAdvance) >= 0);
    if (FP::defined(loggingAdvanceTime))
        loggingLastAdvance = loggingAdvanceTime;

    double metadataGlobalAdvanceEnd = FP::undefined();
    for (auto &add : metadataGlobalState.stream
                                        .streamAdvance(loggingAdvanceTime,
                                                       &metadataGlobalAdvanceEnd)) {
        auto target = loggingDispatch.find(add.getUnit());
        Q_ASSERT(target != loggingDispatch.end());
        target->second->loggingMetadataGlobalOutput(std::move(add));
    }

    if (FP::defined(metadataGlobalAdvanceEnd)) {
        for (const auto &mvs : metadataVariableState) {
            MetadataVariableData *data = mvs.second.get();
            double advanceEnd = FP::undefined();
            auto intermediate =
                    data->state.stream.streamAdvance(metadataGlobalAdvanceEnd, &advanceEnd);
            applyDynamicMetadata(intermediate);

            if (data->isFlags) {
                if (intermediate.empty() && updateMetadataSystemFlagsStream) {
                    if (data->logging.isValid()) {
                        applyValueOverlay(data->logging, possibleSystemFlags);
                        intermediate.emplace_back(data->logging);
                    } else if (FP::defined(advanceEnd) && loggingDispatch.count(mvs.first) != 0) {
                        intermediate.emplace_back(mvs.first, possibleSystemFlags, advanceEnd,
                                                  FP::undefined());
                    }
                } else {
                    for (auto &value : intermediate) {
                        applyValueOverlay(value, possibleSystemFlags);
                    }
                }
            }
            if (!intermediate.empty()) {
                data->logging = intermediate.back();
            } else if (metadataFanoutResend && data->logging.isValid()) {
                intermediate.emplace_back(data->logging);
            } else if (metadataFanoutNew && data->logging.isValid()) {
                auto newUnit = mvs.first;
                auto combinedFlavors = newUnit.getFlavors();
                Util::merge(activeFlavors, combinedFlavors);
                newUnit.setFlavors(combinedFlavors);

                auto duplicate = data->logging;
                duplicate.setUnit(std::move(newUnit));
                data->loggingIngress->incomingData(std::move(duplicate));

                if (FP::defined(advanceEnd)) {
                    data->loggingIngress->incomingAdvance(advanceEnd);
                    if (data->logging.isValid()) {
                        if (Range::compareStartEnd(advanceEnd, data->logging.getEnd()) >= 0) {
                            data->logging = SequenceValue();
                        } else {
                            data->logging.setStart(advanceEnd);
                        }
                    }
                }
                continue;
            } else {
                if (FP::defined(advanceEnd)) {
                    data->loggingIngress->incomingAdvance(advanceEnd);
                    if (data->logging.isValid()) {
                        if (Range::compareStartEnd(advanceEnd, data->logging.getEnd()) >= 0) {
                            data->logging = SequenceValue();
                        } else {
                            data->logging.setStart(advanceEnd);
                        }
                    }
                }
                continue;
            }

            SequenceValue::Transfer combined;
            for (const auto &value : intermediate) {
                Q_ASSERT(value.getUnit() == mvs.first);
                for (const auto &flavorSet : allFlavorSets) {
                    auto newUnit = mvs.first;
                    auto combinedFlavors = newUnit.getFlavors();
                    Util::merge(flavorSet, combinedFlavors);
                    newUnit.setFlavors(std::move(combinedFlavors));

                    combined.emplace_back(
                            SequenceIdentity(std::move(newUnit), value.getStart(), value.getEnd()),
                            value.root());
                }
            }

            data->loggingIngress->incomingData(combined);
            if (FP::defined(advanceEnd)) {
                data->loggingIngress->incomingAdvance(advanceEnd);
                if (data->logging.isValid()) {
                    if (Range::compareStartEnd(advanceEnd, data->logging.getEnd()) >= 0) {
                        data->logging = SequenceValue();
                    } else {
                        data->logging.setStart(advanceEnd);
                    }
                }
            } else if (!combined.empty()) {
                if (data->logging.isValid()) {
                    if (Range::compareStartEnd(combined.back().getStart(),
                                               data->logging.getEnd()) >= 0) {
                        data->logging = SequenceValue();
                    } else {
                        data->logging.setStart(combined.back().getStart());
                    }
                }
            }
        }
    }

    double advanceEnd = FP::undefined();
    variableState.loggingIngress->incomingData(
                         variableState.stream.streamAdvance(loggingAdvanceTime, &advanceEnd));
    if (FP::defined(advanceEnd))
        variableState.loggingIngress->incomingAdvance(advanceEnd);

    advanceEnd = FP::undefined();
    for (auto &add : systemFlagsState.variableStream
                                     .streamAdvance(loggingAdvanceTime, &advanceEnd)) {
        systemFlagsState.flagsStream.streamInsert(std::move(add));
    }
    double flagsAdvanceTime = advanceEnd;
    advanceEnd = FP::undefined();
    systemFlagsState.loggingIngress
                    ->incomingData(systemFlagsState.flagsStream
                                                   .streamAdvance(flagsAdvanceTime, &advanceEnd));
    if (FP::defined(advanceEnd))
        systemFlagsState.loggingIngress->incomingAdvance(advanceEnd);

#ifndef NDEBUG
    if (log_acquisition_variableset_size().isDebugEnabled() &&
            time - lastMultiplexerSizeCheck > 120.0) {
        lastMultiplexerSizeCheck = time;
        if (loggingMux->totalBufferedValues() > 1000) {
            qCDebug(log_acquisition_variableset_size) << "Multiplexer for" << sourceName << " "
                                                      << *loggingMux
                                                      << "has exceeded size threshold" << 1000;

        }
    }
#endif
}

void AcquisitionVariableSet::finish(bool finishRealtime)
{
    if (loggingFinished) {
        if (finishRealtime) {
#ifndef NDEBUG
            for (const auto &del : metadataVariableState) {
                Q_ASSERT(!del.second->loggingIngress);
            }
#endif
            metadataVariableState.clear();
            dataVariableState.clear();
            realtimeDispatch.clear();
            realtimeEgress = nullptr;
            realtimeDiscardEgress = nullptr;
        }
        return;
    }
    loggingFinished = true;

    variableState.loggingIngress->incomingData(variableState.stream.finish());
    variableState.loggingIngress->endData();
    variableState.loggingIngress = nullptr;

    for (auto &add : systemFlagsState.variableStream.finish()) {
        systemFlagsState.flagsStream.streamInsert(std::move(add));
    }
    systemFlagsState.loggingIngress->incomingData(systemFlagsState.flagsStream.finish());
    systemFlagsState.loggingIngress->endData();
    systemFlagsState.loggingIngress = nullptr;

    for (auto &add : metadataGlobalState.stream.finish()) {
        auto target = loggingDispatch.find(add.getUnit());
        Q_ASSERT(target != loggingDispatch.end());
        target->second->loggingMetadataGlobalOutput(std::move(add));
    }

    for (const auto &mvs : metadataVariableState) {
        MetadataVariableData *data = mvs.second.get();
        auto intermediate = data->state.stream.finish();
        applyDynamicMetadata(intermediate);

        if (data->isFlags) {
            for (auto &value : intermediate) {
                applyValueOverlay(value, possibleSystemFlags);
            }
        }

        SequenceValue::Transfer combined;
        for (const auto &value : intermediate) {
            Q_ASSERT(value.getUnit() == mvs.first);
            for (const auto &flavorSet : allFlavorSets) {
                auto newUnit = mvs.first;
                auto combinedFlavors = newUnit.getFlavors();
                Util::merge(flavorSet, combinedFlavors);
                newUnit.setFlavors(std::move(combinedFlavors));

                combined.emplace_back(
                        SequenceIdentity(std::move(newUnit), value.getStart(), value.getEnd()),
                        value.root());
            }
        }

        data->loggingIngress->incomingData(combined);
        data->loggingIngress->endData();
        data->loggingIngress = nullptr;
    }
    if (finishRealtime) {
        metadataVariableState.clear();
        dataVariableState.clear();
    }

    loggingDispatch.clear();

    if (finishRealtime) {
        realtimeDispatch.clear();
        realtimeEgress = nullptr;
        realtimeDiscardEgress = nullptr;
    }
}

bool AcquisitionVariableSet::MetadataStreamState::applyRealtime(SequenceValue &value)
{
    if (!Range::intersectShift(overlay, value.getStart()))
        return true;
    return overlay.front().applyOverlay(value, false);
}

void AcquisitionVariableSet::MetadataVariableData::updateRealtime(const Variant::Read &update)
{
    realtime.write().set(update);
    realtimePersistent = update.metadata("Realtime").hash("Persistent").toBool();
}

AcquisitionVariableSet::DispatchBase::DispatchBase() = default;

AcquisitionVariableSet::DispatchBase::~DispatchBase() = default;

void AcquisitionVariableSet::DispatchBase::loggingMetadataGlobalOutput(const SequenceValue &) const
{
    Q_ASSERT(false);
}

void AcquisitionVariableSet::DispatchBase::realtimeResend(const SequenceValue &value,
                                                          StreamSink *egress,
                                                          StreamSink *discardEgress) const
{ realtime(value, egress, discardEgress); }

void AcquisitionVariableSet::DispatchBase::realtimeFlavorsNew(const SequenceName &,
                                                              const SequenceName::Flavors &,
                                                              double,
                                                              StreamSink *) const
{ }

void AcquisitionVariableSet::DispatchBase::realtimeFlavorsFlush(const SequenceName &,
                                                                const SequenceName::Flavors &,
                                                                double,
                                                                StreamSink *) const
{ }

AcquisitionVariableSet::DispatchMetadata::DispatchMetadata(MetadataStreamState *g,
                                                           MetadataVariableData *d,
                                                           AcquisitionVariableSet *vs) : global(g),
                                                                                         data(d),
                                                                                         variableSet(
                                                                                                 vs)
{ }

AcquisitionVariableSet::DispatchMetadata::~DispatchMetadata() = default;

void AcquisitionVariableSet::DispatchMetadata::logging(const SequenceValue &value) const
{
    global->stream.streamInsert(value);
}

void AcquisitionVariableSet::DispatchMetadata::loggingMetadataGlobalOutput(const SequenceValue &value) const
{
    data->state.stream.streamInsert(value);
}

void AcquisitionVariableSet::DispatchMetadata::realtimeResend(const SequenceValue &value,
                                                              StreamSink *egress,
                                                              StreamSink *discardEgress) const
{
    Q_UNUSED(discardEgress);

    auto result = value;
    if (!global->applyRealtime(result))
        return;
    if (!data->state.applyRealtime(result))
        return;
    if (data->isFlags) {
        applyValueOverlay(result, variableSet->possibleSystemFlags);
    }
    data->updateRealtime(result.read());
    if (!egress)
        return;
    for (const auto &flavorSet : variableSet->allFlavorSets) {
        auto newUnit = value.getUnit();
        auto combinedFlavors = newUnit.getFlavors();
        Util::merge(flavorSet, combinedFlavors);
        newUnit.setFlavors(std::move(combinedFlavors));

        result.setUnit(std::move(newUnit));
        egress->incomingData(result);
    }
}

void AcquisitionVariableSet::DispatchMetadata::realtime(const SequenceValue &value,
                                                        StreamSink *egress, StreamSink *) const
{
    auto result = value;
    variableSet->applyDynamicMetadata(result);
    if (!global->applyRealtime(result))
        return;
    if (!data->state.applyRealtime(result))
        return;
    if (data->isFlags) {
        applyValueOverlay(result, variableSet->possibleSystemFlags);
    }
    data->updateRealtime(result.read());
    if (!egress)
        return;
    for (const auto &flavorSet : variableSet->allFlavorSets) {
        auto newUnit = value.getUnit();
        auto combinedFlavors = newUnit.getFlavors();
        Util::merge(flavorSet, combinedFlavors);
        newUnit.setFlavors(std::move(combinedFlavors));

        result.setUnit(std::move(newUnit));
        egress->incomingData(result);
    }
}

void AcquisitionVariableSet::DispatchMetadata::realtimeFlavorsNew(const SequenceName &baseUnit,
                                                                  const SequenceName::Flavors &newFlavors,
                                                                  double time,
                                                                  StreamSink *egress) const
{
    if (!egress)
        return;
    auto combinedFlavors = baseUnit.getFlavors();
    Util::merge(newFlavors, combinedFlavors);
    auto newUnit = baseUnit;
    newUnit.setFlavors(std::move(combinedFlavors));
    egress->emplaceData(std::move(newUnit), data->realtime, time, FP::undefined());
}

AcquisitionVariableSet::DispatchGeneral::DispatchGeneral(VariableStreamState *s,
                                                         VariableData *d,
                                                         MetadataVariableData *meta) : state(s),
                                                                                       data(d),
                                                                                       metadata(
                                                                                               meta)
{ }

AcquisitionVariableSet::DispatchGeneral::~DispatchGeneral() = default;

void AcquisitionVariableSet::DispatchGeneral::logging(const SequenceValue &value) const
{
    state->stream.streamInsert(value);
}

void AcquisitionVariableSet::DispatchGeneral::realtime(const SequenceValue &value,
                                                       StreamSink *egress,
                                                       StreamSink *discardEgress) const
{
    if (!egress) {
        if (!discardEgress)
            return;
        auto result = value;
        if (state->overlay.applyOverlay(result, false))
            return;
        if (!state->overlay.applyOverlay(result, false, true))
            return;
        return;
    }
    auto result = value;
    if (!state->overlay.applyOverlay(result, false)) {
        if (!discardEgress)
            return;
        if (!state->overlay.applyOverlay(result, false, true))
            return;

        if (metadata->realtimePersistent) {
            data->realtimePersistentValue = result.root();
        }
        discardEgress->incomingData(std::move(result));
        return;
    }
    if (metadata->realtimePersistent) {
        data->realtimePersistentValue = result.root();
    }
    egress->incomingData(std::move(result));
}

void AcquisitionVariableSet::DispatchGeneral::realtimeFlavorsFlush(const SequenceName &baseUnit,
                                                                   const SequenceName::Flavors &previousFlavors,
                                                                   double time,
                                                                   StreamSink *egress) const
{
    if (!egress)
        return;
    auto combinedFlavors = baseUnit.getFlavors();
    Util::merge(previousFlavors, combinedFlavors);
    auto newUnit = baseUnit;
    newUnit.setFlavors(std::move(combinedFlavors));
    egress->emplaceData(std::move(newUnit), Variant::Root(), time, FP::undefined());
}

AcquisitionVariableSet::DispatchSystemFlags::DispatchSystemFlags(SystemFlagsState *s,
                                                                 VariableData *d,
                                                                 MetadataVariableData *meta)
        : state(s), data(d), metadata(meta)
{ }

AcquisitionVariableSet::DispatchSystemFlags::~DispatchSystemFlags() = default;

void AcquisitionVariableSet::DispatchSystemFlags::logging(const SequenceValue &value) const
{
    state->variableStream.streamInsert(value);
}

void AcquisitionVariableSet::DispatchSystemFlags::realtime(const SequenceValue &value,
                                                           StreamSink *egress,
                                                           StreamSink *discardEgress) const
{
    if (!egress) {
        if (!discardEgress)
            return;
        auto result = value;
        if (!state->variableOverlay.applyOverlay(result, false)) {
            if (!state->variableOverlay.applyOverlay(result, false, true))
                return;
            if (!state->flagsOverlay.applyOverlay(result, false)) {
                if (!state->flagsOverlay.applyOverlay(result, false, true))
                    return;
            }
            return;
        }
        if (!state->flagsOverlay.applyOverlay(result, false)) {
            if (!state->flagsOverlay.applyOverlay(result, false, true))
                return;
            return;
        }
        return;
    }
    auto result = value;
    if (!state->variableOverlay.applyOverlay(result, false)) {
        if (!discardEgress)
            return;
        if (!state->variableOverlay.applyOverlay(result, false, true))
            return;
        if (!state->flagsOverlay.applyOverlay(result, false)) {
            if (!state->flagsOverlay.applyOverlay(result, false, true))
                return;
        }

        if (metadata->realtimePersistent) {
            data->realtimePersistentValue = result.root();
        }
        discardEgress->incomingData(std::move(result));
        return;
    }
    if (!state->flagsOverlay.applyOverlay(result, false)) {
        if (!discardEgress)
            return;
        if (!state->flagsOverlay.applyOverlay(result, false, true))
            return;

        if (metadata->realtimePersistent) {
            data->realtimePersistentValue = result.root();
        }
        discardEgress->incomingData(std::move(result));
        return;
    }

    if (metadata->realtimePersistent) {
        data->realtimePersistentValue = result.root();
    }
    egress->incomingData(std::move(result));
}

void AcquisitionVariableSet::DispatchSystemFlags::realtimeFlavorsFlush(const SequenceName &baseUnit,
                                                                       const SequenceName::Flavors &previousFlavors,
                                                                       double time,
                                                                       StreamSink *egress) const
{
    if (!egress)
        return;
    auto combinedFlavors = baseUnit.getFlavors();
    Util::merge(previousFlavors, combinedFlavors);
    auto newUnit = baseUnit;
    newUnit.setFlavors(std::move(combinedFlavors));
    egress->emplaceData(std::move(newUnit), Variant::Root(), time, FP::undefined());
}


bool AcquisitionVariableSet::MetadataOverlay::applyOverlay(SequenceValue &value,
                                                           bool intersecting) const
{
    Q_UNUSED(intersecting);
    applyValueOverlay(value, overlay);
    return true;
}

bool AcquisitionVariableSet::VariableOverlay::applyOverlay(SequenceValue &value,
                                                           bool intersecting,
                                                           bool ignoreDiscard) const
{
    if (intersecting)
        return false;
    if (discard && !ignoreDiscard)
        return false;

    if (!flavors.empty()) {
        auto combined = value.getFlavors();
        Util::merge(flavors, combined);
        value.setFlavors(std::move(combined));
    }

    return true;
}

bool AcquisitionVariableSet::SystemFlagsOverlay::applyOverlay(SequenceValue &value,
                                                              bool,
                                                              bool) const
{
    if (!flags.empty()) {
        value.write().addFlags(flags);
    }

    return true;
}

void AcquisitionVariableSet::setFlavors(const SequenceName::Flavors &flavors)
{
    activeFlavors = flavors;
    flavorsUpdated = true;
}

void AcquisitionVariableSet::setSystemFlags(const Variant::Flags &flags)
{
    if (activeSystemFlags == flags)
        return;
    systemFlagsUpdated = true;
    activeSystemFlags = flags;
}

void AcquisitionVariableSet::setFlushUntil(double time)
{
    if (FP::equal(flushEndTime, time))
        return;
    flushEndTimeUpdated = true;
    flushEndTime = time;
}

void AcquisitionVariableSet::setBypassed(bool b)
{
    if (bypassed == b)
        return;
    bypassUpdated = true;
    bypassed = b;
}

void AcquisitionVariableSet::setPossibleSystemFlags(const Variant::Read &metadata)
{
    possibleSystemFlagsUpdated = true;
    possibleSystemFlags.write().set(metadata);
}

void AcquisitionVariableSet::resendLoggingMetadata()
{
    reinitializeMetadata = true;
}

StreamSink *AcquisitionVariableSet::setRealtimeEgress(StreamSink *egress)
{
    StreamSink *old = realtimeEgress;
    realtimeEgress = egress;
    return old;
}

StreamSink *AcquisitionVariableSet::setRealtimeDiscardEgress(StreamSink *egress)
{
    StreamSink *old = realtimeDiscardEgress;
    realtimeDiscardEgress = egress;
    return old;
}

}
}
