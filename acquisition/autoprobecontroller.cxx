/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <unordered_set>
#include <functional>
#include <cstdint>

#include "acquisition/autoprobecontroller.hxx"
#include "core/number.hxx"

using namespace CPD3::Data;


namespace CPD3 {
namespace Acquisition {

static std::unordered_set<QString> getDefaultComponents(IOTarget &target,
                                                        const ValueSegment::Transfer &config)
{
    std::unordered_set<QString> loadComponents;

    if (!config.empty()) {
        auto check = config.back()["Autoprobe/DisableDefaultComponents"];
        if (check.getType() == Variant::Type::Boolean) {
            if (check.toBool())
                return loadComponents;
        }
        for (auto child : check.toChildren()) {
            auto childInterface = IOTarget::create(child);
            if (!childInterface)
                continue;
            if (target.matchesDevice(*childInterface))
                return loadComponents;
        }
    }

    for (const auto &add : ComponentLoader::list("acquisition")) {
        loadComponents.insert(add.first);
    }

    return loadComponents;
}

static std::string assembleLoggingCategory(IOInterface &io)
{
    std::string base = "cpd3.acquisition.autoprobe.";
    base += io.target(true)->description().toLatin1().constData();
    return base;
}

AutoprobeController::Parameters::Parameters(AcquisitionComponent *component) : component(component),
                                                                               maximumTries(3),
                                                                               passive(false),
                                                                               canChangeInterface(
                                                                                       true),
                                                                               ignoreBackgroundFailure(
                                                                                       false),
                                                                               isPriorityComponent(
                                                                                       false),
                                                                               autoprobeInitialOrder(
                                                                                       0)
{ }

AutoprobeController::Parameters::Parameters() : component(nullptr),
                                                maximumTries(3),
                                                passive(false),
                                                canChangeInterface(true),
                                                isPriorityComponent(false),
                                                autoprobeInitialOrder(0)
{ }

AutoprobeController::Active::Active(IOInterface &io,
                                    Parameters &&params,
                                    std::unique_ptr<AcquisitionInterface> &&iface) : parameters(
        std::move(params)),
                                                                                     control(),
                                                                                     interface(
                                                                                             std::move(
                                                                                                     iface)),
                                                                                     receiver(
                                                                                             new DataReceiver(
                                                                                                     io,
                                                                                                     *interface)),
                                                                                     tries(0)
{ }

AutoprobeController::Active::Active() = default;

AutoprobeController::AutoprobeController(std::unique_ptr<IOInterface> &&iface,
                                         const ValueSegment::Transfer &config,
                                         const QString &priorityComponent) : state(
        State::Initialize),
                                                                             interface(std::move(
                                                                                     iface)),
                                                                             selectedAtRealtime(
                                                                                     FP::undefined()),
                                                                             loggingName(
                                                                                     assembleLoggingCategory(
                                                                                             *interface)),
                                                                             logging(loggingName.data(),
                                                                                     QtWarningMsg)
{
    Q_ASSERT(interface);

    std::unordered_set<QString> loadComponents;

    std::unordered_map<std::string, ValueSegment::Transfer> componentConfiguration;
    int defaultTries = 3;
    bool disableUnconfigured = false;
    auto deviceTarget = interface->target(true);
    if (!config.empty()) {
        std::unique_ptr<IOTarget> target;
        for (const auto &segment : config) {
            for (auto cmp : segment.value()["Components"].toHash()) {
                if (cmp.first.empty())
                    continue;
                if (cmp.second.hash("Fixed").toBool())
                    continue;
                if (cmp.second.hash("Interface").exists()) {
                    auto check = IOTarget::create(cmp.second.hash("Interface"));
                    if (!check || !check->matchesDevice(*deviceTarget))
                        continue;
                }

                componentConfiguration[cmp.first].emplace_back(segment.getStart(), segment.getEnd(),
                                                               Variant::Root(cmp.second));

                if (cmp.second.hash("Name").exists()) {
                    loadComponents.insert(cmp.second.hash("Name").toQString());
                }
            }
        }

        qint64 n = config.back().value()["Autoprobe/Retries"].toInt64();
        if (INTEGER::defined(n) && n >= 0)
            defaultTries = (int) (n + 1);

        disableUnconfigured =
                config.back().value()["Autoprobe/DisableDefaultConfiguration"].toBool();
    }
    Q_ASSERT(defaultTries > 0);

    auto defaultComponents = getDefaultComponents(*interface->target(true), config);
    Util::merge(defaultComponents, loadComponents);

    double tNow = Time::time();

    std::map<int, std::vector<Active>> sorted;
    for (const auto &componentName : loadComponents) {
        QObject *baseComponent = ComponentLoader::create(componentName);
        if (!baseComponent) {
            qCDebug(logging) << "Failed to load component" << componentName;
            continue;
        }
        AcquisitionComponent *component = qobject_cast<AcquisitionComponent *>(baseComponent);
        if (!component)
            continue;

        for (const auto &checkAdd : componentConfiguration) {
            ValueSegment::Transfer configuration;
            for (const auto &segment : checkAdd.second) {
                if (segment.value().hash("Name").exists() &&
                        segment.value().hash("Name").toQString() != componentName)
                    continue;

                configuration.emplace_back(segment);
            }

            if (configuration.empty())
                continue;
            if (FP::defined(configuration.back().getEnd()) && configuration.back().getEnd() < tNow)
                continue;

            std::string loggingContext;
            {
                const auto &add = configuration.back().value().hash("Instrument").toString();
                if (!add.empty() && add.find_first_of('$') == add.npos) {
                    if (!loggingContext.empty())
                        loggingContext += '.';
                    loggingContext += add;
                }
            }
            {
                QString add = deviceTarget->description();
                if (!add.isEmpty()) {
                    if (!loggingContext.empty())
                        loggingContext += '.';
                    loggingContext += add.toStdString();
                }
            }

            std::unique_ptr<AcquisitionInterface>
                    acq(component->createAcquisitionAutoprobe(configuration, loggingContext));
            if (!acq)
                continue;

            auto defaults = acq->getDefaults();

            Parameters parameters(component);

            parameters.componentName = componentName;
            parameters.loggingContext = std::move(loggingContext);

            parameters.passive = config.back().value()["Autoprobe/DefaultPassive"].toBoolean();
            if (configuration.back().value().hash("Passive").exists())
                parameters.passive = configuration.back().value().hash("Passive").toBoolean();

            parameters.ignoreBackgroundFailure =
                    config.back().value()["Autoprobe/DefaultIgnoreBackgroundFailure"].toBoolean();
            if (configuration.back().value().hash("IgnoreBackgroundFailure").exists())
                parameters.ignoreBackgroundFailure =
                        configuration.back().value().hash("IgnoreBackgroundFailure").toBoolean();

            parameters.canChangeInterface =
                    !config.back().value()["Autoprobe/DefaultFixedInterface"].toBoolean();
            if (configuration.back().read().hash("FixedInterface").exists())
                parameters.canChangeInterface =
                        configuration.back().read().hash("FixedInterface").toBoolean();

            parameters.maximumTries = defaultTries;
            if (defaults.autoprobeTries > 0)
                parameters.maximumTries = defaults.autoprobeTries;
            if (parameters.passive && !parameters.canChangeInterface)
                parameters.maximumTries = 1;
            {
                auto i = configuration.back().value().hash("Retries").toInteger();
                if (INTEGER::defined(i) && i >= 0)
                    parameters.maximumTries = static_cast<int>(i) + 1;
            }

            parameters.interfaceParameters
                      .write()
                      .set(configuration.back().value().hash("Interface"));
            parameters.sourceParameters.write().set(configuration.back().value().hash("Match"));

            parameters.instrumentName = configuration.back().value().hash("Instrument").toQString();

            parameters.isPriorityComponent = (priorityComponent == componentName);
            {
                auto i = configuration.back().value().hash("InitialOrder").toInteger();
                if (!INTEGER::defined(i))
                    i = defaults.autoprobeInitialOrder;
                if (!INTEGER::defined(i))
                    i = 0;
                parameters.autoprobeInitialOrder = static_cast<int>(i);
            }

            auto level = configuration.back().value().hash("Priority").toInteger();
            if (!INTEGER::defined(level)) {
                if (parameters.isPriorityComponent)
                    level = 0;
                else
                    level = defaults.autoprobeLevelPriority;
            }

            parameters.configuration = std::move(configuration);
            sorted[level].emplace_back(*interface, std::move(parameters), std::move(acq));
        }

        if (defaultComponents.count(componentName) != 0 && !disableUnconfigured) {
            std::string loggingContext;
            {
                QString add = deviceTarget->description();
                if (!add.isEmpty()) {
                    if (!loggingContext.empty())
                        loggingContext += '.';
                    loggingContext += add.toStdString();
                }
            }

            std::unique_ptr<AcquisitionInterface> acq
                    (component->createAcquisitionAutoprobe(ValueSegment::Transfer(),
                                                           loggingContext));
            if (!acq)
                continue;

            auto defaults = acq->getDefaults();

            Parameters parameters(component);
            parameters.componentName = componentName;
            parameters.loggingContext = std::move(loggingContext);

            if (!config.empty()) {
                parameters.canChangeInterface =
                        !config.back().value()["Autoprobe/DefaultFixedInterface"].toBool();
                parameters.passive = config.back().value()["Autoprobe/DefaultPassive"].toBool();
            }
            parameters.isPriorityComponent = (priorityComponent == componentName);

            parameters.maximumTries = defaultTries;
            if (defaults.autoprobeTries > 0)
                parameters.maximumTries = defaults.autoprobeTries;
            if (parameters.passive && !parameters.canChangeInterface)
                parameters.maximumTries = 1;

            sorted[std::numeric_limits<int>::max()].emplace_back(*interface, std::move(parameters),
                                                                 std::move(acq));
        }
    }

    if (sorted.empty())
        return;

    double now = Time::time();
    for (auto &sortList : sorted) {
        std::stable_sort(sortList.second.begin(), sortList.second.end(),
                         [](const Active &a, const Active &b) {
                             if (a.parameters.isPriorityComponent) {
                                 if (!b.parameters.isPriorityComponent)
                                     return true;
                             } else if (b.parameters.isPriorityComponent) {
                                 return false;
                             }
                             if (!a.parameters.configuration.empty()) {
                                 if (b.parameters.configuration.empty())
                                     return true;
                             } else if (!b.parameters.configuration.empty()) {
                                 return false;
                             }
                             if (a.parameters.autoprobeInitialOrder !=
                                     b.parameters.autoprobeInitialOrder) {
                                 return a.parameters.autoprobeInitialOrder <
                                         b.parameters.autoprobeInitialOrder;
                             }
                             return a.parameters.componentName < b.parameters.componentName;
                         });

        for (auto &active : sortList.second) {
            active.control.reset(new ControlInterface(this));

            active.connections
                  .emplace_back(active.interface
                                      ->sourceMetadataUpdated
                                      .connect(receiver,
                                               std::bind(&AutoprobeController::checkUpdated,
                                                         this)));
            active.connections
                  .emplace_back(active.interface
                                      ->autoprobeStatusUpdated
                                      .connect(receiver,
                                               std::bind(&AutoprobeController::checkUpdated,
                                                         this)));

            active.interface->setControlStream(active.control.get());
            active.interface->start();
            active.interface->incomingTimeout(now);
        }

        pending.emplace_back(std::move(sortList.second));
    }
}

AutoprobeController::~AutoprobeController()
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        state = State::Stopping;
    }
    request.notify_all();
    if (thread.joinable())
        thread.join();
}

void AutoprobeController::checkUpdated()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        updateRequired = true;
    }
    request.notify_all();
}

std::unique_ptr<AutoprobeController::Result> AutoprobeController::takeResult()
{
    Q_ASSERT(state != State::Running);
    return std::unique_ptr<Result>(new Result(*this));
}

static bool checkMetadataMatch(const Variant::Read &matcher, const Variant::Read &parameters)
{
    switch (matcher.getType()) {
    case Variant::Type::Hash: {
        if (parameters.getType() != Variant::Type::Hash)
            return false;
        auto parameterHash = parameters.toHash();
        for (auto check : matcher.toHash()) {
            if (check.first.empty())
                continue;
            auto other = parameterHash.find(check.first);
            if (other == parameterHash.end())
                return false;
            /* No recursive checking for now */
            if (other.value() != check.second)
                return false;
        }
        break;
    }
    case Variant::Type::Empty:
        return true;
    default:
        return matcher == parameters;
    }
    return true;
}

bool AutoprobeController::reselectActive()
{
    if (selected.interface)
        return true;

    /* First, see if the current one has a final status and needs to be moved out */
    if (current.interface) {
        switch (current.interface->getAutoprobeStatus()) {
        case AcquisitionInterface::AutoprobeStatus::InProgress:
            break;
        case AcquisitionInterface::AutoprobeStatus::Success: {
            if (current.parameters.sourceParameters.read().exists()) {
                auto gotParameters = current.interface->getSourceMetadata();
                if (!checkMetadataMatch(current.parameters.sourceParameters, gotParameters)) {
                    qCDebug(logging)
                        << "Removing active autoprobe parameter mismatched component of type"
                        << current.parameters.componentName << ": looking for:"
                        << current.parameters.sourceParameters << "but the component has:"
                        << gotParameters;

                    /* This is a conclusive failure, so don't retry it anymore at all */
                    current.interface->signalTerminate();
                    current.interface->wait();
                    current.interface.reset();

                    current = Active();
                    stateChanged();
                    break;
                }
            }

            for (auto &c : current.connections) {
                c.disconnect();
            }
            current.connections.clear();
            auto save = std::move(current);
            current = Active();
            selected = std::move(save);
            selectedAtRealtime = Time::time();
            handoffControl.reset(new HandoffControl);
            selected.interface->setControlStream(handoffControl.get());
            handoffControl->attached(*interface, selected.control.get());
            selected.control.reset();

            qCDebug(logging) << "Autoprobe selected active component type"
                             << selected.parameters.componentName << "on"
                             << interface->description();
            return true;
        }
        case AcquisitionInterface::AutoprobeStatus::Failure: {

            {
                std::lock_guard<std::mutex> lock(current.control->mutex);
                current.control->enableWrite = false;
                current.control->sendData.clear();
                current.control->timeoutTime = FP::undefined();
            }

            if (current.tries >= current.parameters.maximumTries) {
                qCDebug(logging) << "Removing failed active autoprobe of type"
                                 << current.parameters.componentName;

                current.interface->signalTerminate();
                current.interface->wait();
                current.interface.reset();

                current = Active();
                stateChanged();
                break;
            }


            qCDebug(logging) << "Retrying autoprobe of type" << current.parameters.componentName
                             << "with" << current.tries << "attempt(s) completed";

            Q_ASSERT(!pending.empty());

            current.interface->autoprobeResetPassive(Time::time());

            auto &target = pending.front();
            if (target.empty()) {
                target.emplace_back(std::move(current));
            } else {
                /* Insert it in a random position, in case the try order matters */
                std::size_t
                        index = static_cast<std::size_t>(Random::integer()) % (target.size() + 1);
                target.emplace(target.begin() + index, std::move(current));
            }

            current = Active();
            stateChanged();
            break;
        }
        }
    }

    /* Purge failed and maybe select a new one to try */
    bool isFirstLevel = true;
    for (auto &level : pending) {
        for (auto check = level.begin(); check != level.end();) {
            switch (check->interface->getAutoprobeStatus()) {
            case AcquisitionInterface::AutoprobeStatus::InProgress:
                break;
            case AcquisitionInterface::AutoprobeStatus::Success: {
                /* Don't promote between levels */
                if (!isFirstLevel)
                    break;

                if (check->parameters.sourceParameters.read().exists()) {
                    auto gotParameters = check->interface->getSourceMetadata();
                    if (!checkMetadataMatch(check->parameters.sourceParameters, gotParameters)) {
                        /* If this isn't an interactive capable interface, then this
                         * failure is conclusive */
                        if (check->parameters.passive || !check->parameters.canChangeInterface) {
                            qCDebug(logging)
                                << "Removing pending autoprobe parameter mismatched component of type"
                                << current.parameters.componentName << ": looking for:"
                                << current.parameters.sourceParameters << "but the component has:"
                                << gotParameters;

                            check->interface->signalTerminate();
                            check->interface->wait();
                            check->interface.reset();

                            check = level.erase(check);

                            stateChanged();
                            continue;
                        }
                    }
                }

                /* Success reported, so try it next if we need an active one */
                if (!current.interface) {
                    activatePending(std::move(*check));
                    check = level.erase(check);
                    continue;
                }

                /* Or if both it and the current one are passive, then we know that
                 * we're going to select this one, since we don't get here if the
                 * current one has already succeeded */
                if (current.parameters.passive && check->parameters.passive) {
                    for (auto &c : check->connections) {
                        c.disconnect();
                    }
                    check->connections.clear();
                    auto save = std::move(*check);
                    selected = std::move(save);
                    selectedAtRealtime = Time::time();
                    handoffControl.reset(new HandoffControl);
                    selected.interface->setControlStream(handoffControl.get());
                    handoffControl->attached(*interface, selected.control.get());
                    selected.control.reset();

                    qCDebug(logging) << "Autoprobe selected pending component type"
                                     << selected.parameters.componentName << "on"
                                     << interface->description();
                    return true;
                }

                break;
            }
            case AcquisitionInterface::AutoprobeStatus::Failure:
                if (check->parameters.ignoreBackgroundFailure)
                    break;

                /* If this isn't an interactive capable interface, then this
                 * failure is conclusive */
                if (check->parameters.passive || !check->parameters.canChangeInterface) {
                    check->interface->signalTerminate();
                    check->interface->wait();
                    check->interface.reset();

                    qCDebug(logging) << "Removing failed pending autoprobe of type"
                                     << check->parameters.componentName;
                    check = level.erase(check);
                    stateChanged();
                    continue;
                }
                /* Otherwise, wait until it's got a chance to try full interactive mode */
                break;
            }

            ++check;
        }
        isFirstLevel = false;
    }

    /* No current one, so try one */
    if (!current.interface) {
        for (auto level = pending.begin(); level != pending.end();) {
            if (level->empty()) {
                level = pending.erase(level);
                continue;
            }

            Q_ASSERT(!level->empty());
            activatePending(std::move(level->front()));
            level->erase(level->begin());
            Q_ASSERT(current.interface);
            break;
        }
    }

    if (!current.interface) {
        Q_ASSERT(pending.empty());

        qCDebug(logging) << "All autoprobe candidates exhausted";
        return true;
    }

    return false;
}

void AutoprobeController::activatePending(Active &&pending)
{
    Q_ASSERT(!current.interface);
    current = std::move(pending);

    {
        std::lock_guard<std::mutex> lock(current.control->mutex);
        current.control->sendData.clear();
    }

    Variant::Read effectiveParameters;
    bool needToMerge = false;
    {
        std::vector<std::reference_wrapper<const Variant::Root>> mergeInput;
        auto existing = interface->configuration();
        if (existing.read().exists())
            mergeInput.emplace_back(existing);
        auto defaults = current.interface->getDefaults();
        if (defaults.interface.read().exists()) {
            mergeInput.emplace_back(defaults.interface);
            needToMerge = true;
        }
        if (current.parameters.interfaceParameters.read().exists()) {
            mergeInput.emplace_back(current.parameters.interfaceParameters);
            needToMerge = true;
        }
        effectiveParameters = Variant::Root::overlay(mergeInput.begin(), mergeInput.end()).read();
    }
    if (needToMerge && effectiveParameters.exists() && current.parameters.canChangeInterface) {
        auto m = IOTarget::create(effectiveParameters);
        if (m) {
            interface->merge(*m);
        }
    }

    /* If it was already passive, this won't reset it, so it may immediately
     * finish */
    if (!current.parameters.passive) {
        {
            std::lock_guard<std::mutex> lock(current.control->mutex);
            current.control->enableWrite = true;
        }

        current.interface->autoprobeTryInteractive(Time::time());
    } else {
        current.interface->autoprobeResetPassive(Time::time());
    }

    ++current.tries;

    Q_ASSERT(interface);
    Q_ASSERT(current.interface);
    if (!current.parameters.instrumentName.isEmpty()) {
        if (current.parameters.configuration.empty()) {
            qCDebug(logging) << "Starting autoprobe for type" << current.parameters.componentName
                             << "(" << current.parameters.instrumentName << ") on"
                             << interface->description();
        } else {
            qCDebug(logging) << "Starting autoprobe for type" << current.parameters.componentName
                             << "(explicit" << current.parameters.instrumentName << ") on"
                             << interface->description();
        }
    } else {
        if (current.parameters.configuration.empty()) {
            qCDebug(logging) << "Starting autoprobe for type" << current.parameters.componentName
                             << "on" << interface->description();
        } else {
            qCDebug(logging) << "Starting autoprobe for type" << current.parameters.componentName
                             << "(explicit) on" << interface->description();
        }
    }

    stateChanged();
    updateRequired = true;
}

void AutoprobeController::run()
{
    using Clock = std::chrono::steady_clock;

    interface->read.connect(receiver, std::bind(&AutoprobeController::checkUpdated, this));
    interface->otherControl.connect(receiver, std::bind(&AutoprobeController::checkUpdated, this));
    interface->start();

    qCDebug(logging) << "Autoprobe starting";

    std::unique_lock<std::mutex> lock(mutex);
    auto nextWake = Clock::now() + std::chrono::milliseconds(100);
    for (;;) {
        std::vector<std::function<void()>> toCall;
        if (!lock)
            lock.lock();
        if (state == State::Stopping)
            break;

        switch (state) {
        case State::Running:
            break;
        case State::Initialize:
        case State::Stopping:
        case State::Complete:
            Q_ASSERT(false);
            return;
        }

        toCall = std::move(runQueue);
        runQueue.clear();

        if (!updateRequired && toCall.empty() && nextWake > Clock::now()) {
            request.wait_until(lock, nextWake);
            continue;
        }
        updateRequired = false;

        lock.unlock();

        nextWake = Clock::now() + std::chrono::milliseconds(100);
        for (const auto &call : toCall) {
            call();
        }

        if (reselectActive())
            break;

        Q_ASSERT(current.interface);
        Q_ASSERT(interface);
        double tNow = Time::time();

        Util::ByteArray sendData;
        if (current.control) {
            std::lock_guard<std::mutex> controlLock(current.control->mutex);
            sendData = std::move(current.control->sendData);
            current.control->sendData.clear();
        }

        if (!sendData.empty()) {
            interface->write(sendData);
            for (const auto &level : pending) {
                for (const auto &forward : level) {
                    forward.interface->incomingControl(sendData, tNow);
                }
            }
        }

        current.receiver->flush(tNow);
        for (const auto &level : pending) {
            for (const auto &forward : level) {
                forward.receiver->flush(tNow);
            }
        }

        if (current.control) {
            std::unique_lock<std::mutex> controlLock(current.control->mutex);
            if (FP::defined(current.control->timeoutTime) && current.control->timeoutTime <= tNow) {
                current.control->timeoutTime = FP::undefined();
                controlLock.unlock();
                current.interface->incomingTimeout(tNow);
            }
        }
        for (const auto &level : pending) {
            for (const auto &forward : level) {
                if (!forward.control)
                    continue;
                std::unique_lock<std::mutex> controlLock(forward.control->mutex);
                if (FP::defined(forward.control->timeoutTime) &&
                        forward.control->timeoutTime <= tNow) {
                    forward.control->timeoutTime = FP::undefined();
                    controlLock.unlock();
                    forward.interface->incomingTimeout(tNow);
                }
            }
        }

        current.interface->incomingAdvance(tNow);
        for (const auto &level : pending) {
            for (const auto &forward : level) {
                forward.interface->incomingAdvance(tNow);
            }
        }

        double controlWake = FP::undefined();
        if (current.control) {
            std::lock_guard<std::mutex> controlLock(current.control->mutex);
            if (FP::defined(current.control->timeoutTime)) {
                if (!FP::defined(controlWake) || controlWake > current.control->timeoutTime)
                    controlWake = current.control->timeoutTime;
            }
        }
        for (const auto &level : pending) {
            for (const auto &forward : level) {
                if (!forward.control)
                    continue;
                std::lock_guard<std::mutex> controlLock(forward.control->mutex);
                if (FP::defined(forward.control->timeoutTime)) {
                    if (!FP::defined(controlWake) || controlWake > forward.control->timeoutTime)
                        controlWake = forward.control->timeoutTime;
                }
            }
        }

        if (FP::defined(controlWake)) {
            int millisecondsToWake = static_cast<int>(std::ceil(controlWake - tNow));
            if (millisecondsToWake < 1)
                millisecondsToWake = 1;
            auto clockWake = Clock::now() + std::chrono::milliseconds(millisecondsToWake);
            if (clockWake < nextWake)
                nextWake = clockWake;
        }
    }
    if (!lock)
        lock.lock();
    state = State::Complete;
    runQueue.clear();
    lock.unlock();

    forAllAcquisitionInterfaces([](AcquisitionInterface *interface) {
        interface->signalTerminate();
    });
    forAllAcquisitionInterfaces([](AcquisitionInterface *interface) {
        interface->wait();
    });

    current.interface.reset();
    current.control.reset();
    for (auto &level : pending) {
        for (auto &p : level) {
            p.receiver.reset();
            p.interface.reset();
            p.control.reset();
        }
    }
    pending.clear();


    qCDebug(logging) << "Autoprobe complete";
    response.notify_all();
    stateChanged();
    completed();
}


void AutoprobeController::start()
{
    if (pending.empty()) {
        if (interface) {
            qCDebug(logging) << "No possible autoprobe candidates";
        }
        {
            std::lock_guard<std::mutex> lock(mutex);
            state = State::Complete;
        }
        response.notify_all();
        stateChanged();
        completed();
        return;
    }
    Q_ASSERT(interface);

    {
        std::lock_guard<std::mutex> lock(mutex);
        switch (state) {
        case State::Initialize:
            state = State::Running;
            break;
        case State::Running:
        case State::Complete:
            Q_ASSERT(false);
            break;
        case State::Stopping:
            break;
        }
    }
    thread = std::thread(std::bind(&AutoprobeController::run, this));
}

void AutoprobeController::removeInstrumentName(const QString &name)
{
    std::lock_guard<std::mutex> lock(mutex);
    runQueue.emplace_back([this, name] {
        if (selected.interface)
            return;

        if (current.interface && current.parameters.instrumentName == name) {
            {
                std::lock_guard<std::mutex> lock(current.control->mutex);
                current.control->enableWrite = false;
                current.control->sendData.clear();
                current.control->timeoutTime = FP::undefined();
            }

            qCDebug(logging) << "Removing duplicated active autoprobe of type"
                             << current.parameters.componentName << "as" << name;

            current.interface->signalTerminate();
            current.interface->wait();
            current.interface.reset();

            current = Active();
            stateChanged();
        }

        /* Don't remove levels, since that's only safe in the update process, since the
         * current one may still be active */
        for (auto &level : pending) {
            for (auto check = level.begin(); check != level.end();) {
                if (!check->interface || check->parameters.instrumentName != name) {
                    ++check;
                    continue;
                }

                check->interface->signalTerminate();
                check->interface->wait();
                check->interface.reset();

                qCDebug(logging) << "Removing duplicated pending autoprobe of type"
                                 << check->parameters.componentName << "as" << name;
                check = level.erase(check);

                stateChanged();
            }
        }
    });
}

void AutoprobeController::Active::describe(Data::Variant::Write &&target) const
{
    target["Component"].setString(parameters.componentName);
    if (!parameters.instrumentName.isEmpty())
        target["Instrument"].setString(parameters.instrumentName);
    target["Try"].setInteger(tries);
    target["MaximumTries"].setInteger(parameters.maximumTries);
}

Data::Variant::Root AutoprobeController::describeState()
{
    Data::Variant::Root result(Variant::Type::Hash);

    std::unique_lock<std::mutex> lock(mutex);
    if (state == State::Running) {
        bool complete = false;
        runQueue.emplace_back([this, &result, &complete] {
            if (interface) {
                result["Name"].setString(interface->description(true));
                result["Interface"].set(interface->configuration());
            }
            if (selected.interface) {
                selected.describe(result["Selected"]);
            }
            if (current.interface) {
                current.describe(result["Current"]);
            }
            for (const auto &level : pending) {
                for (const auto &p : level) {
                    if (!p.interface)
                        continue;
                    p.describe(result["Pending"].toArray().after_back());
                }
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                complete = true;
            }
            response.notify_all();
        });
        request.notify_all();
        response.wait(lock, [this, &complete] { return complete || state != State::Running; });
    }
    if (state != State::Complete)
        return result;
    lock.unlock();

    if (interface) {
        result["Name"].setString(interface->description(true));
        result["Interface"].set(interface->configuration());
    }
    if (selected.interface) {
        selected.describe(result["Selected"]);
    }
    return result;
}

AutoprobeController::ControlInterface::ControlInterface(AutoprobeController *p) : parent(p),
                                                                                  enableWrite(
                                                                                          false),
                                                                                  mutex(),
                                                                                  sendData(),
                                                                                  timeoutTime(
                                                                                          FP::undefined())
{ }

AutoprobeController::ControlInterface::~ControlInterface() = default;

void AutoprobeController::ControlInterface::writeControl(const Util::ByteView &data)
{
    Q_ASSERT(parent->interface);
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (!enableWrite)
            return;
        sendData += data;
    }
    parent->checkUpdated();
}

void AutoprobeController::ControlInterface::resetControl()
{
    /* Ignore this, since the failing ones will do it before we disconnect them
     * in general */
}

void AutoprobeController::ControlInterface::requestTimeout(double time)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        timeoutTime = time;
    }
    parent->checkUpdated();
}

AutoprobeController::HandoffControl::HandoffControl() : resetPending(false), writePending()
{ }

AutoprobeController::HandoffControl::~HandoffControl() = default;

void AutoprobeController::HandoffControl::writeControl(const Util::ByteView &data)
{
    if (resetPending)
        return;
    writePending += data;
}

void AutoprobeController::HandoffControl::resetControl()
{
    resetPending = true;
    writePending.clear();
}

void AutoprobeController::HandoffControl::requestTimeout(double)
{
    /*
     * Ignored, since the promote requires the target to resend this.
     */
}

void AutoprobeController::HandoffControl::attached(IOInterface &target, ControlInterface *original)
{
    if (original) {
        /* Flush any old writes, since we just buffer them now */
        Util::ByteArray sendData;
        {
            std::lock_guard<std::mutex> lock(original->mutex);
            sendData = std::move(original->sendData);
            original->sendData.clear();
        }
        if (!sendData.empty()) {
            target.write(std::move(sendData));
        }
    }
}

bool AutoprobeController::HandoffControl::handoff(IOInterface &target)
{
    /* Safe without a mutex, because this should never be called while the handoff is still
     * attached to the acquisition */

    if (resetPending) {
        target.reset();
        return false;
    }

    if (!writePending.empty()) {
        target.write(std::move(writePending));
    }
    return true;
}

AutoprobeController::Result::Result(AutoprobeController &controller) : receiver(
        std::move(controller.selected.receiver)),
                                                                       control(std::move(
                                                                               controller.handoffControl)),
                                                                       handoffAcquisition(controller
                                                                                                  .selected
                                                                                                  .interface
                                                                                                  .get()),
                                                                       handoffInterface(
                                                                               controller.interface
                                                                                         .get()),
                                                                       acquisition(std::move(
                                                                               controller.selected
                                                                                         .interface)),
                                                                       interface(std::move(
                                                                               controller.interface)),
                                                                       activationTime(
                                                                               controller.selectedAtRealtime),
                                                                       isPassive(controller.selected
                                                                                           .parameters
                                                                                           .passive),
                                                                       componentName(std::move(
                                                                               controller.selected
                                                                                         .parameters
                                                                                         .componentName)),
                                                                       instrumentName(std::move(
                                                                               controller.selected
                                                                                         .parameters
                                                                                         .instrumentName)),
                                                                       configuration(std::move(
                                                                               controller.selected
                                                                                         .parameters
                                                                                         .configuration))
{
    Q_ASSERT(controller.state != State::Running);
}

AutoprobeController::Result::~Result()
{
    if (interface) {
        qWarning() << "Handoff not completed for acquisition autoprobe";
    }
}

void AutoprobeController::Result::handoff(double time,
                                          AcquisitionControlStream *cs,
                                          const std::function<void()> &connect)
{
    Q_ASSERT(receiver.get() != nullptr);
    Q_ASSERT(handoffAcquisition != nullptr);
    Q_ASSERT(handoffInterface != nullptr);

    handoffAcquisition->setControlStream(cs);
    if (isPassive || control->handoff(*handoffInterface)) {
        receiver->handoff(time, connect);
    } else {
        receiver->discard(time, connect);
    }
    handoffAcquisition = nullptr;
    handoffInterface = nullptr;
}

bool AutoprobeController::isActive() const
{
    std::lock_guard<std::mutex> lock(const_cast<AutoprobeController *>(this)->mutex);
    switch (state) {
    case State::Initialize:
    case State::Running:
        return true;
    case State::Stopping:
    case State::Complete:
        return false;
    }
    Q_ASSERT(false);
    return false;
}

double AutoprobeController::getActivationTime() const
{
    Q_ASSERT(state != State::Running);
    return selectedAtRealtime;
}

void AutoprobeController::stop()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        switch (state) {
        case State::Initialize:
        case State::Complete:
            return;
        case State::Running:
            state = State::Stopping;
            break;
        case State::Stopping:
            break;
        }
    }
    request.notify_all();
    {
        std::unique_lock<std::mutex> lock(mutex);
        response.wait(lock, [this] { return state == State::Complete; });
    }
}

AutoprobeController::DataReceiver::DataReceiver(IOInterface &from, AcquisitionInterface &to)
        : isDisconnected(false)
{
    AcquisitionInterface *target = &to;
    AcquisitionInterface::IncomingDataType type = AcquisitionInterface::IncomingDataType::Stream;
    if (from.integratedFraming())
        type = AcquisitionInterface::IncomingDataType::Complete;
    from.read.connect(*this, [this, target, type](const Util::ByteArray &data) {
        std::lock_guard<std::mutex> lock(mutex);
        if (isDisconnected)
            return;
        queued.emplace_back([this, data, target, type] {
            target->incomingData(data, time, type);
        });
    });
    from.otherControl.connect(*this, [this, target, type](const Util::ByteArray &data) {
        std::lock_guard<std::mutex> lock(mutex);
        if (isDisconnected)
            return;
        queued.emplace_back([this, data, target, type] {
            target->incomingControl(data, time, type);
        });
    });
}

AutoprobeController::DataReceiver::~DataReceiver()
{
    disconnect();
}

void AutoprobeController::DataReceiver::discard(double, const std::function<void()> &connect)
{
    /*
     * Rely on the fact that signal connects make copies to do the write, so we
     * can block even if the signal is currently activating.  This prevents the
     * IO thread from getting more reads until the new connection has put itself in.
     */
    {
        std::lock_guard<std::mutex> lock(mutex);
        isDisconnected = true;
        connect();
    }
    disconnectImmediate();

    queued.clear();
}

void AutoprobeController::DataReceiver::handoff(double now, const std::function<void()> &connect)
{
    /*
     * Rely on the fact that signal connects make copies to do the write, so we
     * can block even if the signal is currently activating.  This prevents the
     * IO thread from getting more reads until the new connection has put itself in.
     */
    {
        std::lock_guard<std::mutex> lock(mutex);
        isDisconnected = true;
        connect();
    }
    disconnectImmediate();

    time = now;
    for (const auto &c : queued) {
        c();
    }
    queued.clear();
}

void AutoprobeController::DataReceiver::flush(double now)
{
    std::vector<std::function<void()>> pending;
    {
        std::lock_guard<std::mutex> lock(mutex);
        pending = std::move(queued);
        queued.clear();
    }

    time = now;
    for (const auto &c : pending) {
        c();
    }
}


}
}
