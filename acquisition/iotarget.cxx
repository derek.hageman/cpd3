/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <cstdint>
#include <algorithm>
#include <mutex>
#include <condition_variable>
#include <QLoggingCategory>
#include <QCryptographicHash>
#include <QRegularExpression>
#include <QFileInfo>

#if CPD3_CXX >= 201700L

#include <filesystem>

#elif defined(Q_OS_UNIX)
#include <limits.h>
#include <unistd.h>
#endif

#include "iotarget.hxx"
#include "iointerface.hxx"
#include "algorithms/cryptography.hxx"
#include "io/drivers/serial.hxx"
#include "io/drivers/localsocket.hxx"
#include "io/drivers/unixsocket.hxx"
#include "io/drivers/udp.hxx"
#include "io/drivers/file.hxx"
#include "io/drivers/url.hxx"
#include "io/process.hxx"

Q_LOGGING_CATEGORY(log_acquisition_iotarget, "cpd3.acquisition.iotarget", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace Acquisition {

IOTarget::Device::Device() = default;

IOTarget::Device::~Device() = default;

bool IOTarget::Device::isEnded() const
{ return false; }

void IOTarget::Device::start()
{ }

void IOTarget::Device::reset()
{ }

IOTarget::IOTarget() : localSocket(new IO::Socket::Local::Configuration)
{ }

static bool checkCertificate(const Variant::Read &auth,
                             const QSslCertificate &cert,
                             bool defaultResult = true)
{
    if (!auth.exists())
        return defaultResult;

    switch (auth.getType()) {
    case Variant::Type::Hash: {
        QString key(Algorithms::Cryptography::sha512(cert).toHex().toLower());
        QSslCertificate check(Algorithms::Cryptography::getCertificate(auth.hash(key), false));
        if (check.isNull())
            return false;
        return check == cert;
    }

    case Variant::Type::Array:
        for (auto raw : auth.toArray()) {
            QSslCertificate check(Algorithms::Cryptography::getCertificate(raw, false));
            if (check.isNull())
                continue;
            if (check == cert)
                return true;
        }
        return false;

    default: {
        QSslCertificate check(Algorithms::Cryptography::getCertificate(auth, false));
        if (check.isNull())
            return false;
        return check == cert;
    }
    }

    Q_ASSERT(false);
    return false;
}

static std::function<bool(QSslSocket &socket)> toTLSAccept(const Variant::Read &configuration)
{
    if (!configuration.exists())
        return {};

    Variant::Root ssl(configuration);
    return [ssl](QSslSocket &socket) -> bool {
        QObject::connect(&socket, &QSslSocket::encrypted, [&socket, ssl] {
            if (!checkCertificate(ssl["Authorization"], socket.peerCertificate())) {
                qCDebug(log_acquisition_iotarget) << "Disconnecting unauthorized TLS peer"
                                                  << socket.peerAddress();
                socket.disconnectFromHost();
                socket.close();
                socket.readAll();
                return;
            }
        });
        return Algorithms::Cryptography::setupSocket(&socket, ssl.read());
    };
}

IOTarget::IOTarget(const Variant::Read &configuration)
{
    if (!configuration["DisableLocal"].toBool()) {
        localSocket.reset(new IO::Socket::Local::Configuration);
    }

    auto ext = configuration["External"];
    if (ext.exists()) {
        tcpSocket.reset(new IO::Socket::TCP::Configuration);
        auto port = ext["Port"].toInteger();
        if (INTEGER::defined(port) && port > 0 && port < 65536) {
            tcpSocket->port = port;
        }
        tcpSocket->tls = toTLSAccept(ext["SSL"]);
        if (ext["ListenAddress"].exists()) {
            Util::append(ext["ListenAddress"].toChildren().keys(), tcpListen);
        }
    }
}

IOTarget::IOTarget(const IOTarget &other) : tcpListen(other.tcpListen)
{
    if (other.localSocket) {
        localSocket.reset(new IO::Socket::Local::Configuration(*other.localSocket));
    }
    if (other.tcpSocket) {
        tcpSocket.reset(new IO::Socket::TCP::Configuration(*other.tcpSocket));
    }
}

IOTarget::~IOTarget() = default;

bool IOTarget::isValid() const
{ return true; }

bool IOTarget::triggerAutoprobeWhenClosed() const
{ return false; }

bool IOTarget::integratedFraming() const
{ return false; }

void IOTarget::merge(const IOTarget &, Device *)
{ }

static std::string toLocalConnection(const Util::ByteView &uid)
{
    if (uid.empty())
        return {};

    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(uid.toQByteArrayRef());
    auto bytes = hash.result();

    static const char digits[33] = "0123456789abcdefghijklmnopqrstuv";

    std::string result;
    for (const char *b = bytes.constData(), *endB = b + bytes.size(); b != endB; ++b) {
        result.push_back(digits[(static_cast<std::uint8_t>(*b)) >> 3U]);
    }
    if (result.size() > 16) {
        result.resize(16);
    }
    return "CPD3COMMS-" + result;
}

std::unique_ptr<IO::Socket::Connection> IOTarget::clientConnection(bool) const
{
    auto target = toLocalConnection(localSocketUID());
    if (target.empty())
        return {};
    return IO::Socket::Local::connect(target, {}, false);
}

bool IOTarget::spawnServer() const
{ return true; }

std::vector<std::unique_ptr<
        IO::Socket::Server>> IOTarget::serverListeners(const IO::Socket::Server::IncomingConnection &connection,
                                                       bool removeLocal) const
{
    std::vector<std::unique_ptr<IO::Socket::Server>> result;
    if (localSocket) {
        auto target = toLocalConnection(localSocketUID());
        if (!target.empty()) {
            std::unique_ptr<IO::Socket::Local::Server>
                    server(new IO::Socket::Local::Server(connection, target));
            if (!server->startListening(removeLocal)) {
                qCDebug(log_acquisition_iotarget) << description()
                                                  << "failed to listen on local socket";
                return {};
            }
            qCDebug(log_acquisition_iotarget) << description() << "listening on local socket"
                                              << server->describeServer();
            result.emplace_back(std::move(server));
        }
    }
    if (tcpSocket) {
        std::unique_ptr<IO::Socket::TCP::Server> server;
        if (tcpListen.empty()) {
            server.reset(new IO::Socket::TCP::Server(connection, *tcpSocket));
        } else {
            server.reset(new IO::Socket::TCP::Server(connection, tcpListen, *tcpSocket));
        }
        if (server->startListening()) {
            qCDebug(log_acquisition_iotarget) << description() << "listening on TCP socket"
                                              << server->describeServer();
            result.emplace_back(std::move(server));
        }
    }
    return std::move(result);
}

#if CPD3_CXX >= 201700L

static std::string canonicalPath(const std::string &path)
{
    if (path.empty())
        return {};
    try {
        auto result = std::filesystem::canonical(path).string();
        if (!result.empty())
            return result;
    } catch (std::exception &) {
    }
    return path;
}

#elif defined(Q_OS_UNIX)

static std::string canonicalPath(const std::string &path)
{
    if (path.empty())
        return {};
    char buffer[PATH_MAX+1];
    if (::realpath(path.c_str(), buffer) == nullptr)
        return path;
    buffer[PATH_MAX] = '\0';
    auto result = std::string(buffer);
    if (result.empty())
        return path;
    return result;
}
#else

static std::string canonicalPath(const std::string &path)
{
    if (path.empty())
        return {};
    auto result = QFileInfo(QString::fromStdString(path)).canonicalFilePath().toStdString();
    if (result.empty())
        return path;
    return result;
}

#endif

namespace {

enum class SerializationID : std::uint8_t {
    Serial, TCPSocket, LocalSocket, UnixSocket, UDP, TCPListen, LocalListen, UnixListen, FileStream,
    Process,
    Multiplexer,
    URL,
};

class Target_Remote : public IOTarget {
    std::string remoteHost;
    IO::Socket::TCP::Configuration remoteConnection;
public:
    Target_Remote() = delete;

    explicit Target_Remote(const Variant::Read &configuration) : IOTarget(configuration),
                                                                 remoteHost(
                                                                         configuration["Server"].toString())
    {
        auto port = configuration["Port"].toInteger();
        if (INTEGER::defined(port) && port > 0 && port < 65536) {
            remoteConnection.port = port;
        }
        remoteConnection.tls = toTLSAccept(configuration["SSL"]);
    }

    std::unique_ptr<IO::Socket::Connection> clientConnection(bool localOnly) const override
    {
        if (localOnly)
            return {};
        if (remoteHost.empty()) {
            return IO::Socket::TCP::loopback(remoteConnection, false);
        }
        return IO::Socket::TCP::connect(remoteHost, remoteConnection, false);
    }

    bool spawnServer() const override
    { return false; }

    bool matchesDevice(const IOTarget &other) const override
    {
        auto target = dynamic_cast<const Target_Remote *>(&other);
        if (!target)
            return false;
        return remoteHost == target->remoteHost &&
                remoteConnection.port == target->remoteConnection.port;
    }

    Util::ByteArray serialize() const override
    { return {}; }

    std::unique_ptr<Device> backingDevice() const override
    { return {}; }

    QString description(bool = false) const override
    {
        QString result = QString::fromStdString(remoteHost);
        if (result.contains(":"))
            result = "[" + result + "]";
        result += ":";
        result += QString::number(remoteConnection.port);
        return QObject::tr("CPD3=%1", "remote CPD3 description").arg(result);
    }

    Variant::Root configuration(bool = false) const override
    {
        Variant::Root result;
        result["Type"] = "CPD3Server";
        if (!remoteHost.empty()) {
            result["Server"] = remoteHost;
        }
        result["ServerPort"].setInteger(remoteConnection.port);
        return result;
    }

    std::unique_ptr<IOTarget> clone(bool = false) const override
    { return std::unique_ptr<IOTarget>(new Target_Remote(*this)); }

    bool isValid() const override
    { return remoteConnection.port != remoteConnection.port_any; }

protected:
    Target_Remote(const Target_Remote &other) = default;

    Util::ByteArray localSocketUID() const override
    { return {}; }
};

class StreamWrapperDevice : public IOTarget::Device {
    std::unique_ptr<IO::Generic::Stream> stream;
protected:
    inline IO::Generic::Stream &access()
    { return *stream; }

    inline const IO::Generic::Stream &access() const
    { return *stream; }

public:
    explicit StreamWrapperDevice(std::unique_ptr<IO::Generic::Stream> &&st) : stream(std::move(st))
    {
        read = stream->read;
        ended = stream->ended;
        writeStall = stream->writeStall;
    }

    virtual ~StreamWrapperDevice() = default;

    bool isEnded() const override
    { return stream->isEnded(); }

    void start() override
    { return stream->start(); }

    void write(const Util::ByteView &data) override
    { return stream->write(data); }

    void write(const Util::ByteArray &data) override
    { return stream->write(data); }

    void write(Util::ByteArray &&data) override
    { return stream->write(std::move(data)); }

    void write(const QByteArray &data) override
    { return stream->write(data); }

    void write(QByteArray &&data) override
    { return stream->write(std::move(data)); }
};

class Target_Serial : public IOTarget {
    std::string device;
    std::string eavesdropper;
    std::pair<int, bool> baud{9600, false};
    std::pair<int, bool> dataBits{8, false};
    std::pair<int, bool> stopBits{1, false};
    std::pair<IO::Serial::Stream::Parity, bool> parity{IO::Serial::Stream::Parity::None, false};

    struct SoftwareFlowControl {
        IO::Serial::Stream::SoftwareFlowControl
                control = IO::Serial::Stream::SoftwareFlowControl::Disable;
        bool enableResume = false;

        bool operator==(const SoftwareFlowControl &other) const
        { return control == other.control && enableResume == other.enableResume; }

        bool operator!=(const SoftwareFlowControl &other) const
        { return control != other.control || enableResume != other.enableResume; }
    };

    std::pair<SoftwareFlowControl, bool> softwareFlowControl{SoftwareFlowControl(), false};
    std::pair<bool, bool> disableBlank{false, false};

    struct PulseControl {
        double width = 0.05;
        bool enable = false;
        bool invert = false;

        PulseControl() = default;

        PulseControl(const Variant::Read &configuration) : width(configuration["Width"].toReal()),
                                                           enable(configuration["Enable"].toBoolean()),
                                                           invert(configuration["Invert"].toBoolean())
        { }

        void configure(Variant::Write &&target) const
        {
            target["Width"].setReal(width);
            target["Enable"].setBoolean(enable);
            target["Invert"].setBoolean(invert);
        }

        bool operator==(const PulseControl &other) const
        { return width == other.width && enable == other.enable && invert == other.invert; }

        bool operator!=(const PulseControl &other) const
        { return width != other.width || enable != other.enable || invert != other.invert; }
    };

    std::pair<PulseControl, bool> rtsPulse{PulseControl(), false};
    std::pair<PulseControl, bool> dtrPulse{PulseControl(), false};


    enum class HardwareFlowControlType : std::uint8_t {
        Unset, None, Standard, ExplicitRTS
    };

    class HardwareFlowControl {
    public:
        HardwareFlowControl() = delete;

        HardwareFlowControl(const HardwareFlowControl &) = default;

        virtual ~HardwareFlowControl() = default;

        explicit HardwareFlowControl(const Variant::Read &)
        { }

        explicit HardwareFlowControl(QDataStream &)
        { }

        virtual void serialize(QDataStream &stream) const = 0;

        virtual std::unique_ptr<HardwareFlowControl> clone() const = 0;

        virtual bool equals(const HardwareFlowControl &other) const = 0;

        virtual bool apply(IO::Serial::Stream &stream) const = 0;

        virtual void configure(Variant::Write &&target) const = 0;
    };

    class HardwareFlowControl_None : public HardwareFlowControl {
    public:
        HardwareFlowControl_None() = delete;

        HardwareFlowControl_None(const HardwareFlowControl_None &) = default;

        virtual ~HardwareFlowControl_None() = default;

        explicit HardwareFlowControl_None(const Variant::Read &configuration) : HardwareFlowControl(
                configuration)
        { }

        explicit HardwareFlowControl_None(QDataStream &deserialize) : HardwareFlowControl(
                deserialize)
        { }

        void serialize(QDataStream &stream) const override
        { stream << static_cast<quint8>(HardwareFlowControlType::None); }

        std::unique_ptr<HardwareFlowControl> clone() const override
        { return std::unique_ptr<HardwareFlowControl>(new HardwareFlowControl_None(*this)); }

        bool equals(const HardwareFlowControl &other) const override
        { return dynamic_cast<const HardwareFlowControl_None *>(&other) != nullptr; }

        bool apply(IO::Serial::Stream &stream) const override
        { return stream.setNoHardwareFlowControl(); }

        void configure(Variant::Write &&target) const override
        { target["HardwareFlowControl"].setString("None"); }
    };

    class HardwareFlowControl_Standard : public HardwareFlowControl {
    public:
        HardwareFlowControl_Standard() = delete;

        HardwareFlowControl_Standard(const HardwareFlowControl_Standard &) = default;

        virtual ~HardwareFlowControl_Standard() = default;

        explicit HardwareFlowControl_Standard(const Variant::Read &configuration)
                : HardwareFlowControl(configuration)
        { }

        explicit HardwareFlowControl_Standard(QDataStream &deserialize) : HardwareFlowControl(
                deserialize)
        { }

        void serialize(QDataStream &stream) const override
        { stream << static_cast<quint8>(HardwareFlowControlType::Standard); }

        std::unique_ptr<HardwareFlowControl> clone() const override
        { return std::unique_ptr<HardwareFlowControl>(new HardwareFlowControl_Standard(*this)); }

        bool equals(const HardwareFlowControl &other) const override
        { return dynamic_cast<const HardwareFlowControl_Standard *>(&other) != nullptr; }

        bool apply(IO::Serial::Stream &stream) const override
        { return stream.setRS232StandardFlowControl(); }

        void configure(Variant::Write &&target) const override
        { target["HardwareFlowControl"].setString("Standard"); }
    };

    class HardwareFlowControl_ExplicitRTS : public HardwareFlowControl {
        double blankAfter = 0.0;
        double blankPerCharacter = 0.0;
        bool discardReceive = false;
        bool invert = false;
    public:
        HardwareFlowControl_ExplicitRTS() = delete;

        HardwareFlowControl_ExplicitRTS(const HardwareFlowControl_ExplicitRTS &) = default;

        virtual ~HardwareFlowControl_ExplicitRTS() = default;

        explicit HardwareFlowControl_ExplicitRTS(const Variant::Read &configuration)
                : HardwareFlowControl(configuration)
        {
            auto rts = configuration["RTS"];

            double v = rts["BlankAfter"].toReal();
            if (FP::defined(v))
                blankAfter = v;

            v = rts["BlankPerCharacter"].toReal();
            if (FP::defined(v))
                blankPerCharacter = v;

            if (rts["DiscardReceive"].exists())
                discardReceive = rts["DiscardReceive"].toBool();

            if (rts["Invert"].exists())
                invert = rts["Invert"].toBool();
        }

        explicit HardwareFlowControl_ExplicitRTS(QDataStream &deserialize) : HardwareFlowControl(
                deserialize)
        {
            deserialize >> blankAfter >> blankPerCharacter >> discardReceive >> invert;
        }

        void serialize(QDataStream &stream) const override
        {
            stream << static_cast<quint8>(HardwareFlowControlType::ExplicitRTS);
            stream << blankAfter << blankPerCharacter << discardReceive << invert;
        }

        std::unique_ptr<HardwareFlowControl> clone() const override
        { return std::unique_ptr<HardwareFlowControl>(new HardwareFlowControl_ExplicitRTS(*this)); }

        bool equals(const HardwareFlowControl &other) const override
        {
            auto rts = dynamic_cast<const HardwareFlowControl_ExplicitRTS *>(&other);
            if (!rts)
                return false;
            return blankAfter == rts->blankAfter &&
                    blankPerCharacter == rts->blankPerCharacter &&
                    discardReceive == rts->discardReceive &&
                    invert == rts->invert;
        }

        bool apply(IO::Serial::Stream &stream) const override
        {
            return stream.setExplicitRTSOnTransmit(blankAfter, blankPerCharacter, discardReceive,
                                                   invert);
        }

        void configure(Variant::Write &&target) const override
        {
            target["HardwareFlowControl"].setString("Explicit");
            target["RTS/BlankAfter"].setReal(blankAfter);
            target["RTS/BlankPerCharacter"].setReal(blankPerCharacter);
            target["RTS/DiscardReceive"].setBoolean(discardReceive);
            target["RTS/Invert"].setBoolean(invert);
        }
    };

    std::unique_ptr<HardwareFlowControl> hardwareFlowControl;

    class Device : public IOTarget::Device {
    public:
        std::unique_ptr<IO::Serial::Stream> dataStream;
        std::unique_ptr<IO::Serial::Stream> controlStream;

        bool disableBlank = false;
        PulseControl rtsPulse;
        PulseControl dtrPulse;

        Device() = delete;

        explicit Device(std::string port) : dataStream(
                static_cast<IO::Serial::Stream *>(IO::Access::serial(std::move(port))->stream()
                                                                                     .release()))
        {
            if (dataStream) {
                read = dataStream->read;
                ended = dataStream->ended;
                writeStall = dataStream->writeStall;
            }
        }

        virtual ~Device() = default;

        bool isEnded() const override
        {
            if (dataStream) {
                if (dataStream->isEnded())
                    return true;
            }
            return false;
        }

        void start() override
        {
            if (dataStream) {
                dataStream->start();
            }
            if (controlStream) {
                controlStream->start();
            }
        }

        void reset() override
        {
            if (rtsPulse.enable) {
                if (dataStream) {
                    if (!dataStream->pulseRTS(rtsPulse.width, rtsPulse.invert)) {
                        qCInfo(log_acquisition_iotarget) << "Error sending RTS pulse on"
                                                         << dataStream->describePort() << ":"
                                                         << dataStream->describeError();
                    }
                }
                if (controlStream) {
                    if (!controlStream->pulseRTS(rtsPulse.width, rtsPulse.invert)) {
                        qCInfo(log_acquisition_iotarget) << "Error sending control RTS pulse on"
                                                         << controlStream->describePort() << ":"
                                                         << controlStream->describeError();
                    }
                }
            }
            if (dtrPulse.enable) {
                if (dataStream) {
                    if (!dataStream->pulseDTR(dtrPulse.width, dtrPulse.invert)) {
                        qCInfo(log_acquisition_iotarget) << "Error sending DTR pulse on"
                                                         << dataStream->describePort() << ":"
                                                         << dataStream->describeError();
                    }
                }
                if (controlStream) {
                    if (!controlStream->pulseDTR(dtrPulse.width, dtrPulse.invert)) {
                        qCInfo(log_acquisition_iotarget) << "Error sending control DTS pulse on"
                                                         << controlStream->describePort() << ":"
                                                         << controlStream->describeError();
                    }
                }
            }

            if (dataStream) {
                if (!dataStream->flushPort(!disableBlank)) {
                    qCInfo(log_acquisition_iotarget) << "Error flushing"
                                                     << dataStream->describePort() << ":"
                                                     << dataStream->describeError();
                }
            }
            if (controlStream) {
                if (!controlStream->flushPort(!disableBlank)) {
                    qCInfo(log_acquisition_iotarget) << "Error flushing control"
                                                     << controlStream->describePort() << ":"
                                                     << controlStream->describeError();
                }
            }
        }

        void write(const Util::ByteView &data) override
        {
            if (controlStream) {
                controlStream->write(data);
            }
            if (dataStream) {
                dataStream->write(data);
            }
        }

        void write(const Util::ByteArray &data) override
        {
            if (controlStream) {
                controlStream->write(data);
            }
            if (dataStream) {
                dataStream->write(data);
            }
        }

        void write(Util::ByteArray &&data) override
        {
            if (controlStream) {
                controlStream->write(data);
            }
            if (dataStream) {
                dataStream->write(std::move(data));
            }
        }

        void write(const QByteArray &data) override
        {
            if (controlStream) {
                controlStream->write(data);
            }
            if (dataStream) {
                dataStream->write(data);
            }
        }

        void write(QByteArray &&data) override
        {
            if (controlStream) {
                controlStream->write(data);
            }
            if (dataStream) {
                dataStream->write(std::move(data));
            }
        }
    };

    Target_Serial() = default;

    friend class IOTarget;

public:
    explicit Target_Serial(const Variant::Read &configuration) : IOTarget(configuration),
                                                                 device(configuration["Port"].toString()),
                                                                 eavesdropper(
                                                                         configuration["Eavesdropper"]
                                                                                 .toString())
    {
        if (!configuration["Baud"].exists()) {
            baud.second = true;
        } else {
            auto i = configuration["Baud"].toInteger();
            if (INTEGER::defined(i) && i > 0) {
                baud.first = static_cast<int>(i);
                baud.second = true;
            }
        }
        if (!configuration["DataBits"].exists()) {
            dataBits.second = true;
        } else {
            auto i = configuration["DataBits"].toInteger();
            if (INTEGER::defined(i) && i > 0) {
                dataBits.first = static_cast<int>(i);
                dataBits.second = true;
            }
        }
        if (!configuration["StopBits"].exists()) {
            stopBits.second = true;
        } else {
            auto i = configuration["StopBits"].toInteger();
            if (INTEGER::defined(i) && i > 0) {
                stopBits.first = static_cast<int>(i);
                stopBits.second = true;
            }
        }
        if (!configuration["Parity"].exists()) {
            parity.second = true;
        } else {
            const auto &p = configuration["Parity"].toString();
            if (!p.empty()) {
                if (Util::equal_insensitive(p, "Even")) {
                    parity.first = IO::Serial::Stream::Parity::Even;
                } else if (Util::equal_insensitive(p, "Odd")) {
                    parity.first = IO::Serial::Stream::Parity::Odd;
                } else if (Util::equal_insensitive(p, "Mark")) {
                    parity.first = IO::Serial::Stream::Parity::Mark;
                } else if (Util::equal_insensitive(p, "Space")) {
                    parity.first = IO::Serial::Stream::Parity::Space;
                }
                parity.second = true;
            }
        }

        if (!configuration["SoftwareFlowControl"].exists()) {
            softwareFlowControl.second = true;
        } else {
            const auto &p = configuration["SoftwareFlowControl"].toString();
            if (!p.empty()) {
                if (Util::equal_insensitive(p, "Even")) {
                    parity.first = IO::Serial::Stream::Parity::Even;
                } else if (Util::equal_insensitive(p, "Odd")) {
                    parity.first = IO::Serial::Stream::Parity::Odd;
                } else if (Util::equal_insensitive(p, "Mark")) {
                    parity.first = IO::Serial::Stream::Parity::Mark;
                } else if (Util::equal_insensitive(p, "Space")) {
                    parity.first = IO::Serial::Stream::Parity::Space;
                }
                parity.second = true;
            }
        }

        if (!configuration["PulseRTS"].exists()) {
            rtsPulse.second = true;
        } else {
            if (configuration["PulseRTS/Enable"].exists()) {
                rtsPulse.first = PulseControl(configuration["PulseRTS"]);
                rtsPulse.second = true;
            }
        }
        if (!configuration["PulseDTR"].exists()) {
            dtrPulse.second = true;
        } else {
            if (configuration["PulseDTR/Enable"].exists()) {
                dtrPulse.first = PulseControl(configuration["PulseDTR"]);
                dtrPulse.second = true;
            }
        }

        if (!configuration["HardwareFlowControl"].exists()) {
            hardwareFlowControl.reset(new HardwareFlowControl_None(configuration));
        } else {
            const auto &p = configuration["HardwareFlowControl"].toString();
            if (!p.empty()) {
                if (Util::equal_insensitive(p, "Standard", "RS232")) {
                    hardwareFlowControl.reset(new HardwareFlowControl_Standard(configuration));
                } else if (Util::equal_insensitive(p, "Explicit", "RTSOnSend", "RS485")) {
                    hardwareFlowControl.reset(new HardwareFlowControl_ExplicitRTS(configuration));
                } else if (Util::equal_insensitive(p, "Space")) {
                    hardwareFlowControl.reset(new HardwareFlowControl_None(configuration));
                }
            }
        }
    }

    explicit Target_Serial(const std::string &description)
    {
        baud.second = true;
        dataBits.second = true;
        stopBits.second = true;
        parity.second = true;
        softwareFlowControl.second = true;
        disableBlank.second = true;
        rtsPulse.second = true;
        dtrPulse.second = true;
        hardwareFlowControl.reset(new HardwareFlowControl_None(Variant::Read::empty()));

        QString str = QString::fromStdString(description);
        int idx = str.indexOf(QRegularExpression("[:;,]"));
        if (idx != -1 && idx != 0) {
            device = str.left(idx).toStdString();

            auto params = str.mid(idx + 1);
            auto baudString = params;
            int idxTail = baudString.indexOf(
                    QRegularExpression("[NEO]", QRegularExpression::CaseInsensitiveOption));
            if (idxTail != -1) {
                baudString = baudString.left(idxTail);

                switch (params.at(idxTail).toLatin1()) {
                case 'N':
                case 'n':
                    parity.first = IO::Serial::Stream::Parity::None;
                    parity.second = true;
                    break;
                case 'E':
                case 'e':
                    parity.first = IO::Serial::Stream::Parity::Even;
                    parity.second = true;
                    break;
                case 'O':
                case 'o':
                    parity.first = IO::Serial::Stream::Parity::Odd;
                    parity.second = true;
                    break;
                default:
                    parity.second = false;
                    break;
                }
                idxTail++;

                if (idxTail < params.size()) {
                    auto ch = params.at(idxTail).toLatin1();
                    if (ch >= '0' && ch <= '9') {
                        dataBits.first = ch - '0';
                        dataBits.second = true;
                    } else {
                        dataBits.second = false;
                    }
                }
                idxTail++;

                if (idxTail < params.size()) {
                    auto ch = params.at(idxTail).toLatin1();
                    if (ch >= '0' && ch <= '9') {
                        stopBits.first = ch - '0';
                        stopBits.second = true;
                    } else {
                        stopBits.second = false;
                    }
                }
                idxTail++;
            }

            bool ok = false;
            auto n = baudString.toInt(&ok);
            if (ok && n > 0) {
                baud.first = n;
                baud.second = true;
            } else {
                baud.second = false;
            }
        } else {
            device = description;
        }

        if (!IO::Serial::Backing::isValid(device)) {
            device.clear();
        }
    }

    bool isValid() const override
    { return !device.empty(); }

    bool triggerAutoprobeWhenClosed() const override
    { return true; }

    bool matchesDevice(const IOTarget &other) const override
    {
        auto target = dynamic_cast<const Target_Serial *>(&other);
        if (!target)
            return false;
#ifdef Q_OS_UNIX
        return canonicalPath(device) == canonicalPath(target->device);
#else
        return device == target->device;
#endif
    }

    std::unique_ptr<IOTarget> clone(bool deviceOnly = false) const override
    {
        if (deviceOnly) {
            std::unique_ptr<Target_Serial> result(new Target_Serial);
            result->device = device;
            result->eavesdropper = eavesdropper;
            return std::move(result);
        }
        return std::unique_ptr<IOTarget>(new Target_Serial(*this));
    }

    std::unique_ptr<IOTarget::Device> backingDevice() const override
    {
        std::unique_ptr<Device> result(new Device(device));
        if (!eavesdropper.empty()) {
            result->controlStream
                  .reset(static_cast<IO::Serial::Stream *>(IO::Access::serial(
                          eavesdropper)->stream().release()));
            result->control = result->controlStream->read;
        }

        if (!result->dataStream && !result->controlStream) {
            qCDebug(log_acquisition_iotarget) << "No serial port available for" << configuration();
            return {};
        }

        if (disableBlank.second) {
            result->disableBlank = disableBlank.first;
        }
        if (rtsPulse.second) {
            result->rtsPulse = rtsPulse.first;
        }
        if (dtrPulse.second) {
            result->dtrPulse = dtrPulse.first;
        }

        if (baud.second) {
            if (result->dataStream) {
                if (!result->dataStream->setBaud(baud.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting initial baud for" << device
                                                     << "to" << baud.first << ":"
                                                     << result->dataStream->describeError();
                }
            }
            if (result->controlStream) {
                if (!result->controlStream->setBaud(baud.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting control initial baud for"
                                                     << eavesdropper << "to" << baud.first << ":"
                                                     << result->controlStream->describeError();
                }
            }
        }

        if (dataBits.second) {
            if (result->dataStream) {
                if (!result->dataStream->setDataBits(dataBits.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting initial data bits for"
                                                     << device << "to" << dataBits.first << ":"
                                                     << result->dataStream->describeError();
                }
            }
            if (result->controlStream) {
                if (!result->controlStream->setDataBits(dataBits.first)) {
                    qCInfo(log_acquisition_iotarget)
                        << "Error setting control initial data bits for" << eavesdropper << "to"
                        << dataBits.first << ":" << result->controlStream->describeError();
                }
            }
        }

        if (stopBits.second) {
            if (result->dataStream) {
                if (!result->dataStream->setStopBits(stopBits.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting initial stop bits for"
                                                     << device << "to" << stopBits.first << ":"
                                                     << result->dataStream->describeError();
                }
            }
            if (result->controlStream) {
                if (!result->controlStream->setStopBits(stopBits.first)) {
                    qCInfo(log_acquisition_iotarget)
                        << "Error setting control initial stop bits for" << eavesdropper << "to"
                        << stopBits.first << ":" << result->controlStream->describeError();
                }
            }
        }

        if (parity.second) {
            if (result->dataStream) {
                if (!result->dataStream->setParity(parity.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting initial parity for" << device
                                                     << ":" << result->dataStream->describeError();
                }
            }
            if (result->controlStream) {
                if (!result->controlStream->setParity(parity.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting control initial parity for"
                                                     << eavesdropper << ":"
                                                     << result->controlStream->describeError();
                }
            }
        }

        if (softwareFlowControl.second) {
            if (result->dataStream) {
                if (!result->dataStream
                           ->setSoftwareFlowControl(softwareFlowControl.first.control,
                                                    softwareFlowControl.first.enableResume)) {
                    qCInfo(log_acquisition_iotarget)
                        << "Error setting initial software flow control for" << device << ":"
                        << result->dataStream->describeError();
                }
            }
            if (result->controlStream) {
                if (!result->controlStream
                           ->setSoftwareFlowControl(softwareFlowControl.first.control,
                                                    softwareFlowControl.first.enableResume)) {
                    qCInfo(log_acquisition_iotarget)
                        << "Error setting control initial software flow control for" << device
                        << ":" << result->controlStream->describeError();
                }
            }
        }

        if (hardwareFlowControl) {
            if (result->dataStream) {
                if (!hardwareFlowControl->apply(*result->dataStream)) {
                    qCInfo(log_acquisition_iotarget)
                        << "Error setting initial hardware flow control for" << device << ":"
                        << result->dataStream->describeError();
                }
            }
            if (result->controlStream) {
                if (!hardwareFlowControl->apply(*result->controlStream)) {
                    qCInfo(log_acquisition_iotarget)
                        << "Error setting control initial hardware flow control for" << device
                        << ":" << result->controlStream->describeError();
                }
            }
        }

        result->reset();

        if (result->dataStream) {
            if (result->controlStream) {
                qCDebug(log_acquisition_iotarget) << "Opened serial port"
                                                  << result->dataStream->describePort()
                                                  << "with control"
                                                  << result->controlStream->describePort();
            } else {
                qCDebug(log_acquisition_iotarget) << "Opened serial port"
                                                  << result->dataStream->describePort();
            }
        } else if (result->controlStream) {
            qCDebug(log_acquisition_iotarget) << "Opened control serial port"
                                              << result->controlStream->describePort();
        }
        return std::move(result);
    }

    void merge(const IOTarget &baseOverlay, IOTarget::Device *device = nullptr) override
    {
        auto overlay = dynamic_cast<const Target_Serial *>(&baseOverlay);
        if (!overlay) {
            qCDebug(log_acquisition_iotarget) << "Attempt to merge incompatible"
                                              << baseOverlay.description() << "into"
                                              << description();
            return;
        }
        auto serial = dynamic_cast<Device *>(device);
        bool didChange = false;

        qCDebug(log_acquisition_iotarget) << "Merging" << overlay->description() << "into"
                                          << description();

        if (overlay->baud.second && overlay->baud != baud) {
            baud = overlay->baud;
            if (serial && serial->dataStream) {
                if (!serial->dataStream->setBaud(baud.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting baud for" << this->device
                                                     << "to" << baud.first << ":"
                                                     << serial->dataStream->describeError();
                }
                didChange = true;
            }
            if (serial && serial->controlStream) {
                if (!serial->controlStream->setBaud(baud.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting control baud for"
                                                     << this->eavesdropper << "to" << baud.first
                                                     << ":"
                                                     << serial->controlStream->describeError();
                }
                didChange = true;
            }
        }

        if (overlay->dataBits.second && overlay->dataBits != dataBits) {
            dataBits = overlay->dataBits;
            if (serial && serial->dataStream) {
                if (!serial->dataStream->setDataBits(dataBits.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting data bits for"
                                                     << this->device << "to" << dataBits.first
                                                     << ":" << serial->dataStream->describeError();
                }
                didChange = true;
            }
            if (serial && serial->controlStream) {
                if (!serial->controlStream->setDataBits(dataBits.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting control data bits for"
                                                     << this->eavesdropper << "to" << dataBits.first
                                                     << ":"
                                                     << serial->controlStream->describeError();
                }
                didChange = true;
            }
        }

        if (overlay->stopBits.second && overlay->stopBits != stopBits) {
            stopBits = overlay->stopBits;
            if (serial && serial->dataStream) {
                if (!serial->dataStream->setStopBits(stopBits.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting stop bits for"
                                                     << this->device << "to" << stopBits.first
                                                     << ":" << serial->dataStream->describeError();
                }
                didChange = true;
            }
            if (serial && serial->controlStream) {
                if (!serial->controlStream->setStopBits(stopBits.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting control stop bits for"
                                                     << this->eavesdropper << "to" << stopBits.first
                                                     << ":"
                                                     << serial->controlStream->describeError();
                }
                didChange = true;
            }
        }

        if (overlay->parity.second && overlay->parity != parity) {
            parity = overlay->parity;
            if (serial && serial->dataStream) {
                if (!serial->dataStream->setParity(parity.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting parity for" << this->device
                                                     << ":" << serial->dataStream->describeError();
                }
                didChange = true;
            }
            if (serial && serial->controlStream) {
                if (!serial->controlStream->setParity(parity.first)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting control parity for"
                                                     << this->eavesdropper << ":"
                                                     << serial->controlStream->describeError();
                }
                didChange = true;
            }
        }

        if (overlay->softwareFlowControl.second &&
                overlay->softwareFlowControl != softwareFlowControl) {
            softwareFlowControl = overlay->softwareFlowControl;
            if (serial && serial->dataStream) {
                if (!serial->dataStream
                           ->setSoftwareFlowControl(softwareFlowControl.first.control,
                                                    softwareFlowControl.first.enableResume)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting software flow control for"
                                                     << this->device << ":"
                                                     << serial->dataStream->describeError();
                }
                didChange = true;
            }
            if (serial && serial->controlStream) {
                if (!serial->controlStream
                           ->setSoftwareFlowControl(softwareFlowControl.first.control,
                                                    softwareFlowControl.first.enableResume)) {
                    qCInfo(log_acquisition_iotarget)
                        << "Error setting control software flow control for" << this->eavesdropper
                        << ":" << serial->controlStream->describeError();
                }
                didChange = true;
            }
        }

        if (overlay->disableBlank.second) {
            disableBlank = overlay->disableBlank;
            if (serial) {
                serial->disableBlank = disableBlank.first;
            }
        }
        if (overlay->rtsPulse.second) {
            rtsPulse = overlay->rtsPulse;
            if (serial) {
                serial->rtsPulse = rtsPulse.first;
            }
        }
        if (overlay->dtrPulse.second) {
            dtrPulse = overlay->dtrPulse;
            if (serial) {
                serial->dtrPulse = dtrPulse.first;
            }
        }

        if (overlay->hardwareFlowControl &&
                (!hardwareFlowControl ||
                        !hardwareFlowControl->equals(*overlay->hardwareFlowControl))) {
            hardwareFlowControl = overlay->hardwareFlowControl->clone();
            if (serial && serial->dataStream) {
                if (!hardwareFlowControl->apply(*serial->dataStream)) {
                    qCInfo(log_acquisition_iotarget) << "Error setting hardware flow control for"
                                                     << this->device << ":"
                                                     << serial->dataStream->describeError();
                }
                didChange = true;
            }
            if (serial && serial->controlStream) {
                if (!hardwareFlowControl->apply(*serial->controlStream)) {
                    qCInfo(log_acquisition_iotarget)
                        << "Error setting control hardware flow control for" << this->eavesdropper
                        << ":" << serial->controlStream->describeError();
                }
                didChange = true;
            }
        }

        if (device && didChange) {
            device->reset();

            if (serial) {
                if (serial->dataStream) {
                    if (serial->controlStream) {
                        qCDebug(log_acquisition_iotarget) << "Reset updated serial port"
                                                          << serial->dataStream->describePort()
                                                          << "with control"
                                                          << serial->controlStream->describePort();
                    } else {
                        qCDebug(log_acquisition_iotarget) << "Reset updated serial port"
                                                          << serial->dataStream->describePort();
                    }
                } else if (serial->controlStream) {
                    qCDebug(log_acquisition_iotarget) << "Reset updated control serial port"
                                                      << serial->controlStream->describePort();
                }
            }
        }
    }


    Util::ByteArray serialize() const override
    {
        Util::ByteArray result;
        Util::ByteArray::Device dev(result);
        dev.open(QIODevice::WriteOnly);
        QDataStream stream(&dev);

        stream << static_cast<quint8>(SerializationID::Serial);

        stream << device << eavesdropper;
        stream << static_cast<qint32>(baud.first) << baud.second;
        stream << static_cast<qint32>(dataBits.first) << dataBits.second;
        stream << static_cast<qint32>(stopBits.first) << stopBits.second;

        switch (parity.first) {
        case IO::Serial::Stream::Parity::None:
            stream << static_cast<quint8>(0);
            break;
        case IO::Serial::Stream::Parity::Even:
            stream << static_cast<quint8>(1);
            break;
        case IO::Serial::Stream::Parity::Odd:
            stream << static_cast<quint8>(2);
            break;
        case IO::Serial::Stream::Parity::Mark:
            stream << static_cast<quint8>(3);
            break;
        case IO::Serial::Stream::Parity::Space:
            stream << static_cast<quint8>(4);
            break;
        }
        stream << parity.second;

        switch (softwareFlowControl.first.control) {
        case IO::Serial::Stream::SoftwareFlowControl::Disable:
            stream << static_cast<quint8>(0);
            break;
        case IO::Serial::Stream::SoftwareFlowControl::Both:
            stream << static_cast<quint8>(1);
            break;
        case IO::Serial::Stream::SoftwareFlowControl::XON:
            stream << static_cast<quint8>(2);
            break;
        case IO::Serial::Stream::SoftwareFlowControl::XOFF:
            stream << static_cast<quint8>(3);
            break;
        }
        stream << softwareFlowControl.first.enableResume << softwareFlowControl.second;

        stream << disableBlank.first << disableBlank.second;
        stream << rtsPulse.first.width << rtsPulse.first.enable << rtsPulse.first.invert
               << rtsPulse.second;
        stream << dtrPulse.first.width << dtrPulse.first.enable << dtrPulse.first.invert
               << dtrPulse.second;

        if (!hardwareFlowControl) {
            stream << static_cast<quint8>(HardwareFlowControlType::Unset);
        } else {
            hardwareFlowControl->serialize(stream);
        }

        return result;
    }

    explicit Target_Serial(const Util::ByteView &data)
    {
        Util::ByteView::Device dev(data);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);

        stream >> device >> eavesdropper;
        {
            qint32 i = 0;
            stream >> i >> baud.second;
            baud.first = i;
        }
        {
            qint32 i = 0;
            stream >> i >> dataBits.second;
            dataBits.first = i;
        }
        {
            qint32 i = 0;
            stream >> i >> stopBits.second;
            stopBits.first = i;
        }

        {
            quint8 i = 0;
            stream >> i;
            switch (i) {
            case 0:
                parity.first = IO::Serial::Stream::Parity::None;
                break;
            case 1:
                parity.first = IO::Serial::Stream::Parity::Even;
                break;
            case 2:
                parity.first = IO::Serial::Stream::Parity::Odd;
                break;
            case 3:
                parity.first = IO::Serial::Stream::Parity::Mark;
                break;
            case 4:
                parity.first = IO::Serial::Stream::Parity::Space;
                break;
            default:
                break;
            }
            stream >> parity.second;
        }

        {
            quint8 i = 0;
            stream >> i;
            switch (i) {
            case 0:
                softwareFlowControl.first.control =
                        IO::Serial::Stream::SoftwareFlowControl::Disable;
                break;
            case 1:
                softwareFlowControl.first.control = IO::Serial::Stream::SoftwareFlowControl::Both;
                break;
            case 2:
                softwareFlowControl.first.control = IO::Serial::Stream::SoftwareFlowControl::XON;
                break;
            case 3:
                softwareFlowControl.first.control = IO::Serial::Stream::SoftwareFlowControl::XOFF;
                break;
            default:
                break;
            }
            stream >> softwareFlowControl.first.enableResume >> softwareFlowControl.second;
        }


        stream >> disableBlank.first >> disableBlank.second;
        stream >> rtsPulse.first.width >> rtsPulse.first.enable >> rtsPulse.first.invert
               >> rtsPulse.second;
        stream >> dtrPulse.first.width >> dtrPulse.first.enable >> dtrPulse.first.invert
               >> dtrPulse.second;

        {
            quint8 i = 0;
            stream >> i;
            switch (static_cast<HardwareFlowControlType>(i)) {
            case HardwareFlowControlType::Unset:
                hardwareFlowControl.reset();
                break;
            case HardwareFlowControlType::None:
                hardwareFlowControl.reset(new HardwareFlowControl_None(stream));
                break;
            case HardwareFlowControlType::Standard:
                hardwareFlowControl.reset(new HardwareFlowControl_Standard(stream));
                break;
            case HardwareFlowControlType::ExplicitRTS:
                hardwareFlowControl.reset(new HardwareFlowControl_ExplicitRTS(stream));
                break;
            default:
                break;
            }
        }

    }


    QString description(bool deviceOnly = false) const override
    {
        QString result = QString::fromStdString(device);
        if (deviceOnly || (!baud.second && !parity.second && !dataBits.second && !stopBits.second))
            return result;
        result += ":";

        if (baud.second) {
            result += QString::number(baud.first);
        } else {
            result += "0";
        }

        if (!parity.second && !dataBits.second && !stopBits.second)
            return result;

        if (parity.second) {
            switch (parity.first) {
            case IO::Serial::Stream::Parity::None:
                result += "N";
                break;
            case IO::Serial::Stream::Parity::Even:
                result += "E";
                break;
            case IO::Serial::Stream::Parity::Odd:
                result += "O";
                break;
            case IO::Serial::Stream::Parity::Mark:
                result += "M";
                break;
            case IO::Serial::Stream::Parity::Space:
                result += "S";
                break;
            }
        } else {
            result += "_";
        }

        if (dataBits.second) {
            result += QString::number(dataBits.first);
        } else {
            result += "_";
        }

        if (stopBits.second) {
            result += QString::number(stopBits.first);
        } else {
            result += "_";
        }

        return result;
    }

    Variant::Root configuration(bool deviceOnly = false) const override
    {
        Variant::Root result;
        result["Type"] = "SerialPort";
        result["Port"].setString(device);
        if (!eavesdropper.empty()) {
            result["Eavesdropper"].setString(device);
        }
        if (deviceOnly)
            return result;

        if (baud.second) {
            result["Baud"].setInteger(baud.first);
        }
        if (dataBits.second) {
            result["DataBits"].setInteger(dataBits.first);
        }
        if (stopBits.second) {
            result["StopBits"].setInteger(stopBits.first);
        }
        if (parity.second) {
            switch (parity.first) {
            case IO::Serial::Stream::Parity::None:
                result["Parity"].setString("None");
                break;
            case IO::Serial::Stream::Parity::Even:
                result["Parity"].setString("Even");
                break;
            case IO::Serial::Stream::Parity::Odd:
                result["Parity"].setString("Odd");
                break;
            case IO::Serial::Stream::Parity::Mark:
                result["Parity"].setString("Mark");
                break;
            case IO::Serial::Stream::Parity::Space:
                result["Parity"].setString("Space");
                break;
            }
        }
        if (softwareFlowControl.second) {
            switch (softwareFlowControl.first.control) {
            case IO::Serial::Stream::SoftwareFlowControl::Disable:
                result["SoftwareFlowControl"].setString("None");
                break;
            case IO::Serial::Stream::SoftwareFlowControl::Both:
                result["SoftwareFlowControl"].setString("Bidirectional");
                break;
            case IO::Serial::Stream::SoftwareFlowControl::XON:
                result["SoftwareFlowControl"].setString("Output");
                break;
            case IO::Serial::Stream::SoftwareFlowControl::XOFF:
                result["SoftwareFlowControl"].setString("Input");
                break;
            }
            result["SoftwareFlowControlResume"].setBoolean(softwareFlowControl.first.enableResume);
        }
        if (disableBlank.second) {
            result["NoInitialBreak"].setBoolean(disableBlank.first);
        }
        if (rtsPulse.second) {
            rtsPulse.first.configure(result["PulseRTS"]);
        }
        if (dtrPulse.second) {
            dtrPulse.first.configure(result["PulseDTR"]);
        }
        if (hardwareFlowControl) {
            hardwareFlowControl->configure(result.write());
        }
        return result;
    }

protected:
    Target_Serial(const Target_Serial &other) : IOTarget(other),
                                                device(other.device),
                                                dataBits(other.dataBits),
                                                stopBits(other.stopBits),
                                                parity(other.parity),
                                                softwareFlowControl(other.softwareFlowControl),
                                                disableBlank(other.disableBlank),
                                                rtsPulse(other.rtsPulse),
                                                dtrPulse(other.dtrPulse)
    {
        if (other.hardwareFlowControl) {
            hardwareFlowControl = other.hardwareFlowControl->clone();
        }
    }

    Util::ByteArray localSocketUID() const override
    {
        Util::ByteArray result;
        result.push_back(static_cast<std::uint8_t>(SerializationID::Serial));
#ifdef Q_OS_UNIX
        result += canonicalPath(device).c_str();
#else
        result += device.c_str();
#endif
        result.push_back(static_cast<std::uint8_t>(0));
#ifdef Q_OS_UNIX
        result += canonicalPath(eavesdropper).c_str();
#else
        result += eavesdropper.c_str();
#endif
        return result;
    }
};

class Target_TCPSocket : public IOTarget {
    std::string address;
    std::string bind;
    std::uint16_t port;
    Variant::Root tls;

public:
    explicit Target_TCPSocket(const Variant::Read &configuration) : IOTarget(configuration),
                                                                    address(configuration["Server"].toString()),
                                                                    bind(configuration["Bind"].toString()),
                                                                    port(IO::Socket::TCP::Configuration::port_any),
                                                                    tls(configuration["SSL"])
    {
        auto p = configuration["ServerPort"].toInteger();
        if (INTEGER::defined(p) && p > 0 && p <= 0xFFFF) {
            port = p;
        }
    }

    virtual ~Target_TCPSocket() = default;

    bool isValid() const override
    { return port != IO::Socket::TCP::Configuration::port_any; }

    bool matchesDevice(const IOTarget &other) const override
    {
        auto target = dynamic_cast<const Target_TCPSocket *>(&other);
        if (!target)
            return false;
        return address == target->address && bind == target->bind && port == target->port &&
                tls.read() == target->tls.read();
    }

    std::unique_ptr<IOTarget> clone(bool = false) const override
    { return std::unique_ptr<IOTarget>(new Target_TCPSocket(*this)); }

    std::unique_ptr<IOTarget::Device> backingDevice() const override
    {
        std::unique_ptr<IO::Socket::TCP::Connection> socket;
        if (bind.empty()) {
            socket = IO::Socket::TCP::connect(address, {port, toTLSAccept(tls)});
        } else {
            socket = IO::Socket::TCP::connect(address, {port, toTLSAccept(tls)}, bind);
        }
        if (!socket)
            return {};
        return std::unique_ptr<Device>(new StreamWrapperDevice(std::move(socket)));
    }

    Util::ByteArray serialize() const override
    {
        Util::ByteArray result;
        Util::ByteArray::Device dev(result);
        dev.open(QIODevice::WriteOnly);
        QDataStream stream(&dev);

        stream << static_cast<quint8>(SerializationID::TCPSocket);

        stream << address << bind << static_cast<quint16>(port) << tls;

        return result;
    }

    explicit Target_TCPSocket(const Util::ByteView &data)
    {
        Util::ByteView::Device dev(data);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);

        stream >> address;
        stream >> bind;
        {
            quint16 i = 0;
            stream >> i;
            port = i;
        }
        stream >> tls;
    }

    QString description(bool = false) const override
    {
        QString result = QString::fromStdString(address);
        if (result.contains(":"))
            result = "[" + result + "]";
        result += ":";
        result += QString::number(port);
        return QObject::tr("TCP=%1", "remote server description").arg(result);
    }

    Variant::Root configuration(bool = false) const override
    {
        Variant::Root result;
        result["Type"] = "RemoteServer";
        result["Server"].setString(address);
        result["ServerPort"].setInteger(port);
        if (!bind.empty())
            result["Bind"].setString(bind);
        if (tls.read().exists())
            result["SSL"].set(tls);
        return result;
    }

protected:
    Target_TCPSocket(const Target_TCPSocket &other) = default;

    Util::ByteArray localSocketUID() const override
    {
        Util::ByteArray result;
        result.push_back(static_cast<std::uint8_t>(SerializationID::TCPSocket));
        result += address.c_str();
        result.push_back(static_cast<std::uint8_t>(0));
        result += bind.c_str();
        result.push_back(static_cast<std::uint8_t>(0));
        result.appendNumber<quint16>(port);
        if (tls.read().exists()) {
            Util::ByteArray::Device dev(result);
            dev.open(QIODevice::WriteOnly);
            dev.seek(dev.size());
            QDataStream stream(&dev);
            stream << tls;
        }
        return result;
    }
};

class Target_LocalSocket : public IOTarget {
    std::string server;
public:
    explicit Target_LocalSocket(const Variant::Read &configuration) : IOTarget(configuration),
                                                                      server(configuration["Name"].toString())
    { }

    virtual ~Target_LocalSocket() = default;

    bool isValid() const override
    { return !server.empty(); }

    bool matchesDevice(const IOTarget &other) const override
    {
        auto target = dynamic_cast<const Target_LocalSocket *>(&other);
        if (!target)
            return false;
        return server == target->server;
    }

    std::unique_ptr<IOTarget> clone(bool = false) const override
    { return std::unique_ptr<IOTarget>(new Target_LocalSocket(*this)); }

    std::unique_ptr<IOTarget::Device> backingDevice() const override
    {
        auto socket = IO::Socket::Local::connect(server);
        if (!socket)
            return {};
        return std::unique_ptr<Device>(new StreamWrapperDevice(std::move(socket)));
    }

    Util::ByteArray serialize() const override
    {
        Util::ByteArray result;
        Util::ByteArray::Device dev(result);
        dev.open(QIODevice::WriteOnly);
        QDataStream stream(&dev);

        stream << static_cast<quint8>(SerializationID::LocalSocket);

        stream << server;

        return result;
    }

    explicit Target_LocalSocket(const Util::ByteView &data)
    {
        Util::ByteView::Device dev(data);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);

        stream >> server;
    }

    QString description(bool = false) const override
    {
        return QObject::tr("QLocalSocket=%1", "Qt local socket description").arg(
                QString::fromStdString(server));
    }

    Variant::Root configuration(bool = false) const override
    {
        Variant::Root result;
        result["Type"] = "QtLocalSocket";
        result["Name"].setString(server);
        return result;
    }

protected:
    Target_LocalSocket(const Target_LocalSocket &other) = default;

    Util::ByteArray localSocketUID() const override
    {
        Util::ByteArray result;
        result.push_back(static_cast<std::uint8_t>(SerializationID::LocalSocket));
        result += server.c_str();
        return result;
    }
};

class Target_UnixSocket : public IOTarget {
    std::string path;
public:
    explicit Target_UnixSocket(const Variant::Read &configuration) : IOTarget(configuration),
                                                                     path(configuration["File"].toString())
    { }

    virtual ~Target_UnixSocket() = default;

    bool isValid() const override
    { return !path.empty(); }

    bool matchesDevice(const IOTarget &other) const override
    {
        auto target = dynamic_cast<const Target_UnixSocket *>(&other);
        if (!target)
            return false;
        return path == target->path;
    }

    std::unique_ptr<IOTarget> clone(bool = false) const override
    { return std::unique_ptr<IOTarget>(new Target_UnixSocket(*this)); }

    std::unique_ptr<IOTarget::Device> backingDevice() const override
    {
        auto socket = IO::Socket::Unix::connect(path);
        if (!socket)
            return {};
        return std::unique_ptr<Device>(new StreamWrapperDevice(std::move(socket)));
    }

    Util::ByteArray serialize() const override
    {
        Util::ByteArray result;
        Util::ByteArray::Device dev(result);
        dev.open(QIODevice::WriteOnly);
        QDataStream stream(&dev);

        stream << static_cast<quint8>(SerializationID::UnixSocket);

        stream << path;

        return result;
    }

    explicit Target_UnixSocket(const Util::ByteView &data)
    {
        Util::ByteView::Device dev(data);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);

        stream >> path;
    }

    QString description(bool = false) const override
    {
        return QObject::tr("LocalSocket=%1", "Unix socket description").arg(
                QString::fromStdString(path));
    }

    Variant::Root configuration(bool = false) const override
    {
        Variant::Root result;
        result["Type"] = "LocalSocket";
        result["File"].setString(path);
        return result;
    }

protected:
    Target_UnixSocket(const Target_UnixSocket &other) = default;

    Util::ByteArray localSocketUID() const override
    {
        Util::ByteArray result;
        result.push_back(static_cast<std::uint8_t>(SerializationID::UnixSocket));
        result += canonicalPath(path).c_str();
        return result;
    }
};

class Target_UDP : public IOTarget {
    std::string serverAddress;
    std::uint16_t serverPort;
    std::string listenAddress;
    std::uint16_t listenPort;
    std::uint16_t fragmentSize;
    std::pair<bool, bool> disableFraming;

    class Device : public StreamWrapperDevice {
        std::atomic_uint_fast16_t fragmentSize;
    public:
        Device(std::unique_ptr<IO::Generic::Stream> &&st, std::uint16_t fragmentSize)
                : StreamWrapperDevice(std::move(st)), fragmentSize(fragmentSize)
        { }

        virtual ~Device() = default;

        void write(const Util::ByteView &data) override
        {
            auto remaining = data;
            while (!remaining.empty()) {
                auto limit = std::min<std::size_t>(fragmentSize.load(std::memory_order_relaxed),
                                                   remaining.size());
                StreamWrapperDevice::write(remaining.mid(0, limit));
                remaining = remaining.mid(limit);
            }
        }

        void write(const Util::ByteArray &data) override
        {
            if (data.size() <= fragmentSize.load(std::memory_order_relaxed))
                return StreamWrapperDevice::write(data);
            return write(Util::ByteView(data));
        }

        void write(Util::ByteArray &&data) override
        {
            if (data.size() <= fragmentSize.load(std::memory_order_relaxed))
                return StreamWrapperDevice::write(std::move(data));
            return write(Util::ByteView(data));
        }

        void write(const QByteArray &data) override
        {
            if (static_cast<std::size_t>(data.size()) <=
                    fragmentSize.load(std::memory_order_relaxed))
                return StreamWrapperDevice::write(data);
            return write(Util::ByteView(data));
        }

        void write(QByteArray &&data) override
        {
            if (static_cast<std::size_t>(data.size()) <=
                    fragmentSize.load(std::memory_order_relaxed))
                return StreamWrapperDevice::write(std::move(data));
            return write(Util::ByteView(data));
        }

        void setFragmentSize(std::uint16_t setting)
        {
            fragmentSize.store(setting, std::memory_order_release);
        }
    };

public:
    explicit Target_UDP(const Variant::Read &configuration) : IOTarget(configuration),
                                                              serverAddress(
                                                                      configuration["Server"].toString()),
                                                              serverPort(
                                                                      IO::UDP::Configuration::port_any),
                                                              listenAddress(
                                                                      configuration["ListenAddress"]
                                                                              .toString()),
                                                              listenPort(
                                                                      IO::UDP::Configuration::port_any),
                                                              fragmentSize(1300U),
                                                              disableFraming(false, false)
    {
        auto p = configuration["ServerPort"].toInteger();
        if (INTEGER::defined(p) && p > 0 && p <= 0xFFFF) {
            serverPort = p;
        }
        p = configuration["LocalPort"].toInteger();
        if (INTEGER::defined(p) && p > 0 && p <= 0xFFFF) {
            listenPort = p;
        }
        if (configuration["FragmentSize"].exists()) {
            p = configuration["FragmentSize"].toInteger();
            if (INTEGER::defined(p) && p > 0 && p <= 0xFFFF) {
                fragmentSize = p;
            } else {
                fragmentSize = 0;
            }
        }
        if (configuration["DisableFraming"].exists()) {
            disableFraming.second = true;
            disableFraming.first = configuration["DisableFraming"].toBoolean();
        }
    }

    virtual ~Target_UDP() = default;

    bool isValid() const override
    {
        return serverPort != IO::UDP::Configuration::port_any ||
                listenPort != IO::UDP::Configuration::port_any;
    }

    bool integratedFraming() const override
    {
        if (!disableFraming.second)
            return true;
        return !disableFraming.first;
    }

    bool matchesDevice(const IOTarget &other) const override
    {
        auto target = dynamic_cast<const Target_UDP *>(&other);
        if (!target)
            return false;
        return serverAddress == target->serverAddress &&
                serverPort == target->serverPort &&
                listenAddress == target->listenAddress &&
                listenPort == target->listenPort;
    }

    std::unique_ptr<IOTarget> clone(bool deviceOnly = false) const override
    {
        auto result = std::unique_ptr<Target_UDP>(new Target_UDP(*this));
        if (deviceOnly) {
            result->fragmentSize = 0;
            result->disableFraming.second = false;
        }
        return std::move(result);
    }

    std::unique_ptr<IOTarget::Device> backingDevice() const override
    {
        IO::UDP::Configuration cfg;
        if (listenPort != IO::UDP::Configuration::port_any) {
            if (listenAddress.empty()) {
                cfg.listen(IO::UDP::Configuration::AddressFamily::Any, listenPort);
            } else {
                cfg.listen(listenAddress, listenPort);
            }
        }
        if (serverPort != IO::UDP::Configuration::port_any) {
            if (serverAddress.empty()) {
                cfg.loopback(serverPort);
            } else {
                cfg.connect(serverAddress, serverPort);
            }
        }
        auto socket = cfg();
        if (!socket)
            return {};
        return std::unique_ptr<Device>(new Device(std::move(socket), fragmentSize));
    }

    void merge(const IOTarget &baseOverlay, IOTarget::Device *device = nullptr) override
    {
        auto overlay = dynamic_cast<const Target_UDP *>(&baseOverlay);
        if (!overlay) {
            qCDebug(log_acquisition_iotarget) << "Attempt to merge incompatible"
                                              << baseOverlay.description() << "into"
                                              << description();
            return;
        }

        auto udp = dynamic_cast<Device *>(device);

        if (overlay->fragmentSize) {
            fragmentSize = overlay->fragmentSize;
            if (udp) {
                udp->setFragmentSize(fragmentSize);
            }
        }
        if (overlay->disableFraming.second) {
            disableFraming = overlay->disableFraming;
        }
    }

    Util::ByteArray serialize() const override
    {
        Util::ByteArray result;
        Util::ByteArray::Device dev(result);
        dev.open(QIODevice::WriteOnly);
        QDataStream stream(&dev);

        stream << static_cast<quint8>(SerializationID::UDP);

        stream << serverAddress << static_cast<quint16>(serverPort);
        stream << listenAddress << static_cast<quint16>(listenPort);
        stream << static_cast<quint16>(fragmentSize);
        stream << disableFraming.first << disableFraming.second;

        return result;
    }

    explicit Target_UDP(const Util::ByteView &data)
    {
        Util::ByteView::Device dev(data);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);

        stream >> serverAddress;
        {
            quint16 i = 0;
            stream >> i;
            serverPort = i;
        }
        stream >> listenAddress;
        {
            quint16 i = 0;
            stream >> i;
            listenPort = i;
        }
        {
            quint16 i = 0;
            stream >> i;
            fragmentSize = i;
        }
        stream >> disableFraming.first >> disableFraming.second;
    }

    QString description(bool = false) const override
    {
        QString result;
        if (serverPort != IO::UDP::Configuration::port_any) {
            result += QString::fromStdString(serverAddress);
            if (result.contains(":"))
                result = "[" + result + "]";
            result += ":";
            result += QString::number(serverPort);
        }
        if (listenPort != IO::UDP::Configuration::port_any) {
            if (!result.isEmpty())
                result += ":";
            result += QString::number(listenPort);
        }
        return QObject::tr("UDP=%1", "UDP description").arg(result);
    }

    Variant::Root configuration(bool deviceOnly = false) const override
    {
        Variant::Root result;
        result["Type"] = "UDP";
        if (serverPort != IO::UDP::Configuration::port_any) {
            if (!serverAddress.empty())
                result["Server"].setString(serverAddress);
            result["ServerPort"].setInteger(serverPort);
        }
        if (listenPort != IO::UDP::Configuration::port_any) {
            if (!listenAddress.empty())
                result["ListenAddress"].setString(listenAddress);
            result["LocalPort"].setInteger(listenPort);
        }
        if (fragmentSize && !deviceOnly) {
            result["FragmentSize"].setInteger(fragmentSize);
        }
        if (disableFraming.second && !deviceOnly) {
            result["DisableFraming"].setBoolean(disableFraming.first);
        }
        return result;
    }

protected:
    Target_UDP(const Target_UDP &other) = default;

    Util::ByteArray localSocketUID() const override
    {
        Util::ByteArray result;
        result.push_back(static_cast<std::uint8_t>(SerializationID::UDP));
        result += serverAddress.c_str();
        result.push_back(static_cast<std::uint8_t>(0));
        result.appendNumber<quint16>(serverPort);
        result += listenAddress.c_str();
        result.push_back(static_cast<std::uint8_t>(0));
        result.appendNumber<quint16>(listenPort);
        return result;
    }
};

class ListenWrapperDevice : public IOTarget::Device {
    std::mutex mutex;

    struct Active final {
        std::unique_ptr<IO::Socket::Connection> connection;
        Threading::Connection write;

        Active(std::unique_ptr<IO::Socket::Connection> &&connection, Threading::Connection write)
                : connection(std::move(connection)), write(std::move(write))
        { }

        ~Active()
        {
            write.disconnect();
        }
    };

    std::list<Active> connections;

    Threading::Receiver receiver;
    Threading::Signal<const Util::ByteArray &> dataToWrite;
public:
    ListenWrapperDevice() = default;

    virtual ~ListenWrapperDevice()
    {
        connections.clear();
        receiver.disconnect();
    }

    void accept(std::unique_ptr<IO::Socket::Connection> &&connection)
    {
        connection->read.connect(receiver, [this](const Util::ByteArray &data) {
            this->read(data);
        });
        /* Just handle the reaping when we add new connections, rather than use a dedicated
         * thread */

        auto wrc = dataToWrite.connect(
                std::bind(static_cast<void (IO::Generic::Stream::*)(const Util::ByteArray &)>(
                                  &IO::Generic::Stream::write), connection.get(),
                          std::placeholders::_1));

        connection->start();

        qCDebug(log_acquisition_iotarget) << "Accepted connection from"
                                          << connection->describePeer();

        std::lock_guard<std::mutex> lock(mutex);
        connections.emplace_back(std::move(connection), std::move(wrc));
        for (auto it = connections.begin(); it != connections.end();) {
            if (it->connection->isEnded()) {
                it = connections.erase(it);
                continue;
            }
            ++it;
        }
    }

    bool isEnded() const override
    { return false; }

    void write(const Util::ByteView &data) override
    { dataToWrite(Util::ByteArray(data)); }

    void write(const Util::ByteArray &data) override
    { dataToWrite(data); }
};

class Target_TCPListen : public IOTarget {
    std::string address;
    std::uint16_t port;
    Variant::Root tls;

    class Device : public ListenWrapperDevice {
    public:
        std::unique_ptr<IO::Socket::TCP::Server> server;

        Device() = default;

        virtual ~Device() = default;

        void start() override
        { }
    };

public:
    explicit Target_TCPListen(const Variant::Read &configuration) : IOTarget(configuration),
                                                                    address(configuration["ListenAddress"]
                                                                                    .toString()),
                                                                    port(IO::Socket::TCP::Configuration::port_any),
                                                                    tls(configuration["SSL"])
    {
        auto p = configuration["LocalPort"].toInteger();
        if (INTEGER::defined(p) && p > 0 && p <= 0xFFFF) {
            port = p;
        }
    }

    virtual ~Target_TCPListen() = default;

    bool isValid() const override
    { return port != IO::Socket::TCP::Configuration::port_any; }

    bool matchesDevice(const IOTarget &other) const override
    {
        auto target = dynamic_cast<const Target_TCPListen *>(&other);
        if (!target)
            return false;
        return address == target->address &&
                port == target->port &&
                tls.read() == target->tls.read();
    }

    std::unique_ptr<IOTarget> clone(bool = false) const override
    { return std::unique_ptr<IOTarget>(new Target_TCPListen(*this)); }

    std::unique_ptr<IOTarget::Device> backingDevice() const override
    {
        std::unique_ptr<Device> device(new Device);
        if (address.empty()) {
            device->server
                  .reset(new IO::Socket::TCP::Server(
                          std::bind(&Device::accept, device.get(), std::placeholders::_1),
                          IO::Socket::TCP::Configuration(port, toTLSAccept(tls))));
        } else {
            device->server
                  .reset(new IO::Socket::TCP::Server(
                          std::bind(&Device::accept, device.get(), std::placeholders::_1), address,
                          IO::Socket::TCP::Configuration(port, toTLSAccept(tls))));
        }
        if (!device->server->startListening())
            return {};
        return std::move(device);
    }

    Util::ByteArray serialize() const override
    {
        Util::ByteArray result;
        Util::ByteArray::Device dev(result);
        dev.open(QIODevice::WriteOnly);
        QDataStream stream(&dev);

        stream << static_cast<quint8>(SerializationID::TCPListen);

        stream << address << static_cast<quint16>(port) << tls;

        return result;
    }

    explicit Target_TCPListen(const Util::ByteView &data)
    {
        Util::ByteView::Device dev(data);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);

        stream >> address;
        {
            quint16 i = 0;
            stream >> i;
            port = i;
        }
        stream >> tls;
    }

    QString description(bool = false) const override
    { return QObject::tr("TCPListen=%1", "listen server description").arg(port); }

    Variant::Root configuration(bool = false) const override
    {
        Variant::Root result;
        result["Type"] = "TCPListen";
        result["ListenAddress"].setString(address);
        result["LocalPort"].setInteger(port);
        if (tls.read().exists()) {
            result["SSL"].set(tls);
        }
        return result;
    }

protected:
    Target_TCPListen(const Target_TCPListen &other) = default;

    Util::ByteArray localSocketUID() const override
    {
        Util::ByteArray result;
        result.push_back(static_cast<std::uint8_t>(SerializationID::TCPListen));
        result += address.c_str();
        result.push_back(static_cast<std::uint8_t>(0));
        result.appendNumber<quint16>(port);
        if (tls.read().exists()) {
            Util::ByteArray::Device dev(result);
            dev.open(QIODevice::WriteOnly);
            dev.seek(dev.size());
            QDataStream stream(&dev);
            stream << tls;
        }
        return result;
    }
};

class Target_LocalListen : public IOTarget {
    std::string name;

    class Device : public ListenWrapperDevice {
    public:
        std::unique_ptr<IO::Socket::Local::Server> server;

        Device() = default;

        virtual ~Device() = default;

        void start() override
        { }
    };

public:
    explicit Target_LocalListen(const Variant::Read &configuration) : IOTarget(configuration),
                                                                      name(configuration["Name"].toString())
    { }

    virtual ~Target_LocalListen() = default;

    bool isValid() const override
    { return !name.empty(); }

    bool matchesDevice(const IOTarget &other) const override
    {
        auto target = dynamic_cast<const Target_LocalListen *>(&other);
        if (!target)
            return false;
        return name == target->name;
    }

    std::unique_ptr<IOTarget> clone(bool = false) const override
    { return std::unique_ptr<IOTarget>(new Target_LocalListen(*this)); }

    std::unique_ptr<IOTarget::Device> backingDevice() const override
    {
        std::unique_ptr<Device> device(new Device);
        device->server
              .reset(new IO::Socket::Local::Server(
                      std::bind(&Device::accept, device.get(), std::placeholders::_1), name));
        if (!device->server->startListening())
            return {};
        return std::move(device);
    }

    Util::ByteArray serialize() const override
    {
        Util::ByteArray result;
        Util::ByteArray::Device dev(result);
        dev.open(QIODevice::WriteOnly);
        QDataStream stream(&dev);

        stream << static_cast<quint8>(SerializationID::LocalListen);

        stream << name;

        return result;
    }

    explicit Target_LocalListen(const Util::ByteView &data)
    {
        Util::ByteView::Device dev(data);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);

        stream >> name;
    }

    QString description(bool = false) const override
    {
        return QObject::tr("QtLocalListen=%1", "listen server description").arg(
                QString::fromStdString(name));
    }

    Variant::Root configuration(bool = false) const override
    {
        Variant::Root result;
        result["Type"] = "QtLocalListen";
        result["Name"].setString(name);
        return result;
    }

protected:
    Target_LocalListen(const Target_LocalListen &other) = default;

    Util::ByteArray localSocketUID() const override
    {
        Util::ByteArray result;
        result.push_back(static_cast<std::uint8_t>(SerializationID::LocalListen));
        result += name.c_str();
        return result;
    }
};

class Target_UnixListen : public IOTarget {
    std::string path;

    class Device : public ListenWrapperDevice {
    public:
        std::unique_ptr<IO::Socket::Unix::Server> server;

        Device() = default;

        virtual ~Device() = default;

        void start() override
        { }
    };

public:
    explicit Target_UnixListen(const Variant::Read &configuration) : IOTarget(configuration),
                                                                     path(configuration["File"].toString())
    { }

    virtual ~Target_UnixListen() = default;

    bool isValid() const override
    { return !path.empty(); }

    bool matchesDevice(const IOTarget &other) const override
    {
        auto target = dynamic_cast<const Target_UnixListen *>(&other);
        if (!target)
            return false;
        return path == target->path;
    }

    std::unique_ptr<IOTarget> clone(bool = false) const override
    { return std::unique_ptr<IOTarget>(new Target_UnixListen(*this)); }

    std::unique_ptr<IOTarget::Device> backingDevice() const override
    {
        std::unique_ptr<Device> device(new Device);
        device->server
              .reset(new IO::Socket::Unix::Server(
                      std::bind(&Device::accept, device.get(), std::placeholders::_1), path));
        if (!device->server->startListening())
            return {};
        return std::move(device);
    }

    Util::ByteArray serialize() const override
    {
        Util::ByteArray result;
        Util::ByteArray::Device dev(result);
        dev.open(QIODevice::WriteOnly);
        QDataStream stream(&dev);

        stream << static_cast<quint8>(SerializationID::UnixListen);

        stream << path;

        return result;
    }

    explicit Target_UnixListen(const Util::ByteView &data)
    {
        Util::ByteView::Device dev(data);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);

        stream >> path;
    }

    QString description(bool = false) const override
    {
        return QObject::tr("LocalListen=%1", "listen server description").arg(
                QString::fromStdString(path));
    }

    Variant::Root configuration(bool = false) const override
    {
        Variant::Root result;
        result["Type"] = "LocalListen";
        result["File"].setString(path);
        return result;
    }

protected:
    Target_UnixListen(const Target_UnixListen &other) = default;

    Util::ByteArray localSocketUID() const override
    {
        Util::ByteArray result;
        result.push_back(static_cast<std::uint8_t>(SerializationID::UnixListen));
        result += canonicalPath(path).c_str();
        return result;
    }
};

class Target_FileStream : public IOTarget {
    std::string readFile;
    std::string writeFile;
    std::string controlFile;
    std::pair<bool, bool> allowMissing;

    class Device : public IOTarget::Device {
        std::unique_ptr<IO::Generic::Stream> readStream;
        std::unique_ptr<IO::Generic::Stream> writeStream;
        std::unique_ptr<IO::Generic::Stream> controlStream;
        std::atomic_bool anyEnded;

        void receivedEnd()
        {
            if (anyEnded.exchange(true))
                return;
            ended();
        }

    public:
        Device(std::unique_ptr<IO::Generic::Stream> &&read,
               std::unique_ptr<IO::Generic::Stream> &&write,
               std::unique_ptr<IO::Generic::Stream> &&control) : readStream(std::move(read)),
                                                                 writeStream(std::move(write)),
                                                                 controlStream(std::move(control)),
                                                                 anyEnded(false)
        {
            if (readStream) {
                this->read = readStream->read;
                readStream->ended.connect(std::bind(&Device::receivedEnd, this));
            }
            if (controlStream) {
                this->control = controlStream->read;
                controlStream->ended.connect(std::bind(&Device::receivedEnd, this));
            }
            if (writeStream) {
                writeStall = writeStream->writeStall;
                writeStream->ended.connect(std::bind(&Device::receivedEnd, this));
            }
        }

        virtual ~Device()
        {
            readStream.reset();
            writeStream.reset();
            controlStream.reset();
        }

        bool isEnded() const override
        {
            if (readStream && readStream->isEnded())
                return true;
            if (writeStream && writeStream->isEnded())
                return true;
            if (controlStream && controlStream->isEnded())
                return true;
            return anyEnded.load();
        }

        void start() override
        {
            if (readStream)
                readStream->start();
            if (writeStream)
                writeStream->start();
            if (controlStream)
                controlStream->start();
        }

        void write(const Util::ByteView &data) override
        {
            if (!writeStream)
                return;
            return writeStream->write(data);
        }

        void write(const Util::ByteArray &data) override
        {
            if (!writeStream)
                return;
            return writeStream->write(data);
        }

        void write(Util::ByteArray &&data) override
        {
            if (!writeStream)
                return;
            return writeStream->write(std::move(data));
        }

        void write(const QByteArray &data) override
        {
            if (!writeStream)
                return;
            return writeStream->write(data);
        }

        void write(QByteArray &&data) override
        {
            if (!writeStream)
                return;
            return writeStream->write(std::move(data));
        }
    };

public:
    explicit Target_FileStream(const Variant::Read &configuration) : IOTarget(configuration),
                                                                     readFile(
                                                                             configuration["Input"].toString()),
                                                                     writeFile(
                                                                             configuration["Output"]
                                                                                     .toString()),
                                                                     controlFile(
                                                                             configuration["Echo"].toString()),
                                                                     allowMissing(false, false)
    {
        if (configuration["AllowMissing"].exists()) {
            allowMissing.second = true;
            allowMissing.first = configuration["AllowMissing"].toBoolean();
        }
    }

    virtual ~Target_FileStream() = default;

    bool isValid() const override
    { return !readFile.empty() || !writeFile.empty() || !controlFile.empty(); }

    bool matchesDevice(const IOTarget &other) const override
    {
        auto target = dynamic_cast<const Target_FileStream *>(&other);
        if (!target)
            return false;
        return readFile == target->readFile &&
                writeFile == target->writeFile &&
                controlFile == target->controlFile;
    }

    std::unique_ptr<IOTarget> clone(bool deviceOnly = false) const override
    {
        std::unique_ptr<Target_FileStream> result(new Target_FileStream(*this));
        if (deviceOnly) {
            result->allowMissing.second = false;
        }
        return std::move(result);
    }

    std::unique_ptr<IOTarget::Device> backingDevice() const override
    {
        bool requireAll = true;
        if (allowMissing.second && allowMissing.first)
            requireAll = false;

        std::unique_ptr<IO::Generic::Stream> read;
        if (!readFile.empty()) {
            IO::File::Mode mode = IO::File::Mode::readOnly();
            mode.access = IO::File::Mode::Access::NonBlocking;
            mode.create = false;
            read = IO::Access::file(readFile, mode)->stream();

            if (!read && requireAll)
                return {};
        }

        std::unique_ptr<IO::Generic::Stream> control;
        if (!controlFile.empty()) {
            IO::File::Mode mode = IO::File::Mode::readOnly();
            mode.access = IO::File::Mode::Access::NonBlocking;
            mode.create = false;
            control = IO::Access::file(controlFile, mode)->stream();

            if (!control && requireAll)
                return {};
        }

        std::unique_ptr<IO::Generic::Stream> write;
        if (!writeFile.empty()) {
            IO::File::Mode mode = IO::File::Mode::writeOnly();
            mode.access = IO::File::Mode::Access::NonBlocking;
            mode.create = false;
            write = IO::Access::file(writeFile, mode)->stream();

            if (!write && requireAll)
                return {};
        }

        if (!read && !write && !control)
            return {};

        return std::unique_ptr<IOTarget::Device>(
                new Device(std::move(read), std::move(write), std::move(control)));
    }

    void merge(const IOTarget &baseOverlay, IOTarget::Device * = nullptr) override
    {
        auto overlay = dynamic_cast<const Target_FileStream *>(&baseOverlay);
        if (!overlay) {
            qCDebug(log_acquisition_iotarget) << "Attempt to merge incompatible"
                                              << baseOverlay.description() << "into"
                                              << description();
            return;
        }

        if (overlay->allowMissing.second) {
            allowMissing = overlay->allowMissing;
        }
    }

    Util::ByteArray serialize() const override
    {
        Util::ByteArray result;
        Util::ByteArray::Device dev(result);
        dev.open(QIODevice::WriteOnly);
        QDataStream stream(&dev);

        stream << static_cast<quint8>(SerializationID::FileStream);

        stream << readFile << writeFile << controlFile;
        stream << allowMissing.first << allowMissing.second;

        return result;
    }

    explicit Target_FileStream(const Util::ByteView &data) : allowMissing(false, false)
    {
        Util::ByteView::Device dev(data);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);

        stream >> readFile >> writeFile >> controlFile;
        stream >> allowMissing.first >> allowMissing.second;
    }

    QString description(bool = false) const override
    {
        return QObject::tr("Pipe=%1:%2:%3", "pipe description").arg(
                QString::fromStdString(readFile), QString::fromStdString(writeFile),
                QString::fromStdString(controlFile));
    }

    Variant::Root configuration(bool deviceOnly = false) const override
    {
        Variant::Root result;
        result["Type"] = "Pipe";
        if (!readFile.empty()) {
            result["Input"].setString(readFile);
        }
        if (!writeFile.empty()) {
            result["Output"].setString(writeFile);
        }
        if (!controlFile.empty()) {
            result["Echo"].setString(controlFile);
        }
        if (allowMissing.second) {
            result["AllowMissing"].setBoolean(allowMissing.first);
        }
        return result;
    }

protected:
    Target_FileStream(const Target_FileStream &other) = default;

    Util::ByteArray localSocketUID() const override
    {
        Util::ByteArray result;
        result.push_back(static_cast<std::uint8_t>(SerializationID::FileStream));
        result += canonicalPath(readFile).c_str();
        result.push_back(static_cast<std::uint8_t>(0));
        result += canonicalPath(writeFile).c_str();
        result.push_back(static_cast<std::uint8_t>(0));
        result += canonicalPath(controlFile).c_str();
        result.push_back(static_cast<std::uint8_t>(0));
        return result;
    }
};

class Target_Process : public IOTarget {
    std::string command;
    std::vector<std::string> arguments;
    bool errorAsControl;

    class Device : public IOTarget::Device {
        std::shared_ptr<IO::Process::Instance> process;
        std::unique_ptr<IO::Generic::Stream> readStream;
        std::unique_ptr<IO::Generic::Stream> controlStream;
        std::unique_ptr<IO::Generic::Stream> writeStream;

    public:
        Device(std::shared_ptr<IO::Process::Instance> proc, bool errorAsControl) : process(
                std::move(proc))
        {
            readStream = process->outputStream();
            writeStream = process->inputStream();
            if (errorAsControl) {
                controlStream = process->errorStream();
            }

            process->exited.connect([this](int, bool) {
                this->ended();
            });
            if (readStream) {
                read = readStream->read;
            }
            if (controlStream) {
                control = controlStream->read;
            }
            if (writeStream) {
                writeStall = writeStream->writeStall;
            }
        }

        virtual ~Device()
        {
            writeStream.reset();
            readStream.reset();
            controlStream.reset();

            if (!process)
                return;
            process->terminate();
            process->wait(10.0);
            process->kill();
            process.reset();
        }

        bool isEnded() const override
        {
            if (!process)
                return true;
            return !process->isRunning();
        }

        void start() override
        {
            if (!process->start()) {
                writeStream.reset();
                readStream.reset();
                controlStream.reset();
                process.reset();

                ended();
                return;
            }

            if (readStream) {
                readStream->start();
            }
            if (controlStream) {
                controlStream->start();
            }
            if (writeStream) {
                writeStream->start();
            }
        }

        void write(const Util::ByteView &data) override
        {
            if (!writeStream)
                return;
            return writeStream->write(data);
        }

        void write(const Util::ByteArray &data) override
        {
            if (!writeStream)
                return;
            return writeStream->write(data);
        }

        void write(Util::ByteArray &&data) override
        {
            if (!writeStream)
                return;
            return writeStream->write(std::move(data));
        }

        void write(const QByteArray &data) override
        {
            if (!writeStream)
                return;
            return writeStream->write(data);
        }

        void write(QByteArray &&data) override
        {
            if (!writeStream)
                return;
            return writeStream->write(std::move(data));
        }
    };

public:
    explicit Target_Process(const Variant::Read &configuration) : IOTarget(configuration),
                                                                  command(configuration["Command"].toString()),
                                                                  errorAsControl(
                                                                          configuration["ErrorStreamAsEcho"]
                                                                                  .toBoolean())
    {
        if (configuration["Arguments"].getType() != Variant::Type::Array) {
            arguments = Util::split_quoted(command);
            if (!arguments.empty()) {
                command = std::move(arguments.front());
                arguments.erase(arguments.begin());
            }
        } else {
            for (auto arg : configuration["Arguments"].toArray()) {
                arguments.emplace_back(arg.toString());
            }
        }
    }

    bool isValid() const override
    { return !command.empty(); }

    bool matchesDevice(const IOTarget &other) const override
    {
        auto target = dynamic_cast<const Target_Process *>(&other);
        if (!target)
            return false;
        return command == target->command &&
                arguments == target->arguments &&
                errorAsControl == target->errorAsControl;
    }

    std::unique_ptr<IOTarget> clone(bool = false) const override
    { return std::unique_ptr<IOTarget>(new Target_Process(*this)); }

    std::unique_ptr<IOTarget::Device> backingDevice() const override
    {
        IO::Process::Spawn spawn(command, arguments);
        if (errorAsControl) {
            spawn.errorStream = IO::Process::Spawn::StreamMode::Capture;
        }
        auto proc = spawn.create();
        if (!proc)
            return {};
        return std::unique_ptr<IOTarget::Device>(new Device(std::move(proc), errorAsControl));
    }

    Util::ByteArray serialize() const override
    {
        Util::ByteArray result;
        Util::ByteArray::Device dev(result);
        dev.open(QIODevice::WriteOnly);
        QDataStream stream(&dev);

        stream << static_cast<quint8>(SerializationID::Process);
        stream << command << arguments << errorAsControl;

        return result;
    }

    explicit Target_Process(const Util::ByteView &data)
    {
        Util::ByteView::Device dev(data);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);

        stream >> command >> arguments >> errorAsControl;
    }

    QString description(bool deviceOnly = false) const override
    {
        QStringList argList;
        argList += QString::fromStdString(command);
        if (!deviceOnly) {
            for (const auto &arg : arguments) {
                argList += QString::fromStdString(arg);
            }
        }
        return QObject::tr("Command='%1'", "process description").arg(argList.join(" "));
    }

    Variant::Root configuration(bool = false) const override
    {
        Variant::Root result;
        result["Type"] = "Command";
        result["Command"].setString(command);
        for (const auto &arg : arguments) {
            result["Arguments"].toArray().after_back().setString(arg);
        }
        result["ErrorStreamAsEcho"].setBoolean(errorAsControl);
        return result;
    }

protected:
    Target_Process(const Target_Process &other) = default;

    Util::ByteArray localSocketUID() const override
    {
        Util::ByteArray result;
        result.push_back(static_cast<std::uint8_t>(SerializationID::Process));
        result += command.c_str();
        result.push_back(static_cast<std::uint8_t>(0));
        result.push_back(
                errorAsControl ? static_cast<std::uint8_t>(1) : static_cast<std::uint8_t>(0));
        for (const auto &arg : arguments) {
            result += arg.c_str();
            result.push_back(static_cast<std::uint8_t>(0));
        }
        return result;
    }
};

class Target_Multiplexer : public IOTarget {
    std::unordered_set<std::string> readTags;
    std::unordered_set<std::string> controlTags;
    std::unordered_set<std::string> writeTags;

    struct Interface {
        std::unique_ptr<IOTarget> target;
        std::unordered_set<std::string> readTags;
        std::unordered_set<std::string> controlTags;
        std::unordered_set<std::string> writeTags;

        Interface() = default;

        explicit Interface(const Variant::Read &configuration) : target(
                IOTarget::create(configuration)),
                                                                 readTags(
                                                                         configuration["Channels/Input"]
                                                                                 .toFlags()),
                                                                 controlTags(
                                                                         configuration["Channels/Echo"]
                                                                                 .toFlags()),
                                                                 writeTags(
                                                                         configuration["Channels/Output"]
                                                                                 .toFlags())
        { }

        Interface(Interface &&) = default;

        Interface &operator=(Interface &&) = default;

        bool matchesDevice(const Interface &other) const
        {
            if (readTags != other.readTags)
                return false;
            if (controlTags != other.controlTags)
                return false;
            if (writeTags != other.writeTags)
                return false;
            return target->matchesDevice(*other.target);
        }
    };

    std::vector<Interface> interfaces;

    static void addTagsForUID(const std::unordered_set<std::string> &tags, Util::ByteArray &result)
    {
        std::vector<std::string> sorted(tags.begin(), tags.end());
        std::sort(sorted.begin(), sorted.end());
        result.appendNumber<std::uint32_t>(sorted.size());
        for (const auto &add : sorted) {
            result += add.c_str();
            result.push_back(static_cast<std::uint8_t>(0));
        }
    }

    class Device : public IOTarget::Device {
        std::vector<std::unique_ptr<IOInterface>> interfaces;
        std::vector<IOInterface *> writeTargets;

        static bool tagsIntersect(const std::unordered_set<std::string> &first,
                                  const std::unordered_set<std::string> &second)
        {
            for (const auto &check : first) {
                if (second.count(check))
                    return true;
            }
            return false;
        }

    public:
        Device() = default;

        virtual ~Device() = default;

        void addInterface(const Target_Multiplexer &mux, const Interface &add)
        {
            std::unique_ptr<IOInterface> ioif(new IOInterface(add.target->clone()));

            if (tagsIntersect(mux.readTags, add.readTags)) {
                ioif->read.connect([this](const Util::ByteArray &data) {
                    this->read(data);
                });
            }
            if (tagsIntersect(mux.readTags, add.controlTags)) {
                ioif->otherControl.connect([this](const Util::ByteArray &data) {
                    this->read(data);
                });
            }

            if (tagsIntersect(mux.controlTags, add.readTags)) {
                ioif->read.connect([this](const Util::ByteArray &data) {
                    this->control(data);
                });
            }
            if (tagsIntersect(mux.controlTags, add.controlTags)) {
                ioif->otherControl.connect([this](const Util::ByteArray &data) {
                    this->control(data);
                });
            }

            if (tagsIntersect(mux.writeTags, add.writeTags)) {
                writeTargets.emplace_back(ioif.get());
            }

            interfaces.emplace_back(std::move(ioif));
        }

        bool isEnded() const override
        { return false; }

        void start() override
        {
            for (const auto &ioif : interfaces) {
                ioif->start();
            }
        }

        void write(const Util::ByteView &data) override
        {
            for (auto dev : writeTargets) {
                dev->write(data);
            }
        }

        void write(const Util::ByteArray &data) override
        {
            for (auto dev : writeTargets) {
                dev->write(data);
            }
        }

        void write(Util::ByteArray &&data) override
        {
            if (writeTargets.empty())
                return;
            auto dev = writeTargets.begin();
            for (auto endDev = writeTargets.end() - 1; dev != endDev; ++dev) {
                (*dev)->write(data);
            }
            return (*dev)->write(std::move(data));
        }

        void write(const QByteArray &data) override
        {
            for (const auto &dev : writeTargets) {
                dev->write(data);
            }
        }

        void write(QByteArray &&data) override
        {
            if (writeTargets.empty())
                return;
            auto dev = writeTargets.begin();
            for (auto endDev = writeTargets.end() - 1; dev != endDev; ++dev) {
                (*dev)->write(data);
            }
            return (*dev)->write(std::move(data));
        }
    };

public:
    explicit Target_Multiplexer(const Variant::Read &configuration) : IOTarget(configuration),
                                                                      readTags(
                                                                              configuration["Targets/Input"]
                                                                                      .toFlags()),
                                                                      controlTags(
                                                                              configuration["Targets/Echo"]
                                                                                      .toFlags()),
                                                                      writeTags(
                                                                              configuration["Targets/Output"]
                                                                                      .toFlags())
    {
        for (auto add : configuration["Interfaces"].toChildren()) {
            Interface i(add);
            if (!i.target)
                continue;
            if (i.readTags.empty() && i.controlTags.empty() && i.writeTags.empty())
                continue;
            interfaces.emplace_back(std::move(i));
        }
    }

    bool isValid() const override
    {
        if (readTags.empty() && writeTags.empty() && controlTags.empty())
            return false;
        return !interfaces.empty();
    }

    bool integratedFraming() const override
    {
        for (const auto &check : interfaces) {
            if (!check.target->integratedFraming())
                return false;
        }
        return true;
    }

    bool matchesDevice(const IOTarget &other) const override
    {
        auto target = dynamic_cast<const Target_Multiplexer *>(&other);
        if (!target)
            return false;
        if (readTags != target->readTags)
            return false;
        if (controlTags != target->controlTags)
            return false;
        if (writeTags != target->writeTags)
            return false;
        if (interfaces.size() != target->interfaces.size())
            return false;
        std::list<const Interface *> remaining;
        for (const auto &add : interfaces) {
            remaining.emplace_back(&add);
        }
        for (const auto &check : target->interfaces) {
            bool hit = false;
            for (auto r = remaining.begin(); r != remaining.end(); ++r) {
                if (!(*r)->matchesDevice(check))
                    continue;
                remaining.erase(r);
                hit = true;
                break;
            }
            if (!hit)
                return false;
        }
        return true;
    }

    std::unique_ptr<IOTarget> clone(bool deviceOnly = false) const override
    { return std::unique_ptr<IOTarget>(new Target_Multiplexer(*this, deviceOnly)); }

    std::unique_ptr<IOTarget::Device> backingDevice() const override
    {
        std::unique_ptr<Device> dev(new Device);
        for (const auto &add : interfaces) {
            dev->addInterface(*this, add);
        }
        return std::move(dev);
    }

    Util::ByteArray serialize() const override
    {
        Util::ByteArray result;
        Util::ByteArray::Device dev(result);
        dev.open(QIODevice::WriteOnly);
        QDataStream stream(&dev);

        stream << static_cast<quint8>(SerializationID::Multiplexer);
        stream << readTags << controlTags << writeTags;

        Serialize::container(stream, interfaces, [&stream](const Interface &i) {
            stream << i.readTags << i.controlTags << i.writeTags;
            stream << i.target->serialize().toQByteArrayRef();
        });

        return result;
    }

    explicit Target_Multiplexer(const Util::ByteView &data)
    {
        Util::ByteView::Device dev(data);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);

        stream >> readTags >> controlTags >> writeTags;
        Deserialize::container(stream, interfaces, [&stream]() -> Interface {
            Interface i;
            stream >> i.readTags >> i.controlTags >> i.writeTags;
            QByteArray td;
            stream >> td;
            i.target = IOTarget::deserialize(Util::ByteView(td));
            return std::move(i);
        });
    }

    QString description(bool deviceOnly = false) const override
    {
        QStringList components;
        for (const auto &arg : interfaces) {
            components += arg.target->description(deviceOnly);
        }
        return QObject::tr("Multiplexer='%1'", "multiplexer description").arg(components.join(" "));
    }

    Variant::Root configuration(bool deviceOnly = false) const override
    {
        Variant::Root result;
        result["Type"] = "Multiplexer";
        result["Targets/Input"].setFlags(readTags);
        result["Targets/Echo"].setFlags(controlTags);
        result["Targets/Output"].setFlags(writeTags);
        for (const auto &add : interfaces) {
            auto cfgTarget = result["Interfaces"].toArray().after_back();
            cfgTarget.set(add.target->configuration(deviceOnly));
            cfgTarget["Channels/Input"].setFlags(add.readTags);
            cfgTarget["Channels/Echo"].setFlags(add.controlTags);
            cfgTarget["Channels/Output"].setFlags(add.writeTags);
        }
        return result;
    }

protected:
    Target_Multiplexer(const Target_Multiplexer &other, bool deviceOnly = false) : readTags(
            other.readTags), controlTags(other.controlTags), writeTags(other.writeTags)
    {
        for (const auto &dup : other.interfaces) {
            Interface d;
            d.readTags = dup.readTags;
            d.controlTags = dup.controlTags;
            d.writeTags = dup.writeTags;
            d.target = dup.target->clone(deviceOnly);
            interfaces.emplace_back(std::move(d));
        }
    }

    Util::ByteArray localSocketUID() const override
    {
        Util::ByteArray result;
        result.push_back(static_cast<std::uint8_t>(SerializationID::Multiplexer));
        addTagsForUID(readTags, result);
        addTagsForUID(controlTags, result);
        addTagsForUID(writeTags, result);

        result.appendNumber<std::uint32_t>(interfaces.size());
        for (const auto &add : interfaces) {
            addTagsForUID(add.readTags, result);
            addTagsForUID(add.controlTags, result);
            addTagsForUID(add.writeTags, result);
            result += add.target->localSocketUID();
        }
        return result;
    }
};

class Target_URL : public IOTarget {
    QUrl url;
    enum class Method : std::uint8_t {
        GET, PUT, POST,
    } method;
    std::pair<bool, bool> disableFraming;

    class Device : public IOTarget::Device {
        std::function<IO::Access::Handle(const Util::ByteView &)> submit;
        std::mutex mutex;
        std::condition_variable notify;
        std::vector<std::unique_ptr<IO::Generic::Stream>> pendingResponses;
        std::thread thread;
        bool terminated;
        bool resetPending;

        void updateStall(std::size_t before, std::size_t after)
        {
            static constexpr std::size_t stallThreshold = 2;
            bool wasStalled = before > stallThreshold;
            bool isStalled = after > stallThreshold;
            if (wasStalled) {
                if (!isStalled) {
                    writeStall(false);
                }
            } else {
                if (isStalled) {
                    writeStall(true);
                }
            }
        }

        void run()
        {
            std::vector<std::unique_ptr<IO::Generic::Stream>> activeResponses;
            std::vector<IO::Generic::Stream *> endedResponses;
            std::unique_lock<std::mutex> lock(mutex);
            for (;;) {
                if (!lock)
                    lock.lock();
                if (terminated)
                    return;
                if (!endedResponses.empty()) {
                    auto remove = std::move(endedResponses);
                    endedResponses.clear();
                    lock.unlock();

                    auto countBefore = activeResponses.size();
                    for (auto resp : remove) {
                        for (auto i = activeResponses.begin(); i != activeResponses.end();) {
                            if (i->get() == resp) {
                                i = activeResponses.erase(i);
                                break;
                            }
                            ++i;
                        }
                    }
                    auto countAfter = activeResponses.size();
                    updateStall(countBefore, countAfter);
                    continue;
                }
                if (resetPending) {
                    resetPending = false;
                    lock.unlock();
                    auto countBefore = activeResponses.size();
                    activeResponses.clear();
                    updateStall(countBefore, 0);
                    continue;
                }
                if (!pendingResponses.empty()) {
                    auto add = std::move(pendingResponses);
                    pendingResponses.clear();
                    lock.unlock();

                    auto countBefore = activeResponses.size();
                    for (auto &resp : add) {
                        auto buffer = std::make_shared<Util::ByteArray>();
                        auto ptr = resp.get();

                        resp->read.connect([this, buffer](const Util::ByteArray &data) {
                            static constexpr std::size_t maximumBuffer = 16 * 1024 * 1024;

                            *buffer += std::move(data);
                            if (buffer->size() > maximumBuffer) {
                                read(*buffer);
                                buffer->clear();
                            }
                        });
                        resp->ended.connect([this, ptr, buffer, &endedResponses] {
                            if (!buffer->empty()) {
                                read(*buffer);
                                buffer->clear();
                            }
                            {
                                std::lock_guard<std::mutex> lock(mutex);
                                endedResponses.emplace_back(ptr);
                            }
                            notify.notify_all();
                        });

                        resp->start();
                        activeResponses.emplace_back(std::move(resp));
                    }
                    auto countAfter = activeResponses.size();
                    updateStall(countBefore, countAfter);
                    continue;
                }
                notify.wait(lock);
            }
        }

    public:
        Device(std::function<IO::Access::Handle(const Util::ByteView &)> submit) : submit(
                std::move(submit)), terminated(false)
        { }

        virtual ~Device()
        {
            {
                std::lock_guard<std::mutex> lock(mutex);
                terminated = true;
            }
            notify.notify_all();
            if (thread.joinable())
                thread.join();
        }

        bool isEnded() const override
        { return false; }

        void start() override
        {
            Q_ASSERT(!thread.joinable());
            thread = std::thread(std::bind(&Device::run, this));
        }

        void write(const Util::ByteView &data) override
        {
            auto submitted = submit(data);
            if (!submitted)
                return;
            auto response = submitted->stream();
            if (!response)
                return;

            {
                std::lock_guard<std::mutex> lock(mutex);
                pendingResponses.emplace_back(std::move(response));
            }
            notify.notify_all();
        }

        void reset() override
        {
            {
                std::lock_guard<std::mutex> lock(mutex);
                pendingResponses.clear();
                resetPending = true;
            }
            notify.notify_all();
        }
    };

public:
    explicit Target_URL(const Variant::Read &configuration) : IOTarget(configuration),
                                                              url(QUrl(
                                                                      configuration["URL"].toQString())),
                                                              method(Method::GET),
                                                              disableFraming(false, false)
    {
        {
            const auto &methodName = configuration["Method"].toString();
            if (Util::equal_insensitive(methodName, "PUT")) {
                method = Method::PUT;
            } else if (Util::equal_insensitive(methodName, "POST")) {
                method = Method::POST;
            } else {
                method = Method::GET;
            }
        }
        if (configuration["DisableFraming"].exists()) {
            disableFraming.second = true;
            disableFraming.first = configuration["DisableFraming"].toBoolean();
        }
    }

    virtual ~Target_URL() = default;

    bool isValid() const override
    { return url.isValid(); }

    bool integratedFraming() const override
    {
        if (!disableFraming.second)
            return true;
        return !disableFraming.first;
    }

    bool matchesDevice(const IOTarget &other) const override
    {
        auto target = dynamic_cast<const Target_URL *>(&other);
        if (!target)
            return false;
        return url == target->url;
    }

    std::unique_ptr<IOTarget> clone(bool deviceOnly = false) const override
    {
        auto result = std::unique_ptr<Target_URL>(new Target_URL(*this));
        if (deviceOnly) {
            result->disableFraming.second = false;
        }
        return std::move(result);
    }

    std::unique_ptr<IOTarget::Device> backingDevice() const override
    {
        auto context = std::make_shared<IO::URL::Context>();
        QUrl target = url;
        switch (method) {
        case Method::GET:
            return std::unique_ptr<Device>(new Device([target, context](const Util::ByteView &) {
                return IO::Access::urlGET(target, context);
            }));
        case Method::PUT:
            return std::unique_ptr<Device>(new Device([target, context](const Util::ByteView &up) {
                return IO::Access::urlPUT(target, IO::Access::buffer(Util::ByteArray(up)), context);
            }));
        case Method::POST:
            return std::unique_ptr<Device>(new Device([target, context](const Util::ByteView &up) {
                return IO::Access::urlPOST(target, IO::Access::buffer(Util::ByteArray(up)),
                                           context);
            }));
        }
        Q_ASSERT(false);
        return {};
    }

    void merge(const IOTarget &baseOverlay, IOTarget::Device *device = nullptr) override
    {
        auto overlay = dynamic_cast<const Target_URL *>(&baseOverlay);
        if (!overlay) {
            qCDebug(log_acquisition_iotarget) << "Attempt to merge incompatible"
                                              << baseOverlay.description() << "into"
                                              << description();
            return;
        }

        if (overlay->disableFraming.second) {
            disableFraming = overlay->disableFraming;
        }
    }

    Util::ByteArray serialize() const override
    {
        Util::ByteArray result;
        Util::ByteArray::Device dev(result);
        dev.open(QIODevice::WriteOnly);
        QDataStream stream(&dev);

        stream << static_cast<quint8>(SerializationID::URL);

        stream << url;
        stream << static_cast<quint8>(method);
        stream << disableFraming.first << disableFraming.second;

        return result;
    }

    explicit Target_URL(const Util::ByteView &data)
    {
        Util::ByteView::Device dev(data);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);

        stream >> url;
        {
            quint8 i = 0;
            stream >> i;
            method = static_cast<Method>(i);
        }
        stream >> disableFraming.first >> disableFraming.second;
    }

    QString description(bool = false) const override
    {
        return QObject::tr("%1", "URL description").arg(url.toDisplayString(
                QUrl::FormattingOptions(QUrl::PrettyDecoded) | QUrl::RemovePassword));
    }

    Variant::Root configuration(bool deviceOnly = false) const override
    {
        Variant::Root result;
        result["Type"] = "URL";
        result["URL"] = url.toString();
        switch (method) {
        case Method::GET:
            result["Method"] = "GET";
            break;
        case Method::PUT:
            result["Method"] = "PUT";
            break;
        case Method::POST:
            result["Method"] = "POST";
            break;
        }
        if (disableFraming.second && !deviceOnly) {
            result["DisableFraming"].setBoolean(disableFraming.first);
        }
        return result;
    }

protected:
    Target_URL(const Target_URL &other) = default;

    Util::ByteArray localSocketUID() const override
    {
        Util::ByteArray result;
        result.push_back(static_cast<std::uint8_t>(SerializationID::URL));
        result += url.toString().toStdString();
        result.push_back(static_cast<std::uint8_t>(0));
        result.push_back(static_cast<std::uint8_t>(method));
        return result;
    }
};

}


std::unique_ptr<IOTarget> IOTarget::create(const std::string &description)
{ return std::unique_ptr<IOTarget>(new Target_Serial(description)); }

std::unique_ptr<IOTarget> IOTarget::create(const Variant::Read &configuration)
{
    if (configuration.getType() == Variant::Type::String) {
        return std::unique_ptr<IOTarget>(new Target_Serial(configuration.toString()));
    }

    const auto &type = configuration["Type"].toString();
    if (Util::equal_insensitive(type, "CPD3Server")) {
        return std::unique_ptr<IOTarget>(new Target_Remote(configuration));
    } else if (Util::equal_insensitive(type, "RemoteServer")) {
        return std::unique_ptr<IOTarget>(new Target_TCPSocket(configuration));
    } else if (Util::equal_insensitive(type, "QtLocalSocket")) {
        return std::unique_ptr<IOTarget>(new Target_LocalSocket(configuration));
    } else if (Util::equal_insensitive(type, "LocalSocket")) {
        return std::unique_ptr<IOTarget>(new Target_UnixSocket(configuration));
    } else if (Util::equal_insensitive(type, "UDP")) {
        return std::unique_ptr<IOTarget>(new Target_UDP(configuration));
    } else if (Util::equal_insensitive(type, "TCPListen")) {
        return std::unique_ptr<IOTarget>(new Target_TCPListen(configuration));
    } else if (Util::equal_insensitive(type, "QtLocalListen")) {
        return std::unique_ptr<IOTarget>(new Target_LocalListen(configuration));
    } else if (Util::equal_insensitive(type, "LocalListen")) {
        return std::unique_ptr<IOTarget>(new Target_UnixListen(configuration));
    } else if (Util::equal_insensitive(type, "Pipe")) {
        return std::unique_ptr<IOTarget>(new Target_FileStream(configuration));
    } else if (Util::equal_insensitive(type, "Command")) {
        return std::unique_ptr<IOTarget>(new Target_Process(configuration));
    } else if (Util::equal_insensitive(type, "Multiplexer")) {
        return std::unique_ptr<IOTarget>(new Target_Multiplexer(configuration));
    } else if (Util::equal_insensitive(type, "URL")) {
        return std::unique_ptr<IOTarget>(new Target_URL(configuration));
    }

    return std::unique_ptr<IOTarget>(new Target_Serial(configuration));
}

std::vector<std::unique_ptr<IOTarget>> IOTarget::enumerateLocal()
{
    std::vector<std::unique_ptr<IOTarget>> result;
    for (const auto &add : IO::Serial::enumeratePorts()) {
        std::unique_ptr<Target_Serial> target(new Target_Serial);
        target->device = add;
        result.emplace_back(std::move(target));
    }
    return std::move(result);
}

std::unique_ptr<IOTarget> IOTarget::deserialize(const Util::ByteView &data)
{
    if (data.empty())
        return {};
    switch (static_cast<SerializationID>(data.front())) {
    case SerializationID::Serial:
        return std::unique_ptr<IOTarget>(new Target_Serial(data.mid(1)));
    case SerializationID::TCPSocket:
        return std::unique_ptr<IOTarget>(new Target_TCPSocket(data.mid(1)));
    case SerializationID::LocalSocket:
        return std::unique_ptr<IOTarget>(new Target_LocalSocket(data.mid(1)));
    case SerializationID::UnixSocket:
        return std::unique_ptr<IOTarget>(new Target_UnixSocket(data.mid(1)));
    case SerializationID::UDP:
        return std::unique_ptr<IOTarget>(new Target_UDP(data.mid(1)));
    case SerializationID::TCPListen:
        return std::unique_ptr<IOTarget>(new Target_TCPListen(data.mid(1)));
    case SerializationID::LocalListen:
        return std::unique_ptr<IOTarget>(new Target_LocalListen(data.mid(1)));
    case SerializationID::UnixListen:
        return std::unique_ptr<IOTarget>(new Target_UnixListen(data.mid(1)));
    case SerializationID::FileStream:
        return std::unique_ptr<IOTarget>(new Target_FileStream(data.mid(1)));
    case SerializationID::Process:
        return std::unique_ptr<IOTarget>(new Target_Process(data.mid(1)));
    case SerializationID::Multiplexer:
        return std::unique_ptr<IOTarget>(new Target_Multiplexer(data.mid(1)));
    case SerializationID::URL:
        return std::unique_ptr<IOTarget>(new Target_URL(data.mid(1)));
    default:
        break;
    }
    return {};
}

}
}