/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITIONIOINTERFACE_H
#define CPD3ACQUISITIONIOINTERFACE_H

#include "core/first.hxx"

#include <mutex>
#include <thread>
#include <condition_variable>
#include <cstdint>
#include <chrono>

#include "acquisition/acquisition.hxx"
#include "datacore/variant/root.hxx"
#include "io/access.hxx"
#include "io/socket.hxx"
#include "core/threading.hxx"
#include "core/util.hxx"

namespace CPD3 {
namespace Acquisition {

class IOTarget;

/**
 * The abstract interface used to communicate with various I/O backends.  This
 * supports serial ports, sockets and similar interfaces.  It is used to talk
 * to a backend server and abstracts the process of having multiple interfaces
 * communicate with the same physical hardware.  Unless specified explicitly
 * as a remote target server, the interface will attempt to spawn a local
 * server if it can't connect to the one for the backend it is using.  This
 * spawning will attempt to create a detached process running for the backend.
 * <br>
 * An I/O interface has both a conventional read and write channel as well
 * as a "control" echo channel.  The control channel contains any data
 * written by other interfaces connected to the same physical hardware.  This
 * allows acquisition interfaces to attempt to parse commands being issued
 * by other entities so they maintain coherent state.
 * <br>
 * The interface can also request "exclusive" mode.  When in exclusive mode,
 * only data sent from this interface will actually be sent to the physical
 * backend.  Data sent through other interfaces will be discarded.
 * <br>
 * Interfaces allow for reconfiguration while they are running, allowing, for
 * example, the baud rate of a serial port to be changed.
 */
class CPD3ACQUISITION_EXPORT IOInterface final : public IO::Generic::Stream {
    std::mutex mutex;
    std::condition_variable notify;
    std::thread thread;
    bool terminated;
    enum class ConnectionState {
        Initialize, Connecting, Connected, LockPending, LockHeld, Shutdown,
    } connectionState;

    std::unique_ptr<IOTarget> clientSideTarget;

    Util::ByteArray controlPending;
    Util::ByteArray writePending;
    Util::ByteArray readPending;
    bool connectionEnded;

    Util::ByteArray processRead;

    using Clock = std::chrono::steady_clock;
    bool expectingPong;
    Clock::time_point pongTimeout;

    class ConnectionLock;

    std::unique_ptr<IO::Socket::Connection> establishConnection(const IOTarget &target);

    void requeueReadProcessing();

    bool handleNextPacket();

    void run();

public:
    IOInterface() = delete;

    /**
     * Create the interface.
     *
     * @param target    the target to use
     */
    explicit IOInterface(std::unique_ptr<IOTarget> &&target);

    virtual ~IOInterface();

    void write(const Util::ByteView &data) override;

    using IO::Generic::Stream::write;

    bool isEnded() const override;

    void start() override;

    /**
     * Emitted when the target produces control data from any source other than this
     * interface.
     */
    Threading::Signal<const Util::ByteArray &> otherControl;

    /**
     * Emitted when the target produces control data from any source including this
     * interface.
     */
    Threading::Signal<const Util::ByteArray &> anyControl;

    /**
     * Emitted when the backing device has been closed.
     */
    Threading::Signal<> deviceClosed;


    /**
     * Create a copy of the interface.
     *
     * @param deviceOnly    only copy the device and none of the configuration parameters
     * @return              a copy of the interface
     * @see IOTarget::clone(bool) const
     */
    std::unique_ptr<IOTarget> target(bool deviceOnly = false);

    /**
     * Get a human readable description of the interface target.
     *
     * @param deviceOnly    only describe the device
     * @return              the interface description
     * @see IOTarget::description(bool) const
     */
    QString description(bool deviceOnly = false);

    /**
     * Get the configuration of the interface target.
     *
     * @param deviceOnly    only return the configuration for the device
     * @return              the interface target configuration
     * @see IOTarget::configuration(bool) const
     */
    Data::Variant::Root configuration(bool deviceOnly = false);

    /**
     * Test if the device should re-trigger autoprobing when it is closed.  That is, test
     * if this target is a local interface subject to external close sources (e.g. a serial
     * port due to kernel reconfiguration).
     *
     * @return  true if the interface should re-run autoprobing when it is closed
     * @see IOTarget::triggerAutoprobeWhenClosed() const
     */
    bool triggerAutoprobeWhenClosed();

    /**
     * Test if the device has a concept of integrated framing that should be passed on to
     * upper layers.  For example, UDP packets as complete frames.
     *
     * @return  true if the interface should indicate that it has a native framing
     * @see IOTarget::integratedFraming() const
     */
    bool integratedFraming();

    /**
     * Set the interface to exclusive mode.  While in exclusive mode, any writes from other
     * interfaces connected to the same server are ignored and no reads are directed to any
     * other interface.
     *
     * @param enable    enable exclusive mode
     */
    void setExclusive(bool enable = true);

    /**
     * Merge and overlay another target into the current one.  This is used, for example, to
     * change serial port baud rates.
     *
     * @param overlay   the target to overlay
     * @see IOTarget::(const IOTarget &, IOTarget::Device *)
     */
    void merge(const IOTarget &overlay);

    /**
     * Request a reset of the device.  This will cause the configuration to be re-applied and
     * any reset actions (e.g. a serial port break) to be performed.
     *
     * @see IOTarget::Device::reset()
     */
    void reset();

    /**
     * Request a complete close and reopen cycle of the device.
     */
    void reopen();
};

}
}

#endif
