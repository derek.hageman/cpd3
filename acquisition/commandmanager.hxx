/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITIONCOMMANDMANAGER_H
#define CPD3ACQUISITIONCOMMANDMANAGER_H

#include "core/first.hxx"

#include <vector>
#include <unordered_map>
#include <QtGlobal>
#include <QObject>

#include "acquisition/acquisition.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/segment.hxx"
#include "datacore/sequencematch.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * A class for managing the availability and parameters for acquisition control
 * commands.
 * <br>
 * Note that this is NOT thread safe which is why it doesn't actually
 * inhert from Data::StreamSink even though
 * it basically implements the same API.
 */
class CPD3ACQUISITION_EXPORT CommandManager {
    friend struct CommandData;

    struct CommandData {
        CommandManager *parent;
        bool updated;

        Data::Variant::Root currentCommands;
        std::unordered_map<std::string, Data::Variant::Root> currentAggregateCommands;

        struct BreakDown {
            struct TriggerData {
                CommandManager *parent;
                Data::SequenceMatch::OrderedLookup input;
                std::string path;

                enum {
                    Flags,
                    Equal,
                    NotEqual,
                    LessThan,
                    LessThanOrEqual,
                    GreaterThan,
                    GreaterThanOrEqual,
                    Defined,
                } type;
                Data::Variant::Flags triggerFlags;
                double triggerValue;
                Data::Variant::Root triggerExact;

                bool triggered() const;

                TriggerData(CommandManager *p, const Data::Variant::Read &data);
            };

            std::vector<TriggerData> include;
            std::vector<TriggerData> exclude;

            struct ParameterData {
                CommandManager *parent;
                Data::SequenceMatch::OrderedLookup input;
                std::string path;
                Data::Variant::Root fallbackValue;

                ParameterData(CommandManager *p, const Data::Variant::Read &d);

                Data::Variant::Read getValue() const;
            };

            std::unordered_map<std::string, ParameterData> parameters;

            Data::Variant::Root base;

            bool registerInput(const Data::SequenceName &unit);

            bool enabled() const;
        };

        std::unordered_map<std::string, BreakDown> breakdownData;

        void updateCurrent();

        void updateCommandData(const Data::Variant::Read &data);

        bool registerInput(const Data::SequenceName &unit);

        const Data::Variant::Root &getCurrentCommands();

        const std::unordered_map<std::string, Data::Variant::Root> &getAggregateCommands();

        CommandData(CommandManager *p);
    };

    friend struct CommandData::BreakDown::TriggerData;
    friend struct CommandData::BreakDown::ParameterData;
    std::unordered_map<QString, std::unique_ptr<CommandData>> commands;
    std::unordered_map<QString, std::unique_ptr<CommandData>> manual;

    friend struct ValueData;

    struct ValueData {
        std::vector<CommandData *> commandInputs;

        void removeCommandData(CommandData *data);
    };

    Data::SequenceName::Map<std::unique_ptr<ValueData>> values;

    Data::SequenceSegment latest;
public:
    CommandManager();

    virtual ~CommandManager();

	CommandManager(const CommandManager &) = delete;

	CommandManager &operator=(const CommandManager &) = delete;

    /**
     * Reset the command manager.  This can be used on a disconnect to
     * clear out anything existing.
     *
     * @param purgeManual also purge all manually defined data
     */
    void reset(bool purgeManual = false);

    /**
     * Handle incoming data values.
     * 
     * @param values    the list of incoming values
     */
    void incomingData(const Data::SequenceValue::Transfer &values);

    /**
     * Handle a single incoming data value.
     * 
     * @param value     the incoming value
     */
    void incomingData(const Data::SequenceValue &value);

    /**
     * Update the command data for a given target.  Generally the command
     * data is from the "Commands" section of in the realtime information
     * for a target.
     * 
     * @param target    the target to update
     * @param commands  the commands data
     */
    void updateCommands(const QString &target, const Data::Variant::Read &commands);

    /**
     * Get the available and updated command data for a target.
     * 
     * @param target    the target to fetch
     * @return          the target command data
     */
    Data::Variant::Root getCommands(const QString &target);

    /**
     * Update the command data for a given manual target.  Generally these
     * are explicitly specified command data for persistent displays.
     *
     * @param target    the target to update
     * @param commands  the commands data
     */
    void updateManual(const QString &target, const Data::Variant::Read &commands);

    /**
     * Get the available and updated command data for a manual target.
     *
     * @param target    the target to fetch
     * @return          the target command data
     */
    Data::Variant::Root getManual(const QString &target);

    /**
     * Get all aggregate commands.  These are commands that can target
     * multiple real targets.  The command data is the same as a normal
     * commands except that it also contains a "Targets" array of strings
     * specifying the possible targets for the command and a "Command" string
     * that species the command name (instead of the first level of the hash).
     * 
     * @return          a list of aggregate commands
     */
    std::vector<Data::Variant::Root> getAggregateCommands();
};

}
}

#endif
