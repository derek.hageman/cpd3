/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITIONIOSERVER_H
#define CPD3ACQUISITIONIOSERVER_H

#include "core/first.hxx"

#include <vector>
#include <list>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>

#include "acquisition/acquisition.hxx"
#include "datacore/variant/root.hxx"
#include "iointerface.hxx"
#include "iotarget.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * The implementation of the I/O server backend.
 * <br>
 * This is used to communicate with IOInterface instances (and is generally
 * spawned by them on demand).  The communication happens over a local or
 * TCP socket.
 */
class CPD3ACQUISITION_EXPORT IOServer final {
    friend class IOInterface;

    enum class PacketToServer : std::uint8_t {
        /*
         * u16 length
         * u8 data[length]
         */
                Data,

        /*
         * Empty
         */
                Ping,

        /*
         * u8 enable
         */
                SetExclusive,

        /*
         * u32 length
         * u8 serialized[length]
         */
                TargetMerge,

        /*
         * Empty
         */
                Reset,

        /*
         * Empty
         */
                Reopen,
    };
    enum class PacketToClient : std::uint8_t {
        /*
         * u16 length
         * u8 data[length]
         */
                Data,

        /*
         * Empty
         */
                Pong,

        /*
         * u16 length
         * u8 data[length]
         */
                OtherControl,

        /*
         * Empty
         */
                Hello,

        /*
         * Empty
         */
                SetExclusiveResult,

        /*
         * Empty
         */
                TargetMergeResult,

        /*
         * Empty
         */
                ResetResult,

        /*
         * Empty
         */
                ReopenResult,

        /*
         * u32 length
         * u8 serialized[length]
         */
                UpdateTarget,

        /*
         * Empty
         */
                DeviceClosed,
    };

    std::mutex mutex;
    std::condition_variable notify;
    std::thread thread;
    bool terminated;
    bool completed;

    std::unique_ptr<IOTarget> target;
    std::unique_ptr<IOTarget::Device> device;
    bool spawned;

    using Clock = std::chrono::steady_clock;

    class Client final {
        IOServer &server;
        std::unique_ptr<IO::Socket::Connection> connection;

        bool endPending;
        bool processEnd;

        Util::ByteArray readPending;
        Util::ByteArray processRead;

        Clock::time_point pingTimeout;

        bool handleNextPacket();

    public:
        Client() = delete;

        Client(IOServer &server, std::unique_ptr<IO::Socket::Connection> &&connection);

        ~Client();

        bool prepare(const Clock::time_point &now, Clock::time_point &wake);

        bool process();

        void incomingData(const Util::ByteView &data);

        void incomingControl(const Util::ByteView &data);

        void deviceClosed();
    };

    std::list<std::unique_ptr<Client>> clients;
    Client *exclusive;

    std::vector<Util::ByteArray> writeBacklog;
    std::vector<Util::ByteArray> deviceReadPending;
    std::vector<Util::ByteArray> deviceControlPending;

    bool checkAlreadyRunning();

    void run();

public:
    IOServer() = delete;

    /**
     * Create the server.
     *
     * @param target    the target (must specify at least a device)
     * @param spawned   if set then the server will exit after all connection (after the first) are closed
     */
    explicit IOServer(std::unique_ptr<IOTarget> &&target, bool spawned = false);

    ~IOServer();

    /**
     * Start the server running.
     */
    void start();

    /**
     * Terminate the server.
     */
    void signalTerminate();

    /**
     * Test if the server has completed execution.
     *
     * @return  true if the server has finished
     */
    bool isFinished();

    /**
     * Wait for the server to finish.
     *
     * @param timeout   the maximum time to wait
     * @return          true if the server has finished
     */
    bool wait(double timeout = FP::undefined());

    /**
     * Emitted when the server has finished execution.
     */
    Threading::Signal<> finished;
};

}
}

#endif
