/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QEventLoop>
#include <QTimer>

#include "acquisition/acquisitioncomponent.hxx"
#include "core/threadpool.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Acquisition {

/** @file acquisition/acquisitioncomponent.hxx
 * General utilities for acquisition components.
 */

AcquisitionControlStream::AcquisitionControlStream() = default;

AcquisitionControlStream::~AcquisitionControlStream() = default;

void AcquisitionControlStream::writeControl(Util::ByteArray &&data)
{ return writeControl(Util::ByteView(data)); }

void AcquisitionControlStream::writeControl(const QByteArray &data)
{ return writeControl(Util::ByteView(data)); }

void AcquisitionControlStream::writeControl(const std::string &data)
{ return writeControl(Util::ByteView(data)); }

void AcquisitionControlStream::writeControl(const QString &data)
{ return writeControl(Util::ByteView(data.toUtf8())); }

void AcquisitionControlStream::writeControl(const char *data)
{ return writeControl(Util::ByteView(data)); }

AcquisitionState::AcquisitionState() = default;

AcquisitionState::~AcquisitionState() = default;

void AcquisitionState::remap(const SequenceName::Component &, Variant::Root &)
{ }

void AcquisitionState::realtimeAdvanced(double)
{ }

void AcquisitionState::requestDataWakeup(double)
{ }

void AcquisitionState::requestControlWakeup(double)
{ }

void AcquisitionState::loggingHalted(double)
{ }

AcquisitionInterface::AcquisitionInterface() = default;

AcquisitionInterface::~AcquisitionInterface() = default;

void AcquisitionInterface::setPersistentEgress(StreamSink *egress)
{
    if (egress)
        egress->endData();
}

SequenceValue::Transfer AcquisitionInterface::getPersistentValues()
{ return SequenceValue::Transfer(); }

void AcquisitionInterface::incomingData(Util::ByteArray &&data, double time, IncomingDataType type)
{ return incomingData(Util::ByteView(data), time, type); }

void AcquisitionInterface::incomingData(const QByteArray &data, double time, IncomingDataType type)
{ return incomingData(Util::ByteView(data), time, type); }

void AcquisitionInterface::incomingData(const std::string &data, double time, IncomingDataType type)
{ return incomingData(Util::ByteView(data), time, type); }

void AcquisitionInterface::incomingData(const QString &data, double time, IncomingDataType type)
{ return incomingData(Util::ByteView(data.toUtf8()), time, type); }

void AcquisitionInterface::incomingData(const char *data, double time, IncomingDataType type)
{ return incomingData(Util::ByteView(data), time, type); }

void AcquisitionInterface::incomingControl(const Util::ByteView &, double, IncomingDataType)
{ }

void AcquisitionInterface::incomingControl(Util::ByteArray &&data,
                                           double time,
                                           IncomingDataType type)
{ return incomingControl(Util::ByteView(data), time, type); }

void AcquisitionInterface::incomingControl(const QByteArray &data,
                                           double time,
                                           IncomingDataType type)
{ return incomingControl(Util::ByteView(data), time, type); }

void AcquisitionInterface::incomingControl(const std::string &data,
                                           double time,
                                           IncomingDataType type)
{ return incomingControl(Util::ByteView(data), time, type); }

void AcquisitionInterface::incomingControl(const QString &data, double time, IncomingDataType type)
{ return incomingControl(Util::ByteView(data.toUtf8()), time, type); }

void AcquisitionInterface::incomingControl(const char *data, double time, IncomingDataType type)
{ return incomingControl(Util::ByteView(data), time, type); }

void AcquisitionInterface::incomingTimeout(double)
{ }

void AcquisitionInterface::incomingCommand(const Variant::Read &)
{ }

void AcquisitionInterface::incomingAdvance(double)
{ }

Variant::Root AcquisitionInterface::getCommands()
{ return Variant::Root(); }

SequenceMatch::Composite AcquisitionInterface::getExplicitGroupedVariables()
{ return SequenceMatch::Composite(); }

StreamSink *AcquisitionInterface::getRealtimeIngress()
{ return nullptr; }

AcquisitionInterface::AutoprobeStatus AcquisitionInterface::getAutoprobeStatus()
{ return AutoprobeStatus::Failure; }

void AcquisitionInterface::autoprobeTryInteractive(double)
{ }

void AcquisitionInterface::autoprobeResetPassive(double)
{ }

void AcquisitionInterface::autoprobePromote(double)
{ }

void AcquisitionInterface::autoprobePromotePassive(double)
{ }

void AcquisitionInterface::serializeState(QDataStream &)
{ }

void AcquisitionInterface::deserializeState(QDataStream &)
{ }

void AcquisitionInterface::initializeState()
{ }

Variant::Root AcquisitionInterface::getSystemFlagsMetadata()
{ return Variant::Root(); }

Variant::Root AcquisitionInterface::getSourceMetadata()
{ return Variant::Root(); }

AcquisitionInterface::GeneralStatus AcquisitionInterface::getGeneralStatus()
{ return GeneralStatus::Normal; }

AcquisitionInterface::AutomaticDefaults::AutomaticDefaults()
        : interface(),
          name("X$2"),
          autoprobeLevelPriority(0),
          autoprobeInitialOrder(0),
          autoprobeTries(0)
{ }

void AcquisitionInterface::AutomaticDefaults::setSerialN81(int baud)
{
    interface["Baud"].setInt64(baud);
    interface["Parity"].setString("None");
    interface["DataBits"].setInt64(8);
    interface["StopBits"].setInt64(1);
}

AcquisitionInterface::AutomaticDefaults AcquisitionInterface::getDefaults()
{ return AcquisitionInterface::AutomaticDefaults(); }

void AcquisitionInterface::start()
{ }

void AcquisitionInterface::initiateShutdown()
{ signalTerminate(); }

bool AcquisitionInterface::wait(double timeout)
{ return completed.wait(std::bind(&AcquisitionInterface::isCompleted, this), timeout); }

ComponentOptions AcquisitionComponent::getPassiveOptions()
{ return ComponentOptions(); }

QList<ComponentExample> AcquisitionComponent::getPassiveExamples()
{ return QList<ComponentExample>(); }

std::unique_ptr<
        AcquisitionInterface> AcquisitionComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                               const std::string &loggingContext)
{ return createAcquisitionPassive(config, loggingContext); }

std::unique_ptr<
        AcquisitionInterface> AcquisitionComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                 const std::string &loggingContext)
{ return createAcquisitionPassive(config, loggingContext); }

static std::string assembleLoggingCategory(const std::string &loggingIdentifier,
                                           const std::string &loggingContext)
{
    std::string result = "cpd3.acquisition.instrument";
    if (!loggingIdentifier.empty()) {
        result += '.';
        result += loggingIdentifier;
    }
    if (!loggingContext.empty()) {
        result += '.';
        result += loggingContext;
    }
    return result;
}

InstrumentAcquisitionInterface::InstrumentAcquisitionInterface(const std::string &loggingIdentifier,
                                                               const std::string &loggingContext)
        : controlState(ControlState::Initialize),
          loggingName(assembleLoggingCategory(loggingIdentifier, loggingContext)),
          pendingControlStream(nullptr),
          pendingState(nullptr),
          pendingRealtimeEgress(nullptr),
          pendingLoggingEgress(nullptr),
          pendingPersistentEgress(nullptr),
          controlStream(nullptr),
          state(nullptr),
          realtimeEgress(nullptr),
          loggingEgress(nullptr),
          persistentEgress(nullptr),
          log(loggingName.data(), QtWarningMsg)
{ }

InstrumentAcquisitionInterface::~InstrumentAcquisitionInterface()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        controlState = ControlState::Terminated;
        request.notify_all();
    }
    if (thread.joinable())
        thread.join();
}

void InstrumentAcquisitionInterface::run()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!lock)
            lock.lock();

        if (controlStream != pendingControlStream) {
            controlStream = pendingControlStream;
            response.notify_all();
        }
        if (state != pendingState) {
            state = pendingState;
            response.notify_all();
        }
        if (realtimeEgress != pendingRealtimeEgress) {
            realtimeEgress = pendingRealtimeEgress;
            response.notify_all();
        }
        if (loggingEgress != pendingLoggingEgress) {
            loggingEgress = pendingLoggingEgress;
            response.notify_all();
        }
        if (persistentEgress != pendingPersistentEgress) {
            persistentEgress = pendingPersistentEgress;
            response.notify_all();
        }

        if (controlState == ControlState::Terminated)
            break;
        switch (controlState) {
        case ControlState::Initialize:
            controlState = ControlState::Running;
            response.notify_all();
            /* fall through */
        case ControlState::Running:
            break;
        case ControlState::Terminated:
        case ControlState::Complete:
            Q_ASSERT(false);
            break;
        case ControlState::PauseRequested:
            controlState = ControlState::Paused;
            response.notify_all();
            /* Fall through */
        case ControlState::Paused:
            request.wait(lock);
            continue;
        }

        bool runProcess = false;
        std::vector<Data::Variant::Root> pendingCommands;
        if (prepare()) {
            runProcess = true;
            pendingCommands = std::move(commandQueue);
            commandQueue.clear();
        } else if (!commandQueue.empty()) {
            pendingCommands = std::move(commandQueue);
            commandQueue.clear();
        } else {
            request.wait(lock);
            continue;
        }
        lock.unlock();

        for (const auto &cmd : pendingCommands) {
            command(cmd.read());
        }

        if (runProcess)
            process();
    }
    controlState = ControlState::Complete;
    lock.unlock();
    response.notify_all();

    {
        if (realtimeEgress)
            realtimeEgress->endData();
        if (loggingEgress)
            loggingEgress->endData();
        if (persistentEgress)
            persistentEgress->endData();
    }

    completed();
}

void InstrumentAcquisitionInterface::notifyUpdate()
{ request.notify_all(); }

bool InstrumentAcquisitionInterface::prepare()
{ return false; }

void InstrumentAcquisitionInterface::process()
{ }

void InstrumentAcquisitionInterface::command(const Variant::Read &)
{ }

void InstrumentAcquisitionInterface::setControlStream(AcquisitionControlStream *set)
{
    std::unique_lock<std::mutex> lock(mutex);
    while (controlStream != set) {
        pendingControlStream = set;
        switch (controlState) {
        case ControlState::Initialize:
            controlStream = set;
            return;
        case ControlState::Running:
        case ControlState::PauseRequested:
        case ControlState::Paused:
            break;
        case ControlState::Terminated:
        case ControlState::Complete:
            return;
        }
        request.notify_all();
        response.wait(lock);
    }
}

void InstrumentAcquisitionInterface::setState(AcquisitionState *set)
{
    std::unique_lock<std::mutex> lock(mutex);
    while (state != set) {
        pendingState = set;
        switch (controlState) {
        case ControlState::Initialize:
            state = set;
            return;
        case ControlState::Running:
        case ControlState::PauseRequested:
        case ControlState::Paused:
            break;
        case ControlState::Terminated:
        case ControlState::Complete:
            return;
        }
        request.notify_all();
        response.wait(lock);
    }
}

void InstrumentAcquisitionInterface::setRealtimeEgress(StreamSink *egress)
{
    std::unique_lock<std::mutex> lock(mutex);
    while (realtimeEgress != egress) {
        pendingRealtimeEgress = egress;
        switch (controlState) {
        case ControlState::Initialize:
            realtimeEgress = egress;
            return;
        case ControlState::Running:
        case ControlState::PauseRequested:
        case ControlState::Paused:
            break;
        case ControlState::Terminated:
        case ControlState::Complete:
            lock.unlock();
            if (egress) {
                egress->endData();
            }
            return;
        }
        request.notify_all();
        response.wait(lock);
    }
}

void InstrumentAcquisitionInterface::setLoggingEgress(StreamSink *egress)
{
    std::unique_lock<std::mutex> lock(mutex);
    while (loggingEgress != egress) {
        pendingLoggingEgress = egress;
        switch (controlState) {
        case ControlState::Initialize:
            loggingEgress = egress;
            return;
        case ControlState::Running:
        case ControlState::PauseRequested:
        case ControlState::Paused:
            break;
        case ControlState::Terminated:
        case ControlState::Complete:
            lock.unlock();
            if (egress) {
                egress->endData();
            }
            return;
        }
        request.notify_all();
        response.wait(lock);
    }
}

void InstrumentAcquisitionInterface::setPersistentEgress(StreamSink *egress)
{
    std::unique_lock<std::mutex> lock(mutex);
    while (persistentEgress != egress) {
        pendingPersistentEgress = egress;
        switch (controlState) {
        case ControlState::Initialize:
            persistentEgress = egress;
            return;
        case ControlState::Running:
        case ControlState::PauseRequested:
        case ControlState::Paused:
            break;
        case ControlState::Terminated:
        case ControlState::Complete:
            lock.unlock();
            if (egress) {
                egress->endData();
            }
            return;
        }
        request.notify_all();
        response.wait(lock);
    }
}

void InstrumentAcquisitionInterface::incomingCommand(const Variant::Read &command)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        commandQueue.emplace_back(command);
    }
    request.notify_all();
}

InstrumentAcquisitionInterface::PauseLock::PauseLock(InstrumentAcquisitionInterface &interface)
        : interface(interface)
{
    std::unique_lock<std::mutex> lock(interface.mutex);
    while (interface.controlState != ControlState::Running) {
        switch (interface.controlState) {
        case ControlState::Running:
            Q_ASSERT(false);
            break;
        case ControlState::Initialize:
        case ControlState::PauseRequested:
        case ControlState::Paused:
        case ControlState::Terminated:
            break;
        case ControlState::Complete:
            return;
        }
        interface.response.wait(lock);
    }
    Q_ASSERT(interface.controlState == ControlState::Running);

    interface.controlState = ControlState::PauseRequested;
    interface.request.notify_all();

    while (interface.controlState != ControlState::Paused) {
        switch (interface.controlState) {
        case ControlState::Running:
        case ControlState::Initialize:
        case ControlState::Paused:
            Q_ASSERT(false);
            break;
        case ControlState::PauseRequested:
        case ControlState::Terminated:
            break;
        case ControlState::Complete:
            return;
        }
        interface.response.wait(lock);
    }
}

InstrumentAcquisitionInterface::PauseLock::~PauseLock()
{
    {
        std::lock_guard<std::mutex> lock(interface.mutex);
        switch (interface.controlState) {
        case ControlState::Initialize:
        case ControlState::Running:
        case ControlState::PauseRequested:
            Q_ASSERT(false);
        case ControlState::Paused:
            break;
        case ControlState::Terminated:
        case ControlState::Complete:
            return;
        }
        interface.controlState = ControlState::Running;
    }
    interface.request.notify_all();
    interface.response.notify_all();
}

bool InstrumentAcquisitionInterface::isCompleted()
{
    std::lock_guard<std::mutex> lock(mutex);
    return controlState == ControlState::Complete;
}

void InstrumentAcquisitionInterface::start()
{
    Q_ASSERT(controlState != ControlState::Running);
    thread = std::thread(std::bind(&InstrumentAcquisitionInterface::run, this));
}

void InstrumentAcquisitionInterface::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (controlState == ControlState::Complete)
            return;
        controlState = ControlState::Terminated;
    }
    request.notify_all();
    response.notify_all();
}

}
}
