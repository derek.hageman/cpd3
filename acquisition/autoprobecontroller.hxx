/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3AUTOPROBECONTROLLER_H
#define CPD3AUTOPROBECONTROLLER_H

#include "core/first.hxx"

#include <mutex>
#include <unordered_map>
#include <condition_variable>

#include "acquisition/acquisition.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/iointerface.hxx"
#include "acquisition/iotarget.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/segment.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * A controller that handles autoprobing the instrument on a target I/O
 * interface.
 */
class CPD3ACQUISITION_EXPORT AutoprobeController {
    std::mutex mutex;
    std::condition_variable request;
    std::condition_variable response;
    enum class State {
        Initialize, Running, Stopping, Complete,
    } state;
    std::thread thread;
    bool updateRequired;
    std::vector<std::function<void()>> runQueue;

    std::unique_ptr<IOInterface> interface;

    class ControlInterface;

    struct Parameters {
        AcquisitionComponent *component;
        std::string loggingContext;

        QString componentName;
        int maximumTries;
        Data::Variant::Root interfaceParameters;
        Data::Variant::Root sourceParameters;
        QString instrumentName;
        bool passive;
        bool canChangeInterface;
        bool ignoreBackgroundFailure;
        bool isPriorityComponent;
        int autoprobeInitialOrder;
        Data::ValueSegment::Transfer configuration;

        Parameters(AcquisitionComponent *component);

        Parameters();
    };

    class DataReceiver : public Threading::Receiver {
        std::mutex mutex;
        std::condition_variable notify;
        std::vector<std::function<void()>> queued;
        bool isDisconnected;
        double time;

    public:
        DataReceiver(IOInterface &from, AcquisitionInterface &to);

        virtual ~DataReceiver();

        void discard(double now, const std::function<void()> &connect);

        void handoff(double now, const std::function<void()> &connect);

        void flush(double now);
    };

    struct Active {
        Parameters parameters;

        std::vector<Threading::Connection> connections;
        std::unique_ptr<ControlInterface> control;
        std::unique_ptr<AcquisitionInterface> interface;
        std::unique_ptr<DataReceiver> receiver;

        int tries;

        Active(IOInterface &io,
               Parameters &&parameters,
               std::unique_ptr<AcquisitionInterface> &&interface);

        Active();

        void describe(Data::Variant::Write &&target) const;
    };

    Active current;
    std::vector<std::vector<Active>> pending;

    Active selected;
    double selectedAtRealtime;

    template<typename Functor>
    void forAllAcquisitionInterfaces(Functor f)
    {
        if (current.interface)
            f(current.interface.get());
        for (auto &level : pending) {
            for (auto &p : level) {
                if (!p.interface)
                    continue;
                f(p.interface.get());
            }
        }
    }

    void activatePending(Active &&pending);

    class HandoffControl;

    class ControlInterface : public AcquisitionControlStream {
        AutoprobeController *parent;
        bool enableWrite;

        std::mutex mutex;
        Util::ByteArray sendData;
        double timeoutTime;

        friend class AutoprobeController;

        friend class HandoffControl;

    public:
        ControlInterface(AutoprobeController *p);

        virtual ~ControlInterface();

        void writeControl(const Util::ByteView &data) override;

        void resetControl() override;

        void requestTimeout(double time) override;
    };

    class HandoffControl final : public AcquisitionControlStream {
        bool resetPending;
        Util::ByteArray writePending;
    public:
        HandoffControl();

        virtual ~HandoffControl();

        void writeControl(const Util::ByteView &data) override;

        void resetControl() override;

        void requestTimeout(double time) override;

        void attached(IOInterface &target, ControlInterface *original);

        bool handoff(IOInterface &target);
    };

    std::unique_ptr<HandoffControl> handoffControl;

    std::string loggingName;
    QLoggingCategory logging;

    Threading::Receiver receiver;

    void run();

    void checkUpdated();

    bool reselectActive();

public:
    /**
     * Create a new controller for the given interface.
     * 
     * @param interface         the I/O interface to connect to
     * @param config            the configuration of the controller
     * @param priorityComponent the name of the component to give priority when equal (generally the previously matched component)
     */
    AutoprobeController(std::unique_ptr<IOInterface> &&interface,
                        const Data::ValueSegment::Transfer &config,
                        const QString &priorityComponent = {});

    virtual ~AutoprobeController();

    /**
     * The result of the autoprobing.
     */
    class CPD3ACQUISITION_EXPORT Result {
        std::unique_ptr<DataReceiver> receiver;
        std::unique_ptr<HandoffControl> control;
        AcquisitionInterface *handoffAcquisition;
        IOInterface *handoffInterface;

        friend class AutoprobeController;

        explicit Result(AutoprobeController &controller);

    public:
        Result() = delete;

        /**
         * The acquisition component selected.
         */
        std::unique_ptr<AcquisitionInterface> acquisition;

        /**
         * The interface used.
         */
        std::unique_ptr<IOInterface> interface;

        /**
         * The time the instrument was activated at.  This is the latest
         * real time that was sent to the instrument.  This should be used to
         * ensure that the instrument does not receive any timestamps before
         * what it considers the current time.
         */
        double activationTime;

        /**
         * The passive state of the selected component.  That is, this will
         * be true if the selected component should be promoted into
         * passive mode.
         */
        bool isPassive;

        /**
         * The name of the component that was successfully autoprobed.  This
         * is used to accelerate later autoprobes by giving that interface
         * interactive priority.
         *
         * @return the interface name
         */
        QString componentName;

        /**
         * The instrument name (as per the AcquisitionComponent specification)
         * of the matched instrument.
         */
        QString instrumentName;

        /**
         * The configuration used in the resulting activated component.  This
         * can be used to extract further overlays that may need to be applied
         * to it.
         */
        Data::ValueSegment::Transfer configuration;

        Result(const Result &) = delete;

        Result &operator=(const Result &) = delete;

        ~Result();

        /**
         * Perform the handoff, flushing any pending operations and data.
         *
         * @param time      the current time, must be >=  activationTime
         * @param connect   the function to call to connect the interface data signals
         */
        void handoff(double time, AcquisitionControlStream *control, const std::function<void()> &connect);
    };

    /**
     * Take the matched component from the autoprobe controller.  The interface
     * must be promoted with interface->autoprobePromote(double) before
     * it is used to log data.
     * <br>
     * The interface will still have a control stream tied to the autoprobe
     * controller, so it must be switched before the autoprobe controller
     * is deleted.
     * <br>
     * Will be empty if autoprobing has failed.
     *
     * @return the successfully autoprobed interface
     */
    std::unique_ptr<Result> takeResult();

    /**
     * Test if the autoprobe sequence is still active.  Once this returns
     * false the autoprobe will have completed successfully (if the activation
     * time is valid) or exhausted all interfaces (if it is not).
     * 
     * @return true if the autoprobe is still active
     */
    bool isActive() const;

    /**
     * Get the time the instrument was activated at.  This is the latest
     * real time that was sent to the instrument.  This should be used to
     * ensure that the instrument does not receive any timestamps before
     * what it considers the current time.
     *
     * @return a real time value as per Time::time()
     */
    double getActivationTime() const;

    /**
     * Remove all candidates that match a given instrument name.  This
     * can be used to speed up autodetection by removing any instruments
     * that where matched on another port.
     *
     * @param name  the name to remove
     */
    void removeInstrumentName(const QString &name);

    /**
     * Get a description of the current status of autoprobing.
     *
     * @return      a state description
     */
    Data::Variant::Root describeState();

    /**
     * Start the autoprobe controller.
     */
    void start();

    /**
     * Halt the autoprobe controller.
     */
    void stop();


    /**
     * Emitted when the autoprobe has completed (successfully or not).
     */
    Threading::Signal<> completed;

    /**
     * Emitted when the state description has changed.
     */
    Threading::Signal<> stateChanged;
};

}
}


#endif
