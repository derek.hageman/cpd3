/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3ACQUISITIONARCHIVE_HXX
#define CPD3ACQUISITIONARCHIVE_HXX

#include "core/first.hxx"

#include <memory>
#include <mutex>
#include <unordered_map>
#include <unordered_set>
#include <functional>

#include <QObject>
#include <QByteArray>
#include <QString>

#include "acquisition.hxx"

#include "core/threading.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "database/runlock.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * The implementation of the acquisition archive access controller.  This handles
 * creation and management of the actual archive backend.
 */
class CPD3ACQUISITION_EXPORT AcquisitionArchive {
    bool loggingDeferWrite;
    double maximumPurgeAge;
    double minimumPurgePreserveSize;

    bool inShutdown;

    std::mutex mutex;
    std::condition_variable completion;
    /* Need a separate mutex for operations that have locks inside a transaction,
     * since we use the main mutex to start them.  So we can't acquire the main
     * mutex inside a transaction due to lock ordering. */
    std::mutex transactionMutex;

    std::list<std::function<void(std::unique_lock<std::mutex> &)>> backgroundQueue;

    void backgroundLaunch(const std::function<void(std::unique_lock<std::mutex> &)> &run);

    void backgroundLaunch(std::function<void(std::unique_lock<std::mutex> &)> &&run);

    void backgroundThread();

    std::unordered_map<QString, QByteArray> pendingState;

    void stateRun(std::unique_lock<std::mutex> &lock);

    std::unique_ptr<Data::Archive::Access> access;

    void reapAccess();

    void acquireAccess(std::unique_lock<std::mutex> &lock);

    class EventWrite : public Data::Archive::Access::BackgroundWrite {
        Data::SequenceValue::Transfer incoming;
        Data::SequenceValue::Transfer processing;

        double beginTime;

    public:
        EventWrite(Data::Archive::Access &access);

        virtual ~EventWrite();

        void incomingEvent(const Data::SequenceValue &event);

        void incomingEvent(Data::SequenceValue &&event);

    protected:
        virtual bool prepare();

        virtual Result operation(Data::Archive::Access &access);

        virtual void success();
    };

    std::shared_ptr<EventWrite> events;

    class CPD3ACQUISITION_EXPORT ArchiveSink : public Data::StreamSink {
        AcquisitionArchive &archive;
        bool ended;
    public:
        ArchiveSink(AcquisitionArchive &archive);

        virtual ~ArchiveSink();

        void incomingData(const Data::SequenceValue::Transfer &values) override;

        void incomingData(Data::SequenceValue::Transfer &&values) override;

        void incomingData(const Data::SequenceValue &value) override;

        void incomingData(Data::SequenceValue &&value) override;

        virtual void endData();
    };

    friend class ArchiveIngress;

    bool shouldDeferLogging(std::size_t incoming) const;

    void backgroundLoggingFlush();

    void incomingLogging(const Data::SequenceValue::Transfer &values);

    void incomingLogging(Data::SequenceValue::Transfer &&values);

    void incomingLogging(const Data::SequenceValue &value);

    void incomingLogging(Data::SequenceValue &&value);

    Data::SequenceValue::Transfer deferredLogging;
    Data::SequenceName::Map<double> purgedInstrumentData;

    bool checkPurgeValue(const Data::SequenceValue &add) const;

    class ArchiveWrite : public Data::Archive::Access::BackgroundWrite {
        AcquisitionArchive &archive;

        Data::SequenceValue::Transfer incoming;
        Data::SequenceValue::Transfer processing;

        double beginTime;

        Data::SequenceName::Map<Time::Bounds> purgeRanges;
        double firstWrittenTime;

    public:
        ArchiveWrite(AcquisitionArchive &archive, Data::Archive::Access &access);

        virtual ~ArchiveWrite();

        void incomingData(const Data::SequenceValue::Transfer &values);

        void incomingData(Data::SequenceValue::Transfer &&values);

        void incomingData(const Data::SequenceValue &value);

        void incomingData(Data::SequenceValue &&value);

    protected:
        virtual bool prepare();

        virtual Result operation(Data::Archive::Access &access);

        virtual void success();
    };

    std::shared_ptr<ArchiveWrite> archive;

    std::unordered_set<Data::SequenceName> seenPersistent;

    class PersistentWrite : public Data::Archive::Access::BackgroundWrite {
        AcquisitionArchive &archive;

        Data::SequenceValue::Transfer incoming;
        Data::SequenceValue::Transfer processing;

        double beginTime;

        std::unordered_set<Data::SequenceName> notifySeen;

    public:
        PersistentWrite(AcquisitionArchive &archive, Data::Archive::Access &access);

        virtual ~PersistentWrite();

        void incomingData(const Data::SequenceValue::Transfer &values);

        void incomingData(Data::SequenceValue::Transfer &&values);

        void incomingData(const Data::SequenceValue &value);

        void incomingData(Data::SequenceValue &&value);

    protected:
        virtual bool prepare();

        virtual Result operation(Data::Archive::Access &access);

        virtual void success();
    };

    std::shared_ptr<PersistentWrite> persistent;

    static void flattenOverlapping(Data::Archive::Access &access,
                                   Data::SequenceValue::Transfer incoming);

public:
    /**
     * Create a new acquisition archive.
     *
     * @param configuration the configuration settings
     */
    AcquisitionArchive(const Data::Variant::Read &configuration = Data::Variant::Read::empty());

    virtual ~AcquisitionArchive();

    /**
     * Create an sink connected to the data writing archive.  The ingress will delay
     * deletion until it is ended.
     *
     * @return  a new sink point, owned by the caller
     */
    Data::StreamSink *createLoggingSink();

    /**
     * Flush any pending archive data.
     */
    void flushLogging();

    /**
     * Terminate all logged values at the given end time.
     * <br>
     * This can only be used once all write operations have completed (i.e. after wait()).
     *
     * @param time  the end time to clamp logging values to
     */
    void terminateLogging(double time);

    /**
     * Write an event to the archive.
     *
     * @param event the event to write
     */
    void incomingEvent(const Data::SequenceValue &event);

    /** @see incomingEvent(const Data::SequenceValue &) */
    void incomingEvent(Data::SequenceValue &&event);

    /**
     * Write the given persistent values to the archive.
     *
     * @param values    the values to write if any
     */
    void incomingPersistent(const Data::SequenceValue::Transfer &values);

    /** @see incomingPersistent(const Data::SequenceValue::Transfer &) */
    void incomingPersistent(Data::SequenceValue::Transfer &&values);

    /** @see incomingPersistent(const Data::SequenceValue::Transfer &) */
    void incomingPersistent(const Data::SequenceValue &value);

    /** @see incomingPersistent(const Data::SequenceValue::Transfer &) */
    void incomingPersistent(Data::SequenceValue &&value);

    /**
     * Save the given data as a state key.
     *
     * @param key   the state key
     * @param data  the data to save
     */
    void saveState(const QString &key, const QByteArray &data);

    /** @see saveState(QString &key, const QByteArray &) */
    void saveState(QString &&key, QByteArray &&data);

    /**
     * Start shutdown of the archive.
     */
    void initiateShutdown();

    /**
     * Wait for all archive operations to complete.
     */
    void wait();

    /**
     * Emitted when logging data are written, with the time of the first
     * value.
     */
    Threading::Signal<double> loggingWritten;
};

}
}

#endif
