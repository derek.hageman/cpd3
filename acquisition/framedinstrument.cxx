/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/threadpool.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"

using namespace CPD3::Data;


namespace CPD3 {
namespace Acquisition {

/** @file acquisition/framedinstrument.hxx
 * A base implementation for instruments that operate on known data framing
 * markers.
 */

void FramedInstrument::incomingInstrumentTimeout(double)
{ }

void FramedInstrument::discardDataCompleted(double)
{ }

void FramedInstrument::discardControlCompleted(double)
{ }

void FramedInstrument::shutdownInProgress(double)
{ }

bool FramedInstrument::applyDataFraming(const Util::ByteArray &input,
                                        std::size_t &start,
                                        std::size_t &end,
                                        bool terminal) const
{
    if (input.size() <= start)
        return false;
    start = dataFrameStart(start, input);
    end = input.npos;
    if (start == input.npos)
        return false;
    end = dataFrameEnd(start, input);
    if (end == input.npos) {
        if (terminal) {
            end = input.size();
        } else {
            return false;
        }
    }
    if (start == end)
        return false;
    return true;
}

bool FramedInstrument::applyControlFraming(const Util::ByteArray &data,
                                           std::size_t &start,
                                           std::size_t &end,
                                           bool terminal) const
{
    if (data.size() <= start)
        return false;
    start = controlFrameStart(start, data);
    end = data.npos;
    if (start == data.npos)
        return false;
    end = controlFrameEnd(start, data);
    if (end == data.npos) {
        if (terminal) {
            end = data.size();
        } else {
            return false;
        }
    }
    if (start == end)
        return false;
    return true;
}

std::size_t FramedInstrument::dataFrameStart(std::size_t offset, const Util::ByteArray &input) const
{
    auto max = input.size();
    while (offset < max) {
        auto ch = input[offset];
        if (ch == '\r' || ch == '\n') {
            offset++;
            continue;
        }
        return offset;
    }
    return input.npos;
}

std::size_t FramedInstrument::dataFrameEnd(std::size_t start, const Util::ByteArray &input) const
{
    for (auto max = input.size(); start < max; start++) {
        char ch = input[start];
        if (ch == '\r' || ch == '\n')
            return start;
    }
    return input.npos;
}

std::size_t FramedInstrument::controlFrameStart(std::size_t offset,
                                                const Util::ByteArray &input) const
{
    auto max = input.size();
    while (offset < max) {
        char ch = input[offset];
        if (ch == '\r' || ch == '\n') {
            offset++;
            continue;
        }
        return offset;
    }
    return input.npos;
}

std::size_t FramedInstrument::controlFrameEnd(std::size_t start, const Util::ByteArray &input) const
{
    for (auto max = input.size(); start < max; start++) {
        auto ch = input[start];
        if (ch == '\r' || ch == '\n')
            return start;
    }
    return input.npos;
}

FramedInstrument::FramedInstrument(const std::string &loggingIdentifier,
                                   const std::string &loggingContext)
        : InstrumentAcquisitionInterface(loggingIdentifier, loggingContext), pendingEvents(),
          processEvents(),
          dataBuffer(),
          controlBuffer(),
          pendingAdvance(FP::undefined()),
          processAdvance(FP::undefined()),
          loggingHaltTime(FP::undefined()),
          latestTime(FP::undefined()),
          discardDataEndTime(FP::undefined()),
          discardDataFrames(0),
          discardControlEndTime(FP::undefined()),
          discardControlFrames(0),
          processingIndex(-1)
{
}

FramedInstrument::~FramedInstrument()
{ }

void FramedInstrument::clearDataBuffer()
{
    Q_ASSERT(!mutex.try_lock());
    for (auto it = pendingEvents.begin(); it != pendingEvents.end();) {
        if (it->type != Event::Type::DataFrame) {
            ++it;
            continue;
        }
        it = pendingEvents.erase(it);
    }
    dataBuffer.clear();
}

void FramedInstrument::clearControlBuffer()
{
    Q_ASSERT(!mutex.try_lock());
    for (auto it = pendingEvents.begin(); it != pendingEvents.end();) {
        if (it->type != Event::Type::ControlFrame) {
            ++it;
            continue;
        }
        it = pendingEvents.erase(it);
    }
    controlBuffer.clear();
}

void FramedInstrument::limitDataBuffer(Util::ByteArray &buffer)
{
    static constexpr std::size_t maxmimumSize = 65536;
    if (buffer.size() <= maxmimumSize)
        return;
    buffer.pop_front(buffer.size() - maxmimumSize);
}

void FramedInstrument::limitControlBuffer(Util::ByteArray &buffer)
{
    static constexpr std::size_t maxmimumSize = 65536;
    if (buffer.size() <= maxmimumSize)
        return;
    buffer.pop_front(buffer.size() - maxmimumSize);
}

void FramedInstrument::incomingData(const Util::ByteView &data,
                                    double time,
                                    AcquisitionInterface::IncomingDataType type)
{
    std::lock_guard<std::mutex> lock(mutex);

    if (FP::defined(time) && (!FP::defined(latestTime) || time > latestTime))
        latestTime = time;

    if (FP::defined(discardDataEndTime)) {
        if (FP::defined(time) && time < discardDataEndTime)
            return;
        discardDataEndTime = FP::undefined();
        if (discardDataFrames <= 0) {
            pendingEvents.emplace_back(time, Event::Type::DataDiscardComplete);
        }
    }

    if (type == AcquisitionInterface::IncomingDataType::Complete) {
        std::size_t start = 0;
        std::size_t end = Util::ByteArray::npos;
        std::size_t lastEnd = Util::ByteArray::npos;
        for (;;) {
            if (!applyDataFraming(dataBuffer, start, end, true))
                break;
            Q_ASSERT(start != Util::ByteArray::npos &&
                             start >= 0 &&
                             end != Util::ByteArray::npos &&
                             end >= start);
            if (discardDataFrames > 0) {
                discardDataFrames--;
                if (discardDataFrames <= 0) {
                    pendingEvents.emplace_back(time, Event::Type::DataDiscardComplete);
                }
            } else {
                pendingEvents.emplace_back(dataBuffer.mid(start, end - start), time,
                                           Event::Type::DataFrame);
            }
            lastEnd = end;
            start = end;
        }
        if (lastEnd != Util::ByteArray::npos) {
            dataBuffer.pop_front(lastEnd);
        }

        if (discardDataFrames > 0) {
            discardDataFrames--;
            if (discardDataFrames <= 0) {
                pendingEvents.emplace_back(time, Event::Type::DataDiscardComplete);
            }
        } else {
            pendingEvents.emplace_back(data, time, Event::Type::DataFrame);
        }
        notifyUpdate();
        return;
    }

    dataBuffer += data;

    std::size_t start = 0;
    std::size_t end = Util::ByteArray::npos;
    std::size_t lastEnd = Util::ByteArray::npos;
    for (;;) {
        if (!applyDataFraming(dataBuffer, start, end,
                              type == AcquisitionInterface::IncomingDataType::Final))
            break;
        Q_ASSERT(start != Util::ByteArray::npos &&
                         start >= 0 &&
                         end != Util::ByteArray::npos &&
                         end >= start);
        if (discardDataFrames > 0) {
            discardDataFrames--;
            if (discardDataFrames <= 0) {
                pendingEvents.emplace_back(time, Event::Type::DataDiscardComplete);
            }
        } else {
            pendingEvents.emplace_back(dataBuffer.mid(start, end - start), time,
                                       Event::Type::DataFrame);
        }
        lastEnd = end;
        start = end;
    }
    if (lastEnd != Util::ByteArray::npos) {
        dataBuffer.pop_front(lastEnd);
    }
    limitDataBuffer(dataBuffer);

    if (!pendingEvents.empty())
        notifyUpdate();
}

void FramedInstrument::incomingControl(const Util::ByteView &data,
                                       double time,
                                       AcquisitionInterface::IncomingDataType type)
{
    std::lock_guard<std::mutex> lock(mutex);

    if (FP::defined(time) && (!FP::defined(latestTime) || time > latestTime))
        latestTime = time;

    if (FP::defined(discardControlEndTime)) {
        if (FP::defined(time) && time < discardControlEndTime)
            return;
        discardControlEndTime = FP::undefined();
        if (discardControlFrames <= 0) {
            pendingEvents.emplace_back(time, Event::Type::ControlDiscardComplete);
        }
    }

    if (type == AcquisitionInterface::IncomingDataType::Complete) {
        std::size_t start = 0;
        std::size_t end = Util::ByteArray::npos;
        std::size_t lastEnd = Util::ByteArray::npos;
        for (;;) {
            if (!applyControlFraming(controlBuffer, start, end, true))
                break;
            Q_ASSERT(start != Util::ByteArray::npos &&
                             start >= 0 &&
                             end != Util::ByteArray::npos &&
                             end >= start);
            if (discardControlFrames > 0) {
                discardControlFrames--;
                if (discardControlFrames <= 0) {
                    pendingEvents.emplace_back(time, Event::Type::ControlDiscardComplete);
                }
            } else {
                pendingEvents.emplace_back(controlBuffer.mid(start, end - start), time,
                                           Event::Type::ControlFrame);
            }
            lastEnd = end;
            start = end;
        }
        if (lastEnd != Util::ByteArray::npos) {
            controlBuffer.pop_front(lastEnd);
        }

        if (discardControlFrames > 0) {
            discardControlFrames--;
            if (discardControlFrames <= 0) {
                pendingEvents.emplace_back(time, Event::Type::ControlDiscardComplete);
            }
        } else {
            pendingEvents.emplace_back(data, time, Event::Type::ControlFrame);
        }
        notifyUpdate();
        return;
    }

    controlBuffer += data;

    std::size_t start = 0;
    std::size_t end = Util::ByteArray::npos;
    std::size_t lastEnd = Util::ByteArray::npos;
    for (;;) {
        if (!applyControlFraming(controlBuffer, start, end,
                                 type == AcquisitionInterface::IncomingDataType::Final))
            break;
        Q_ASSERT(start != Util::ByteArray::npos &&
                         start >= 0 &&
                         end != Util::ByteArray::npos &&
                         end >= start);
        if (discardControlFrames > 0) {
            discardControlFrames--;
            if (discardControlFrames <= 0) {
                pendingEvents.emplace_back(time, Event::Type::ControlDiscardComplete);
            }
        } else {
            pendingEvents.emplace_back(controlBuffer.mid(start, end - start), time,
                                       Event::Type::ControlFrame);
        }
        lastEnd = end;
        start = end;
    }
    if (lastEnd != Util::ByteArray::npos) {
        controlBuffer.pop_front(lastEnd);
    }
    limitControlBuffer(controlBuffer);

    if (!pendingEvents.empty())
        notifyUpdate();
}

void FramedInstrument::incomingAdvance(double time)
{
    if (!FP::defined(time))
        return;
    std::lock_guard<std::mutex> lock(mutex);
    Q_ASSERT(!FP::defined(pendingAdvance) || time >= pendingAdvance);
    Q_ASSERT(pendingEvents.empty() ||
                     !FP::defined(pendingEvents.back().time) ||
                     pendingEvents.back().time <= time);
    pendingAdvance = time;

    if (FP::defined(discardDataEndTime)) {
        if (time >= discardDataEndTime) {
            discardDataEndTime = FP::undefined();
            if (discardDataFrames <= 0) {
                pendingEvents.emplace_back(time, Event::Type::DataDiscardComplete);
            }
        }
    }
    if (FP::defined(discardControlEndTime)) {
        if (time >= discardControlEndTime) {
            discardControlEndTime = FP::undefined();
            if (discardControlFrames <= 0) {
                pendingEvents.emplace_back(time, Event::Type::ControlDiscardComplete);
            }
        }
    }

    notifyUpdate();
}

void FramedInstrument::incomingTimeout(double time)
{
    if (!FP::defined(time))
        return;
    std::lock_guard<std::mutex> lock(mutex);
    Q_ASSERT(pendingEvents.empty() ||
                     !FP::defined(pendingEvents.back().time) ||
                     pendingEvents.back().time <= time);
    pendingEvents.emplace_back(time, Event::Type::Timeout);
    notifyUpdate();
}

bool FramedInstrument::prepare()
{
    Util::append(std::move(pendingEvents), processEvents);
    pendingEvents.clear();

    processAdvance = pendingAdvance;
    pendingAdvance = FP::undefined();

    return !processEvents.empty() || FP::defined(processAdvance);
}

void FramedInstrument::process()
{
    bool hadLoggingHalt;
    {
        std::lock_guard<std::mutex> lock(mutex);
        hadLoggingHalt = FP::defined(loggingHaltTime);
    }

    /* Don't store max, since this may be modified */
    for (processingIndex = 0;
            processingIndex < static_cast<int>(processEvents.size());
            processingIndex++) {
        const auto &event = processEvents[processingIndex];
        switch (event.type) {
        case Event::Type::DataFrame:
            if (hadLoggingHalt) {
                std::lock_guard<std::mutex> lock(mutex);
                loggingHaltTime = FP::undefined();
            }
            incomingDataFrame(event.data, event.time);
            if (hadLoggingHalt) {
                std::lock_guard<std::mutex> lock(mutex);
                hadLoggingHalt = FP::defined(loggingHaltTime);
            }
            break;
        case Event::Type::ControlFrame:
            incomingControlFrame(event.data, event.time);
            break;
        case Event::Type::DataDiscardComplete:
            discardDataCompleted(event.time);
            break;
        case Event::Type::ControlDiscardComplete:
            discardControlCompleted(event.time);
            break;
        case Event::Type::Timeout:
            incomingInstrumentTimeout(event.time);
            break;
        case Event::Type::ProcessShutdown:
            shutdownInProgress(event.time);
            break;
        }
    }
    processingIndex = -1;
    processEvents.clear();

    double latestTime = processAdvance;
    if (FP::defined(processAdvance)) {
        if (state) {
            state->realtimeAdvanced(processAdvance);
        }
        processAdvance = FP::undefined();
    }
    if (hadLoggingHalt) {
        double processHaltTime;
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (FP::defined(loggingHaltTime) && FP::defined(latestTime))
                loggingHaltTime = latestTime;
            processHaltTime = loggingHaltTime;
        }
        if (state) {
            state->loggingHalted(processHaltTime);
        }
    }
}

void FramedInstrument::timeoutAt(double time)
{
    if (controlStream)
        controlStream->requestTimeout(time);

    if (!processEvents.empty()) {
        for (auto it = processEvents.begin() + processingIndex + 1; it != processEvents.end();) {
            if (!FP::defined(it->time))
                break;
            if (it->type != Event::Type::Timeout) {
                ++it;
                continue;
            }
            if (FP::defined(time) && time <= it->time)
                break;
            it = processEvents.erase(it);
        }
    }

    std::lock_guard<std::mutex> lock(mutex);
    for (auto it = pendingEvents.begin(); it != pendingEvents.end();) {
        if (!FP::defined(it->time))
            break;
        if (it->type != Event::Type::Timeout) {
            ++it;
            continue;
        }
        if (FP::defined(time) && time <= it->time)
            break;
        it = pendingEvents.erase(it);
    }
}

void FramedInstrument::handleDiscard(std::vector<Event> &target,
                                     int offset,
                                     double &endTime,
                                     int &discardAfterEnd,
                                     Event::Type discardEvent,
                                     Event::Type frameEvent)
{
    if (target.empty())
        return;
    for (auto it = target.begin() + offset; it != target.end();) {
        if (!FP::defined(it->time))
            break;
        if (it->type == discardEvent) {
            if (!FP::defined(endTime) || it->time < endTime) {
                it = target.erase(it);
                continue;
            }
        } else if (it->type == frameEvent) {
            if (FP::defined(endTime) && it->time < endTime) {
                it = target.erase(it);
                continue;
            } else if (discardAfterEnd > 0) {
                endTime = FP::undefined();
                double saveTime = it->time;
                it = target.erase(it);
                discardAfterEnd--;
                if (discardAfterEnd <= 0) {
                    it = target.insert(it, Event(saveTime, discardEvent));
                    ++it;
                }
                continue;
            } else if (FP::defined(endTime)) {
                it = target.insert(it, Event(it->time, discardEvent));
                ++it;
                endTime = FP::undefined();
            }
        }

        ++it;
    }
}

void FramedInstrument::discardData(double endTime, int discardAfterEnd)
{
    std::unique_lock<std::mutex> lock(mutex);

    handleDiscard(processEvents, processingIndex + 1, endTime, discardAfterEnd,
                  Event::Type::DataDiscardComplete, Event::Type::DataFrame);
    handleDiscard(pendingEvents, 0, endTime, discardAfterEnd, Event::Type::DataDiscardComplete,
                  Event::Type::DataFrame);

    discardDataEndTime = endTime;
    discardDataFrames = discardAfterEnd;

    if (FP::defined(endTime) &&
            (static_cast<int>(processEvents.size()) <= processingIndex + 1 ||
                    (FP::defined(processEvents.back().time) &&
                            processEvents.back().time <= endTime)) &&
            (pendingEvents.empty() ||
                    (FP::defined(pendingEvents.back().time) &&
                            pendingEvents.back().time <= endTime))) {
        dataBuffer.clear();
        lock.unlock();

        if (state)
            state->requestDataWakeup(endTime);
    }
}

void FramedInstrument::discardControl(double endTime, int discardAfterEnd)
{
    std::unique_lock<std::mutex> lock(mutex);

    handleDiscard(processEvents, processingIndex + 1, endTime, discardAfterEnd,
                  Event::Type::ControlDiscardComplete, Event::Type::ControlFrame);
    handleDiscard(pendingEvents, 0, endTime, discardAfterEnd, Event::Type::ControlDiscardComplete,
                  Event::Type::ControlFrame);

    discardControlEndTime = endTime;
    discardControlFrames = discardAfterEnd;

    if (FP::defined(endTime) &&
            (static_cast<int>(processEvents.size()) <= processingIndex + 1 ||
                    (FP::defined(processEvents.back().time) &&
                            processEvents.back().time <= endTime)) &&
            (pendingEvents.empty() ||
                    (FP::defined(pendingEvents.back().time) &&
                            pendingEvents.back().time <= endTime))) {
        controlBuffer.clear();
        lock.unlock();

        if (state)
            state->requestControlWakeup(endTime);
    }
}

void FramedInstrument::loggingLost(double time)
{
    std::lock_guard<std::mutex> lock(mutex);
    loggingHaltTime = time;
}

void FramedInstrument::finishStreams(double time)
{
    std::unique_lock<std::mutex> lock(mutex);

    if (!FP::defined(time))
        time = latestTime;

    if (!FP::defined(discardDataEndTime) || !FP::defined(time) || time >= discardDataEndTime) {
        std::size_t start = 0;
        std::size_t end = Util::ByteArray::npos;
        for (;;) {
            if (!applyDataFraming(dataBuffer, start, end, true))
                break;
            Q_ASSERT(start >= 0 && end >= start);
            if (discardDataFrames > 0) {
                discardDataFrames--;
                if (discardDataFrames <= 0) {
                    pendingEvents.emplace_back(time, Event::Type::DataDiscardComplete);
                }
            } else {
                pendingEvents.emplace_back(dataBuffer.mid(start, end - start), time,
                                           Event::Type::DataFrame);
            }
            start = end;
        }
    }
    dataBuffer.clear();

    if (!FP::defined(discardControlEndTime) ||
            !FP::defined(time) ||
            time >= discardControlEndTime) {
        std::size_t start = 0;
        std::size_t end = Util::ByteArray::npos;
        for (;;) {
            if (!applyControlFraming(controlBuffer, start, end, true))
                break;
            Q_ASSERT(start != Util::ByteArray::npos &&
                             start >= 0 &&
                             end != Util::ByteArray::npos &&
                             end >= start);
            if (discardControlFrames > 0) {
                discardControlFrames--;
                if (discardControlFrames <= 0) {
                    pendingEvents.emplace_back(time, Event::Type::ControlDiscardComplete);
                }
            } else {
                pendingEvents.emplace_back(controlBuffer.mid(start, end - start), time,
                                           Event::Type::ControlFrame);
            }
            start = end;
        }
    }
    controlBuffer.clear();

    pendingEvents.emplace_back(time, Event::Type::ProcessShutdown);

    /* Polling loop, but not really worth implementing another wait condition
     * just for this. */
    while (!pendingEvents.empty()) {
        lock.unlock();
        notifyUpdate();
        std::this_thread::yield();
        if (isCompleted())
            return;
        lock.lock();
    }
}

void FramedInstrument::initiateShutdown()
{
    finishStreams();

    /* Do this so we know the processor has finished: the finishStreams() makes
     * sure it started running on all the events, but the pause will make sure 
     * it's also returned control */
    {
        PauseLock paused(*this);
    }

    AcquisitionInterface::initiateShutdown();
}

}
}
