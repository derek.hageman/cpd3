/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include <QRegExp>
#include <QStringList>
#include <QSettings>

#include "acquisition/realtimelayout.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/variant/composite.hxx"
#include "core/textsubstitution.hxx"

#include "guicore/guicore.hxx"

using namespace CPD3::Data;


namespace CPD3 {
namespace Acquisition {

/** @file acquisition/realtimelayout.hxx
 * The system to provide text layout for realtime data.
 */


static double calculatePitotFlow(double dP,
                                 double A = 0.00155179165474,
                                 double t = 273.15,
                                 double p = 1013.25)
{
    if (!FP::defined(dP) || dP < 0.0)
        return FP::undefined();
    if (!FP::defined(A) || A <= 0.0)
        return FP::undefined();
    if (dP == 0.0)
        return 0.0;
    if (!FP::defined(t) || t <= 0.0)
        return FP::undefined();
    if (!FP::defined(p) || p <= 0.0)
        return FP::undefined();

    double density = 1.2922;    /* kg/m^3 at 0C */
    density *= (273.15 / t) * (p / 1013.25);

    double v = sqrt(2.0 * (dP * 100.0) / density);   /* m/s */
    double Q = v * A;   /* m^3/s */

    Q *= 60000.0;   /* lpm */
    Q *= (p / 1013.25) * (273.15 / t);  /* To mass flow */

    return Q;
}

namespace {
class HashSubstitution : public TextSubstitution {
    const Variant::Read &value;
    const std::unordered_map<std::string, QString> &translation;
public:
    explicit HashSubstitution(const Variant::Read &v,
                              const std::unordered_map<std::string,
                                                       QString> &t = std::unordered_map<std::string,
                                                                                        QString>())
            : value(v), translation(t)
    { }

    virtual ~HashSubstitution() = default;

protected:
    virtual QString substitution(const QStringList &elements) const
    {
        int index = 0;
        QString type;
        if (index < elements.size())
            type = elements.at(index++).toLower();

        QString path;
        if (index < elements.size())
            path = elements.at(index++);

        if (type == "usertime") {
            QString mvc(QObject::tr("None"));
            if (index < elements.size())
                mvc = elements.at(index++);

            double time = value.getPath(path).toDouble();
            if (!FP::defined(time))
                return mvc;

            QSettings settings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION);
            if (settings.value("time/displayformat").toString() == "datetime") {
                return Time::toISO8601(time);
            } else {
                return Time::toYearDOY(time, QString(':'), Time::Year, QChar(), Time::DOYAsNeeded);
            }
        } else if (type == "ahead") {
            double seconds = value.getPath(path).toDouble();
            if (FP::defined(seconds))
                seconds -= Time::time();
            if (seconds < 0.0)
                seconds = 0.0;
            return formatDuration(seconds, elements.mid(index));
        } else if (type == "behind") {
            double seconds = value.getPath(path).toDouble();
            if (FP::defined(seconds))
                seconds = Time::time() - seconds;
            if (seconds < 0.0)
                seconds = 0.0;
            return formatDuration(seconds, elements.mid(index));
        } else if (type == "translate") {
            auto check = translation.find(value.getPath(path).toString());
            if (check != translation.end())
                return HashSubstitution(value).apply(check->second);
            return value.getPath(path).toDisplayString();
        } else if (type == "pitotflow") {
            double v = value.getPath(path).toDouble();
#if 1
            v = calculatePitotFlow(v);
#else
            /* Old calculation from unknown origins */
            if (FP::defined(v) && v > 0.0) {
                v = 2011.0 * ::sqrt( 273.15 * v / 1013.25 );
            } else {
                v = FP::undefined();
            }
#endif
            return formatDouble(v, elements.mid(index));
        }

        return Variant::Composite::applyFormat(value, elements);
    }
};
}

QString RealtimeLayout::hashFormat(const QString &format,
                                   const Variant::Read &value,
                                   const std::unordered_map<std::string, QString> &translation)
{
    return HashSubstitution(value, translation).apply(format);
}

QString RealtimeLayout::removeConsoleEscapes(QString input)
{
    int startIndex = 0;
    while (startIndex < input.size()) {
        startIndex = input.indexOf('&', startIndex);
        if (startIndex < 0)
            break;
        if (startIndex + 1 < input.size()) {
            QChar cmp(input.at(startIndex + 1));
            if (cmp == '&') {
                input.remove(startIndex + 1, 1);
                startIndex++;
                continue;
            } else {
                input.remove(startIndex, 1);
                continue;
            }
        } else {
            break;
        }
    }
    return input;
}

RealtimeLayout::LayoutFormatter::LayoutFormatter() : parent(nullptr),
                                                     canCache(false),
                                                     lastResult(),
                                                     fanout(nullptr),
                                                     unit(),
                                                     path(),
                                                     previousType(Variant::Type::Empty),
                                                     format(),
                                                     flagsTranslation(),
                                                     stringTranslation(),
                                                     hashFormat(),
                                                     prefix()
{ }

QString RealtimeLayout::LayoutFormatter::generate(bool *valid)
{
    if (canCache && !fanout->updated) {
        if (valid)
            *valid = true;
        return lastResult;
    }

    if (valid)
        *valid = false;

    for (auto check : fanout->sources) {
        if (!Variant::Composite::isDefined(check->latest.read())) {
            if (valid)
                *valid = true;
            continue;
        }

        auto v = check->latest.read().getPath(path);
        auto type = v.getType();
        if (type != previousType) {
            previousType = type;
            if (valid)
                *valid = false;
        } else {
            if (valid)
                *valid = true;
        }

        switch (type) {
        case Variant::Type::Empty: {
            canCache = true;
            lastResult = prefix;
            return lastResult;
        }
        case Variant::Type::Real: {
            canCache = true;
            double f = v.toDouble();
            if (!FP::defined(f))
                lastResult = prefix + QString(format.width(), QChar(' '));
            else
                lastResult = prefix + format.applyClipped(f, QChar(' '));
            return lastResult;
        }
        case Variant::Type::Integer: {
            canCache = true;
            qint64 i = v.toInt64();
            if (!INTEGER::defined(i))
                lastResult = prefix + QString(format.width(), QChar(' '));
            else
                lastResult = prefix + format.applyClipped(i, QChar(' '));
            return lastResult;
        }
        case Variant::Type::String: {
            canCache = true;

            auto tr = stringTranslation.find(v.toString());
            if (tr == stringTranslation.end()) {
                lastResult =
                        prefix + v.toDisplayString().rightJustified(format.width(), QChar(' '));
            } else {
                lastResult = prefix + tr->second.rightJustified(format.width(), QChar(' '));
            }
            return lastResult;
        }
        case Variant::Type::Boolean: {
            canCache = true;
            bool bv = v.toBool();
            auto tr = stringTranslation.find(bv ? "TRUE" : "FALSE");
            if (tr != stringTranslation.end()) {
                lastResult = prefix + tr->second.rightJustified(format.width(), QChar(' '));
            } else if (bv) {
                lastResult =
                        prefix + QObject::tr("TRUE").rightJustified(format.width(), QChar(' '));
            } else {
                lastResult =
                        prefix + QObject::tr("FALSE").rightJustified(format.width(), QChar(' '));
            }
            return lastResult;
        }
        case Variant::Type::Flags: {
            canCache = true;
            std::uint_fast64_t f = 0;
            for (const auto &add : v.toFlags()) {
                auto lf = flagsTranslation.find(add);
                if (lf == flagsTranslation.end())
                    continue;
                f |= lf->second;
            }
            lastResult = prefix + format.applyClipped(static_cast<qint64>(f), QChar('0'));
            return lastResult;
        }
        case Variant::Type::Hash: {
            canCache = false;
            return prefix +
                    RealtimeLayout::hashFormat(hashFormat, v, stringTranslation).rightJustified(
                            format.width(), QChar(' '));
        }
        default:
            break;
        }
    }

    canCache = true;
    lastResult = QString(format.width(), QChar(' '));
    return lastResult;
}

void RealtimeLayout::BoxBreakdownData::addPageItem(const RealtimeLayout::PageBreakdownData &add)
{
    if (add.value->metadata.read().getType() == Variant::Type::MetadataArray) {
        arrayColumns.push_back(add);
        return;
    }

    auto groupColumn = add.realtime.hash("GroupColumn").toDisplayString();
    if (!groupColumn.isEmpty()) {
        if (add.value->parent->console)
            ExternalSink::simplifyUnicode(groupColumn);
        auto check = combinedColumnData.find(groupColumn);
        if (check == combinedColumnData.end()) {
            combinedColumnOrder.push_back(groupColumn);
            check = combinedColumnData.emplace(groupColumn, std::vector<PageBreakdownData>()).first;
        }
        check->second.push_back(add);
        return;
    }

    if (add.realtime.hash("FullLine").toBool()) {
        fullLineData.push_back(add);
        return;
    }

    qint64 rowOrder = add.realtime.hash("RowOrder").toInt64();
    if (rowData.empty() || !INTEGER::equal(activeRowOrder, rowOrder)) {
        activeRowOrder = rowOrder;
        rowData.emplace_back();
    }

    rowData.back().push_back(add);
}

int RealtimeLayout::BoxBreakdownData::calculateRows(int dataColumns) const
{
    int rows = 0;

    /* Header */
    if (!arrayColumns.empty() || !combinedColumnOrder.empty())
        ++rows;

    /* Column orientated data */
    int maximumCombined = 0;
    for (const auto &add : combinedColumnData) {
        maximumCombined = qMax(maximumCombined, (int) add.second.size());
    }
    for (const auto &add : arrayColumns) {
        qint64 n = add.value->metadata.read().metadataArray("Count").toInt64();
        if (!INTEGER::defined(n) || n < 0)
            n = 0;
        n = qMax(n, (qint64) add.value->latest.read().toArray().size());
        maximumCombined = qMax(maximumCombined, (int) n);
    }
    rows += maximumCombined;

    /* Each set of grouped rows */
    for (const auto &add : rowData) {
        div_t dt = div((int) add.size(), dataColumns);
        rows += dt.quot;
        if (dt.rem != 0)
            ++rows;
    }

    /* All full line values */
    rows += fullLineData.size();

    return rows;
}

static Variant::Read metadataAbstractionLookup(const Variant::Read &metadata,
                                               const Variant::Read &realtime,
                                               const QString &key)
{
    if (realtime.hash(key).exists())
        return realtime.hash(key);
    if (metadata.getType() == Variant::Type::MetadataArray) {
        if (metadata.metadataArray("Children").metadata(key).exists())
            return metadata.metadataArray("Children").metadata(key);
    }
    return metadata.metadata(key);
}

RealtimeLayout::PageBreakdownData::PageBreakdownData() : value(nullptr),
                                                         realtime(Variant::Read::empty())
{ }

QString RealtimeLayout::PageBreakdownData::getFormatString() const
{
    return metadataAbstractionLookup(value->metadata, realtime, "Format").toDisplayString();
}

static std::unordered_map<std::string, QString> buildTranslation(const Variant::Read &t)
{
    std::unordered_map<std::string, QString> result;
    for (auto add : t.toHash()) {
        result.emplace(add.first, add.second.toDisplayString());
    }
    return result;
}

int RealtimeLayout::PageBreakdownData::getExpectedWidth() const
{
    if (value->latest.read().getType() == Variant::Type::String ||
            value->metadata.read().getType() == Variant::Type::MetadataString) {
        auto t = realtime.hash("Translation");
        auto children = t.toHash();
        if (!children.empty()) {
            int width = 0;
            for (auto add : children) {
                width = qMax(width, add.second.toQString().length());
            }
            auto check = children.find(value->latest.read().toString());
            if (check == children.end())
                width = qMax(width, value->latest.read().toDisplayString().length());
            return width;
        } else {
            return value->latest.read().toDisplayString().length();
        }
    } else if (value->latest.read().getType() == Variant::Type::Boolean ||
            value->metadata.read().getType() == Variant::Type::MetadataBoolean) {
        auto t = realtime.hash("Translation");
        int widthTrue =
                qMax(QObject::tr("TRUE").length(), t.hash("TRUE").toDisplayString().length());
        int widthFalse =
                qMax(QObject::tr("FALSE").length(), t.hash("FALSE").toDisplayString().length());
        return qMax(widthTrue, widthFalse);
    } else if (value->latest.read().getType() == Variant::Type::Hash ||
            value->metadata.read().getType() == Variant::Type::MetadataHash) {
        return RealtimeLayout::hashFormat(realtime.hash("HashFormat").toDisplayString(),
                                          value->latest,
                                          buildTranslation(realtime.hash("Translation"))).length();
    }

    NumberFormat nf(getFormatString());
    return nf.width();
}

int RealtimeLayout::PageBreakdownData::getIntegerWidth() const
{
    if (value->latest.read().getType() == Variant::Type::String ||
            value->metadata.read().getType() == Variant::Type::MetadataString ||
            value->latest.read().getType() == Variant::Type::Boolean ||
            value->metadata.read().getType() == Variant::Type::MetadataBoolean) {
        return 0;
    }

    NumberFormat nf(getFormatString());
    return nf.getIntegerDigits();
}

QString RealtimeLayout::convertConsoleUnits(const QString &input)
{
    if (input == QString::fromUtf8("\xC2\xB0\x43"))
        return QObject::tr("C", "temperature unit");
    if (input == QString::fromUtf8("Mm\xE2\x81\xBB¹"))
        return QObject::tr("Mm-1", "extinction component unit");
    if (input == QString::fromUtf8("cm\xE2\x81\xBB³"))
        return QObject::tr("cm-3", "concentration unit");
    if (input == QString::fromUtf8("\xCE\xBCm"))
        return QObject::tr("um", "micrometer unit");
    if (input == QString::fromUtf8("m\xE2\x81\xBB¹"))
        return QObject::tr("m-1", "inverse meters unit");
    if (input == QString::fromUtf8("\xCE\xBCg/m\xC2\xB3"))
        return QObject::tr("ug/m3", "grams per m3 unit");
    if (input == QString::fromUtf8("\xC2\xB0"))
        return QObject::tr("deg", "degrees unit");

    QString result(input);
    ExternalSink::simplifyUnicode(result);
    return input;
}

QString RealtimeLayout::PageBreakdownData::getUnitText() const
{
    QString unit(metadataAbstractionLookup(value->metadata, realtime, "Units").toDisplayString());
    if (unit.isEmpty())
        return unit;
    if (!value->parent->console)
        return unit;

    return RealtimeLayout::convertConsoleUnits(unit);
}

QString RealtimeLayout::PageBreakdownData::getGroupLabel() const
{
    QString label;
    if (realtime.hash("GroupName").exists()) {
        label = realtime.hash("GroupName").toDisplayString();
    } else if (realtime.hash("Name").exists()) {
        label = realtime.hash("Name").toDisplayString();
    } else {
        label = value->unit.getVariableQString();
        int idxUnderscore = label.indexOf('_');
        if (idxUnderscore > 0)
            label.truncate(idxUnderscore);
    }

    QString unit(getUnitText());
    if (!unit.isEmpty()) {
        if (!label.isEmpty()) {
            label.append(" (");
            label.append(unit);
            label.append(')');
        } else {
            label = unit;
        }
    }

    if (!label.isEmpty())
        label.append(':');
    if (value->parent->console)
        ExternalSink::simplifyUnicode(label);
    return label;
}

QString RealtimeLayout::PageBreakdownData::getArrayLabel() const
{
    QString label;
    if (realtime.hash("Name").exists()) {
        label = realtime.hash("Name").toDisplayString();
    } else {
        label = value->unit.getVariableQString();
        int idxUnderscore = label.indexOf('_');
        if (idxUnderscore > 0)
            label.truncate(idxUnderscore);
    }

    QString unit(getUnitText());
    if (!unit.isEmpty()) {
        if (!label.isEmpty()) {
            label.append(" (");
            label.append(unit);
            label.append(')');
        } else {
            label = unit;
        }
    }

    if (value->parent->console)
        ExternalSink::simplifyUnicode(label);
    return label;
}

QString RealtimeLayout::PageBreakdownData::getLabel() const
{
    QString label;
    if (realtime.hash("Name").exists()) {
        label = realtime.hash("Name").toDisplayString();
    } else {
        label = value->unit.getVariableQString();
        int idxUnderscore = label.indexOf('_');
        if (idxUnderscore > 0)
            label.truncate(idxUnderscore);
    }

    QString unit(getUnitText());
    if (!unit.isEmpty()) {
        if (!label.isEmpty()) {
            label.append(" (");
            label.append(unit);
            label.append(')');
        } else {
            label = unit;
        }
    }

    if (!label.isEmpty())
        label.append(':');
    if (value->parent->console)
        ExternalSink::simplifyUnicode(label);
    return label;
}

RealtimeLayout::LayoutFormatter *RealtimeLayout::PageBreakdownData::createFormatter(int width,
                                                                                    const Variant::Path &path) const
{
    LayoutFormatter *format = new LayoutFormatter;
    Q_ASSERT(format);
    format->parent = value->parent;
    format->fanout = value->fanout;
    format->unit = value->unit;
    format->path = path;

    auto mdCheck = realtime.hash("MetaFlags");
    if (mdCheck.getType() != Variant::Type::MetadataFlags &&
            value->metadata.read().getType() == Variant::Type::MetadataArray) {
        mdCheck = value->metadata.read().metadataArray("Children");
    }
    if (mdCheck.getType() != Variant::Type::MetadataFlags) {
        mdCheck = value->metadata;
    }
    if (mdCheck.getType() == Variant::Type::MetadataFlags) {
        for (auto add : mdCheck.toMetadataSingleFlag()) {
            auto bits = add.second.hash("Bits").toInteger();
            if (!INTEGER::defined(bits))
                continue;
            format->flagsTranslation.emplace(add.first, bits);
        }
    }

    format->stringTranslation = buildTranslation(realtime.hash("Translation"));
    format->hashFormat = realtime.hash("HashFormat").toDisplayString();

    format->format = NumberFormat(getFormatString());

    if (value->latest.read().getType() == Variant::Type::String ||
            value->metadata.read().getType() == Variant::Type::MetadataString ||
            value->latest.read().getType() == Variant::Type::Boolean ||
            value->metadata.read().getType() == Variant::Type::MetadataBoolean)
        return format;

    if (format->format.getIntegerDigits() < width) {
        format->prefix = QString(width - format->format.getIntegerDigits(), QChar(' '));
    }

    return format;
}

static void addToColumnsWidth(std::vector<int> &widths, size_t col, int add)
{
    if (widths.size() <= col)
        widths.resize(col + 1, 0);
    if (widths[col] >= add)
        return;
    widths[col] = add;
}

void RealtimeLayout::BoxBreakdownData::calculateColumns(std::vector<int> &widths,
                                                        int dataColumns) const
{

    int column = 0;
    if (!combinedColumnOrder.empty()) {
        column = 1;
    } else if (!rowData.empty()) {
        column = 1;
    }

    /* Column combined */
    for (auto add = combinedColumnOrder.begin(), endAdd = combinedColumnOrder.end();
            add != endAdd;
            ++add, ++column) {
        /* Header */
        addToColumnsWidth(widths, column, add->length() + 1);

        auto lookup = combinedColumnData.find(*add);
        Q_ASSERT(lookup != combinedColumnData.end());

        /* Data */
        for (const auto &data : lookup->second) {
            /* Cell data */
            addToColumnsWidth(widths, column, data.getExpectedWidth() + 1);

            /* Column label */
            addToColumnsWidth(widths, 0, data.getGroupLabel().length() + 1);
        }
    }

    /* Array columns */
    for (auto add = arrayColumns.begin(), endAdd = arrayColumns.end();
            add != endAdd;
            ++add, ++column) {
        /* Header */
        addToColumnsWidth(widths, column, add->getArrayLabel().length() + 1);

        /* Data width */
        addToColumnsWidth(widths, column, add->getExpectedWidth() + 1);
    }

    /* All row groups */
    for (const auto &group : rowData) {
        div_t dt = div(group.size(), dataColumns);
        int maximumRows = dt.quot;
        if (dt.rem != 0)
            ++maximumRows;

        column = 0;
        int row = 0;
        for (const auto &data : group) {
            /* Label */
            addToColumnsWidth(widths, column * 2, data.getLabel().length() + 1);

            /* Data */
            addToColumnsWidth(widths, column * 2 + 1, data.getExpectedWidth() + 1);

            ++row;
            if (row >= maximumRows) {
                row = 0;
                ++column;
            }
        }
    }
}

void RealtimeLayout::BoxBreakdownData::calculateFormatColumns(std::vector<int> &widths,
                                                              int dataColumns) const
{

    int column = 0;
    if (!combinedColumnOrder.empty()) {
        column = 1;
    } else if (!rowData.empty()) {
        column = 1;
    }

    /* Column combined */
    for (auto add = combinedColumnOrder.begin(), endAdd = combinedColumnOrder.end();
            add != endAdd;
            ++add, ++column) {
        auto lookup = combinedColumnData.find(*add);
        Q_ASSERT(lookup != combinedColumnData.end());

        /* Data */
        for (const auto &data : lookup->second) {
            /* Cell data */
            addToColumnsWidth(widths, column, data.getIntegerWidth());
        }
    }

    /* Array columns */
    for (auto add = arrayColumns.begin(), endAdd = arrayColumns.end();
            add != endAdd;
            ++add, ++column) {
        /* Data width */
        addToColumnsWidth(widths, column, add->getIntegerWidth());
    }

    /* All row groups */
    for (const auto &group : rowData) {
        div_t dt = div(group.size(), dataColumns);
        int maximumRows = dt.quot;
        if (dt.rem != 0)
            ++maximumRows;

        column = 0;
        int row = 0;
        for (const auto &data : group) {
            /* Data */
            addToColumnsWidth(widths, column * 2 + 1, data.getIntegerWidth());

            ++row;
            if (row >= maximumRows) {
                row = 0;
                ++column;
            }
        }
    }
}

int RealtimeLayout::BoxBreakdownData::getMinimumGlobalWidth() const
{
    int totalWidth = 0;
    for (const auto &add : fullLineData) {
        QString label(add.getLabel().length());
        if (!label.isEmpty()) {
            totalWidth = qMax(totalWidth, label.length() + 1 + add.getExpectedWidth());
        } else {
            totalWidth = qMax(totalWidth, add.getExpectedWidth());
        }
    }
    return totalWidth;
}

void RealtimeLayout::BoxBreakdownData::generateBoxData(std::vector<LayoutElement> &output,
                                                       int &uid,
                                                       int dataColumns) const
{
    std::vector<int> columnFormats;
    calculateFormatColumns(columnFormats, dataColumns);

    int column = 0;
    if (!combinedColumnOrder.empty()) {
        column = 1;
    } else if (!rowData.empty()) {
        column = 1;
    }

    int rowBase = 0;
    int rowMax = 0;

    /* Group columns */
    std::vector<int> groupColumnLabels;
    for (auto add = combinedColumnOrder.begin(), endAdd = combinedColumnOrder.end();
            add != endAdd;
            ++add, ++column) {
        /* Column header */
        output.emplace_back();
        output.back().uid = uid++;
        output.back().type = CenteredLabel;
        output.back().row = rowBase;
        output.back().column = column;
        output.back().text = *add;
        rowMax = qMax(rowMax, rowBase + 1);

        auto lookup = combinedColumnData.find(*add);
        Q_ASSERT(lookup != combinedColumnData.end());

        /* Data */
        int row = rowBase + 1;
        for (auto data = lookup->second.begin(), endData = lookup->second.end();
                data != endData;
                ++data, ++row) {
            /* Cell data */
            output.emplace_back();
            output.back().uid = uid++;
            output.back().type = ValueText;
            output.back().row = row;
            output.back().column = column;
            output.back().origin.reset(data->createFormatter(columnFormats[column]));
            output.back().text = output.back().origin->generate();

            /* Row label, but only once for all the columns */
            while (row >= (int) groupColumnLabels.size()) {
                groupColumnLabels.push_back(-1);
            }
            int targetLabel;
            if (groupColumnLabels[row] < 0) {
                targetLabel = output.size();
                output.emplace_back();
                output.back().uid = uid++;
                output.back().type = LeftJustifiedLabel;
                output.back().row = row;
                output.back().column = 0;
                groupColumnLabels[row] = targetLabel;
            } else {
                targetLabel = groupColumnLabels[row];
            }
            output[targetLabel].text = data->getGroupLabel();

            rowMax = qMax(rowMax, row + 1);
        }
    }

    /* Array columns */
    for (auto add = arrayColumns.begin(), endAdd = arrayColumns.end();
            add != endAdd;
            ++add, ++column) {
        /* Column header */
        output.emplace_back();
        output.back().uid = uid++;
        output.back().type = CenteredLabel;
        output.back().row = rowBase;
        output.back().column = column;
        output.back().text = add->getArrayLabel();
        rowMax = qMax(rowMax, rowBase + 1);

        qint64 n = add->value->metadata.read().metadataArray("Count").toInt64();
        if (!INTEGER::defined(n) || n < 0)
            n = 0;
        n = qMax(n, (qint64) add->value->latest.read().toArray().size());

        int row = rowBase + 1;
        for (int i = 0, max = (int) n; i < max; i++, row++) {
            auto vd = add->value->latest.read().array(i);

            /* Cell data */
            output.emplace_back();
            output.back().uid = uid++;
            output.back().type = ValueText;
            output.back().row = row;
            output.back().column = column;
            output.back()
                  .origin
                  .reset(add->createFormatter(columnFormats[column], vd.currentPath()));
            output.back().text = output.back().origin->generate();

            rowMax = qMax(rowMax, row + 1);
        }
    }

    /* All row groups */
    for (auto group = rowData.begin(), endGroup = rowData.end(); group != endGroup; ++group) {
        rowBase = rowMax;

        div_t dt = div(group->size(), dataColumns);
        int maximumRows = dt.quot;
        if (dt.rem != 0)
            ++maximumRows;

        int column = 0;
        int row = 0;
        for (const auto &data : *group) {
            /* Label */
            output.emplace_back();
            output.back().uid = uid++;
            output.back().type = LeftJustifiedLabel;
            output.back().row = row + rowBase;
            output.back().column = column * 2;
            output.back().text = data.getLabel();

            /* Cell data */
            output.emplace_back();
            output.back().uid = uid++;
            output.back().type = ValueText;
            output.back().row = row + rowBase;
            output.back().column = column * 2 + 1;
            output.back().origin.reset(data.createFormatter(columnFormats[column * 2 + 1]));
            output.back().text = output.back().origin->generate();

            rowMax = qMax(rowMax, row + rowBase + 1);

            ++row;
            if (row >= maximumRows) {
                row = 0;
                ++column;
            }
        }
    }

    rowBase = rowMax;

    /* Full lines */
    for (auto add = fullLineData.begin(), endAdd = fullLineData.end();
            add != endAdd;
            ++add, ++rowBase) {
        output.emplace_back();
        output.back().uid = uid++;
        output.back().type = ValueLine;
        output.back().row = rowBase;
        output.back().column = 0;
        output.back().origin.reset(add->createFormatter(0));
        output.back().origin->prefix = add->getLabel();
        if (!output.back().origin->prefix.isEmpty())
            output.back().origin->prefix.append(' ');
        output.back().text = output.back().origin->generate();
    }
}

void RealtimeLayout::generatePage(std::vector<LayoutElement> &output,
                                  const std::vector<PageBreakdownData> &input) const
{

    std::vector<BoxBreakdownData> boxes;
    for (const auto &add : input) {
        auto boxID = add.realtime.hash("BoxID");
        if (!boxID.exists())
            boxID = add.realtime.hash("Box");
        if (boxes.empty() ||
                boxID.exists() != boxes.back().definedBox ||
                boxID.toString() != boxes.back().boxID) {
            boxes.emplace_back();

            boxes.back().definedBox = boxID.exists();
            boxes.back().boxTitle = add.realtime.hash("Box").toDisplayString();
            if (console) {
                boxes.back().boxTitle = removeConsoleEscapes(boxes.back().boxTitle);
            }
            boxes.back().boxID = boxID.toString();
            boxes.back().activeRowOrder = INTEGER::undefined();
        }

        boxes.back().addPageItem(add);
    }
    Q_ASSERT(!boxes.empty());

    /* Find the maximum number of always existing columns in any box as well
     * as the minimum global width of the whole display */
    int maximumFixedColumns = 0;
    int minimumGlobalWidth = 0;
    for (const auto &box : boxes) {
        int add = box.arrayColumns.size();
        if (!box.combinedColumnOrder.empty())
            add += box.combinedColumnOrder.size() + 1;
        maximumFixedColumns = qMax(maximumFixedColumns, add);

        minimumGlobalWidth = qMax(minimumGlobalWidth, box.getMinimumGlobalWidth());
    }

    /* See if we need to add more data columns */
    QSet<int> validDataColumns;
    for (int dataColumns = 1, maxDataColumns = qMax((console ? 3 : 5), maximumFixedColumns / 2);
            dataColumns <= maxDataColumns;
            dataColumns++) {

        int rowCount = 0;
        int totalWidth = 0;

        for (const auto &box : boxes) {
            int boxDataRows = box.calculateRows(dataColumns);
            Q_ASSERT(boxDataRows != 0);

            if (!box.definedBox) {
                /* Dividing line on the last if coming from another box */
                if (rowCount != 0)
                    ++rowCount;
            } else {
                /* Dividing line between boxes, but also on the first if it's
                 * got a title */
                if (rowCount != 0 || !box.boxTitle.isEmpty())
                    ++rowCount;
            }

            rowCount += boxDataRows;

            std::vector<int> columnWidths;
            box.calculateColumns(columnWidths, dataColumns);
            int localWidth = 0;
            for (std::vector<int>::const_iterator add = columnWidths.begin(),
                    endAdd = columnWidths.end(); add != endAdd; ++add) {
                localWidth += *add;
            }
            totalWidth = qMax(totalWidth, localWidth);
        }

        if (console) {
            if (rowCount > 21)
                continue;
        } else {
            if (rowCount > 40)
                continue;
        }

        totalWidth = qMax(minimumGlobalWidth, totalWidth);
        if (console) {
            if (totalWidth > 70)
                continue;
        } else {
            if (totalWidth > 160)
                continue;
        }

        validDataColumns.insert(dataColumns);
    }

    /* Pick the optimal number of data columns, with the exception
     * that we'll use two in console mode if we're being forced to do
     * that anyway */
    int dataColumns = 1;
    if (!validDataColumns.isEmpty()) {
        if (console && maximumFixedColumns >= 4 && validDataColumns.contains(2)) {
            dataColumns = 2;
        } else {
            QList<int> sorted(validDataColumns.values());
            std::sort(sorted.begin(), sorted.end());
            dataColumns = sorted.at(0);
        }
    }


    int uid = 0;
    for (auto box = boxes.begin(), endBoxes = boxes.end(), lastBox = boxes.end() - 1;
            box != endBoxes;
            ++box) {
        if (box == lastBox) {
            LayoutElement add;
            add.uid = uid++;
            add.type = FinalGroupBox;
            add.text = box->boxTitle;
            output.push_back(add);
        } else {
            LayoutElement add;
            add.uid = uid++;
            add.type = GroupBox;
            add.text = box->boxTitle;
            output.push_back(add);
        }

        box->generateBoxData(output, uid, dataColumns);
    }
}

RealtimeLayout::LayoutElement::LayoutElement() : type(LeftJustifiedLabel),
                                                 uid(-1),
                                                 row(0),
                                                 column(0),
                                                 text(),
                                                 origin()
{ }

RealtimeLayout::LayoutElement::~LayoutElement() = default;

RealtimeLayout::LayoutElement::LayoutElement(const LayoutElement &other) : type(other.type),
                                                                           uid(other.uid),
                                                                           row(other.row),
                                                                           column(other.column),
                                                                           text(other.text),
                                                                           origin()
{
    if (other.origin)
        origin.reset(new LayoutFormatter(*other.origin));
}

RealtimeLayout::LayoutElement &RealtimeLayout::LayoutElement::operator=(const LayoutElement &other)
{
    type = other.type;
    uid = other.uid;
    row = other.row;
    column = other.column;
    text = other.text;
    if (other.origin)
        origin.reset(new LayoutFormatter(*other.origin));
    else
        origin.reset();
    return *this;
}

RealtimeLayout::LayoutElement::LayoutElement(LayoutElement &&other) = default;

RealtimeLayout::LayoutElement &RealtimeLayout::LayoutElement::operator=(LayoutElement &&other) = default;

bool RealtimeLayout::PageBreakdownData::operator<(const PageBreakdownData &other) const
{
    qint64 rowA = realtime.hash("RowOrder").toInt64();
    qint64 rowB = other.realtime.hash("RowOrder").toInt64();
    if (!INTEGER::defined(rowB)) {
        if (INTEGER::defined(rowA))
            return true;
    } else {
        if (!INTEGER::defined(rowA))
            return false;
        if (rowA != rowB)
            return rowA < rowB;
    }

    QString groupA(realtime.hash("GroupColumn").toDisplayString());
    QString groupB(other.realtime.hash("GroupColumn").toDisplayString());
    if (groupB.isEmpty()) {
        if (!groupA.isEmpty())
            return true;
    } else {
        if (groupA.isEmpty())
            return false;

        qint64 colA = realtime.hash("ColumnOrder").toInt64();
        qint64 colB = other.realtime.hash("ColumnOrder").toInt64();
        if (!INTEGER::defined(colB)) {
            if (INTEGER::defined(colA))
                return true;
        } else {
            if (!INTEGER::defined(colA))
                return false;
            if (colA != colB)
                return colA < colB;
        }
    }

    groupA = realtime.hash("GroupName").toDisplayString();
    groupB = other.realtime.hash("GroupName").toDisplayString();
    if (groupB.isEmpty()) {
        if (!groupA.isEmpty())
            return true;
    } else {
        if (groupA.isEmpty())
            return false;
        if (groupA != groupB)
            return groupA < groupB;
    }

    qint64 sortA = realtime.hash("SortOrder").toInt64();
    qint64 sortB = other.realtime.hash("SortOrder").toInt64();
    if (!INTEGER::defined(sortB)) {
        if (INTEGER::defined(sortA))
            return true;
    } else {
        if (!INTEGER::defined(sortA))
            return false;
        if (sortA != sortB)
            return sortA < sortB;
    }

    /*
     * Separate since we want to sort by variable, then size, but the default is size then
     * variable.  This is so that multiple instances of the same size are clustered together
     * for instruments that measure concurrently).
     */
    auto varA = SequenceName::OrderDisplay::priority(value->unit.getVariable());
    auto varB = SequenceName::OrderDisplay::priority(other.value->unit.getVariable());
    if (varA != varB)
        return varA < varB;

    return SequenceName::OrderDisplay()(value->unit, other.value->unit);
}

bool RealtimeLayout::update()
{
    if (!layoutOutdated) {
        bool valid = true;

        for (auto &page : layout) {
            for (auto &data : page) {
                if (data.origin) {
                    bool localValid;
                    data.text = data.origin->generate(&localValid);
                    if (!localValid) {
                        valid = false;
                        break;
                    }
                }
            }
        }

        for (const auto &fd : fanoutTracking) {
            fd.second->updated = false;
        }

        if (valid)
            return false;
    } else {
        for (const auto &fd : fanoutTracking) {
            fd.second->updated = false;
        }
    }
    layoutOutdated = false;

    std::vector<std::vector<PageBreakdownData> > pageBreakdown;
    std::vector<PageBreakdownData> allPageData;
    for (const auto &fd : fanoutTracking) {
        Variant::Read md;
        ValueData *add = nullptr;
        for (auto check : fd.second->sources) {
            auto v = check->metadata.read().metadata("Realtime");
            if (!v.exists())
                continue;
            if (!add) {
                /* If we have no existing value, then use any available one */
                md = v;
                add = check;
                continue;
            }

            /* Pick the first non-hidden and defined value */
            if (v.hash("Hide").toBool())
                continue;
            if (!Variant::Composite::isDefined(check->latest.read()))
                continue;

            md = v;
            add = check;
            break;
        }
        if (!add)
            continue;

        switch (md.getType()) {
        case Variant::Type::Hash: {
            if (md.hash("Hide").toBool())
                break;

            PageBreakdownData data;
            data.value = add;
            data.realtime = md;

            qint64 page = md.hash("Page").toInt64();
            if (!INTEGER::defined(page)) {
                page = md.hash("PageMask").toInt64();
                if (INTEGER::defined(page)) {
                    for (int i = 0; i < 63; i++) {
                        if (!(page & (Q_INT64_C(1) << i)))
                            continue;
                        while ((int) pageBreakdown.size() <= i) {
                            pageBreakdown.push_back(allPageData);
                        }
                        pageBreakdown[i].push_back(data);
                    }
                    break;
                }
                page = 0;
            }

            if (page < 0) {
                allPageData.push_back(data);
                for (auto &ins : pageBreakdown) {
                    ins.push_back(data);
                }
            } else {
                while ((int) pageBreakdown.size() <= page) {
                    pageBreakdown.push_back(allPageData);
                }
                pageBreakdown[page].push_back(data);
            }
            break;
        }

        case Variant::Type::Array: {
            PageBreakdownData data;
            data.value = add;

            auto children = md.toArray();
            for (int page = 0, max = (int) children.size(); page < max; ++page) {
                auto local = children[page];
                if (!local.exists() ||
                        (local.getType() == Variant::Type::Boolean && !local.toBool()) ||
                        local.hash("Hide").toBool())
                    continue;
                data.realtime = local;
                while ((int) pageBreakdown.size() <= page) {
                    pageBreakdown.push_back(allPageData);
                }
                pageBreakdown[page].push_back(data);
            }
        }

        case Variant::Type::Boolean: {
            if (!md.toBool())
                break;
            if (pageBreakdown.empty())
                pageBreakdown.push_back(allPageData);
            PageBreakdownData data;
            data.value = add;
            pageBreakdown.back().push_back(data);
            break;
        }
        default:
            break;
        }
    }


    layout.clear();

    for (auto &ins : pageBreakdown) {
        if (ins.empty())
            continue;
        std::sort(ins.begin(), ins.end());

        layout.emplace_back();
        generatePage(layout.back(), ins);
    }

    return true;
}


RealtimeLayout::RealtimeLayout(bool consoleMode) : console(consoleMode),
                                                   values(),
                                                   dispatch(),
                                                   layoutOutdated(true),
                                                   layout()
{ }

RealtimeLayout::~RealtimeLayout() = default;

RealtimeLayout::ValueData::ValueData(RealtimeLayout *p, const SequenceName &u, FanoutData *fd)
        : parent(p), unit(u), fanout(fd), metadata(), latest()
{ }

void RealtimeLayout::ValueData::updateMetadata(const Variant::Root &update)
{
    parent->layoutOutdated = true;
    metadata = update;
}

void RealtimeLayout::ValueData::updateLatest(const Variant::Root &update)
{
    latest = update;
    fanout->updated = true;
}

RealtimeLayout::FanoutData *RealtimeLayout::lookupFanoutData(const SequenceName &unit)
{
    auto check = fanoutTracking.find(unit);
    if (check != fanoutTracking.end())
        return check->second.get();

    std::unique_ptr<FanoutData> add(new FanoutData);
    add->updated = true;
    return fanoutTracking.emplace(unit, std::move(add)).first->second.get();
}

RealtimeLayout::ValueData *RealtimeLayout::lookupValueData(const SequenceName &unit)
{
    auto check = values.find(unit);
    if (check != values.end())
        return check->second.get();

    FanoutData *fanout = lookupFanoutData(unit.withoutAllFlavors());
    std::unique_ptr<ValueData> add(new ValueData(this, unit, fanout));
    fanout->sources.emplace_back(add.get());

    struct SourceSortOrder {
        SequenceName::OrderDisplayCaching comp;

        inline bool operator()(const ValueData *a, const ValueData *b)
        { return comp(a->unit, b->unit); }
    };
    std::sort(fanout->sources.begin(), fanout->sources.end(), SourceSortOrder());

    return values.emplace(unit, std::move(add)).first->second.get();
}

RealtimeLayout::Dispatch::~Dispatch() = default;

RealtimeLayout::DispatchValue::DispatchValue(ValueData *t) : target(t)
{ }

RealtimeLayout::DispatchValue::~DispatchValue() = default;

void RealtimeLayout::DispatchValue::incomingData(const SequenceValue &value)
{ target->updateLatest(value.root()); }

RealtimeLayout::DispatchMetadata::DispatchMetadata(ValueData *t) : target(t)
{ }

RealtimeLayout::DispatchMetadata::~DispatchMetadata() = default;

void RealtimeLayout::DispatchMetadata::incomingData(const SequenceValue &value)
{ target->updateMetadata(value.root()); }

RealtimeLayout::DispatchDiscard::DispatchDiscard() = default;

RealtimeLayout::DispatchDiscard::~DispatchDiscard() = default;

void RealtimeLayout::DispatchDiscard::incomingData(const SequenceValue &)
{ }


void RealtimeLayout::incomingData(const SequenceValue &value)
{
    auto target = dispatch.find(value.getUnit());
    if (target != dispatch.end()) {
        target->second->incomingData(value);
        return;
    }

    std::unique_ptr<Dispatch> add;
    if (value.getUnit().hasFlavor("cover") || value.getUnit().hasFlavor("stats")) {
        add.reset(new DispatchDiscard);
    } else if (value.getUnit().isMeta()) {
        add.reset(new DispatchMetadata(lookupValueData(value.getUnit().fromMeta())));
    } else {
        add.reset(new DispatchValue(lookupValueData(value.getUnit())));
    }

    dispatch.emplace(value.getUnit(), std::move(add)).first->second->incomingData(value);
}

void RealtimeLayout::incomingData(const SequenceValue::Transfer &values)
{
    for (const auto &add : values) {
        incomingData(add);
    }
}

}
}
