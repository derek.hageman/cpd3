/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITIONCONTROLLER_H
#define CPD3ACQUISITIONCONTROLLER_H

#include "core/first.hxx"

#include <stdlib.h>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <unordered_map>
#include <functional>
#include <QRegularExpression>

#include "acquisition/acquisition.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/autoprobecontroller.hxx"
#include "acquisition/acquisitionvariableset.hxx"
#include "acquisition/acquisitionnetwork.hxx"
#include "acquisition/acquisitionarchive.hxx"
#include "core/timeutils.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/segment.hxx"
#include "database/runlock.hxx"
#include "datacore/dynamicinput.hxx"
#include "smoothing/realtimechain.hxx"
#include "smoothing/fixedtime.hxx"

#ifndef NDEBUG
#define ACQUISITION_CONTROLLER_LOGGING_TIME_DEBUGGING
#endif

namespace CPD3 {
namespace Acquisition {

/**
 * The control interface that handles a realtime acquisition system.  This
 * system consists of a set of active acquisition interfaces (fully
 * promotes instruments and fixed controllers) and a set of in progress
 * autoprobing interfaces.
 * <br>
 * The general structure breaks down into a list of interfaces and autoprobe
 * controllers.  When an autoprobe controller succeeds it is promoted to
 * an active interface.
 * <br>
 * Each active interface has a set of variable dispatch groups that track
 * the active state overlay for that group set of variables coming out of
 * the interface.  In general an interface will have one group that matches
 * the groups of the interface, but certain interfaces may have more (e.x.
 * a uMAC).  Each variable dispatch group is linked to a global variable
 * set, which represents the combination of all variables of the same
 * group into the main archive/realtime stream.
 * <br>
 * The global variable set handles the averaging (if any) and archive writing
 * of a stream of variables from interfaces.  That is, a group of variables
 * is all averaged together with the same averaging time base, the same
 * flushing, and flags handling.
 */
class CPD3ACQUISITION_EXPORT AcquisitionController final {
    std::mutex mutex;
    std::condition_variable request;
    std::thread thread;
    bool terminated;
    bool threadCompleted;
    std::vector<std::function<void()>> call;
    bool serviceRequired;
    double autoprobeRetryTime;

    void asyncCall(std::function<void()> c);

    void asyncDrain();

    using Groups = Data::Variant::Flags;

    Data::ValueSegment::Transfer config;
    std::string stateBaseName;
    double dataUpdateInterval;

    struct InterfaceComponentData {
        std::unique_ptr<IOTarget> target;
        QString componentName;
    };
    std::vector<InterfaceComponentData> interfaceComponentLookup;

    std::unique_ptr<AcquisitionNetworkServer> network;

    std::vector<std::unique_ptr<AutoprobeController>> autoprobe;

    class ActiveInterface;

    struct InterfaceSetSelection {
        QRegularExpression componentNameMatch;
        QRegularExpression sourceNameMatch;
        Groups groups;
    };
    std::vector<InterfaceSetSelection> interfaceSetList;

    struct AveragingInternval {
        Time::LogicalTimeUnit unit;
        int count;
        bool aligned;

        inline bool operator==(const AveragingInternval &other) const
        {
            return unit == other.unit && count == other.count && aligned == other.aligned;
        }
    };

    struct VariableSet {
        AcquisitionController &controller;
        Groups groups;

        Data::SinkMultiplexer loggingMux;
        Data::SinkMultiplexer::Sink *loggingSystemIngress;
        std::unique_ptr<Smoothing::SmoothingEngineFixedTime> loggingSmoother;
        std::unique_ptr<Data::StreamSink> loggingArchive;

        std::unique_ptr<Smoothing::RealtimeEngine> realtimeSmoother;

        AveragingInternval average;

        VariableSet(AcquisitionController &controller, Groups groups);

        ~VariableSet();

        void advance(double time);

        void setAveragingTime(const AveragingInternval &change);
    };

    struct GroupsHash {
        std::size_t operator()(const Groups &groups) const;
    };

    static bool groupsOverlap(const Groups &a, const Groups &b);

    std::unordered_map<Groups, std::unique_ptr<VariableSet>, GroupsHash> variableSets;

    std::unordered_map<Groups, AveragingInternval, GroupsHash> groupAveragingTime;
    AveragingInternval defaultAverageTime;
    bool defaultEnableStatistics;
    bool defaultEnableRealtimeStatistics;
    bool defaultEnableRealtimeDiscard;

    Data::Variant::Flags networkInjectedBypassFlags;
    Data::Variant::Flags networkInjectedSystemFlags;

    struct VariableSetSelection {
        Data::SequenceMatch::Composite selection;
        Groups groups;
    };
    std::vector<VariableSetSelection> globalVariableGroupList;

    class LoggingIngress : public Data::StreamSink {
        ActiveInterface &interface;
    public:
        LoggingIngress(ActiveInterface &interface);

        virtual ~LoggingIngress();

        void incomingData(const Data::SequenceValue::Transfer &values) override;

        void incomingData(Data::SequenceValue::Transfer &&values) override;

        void incomingData(const Data::SequenceValue &value) override;

        void incomingData(Data::SequenceValue &&value) override;

        void endData() override;
    };

    class RealtimeIngress : public Data::StreamSink {
        ActiveInterface &interface;
    public:
        RealtimeIngress(ActiveInterface &interface);

        virtual ~RealtimeIngress();

        void incomingData(const Data::SequenceValue::Transfer &values) override;

        void incomingData(Data::SequenceValue::Transfer &&values) override;

        void incomingData(const Data::SequenceValue &value) override;

        void incomingData(Data::SequenceValue &&value) override;

        void endData() override;
    };

    class PersistentIngress : public Data::StreamSink {
        ActiveInterface &interface;
    public:
        PersistentIngress(ActiveInterface &interface);

        virtual ~PersistentIngress();

        void incomingData(const Data::SequenceValue::Transfer &values) override;

        void incomingData(Data::SequenceValue::Transfer &&values) override;

        void incomingData(const Data::SequenceValue &value) override;

        void incomingData(Data::SequenceValue &&value) override;

        void endData() override;
    };

    class ActiveInterface {
        friend class AcquisitionController;

        class ControllerInterface
                : virtual public AcquisitionState, virtual public AcquisitionControlStream {
            AcquisitionController &controller;
            ActiveInterface &interface;
            bool passive;
        public:
            Threading::Receiver receiver;

            ControllerInterface(AcquisitionController &controller,
                                ActiveInterface &interface,
                                bool passive);

            virtual ~ControllerInterface();

            void writeControl(const Util::ByteView &data) override;

            void writeControl(Util::ByteArray &&data) override;

            void requestTimeout(double time) override;

            void resetControl() override;

            void remap(const Data::SequenceName::Component &name,
                       Data::Variant::Root &value) override;

            void setBypassFlag(const Data::Variant::Flag &flag = "Bypass") override;

            void clearBypassFlag(const Data::Variant::Flag &flag = "Bypass") override;

            void setSystemFlag(const Data::Variant::Flag &flag = "Contaminated") override;

            void clearSystemFlag(const Data::Variant::Flag &flag = "Contaminated") override;

            void requestFlush(double seconds = -1.0) override;

            void sendCommand(const std::string &target,
                             const Data::Variant::Read &command) override;

            void setSystemFlavors(const Data::SequenceName::Flavors &flavors) override;

            void setAveragingTime(Time::LogicalTimeUnit unit,
                                  int count,
                                  bool setAligned = true) override;

            void requestStateSave() override;

            void requestGlobalStateSave() override;

            void event(double time,
                       const QString &text,
                       bool showRealtime = true,
                       const Data::Variant::Read &information = Data::Variant::Read::empty()) override;

            void realtimeAdvanced(double time) override;

            void loggingHalted(double time) override;

            void requestDataWakeup(double time) override;

            void requestControlWakeup(double time) override;

            void interfaceInformationUpdated();

            void interfaceStateUpdated();

            void persistentValuesUpdated();
        };

        AcquisitionController &controller;

        std::unique_ptr<ControllerInterface> controllerInterface;

        std::unique_ptr<AcquisitionInterface> acquisition;
        std::unique_ptr<IOInterface> io;
        bool wasAutoprobed;
        Groups groups;
        Groups outputGroups;
        QString sourceName;
        QString componentName;
        QString menuCharacter;

        struct InterfaceVariableSet {
            double flushEndTime;

            double loggingBeginTime;
#ifdef ACQUISITION_CONTROLLER_LOGGING_TIME_DEBUGGING
            double loggingLastAdvance;
#endif
            bool loggingEnded;
            std::unique_ptr<AcquisitionVariableSet> set;

            Data::SequenceValue::Transfer pendingLoggingData;
            Data::SequenceValue::Transfer pendingRealtimeData;

            InterfaceVariableSet();
        };

        struct InterfaceSetProcessData {
            InterfaceVariableSet *set;
            Data::SequenceValue::Transfer processLogging;
            Data::SequenceValue::Transfer processRealtime;
            double loggingAdvanceTime;
            bool loggingEnded;

            InterfaceSetProcessData();

            InterfaceSetProcessData(InterfaceVariableSet *target, double advance);
        };

        std::unordered_map<Groups, std::unique_ptr<InterfaceVariableSet>, GroupsHash>
                interfaceVariableSets;

        struct VariableDispatch {
            Data::SequenceName unit;
            InterfaceVariableSet *target;
            bool isMeta;

            VariableDispatch();
        };

        Data::SequenceName::Map<std::unique_ptr<VariableDispatch>> variableDispatch;

        std::vector<VariableSetSelection> variableGroupList;

        class Remapping {
            std::mutex mutex;
            AcquisitionController &controller;
            std::unique_ptr<Data::DynamicInput> input;
            std::unique_ptr<Calibration> calibration;
        public:
            Remapping(AcquisitionController &controller,
                      std::unique_ptr<Data::DynamicInput> &&input,
                      std::unique_ptr<Calibration> &&calibration);

            ~Remapping();

            void apply(Data::Variant::Root &value);

            void handleNewUnits(const QSet<Data::SequenceName> &units);
        };

        std::unordered_map<std::string, std::unique_ptr<Remapping>> remapData;

        double dataWakeup;
        double controlWakeup;
        double controlTimeout;
        Util::ByteArray controlWrite;
        bool controlReset;
        double loggingHalt;
        double loggingLatest;
#ifdef ACQUISITION_CONTROLLER_LOGGING_TIME_DEBUGGING
        double loggingOrderCheck;
#endif
        std::vector<Util::ByteArray> pendingData;
        std::vector<Util::ByteArray> pendingControl;

        Data::SequenceName::Flavors systemFlavors;
        bool systemFlavorsUpdated;
        Data::Variant::Flags activeBypassFlags;
        bool activeBypassFlagsUpdated;
        Data::Variant::Flags previousBypass;
        Data::Variant::Flags activeSystemFlags;
        bool activeSystemFlagsUpdated;
        Data::Variant::Flags previousSystemFlags;
        double flushRequestTime;
        double flushEndTime;

        bool requestStateSave;
        bool requestPersistentUpdate;

        Data::ValueSegment::Transfer config;
        double defaultFlushTime;

        LoggingIngress localLoggingIngress;
        RealtimeIngress localRealtimeIngress;
        PersistentIngress localPersistentIngress;
        bool loggingEnded;

        Data::SequenceName::Component targetStation;
        bool realtimeDiscard;

        void handleSetCreation(double time);

        VariableDispatch *lookupVariableDispatch(const Data::SequenceName &unit);

    public:
        ActiveInterface(AcquisitionController &controller,
                        std::unique_ptr<AcquisitionInterface> &&acquisition,
                        std::unique_ptr<IOInterface> &&io,
                        const Data::ValueSegment::Transfer &configuration,
                        bool passive);

        ~ActiveInterface();

        void handleShutdown(double time);

        void handleNewUnits(const QSet<Data::SequenceName> &units) const;

        double advanceData(double time, bool processIO = true);

        void advanceState(double time);

        void processData(double time, bool inShutdown = false);

        void saveState();

        void restoreState();

        void writePersistentValues();

        void issueFlush(const Groups &groups, double endTime);

        void issueGlobalFlush(double seconds);

        void averagingChanged(const Groups &groups);

        void averagingChanged();

        void updateSystemFlavors(const Groups &groups,
                                 std::unordered_map<Groups, Data::SequenceName::Flavors,
                                                    GroupsHash> &lookup);

        void updateBypass(const Groups &groups,
                          std::unordered_map<Groups, Data::Variant::Flags, GroupsHash> &lookup);

        void updateGlobalBypass(std::unordered_map<Groups, Data::Variant::Flags,
                                                   GroupsHash> &lookup);

        void updateSystemFlags(const Groups &groups,
                               std::unordered_map<Groups, Data::Variant::Flags,
                                                  GroupsHash> &lookup);

        void updateGlobalSystemFlags(std::unordered_map<Groups, Data::Variant::Flags,
                                                        GroupsHash> &lookup);

        void updateSystemFlagsMetadata(const Groups &groups,
                                       std::unordered_map<Groups, Data::Variant::Root,
                                                          GroupsHash> &lookup);

        bool checkCompleted(double time);

        void handleLogging(Data::SequenceValue::Transfer &&values);

        void handleLogging(Data::SequenceValue &&value);

        void endLogging();

        void handleRealtime(Data::SequenceValue::Transfer &&values);

        void handleRealtime(Data::SequenceValue &&value);

        void handlePersistent(Data::SequenceValue::Transfer &&values);

        void handlePersistent(Data::SequenceValue &&value);

        Data::Variant::Root getCommands();

        void connectInterfaceData(bool startInterface);
    };

    std::vector<std::unique_ptr<ActiveInterface>> active;

    class SystemRealtimeIngress : public Data::StreamSink {
        AcquisitionController &controller;
    public:
        SystemRealtimeIngress(AcquisitionController &controller);

        virtual ~SystemRealtimeIngress();

        void incomingData(const Data::SequenceValue::Transfer &values) override;

        void incomingData(Data::SequenceValue::Transfer &&values) override;

        void incomingData(const Data::SequenceValue &value) override;

        void incomingData(Data::SequenceValue &&value) override;

        void endData() override;
    };

    SystemRealtimeIngress realtimeIngress;

    class SystemRealtimeDiscardedIngress : public SystemRealtimeIngress {
    public:
        SystemRealtimeDiscardedIngress(AcquisitionController &controller);

        virtual ~SystemRealtimeDiscardedIngress();

        void incomingData(const Data::SequenceValue::Transfer &values) override;

        void incomingData(Data::SequenceValue::Transfer &&values) override;

        void incomingData(const Data::SequenceValue &value) override;

        void incomingData(Data::SequenceValue &&value) override;
    };

    SystemRealtimeDiscardedIngress realtimeDiscardedIngress;

    Data::SequenceName::Component defaultStation;
    bool shutdownWithNoInterfaces;
    bool retryOnNoAutoprobe;

    struct PendingCommand {
        Groups groups;
        std::string target;
        Data::Variant::Root command;
    };
    std::mutex pendingCommandsMutex;
    std::vector<PendingCommand> pendingCommands;
    std::vector<PendingCommand> initializeCommands;

    std::mutex realtimeLatestMutex;
    Data::SequenceSegment realtimeLatest;
    QSet<Data::SequenceName> realtimeSeenUnits;
    QSet<Data::SequenceName> realtimeNewUnits;
    Data::SequenceValue::Transfer realtimeDistribute;

    Data::Variant::Root metadataProcessing;

    std::unique_ptr<AcquisitionArchive> archive;

    double scheduledWakeup;
    double lastDataProcess;
    double nextHearbeat;
    double nextFlush;
    bool inShutdown;
    enum class AutoprobeStatus {
        NeverSucceeded,
        HaveActive,
        HaveHadActive,
        RetryPendingWithSuccesses,
        RetryPendingNeverSucceeded,
    } autoprobeStatus;

    std::mutex injectPendingMutex;
    struct InterfaceInjectPending {
        AcquisitionComponent *component;
        Data::ValueSegment::Transfer config;
    };
    std::vector<InterfaceInjectPending> injectPending;

    double getFlushEndTime(const Groups &group) const;

    Data::SequenceName::Flavors getSystemFlavors(const Groups &group) const;

    Data::Variant::Flags getBypassFlags(const Groups &group) const;

    Data::Variant::Flags getSystemFlags(const Groups &group) const;

    Data::Variant::Root getSystemFlagsMetadata(const Groups &group) const;

    VariableSet &getVariableSet(const Groups &group);

    bool readyToExit();

    QString generateSourceName(double time, const QString &base, qint64 line);

    void setAveragingTime(const Groups &groups,
                          Time::LogicalTimeUnit unit,
                          int count,
                          bool aligned);

    void issueExistingRealtime(AcquisitionInterface *interface);

    void reassignMenuCharacters(ActiveInterface *skipExisting = NULL);

    void finalizeInterface(ActiveInterface *interface);

    void promoteAutoprobe(double time, AutoprobeController *controller);

    void loadComponentLookup();

    void fillNetworkEvent(Data::Variant::Write &event);

    inline void fillNetworkEvent(Data::Variant::Write &&event)
    { return fillNetworkEvent(event); }

    void createNetwork();

    void createFixedInterfaces();

    std::vector<
            VariableSetSelection> parseVariableSelection(const Data::Variant::Read &config) const;

    void createFixed(AcquisitionComponent *component,
                     const Data::ValueSegment::Transfer &config,
                     const std::string &componentName);

    void archiveEvent(double time,
                      const Data::SequenceName::Component &station,
                      CPD3::Data::Variant::Root &&content);

    void processEvent(double time,
                      const Data::SequenceName::Component &station,
                      const Groups &groups,
                      CPD3::Data::Variant::Root content);


    void serviceCommands();

    void serviceUpdate(bool disableThrottle = false);

    void checkCompleted();

    void beginAutoprobe();

    void saveComponentLookup();

    void updateAutoprobeStatus();

    void networkMessageLogEvent(const CPD3::Data::Variant::Root &event);

    void networkCommandReceived(const std::string &target,
                                const CPD3::Data::Variant::Root &command);

    void networkFlushRequest(double seconds);

    void networkSetAveragingTime(CPD3::Time::LogicalTimeUnit unit, int count, bool aligned);

    void networkBypassFlagSet(const CPD3::Data::Variant::Flag &flag);

    void networkBypassFlagClear(const CPD3::Data::Variant::Flag &flag);

    void networkBypassFlagClearAll();

    void networkSystemFlagSet(const CPD3::Data::Variant::Flag &flag);

    void networkSystemFlagClear(const CPD3::Data::Variant::Flag &flag);

    void networkSystemFlagClearAll();

    void startup();

    void shutdown();

    void processLoop();

    void run();

public:
    /**
     * Create a new acquisition controller.  The controller must be started
     * as a thread before it takes any action.
     * 
     * @param config            the configuration
     * @param defaultStation    the default station code
     * @param shutdownWithNoInterfaces when set, shut down when there are no more active interfaces possible
     */
    AcquisitionController(const Data::ValueSegment::Transfer &config,
                          const Data::SequenceName::Component &defaultStation = "nil",
                          bool shutdownWithNoInterfaces = true);

    virtual ~AcquisitionController();

    /**
     * Start the controller.
     */
    void start();

    /**
     * Inject an interface into the active set.  This is generally only
     * used for debugging.
     * 
     * @param component         the component
     * @param config            the configuration
     */
    void injectInterface(AcquisitionComponent *component,
                         const Data::ValueSegment::Transfer &config = {});

    /**
     * Emitted once the controller has finished starting and is ready to
     * handle event processing.  This can be used to delay the exit of
     * a daemonized process until the server is ready to accept connections.
     */
    Threading::Signal<> controllerReady;

    /**
     * Emitted when a restart of the system has been requested.  This does
     * NOT cause the system to begin a shutdown procedure, so that must
     * be initiated by external control if the signal is honored.
     */
    Threading::Signal<> restartRequested;

    /**
     * Emitted every five seconds from the main processing function.
     */
    Threading::Signal<> heartbeat;


    /**
     * Begin a fully controlled system shutdown.  All interfaces will complete
     * their full shutdown procedures.
     */
    void initiateShutdown();

    /**
     * Signal the entire system for termination.  This shuts down the system as
     * soon as possible.  Some interfaces may not finish their standard exit
     * cleanup, but all data will be preserved.
     */
    void signalTerminate();

    /**
     * Initiate a flush of data.  This causes all buffered log data to be written
     * to the archive.
     */
    void flushData();

    /**
     * Signal the system that the autoprobe backend has changed somehow.  This
     * causes all autoprobed interfaces to shut down and be re-created.  This
     * allows the system to account for them having been moved between different
     * system interfaces.
     */
    void autoprobeSystemChanged();

    /**
     * Wait for the controller to finish.
     *
     * @param timeout   the maximum time to wait
     * @return          true if the controller has finished
     */
    bool wait(double timeout = FP::undefined());

    /**
     * Emitted when the controller has finished execution.
     */
    Threading::Signal<> finished;
};

}
}


#endif
