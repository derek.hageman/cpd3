/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITIONSPANCHECK_H
#define CPD3ACQUISITIONSPANCHECK_H

#include "core/first.hxx"

#include <unordered_map>
#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QFlags>
#include <QSet>

#include "acquisition/acquisition.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/segment.hxx"
#include "datacore/stream.hxx"
#include "smoothing/baseline.hxx"
#include "algorithms/rayleigh.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * A class that provides control calculation for performing nephelometer
 * span checks.
 */
class CPD3ACQUISITION_EXPORT NephelometerSpancheckController {
public:
    /**
     * The base interface used by the spancheck controller to interface
     * with the underlying instrument and acquisition system.
     * <br>
     * By default all the handlers are stubs that do nothing.
     */
    class CPD3ACQUISITION_EXPORT Interface {
    public:
        Interface();

        virtual ~Interface();

        /**
         * Called when the controller needs to change the system
         * bypass state.
         * 
         * @param enable    enable the system bypass
         */
        virtual void setBypass(bool enable);

        /**
         * Called to switch the instrument to fill with filtered air.
         */
        virtual void switchToFilteredAir();

        /**
         * Called to switch the instrument to fill with the selected gas
         * (usually ignored as there is only one choice).
         * 
         * @param gas   the requested gas
         */
        virtual void switchToGas(Algorithms::Rayleigh::Gas gas);

        /**
         * Called to normal zero adjustment (usually as the final part
         * of the spancheck).
         */
        virtual void issueZero();

        /**
         * Called to request a general command to the acquisition system.
         * 
         * @param target    the command target
         * @param command   the command data
         */
        virtual void issueCommand(const QString &target, const Data::Variant::Read &command);

        /**
         * Called when the spancheck is initializing.
         * 
         * @return true if the spancheck can continue
         */
        virtual bool start();

        /**
         * Called when the spancheck is completed.
         */
        virtual void completed();

        /**
         * Called when the spancheck is aborted before completion.
         */
        virtual void aborted();
    };

    /** Spancheck calculation flags. */
    enum Flag {
        /** No additional flags. */
                None = 0x00,

        /** Use a chopper (and revolutions for it) in the calculation. */
        Chopper = 0x01,

        /** No count data is available. */
        NoCounts = 0x02,

        /** Calculate TSI 3563 neph calibrations */
        TSI3563Calibrations = 0x04,

        /** Calculate Ecotech Aurora neph calibrations */
        EcotechAuroraCalibrations = 0x08,

        /** Count data only available conditionally (e.x. only on some angles). */
        PartialCounts = 0x10,

        /** The default flags if none are given */
        Default = None,
    };
    Q_DECLARE_FLAGS(Flags, Flag);

    /** The type of parameter being reported. */
    enum Parameter {
        /** Instrument calculated scattering. */
                Scattering = 0,

        /** Measurement counts. */
                MeasurementCounts,

        /** Reference counts */
                ReferenceCounts,

        /** Dark counts. */
                DarkCounts,

        /** Sample temperature. */
                Temperature,

        /** Sample pressure */
                Pressure,

        /** Chopper revolutions. */
                Revolutions,
    };

    /** The type of result data to return. */
    enum ResultType {
        /** The general hash containing the full results. */
                FullResults,

        /** The average percent error over all channels and angles. */
                AveragePercentError,

        /** The array of angles. */
                Angles,

        /** The percent error array. */
                PercentError,

        /** The total scattering percent error. */
                TotalPercentError,

        /** The backwards scattering percent error. */
                BackPercentError,

        /** The sensitivity factor array. */
                SensitivityFactor,

        /** The total scattering sensitivity factor. */
                TotalSensitivityFactor,

        /** The backwards scattering sensitivity factor. */
                BackSensitivityFactor,

        /** The K2 value for the TSI 3563 calibration. */
                TSI3563K2,

        /** The K4 value for the TSI 3563 calibration. */
                TSI3563K4,

        /** The M value for the Ecotech Aurora calibration. */
                TotalEcotechAuroraM,

        /** The C value for the Ecotech Aurora calibration. */
                TotalEcotechAuroraC,

        /** The M value for the Ecotech Aurora calibration. */
                BackEcotechAuroraM,

        /** The C value for the Ecotech Aurora calibration. */
                BackEcotechAuroraC,

        /** The M value for the Ecotech Aurora polar calibration. */
                EcotechAuroraMPolar,

        /** The C value for the Ecotech Aurora polar calibration. */
                EcotechAuroraCPolar,

        /** The realtime state. */
                RealtimeState,
    };

private:
    Interface *interface;
    Flags flags;

    enum State {
        Inactive,

        Initializing,

        GasAirFlush, GasFlush, GasSample,

        AirFlush, AirSample,

        Aborting,
    };
    State state;
    double currentTime;
    double stateBeginTime;
    double stateEndTime;
    std::unordered_map<QString, Data::Variant::Read> pendingCommands;

    friend class Configuration;

    class Configuration {
        double start;
        double end;

        void setFromSegment(const Data::ValueSegment &config);

        void setCommands(std::unordered_map<QString, Data::Variant::Root> &target,
                         const Data::Variant::Read &add) const;

    public:
        double terminalAngle;

        Algorithms::Rayleigh::Gas gas;

        std::unordered_map<QString, Data::Variant::Root> commandsStart;
        std::unordered_map<QString, Data::Variant::Root> commandsEnd;

        std::unordered_map<QString, Data::Variant::Root> commandsActivateAir;
        std::unordered_map<QString, Data::Variant::Root> commandsDeactivateAir;
        std::unordered_map<QString, Data::Variant::Root> commandsActivateGas;
        std::unordered_map<QString, Data::Variant::Root> commandsDeactivateGas;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const Data::ValueSegment &over,
                      double start,
                      double end);
    };

    QList<Configuration> config;
    Data::DynamicTimeInterval *gasMinimumSample;
    Data::DynamicTimeInterval *gasMaximumSample;
    Data::DynamicTimeInterval *gasFlush;
    Data::DynamicTimeInterval *airMinimumSample;
    Data::DynamicTimeInterval *airMaximumSample;
    Data::DynamicTimeInterval *airFlush;

    class SampleData {
        QSet<Parameter> seenParameters;

        std::unique_ptr<Smoothing::BaselineSmoother> scattering;
        std::unique_ptr<Smoothing::BaselineSmoother> measurementCounts;
        std::unique_ptr<Smoothing::BaselineSmoother> referenceCounts;
        std::unique_ptr<Smoothing::BaselineSmoother> darkCounts;
        std::unique_ptr<Smoothing::BaselineSmoother> temperature;
        std::unique_ptr<Smoothing::BaselineSmoother> pressure;
        std::unique_ptr<Smoothing::BaselineSmoother> revolutions;
    public:
        SampleData() = delete;

        SampleData(const Data::ValueSegment::Transfer &config);

        SampleData(const SampleData &other);

        SampleData &operator=(const SampleData &other);

        SampleData(SampleData &&);

        SampleData &operator=(SampleData &&);

        ~SampleData();

        void reset();

        void overlay(const SampleData &other);

        void addValue(NephelometerSpancheckController::Parameter param, double value, double time);

        bool stable(bool requireRevolutions, bool requireCounts) const;

        inline double getScattering() const
        { return scattering->value(); }

        inline double getMeasurementCounts() const
        { return measurementCounts->value(); }

        inline double getReferenceCounts() const
        { return referenceCounts->value(); }

        inline double getDarkCounts() const
        { return darkCounts->value(); }

        inline double getTemperature() const
        { return temperature->value(); }

        inline double getPressure() const
        { return pressure->value(); }

        inline double getRevolutions() const
        { return revolutions->value(); }
    };

    class WavelengthData;

    class AngleData {
        SampleData gas;
        SampleData air;

        friend class WavelengthData;

    public:
        AngleData() = delete;

        AngleData(const SampleData &dataAll);

        AngleData(const AngleData &other);

        AngleData &operator=(const AngleData &other);

        void reset();

        void reset(bool resetGas);

        void overlay(const AngleData &other);

        void addValue(NephelometerSpancheckController::Parameter param,
                      double value,
                      double time,
                      bool toGas);

        bool stable(bool isGas, bool requireRevolutions, bool requireCounts) const;
    };

    class WavelengthData {
        NephelometerSpancheckController *parent;

        std::size_t wavelengthIndex;
        double wavelength;
        QString code;
        std::unordered_map<double, AngleData> angles;

        double deadTimeCorrection;

        double correctCounts(double counts, double revolutions, double scale) const;

        Data::Variant::Write calculate(double angle, const AngleData &data) const;

    public:
        WavelengthData() = delete;

        WavelengthData(std::size_t idx, NephelometerSpancheckController *p);

        WavelengthData(const WavelengthData &);

        WavelengthData &operator=(const WavelengthData &);

        WavelengthData(WavelengthData &&);

        WavelengthData &operator=(WavelengthData &&);

        inline QString getCode() const
        { return code; }

        void reset();

        void reset(bool resetGas);

        void addValue(NephelometerSpancheckController::Parameter param,
                      double value,
                      double time,
                      bool toGas,
                      double angle);

        void overlay(const std::unordered_map<double, AngleData> &incomingData);

        void setWavelength(double wavelength, const QString &code);

        inline void setDeadTimeCorrection(double k)
        { deadTimeCorrection = k; }

        bool stable(bool isGas, bool requireRevolutions, bool requireCounts) const;

        void calculate(std::unordered_map<double, Data::Variant::Write> &outputAngles,
                       Data::Variant::Write &outputCommon) const;
    };

    friend class WavelengthData;

    std::vector<WavelengthData> data;
    AngleData dataAll;
    std::unordered_map<double, AngleData> dataAllWavelengths;
    std::vector<AngleData> dataAllAngles;

    double chopperScaleDark;
    double chopperScaleMeasure;
    double chopperScaleReference;

    Data::Variant::Root globalResults;

    void reset();

    void mergePendingCommands(const std::unordered_map<QString, Data::Variant::Root> &overlay);

    void issueCommands(const std::unordered_map<QString, Data::Variant::Read> &commands);

    bool dataStable(bool isGas) const;

    Data::Variant::Root buildRealtimeState() const;

    void completed();

    Data::Variant::Read spancheckPhaseCommonMetadata() const;

public:
    NephelometerSpancheckController(const Data::ValueSegment::Transfer &config = Data::ValueSegment::Transfer(),
                                    const Data::Variant::Read &defaultConfig = Data::Variant::Read::empty(),
                                    Flags f = Default);

    ~NephelometerSpancheckController();

    /**
     * Set the interface the controller is using.
     * 
     * @param i     the spancheck control interface
     */
    void setInterface(Interface *i);

    /**
     * Set the wavelength for the given index.
     * 
     * @param index         the wavelength index
     * @param wavelength    the wavelength in nm
     * @param code          the wavelength code (e.x. "R")
     */
    void setWavelength(int index, double wavelength, const QString &code);

    /**
     * Set the dead time correction factor.
     * 
     * @param index         the wavelength index
     * @param k             the correction factor
     */
    void setDeadTimeCorrection(int index, double k);

    /**
     * Set the chopper scale factors.
     * 
     * @param scaleDark         the dark scale factor ((360*22.994)/60 for the TSI 3563)
     * @param scaleMeasure      the measurement scale factor ((360*22.994)/140 for the TSI 3563)
     * @param scaleReference    the reference scale factor ((360*22.994)/40 for the TSI 3563)
     */
    inline void setChopper(double scaleDark, double scaleMeasure, double scaleReference)
    {
        this->chopperScaleDark = scaleDark;
        this->chopperScaleMeasure = scaleMeasure;
        this->chopperScaleReference = scaleReference;
    }

    /**
     * Terminate the spancheck, if it's currently active.  This does not
     * call the normal abort handler and is intended for cases like 
     * communications lost.
     */
    void terminate();

    /**
     * Add the commands the spancheck controller can accept to the
     * given target.
     * 
     * @param target    the command description target
     */
    void addCommands(Data::Variant::Write &target) const;

    inline void addCommands(Data::Variant::Write &&target) const
    { return addCommands(target); }

    /**
     * Get a description of all commands the controller can accept.
     * 
     * @return          the command description
     */
    inline Data::Variant::Root getCommands() const
    {
        Data::Variant::Root result;
        addCommands(result.write());
        return result;
    }

    /**
     * Handling an incoming command.
     * 
     * @param command   the command data
     */
    void command(const Data::Variant::Read &command);

    /**
     * Advance the spancheck controller to the given time.  The interface
     * is only accessed during this function.
     * 
     * @param time              the current time
     * @param realtimeEgress    the egress for realtime state
     */
    void advance(double time, Data::StreamSink *realtimeEgress = NULL);

    /**
     * Add an incoming value to the spancheck.  This is intended to be used
     * during acquisition to inform the spancheck controller of a value
     * update.  The current time is the time of the last advance.
     * 
     * @param param         the parameter being updated
     * @param value         the updated value
     * @param wavelength    the wavelength index or -1 for all wavelengths
     * @param angle         the measurement angle or undefined for all angles
     */
    void update(Parameter param, double value, int wavelength = -1, double angle = FP::undefined());

    /** @see update(Parameter, double, int, double) for angle = 0.0 */
    inline void updateTotal(Parameter param, double value, int wavelength = -1)
    { update(param, value, wavelength, 0.0); }

    /** @see update(Parameter, double, int, double) for angle = 90.0 */
    inline void updateBack(Parameter param, double value, int wavelength = -1)
    { update(param, value, wavelength, 90.0); }

    /**
     * Get the metadata for a given result type.
     * 
     * @param type          the result type
     * @param processing    the default processing metadata
     * @return              the metadata
     */
    Data::Variant::Root metadata(ResultType type,
                                 const Data::Variant::Read &processing = Data::Variant::Read::empty()) const;

    /**
     * Get a specific type of result from the spancheck after completion.
     * 
     * @param global    the global result data
     * @param type      the result type
     * @param code      the wavelength code to query
     * @return          the spancheck result
     */
    static Data::Variant::Root results(const Data::Variant::Read &global,
                                       ResultType type,
                                       const QString &code = QString());

    /**
     * Get a specific type of result from the spancheck after completion.
     * 
     * @param type      the result type
     * @param code      the wavelength code to query
     * @return          the spancheck result
     */
    Data::Variant::Root results(ResultType type, const QString &code = QString()) const;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(NephelometerSpancheckController::Flags)

}
}

#endif
