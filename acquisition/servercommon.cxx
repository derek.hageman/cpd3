/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTcpSocket>
#include <QSslSocket>
#include <QLocalSocket>
#include <QTcpServer>
#include <QLocalServer>
#include <QLoggingCategory>

#include "acquisition/servercommon.hxx"
#include "algorithms/cryptography.hxx"


Q_LOGGING_CATEGORY(log_acquisition_servercommon, "cpd3.acquisition.servercommon", QtWarningMsg)


using namespace CPD3::Data;
using namespace CPD3::Algorithms;


namespace CPD3 {
namespace Acquisition {

/** @file acquisition/servercommon.hxx
 * Common routines for network and local servers.
 */

AcquisitionLocalSocket::AcquisitionLocalSocket(QObject *parent) : QLocalSocket(parent)
{ }

bool AcquisitionLocalSocket::atEnd() const
{
    QLocalSocket::LocalSocketState s = state();
    return s == QLocalSocket::UnconnectedState || s == QLocalSocket::ClosingState;
}

AcquisitionTCPSocket::AcquisitionTCPSocket(QObject *parent) : QTcpSocket(parent)
{ }

bool AcquisitionTCPSocket::atEnd() const
{
    QAbstractSocket::SocketState s = state();
    return s == QAbstractSocket::UnconnectedState || s == QAbstractSocket::ClosingState;
}

AcquisitionSSLSocket::AcquisitionSSLSocket(QObject *parent) : QSslSocket(parent)
{ }

bool AcquisitionSSLSocket::atEnd() const
{
    QAbstractSocket::SocketState s = state();
    return s == QAbstractSocket::UnconnectedState || s == QAbstractSocket::ClosingState;
}

bool AcquisitionSSLSocket::checkCertificate(const Variant::Read &auth,
                                            const QSslCertificate &cert,
                                            bool defaultResult)
{
    if (!auth.exists())
        return defaultResult;

    switch (auth.getType()) {
    case Variant::Type::Hash: {
        QString key(Cryptography::sha512(cert).toHex().toLower());
        QSslCertificate check(Cryptography::getCertificate(auth.hash(key), false));
        if (check.isNull())
            return false;
        return check == cert;
    }

    case Variant::Type::Array:
        for (auto raw : auth.toArray()) {
            QSslCertificate check(Cryptography::getCertificate(raw, false));
            if (check.isNull())
                continue;
            if (check == cert)
                return true;
        }
        return false;

    default: {
        QSslCertificate check(Cryptography::getCertificate(auth, false));
        if (check.isNull())
            return false;
        return check == cert;
    }
    }

    Q_ASSERT(false);
    return false;
}

bool AcquisitionSSLSocket::peerAuthorized(const Variant::Read &auth, bool defaultResult) const
{ return checkCertificate(auth, peerCertificate(), defaultResult); }


QPair<QHostAddress, int> AcquisitionAddressFilter::parseAddress(const Variant::Read &addr)
{
    QString str(addr.toQString());
    if (!str.contains('/')) {
        QHostAddress result(str);
#ifndef QT_NO_IPV6
        if (result.protocol() == QAbstractSocket::IPv6Protocol)
            return QPair<QHostAddress, int>(result, 128);
#endif
#ifdef QT_NO_IPV4
        return QPair<QHostAddress, int>(result, 128);
#else
        return QPair<QHostAddress, int>(result, 32);
#endif
    }
    return QHostAddress::parseSubnet(str);
}

AcquisitionAddressFilter::AcquisitionAddressFilter(const Variant::Read &configuration)
        : acceptAddresses(), rejectAddresses()
{
    if (configuration.hash("Accept").exists()) {
        if (configuration.hash("Accept").getType() == Variant::Type::Array) {
            for (auto add : configuration.hash("Accept").toArray()) {
                acceptAddresses.push_back(parseAddress(add));
            }
        } else {
            acceptAddresses.push_back(parseAddress(configuration.hash("Accept")));
        }
    }

    if (configuration.hash("Reject").exists()) {
        if (configuration.hash("Reject").getType() == Variant::Type::Array) {
            for (auto add : configuration.hash("Reject").toArray()) {
                rejectAddresses.push_back(parseAddress(add));
            }
        } else {
            rejectAddresses.push_back(parseAddress(configuration.hash("Reject")));
        }
    }
}

bool AcquisitionAddressFilter::shouldAccept(const QHostAddress &peerAddress) const
{
    if (!rejectAddresses.empty()) {
        for (const auto &check : rejectAddresses) {
            if (peerAddress.isInSubnet(check))
                return false;
        }
    }

    if (!acceptAddresses.empty()) {
        for (const auto &check : acceptAddresses) {
            if (peerAddress.isInSubnet(check))
                return true;
        }
        return false;
    }

    return true;
}

AcquisitionUDPSocket::AcquisitionUDPSocket(const Variant::Read &configuration, QObject *parent)
        : QUdpSocket(parent),
          filter(configuration),
          targetAddress(),
          targetPort(0),
          fragmentSize(configuration.hash("FragmentSize").toInt64()),
          readDelimiter(configuration.hash("ReadDelimiter").toBinary()),
          readOverflow()
{
    QString name(configuration.hash("ServerAddress").toQString());
    if (!name.isEmpty()) {
        targetAddress = QHostAddress(name);
        qint64 p = configuration.hash("ServerPort").toInt64();
        if (INTEGER::defined(p) && p > 0)
            targetPort = (quint16) p;
    }
    open(QIODevice::ReadWrite);
}

bool AcquisitionUDPSocket::atEnd() const
{
    return false;
}

qint64 AcquisitionUDPSocket::bytesAvailable() const
{
    qint64 n = pendingDatagramSize();
    if (n < 0)
        n = 0;
    n += readOverflow.size();
    return n;
}

qint64 AcquisitionUDPSocket::writeData(const char *data, qint64 maxSize)
{
    if (targetAddress.isNull() || targetPort <= 0)
        return maxSize;

    qint64 written = 0;
    while (maxSize > 0) {
        qint64 toWrite = maxSize;
        if (INTEGER::defined(fragmentSize) && toWrite > fragmentSize)
            toWrite = fragmentSize;
        qint64 n = writeDatagram(data, toWrite, targetAddress, targetPort);
        if (n < 0) {
            if (written > 0)
                return written;
            return -1;
        }
        written += n;
        data += n;
        maxSize -= n;
    }
    return written;
}

qint64 AcquisitionUDPSocket::readData(char *data, qint64 maxSize)
{
    if (maxSize <= 0)
        return 0;
    qint64 totalRead = 0;
    if (readOverflow.size() > 0) {
        qint64 n = qMin((qint64) readOverflow.size(), maxSize);
        ::memcpy(data, readOverflow.constData(), (size_t) n);
        int remaining = readOverflow.size() - (int) n;
        if (remaining <= 0) {
            readOverflow.clear();
        } else {
            ::memmove(readOverflow.data(), readOverflow.data() + n, (size_t) remaining);
            readOverflow.resize(remaining);
        }
        maxSize -= n;
        totalRead += n;
        if (maxSize <= 0)
            return totalRead;
        data += n;
    }

    for (; ;) {
        qint64 pending = pendingDatagramSize();
        if (pending < 0)
            break;

        if (pending + readDelimiter.size() <= maxSize) {
            QHostAddress addr;
            qint64 n = readDatagram(data, maxSize, &addr);
            if (n < 0 || !filter.shouldAccept(addr))
                continue;

            maxSize -= n;
            totalRead += n;
            data += n;

            n = readDelimiter.size();
            if (n > 0) {
                ::memcpy(data, readDelimiter.constData(), (size_t) n);
                maxSize -= n;
                totalRead += n;
                data += n;
            }

            if (maxSize <= 0)
                return totalRead;
        }

        int offset = readOverflow.size();
        readOverflow.resize(offset + (int) pending);

        QHostAddress addr;
        qint64 n = readDatagram(readOverflow.data() + offset, pending, &addr);
        if (n < 0 || !filter.shouldAccept(addr)) {
            readOverflow.resize(offset);
            continue;
        }

        readOverflow.resize(offset + (int) n);

        n = readDelimiter.size();
        if (n > 0) {
            offset = readOverflow.size();
            readOverflow.resize(offset + (int) n);
            ::memcpy(readOverflow.data() + offset, readDelimiter.constData(), (size_t) n);
        }

        return readData(data, maxSize);
    }

    return totalRead;
}

AcquisitionTCPServer::AcquisitionTCPServer(const Variant::Read &configuration, QObject *parent)
        : QTcpServer(parent), filter(configuration), pending()
{ }

AcquisitionTCPServer::~AcquisitionTCPServer()
{
    close();
    for (QList<AcquisitionTCPSocket *>::iterator del = pending.begin(), endDel = pending.end();
            del != endDel;
            ++del) {
        (*del)->close();
        delete *del;
    }
}

bool AcquisitionTCPServer::hasPendingConnections() const
{ return !pending.isEmpty(); }

QTcpSocket *AcquisitionTCPServer::nextPendingConnection()
{
    while (!pending.isEmpty()) {
        QTcpSocket *socket = pending.takeFirst();

        if (!socket->waitForConnected(5000)) {
            socket->close();
            delete socket;
            continue;
        }

        return socket;
    }
    return NULL;
}

bool AcquisitionTCPServer::shouldAccept(QTcpSocket *socket) const
{
    return filter.shouldAccept(socket->peerAddress());
}

void AcquisitionTCPServer::incomingConnection(qintptr socketDescriptor)
{
    AcquisitionTCPSocket *socket = new AcquisitionTCPSocket(this);
    socket->setSocketDescriptor(socketDescriptor, QAbstractSocket::ConnectedState,
                                QIODevice::ReadWrite);
    if (!shouldAccept(socket)) {
        socket->close();
        delete socket;
        return;
    }
    socket->setReadBufferSize(16777216);

    pending.append(socket);
    emit newConnection();
}

AcquisitionSSLServer::AcquisitionSSLServer(const Variant::Read &conf, QObject *parent)
        : AcquisitionTCPServer(conf, parent), configuration(conf)
{ }

AcquisitionSSLServer::~AcquisitionSSLServer()
{
    close();
    for (QList<AcquisitionSSLSocket *>::iterator del = pending.begin(), endDel = pending.end();
            del != endDel;
            ++del) {
        (*del)->close();
        delete *del;
    }
}

bool AcquisitionSSLServer::hasPendingConnections() const
{ return !pending.isEmpty(); }

QTcpSocket *AcquisitionSSLServer::nextPendingConnection()
{
    while (!pending.isEmpty()) {
        AcquisitionSSLSocket *socket = pending.takeFirst();

        if (!socket->waitForEncrypted(5000)) {
            socket->close();
            delete socket;
            continue;
        }

        if (!socket->peerAuthorized(configuration.read().hash("SSL").hash("Authorized"))) {
            socket->close();
            delete socket;
            continue;
        }

        return socket;
    }
    return NULL;
}


void AcquisitionSSLServer::incomingConnection(qintptr socketDescriptor)
{
    AcquisitionSSLSocket *socket = new AcquisitionSSLSocket(this);
    if (!shouldAccept(socket)) {
        socket->setSocketDescriptor(socketDescriptor, QAbstractSocket::ConnectedState,
                                    QIODevice::ReadWrite);
        socket->close();
        delete socket;
        return;
    }

    if (!Cryptography::setupSocket(socket, configuration.read().hash("SSL"))) {
        qCDebug(log_acquisition_servercommon) << "Failed to setup SSL on socket";
    }

    if (!socket->setSocketDescriptor(socketDescriptor, QAbstractSocket::ConnectedState,
                                     QIODevice::ReadWrite)) {
        socket->close();
        delete socket;
        return;
    }
    socket->setReadBufferSize(16777216);

    socket->startServerEncryption();

    pending.append(socket);
    emit newConnection();
}


AcquisitionLocalServer::AcquisitionLocalServer(QObject *parent) : QLocalServer(parent), pending()
{
    /* Unix domain sockets obey umask and permissions */
#if !defined(Q_OS_UNIX)
    setSocketOptions(QLocalServer::UserAccessOption);
#endif
}

AcquisitionLocalServer::~AcquisitionLocalServer()
{
    close();
    for (QList<AcquisitionLocalSocket *>::iterator del = pending.begin(), endDel = pending.end();
            del != endDel;
            ++del) {
        (*del)->close();
        delete *del;
    }
}

bool AcquisitionLocalServer::hasPendingConnections() const
{ return !pending.isEmpty(); }

QLocalSocket *AcquisitionLocalServer::nextPendingConnection()
{
    while (!pending.isEmpty()) {
        AcquisitionLocalSocket *socket = pending.takeFirst();

        if (!socket->waitForConnected(5000)) {
            socket->close();
            delete socket;
            continue;
        }

        return socket;
    }
    return NULL;
}

void AcquisitionLocalServer::incomingConnection(quintptr socketDescriptor)
{
    AcquisitionLocalSocket *socket = new AcquisitionLocalSocket(this);
    socket->setSocketDescriptor(socketDescriptor, QLocalSocket::ConnectedState,
                                QIODevice::ReadWrite | QIODevice::Unbuffered);
    socket->setReadBufferSize(16777216);

    pending.append(socket);
    emit newConnection();
}


}
}
