/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITIONLINEINGRESS_H
#define CPD3ACQUISITIONLINEINGRESS_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "acquisition/acquisition.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/stream.hxx"
#include "core/component.hxx"
#include "core/waitutils.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * A base class that provides an instrument that is both controlled by
 * and issues data that is "framed".  For example, a line driven instrument
 * is framed by nothing at the start and a newline at the end.  Realtime advance
 * is called after all frames have been processed.
 * <br>
 * The default implementation provides framing for a single line driven
 * instrument.
 */
class CPD3ACQUISITION_EXPORT LineIngressWrapper : public Data::ExternalSourceProcessor {
    QByteArray processBuffer;
    Data::SequenceValue::Transfer pendingValues;
    Data::SequenceValue::Transfer processValues;
    enum {
        Running, SendEnd, AwaitingInterfaceEnd, Ended,
    } endState;
    bool endCheckPending;

    Data::SequenceName::Component suffix;
    Data::SequenceName::Component station;

    struct OutputTransform {
        Data::SequenceName unit;
        bool metadataHandling;
    };
    Data::SequenceName::Map<OutputTransform> outputTransform;

    class OutputFilter : public Data::StreamSink {
        LineIngressWrapper *wrapper;
    public:
        OutputFilter(LineIngressWrapper *setWrapper);

        virtual ~OutputFilter();

        void incomingData(const Data::SequenceValue::Transfer &v) override;

        void incomingData(Data::SequenceValue::Transfer &&v) override;

        void incomingData(const Data::SequenceValue &v) override;

        void incomingData(Data::SequenceValue &&v) override;

        void endData() override;
    };

    friend class OutputFilter;

    std::unique_ptr<OutputFilter> filter;

    void runValueProcessing(Data::SequenceValue::Transfer &&values);

    void interfaceCompleted();

protected:
    LineIngressWrapper(std::unique_ptr<AcquisitionInterface> &&setInterface,
                       const ComponentOptions &options);

    std::unique_ptr<AcquisitionInterface> interface;

    virtual void processData(const QByteArray &data) = 0;

    virtual void signalEnd() = 0;

    bool prepare() override;

    bool process() override;

    bool begin() override;

    void finalize() override;

    void finish() override;

public:
    virtual ~LineIngressWrapper();

    static void addOptions(ComponentOptions &options, bool implicitTime = false);

    static LineIngressWrapper *create(std::unique_ptr<AcquisitionInterface> &&interface,
                                      const ComponentOptions &options,
                                      bool implicitTime = false);

    static QList<ComponentExample> standardExamples(bool implicitTime = false);

    using Data::ExternalSourceProcessor::incomingData;
};

}
}

#endif
