/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "acquisition/commandmanager.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Acquisition {

/** @file acquisition/commandmanager.hxx
 * A system to manager commands that can be issued to acquisition components.
 */

bool CommandManager::CommandData::BreakDown::TriggerData::triggered() const
{
    auto current = input.lookup(parent->latest).getPath(path);

    switch (type) {
    case Flags: {
        const auto &currentFlags = current.toFlags();
        for (const auto &check : triggerFlags) {
            if (currentFlags.count(check) != 0)
                return true;
        }
        return false;
    }
    case Equal:
        return current == triggerExact;
    case NotEqual:
        return current != triggerExact;
    case LessThan: {
        double currentValue = Variant::Composite::toNumber(current);
        if (!FP::defined(currentValue) || !FP::defined(triggerValue))
            return false;
        return currentValue < triggerValue;
    }
    case LessThanOrEqual: {
        double currentValue = Variant::Composite::toNumber(current);
        if (!FP::defined(currentValue) || !FP::defined(triggerValue))
            return false;
        return currentValue <= triggerValue;
    }
    case GreaterThan: {
        double currentValue = Variant::Composite::toNumber(current);
        if (!FP::defined(currentValue) || !FP::defined(triggerValue))
            return false;
        return currentValue > triggerValue;
    }
    case GreaterThanOrEqual: {
        double currentValue = Variant::Composite::toNumber(current);
        if (!FP::defined(currentValue) || !FP::defined(triggerValue))
            return false;
        return currentValue >= triggerValue;
    }
    case Defined: {
        return current.exists();
    }
    }

    return false;
}

static Data::SequenceMatch::OrderedLookup constructInput(const Variant::Read &data)
{
    if (!data.hash("Variable").exists())
        return Data::SequenceMatch::OrderedLookup();
    return Data::SequenceMatch::OrderedLookup(data, {}, {"rt_instant"}, {});
}

CommandManager::CommandData::BreakDown::TriggerData::TriggerData(CommandManager *p,
                                                                 const Variant::Read &data)
        : parent(p),
          input(constructInput(data)),
          path(data.hash("Path").toString()),
          type(Flags),
          triggerFlags(data.hash("Flags").toFlags()),
          triggerValue(Variant::Composite::toNumber(data.hash("Value"))),
          triggerExact()
{
    const auto &t = data.hash("Type").toString();
    if (Util::equal_insensitive(t, "flags")) {
        type = Flags;
    } else if (Util::equal_insensitive(t, "equal")) {
        type = Equal;
        triggerExact.write().set(data.hash("Value"));
    } else if (Util::equal_insensitive(t, "notequal")) {
        type = NotEqual;
        triggerExact.write().set(data.hash("Value"));
    } else if (Util::equal_insensitive(t, "lessthan")) {
        type = LessThan;
    } else if (Util::equal_insensitive(t, "lessthanequal", "lessthanorequal")) {
        type = LessThanOrEqual;
    } else if (Util::equal_insensitive(t, "greaterthan")) {
        type = GreaterThan;
    } else if (Util::equal_insensitive(t, "greaterthanequal", "greaterthanorequal")) {
        type = GreaterThanOrEqual;
    } else if (Util::equal_insensitive(t, "defined", "exists")) {
        type = Defined;
    }
}

CommandManager::CommandData::BreakDown::ParameterData::ParameterData(CommandManager *p,
                                                                     const Variant::Read &d)
        : parent(p),
          input(constructInput(d)),
          path(d.hash("Path").toString()),
          fallbackValue(d.hash("Default"))
{ }

Variant::Read CommandManager::CommandData::BreakDown::ParameterData::getValue() const
{
    auto v = input.lookup(parent->latest).getPath(path);
    if (v.exists())
        return v;
    return fallbackValue.read();
}

bool CommandManager::CommandData::BreakDown::registerInput(const SequenceName &unit)
{
    bool selected = false;
    for (auto &check : include) {
        if (check.input.registerInput(unit))
            selected = true;
    }
    for (auto &check : exclude) {
        if (check.input.registerInput(unit))
            selected = true;
    }
    for (auto &check : parameters) {
        if (check.second.input.registerInput(unit))
            selected = true;
    }
    return selected;
}

bool CommandManager::CommandData::BreakDown::enabled() const
{
    for (const auto &check : exclude) {
        if (check.triggered())
            return false;
    }

    if (!include.empty()) {
        bool hit = false;
        for (const auto &check : include) {
            if (check.triggered()) {
                hit = true;
                break;
            }
        }
        if (!hit)
            return false;
    }

    return true;
}

void CommandManager::CommandData::updateCurrent()
{
    if (!updated)
        return;
    updated = false;

    currentCommands.write().setEmpty();
    currentAggregateCommands.clear();
    if (breakdownData.empty())
        return;

    for (auto &command : breakdownData) {
        if (!command.second.enabled())
            continue;

        auto data = command.second.base.write();

        for (const auto &parameter : command.second.parameters) {
            data.hash("Parameters").hash(parameter.first)
                .hash("Value").set(parameter.second.getValue());
        }

        if (data.hash("Aggregate").toBool()) {
            data.hash("Command").setString(command.first);
            Util::insert_or_assign(currentAggregateCommands, command.first, Variant::Root(data));
        } else {
            currentCommands.write().hash(command.first).set(data);
        }
    }
}

void CommandManager::CommandData::updateCommandData(const Variant::Read &data)
{
    bool wasValid = !breakdownData.empty();
    updated = true;

    breakdownData.clear();
    for (auto command : data.toHash()) {
        if (command.first.empty())
            continue;
        auto &target = breakdownData.emplace(command.first, BreakDown()).first->second;

        target.base.write().set(command.second);

        for (auto add : command.second.hash("Include").toArray()) {
            target.include.push_back(BreakDown::TriggerData(parent, add));
        }

        for (auto add : command.second.hash("Exclude").toArray()) {
            target.exclude.push_back(BreakDown::TriggerData(parent, add));
        }

        for (auto add : command.second.hash("Parameters").toHash()) {
            if (add.first.empty())
                continue;
            target.parameters.emplace(add.first, BreakDown::ParameterData(parent, add.second));
            target.base.write().hash("Parameters").hash(add.first).hash("Value").remove();
        }
    }

    for (auto &vd : parent->values) {
        if (wasValid)
            vd.second->removeCommandData(this);
        if (registerInput(vd.first))
            vd.second->commandInputs.push_back(this);
    }
}

bool CommandManager::CommandData::registerInput(const SequenceName &unit)
{
    bool selected = false;
    for (auto &command : breakdownData) {
        if (command.second.registerInput(unit))
            selected = true;
    }
    if (!selected)
        return false;

    updated = true;
    return true;
}

const Variant::Root &CommandManager::CommandData::getCurrentCommands()
{
    updateCurrent();
    return currentCommands;
}

const std::unordered_map<std::string,
                         Variant::Root> &CommandManager::CommandData::getAggregateCommands()
{
    updateCurrent();
    return currentAggregateCommands;
}

CommandManager::CommandData::CommandData(CommandManager *p) : parent(p),
                                                              updated(false),
                                                              currentCommands(),
                                                              currentAggregateCommands()
{ }


void CommandManager::ValueData::removeCommandData(CommandData *data)
{
    if (commandInputs.empty())
        return;
    for (auto del = commandInputs.begin(), endDel = commandInputs.end(); del != endDel; ++del) {
        if (*del != data)
            continue;
        commandInputs.erase(del);
        break;
    }
}

CommandManager::CommandManager() = default;

CommandManager::~CommandManager() = default;

void CommandManager::reset(bool purgeManual)
{
    commands.clear();
    if (purgeManual) {
        manual.clear();
    }
    values.clear();
    latest = SequenceSegment();
}

void CommandManager::incomingData(const SequenceValue &value)
{
    latest.setValue(value.getUnit(), value.root());

    auto update = values.find(value.getUnit());
    ValueData *vd;
    if (update == values.end()) {
        vd = new ValueData;
        values.emplace(value.getUnit(), std::unique_ptr<ValueData>(vd));

        for (const auto &target : commands) {
            if (target.second->registerInput(value.getUnit())) {
                vd->commandInputs.push_back(target.second.get());
            }
        }
        for (const auto &target : manual) {
            if (target.second->registerInput(value.getUnit())) {
                vd->commandInputs.push_back(target.second.get());
            }
        }
    } else {
        vd = update->second.get();
    }

    for (auto cd : vd->commandInputs) {
        cd->updated = true;
    }
}

void CommandManager::incomingData(const SequenceValue::Transfer &values)
{
    for (const auto &add : values) {
        incomingData(add);
    }
}

void CommandManager::updateCommands(const QString &target, const Variant::Read &data)
{
    auto update = commands.find(target);
    if (update == commands.end()) {
        update =
                commands.emplace(target, std::unique_ptr<CommandData>(new CommandData(this))).first;
    }
    update->second->updateCommandData(data);
}

Data::Variant::Root CommandManager::getCommands(const QString &target)
{
    auto check = commands.find(target);
    if (check == commands.end())
        return Data::Variant::Root();
    return check->second->getCurrentCommands();
}

void CommandManager::updateManual(const QString &target, const Variant::Read &data)
{
    auto update = manual.find(target);
    if (update == manual.end()) {
        update = manual.emplace(target, std::unique_ptr<CommandData>(new CommandData(this))).first;
    }
    update->second->updateCommandData(data);
}

Data::Variant::Root CommandManager::getManual(const QString &target)
{
    auto check = manual.find(target);
    if (check == manual.end())
        return Data::Variant::Root();
    return check->second->getCurrentCommands();
}

std::vector<Variant::Root> CommandManager::getAggregateCommands()
{
    std::unordered_map<std::string, Variant::Root> combined;
    for (const auto &add : commands) {
        for (const auto &merge : add.second->getAggregateCommands()) {
            auto target = combined.find(merge.first);
            if (target == combined.end()) {
                target = combined.emplace(merge.first, merge.second).first;
            }
            target->second.write().hash("Targets").toArray().after_back().setString(add.first);
        }
    }
    for (const auto &add : manual) {
        for (const auto &merge : add.second->getAggregateCommands()) {
            auto target = combined.find(merge.first);
            if (target == combined.end()) {
                target = combined.emplace(merge.first, merge.second).first;
            }
            target->second.write().hash("Targets").toArray().after_back().setString(add.first);
        }
    }
    std::vector<Variant::Root> result;
    for (auto &add : combined) {
        result.emplace_back(std::move(add.second));
    }
    return result;
}

}
}
