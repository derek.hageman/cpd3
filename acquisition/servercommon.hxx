/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITIONSERVERCOMMON_H
#define CPD3ACQUISITIONSERVERCOMMON_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QTcpServer>
#include <QLocalServer>
#include <QPair>
#include <QHostAddress>
#include <QTcpSocket>
#include <QSslSocket>
#include <QLocalSocket>
#include <QUdpSocket>

#include "acquisition/acquisition.hxx"
#include "datacore/variant/root.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * An overridden local socket that provides a more sensible atEnd().
 */
class CPD3ACQUISITION_EXPORT AcquisitionLocalSocket : public QLocalSocket {
Q_OBJECT

public:
    AcquisitionLocalSocket(QObject *parent = 0);

    virtual bool atEnd() const;
};

/**
 * An overridden TCP socket that provides a more sensible atEnd().
 */
class CPD3ACQUISITION_EXPORT AcquisitionTCPSocket : public QTcpSocket {
Q_OBJECT

public:
    AcquisitionTCPSocket(QObject *parent = 0);

    virtual bool atEnd() const;
};

/**
 * An overridden SSL socket that provides a more sensible atEnd().
 */
class CPD3ACQUISITION_EXPORT AcquisitionSSLSocket : public QSslSocket {
Q_OBJECT

public:
    AcquisitionSSLSocket(QObject *parent = 0);

    virtual bool atEnd() const;

    /**
     * Check if a certificate matches an authorization specification.
     * 
     * @param auth          the authorization specification
     * @param cert          the certificate
     * @param defaultResult the default authorization if the specification is empty
     * @return              true if the certificate is authorized
     */
    static bool checkCertificate(const Data::Variant::Read &auth,
                                 const QSslCertificate &cert,
                                 bool defaultResult = true);

    /**
     * Check if the current pper is authorized.
     * 
     * @param auth          the authorization specification
     * @param defaultResult the default authorization if the specification is empty
     * @return              true if the peer is authorized
     */
    bool peerAuthorized(const Data::Variant::Read &auth, bool defaultResult = true) const;
};

/**
 * A simple filter for address accept/reject.
 */
class CPD3ACQUISITION_EXPORT AcquisitionAddressFilter {
    std::vector<QPair<QHostAddress, int> > acceptAddresses;
    std::vector<QPair<QHostAddress, int> > rejectAddresses;

    static QPair<QHostAddress, int> parseAddress(const Data::Variant::Read &addr);

public:
    AcquisitionAddressFilter(const Data::Variant::Read &configuration = Data::Variant::Read::empty());

    bool shouldAccept(const QHostAddress &peerAddress) const;
};

/**
 * An overridden UDP socket that provides address filtering and a more sensible atEnd().
 */
class CPD3ACQUISITION_EXPORT AcquisitionUDPSocket : public QUdpSocket {
Q_OBJECT

    AcquisitionAddressFilter filter;
    QHostAddress targetAddress;
    quint16 targetPort;
    qint64 fragmentSize;
    QByteArray readDelimiter;
    QByteArray readOverflow;
public:
    AcquisitionUDPSocket(const Data::Variant::Read &configuration = Data::Variant::Read::empty(),
                         QObject *parent = 0);

    virtual bool atEnd() const;

    virtual qint64 bytesAvailable() const;

protected:
    virtual qint64 writeData(const char *data, qint64 maxSize);

    virtual qint64 readData(char *data, qint64 maxSize);
};

/**
 * A simple implementation of a TCP server with address filtering.
 */
class CPD3ACQUISITION_EXPORT AcquisitionTCPServer : public QTcpServer {
Q_OBJECT

    AcquisitionAddressFilter filter;
    QList<AcquisitionTCPSocket *> pending;

public:
    /**
     * Create a new TCP server.  The configuration specifies the
     * accept and reject lists, if any.
     * 
     * @param configuration     the socket configuration
     * @param parent            the parent object
     */
    AcquisitionTCPServer(const Data::Variant::Read &configuration = Data::Variant::Read::empty(),
                         QObject *parent = 0);

    virtual ~AcquisitionTCPServer();

    virtual bool hasPendingConnections() const;

    virtual QTcpSocket *nextPendingConnection();

protected:
    bool shouldAccept(QTcpSocket *socket) const;

    virtual void incomingConnection(qintptr socketDescriptor);
};

/**
 * A simple implementation of an SSL TCP server with address filtering and
 * certificate authorization lists.
 */
class CPD3ACQUISITION_EXPORT AcquisitionSSLServer : public AcquisitionTCPServer {
Q_OBJECT

    Data::Variant::Root configuration;
    QList<AcquisitionSSLSocket *> pending;
public:
    /**
     * Create a new SSL server.  The configuration specifies the authorized
     * addresses and certificates, if any.
     * 
     * @param conf      the configuration
     * @param parent    the parent object
     */
    AcquisitionSSLServer(const Data::Variant::Read &conf = Data::Variant::Read::empty(),
                         QObject *parent = 0);

    virtual ~AcquisitionSSLServer();

    virtual bool hasPendingConnections() const;

    virtual QTcpSocket *nextPendingConnection();

protected:

    virtual void incomingConnection(qintptr socketDescriptor);
};

/**
 * A simple implementation of a local socket server that wraps the
 * local sockets with the modified atEnd() modified version.
 */
class CPD3ACQUISITION_EXPORT AcquisitionLocalServer : public QLocalServer {
Q_OBJECT

    QList<AcquisitionLocalSocket *> pending;
public:
    AcquisitionLocalServer(QObject *parent = 0);

    virtual ~AcquisitionLocalServer();

    virtual bool hasPendingConnections() const;

    virtual QLocalSocket *nextPendingConnection();

protected:
    virtual void incomingConnection(quintptr socketDescriptor);
};

}
}

#endif
