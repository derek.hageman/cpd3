/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>
#include <QtEndian>
#include <datacore/externalsink.hxx>
#include <datacore/stream.hxx>

#include "acquisition/acquisitionnetwork.hxx"
#include "io/drivers/localsocket.hxx"
#include "io/drivers/tcp.hxx"
#include "algorithms/cryptography.hxx"
#include "core/timeutils.hxx"
#include "core/environment.hxx"

#ifdef Q_OS_UNIX

#include "io/drivers/unixsocket.hxx"

#endif

using namespace CPD3::Data;

Q_LOGGING_CATEGORY(log_acquisition_network, "cpd3.acquisition.network", QtWarningMsg)

namespace CPD3 {
namespace Acquisition {

namespace {

enum class PacketToServer : std::uint8_t {
    /* No data */
    Hello = 0,

    /* No data */
    Ping,

    /* No data */
    RealtimeResend,

    /*
     * uint32_t size;
     * uint8_t data[size];
     */
    StartArchiveRead,

    /* No data */
    AbortArchiveRead,

    /*
     * uint32_t size;
     * uint8_t data[size];
     */
    MessageLogEvent,

    /*
     * uint16_t name_length;
     * char name[name_length];
     * uint32_t data_length;
     * uint8_t data[data_length];
     */
    Command,

    /*
     * double duration;
     */
    SystemFlush,

    /*
     * uint8_t unit;
     * int32_t count;
     * uint8_t align;
     */
    SetAveragingTime,

    /* No data */
    DataFlush,

    /*
     * uint16_t size;
     * char flag[size];
     */
    BypassFlagSet,

    /*
     * uint16_t size;
     * char flag[size];
     */
    BypassFlagClear,

    /* No data */
    BypassFlagsClearAll,

    /*
     * uint16_t size;
     * char flag[size];
     */
    SystemFlagSet,

    /*
     * uint16_t size;
     * char flag[size];
     */
    SystemFlagClear,

    /* No data */
    SystemFlagsClearAll,

    /* No data */
    RestartRequest,
};

enum class PacketToClient : std::uint8_t {
    /* No data */
    Hello = 0,

    /* No Data */
    Pong,

    /*
     * uint32_t size;
     * uint8_t data[size];
     */
    Event,

    /*
     * uint32_t size;
     * uint8_t data[size];
     */
    AutoprobeState,

    /*
     * uint16_t name_length;
     * char name[name_length];
     * uint32_t data_length;
     * uint8_t data[data_length];
     */
    InterfaceInformation,

    /*
     * uint16_t name_length;
     * char name[name_length];
     * uint32_t data_length;
     * uint8_t data[data_length];
     */
    InterfaceState,

    /*
     * uint32_t size;
     * uint8_t data[size];
     */
    RealtimeName,

    /*
     * uint16_t name;
     * uint32_t size;
     * uint8_t data[size];
     */
    RealtimeValue,

    /*
     * uint32_t size;
     * uint8_t data[size];
     */
    ArchiveValue,

    /* No Data */
    ArchiveEnd,
};

static const QString defaultLocalSocketName = "CPD3Acquisition";
static const std::uint16_t defaultPort = 14234;

static bool checkCertificate(const Variant::Read &auth,
                             const QSslCertificate &cert,
                             bool defaultResult = true)
{
    if (!auth.exists())
        return defaultResult;

    switch (auth.getType()) {
    case Variant::Type::Hash: {
        QString key(Algorithms::Cryptography::sha512(cert).toHex().toLower());
        QSslCertificate check(Algorithms::Cryptography::getCertificate(auth.hash(key), false));
        if (check.isNull())
            return false;
        return check == cert;
    }

    case Variant::Type::Array:
        for (auto raw : auth.toArray()) {
            QSslCertificate check(Algorithms::Cryptography::getCertificate(raw, false));
            if (check.isNull())
                continue;
            if (check == cert)
                return true;
        }
        return false;

    default: {
        QSslCertificate check(Algorithms::Cryptography::getCertificate(auth, false));
        if (check.isNull())
            return false;
        return check == cert;
    }
    }

    Q_ASSERT(false);
    return false;
}

static std::function<bool(QSslSocket &socket)> toTLSAccept(const Variant::Read &configuration)
{
    if (!configuration.exists())
        return {};

    Variant::Root ssl(configuration);
    return [ssl](QSslSocket &socket) -> bool {
        QObject::connect(&socket, &QSslSocket::encrypted, [&socket, ssl] {
            if (!checkCertificate(ssl["Authorization"], socket.peerCertificate())) {
                qCDebug(log_acquisition_network) << "Disconnecting unauthorized TLS peer"
                                                 << socket.peerAddress();
                socket.disconnectFromHost();
                socket.close();
                socket.readAll();
                return;
            }
        });
        return Algorithms::Cryptography::setupSocket(&socket, ssl.read());
    };
}

}

bool AcquisitionNetworkServer::checkExisting(const Variant::Read &configuration)
{
    if (!configuration["DisableLocal"].toBool()) {
        auto socketName = configuration["LocalSocket"].toQString();
        if (socketName.isEmpty())
            socketName = defaultLocalSocketName;

        return IO::Socket::Local::connect(socketName).get() != nullptr;
    }

    if (!configuration["DisableLoopback"].toBool()) {
        auto port = configuration["LoopbackPort"].toInteger();
        if (!INTEGER::defined(port) || port <= 1 || port > 65535)
            port = defaultPort;

        auto check = IO::Socket::TCP::loopback({static_cast<std::uint16_t>(port)});
        if (!check)
            return false;

        std::mutex mutex;
        std::condition_variable cv;
        enum class State {
            Wait, Running, Invalid, Ended,
        } state = State::Wait;

        check->read.connect([&](const Util::ByteArray &data) {
            if (data.empty())
                return;
            {
                std::lock_guard<std::mutex> lock(mutex);
                if (state != State::Wait)
                    return;
                if (data.front() == static_cast<std::uint8_t>(PacketToClient::Hello)) {
                    state = State::Running;
                } else {
                    state = State::Invalid;
                }
            }
            cv.notify_all();
        });
        check->ended.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                state = State::Ended;
            }
            cv.notify_all();
        });

        check->start();

        {
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(PacketToServer::Hello));
            check->write(std::move(packet));
        }

        std::unique_lock<std::mutex> lock(mutex);
        cv.wait_for(lock, std::chrono::seconds(5), [&state] { return state != State::Wait; });
        bool isRunning = (state == State::Running);
        lock.unlock();
        check.reset();

        return isRunning;
    }

    return false;
}

AcquisitionNetworkServer::AcquisitionNetworkServer(const Variant::Read &configuration) : terminated(
        false)
{
    if (!configuration["DisableLocal"].toBool()) {
        localSocketListen = configuration["LocalSocket"].toQString();
        if (localSocketListen.isEmpty())
            localSocketListen = defaultLocalSocketName;
    }

    if (!configuration["DisableLoopback"].toBool()) {
        auto port = configuration["LoopbackPort"].toInteger();
        if (!INTEGER::defined(port) || port <= 1 || port > 65535)
            port = defaultPort;

        tcpListen.emplace_back(" ",
                               IO::Socket::TCP::Configuration(static_cast<std::uint16_t>(port)));
    }

    if (configuration["Listen"].getType() == Variant::Type::Hash) {
        auto port = configuration["Listen/Port"].toInteger();
        if (!INTEGER::defined(port) || port <= 1 || port > 65535)
            port = defaultPort;

        tcpListen.emplace_back(configuration["Listen/Address"].toString(),
                               IO::Socket::TCP::Configuration(static_cast<std::uint16_t>(port),
                                                              toTLSAccept(
                                                                      configuration["Listen/SSL"])));
    } else {
        for (auto interface : configuration["Listen"].toChildren()) {
            auto port = interface["Port"].toInteger();
            if (!INTEGER::defined(port) || port <= 1 || port > 65535)
                port = defaultPort;

            tcpListen.emplace_back(interface["Address"].toString(),
                                   IO::Socket::TCP::Configuration(static_cast<std::uint16_t>(port),
                                                                  toTLSAccept(interface["SSL"])));
        }
    }
}

AcquisitionNetworkServer::~AcquisitionNetworkServer()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    notify.notify_all();

    if (thread.joinable())
        thread.join();

    clients.clear();
}

void AcquisitionNetworkServer::start()
{
    if (!localSocketListen.isEmpty()) {
        std::unique_ptr<IO::Socket::Local::Server> server(new IO::Socket::Local::Server(
                std::bind(&AcquisitionNetworkServer::incomingConnection, this,
                          std::placeholders::_1), localSocketListen));
        if (server->startListening()) {
            servers.emplace_back(std::move(server));
        }
    }
    for (auto &interface : tcpListen) {
        std::unique_ptr<IO::Socket::TCP::Server> server;
        if (interface.address.empty()) {
            server.reset(new IO::Socket::TCP::Server(
                    std::bind(&AcquisitionNetworkServer::incomingConnection, this,
                              std::placeholders::_1), std::move(interface.config)));
        } else if (interface.address == " ") {
            server.reset(new IO::Socket::TCP::Server(
                    std::bind(&AcquisitionNetworkServer::incomingConnection, this,
                              std::placeholders::_1),
                    IO::Socket::TCP::Server::AddressFamily::Loopback, std::move(interface.config)));
        } else {
            server.reset(new IO::Socket::TCP::Server(
                    std::bind(&AcquisitionNetworkServer::incomingConnection, this,
                              std::placeholders::_1), std::move(interface.address),
                    std::move(interface.config)));
        }
        if (!server->startListening())
            continue;

        servers.emplace_back(std::move(server));
    }

    qCDebug(log_acquisition_network) << "Server startup complete with" << servers.size()
                                     << "sockets";

    thread = std::thread(std::bind(&AcquisitionNetworkServer::run, this));
}

static const SequenceName::Component archiveRaw = "raw";
static const SequenceName::Component archiveRawMeta = "raw_meta";
static const SequenceName::Component archiveRealtimeInstant = "rt_instant";
static const SequenceName::Component archiveRealtimeBoxcar = "rt_boxcar";

static bool retainArchiveValue(const SequenceValue &value)
{
    if (Range::compareStartEnd(value.getStart(), value.getEnd()) >= 0)
        return false;
    return value.getArchive() == archiveRaw || value.getArchive() == archiveRawMeta;
}

void AcquisitionNetworkServer::run()
{
    for (;;) {
        std::vector<std::unique_ptr<IO::Socket::Connection>> attachConnections;
        std::vector<std::function<void()>> executeCalls;
        Data::SequenceValue::Transfer dispatchRealtime;
        {
            std::unique_lock<std::mutex> lock(mutex);
            for (;;) {
                if (terminated)
                    return;

                attachConnections = std::move(receivedConnections);
                receivedConnections.clear();

                executeCalls = std::move(pendingCalls);
                pendingCalls.clear();

                dispatchRealtime = std::move(realtimeDataUpdate);
                realtimeDataUpdate.clear();

                if (!attachConnections.empty())
                    break;
                if (!executeCalls.empty())
                    break;
                if (!dispatchRealtime.empty())
                    break;

                Clock::time_point wakeTime = Clock::now() + std::chrono::seconds(5);
                bool waitForAction = true;
                for (const auto &c : clients) {
                    if (c->wakeAt(wakeTime)) {
                        waitForAction = false;
                        break;
                    }
                }
                if (!waitForAction)
                    break;

                notify.wait_until(lock, wakeTime);
            }
        }

        Clock::time_point now = Clock::now();

        for (auto &connection : attachConnections) {
            clients.emplace_back(new ConnectedClient(*this, std::move(connection)));
        }

        if (!dispatchRealtime.empty()) {
            for (const auto &value : dispatchRealtime) {
                if (value.getName().isMeta()) {
                    auto base = value.getName();
                    base.clearMeta();
                    auto target = retainedRealtime.find(base);
                    if (target == retainedRealtime.end()) {
                        RealtimeData data(base);
                        target = retainedRealtime.emplace(std::move(base), std::move(data)).first;
                    }
                    target->second.updateMetadata(value.root());
                } else {
                    auto target = retainedRealtime.find(value.getName());
                    if (target == retainedRealtime.end()) {
                        target = retainedRealtime.emplace(value.getName(),
                                                          RealtimeData(value.getName())).first;
                    }
                    target->second.updateData(value.root());
                }
            }

            for (auto &c : clients) {
                c->incomingRealtime(now, dispatchRealtime);
            }

            for (auto &value : dispatchRealtime) {
                if (!retainArchiveValue(value))
                    continue;
                value.setStart(Time::time());
                value.setEnd(FP::undefined());
                retainedArchive.emplace_back(std::move(value));
            }

            static constexpr std::size_t maximumArchiveRetain = 16384;
            if (retainedArchive.size() > maximumArchiveRetain) {
                auto remove = retainedArchive.size() - maximumArchiveRetain;
                retainedArchive.erase(retainedArchive.begin(), retainedArchive.begin() + remove);
            }
        }

        for (auto c = clients.begin(); c != clients.end();) {
            if (!(*c)->update(now)) {
                c = clients.erase(c);
                continue;
            }
            ++c;
        }

        for (const auto &call : executeCalls) {
            call();
        }
    }
}

void AcquisitionNetworkServer::incomingRealtime(const SequenceValue::Transfer &values)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        Util::append(values, realtimeDataUpdate);
    }
    notify.notify_all();
}

void AcquisitionNetworkServer::incomingRealtime(SequenceValue::Transfer &&values)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        Util::append(std::move(values), realtimeDataUpdate);
    }
    notify.notify_all();
}

void AcquisitionNetworkServer::incomingRealtime(const SequenceValue &value)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        realtimeDataUpdate.emplace_back(value);
    }
    notify.notify_all();
}

void AcquisitionNetworkServer::incomingRealtime(SequenceValue &&value)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        realtimeDataUpdate.emplace_back(std::move(value));
    }
    notify.notify_all();
}

void AcquisitionNetworkServer::incomingEvent(const Variant::Read &event)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        Data::Variant::Root save(event);
        pendingCalls.emplace_back([this, save] {
            for (const auto &client : clients) {
                client->incomingEvent(save);
            }
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkServer::archiveFlush(double time)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this, time] {
            auto firstRetain =
                    Range::lowerBound(retainedArchive.begin(), retainedArchive.end(), time);
            if (firstRetain == retainedArchive.begin())
                return;
            retainedArchive.erase(retainedArchive.begin(), firstRetain);
        });
    }
    notify.notify_all();
}

static void updateRetained(std::unordered_map<std::string, Variant::Root> &retained,
                           const std::string &name,
                           const Variant::Read &data)
{
    auto target = retained.find(name);
    if (!data.exists()) {
        if (target != retained.end()) {
            retained.erase(target);
        }
    } else {
        if (target == retained.end()) {
            retained.emplace(name, Variant::Root(data));
        } else {
            target->second.write().set(data);
        }
    }
}

void AcquisitionNetworkServer::updateInterfaceInformation(const std::string &name,
                                                          const Variant::Read &data)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        Data::Variant::Root save(data);
        pendingCalls.emplace_back([this, name, save] {
            for (const auto &client : clients) {
                client->updateInterfaceInformation(name, save);
            }
            updateRetained(retainedInterfaceInformation, name, save);
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkServer::updateInterfaceState(const std::string &name,
                                                    const Variant::Read &data)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        Data::Variant::Root save(data);
        pendingCalls.emplace_back([this, name, save] {
            for (const auto &client : clients) {
                client->updateInterfaceState(name, save);
            }
            updateRetained(retainedInterfaceState, name, save);
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkServer::updateAutoprobeState(const Variant::Read &data)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        Data::Variant::Root save(data);
        pendingCalls.emplace_back([this, save] {
            for (const auto &client : clients) {
                client->updateAutoprobeState(save);
            }
            retainedAutoprobeState.write().set(save);
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkServer::incomingConnection(std::unique_ptr<
        IO::Socket::Connection> &&connection)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        receivedConnections.emplace_back(std::move(connection));
    }
    notify.notify_all();
}

std::deque<SequenceValue> AcquisitionNetworkServer::getRetainedArchive(double startTime,
                                                                       SequenceMatch::Basic &filter) const
{
    /* There's likely to be a realtime value at nearly the exact end, so
     * add a grace period to make sure it's dropped.  This prevents an
     * odd flatline anomaly right at the transition time. */
    if (FP::defined(startTime))
        startTime += 1.0;

    auto lb = Range::upperBound(retainedArchive.cbegin(), retainedArchive.cend(), startTime);
    if (lb == retainedArchive.cend())
        return {};

    std::deque<SequenceValue> result;
    for (auto endList = retainedArchive.cend(); lb != endList; ++lb) {
        if (!filter.matches(lb->getUnit(), lb->getStart(), lb->getEnd()))
            continue;
        result.emplace_back(*lb);
    }
    return result;
}

AcquisitionNetworkServer::RealtimeData::RealtimeData() : onlySendOnUpdate(false),
                                                         sendToNewConnection(true),
                                                         priority(0)
{ }

AcquisitionNetworkServer::RealtimeData::RealtimeData(const Data::SequenceName &name)
        : onlySendOnUpdate(
        name.getArchive() == archiveRaw || name.getArchive() == archiveRealtimeBoxcar),
          sendToNewConnection(!onlySendOnUpdate),
          priority(0)
{ }

void AcquisitionNetworkServer::RealtimeData::updateData(const Data::Variant::Root &value)
{
    latest = value;
}

void AcquisitionNetworkServer::RealtimeData::updateMetadata(const Data::Variant::Root &value)
{
    metadata = value;

    auto rt = metadata.read().metadata("Realtime");

    {
        auto p = rt.hash("NetworkPriority").toInteger();
        if (INTEGER::defined(p)) {
            priority = p;
        }
    }
    if (!onlySendOnUpdate) {
        auto p = rt.hash("NetworkTransient");
        if (p.exists()) {
            sendToNewConnection = !p.toBoolean();
        }
    }
}

AcquisitionNetworkServer::ConnectedClient::ConnectedClient(AcquisitionNetworkServer &par,
                                                           std::unique_ptr<
                                                                   IO::Socket::Connection> &&conn)
        : parent(par),
          state(State::AwaitHandshake),
          timeout(Clock::now() + std::chrono::seconds(30)),
          incomingEnd(false),
          writeStalled(false),
          realtimeNameReplace(0),
          connection(std::move(conn))
{
    connection->read.connect([this](const Util::ByteArray &data) {
        {
            std::lock_guard<std::mutex> lock(parent.mutex);
            incomingRead += data;
        }
        parent.notify.notify_all();
    });
    connection->ended.connect([this]() {
        {
            std::lock_guard<std::mutex> lock(parent.mutex);
            incomingEnd = true;
        }
        parent.notify.notify_all();
    });
    connection->writeStall.connect([this](bool stalled) {
        {
            std::lock_guard<std::mutex> lock(parent.mutex);
            writeStalled = stalled;
        }
        if (stalled)
            return;
        parent.notify.notify_all();
    });
    connection->start();
}

AcquisitionNetworkServer::ConnectedClient::~ConnectedClient()
{
    connection.reset();

    if (archive) {
        archive->sink.terminate();
        archive->reader->signalTerminate();
        archive->reader->wait();
        archive->access.signalTerminate();
        archive.reset();
    }
}

bool AcquisitionNetworkServer::ConnectedClient::wakeAt(Clock::time_point &wake) const
{
    if (timeout < wake)
        wake = timeout;
    if (!incomingRead.empty())
        return true;
    if (incomingEnd)
        return true;
    if (state != State::Running)
        return false;

    if (!writeStalled) {
        if (!pendingRealtimeUpdate.empty())
            return true;
        if (archive) {
            if (!archive->outputQueue.empty())
                return true;
            if (archive->reader && archive->sink.ended)
                return true;
        }
    }

    return false;
}

bool AcquisitionNetworkServer::ConnectedClient::update(const Clock::time_point &now)
{
    if (timeout <= now) {
        qCDebug(log_acquisition_network) << "Connection to" << connection->describePeer()
                                         << "timed out";
        return false;
    }

    switch (state) {
    case State::AwaitHandshake: {
        {
            std::lock_guard<std::mutex> lock(parent.mutex);
            if (incomingEnd && incomingRead.empty()) {
                qCDebug(log_acquisition_network) << "Connection to" << connection->describePeer()
                                                 << "ended without a handshake";
                state = State::Ended;
                return false;
            }
            if (incomingRead.empty())
                return true;
            if (incomingRead.front() != static_cast<std::uint8_t>(PacketToServer::Hello)) {
                qCDebug(log_acquisition_network) << "Invalid handshake from"
                                                 << connection->describePeer();
                state = State::Ended;
                return false;
            }
            incomingRead.pop_front();
        }
        qCDebug(log_acquisition_network) << "Connection to" << connection->describePeer()
                                         << "completed handshake";

        timeout = now + std::chrono::seconds(30);
        timeOfStreamStall = now;
        state = State::Running;

        {
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(PacketToClient::Hello));
            connection->write(std::move(packet));
        }

        for (const auto &info : parent.retainedInterfaceInformation) {
            updateInterfaceInformation(info.first, info.second);
        }
        for (const auto &state : parent.retainedInterfaceState) {
            updateInterfaceState(state.first, state.second);
        }
        if (parent.retainedAutoprobeState.read().exists()) {
            updateAutoprobeState(parent.retainedAutoprobeState);
        }
        for (const auto &v : parent.retainedRealtime) {
            if (!v.second.metadata.read().exists())
                continue;
            sendRealtimeValue(v.first.toMeta(), v.second.metadata);
        }
        for (const auto &v : parent.retainedRealtime) {
            if (!v.second.sendToNewConnection)
                continue;
            if (!v.second.latest.read().exists())
                continue;
            sendRealtimeValue(v.first, v.second.latest);
        }

        break;
    }
    case State::Running:
        break;
    case State::Ended:
        return false;
    }

    bool streamValues;
    {
        std::lock_guard<std::mutex> lock(parent.mutex);
        processRead += std::move(incomingRead);
        incomingRead.clear();
        if (incomingEnd) {
            state = State::Ended;
        }
        streamValues = !writeStalled;
    }

    if (streamValues) {
        flushPendingRealtime(now);
        flushPendingArchive(now);
    } else {
        timeOfStreamStall = now;
    }

    while (processNextPacket(now)) { }
    return state != State::Ended;
}

static bool processValuePacket(Util::ByteArray &buffer,
                               const std::function<void(Variant::Root &&)> &call)
{
    if (buffer.size() < 5U)
        return false;
    auto size = buffer.readNumber<quint32>(1U);
    if (buffer.size() < 5U + size)
        return false;

    Variant::Root value;
    {
        auto contents = buffer.mid(5U, size);
        Util::ByteView::Device dev(contents);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);
        stream >> value;

        if (stream.status() != QDataStream::Ok) {
            buffer.pop_front(5U + size);
            return true;
        }
    }

    buffer.pop_front(5U + size);
    call(std::move(value));
    return true;
}

static bool processNameValuePacket(Util::ByteArray &buffer,
                                   const std::function<
                                           void(std::string &&, Variant::Root &&)> &call)
{
    if (buffer.size() < 3U)
        return false;
    auto nameSize = buffer.readNumber<quint16>(1);
    if (buffer.size() < 3U + nameSize + 4U)
        return false;
    auto dataSize = buffer.readNumber<quint32>(3U + nameSize);
    if (buffer.size() < 3U + nameSize + 4U + dataSize)
        return false;

    std::string name(buffer.data<const char *>(3U), nameSize);

    Variant::Root value;
    {
        auto contents = buffer.mid(3U + nameSize + 4U, dataSize);
        Util::ByteView::Device dev(contents);
        dev.open(QIODevice::ReadOnly);
        QDataStream stream(&dev);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);
        stream >> value;

        if (stream.status() != QDataStream::Ok) {
            buffer.pop_front(3U + nameSize + 4U + dataSize);
            return true;
        }
    }
    buffer.pop_front(3U + nameSize + 4U + dataSize);

    call(std::move(name), std::move(value));
    return true;
}

static bool processFlagPacket(Util::ByteArray &buffer,
                              const std::function<void(CPD3::Data::Variant::Flag &&)> &call)
{
    if (buffer.size() < 3U)
        return false;
    auto size = buffer.readNumber<quint16>(1U);
    if (buffer.size() < 3U + size)
        return false;

    CPD3::Data::Variant::Flag flag(buffer.data<const char *>(3U), size);
    buffer.pop_front(3U + size);

    call(std::move(flag));
    return true;
}

bool AcquisitionNetworkServer::ConnectedClient::processNextPacket(const Clock::time_point &now)
{
    if (processRead.empty())
        return false;

    switch (static_cast<PacketToServer>(processRead.front())) {
    case PacketToServer::Ping: {
        processRead.pop_front();
        {
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(PacketToClient::Pong));
            connection->write(std::move(packet));
        }
        timeout = now + std::chrono::seconds(30);
        break;
    }

    case PacketToServer::RealtimeResend: {
        processRead.pop_front();
        for (const auto &v : parent.retainedRealtime) {
            if (!v.second.metadata.read().exists())
                continue;
            sendRealtimeValue(v.first.toMeta(), v.second.metadata);
        }
        for (const auto &v : parent.retainedRealtime) {
            if (!v.second.sendToNewConnection)
                continue;
            if (!v.second.latest.read().exists())
                continue;
            sendRealtimeValue(v.first, v.second.latest);
        }

        qCDebug(log_acquisition_network) << "Resending realtime to" << connection->describePeer();
        break;
    }

    case PacketToServer::StartArchiveRead: {
        if (processRead.size() < 5U)
            return false;
        auto size = processRead.readNumber<quint32>(1U);
        if (processRead.size() < 5U + size)
            return false;
        if (archive) {
            abortArchiveRead();
        }

        Data::Archive::Selection::List selections;
        {
            auto contents = processRead.mid(5U, size);
            Util::ByteView::Device dev(contents);
            dev.open(QIODevice::ReadOnly);
            QDataStream stream(&dev);
            stream.setByteOrder(QDataStream::LittleEndian);
            stream.setVersion(QDataStream::Qt_4_5);
            stream >> selections;

            if (stream.status() != QDataStream::Ok)
                selections = Data::Archive::Selection::List();
        }
        processRead.pop_front(5U + size);

        if (selections.empty()) {
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(PacketToClient::ArchiveEnd));
            connection->write(std::move(packet));
            break;
        }

        qCDebug(log_acquisition_network) << "Client" << connection->describePeer()
                                         << "requested archive read" << selections;

        archive.reset(new Archive(*this, std::move(selections)));
        break;
    }

    case PacketToServer::AbortArchiveRead: {
        processRead.pop_front();
        if (archive) {
            abortArchiveRead();
        }
        qCDebug(log_acquisition_network) << "Archive read aborted on" << connection->describePeer();
        break;
    }

    case PacketToServer::MessageLogEvent:
        return processValuePacket(processRead, [this](Variant::Root &&message) {
            parent.messageLogEvent(message);
        });

    case PacketToServer::Command:
        return processNameValuePacket(processRead,
                                      [this](std::string &&name, Variant::Root &&command) {
                                          parent.commandReceived(name, command);
                                      });

    case PacketToServer::SystemFlush: {
        if (processRead.size() < 9U)
            return false;
        Util::ByteArray::Device dev(processRead);
        dev.open(QIODevice::ReadOnly);
        dev.seek(1);
        double duration = 0;
        QDataStream stream(&dev);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);
        stream >> duration;
        processRead.pop_front(9U);

        qCDebug(log_acquisition_network) << "Client" << connection->describePeer()
                                         << "requested system flush for"
                                         << Logging::elapsed(duration);
        parent.flushRequest(duration);
        break;
    }

    case PacketToServer::SetAveragingTime: {
        if (processRead.size() < 1U + 1U + 4U + 1U)
            return false;
        auto unit = processRead[1U];
        auto count = processRead.readNumber<qint32>(1U + 1U);
        bool align = processRead[1U + 1U + 4U] != 0;
        processRead.pop_front(1U + 1U + 4U + 1U);

        if (count < 0)
            count = 0;

        switch (static_cast<Time::LogicalTimeUnit>(unit)) {
        case Time::Millisecond:
        case Time::Second:
        case Time::Minute:
        case Time::Hour:
        case Time::Day:
        case Time::Week:
        case Time::Month:
        case Time::Quarter:
        case Time::Year:
            qCDebug(log_acquisition_network) << "Client" << connection->describePeer()
                                             << "set averaging time" << count
                                             << static_cast<Time::LogicalTimeUnit>(unit);

            parent.setAveragingTime(static_cast<Time::LogicalTimeUnit>(unit),
                                    static_cast<int>(count), align);
            break;
        default:
            break;
        }
        break;
    }

    case PacketToServer::DataFlush: {
        processRead.pop_front();
        parent.dataFlushRequest();
        break;
    }

    case PacketToServer::BypassFlagSet:
        return processFlagPacket(processRead, [this](Data::Variant::Flag &&flag) {
            parent.bypassFlagSet(flag);
        });

    case PacketToServer::BypassFlagClear:
        return processFlagPacket(processRead, [this](Data::Variant::Flag &&flag) {
            parent.bypassFlagClear(flag);
        });

    case PacketToServer::BypassFlagsClearAll: {
        processRead.pop_front();
        parent.bypassFlagClearAll();
        break;
    }

    case PacketToServer::SystemFlagSet:
        return processFlagPacket(processRead, [this](Data::Variant::Flag &&flag) {
            parent.systemFlagSet(flag);
        });

    case PacketToServer::SystemFlagClear:
        return processFlagPacket(processRead, [this](Data::Variant::Flag &&flag) {
            parent.systemFlagClear(flag);
        });

    case PacketToServer::SystemFlagsClearAll: {
        processRead.pop_front();
        parent.systemFlagClearAll();
        break;
    }

    case PacketToServer::RestartRequest: {
        processRead.pop_front();
        parent.restartRequested();
        break;
    }

    default:
        qCDebug(log_acquisition_network) << "Invalid packet type (" << processRead.front()
                                         << ") from" << connection->describePeer()
                                         << ", connection closing";
        state = State::Ended;
        processRead.clear();
        return false;
    }

    return true;
}


static Util::ByteArray serializeSingleValue(PacketToClient type, const Variant::Read &contents)
{
    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(type));
    packet.resize(5);
    {
        Util::ByteArray::Device dev(packet);
        dev.open(QIODevice::WriteOnly);
        dev.seek(5);
        QDataStream stream(&dev);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);
        stream << contents;
    }
    qToLittleEndian<quint32>(packet.size() - 5, packet.data<uchar *>(1));
    return packet;
}

static Util::ByteArray serializeNameAndValue(PacketToClient type,
                                             const std::string &name,
                                             const Variant::Read &contents)
{
    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(type));
    {
        std::uint16_t effectiveLength = name.size();
        if (effectiveLength > 0xFFFF)
            effectiveLength = 0xFFFF;
        packet.appendNumber<quint16>(effectiveLength);
        packet.push_back(name.data(), effectiveLength);
    }
    auto dataLengthOrigin = packet.size();
    packet.resize(dataLengthOrigin + 4);
    {
        Util::ByteArray::Device dev(packet);
        dev.open(QIODevice::WriteOnly);
        dev.seek(dataLengthOrigin + 4);
        QDataStream stream(&dev);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);
        stream << contents;
    }
    qToLittleEndian<quint32>(packet.size() - dataLengthOrigin - 4,
                             packet.data<uchar *>(dataLengthOrigin));
    return packet;
}

void AcquisitionNetworkServer::ConnectedClient::incomingEvent(const Variant::Read &event)
{ connection->write(serializeSingleValue(PacketToClient::Event, event)); }

void AcquisitionNetworkServer::ConnectedClient::updateInterfaceInformation(const std::string &name,
                                                                           const Variant::Read &data)
{ connection->write(serializeNameAndValue(PacketToClient::InterfaceInformation, name, data)); }

void AcquisitionNetworkServer::ConnectedClient::updateInterfaceState(const std::string &name,
                                                                     const Variant::Read &data)
{ connection->write(serializeNameAndValue(PacketToClient::InterfaceState, name, data)); }

void AcquisitionNetworkServer::ConnectedClient::updateAutoprobeState(const Variant::Read &data)
{ connection->write(serializeSingleValue(PacketToClient::AutoprobeState, data)); }

AcquisitionNetworkServer::ConnectedClient::QueuedRealtimeUpdate::QueuedRealtimeUpdate() = default;

AcquisitionNetworkServer::ConnectedClient::QueuedRealtimeUpdate::QueuedRealtimeUpdate(const Clock::time_point &now)
        : oldestUpdate(now), unsentUpdates(0)
{ }

void AcquisitionNetworkServer::ConnectedClient::incomingRealtime(const Clock::time_point &now,
                                                                 const SequenceValue::Transfer &values)
{
    for (const auto &v : values) {
        auto existing = pendingRealtimeUpdate.find(v.getName());
        if (existing == pendingRealtimeUpdate.end()) {
            pendingRealtimeUpdate.emplace(v.getName(), now);
            continue;
        }
        existing->second.unsentUpdates++;
    }
}

std::uint_fast16_t AcquisitionNetworkServer::ConnectedClient::assignRealtimeName(const SequenceName &name)
{
    {
        auto check = realtimeNameForward.find(name);
        if (check != realtimeNameForward.end())
            return check->second;
    }

    std::uint_fast16_t id;
    if (realtimeNameReverse.size() < 0xFFFFU) {
        id = realtimeNameReverse.size();
        realtimeNameReverse.emplace_back(name);
        realtimeNameForward.emplace(name, id);
    } else {
        id = realtimeNameReplace;
        realtimeNameReplace = (realtimeNameReplace + 1U) % 0xFFFFU;
        auto &reverse = realtimeNameReverse[id];
        realtimeNameForward.erase(reverse);
        reverse = name;
        realtimeNameForward.emplace(name, id);
    }

    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(PacketToClient::RealtimeName));
    packet.resize(5);
    {
        Util::ByteArray::Device dev(packet);
        dev.open(QIODevice::WriteOnly);
        dev.seek(5);
        QDataStream stream(&dev);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);
        stream << name;
    }
    qToLittleEndian<quint32>(packet.size() - 5, packet.data<uchar *>(1));
    connection->write(std::move(packet));

    return id;
}

void AcquisitionNetworkServer::ConnectedClient::sendRealtimeValue(const SequenceName &name,
                                                                  const Variant::Read &value)
{
    pendingRealtimeUpdate.erase(name);

    auto id = assignRealtimeName(name);
    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(PacketToClient::RealtimeValue));
    packet.appendNumber<quint16>(id);
    auto dataLengthOrigin = packet.size();
    packet.resize(dataLengthOrigin + 4);
    {
        Util::ByteArray::Device dev(packet);
        dev.open(QIODevice::WriteOnly);
        dev.seek(dataLengthOrigin + 4);
        QDataStream stream(&dev);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);
        stream << value;
    }
    qToLittleEndian<quint32>(packet.size() - dataLengthOrigin - 4,
                             packet.data<uchar *>(dataLengthOrigin));

    connection->write(std::move(packet));
}

void AcquisitionNetworkServer::ConnectedClient::flushPendingRealtime(const Clock::time_point &now)
{
    if (pendingRealtimeUpdate.empty())
        return;

    struct SortElement {
        SequenceName name;
        Clock::time_point oldestUpdate;
        std::uint_fast64_t priority;
        bool isMeta;
        bool largeBacklog;
        bool isInstant;

        Variant::Read value;

        SortElement() = default;

        SortElement(const Clock::time_point &now,
                    SequenceName inc,
                    const QueuedRealtimeUpdate &queued,
                    const RealtimeData &data) : name(std::move(inc)),
                                                oldestUpdate(queued.oldestUpdate),
                                                priority(data.priority),
                                                isMeta(name.isMeta()),
                                                largeBacklog(false),
                                                isInstant(
                                                        name.getArchive() == archiveRealtimeInstant)
        {
            if (isMeta) {
                value = data.metadata.read();
            } else {
                value = data.latest.read();
            }

            std::size_t backlogThreshold = 2;
            if (priority > 0) {
                backlogThreshold += priority;
            }
            if (queued.unsentUpdates > backlogThreshold) {
                if ((now - oldestUpdate) > std::chrono::seconds(5)) {
                    largeBacklog = true;
                }
            }
        }

        bool operator<(const SortElement &other) const
        {
            if (isMeta) {
                if (!other.isMeta)
                    return true;
            } else if (other.isMeta) {
                return false;
            }

            if (largeBacklog) {
                if (!other.largeBacklog)
                    return true;
            } else if (other.largeBacklog) {
                return false;
            }

            if (priority != other.priority) {
                return priority < other.priority;
            }

            /* Update is the time of the first unsent update, so send older ones first */
            if (oldestUpdate != other.oldestUpdate) {
                return oldestUpdate < other.oldestUpdate;
            }

            if (isInstant) {
                if (!other.isInstant)
                    return true;
            } else if (other.isInstant) {
                return false;
            }

            return SequenceName::OrderLogical()(name, other.name);
        }
    };
    std::vector<SortElement> order;
    for (const auto &pend : pendingRealtimeUpdate) {
        auto lookup = parent.retainedRealtime.find(pend.first.fromMeta());
        if (lookup == parent.retainedRealtime.end()) {
            pendingRealtimeUpdate.erase(pend.first);
            continue;
        }
        order.emplace_back(now, pend.first, pend.second, lookup->second);
    }
    std::sort(order.begin(), order.end());

    std::size_t flushPerIteration = 256U;
    {
        auto timeSinceStall =
                std::chrono::duration_cast<std::chrono::seconds>(now - timeOfStreamStall).count();
        if (timeSinceStall > 4)
            timeSinceStall = 4;
        flushPerIteration += 1024U * timeSinceStall;
        if (flushPerIteration > 4096U)
            flushPerIteration = 4096U;
    }

    if (order.size() > flushPerIteration)
        order.resize(flushPerIteration);
    for (const auto &o : order) {
        sendRealtimeValue(o.name, o.value);
    }
}

void AcquisitionNetworkServer::ConnectedClient::sendArchiveValue(const Data::SequenceValue &value)
{
    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(PacketToClient::ArchiveValue));
    packet.resize(5);
    {
        Util::ByteArray::Device dev(packet);
        dev.open(QIODevice::WriteOnly);
        dev.seek(5);
        QDataStream stream(&dev);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);
        stream << value;
    }
    qToLittleEndian<quint32>(packet.size() - 5, packet.data<uchar *>(1));
    connection->write(std::move(packet));
}

void AcquisitionNetworkServer::ConnectedClient::flushPendingArchive(const Clock::time_point &now)
{
    if (!archive)
        return;

    std::size_t writePossible = 256U;
    if (pendingRealtimeUpdate.empty()) {
        auto timeSinceStall =
                std::chrono::duration_cast<std::chrono::seconds>(now - timeOfStreamStall).count();
        if (timeSinceStall > 4)
            timeSinceStall = 4;
        writePossible += 1024U * timeSinceStall;
        if (writePossible > 4096U)
            writePossible = 4096U;
    } else {
        writePossible = 32U;
    }

    if (archive->reader) {
        for (;;) {
            if (!writePossible)
                return;

            std::unique_lock<std::mutex> lock(parent.mutex);
            if (archive->outputQueue.empty()) {
                if (!archive->sink.ended)
                    return;
                lock.unlock();

                archive->reader.reset();
                archive->outputQueue =
                        parent.getRetainedArchive(archive->endOfRead, *archive->filter);
                archive->filter.reset();
                break;
            }
            auto send = std::move(archive->outputQueue.front());
            archive->outputQueue.pop_front();
            lock.unlock();
            archive->sink.dataConsumed.notify_all();

            archive->endOfRead = send.getStart();
            sendArchiveValue(send);
            --writePossible;
        }
    }

    Q_ASSERT(archive->reader.get() == nullptr);
    for (;;) {
        if (!writePossible)
            return;

        if (archive->outputQueue.empty()) {
            archive.reset();

            {
                Util::ByteArray packet;
                packet.push_back(static_cast<std::uint8_t>(PacketToClient::ArchiveEnd));
                connection->write(std::move(packet));
            }
            return;
        }
        sendArchiveValue(archive->outputQueue.front());
        archive->outputQueue.pop_front();
        --writePossible;
    }
}

void AcquisitionNetworkServer::ConnectedClient::abortArchiveRead()
{
    if (archive) {
        archive->sink.terminate();
        archive->reader->signalTerminate();
        archive->reader->wait();
        archive->access.signalTerminate();
        archive.reset();
    }

    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(PacketToClient::ArchiveEnd));
    connection->write(std::move(packet));
}

AcquisitionNetworkServer::ConnectedClient::Archive::Archive(ConnectedClient &parent,
                                                            Data::Archive::Selection::List selections)
        : sink(*this, parent.parent), endOfRead(FP::undefined())
{
    for (auto &mod : selections) {
        if (mod.archives.empty()) {
            mod.archives = Data::Archive::Selection::Match{"raw"};
            mod.includeMetaArchive = true;
        } else if (mod.archives[0] != "raw_meta") {
            mod.archives = Data::Archive::Selection::Match{"raw"};
        } else {
            mod.archives = Data::Archive::Selection::Match{"raw_meta"};
            mod.includeMetaArchive = false;
        }
    }
    filter = SequenceMatch::Basic::compile(selections);
    reader = access.readStream(selections, &sink);
}

AcquisitionNetworkServer::ConnectedClient::Archive::Sink::Sink(AcquisitionNetworkServer::ConnectedClient::Archive &target,
                                                               AcquisitionNetworkServer &parent)
        : mutex(parent.mutex),
          notify(parent.notify),
          queue(target.outputQueue),
          ended(false),
          terminated(false)
{ }

AcquisitionNetworkServer::ConnectedClient::Archive::Sink::~Sink() = default;

void AcquisitionNetworkServer::ConnectedClient::Archive::Sink::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Util::append(values, queue);
    }
    notify.notify_all();
}

void AcquisitionNetworkServer::ConnectedClient::Archive::Sink::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Util::append(std::move(values), queue);
    }
    notify.notify_all();
}

void AcquisitionNetworkServer::ConnectedClient::Archive::Sink::incomingData(const SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        queue.emplace_back(value);
    }
    notify.notify_all();
}

void AcquisitionNetworkServer::ConnectedClient::Archive::Sink::incomingData(SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        queue.emplace_back(std::move(value));
    }
    notify.notify_all();
}

void AcquisitionNetworkServer::ConnectedClient::Archive::Sink::endData()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        ended = true;
    }
    notify.notify_all();
}

void AcquisitionNetworkServer::ConnectedClient::Archive::Sink::stall(std::unique_lock<
        std::mutex> &lock)
{
    while (queue.size() > StreamSink::stallThreshold) {
        if (terminated)
            return;
        dataConsumed.wait(lock);
    }
}

void AcquisitionNetworkServer::ConnectedClient::Archive::Sink::terminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    dataConsumed.notify_all();
}


AcquisitionNetworkClient::AcquisitionNetworkClient(const Variant::Read &config) : configuration(
        config),
                                                                                  terminated(false),
                                                                                  incomingEnded(
                                                                                          false),
                                                                                  realtimeSink(
                                                                                          nullptr),
                                                                                  archiveSink(
                                                                                          nullptr),
                                                                                  realtimeNameReplace(
                                                                                          0)
{
    if (configuration.getType() == Variant::Type::String) {
        auto parts = configuration.toQString().split(':');
        if (!parts.isEmpty()) {
            auto updated = Variant::Write::empty();
            updated.hash("Type").setString("RemoteServer");
            updated.hash("Server").setString(parts.front());
            if (parts.size() >= 2) {
                updated.hash("ServerPort").setInteger(parts[1].toInt());
            }
            configuration = updated;
        }
    }
    const auto &type = configuration.hash("Type").toString();
    if (Util::equal_insensitive(type, "remoteserver", "remote", "tcp", "ssl")) {
        auto port = configuration.hash("ServerPort").toInteger();
        if (!INTEGER::defined(port) || port <= 1 || port >= 65536)
            port = defaultPort;

        tcpConfiguration.reset(
                new IO::Socket::TCP::Configuration(static_cast<std::uint16_t>(port)));
        connectionTarget = configuration.hash("Server").toQString();

        if (!Util::equal_insensitive(type, "tcp") &&
                (configuration.hash("SSL").exists() || Util::equal_insensitive(type, "ssl"))) {
            tcpConfiguration->tls = toTLSAccept(configuration.hash("SSL"));
        }
    } else {
        connectionTarget = configuration.hash("Name").toQString();
        if (connectionTarget.isEmpty())
            connectionTarget = defaultLocalSocketName;
    }
}

AcquisitionNetworkClient::~AcquisitionNetworkClient()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    notify.notify_all();

    if (thread.joinable())
        thread.join();
}

std::unordered_map<std::string, Variant::Root> AcquisitionNetworkClient::getInterfaceInformation()
{
    std::lock_guard<std::mutex> lock(mutex);
    return interfaceInformation;
}

Variant::Root AcquisitionNetworkClient::getInterfaceInformation(const std::string &interface)
{
    std::lock_guard<std::mutex> lock(mutex);
    auto check = interfaceInformation.find(interface);
    if (check == interfaceInformation.end())
        return {};
    return check->second;
}

std::unordered_map<std::string, Variant::Root> AcquisitionNetworkClient::getInterfaceState()
{
    std::lock_guard<std::mutex> lock(mutex);
    return interfaceState;
}

Variant::Root AcquisitionNetworkClient::getInterfaceState(const std::string &interface)
{
    std::lock_guard<std::mutex> lock(mutex);
    auto check = interfaceState.find(interface);
    if (check == interfaceState.end())
        return {};
    return check->second;
}

Variant::Root AcquisitionNetworkClient::getAutoprobeState()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeState;
}

void AcquisitionNetworkClient::setRealtimeEgress(StreamSink *sink)
{
    std::lock_guard<std::mutex> lock(realtimeOutputLock);
    realtimeSink = sink;
}

void AcquisitionNetworkClient::realtimeResend()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this] {
            if (!connection)
                return;
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(PacketToServer::RealtimeResend));
            connection->write(std::move(packet));
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::remoteDataRead(StreamSink *sink,
                                              const Archive::Selection::List &selection)
{
    {
        std::unique_lock<std::mutex> output(archiveReadLock);
        if (archiveSink != nullptr) {
            output.unlock();
            remoteDataReadStop();
        }
    }
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this, sink, selection] {
            if (!connection) {
                sink->endData();
                return;
            }

            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(PacketToServer::StartArchiveRead));
            packet.resize(5);
            {
                Util::ByteArray::Device dev(packet);
                dev.open(QIODevice::WriteOnly);
                dev.seek(5);
                QDataStream stream(&dev);
                stream.setByteOrder(QDataStream::LittleEndian);
                stream.setVersion(QDataStream::Qt_4_5);
                stream << selection;
            }
            qToLittleEndian<quint32>(packet.size() - 5, packet.data<uchar *>(1));
            connection->write(std::move(packet));

            std::lock_guard<std::mutex> output(archiveReadLock);
            if (archiveSink) {
                archiveSink->endData();
            }
            archiveSink = sink;
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::remoteDataReadStop()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this] {
            if (!connection)
                return;
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(PacketToServer::AbortArchiveRead));
            connection->write(std::move(packet));
        });
    }
    notify.notify_all();
    {
        std::unique_lock<std::mutex> output(archiveReadLock);
        notify.wait(output, [this] { return archiveSink == nullptr; });
    }
}

static Util::ByteArray serializeSingleValue(PacketToServer type, const Variant::Read &contents)
{
    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(type));
    packet.resize(5);
    {
        Util::ByteArray::Device dev(packet);
        dev.open(QIODevice::WriteOnly);
        dev.seek(5);
        QDataStream stream(&dev);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);
        stream << contents;
    }
    qToLittleEndian<quint32>(packet.size() - 5, packet.data<uchar *>(1));
    return packet;
}

static Util::ByteArray serializeNameAndValue(PacketToServer type,
                                             const std::string &name,
                                             const Variant::Read &contents)
{
    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(type));
    {
        std::uint16_t effectiveLength = name.size();
        if (effectiveLength > 0xFFFF)
            effectiveLength = 0xFFFF;
        packet.appendNumber<quint16>(effectiveLength);
        packet.push_back(name.data(), effectiveLength);
    }
    auto dataLengthOrigin = packet.size();
    packet.resize(dataLengthOrigin + 4);
    {
        Util::ByteArray::Device dev(packet);
        dev.open(QIODevice::WriteOnly);
        dev.seek(dataLengthOrigin + 4);
        QDataStream stream(&dev);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);
        stream << contents;
    }
    qToLittleEndian<quint32>(packet.size() - dataLengthOrigin - 4,
                             packet.data<uchar *>(dataLengthOrigin));
    return packet;
}

static Util::ByteArray serializeFlag(PacketToServer type, const Variant::Flag &flag)
{
    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(type));
    {
        std::uint16_t effectiveLength = flag.size();
        if (effectiveLength > 0xFFFF)
            effectiveLength = 0xFFFF;
        packet.appendNumber<quint16>(effectiveLength);
        packet.push_back(flag.data(), effectiveLength);
    }
    return packet;
}

void AcquisitionNetworkClient::messageLogEvent(const Variant::Read &event)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        Data::Variant::Root save(event);
        pendingCalls.emplace_back([this, save] {
            if (!connection)
                return;
            connection->write(serializeSingleValue(PacketToServer::MessageLogEvent, save));
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::issueCommand(const std::string &target, const Variant::Read &command)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        Data::Variant::Root save(command);
        pendingCalls.emplace_back([this, target, save] {
            if (!connection)
                return;
            connection->write(serializeNameAndValue(PacketToServer::Command, target, save));
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::flushRequest(double seconds)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this, seconds] {
            if (!connection)
                return;
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(PacketToServer::SystemFlush));
            {
                Util::ByteArray::Device dev(packet);
                dev.open(QIODevice::WriteOnly);
                dev.seek(1);
                QDataStream stream(&dev);
                stream.setByteOrder(QDataStream::LittleEndian);
                stream.setVersion(QDataStream::Qt_4_5);
                stream << seconds;
            }
            connection->write(std::move(packet));
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::setAveragingTime(CPD3::Time::LogicalTimeUnit unit,
                                                int count,
                                                bool aligned)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this, unit, count, aligned] {
            if (!connection)
                return;
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(PacketToServer::SetAveragingTime));
            packet.push_back(static_cast<quint8>(unit));
            packet.appendNumber<qint32>(count);
            packet.push_back(static_cast<quint8>(aligned ? 1 : 0));
            connection->write(std::move(packet));
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::dataFlush()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this] {
            if (!connection)
                return;
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(PacketToServer::DataFlush));
            connection->write(std::move(packet));
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::bypassFlagSet(const Variant::Flag &flag)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this, flag] {
            if (!connection)
                return;
            connection->write(serializeFlag(PacketToServer::BypassFlagSet, flag));
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::bypassFlagClear(const Variant::Flag &flag)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this, flag] {
            if (!connection)
                return;
            connection->write(serializeFlag(PacketToServer::BypassFlagClear, flag));
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::bypassFlagClearAll()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this] {
            if (!connection)
                return;
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(PacketToServer::BypassFlagsClearAll));
            connection->write(std::move(packet));
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::systemFlagSet(const Variant::Flag &flag)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this, flag] {
            if (!connection)
                return;
            connection->write(serializeFlag(PacketToServer::SystemFlagSet, flag));
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::systemFlagClear(const Variant::Flag &flag)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this, flag] {
            if (!connection)
                return;
            connection->write(serializeFlag(PacketToServer::SystemFlagClear, flag));
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::systemFlagClearAll()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this] {
            if (!connection)
                return;
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(PacketToServer::SystemFlagsClearAll));
            connection->write(std::move(packet));
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::restartRequested()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        pendingCalls.emplace_back([this] {
            if (!connection)
                return;
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(PacketToServer::RestartRequest));
            connection->write(std::move(packet));
        });
    }
    notify.notify_all();
}

void AcquisitionNetworkClient::start()
{
    thread = std::thread(std::bind(&AcquisitionNetworkClient::run, this));
}

void AcquisitionNetworkClient::run()
{
    retryConnection = Clock::now();

    for (;;) {
        std::vector<std::function<void()>> executeCalls;
        bool processEnd = false;
        {
            std::unique_lock<std::mutex> lock(mutex);
            for (;;) {
                if (terminated) {
                    lock.unlock();
                    connection.reset();
                    {
                        std::lock_guard<std::mutex> output(archiveReadLock);
                        if (archiveSink) {
                            archiveSink->endData();
                            archiveSink = nullptr;
                            notify.notify_all();
                        }
                    }
                    return;
                }

                executeCalls = std::move(pendingCalls);
                pendingCalls.clear();

                processEnd = incomingEnded;
                incomingEnded = false;

                if (!incomingRead.empty()) {
                    processRead += std::move(incomingRead);
                    incomingRead.clear();
                    break;
                }

                if (processEnd)
                    break;
                if (!executeCalls.empty())
                    break;

                Clock::time_point wakeTime;
                auto now = Clock::now();
                if (!connection) {
                    if (retryConnection <= now)
                        break;
                    wakeTime = retryConnection;
                } else {
                    if (nextPing <= now)
                        break;
                    wakeTime = nextPing;

                    if (pingResponseTimeout <= now)
                        break;
                    if (wakeTime > pingResponseTimeout)
                        wakeTime = pingResponseTimeout;
                }
                notify.wait_until(lock, wakeTime);
            }
        }

        auto now = Clock::now();
        if (!connection) {
            processRead.clear();

            if (retryConnection <= now) {
                auto randomTime = Random::fp() * 5.0;
                randomTime += 5.0;
                retryConnection =
                        now + std::chrono::milliseconds(static_cast<int>(randomTime * 1000.0));

                establishConnection();
            }
            continue;
        }

        for (const auto &c : executeCalls) {
            c();
        }

        while (processNextPacket()) { }

        if (connection && processEnd) {
            qCDebug(log_acquisition_network) << "Client connection closed";
            connection.reset();
            retryConnection = now;

            {
                std::lock_guard<std::mutex> output(archiveReadLock);
                if (archiveSink) {
                    archiveSink->endData();
                    archiveSink = nullptr;
                }
            }
            notify.notify_all();
            connectionState(false);
            continue;
        } else if (connection) {
            if (nextPing <= now) {
                nextPing = Clock::now() + std::chrono::seconds(10);

                Util::ByteArray packet;
                packet.push_back(static_cast<std::uint8_t>(PacketToServer::Ping));
                connection->write(std::move(packet));
            }
            if (pingResponseTimeout <= now) {
                qCDebug(log_acquisition_network) << "Connection to server"
                                                 << connection->describePeer() << "timed out";
                connection.reset();

                {
                    std::lock_guard<std::mutex> output(archiveReadLock);
                    if (archiveSink) {
                        archiveSink->endData();
                        archiveSink = nullptr;
                    }
                }
                notify.notify_all();
                connectionState(false);
                continue;
            }
        }
    }
}

void AcquisitionNetworkClient::establishConnection()
{
    Q_ASSERT(connection.get() == nullptr);

    if (tcpConfiguration) {
        qCDebug(log_acquisition_network) << "Client starting connection to" << connectionTarget
                                         << "on port" << tcpConfiguration->port;
        if (connectionTarget.isEmpty()) {
            connection = IO::Socket::TCP::loopback(*tcpConfiguration, false);
        } else {
            connection = IO::Socket::TCP::connect(connectionTarget.toStdString(), *tcpConfiguration,
                                                  false);
        }
    } else {
        qCDebug(log_acquisition_network) << "Client starting connection to" << connectionTarget;
        connection = IO::Socket::Local::connect(connectionTarget, {}, false);
    }
    if (!connection) {
        connectionFailed();
        return;
    }

    incomingRead.clear();
    incomingEnded = false;
    realtimeNameLookup.clear();
    realtimeNameReplace = 0;

    connection->read.connect([this](const Util::ByteArray &data) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            incomingRead += data;
        }
        notify.notify_all();
    });
    connection->ended.connect([this]() {
        {
            std::lock_guard<std::mutex> lock(mutex);
            incomingEnded = true;
        }
        notify.notify_all();
    });

    connection->start();

    {
        Util::ByteArray packet;
        packet.push_back(static_cast<std::uint8_t>(PacketToServer::Hello));
        connection->write(std::move(packet));
    }

    {
        std::unique_lock<std::mutex> lock(mutex);
        auto timeout = Clock::now() + std::chrono::seconds(30);
        while (timeout > Clock::now()) {
            if (!incomingRead.empty()) {
                if (static_cast<PacketToClient>(incomingRead.front()) == PacketToClient::Hello) {
                    incomingRead.pop_front();

                    lock.unlock();

                    nextPing = Clock::now() + std::chrono::seconds(10);
                    pingResponseTimeout = Clock::now() + std::chrono::seconds(31);
                    connectionState(true);
                    return;
                }
                break;
            }
            if (incomingEnded)
                break;
            if (terminated) {
                lock.unlock();
                connection.reset();
                return;
            }

            notify.wait_until(lock, timeout);
        }
    }

    qCDebug(log_acquisition_network) << "Client connection to" << connection->describePeer()
                                     << "failed";
    connection.reset();

    auto randomTime = Random::fp() * 5.0;
    randomTime += 5.0;
    retryConnection =
            Clock::now() + std::chrono::milliseconds(static_cast<int>(randomTime * 1000.0));

    connectionFailed();
}

bool AcquisitionNetworkClient::processNextPacket()
{
    if (processRead.empty())
        return false;

    switch (static_cast<PacketToClient>(processRead.front())) {
    case PacketToClient::Pong:
        processRead.pop_front();
        pingResponseTimeout = Clock::now() + std::chrono::seconds(31);
        break;

    case PacketToClient::Event:
        return processValuePacket(processRead, [this](Variant::Root &&event) {
            realtimeEvent(event);
        });

    case PacketToClient::AutoprobeState:
        return processValuePacket(processRead, [this](Variant::Root &&state) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeState = std::move(state);
            }
            autoprobeStateUpdated();
        });

    case PacketToClient::InterfaceInformation:
        return processNameValuePacket(processRead,
                                      [this](std::string &&name, Variant::Root &&info) {
                                          {
                                              std::lock_guard<std::mutex> lock(mutex);
                                              Util::insert_or_assign(interfaceInformation, name,
                                                                     std::move(info));
                                          }
                                          interfaceInformationUpdated(name);
                                      });

    case PacketToClient::InterfaceState:
        return processNameValuePacket(processRead,
                                      [this](std::string &&name, Variant::Root &&state) {
                                          {
                                              std::lock_guard<std::mutex> lock(mutex);
                                              Util::insert_or_assign(interfaceState, name,
                                                                     std::move(state));
                                          }
                                          interfaceStateUpdated(name);
                                      });

    case PacketToClient::RealtimeName: {
        if (processRead.size() < 5U)
            return false;
        auto size = processRead.readNumber<quint32>(1U);
        if (processRead.size() < 5U + size)
            return false;

        {
            auto contents = processRead.mid(5U, size);
            Util::ByteView::Device dev(contents);
            dev.open(QIODevice::ReadOnly);
            QDataStream stream(&dev);
            stream.setByteOrder(QDataStream::LittleEndian);
            stream.setVersion(QDataStream::Qt_4_5);
            SequenceName name;
            stream >> name;

            if (stream.status() == QDataStream::Ok) {
                if (realtimeNameLookup.size() < 0xFFFFU) {
                    realtimeNameLookup.emplace_back(std::move(name));
                } else {
                    realtimeNameLookup[realtimeNameReplace] = std::move(name);
                    realtimeNameReplace = (realtimeNameReplace + 1U) % 0xFFFFU;
                }
            }
        }

        processRead.pop_front(5U + size);
        break;
    }

    case PacketToClient::RealtimeValue: {
        if (processRead.size() < 1U + 2U + 4U)
            return false;
        auto name = processRead.readNumber<quint16>(1U);
        auto size = processRead.readNumber<quint32>(1U + 2U);
        if (processRead.size() < 1U + 2U + 4U + size)
            return false;

        {
            auto contents = processRead.mid(1U + 2U + 4U, size);
            Util::ByteView::Device dev(contents);
            dev.open(QIODevice::ReadOnly);
            QDataStream stream(&dev);
            stream.setByteOrder(QDataStream::LittleEndian);
            stream.setVersion(QDataStream::Qt_4_5);
            auto value = Variant::Root::deserialize(stream);

            if (name < realtimeNameLookup.size() && stream.status() == QDataStream::Ok) {
                std::lock_guard<std::mutex> output(realtimeOutputLock);
                if (realtimeSink) {
                    realtimeSink->emplaceData(realtimeNameLookup[name], std::move(value),
                                              Time::time(), FP::undefined());
                }
            }
        }

        processRead.pop_front(1U + 2U + 4U + size);
        break;
    }

    case PacketToClient::ArchiveValue: {
        if (processRead.size() < 5U)
            return false;
        auto size = processRead.readNumber<quint32>(1U);
        if (processRead.size() < 5U + size)
            return false;

        {
            auto contents = processRead.mid(5U, size);
            Util::ByteView::Device dev(contents);
            dev.open(QIODevice::ReadOnly);
            QDataStream stream(&dev);
            stream.setByteOrder(QDataStream::LittleEndian);
            stream.setVersion(QDataStream::Qt_4_5);
            SequenceValue value;
            stream >> value;

            if (stream.status() == QDataStream::Ok) {
                std::lock_guard<std::mutex> output(archiveReadLock);
                if (archiveSink) {
                    archiveSink->incomingData(std::move(value));
                }
            }
        }

        processRead.pop_front(5U + size);
        break;
    }

    case PacketToClient::ArchiveEnd: {
        processRead.pop_front();

        {
            std::lock_guard<std::mutex> output(archiveReadLock);
            if (archiveSink) {
                archiveSink->endData();
                archiveSink = nullptr;
            }
        }
        notify.notify_all();
        break;
    }

    default:
        qCDebug(log_acquisition_network) << "Invalid packet type received from"
                                         << connection->describePeer() << ", connection closed";
        connection.reset();
        processRead.clear();
        connectionState(false);
        return false;
    }

    return true;
}


AcquisitionTrayCommandHandler::AcquisitionTrayCommandHandler()
{
#ifdef Q_OS_UNIX
    {
        auto runtime = qgetenv("XDG_RUNTIME_DIR");
        if (!runtime.isEmpty()) {
            QFileInfo info(QString::fromUtf8(runtime));
            if (info.isDir() && info.isWritable()) {
                std::unique_ptr<IO::Socket::Unix::Server> raw(new IO::Socket::Unix::Server(
                        std::bind(&AcquisitionTrayCommandHandler::acceptConnection, this,
                                  std::placeholders::_1), runtime.toStdString() +
                                "/CPD3AcquisitionTray-" +
                                Environment::user().toStdString()));
                if (raw->startListening(true)) {
                    server = std::move(raw);
                }
            }
        }
    }
#endif

    if (!server) {
        std::unique_ptr<IO::Socket::Local::Server> raw(new IO::Socket::Local::Server(
                std::bind(&AcquisitionTrayCommandHandler::acceptConnection, this,
                          std::placeholders::_1), "CPD3AcquisitionTray-" + Environment::user()));
        if (raw->startListening(true)) {
            server = std::move(raw);
        }
    }
}

AcquisitionTrayCommandHandler::~AcquisitionTrayCommandHandler() = default;

void AcquisitionTrayCommandHandler::acceptConnection(std::unique_ptr<
        IO::Socket::Connection> &&connection)
{
    struct Context {
        std::unique_ptr<IO::Socket::Connection> connection;
        Threading::Signal<Command> received;

        Context(AcquisitionTrayCommandHandler &parent,
                std::unique_ptr<IO::Socket::Connection> &&connection) : connection(
                std::move(connection)), received(parent.commandReceived)
        { }

        static void run(const std::shared_ptr<Context> &self)
        {
            self->connection->read.connect([self](const Util::ByteArray &data) {
                for (auto cmd : data) {
                    switch (static_cast<Command>(cmd)) {
                    case Command::ShowRealtime:
                        self->received(Command::ShowRealtime);
                        break;
                    default:
                        break;
                    }
                }
            });

            std::mutex mutex;
            std::condition_variable cv;
            bool ended = false;

            self->connection->ended.connect([&] {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    ended = true;
                }
                cv.notify_all();
            });

            self->connection->start();

            {
                std::unique_lock<std::mutex> lock(mutex);
                cv.wait(lock, [&] { return ended; });
            }

            self->connection.reset();
        }
    };

    std::thread(std::bind(&Context::run,
                          std::make_shared<Context>(*this, std::move(connection)))).detach();
}

static std::unique_ptr<IO::Socket::Connection> connectToTray()
{
#ifdef Q_OS_UNIX
    {
        auto runtime = qgetenv("XDG_RUNTIME_DIR");
        if (!runtime.isEmpty()) {
            QFileInfo info(QString::fromUtf8(runtime));
            if (info.isDir() && info.isWritable()) {
                auto socket = IO::Socket::Unix::connect(runtime.toStdString() +
                                                                "/CPD3AcquisitionTray-" +
                                                                Environment::user().toStdString());
                if (socket) {
                    return std::move(socket);
                }
            }
        }
    }
#endif

    return IO::Socket::Local::connect("CPD3AcquisitionTray-" + Environment::user(), {}, true);
}

bool AcquisitionTrayCommandHandler::issueCommand(Command command)
{
    auto socket = connectToTray();
    if (!socket)
        return false;
    socket->start();

    {
        Util::ByteArray packet;
        packet.push_back(static_cast<std::uint8_t>(command));
        socket->write(std::move(packet));
    }
    return true;
}

}
}