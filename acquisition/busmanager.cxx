/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "acquisition/busmanager.hxx"

namespace CPD3 {
namespace Acquisition {

/** @file acquisition/busmanager.hxx
 * A system to manage the arbitration of a bus of instruments.
 */

BusManager::DeviceInterface::DeviceInterface()
{ Q_ASSERT(false); }

BusManager::DeviceInterface::DeviceInterface(BusManager *m) : manager(m),
                                                              device(NULL),
                                                              timeoutTime(FP::undefined()),
                                                              removeRequested(false),
                                                              provisional(false),
                                                              provisionalRequest(FP::undefined()),
                                                              provisionalTime(FP::undefined())
{ }

BusManager::DeviceInterface::~DeviceInterface()
{
    Q_ASSERT(device != NULL);

    manager->removeInterface(this);
}

void BusManager::DeviceInterface::initializeDevice(Device *dev)
{
    device = dev;
}

void BusManager::DeviceInterface::timeoutAt(double time)
{
    timeoutTime = time;
}

void BusManager::DeviceInterface::queueResponse()
{
    manager->responseQueue.append(this);
}

void BusManager::DeviceInterface::receivedResponse()
{
    manager->responseQueue.removeOne(this);
}

void BusManager::DeviceInterface::clearResponseQueue()
{
    manager->responseQueue.removeAll(this);
}

void BusManager::DeviceInterface::removeDevice()
{
    removeRequested = true;
}

void BusManager::DeviceInterface::lockBus()
{
    manager->lockAcquireQueue.append(this);
}

void BusManager::DeviceInterface::unlockBus()
{
    if (manager->activeLock != this)
        return;
    manager->activeLock = NULL;
}

void BusManager::DeviceInterface::setProvisional(double expireTimout)
{
    provisional = true;
    provisionalRequest = expireTimout;
    provisionalTime = FP::undefined();
}

void BusManager::DeviceInterface::setAccepted()
{
    provisional = false;
}

BusManager::Backend::~Backend()
{ }

BusManager::BusManager(Backend *be) : backend(be),
                                      responseQueue(),
                                      lockAcquireQueue(),
                                      activeLock(NULL)
{ }

BusManager::~BusManager()
{
}

BusManager::DeviceInterface *BusManager::createDeviceInterface()
{ return new DeviceInterface(this); }

bool BusManager::advanceRemoveAllRequested(Device *&beginDevice)
{
    Device *dev;
    backend->beginSimpleIteration();
    while ((dev = backend->nextSimpleIteration()) != NULL) {
        DeviceInterface *interface = dev->interface();
        if (interface->removeRequested) {
            Q_ASSERT(interface->device == dev);
            if (dev == beginDevice)
                beginDevice = NULL;
            backend->removeDevice(dev);
            return true;
        }
    }
    return false;
}

void BusManager::advanceProcessAll(double time)
{
    Device *dev;
    backend->beginSimpleIteration();
    while ((dev = backend->nextSimpleIteration()) != NULL) {
        DeviceInterface *interface = dev->interface();
        Q_ASSERT(interface->device == dev);
        if (FP::defined(interface->timeoutTime) && interface->timeoutTime <= time) {
            interface->timeoutTime = FP::undefined();
            dev->incomingTimeout(time);
        }
        if (interface->provisional) {
            if (FP::defined(interface->provisionalRequest)) {
                interface->provisionalTime = time + interface->provisionalRequest;
                interface->provisionalRequest = FP::undefined();
            } else if (FP::defined(interface->provisionalTime) &&
                    interface->provisionalTime < time) {
                interface->provisionalTime = FP::undefined();
                interface->removeDevice();
            }
        }
    }
}

bool BusManager::advanceAction(double time, Device *&beginDevice)
{
    if (activeLock != NULL) {
        activeLock->device->lockHeld(time);
        if (activeLock == NULL)
            return true;
    } else {
        if (!lockAcquireQueue.isEmpty()) {
            activeLock = lockAcquireQueue.takeFirst();
            activeLock->device->lockAcquired(time);
            return true;
        }
    }

    advanceProcessAll(time);
    if (advanceRemoveAllRequested(beginDevice))
        return true;

    if (activeLock == NULL) {
        if (!responseQueue.isEmpty())
            return false;

        Device *controllingDevice = backend->advanceControl();
        if (beginDevice == NULL) {
            beginDevice = controllingDevice;
        } else {
            if (beginDevice == controllingDevice)
                return false;
        }

        if (controllingDevice != NULL) {
            controllingDevice->busControlAcquired(time);
            return true;
        }
    }

    advanceProcessAll(time);
    if (advanceRemoveAllRequested(beginDevice))
        return true;

    return false;
}

void BusManager::advance(double time)
{
    Device *beginDevice = NULL;
    while (advanceAction(time, beginDevice)) { }

    double nextWakeTime = FP::undefined();
    Device *dev;
    backend->beginSimpleIteration();
    while ((dev = backend->nextSimpleIteration()) != NULL) {
        DeviceInterface *interface = dev->interface();
        if (FP::defined(interface->timeoutTime)) {
            if (!FP::defined(nextWakeTime) || nextWakeTime > interface->timeoutTime) {
                nextWakeTime = interface->timeoutTime;
            }
        }
        if (interface->provisional && FP::defined(interface->provisionalTime)) {
            if (!FP::defined(nextWakeTime) || nextWakeTime > interface->provisionalTime) {
                nextWakeTime = interface->provisionalTime;
            }
        }
    }
    backend->timeoutAt(nextWakeTime);
}

BusManager::Device *BusManager::nextResponder() const
{
    if (responseQueue.isEmpty())
        return NULL;
    return responseQueue.front()->device;
}

BusManager::Device *BusManager::busLocker() const
{
    if (activeLock == NULL)
        return NULL;
    return activeLock->device;
}

void BusManager::removeInterface(DeviceInterface *interface)
{
    responseQueue.removeAll(interface);
    lockAcquireQueue.removeAll(interface);
    if (activeLock == interface)
        activeLock = NULL;
}


BusManager::Device::Device()
{ Q_ASSERT(false); }

BusManager::Device::Device(DeviceInterface *interface) : deviceInterface(interface)
{
    deviceInterface->initializeDevice(this);
}

BusManager::Device::~Device()
{
    delete deviceInterface;
}

void BusManager::Device::lockAcquired(double time)
{ Q_UNUSED(time); }

void BusManager::Device::lockHeld(double time)
{ Q_UNUSED(time); }

void BusManager::Device::incomingTimeout(double time)
{ Q_UNUSED(time); }

void BusManager::Device::busControlAcquired(double time)
{ Q_UNUSED(time); }


}
}
