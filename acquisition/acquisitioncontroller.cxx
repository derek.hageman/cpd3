/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>

#ifdef Q_OS_UNIX

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#endif

#include <memory>
#include <QSettings>
#include <QLoggingCategory>

#ifdef Q_OS_UNIX

#include <QDir>

#endif

#include "acquisition/acquisitioncontroller.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/variant/composite.hxx"
#include "database/runlock.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"
#include "core/util.hxx"
#include "core/memory.hxx"


Q_LOGGING_CATEGORY(log_acquisition_controller, "cpd3.acquisition.controller", QtWarningMsg)

Q_LOGGING_CATEGORY(log_acquisition_controller_multiplexer,
                   "cpd3.acquisition.controller.multiplexer", QtWarningMsg)


using namespace CPD3::Data;
using namespace CPD3::Smoothing;


namespace CPD3 {
namespace Acquisition {

std::size_t AcquisitionController::GroupsHash::operator()(const Groups &groups) const
{
    std::size_t result = 0;
    /* Just XOR so order doesn't matter */
    for (const auto &add : groups) {
        result ^= std::hash<Variant::Flag>()(add);
    }
    return result;
}

bool AcquisitionController::groupsOverlap(const Groups &a, const Groups &b)
{
    for (const auto &check : a) {
        if (b.count(check) != 0)
            return true;
    }
    return false;
}

static const SequenceName::Component archiveRaw = "raw";
static const SequenceName::Component archiveRawMeta = "raw_meta";
static const SequenceName::Component archiveRTInstant = "rt_instant";

std::vector<
        AcquisitionController::VariableSetSelection> AcquisitionController::parseVariableSelection(
        const Variant::Read &config) const
{
    std::vector<VariableSetSelection> result;

    for (auto child : config.toChildren()) {
        VariableSetSelection add;
        add.selection = SequenceMatch::Composite(child.hash("Variable"));
        add.groups = child.hash("Groups").toFlags();
        result.emplace_back(std::move(add));
    }

    return result;
}

AcquisitionController::AcquisitionController(const Data::ValueSegment::Transfer &config,
                                             const SequenceName::Component &defaultStation,
                                             bool shutdownWithNoInterfaces) : terminated(false),
                                                                              threadCompleted(
                                                                                      false),
                                                                              serviceRequired(
                                                                                      false),
                                                                              autoprobeRetryTime(
                                                                                      FP::undefined()),
                                                                              config(config),
                                                                              stateBaseName(
                                                                                      "acquisition"),
                                                                              dataUpdateInterval(
                                                                                      0.2),
                                                                              defaultEnableStatistics(
                                                                                      false),
                                                                              defaultEnableRealtimeStatistics(
                                                                                      false),
                                                                              defaultEnableRealtimeDiscard(
                                                                                      false),
                                                                              networkInjectedBypassFlags(),
                                                                              networkInjectedSystemFlags(),
                                                                              realtimeIngress(
                                                                                      *this),
                                                                              realtimeDiscardedIngress(
                                                                                      *this),
                                                                              defaultStation(
                                                                                      defaultStation),
                                                                              shutdownWithNoInterfaces(
                                                                                      shutdownWithNoInterfaces),
                                                                              retryOnNoAutoprobe(
                                                                                      false),
                                                                              scheduledWakeup(
                                                                                      FP::undefined()),
                                                                              lastDataProcess(0),
                                                                              nextHearbeat(0),
                                                                              inShutdown(false),
                                                                              autoprobeStatus(
                                                                                      AutoprobeStatus::NeverSucceeded)
{
    defaultAverageTime.unit = Time::Minute;
    defaultAverageTime.count = 1;
    defaultAverageTime.aligned = true;

    stateBaseName += " ";
    stateBaseName += defaultStation;
    stateBaseName += " ";
    if (!this->config.empty()) {
        auto active = this->config.back().read();

        {
            const auto &test = active.hash("State").toString();
            if (!test.empty()) {
                stateBaseName = test;
                if (stateBaseName.back() != ' ')
                    stateBaseName += " ";
            }
        }
        {
            double test = active.hash("DataUpdateInterval").toDouble();
            if (FP::defined(test))
                dataUpdateInterval = test;
        }

        globalVariableGroupList = parseVariableSelection(active.hash("VariableGroups"));

        for (auto child : active.hash("ComponentGroups").toChildren()) {
            QString component(child.hash("Component").toQString());
            QString source(child.hash("Name").toQString());
            if (component.isEmpty() && source.isEmpty())
                continue;

            InterfaceSetSelection add;
            add.componentNameMatch = QRegularExpression(component);
            add.sourceNameMatch = QRegularExpression(source);
            add.groups = child.hash("Groups").toFlags();
            interfaceSetList.emplace_back(std::move(add));
        }

        defaultEnableStatistics = active.hash("EnableStatistics").toBool();
        if (active.hash("EnableRealtimeStatistics").exists()) {
            defaultEnableRealtimeStatistics = active.hash("EnableRealtimeStatistics").toBool();
        } else {
            defaultEnableRealtimeStatistics = defaultEnableStatistics;
        }
        defaultEnableRealtimeDiscard = active.hash("EnableRealtimeDiscardInstant").toBool();

        retryOnNoAutoprobe = active.hash("Autoprobe").hash("RetryOnEmpty").toBoolean();

        if (active.hash("AveragingInterval").exists()) {
            Variant::Composite::toTimeInterval(active.hash("AveragingInterval"),
                                               &defaultAverageTime.unit, &defaultAverageTime.count,
                                               &defaultAverageTime.aligned);
        }
    }

    metadataProcessing["Revision"].setString(Environment::revision());
}

AcquisitionController::~AcquisitionController()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    request.notify_all();
    if (thread.joinable())
        thread.join();
}

void AcquisitionController::start()
{
    thread = std::thread(std::bind(&AcquisitionController::run, this));
}


void AcquisitionController::startup()
{
    archive.reset(new AcquisitionArchive(
            config.empty() ? Variant::Read::empty() : config.back().read().hash("Archive")));

    qCDebug(log_acquisition_controller) << "Creating networking";
    createNetwork();
    qCDebug(log_acquisition_controller) << "Loading state";
    loadComponentLookup();

    nextFlush = Time::time() + 15 * 60;

    qCDebug(log_acquisition_controller) << "Creating fixed interfaces";
    createFixedInterfaces();
    qCDebug(log_acquisition_controller) << "Initializing autoprobing";
    beginAutoprobe();
    qCDebug(log_acquisition_controller) << "Performing initial update";
    serviceUpdate();

    archive->loggingWritten.connect([this](double time) {
        network->archiveFlush(time);
    });
    network->start();
}

void AcquisitionController::shutdown()
{
    serviceUpdate(true);
    saveComponentLookup();

    autoprobe.clear();

    {
        double time = Time::time();
        for (const auto &interface : active) {
            interface->handleShutdown(time);
        }
    }
    active.clear();

    for (const auto &del : variableSets) {
        VariableSet *set = del.second.get();
        set->loggingMux.sinkCreationComplete();

        if (set->loggingSystemIngress) {
            set->loggingSystemIngress->endData();
            set->loggingSystemIngress = nullptr;
        }
        if (set->realtimeSmoother)
            set->realtimeSmoother->signalTerminate();

#if defined(MULTIPLEXER_UNIT_TRACKING) || defined(MULTIPLEXER_TAG_TRACKING)
        qCDebug(log_acquisition_controller_multiplexer) << "Multiplexer for groups"
                                                        << del.key() << "ending as"
                                                        << set->loggingMux;
#endif
    }

    for (auto &del : variableSets) {
        VariableSet *set = del.second.get();

        set->loggingMux.wait();

        if (set->loggingSmoother) {
            set->loggingSmoother->wait();
            set->loggingSmoother.reset();
        }
        if (set->realtimeSmoother) {
            set->realtimeSmoother->endData();
            set->realtimeSmoother->wait();
            set->realtimeSmoother.reset();
        }
        if (set->loggingArchive) {
            set->loggingArchive.reset();
        }
    }
    variableSets.clear();

    archive->initiateShutdown();
    archive->wait();
    archive->terminateLogging(Time::time() + 6.0 * 3600.0);
    archive.reset();

    network.reset();
}

void AcquisitionController::processLoop()
{
    for (;;) {
        double nextWake = Time::time() + 0.25;
        if (FP::defined(autoprobeRetryTime) && nextWake > autoprobeRetryTime) {
            nextWake = autoprobeRetryTime;
        }
        if (FP::defined(scheduledWakeup) && nextWake > scheduledWakeup) {
            nextWake = scheduledWakeup;
        }
        if (nextWake > nextHearbeat) {
            nextWake = nextHearbeat;
        }
        if (nextWake > nextFlush) {
            nextWake = nextFlush;
        }

        std::vector<std::function<void()>> processCall;
        {
            std::unique_lock<std::mutex> lock(mutex);
            for (;;) {
                if (terminated)
                    return;

                processCall = std::move(call);
                call.clear();

                if (serviceRequired) {
                    serviceRequired = false;
                    break;
                }
                if (!processCall.empty())
                    break;

                double waitTime = nextWake - Time::time();
                if (waitTime <= 0.0)
                    break;
                request.wait_until(lock, std::chrono::steady_clock::now() +
                        std::chrono::duration<double>(waitTime));
            }
        }

        for (const auto &c : processCall) {
            c();
        }

        serviceUpdate();

        auto now = Time::time();
        if (FP::defined(autoprobeRetryTime) && autoprobeRetryTime <= now) {
            autoprobeRetryTime = FP::undefined();
            beginAutoprobe();
        }
        if (nextFlush <= now) {
            nextFlush = now + 15 * 60;
            flushData();
        }
    }
}

void AcquisitionController::asyncCall(std::function<void()> c)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        call.emplace_back(std::move(c));
    }
    request.notify_all();
}

void AcquisitionController::asyncDrain()
{
    Q_ASSERT(!thread.joinable() || thread.get_id() == std::this_thread::get_id());
    std::vector<std::function<void()>> processCall;
    {
        std::lock_guard<std::mutex> lock(mutex);
        processCall = std::move(call);
        call.clear();
    }
    for (const auto &c : processCall) {
        c();
    }
}

void AcquisitionController::run()
{
    qCDebug(log_acquisition_controller) << "Acquisition controller thread started";

    bool enableLockout = config.empty() || !config.back().read().hash("AllowMultiple").toBool();
#ifdef Q_OS_UNIX
    int lockFD = -1;
#endif
    if (enableLockout) {
        qCDebug(log_acquisition_controller) << "Starting exclusive instance check";

#ifdef Q_OS_UNIX
        if (config.empty() || !config.back().read().hash("DisableFileLockout").toBool()) {
            QString fileName(QDir(QDir::tempPath()).filePath("/.CPD3AcquisitionLock"));
            if ((lockFD = ::open(fileName.toUtf8().constData(), O_RDWR | O_CREAT | O_NOFOLLOW,
                                 S_IRWXU | S_IRWXG)) >= 0) {
                if (::lockf(lockFD, F_TLOCK, 0) != 0) {
                    ::close(lockFD);
                    qCInfo(log_acquisition_controller)
                        << "Failed to acquire an exclusive file lock, controller stopping";

                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        threadCompleted = true;
                    }
                    finished();
                    return;
                }
            }
        }
#endif

        {
            Variant::Read cfg = Variant::Read::empty();
            if (!config.empty())
                cfg = config.back().read();
            if (AcquisitionNetworkServer::checkExisting(cfg)) {
                qCInfo(log_acquisition_controller)
                    << "There appears to already be a networked instance running, controller stopping";

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    threadCompleted = true;
                }
                finished();
                return;
            }
        }

        qCDebug(log_acquisition_controller) << "No existing instance detected, startup continuing";
    }

    startup();
    qCDebug(log_acquisition_controller) << "Entering main processing loop";
    controllerReady();
    processLoop();
    qCDebug(log_acquisition_controller) << "Starting cleanup sequence";
    shutdown();

#ifdef Q_OS_UNIX
    if (lockFD >= 0) {
        QString fileName(QDir(QDir::tempPath()).filePath("/.CPD3AcquisitionLock"));
        ::unlink(fileName.toUtf8().constData());
        ::lockf(lockFD, F_ULOCK, 0);
        ::close(lockFD);
    }
#endif

    qCDebug(log_acquisition_controller) << "Acquisition controller thread shutting down";
    {
        std::lock_guard<std::mutex> lock(mutex);
        threadCompleted = true;
    }
    finished();
}

bool AcquisitionController::wait(double timeout)
{
    return finished.wait([this] {
        std::lock_guard<std::mutex> lock(mutex);
        return threadCompleted;
    }, timeout);
}


AcquisitionController::ActiveInterface::ControllerInterface::ControllerInterface(
        AcquisitionController &controller,
        ActiveInterface &interface,
        bool passive) :

        controller(controller), interface(interface), passive(passive)
{ }

AcquisitionController::ActiveInterface::ControllerInterface::~ControllerInterface()
{
    receiver.disconnect();
    /* Since several async calls reference ourselves, make sure none are outstanding before
     * we are deleted */
    controller.asyncDrain();
}

void AcquisitionController::ActiveInterface::ControllerInterface::writeControl(const Util::ByteView &data)
{
    if (passive)
        return;
    if (!interface.io)
        return;
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        interface.controlWrite += data;
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::writeControl(Util::ByteArray &&data)
{
    if (passive)
        return;
    if (!interface.io)
        return;
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        interface.controlWrite += std::move(data);
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::resetControl()
{
    if (passive)
        return;
    if (!interface.io)
        return;
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        interface.controlReset = true;
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::requestTimeout(double time)
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        interface.controlTimeout = time;
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::remap(const SequenceName::Component &name,
                                                                        Variant::Root &value)
{
    /* Don't need to lock the interface mutex as the remap data is only
     * written during construction. */
    auto map = interface.remapData.find(name);
    if (map == interface.remapData.end())
        return;
    map->second->apply(value);
}

void AcquisitionController::ActiveInterface::ControllerInterface::setBypassFlag(const Variant::Flag &flag)
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        if (interface.activeBypassFlags.count(flag) != 0)
            return;
        interface.activeBypassFlags.insert(flag);
        interface.activeBypassFlagsUpdated = true;
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::clearBypassFlag(const Variant::Flag &flag)
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        if (interface.activeBypassFlags.count(flag) == 0)
            return;
        interface.activeBypassFlags.erase(flag);
        interface.activeBypassFlagsUpdated = true;
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::setSystemFlag(const Variant::Flag &flag)
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        if (interface.activeSystemFlags.count(flag) != 0)
            return;
        controller.serviceRequired = true;
        interface.activeSystemFlags.insert(flag);
        interface.activeSystemFlagsUpdated = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::clearSystemFlag(const Variant::Flag &flag)
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        if (interface.activeSystemFlags.count(flag) == 0)
            return;
        interface.activeSystemFlags.erase(flag);
        interface.activeSystemFlagsUpdated = true;
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::requestFlush(double seconds)
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        interface.flushRequestTime = seconds;
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::sendCommand(const std::string &target,
                                                                              const Variant::Read &command)
{
    PendingCommand add;
    add.groups = interface.outputGroups;
    add.target = target;
    add.command = Variant::Root(command);
    {
        std::lock_guard<std::mutex> lock(controller.pendingCommandsMutex);
        controller.pendingCommands.emplace_back(std::move(add));
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::setSystemFlavors(const SequenceName::Flavors &flavors)
{
    SequenceName::Flavors converted;
    for (const auto &f : flavors) {
        converted.insert(Util::to_lower(f));
    }

    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        if (interface.systemFlavors == converted)
            return;
        interface.systemFlavors = std::move(converted);
        interface.systemFlavorsUpdated = true;
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::setAveragingTime(Time::LogicalTimeUnit unit,
                                                                                   int count,
                                                                                   bool aligned)
{
    controller.asyncCall([=] {
        controller.setAveragingTime(interface.outputGroups, unit, count, aligned);
    });
}

void AcquisitionController::ActiveInterface::ControllerInterface::requestStateSave()
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        interface.requestStateSave = true;
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::requestGlobalStateSave()
{
    controller.asyncCall([=] {
        {
            std::lock_guard<std::mutex> lock(controller.mutex);
            for (const auto &interface : controller.active) {
                interface->requestStateSave = true;
                interface->requestPersistentUpdate = true;
            }
        }
        controller.saveComponentLookup();
    });
    controller.flushData();
}

void AcquisitionController::ActiveInterface::ControllerInterface::persistentValuesUpdated()
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        interface.requestPersistentUpdate = true;
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::event(double time,
                                                                        const QString &text,
                                                                        bool showRealtime,
                                                                        const Variant::Read &information)
{
    Variant::Root data;
    auto content = data.write();
    content.hash("Information").set(information);
    content.hash("Text").setString(text);
    content.hash("ShowRealtime").setBool(showRealtime);

    content.hash("Source").setString(interface.sourceName);
    content.hash("Component").setString(interface.componentName);

    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        content.hash("State").hash("LocalSystemFlags").setFlags(interface.activeSystemFlags);
        content.hash("State").hash("LocalBypassFlags").setFlags(interface.activeBypassFlags);
        content.hash("State").hash("LocalFlavors").setFlags(interface.systemFlavors);
        content.hash("State").hash("LocalFlushEndTime").setDouble(interface.flushEndTime);
    }
    controller.asyncCall(std::bind(&AcquisitionController::processEvent, &controller, time,
                                   interface.targetStation, interface.groups, std::move(data)));
}

void AcquisitionController::ActiveInterface::ControllerInterface::realtimeAdvanced(double)
{ }

void AcquisitionController::ActiveInterface::ControllerInterface::loggingHalted(double time)
{
    std::lock_guard<std::mutex> lock(controller.mutex);
    interface.loggingHalt = time;
    interface.loggingLatest = FP::undefined();
    /* Don't schedule a service, since this only matters for other
     * data advancing (i.e. it only has any effect for reducing the backlog).
     * This also saves us from going into a loop when we don't have comms
     * and continually servicing. */
}

void AcquisitionController::ActiveInterface::ControllerInterface::requestDataWakeup(double time)
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        interface.dataWakeup = time;
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::requestControlWakeup(double time)
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        interface.controlWakeup = time;
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::ControllerInterface::interfaceInformationUpdated()
{
    controller.asyncCall([this] {
        /*
         * This can happen during shutdown, since the acquisition is deleted before
         * the interface triggers the drain.
         */
        if (!interface.acquisition)
            return;

        Variant::Write data = Variant::Write::empty();
        data.hash("Source").set(interface.acquisition->getSourceMetadata());
        data.hash("Source").hash("Component").setString(interface.componentName);
        data.hash("Source").hash("Name").setString(interface.sourceName);
        data.hash("Commands").set(interface.getCommands());
        if (interface.io)
            data.hash("Interface").set(interface.io->configuration());

        QString menuName;
        if (!interface.config.empty()) {
            menuName = interface.config.back().getValue().hash("MenuEntry").toDisplayString();
        }
        if (menuName.isEmpty()) {
            menuName = interface.sourceName;

            QString str;
            if (!interface.config.empty()) {
                str = interface.config
                               .back()
                               .getValue()
                               .hash("Source")
                               .hash("Manufacturer")
                               .toDisplayString();
            }
            if (str.isEmpty() && !interface.config.empty()) {
                str = interface.config
                               .back()
                               .getValue()
                               .hash("GlobalMetadata")
                               .metadata("Source")
                               .hash("Manufacturer")
                               .toDisplayString();
            }
            if (str.isEmpty()) {
                str = data.hash("Source").hash("Manufacturer").toDisplayString();
            }
            if (!str.isEmpty()) {
                if (!menuName.isEmpty())
                    menuName.append(QObject::tr(" ", "menu name delimiter"));
                menuName.append(str);
            }

            str.clear();
            if (!interface.config.empty()) {
                str = interface.config
                               .back()
                               .getValue()
                               .hash("Source")
                               .hash("Model")
                               .toDisplayString();
            }
            if (str.isEmpty() && !interface.config.empty()) {
                str = interface.config
                               .back()
                               .getValue()
                               .hash("GlobalMetadata")
                               .metadata("Source")
                               .hash("Model")
                               .toDisplayString();
            }
            if (str.isEmpty()) {
                str = data.hash("Source").hash("Model").toDisplayString();
            }
            if (!str.isEmpty()) {
                if (!menuName.isEmpty())
                    menuName.append(QObject::tr(" ", "menu name delimiter"));
                menuName.append(str);
            }

            str.clear();
            if (!interface.config.empty()) {
                str = interface.config
                               .back()
                               .getValue()
                               .hash("Source")
                               .hash("SerialNumber")
                               .toDisplayString();
            }
            if (str.isEmpty() && !interface.config.empty()) {
                str = interface.config
                               .back()
                               .getValue()
                               .hash("GlobalMetadata")
                               .metadata("Source")
                               .hash("SerialNumber")
                               .toDisplayString();
            }
            if (str.isEmpty()) {
                str = data.hash("Source").hash("SerialNumber").toDisplayString();
            }
            if (!str.isEmpty()) {
                if (!menuName.isEmpty())
                    menuName.append(QObject::tr(" ", "menu name delimiter"));
                menuName.append(QObject::tr("#", "menu name serial number leader"));
                menuName.append(str);
            }
        }
        if (menuName.isEmpty())
            menuName = interface.componentName;
        data.hash("MenuEntry").setString(menuName);
        data.hash("MenuCharacter").setString(interface.menuCharacter);

        QString windowTitle;
        if (!interface.config.empty()) {
            windowTitle = interface.config.back().getValue().hash("WindowTitle").toDisplayString();
            if (interface.config.back().getValue().hash("MenuHide").toBool())
                data.hash("MenuHide").setBool(true);
        }
        if (windowTitle.isEmpty())
            windowTitle = menuName;
        data.hash("WindowTitle").setString(windowTitle);

        controller.network->updateInterfaceInformation(interface.sourceName.toStdString(), data);
    });
}

void AcquisitionController::ActiveInterface::ControllerInterface::interfaceStateUpdated()
{
    controller.asyncCall([this] {
        /*
         * This can happen during shutdown, since the acquisition is deleted before
         * the interface triggers the drain.
         */
        if (!interface.acquisition)
            return;

        Variant::Write data = Variant::Write::empty();

        switch (interface.acquisition->getGeneralStatus()) {
        case AcquisitionInterface::GeneralStatus::Normal:
            data.hash("Status").setString("Normal");
            break;
        case AcquisitionInterface::GeneralStatus::NoCommunications:
            data.hash("Status").setString("NoCommunications");
            break;
        case AcquisitionInterface::GeneralStatus::Disabled:
            data.hash("Status").setString("Disabled");
            break;
        }

        data.hash("SystemFlags").setFlags(interface.previousSystemFlags);
        data.hash("BypassFlags").setFlags(interface.previousBypass);

        controller.network->updateInterfaceState(interface.sourceName.toStdString(), data);
    });
}


AcquisitionController::ActiveInterface::ActiveInterface(AcquisitionController &controller,
                                                        std::unique_ptr<AcquisitionInterface> &&acq,
                                                        std::unique_ptr<IOInterface> &&ioif,
                                                        const ValueSegment::Transfer &configuration,
                                                        bool passive) : controller(controller),
                                                                        controllerInterface(
                                                                                new ControllerInterface(
                                                                                        controller,
                                                                                        *this,
                                                                                        passive)),
                                                                        acquisition(std::move(acq)),
                                                                        io(std::move(ioif)),
                                                                        wasAutoprobed(false),
                                                                        dataWakeup(FP::undefined()),
                                                                        controlWakeup(
                                                                                FP::undefined()),
                                                                        controlTimeout(
                                                                                FP::undefined()),
                                                                        controlReset(false),
                                                                        loggingHalt(
                                                                                FP::undefined()),
                                                                        loggingLatest(
                                                                                FP::undefined()),
#ifdef ACQUISITION_CONTROLLER_LOGGING_TIME_DEBUGGING
                                                                        loggingOrderCheck(
                                                                                FP::undefined()),
#endif
                                                                        systemFlavorsUpdated(false),
                                                                        activeBypassFlagsUpdated(
                                                                                false),
                                                                        activeSystemFlagsUpdated(
                                                                                false),
                                                                        flushRequestTime(
                                                                                FP::undefined()),
                                                                        flushEndTime(
                                                                                FP::undefined()),
                                                                        requestStateSave(false),
                                                                        requestPersistentUpdate(
                                                                                false),
                                                                        config(configuration),
                                                                        defaultFlushTime(
                                                                                FP::undefined()),
                                                                        localLoggingIngress(*this),
                                                                        localRealtimeIngress(*this),
                                                                        localPersistentIngress(
                                                                                *this),
                                                                        loggingEnded(false),
                                                                        targetStation(
                                                                                controller.defaultStation),
                                                                        realtimeDiscard(
                                                                                controller.defaultEnableRealtimeDiscard)
{

    {
        VariableSetSelection add;
        add.selection = acquisition->getExplicitGroupedVariables();
        variableGroupList.emplace_back(std::move(add));
    }

    if (!config.empty()) {
        const auto &active = config.back();

        const auto &check = active.read().hash("Station").toString();
        if (!check.empty())
            targetStation = Util::to_lower(check);

        Util::append(controller.parseVariableSelection(active.read().hash("VariableGroups")),
                     variableGroupList);

        if (active.read().hash("RealtimeDiscardInstant").exists()) {
            realtimeDiscard = active.read().hash("RealtimeDiscardInstant").toBool();
        }

        for (auto child : active.read().hash("Remap").toHash()) {
            if (child.first.empty())
                continue;

            std::unique_ptr<Calibration> cal;
            if (child.second.hash("Calibration").exists()) {
                cal.reset(new Calibration(
                        Variant::Composite::toCalibration(child.second.hash("Calibration"))));
            }
            std::unique_ptr<DynamicInput> input;
            if (child.second.hash("Input").exists()) {
                input.reset(DynamicInput::fromConfiguration(config, QString("Remap/%1/Input").arg(
                        QString::fromStdString(child.first))));
            }
            remapData.emplace(child.first, std::unique_ptr<Remapping>(
                    new Remapping(controller, std::move(input), std::move(cal))));
        }
    }

    Util::append(controller.globalVariableGroupList, variableGroupList);
}

AcquisitionController::ActiveInterface::~ActiveInterface()
{
    if (acquisition) {
        acquisition->signalTerminate();
        acquisition->wait();
        acquisition.reset();
    }
    io.reset();
    controllerInterface.reset();
    for (auto &del : interfaceVariableSets) {
        if (del.second->set) {
            del.second->set->finish();
            del.second->set.reset();
        }
    }
    interfaceVariableSets.clear();
    variableDispatch.clear();
    remapData.clear();
}

/**
 * Handle the shutdown of the interface.  This finishes all data processing.
 * 
 * @param time  the current time
 */
void AcquisitionController::ActiveInterface::handleShutdown(double time)
{
    if (acquisition) {
        acquisition->signalTerminate();
        acquisition->wait();
        acquisition.reset();
    }

    handleSetCreation(time);
    processData(time, true);

    if (io) {
        io.reset();
    }

    std::unordered_map<Groups, std::unique_ptr<InterfaceVariableSet>, GroupsHash> toRelease;
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        toRelease = std::move(interfaceVariableSets);
        interfaceVariableSets.clear();
    }
    for (auto &del : toRelease) {
        if (del.second->set) {
            del.second->set->finish();
            del.second->set.reset();
        }
    }
}

template<typename K, typename V, typename HashType>
static std::unordered_map<K, V *, HashType> pointerCopy(const std::unordered_map<K,
                                                                                 std::unique_ptr<V>,
                                                                                 HashType> &input)
{
    std::unordered_map<K, V *, HashType> result;
    for (const auto &add : input) {
        result.emplace(add.first, add.second.get());
    }
    return result;
}

/**
 * Handle creation of the variable overlay sets for any that where created
 * by calls in the interface thread handler, but still require overlay
 * creation.
 * 
 * @param time  the current time
 */
void AcquisitionController::ActiveInterface::handleSetCreation(double time)
{
    std::unique_lock<std::mutex> lock(controller.mutex);
    for (const auto &set : pointerCopy(interfaceVariableSets)) {
        if (set.second->set)
            continue;

        auto &vs = controller.getVariableSet(set.first);
        Variant::Root processing(controller.metadataProcessing);
        processing["AcquisitionGroups"].setFlags(set.first);
        Variant::Root global;
        global.write().metadata("Source").hash("Component").setString(componentName);
        global.write().metadata("Source").hash("Name").setString(sourceName);
        if (io) {
            global.write().metadata("Source").hash("Interface").setString(io->description());
        }
        std::unique_ptr<AcquisitionVariableSet> avs
                (new AcquisitionVariableSet(&vs.loggingMux, config, processing, global,
                                            sourceName));

        lock.unlock();
        double setFlushEndTime = controller.getFlushEndTime(set.first);
        avs->setFlavors(controller.getSystemFlavors(set.first));
        avs->setSystemFlags(controller.getSystemFlags(set.first));
        avs->setFlushUntil(flushEndTime);
        avs->setBypassed(!controller.getBypassFlags(set.first).empty());
        avs->setPossibleSystemFlags(controller.getSystemFlagsMetadata(set.first));
        avs->setRealtimeEgress(vs.realtimeSmoother.get());
        if (!realtimeDiscard) {
            avs->setRealtimeDiscardEgress(&controller.realtimeDiscardedIngress);
        }
        if (FP::defined(time)) {
            /* Process nothing so we set the advance time and can't move 
             * backwards: an interface that is already halted would
             * send a halt time potentially behind the current time, which
             * would cause the newly created multiplexer outputs to try
             * to go backwards (since they are created at the current time) */
            avs->process(time, SequenceValue::Transfer(), SequenceValue::Transfer(), time);
        }
        lock.lock();

        set.second->set = std::move(avs);
        set.second->loggingBeginTime = time;
        if (!FP::defined(set.second->flushEndTime) ||
                (FP::defined(setFlushEndTime) && setFlushEndTime > set.second->flushEndTime)) {
            set.second->flushEndTime = setFlushEndTime;
        }

        /* If this is processing values after the end of logging (e.x.
         * realtime only), then make sure to mark it as such */
        if (loggingEnded) {
            set.second->loggingEnded = true;
        }
    }
}

/**
 * Handle newly available realtime units.  This registers those units with
 * the remappings.
 */
void AcquisitionController::ActiveInterface::handleNewUnits(const QSet<SequenceName> &units) const
{
    for (const auto &service : remapData) {
        service.second->handleNewUnits(units);
    }
}

/**
 * Advance the data of the backend interface to the current time.  This
 * calls into the backend for the current time to allow it to process data.
 * 
 * @param time      the current time
 * @param processIO true to actually process IO data instead of just advancing
 * @return          the next requested wake time
 */
double AcquisitionController::ActiveInterface::advanceData(double time, bool processIO)
{
    handleSetCreation(time);

    if (io && processIO) {
        AcquisitionInterface::IncomingDataType
                type = AcquisitionInterface::IncomingDataType::Stream;
        if (io->integratedFraming())
            type = AcquisitionInterface::IncomingDataType::Complete;

        std::unique_lock<std::mutex> lock(controller.mutex);
        auto process = std::move(pendingData);
        pendingData.clear();
        if (!process.empty()) {
            lock.unlock();
            for (auto &d : process) {
                acquisition->incomingData(std::move(d), time, type);
            }
        } else {
            if (FP::defined(dataWakeup) && dataWakeup <= time) {
                dataWakeup = FP::undefined();
                lock.unlock();
                acquisition->incomingData(Util::ByteView(), time);
            } else if (FP::defined(controlTimeout) && controlTimeout <= time) {
                controlTimeout = FP::undefined();
                lock.unlock();
                acquisition->incomingTimeout(time);
            }
        }

        if (!lock)
            lock.lock();
        process = std::move(pendingControl);
        pendingControl.clear();
        if (!process.empty()) {
            lock.unlock();
            for (auto &d : process) {
                acquisition->incomingControl(std::move(d), time, type);
            }
        } else {
            if (FP::defined(controlWakeup) && controlWakeup <= time) {
                controlWakeup = FP::undefined();
                lock.unlock();
                acquisition->incomingControl(Util::ByteView(), time);
            }
        }

        bool doReset = false;
        if (!lock)
            lock.lock();
        doReset = controlReset;
        controlReset = false;
        auto write = std::move(controlWrite);
        controlWrite.clear();

        lock.unlock();
        if (doReset) {
            io->reopen();
        } else if (!write.empty()) {
            io->write(std::move(write));
        }
    } else {
        std::unique_lock<std::mutex> lock(controller.mutex);
        if (FP::defined(dataWakeup) && dataWakeup <= time) {
            dataWakeup = FP::undefined();
            lock.unlock();
            acquisition->incomingData(Util::ByteView(), time);
            lock.lock();
        } else if (FP::defined(controlTimeout) && controlTimeout <= time) {
            controlTimeout = FP::undefined();
            lock.unlock();
            acquisition->incomingTimeout(time);
            lock.lock();
        }

        if (FP::defined(controlWakeup) && controlWakeup <= time) {
            controlWakeup = FP::undefined();
            lock.unlock();
            acquisition->incomingControl(Util::ByteView(), time);
        }
    }

    acquisition->incomingAdvance(time);

    std::lock_guard<std::mutex> lock(controller.mutex);
    double resultTime = controlTimeout;
    if (FP::defined(dataWakeup) && (!FP::defined(resultTime) || dataWakeup < resultTime))
        resultTime = dataWakeup;
    if (FP::defined(controlWakeup) && (!FP::defined(resultTime) || controlWakeup < resultTime))
        resultTime = controlWakeup;
    return resultTime;
}

/**
 * Advance the system state for the given time.  This sends the updated
 * state of the interface to all affected variable overlays.
 * 
 * @param time  the current time
 */
void AcquisitionController::ActiveInterface::advanceState(double time)
{
    std::unique_lock<std::mutex> lock(controller.mutex);

    if (requestStateSave) {
        requestStateSave = false;
        lock.unlock();
        saveState();
        lock.lock();
    }

    if (requestPersistentUpdate) {
        requestPersistentUpdate = false;
        lock.unlock();
        writePersistentValues();
        lock.lock();
    }

    if (FP::defined(flushRequestTime)) {
        if (flushRequestTime <= 0.0)
            flushRequestTime = defaultFlushTime;
        double proposedTime = time + flushRequestTime;

        if (!FP::defined(flushEndTime) || proposedTime > flushEndTime) {
            flushEndTime = proposedTime;
            double requestTime = flushRequestTime;
            flushRequestTime = FP::undefined();
            qCDebug(log_acquisition_controller) << "Interface" << sourceName << "issued"
                                                << requestTime << "second flush ending at"
                                                << Logging::time(flushEndTime);

            requestTime = flushEndTime;
            lock.unlock();
            for (const auto &target : controller.active) {
                target->issueFlush(outputGroups, requestTime);
            }
            lock.lock();
        }
    }

    if (systemFlavorsUpdated) {
        systemFlavorsUpdated = false;

        qCDebug(log_acquisition_controller) << "Interface" << sourceName
                                            << "changed system flavors to" << systemFlavors;

        lock.unlock();
        std::unordered_map<Groups, SequenceName::Flavors, GroupsHash> lookup;
        for (const auto &target : controller.active) {
            target->updateSystemFlavors(outputGroups, lookup);
        }
        lock.lock();
    }

    if (activeBypassFlagsUpdated) {
        activeBypassFlagsUpdated = false;

        qCDebug(log_acquisition_controller) << "Interface" << sourceName
                                            << "changed bypass flags to:" << activeBypassFlags;

        lock.unlock();
        std::unordered_map<Groups, Variant::Flags, GroupsHash> lookup;
        for (const auto &target : controller.active) {
            target->updateBypass(outputGroups, lookup);
        }
        lock.lock();
    }

    if (activeSystemFlagsUpdated) {
        activeSystemFlagsUpdated = false;

        qCDebug(log_acquisition_controller) << "Interface" << sourceName
                                            << "changed system flags to:" << activeSystemFlags;

        lock.unlock();
        std::unordered_map<Groups, Variant::Flags, GroupsHash> lookup;
        for (const auto &target : controller.active) {
            target->updateSystemFlags(outputGroups, lookup);
        }
        lock.lock();
    }
}

AcquisitionController::ActiveInterface::InterfaceSetProcessData::InterfaceSetProcessData() : set(
        nullptr), loggingAdvanceTime(FP::undefined()), loggingEnded(false)
{ }

AcquisitionController::ActiveInterface::InterfaceSetProcessData::InterfaceSetProcessData(
        InterfaceVariableSet *target,
        double advance) : set(target),
                          loggingAdvanceTime(advance),
                          loggingEnded(target->loggingEnded)
{ }

/**
 * Process all pending data for the given time.  This causes data to be sent
 * to the global variable sets.
 * 
 * @param time  the current time
 * @param inShutdown true if this is a processing during shutdown (no further data)
 */
void AcquisitionController::ActiveInterface::processData(double time, bool inShutdown)
{
    using std::swap;
    std::vector<InterfaceSetProcessData> processing;
    {
        std::lock_guard<std::mutex> lock(controller.mutex);

        double processHalt = loggingHalt;
        double processLoggingAdvance = loggingLatest;
        loggingLatest = FP::undefined();

        Q_ASSERT(!FP::defined(processHalt) ||
                         (FP::defined(processHalt) && !FP::defined(processLoggingAdvance)));
        Q_ASSERT(!FP::defined(processLoggingAdvance) ||
                         (FP::defined(processLoggingAdvance) && !FP::defined(processHalt)));

        for (const auto &set : interfaceVariableSets) {
            InterfaceVariableSet *target = set.second.get();
            if (!target->set)
                continue;

            processing.emplace_back(target, processLoggingAdvance);
            InterfaceSetProcessData &p = processing.back();

            Q_ASSERT(FP::defined(target->loggingBeginTime) ||
                             target->pendingLoggingData.empty() ||
                             !FP::defined(p.loggingAdvanceTime) ||
                             Range::compareStart(p.loggingAdvanceTime,
                                                 target->pendingLoggingData.back().getStart()) >=
                                     0);

            /* If we're halting, make sure we don't try anything before the
             * halt until we get something after it. */
            if (FP::defined(processHalt)) {
                /* However, only do this if we haven't already advanced through
                 * the halt anyway */
                if (Range::compareStart(processHalt, p.loggingAdvanceTime) >= 0) {
                    p.loggingAdvanceTime = processHalt;

                    /* May have an initial halt that comes before the logging connection discard,
                     * so make sure not to set this backwards */
                    if (!FP::defined(target->loggingBeginTime) ||
                            processHalt > target->loggingBeginTime)
                        target->loggingBeginTime = processHalt;
                }
            }

            /* Clean up the incoming to handle the creation/multiplexer time
             * discrepancy. */
            if (!FP::defined(target->loggingBeginTime)) {
                /* Already far enough, so just pass through */
                swap(p.processLogging, target->pendingLoggingData);
            } else if (!target->pendingLoggingData.empty()) {
                /* Remove only only values that cross the boundary if they're
                 * finite time (non-metadata in general).  Stop once we have
                 * a value that's completely past the discard time.  We don't
                 * consider the halt so we allow for an advance to after it when
                 * resuming.  The begin time will already have been set to the
                 * last halt passed through, so we'll throw away what we must
                 * anyway. */

                double checkTime = target->loggingBeginTime;
                bool hit = false;
                for (auto check = target->pendingLoggingData.begin();
                        check != target->pendingLoggingData.end();) {
                    if (!FP::defined(check->getEnd())) {
                        if (Range::compareStart(check->getStart(), checkTime) < 0) {
                            check->setStart(checkTime);
                        }
                        ++check;
                        continue;
                    }
                    if (!FP::defined(check->getStart()) || check->getStart() < checkTime) {
                        check = target->pendingLoggingData.erase(check);
                        continue;
                    }

                    hit = true;
                    break;
                }

                /* Another stream may have advanced past the begin, so count that as complete
                 * even if we haven't seen the any of our own values yet */
                if (FP::defined(processLoggingAdvance) && processLoggingAdvance >= checkTime)
                    hit = true;

                /* If we got anything past the boundary or if we're in shutdown
                 * (all metadata) then process and flag for normal operation
                 * (which isn't anything if this is a shutdown) */
                if (hit || inShutdown) {
                    target->loggingBeginTime = FP::undefined();
                    swap(p.processLogging, target->pendingLoggingData);
                }
            }

            Q_ASSERT(p.processLogging.empty() ||
                             !FP::defined(p.loggingAdvanceTime) ||
                             Range::compareStart(p.loggingAdvanceTime,
                                                 p.processLogging.back().getStart()) >= 0);

#ifdef ACQUISITION_CONTROLLER_LOGGING_TIME_DEBUGGING
            Q_ASSERT(!FP::defined(p.loggingAdvanceTime) ||
                             Range::compareStart(p.loggingAdvanceTime,
                                                 target->loggingLastAdvance) >= 0);
            Q_ASSERT(p.processLogging.empty() ||
                             Range::compareStart(p.processLogging.back().getStart(),
                                                 target->loggingLastAdvance) >= 0);
            if (FP::defined(p.loggingAdvanceTime)) {
                target->loggingLastAdvance = p.loggingAdvanceTime;
            } else if (!p.processLogging.empty()) {
                target->loggingLastAdvance = p.processLogging.back().getStart();
            }
#endif

            swap(target->pendingRealtimeData, p.processRealtime);
        }
    }

    /* Now do the actual processing outside of the lock; this has to be a single step, since
     * the above makes assumptions about the state of the logging advance, which is shared
     * between sets.  So it can't unlock during processing. */
    if (!processing.empty()) {
        for (const auto &p : processing) {
            p.set->set->process(time, p.processLogging, p.processRealtime, p.loggingAdvanceTime);
            if (p.loggingEnded)
                p.set->set->finish(false);
        }
    }
}

/**
 * Save the state of the interface.
 */
void AcquisitionController::ActiveInterface::saveState()
{
    QByteArray buffer;
    {
        QDataStream stream(&buffer, QIODevice::WriteOnly);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);
        stream << componentName;
        acquisition->serializeState(stream);
    }

    auto keyName = QString::fromStdString(controller.stateBaseName);
    keyName += sourceName;

    controller.archive->saveState(std::move(keyName), std::move(buffer));
}

/**
 * Restore the saved of the interface.
 */
void AcquisitionController::ActiveInterface::restoreState()
{
    auto keyName = QString::fromStdString(controller.stateBaseName);
    keyName += sourceName;

    /* This has to be done in blocking context, since we need it immediately
     * before we log data from the instrument.  So we'll just have to accept
     * that it may cause spurious timeouts during late-phase startup.  Those
     * are mostly due to flush data intersections, so they should be even
     * more rare anyway. */

    QByteArray buffer = Database::RunLock().blindGet(keyName);
    if (buffer.isEmpty()) {
        acquisition->initializeState();
        return;
    }

    QDataStream stream(&buffer, QIODevice::ReadOnly);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setVersion(QDataStream::Qt_4_5);
    QString checkName;
    stream >> checkName;
    if (checkName != componentName) {
        acquisition->initializeState();
        return;
    }
    acquisition->deserializeState(stream);
}

/**
 * Write the persistent values of the interface.
 */
void AcquisitionController::ActiveInterface::writePersistentValues()
{
    handlePersistent(acquisition->getPersistentValues());
}

/**
 * Issue a new flush end time to all affected groups.
 * 
 * @param groups    the affected groups
 * @param endTime   the end time of the new flush
 */
void AcquisitionController::ActiveInterface::issueFlush(const Groups &groups, double endTime)
{
    std::lock_guard<std::mutex> lock(controller.mutex);
    for (const auto &set : interfaceVariableSets) {
        if (!groupsOverlap(groups, set.first))
            continue;
        InterfaceVariableSet *target = set.second.get();
        if (FP::defined(target->flushEndTime) && endTime <= target->flushEndTime)
            continue;

        target->flushEndTime = endTime;

        if (!target->set)
            continue;
        target->set->setFlushUntil(endTime);
    }
}

/**
 * Issue a flush for all variables.
 * 
 * @param endTime   the end time of the new flush
 */
void AcquisitionController::ActiveInterface::issueGlobalFlush(double seconds)
{
    if (!FP::defined(seconds) || seconds <= 0.0)
        seconds = defaultFlushTime;
    seconds += Time::time();

    std::lock_guard<std::mutex> lock(controller.mutex);
    if (!FP::defined(flushEndTime) || seconds > flushEndTime)
        flushEndTime = seconds;

    for (const auto &set : interfaceVariableSets) {
        InterfaceVariableSet *target = set.second.get();
        if (FP::defined(target->flushEndTime) && seconds <= target->flushEndTime)
            continue;

        target->flushEndTime = seconds;

        if (!target->set)
            continue;
        target->set->setFlushUntil(seconds);
    }
}

/**
 * Notify the interface that global averaging has changed.
 *
 * @param groups    the affected groups
 */
void AcquisitionController::ActiveInterface::averagingChanged(const Groups &groups)
{
    std::lock_guard<std::mutex> lock(controller.mutex);
    for (const auto &set : interfaceVariableSets) {
        if (!groupsOverlap(groups, set.first))
            continue;
        InterfaceVariableSet *target = set.second.get();
        if (!target->set)
            continue;
        target->set->resendLoggingMetadata();
    }
}

/**
 * Notify the interface that global averaging has changed.
 */
void AcquisitionController::ActiveInterface::averagingChanged()
{
    std::lock_guard<std::mutex> lock(controller.mutex);
    for (const auto &set : interfaceVariableSets) {
        InterfaceVariableSet *target = set.second.get();
        if (!target->set)
            continue;
        target->set->resendLoggingMetadata();
    }
}

/**
 * Get the end time of the active flush for the given group, if any.
 * 
 * @param group     the group to calculate
 * @return          the end of the flush or undefined if none is active
 */
double AcquisitionController::getFlushEndTime(const Groups &group) const
{
    double endTime = FP::undefined();
    for (const auto &interface : active) {
        if (!groupsOverlap(group, interface->outputGroups))
            continue;
        std::lock_guard<std::mutex> lock(const_cast<AcquisitionController *>(this)->mutex);
        if (!FP::defined(endTime) || endTime < interface->flushEndTime)
            endTime = interface->flushEndTime;
    }
    return endTime;
}

/**
 * Calculate the active flavors for the given group.
 * 
 * @param group     the group to calculate
 * @return          the flavors
 */
SequenceName::Flavors AcquisitionController::getSystemFlavors(const Groups &group) const
{
    SequenceName::Flavors result;
    for (const auto &interface : active) {
        if (!groupsOverlap(group, interface->outputGroups))
            continue;
        std::lock_guard<std::mutex> lock(const_cast<AcquisitionController *>(this)->mutex);
        Util::merge(interface->systemFlavors, result);
    }
    return result;
}

/**
 * Calculate the active bypass flags for the given group.
 * 
 * @param group     the group to calculate
 * @return          the bypass flags
 */
Data::Variant::Flags AcquisitionController::getBypassFlags(const Groups &group) const
{
    Variant::Flags result = networkInjectedBypassFlags;
    for (const auto &interface : active) {
        if (!groupsOverlap(group, interface->outputGroups))
            continue;
        std::lock_guard<std::mutex> lock(const_cast<AcquisitionController *>(this)->mutex);
        Util::merge(interface->activeBypassFlags, result);
    }
    return result;
}

/**
 * Calculate the active system flags for the given group.
 * 
 * @param group     the group to calculate
 * @return          the system flags
 */
Data::Variant::Flags AcquisitionController::getSystemFlags(const Groups &group) const
{
    Variant::Flags result = networkInjectedSystemFlags;
    for (const auto &interface : active) {
        if (!groupsOverlap(group, interface->outputGroups))
            continue;
        std::lock_guard<std::mutex> lock(const_cast<AcquisitionController *>(this)->mutex);
        Util::merge(interface->activeSystemFlags, result);
    }
    return result;
}

/**
 * Calculate the system flags metadata for the given group.
 * 
 * @param group     the group to calculate
 * @return          the metadata
 */
Variant::Root AcquisitionController::getSystemFlagsMetadata(const Groups &group) const
{
    std::vector<Variant::Root> merge;
    for (const auto &interface : active) {
        if (!groupsOverlap(group, interface->outputGroups))
            continue;
        auto add = interface->acquisition->getSystemFlagsMetadata();
        if (!add.read().exists())
            continue;
        merge.emplace_back(std::move(add));
    }
    return Variant::Root::overlay(merge.begin(), merge.end());
}

/**
 * Update the flavors for all variable sets in the interface.
 * 
 * @param groups    the affected groups
 * @param lookup    the lookup for already calculated data
 */
void AcquisitionController::ActiveInterface::updateSystemFlavors(const Groups &groups,
                                                                 std::unordered_map<Groups,
                                                                                    SequenceName::Flavors,
                                                                                    GroupsHash> &lookup)
{
    /* Use a copy of the sets so we can unlock the mutex during processing */
    std::unique_lock<std::mutex> lock(controller.mutex);
    for (const auto &set : pointerCopy(interfaceVariableSets)) {
        if (!groupsOverlap(groups, set.first))
            continue;
        InterfaceVariableSet *target = set.second;
        if (!target->set)
            continue;

        auto check = lookup.find(set.first);
        if (check == lookup.end()) {
            lock.unlock();
            check = lookup.emplace(set.first, controller.getSystemFlavors(set.first)).first;
            lock.lock();
        }

        if (!target->set)
            continue;
        target->set->setFlavors(check->second);
    }
}


/**
 * Update the bypass status for all variable sets in the interface.
 * 
 * @param groups    the affected groups
 * @param lookup    the lookup for already calculated data
 */
void AcquisitionController::ActiveInterface::updateBypass(const Groups &groups,
                                                          std::unordered_map<Groups,
                                                                             Data::Variant::Flags,
                                                                             GroupsHash> &lookup)
{
    /* Use a copy of the sets so we can unlock the mutex during processing */
    {
        std::unique_lock<std::mutex> lock(controller.mutex);
        for (const auto &set : pointerCopy(interfaceVariableSets)) {
            if (!groupsOverlap(groups, set.first))
                continue;
            InterfaceVariableSet *target = set.second;
            if (!target->set)
                continue;

            auto check = lookup.find(set.first);
            if (check == lookup.end()) {
                lock.unlock();
                check = lookup.emplace(set.first, controller.getBypassFlags(set.first)).first;
                lock.lock();
            }

            target->set->setBypassed(!check->second.empty());
        }

        if (!groupsOverlap(groups, this->groups))
            return;
    }

    auto check = lookup.find(this->groups);
    if (check == lookup.end()) {
        check = lookup.emplace(this->groups, controller.getBypassFlags(this->groups)).first;
    }

    if (check->second == previousBypass)
        return;
    if (check->second.empty() == previousBypass.empty()) {
        previousBypass = check->second;
        controllerInterface->interfaceStateUpdated();
        return;
    }
    previousBypass = check->second;

    Variant::Root command;
    if (!check->second.empty()) {
        command["Bypass"].setBool(true);
    } else {
        command["UnBypass"].setBool(true);
    }
    acquisition->incomingCommand(command);

    controllerInterface->interfaceStateUpdated();
}

/**
 * Update the bypass status for all variable sets.
 * 
 * @param lookup    the lookup for already calculated data
 */
void AcquisitionController::ActiveInterface::updateGlobalBypass(std::unordered_map<Groups,
                                                                                   Data::Variant::Flags,
                                                                                   GroupsHash> &lookup)
{
    /* Use a copy of the sets so we can unlock the mutex during processing */
    {
        std::unique_lock<std::mutex> lock(controller.mutex);
        for (const auto &set : pointerCopy(interfaceVariableSets)) {
            InterfaceVariableSet *target = set.second;
            if (!target->set)
                continue;

            auto check = lookup.find(set.first);
            if (check == lookup.end()) {
                lock.unlock();
                check = lookup.emplace(set.first, controller.getBypassFlags(set.first)).first;
                lock.lock();
            }

            target->set->setBypassed(!check->second.empty());
        }
    }

    auto check = lookup.find(this->groups);
    if (check == lookup.end()) {
        check = lookup.emplace(this->groups, controller.getBypassFlags(this->groups)).first;
    }

    if (check->second == previousBypass)
        return;
    if (check->second.empty() == previousBypass.empty()) {
        previousBypass = check->second;
        controllerInterface->interfaceStateUpdated();
        return;
    }
    previousBypass = check->second;

    Variant::Root command;
    if (!check->second.empty()) {
        command["Bypass"].setBool(true);
    } else {
        command["UnBypass"].setBool(true);
    }
    acquisition->incomingCommand(command);

    controllerInterface->interfaceStateUpdated();
}

/**
 * Update the active system flags for all variable sets in the interface.
 * 
 * @param groups    the affected groups
 * @param lookup    the lookup for already calculated data
 */
void AcquisitionController::ActiveInterface::updateSystemFlags(const Groups &groups,
                                                               std::unordered_map<Groups,
                                                                                  Data::Variant::Flags,
                                                                                  GroupsHash> &lookup)
{
    /* Use a copy of the sets so we can unlock the mutex during processing */
    {
        std::unique_lock<std::mutex> lock(controller.mutex);
        for (const auto &set : pointerCopy(interfaceVariableSets)) {
            if (!groupsOverlap(groups, set.first))
                continue;
            InterfaceVariableSet *target = set.second;
            if (!target->set)
                continue;

            auto check = lookup.find(set.first);
            if (check == lookup.end()) {
                lock.unlock();
                check = lookup.emplace(set.first, controller.getSystemFlags(set.first)).first;
                lock.lock();
            }

            target->set->setSystemFlags(check->second);
        }

        if (!groupsOverlap(groups, this->groups))
            return;
    }

    auto check = lookup.find(this->groups);
    if (check == lookup.end()) {
        check = lookup.emplace(this->groups, controller.getSystemFlags(this->groups)).first;
    }

    if (check->second == previousSystemFlags)
        return;

    previousSystemFlags = check->second;
    controllerInterface->interfaceStateUpdated();
}

/**
 * Update the active system flags for all variable sets.
 * 
 * @param lookup    the lookup for already calculated data
 */
void AcquisitionController::ActiveInterface::updateGlobalSystemFlags(std::unordered_map<Groups,
                                                                                        Data::Variant::Flags,
                                                                                        GroupsHash> &lookup)
{
    /* Use a copy of the sets so we can unlock the mutex during processing */
    {
        std::unique_lock<std::mutex> lock(controller.mutex);
        for (const auto &set : pointerCopy(interfaceVariableSets)) {
            InterfaceVariableSet *target = set.second;
            if (!target->set)
                continue;

            auto check = lookup.find(set.first);
            if (check == lookup.end()) {
                lock.unlock();
                check = lookup.emplace(set.first, controller.getSystemFlags(set.first)).first;
                lock.lock();
            }

            target->set->setSystemFlags(check->second);
        }
    }

    auto check = lookup.find(this->groups);
    if (check == lookup.end()) {
        check = lookup.emplace(this->groups, controller.getSystemFlags(this->groups)).first;
    }

    if (check->second == previousSystemFlags)
        return;

    previousSystemFlags = check->second;
    controllerInterface->interfaceStateUpdated();
}

/**
 * Update the system flags metadata for all variable sets in the interface.
 * 
 * @param groups    the affected groups
 * @param lookup    the lookup for already calculated data
 */
void AcquisitionController::ActiveInterface::updateSystemFlagsMetadata(const Groups &groups,
                                                                       std::unordered_map<Groups,
                                                                                          Data::Variant::Root,
                                                                                          GroupsHash> &lookup)
{
    /* Use a copy of the sets so we can unlock the mutex during processing */
    std::unique_lock<std::mutex> lock(controller.mutex);
    for (const auto &set : pointerCopy(interfaceVariableSets)) {
        if (!groupsOverlap(groups, set.first))
            continue;
        InterfaceVariableSet *target = set.second;
        if (!target->set)
            continue;

        auto check = lookup.find(set.first);
        if (check == lookup.end()) {
            lock.unlock();
            check = lookup.emplace(set.first, controller.getSystemFlagsMetadata(set.first)).first;
            lock.lock();
        }

        target->set->setPossibleSystemFlags(check->second);
    }
}

/**
 * Test if the interface has completed.  This handles all cleanup if it has
 * finished.
 *
 * @param time  the current time
 * @return      true if the interface has completed
 */
bool AcquisitionController::ActiveInterface::checkCompleted(double time)
{
    if (!acquisition)
        return true;
    if (!acquisition->isCompleted())
        return false;

    saveState();
    writePersistentValues();

    acquisition->wait();
    acquisition.reset();

    io.reset();
    controllerInterface.reset();

    handleShutdown(time);
    return true;
}

/**
 * Get the commands for the acquisition interface.  This includes transformation
 * of the variables they reference.
 *
 * @return the transformed command data
 */
Variant::Root AcquisitionController::ActiveInterface::getCommands()
{
    auto commands = acquisition->getCommands();

    for (auto command : commands.write().toHash()) {
        if (command.second.hash("Include").exists()) {
            for (auto child : command.second.hash("Include").toArray()) {
                QString variable(child.hash("Variable").toQString());
                if (!variable.isEmpty()) {
                    if (!variable.contains('_')) {
                        variable.append('_');
                        variable.append(sourceName);
                        child.hash("Variable").setString(variable);
                        child.hash("Station").setString(targetStation);
                        child.hash("Archive").setString(archiveRTInstant);
                    } else {
                        if (child.hash("Station").toString().empty())
                            child.hash("Station").setString(targetStation);
                        if (child.hash("Archive").toString().empty())
                            child.hash("Archive").setString(archiveRTInstant);
                    }
                }
            }
        }

        if (command.second.hash("Exclude").exists()) {
            for (auto child : command.second.hash("Exclude").toArray()) {
                QString variable(child.hash("Variable").toQString());
                if (!variable.isEmpty()) {
                    if (!variable.contains('_')) {
                        variable.append('_');
                        variable.append(sourceName);
                        child.hash("Variable").setString(variable);
                        child.hash("Station").setString(targetStation);
                        child.hash("Archive").setString(archiveRTInstant);
                    } else {
                        if (child.hash("Station").toString().empty())
                            child.hash("Station").setString(targetStation);
                        if (child.hash("Archive").toString().empty())
                            child.hash("Archive").setString(archiveRTInstant);
                    }
                }
            }
        }

        if (command.second.hash("Parameters").exists()) {
            for (auto child : command.second.hash("Parameters").toHash()) {
                if (child.first.empty())
                    continue;
                QString variable(child.second.hash("Variable").toQString());
                if (!variable.isEmpty()) {
                    if (!variable.contains('_')) {
                        variable.append('_');
                        variable.append(sourceName);
                        child.second.hash("Variable").setString(variable);
                        child.second.hash("Station").setString(targetStation);
                        child.second.hash("Archive").setString(archiveRTInstant);
                    } else {
                        if (child.second.hash("Station").toString().empty()) {
                            child.second.hash("Station").setString(targetStation);
                        }
                        if (child.second.hash("Archive").toString().empty()) {
                            child.second.hash("Archive").setString(archiveRTInstant);
                        }
                    }
                }
            }
        }
    }

    return commands;
}

/**
 * Connect the interface data streams so that incoming data can be handled.
 *
 * @param startInterface    also start the interface
 */
void AcquisitionController::ActiveInterface::connectInterfaceData(bool startInterface)
{
    if (!io)
        return;

    io->read.connect([this](const Util::ByteArray &data) {
        {
            std::lock_guard<std::mutex> lock(controller.mutex);
            pendingData.emplace_back(data);
            controller.serviceRequired = true;
        }
        controller.request.notify_all();
    });
    io->otherControl.connect([this](const Util::ByteArray &data) {
        {
            std::lock_guard<std::mutex> lock(controller.mutex);
            pendingControl.emplace_back(data);
            controller.serviceRequired = true;
        }
        controller.request.notify_all();
    });

    if (startInterface)
        io->start();
}

/**
 * Get the target dispatch for a given variable unit.  This will create
 * the dispatch if it does not exist.
 *
 * @param unit  the unit to dispatch
 * @return      the target dispatch
 */
AcquisitionController::ActiveInterface::VariableDispatch *AcquisitionController::ActiveInterface::lookupVariableDispatch(
        const SequenceName &unit)
{
    auto check = variableDispatch.find(unit);
    if (check != variableDispatch.end())
        return check->second.get();

    auto variableName = unit.getVariable();
    auto setStation = unit.getStation();
    if (!Util::contains(variableName, '_')) {
        variableName += '_';
        variableName += sourceName.toStdString();
        setStation = targetStation;
    }
    if (setStation.empty())
        setStation = targetStation;

    std::unique_ptr<VariableDispatch> add(new VariableDispatch);
    if (unit.isMeta()) {
        add->unit = SequenceName(setStation, archiveRawMeta, variableName, unit.getFlavors());
        add->isMeta = true;
    } else {
        add->unit = SequenceName(setStation, archiveRaw, variableName, unit.getFlavors());
        add->isMeta = false;
    }

    auto targetGroups = groups;
    for (auto &vg : variableGroupList) {
        /* Look for both the match without the suffix and with it.  This is required
         * to handle the explicitly groups variables correctly, since they don't
         * specify a suffix ever. */
        if (!vg.selection.matches(unit) && !vg.selection.matches(add->unit))
            continue;
        targetGroups = vg.groups;
        break;
    }

    auto ivs = interfaceVariableSets.find(targetGroups);
    if (ivs == interfaceVariableSets.end()) {
        ivs = interfaceVariableSets.emplace(targetGroups, std::unique_ptr<InterfaceVariableSet>(
                new InterfaceVariableSet)).first;

        loggingLatest = FP::undefined();
    }

    add->target = ivs->second.get();
    return variableDispatch.emplace(unit, std::move(add)).first->second.get();
}

/**
 * Handling incoming values on the logging interface.
 * 
 * @param values    the values to log
 */
void AcquisitionController::ActiveInterface::handleLogging(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    {
        std::lock_guard<std::mutex> lock(controller.mutex);

        /* Incoming data releases any halt held.  Any halts processed will
         * have set the logging begin/discard time anyway. */
        loggingHalt = FP::undefined();

        /* Always track how far we can advance to */
        loggingLatest = values.back().getStart();
#ifdef ACQUISITION_CONTROLLER_LOGGING_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(loggingOrderCheck, loggingLatest) <= 0);
        loggingOrderCheck = loggingLatest;
#endif

        for (auto &value : values) {
            VariableDispatch *target = lookupVariableDispatch(value.getUnit());
            target->target->pendingLoggingData.emplace_back(std::move(value));
            auto &added = target->target->pendingLoggingData.back();
            added.setName(target->unit);
            added.setPriority(0);
            if (target->isMeta) {
                added.write().metadata("Realtime").remove(false);
            }
        }

        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::handleLogging(SequenceValue &&value)
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);

        /* Incoming data releases any halt held.  Any halts processed will
         * have set the logging begin/discard time anyway. */
        loggingHalt = FP::undefined();

        /* Always track how far we can advance to */
        loggingLatest = value.getStart();
#ifdef ACQUISITION_CONTROLLER_LOGGING_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(loggingOrderCheck, loggingLatest) <= 0);
        loggingOrderCheck = loggingLatest;
#endif

        VariableDispatch *target = lookupVariableDispatch(value.getUnit());
        target->target->pendingLoggingData.emplace_back(std::move(value));
        auto &added = target->target->pendingLoggingData.back();
        added.setName(target->unit);
        added.setPriority(0);
        if (target->isMeta) {
            added.write().metadata("Realtime").remove(false);
        }

        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

/**
 * End the logging interface for the backend.
 */
void AcquisitionController::ActiveInterface::endLogging()
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        for (auto &set : interfaceVariableSets) {
            set.second->loggingEnded = true;
        }
        loggingEnded = true;

        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

/**
 * Handle incoming realtime values from the acquisition backend.
 * 
 * @param values    the values to process
 */
void AcquisitionController::ActiveInterface::handleRealtime(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        for (auto &value : values) {
            VariableDispatch *target = lookupVariableDispatch(value.getUnit());
            target->target->pendingRealtimeData.emplace_back(std::move(value));
            auto &added = target->target->pendingRealtimeData.back();
            added.setName(target->unit);
            added.setPriority(0);
        }

        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::ActiveInterface::handleRealtime(SequenceValue &&value)
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        VariableDispatch *target = lookupVariableDispatch(value.getUnit());
        target->target->pendingRealtimeData.emplace_back(std::move(value));
        auto &added = target->target->pendingRealtimeData.back();
        added.setName(target->unit);
        added.setPriority(0);

        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

/**
 * Handling incoming persistent values from the acquisition backend.
 * 
 * @param values    the values to log
 */
void AcquisitionController::ActiveInterface::handlePersistent(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        for (auto &value : values) {
            VariableDispatch *target = lookupVariableDispatch(value.getUnit());
            value.setUnit(target->unit);
        }
    }
    controller.archive->incomingPersistent(std::move(values));
}

void AcquisitionController::ActiveInterface::handlePersistent(SequenceValue &&value)
{
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        VariableDispatch *target = lookupVariableDispatch(value.getUnit());
        value.setUnit(target->unit);
    }
    controller.archive->incomingPersistent(std::move(value));
}


AcquisitionController::ActiveInterface::InterfaceVariableSet::InterfaceVariableSet() : flushEndTime(
        FP::undefined()),
                                                                                       loggingBeginTime(
                                                                                               FP::undefined()),
#ifdef ACQUISITION_CONTROLLER_LOGGING_TIME_DEBUGGING
                                                                                       loggingLastAdvance(
                                                                                               FP::undefined()),
#endif
                                                                                       loggingEnded(
                                                                                               false),
                                                                                       set(),
                                                                                       pendingLoggingData(),
                                                                                       pendingRealtimeData()
{ }

AcquisitionController::ActiveInterface::VariableDispatch::VariableDispatch()
        : unit(), target(nullptr), isMeta(false)
{ }

AcquisitionController::ActiveInterface::Remapping::Remapping(AcquisitionController &controller,
                                                             std::unique_ptr<
                                                                     Data::DynamicInput> &&input,
                                                             std::unique_ptr<
                                                                     Calibration> &&calibration)
        : controller(controller), input(std::move(input)), calibration(std::move(calibration))
{ }

AcquisitionController::ActiveInterface::Remapping::~Remapping() = default;

/**
 * Apply the remapping to the value.
 * 
 * @param value the input and output value
 */
void AcquisitionController::ActiveInterface::Remapping::apply(Variant::Root &value)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (input) {
        std::lock_guard<std::mutex> realtimeLock(controller.realtimeLatestMutex);
        value.write().set(input->getValue(controller.realtimeLatest));
    }
    if (calibration) {
        switch (value.read().getType()) {
        case Variant::Type::Integer: {
            auto i = value.read().toInteger();
            double v;
            if (!INTEGER::defined(i)) {
                v = calibration->apply(FP::undefined());
            } else {
                v = calibration->apply(i);
            }
            if (!FP::defined(v)) {
                value.write().setInteger(INTEGER::undefined());
            } else {
                value.write().setInteger(static_cast<std::int_fast64_t>(std::round(v)));
            }
            break;
        }
        case Variant::Type::Array:
            for (auto child : value.write().toArray()) {
                child.setReal(calibration->apply(child.toReal()));
            }
            break;
        case Variant::Type::Matrix:
            for (auto child : value.write().toMatrix()) {
                child.second.setReal(calibration->apply(child.second.toReal()));
            }
            break;
        default:
            value.write().setReal(calibration->apply(value.read().toReal()));
            break;
        }
    }
}

/**
 * Register new realtime units with the remapping.
 * 
 * @param units the set of new units to register
 */
void AcquisitionController::ActiveInterface::Remapping::handleNewUnits(const QSet<
        SequenceName> &units)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (input) {
        for (const auto &add : units) {
            input->registerInput(add);
        }
    }
}

AcquisitionController::VariableSet::VariableSet(AcquisitionController &controller, Groups groups)
        : controller(controller),
          groups(std::move(groups)),
          loggingMux(false),
          loggingSystemIngress(nullptr),
          loggingSmoother(nullptr),
          loggingArchive(nullptr),
          realtimeSmoother(nullptr),
          average()
{
    loggingMux.start();
#ifdef MULTIPLEXER_TAG_TRACKING
    std::vector<std::string> sorted;
    Util::append(this->groups, sorted);
    std::sort(sorted.begin(), sorted.end());
    QString tag;
    for (const auto &s : sorted) {
        if (!tag.empty)
            tag += ":";
        tag += QString::fromStdString(s);
    }
    loggingMux.setTag(tag.toUpper());
    loggingSystemIngress = loggingMux.createIngress(false, true, QString(":SI"));
#else
    loggingSystemIngress = loggingMux.createSink();
#endif
}

AcquisitionController::VariableSet::~VariableSet()
{
    if (loggingSystemIngress)
        loggingSystemIngress->endData();
    loggingMux.signalTerminate(false);
    loggingMux.wait();
    if (loggingSmoother) {
        loggingSmoother->signalTerminate();
        loggingSmoother->wait();
        loggingSmoother.reset();
    }
    loggingArchive.reset();
    if (realtimeSmoother) {
        realtimeSmoother->signalTerminate();
        realtimeSmoother->wait();
        realtimeSmoother.reset();
    }
}

/**
 * Advance the variable set to the given time.  This moves the holdback
 * forward so no data can be received before that time and calls into
 * the process function for the archive.
 */
void AcquisitionController::VariableSet::advance(double time)
{
    loggingSystemIngress->incomingAdvance(time);
}


/**
 * Get the variable set for a given group.  This will create the set
 * if it does not exist.
 * 
 * @param group the group to find
 * @return      the variable set for the group
 */
AcquisitionController::VariableSet &AcquisitionController::getVariableSet(const Groups &group)
{
    auto lookup = variableSets.find(group);
    if (lookup != variableSets.end())
        return *(lookup->second);

    std::unique_ptr<VariableSet> vs(new VariableSet(*this, group));

    vs->average = defaultAverageTime;
    for (const auto &check : groupAveragingTime) {
        if (check.first == group) {
            vs->average = check.second;
            break;
        }
        if (!groupsOverlap(group, check.first))
            continue;
        vs->average = check.second;
    }

    if (vs->average.count <= 0) {
        vs->loggingArchive.reset(archive->createLoggingSink());
        vs->loggingMux.setEgress(vs->loggingArchive.get());
    } else {
        vs->loggingArchive.reset(archive->createLoggingSink());
        vs->loggingSmoother
          .reset(new SmoothingEngineFixedTime(
                  new DynamicTimeInterval::Constant(vs->average.unit, vs->average.count,
                                                    vs->average.aligned), nullptr, nullptr, true,
                  defaultEnableStatistics));
        vs->loggingSmoother->finished.connect([this] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serviceRequired = true;
            }
            request.notify_all();
        });
        vs->loggingSmoother->start();
        vs->loggingSmoother->setEgress(vs->loggingArchive.get());
        vs->loggingMux.setEgress(vs->loggingSmoother.get());
    }

    vs->realtimeSmoother.reset(new RealtimeEngine(defaultEnableRealtimeStatistics));
    vs->realtimeSmoother->finished.connect([this] {
        {
            std::lock_guard<std::mutex> lock(mutex);
            serviceRequired = true;
        }
        request.notify_all();
    });
    /* Set this before starting, since in a single-thread case something can be blocked on our
     * mutex (which we've got held right now) but this would block waiting for an available
     * thread, resulting in a kind of soft-deadlock. */
    vs->realtimeSmoother->setAveraging(vs->average.unit, vs->average.count, vs->average.aligned);
    vs->realtimeSmoother->setEgress(&realtimeIngress);
    vs->realtimeSmoother->start();

    return *(variableSets.emplace(group, std::move(vs)).first->second);
}

/**
 * Check if the system is ready to exit.
 */
bool AcquisitionController::readyToExit()
{
    if (!active.empty())
        return false;
    if (inShutdown)
        return true;

    if (!shutdownWithNoInterfaces)
        return false;
    if (retryOnNoAutoprobe)
        return false;
    if (!autoprobe.empty())
        return false;

    switch (autoprobeStatus) {
    case AutoprobeStatus::NeverSucceeded:
    case AutoprobeStatus::HaveActive:
    case AutoprobeStatus::HaveHadActive:
        break;
    case AutoprobeStatus::RetryPendingWithSuccesses:
    case AutoprobeStatus::RetryPendingNeverSucceeded:
        return false;
    }

    {
        std::lock_guard<std::mutex> lock(injectPendingMutex);
        if (!injectPending.empty())
            return false;
    }
    return true;
}

/**
 * Dispatch all pending queued commands.
 */
void AcquisitionController::serviceCommands()
{
    std::vector<PendingCommand> service;
    {
        std::lock_guard<std::mutex> lock(pendingCommandsMutex);
        if (pendingCommands.empty())
            return;
        service = std::move(pendingCommands);
        pendingCommands.clear();
    }

    for (auto &command : service) {
        QRegularExpression matcher(QString::fromStdString(command.target));
        for (const auto &interface : active) {
            if (!command.target.empty()) {
                if (!Util::exact_match(interface->sourceName, matcher))
                    continue;
            } else {
                if (!groupsOverlap(command.groups, interface->groups))
                    continue;
            }

            interface->acquisition->incomingCommand(command.command);

            qCDebug(log_acquisition_controller) << "Issued command" << command.command << "to"
                                                << interface->sourceName;
        }
        if (command.command.read().hash("Persistent").toBool()) {
            initializeCommands.emplace_back(std::move(command));
        }
    }
}

/**
 * Service a system update.  The update is a series of:
 * <ol>
 *  <li>Autoprobe interfaces are promoted when they complete successfully
 *  <li>New realtime units are registered with all remappings
 *  <li>Pending commands are dispatched
 *  <li>advanceData(double) is called on all interfaces to service their data
 *      and general time advances
 *  <li>advanceState(double) is called on all interfaces to update the system
 *      state in their overlays
 *  <li>processData(double) is called on all interfaces to trigger the 
 *      processing of pending data in their overlays
 *  <li>advance(double) is called on all variable sets to move their holdback
 *      interfaces forward now that all state has been set for the current time
 *  <li>If system is ready to exit then trigger the event loop exit
 * </ol>
 * The data advances are normally throttled to 5Hz.
 * 
 * @param disableThrottle   if set then the data advances are not throttled
 * @return                  false if the thread should exit
 */
void AcquisitionController::serviceUpdate(bool disableThrottle)
{
    double time = Time::time();

    double nextWakeup = FP::undefined();
    /* If we have a schedule time that's elapsed, do state processing at that
     * time.  This allows scheduled events (e.x. cut size) to run before real
     * data are generated, so intersections are hit correctly */
    if (FP::defined(scheduledWakeup) && time >= scheduledWakeup) {
        double effectiveTime = scheduledWakeup;
        scheduledWakeup = FP::undefined();

        Q_ASSERT(lastDataProcess == 0 || effectiveTime >= lastDataProcess);

        for (const auto &interface : active) {
            double checkTime = interface->advanceData(effectiveTime, false);
            if (FP::defined(checkTime) && (!FP::defined(nextWakeup) || checkTime < nextWakeup))
                nextWakeup = checkTime;
        }
        for (const auto &interface : active) {
            interface->advanceState(effectiveTime);
        }
        for (const auto &interface : active) {
            interface->processData(effectiveTime);
        }
    }

    Q_ASSERT(FP::defined(nextHearbeat));
    if (time >= nextHearbeat) {
        heartbeat();
        nextHearbeat = time + 5.0;
    }

    if (!inShutdown) {
        for (auto controller = autoprobe.begin(); controller != autoprobe.end();) {
            if (!(*controller)->isActive()) {
                if (!FP::defined((*controller)->getActivationTime())) {
                    controller = autoprobe.erase(controller);
                    updateAutoprobeStatus();
                    continue;
                }
                if ((*controller)->getActivationTime() <= time) {
                    auto save = std::move(*controller);
                    controller = autoprobe.erase(controller);
                    promoteAutoprobe(time, save.get());
                    updateAutoprobeStatus();
                    continue;
                }
            }
            ++controller;
        }

        /* If we've ever had something succeed, but none did and we're out,
         * retry from the top, in case this was a long delayed reconnection
         * or something */
        if (autoprobe.empty()) {
            switch (autoprobeStatus) {
            case AutoprobeStatus::NeverSucceeded:
                if (retryOnNoAutoprobe) {
                    autoprobeStatus = AutoprobeStatus::RetryPendingNeverSucceeded;
                    qCDebug(log_acquisition_controller)
                        << "All autoprobe interfaces failed but continual retry is enabled, initiating full autoprobe reset";

                    for (const auto &a : active) {
                        if (!a->wasAutoprobed)
                            continue;
                        a->saveState();
                        a->acquisition->initiateShutdown();
                    }
                    autoprobeRetryTime = time + 30.0;
                    break;
                }
                /* Fall through */
            case AutoprobeStatus::HaveActive:
            case AutoprobeStatus::RetryPendingWithSuccesses:
            case AutoprobeStatus::RetryPendingNeverSucceeded:
                break;
            case AutoprobeStatus::HaveHadActive:
                autoprobeStatus = AutoprobeStatus::RetryPendingWithSuccesses;
                qCDebug(log_acquisition_controller)
                    << "All autoprobe interfaces failed but have had prior successes, initiating full autoprobe reset";

                for (const auto &a : active) {
                    if (!a->wasAutoprobed)
                        continue;
                    a->saveState();
                    a->acquisition->initiateShutdown();
                }
                autoprobeRetryTime = time + 30.0;
                break;
            }
        }
    }

    {
        std::unique_lock<std::mutex> lock(realtimeLatestMutex);
        realtimeLatest.setStart(time);

        if (!realtimeNewUnits.isEmpty()) {
            auto units = std::move(realtimeNewUnits);
            realtimeNewUnits.clear();
            lock.unlock();

            for (const auto &interface : active) {
                interface->handleNewUnits(units);
            }

            lock.lock();
        }

        if (!realtimeDistribute.empty()) {
            auto values = std::move(realtimeDistribute);
            realtimeDistribute.clear();

            lock.unlock();
            for (const auto &interface : active) {
                StreamSink *ingress = interface->acquisition->getRealtimeIngress();
                if (!ingress)
                    continue;
                ingress->incomingData(values);
            }
        }
    }

    {
        std::unique_lock<std::mutex> lock(injectPendingMutex);
        auto pending = std::move(injectPending);
        injectPending.clear();
        if (!pending.empty()) {
            lock.unlock();

            for (const auto &add : pending) {
                createFixed(add.component, add.config, "INJECTED");
            }

            asyncCall(std::bind(&AcquisitionController::checkCompleted, this));
        }
    }

    serviceCommands();

    for (const auto &interface : active) {
        double checkTime = interface->advanceData(time);
        if (FP::defined(checkTime) && (!FP::defined(nextWakeup) || checkTime < nextWakeup))
            nextWakeup = checkTime;
    }
    for (const auto &interface : active) {
        interface->advanceState(time);
    }
    if (time - lastDataProcess >= dataUpdateInterval || disableThrottle) {
        lastDataProcess = time;

        for (const auto &interface : active) {
            interface->processData(time);
        }
        for (const auto &set : variableSets) {
            set.second->advance(time);
        }
    }

    if (readyToExit()) {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }

    /* Store the scheduled time if it's after the current time so we can execute
     * a process at the schedule time, once it passes */
    if (FP::defined(nextWakeup) && nextWakeup > time) {
        scheduledWakeup = nextWakeup;
    } else {
        scheduledWakeup = FP::undefined();
    }
}

/**
 * Check all interfaces to see if they have completed.  Then if the system
 * has has finished, exit.
 */
void AcquisitionController::checkCompleted()
{
    double time = Time::time();
    Groups updated;
    for (auto interface = active.begin(); interface != active.end();) {
        if (!(*interface)->checkCompleted(time)) {
            ++interface;
            continue;
        }
        network->updateInterfaceInformation((*interface)->sourceName.toStdString(),
                                            Variant::Root());
        network->updateInterfaceState((*interface)->sourceName.toStdString(), Variant::Root());
        qCInfo(log_acquisition_controller) << "Interface" << (*interface)->sourceName
                                           << "shut down";

        if (!inShutdown) {
            Util::merge((*interface)->outputGroups, updated);
        }

        /* Make sure we delete it only after removing it from the list */
        auto save = std::move(*interface);
        interface = active.erase(interface);
        save.reset();
    }
    if (readyToExit()) {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    if (updated.empty())
        return;

    std::unordered_map<Groups, SequenceName::Flavors, GroupsHash> lookupSystemFlavors;
    std::unordered_map<Groups, Variant::Flags, GroupsHash> lookupBypassed;
    std::unordered_map<Groups, Variant::Flags, GroupsHash> lookupSystemFlags;
    for (const auto &interface : active) {
        interface->updateSystemFlavors(updated, lookupSystemFlavors);
        interface->updateBypass(updated, lookupBypassed);
        interface->updateSystemFlags(updated, lookupSystemFlags);
    }

    std::lock_guard<std::mutex> lock(mutex);
    serviceRequired = true;
}

static const Data::SequenceName::Component eventArchive = "events";
static const Data::SequenceName::Component eventVariable = "acquisition";

/**
 * Write an event to the archive.
 * 
 * @param time      the time of the event
 * @param groups    the groups of the interface that generated it
 * @param content   the event content
 */
void AcquisitionController::archiveEvent(double time,
                                         const SequenceName::Component &station,
                                         Variant::Root &&content)
{
    archive->incomingEvent(
            SequenceValue(SequenceName(station, eventArchive, eventVariable), std::move(content),
                          time, time));
}

/**
 * Process a logged event.
 * 
 * @param time      the time of the event
 * @param station   the station for the event
 * @param groups    the groups of the interface that generated it
 * @param content   the event content
 */
void AcquisitionController::processEvent(double time,
                                         const SequenceName::Component &station,
                                         const Groups &groups,
                                         Variant::Root content)
{
    if (!FP::defined(time))
        time = Time::time();

    Variant::Flags systemFlavors;
    Variant::Flags systemFlags = networkInjectedSystemFlags;
    Variant::Flags bypassFlags = networkInjectedBypassFlags;
    double flushEnd = FP::undefined();
    for (const auto &interface : active) {
        if (!groupsOverlap(groups, interface->outputGroups))
            continue;

        std::lock_guard<std::mutex> lock(mutex);
        for (const auto &f : interface->systemFlavors) {
            systemFlavors.insert(f);
        }
        Util::merge(interface->activeBypassFlags, bypassFlags);
        Util::merge(interface->activeSystemFlags, systemFlags);
        if (!FP::defined(flushEnd) ||
                (FP::defined(interface->flushEndTime) && interface->flushEndTime < flushEnd))
            flushEnd = interface->flushEndTime;
    }
    auto target = content.write();

    target.hash("State").hash("SystemFlags").setFlags(systemFlags);
    target.hash("State").hash("BypassFlags").setFlags(bypassFlags);
    target.hash("State").hash("Flavors").setFlags(systemFlavors);
    target.hash("State").hash("FlushEnd").setDouble(flushEnd);

    if (target.hash("ShowRealtime").toBool()) {
        auto copy = content;
        copy.write().hash("Time").setDouble(time);
        network->incomingEvent(std::move(copy));
    }

    archiveEvent(time, std::move(station), std::move(content));
}


/**
 * Set the averaging time for the variable set.
 * 
 * @param change    the averaging time to change to
 */
void AcquisitionController::VariableSet::setAveragingTime(const AveragingInternval &change)
{
    if (average == change)
        return;
    average = change;

    if (average.count <= 0) {
        if (loggingSmoother) {
            loggingMux.setEgress(nullptr);
            loggingSmoother->endData();
            loggingSmoother->wait();
            loggingSmoother.reset();

            loggingArchive.reset(controller.archive->createLoggingSink());
            loggingMux.setEgress(loggingArchive.get());
        }

        realtimeSmoother->setAveraging(average.unit, average.count, average.aligned);
        return;
    }

    loggingMux.setEgress(nullptr);

    if (loggingSmoother) {
        loggingSmoother->endData();
        loggingSmoother->wait();
        loggingSmoother.reset();
    } else {
        loggingArchive->endData();
    }

    loggingArchive.reset(controller.archive->createLoggingSink());
    loggingSmoother.reset(new SmoothingEngineFixedTime(
            new DynamicTimeInterval::Constant(average.unit, average.count, average.aligned),
            nullptr, nullptr, true, controller.defaultEnableStatistics));
    loggingSmoother->finished.connect([this] {
        {
            std::lock_guard<std::mutex> lock(controller.mutex);
            controller.serviceRequired = true;
        }
        controller.request.notify_all();
    });
    loggingSmoother->start();
    loggingSmoother->setEgress(loggingArchive.get());
    loggingMux.setEgress(loggingSmoother.get());

    realtimeSmoother->setAveraging(average.unit, average.count, average.aligned);
}

/**
 * Set the averaging time for a given group set.  Setting to zero count
 * disables averaging (data are written at the logged time base).
 * 
 * @param groups    the group set to change
 * @param unit      the time unit
 * @param count     the number of units
 * @param aligned   true if the averages should be aligned
 */
void AcquisitionController::setAveragingTime(const Groups &groups,
                                             Time::LogicalTimeUnit unit,
                                             int count,
                                             bool aligned)
{
    AveragingInternval avg;
    avg.unit = unit;
    avg.count = count;
    avg.aligned = aligned;
    for (const auto &set : variableSets) {
        if (!groupsOverlap(groups, set.first))
            continue;
        set.second->setAveragingTime(avg);
        Util::insert_or_assign(groupAveragingTime, set.first, avg);
    }
    Util::insert_or_assign(groupAveragingTime, groups, std::move(avg));

    for (const auto &a : active) {
        a->averagingChanged(groups);
    }

    qCDebug(log_acquisition_controller) << "Averaging time set to"
                                        << Time::describeOffset(unit, count, aligned)
                                        << "for groups:" << groups;
}

/**
 * Generate a unique source name from the given base.
 * 
 * @param time      the time of activation
 * @param base      the base name
 * @param line      the sampling line
 */
QString AcquisitionController::generateSourceName(double time, const QString &base, qint64 line)
{
    if (!INTEGER::defined(line))
        line = 1;
    QString result;
    if (!base.contains("$2")) {
        result = base;
        result.replace("$1", QString::number(line));

        bool hit = false;
        for (const auto &interface : active) {
            if (interface->sourceName == result) {
                hit = true;
                break;
            }
        }
        if (!hit)
            return result;

        qCWarning(log_acquisition_controller) << "Source name" << result
                                              << "is non-unique.  This may result in duplicate variables.";

        Variant::Root content;
        content["Information/BaseName"].setString(base);
        content["Information/Line"].setInteger(line);
        content["Information/OutputName"].setString(result);
        content["Text"].setString(QObject::tr(
                "Source name \"%1\" is non-unique.  This may result in duplicated variables.").arg(
                result));
        content["ShowRealtime"].setBool(true);
        content["Source"].setString(result);
        content["Component"].setString("acquisitioncontroller");

        processEvent(time, defaultStation, Groups(), std::move(content));

        return result;
    }
    for (int instance = 1; instance < 9999; instance++) {
        result = base;
        result.replace("$1", QString::number(line));
        result.replace("$2", QString::number(instance));

        bool hit = false;
        for (const auto &interface : active) {
            if (interface->sourceName == result) {
                hit = true;
                break;
            }
        }
        if (!hit)
            return result;
    }

    qCWarning(log_acquisition_controller) << "Failed to find a free instance for the base name"
                                          << base
                                          << ".  Assigning it to zero.  This may result in duplicate variables.";

    result = base;
    result.replace("$1", QString::number(line));
    result.replace("$2", "0");

    Variant::Root content;
    content["Information/BaseName"].setString(base);
    content["Information/Line"].setInteger(line);
    content["Information/OutputName"].setString(result);
    content["Text"].setString(QObject::tr(
            "Source name \"%1\" is non-unique due to instance exhaustion.  This may result in duplicated variables.")
                                      .arg(result));
    content["ShowRealtime"].setBool(true);
    content["Source"].setString(result);
    content["Component"].setString("acquisitioncontroller");

    processEvent(time, defaultStation, Groups(), std::move(content));

    return result;
}

/**
 * Issue all existing realtime values to a new interface.
 * 
 * @param interface the target interface
 */
void AcquisitionController::issueExistingRealtime(AcquisitionInterface *interface)
{
    StreamSink *ingress = interface->getRealtimeIngress();
    if (!ingress)
        return;

    SequenceValue::Transfer send;
    {
        std::lock_guard<std::mutex> lock(realtimeLatestMutex);
        for (const auto &name : realtimeSeenUnits) {
            send.emplace_back(realtimeLatest.getSequenceValue(name));
        }
        if (send.empty())
            return;
    }
    ingress->incomingData(std::move(send));
}

static QChar generateMenuCharacter(int idx)
{
    idx = idx % 36;
    if (idx < 26)
        return 'A' + idx;
    if (idx != 35)
        return '1' + (idx - 26);
    return '0';
}

/**
 * Re-assign all menu characters for all interfaces, and cause the changed
 * ones to emit their updated signal (if they're not the skip interface).
 * 
 * @param skipExisting      the interface to skip changed re-emission for
 */
void AcquisitionController::reassignMenuCharacters(ActiveInterface *skipExisting)
{
    QSet<QChar> claimedMenuCharacters;
    QHash<ActiveInterface *, QChar> previousMenu;
    for (const auto &existing : active) {
        ActiveInterface *i = existing.get();

        if (!i->menuCharacter.isEmpty())
            previousMenu.insert(i, i->menuCharacter.at(0));

        if (!i->config.empty() && i->config.back().value().hash("MenuCharacter").exists()) {
            i->menuCharacter = i->config.back().value().hash("MenuCharacter").toDisplayString();
        } else {
            i->menuCharacter.clear();
        }

        if (!i->menuCharacter.isEmpty())
            claimedMenuCharacters.insert(i->menuCharacter.at(0));
    }

    /* First pass, only try ones that have specific overrides */
    for (const auto &existing : active) {
        ActiveInterface *i = existing.get();
        if (!i->menuCharacter.isEmpty())
            continue;
        if (!i->config.empty() && i->config.back().value().hash("MenuCharacter").exists())
            continue;

        int offset = 0;
        if (i->sourceName.startsWith('S')) {
            offset = 'N' - 'A';
        } else if (i->sourceName.startsWith('N')) {
            offset = 'C' - 'A';
        } else if (i->componentName.contains("clap3w")) {
            offset = 'W' - 'A';
        } else if (i->sourceName.startsWith('A')) {
            offset = 'L' - 'A';
        } else if (i->componentName.contains("pid")) {
            offset = 'P' - 'A';
        } else if (i->componentName.contains("umac") || i->componentName.contains("cr1000")) {
            offset = 'U' - 'A';
        } else {
            continue;
        }

        for (int idx = 0; idx < 36; idx++) {
            QChar proposed(generateMenuCharacter(idx + offset));
            if (!claimedMenuCharacters.contains(proposed)) {
                i->menuCharacter = QString(proposed);
                break;
            }
        }
    }

    /* Second pass, just do everything */
    for (const auto &existing : active) {
        ActiveInterface *i = existing.get();
        if (!i->menuCharacter.isEmpty())
            continue;
        if (!i->config.empty() && i->config.back().value().hash("MenuCharacter").exists())
            continue;

        for (int idx = 0; idx < 36; idx++) {
            QChar proposed(generateMenuCharacter(idx));
            if (!claimedMenuCharacters.contains(proposed)) {
                i->menuCharacter = QString(proposed);
                break;
            }
        }
    }

    for (const auto &existing : active) {
        ActiveInterface *i = existing.get();
        if (i == skipExisting)
            continue;
        auto check = previousMenu.constFind(i);
        if (check == previousMenu.constEnd()) {
            if (!i->menuCharacter.isEmpty()) {
                i->controllerInterface->interfaceInformationUpdated();
            }
        } else {
            if (i->menuCharacter.isEmpty() || i->menuCharacter.at(0) != check.value()) {
                i->controllerInterface->interfaceInformationUpdated();
            }
        }
    }
}

/**
 * Finalize an interface for execution.
 * 
 * @param acquisition   the interface to start
 */
void AcquisitionController::finalizeInterface(ActiveInterface *interface)
{
    if (!interface->config.empty() && interface->config.back().value().hash("Groups").exists()) {
        interface->groups = interface->config.back().value().hash("Groups").toFlags();
    } else {
        for (const auto &check : interfaceSetList) {
            if (!check.componentNameMatch.pattern().isEmpty() &&
                    !Util::exact_match(interface->componentName, check.componentNameMatch))
                continue;
            if (!check.sourceNameMatch.pattern().isEmpty() &&
                    !Util::exact_match(interface->sourceName, check.sourceNameMatch))
                continue;

            interface->groups = check.groups;
        }
    }
    if (!interface->config.empty() &&
            interface->config.back().value().hash("OutputGroups").exists()) {
        interface->outputGroups = interface->config.back().value().hash("OutputGroups").toFlags();
    } else {
        interface->outputGroups = interface->groups;
    }

    interface->acquisition->setRealtimeEgress(&interface->localRealtimeIngress);
    interface->acquisition->setLoggingEgress(&interface->localLoggingIngress);
    interface->acquisition->setPersistentEgress(&interface->localPersistentIngress);

    active.emplace_back(interface);
    if (interface->acquisition->getSystemFlagsMetadata().read().exists()) {
        std::unordered_map<Groups, Variant::Root, GroupsHash> lookup;
        for (const auto &update : active) {
            update->updateSystemFlagsMetadata(update->outputGroups, lookup);
        }
    }
    interface->previousBypass = getBypassFlags(interface->groups);
    if (!interface->previousBypass.empty()) {
        Variant::Root command;
        command["Bypass"].setBool(true);
        interface->acquisition->incomingCommand(command);
    }
    for (const auto &command : initializeCommands) {
        QRegularExpression matcher(QString::fromStdString(command.target));
        if (!command.target.empty()) {
            if (!Util::exact_match(interface->sourceName, matcher))
                continue;
        } else {
            if (!groupsOverlap(command.groups, interface->groups))
                continue;
        }
        interface->acquisition->incomingCommand(command.command);
    }
    interface->previousSystemFlags = getSystemFlags(interface->groups);

    reassignMenuCharacters(interface);

    interface->acquisition
             ->persistentValuesUpdated
             .connect(interface->controllerInterface->receiver, std::bind(
                     &AcquisitionController::ActiveInterface::ControllerInterface::persistentValuesUpdated,
                     interface->controllerInterface.get()));
    interface->acquisition
             ->sourceMetadataUpdated
             .connect(interface->controllerInterface->receiver, std::bind(
                     &AcquisitionController::ActiveInterface::ControllerInterface::interfaceInformationUpdated,
                     interface->controllerInterface.get()));
    interface->acquisition
             ->generalStatusUpdated
             .connect(interface->controllerInterface->receiver, std::bind(
                     &AcquisitionController::ActiveInterface::ControllerInterface::interfaceStateUpdated,
                     interface->controllerInterface.get()));
    interface->writePersistentValues();
    interface->controllerInterface->interfaceInformationUpdated();
    interface->controllerInterface->interfaceStateUpdated();

    issueExistingRealtime(interface->acquisition.get());

    if (!interface->io)
        return;

    /* Don't connect this until we have an active interface so we don't
     * continually restart on ports that are detected but are bad
     * (some x86 UARTS always report 4 ports, but they're not valid) */
    if (interface->io->triggerAutoprobeWhenClosed()) {
        interface->io->deviceClosed.connect([this] {
            asyncCall(std::bind(&AcquisitionController::autoprobeSystemChanged, this));
        });
    }

    auto matcher = interface->io->target(true);
    for (auto &check : interfaceComponentLookup) {
        if (!check.target)
            continue;
        if (!matcher->matchesDevice(*check.target))
            continue;

        check.target = std::move(matcher);
        check.componentName = interface->componentName;
        return;
    }

    InterfaceComponentData add;
    add.target = std::move(matcher);
    add.componentName = interface->componentName;
    interfaceComponentLookup.emplace_back(std::move(add));
}

/**
 * Promote a completed autoprobe controller to a fully defined acquisition
 * interface.
 * 
 * @param time          the time of activeation
 * @param controller    the controller to activate
 */
void AcquisitionController::promoteAutoprobe(double time, AutoprobeController *controller)
{
    auto probed = controller->takeResult();
    if (!probed)
        return;
    Q_ASSERT(probed->acquisition);

    autoprobeStatus = AutoprobeStatus::HaveActive;

    auto defaults = probed->acquisition->getDefaults();

    Q_ASSERT(!probed->componentName.isEmpty());
    QString instrumentName(probed->instrumentName);
    if (instrumentName.isEmpty())
        instrumentName = QString::fromStdString(defaults.name);
    if (instrumentName.isEmpty())
        instrumentName = "X$2";
    auto config = std::move(probed->configuration);
    qint64 line = INTEGER::undefined();
    if (!config.empty())
        line = config.back().value().hash("Line").toInt64();
    instrumentName = generateSourceName(time, instrumentName, line);

    ActiveInterface *interface =
            new ActiveInterface(*this, std::move(probed->acquisition), std::move(probed->interface),
                                config, probed->isPassive);
    interface->wasAutoprobed = true;
    interface->componentName = std::move(probed->componentName);
    interface->sourceName = instrumentName;

    /* Set a timeout in case the instrument itself doesn't set one as part
     * of promotion in the hopes that it will handle it and drop comms
     * eventually if it never receives any data */
    double failsafeTimeout = FP::undefined();
    if (!this->config.empty()) {
        failsafeTimeout =
                this->config.back().value().hash("Autoprobe").hash("FailsafeTimeout").toDouble();
    }
    if (!FP::defined(failsafeTimeout))
        failsafeTimeout = 3601.0;
    if (failsafeTimeout >= 0.0)
        interface->controlTimeout = time + failsafeTimeout;

    interface->restoreState();
    probed->handoff(time, interface->controllerInterface.get(), [interface] {
        interface->connectInterfaceData(false);
    });
    if (probed->isPassive) {
        interface->acquisition->autoprobePromotePassive(time);
    } else {
        interface->acquisition->autoprobePromote(time);
    }
    finalizeInterface(interface);
    /* Must be done after finalize, so the groups and names are set */
    interface->acquisition->setState(interface->controllerInterface.get());

    qCInfo(log_acquisition_controller) << "Promoted autoprobed interface"
                                       << interface->componentName << "as" << instrumentName
                                       << "with groups:" << interface->groups;

    for (const auto &purge : autoprobe) {
        purge->removeInstrumentName(instrumentName);
    }

    interface->acquisition->completed.connect([this] {
        asyncCall(std::bind(&AcquisitionController::checkCompleted, this));
    });
    asyncCall(std::bind(&AcquisitionController::checkCompleted, this));
}

/**
 * Begin the autoprobe of all interfaces.  Any already active autoprobing
 * systems are terminated first.
 */
void AcquisitionController::beginAutoprobe()
{
    qCDebug(log_acquisition_controller) << "Autoprobe creation started";

    autoprobe.clear();
    switch (autoprobeStatus) {
    case AutoprobeStatus::NeverSucceeded:
    case AutoprobeStatus::HaveActive:
    case AutoprobeStatus::HaveHadActive:
        break;
    case AutoprobeStatus::RetryPendingWithSuccesses:
        autoprobeStatus = AutoprobeStatus::HaveHadActive;
        break;
    case AutoprobeStatus::RetryPendingNeverSucceeded:
        autoprobeStatus = AutoprobeStatus::NeverSucceeded;
        break;
    }

    std::vector<std::unique_ptr<IOTarget>> exclude;
    for (const auto &a : active) {
        if (a->wasAutoprobed)
            continue;
        if (!a->io)
            continue;
        exclude.emplace_back(a->io->target(true));
    }

    std::vector<std::unique_ptr<IOTarget>> interfaces;
    bool enableLocal = true;
    if (!config.empty()) {
        auto active = config.back().read();

        if (active["Autoprobe/Disable"].toBool()) {
            qCDebug(log_acquisition_controller) << "Autoprobe disabled";
            return;
        }
        if (active["Autoprobe/DisableDefaultInterfaces"].toBool())
            enableLocal = false;

        for (auto child : active["Autoprobe/Blacklist"].toChildren()) {
            auto insert = IOTarget::create(child);
            if (!insert)
                continue;
            exclude.emplace_back(std::move(insert));
        }

        for (auto child : active["Autoprobe/Additional"].toChildren()) {
            auto insert = IOTarget::create(child);
            if (!insert)
                continue;
            if (!insert->isValid())
                continue;
            exclude.emplace_back(insert->clone(true));
            interfaces.emplace_back(std::move(insert));
        }

        for (const auto &segment : config) {
            for (auto add : segment.value().hash("Components").toHash()) {
                if (add.second.hash("Fixed").toBool())
                    continue;
                auto check = add.second.hash("Interface");
                if (!check.exists())
                    continue;
                auto insert = IOTarget::create(check);
                if (!insert)
                    continue;
                if (!insert->isValid())
                    continue;

                bool hit = false;
                for (const auto &matchCheck : exclude) {
                    if (matchCheck->matchesDevice(*insert)) {
                        hit = true;
                        break;
                    }
                }
                if (hit)
                    continue;

                exclude.emplace_back(insert->clone(true));
                interfaces.emplace_back(std::move(insert));
            }
        }
    }

    if (enableLocal) {
        auto localInterfaces = IOTarget::enumerateLocal();
        for (auto &i : localInterfaces) {
            bool excluded = false;
            for (const auto &check : exclude) {
                if (check->matchesDevice(*i)) {
                    excluded = true;
                    break;
                }
            }
            if (!excluded) {
                interfaces.emplace_back(std::move(i));
            }
        }
    }
    exclude.clear();

    if (interfaces.empty()) {
        qCDebug(log_acquisition_controller) << "No interfaces to autoprobe";
        return;
    }

    qCDebug(log_acquisition_controller) << "Starting autoprobe on" << interfaces.size()
                                        << "interface(s)";
    for (auto &add : interfaces) {
        QString previousComponent;
        for (const auto &check : interfaceComponentLookup) {
            if (!add->matchesDevice(*check.target))
                continue;
            previousComponent = check.componentName;
            break;
        }

        autoprobe.emplace_back(new AutoprobeController(
                std::unique_ptr<IOInterface>(new IOInterface(std::move(add))), config,
                previousComponent));
        auto &ac = autoprobe.back();
        ac->completed.connect([this] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                serviceRequired = true;
            }
            request.notify_all();
        });
        ac->stateChanged.connect([this] {
            asyncCall(std::bind(&AcquisitionController::updateAutoprobeStatus, this));
        });

        ac->start();
    }

    updateAutoprobeStatus();
}

/**
 * Signal the system that the autoprobe backend has changed somehow.  This
 * causes all autoprobed interfaces to shut down and be re-created.  This
 * allows the system to account for them having been moved between different
 * system interfaces.
 */
void AcquisitionController::autoprobeSystemChanged()
{
    switch (autoprobeStatus) {
    case AutoprobeStatus::NeverSucceeded:
        autoprobeStatus = AutoprobeStatus::RetryPendingNeverSucceeded;
        break;
    case AutoprobeStatus::HaveActive:
    case AutoprobeStatus::HaveHadActive:
        autoprobeStatus = AutoprobeStatus::RetryPendingWithSuccesses;
        break;
    case AutoprobeStatus::RetryPendingWithSuccesses:
    case AutoprobeStatus::RetryPendingNeverSucceeded:
        return;
    }

    qCInfo(log_acquisition_controller) << "I/O system changed, initiating full autoprobe reset";
    for (const auto &a : active) {
        if (!a->wasAutoprobed)
            continue;
        a->saveState();
        a->acquisition->initiateShutdown();
    }
    for (const auto &a : autoprobe) {
        a->stop();
    }
    autoprobeRetryTime = Time::time() + 30.0;
}

/**
 * Initiate a flush of data.  This causes all buffered log data to be written
 * to the archive.
 */
void AcquisitionController::flushData()
{
    qCDebug(log_acquisition_controller) << "Executing data flush";

    /* Advance the logging smoother until the end of the mux, so that when we have
     * a set with no streaming values (e.x. the global one with everything but persistent
     * values cut split), we actually write things out */
    for (const auto &set : variableSets) {
        if (set.second->loggingSmoother) {
            double adv = set.second->loggingMux.trackedTime();
            if (FP::defined(adv)) {
                set.second->loggingSmoother->incomingAdvance(adv);
            }
        }
    }
    /* Not really a good way of making sure that everything has made it through the
     * smoothers, so just yield and hope */
    std::this_thread::yield();
    std::this_thread::yield();
    std::this_thread::yield();

    archive->flushLogging();
    network->archiveFlush();

    for (const auto &a : active) {
        a->saveState();
        a->writePersistentValues();
    }

    Memory::release_unused();
}


void AcquisitionController::initiateShutdown()
{
    asyncCall([this] {
        if (!inShutdown) {
            qCInfo(log_acquisition_controller)
                << "Controlled shutdown of acquisition system initiated";
        }

        for (const auto &a : autoprobe) {
            a->stop();
        }
        for (const auto &a : active) {
            a->acquisition->initiateShutdown();
        }

        inShutdown = true;
        asyncCall(std::bind(&AcquisitionController::checkCompleted, this));
    });
}


void AcquisitionController::signalTerminate()
{
    asyncCall([this] {
        for (const auto &del : variableSets) {
            if (del.second->loggingSmoother)
                del.second->loggingSmoother->signalTerminate();
            if (del.second->realtimeSmoother)
                del.second->realtimeSmoother->signalTerminate();
            del.second->loggingMux.signalTerminate(true);
        }
        qCInfo(log_acquisition_controller) << "Terminate of acquisition system initiated";
        inShutdown = true;
        {
            std::lock_guard<std::mutex> lock(mutex);
            terminated = true;
        }
    });
}

/**
 * Create a fixed interface.
 *
 * @param component     the creation component
 * @param config        the configuration
 * @param componentName the component name
 */
void AcquisitionController::createFixed(AcquisitionComponent *component,
                                        const ValueSegment::Transfer &config,
                                        const std::string &componentName)
{
    std::unique_ptr<IOInterface> io;
    std::string loggingContext;

    {
        const auto &add = config.back().value().hash("Instrument").toString();
        if (!add.empty() && add.find_first_of('$') == add.npos) {
            if (!loggingContext.empty())
                loggingContext += '.';
            loggingContext += add;
        }
    }

    if (config.back().value().hash("Interface").exists()) {
        auto target = IOTarget::create(config.back().value().hash("Interface"));
        if (!target) {
            qCWarning(log_acquisition_controller) << "Unable to create interface for"
                                                  << componentName;
            return;
        }
        QString add = target->clone(true)->description();
        io.reset(new IOInterface(std::move(target)));
        if (!add.isEmpty()) {
            if (!loggingContext.empty())
                loggingContext += '.';
            loggingContext += add.toStdString();
        }
    }

    std::unique_ptr<AcquisitionInterface> acquisition;
    bool passive = config.back().value().hash("Passive").toBool();
    if (passive) {
        acquisition = component->createAcquisitionPassive(config, loggingContext);
    } else {
        acquisition = component->createAcquisitionInteractive(config, loggingContext);
    }
    if (!acquisition)
        return;

    acquisition->completed.connect([this] {
        asyncCall(std::bind(&AcquisitionController::checkCompleted, this));
    });
    acquisition->start();

    auto defaults = acquisition->getDefaults();

    QString instrumentName(config.back().value().hash("Instrument").toQString());
    if (instrumentName.isEmpty())
        instrumentName = QString::fromStdString(defaults.name);
    if (instrumentName.isEmpty())
        instrumentName = "X$2";
    instrumentName = generateSourceName(Time::time(), instrumentName,
                                        config.back().value().hash("Line").toInt64());

    ActiveInterface *interface =
            new ActiveInterface(*this, std::move(acquisition), std::move(io), config, passive);
    interface->componentName = QString::fromStdString(componentName);
    interface->sourceName = instrumentName;

    /* Initialization timeout is immediate */
    interface->controlTimeout = Time::time();

    interface->restoreState();
    interface->acquisition->setControlStream(interface->controllerInterface.get());
    finalizeInterface(interface);
    interface->connectInterfaceData(true);

    if (interface->io) {
        /* Have to do this after the start, since the merge needs to connect to it */
        if (defaults.interface.read().exists()) {
            auto overlay = IOTarget::create(defaults.interface);
            if (overlay) {
                interface->io->merge(*overlay);
            }
        }
    }

    /* Must be done after finalize, so the groups and names are set */
    interface->acquisition->setState(interface->controllerInterface.get());

    qCInfo(log_acquisition_controller) << "Created fixed interface of type" << componentName << "as"
                                       << instrumentName << "with groups:" << interface->groups;
}

void AcquisitionController::injectInterface(AcquisitionComponent *component,
                                            const ValueSegment::Transfer &config)
{
    Q_ASSERT(component);
    {
        std::lock_guard<std::mutex> lock(injectPendingMutex);

        InterfaceInjectPending add;
        add.component = component;
        add.config = config;
        injectPending.emplace_back(std::move(add));

        serviceRequired = true;
    }
    request.notify_all();
}

/**
 * Create all fixed interfaces.
 */
void AcquisitionController::createFixedInterfaces()
{
    std::unordered_map<Variant::PathElement::HashIndex, ValueSegment::Transfer>
            componentConfiguration;
    for (const auto &segment : config) {
        for (auto cmp : segment.read().hash("Components").toHash()) {
            if (cmp.first.empty())
                continue;
            if (!cmp.second.hash("Fixed").toBool())
                continue;

            componentConfiguration[cmp.first].emplace_back(segment.getStart(), segment.getEnd(),
                                                           Variant::Root(cmp.second));
        }
    }

    struct InterfaceSort {
        qint64 priority;
        std::string componentName;
        ValueSegment::Transfer configuration;
    };

    std::vector<InterfaceSort> sortList;
    for (auto &checkAdd : componentConfiguration) {
        InterfaceSort add;
        add.configuration = std::move(checkAdd.second);
        add.priority = 0;

        for (const auto &segment : add.configuration) {
            const auto &check = segment.value().hash("Name").toString();
            if (!check.empty())
                add.componentName = check;

            auto i = segment.value().hash("Priority").toInteger();
            if (INTEGER::defined(i))
                add.priority = i;
        }
        if (add.componentName.empty())
            continue;

        sortList.emplace_back(std::move(add));
    }

    std::stable_sort(sortList.begin(), sortList.end(),
                     [](const InterfaceSort &a, const InterfaceSort &b) {
                         if (!INTEGER::defined(a.priority)) {
                             if (INTEGER::defined(b.priority))
                                 return false;
                         } else if (!INTEGER::defined(b.priority)) {
                             return true;
                         }
                         if (a.priority != b.priority)
                             return a.priority < b.priority;
                         return a.componentName < b.componentName;
                     });

    for (const auto &component : sortList) {
        Q_ASSERT(!component.configuration.empty());
        Q_ASSERT(!component.componentName.empty());

        AcquisitionComponent *factory = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create(QString::fromStdString(component.componentName)));
        if (!factory) {
            qCDebug(log_acquisition_controller) << "Failed to load component"
                                                << component.componentName;
            continue;
        }

        createFixed(factory, component.configuration, component.componentName);
    }
}

/**
 * Load component lookup information.
 */
void AcquisitionController::loadComponentLookup()
{
    interfaceComponentLookup.clear();

    auto keyName = QString::fromStdString(stateBaseName);
    keyName += "components";

    QByteArray buffer = Database::RunLock().blindGet(keyName);
    if (buffer.isEmpty())
        return;

    QDataStream stream(&buffer, QIODevice::ReadOnly);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setVersion(QDataStream::Qt_4_5);

    Deserialize::container(stream, interfaceComponentLookup, [&]() {
        InterfaceComponentData add;
        {
            Variant::Root cfg;
            stream >> cfg;
            add.target = IOTarget::create(cfg);
        }
        stream >> add.componentName;
        return std::move(add);
    });
    for (auto it = interfaceComponentLookup.begin(); it != interfaceComponentLookup.end();) {
        if (it->target && it->target->isValid()) {
            ++it;
            continue;
        }
        it = interfaceComponentLookup.erase(it);
    }
}

/**
 * Save component lookup information.
 */
void AcquisitionController::saveComponentLookup()
{
    QByteArray buffer;
    QDataStream stream(&buffer, QIODevice::WriteOnly);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setVersion(QDataStream::Qt_4_5);

    Serialize::container(stream, interfaceComponentLookup, [&](const InterfaceComponentData &add) {
        stream << add.target->configuration(true);
        stream << add.componentName;
    });

    auto keyName = QString::fromStdString(stateBaseName);
    keyName += "components";

    Database::RunLock().blindSet(keyName, buffer);
}

/**
 * Set the status for autoprobing
 */
void AcquisitionController::updateAutoprobeStatus()
{
    Variant::Root status(Variant::Type::Hash);
    if (autoprobe.empty()) {
        status["Active"].setBoolean(false);
    } else {
        status["Active"].setBoolean(true);

        auto interfaces = status["Interfaces"].toArray();
        for (const auto &a : autoprobe) {
            interfaces.after_back().set(a->describeState());
        }
    }

    if (network) {
        network->updateAutoprobeState(status);
    }
}

/**
 * Fill a network event with the standard system data.
 * 
 * @param event the target event
 */
void AcquisitionController::fillNetworkEvent(Variant::Write &event)
{
    event.hash("Source").setString("EXTERNAL");

    Variant::Flags systemFlavors;
    Variant::Flags systemFlags = networkInjectedSystemFlags;
    Variant::Flags bypassFlags = networkInjectedBypassFlags;
    double flushEnd = FP::undefined();
    {
        std::lock_guard<std::mutex> lock(mutex);
        for (const auto &interface : active) {
            Util::merge(interface->systemFlavors, systemFlavors);
            Util::merge(interface->activeBypassFlags, bypassFlags);
            Util::merge(interface->activeSystemFlags, systemFlags);
            if (!FP::defined(flushEnd) ||
                    (FP::defined(interface->flushEndTime) && interface->flushEndTime < flushEnd))
                flushEnd = interface->flushEndTime;
        }
    }
    event.hash("State").hash("SystemFlags").setFlags(systemFlags);
    event.hash("State").hash("BypassFlags").setFlags(bypassFlags);
    event.hash("State").hash("Flavors").setFlags(systemFlavors);
    event.hash("State").hash("FlushEnd").setDouble(flushEnd);
}

/**
 * Network issued message log event.
 * 
 * @param event the event data
 */
void AcquisitionController::networkMessageLogEvent(const Variant::Root &base)
{
    Variant::Root event = base;
    if (event.read().getType() == Variant::Type::String) {
        std::string str = event.read().toString();
        event.write().hash("Text").setString(std::move(str));
    }

    fillNetworkEvent(event.write());
    archiveEvent(Time::time(), defaultStation, std::move(event));
}

/**
 * Network issued command.
 * 
 * @param target    the target (regular expression)
 * @param command   the command
 */
void AcquisitionController::networkCommandReceived(const std::string &target,
                                                   const Variant::Root &command)
{
    qCDebug(log_acquisition_controller) << "Network command" << command << "to" << target
                                        << "received";

    if (target.empty()) {
        for (const auto &interface : active) {
            interface->acquisition->incomingCommand(command);
        }
        return;
    }

    QRegularExpression matcher(QString::fromStdString(target));
    if (!matcher.isValid())
        return;

    for (const auto &interface : active) {
        if (!Util::exact_match(interface->sourceName, matcher))
            continue;
        interface->acquisition->incomingCommand(command);
    }
}

/**
 * Network issued flush request.
 * 
 * @param seconds   the amount of time to flush
 */
void AcquisitionController::networkFlushRequest(double seconds)
{
    for (const auto &interface : active) {
        interface->issueGlobalFlush(seconds);
    }

    qCDebug(log_acquisition_controller) << "Network global flush for " << Logging::elapsed(seconds)
                                        << "issued";

    Variant::Root event;
    event.write().hash("Information").hash("Duration").setDouble(seconds);
    event.write().hash("Text").setString("System flush initiated by an external source");
    fillNetworkEvent(event.write());
    archiveEvent(Time::time(), defaultStation, std::move(event));
}

/**
 * Network issued averaging time set request.
 * 
 * @param unit      the unit of time
 * @param count     the number of units
 * @param aligned   if set then align averages
 */
void AcquisitionController::networkSetAveragingTime(Time::LogicalTimeUnit unit,
                                                    int count,
                                                    bool aligned)
{
    AveragingInternval avg;
    avg.unit = unit;
    avg.count = count;
    avg.aligned = aligned;
    for (const auto &set : variableSets) {
        set.second->setAveragingTime(avg);
        Util::insert_or_assign(groupAveragingTime, set.first, avg);
    }
    for (const auto &interface : active) {
        Util::insert_or_assign(groupAveragingTime, interface->outputGroups, avg);
    }
    defaultAverageTime = avg;

    for (const auto &a : active) {
        a->averagingChanged();
    }

    qCDebug(log_acquisition_controller).noquote() << "Network averaging time set to"
                                                  << Time::describeOffset(unit, count, aligned);

    Variant::Root event;
    Variant::Composite::fromTimeInterval(event.write().hash("Information").hash("Interval"), unit,
                                         count, aligned);
    event.write().hash("Text").setString("Averaging time changed by an external source");
    fillNetworkEvent(event.write());
    archiveEvent(Time::time(), defaultStation, std::move(event));
}

/**
 * Network issued bypass flag set request.
 * 
 * @param flag  the flag to set
 */
void AcquisitionController::networkBypassFlagSet(const Data::Variant::Flag &flag)
{
    networkInjectedBypassFlags.insert(flag);

    {
        std::lock_guard<std::mutex> lock(mutex);
        for (const auto &interface : active) {
            interface->activeBypassFlags.insert(flag);
        }
    }

    std::unordered_map<Groups, Data::Variant::Flags, GroupsHash> lookup;
    for (const auto &interface : active) {
        interface->updateGlobalBypass(lookup);
    }

    qCDebug(log_acquisition_controller) << "Network bypass flag" << flag << "set";

    Variant::Root event;
    event.write().hash("Information").hash("Flag").setString(flag);
    event.write().hash("Text").setString("Bypass flag set by an external source");
    fillNetworkEvent(event.write());
    archiveEvent(Time::time(), defaultStation, std::move(event));
}

/**
 * Network issued bypass flag clear request.
 * 
 * @param flag  the flag to clear
 */
void AcquisitionController::networkBypassFlagClear(const Data::Variant::Flag &flag)
{
    networkInjectedBypassFlags.erase(flag);

    {
        std::lock_guard<std::mutex> lock(mutex);
        for (const auto &interface : active) {
            interface->activeBypassFlags.erase(flag);
        }
    }

    std::unordered_map<Groups, Data::Variant::Flags, GroupsHash> lookup;
    for (const auto &interface : active) {
        interface->updateGlobalBypass(lookup);
    }

    qCDebug(log_acquisition_controller) << "Network bypass flag" << flag << "cleared";

    Variant::Root event;
    event.write().hash("Information").hash("Flag").setString(flag);
    event.write().hash("Text").setString("Bypass flag cleared by an external source");
    fillNetworkEvent(event.write());
    archiveEvent(Time::time(), defaultStation, std::move(event));
}

/**
 * Network issued request to clear all bypass flags.
 */
void AcquisitionController::networkBypassFlagClearAll()
{
    networkInjectedBypassFlags.clear();

    {
        std::lock_guard<std::mutex> lock(mutex);
        for (const auto &interface : active) {
            interface->activeBypassFlags.clear();
        }
    }

    std::unordered_map<Groups, Data::Variant::Flags, GroupsHash> lookup;
    for (const auto &interface : active) {
        interface->updateGlobalBypass(lookup);
    }

    qCDebug(log_acquisition_controller) << "Network cleared all bypass flags";

    Variant::Root event;
    event.write().hash("Text").setString("All bypass flags cleared by an external source");
    fillNetworkEvent(event.write());
    archiveEvent(Time::time(), defaultStation, std::move(event));
}

/**
 * Network issued system flag set request.
 * 
 * @param flag  the flag to set
 */
void AcquisitionController::networkSystemFlagSet(const Data::Variant::Flag &flag)
{
    networkInjectedSystemFlags.insert(flag);

    {
        std::lock_guard<std::mutex> lock(mutex);
        for (const auto &interface : active) {
            interface->activeSystemFlags.insert(flag);
        }
    }

    std::unordered_map<Groups, Data::Variant::Flags, GroupsHash> lookup;
    for (const auto &interface : active) {
        interface->updateGlobalSystemFlags(lookup);
    }

    qCDebug(log_acquisition_controller) << "Network system flag" << flag << "set";

    Variant::Root event;
    event.write().hash("Information").hash("Flag").setString(flag);
    event.write().hash("Text").setString("System flag set by an external source");
    fillNetworkEvent(event.write());
    archiveEvent(Time::time(), defaultStation, std::move(event));
}

/**
 * Network issued system flag clear request.
 * 
 * @param flag  the flag to clear
 */
void AcquisitionController::networkSystemFlagClear(const Data::Variant::Flag &flag)
{
    networkInjectedSystemFlags.erase(flag);

    {
        std::lock_guard<std::mutex> lock(mutex);
        for (const auto &interface : active) {
            interface->activeSystemFlags.erase(flag);
        }
    }

    std::unordered_map<Groups, Data::Variant::Flags, GroupsHash> lookup;
    for (const auto &interface : active) {
        interface->updateGlobalSystemFlags(lookup);
    }

    qCDebug(log_acquisition_controller) << "Network system flag" << flag << "cleared";

    Variant::Root event;
    event.write().hash("Information").hash("Flag").setString(flag);
    event.write().hash("Text").setString("System flag cleared by an external source");
    fillNetworkEvent(event.write());
    archiveEvent(Time::time(), defaultStation, std::move(event));
}

/**
 * Network issued request to clear all system flags.
 */
void AcquisitionController::networkSystemFlagClearAll()
{
    networkInjectedSystemFlags.clear();

    {
        std::lock_guard<std::mutex> lock(mutex);
        for (const auto &interface : active) {
            interface->activeSystemFlags.clear();
        }
    }

    std::unordered_map<Groups, Data::Variant::Flags, GroupsHash> lookup;
    for (const auto &interface : active) {
        interface->updateGlobalSystemFlags(lookup);
    }

    qCDebug(log_acquisition_controller) << "Network cleared all system flags";

    Variant::Root event;
    event.write().hash("Text").setString("All system flags cleared by an external source");
    fillNetworkEvent(event.write());
    archiveEvent(Time::time(), defaultStation, std::move(event));
}

/**
 * Create the network external interface.
 */
void AcquisitionController::createNetwork()
{
    if (!config.empty())
        network.reset(new AcquisitionNetworkServer(config.back().value()));
    else
        network.reset(new AcquisitionNetworkServer(Variant::Root()));

    network->messageLogEvent.connect([this](const Data::Variant::Root &event) {
        asyncCall(std::bind(&AcquisitionController::networkMessageLogEvent, this, event));
    });
    network->commandReceived
           .connect([this](const std::string &target, const Variant::Root &command) {
               asyncCall(std::bind(&AcquisitionController::networkCommandReceived, this, target,
                                   command));
           });
    network->flushRequest.connect([this](double seconds) {
        asyncCall(std::bind(&AcquisitionController::networkFlushRequest, this, seconds));
    });
    network->setAveragingTime.connect([this](Time::LogicalTimeUnit unit, int count, bool aligned) {
        asyncCall(std::bind(&AcquisitionController::networkSetAveragingTime, this, unit, count,
                            aligned));
    });
    network->dataFlushRequest.connect([this]() {
        asyncCall(std::bind(&AcquisitionController::flushData, this));
    });
    network->bypassFlagSet.connect([this](const Data::Variant::Flag &flag) {
        asyncCall(std::bind(&AcquisitionController::networkBypassFlagSet, this, flag));
    });
    network->bypassFlagClear.connect([this](const Data::Variant::Flag &flag) {
        asyncCall(std::bind(&AcquisitionController::networkBypassFlagClear, this, flag));
    });
    network->bypassFlagClearAll.connect([this]() {
        asyncCall(std::bind(&AcquisitionController::networkBypassFlagClearAll, this));
    });
    network->systemFlagSet.connect([this](const Data::Variant::Flag &flag) {
        asyncCall(std::bind(&AcquisitionController::networkSystemFlagSet, this, flag));
    });
    network->systemFlagClear.connect([this](const Data::Variant::Flag &flag) {
        asyncCall(std::bind(&AcquisitionController::networkSystemFlagClear, this, flag));
    });
    network->systemFlagClearAll.connect([this]() {
        asyncCall(std::bind(&AcquisitionController::networkSystemFlagClearAll, this));
    });
    network->restartRequested.connect([this] {
        restartRequested();
    });
}


AcquisitionController::LoggingIngress::LoggingIngress(ActiveInterface &interface) : interface(
        interface)
{ }

AcquisitionController::LoggingIngress::~LoggingIngress() = default;

void AcquisitionController::LoggingIngress::incomingData(const SequenceValue::Transfer &values)
{ interface.handleLogging(SequenceValue::Transfer(values)); }

void AcquisitionController::LoggingIngress::incomingData(SequenceValue::Transfer &&values)
{ interface.handleLogging(std::move(values)); }

void AcquisitionController::LoggingIngress::incomingData(const SequenceValue &value)
{ interface.handleLogging(SequenceValue(value)); }

void AcquisitionController::LoggingIngress::incomingData(SequenceValue &&value)
{ interface.handleLogging(std::move(value)); }

void AcquisitionController::LoggingIngress::endData()
{ interface.endLogging(); }

AcquisitionController::RealtimeIngress::RealtimeIngress(ActiveInterface &interface) : interface(
        interface)
{ }

AcquisitionController::RealtimeIngress::~RealtimeIngress() = default;

void AcquisitionController::RealtimeIngress::incomingData(const SequenceValue::Transfer &values)
{ interface.handleRealtime(SequenceValue::Transfer(values)); }

void AcquisitionController::RealtimeIngress::incomingData(SequenceValue::Transfer &&values)
{ interface.handleRealtime(std::move(values)); }

void AcquisitionController::RealtimeIngress::incomingData(const SequenceValue &value)
{ interface.handleRealtime(SequenceValue(value)); }

void AcquisitionController::RealtimeIngress::incomingData(SequenceValue &&value)
{ interface.handleRealtime(std::move(value)); }

void AcquisitionController::RealtimeIngress::endData()
{ }

AcquisitionController::PersistentIngress::PersistentIngress(ActiveInterface &interface) : interface(
        interface)
{ }

AcquisitionController::PersistentIngress::~PersistentIngress() = default;

void AcquisitionController::PersistentIngress::incomingData(const SequenceValue::Transfer &values)
{ interface.handlePersistent(SequenceValue::Transfer(values)); }

void AcquisitionController::PersistentIngress::incomingData(SequenceValue::Transfer &&values)
{ interface.handlePersistent(std::move(values)); }

void AcquisitionController::PersistentIngress::incomingData(const SequenceValue &value)
{ interface.handlePersistent(SequenceValue(value)); }

void AcquisitionController::PersistentIngress::incomingData(SequenceValue &&value)
{ interface.handlePersistent(std::move(value)); }

void AcquisitionController::PersistentIngress::endData()
{ }

AcquisitionController::SystemRealtimeIngress::SystemRealtimeIngress(AcquisitionController &controller)
        : controller(controller)
{ }

AcquisitionController::SystemRealtimeIngress::~SystemRealtimeIngress() = default;

void AcquisitionController::SystemRealtimeIngress::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;

    {
        std::lock_guard<std::mutex> lock(controller.realtimeLatestMutex);
        for (const auto &add : values) {
            controller.realtimeLatest.setValue(add.getUnit(), add.root());
            if (!controller.realtimeSeenUnits.contains(add.getUnit())) {
                controller.realtimeSeenUnits |= add.getUnit();
                controller.realtimeNewUnits |= add.getUnit();
            }
        }
        Util::append(values, controller.realtimeDistribute);
    }

    controller.network->incomingRealtime(values);
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::SystemRealtimeIngress::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;

    {
        std::lock_guard<std::mutex> lock(controller.realtimeLatestMutex);
        for (const auto &add : values) {
            controller.realtimeLatest.setValue(add.getUnit(), add.root());
            if (!controller.realtimeSeenUnits.contains(add.getUnit())) {
                controller.realtimeSeenUnits |= add.getUnit();
                controller.realtimeNewUnits |= add.getUnit();
            }
        }
        Util::append(values, controller.realtimeDistribute);
    }

    controller.network->incomingRealtime(std::move(values));
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::SystemRealtimeIngress::incomingData(const SequenceValue &value)
{
    {
        std::lock_guard<std::mutex> lock(controller.realtimeLatestMutex);
        controller.realtimeLatest.setValue(value.getUnit(), value.root());
        if (!controller.realtimeSeenUnits.contains(value.getUnit())) {
            controller.realtimeSeenUnits |= value.getUnit();
            controller.realtimeNewUnits |= value.getUnit();
        }
        controller.realtimeDistribute.emplace_back(value);
    }

    controller.network->incomingRealtime(value);
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::SystemRealtimeIngress::incomingData(SequenceValue &&value)
{
    {
        std::lock_guard<std::mutex> lock(controller.realtimeLatestMutex);
        controller.realtimeLatest.setValue(value.getUnit(), value.root());
        if (!controller.realtimeSeenUnits.contains(value.getUnit())) {
            controller.realtimeSeenUnits |= value.getUnit();
            controller.realtimeNewUnits |= value.getUnit();
        }
        controller.realtimeDistribute.emplace_back(value);
    }

    controller.network->incomingRealtime(std::move(value));
    {
        std::lock_guard<std::mutex> lock(controller.mutex);
        controller.serviceRequired = true;
    }
    controller.request.notify_all();
}

void AcquisitionController::SystemRealtimeIngress::endData()
{ }

AcquisitionController::SystemRealtimeDiscardedIngress::SystemRealtimeDiscardedIngress(
        AcquisitionController &controller) : SystemRealtimeIngress(controller)
{ }

AcquisitionController::SystemRealtimeDiscardedIngress::~SystemRealtimeDiscardedIngress() = default;

void AcquisitionController::SystemRealtimeDiscardedIngress::incomingData(const SequenceValue::Transfer &values)
{
    SequenceValue::Transfer send = values;
    for (auto &v : send) {
        v.setArchive(archiveRTInstant);
    }
    SystemRealtimeIngress::incomingData(std::move(send));
}

void AcquisitionController::SystemRealtimeDiscardedIngress::incomingData(SequenceValue::Transfer &&values)
{
    for (auto &v : values) {
        v.setArchive(archiveRTInstant);
    }
    SystemRealtimeIngress::incomingData(std::move(values));
}

void AcquisitionController::SystemRealtimeDiscardedIngress::incomingData(const SequenceValue &value)
{
    SequenceValue send = value;
    send.setArchive(archiveRTInstant);
    SystemRealtimeIngress::incomingData(std::move(send));
}

void AcquisitionController::SystemRealtimeDiscardedIngress::incomingData(SequenceValue &&value)
{
    value.setArchive(archiveRTInstant);
    SystemRealtimeIngress::incomingData(std::move(value));
}

}
}
