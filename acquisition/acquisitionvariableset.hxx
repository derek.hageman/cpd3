/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITIONVARIABLESET_H
#define CPD3ACQUISITIONVARIABLESET_H

#include "core/first.hxx"

#include <unordered_set>
#include <unordered_map>
#include <QtGlobal>
#include <QObject>
#include <QString>

#include "acquisition/acquisition.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/segment.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "core/range.hxx"
#include "core/stream.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * This is a set of grouped variables in the acquisition interface.  That is,
 * these variables are affected by the same control signals (flavors, flags,
 * and discard).  There is at least one of these for each active acquisition
 * interface, but some may have more than one (e.x. uMACs).
 * <br>
 * This takes care of merging the static metadata overlays as well as the
 * dynamic insertion system flags, flavor changes (including fanout of
 * metadata), and discard (system flush) events.  Essentially a 
 * variable set is connected to each unique grouping of variables for
 * an acquisition interface and updated with the associated system state.  It
 * then directs its output to the realtime handler or the multiplexer for
 * averaging or direct archive insertion.
 */
class CPD3ACQUISITION_EXPORT AcquisitionVariableSet {
    struct MetadataOverlay {
        Data::Variant::Root overlay;
        double start;
        double end;

        MetadataOverlay();

        MetadataOverlay(const MetadataOverlay &);

        MetadataOverlay &operator=(const MetadataOverlay &);

        MetadataOverlay(MetadataOverlay &&);

        MetadataOverlay &operator=(MetadataOverlay &&);

        MetadataOverlay(const MetadataOverlay &other, double start, double end);

        MetadataOverlay(const Data::ValueSegment &other, double start, double end);

        MetadataOverlay(const MetadataOverlay &under, const Data::ValueSegment &over,
                        double start,
                        double end);

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        bool applyOverlay(Data::SequenceValue &value, bool intersecting) const;
    };

    struct VariableOverlay {
        bool discard;
        Data::SequenceName::Flavors flavors;

        VariableOverlay();

        VariableOverlay(const VariableOverlay &);

        VariableOverlay &operator=(const VariableOverlay &);

        VariableOverlay(VariableOverlay &&);

        VariableOverlay &operator=(VariableOverlay &&);

        bool applyOverlay(Data::SequenceValue &value,
                          bool intersecting,
                          bool ignoreDiscard = false) const;
    };

    struct SystemFlagsOverlay {
        Data::Variant::Flags flags;

        SystemFlagsOverlay();

        SystemFlagsOverlay(const SystemFlagsOverlay &);

        SystemFlagsOverlay &operator=(const SystemFlagsOverlay &);

        SystemFlagsOverlay(SystemFlagsOverlay &&);

        SystemFlagsOverlay &operator=(SystemFlagsOverlay &&);

        bool applyOverlay(Data::SequenceValue &value,
                          bool intersecting,
                          bool ignoreDiscard = false) const;
    };

    struct MetadataStreamState {
        std::deque<MetadataOverlay> overlay;
        OverlayStream<Data::SequenceValue, MetadataOverlay> stream;

        bool applyRealtime(Data::SequenceValue &value);
    };

    MetadataStreamState metadataGlobalState;

    struct MetadataVariableData {
        MetadataStreamState state;
        Data::SequenceValue logging;
        Data::Variant::Root realtime;
        bool isFlags;
        bool realtimePersistent;
        Data::SinkMultiplexer::Sink *loggingIngress;

        void updateRealtime(const Data::Variant::Read &update);
    };

    Data::SequenceName::Map<std::unique_ptr<MetadataVariableData>> metadataVariableState;

    struct VariableData {
        MetadataVariableData *metadata;
        Data::Variant::Root realtimePersistentValue;
    };
    Data::SequenceName::Map<std::unique_ptr<VariableData>> dataVariableState;

    struct VariableStreamState {
        VariableOverlay overlay;
        OverlayStream<Data::SequenceValue, VariableOverlay> stream;
        Data::SinkMultiplexer::Sink *loggingIngress;
    };
    VariableStreamState variableState;

    struct SystemFlagsState {
        VariableOverlay variableOverlay;
        OverlayStream<Data::SequenceValue, VariableOverlay> variableStream;
        SystemFlagsOverlay flagsOverlay;
        OverlayStream<Data::SequenceValue, SystemFlagsOverlay> flagsStream;
        Data::SinkMultiplexer::Sink *loggingIngress;
    };
    SystemFlagsState systemFlagsState;

    bool flavorsUpdated;
    std::unordered_set<Data::SequenceName::Flavors, Data::SequenceName::FlavorsHash> allFlavorSets;
    Data::SequenceName::Flavors previousFlavors;
    Data::SequenceName::Flavors activeFlavors;

    bool systemFlagsUpdated;
    Data::Variant::Flags activeSystemFlags;

    bool flushEndTimeUpdated;
    double flushEndTime;

    bool bypassUpdated;
    bool bypassed;

    bool possibleSystemFlagsUpdated;
    Data::Variant::Root possibleSystemFlags;

    bool reinitializeMetadata;

    Data::SinkMultiplexer *loggingMux;
    Data::StreamSink *realtimeEgress;
    Data::StreamSink *realtimeDiscardEgress;
    bool loggingFinished;
    double loggingLastAdvance;

    struct VariableMetadataConfiguration {
        Data::SequenceMatch::Composite selection;
        Data::ValueSegment overlay;
    };

    std::vector<VariableMetadataConfiguration> variableMetadata;

    class DispatchMetadata;

    class DispatchGeneral;

    class DispatchSystemFlags;

    friend class DispatchMetadata;

    friend class DispatchGeneral;

    friend class DispatchSystemFlags;

    class DispatchBase {
    public:
        DispatchBase();

        virtual ~DispatchBase();

        virtual void logging(const Data::SequenceValue &value) const = 0;

        virtual void loggingMetadataGlobalOutput(const Data::SequenceValue &value) const;

        virtual void realtime(const Data::SequenceValue &value,
                              Data::StreamSink *egress,
                              Data::StreamSink *discardEgress) const = 0;

        virtual void realtimeFlavorsFlush(const Data::SequenceName &baseUnit,
                                          const Data::SequenceName::Flavors &previousFlavors,
                                          double time,
                                          Data::StreamSink *egress) const;

        virtual void realtimeFlavorsNew(const Data::SequenceName &baseUnit,
                                        const Data::SequenceName::Flavors &newFlavors,
                                        double time,
                                        Data::StreamSink *egress) const;

        virtual void realtimeResend(const CPD3::Data::SequenceValue &value,
                                    Data::StreamSink *egress,
                                    Data::StreamSink *discardEgress) const;
    };

    class DispatchMetadata : public DispatchBase {
        MetadataStreamState *global;
        MetadataVariableData *data;
        AcquisitionVariableSet *variableSet;
    public:
        DispatchMetadata(MetadataStreamState *g,
                         MetadataVariableData *d,
                         AcquisitionVariableSet *vs);

        virtual ~DispatchMetadata();

        void logging(const Data::SequenceValue &value) const override;

        void loggingMetadataGlobalOutput(const Data::SequenceValue &value) const override;

        void realtimeResend(const CPD3::Data::SequenceValue &value,
                            Data::StreamSink *egress,
                            Data::StreamSink *discardEgress) const override;

        void realtime(const Data::SequenceValue &value,
                      Data::StreamSink *egress,
                      Data::StreamSink *discardEgress) const override;

        void realtimeFlavorsNew(const Data::SequenceName &baseUnit,
                                const Data::SequenceName::Flavors &newFlavors,
                                double time,
                                Data::StreamSink *egress) const override;
    };

    class DispatchGeneral : public DispatchBase {
        VariableStreamState *state;
        VariableData *data;
        MetadataVariableData *metadata;
    public:
        DispatchGeneral(VariableStreamState *s, VariableData *d, MetadataVariableData *meta);

        virtual ~DispatchGeneral();

        void logging(const Data::SequenceValue &value) const override;

        void realtime(const Data::SequenceValue &value,
                      Data::StreamSink *egress,
                      Data::StreamSink *discardEgress) const override;

        void realtimeFlavorsFlush(const Data::SequenceName &baseUnit,
                                  const Data::SequenceName::Flavors &previousFlavors,
                                  double time,
                                  Data::StreamSink *egress) const override;
    };

    class DispatchSystemFlags : public DispatchBase {
        SystemFlagsState *state;
        VariableData *data;
        MetadataVariableData *metadata;
    public:
        DispatchSystemFlags(SystemFlagsState *s, VariableData *d, MetadataVariableData *meta);

        virtual ~DispatchSystemFlags();

        void logging(const Data::SequenceValue &value) const override;

        void realtime(const Data::SequenceValue &value,
                      Data::StreamSink *egress,
                      Data::StreamSink *discardEgress) const override;

        void realtimeFlavorsFlush(const Data::SequenceName &baseUnit,
                                  const Data::SequenceName::Flavors &previousFlavors,
                                  double time,
                                  Data::StreamSink *egress) const override;
    };

    Data::SequenceName::Map<std::unique_ptr<DispatchBase>> loggingDispatch;
    Data::SequenceName::Map<std::unique_ptr<DispatchBase>> realtimeDispatch;

    Data::Variant::Root metadataProcessing;
    QString sourceName;

#ifndef NDEBUG
    double lastMultiplexerSizeCheck;
#endif

    void finalizeMetadataOverlay(MetadataStreamState &target);

    MetadataVariableData *lookupMetadataVariableData(const Data::SequenceName &unit);

    VariableData *lookupVariableData(const Data::SequenceName &unit);

    DispatchBase *createDispatch(Data::SequenceName::Map<std::unique_ptr<DispatchBase>> &dispatch,
                                 const Data::SequenceName &unit);

    void applyDynamicMetadata(Data::SequenceValue &meta) const;

    void applyDynamicMetadata(Data::SequenceValue::Transfer &meta) const;

public:
    /**
     * Create a new variable set.
     * 
     * @param loggingOutput the data multiplexer used to output logging data
     * @param config        the configuration of the variable set (generally the instrument configuration)
     * @param processing    the processing metadata to add to all variables
     * @param globalOverlay the metadata to overlay on all variables
     * @param sourceName    the source name suffix
     */
    AcquisitionVariableSet(Data::SinkMultiplexer *loggingOutput,
                           const Data::ValueSegment::Transfer &config,
                           const Data::Variant::Read &processing = Data::Variant::Read::empty(),
                           const Data::Variant::Read &globalOverlay = Data::Variant::Read::empty(),
                           const QString &sourceName = QString());

    ~AcquisitionVariableSet();

	AcquisitionVariableSet() = delete;

	AcquisitionVariableSet(const AcquisitionVariableSet &) = delete;

	AcquisitionVariableSet &operator=(const AcquisitionVariableSet &) = delete;

    /**
     * Advance the time to the given value.  This should only be called
     * after the appropriate sets (which will be applied at the given time).
     * The lists of data are processed and dispatched with this call.
     * 
     * @param time      the current time
     * @param logging   the list of logging values to process
     * @param realtime  the list of realtime values to process
     * @param loggingAdvanceTime the time advance the logging interface to (at least)
     */
    void process(double time,
                 const Data::SequenceValue::Transfer &logging,
                 const Data::SequenceValue::Transfer &realtime,
                 double loggingAdvanceTime = FP::undefined());

    /**
     * Finish handling of all data.
     * 
     * @param finishRealtime if set then end the realtime stream as well
     */
    void finish(bool finishRealtime = true);

    /**
     * Set the flavors of the variable set.  All variables in the stream will
     * be re-assigned as needed.
     * 
     * @param flavors   the flavors of the stream
     */
    void setFlavors(const Data::SequenceName::Flavors &flavors);

    /**
     * Set the system flags of the stream.
     * 
     * @param flags     the system flags to be added
     */
    void setSystemFlags(const Data::Variant::Flags &flags);

    /**
     * Set the time that the system flush ends at.
     * 
     * @param time      the end of the flush
     */
    void setFlushUntil(double time);

    /**
     * Set if the system is currently bypassed (data discarded).
     * 
     * @param b         true if the system is bypassed
     */
    void setBypassed(bool b);

    /**
     * Set the metadata overlay of possible system flags.
     * 
     * @param metadata  the metadata description of possible additional flags
     */
    void setPossibleSystemFlags(const Data::Variant::Read &metadata);

    /**
     * Resend a copy of the last logging metadata for all outputs.  This
     * is usually used when the backend output (read: averaging) has
     * changed, so the new one gets a copy.
     */
    void resendLoggingMetadata();

    /**
     * Set the egress point for realtime data.  This is generally the egress
     * provided by the averager or directly to the output target.
     * 
     * @param egress    the target egress
     * @return          the old egress
     */
    Data::StreamSink *setRealtimeEgress(Data::StreamSink *egress);

    /**
     * Set the egress point for realtime data that would otherwise be
     * discarded.  This is used to allow bypassing the averaging for 
     * instantaneous values during flushing, for example.  This is generally
     * connected directly to the output target.
     * 
     * @param egress    the target egress
     * @return          the old egress
     */
    Data::StreamSink *setRealtimeDiscardEgress(Data::StreamSink *egress);

    /**
     * Apply a value overlay.  This attempts to preserve the type of the 
     * existing value as much as possible.
     * 
     * @param under     the input and output value
     * @param over      the value being overlaid
     */
    static void applyValueOverlay(Data::SequenceValue &under, const Data::Variant::Root &over);
};

}
}

#endif
