/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITIONREALTIMELAYOUT_H
#define CPD3ACQUISITIONREALTIMELAYOUT_H

#include "core/first.hxx"

#include <vector>
#include <unordered_map>
#include <QtGlobal>
#include <QObject>
#include <QHash>
#include <QString>

#include "acquisition/acquisition.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * A class that provides a system for laying out realtime data values as
 * text in a rows and columns type format.  This is generally used by both
 * the text interface and the GUI interface for dynamically constructing
 * the instrument specific windows.
 * <br>
 * Note that this is NOT thread safe which is why it doesn't actually
 * inhert from Data::StreamSink even though
 * it basically implements the same API.
 */
class CPD3ACQUISITION_EXPORT RealtimeLayout {
    bool console;

    friend struct ValueData;
    struct FanoutData;

    struct ValueData {
        RealtimeLayout *parent;
        Data::SequenceName unit;
        FanoutData *fanout;

        Data::Variant::Root metadata;
        Data::Variant::Root latest;

        ValueData(RealtimeLayout *p, const Data::SequenceName &u, FanoutData *fd);

        void updateMetadata(const Data::Variant::Root &update);

        void updateLatest(const Data::Variant::Root &update);
    };

    Data::SequenceName::Map<std::unique_ptr<ValueData>> values;

    ValueData *lookupValueData(const Data::SequenceName &unit);

    struct FanoutData {
        bool updated;
        std::vector<ValueData *> sources;
    };
    Data::SequenceName::Map<std::unique_ptr<FanoutData>> fanoutTracking;

    FanoutData *lookupFanoutData(const Data::SequenceName &unit);

    class Dispatch {
    public:
        virtual ~Dispatch();

        virtual void incomingData(const Data::SequenceValue &value) = 0;
    };

    friend class DispatchValue;

    class DispatchValue : public Dispatch {
        ValueData *target;
    public:
        DispatchValue(ValueData *t);

        virtual ~DispatchValue();

        virtual void incomingData(const Data::SequenceValue &value);
    };

    friend class DispatchMetadata;

    class DispatchMetadata : public Dispatch {
        ValueData *target;
    public:
        DispatchMetadata(ValueData *t);

        virtual ~DispatchMetadata();

        virtual void incomingData(const Data::SequenceValue &value);
    };

    class DispatchDiscard : public Dispatch {
    public:
        DispatchDiscard();

        virtual ~DispatchDiscard();

        virtual void incomingData(const Data::SequenceValue &value);
    };

    Data::SequenceName::Map<std::unique_ptr<Dispatch>> dispatch;

    bool layoutOutdated;

    friend struct LayoutFormatter;

    struct LayoutFormatter {
        RealtimeLayout *parent;
        bool canCache;
        QString lastResult;

        FanoutData *fanout;
        Data::SequenceName unit;
        Data::Variant::Path path;
        Data::Variant::Type previousType;
        NumberFormat format;

        std::unordered_map<std::string, std::uint_fast64_t> flagsTranslation;
        std::unordered_map<std::string, QString> stringTranslation;
        QString hashFormat;

        QString prefix;

        LayoutFormatter();

        QString generate(bool *valid = NULL);
    };

public:
    enum LayoutItemType {
        /** A box of grouped values.  This may have a title (to be
         * rendered at the top) or it may not, which is just used
         * to separate groups of values. */
                GroupBox = 0, /** The final group box. */
        FinalGroupBox, /** A left justified text label. */
        LeftJustifiedLabel, /** A center justified text label. */
        CenteredLabel, /** A right justified text label. */
        RightJustifiedLabel, /** A single value text.  This should generally be rendered using
         * a fixed width font. */
        ValueText, /** A whole line of dynamic text. */
        ValueLine,
    };
private:

    friend struct LayoutElement;

    struct LayoutElement {
        LayoutItemType type;
        int uid;
        int row;
        int column;
        QString text;

        std::unique_ptr<LayoutFormatter> origin;

        LayoutElement();

        ~LayoutElement();

        LayoutElement(const LayoutElement &other);

        LayoutElement &operator=(const LayoutElement &other);

        LayoutElement(LayoutElement &&other);

        LayoutElement &operator=(LayoutElement &&other);
    };

    std::vector<std::vector<LayoutElement>> layout;

    friend struct PageBreakdownData;

    struct PageBreakdownData {
        ValueData *value;
        Data::Variant::Read realtime;

        PageBreakdownData();

        bool operator<(const PageBreakdownData &other) const;

        QString getFormatString() const;

        int getExpectedWidth() const;

        int getIntegerWidth() const;

        QString getUnitText() const;

        QString getGroupLabel() const;

        QString getArrayLabel() const;

        QString getLabel() const;

        LayoutFormatter *createFormatter(int width,
                                         const Data::Variant::Path &path = Data::Variant::Path()) const;
    };

    friend struct BoxBreakdownData;

    struct BoxBreakdownData {
        std::unordered_map<QString, std::vector<PageBreakdownData> > combinedColumnData;
        std::vector<QString> combinedColumnOrder;

        std::vector<PageBreakdownData> arrayColumns;
        std::vector<std::vector<PageBreakdownData> > rowData;
        std::vector<PageBreakdownData> fullLineData;
        QString boxTitle;
        std::string boxID;
        bool definedBox;

        qint64 activeRowOrder;

        void addPageItem(const PageBreakdownData &add);

        int calculateRows(int dataColumns) const;

        void calculateColumns(std::vector<int> &widths, int dataColumns) const;

        void calculateFormatColumns(std::vector<int> &widths, int dataColumns) const;

        int getMinimumGlobalWidth() const;

        void generateBoxData(std::vector<LayoutElement> &output, int &uid, int dataColumns) const;
    };

    void generatePage(std::vector<LayoutElement> &output,
                      const std::vector<PageBreakdownData> &input) const;

public:
    /**
     * Create a new layout engine.
     * 
     * @param consoleMode   if set then use console mode settings (e.x. pure ASCII units)
     * @param parent        the parent QObject
     */
    RealtimeLayout(bool consoleMode = false);

    RealtimeLayout(const RealtimeLayout &) = delete;

    RealtimeLayout &operator=(const RealtimeLayout &) = delete;

    virtual ~RealtimeLayout();

    /**
     * Handle incoming data values.
     * 
     * @param values    the list of incoming values
     */
    void incomingData(const Data::SequenceValue::Transfer &values);

    /**
     * Handle a single incoming data value.
     * 
     * @param value     the incoming value
     */
    void incomingData(const Data::SequenceValue &value);

    /**
     * Update the layout.  Returns true if the layout was recalculated
     * and thus any cached widgets should be discarded.
     * 
     * @return true if the layout has been recalculated
     */
    bool update();

    /**
     * Get the number of pages in the layout.
     * 
     * @return the number of pages in the layout
     */
    inline int pages() const
    { return layout.size(); }

    friend class iterator;

    class CPD3ACQUISITION_EXPORT iterator {
        friend class RealtimeLayout;

        std::vector<LayoutElement>::const_iterator element;

        iterator(std::vector<LayoutElement>::const_iterator e) : element(e)
        { }

    public:
        inline iterator(const iterator &other) : element(other.element)
        { }

        inline iterator &operator=(const iterator &other)
        {
            element = other.element;
            return *this;
        }

        inline bool operator==(const iterator &other)
        { return element == other.element; }

        inline bool operator!=(const iterator &other)
        { return element != other.element; }

        inline iterator &operator++()
        {
            ++element;
            return *this;
        }

        inline iterator operator++(int)
        {
            iterator result(*this);
            ++element;
            return result;
        }

        inline iterator &operator+=(int n)
        {
            element += n;
            return *this;
        }

        inline iterator operator+(int n)
        { return iterator(element + n); }

        inline iterator &operator--()
        {
            --element;
            return *this;
        }

        inline iterator operator--(int)
        {
            iterator result(*this);
            --element;
            return result;
        }

        inline iterator &operator-=(int n)
        {
            element -= n;
            return *this;
        }

        inline iterator operator-(int n)
        { return iterator(element - n); }

        /**
         * Get the type of the item the iterator points to.
         * 
         * @return the iterator item type
         */
        inline LayoutItemType type() const
        { return element->type; }

        /**
         * Get the row of the item the iterator points to.
         * 
         * @return the iterator item row
         */
        inline int row() const
        { return element->row; }

        /**
         * Get the column of the item the iterator points to.
         * 
         * @return the iterator item column
         */
        inline int column() const
        { return element->column; }

        /**
         * Get the test of the item the iterator points to.
         * 
         * @return the iterator item text
         */
        inline QString text() const
        { return element->text; }

        /**
         * Get the UID of the iterator item.  This UID is unique in the page
         * and can be used to identify persistent widgets for updating (e.x.
         * labels for text).  The UID is always >= 0 (suitable for list 
         * indexing).
         * 
         * @return the iterator UID
         */
        inline int uid() const
        { return element->uid; }
    };

    inline iterator begin(int page = 0) const
    { return iterator(layout[page].begin()); }

    inline iterator end(int page = 0) const
    { return iterator(layout[page].end()); }

    /**
     * Convert the raw unit string to something more logical for console output.
     * This is intended to strip out strange unicode characters that the
     * standard console can't display.
     * 
     * @param input     the input units string
     * @return          the converted units string
     */
    static QString convertConsoleUnits(const QString &input);

    /**
     * Remove console string escapes (e.x. '&' for hotkeys).
     * 
     * @param input     the input string
     * @return          the de-escaped string
     */
    static QString removeConsoleEscapes(QString input);

    /**
     * Apply a hash format string.
     * 
     * @param format    the format specification string
     * @param value     the hash value
     * @param translation the additional lookup for string translation
     * @return          the string created by applying the format
     */
    static QString hashFormat(const QString &format,
                              const Data::Variant::Read &value = Data::Variant::Read::empty(),
                              const std::unordered_map<std::string,
                                                       QString> &translation = std::unordered_map<
                                      std::string, QString>());
};

}
}

#endif
