/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3ACQUISITION_IOTARGET_HXX
#define CPD3ACQUISITION_IOTARGET_HXX

#include "core/first.hxx"

#include <vector>
#include <memory>

#include "acquisition.hxx"
#include "core/util.hxx"
#include "io/access.hxx"
#include "io/socket.hxx"
#include "io/drivers/localsocket.hxx"
#include "io/drivers/tcp.hxx"
#include "datacore/variant/root.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * A description of an interface target for acquisition IO.
 */
class CPD3ACQUISITION_EXPORT IOTarget {
    std::unique_ptr<IO::Socket::Local::Configuration> localSocket;

    std::unique_ptr<IO::Socket::TCP::Configuration> tcpSocket;
    std::vector<std::string> tcpListen;
public:
    virtual ~IOTarget();

    /**
     * Create a target from a description string.
     *
     * @param description   the string description
     * @return              a new IO target
     */
    static std::unique_ptr<IOTarget> create(const std::string &description);

    /**
     * Create a target from a configuration.
     *
     * @param configuration the configuration
     * @return              a new IO target
     */
    static std::unique_ptr<IOTarget> create(const Data::Variant::Read &configuration);

    /**
     * Get targets for all locally detected interfaces (generally serial ports)
     *
     * @return  targets for all local interfaces
     */
    static std::vector<std::unique_ptr<IOTarget>> enumerateLocal();


    /**
     * Test if the target will produce a valid connection.
     */
    virtual bool isValid() const;

    /**
     * Test if the device should re-trigger autoprobing when it is closed.  That is, test
     * if this target is a local interface subject to external close sources (e.g. a serial
     * port due to kernel reconfiguration).
     *
     * @return  true if the interface should re-run autoprobing when it is closed
     */
    virtual bool triggerAutoprobeWhenClosed() const;

    /**
     * Test if the device has a concept of integrated framing that should be passed on to
     * upper layers.  For example, UDP packets as complete frames.
     *
     * @return  true if the interface should indicate that it has a native framing
     */
    virtual bool integratedFraming() const;


    /**
     * Test if the interface matches (with respect to the backing device) another
     * interface.
     *
     * @param other the other interface
     * @return      true if the backing devices match
     */
    virtual bool matchesDevice(const IOTarget &other) const = 0;

    /**
     * Create a copy of the interface.
     *
     * @param deviceOnly    only copy the device and none of the configuration parameters
     * @return              a copy of the interface
     */
    virtual std::unique_ptr<IOTarget> clone(bool deviceOnly = false) const = 0;

    /**
     * A device used to interface with the target.
     */
    class CPD3ACQUISITION_EXPORT Device : public IO::Generic::Stream {
    public:
        Device();

        virtual ~Device();

        bool isEnded() const override;

        void start() override;

        /**
         * Reset the device (e.g. reconfigure a serial port).
         */
        virtual void reset();

        /**
         * Emitted when control data is received by the device (e.g. via a separate eavesdropper
         * serial port).
         */
        Threading::Signal<const Util::ByteArray &> control;
    };

    virtual std::unique_ptr<Device> backingDevice() const = 0;

    /**
     * Merge and apply the stream to a device (if provided).
     *
     * @param overlay   the interface description to overlay
     * @param device    the device to change (if any)
     */
    virtual void merge(const IOTarget &overlay, Device *device = nullptr);


    /**
     * Get a serialized version of the interface.
     *
     * @return      the serialized representation of the interface
     */
    virtual Util::ByteArray serialize() const = 0;

    /**
     * Reconstruct an interface target from serialized data.
     *
     * @param data  the serialized data
     * @return      the reconstructed interface (if any)
     */
    static std::unique_ptr<IOTarget> deserialize(const Util::ByteView &data);


    /**
     * Get a human readable description of the interface target.
     *
     * @param deviceOnly    decribe the device only
     * @return              the interface description
     */
    virtual QString description(bool deviceOnly = false) const = 0;

    /**
     * Get the configuration of the interface target.
     *
     * @param deviceOnly    only return the configuration for the device
     * @return              the interface target configuration
     */
    virtual Data::Variant::Root configuration(bool deviceOnly = false) const = 0;


    /**
     * Get the client connection for to interface target or null if connection
     * fails.
     *
     * @param localOnly     only attempt local connections
     * @return              the client connection
     */
    virtual std::unique_ptr<IO::Socket::Connection> clientConnection(bool localOnly = false) const;

    /**
     * Test if a server should be spawned for the interface.
     *
     * @return true if a server should be spawned when required
     */
    virtual bool spawnServer() const;

    /**
     * Get the server listeners for the interface.
     *
     * @param connection    the function to call on a new connection
     * @param removeLocal   remove the existing local socket if any
     * @return              the servers that are listening
     */
    std::vector<std::unique_ptr<
            IO::Socket::Server>> serverListeners(const IO::Socket::Server::IncomingConnection &connection,
                                                 bool removeLocal = true) const;

    /**
     * Get a UID of raw data that identifies the interface for a unique socket.
     *
     * @return              raw data uniquely identifying the target
     */
    virtual Util::ByteArray localSocketUID() const = 0;

protected:
    IOTarget();

    IOTarget(const IOTarget &other);

    explicit IOTarget(const Data::Variant::Read &configuration);
};

}
}

#endif //CPD3ACQUISITION_IOTARGET_HXX
