/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVector>

#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"
#include "core/csv.hxx"
#include "core/timeparse.hxx"

using namespace CPD3::Data;


namespace CPD3 {
namespace Acquisition {

/** @file acquisition/lineingress.hxx
 * Wrapper implementations for line driven instruments to parse plain
 * incoming data.
 */

enum {
    Mode_CSV_OneField, Mode_CSV_TwoField, Mode_CPD1_RawData, Mode_CPD2_RawData,

    Mode_PassThrough,
};


LineIngressWrapper::~LineIngressWrapper()
{
    interface.reset();
}

LineIngressWrapper::LineIngressWrapper(std::unique_ptr<AcquisitionInterface> &&setInterface,
                                       const ComponentOptions &options)
        : processBuffer(),
          pendingValues(),
          processValues(),
          endState(Running),
          suffix(),
          station("nil"),
          outputTransform(),
          filter(),
          interface(std::move(setInterface))
{
    Q_ASSERT(interface.get() != nullptr);

    filter.reset(new OutputFilter(this));
    interface->setLoggingEgress(filter.get());

    if (options.isSet("station")) {
        station = qobject_cast<ComponentOptionSingleString *>(options.get("station"))->get()
                                                                                     .toStdString();
    }

    if (options.isSet("suffix")) {
        suffix = qobject_cast<ComponentOptionSingleString *>(options.get("suffix"))->get()
                                                                                   .toStdString();
    } else {
        suffix = interface->getDefaults().name;
        Util::replace_all(suffix, "$1", "1");
        Util::replace_all(suffix, "$2", "1");
    }

    interface->completed.connect(std::bind(&LineIngressWrapper::interfaceCompleted, this));
}

void LineIngressWrapper::runValueProcessing(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;

    for (auto &v : values) {
        auto tr = outputTransform.find(v.getUnit());
        if (tr == outputTransform.end()) {
            OutputTransform add;
            add.unit = v.getUnit();

            if (add.unit.getStation().empty()) {
                add.unit.setStation(station);
            }

            if (!Util::contains(add.unit.getVariable(), '_')) {
                auto var = add.unit.getVariable();
                var += "_";
                var += suffix;
                add.unit.setVariable(std::move(var));
            }

            if (add.unit.isMeta()) {
                add.metadataHandling = true;
            } else {
                add.metadataHandling = false;
            }

            tr = outputTransform.emplace(v.getUnit(), std::move(add)).first;
        }

        v.setUnit(tr->second.unit);
        if (tr->second.metadataHandling) {
            v.write().metadata("Realtime").remove(false);

            auto parameters = v.write().metadata("Smoothing").hash("Parameters");
            if (parameters.getType() == Variant::Type::Hash) {
                /* This may need better logic if the smoothing parameters ever
                 * require strings that aren't variable names */
                for (auto mod : parameters.toHash()) {
                    if (mod.second.getType() != Variant::Type::String)
                        continue;
                    auto var = mod.second.toString();
                    if (var.empty() || Util::contains(var, '_'))
                        continue;
                    var += "_";
                    var += suffix;
                    mod.second.setString(var);
                }
            }
        }
    }
    outputData(std::move(values));
}

bool LineIngressWrapper::prepare()
{
    /* Don't copy out of buffers while we can't output, so the limiting
     * works while paused */
    if (egress == nullptr && !dataEnded)
        return false;

    bool runProcess = false;
    if (!buffer.empty()) {
        runProcess = true;
        processBuffer += buffer.toQByteArrayRef();
        buffer.clear();
    }
    if (!pendingValues.empty()) {
        runProcess = true;
        Util::append(std::move(pendingValues), processValues);
        pendingValues.clear();
    }
    if (runProcess)
        return true;

    if (dataEnded) {
        switch (endState) {
        case Running:
            endState = SendEnd;
            return true;
        case AwaitingInterfaceEnd:
            break;
        case SendEnd:
        case Ended:
            return true;
        }
        if (endCheckPending) {
            endCheckPending = false;
            return true;
        }
    }

    return false;
}

bool LineIngressWrapper::process()
{
    if (!processValues.empty()) {
        runValueProcessing(std::move(processValues));
        processValues.clear();
    }
    releaseEgressLock();

    if (!processBuffer.isEmpty()) {
        processData(processBuffer);
        processBuffer.clear();
    }

    switch (endState) {
    case Running:
        break;
    case SendEnd:

        signalEnd();
        interface->initiateShutdown();
        endState = AwaitingInterfaceEnd;

        /* Fall through */
    case AwaitingInterfaceEnd:
        if (interface->isCompleted()) {
            endState = Ended;
            /* Run again so we do the final processing */
        }
        break;
    case Ended:
        return false;
    }
    return true;
}

bool LineIngressWrapper::begin()
{
    interface->start();
    return true;
}

void LineIngressWrapper::finalize()
{
    interface->initiateShutdown();
    interface->completed
             .waitInEventLoop(std::bind(&AcquisitionInterface::isCompleted, interface.get()));

    auto lock = acquireLock();
    Util::append(std::move(pendingValues), processValues);
    pendingValues.clear();
}

void LineIngressWrapper::finish()
{
    if (egress) {
        runValueProcessing(std::move(processValues));
        processValues.clear();
    }
}

void LineIngressWrapper::interfaceCompleted()
{
    auto lock = acquireLock();
    endCheckPending = true;
    externalNotify();
}

LineIngressWrapper::OutputFilter::OutputFilter(LineIngressWrapper *setWrapper) : wrapper(setWrapper)
{ }

LineIngressWrapper::OutputFilter::~OutputFilter() = default;

void LineIngressWrapper::OutputFilter::incomingData(const SequenceValue::Transfer &v)
{
    auto lock = wrapper->acquireLock();
    Util::append(v, wrapper->pendingValues);
    wrapper->externalNotify();
}

void LineIngressWrapper::OutputFilter::incomingData(SequenceValue::Transfer &&v)
{
    auto lock = wrapper->acquireLock();
    Util::append(std::move(v), wrapper->pendingValues);
    wrapper->externalNotify();
}

void LineIngressWrapper::OutputFilter::incomingData(const SequenceValue &v)
{
    auto lock = wrapper->acquireLock();
    wrapper->pendingValues.emplace_back(v);
    wrapper->externalNotify();
}

void LineIngressWrapper::OutputFilter::incomingData(SequenceValue &&v)
{
    auto lock = wrapper->acquireLock();
    wrapper->pendingValues.emplace_back(std::move(v));
    wrapper->externalNotify();
}

void LineIngressWrapper::OutputFilter::endData()
{ }


namespace {

class LineIngressWrapperRaw : public LineIngressWrapper {
protected:
    void processData(const QByteArray &data) override
    {
        interface->incomingData(data, FP::undefined(),
                                AcquisitionInterface::IncomingDataType::Stream);
    }

    void signalEnd() override
    {
        interface->incomingData(QByteArray(), FP::undefined(),
                                AcquisitionInterface::IncomingDataType::Final);
    }

public:
    LineIngressWrapperRaw(std::unique_ptr<AcquisitionInterface> &&setInterface,
                          const ComponentOptions &options) : LineIngressWrapper(
            std::move(setInterface), options)
    { }

    virtual ~LineIngressWrapperRaw()
    { }
};


class LineIngressWrapperLineGeneral : public LineIngressWrapper {
    QByteArray buffer;

    void processAll()
    {
        if (buffer.isEmpty())
            return;
        const char *first = buffer.constData();
        int length = buffer.size();
        const char *start = first;
        while (length > 0) {
            for (; length > 0 && (*start == '\n' || *start == '\r'); --length, start++) { }
            if (length <= 1) break;
            const char *end = start + 1;
            for (--length; length > 0 && *end != '\n' && *end != '\r'; --length, end++) { }
            if (length <= 0)
                break;
            if (*end != '\n' && *end != '\r')
                break;

            processLine(QByteArray(start, end - start));

            start = end + 1;
            length--;
        }

        if (start >= first + buffer.size()) {
            buffer.clear();
        } else if (start != first) {
            length = buffer.size() - (start - first);
            memmove(buffer.data(), start, length);
            buffer.resize(length);
        }
    }

protected:
    virtual void processLine(const QByteArray &line) = 0;

    void processData(const QByteArray &data) override
    {
        if (buffer.isEmpty()) {
            buffer = data;
        } else {
            buffer.append(data);
        }
        processAll();
    }

    void signalEnd() override
    {
        if (!buffer.isEmpty() && !buffer.endsWith('\n') && !buffer.endsWith('\r'))
            buffer.append('\n');
        processAll();
    }

public:
    LineIngressWrapperLineGeneral(std::unique_ptr<AcquisitionInterface> &&setInterface,
                                  const ComponentOptions &options) : LineIngressWrapper(
            std::move(setInterface), options)
    { }

    virtual ~LineIngressWrapperLineGeneral()
    { }
};

class LineIngressWrapperCSV : public LineIngressWrapperLineGeneral {
    QVector<int> timeIndices;
    int dataIndex;
    bool allowUndefinedTime;
protected:
    void processLine(const QByteArray &line) override
    {
        QStringList fields(CSV::split(QString(line)));

        double time = FP::undefined();
        if (!timeIndices.isEmpty()) {
            QStringList timeFieldData;
            for (QVector<int>::const_iterator it = timeIndices.constBegin(),
                    end = timeIndices.constEnd(); it != end; ++it) {
                if (*it < 0 || *it >= fields.size())
                    continue;
                timeFieldData.append(fields.at(*it));
            }
            if (!timeFieldData.isEmpty()) {
                try {
                    time = TimeParse::parseListSingle(timeFieldData);
                } catch (TimeParsingException tpe) {
                    if (timeFieldData.at(0) == "Year")
                        return;
                    time = FP::undefined();
                }
            }
        }
        if (!allowUndefinedTime && !FP::defined(time))
            return;

        if (dataIndex < 0 || dataIndex >= fields.size())
            return;

        interface->incomingData(fields.at(dataIndex).toLatin1(), time,
                                AcquisitionInterface::IncomingDataType::Complete);
    }

public:
    LineIngressWrapperCSV(std::unique_ptr<AcquisitionInterface> &&setInterface,
                          const ComponentOptions &options,
                          const QVector<int> &setTimeIndices,
                          int setDataIndex,
                          bool setAllowUndefinedTime) : LineIngressWrapperLineGeneral(
            std::move(setInterface), options),
                                                        timeIndices(setTimeIndices),
                                                        dataIndex(setDataIndex),
                                                        allowUndefinedTime(setAllowUndefinedTime)
    { }

    virtual ~LineIngressWrapperCSV()
    { }
};

class LineIngressWrapperCSVTail : public LineIngressWrapperLineGeneral {
    QVector<int> timeIndices;
    int dataIndex;
    bool allowUndefinedTime;
protected:
    void processLine(const QByteArray &line) override
    {
        if (dataIndex < 0 || line.isEmpty())
            return;

        QStringList fields;
        const char *start = line.constData();
        int length = line.size();
        for (int i = 0; i < dataIndex; i++) {
            const char *end = start + 1;
            for (; length > 0 && *end != ','; ++end, --length) { }
            fields.append(QString::fromLatin1(start, end - start));

            start = end + 1;
            length--;
        }
        length = line.constData() + line.size() - start;
        if (length <= 0)
            return;
        fields.append(QString::fromLatin1(start, length));

        double time = FP::undefined();
        if (!timeIndices.isEmpty()) {
            QStringList timeFieldData;
            for (QVector<int>::const_iterator it = timeIndices.constBegin(),
                    end = timeIndices.constEnd(); it != end; ++it) {
                if (*it < 0 || *it >= fields.size())
                    continue;
                timeFieldData.append(fields.at(*it));
            }
            if (!timeFieldData.isEmpty()) {
                try {
                    time = TimeParse::parseListSingle(timeFieldData);
                } catch (TimeParsingException tpe) {
                    if (timeFieldData.at(0) == "Year")
                        return;
                    time = FP::undefined();
                }
            }
        }
        if (!allowUndefinedTime && !FP::defined(time))
            return;

        interface->incomingData(fields.last().toLatin1(), time,
                                AcquisitionInterface::IncomingDataType::Complete);
    }

public:
    LineIngressWrapperCSVTail(std::unique_ptr<AcquisitionInterface> &&setInterface,
                              const ComponentOptions &options,
                              const QVector<int> &setTimeIndices,
                              int setDataIndex,
                              bool setAllowUndefinedTime) : LineIngressWrapperLineGeneral(
            std::move(setInterface), options),
                                                            timeIndices(setTimeIndices),
                                                            dataIndex(setDataIndex),
                                                            allowUndefinedTime(
                                                                    setAllowUndefinedTime)
    { }

    virtual ~LineIngressWrapperCSVTail()
    { }
};

class LineIngressWrapperCPD2 : public LineIngressWrapperLineGeneral {

    bool inHeader;

    struct ParseData {
        int epochIndex;
        int dataIndex;
        QString epochMVC;
        QString dataMVC;
        QStringList mvcs;

        ParseData() : epochIndex(-1), dataIndex(-1)
        { }
    };

    QHash<QString, ParseData> parsing;
protected:
    void processLine(const QByteArray &line) override
    {
        if (inHeader) {
            if (line.startsWith('!')) {
                QStringList fields(QString(line).split(','));
                if (fields.size() < 2)
                    return;
                QString key(fields.at(0));
                QString value(fields.at(1));

                if (key.startsWith("!row;colhdr;")) {
                    QString record(key.mid(12));
                    QStringList names(value.split(';'));

                    for (int i = 0, max = names.size(); i < max; i++) {
                        if (names.at(i) == "EPOCH") {
                            parsing[record].epochIndex = i;
                            if (i < parsing[record].mvcs.size()) {
                                parsing[record].epochMVC = parsing[record].mvcs.at(i);
                            } else {
                                parsing[record].epochMVC.clear();
                            }
                        } else if (names.at(i).startsWith("ZReport_")) {
                            parsing[record].dataIndex = i;
                            if (i < parsing[record].mvcs.size()) {
                                parsing[record].dataMVC = parsing[record].mvcs.at(i);
                            } else {
                                parsing[record].dataMVC.clear();
                            }
                        }
                    }

                } else if (key.startsWith("!row;mvc;")) {
                    QString record(key.mid(9));
                    parsing[record].mvcs = value.split(';');
                    if (parsing[record].epochIndex >= 0 &&
                            parsing[record].epochIndex < parsing[record].mvcs.size()) {
                        parsing[record].epochMVC =
                                parsing[record].mvcs.at(parsing[record].epochIndex);
                    } else {
                        parsing[record].epochMVC.clear();
                    }
                    if (parsing[record].dataIndex >= 0 &&
                            parsing[record].dataIndex < parsing[record].mvcs.size()) {
                        parsing[record].dataMVC =
                                parsing[record].mvcs.at(parsing[record].dataIndex);
                    } else {
                        parsing[record].dataMVC.clear();
                    }
                }

                return;
            } else {
                inHeader = false;

                for (QHash<QString, ParseData>::iterator check = parsing.begin();
                        check != parsing.end();) {
                    if (check.value().epochIndex < 0 || check.value().dataIndex < 0) {
                        check = parsing.erase(check);
                        continue;
                    }
                    ++check;
                }
            }
        }

        QStringList fields(CSV::split(QString(line)));
        if (fields.isEmpty())
            return;
        QHash<QString, ParseData>::const_iterator p = parsing.constFind(fields.at(0));
        if (p == parsing.constEnd())
            return;

        if (p.value().epochIndex >= fields.size())
            return;
        QString value(fields.at(p.value().epochIndex));
        if (value == p.value().epochMVC)
            return;
        bool ok = false;
        double time = value.toDouble(&ok);
        if (!ok)
            return;

        if (p.value().dataIndex >= fields.size())
            return;
        value = fields.at(p.value().dataIndex);
        if (value == p.value().dataMVC)
            return;
        interface->incomingData(value.toLatin1(), time,
                                AcquisitionInterface::IncomingDataType::Complete);
    }

public:
    LineIngressWrapperCPD2(std::unique_ptr<AcquisitionInterface> &&setInterface,
                           const ComponentOptions &options) : LineIngressWrapperLineGeneral(
            std::move(setInterface), options), inHeader(true)
    { }

    virtual ~LineIngressWrapperCPD2()
    { }
};

}

/**
 * Add the options to control the interface wrapper to the given set.
 * 
 * @param options       the target options to add to
 * @param implicitTime  if set to true then enable implicit time modes
 */
void LineIngressWrapper::addOptions(ComponentOptions &options, bool implicitTime)
{
    ComponentOptionEnum *mode =
            new ComponentOptionEnum(QObject::tr("mode", "name"), QObject::tr("Base mode"),
                                    QObject::tr(
                                            "This is the base import mode.  It defines the defaults for other "
                                            "options.  Thos defaults may be overridden by setting those "
                                            "options."),
                                    implicitTime ? QObject::tr("Raw data") : QObject::tr(
                                            "Single time field"), -1);
    mode->add(Mode_CSV_OneField, "singletime", QObject::tr("singletime", "mode name"),
              QObject::tr("CSV input with a single time field"));
    mode->add(Mode_CSV_TwoField, "twotime", QObject::tr("twotime", "mode name"),
              QObject::tr("CSV input with a two part time field"));
    mode->alias(Mode_CSV_TwoField, QObject::tr("yeardoy", "mode name"));
    mode->alias(Mode_CSV_TwoField, QObject::tr("doy", "mode name"));
    mode->add(Mode_CPD1_RawData, "cpd1", QObject::tr("cpd1", "mode name"),
              QObject::tr("CPD1 raw data recorder output"));
    mode->add(Mode_CPD2_RawData, "cpd2", QObject::tr("cpd2", "mode name"),
              QObject::tr("CPD2 raw data recorder output"));
    if (implicitTime) {
        mode->add(Mode_PassThrough, "raw", QObject::tr("raw", "mode name"),
                  QObject::tr("Raw data input, with implicit times"));
    }
    options.add("mode", mode);

    ComponentOptionSingleInteger *index =
            new ComponentOptionSingleInteger(QObject::tr("time-index", "name"),
                                             QObject::tr("The index of the (first) time field"),
                                             QObject::tr(
                                                     "The index, starting from one, of the first (and only unless using "
                                                     "multiple fields) time field in each record."),
                                             "");
    index->setMinimum(1);
    index->setAllowUndefined(true);
    options.add("time-index", index);

    index = new ComponentOptionSingleInteger(QObject::tr("time-index2", "name"),
                                             QObject::tr("The index of the second time field"),
                                             QObject::tr(
                                                     "The index, starting from one, of the second time field."),
                                             "");
    index->setMinimum(1);
    index->setAllowUndefined(true);
    options.add("time-index2", index);

    index = new ComponentOptionSingleInteger(QObject::tr("record-index", "name"),
                                             QObject::tr("The index of the record value"),
                                             QObject::tr(
                                                     "The index, starting from one, of the record data."),
                                             "");
    index->setMinimum(1);
    index->setAllowUndefined(false);
    options.add("record-index", index);

    options.add("suffix", new ComponentOptionSingleString(QObject::tr("suffix", "name"),
                                                          QObject::tr(
                                                                  "The instrument suffix to use"),
                                                          QObject::tr(
                                                                  "The instrument suffix (e.x. \"S11\") to use when completing "
                                                                  "variables."),
                                                          QObject::tr("Automatically determined")));

    options.add("station", new ComponentOptionSingleString(QObject::tr("station", "name"),
                                                           QObject::tr("The default station"),
                                                           QObject::tr(
                                                                   "The station code to assign to data values."),
                                                           QObject::tr("NIL")));
}

static void setFields(QVector<int> &timeFields, int &dataField, const ComponentOptions &options)
{
    if (options.isSet("time-index")) {
        qint64 i = qobject_cast<ComponentOptionSingleInteger *>(options.get("time-index"))->get();
        if (!INTEGER::defined(i) || i <= 0 || i >= 0xFFFFFFFF) {
            timeFields.clear();
        } else {
            if (timeFields.size() < 1) {
                timeFields.append((int) (i - 1));
            } else {
                timeFields[0] = (int) (i - 1);
            }
        }
    }

    if (options.isSet("time-index2")) {
        qint64 i = qobject_cast<ComponentOptionSingleInteger *>(options.get("time-index2"))->get();
        if (!INTEGER::defined(i) || i <= 0 || i >= 0xFFFFFFFF) {
            if (timeFields.size() > 1)
                timeFields.pop_back();
        } else {
            if (timeFields.size() < 2) {
                timeFields.append((int) (i - 1));
            } else {
                timeFields[1] = (int) (i - 1);
            }
        }
    }

    if (options.isSet("record-index")) {
        qint64 i = qobject_cast<ComponentOptionSingleInteger *>(options.get("record-index"))->get();
        if (INTEGER::defined(i) && i > 0 && i < 0xFFFFFFFF) {
            dataField = (int) (i - 1);
        }
    }
}

/**
 * Create a new interface wrapper.  The caller is responsible for deleting it.
 * 
 * @param interface     the interface to wrap, this takes ownership of it
 * @param options       the options to use
 * @param implicitTime  if set to true then enable implicit time modes
 * @return              a new interface wrapper
 */
LineIngressWrapper *LineIngressWrapper::create(std::unique_ptr<AcquisitionInterface> &&interface,
                                               const ComponentOptions &options,
                                               bool implicitTime)
{
    if (!options.isSet("mode")) {
        if (implicitTime) {
            return new LineIngressWrapperRaw(std::move(interface), options);
        } else {
            QVector<int> timeFields;
            timeFields.append(0);
            int dataField = 1;
            setFields(timeFields, dataField, options);
            return new LineIngressWrapperCSVTail(std::move(interface), options, timeFields,
                                                 dataField, implicitTime);
        }
    } else {
        switch (qobject_cast<ComponentOptionEnum *>(options.get("mode"))->get().getID()) {
        case Mode_CSV_OneField: {
            QVector<int> timeFields;
            timeFields.append(0);
            int dataField = 1;
            setFields(timeFields, dataField, options);
            return new LineIngressWrapperCSV(std::move(interface), options, timeFields, dataField,
                                             implicitTime);
        }

        case Mode_CSV_TwoField: {
            QVector<int> timeFields;
            timeFields.append(0);
            timeFields.append(1);
            int dataField = 2;
            setFields(timeFields, dataField, options);
            return new LineIngressWrapperCSV(std::move(interface), options, timeFields, dataField,
                                             implicitTime);
        }

        case Mode_CPD1_RawData: {
            QVector<int> timeFields;
            timeFields.append(1);
            timeFields.append(2);
            int dataField = 4;
            setFields(timeFields, dataField, options);
            return new LineIngressWrapperCSVTail(std::move(interface), options, timeFields,
                                                 dataField, implicitTime);
        }

        case Mode_CPD2_RawData:
            return new LineIngressWrapperCPD2(std::move(interface), options);

        case Mode_PassThrough:
            return new LineIngressWrapperRaw(std::move(interface), options);
        }
    }

    Q_ASSERT(false);
    return NULL;
}

/**
 * Get the list of standard examples.
 * 
 * @param implicitTime  if set to true then enable implicit time modes
 * @return              a list of standard examples
 */
QList<ComponentExample> LineIngressWrapper::standardExamples(bool implicitTime)
{
    QList<ComponentExample> examples;

    ComponentOptions options;
    addOptions(options, implicitTime);
    (qobject_cast<ComponentOptionEnum *>(options.get("mode")))->set("cpd1");
    examples.append(ComponentExample(options, QObject::tr("Ingest CPD1 raw data record output")));

    options = ComponentOptions();
    addOptions(options, implicitTime);
    (qobject_cast<ComponentOptionEnum *>(options.get("mode")))->set("twotime");
    (qobject_cast<ComponentOptionSingleInteger *>(options.get("time-index")))->set(2);
    (qobject_cast<ComponentOptionSingleInteger *>(options.get("time-index2")))->set(3);
    (qobject_cast<ComponentOptionSingleInteger *>(options.get("record-index")))->set(4);
    examples.append(ComponentExample(options, QObject::tr(
            "Discard the first field and ingest year and DOY times")));

    if (implicitTime) {
        options = ComponentOptions();
        addOptions(options, implicitTime);
        (qobject_cast<ComponentOptionEnum *>(options.get("mode")))->set("raw");
        examples.append(ComponentExample(options, QObject::tr("Ingest completely raw data")));
    }

    return examples;
}

}
}
