/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITIONNETWORK_H
#define CPD3ACQUISITIONNETWORK_H

#include "core/first.hxx"

#include <deque>

#include "acquisition/acquisition.hxx"
#include "core/threading.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/archive/selection.hxx"
#include "datacore/archive/access.hxx"
#include "io/socket.hxx"
#include "io/drivers/tcp.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * The implementation of the network protocol for the acquisition system
 * on the server side.
 */
class CPD3ACQUISITION_EXPORT AcquisitionNetworkServer final {
    QString localSocketListen;

    struct TCPListen {
        std::string address;
        IO::Socket::TCP::Configuration config;

        inline TCPListen(std::string address, IO::Socket::TCP::Configuration config) : address(
                std::move(address)), config(std::move(config))
        { }
    };

    std::vector<TCPListen> tcpListen;

    std::vector<std::unique_ptr<IO::Socket::Server>> servers;
    std::thread thread;

    std::mutex mutex;
    std::condition_variable notify;
    bool terminated;

    using Clock = std::chrono::steady_clock;

    class ConnectedClient final {
        AcquisitionNetworkServer &parent;

        enum class State {
            AwaitHandshake, Running, Ended,
        } state;
        Clock::time_point timeout;

        Util::ByteArray incomingRead;
        bool incomingEnd;
        bool writeStalled;

        Util::ByteArray processRead;

        struct QueuedRealtimeUpdate {
            Clock::time_point oldestUpdate;
            std::size_t unsentUpdates;

            QueuedRealtimeUpdate();

            QueuedRealtimeUpdate(const Clock::time_point &now);
        };

        Clock::time_point timeOfStreamStall;
        Data::SequenceName::Map<QueuedRealtimeUpdate> pendingRealtimeUpdate;

        Data::SequenceName::Map<std::uint_fast16_t> realtimeNameForward;
        std::vector<Data::SequenceName> realtimeNameReverse;
        std::size_t realtimeNameReplace;

        struct Archive {
            std::unique_ptr<Data::SequenceMatch::Basic> filter;

            class Sink final : public Data::StreamSink {
                std::mutex &mutex;
                std::condition_variable &notify;
                std::deque<Data::SequenceValue> &queue;

                void stall(std::unique_lock<std::mutex> &lock);

            public:
                bool ended;
                bool terminated;

                std::condition_variable dataConsumed;

                Sink(Archive &target, AcquisitionNetworkServer &parent);

                virtual ~Sink();

                void incomingData(const Data::SequenceValue::Transfer &values) override;

                void incomingData(Data::SequenceValue::Transfer &&values) override;

                void incomingData(const Data::SequenceValue &value) override;

                void incomingData(Data::SequenceValue &&value) override;

                void endData() override;

                void terminate();
            };

            Sink sink;

            Data::Archive::Access access;
            Data::Archive::Access::StreamHandle reader;

            double endOfRead;
            std::deque<Data::SequenceValue> outputQueue;

            Archive(ConnectedClient &parent, Data::Archive::Selection::List selections);
        };

        std::unique_ptr<Archive> archive;

        std::unique_ptr<IO::Socket::Connection> connection;

        bool processNextPacket(const Clock::time_point &now);

        std::uint_fast16_t assignRealtimeName(const Data::SequenceName &name);

        void sendRealtimeValue(const Data::SequenceName &name, const Data::Variant::Read &value);

        void flushPendingRealtime(const Clock::time_point &now);

        void sendArchiveValue(const Data::SequenceValue &value);

        void flushPendingArchive(const Clock::time_point &now);

        void abortArchiveRead();

    public:
        ConnectedClient(AcquisitionNetworkServer &parent,
                        std::unique_ptr<IO::Socket::Connection> &&connection);

        ~ConnectedClient();

        bool wakeAt(Clock::time_point &wake) const;

        bool update(const Clock::time_point &now);

        void incomingEvent(const Data::Variant::Read &event);

        void updateInterfaceInformation(const std::string &name, const Data::Variant::Read &data);

        void updateInterfaceState(const std::string &name, const Data::Variant::Read &data);

        void updateAutoprobeState(const Data::Variant::Read &data);

        void incomingRealtime(const Clock::time_point &now,
                              const Data::SequenceValue::Transfer &values);
    };

    std::vector<std::unique_ptr<IO::Socket::Connection>> receivedConnections;
    std::vector<std::unique_ptr<ConnectedClient>> clients;

    std::vector<std::function<void()>> pendingCalls;

    Data::SequenceValue::Transfer realtimeDataUpdate;

    std::deque<Data::SequenceValue> retainedArchive;
    std::unordered_map<std::string, Data::Variant::Root> retainedInterfaceInformation;
    std::unordered_map<std::string, Data::Variant::Root> retainedInterfaceState;
    Data::Variant::Root retainedAutoprobeState;

    std::deque<Data::SequenceValue> getRetainedArchive(double startTime,
                                                       Data::SequenceMatch::Basic &filter) const;

    struct RealtimeData {
        Data::Variant::Root latest;
        Data::Variant::Root metadata;

        bool onlySendOnUpdate;
        bool sendToNewConnection;
        std::uint_fast64_t priority;

        RealtimeData();

        RealtimeData(const Data::SequenceName &name);

        void updateData(const Data::Variant::Root &value);

        void updateMetadata(const Data::Variant::Root &value);
    };

    Data::SequenceName::Map<RealtimeData> retainedRealtime;

    void incomingConnection(std::unique_ptr<IO::Socket::Connection> &&connection);

    void run();

public:
    /**
     * Create a new network server from the given configuration.
     * 
     * @param config    the configuration
     */
    explicit AcquisitionNetworkServer(const Data::Variant::Read &config);

    ~AcquisitionNetworkServer();

    AcquisitionNetworkServer(const AcquisitionNetworkServer &) = delete;

    AcquisitionNetworkServer &operator=(const AcquisitionNetworkServer &) = delete;

    AcquisitionNetworkServer(AcquisitionNetworkServer &&) = delete;

    AcquisitionNetworkServer &operator=(AcquisitionNetworkServer &&) = delete;

    /**
     * Start the server and begin accepting connections.
     */
    void start();

    /**
     * Handle incoming realtime data from the acquisition system.
     * 
     * @param values    the incoming values
     */
    void incomingRealtime(const Data::SequenceValue::Transfer &values);

    void incomingRealtime(Data::SequenceValue::Transfer &&values);

    void incomingRealtime(const Data::SequenceValue &values);

    void incomingRealtime(Data::SequenceValue &&values);

    /**
     * Handle a new event that should be sent to realtime systems.
     * 
     * @param event     the event
     */
    void incomingEvent(const Data::Variant::Read &event);

    /**
     * Signal the network server that the archive has been flushed.
     *
     * @param time      the time of the first value written
     */
    void archiveFlush(double time = Time::time() - 15 * 60.0);

    /**
     * Update the information for an interface.  The name is the
     * global name of the interface (e.x. A11).
     * 
     * @param name      the interface name
     * @param data      the interface information
     */
    void updateInterfaceInformation(const std::string &name, const Data::Variant::Read &data);

    /**
     * Update the state for an interface.  The name is the
     * global name of the interface (e.x. A11).
     * 
     * @param name      the interface name
     * @param data      the interface information
     */
    void updateInterfaceState(const std::string &name, const Data::Variant::Read &data);

    /**
     * Update the system autoprobe state.
     *
     * @param data      the autoprobe information
     */
    void updateAutoprobeState(const Data::Variant::Read &data);

    /**
     * Check for an existing network server based on the given configuration.
     * This is used to prevent starting duplicate instances of the controller.
     * 
     * @param configuration     the configuration
     * @return                  true if there is already a network server running
     */
    static bool checkExisting(const Data::Variant::Read &configuration);

    /**
     * Emitted when a new message log entry comes in from a client.  This
     * should be converted and written to the logging archive.
     * 
     * @param event     the event log data
     */
    Threading::Signal<const Data::Variant::Root &> messageLogEvent;

    /**
     * Emitted when a command has been received.  This should cause the
     * command data to be dispatched to all appropriate targets.
     * 
     * @param target    the regular expression of targets or empty for all
     * @param command   the command data
     */
    Threading::Signal<const std::string &, const CPD3::Data::Variant::Root &> commandReceived;

    /**
     * Emitted when a flush request has been received.
     * 
     * @param seconds   the number of seconds requested, zero or negative for the system default
     */
    Threading::Signal<double> flushRequest;

    /**
     * Emitted when a request to change the averaging time has been received.
     * 
     * @param unit      the new averaging unit
     * @param count     the new number of units to average
     * @param aligned   if set then align averages
     */
    Threading::Signal<Time::LogicalTimeUnit, int, bool> setAveragingTime;

    /**
     * Emitted when a client has requested a data flush for the server.
     */
    Threading::Signal<> dataFlushRequest;

    /**
     * Emitted when a request to set a bypass flag is received.  This
     * should result in the flag being set for all variable sets.
     * 
     * @param flag      the flag to set
     */
    Threading::Signal<const Data::Variant::Flag &> bypassFlagSet;

    /**
     * Emitted when a request to clear a bypass flag is received.  This
     * should result in the flag being cleared from all variable sets.
     * 
     * @param flag      the flag to clear
     */
    Threading::Signal<const Data::Variant::Flag &> bypassFlagClear;

    /**
     * Emitted when a request to clear all bypass flags is received.  This
     * should result in all bypass flags being cleared from all variable sets.
     */
    Threading::Signal<> bypassFlagClearAll;


    /**
     * Emitted when a request to set a system flag is received.  This
     * should result in the flag being set for all variable sets.
     * 
     * @param flag      the flag to set
     */
    Threading::Signal<const Data::Variant::Flag &> systemFlagSet;

    /**
     * Emitted when a request to clear a system flag is received.  This
     * should result in the flag being cleared from all variable sets.
     * 
     * @param flag      the flag to clear
     */
    Threading::Signal<const Data::Variant::Flag &> systemFlagClear;

    /**
     * Emitted when a request to clear all system flags is received.  This
     * should result in all system flags being cleared from all variable sets.
     */
    Threading::Signal<> systemFlagClearAll;

    /**
     * Emitted when a restart request is received.
     */
    Threading::Signal<> restartRequested;
};

/**
 * The external client interface to the acquisition network protocol.
 */
class CPD3ACQUISITION_EXPORT AcquisitionNetworkClient final {
    Data::Variant::Read configuration;

    using Clock = std::chrono::steady_clock;

    QString connectionTarget;
    std::unique_ptr<IO::Socket::TCP::Configuration> tcpConfiguration;
    Clock::time_point retryConnection;

    std::thread thread;
    std::mutex mutex;
    std::condition_variable notify;
    bool terminated;
    Util::ByteArray incomingRead;
    bool incomingEnded;
    std::vector<std::function<void()>> pendingCalls;

    std::unordered_map<std::string, Data::Variant::Root> interfaceInformation;
    std::unordered_map<std::string, Data::Variant::Root> interfaceState;
    Data::Variant::Root autoprobeState;

    std::mutex realtimeOutputLock;
    Data::StreamSink *realtimeSink;

    std::mutex archiveReadLock;
    Data::StreamSink *archiveSink;

    std::unique_ptr<IO::Socket::Connection> connection;

    Util::ByteArray processRead;
    Clock::time_point nextPing;
    Clock::time_point pingResponseTimeout;

    std::vector<Data::SequenceName> realtimeNameLookup;
    std::size_t realtimeNameReplace;

    void run();

    void establishConnection();

    bool processNextPacket();

public:
    explicit AcquisitionNetworkClient(const Data::Variant::Read &config);

    ~AcquisitionNetworkClient();

    AcquisitionNetworkClient(const AcquisitionNetworkClient &) = delete;

    AcquisitionNetworkClient &operator=(const AcquisitionNetworkClient &) = delete;

    AcquisitionNetworkClient(AcquisitionNetworkClient &&) = delete;

    AcquisitionNetworkClient &operator=(AcquisitionNetworkClient &&) = delete;

    /**
     * Get the information about all interfaces.
     * 
     * @return the interface information
     */
    std::unordered_map<std::string, Data::Variant::Root> getInterfaceInformation();

    /**
     * Get the information about a specific interface.
     * 
     * @param interface the interface name
     * @return          the interface information
     */
    Data::Variant::Root getInterfaceInformation(const std::string &interface);

    /**
     * Get the state for all interfaces.
     * 
     * @return the interface state
     */
    std::unordered_map<std::string, Data::Variant::Root> getInterfaceState();

    /**
     * Get the state for a specific interface.
     * 
     * @param interface the interface name
     * @return          the interface state
     */
    Data::Variant::Root getInterfaceState(const std::string &interface);

    /**
     * Get the system autoprobe state.
     *
     * @return          the autporobe state
     */
    Data::Variant::Root getAutoprobeState();

    /**
     * Set the sink point for realtime data.  A null egress disables
     * the realtime output (values will be discarded).  If an egress is in
     * use, it should be set before the client is started otherwise
     * the initial values may be lost.
     * 
     * @param sink  the output sink
     */
    void setRealtimeEgress(Data::StreamSink *sink);

    /**
     * Request a resend of realtime values.
     */
    void realtimeResend();

    /**
     * Issue a read to the remote data source.
     * 
     * @param egress    the target for the data
     * @param selection the data to read
     */
    void remoteDataRead(Data::StreamSink *egress, const Data::Archive::Selection::List &selection);

    /** @see remoteDataRead( Data::StreamSink *,
     * const QList<Data::ArchiveSelection> & )
     */
    inline void remoteDataRead(Data::StreamSink *egress, const Data::Archive::Selection &selection)
    {
        remoteDataRead(egress, Data::Archive::Selection::List{selection});
    }

    /**
     * Get the configuration used to create the client.
     * 
     * @return the client configuration
     */
    const Data::Variant::Read &getConfiguration() const
    { return configuration; }

    /**
     * Stops any in progress data read.
     */
    void remoteDataReadStop();

    /**
     * Issue a message log entry.
     * 
     * @param event the message log event data
     */
    void messageLogEvent(const Data::Variant::Read &event);

    /**
     * Issue a command to the system.
     * 
     * @param target    the regular expression of targets or empty for all
     * @param command   the command data
     */
    void issueCommand(const std::string &target, const Data::Variant::Read &command);

    /**
     * Request a data flush.
     * 
     * @param seconds   the number of seconds requested, zero or negative for the system default
     */
    void flushRequest(double seconds = -1.0);

    /**
     * Request a change in the averaging time.
     * 
     * @param unit      the new averaging unit
     * @param count     the new number of units to average
     * @param aligned   if set then align averages
     */
    void setAveragingTime(CPD3::Time::LogicalTimeUnit unit, int count, bool aligned);

    /**
     * Request that the server write out all buffered data it has to
     * its local archive.  This is generally only used by the data sending
     * routines to send as much data as possible.
     */
    void dataFlush();


    /**
     * Request setting a single bypass flag
     * 
     * @param flag      the flag to set
     */
    void bypassFlagSet(const CPD3::Data::Variant::Flag &flag = "Bypass");

    /**
     * Request clearing a single bypass flag
     * 
     * @param flag      the flag to clear
     */
    void bypassFlagClear(const CPD3::Data::Variant::Flag &flag = "Bypass");

    /**
     * Request clearing all active bypass flags.
     */
    void bypassFlagClearAll();


    /**
     * Request setting a single system flag
     * 
     * @param flag      the flag to set
     */
    void systemFlagSet(const CPD3::Data::Variant::Flag &flag = "Contaminated");

    /**
     * Request clearing a single system flag
     * 
     * @param flag      the flag to clear
     */
    void systemFlagClear(const CPD3::Data::Variant::Flag &flag = "Contaminated");

    /**
     * Request clearing all active system flags.
     */
    void systemFlagClearAll();

    /**
     * Request a system restart.
     */
    void restartRequested();

    /**
     * Start connection to the server.
     */
    void start();

    /**
     * Emitted when the information about an interface has changed.
     * 
     * @param name      the updated interface name
     */
    Threading::Signal<const std::string &> interfaceInformationUpdated;

    /**
     * Emitted when the state for an interface has changed.
     * 
     * @param name      the updated interface name
     */
    Threading::Signal<const std::string &> interfaceStateUpdated;

    /**
     * Emitted when the autoprobe state has changed.
     */
    Threading::Signal<> autoprobeStateUpdated;

    /**
     * Emitted when the connection has a failure event.  This is distinct from
     * the connection lost, which only occurs after one has been established.
     * This signal can be emitted on each connection attempt, which is normally
     * hidden from the users of the class.
     */
    Threading::Signal<> connectionFailed;

    /**
     * Emitted when the connection to the network has been established or lost.
     */
    Threading::Signal<bool> connectionState;

    /**
     * Emitted when an event that should be shown in the realtime display
     * has been received.
     * 
     * @param event the event data
     */
    Threading::Signal<const Data::Variant::Root &> realtimeEvent;
};


/**
 * An interface that handles remote commands from the acquisition system
 * tray.
 */
class CPD3ACQUISITION_EXPORT AcquisitionTrayCommandHandler final {
    std::unique_ptr<IO::Socket::Server> server;

    void acceptConnection(std::unique_ptr<IO::Socket::Connection> &&connection);

public:
    /**
     * The possible commands that can be issued.
     */
    enum class Command : std::uint8_t {
        /**
         * Show the realtime display.
         */
                ShowRealtime = 0,
    };

    AcquisitionTrayCommandHandler();

    ~AcquisitionTrayCommandHandler();

    /**
     * Issue a command to a listening handler.  Returns false
     * if there was nothing to receive the command.
     * 
     * @param command   the command to issue
     * @return          true if the command was received
     */
    static bool issueCommand(Command command = Command::ShowRealtime);

    /**
     * Emitted when a remote command has been received.
     */
    Threading::Signal<Command> commandReceived;
};

}
}

#endif
