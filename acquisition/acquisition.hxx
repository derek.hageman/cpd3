/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITION_H
#define CPD3ACQUISITION_H

#include <QtCore/QtGlobal>

#if defined(cpd3acquisition_EXPORTS)
#   define CPD3ACQUISITION_EXPORT Q_DECL_EXPORT
#else
#   define CPD3ACQUISITION_EXPORT Q_DECL_IMPORT
#endif

#endif
