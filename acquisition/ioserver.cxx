/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QLoggingCategory>

#include "ioserver.hxx"

Q_LOGGING_CATEGORY(log_acquisition_ioserver, "cpd3.acquisition.ioserver", QtWarningMsg)

using namespace CPD3::Data;


namespace CPD3 {
namespace Acquisition {

IOServer::IOServer(std::unique_ptr<IOTarget> &&target, bool spawned) : terminated(false),
                                                                       completed(false),
                                                                       target(std::move(target)),
                                                                       spawned(spawned),
                                                                       exclusive(nullptr)
{ }

IOServer::~IOServer()
{
    signalTerminate();
    if (thread.joinable()) {
        thread.join();
    }
}

void IOServer::start()
{
    Q_ASSERT(!completed);
    thread = std::thread(std::bind(&IOServer::run, this));
}

void IOServer::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    notify.notify_all();
}

bool IOServer::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return completed;
}

bool IOServer::wait(double timeout)
{ return Threading::waitForTimeout(timeout, mutex, notify, [this] { return completed; }); }


bool IOServer::checkAlreadyRunning()
{
    bool ended = false;
    bool seenHello = false;

    qCDebug(log_acquisition_ioserver) << "Checking for existing server for"
                                      << target->description();

    auto conn = target->clientConnection(true);
    if (!conn)
        return false;
    conn->ended.connect([this, &ended] {
        {
            std::lock_guard<std::mutex> lock(mutex);
            ended = true;
        }
        notify.notify_all();
    });
    conn->read.connect([this, &seenHello, &ended](const Util::ByteArray &data) {
        if (data.empty())
            return;
        if (data.front() != static_cast<std::uint8_t>(PacketToClient::Hello)) {
            std::lock_guard<std::mutex> lock(mutex);
            ended = true;
        } else {
            std::lock_guard<std::mutex> lock(mutex);
            seenHello = true;
        }
        notify.notify_all();
    });

    conn->start();
    {
        std::unique_lock<std::mutex> lock(mutex);
        notify.wait_for(lock, std::chrono::seconds(5), [&ended, &seenHello] {
            return ended || seenHello;
        });
    }
    conn.reset();

    if (seenHello) {
        qCDebug(log_acquisition_ioserver) << "Existing server for" << target->description()
                                          << "detected";
    } else if (ended) {
        qCDebug(log_acquisition_ioserver) << "No connection accepted for" << target->description();
    } else {
        qCDebug(log_acquisition_ioserver) << "Timeout checking for connection on"
                                          << target->description();
    }

    return seenHello;
}

void IOServer::run()
{
    std::vector<std::unique_ptr<IO::Socket::Connection>> pendingConnections;
    std::vector<std::unique_ptr<IO::Socket::Server>> servers;
    if (checkAlreadyRunning()) {
        qCDebug(log_acquisition_ioserver) << "Server for" << target->description()
                                          << "exiting because an existing local one was already detected";
        {
            std::lock_guard<std::mutex> lock(mutex);
            completed = true;
        }
        finished();
        return;
    }

    servers = target->serverListeners(
            [this, &pendingConnections](std::unique_ptr<IO::Socket::Connection> &&c) {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    pendingConnections.emplace_back(std::move(c));
                }
                notify.notify_all();
            });
    if (servers.empty()) {
        qCDebug(log_acquisition_ioserver) << "Server for" << target->description()
                                          << "exiting because it was unable to listen for any connections";
        {
            std::lock_guard<std::mutex> lock(mutex);
            completed = true;
        }
        finished();
        return;
    }

    Clock::time_point firstConnectionTimeout;
    bool haveHadConnection = false;
    if (spawned) {
        firstConnectionTimeout = Clock::now() + std::chrono::seconds(30);
    }

    Clock::time_point deviceReopenTime = Clock::now();
    int deviceOpenBackoff = 0;
    bool deviceEndPending = false;

    for (;;) {
        std::vector<std::unique_ptr<IO::Socket::Connection>> processConnections;
        std::vector<Util::ByteArray> processDeviceRead;
        std::vector<Util::ByteArray> processDeviceControl;
        bool processDeviceEnd = false;

        {
            std::unique_lock<std::mutex> lock(mutex);
            for (;;) {
                if (terminated) {
                    completed = true;
                    lock.unlock();
                    exclusive = nullptr;
                    device.reset();
                    clients.clear();
                    servers.clear();
                    finished();
                    return;
                }

                auto now = Clock::now();
                auto wake = now + std::chrono::seconds(5);
                bool needProcessing = false;

                if (spawned && !haveHadConnection) {
                    if (now > firstConnectionTimeout) {
                        qCDebug(log_acquisition_ioserver) << "Server for" << target->description()
                                                          << "shutting down because no client connected";
                        terminated = true;
                        continue;
                    }
                    if (wake > firstConnectionTimeout)
                        wake = firstConnectionTimeout;
                }

                processConnections = std::move(pendingConnections);
                pendingConnections.clear();
                if (!processConnections.empty())
                    needProcessing = true;

                if (!device) {
                    if (deviceReopenTime <= now) {
                        needProcessing = true;
                    } else if (wake > deviceReopenTime) {
                        wake = deviceReopenTime;
                    }
                } else {
                    processDeviceRead = std::move(deviceReadPending);
                    deviceReadPending.clear();
                    if (!processDeviceRead.empty())
                        needProcessing = true;

                    processDeviceControl = std::move(deviceControlPending);
                    deviceControlPending.clear();
                    if (!processDeviceControl.empty())
                        needProcessing = true;

                    processDeviceEnd = deviceEndPending;
                    if (processDeviceEnd)
                        needProcessing = true;

                    if (!writeBacklog.empty())
                        needProcessing = true;
                }

                for (const auto &c : clients) {
                    if (c->prepare(now, wake)) {
                        needProcessing = true;
                    }
                }

                if (!needProcessing) {
                    notify.wait_until(lock, wake);
                    continue;
                }
                break;
            }
        }

        if (!device && deviceReopenTime < Clock::now()) {
            device = target->backingDevice();

            if (!device) {
                if (deviceOpenBackoff == 0) {
                    qCDebug(log_acquisition_ioserver) << "Failed to open backing device for"
                                                      << target->description();
                }

                ++deviceOpenBackoff;
                if (deviceOpenBackoff > 10)
                    deviceOpenBackoff = 10;

                auto sleepTime = Random::fp() * (deviceOpenBackoff / 10.0 + 0.25);
                deviceReopenTime = Clock::now() +
                        std::chrono::milliseconds(static_cast<int>(sleepTime * 1000.0));
            } else {
                deviceOpenBackoff = 0;
                deviceEndPending = false;
                processDeviceEnd = false;

                device->ended.connect([this, &deviceEndPending] {
                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        deviceEndPending = true;
                    }
                    notify.notify_all();
                });
                device->read.connect([this](const Util::ByteArray &data) {
                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        deviceReadPending.emplace_back(data);
                    }
                    notify.notify_all();
                });
                device->control.connect([this](const Util::ByteArray &data) {
                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        deviceControlPending.emplace_back(data);
                    }
                    notify.notify_all();
                });

                device->start();

                qCDebug(log_acquisition_ioserver) << "Server opened" << target->description();
            }
        }

        if (device) {
            for (auto &wr : writeBacklog) {
                device->write(std::move(wr));
            }
            writeBacklog.clear();

            for (const auto &rd : processDeviceRead) {
                for (const auto &cl : clients) {
                    cl->incomingData(rd);
                }
            }
            for (const auto &rd : processDeviceControl) {
                for (const auto &cl : clients) {
                    cl->incomingControl(rd);
                }
            }

            if (processDeviceEnd) {
                if (device) {
                    qCDebug(log_acquisition_ioserver) << "Close detected on"
                                                      << target->description();

                    for (const auto &cl : clients) {
                        cl->deviceClosed();
                    }
                }
                device.reset();
            }
        } else if (writeBacklog.size() > 2048U) {
            writeBacklog.erase(writeBacklog.begin(),
                               writeBacklog.begin() + (writeBacklog.size() - 2048U));
        }

        for (auto c = clients.begin(); c != clients.end();) {
            if ((*c)->process()) {
                ++c;
                continue;
            }
            if (c->get() == exclusive) {
                qCDebug(log_acquisition_ioserver) << "Released exclusive client on disconnect";
                exclusive = nullptr;
            }
            c = clients.erase(c);
        }

        if (!processConnections.empty()) {
            haveHadConnection = true;

            for (auto &conn : processConnections) {
                clients.emplace_back(new Client(*this, std::move(conn)));
            }
        }

        if (clients.empty() && spawned && haveHadConnection) {
            qCDebug(log_acquisition_ioserver)
                << "Server shutting down after the last client closed";
            {
                std::lock_guard<std::mutex> lock(mutex);
                terminated = true;
            }
        }
    }
}

IOServer::Client::Client(IOServer &s, std::unique_ptr<IO::Socket::Connection> &&c) : server(s),
                                                                                     connection(
                                                                                             std::move(
                                                                                                     c)),
                                                                                     endPending(
                                                                                             false),
                                                                                     processEnd(
                                                                                             false)
{
    connection->ended.connect([this] {
        {
            std::lock_guard<std::mutex> lock(server.mutex);
            endPending = true;
        }
        server.notify.notify_all();
    });
    connection->read.connect([this](const Util::ByteArray &data) {
        {
            std::lock_guard<std::mutex> lock(server.mutex);
            readPending += data;
        }
        server.notify.notify_all();
    });

    connection->start();

    {
        Util::ByteArray packet;
        packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToClient::Hello));
        connection->write(std::move(packet));
    }

    pingTimeout = Clock::now() + std::chrono::seconds(30);
}

IOServer::Client::~Client() = default;

bool IOServer::Client::prepare(const Clock::time_point &now, Clock::time_point &wake)
{
    if (pingTimeout < now) {
        endPending = true;

        qCDebug(log_acquisition_ioserver) << "Connection timed out to"
                                          << connection->describePeer();
    } else {
        if (wake > pingTimeout)
            wake = pingTimeout;
    }
    processEnd = endPending;

    if (readPending.empty())
        return processEnd;

    processRead += std::move(readPending);
    readPending.clear();
    return true;
}

bool IOServer::Client::process()
{
    while (handleNextPacket()) { }

    if (processEnd) {
        qCDebug(log_acquisition_ioserver) << "Closing connection to" << connection->describePeer();
        connection.reset();
        return false;
    }
    return true;
}

bool IOServer::Client::handleNextPacket()
{
    if (processRead.empty())
        return false;

    switch (static_cast<IOServer::PacketToServer>(processRead.front())) {
    case PacketToServer::Data: {
        if (processRead.size() < 3U)
            return false;
        auto size = processRead.readNumber<quint16>(1U);
        if (processRead.size() < 3U + size)
            return false;
        if (!server.exclusive || server.exclusive == this) {
            auto contents = processRead.mid(3U, size);
            if (!server.device) {
                server.writeBacklog.emplace_back(contents);
            } else {
                server.device->write(contents);
                if (!server.exclusive) {
                    for (const auto &c : server.clients) {
                        if (c.get() == this)
                            continue;
                        c->incomingControl(contents);
                    }
                }
            }
        }

        processRead.pop_front(3U + size);
        return true;
    }
    case PacketToServer::Ping: {
        pingTimeout = Clock::now() + std::chrono::seconds(30);
        Util::ByteArray packet;
        packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToClient::Pong));
        connection->write(std::move(packet));

        processRead.pop_front();
        return true;
    }
    case PacketToServer::SetExclusive: {
        if (processRead.size() < 2U)
            return false;

        if (processRead[1]) {
            if (server.exclusive) {
                qCDebug(log_acquisition_ioserver) << "Client" << connection->describePeer()
                                                  << "stealing exclusive mode";
            } else {
                qCDebug(log_acquisition_ioserver) << "Client" << connection->describePeer()
                                                  << "set exclusive mode";
            }
            server.exclusive = this;
        } else {
            if (server.exclusive == this) {
                qCDebug(log_acquisition_ioserver) << "Client" << connection->describePeer()
                                                  << "released exclusive mode";
                server.exclusive = nullptr;
            }
        }

        Util::ByteArray packet;
        packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToClient::SetExclusiveResult));
        connection->write(std::move(packet));

        processRead.pop_front(2U);
        return true;
    }
    case PacketToServer::TargetMerge: {
        if (processRead.size() < 5U)
            return false;
        auto size = processRead.readNumber<quint32>(1);
        if (processRead.size() < 5U + size)
            return false;
        auto deserialized = IOTarget::deserialize(processRead.mid(5U, size));
        if (deserialized) {
            server.target->merge(*deserialized, server.device.get());
            qCDebug(log_acquisition_ioserver) << "Merged" << deserialized->description() << "into"
                                              << server.target->description() << "from"
                                              << connection->describePeer();
        }

        {
            Util::ByteArray packet;
            packet.push_back(
                    static_cast<std::uint8_t>(IOServer::PacketToClient::TargetMergeResult));
            connection->write(std::move(packet));
        }

        if (deserialized) {
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToClient::UpdateTarget));
            {
                auto add = server.target->serialize();
                packet.appendNumber<quint32>(add.size());
                packet += std::move(add);
            }
            for (const auto &c : server.clients) {
                c->connection->write(packet);
            }
        } else {
            Util::ByteArray packet;
            packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToClient::UpdateTarget));
            {
                auto add = server.target->serialize();
                packet.appendNumber<quint32>(add.size());
                packet += std::move(add);
            }
            connection->write(std::move(packet));
        }

        processRead.pop_front(5U + size);
        return true;
    }
    case PacketToServer::Reset: {
        qCDebug(log_acquisition_ioserver) << "Client" << connection->describePeer()
                                          << "triggering a device reset";
        if (server.device) {
            server.device->reset();
        }
        {
            std::lock_guard<std::mutex> lock(server.mutex);
            server.writeBacklog.clear();
            server.deviceReadPending.clear();
            server.deviceControlPending.clear();
        }

        Util::ByteArray packet;
        packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToClient::ResetResult));
        connection->write(std::move(packet));

        processRead.pop_front();
        return true;
    }
    case PacketToServer::Reopen: {
        qCDebug(log_acquisition_ioserver) << "Client" << connection->describePeer()
                                          << "triggering a device reopen";

        /* Make sure the shutdown doesn't block (network connections) so we can respond and
         * handle requests immediately. */
        struct Context {
            std::unique_ptr<IOTarget::Device> device;

            Context(std::unique_ptr<IOTarget::Device> &&device) : device(std::move(device))
            { }
        };
        auto context = std::make_shared<Context>(std::move(server.device));
        server.device.reset();
        std::thread([context] {
            context->device.reset();
        }).detach();

        {
            std::lock_guard<std::mutex> lock(server.mutex);
            server.writeBacklog.clear();
            server.deviceReadPending.clear();
            server.deviceControlPending.clear();
        }

        Util::ByteArray packet;
        packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToClient::ReopenResult));
        connection->write(std::move(packet));

        processRead.pop_front();
        return true;
    }
    default:
        break;
    }

    qCWarning(log_acquisition_ioserver) << "Invalid packet received from client "
                                        << connection->describePeer() << " (" << processRead.front()
                                        << ")";
    processEnd = true;
    return false;
}

void IOServer::Client::incomingData(const Util::ByteView &data)
{
    if (server.exclusive && server.exclusive != this)
        return;
    if (data.empty())
        return;
    Util::ByteView remaining = data;
    Util::ByteArray packet;
    for (;;) {
        std::size_t packetSize = remaining.size();
        if (!packetSize)
            break;
        if (packetSize > 0xFFFFU)
            packetSize = 0xFFFFU;
        packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToClient::Data));
        packet.appendNumber<quint16>(packetSize);
        packet += remaining.mid(0, packetSize);
        remaining = remaining.mid(packetSize);
    }
    connection->write(std::move(packet));
}

void IOServer::Client::incomingControl(const Util::ByteView &data)
{
    if (server.exclusive && server.exclusive != this)
        return;
    if (data.empty())
        return;
    Util::ByteView remaining = data;
    Util::ByteArray packet;
    for (;;) {
        std::size_t packetSize = remaining.size();
        if (!packetSize)
            break;
        if (packetSize > 0xFFFFU)
            packetSize = 0xFFFFU;
        packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToClient::OtherControl));
        packet.appendNumber<quint16>(packetSize);
        packet += remaining.mid(0, packetSize);
        remaining = remaining.mid(packetSize);
    }
    connection->write(std::move(packet));
}

void IOServer::Client::deviceClosed()
{
    if (server.exclusive && server.exclusive != this)
        return;
    Util::ByteArray packet;
    packet.push_back(static_cast<std::uint8_t>(IOServer::PacketToClient::DeviceClosed));
    connection->write(std::move(packet));
}

}
}
