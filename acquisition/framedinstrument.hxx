/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITIONFRAMEDINSTRUMENT_H
#define CPD3ACQUISITIONFRAMEDINSTRUMENT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "acquisition/acquisition.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/waitutils.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * A base class that provides an instrument that is both controlled by
 * and issues data that is "framed".  For example, a line driven instrument
 * is framed by nothing at the start and a newline at the end.  Realtime advance
 * is called after all frames have been processed.
 * <br>
 * The default implementation provides framing for a single line driven
 * instrument.
 */
class CPD3ACQUISITION_EXPORT FramedInstrument : public InstrumentAcquisitionInterface {
    struct Event {
        Util::ByteArray data;
        double time;

        enum class Type {
            DataFrame,
            ControlFrame,
            DataDiscardComplete,
            ControlDiscardComplete,
            Timeout,
            ProcessShutdown
        };
        Type type;

        inline Event() = default;

        inline Event(Util::ByteArray data, double time, Type type = Type::DataFrame) : data(
                std::move(data)), time(time), type(type)
        { }

        inline Event(const Util::ByteView &data, double time, Type type = Type::DataFrame) : data(
                data), time(time), type(type)
        { }

        inline Event(double time, Type type) : time(time), type(type)
        { }
    };

    std::vector<Event> pendingEvents;
    std::vector<Event> processEvents;
    Util::ByteArray dataBuffer;
    Util::ByteArray controlBuffer;
    double pendingAdvance;
    double processAdvance;
    double loggingHaltTime;
    double latestTime;

    double discardDataEndTime;
    int discardDataFrames;
    double discardControlEndTime;
    int discardControlFrames;

    int processingIndex;

    static void handleDiscard(std::vector<Event> &target,
                              int offset,
                              double &endTime,
                              int &discardAfterEnd,
                              Event::Type discardEvent,
                              Event::Type frameEvent);

    void finishStreams(double time = FP::undefined());

protected:
    bool prepare() override;

    void process() override;

    /**
     * Clear all buffered data and data frames.  This can be used as part
     * of a comms start sequence to flush out anything existing.
     * <br>
     * The mutex must be locked before calling.
     */
    void clearDataBuffer();

    /**
     * Clear all buffered data and control frames.  This can be used as part
     * of a comms start sequence to flush out anything existing.
     * <br>
     * The mutex must be locked before calling.
     */
    void clearControlBuffer();

    /**
     * Called when a new data frame is completed.
     * 
     * @param frame the complete frame
     * @param time  the time of the frame
     */
    virtual void incomingDataFrame(const Util::ByteArray &frame, double time) = 0;

    /**
     * Called when a new control frame is completed.
     * 
     * @param frame the complete frame
     * @param time  the time of the frame
     */
    virtual void incomingControlFrame(const Util::ByteArray &frame, double time) = 0;

    /**
     * Called when a timeout has occurred.  This is NOT guaranteed to be 
     * called after a timeout request (e.x. on a file playback there are no
     * timeouts).  This is synchronized with the framing functions.
     * <br>
     * The default implementation does nothing.
     * 
     * @param time  the time of the timeout
     */
    virtual void incomingInstrumentTimeout(double time);

    /**
     * Called when the discard of data has been completed, with the time of
     * completion.
     * <br>
     * The default implementation does nothing.
     * 
     * @param time  the time the discard was completed at
     */
    virtual void discardDataCompleted(double time);

    /**
     * Called when the discard of control has been completed, with the time of
     * completion.
     * <br>
     * The default implementation does nothing.
     * 
     * @param time  the time the discard was completed at
     */
    virtual void discardControlCompleted(double time);

    /**
     * Called during a controlled shutdown.  This may be called multiple times during
     * shutdown.  The normal procedure is to flush any ongoing operations in a call
     * to this and to send any final commands (but not to wait for responses).
     * <br>
     * The default implementation does nothing.
     *
     * @param time  the time of the shutdown
     */
    virtual void shutdownInProgress(double time);

    /**
     * Apply input data framing.
     * <br>
     * The default implementation calls dataFrameStart(int, const QByteArray &)
     * and dataFrameEnd(int, const QByteArray &).  It will extract a terminal
     * frame if it has any start defined.  It will skip empty frames.
     * 
     * @param input     the input data
     * @param start     the input start offset and the output start index (inclusive)
     * @param end       the output end index (exclusive)
     * @param terminal  if set then this is a "terminal" frame and the tail should be treated as such (e.x. allow the final line not to also have a newline)
     * @return          true to continue extraction
     */
    virtual bool applyDataFraming(const Util::ByteArray &input,
                                  std::size_t &start,
                                  std::size_t &end,
                                  bool terminal = false) const;

    /**
     * Apply input control framing.
     * <br>
     * The default implementation calls controlFrameStart(int, const QByteArray &)
     * and controlFrameEnd(int, const QByteArray &).  It will extract a terminal
     * frame if it has any start defined.  It will skip empty frames.
     * 
     * @param input     the input control
     * @param start     the input start offset and the output start index (inclusive)
     * @param end       the output end index (exclusive)
     * @param terminal  if set then this is a "terminal" frame and the tail should be treated as such (e.x. allow the final line not to also have a newline)
     * @return          true to continue extraction
     */
    virtual bool applyControlFraming(const Util::ByteArray &data,
                                     std::size_t &start,
                                     std::size_t &end,
                                     bool terminal = false) const;

    /**
     * Find the start of a data frame from the given offset.
     * <br>
     * The default implementation returns the offset of the first non-newline
     * character.
     * 
     * @param offset    the starting offset
     * @param input     the input data
     */
    virtual std::size_t dataFrameStart(std::size_t offset, const Util::ByteArray &input) const;

    /**
     * Find the end of a data frame from the given offset.
     * <br>
     * The default implementation returns the index of the next newline.
     * 
     * @param start     the index of the start of the frame
     * @param input     the input data
     */
    virtual std::size_t dataFrameEnd(std::size_t start, const Util::ByteArray &input) const;

    /**
     * Find the start of a data frame from the given offset.
     * <br>
     * The default implementation returns the offset of the first non-newline
     * character.
     * 
     * @param offset    the starting offset
     * @param input     the input data
     */
    virtual std::size_t controlFrameStart(std::size_t offset, const Util::ByteArray &input) const;

    /**
     * Find the end of a data frame from the given offset.
     * <br>
     * The default implementation returns the index of the next newline.
     * 
     * @param start     the index of the start of the frame
     * @param input     the input data
     */
    virtual std::size_t controlFrameEnd(std::size_t start, const Util::ByteArray &input) const;

    /**
     * Apply size limiting to the data buffer.  This is called during
     * data insertion while the mutex is held.
     * <br>
     * The default implementation shifts out the start of the buffer until
     * the total size is less than 2^16 bytes.
     * 
     * @param buffer the input and output buffer
     */
    virtual void limitDataBuffer(Util::ByteArray &buffer);

    /**
     * Apply size limiting to the control buffer.  This is called during
     * data insertion while the mutex is held.
     * <br>
     * The default implementation shifts out the start of the buffer until
     * the total size is less than 2^16 bytes.
     * 
     * @param buffer the input and output buffer
     */
    virtual void limitControlBuffer(Util::ByteArray &buffer);

    /**
     * Request a timeout at the given time.  An undefined time cancels all
     * timeouts.  Any pending timeouts before the given time will be cleared.
     * <br>
     * This may not be called by external threads unless the interface is 
     * paused.
     * 
     * @param time  the requested timeout time
     */
    void timeoutAt(double time);

    /**
     * Discard all data until the given time possibly discarding frames
     * after the end time.  Calls discardDataCompleted() at the end of the
     * discard time after all required frames have been discarded.
     * <br>
     * This may not be called by external threads unless the interface is 
     * paused.
     * 
     * @param endTime           the time the discard ends at, may be undefined to just discard frames
     * @param discardAfterEnd   the number of frames to discard after the end time
     */
    void discardData(double endTime, int discardAfterEnd = 0);

    /**
     * Discard all control until the given time possibly discarding frames
     * after the end time.  Calls discardDataCompleted() at the end of the
     * discard time after all required frames have been discarded.
     * <br>
     * This may not be called by external threads unless the interface is 
     * paused.
     * 
     * @param endTime           the time the discard ends at, may be undefined to just discard frames
     * @param discardAfterEnd   the number of frames to discard after the end time
     */
    void discardControl(double endTime, int discardAfterEnd = 0);

    /**
     * Indicate that the interface has lost the logging stream all values
     * before the given time have been emitted.  This should be called for
     * every invalid data frame processed that generates no data after the
     * buffers have been emptied (if it was the first frame to cause
     * communications loss).  Calling this with an undefined time will
     * cease the logging halt procedure.
     * <br>
     * That is, if the instrument is not communicating it should call
     * this for every data frame it receives and for the event that causes
     * the loss of communications.  If communications are restored without
     * receiving a data frame, then it must be called with undefined.
     * <br>
     * This may not be called by external threads unless the interface is 
     * paused.
     * 
     * @param time      the time of the loss, all logging values before this must have been emitted
     */
    void loggingLost(double time);

public:
    /**
     * Create the interface.
     *
     * @param loggingIdentifier the logging identifier code
     * @param loggingContext    the logging context if any
     */
    FramedInstrument(const std::string &loggingIdentifier,
                     const std::string &loggingContext = std::string());

    virtual ~FramedInstrument();

    void incomingData(const Util::ByteView &data,
                      double time,
                      AcquisitionInterface::IncomingDataType type = IncomingDataType::Stream) override;

    void incomingControl(const Util::ByteView &data,
                         double time,
                         AcquisitionInterface::IncomingDataType type = IncomingDataType::Stream) override;

    void incomingTimeout(double time) override;

    void incomingAdvance(double time) override;

    /**
     * Shutdown the instrument.  By default this just calls finishStreams()
     * followed by the parent initiateShutdown() (which terminates the thread).
     */
    void initiateShutdown() override;
};

}
}

#endif
