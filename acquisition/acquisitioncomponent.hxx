/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ACQUISITIONCOMPONENT_H
#define CPD3ACQUISITIONCOMPONENT_H

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <QtGlobal>
#include <QByteArray>
#include <QRunnable>
#include <QLoggingCategory>

#include "acquisition/acquisition.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/segment.hxx"
#include "core/timeutils.hxx"
#include "core/waitutils.hxx"
#include "core/component.hxx"

namespace CPD3 {
namespace Acquisition {

/**
 * A control interface for an acquisition component.  This allows the component
 * to write out data to the control stream.
 */
class CPD3ACQUISITION_EXPORT AcquisitionControlStream {
public:
    AcquisitionControlStream();

    virtual ~AcquisitionControlStream();

    /**
     * Accept incoming bytes to be written out to the control stream.
     * <br>
     * This function must be thread safe.
     * 
     * @param data  the data to write
     */
    virtual void writeControl(const Util::ByteView &data) = 0;

    virtual void writeControl(Util::ByteArray &&data);

    void writeControl(const QByteArray &data);

    void writeControl(const std::string &data);

    void writeControl(const QString &data);

    void writeControl(const char *data);

    /**
     * Reset the control and input streams if possible.
     */
    virtual void resetControl() = 0;

    /**
     * Request a timeout to be issued at the given time.  This replaces any
     * prior timeout requested.
     * <br>
     * This function must be thread safe.
     * 
     * @param time  the time to timeout at
     */
    virtual void requestTimeout(double time) = 0;
};

/**
 * The base class for interacting with system state.
 */
class CPD3ACQUISITION_EXPORT AcquisitionState {
public:
    AcquisitionState();

    virtual ~AcquisitionState();

    /**
     * Apply variable remapping.  This allows instrument input variables
     * to be remapped to other variables in the system.
     * <br>
     * The default implementation does nothing.
     * 
     * @param name  the internal name (generally the suffix-less name)
     * @param value the input and output value
     */
    virtual void remap(const Data::SequenceName::Component &name, Data::Variant::Root &value);

    /**
     * Set the system bypass.  This will only bypass the relevant subsystem
     * in the case of multiple bypasses.
     * 
     * @param flag      the bypass flag to set
     */
    virtual void setBypassFlag(const Data::Variant::Flag &flag = "Bypass") = 0;

    /**
     * Set the system to un-bypass.  This will only bypass the relevant 
     * subsystem in the case of multiple bypasses.
     * 
     * @param flag      the bypass flag to clear
     */
    virtual void clearBypassFlag(const Data::Variant::Flag &flag = "Bypass") = 0;

    /**
     * Set a flag in all "primary" flags outputs (F1_...).  This is generally
     * used to contaminate data.
     * 
     * @param flag  the contamination flag to set
     */
    virtual void setSystemFlag(const Data::Variant::Flag &flag = "Contaminated") = 0;

    /**
     * Clear a flag in all "primary" flags outputs (F1_...).  This is generally
     * used to contaminate data.
     * 
     * @param flag  the contamination flag to set
     */
    virtual void clearSystemFlag(const Data::Variant::Flag &flag = "Contaminated") = 0;

    /**
     * Request a system flush (discard values from the averaging) for the
     * given duration.  If set to less than or equal to zero, use the
     * system default.  This only applies to the relevant subsystem.
     * 
     * @param seconds   the duration of the flush
     */
    virtual void requestFlush(double seconds = -1.0) = 0;

    /**
     * Send the given command to the given target (or all targets in the
     * same subsystem group if empty).
     * 
     * @param target        the target (regular expression)
     * @param command       the command to send
     */
    virtual void sendCommand(const std::string &target, const Data::Variant::Read &command) = 0;

    /**
     * Set the additional flavors this target is contributing to the system.
     * Any flavors set here will be added to all variables belonging to any
     * subsystem this target belongs to.
     * <br>
     * This is normally used to set cut sizes by setting either the pm1,
     * pm10 or pm25 flavors.
     * 
     * @param flavors       the set of flavors being added by this target
     */
    virtual void setSystemFlavors(const Data::SequenceName::Flavors &flavors) = 0;

    /**
     * Set the averaging time for the system.  Changes to the averaging time
     * take effect immediately on all values rather than considering the
     * current position of the streams.  Generally that means that the average
     * before and after a change are questionable.  Setting the count to
     * zero of any unit disables averaging (values are written out at the
     * frequency and timing they are acquired at).
     */
    virtual void setAveragingTime(Time::LogicalTimeUnit unit,
                                  int count,
                                  bool setAligned = true) = 0;

    /**
     * Request a save of the local state.
     */
    virtual void requestStateSave() = 0;

    /**
     * Request a save of the global state.
     */
    virtual void requestGlobalStateSave() = 0;

    /**
     * Log an event to the system event log.
     * 
     * @param time          the time of the message
     * @param text          the text of the message
     * @param showRealtime  show in a realtime display
     * @param information   information pertaining to the message (e.x. autodetect parameters)
     */
    virtual void event(double time,
                       const QString &text,
                       bool showRealtime = true,
                       const Data::Variant::Read &information = Data::Variant::Read::empty()) = 0;


    /**
     * Indicate that the realtime stream has advanced to the given time.
     * <br>
     * The default implementation does nothing.
     * 
     * @param time  the time of the advance, no realtime data may be emitted before this
     */
    virtual void realtimeAdvanced(double time);

    /**
     * Indicate that the logging stream has halted at a given time.  A halt
     * means that all values before the given time have been emitted.  This
     * allows the system to advance the final output to the given time and
     * keeps the backlog from building up.
     * <br>
     * Generally this should be called when the interface losses communications
     * after it flushes buffers.
     * <br>
     * The default implementation does nothing.
     * 
     * @param time  the time of the loss, all values before this have been emitted
     */
    virtual void loggingHalted(double time);

    /**
     * Request a data "wakeup" (zero-length call to 
     * AcquisitionInterface::incomingData( const QByteArray &, double,
     * AcquisitionInterface::IncomingDataType ) ) at the given time.  This
     * may not actually occur (if real data comes in concurrently, for 
     * example).
     * <br>
     * The default implementation does nothing.
     * 
     * @param time  the requested time of the wakeup
     */
    virtual void requestDataWakeup(double time);

    /**
     * Request a control "wakeup" (zero-length call to 
     * AcquisitionInterface::incomingControl( const QByteArray &, double,
     * AcquisitionInterface::IncomingDataType ) ) at the given time.  This
     * may not actually occur (if real data comes in concurrently, for 
     * example).
     * <br>
     * The default implementation does nothing.
     * 
     * @param time  the requested time of the wakeup
     */
    virtual void requestControlWakeup(double time);
};

/**
 * The base class that all acquisition interfaces are derived from.  The
 * values emitted through the egress will be transformed according to the
 * system state (e.x. flavors set).
 */
class CPD3ACQUISITION_EXPORT AcquisitionInterface {
public:
    AcquisitionInterface();

    virtual ~AcquisitionInterface();

    /**
     * Set the control for this interface.  This does not return until the
     * interface point has been successfully changed.  If set to NULL no
     * control output will be written.
     * <br>
     * This function must be thread safe.
     * 
     * @param controlStream the control interface to change to
     */
    virtual void setControlStream(AcquisitionControlStream *controlStream) = 0;

    /**
     * Set the state for this interface.  This does not return until the
     * interface point has been successfully changed.
     * <br>
     * This function must be thread safe.
     * 
     * @param state     the control interface to change to
     */
    virtual void setState(AcquisitionState *state) = 0;

    /**
     * Set the egress point for realtime data.  This does not return until
     * the egress point is changed.  Any data will be discarded if the egress
     * is NULL.  Realtime data should be emitted as it is received with a "best
     * guess" end time.
     * 
     * @param egress    the new egress point
     */
    virtual void setRealtimeEgress(Data::StreamSink *egress) = 0;

    /**
     * Set the egress point for logged data.  This does not return until
     * the egress point is changed.  Any data will be discarded if the egress
     * is NULL.
     * 
     * @param egress    the new egress point
     */
    virtual void setLoggingEgress(Data::StreamSink *egress) = 0;

    /**
     * Set the egress point for persistent data.  This does not return until
     * the egress point is changed.  Any data will be discarded if the egress
     * is NULL.  Values are only emitted to this egress once they will not be
     * changed further and are no longer active.  
     * <br>
     * This egress MUST allow for un-ordered values between units.  That is, 
     * it must permit each unit to have independently ascending start times 
     * (this is more relaxed than a normal ingress point).  These values
     * are NOT duplicated to the realtime egress, so they must be independently
     * added there (with undefined end times, usually).
     * <br>
     * The default implementation calls endData() if not NULL and returns.
     * 
     * @param egress    the new egress point
     */
    virtual void setPersistentEgress(Data::StreamSink *egress);

    /**
     * Get the currently effective persistent values.
     * <br>
     * The default implementation returns an empty list.
     * 
     * @return the persistent values
     */
    virtual Data::SequenceValue::Transfer getPersistentValues();

    /**
     * The type of the incoming data being delivered.
     */
    enum class IncomingDataType {
        /**
         * The data is part of an in progress stream and should be buffered
         * when looking for delimiters.
         */
        Stream, /**
         * The data represents a complete "packet" (e.x. a complete line)
         * and should not be buffered with other data.
         */
        Complete, /**
         * The data represents the end of the stream.  Any buffering should
         * be finished.
         */
        Final
    };

    /**
     * Called to deliver bytes to the interface.
     * 
     * @param data  the incoming data bytes
     * @param time  the time attached to these bytes
     * @param type  the type of the bytes
     * @param pooledThread set if the calling thread is executing on the global thread pool
     */
    virtual void incomingData(const Util::ByteView &data,
                              double time,
                              IncomingDataType type = IncomingDataType::Stream) = 0;

    virtual void incomingData(Util::ByteArray &&data,
                              double time,
                              IncomingDataType type = IncomingDataType::Stream);

    void incomingData(const QByteArray &data,
                      double time,
                      IncomingDataType type = IncomingDataType::Stream);

    void incomingData(const std::string &data,
                      double time,
                      IncomingDataType type = IncomingDataType::Stream);

    void incomingData(const QString &data,
                      double time,
                      IncomingDataType type = IncomingDataType::Stream);

    void incomingData(const char *data,
                      double time,
                      IncomingDataType type = IncomingDataType::Stream);

    /**
     * Called to deliver bytes from a control stream (e.x. a capture of serial
     * port echo).
     * <br>
     * The default implementation does nothing.
     * 
     * @param data  the incoming data bytes
     * @param time  the time attached to these bytes
     * @param type  the type of the bytes
     * @param pooledThread set if the calling thread is executing on the global thread pool
     */
    virtual void incomingControl(const Util::ByteView &data,
                                 double time,
                                 IncomingDataType type = IncomingDataType::Stream);

    virtual void incomingControl(Util::ByteArray &&data,
                                 double time,
                                 IncomingDataType type = IncomingDataType::Stream);

    void incomingControl(const QByteArray &data,
                         double time,
                         IncomingDataType type = IncomingDataType::Stream);

    void incomingControl(const std::string &data,
                         double time,
                         IncomingDataType type = IncomingDataType::Stream);

    void incomingControl(const QString &data,
                         double time,
                         IncomingDataType type = IncomingDataType::Stream);

    void incomingControl(const char *data,
                         double time,
                         IncomingDataType type = IncomingDataType::Stream);

    /**
     * Called to wake up the interface if a specified timeout has elapsed.
     * The timeout is set using the control interface and may be ignored (e.x.
     * when reading from a file).
     * <br>
     * The default implementation does nothing.
     */
    virtual void incomingTimeout(double time);

    /**
     * Called when a command was issued by an external source.
     * <br>
     * The default implementation does nothing.
     * 
     * @param command   the command (type defined by the value of the hash that is defined)
     */
    virtual void incomingCommand(const Data::Variant::Read &command);

    /**
     * Optionally periodically to indicate that the incoming data, control, or
     * timeouts will not occur before the given time.  This can be used to
     * advance the realtime stream to the given time once all pending data
     * has been processed.
     * <br>
     * The default implementation does nothing.
     * 
     * @param time  the time to advance to
     */
    virtual void incomingAdvance(double time);

    /**
     * Get the metadata describing all commands the component can accept.  This
     * must be available before the thread is started.
     * <br>
     * The default implementation returns an undefined value
     * 
     * @return the command metadata description (a hash of commands)
     */
    virtual Data::Variant::Root getCommands();

    /**
     * Get a selection that matches all variables that the interface requires
     * to be explicitly grouped.  These variables are not assigned to the
     * interface's normal groups, instead they are assigned only to
     * an empty group set (they can still be assigned groups by explicit
     * group definitions).  This is normally used for persistent values to
     * ensure they don't end up in cut size selection.  This must be available 
     * before the thread is started.
     * <br>
     * The default implementation returns an empty selection.
     * 
     * @return a selection matching variables that are ungrouped by default
     */
    virtual Data::SequenceMatch::Composite getExplicitGroupedVariables();

    /**
     * Get the ingress point for realtime data values.
     * <br>
     * Ownership is retained by the acquisition interface.
     * <br>
     * The default implementation returns NULL
     * 
     * @return the ingress point for realtime data, if not NULL
     */
    virtual Data::StreamSink *getRealtimeIngress();


    /**
     * The current status of the autoprobe.
     */
    enum class AutoprobeStatus {
        /** In progress, current result unknown. */
        InProgress,

        /** Succeeded, conclusively identified as being applicable. */
        Success,

        /** Failed,  conclusively identified as being NOT applicable. */
        Failure
    };

    /**
     * Get the current status of the autoprobe.
     * <br>
     * The default implementation returns failure always.
     * 
     * @return the autoprobe status
     */
    virtual AutoprobeStatus getAutoprobeStatus();

    /**
     * Change a passive autoprobe to interactive.  That is, after this is
     * called, the autoprobe can issue bytes to the control interface and
     * have them serviced (set before this is called).
     * <br>
     * The default implementation does nothing.
     * 
     * @param time  the time of the attempt
     */
    virtual void autoprobeTryInteractive(double time);

    /**
     * Reset the component to passive mode for autoprobing.  This is called
     * after an interactive attempt fails or during a re-attempt and passive
     * detection.
     * <br>
     * The default implementation does nothing.
     *
     * @param time  the time of the release
     */
    virtual void autoprobeResetPassive(double time);

    /**
     * Promote an autoprobing instance to fully interactive and in control.
     * The output interface will be set before this is called.  After this
     * is called, the interface is in full control and logging of the
     * input.
     * <br>
     * The default implementation does nothing.
     * 
     * @param time  the time of the promotion
     */
    virtual void autoprobePromote(double time);

    /**
     * Promote an autoprobing instance to passive.  This means that the
     * interface should generate values if possible, but will not have
     * any control output.
     * <br>
     * The default implementation does nothing.
     * 
     * @param time  the time of the promotion
     */
    virtual void autoprobePromotePassive(double time);

    /**
     * Serialize the current state into the given stream.  The execution
     * of the interface should be paused during this time.  For example, this
     * would store normalization values.
     * <br>
     * The default implementation does nothing.
     * 
     * @param stream    the output stream
     */
    virtual void serializeState(QDataStream &stream);

    /**
     * De-serialize the current state from the given stream.  The execution
     * of the interface should be paused during this time.  For example, this
     * would restore normalization values.
     * <br>
     * The default implementation does nothing.
     * 
     * @param stream    the input stream
     */
    virtual void deserializeState(QDataStream &stream);

    /**
     * Called when state is being restored but there was nothing pre-existing.
     * <br>
     * The default implementation does nothing.
     */
    virtual void initializeState();

    /**
     * The the metadata for any system flags this component sets.
     * <br>
     * The default implementation returns an undefined value.
     * 
     * @return  the flags metadata
     */
    virtual Data::Variant::Root getSystemFlagsMetadata();

    /**
     * Get the instrument source metadata.  This is used to match the instrument
     * against possible candidates by serial number, etc.  Generally this
     * is equivalent to the "Source" field of all variables it generates.
     * <br>
     * The default implementation returns an undefined value.
     * 
     * @return  the instrument source metadata
     */
    virtual Data::Variant::Root getSourceMetadata();

    /**
     * The general status of the component.
     */
    enum class GeneralStatus {
        /** The component is operating normally. */
        Normal,

        /** The component has no communications with the instrument. */
        NoCommunications,

        /** The component is disabled. */
        Disabled,
    };

    /**
     * Get the general status of the component.
     * <br>
     * The default implementation always returns normal operation.
     * 
     * @return  the component status
     */
    virtual GeneralStatus getGeneralStatus();

    /**
     * The defaults used by the acquisition interface.
     */
    struct CPD3ACQUISITION_EXPORT AutomaticDefaults {
        /**
         * The IO interface default parameters used by the interface.
         */
        Data::Variant::Root interface;

        /**
         * The default instrument name.  This may contain "$1" which is replaced
         * by a line number, if specified and "$2" which is replaced by the instance
         * number on the line.
         */
        std::string name;

        /**
         * The level priority of autoprobing.  Larger levels are not tried until
         * all components within lower ones have failed.  Defaults to zero.
         */
        int autoprobeLevelPriority;

        /**
         * The order within the the level of autoprobing.  This is used to determine
         * the order that components are tried.  Defaults to zero.
         */
        int autoprobeInitialOrder;

        /**
         * The total number of attempts made to autodetect the instrument.  Defaults
         * to three.
         */
        int autoprobeTries;

        AutomaticDefaults();

        /**
         * Set the interface to serial parameters at the specified baud rate, no parity,
         * eight data bits and one stop bit.
         *
         * @param baud  the baud rate
         */
        void setSerialN81(int baud);
    };

    virtual AutomaticDefaults getDefaults();

    /**
     * Test if the interface has completed its execution.  Once this returns
     * true the interface may be destroyed at will.
     * 
     * @return true if the interface is finished
     */
    virtual bool isCompleted() = 0;

    /**
     * Wait for completion of the interface.
     * 
     * @param timeout   the maximum time to wait
     * @return          true if the interface has completed
     */
    virtual bool wait(double timeout = FP::undefined());

    /**
     * Signal thread termination as soon as possible.  This will not trigger
     * graceful shutdown procedures and the thread will exit as soon as
     * possible.
     */
    virtual void signalTerminate() = 0;

    /**
     * Start the interface.  This will be called before actual processing
     * on the interface is performed.
     * <br>
     * The default implementation does nothing.
     */
    virtual void start();

    /**
     * Initiate a graceful shutdown.  This may cause control data to be
     * emitted.  The thread will eventually exit when the shutdown is complete.
     * <br>
     * The default implementation calls signalTerminate()
     */
    virtual void initiateShutdown();

    /**
     * Emitted when the autoprobe status has changed.
     */
    Threading::Signal<> autoprobeStatusUpdated;

    /**
     * Emitted when the persistent values have had an "important" update.
     * For example, the start of a CLAP filter.  This will cause the current
     * ones to be written to the archive.
     */
    Threading::Signal<> persistentValuesUpdated;

    /**
     * Emitted when the source metadata has changed after initial construction.
     * For example, during start comms when the instrument serial number is
     * read back.  This can cause the autoprobing logic to discard/abort
     * the instrument if it fails to match.
     */
    Threading::Signal<> sourceMetadataUpdated;

    /**
     * Emitted when the general status of the component changes.  That is,
     * this should be emitted when the result of a call to getGeneralStatus() 
     * changes.
     */
    Threading::Signal<> generalStatusUpdated;

    /**
     * Emitted when the interface has completed.
     */
    Threading::Signal<> completed;

};

/**
 * A class that provides a base for "conventional" instruments.  This provides
 * a simple interface for executing a processing function on the global
 * thread pool.
 */
class CPD3ACQUISITION_EXPORT InstrumentAcquisitionInterface : public AcquisitionInterface {
    std::vector<Data::Variant::Root> commandQueue;
    enum class ControlState {
        Initialize, Running, PauseRequested, Paused, Terminated, Complete
    } controlState;

    std::condition_variable request;
    std::condition_variable response;

    std::string loggingName;

    std::thread thread;

    AcquisitionControlStream *pendingControlStream;
    AcquisitionState *pendingState;
    Data::StreamSink *pendingRealtimeEgress;
    Data::StreamSink *pendingLoggingEgress;
    Data::StreamSink *pendingPersistentEgress;

    void run();

protected:
    /**
     * The main mutex.  Any data accessed from prepare() must be locked
     * by this.
     */
    std::mutex mutex;

    /**
     * The control stream.  This must NOT be accessed while the mutex is held.
     * This may be NULL.
     */
    AcquisitionControlStream *controlStream;

    /**
     * The state control.  This must NOT be accessed while the mutex is held.
     * This may be NULL.
     */
    AcquisitionState *state;

    /**
     * Realtime egress.  This must NOT be accessed while the mutex is held.
     * This may be NULL.
     */
    Data::StreamSink *realtimeEgress;

    /**
     * Logging egress.  This must NOT be accessed while the mutex is held.
     * This may be NULL.
     */
    Data::StreamSink *loggingEgress;

    /**
     * Persistent egress.  This must NOT be accessed while the mutex is held.
     * This may be NULL.
     */
    Data::StreamSink *persistentEgress;

    /**
     * The logging category to use.
     */
    QLoggingCategory log;

    /**
     * Notify the handler that some part of the internal state
     * has been updated.  This should be called whenever a condition
     * that could change the state of the prepare() call changes, for
     * example.
     */
    void notifyUpdate();

    /**
     * Prepare for processing.  This must return TRUE if any processing
     * needs to be done.  This will be called while the mutex is held.
     * <br>
     * The default implementation does nothing and returns false.
     * 
     * @return true to continue processing
     */
    virtual bool prepare();

    /**
     * Run processing.  This will be called without the mutex held.  This will
     * not be called if prepare returned false.
     * <br>
     * The default implementation does nothing.
     */
    virtual void process();

    /**
     * Called for an incoming command.  This will be called without the mutex
     * held.
     * <br>
     * The default implementation does nothing.
     * 
     * @param command   the incoming command
     */
    virtual void command(const Data::Variant::Read &command);

    /**
     * A wrapper that call the function of the same name on the state, if
     * it exists or does nothing if it does not.
     * <br>
     * This must NOT be called with the mutex held.
     * 
     * @param time          the time of the message
     * @param text          the text of the message
     * @param showRealtime  show in a realtime display
     * @param information   information pertaining to the message (e.x. autodetect parameters)
     * @see AcquisitionState::event(double, const QString &, bool, const Data::Variant::Read &)
     */
    inline void event(double time,
                      const QString &text,
                      bool showRealtime = true,
                      const Data::Variant::Read &information = Data::Variant::Read::empty())
    {
        if (state) {
            state->event(time, text, showRealtime, information);
        }
    }

    /**
     * A wrapper that call the function of the same name on the state, if
     * it exists or does nothing if it does not.
     * <br>
     * This must NOT be called with the mutex held.
     * 
     * @param name  the internal name (generally the suffix-less name)
     * @param value the input and output value
     * @see AcquisitionState::remap(const QString &, Data::Variant::Root &)
     */
    inline void remap(const Data::SequenceName::Component &name, Data::Variant::Root &value)
    {
        if (state) {
            state->remap(name, value);
        }
    }

    /**
     * Pause the interface by blocking the thread loop until it is unpaused.
     * This can be used to implement a block for serialization or
     * de-serialization without having to protect all parameters with the
     * mutex.
     * <br>
     * This MUST NOT be used from the thread loop (e.x. prepare() or
     * process()
     */
    class CPD3ACQUISITION_EXPORT PauseLock final {
        InstrumentAcquisitionInterface &interface;
    public:
        explicit PauseLock(InstrumentAcquisitionInterface &interface);

        ~PauseLock();
    };

public:
    /**
     * Create the interface.
     *
     * @param loggingIdentifier the logging identifier code
     * @param loggingContext    the logging context if any
     */
    InstrumentAcquisitionInterface(const std::string &loggingIdentifier,
                                   const std::string &loggingContext = {});

    virtual ~InstrumentAcquisitionInterface();

    void setControlStream(AcquisitionControlStream *controlStream) override;

    void setState(AcquisitionState *state) override;

    void setRealtimeEgress(Data::StreamSink *egress) override;

    void setLoggingEgress(Data::StreamSink *egress) override;

    void setPersistentEgress(Data::StreamSink *egress) override;

    void incomingCommand(const Data::Variant::Read &command) override;

    void start() override;

    bool isCompleted() override;

    void signalTerminate() override;
};

/**
 * The component interface for acquisition component.
 */
class CPD3ACQUISITION_EXPORT AcquisitionComponent {
public:
    /**
     * Get an options object (set to all defaults) for the acquisition type.
     * 
     * @return an options object for this acquisition type
     */
    virtual ComponentOptions getPassiveOptions();

    /**
     * Gets a list of examples that show usage of the component.  The caller
     * will use the specified options to construct usage in the context it's
     * called (e.x. command lines).
     * 
     * @return a list in display order of examples
     */
    virtual QList<ComponentExample> getPassiveExamples();

    /**
     * Create a new acquisition interface with the given options.  The caller
     * must delete it when done.  This should be configured for passive
     * reading of input.
     * 
     * @param options           the options to use
     * @param loggingContext    the context information to log with
     * @return                  a new acquisition interface
     */
    virtual std::unique_ptr<
            AcquisitionInterface> createAcquisitionPassive(const ComponentOptions &options = {},
                                                           const std::string &loggingContext = {}) = 0;

    /**
     * Create a new acquisition interface with the given configuration.  The 
     * caller must delete it when done.  This should be configured for passive
     * reading of input.
     * 
     * @param config            the configuration
     * @param loggingContext    the context information to log with
     * @return                  a new acquisition interface
     */
    virtual std::unique_ptr<
            AcquisitionInterface> createAcquisitionPassive(const Data::ValueSegment::Transfer &config,
                                                           const std::string &loggingContext = {}) = 0;

    /**
     * Create a new acquisition interface with the given configuration.  The 
     * caller must delete it when done.  This should be configured for
     * autoprobing of devices.  If this returns NULL the component can not
     * be autoprobed for the given configuration.  A timeout is issued
     * immediately that can be used to start the communications sequence.
     * 
     * @param config            the configuration
     * @param loggingContext    the context information to log with
     * @return                  a new acquisition interface or NULL
     */
    virtual std::unique_ptr<
            AcquisitionInterface> createAcquisitionAutoprobe(const Data::ValueSegment::Transfer &config = {},
                                                             const std::string &loggingContext = {});

    /**
     * Create a new acquisition interface with the given configuration.  The 
     * caller must delete it when done.  This should be configured for
     * full interactive control of a device.  The control and state will
     * be set before the thread is started and a timeout will be scheduled
     * immediately.  That is, the timeout should be used to actually start
     * the communications initialization sequence.
     * 
     * @param config            the configuration
     * @param loggingContext    the context information to log with
     * @return                  a new acquisition interface
     */
    virtual std::unique_ptr<
            AcquisitionInterface> createAcquisitionInteractive(const Data::ValueSegment::Transfer &config = {},
                                                               const std::string &loggingContext = {});
};

}
}

Q_DECLARE_INTERFACE(CPD3::Acquisition::AcquisitionComponent,
                    "CPD3.Acquisition.AcquisitionComponent/1.0");

#endif
