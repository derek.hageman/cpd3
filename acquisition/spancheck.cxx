/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "acquisition/spancheck.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"


Q_LOGGING_CATEGORY(log_acquisition_spancheck, "cpd3.acquisition.spancheck", QtWarningMsg)

using namespace CPD3::Data;
using namespace CPD3::Algorithms;
using namespace CPD3::Smoothing;


namespace CPD3 {
namespace Acquisition {

/** @file acquisition/spancheck.hxx
 * Provides common utilities for dealing with gas span checks.
 */

static Variant::Read makeDefaultConfiguration(const Variant::Read &internal,
                                              const Variant::Read &external)
{
    if (!external.exists())
        return internal;
    return Variant::Root::overlay(Variant::Root(internal), Variant::Root(external)).read();
}

NephelometerSpancheckController::NephelometerSpancheckController(const ValueSegment::Transfer &configData,
                                                                 const Variant::Read &defaultConfig,
                                                                 Flags f) : interface(NULL),
                                                                            flags(f),
                                                                            state(Inactive),
                                                                            currentTime(
                                                                                    FP::undefined()),
                                                                            stateBeginTime(
                                                                                    FP::undefined()),
                                                                            stateEndTime(
                                                                                    FP::undefined()),
                                                                            pendingCommands(),
                                                                            config(),
                                                                            gasMinimumSample(NULL),
                                                                            gasMaximumSample(NULL),
                                                                            gasFlush(NULL),
                                                                            airMinimumSample(NULL),
                                                                            airMaximumSample(NULL),
                                                                            airFlush(NULL),
                                                                            data(),
                                                                            dataAll(configData),
                                                                            dataAllWavelengths(),
                                                                            dataAllAngles(),
                                                                            chopperScaleDark(
                                                                                    (360.0 *
                                                                                            22.994) /
                                                                                            60.0),
                                                                            chopperScaleMeasure(
                                                                                    (360.0 *
                                                                                            22.994) /
                                                                                            140.0),
                                                                            chopperScaleReference(
                                                                                    (360.0 *
                                                                                            22.994) /
                                                                                            40.0)
{

    config.append(Configuration(
            ValueSegment(FP::undefined(), FP::undefined(), Variant::Root(defaultConfig)),
            FP::undefined(), FP::undefined()));
    for (const auto &add: configData) {
        Range::overlayFragmenting(config, add);
    }


    DynamicTimeInterval::Variable *defaultTime = new DynamicTimeInterval::Variable;

    defaultTime->set(
            makeDefaultConfiguration(Variant::Root(300), defaultConfig["Gas/MinimumSample"]));
    gasMinimumSample =
            DynamicTimeInterval::fromConfiguration(configData, "Gas/MinimumSample", FP::undefined(),
                                                   FP::undefined(), true, defaultTime);

    defaultTime->set(makeDefaultConfiguration(Variant::Root("Undefined"),
                                              defaultConfig["Gas/MaximumSample"]));
    gasMaximumSample =
            DynamicTimeInterval::fromConfiguration(configData, "Gas/MaximumSample", FP::undefined(),
                                                   FP::undefined(), true, defaultTime);

    defaultTime->set(makeDefaultConfiguration(Variant::Root(600), defaultConfig["Gas/Flush"]));
    gasFlush = DynamicTimeInterval::fromConfiguration(configData, "Gas/Flush", FP::undefined(),
                                                      FP::undefined(), true, defaultTime);

    defaultTime->set(
            makeDefaultConfiguration(Variant::Root(600), defaultConfig["Air/MinimumSample"]));
    airMinimumSample =
            DynamicTimeInterval::fromConfiguration(configData, "Air/MinimumSample", FP::undefined(),
                                                   FP::undefined(), true, defaultTime);

    defaultTime->set(makeDefaultConfiguration(Variant::Root("Undefined"),
                                              defaultConfig["Air/MaximumSample"]));
    airMaximumSample =
            DynamicTimeInterval::fromConfiguration(configData, "Air/MaximumSample", FP::undefined(),
                                                   FP::undefined(), true, defaultTime);

    defaultTime->set(makeDefaultConfiguration(Variant::Root(180), defaultConfig["Air/Flush"]));
    airFlush = DynamicTimeInterval::fromConfiguration(configData, "Air/Flush", FP::undefined(),
                                                      FP::undefined(), true, defaultTime);

    delete defaultTime;
}

NephelometerSpancheckController::~NephelometerSpancheckController()
{
    delete gasMinimumSample;
    delete gasMaximumSample;
    delete gasFlush;
    delete airMinimumSample;
    delete airMaximumSample;
    delete airFlush;
}

NephelometerSpancheckController::Configuration::Configuration() : start(FP::undefined()),
                                                                  end(FP::undefined()),
                                                                  terminalAngle(180.0),
                                                                  gas(Rayleigh::CO2),
                                                                  commandsStart(),
                                                                  commandsEnd(),
                                                                  commandsActivateAir(),
                                                                  commandsDeactivateAir(),
                                                                  commandsActivateGas(),
                                                                  commandsDeactivateGas()
{ }

NephelometerSpancheckController::Configuration::Configuration(const Configuration &other,
                                                              double s,
                                                              double e) : start(s),
                                                                          end(e),
                                                                          terminalAngle(
                                                                                  other.terminalAngle),
                                                                          gas(other.gas),
                                                                          commandsStart(
                                                                                  other.commandsStart),
                                                                          commandsEnd(
                                                                                  other.commandsEnd),
                                                                          commandsActivateAir(
                                                                                  other.commandsActivateAir),
                                                                          commandsDeactivateAir(
                                                                                  other.commandsDeactivateAir),
                                                                          commandsActivateGas(
                                                                                  other.commandsActivateGas),
                                                                          commandsDeactivateGas(
                                                                                  other.commandsDeactivateGas)
{ }

NephelometerSpancheckController::Configuration::Configuration(const ValueSegment &other,
                                                              double s,
                                                              double e) : start(s),
                                                                          end(e),
                                                                          terminalAngle(180.0),
                                                                          gas(Rayleigh::CO2),
                                                                          commandsStart(),
                                                                          commandsEnd(),
                                                                          commandsActivateAir(),
                                                                          commandsDeactivateAir(),
                                                                          commandsActivateGas(),
                                                                          commandsDeactivateGas()
{
    setFromSegment(other);
}

NephelometerSpancheckController::Configuration::Configuration(const Configuration &under,
                                                              const ValueSegment &over,
                                                              double s,
                                                              double e) : start(s),
                                                                          end(e),
                                                                          terminalAngle(
                                                                                  under.terminalAngle),
                                                                          gas(under.gas),
                                                                          commandsStart(
                                                                                  under.commandsStart),
                                                                          commandsEnd(
                                                                                  under.commandsEnd),
                                                                          commandsActivateAir(
                                                                                  under.commandsActivateAir),
                                                                          commandsDeactivateAir(
                                                                                  under.commandsDeactivateAir),
                                                                          commandsActivateGas(
                                                                                  under.commandsActivateGas),
                                                                          commandsDeactivateGas(
                                                                                  under.commandsDeactivateGas)
{
    setFromSegment(over);
}

void NephelometerSpancheckController::Configuration::setFromSegment(const ValueSegment &config)
{
    {
        double check = config["TerminalAngle"].toDouble();
        if (FP::defined(check) && check >= 0.0 && check <= 360.0)
            terminalAngle = check;
    }

    if (config["Gas/Type"].exists())
        gas = Rayleigh::gasFromString(config["Gas/Type"].toQString());

    setCommands(commandsStart, config["Start"]);
    setCommands(commandsEnd, config["End"]);
    setCommands(commandsActivateAir, config["Air/Activate"]);
    setCommands(commandsDeactivateAir, config["Air/Deactivate"]);
    setCommands(commandsActivateGas, config["Gas/Activate"]);
    setCommands(commandsDeactivateGas, config["Gas/Deactivate"]);
}

void NephelometerSpancheckController::Configuration::setCommands(std::unordered_map<QString,
                                                                                    Variant::Root> &target,
                                                                 const Variant::Read &insert) const
{
    if (insert["Commands"].exists()) {
        Util::insert_or_assign(target, QString(), Variant::Root::overlay(target[QString()],
                                                                         Variant::Root(
                                                                                 insert["Commands"])));
    }

    for (auto add : insert["InstrumentCommands"].toHash()) {
        if (add.first.empty())
            continue;
        Util::insert_or_assign(target, QString::fromStdString(add.first),
                               Variant::Root::overlay(target[QString::fromStdString(add.first)],
                                                      Variant::Root(add.second)));
    }
}


NephelometerSpancheckController::SampleData::SampleData(const ValueSegment::Transfer &config)
        : seenParameters()
{
    Variant::Write defaultSmoothing = Variant::Write::empty();
    defaultSmoothing["Type"].setString("Forever");

    scattering.reset(
            BaselineSmoother::fromConfiguration(defaultSmoothing, config, "Smoothing/Scattering"));
    measurementCounts.reset(
            BaselineSmoother::fromConfiguration(defaultSmoothing, config, "Smoothing/Measurement"));
    referenceCounts.reset(
            BaselineSmoother::fromConfiguration(defaultSmoothing, config, "Smoothing/Reference"));
    darkCounts.reset(
            BaselineSmoother::fromConfiguration(defaultSmoothing, config, "Smoothing/Dark"));
    temperature.reset(
            BaselineSmoother::fromConfiguration(defaultSmoothing, config, "Smoothing/Temperature"));
    pressure.reset(
            BaselineSmoother::fromConfiguration(defaultSmoothing, config, "Smoothing/Pressure"));
    revolutions.reset(
            BaselineSmoother::fromConfiguration(defaultSmoothing, config, "Smoothing/Revolutions"));
}

NephelometerSpancheckController::SampleData::SampleData(const SampleData &other) : seenParameters(
        other.seenParameters),
                                                                                   scattering(
                                                                                           other.scattering
                                                                                                ->clone()),
                                                                                   measurementCounts(
                                                                                           other.measurementCounts
                                                                                                ->clone()),
                                                                                   referenceCounts(
                                                                                           other.referenceCounts
                                                                                                ->clone()),
                                                                                   darkCounts(
                                                                                           other.darkCounts
                                                                                                ->clone()),
                                                                                   temperature(
                                                                                           other.temperature
                                                                                                ->clone()),
                                                                                   pressure(
                                                                                           other.pressure
                                                                                                ->clone()),
                                                                                   revolutions(
                                                                                           other.revolutions
                                                                                                ->clone())
{ }

NephelometerSpancheckController::SampleData &NephelometerSpancheckController::SampleData::operator=(
        const SampleData &other)
{
    seenParameters = other.seenParameters;
    scattering.reset(other.scattering->clone());
    measurementCounts.reset(other.measurementCounts->clone());
    referenceCounts.reset(other.referenceCounts->clone());
    darkCounts.reset(other.darkCounts->clone());
    temperature.reset(other.temperature->clone());
    pressure.reset(other.pressure->clone());
    revolutions.reset(other.revolutions->clone());
    return *this;
}

NephelometerSpancheckController::SampleData::SampleData(SampleData &&) = default;

NephelometerSpancheckController::SampleData &NephelometerSpancheckController::SampleData::operator=(
        SampleData &&) = default;

NephelometerSpancheckController::SampleData::~SampleData() = default;

void NephelometerSpancheckController::SampleData::overlay(const SampleData &other)
{
    if (other.seenParameters.contains(NephelometerSpancheckController::Scattering)) {
        scattering.reset(other.scattering->clone());
    }
    if (other.seenParameters.contains(NephelometerSpancheckController::MeasurementCounts)) {
        measurementCounts.reset(other.measurementCounts->clone());
    }
    if (other.seenParameters.contains(NephelometerSpancheckController::ReferenceCounts)) {
        referenceCounts.reset(other.referenceCounts->clone());
    }
    if (other.seenParameters.contains(NephelometerSpancheckController::DarkCounts)) {
        darkCounts.reset(other.darkCounts->clone());
    }
    if (other.seenParameters.contains(NephelometerSpancheckController::Temperature)) {
        temperature.reset(other.temperature->clone());
    }
    if (other.seenParameters.contains(NephelometerSpancheckController::Pressure)) {
        pressure.reset(other.pressure->clone());
    }
    if (other.seenParameters.contains(NephelometerSpancheckController::Revolutions)) {
        revolutions.reset(other.revolutions->clone());
    }

    seenParameters |= other.seenParameters;
}

NephelometerSpancheckController::AngleData::AngleData(const AngleData &) = default;

NephelometerSpancheckController::AngleData &NephelometerSpancheckController::AngleData::operator=(
        const AngleData &) = default;

NephelometerSpancheckController::WavelengthData::WavelengthData(std::size_t idx,
                                                                NephelometerSpancheckController *p)
        : parent(p),
          wavelengthIndex(idx),
          wavelength(FP::undefined()),
          code(),
          angles(),
          deadTimeCorrection(0)
{ }

NephelometerSpancheckController::WavelengthData::WavelengthData(const WavelengthData &) = default;

NephelometerSpancheckController::WavelengthData &NephelometerSpancheckController::WavelengthData::operator=(
        const WavelengthData &) = default;

NephelometerSpancheckController::WavelengthData::WavelengthData(WavelengthData &&) = default;

NephelometerSpancheckController::WavelengthData &NephelometerSpancheckController::WavelengthData::operator=(
        WavelengthData &&) = default;

NephelometerSpancheckController::Interface::Interface() = default;

NephelometerSpancheckController::Interface::~Interface() = default;

void NephelometerSpancheckController::Interface::setBypass(bool)
{ }

void NephelometerSpancheckController::Interface::switchToFilteredAir()
{ }

void NephelometerSpancheckController::Interface::switchToGas(Rayleigh::Gas)
{ }

void NephelometerSpancheckController::Interface::issueZero()
{ }

void NephelometerSpancheckController::Interface::issueCommand(const QString &,
                                                              const Variant::Read &)
{ }

bool NephelometerSpancheckController::Interface::start()
{ return true; }

void NephelometerSpancheckController::Interface::completed()
{ }

void NephelometerSpancheckController::Interface::aborted()
{ }

void NephelometerSpancheckController::setInterface(Interface *i)
{ interface = i; }

void NephelometerSpancheckController::WavelengthData::overlay(const std::unordered_map<double,
                                                                                       AngleData> &incomingData)
{
    for (const auto &add : incomingData) {
        auto target = angles.find(add.first);
        if (target == angles.end())
            target = angles.emplace(add.first, parent->dataAll).first;
        target->second.overlay(add.second);
    }
}

void NephelometerSpancheckController::WavelengthData::setWavelength(double wavelength,
                                                                    const QString &code)
{
    this->wavelength = wavelength;
    this->code = code;
}

void NephelometerSpancheckController::setWavelength(int index,
                                                    double wavelength,
                                                    const QString &code)
{
    Q_ASSERT(index >= 0);
    while (static_cast<std::size_t>(index) >= dataAllAngles.size()) {
        dataAllAngles.emplace_back(dataAll);
    }
    while (static_cast<std::size_t>(index) >= data.size()) {
        data.emplace_back(data.size(), this);
        data.back().overlay(dataAllWavelengths);
    }
    data[index].setWavelength(wavelength, code);
}

void NephelometerSpancheckController::setDeadTimeCorrection(int index, double k)
{
    Q_ASSERT(index >= 0);
    Q_ASSERT(static_cast<std::size_t>(index) < data.size());
    data[index].setDeadTimeCorrection(k);
}

void NephelometerSpancheckController::terminate()
{
    Q_ASSERT(!config.isEmpty());

    switch (state) {
    case Inactive:
        break;
    case Initializing:
    case GasAirFlush:
    case GasFlush:
    case GasSample:
    case AirFlush:
    case AirSample:
    case Aborting:

        qCDebug(log_acquisition_spancheck) << "Spancheck terminated in state" << state;
        state = Inactive;

        mergePendingCommands(config.first().commandsEnd);
        issueCommands(pendingCommands);
        pendingCommands.clear();
        break;
    }
}

void NephelometerSpancheckController::addCommands(Variant::Write &target) const
{
    target.hash("StartSpancheck").hash("DisplayName").setString("Start &Spancheck");
    target.hash("StartSpancheck").hash("ToolTip").setString("Start a span gas calibration check.");
    target.hash("StartSpancheck").hash("Include").array(0).hash("Type").setString("Equal");
    target.hash("StartSpancheck").hash("Include").array(0).hash("Value").setString("Run");
    target.hash("StartSpancheck").hash("Include").array(0).hash("Variable").setString("ZSTATE");
    target.hash("StartSpancheck").hash("Aggregate").setBool(true);

    target.hash("StopSpancheck").hash("DisplayName").setString("Stop &Spancheck");
    target.hash("StopSpancheck")
          .hash("ToolTip")
          .setString("Abort the current span gas calibration check.");
    target.hash("StopSpancheck").hash("Include").array(0).hash("Type").setString("Equal");
    target.hash("StopSpancheck").hash("Include").array(0).hash("Value").setString("Spancheck");
    target.hash("StopSpancheck").hash("Include").array(0).hash("Variable").setString("ZSTATE");
    target.hash("StopSpancheck").hash("Aggregate").setBool(true);
}

void NephelometerSpancheckController::command(const Variant::Read &command)
{
    if (command["StartSpancheck"].exists()) {
        switch (state) {
        case Inactive:
        case Aborting:
            state = Initializing;
            qCDebug(log_acquisition_spancheck) << "Spancheck start command received";
            break;
        case Initializing:
        case GasAirFlush:
        case GasFlush:
        case GasSample:
        case AirFlush:
        case AirSample:
            qCDebug(log_acquisition_spancheck)
                << "Ignoring spancheck start command while an active spancheck is in progress (state"
                << state << ")";
            break;
        }
    }

    if (command["StopSpancheck"].exists()) {
        switch (state) {
        case Inactive:
        case Aborting:
            qCDebug(log_acquisition_spancheck)
                << "Spancheck stop command ignored since no spancheck is in progress";
            break;
        case Initializing:
        case GasAirFlush:
        case GasFlush:
        case GasSample:
        case AirFlush:
        case AirSample:
            state = Aborting;
            qCDebug(log_acquisition_spancheck) << "Spancheck aborting in state" << state;
            break;
        }
    }
}

void NephelometerSpancheckController::issueCommands(const std::unordered_map<QString,
                                                                             Variant::Read> &commands)
{
    if (interface == NULL)
        return;
    for (const auto &issue : commands) {
        interface->issueCommand(issue.first, issue.second);
    }
}

void NephelometerSpancheckController::mergePendingCommands(const std::unordered_map<QString,
                                                                                    Variant::Root> &overlay)
{
    for (const auto &add : overlay) {
        auto target = pendingCommands.find(add.first);
        if (target == pendingCommands.end()) {
            pendingCommands.emplace(add.first, add.second);
            continue;
        }
        target->second = Variant::Root::overlay(Variant::Root(target->second), add.second).read();
    }
}

void NephelometerSpancheckController::SampleData::reset()
{
    scattering->reset();
    measurementCounts->reset();
    referenceCounts->reset();
    darkCounts->reset();
    revolutions->reset();
    temperature->reset();
    pressure->reset();
}

void NephelometerSpancheckController::AngleData::reset()
{
    gas.reset();
    air.reset();
}

void NephelometerSpancheckController::AngleData::reset(bool resetGas)
{
    if (resetGas)
        gas.reset();
    else
        air.reset();
}

void NephelometerSpancheckController::WavelengthData::reset()
{
    angles.clear();
}

void NephelometerSpancheckController::WavelengthData::reset(bool resetGas)
{
    for (auto &add : angles) {
        add.second.reset(resetGas);
    }
}

void NephelometerSpancheckController::reset()
{
    dataAllAngles.clear();
    dataAllWavelengths.clear();
    dataAll.reset();
    for (auto &r : data) {
        r.reset();
    }
}

static double calculateDensity(double T, double P)
{
    if (!FP::defined(T) || !FP::defined(P))
        return FP::undefined();
    if (T < 150.0)
        T += 273.15;
    if (T < 150.0 || T > 350.0 || P < 10.0 || P > 2000.0)
        return FP::undefined();
    return (P / 1013.25) * (273.15 / T);
}

static double scatteringToSTP(double scattering, double density)
{
    if (!FP::defined(scattering) || !FP::defined(density))
        return FP::undefined();
    if (density < 0.01)
        return FP::undefined();
    return scattering / density;
}

double NephelometerSpancheckController::WavelengthData::correctCounts(double counts,
                                                                      double revolutions,
                                                                      double scale) const
{
    if (!FP::defined(counts))
        return FP::undefined();
    if (parent->flags.testFlag(NephelometerSpancheckController::Chopper)) {
        if (!FP::defined(revolutions) || revolutions < 0.01)
            return FP::undefined();
        counts *= scale / revolutions;
    }
    if (FP::defined(deadTimeCorrection) && deadTimeCorrection != 0.0)
        counts *= (counts * deadTimeCorrection + 1.0);
    return counts;
}

Variant::Write NephelometerSpancheckController::WavelengthData::calculate(double angle,
                                                                          const AngleData &data) const
{
    Variant::Write result = Variant::Write::empty();

    double gasT = data.gas.getTemperature();
    double gasP = data.gas.getPressure();
    double gasDensity = calculateDensity(gasT, gasP);
    double gasScattering = scatteringToSTP(data.gas.getScattering(), gasDensity);
    double gasRevolutions = data.gas.getRevolutions();
    double gasCDark =
            correctCounts(data.gas.getDarkCounts(), gasRevolutions, parent->chopperScaleDark);
    double gasCMeasure = correctCounts(data.gas.getMeasurementCounts(), gasRevolutions,
                                       parent->chopperScaleMeasure);
    double gasCReference = correctCounts(data.gas.getReferenceCounts(), gasRevolutions,
                                         parent->chopperScaleReference);
    double gasRayleighScattering = FP::undefined();
    if (FP::defined(wavelength) && wavelength > 0.0) {
        gasRayleighScattering =
                Rayleigh::angleScattering(wavelength, angle, parent->config.first().terminalAngle,
                                          parent->config.first().gas);
    }

    result.hash("Gas").hash("Type").setString(Rayleigh::gasToString(parent->config.first().gas));
    result.hash("Gas").hash("Bs").setDouble(gasScattering);
    result.hash("Gas").hash("Bsf").setDouble(gasRayleighScattering);
    result.hash("Gas").hash("T").setDouble(gasT);
    result.hash("Gas").hash("P").setDouble(gasP);
    if (!parent->flags.testFlag(NephelometerSpancheckController::NoCounts)) {
        result.hash("Gas").hash("Cd").setDouble(gasCDark);
        result.hash("Gas").hash("Cf").setDouble(gasCReference);
        result.hash("Gas").hash("C").setDouble(gasCMeasure);
    }
    if (parent->flags.testFlag(NephelometerSpancheckController::Chopper)) {
        result.hash("Gas").hash("Revs").setDouble(gasRevolutions);
    }


    double airT = data.air.getTemperature();
    double airP = data.air.getPressure();
    double airDensity = calculateDensity(airT, airP);
    double airScattering = scatteringToSTP(data.air.getScattering(), airDensity);
    double airRevolutions = data.air.getRevolutions();
    double airCDark =
            correctCounts(data.air.getDarkCounts(), airRevolutions, parent->chopperScaleDark);
    double airCMeasure = correctCounts(data.air.getMeasurementCounts(), airRevolutions,
                                       parent->chopperScaleMeasure);
    double airCReference = correctCounts(data.air.getReferenceCounts(), airRevolutions,
                                         parent->chopperScaleReference);
    double airRayleighScattering = FP::undefined();
    if (FP::defined(wavelength) && wavelength > 0.0) {
        airRayleighScattering =
                Rayleigh::angleScattering(wavelength, angle, parent->config.first().terminalAngle,
                                          Rayleigh::Air);
    }

    result.hash("Air").hash("Bs").setDouble(airScattering);
    result.hash("Air").hash("Bsf").setDouble(airRayleighScattering);
    result.hash("Air").hash("T").setDouble(airT);
    result.hash("Air").hash("P").setDouble(airP);
    if (!parent->flags.testFlag(NephelometerSpancheckController::NoCounts)) {
        result.hash("Air").hash("Cd").setDouble(airCDark);
        result.hash("Air").hash("Cf").setDouble(airCReference);
        result.hash("Air").hash("C").setDouble(airCMeasure);
    }
    if (parent->flags.testFlag(NephelometerSpancheckController::Chopper)) {
        result.hash("Air").hash("Revs").setDouble(airRevolutions);
    }


    double correctedScattering = FP::undefined();
    if (FP::defined(airRayleighScattering) &&
            FP::defined(gasScattering) &&
            FP::defined(airScattering)) {
        correctedScattering = gasScattering - airScattering + airRayleighScattering;
        result.hash("Gas").hash("Bsc").setDouble(correctedScattering);
    }

    if (FP::defined(correctedScattering) &&
            FP::defined(gasRayleighScattering) &&
            fabs(gasRayleighScattering) > 0.01) {
        double pctError = (correctedScattering / gasRayleighScattering - 1.0) * 100.0;
        result.hash("PCT").setDouble(pctError);
    }

    if (!parent->flags.testFlag(NephelometerSpancheckController::NoCounts) &&
            FP::defined(gasCDark) &&
            FP::defined(gasCMeasure) &&
            FP::defined(airCDark) &&
            FP::defined(airCMeasure) &&
            FP::defined(gasDensity) &&
            FP::defined(airDensity) &&
            gasDensity > 0.01 &&
            airDensity > 0.01 &&
            FP::defined(gasRayleighScattering) &&
            FP::defined(airRayleighScattering)) {
        double ratio = gasRayleighScattering / airRayleighScattering - 1.0;
        if (fabs(ratio) > 0.01) {
            double cGas = (gasCMeasure - gasCDark) / gasDensity;
            double cAir = (airCMeasure - airCDark) / airDensity;
            double sensitivityFactor = (cGas - cAir) / ratio;
            result.hash("Cc").setDouble(sensitivityFactor);
        }
    }

    if (parent->flags.testFlag(NephelometerSpancheckController::EcotechAuroraCalibrations) &&
            FP::defined(gasCMeasure) &&
            FP::defined(gasCReference) &&
            FP::defined(airCMeasure) &&
            FP::defined(airCReference) &&
            gasCReference != 0.0 &&
            airCReference != 0.0 &&
            gasDensity > 0.01 &&
            airDensity > 0.01 &&
            FP::defined(gasRayleighScattering) &&
            FP::defined(airRayleighScattering)) {
        double gasMR = gasCMeasure / gasCReference;
        double airMR = airCMeasure / airCReference;
        double gasX = gasRayleighScattering * gasDensity;
        double airX = airRayleighScattering * airDensity;
        double M = (gasMR - airMR) / (gasX - airX);
        double C = gasMR - M * gasX;
        result.hash("CalM").setDouble(M);
        result.hash("CalC").setDouble(C);
    }

    return result;
}

void NephelometerSpancheckController::WavelengthData::calculate(std::unordered_map<double,
                                                                                   Variant::Write> &outputAngles,
                                                                Variant::Write &outputCommon) const
{
    double totalAngle = FP::undefined();
    Variant::Read total = Variant::Read::empty();
    double backAngle = FP::undefined();
    Variant::Read back = Variant::Read::empty();

    outputCommon.hash("Wavelength").setDouble(wavelength);

    for (auto angle : angles) {
        double a = angle.first;
        auto result = calculate(a, angle.second);
        if (!FP::defined(totalAngle) || fabs(a) < fabs(totalAngle)) {
            total = result;
            totalAngle = a;
        }
        if (!FP::defined(backAngle) || fabs(a - 90.0) < fabs(backAngle - 90.0)) {
            back = result;
            backAngle = a;
        }
        outputAngles.emplace(a, std::move(result));
    }

    if (parent->flags.testFlag(NephelometerSpancheckController::TSI3563Calibrations) &&
            FP::defined(wavelength) &&
            wavelength > 0.0) {
        double gasT = total.hash("Gas").hash("T").toDouble();
        double gasP = total.hash("Gas").hash("P").toDouble();
        double airT = total.hash("Air").hash("T").toDouble();
        double airP = total.hash("Air").hash("P").toDouble();
        double totalGasCDark = total.hash("Gas").hash("Cd").toDouble();
        double totalGasCReference = total.hash("Gas").hash("Cf").toDouble();
        double totalGasCMeasure = total.hash("Gas").hash("C").toDouble();
        double totalAirCDark = total.hash("Air").hash("Cd").toDouble();
        double totalAirCReference = total.hash("Air").hash("Cf").toDouble();
        double totalAirCMeasure = total.hash("Air").hash("C").toDouble();
        double backGasCDark = back.hash("Gas").hash("Cd").toDouble();
        double backGasCMeasure = back.hash("Gas").hash("C").toDouble();
        double backAirCDark = back.hash("Air").hash("Cd").toDouble();
        double backAirCMeasure = back.hash("Air").hash("C").toDouble();

        if (FP::defined(gasT) &&
                FP::defined(gasP) &&
                FP::defined(airT) &&
                FP::defined(airP) &&
                FP::defined(totalGasCDark) &&
                FP::defined(totalGasCMeasure) &&
                FP::defined(totalGasCReference) &&
                FP::defined(totalAirCDark) &&
                FP::defined(totalAirCMeasure) &&
                FP::defined(totalAirCReference) &&
                totalGasCReference != totalGasCDark &&
                totalGasCMeasure != totalGasCDark &&
                totalAirCReference != totalAirCDark &&
                totalAirCMeasure != totalAirCDark) {
            double gasAmbientRayleigh = Rayleigh::angleScattering(wavelength, totalAngle,
                                                                  parent->config
                                                                        .first()
                                                                        .terminalAngle,
                                                                  parent->config.first().gas, gasT,
                                                                  gasP);
            double airAmbientRayleigh = Rayleigh::angleScattering(wavelength, totalAngle,
                                                                  parent->config
                                                                        .first()
                                                                        .terminalAngle,
                                                                  Rayleigh::Air, airT, airP);

            double k3span = FP::undefined();
            if (FP::defined(gasAmbientRayleigh) && FP::defined(airAmbientRayleigh)) {
                /* Must me in m-1 */
                k3span = (gasAmbientRayleigh - airAmbientRayleigh) * 1E-6;
            }

            if (FP::defined(k3span)) {
                double div = ((totalGasCMeasure - totalGasCDark) /
                        (totalGasCReference - totalGasCDark)) -
                        ((totalAirCMeasure - totalAirCDark) / (totalAirCReference - totalAirCDark));
                double k2 = FP::undefined();
                if (div != 0.0) {
                    k2 = k3span / div;
                    outputCommon.hash("K2").setDouble(k2);
                }

                if (FP::defined(k2) &&
                        k3span != 0.0 &&
                        FP::defined(backGasCMeasure) &&
                        FP::defined(backGasCDark) &&
                        FP::defined(backAirCMeasure) &&
                        FP::defined(backAirCDark)) {
                    double k4 = (k2 *
                            (((backGasCMeasure - backGasCDark) /
                                    (totalGasCReference - totalGasCDark)) -
                                    ((backAirCMeasure - backAirCDark) /
                                            (totalAirCReference - totalAirCDark)))) / k3span;
                    outputCommon.hash("K4").setDouble(k4);
                }
            }
        }
    }
}

void NephelometerSpancheckController::completed()
{
    state = Inactive;
    stateEndTime = FP::undefined();
    pendingCommands.clear();

    globalResults.write().setEmpty();
    std::vector<std::unordered_map<double, Variant::Write>> wavelengthAngleData;
    std::unordered_set<double> mergedAngles;
    double sumPercentError = 0.0;
    int countPercentError = 0;
    for (const auto &wl : data) {
        std::unordered_map<double, Variant::Write> outputAngles;
        Variant::Write outputWavelength = Variant::Write::empty();
        wl.calculate(outputAngles, outputWavelength);

        globalResults.write().hash(wl.getCode()).set(outputWavelength);
        for (const auto &angleData : outputAngles) {
            mergedAngles.insert(angleData.first);

            double pct = angleData.second.hash("PCT").toDouble();
            if (!FP::defined(pct))
                continue;
            sumPercentError += std::fabs(pct);
            countPercentError++;
        }

        wavelengthAngleData.emplace_back(std::move(outputAngles));
    }
    std::vector<double> sortedAngles(mergedAngles.begin(), mergedAngles.end());
    std::sort(sortedAngles.begin(), sortedAngles.end());

    if (countPercentError != 0) {
        globalResults.write().hash("PCT").setDouble(sumPercentError / (double) countPercentError);
    }

    for (auto angle : sortedAngles) {
        globalResults.write().hash("Angles").toArray().after_back().setReal(angle);
    }

    auto angleData = wavelengthAngleData.cbegin();
    for (auto wl = data.cbegin(), endWL = data.cend(); wl != endWL; ++wl, ++angleData) {
        for (auto angle : sortedAngles) {
            auto targetData = angleData->find(angle);
            if (targetData == angleData->end())
                continue;
            globalResults.write()
                         .hash(wl->getCode())
                         .hash("Results")
                         .toArray()
                         .after_back()
                         .set(targetData->second);
        }
    }

    reset();
    if (interface) {
        interface->setBypass(false);
        interface->issueZero();
        interface->completed();
    }
}

Variant::Read NephelometerSpancheckController::spancheckPhaseCommonMetadata() const
{
    Variant::Write result = Variant::Write::empty();

    result.metadataHashChild("Bs").metadataReal("Format").setString("0000.00");
    result.metadataHashChild("Bs").metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.metadataHashChild("Bs").metadataReal("Description")
          .setString("Measured light scattering coefficient");
    result.metadataHashChild("Bs").metadataReal("ReportT").setDouble(0.0);
    result.metadataHashChild("Bs").metadataReal("ReportP").setDouble(1013.25);

    result.metadataHashChild("Bsf").metadataReal("Format").setString("0000.00");
    result.metadataHashChild("Bsf").metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.metadataHashChild("Bsf").metadataReal("Description")
          .setString("Calculated Rayleigh light scattering coefficient");
    result.metadataHashChild("Bsf").metadataReal("ReportT").setDouble(0.0);
    result.metadataHashChild("Bsf").metadataReal("ReportP").setDouble(1013.25);

    result.metadataHashChild("T").metadataReal("Format").setString("00.0");
    result.metadataHashChild("T").metadataReal("Units").setString("\xC2\xB0\x43");
    result.metadataHashChild("T").metadataReal("Description").setString("Temperature");

    result.metadataHashChild("P").metadataReal("Format").setString("0000.0");
    result.metadataHashChild("P").metadataReal("Units").setString("hPa");
    result.metadataHashChild("P").metadataReal("Description").setString("Pressure");

    if (!flags.testFlag(NephelometerSpancheckController::NoCounts)) {
        result.metadataHashChild("Cd").metadataReal("Format").setString("0000000");
        result.metadataHashChild("Cd").metadataReal("Units").setString("Hz");
        result.metadataHashChild("Cd").metadataReal("Description").setString("Dark count rate");

        result.metadataHashChild("Cf").metadataReal("Format").setString("0000000");
        result.metadataHashChild("Cf").metadataReal("Units").setString("Hz");
        result.metadataHashChild("Cf").metadataReal("Description")
              .setString("Reference count rate");

        result.metadataHashChild("C").metadataReal("Format").setString("0000000");
        result.metadataHashChild("C").metadataReal("Units").setString("Hz");
        result.metadataHashChild("C").metadataReal("Description")
              .setString("Measurement count rate");
    }

    if (flags.testFlag(NephelometerSpancheckController::Chopper)) {
        result.metadataHashChild("Revs").metadataReal("Format").setString("00000.0");
        result.metadataHashChild("Revs").metadataReal("Description")
              .setString("Number of chopper revolutions");
    }

    return result;
}

Variant::Root NephelometerSpancheckController::metadata(ResultType type,
                                                        const Variant::Read &processing) const
{
    Variant::Root root;
    Variant::Write result = root.write();

    switch (type) {
    case FullResults:
        result.metadataHash("Description").setString("Full spancheck results");
        result.metadataHashChild("PCT").set(metadata(AveragePercentError));
        result.metadataHashChild("Angles").set(metadata(Angles));
        result.metadataHashChild("")
              .metadataHash("Description")
              .setString("Wavelength specific spancheck results");

        result.metadataHashChild("").metadataHashChild("Wavelength").metadataReal("Format")
              .setString("000");
        result.metadataHashChild("").metadataHashChild("Wavelength").metadataReal("Units")
              .setString("nm");
        result.metadataHashChild("").metadataHashChild("Wavelength").metadataReal("Description")
              .setString("Effective wavelength");

        if (flags.testFlag(NephelometerSpancheckController::TSI3563Calibrations)) {
            result.metadataHashChild("").metadataHashChild("K2").set(metadata(TSI3563K2));
            result.metadataHashChild("").metadataHashChild("K4").set(metadata(TSI3563K4));
        }

        result.metadataHashChild("").metadataHashChild("Results")
              .metadataArray("Description")
              .setString("Spancheck result data by measurement angle");

        result.metadataHashChild("").metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHash("Description")
              .setString("Spancheck result data at a given wavelength and angle");

        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("PCT")
              .metadataReal("Format")
              .setString("00.00");
        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("PCT")
              .metadataReal("Units")
              .setString("%");
        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("PCT")
              .metadataReal("Description")
              .setString("Light scattering coefficient percent error");

        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("Cc")
              .metadataReal("Format")
              .setString("00000.0");
        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("Cc")
              .metadataReal("Units")
              .setString("Hz");
        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("Cc")
              .metadataReal("Description")
              .setString("Sensitivity factor");

        if (flags.testFlag(NephelometerSpancheckController::EcotechAuroraCalibrations)) {
            result.metadataHashChild("")
                  .metadataHashChild("Results")
                  .metadataArray("Children")
                  .metadataHashChild("CalM").set(metadata(TotalEcotechAuroraM));
            result.metadataHashChild("")
                  .metadataHashChild("Results")
                  .metadataArray("Children")
                  .metadataHashChild("CalC").set(metadata(TotalEcotechAuroraC));
        }


        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("Gas")
              .set(spancheckPhaseCommonMetadata());
        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("Gas")
              .metadataHash("Description")
              .setString("Span gas sampling phase results");
        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("Gas")
              .metadataHashChild("Type")
              .metadataString("Description")
              .setString("Span gas used");
        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("Gas")
              .metadataHashChild("Bsc")
              .metadataReal("Format")
              .setString("0000.00");
        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("Gas")
              .metadataHashChild("Bsc").metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("Gas")
              .metadataHashChild("Bsc")
              .metadataReal("Description")
              .setString("Span gas measured Rayleigh scattering at STP");
        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("Gas")
              .metadataHashChild("Bsc")
              .metadataReal("ReportT")
              .setDouble(0.0);
        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("Gas")
              .metadataHashChild("Bsc")
              .metadataReal("ReportP")
              .setDouble(1013.25);

        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("Air")
              .set(spancheckPhaseCommonMetadata());
        result.metadataHashChild("")
              .metadataHashChild("Results")
              .metadataArray("Children")
              .metadataHashChild("Air")
              .metadataHash("Description")
              .setString("Filtered air sampling phase results");
        break;

    case AveragePercentError:
        result.metadataReal("Format").setString("000.00");
        result.metadataReal("Units").setString("%");
        result.metadataReal("Description")
              .setString("Average spancheck percent error across all angles and wavelengths");
        break;

    case Angles:
        result.metadataArray("Children").metadataReal("Format").setString("00");
        result.metadataArray("Units").setString("\xC2\xB0");
        result.metadataArray("Description").setString("Spancheck measurement start angle");
        break;

    case PercentError:
        result.metadataArray("Children").metadataReal("Format").setString("00");
        result.metadataArray("Units").setString("%");
        result.metadataArray("Description").setString("Spancheck percent error");
        break;

    case TotalPercentError:
        result.metadataReal("Format").setString("000.00");
        result.metadataReal("Units").setString("%");
        result.metadataReal("Description")
              .setString("Spancheck total light scattering coefficient percent error");
        break;
    case BackPercentError:
        result.metadataReal("Format").setString("000.00");
        result.metadataReal("Units").setString("%");
        result.metadataReal("Description")
              .setString(
                      "Spancheck backwards-hemispheric light scattering coefficient percent error");
        break;

    case SensitivityFactor:
        result.metadataArray("Children").metadataReal("Format").setString("00000.0");
        result.metadataArray("Units").setString("Hz");
        result.metadataArray("Description")
              .setString(
                      "Spancheck sensitivity factor, defined as the photon count rate attributable to Rayleigh scattering by air at STP");
        break;

    case TotalSensitivityFactor:
        result.metadataReal("Format").setString("00000.0");
        result.metadataReal("Units").setString("Hz");
        result.metadataReal("Description")
              .setString(
                      "Spancheck sensitivity factor, defined as the photon count rate attributable to Rayleigh scattering by air at STP");
        break;
    case BackSensitivityFactor:
        result.metadataReal("Format").setString("00000.0");
        result.metadataReal("Units").setString("Hz");
        result.metadataReal("Description")
              .setString(
                      "Spancheck backwards-hemispheric sensitivity factor, defined as the photon count rate attributable to Rayleigh backwards-hemispheric by air at STP");
        break;

    case TSI3563K2:
        result.metadataReal("Format").setString("0.000E0");
        result.metadataReal("Units").setString("m\xE2\x81\xBB¹");
        result.metadataReal("Description").setString("TSI 3563 K2 total calibration");
        break;
    case TSI3563K4:
        result.metadataReal("Format").setString("0.000");
        result.metadataReal("Description")
              .setString("TSI 3563 K4 backscattering Rayleigh fraction");
        break;

    case TotalEcotechAuroraM:
    case BackEcotechAuroraM:
        result.metadataReal("Format").setString("0.000E0");
        result.metadataReal("Units").setString("Mm");
        result.metadataReal("Description").setString("Ecotech Aurora calibration slope");
        break;
    case TotalEcotechAuroraC:
    case BackEcotechAuroraC:
        result.metadataReal("Format").setString("0.000E0");
        result.metadataReal("Description").setString("Ecotech Aurora calibration intercept");
        break;

    case EcotechAuroraMPolar:
        result.metadataArray("Children").metadataReal("Format").setString("0.000E0");
        result.metadataArray("Units").setString("Mm");
        result.metadataArray("Description").setString("Ecotech Aurora calibration slope");
        break;
    case EcotechAuroraCPolar:
        result.metadataArray("Children").metadataReal("Format").setString("0.000E0");
        result.metadataArray("Description").setString("Ecotech Aurora calibration intercept");
        break;

    case RealtimeState:
        result.metadataHash("Realtime").hash("Persistent").setBool(true);
        result.metadataHash("Realtime").hash("Hide").setBool(true);
        result.metadataHashChild("Current")
              .metadataString("Description")
              .setString("The current spancheck state");
        result.metadataHashChild("Gas")
              .metadataString("Description")
              .setString("The span gas currently being used");
        result.metadataHashChild("StartTime").metadataReal("Description")
              .setString("The start time of the current state");
        result.metadataHashChild("EndTime").metadataReal("Description")
              .setString("The end time of the current state");
        break;
    }

    if (processing.exists())
        result.metadata("Processing").toArray().after_back().set(processing);
    result.metadata("Smoothing").hash("Mode").setString("None");
    return root;
}

static Variant::Root selectResultAngleValue(const Variant::Read &global,
                                            const QString &code,
                                            const QString &path,
                                            double targetAngle)
{
    double bestAngle = FP::undefined();
    Variant::Root output;
    auto angles = global.hash("Angles").toArray();
    for (std::size_t i = 0, max = angles.size(); i < max; i++) {
        double ang = global.hash("Angles").array(i).toDouble();
        if (!FP::defined(ang))
            continue;
        ang -= targetAngle;

        auto check = global.hash(code).hash("Results").array(i).getPath(path);
        if (!check.exists())
            continue;
        if (std::fabs(ang) > 10.0)
            continue;
        if (!FP::defined(bestAngle) || std::fabs(ang) < std::fabs(bestAngle)) {
            bestAngle = ang;
            output.write().set(check);
        }
    }
    return output;
}

Variant::Root NephelometerSpancheckController::results(const Variant::Read &global,
                                                       ResultType type,
                                                       const QString &code)
{
    switch (type) {
    case FullResults:
        return Variant::Root(global);

    case AveragePercentError: {
        Variant::Root output;
        output.write().set(global.hash("PCT"));
        return output;
    }
    case Angles: {
        Variant::Root output;
        output.write().set(global.hash("Angles"));
        return output;
    }

    case PercentError: {
        Variant::Root output;
        auto target = output.write().toArray();
        for (auto add : global.hash(code).hash("Results").toArray()) {
            target.after_back().set(add.hash("PCT"));
        }
        return output;
    }
    case TotalPercentError:
        return selectResultAngleValue(global, code, "PCT", 0.0);
    case BackPercentError:
        return selectResultAngleValue(global, code, "PCT", 90.0);

    case SensitivityFactor: {
        Variant::Root output;
        auto target = output.write().toArray();
        for (auto add : global.hash(code).hash("Results").toArray()) {
            target.after_back().set(add.hash("Cc"));
        }
        return output;
    }

    case TotalSensitivityFactor:
        return selectResultAngleValue(global, code, "Cc", 0.0);
    case BackSensitivityFactor:
        return selectResultAngleValue(global, code, "Cc", 90.0);

    case TSI3563K2: {
        Variant::Root output;
        output.write().set(global.hash(code).hash("K2"));
        return output;
    }
    case TSI3563K4: {
        Variant::Root output;
        output.write().set(global.hash(code).hash("K4"));
        return output;
    }

    case TotalEcotechAuroraM:
        return Variant::Root(global.hash(code).hash("Results").toArray().front().hash("CalM"));
    case BackEcotechAuroraM:
        return Variant::Root(global.hash(code).hash("Results").toArray().back().hash("CalM"));

    case TotalEcotechAuroraC:
        return Variant::Root(global.hash(code).hash("Results").toArray().front().hash("CalC"));
    case BackEcotechAuroraC:
        return Variant::Root(global.hash(code).hash("Results").toArray().back().hash("CalC"));

    case EcotechAuroraMPolar: {
        Variant::Root output;
        auto target = output.write().toArray();
        for (auto add : global.hash(code).hash("Results").toArray()) {
            target.after_back().set(add.hash("CalM"));
        }
        return output;
    }
    case EcotechAuroraCPolar: {
        Variant::Root output;
        auto target = output.write().toArray();
        for (auto add : global.hash(code).hash("Results").toArray()) {
            target.after_back().set(add.hash("CalC"));
        }
        return output;
    }

    case RealtimeState: {
        Variant::Root output;
        output.write().hash("Current").setString("Inactive");
        output.write().hash("StartTime").setDouble(FP::undefined());
        output.write().hash("EndTime").setDouble(FP::undefined());
        return output;
    }

    }

    Q_ASSERT(false);
    return Variant::Root();
}

Data::Variant::Root NephelometerSpancheckController::results(ResultType type,
                                                             const QString &code) const
{ return results(globalResults, type, code); }

bool NephelometerSpancheckController::SampleData::stable(bool requireRevolutions,
                                                         bool requireCounts) const
{
    if (requireRevolutions && !revolutions->stable())
        return false;
    if (requireCounts &&
            (!measurementCounts->stable() || !referenceCounts->stable() || !darkCounts->stable()))
        return false;
    return scattering->stable() && temperature->stable() && pressure->stable();
}

bool NephelometerSpancheckController::AngleData::stable(bool isGas,
                                                        bool requireRevolutions,
                                                        bool requireCounts) const
{
    if (isGas)
        return gas.stable(requireRevolutions, requireCounts);
    else
        return air.stable(requireRevolutions, requireCounts);
}

bool NephelometerSpancheckController::WavelengthData::stable(bool isGas,
                                                             bool requireRevolutions,
                                                             bool requireCounts) const
{
    if (angles.empty())
        return false;
    for (const auto &check : angles) {
        if (!check.second.stable(isGas, requireRevolutions, requireCounts))
            return false;
    }
    return true;
}

bool NephelometerSpancheckController::dataStable(bool isGas) const
{
    bool usRevolutionData = flags.testFlag(Chopper);
    bool useCountData = !flags.testFlag(NoCounts) && !flags.testFlag(PartialCounts);
    for (const auto &check: data) {
        if (!check.stable(isGas, usRevolutionData, useCountData))
            return false;
    }
    return true;
}

Data::Variant::Root NephelometerSpancheckController::buildRealtimeState() const
{
    Data::Variant::Root stateData;
    switch (state) {
    case Inactive:
        stateData.write().hash("Current").setString("Inactive");
        break;
    case Initializing:
        stateData.write().hash("Current").setString("Initializing");
        break;
    case Aborting:
        stateData.write().hash("Current").setString("Aborting");
        break;
    case GasAirFlush:
        stateData.write().hash("Current").setString("GasAirFlush");
        stateData.write().hash("Gas").setString(Rayleigh::gasToString(config.first().gas));
        break;
    case GasFlush:
        stateData.write().hash("Current").setString("GasFlush");
        stateData.write().hash("Gas").setString(Rayleigh::gasToString(config.first().gas));
        break;
    case GasSample:
        stateData.write().hash("Current").setString("GasSample");
        stateData.write().hash("Gas").setString(Rayleigh::gasToString(config.first().gas));
        break;
    case AirFlush:
        stateData.write().hash("Current").setString("AirFlush");
        stateData.write().hash("Gas").setString(Rayleigh::gasToString(config.first().gas));
        break;
    case AirSample:
        stateData.write().hash("Current").setString("AirSample");
        stateData.write().hash("Gas").setString(Rayleigh::gasToString(config.first().gas));
        break;
    }

    stateData.write().hash("StartTime").setDouble(stateBeginTime);
    stateData.write().hash("EndTime").setDouble(stateEndTime);

    return stateData;
}

void NephelometerSpancheckController::advance(double currentTime, StreamSink *realtimeEgress)
{
    Q_ASSERT(!config.isEmpty());
    if (!Range::intersectShift(config, currentTime)) {
        qCWarning(log_acquisition_spancheck) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    this->currentTime = currentTime;

    State originalState = state;
    State previousState;
    do {
        previousState = state;
        switch (state) {
        case Inactive:
            stateEndTime = FP::undefined();
            stateBeginTime = currentTime;
            pendingCommands.clear();
            break;
        case Aborting:
            if (interface != NULL)
                interface->aborted();
            mergePendingCommands(config.first().commandsEnd);
            issueCommands(pendingCommands);
            pendingCommands.clear();
            state = Inactive;
            stateBeginTime = currentTime;
            if (interface != NULL) {
                interface->setBypass(false);
                interface->aborted();
            }
            break;

        case Initializing:
            if (interface != NULL) {
                if (!interface->start()) {
                    pendingCommands.clear();
                    state = Inactive;
                    stateBeginTime = currentTime;
                    qCDebug(log_acquisition_spancheck) << "Spancheck initialization aborted";
                    break;
                }
                interface->setBypass(false);
                interface->switchToFilteredAir();
            }

            reset();

            stateBeginTime = currentTime;
            pendingCommands.clear();
            for (const auto &add : config.first().commandsStart) {
                pendingCommands.emplace(add.first, add.second);
            }
            mergePendingCommands(config.first().commandsActivateAir);
            issueCommands(pendingCommands);
            pendingCommands.clear();
            for (const auto &add : config.first().commandsDeactivateAir) {
                pendingCommands.emplace(add.first, add.second);
            }

            state = GasAirFlush;
            stateEndTime = airFlush->apply(currentTime, currentTime, true);

            qCDebug(log_acquisition_spancheck).noquote() << "Initiating air flush before"
                                                         << Rayleigh::gasToString(
                                                                 config.first().gas) << "flush at"
                                                         << Logging::time(currentTime)
                                                         << "ending at"
                                                         << Logging::time(stateEndTime);
            break;

        case GasAirFlush:
            if (FP::defined(stateEndTime) && stateEndTime >= currentTime)
                break;

            state = GasFlush;
            stateBeginTime = currentTime;
            stateEndTime = gasFlush->apply(currentTime, currentTime, true);

            qCDebug(log_acquisition_spancheck).noquote() << "Starting" << Rayleigh::gasToString(
                        config.first().gas) << "flush at" << Logging::time(currentTime)
                                                         << "ending at"
                                                         << Logging::time(stateEndTime);

            mergePendingCommands(config.first().commandsActivateGas);
            issueCommands(pendingCommands);
            pendingCommands.clear();
            for (const auto &add : config.first().commandsDeactivateGas) {
                pendingCommands.emplace(add.first, add.second);
            }

            if (interface != NULL) {
                interface->switchToGas(config.first().gas);
                interface->setBypass(true);
            }
            break;

        case GasFlush:
            if (FP::defined(stateEndTime) && stateEndTime >= currentTime)
                break;

            state = GasSample;
            stateBeginTime = currentTime;
            stateEndTime = gasMinimumSample->apply(currentTime, currentTime, true);

            dataAllAngles.clear();
            dataAllWavelengths.clear();
            dataAll.reset(false);
            for (auto &r : data) {
                r.reset(false);
            }

            qCDebug(log_acquisition_spancheck).noquote() << "Starting" << Rayleigh::gasToString(
                        config.first().gas) << "sampling at" << Logging::time(currentTime)
                                                         << "ending after"
                                                         << Logging::time(stateEndTime);
            break;

        case GasSample:
            if (FP::defined(stateEndTime) && stateEndTime >= currentTime)
                break;
            if (!dataStable(true)) {
                double check = gasMaximumSample->apply(currentTime, stateBeginTime, true);
                if (!FP::defined(check) || currentTime < check)
                    break;
            }


            stateBeginTime = currentTime;
            state = AirFlush;
            stateEndTime = airFlush->apply(currentTime, currentTime, true);

            qCDebug(log_acquisition_spancheck) << "Beginning air flush before sampling at"
                                               << Logging::time(currentTime) << "ending at"
                                               << Logging::time(stateEndTime);

            mergePendingCommands(config.first().commandsActivateAir);
            issueCommands(pendingCommands);
            pendingCommands.clear();
            for (const auto &add : config.first().commandsDeactivateAir) {
                pendingCommands.emplace(add.first, add.second);
            }

            if (interface != NULL) {
                interface->setBypass(false);
                interface->switchToFilteredAir();
            }
            break;

        case AirFlush:
            if (FP::defined(stateEndTime) && stateEndTime >= currentTime)
                break;

            state = AirSample;
            stateBeginTime = currentTime;
            stateEndTime = airMinimumSample->apply(currentTime, currentTime, true);

            qCDebug(log_acquisition_spancheck) << "Starting air sampling at"
                                               << Logging::time(currentTime) << "ending after"
                                               << Logging::time(stateEndTime);
            break;

        case AirSample:
            if (FP::defined(stateEndTime) && stateEndTime >= currentTime)
                break;
            if (!dataStable(false)) {
                double check = airMaximumSample->apply(currentTime, stateBeginTime, true);
                if (!FP::defined(check) || currentTime < check)
                    break;
            }

            stateBeginTime = currentTime;

            mergePendingCommands(config.first().commandsEnd);
            issueCommands(pendingCommands);

            qCDebug(log_acquisition_spancheck) << "Air sampling completed at"
                                               << Logging::time(stateEndTime);

            completed();
            break;
        }
    } while (previousState != state);

    if (originalState == state)
        return;
    stateBeginTime = currentTime;

    if (realtimeEgress == NULL)
        return;

    realtimeEgress->incomingData(
            SequenceValue({{}, "raw", "ZSPANCHECKSTATE"}, buildRealtimeState(), currentTime,
                          FP::undefined()));
}

void NephelometerSpancheckController::SampleData::addValue(NephelometerSpancheckController::Parameter param,
                                                           double value,
                                                           double time)
{
    if (FP::defined(value))
        seenParameters |= param;

    switch (param) {
    case NephelometerSpancheckController::Scattering:
        scattering->add(value, time);
        break;
    case NephelometerSpancheckController::MeasurementCounts:
        measurementCounts->add(value, time);
        break;
    case NephelometerSpancheckController::ReferenceCounts:
        referenceCounts->add(value, time);
        break;
    case NephelometerSpancheckController::DarkCounts:
        darkCounts->add(value, time);
        break;
    case NephelometerSpancheckController::Temperature:
        temperature->add(value, time);
        break;
    case NephelometerSpancheckController::Pressure:
        pressure->add(value, time);
        break;
    case NephelometerSpancheckController::Revolutions:
        revolutions->add(value, time);
        break;
    }
}

NephelometerSpancheckController::AngleData::AngleData(const SampleData &dataAll) : gas(dataAll),
                                                                                   air(dataAll)
{ }

void NephelometerSpancheckController::AngleData::overlay(const AngleData &other)
{
    gas.overlay(other.gas);
    air.overlay(other.air);
}

void NephelometerSpancheckController::AngleData::addValue(NephelometerSpancheckController::Parameter param,
                                                          double value,
                                                          double time,
                                                          bool toGas)
{
    if (toGas)
        gas.addValue(param, value, time);
    else
        air.addValue(param, value, time);
}

void NephelometerSpancheckController::WavelengthData::addValue(NephelometerSpancheckController::Parameter param,
                                                               double value,
                                                               double time,
                                                               bool toFirst,
                                                               double angle)
{
    if (!FP::defined(angle)) {
        for (auto &add : angles) {
            add.second.addValue(param, value, time, toFirst);
        }
        return;
    }

    auto target = angles.find(angle);
    if (target == angles.end()) {
        target = angles.emplace(angle, parent->dataAll).first;
        auto check = parent->dataAllWavelengths.find(angle);
        if (check != parent->dataAllWavelengths.end())
            target->second.overlay(check->second);
        if (wavelengthIndex < parent->dataAllAngles.size())
            target->second.overlay(parent->dataAllAngles[wavelengthIndex]);
    }

    target->second.addValue(param, value, time, toFirst);
}

void NephelometerSpancheckController::update(Parameter param,
                                             double value,
                                             int wavelength,
                                             double angle)
{
    switch (state) {
    case Inactive:
    case Aborting:
    case Initializing:
    case GasAirFlush:
    case GasFlush:
    case AirFlush:
        return;
    case GasSample:
    case AirSample:
        break;
    }

    if (wavelength < 0) {
        for (auto &add : dataAllWavelengths) {
            add.second.addValue(param, value, currentTime, state == GasSample);
        }

        for (auto &wlData : data) {
            wlData.addValue(param, value, currentTime, state == GasSample, angle);
        }

        if (!FP::defined(angle)) {
            dataAll.addValue(param, value, currentTime, state == GasSample);

            for (auto &add : dataAllAngles) {
                add.addValue(param, value, currentTime, state == GasSample);
            }

            return;
        }
        return;
    }
    Q_ASSERT(static_cast<std::size_t>(wavelength) < data.size());

    if (!FP::defined(angle)) {
        while (static_cast<std::size_t>(wavelength) >= dataAllAngles.size()) {
            dataAllAngles.emplace_back(dataAll);
        }

        dataAllAngles[wavelength].addValue(param, value, currentTime, state == GasSample);
    }

    data[wavelength].addValue(param, value, currentTime, state == GasSample, angle);
}

}
}
