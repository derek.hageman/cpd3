/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGTRIGGERMERGE_H
#define CPD3EDITINGTRIGGERMERGE_H

#include "core/first.hxx"

#include <string.h>
#include <vector>
#include <deque>
#include <algorithm>
#include <QtGlobal>
#include <QList>
#include <QVector>
#include <QDebug>
#include <QDebugStateSaver>

#include "editing/editing.hxx"

#include "core/timeutils.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/util.hxx"
#include "datacore/variant/root.hxx"
#include "editing/triggerinput.hxx"

namespace CPD3 {
namespace Editing {

namespace Internal {

struct InputMergeOverlay {
    double start;
    double end;
    double value;
    int index;

    inline double getStart() const
    { return start; }

    inline double getEnd() const
    { return end; }

    inline InputMergeOverlay(double s, double e, double v, int i) : start(s),
                                                                    end(e),
                                                                    value(v),
                                                                    index(i)
    { }
};

struct InputMergedDynamic {
    double start;
    double end;
    std::vector<double> values;
    quint32 seen;
    std::vector<bool> seenExtended;

    inline double getStart() const
    { return start; }

    inline double getEnd() const
    { return end; }

    inline void setStart(double s)
    { start = s; }

    inline void setEnd(double e)
    { end = e; }

    inline InputMergedDynamic() : start(FP::undefined()),
                                  end(FP::undefined()),
                                  values(),
                                  seen(0),
                                  seenExtended()
    { }

    inline InputMergedDynamic(const InputMergedDynamic &other) : start(other.start),
                                                                 end(other.end),
                                                                 values(other.values),
                                                                 seen(other.seen),
                                                                 seenExtended(other.seenExtended)
    { }

    inline InputMergedDynamic &operator=(const InputMergedDynamic &other)
    {
        if (&other == this) return *this;
        start = other.start;
        end = other.end;
        values = other.values;
        seen = other.seen;
        seenExtended = other.seenExtended;
        return *this;
    }

    inline InputMergedDynamic(const InputMergedDynamic &other, double s, double e) : start(s),
                                                                                     end(e),
                                                                                     values(other.values),
                                                                                     seen(other.seen),
                                                                                     seenExtended(
                                                                                             other.seenExtended)
    { }

    inline InputMergedDynamic(const InputMergeOverlay &other, double s, double e) : start(s),
                                                                                    end(e),
                                                                                    values(other.index +
                                                                                                   1,
                                                                                           FP::undefined()),
                                                                                    seen(0),
                                                                                    seenExtended()
    {
        values[other.index] = other.value;

        if (other.index < 31) {
            seen |= 1 << other.index;
        } else {
            seenExtended.resize(other.index - 31 + 1, false);
            seenExtended[other.index - 31] = true;
        }
    }

    /* A bit of weirdness here in that we modify the under type to remove
     * the seen flag so we don't continue emission until we get another
     * update or advance. */
    inline InputMergedDynamic(const InputMergedDynamic &under,
                              const InputMergeOverlay &over,
                              double s,
                              double e) : start(s),
                                          end(e),
                                          values(under.values),
                                          seen(under.seen),
                                          seenExtended(under.seenExtended)
    {
        if (over.index >= (int) values.size())
            values.resize(over.index + 1, FP::undefined());
        values[over.index] = over.value;

        if (over.index < 31) {
            seen |= 1 << over.index;
        } else {
            if (over.index - 31 >= (int) seenExtended.size())
                seenExtended.resize(over.index - 31 + 1, false);
            seenExtended[over.index - 31] = true;
        }
    }

    inline bool allSeen(int maximum) const
    {
        if (maximum <= 31)
            return seen == (quint32) ((1 << (quint32) maximum) - 1);
        if (seen != 0x7FFFFFFF)
            return false;
        if (maximum - 31 > (int) seenExtended.size())
            return false;
        for (std::vector<bool>::const_iterator c = seenExtended.begin(), endC = seenExtended.end();
                c != endC;
                ++c) {
            if (!(*c))
                return false;
        }
        return true;
    }

    inline bool specificSeen(int index) const
    {
        if (index <= 31)
            return (seen & (1 << index)) != 0;
        index -= 31;
        if (index >= (int) seenExtended.size())
            return false;
        return seenExtended[index];
    }

    inline void unsee(int index)
    {
        if (index <= 31) {
            seen &= ~(1 << index);
            return;
        }
        index -= 31;
        if (index >= (int) seenExtended.size())
            return;
        seenExtended[index] = false;
    }

    inline void seeAll(int maximum)
    {
        if (maximum <= 31) {
            seen = (quint32) ((1 << (quint32) maximum) - 1);
            return;
        }
        seen = 0x7FFFFFFF;
        std::fill(seenExtended.begin(), seenExtended.end(), true);
    }
};

template<int N>
struct InputMergedFixed {
    double start;
    double end;
    quint32 seen;
    double values[N];

    inline double getStart() const
    { return start; }

    inline double getEnd() const
    { return end; }

    inline void setStart(double s)
    { start = s; }

    inline void setEnd(double e)
    { end = e; }

    inline InputMergedFixed() : start(FP::undefined()), end(FP::undefined()), seen(0)
    {
        Q_ASSERT(N <= 31);
        for (int i = 0; i < N; i++) {
            values[i] = FP::undefined();
        }
    }

    inline InputMergedFixed(const InputMergedFixed<N> &other) : start(other.start),
                                                                end(other.end),
                                                                seen(other.seen)
    {
        memcpy(values, other.values, N * sizeof(double));
    }

    inline InputMergedFixed<N> &operator=(const InputMergedFixed<N> &other)
    {
        if (&other == this) return *this;
        start = other.start;
        end = other.end;
        seen = other.seen;
        memcpy(values, other.values, N * sizeof(double));
        return *this;
    }

    inline InputMergedFixed(const InputMergedFixed<N> &other, double s, double e) : start(s),
                                                                                    end(e),
                                                                                    seen(other.seen)
    {
        memcpy(values, other.values, N * sizeof(double));
    }

    inline InputMergedFixed(const InputMergeOverlay &other, double s, double e) : start(s),
                                                                                  end(e),
                                                                                  seen(1
                                                                                               << other.index)
    {
        Q_ASSERT(N <= 31);
        Q_ASSERT(other.index < N);
        for (int i = 0; i < N; i++) {
            values[i] = FP::undefined();
        }
        values[other.index] = other.value;
    }

    /* A bit of weirdness here in that we modify the under type to remove
     * the seen flag so we don't continue emission until we get another
     * update or advance. */
    inline InputMergedFixed(const InputMergedFixed<N> &under,
                            const InputMergeOverlay &over,
                            double s,
                            double e) : start(s), end(e), seen(under.seen | (1 << over.index))
    {
        memcpy(values, under.values, N * sizeof(double));
        values[over.index] = over.value;
    }

    inline bool allSeen(int maximum) const
    {
        Q_UNUSED(maximum);
        return seen == (quint32) ((1 << (quint32) N) - 1);
    }

    inline bool specificSeen(int index) const
    {
        return seen & (1 << index);
    }

    inline void unsee(int index)
    {
        seen &= ~(1 << index);
    }

    inline void seeAll(int maximum)
    {
        Q_UNUSED(maximum);
        seen = (quint32) ((1 << (quint32) N) - 1);
    }
};

}

CPD3EDITING_EXPORT QDebug operator<<(QDebug stream, const Internal::InputMergedDynamic &v);

template<int N>
QDebug operator<<(QDebug stream, const Internal::InputMergedFixed<N> &v)
{
    QDebugStateSaver saver(stream);
    stream << "InputMergedFixed<" << N << ">(" << Logging::range(v.start, v.end) << ',' << hex
           << v.seen;
    for (int i = 0; i < N; i++) {
        stream << ',' << v.d[i];
    }
    stream << ')';
    return stream;
}

}
}

Q_DECLARE_TYPEINFO(CPD3::Editing::Internal::InputMergedDynamic, Q_MOVABLE_TYPE);

/* Movable type, declared like this since we can't do an unspecialized
 * template. */
template<int N>
class QTypeInfo<CPD3::Editing::Internal::InputMergedFixed<N> > {
public:
    enum {
        isComplex = true, isStatic = false, isLarge = true, isPointer = false, isDummy = false,
    };

    static inline const char *name()
    { return "CPD3::Editing::Internal::InputMergedFixed<N>"; }
};

namespace CPD3 {
namespace Editing {

/**
 * A class that handles merging streams of double values into output segments.
 */
template<class Merged, class BaseClass>
class TriggerInputMerger : public BaseClass {
    std::vector<TriggerInput *> inputs;

    std::deque<Merged> buffer;
    std::size_t nextOutputIndex;
    bool completed;
    double checkReadyTime;

    bool inputsReady(double end) const
    {
        for (std::vector<TriggerInput *>::const_iterator in = inputs.begin(), endIn = inputs.end();
                in != endIn;
                ++in) {
            if (!(*in)->isReady(end))
                return false;
        }
        return true;
    }

    void removeAllCompleted()
    {
        auto begin = buffer.begin();
        auto add = begin;
        for (auto end = add + nextOutputIndex; add != end; ++add) {
            if (!inputsReady(add->getEnd()))
                break;
        }
        if (add != begin) {
            nextOutputIndex -= (add - begin);
            buffer.erase(begin, add);
        }
    }

    void addIncoming(const QVector<TriggerInput::Result> &incoming, int index)
    {
        for (const auto &add : incoming) {
            Q_ASSERT(static_cast<std::size_t>(Range::lowerBound(buffer.cbegin(), buffer.cend(),
                                                                add.getStart()) -
                    buffer.cbegin()) >= nextOutputIndex);

            Range::overlayFragmenting(buffer,
                                      Internal::InputMergeOverlay(add.getStart(), add.getEnd(),
                                                                  add.getValue(), index));

            for (auto end = buffer.end(),
                    target = Range::upperBound(buffer.begin(), end, add.getStart());
                    target != end;
                    ++target) {
                if (!target->specificSeen(index))
                    break;
                target->unsee(index);
            }
        }
    }

    void addAllIncoming()
    {
        int index = 0;
        for (std::vector<TriggerInput *>::iterator in = inputs.begin(), endIn = inputs.end();
                in != endIn;
                ++in, ++index) {
            addIncoming((*in)->process(), index);
        }
    }

    void fragmentBefore(double time)
    {
        if (nextOutputIndex >= buffer.size())
            return;
        Q_ASSERT(FP::defined(time));

        /* Fragment the current segment so the older parts can be purged. */
        auto check = buffer.begin() + nextOutputIndex;
        /* Arbitrary threshold to keep from fragmenting unnecessarily.  This should
         * really be based on what we're holding up, but there's no way to determine
         * that. */
        if (Range::compareStart(time - 3600.0, check->getStart()) <= 0)
            return;
        if (Range::compareStartEnd(time, check->getEnd()) >= 0)
            return;

        check = buffer.emplace(check, *check);
        check->setEnd(time);
        Q_ASSERT(Range::compareStartEnd(check->getStart(), check->getEnd()) < 0);
        (check + 1)->setStart(time);
        Q_ASSERT(Range::compareStartEnd((check + 1)->getStart(), (check + 1)->getEnd()) < 0);
    }

public:
    /**
     * Create a new merger.  This takes ownership of the inputs
     * 
     * @param in        the inputs to use
     */
    TriggerInputMerger(const std::vector<TriggerInput *> &in) : inputs(in),
                                                                buffer(),
                                                                nextOutputIndex(0),
                                                                completed(false),
                                                                checkReadyTime(FP::undefined())
    { }

    /**
     * Create a new merger.  This takes ownership of the inputs
     * 
     * @param in        the inputs to use
     */
    TriggerInputMerger(const QVector<TriggerInput *> &in) : inputs(in.toStdVector()),
                                                            buffer(),
                                                            nextOutputIndex(0),
                                                            completed(false),
                                                            checkReadyTime(FP::undefined())
    { }

    /**
     * Create a new merger.  This takes ownership of the inputs
     * 
     * @param in        the inputs to use
     */
    TriggerInputMerger(const QList<TriggerInput *> &in)
            : inputs(),
              buffer(),
              nextOutputIndex(0),
              completed(false),
              checkReadyTime(FP::undefined())
    {
        inputs.reserve(in.size());
        for (QList<TriggerInput *>::const_iterator add = in.constBegin(), endAdd = in.constEnd();
                add != endAdd;
                ++add) {
            inputs.push_back(*add);
        }
    }

    virtual ~TriggerInputMerger()
    {
        qDeleteAll(inputs);
    }

    virtual void finalize()
    {
        for (std::vector<TriggerInput *>::iterator in = inputs.begin(), endIn = inputs.end();
                in != endIn;
                ++in) {
            (*in)->finalize();
        }
        completed = true;
    }

    virtual void extendProcessing(double &start, double &end) const
    {
        double originalStart = start;
        double originalEnd = end;
        for (std::vector<TriggerInput *>::const_iterator in = inputs.begin(), endIn = inputs.end();
                in != endIn;
                ++in) {
            double inputStart = originalStart;
            double inputEnd = originalEnd;
            (*in)->extendProcessing(inputStart, inputEnd);
            if (Range::compareStart(inputStart, start) < 0)
                start = inputStart;
            if (Range::compareEnd(inputEnd, end) > 0)
                end = inputEnd;
        }
    }

    virtual QList<EditDataTarget *> unhandledInput(const Data::SequenceName &unit)
    {
        QList<EditDataTarget *> result;
        for (std::vector<TriggerInput *>::iterator in = inputs.begin(), endIn = inputs.end();
                in != endIn;
                ++in) {
            QList<EditDataTarget *> add((*in)->unhandledInput(unit));
            if (add.isEmpty())
                continue;
            result.append(add);
        }
        return result;
    }

    virtual void incomingDataAdvance(double time)
    {
        for (std::vector<TriggerInput *>::iterator in = inputs.begin(), endIn = inputs.end();
                in != endIn;
                ++in) {
            (*in)->incomingDataAdvance(time);
        }
        addAllIncoming();

        if (!FP::defined(checkReadyTime)) {
            if (inputsReady(time))
                fragmentBefore(time);
            else
                checkReadyTime = time;
        } else {
            if (inputsReady(checkReadyTime)) {
                fragmentBefore(checkReadyTime);
                checkReadyTime = FP::undefined();
            }
        }

        removeAllCompleted();
    }

    virtual bool isReady(double end) const
    {
        if (nextOutputIndex < buffer.size()) {
            if (Range::compareStartEnd(buffer[nextOutputIndex].getStart(), end) < 0)
                return false;
        }
        return inputsReady(end);
    }

    virtual double advancedTime() const
    {
        double time = FP::undefined();
        for (std::vector<TriggerInput *>::const_iterator in = inputs.begin(), endIn = inputs.end();
                in != endIn;
                ++in) {
            double adv = (*in)->advancedTime();
            if (!FP::defined(adv)) {
                if (!(*in)->isReady(FP::undefined()))
                    return FP::undefined();
                continue;
            }
            if (!FP::defined(time) || time > adv)
                time = adv;
        }

        if (nextOutputIndex < buffer.size()) {
            double adv = buffer.at(nextOutputIndex).getStart();
            if (!FP::defined(adv))
                return adv;
            if (!FP::defined(time))
                return adv;
            if (adv < time)
                time = adv;
        }
        return time;
    }

    virtual Data::SequenceName::Set requestedInputs() const
    {
        Data::SequenceName::Set result;
        for (const auto &in : inputs) {
            Util::merge(in->requestedInputs(), result);
        }
        return result;
    }

protected:
    TriggerInputMerger(const TriggerInputMerger<Merged, BaseClass> &other) : BaseClass(other),
                                                                             inputs(),
                                                                             buffer(),
                                                                             nextOutputIndex(0),
                                                                             completed(false),
                                                                             checkReadyTime(
                                                                                     FP::undefined())
    {
        inputs.reserve(other.inputs.size());
        for (std::vector<TriggerInput *>::const_iterator in = other.inputs.begin(),
                endIn = other.inputs.end(); in != endIn; ++in) {
            inputs.push_back((*in)->clone());
        }
    }

    /**
     * Take all ready segments that the merger has completed.
     * 
     * @return  the pending completed segments
     */
    QVector<Merged> takeAllReady()
    {
        addAllIncoming();
        removeAllCompleted();

        Q_ASSERT(nextOutputIndex >= 0 && nextOutputIndex <= buffer.size());

        QVector<Merged> result;
        if (completed) {
            if (nextOutputIndex < buffer.size()) {
                std::move(buffer.begin() + nextOutputIndex, buffer.end(),
                          Util::back_emplacer(result));
            }
            buffer.clear();
            nextOutputIndex = 0;
            return result;
        }

        for (auto add = buffer.cbegin() + nextOutputIndex, endAdd = buffer.cend();
                add != endAdd;
                ++add, ++nextOutputIndex) {
            if (!add->allSeen(inputs.size()) && !inputsReady(add->getEnd()))
                break;

            result.append(*add);
        }
        return result;
    }
};


/**
 * A base class for trigger input functions that take an a list of arbitrary
 * parameters.
 */
class CPD3EDITING_EXPORT TriggerInputMultipleFunction : public TriggerInputMerger<
        Internal::InputMergedDynamic, TriggerInput> {
    size_t expectedSize;
public:
    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs    the inputs to use
     */
    TriggerInputMultipleFunction(const std::vector<TriggerInput *> &inputs);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs    the inputs to use
     */
    TriggerInputMultipleFunction(const QVector<TriggerInput *> &inputs);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs    the inputs to use
     */
    TriggerInputMultipleFunction(const QList<TriggerInput *> &inputs);

    virtual ~TriggerInputMultipleFunction();

    virtual QVector<Result> process();

protected:
    TriggerInputMultipleFunction(const TriggerInputMultipleFunction &other);

    /**
     * Apply the function to the given inputs.
     * 
     * @param inputs    the input values
     * @param full      true if all inputs are defined
     * @return          the output value
     */
    virtual double apply(const std::vector<double> &inputs, bool full) = 0;
};

/**
 * A trigger input that sums together other inputs.
 */
class CPD3EDITING_EXPORT TriggerInputSum : public TriggerInputMultipleFunction {
    bool requireAll;
public:
    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputSum(const std::vector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputSum(const QVector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputSum(const QList<TriggerInput *> &inputs, bool requireAll = true);

    virtual ~TriggerInputSum();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputSum(const TriggerInputSum &other);

    virtual double apply(const std::vector<double> &inputs, bool full);
};

/**
 * A trigger input that takes the difference of other inputs.
 */
class CPD3EDITING_EXPORT TriggerInputDifference : public TriggerInputMultipleFunction {
    bool requireAll;
public:
    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputDifference(const std::vector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputDifference(const QVector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputDifference(const QList<TriggerInput *> &inputs, bool requireAll = true);

    virtual ~TriggerInputDifference();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputDifference(const TriggerInputDifference &other);

    virtual double apply(const std::vector<double> &inputs, bool full);
};

/**
 * A trigger input that takes the product of other inputs.
 */
class CPD3EDITING_EXPORT TriggerInputProduct : public TriggerInputMultipleFunction {
    bool requireAll;
public:
    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputProduct(const std::vector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputProduct(const QVector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputProduct(const QList<TriggerInput *> &inputs, bool requireAll = true);

    virtual ~TriggerInputProduct();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputProduct(const TriggerInputProduct &other);

    virtual double apply(const std::vector<double> &inputs, bool full);
};

/**
 * A trigger input that takes the quotient of dividing the inputs sequentially.
 */
class CPD3EDITING_EXPORT TriggerInputQuotient : public TriggerInputMultipleFunction {
    bool requireAll;
public:
    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputQuotient(const std::vector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputQuotient(const QVector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputQuotient(const QList<TriggerInput *> &inputs, bool requireAll = true);

    virtual ~TriggerInputQuotient();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputQuotient(const TriggerInputQuotient &other);

    virtual double apply(const std::vector<double> &inputs, bool full);
};

/**
 * A trigger input that raises the first input to the powers of all other
 * inputs sequentially.
 */
class CPD3EDITING_EXPORT TriggerInputPower : public TriggerInputMultipleFunction {
    bool requireAll;
public:
    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputPower(const std::vector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputPower(const QVector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputPower(const QList<TriggerInput *> &inputs, bool requireAll = true);

    virtual ~TriggerInputPower();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputPower(const TriggerInputPower &other);

    virtual double apply(const std::vector<double> &inputs, bool full);
};

/**
 * A trigger input that returns the largest of all inputs.
 */
class CPD3EDITING_EXPORT TriggerInputLargest : public TriggerInputMultipleFunction {
    bool requireAll;
public:
    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputLargest(const std::vector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputLargest(const QVector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputLargest(const QList<TriggerInput *> &inputs, bool requireAll = true);

    virtual ~TriggerInputLargest();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputLargest(const TriggerInputLargest &other);

    virtual double apply(const std::vector<double> &inputs, bool full);
};

/**
 * A trigger input that returns the largest of all inputs.
 */
class CPD3EDITING_EXPORT TriggerInputSmallest : public TriggerInputMultipleFunction {
    bool requireAll;
public:
    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputSmallest(const std::vector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputSmallest(const QVector<TriggerInput *> &inputs, bool requireAll = true);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     * @param requireAll    if set then all inputs are required to be defined
     */
    TriggerInputSmallest(const QList<TriggerInput *> &inputs, bool requireAll = true);

    virtual ~TriggerInputSmallest();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputSmallest(const TriggerInputSmallest &other);

    virtual double apply(const std::vector<double> &inputs, bool full);
};

/**
 * A trigger input outputs the first valid of its inputs.
 */
class CPD3EDITING_EXPORT TriggerInputFirstValid : public TriggerInputMultipleFunction {
public:
    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     */
    TriggerInputFirstValid(const std::vector<TriggerInput *> &inputs);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     */
    TriggerInputFirstValid(const QVector<TriggerInput *> &inputs);

    /**
     * Create a new function.  This takes ownership of the inputs
     * 
     * @param inputs        the inputs to use
     */
    TriggerInputFirstValid(const QList<TriggerInput *> &inputs);

    virtual ~TriggerInputFirstValid();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputFirstValid(const TriggerInputFirstValid &other);

    virtual double apply(const std::vector<double> &inputs, bool full);
};

}
}

#endif
