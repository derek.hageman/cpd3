/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "editing/actionpoly.hxx"
#include "algorithms/cspline.hxx"
#include "algorithms/linearint.hxx"
#include "datacore/variant/composite.hxx"
#include "datacore/segmentprocessingstage.hxx"

using namespace CPD3::Data;
using namespace CPD3::Algorithms;

namespace CPD3 {
namespace Editing {

/** @file editing/actionpoly.hxx
 *  Edit directive actions that apply calibration polynomials.
 */

ActionApplyPoly::ActionApplyPoly(const SequenceMatch::Composite &sel, const Calibration &p)
        : selection(sel), poly(p)
{ }

ActionApplyPoly::~ActionApplyPoly() = default;

ActionApplyPoly::ActionApplyPoly(const ActionApplyPoly &other) = default;

Action *ActionApplyPoly::clone() const
{ return new ActionApplyPoly(*this); }

void ActionApplyPoly::unhandledInput(const SequenceName &unit, QList<EditDataTarget *> &,
                                     QList<EditDataTarget *> &outputs,
                                     QList<EditDataModification *> &modifiers)
{
    if (selection.matches(unit)) {
        outputs.append(this);
        return;
    }
    if (unit.isMeta() && selection.matches(unit.fromMeta())) {
        modifiers.append(this);
    }
}

void ActionApplyPoly::incomingDataAdvance(double time)
{ outputAdvance(time); }

void ActionApplyPoly::incomingSequenceValue(const SequenceValue &value)
{
    auto result = Variant::Composite::applyTransform(value.read(),
                                                     [this](const Variant::Read &input,
                                                            Variant::Write &output) {
                                                         output.setReal(poly.apply(input.toReal()));
                                                     });
    outputData(SequenceValue(value.getIdentity(), std::move(result)));
}

void ActionApplyPoly::modifySequenceValue(Data::SequenceValue &value)
{
    SegmentProcessingStage::clearPropagatedSmoothing(value.write().metadata("Smoothing"));
}


ActionInversePoly::ActionInversePoly(const SequenceMatch::Composite &sel,
                                     const Calibration &p,
                                     double mn,
                                     double mx) : selection(sel), poly(p), min(mn), max(mx)
{ }

ActionInversePoly::~ActionInversePoly() = default;

ActionInversePoly::ActionInversePoly(const ActionInversePoly &other) = default;

Action *ActionInversePoly::clone() const
{ return new ActionInversePoly(*this); }

void ActionInversePoly::unhandledInput(const SequenceName &unit,
                                       QList<EditDataTarget *> &inputs,
                                       QList<EditDataTarget *> &outputs,
                                       QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    Q_UNUSED(modifiers);
    if (!selection.matches(unit))
        return;
    outputs.append(this);
}

void ActionInversePoly::incomingDataAdvance(double time)
{ outputAdvance(time); }

void ActionInversePoly::incomingSequenceValue(const SequenceValue &value)
{
    auto result = Variant::Composite::applyTransform(value.read(),
                                                     [this](const Variant::Read &input,
                                                            Variant::Write &output) {
                                                         output.setReal(poly.inverse(input.toReal(),
                                                                                     Calibration::BestInRange,
                                                                                     min, max));
                                                     });
    outputData(SequenceValue(value.getIdentity(), std::move(result)));
}


ActionReapplyPoly::ActionReapplyPoly(const SequenceMatch::Composite &sel,
                                     const Calibration &t,
                                     const Calibration &f,
                                     double mn,
                                     double mx) : selection(sel), to(t), from(f), min(mn), max(mx)
{ }

ActionReapplyPoly::~ActionReapplyPoly() = default;

ActionReapplyPoly::ActionReapplyPoly(const ActionReapplyPoly &other) = default;

Action *ActionReapplyPoly::clone() const
{ return new ActionReapplyPoly(*this); }

void ActionReapplyPoly::unhandledInput(const SequenceName &unit,
                                       QList<EditDataTarget *> &inputs,
                                       QList<EditDataTarget *> &outputs,
                                       QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    Q_UNUSED(modifiers);
    if (!selection.matches(unit))
        return;
    outputs.append(this);
}

void ActionReapplyPoly::incomingDataAdvance(double time)
{ outputAdvance(time); }

void ActionReapplyPoly::incomingSequenceValue(const SequenceValue &value)
{
    auto result = Variant::Composite::applyTransform(value.read(),
                                                     [this](const Variant::Read &input,
                                                            Variant::Write &output) {
                                                         output.setReal(to.apply(
                                                                 from.inverse(input.toReal(),
                                                                              Calibration::BestInRange,
                                                                              min, max)));
                                                     });
    outputData(SequenceValue(value.getIdentity(), std::move(result)));
}


ActionMultiPoly::ActionMultiPoly(const SequenceMatch::Composite &sel,
                                 const QMap<double, Calibration> &poly,
                                 Interpolation method,
                                 TimeSelection timeSel,
                                 bool ext,
                                 double k0,
                                 double k1) : selection(sel),
                                              coefficients(),
                                              timeSelection(timeSel),
                                              extrapolate(ext),
                                              startTime(FP::undefined()),
                                              endTime(FP::undefined())
{
    QVector<double> times;
    for (QMap<double, Calibration>::const_iterator add = poly.constBegin(),
            endAdd = poly.constEnd(); add != endAdd; ++add) {
        Q_ASSERT(FP::defined(add.key()));
        Q_ASSERT(times.empty() || add.key() > times.back());
        times.push_back(add.key());

        while ((int) coefficients.size() < add.value().size()) {
            coefficients.emplace_back();
        }
    }
    if (!times.empty()) {
        startTime = times.front();
        endTime = times.back();
    }

    for (int i = 0, max = (int) coefficients.size(); i < max; i++) {
        QVector<double> constants;
        constants.reserve(times.size());
        for (QMap<double, Calibration>::const_iterator add = poly.constBegin(),
                endAdd = poly.constEnd(); add != endAdd; ++add) {
            double v = add->get(i);
            if (!FP::defined(v))
                v = 0.0;
            constants.push_back(v);
        }

        Model *model = nullptr;
        switch (method) {
        case Linear:
            model = new LinearInterpolator(times, constants);
            break;
        case CSpline:
            model = new CSplineInterpolator(times, constants, k0, k1);
            break;
        case Sin:
            if (!FP::defined(k0) || k0 <= 0.0) {
                model = new LinearInterpolator(times, constants);
                break;
            }
            model = new LinearTransferInterpolator(times, constants,
                                                   LinearTransferInterpolator::Transfer_Sin, k0);
            break;
        case Cos:
            if (!FP::defined(k0) || k0 <= 0.0) {
                model = new LinearInterpolator(times, constants);
                break;
            }
            model = new LinearTransferInterpolator(times, constants,
                                                   LinearTransferInterpolator::Transfer_Cos, k0);
            break;
        case Sigmoid:
            if (!FP::defined(k0) || k0 <= -1.0 || k0 >= 1.0 || k0 == 0.0) {
                model = new LinearInterpolator(times, constants);
                break;
            }
            model = new LinearTransferInterpolator(times, constants,
                                                   LinearTransferInterpolator::Transfer_NormalizedSigmoid,
                                                   k0);
            break;
        }
        if (!model)
            model = new ModelConstant(0);

        coefficients[i].reset(model);
    }
}

ActionMultiPoly::~ActionMultiPoly() = default;

ActionMultiPoly::ActionMultiPoly(const ActionMultiPoly &other) : Action(other),
                                                                 selection(other.selection),
                                                                 coefficients(),
                                                                 timeSelection(other.timeSelection),
                                                                 extrapolate(other.extrapolate),
                                                                 startTime(other.startTime),
                                                                 endTime(other.endTime)
{
    for (const auto &add : other.coefficients) {
        coefficients.emplace_back(add->clone());
    }
}

Action *ActionMultiPoly::clone() const
{ return new ActionMultiPoly(*this); }

void ActionMultiPoly::unhandledInput(const SequenceName &unit,
                                     QList<EditDataTarget *> &inputs,
                                     QList<EditDataTarget *> &outputs,
                                     QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    Q_UNUSED(modifiers);
    if (!selection.matches(unit))
        return;
    outputs.append(this);
}

void ActionMultiPoly::incomingDataAdvance(double time)
{ outputAdvance(time); }

double ActionMultiPoly::apply(double start, double end, double value)
{
    if (coefficients.empty())
        return value;
    if (!FP::defined(startTime))
        return value;

    double timePoint = start;
    switch (timeSelection) {
    case Middle:
        if (!FP::defined(start))
            timePoint = end;
        else if (FP::defined(end))
            timePoint = (start + end) * 0.5;
        break;
    case Start:
        if (!FP::defined(timePoint))
            timePoint = end;
        break;
    case End:
        if (FP::defined(end))
            timePoint = end;
        break;
    }
    if (!FP::defined(timePoint))
        timePoint = startTime;
    Q_ASSERT(FP::defined(timePoint));

    if (!extrapolate) {
        if (timePoint < startTime)
            timePoint = startTime;
        else if (timePoint > endTime)
            timePoint = endTime;
    }

    double result = 0.0;
    double accumulator = 1.0;
    for (const auto &add : coefficients) {
        double c = add->apply(timePoint);
        if (!FP::defined(c))
            c = 0.0;
        result += accumulator * c;
        accumulator *= value;
    }

    return result;
}

void ActionMultiPoly::incomingSequenceValue(const SequenceValue &value)
{
    double start = value.getStart();
    double end = value.getEnd();

    auto result = Variant::Composite::applyTransform(value.read(),
                                                     [this, start, end](const Variant::Read &input,
                                                                        Variant::Write &output) {
                                                         output.setReal(
                                                                 apply(start, end, input.toReal()));
                                                     });
    outputData(SequenceValue(value.getIdentity(), std::move(result)));
}

}
}
