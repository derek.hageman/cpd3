/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QDebugStateSaver>

#include "editing/triggerinput.hxx"
#include "core/qtcompat.hxx"
#include "datacore/variant/composite.hxx"

namespace CPD3 {
namespace Editing {

/** @file editing/triggerinput.hxx
 * The handling for inputs to simple value dependent triggers.
 */

TriggerInput::TriggerInput()
{ }

TriggerInput::TriggerInput(const TriggerInput &other)
{ Q_UNUSED(other); }

TriggerInput::~TriggerInput()
{ }

void TriggerInput::extendProcessing(double &start, double &end) const
{
    Q_UNUSED(start);
    Q_UNUSED(end);
}

QList<EditDataTarget *> TriggerInput::unhandledInput(const Data::SequenceName &unit)
{
    Q_UNUSED(unit);
    return QList<EditDataTarget *>();
}

void TriggerInput::incomingDataAdvance(double time)
{ Q_UNUSED(time); }

double TriggerInput::advancedTime() const
{ return FP::undefined(); }

void TriggerInput::finalize()
{ }

Data::SequenceName::Set TriggerInput::requestedInputs() const
{ return Data::SequenceName::Set(); }

QDebug operator<<(QDebug stream, const TriggerInput::Result &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "TriggerInput(" << Logging::range(v.start, v.end) << ',' << v.value << ')';
    return stream;
}


TriggerInputConstant::TriggerInputConstant(double v) : value(v), emitted(false)
{ }

TriggerInputConstant::TriggerInputConstant(const TriggerInputConstant &other) : TriggerInput(other),
                                                                                value(other.value),
                                                                                emitted(false)
{ }

TriggerInputConstant::~TriggerInputConstant()
{ }

TriggerInput *TriggerInputConstant::clone() const
{ return new TriggerInputConstant(*this); }

bool TriggerInputConstant::isReady(double end) const
{
    Q_UNUSED(end);
    return true;
}

QVector<TriggerInput::Result> TriggerInputConstant::process()
{
    if (emitted)
        return QVector<TriggerInput::Result>();
    emitted = true;
    return QVector<Result>() << Result(FP::undefined(), FP::undefined(), value);
}


TriggerInputValue::TriggerInputValue(const Data::SequenceMatch::OrderedLookup &input,
                                     const std::string &p) : selection(input),
                                                             path(p),
                                                             reader(),
                                                             latestTime(FP::undefined()),
                                                             completed(false),
                                                             result()
{ }

TriggerInputValue::~TriggerInputValue()
{ }

TriggerInputValue::TriggerInputValue(const TriggerInputValue &other) : TriggerInput(other),
                                                                       selection(other.selection),
                                                                       path(other.path),
                                                                       reader(),
                                                                       latestTime(FP::undefined()),
                                                                       completed(false),
                                                                       result()
{ }

TriggerInput *TriggerInputValue::clone() const
{ return new TriggerInputValue(*this); }

double TriggerInputValue::advancedTime() const
{ return latestTime; }

bool TriggerInputValue::isReady(double end) const
{ return Range::compareStartEnd(latestTime, end) >= 0 || completed; }

QList<EditDataTarget *> TriggerInputValue::unhandledInput(const Data::SequenceName &unit)
{
    if (!selection.matches(unit))
        return QList<EditDataTarget *>();
    QList<EditDataTarget *> result;
    result.append(this);
    return result;
}

void TriggerInputValue::convertSegments(const Data::ValueSegment::Transfer &segments)
{
    if (segments.empty())
        return;
    result.reserve(result.size() + segments.size());
    for (const auto &add : segments) {
        result.append(Result(add.getStart(), add.getEnd(),
                             Data::Variant::Composite::toNumber(add[path])));
    }
}

void TriggerInputValue::incomingSequenceValue(const Data::SequenceValue &value)
{
    Q_ASSERT(!completed);
    Q_ASSERT(Range::compareStart(latestTime, value.getStart()) <= 0);
    latestTime = value.getStart();
    convertSegments(reader.add(value));
}

void TriggerInputValue::incomingDataAdvance(double time)
{
    Q_ASSERT(!completed);
    Q_ASSERT(Range::compareStart(latestTime, time) <= 0);
    latestTime = time;
    convertSegments(reader.advance(time));
}

void TriggerInputValue::finalize()
{
    completed = true;
    convertSegments(reader.finish());
}

QVector<TriggerInput::Result> TriggerInputValue::process()
{
    if (result.isEmpty())
        return result;
    QVector<Result> ret(result);
    result.clear();
    return ret;
}

Data::SequenceName::Set TriggerInputValue::requestedInputs() const
{ return selection.knownInputs(); }


TriggerInputFunction::TriggerInputFunction(TriggerInput *i) : input(i)
{ }

TriggerInputFunction::~TriggerInputFunction()
{ delete input; }

TriggerInputFunction::TriggerInputFunction(const TriggerInputFunction &other) : TriggerInput(other),
                                                                                input(other.input
                                                                                           ->clone())
{ }

void TriggerInputFunction::extendProcessing(double &start, double &end) const
{ input->extendProcessing(start, end); }

double TriggerInputFunction::advancedTime() const
{ return input->advancedTime(); }

bool TriggerInputFunction::isReady(double end) const
{ return input->isReady(end); }

QList<EditDataTarget *> TriggerInputFunction::unhandledInput(const Data::SequenceName &unit)
{ return input->unhandledInput(unit); }

void TriggerInputFunction::incomingDataAdvance(double time)
{ input->incomingDataAdvance(time); }

void TriggerInputFunction::finalize()
{ return input->finalize(); }

QVector<TriggerInput::Result> TriggerInputFunction::process()
{
    QVector<Result> result(input->process());
    if (result.isEmpty())
        return result;
    for (QVector<Result>::iterator v = result.begin(), endV = result.end(); v != endV; ++v) {
        v->value = apply(v->value);
    }
    return result;
}

Data::SequenceName::Set TriggerInputFunction::requestedInputs() const
{ return input->requestedInputs(); }


TriggerInputSin::TriggerInputSin(TriggerInput *input) : TriggerInputFunction(input)
{ }

TriggerInputSin::~TriggerInputSin()
{ }

TriggerInputSin::TriggerInputSin(const TriggerInputSin &other) : TriggerInputFunction(other)
{ }

TriggerInput *TriggerInputSin::clone() const
{ return new TriggerInputSin(*this); }

double TriggerInputSin::apply(double value)
{
    if (!FP::defined(value))
        return value;
    return ::sin(value);
}

TriggerInputCos::TriggerInputCos(TriggerInput *input) : TriggerInputFunction(input)
{ }

TriggerInputCos::~TriggerInputCos()
{ }

TriggerInputCos::TriggerInputCos(const TriggerInputCos &other) : TriggerInputFunction(other)
{ }

TriggerInput *TriggerInputCos::clone() const
{ return new TriggerInputCos(*this); }

double TriggerInputCos::apply(double value)
{
    if (!FP::defined(value))
        return value;
    return ::cos(value);
}

TriggerInputLog::TriggerInputLog(TriggerInput *input) : TriggerInputFunction(input)
{ }

TriggerInputLog::~TriggerInputLog()
{ }

TriggerInputLog::TriggerInputLog(const TriggerInputLog &other) : TriggerInputFunction(other)
{ }

TriggerInput *TriggerInputLog::clone() const
{ return new TriggerInputLog(*this); }

double TriggerInputLog::apply(double value)
{
    if (!FP::defined(value) || value <= 0.0)
        return value;
    return ::log(value);
}

TriggerInputLog10::TriggerInputLog10(TriggerInput *input) : TriggerInputFunction(input)
{ }

TriggerInputLog10::~TriggerInputLog10()
{ }

TriggerInputLog10::TriggerInputLog10(const TriggerInputLog10 &other) : TriggerInputFunction(other)
{ }

TriggerInput *TriggerInputLog10::clone() const
{ return new TriggerInputLog10(*this); }

double TriggerInputLog10::apply(double value)
{
    if (!FP::defined(value) || value <= 0.0)
        return value;
    return ::log10(value);
}

TriggerInputExp::TriggerInputExp(TriggerInput *input) : TriggerInputFunction(input)
{ }

TriggerInputExp::~TriggerInputExp()
{ }

TriggerInputExp::TriggerInputExp(const TriggerInputExp &other) : TriggerInputFunction(other)
{ }

TriggerInput *TriggerInputExp::clone() const
{ return new TriggerInputExp(*this); }

double TriggerInputExp::apply(double value)
{
    if (!FP::defined(value))
        return value;
    return ::exp(value);
}

TriggerInputAbs::TriggerInputAbs(TriggerInput *input) : TriggerInputFunction(input)
{ }

TriggerInputAbs::~TriggerInputAbs()
{ }

TriggerInputAbs::TriggerInputAbs(const TriggerInputAbs &other) : TriggerInputFunction(other)
{ }

TriggerInput *TriggerInputAbs::clone() const
{ return new TriggerInputAbs(*this); }

double TriggerInputAbs::apply(double value)
{
    if (!FP::defined(value))
        return value;
    return ::fabs(value);
}

TriggerInputPoly::TriggerInputPoly(TriggerInput *input, const Calibration &p)
        : TriggerInputFunction(input), poly(p)
{ }

TriggerInputPoly::~TriggerInputPoly()
{ }

TriggerInputPoly::TriggerInputPoly(const TriggerInputPoly &other) : TriggerInputFunction(other),
                                                                    poly(other.poly)
{ }

TriggerInput *TriggerInputPoly::clone() const
{ return new TriggerInputPoly(*this); }

double TriggerInputPoly::apply(double value)
{ return poly.apply(value); }

TriggerInputPolyInvert::TriggerInputPolyInvert(TriggerInput *input,
                                               const Calibration &p,
                                               double m,
                                               double x) : TriggerInputFunction(input),
                                                           poly(p),
                                                           min(m),
                                                           max(x)
{ }

TriggerInputPolyInvert::~TriggerInputPolyInvert()
{ }

TriggerInputPolyInvert::TriggerInputPolyInvert(const TriggerInputPolyInvert &other)
        : TriggerInputFunction(other), poly(other.poly), min(other.min), max(other.max)
{ }

TriggerInput *TriggerInputPolyInvert::clone() const
{ return new TriggerInputPolyInvert(*this); }

double TriggerInputPolyInvert::apply(double value)
{ return poly.inverse(value, min, max); }


}
}
