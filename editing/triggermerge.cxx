/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <vector>
#include <QDebugStateSaver>

#include "editing/triggermerge.hxx"
#include "core/qtcompat.hxx"

namespace CPD3 {
namespace Editing {

/** @file editing/triggermerge.hxx
 * Routines for merging multiple components of a trigger together.
 */

QDebug operator<<(QDebug stream, const Internal::InputMergedDynamic &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "InputMergedFixed(" << Logging::range(v.start, v.end) << ',';
    for (size_t i = 0; i < v.values.size(); i++) {
        stream << (v.specificSeen(i) ? '1' : '0');
    }
    for (size_t i = 0; i < v.values.size(); i++) {
        stream << ',' << v.values[i];
    }
    stream << ')';
    return stream;
}

TriggerInputMultipleFunction::TriggerInputMultipleFunction(const std::vector<
        TriggerInput *> &inputs) : TriggerInputMerger<Internal::InputMergedDynamic, TriggerInput>(
        inputs), expectedSize((size_t) inputs.size())
{ }

TriggerInputMultipleFunction::TriggerInputMultipleFunction(const QVector<TriggerInput *> &inputs)
        : TriggerInputMerger<Internal::InputMergedDynamic, TriggerInput>(inputs),
          expectedSize((size_t) inputs.size())
{ }

TriggerInputMultipleFunction::TriggerInputMultipleFunction(const QList<TriggerInput *> &inputs)
        : TriggerInputMerger<Internal::InputMergedDynamic, TriggerInput>(inputs),
          expectedSize((size_t) inputs.size())
{ }

TriggerInputMultipleFunction::~TriggerInputMultipleFunction()
{ }

TriggerInputMultipleFunction::TriggerInputMultipleFunction(const TriggerInputMultipleFunction &other)
        : TriggerInputMerger<Internal::InputMergedDynamic, TriggerInput>(other),
          expectedSize(other.expectedSize)
{ }

QVector<TriggerInput::Result> TriggerInputMultipleFunction::process()
{
    QVector<Result> result;
    QVector<Internal::InputMergedDynamic> merged(takeAllReady());
    if (merged.isEmpty())
        return result;
    result.reserve(merged.size());
    for (QVector<Internal::InputMergedDynamic>::const_iterator add = merged.constBegin(),
            endAdd = merged.constEnd(); add != endAdd; ++add) {
        result.append(Result(add->getStart(), add->getEnd(),
                             apply(add->values, expectedSize == add->values.size())));
    }
    return result;
}


TriggerInputSum::TriggerInputSum(const std::vector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputSum::TriggerInputSum(const QVector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputSum::TriggerInputSum(const QList<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputSum::~TriggerInputSum()
{ }

TriggerInputSum::TriggerInputSum(const TriggerInputSum &other) : TriggerInputMultipleFunction(
        other), requireAll(other.requireAll)
{ }

TriggerInput *TriggerInputSum::clone() const
{ return new TriggerInputSum(*this); }

double TriggerInputSum::apply(const std::vector<double> &inputs, bool full)
{
    double out = 0.0;
    if (requireAll) {
        if (!full)
            return FP::undefined();
        for (std::vector<double>::const_iterator add = inputs.begin(), endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (!FP::defined(*add))
                return FP::undefined();
            out += *add;
        }
    } else {
        bool anyValid = false;
        for (std::vector<double>::const_iterator add = inputs.begin(), endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (FP::defined(*add)) {
                out += *add;
                anyValid = true;
            }
        }
        if (!anyValid)
            return FP::undefined();
    }
    return out;
}

TriggerInputDifference::TriggerInputDifference(const std::vector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputDifference::TriggerInputDifference(const QVector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputDifference::TriggerInputDifference(const QList<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputDifference::~TriggerInputDifference()
{ }

TriggerInputDifference::TriggerInputDifference(const TriggerInputDifference &other)
        : TriggerInputMultipleFunction(other), requireAll(other.requireAll)
{ }

TriggerInput *TriggerInputDifference::clone() const
{ return new TriggerInputDifference(*this); }

double TriggerInputDifference::apply(const std::vector<double> &inputs, bool full)
{
    if (inputs.empty())
        return FP::undefined();
    if (inputs.size() < 2)
        return inputs[0];

    double out = inputs[0];
    bool anyValid = false;
    if (!FP::defined(out)) {
        if (requireAll)
            return FP::undefined();
        out = 0.0;
    } else {
        anyValid = true;
    }

    if (requireAll) {
        if (!full)
            return FP::undefined();
        for (std::vector<double>::const_iterator add = inputs.begin() + 1, endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (!FP::defined(*add))
                return FP::undefined();
            out -= *add;
        }
    } else {
        for (std::vector<double>::const_iterator add = inputs.begin() + 1, endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (FP::defined(*add)) {
                out -= *add;
                anyValid = true;
            }
        }
        if (!anyValid)
            return FP::undefined();
    }
    return out;
}

TriggerInputProduct::TriggerInputProduct(const std::vector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputProduct::TriggerInputProduct(const QVector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputProduct::TriggerInputProduct(const QList<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputProduct::~TriggerInputProduct()
{ }

TriggerInputProduct::TriggerInputProduct(const TriggerInputProduct &other)
        : TriggerInputMultipleFunction(other), requireAll(other.requireAll)
{ }

TriggerInput *TriggerInputProduct::clone() const
{ return new TriggerInputProduct(*this); }

double TriggerInputProduct::apply(const std::vector<double> &inputs, bool full)
{
    double out = 1.0;
    if (requireAll) {
        if (!full)
            return FP::undefined();
        for (std::vector<double>::const_iterator add = inputs.begin(), endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (!FP::defined(*add))
                return FP::undefined();
            out *= *add;
        }
    } else {
        bool anyValid = false;
        for (std::vector<double>::const_iterator add = inputs.begin(), endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (FP::defined(*add)) {
                out *= *add;
                anyValid = true;
            }
        }
        if (!anyValid)
            return FP::undefined();
    }
    return out;
}

TriggerInputQuotient::TriggerInputQuotient(const std::vector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputQuotient::TriggerInputQuotient(const QVector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputQuotient::TriggerInputQuotient(const QList<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputQuotient::~TriggerInputQuotient()
{ }

TriggerInputQuotient::TriggerInputQuotient(const TriggerInputQuotient &other)
        : TriggerInputMultipleFunction(other), requireAll(other.requireAll)
{ }

TriggerInput *TriggerInputQuotient::clone() const
{ return new TriggerInputQuotient(*this); }

double TriggerInputQuotient::apply(const std::vector<double> &inputs, bool full)
{
    if (inputs.empty())
        return FP::undefined();
    if (inputs.size() < 2)
        return inputs[0];

    double out = inputs[0];
    bool anyValid = false;
    if (!FP::defined(out)) {
        if (requireAll)
            return FP::undefined();
        out = 0.0;
    } else {
        anyValid = true;
    }

    if (requireAll) {
        if (!full)
            return FP::undefined();
        for (std::vector<double>::const_iterator add = inputs.begin() + 1, endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (!FP::defined(*add) || *add == 0.0)
                return FP::undefined();
            out /= *add;
        }
    } else {
        for (std::vector<double>::const_iterator add = inputs.begin() + 1, endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (FP::defined(*add)) {
                if (*add == 0.0)
                    return FP::undefined();
                out /= *add;
                anyValid = true;
            }
        }
        if (!anyValid)
            return FP::undefined();
    }
    return out;
}

TriggerInputPower::TriggerInputPower(const std::vector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputPower::TriggerInputPower(const QVector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputPower::TriggerInputPower(const QList<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputPower::~TriggerInputPower()
{ }

TriggerInputPower::TriggerInputPower(const TriggerInputPower &other) : TriggerInputMultipleFunction(
        other), requireAll(other.requireAll)
{ }

TriggerInput *TriggerInputPower::clone() const
{ return new TriggerInputPower(*this); }

double TriggerInputPower::apply(const std::vector<double> &inputs, bool full)
{
    if (inputs.empty())
        return FP::undefined();
    if (inputs.size() < 2)
        return inputs[0];

    double out = inputs[0];
    bool anyValid = false;
    if (!FP::defined(out)) {
        if (requireAll)
            return FP::undefined();
        out = 0.0;
    } else {
        anyValid = true;
    }

    if (requireAll) {
        if (!full)
            return FP::undefined();
        for (std::vector<double>::const_iterator add = inputs.begin() + 1, endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (!FP::defined(*add))
                return FP::undefined();
            out = pow(out, *add);
        }
    } else {
        for (std::vector<double>::const_iterator add = inputs.begin() + 1, endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (FP::defined(*add)) {
                out = pow(out, *add);
                anyValid = true;
            }
        }
        if (!anyValid)
            return FP::undefined();
    }
    if (!FP::defined(out))
        return FP::undefined();
    return out;
}

TriggerInputLargest::TriggerInputLargest(const std::vector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputLargest::TriggerInputLargest(const QVector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputLargest::TriggerInputLargest(const QList<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputLargest::~TriggerInputLargest()
{ }

TriggerInputLargest::TriggerInputLargest(const TriggerInputLargest &other)
        : TriggerInputMultipleFunction(other), requireAll(other.requireAll)
{ }

TriggerInput *TriggerInputLargest::clone() const
{ return new TriggerInputLargest(*this); }

double TriggerInputLargest::apply(const std::vector<double> &inputs, bool full)
{
    if (inputs.empty())
        return FP::undefined();
    if (inputs.size() < 2)
        return inputs[0];

    double out = inputs[0];
    bool anyValid = false;
    if (!FP::defined(out)) {
        if (requireAll)
            return FP::undefined();
        out = 0.0;
    } else {
        anyValid = true;
    }

    if (requireAll) {
        if (!full)
            return FP::undefined();
        for (std::vector<double>::const_iterator add = inputs.begin() + 1, endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (!FP::defined(*add))
                return FP::undefined();
            if (*add < out)
                continue;
            out = *add;
        }
    } else {
        for (std::vector<double>::const_iterator add = inputs.begin() + 1, endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (FP::defined(*add)) {
                anyValid = true;
                if (*add < out)
                    continue;
                out = *add;
            }
        }
        if (!anyValid)
            return FP::undefined();
    }
    return out;
}

TriggerInputSmallest::TriggerInputSmallest(const std::vector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputSmallest::TriggerInputSmallest(const QVector<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputSmallest::TriggerInputSmallest(const QList<TriggerInput *> &inputs, bool rA)
        : TriggerInputMultipleFunction(inputs), requireAll(rA)
{ }

TriggerInputSmallest::~TriggerInputSmallest()
{ }

TriggerInputSmallest::TriggerInputSmallest(const TriggerInputSmallest &other)
        : TriggerInputMultipleFunction(other), requireAll(other.requireAll)
{ }

TriggerInput *TriggerInputSmallest::clone() const
{ return new TriggerInputSmallest(*this); }

double TriggerInputSmallest::apply(const std::vector<double> &inputs, bool full)
{
    if (inputs.empty())
        return FP::undefined();
    if (inputs.size() < 2)
        return inputs[0];

    double out = inputs[0];
    bool anyValid = false;
    if (!FP::defined(out)) {
        if (requireAll)
            return FP::undefined();
        out = 0.0;
    } else {
        anyValid = true;
    }

    if (requireAll) {
        if (!full)
            return FP::undefined();
        for (std::vector<double>::const_iterator add = inputs.begin() + 1, endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (!FP::defined(*add))
                return FP::undefined();
            if (*add > out)
                continue;
            out = *add;
        }
    } else {
        for (std::vector<double>::const_iterator add = inputs.begin() + 1, endAdd = inputs.end();
                add != endAdd;
                ++add) {
            if (FP::defined(*add)) {
                anyValid = true;
                if (*add > out)
                    continue;
                out = *add;
            }
        }
        if (!anyValid)
            return FP::undefined();
    }
    return out;
}

TriggerInputFirstValid::TriggerInputFirstValid(const std::vector<TriggerInput *> &inputs)
        : TriggerInputMultipleFunction(inputs)
{ }

TriggerInputFirstValid::TriggerInputFirstValid(const QVector<TriggerInput *> &inputs)
        : TriggerInputMultipleFunction(inputs)
{ }

TriggerInputFirstValid::TriggerInputFirstValid(const QList<TriggerInput *> &inputs)
        : TriggerInputMultipleFunction(inputs)
{ }

TriggerInputFirstValid::~TriggerInputFirstValid()
{ }

TriggerInputFirstValid::TriggerInputFirstValid(const TriggerInputFirstValid &other)
        : TriggerInputMultipleFunction(other)
{ }

TriggerInput *TriggerInputFirstValid::clone() const
{ return new TriggerInputFirstValid(*this); }

double TriggerInputFirstValid::apply(const std::vector<double> &inputs, bool full)
{
    Q_UNUSED(full);
    for (std::vector<double>::const_iterator add = inputs.begin(), endAdd = inputs.end();
            add != endAdd;
            ++add) {
        if (FP::defined(*add))
            return *add;
    }
    return FP::undefined();
}

}
}
