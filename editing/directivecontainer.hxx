/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGDIRECTIVECONTAINER_H
#define CPD3EDITINGDIRECTIVECONTAINER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QList>

#include "editing/editing.hxx"

#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "editing/editdirective.hxx"


namespace CPD3 {

namespace Data {
namespace Archive {
class Access;
}
}

namespace Editing {

class Trigger;

/**
 * An abstracted non-instantiated edit directive.
 */
class CPD3EDITING_EXPORT DirectiveContainer {
    Data::SequenceValue original;
    Data::SequenceValue value;

public:
    DirectiveContainer();

    DirectiveContainer(const DirectiveContainer &);

    DirectiveContainer &operator=(const DirectiveContainer &);

    DirectiveContainer(DirectiveContainer &&);

    DirectiveContainer &operator=(DirectiveContainer &&);

    /**
     * Create a directive container from an original archive directive.
     * 
     * @param archive   the archived directive
     */
    explicit DirectiveContainer(const Data::SequenceValue &archive);

    /**
     * Create a directive container copied from another one but forked into
     * a different target.
     * 
     * @param target    the target unit
     * @param other     the original container
     */
    DirectiveContainer(const Data::SequenceName &target, const DirectiveContainer &other);

    /**
     * Create a trigger from the given configuration.
     * 
     * @param config    the configuration
     * @param station   the station
     * @param archive   the archive
     * @return          a newly allocated trigger
     */
    static Trigger *createTrigger(const Data::Variant::Read &config,
                                  const Data::SequenceName::Component &station = {},
                                  const Data::SequenceName::Component &archive = {});

    /**
     * Parse a configuration to a fanout mode.
     * 
     * @param config        the configuration
     * @param defaultFanout the default fanout mode
     * @return              a fanout mode
     */
    static EditDirective::FanoutMode toFanout(const Data::Variant::Read &config,
                                              EditDirective::FanoutMode defaultFanout = EditDirective::Fanout_Default);

    /**
     * Get the start time of the directive.
     * 
     * @return the start time
     */
    inline double getStart() const
    { return value.getStart(); }

    /**
     * Get the end time of the directive.
     * 
     * @return the end time
     */
    inline double getEnd() const
    { return value.getEnd(); }

    /**
     * Get the backing data value containing all information about the edit
     * 
     * @return  the backing value
     */
    inline Data::Variant::Read getValue() const
    { return value.read(); }

    inline Data::Variant::Write getValue()
    { return value.write(); }

    /**
     * Return the original value used in construction of the directive.
     * 
     * @return the original data value
     */
    inline Data::SequenceValue getOriginal() const
    { return original; }

    /**
     * Set the start time of the directive
     * 
     * @param t the start time
     */
    inline void setStart(double t)
    { value.setStart(t); }

    /**
     * Set the end time of the directive
     * 
     * @param t the start time
     */
    inline void setEnd(double t)
    { value.setEnd(t); }

    /**
     * Get the profile of the directive.
     * 
     * @return the profile
     */
    inline QString getProfile() const
    { return value.getVariableQString(); }

    /**
     * Change the profile of the directive.
     * 
     * @param p the new profile
     */
    inline void setProfile(const QString &p)
    { value.setVariable(p.toStdString()); }

    /**
     * Test if the directive is currently active.
     * 
     * @return  true if the directive is actually active
     */
    bool isActive() const;

    /**
     * Instantiate the edit directive as a proper action.  This creates
     * a operational directives owned by the caller.
     * 
     * @param start     the expected start time
     * @param end       the expected end time
     * @param station   the expected station
     * @param archive   the expected archive
     * @return          new directives owned by the caller
     */
    std::vector<std::unique_ptr<EditDirective>> instantiate(double start = FP::undefined(),
                                                            double end = FP::undefined(),
                                                            const Data::SequenceName::Component &station = {},
                                                            const Data::SequenceName::Component &archive = "raw") const;

    /**
     * Extend the given bounds if required.  This produces the bounds that
     * the directive wants for input.
     * 
     * @param start     the start time
     * @param end       the end time
     * @param reader    the archive reader to use if available
     * @param station   the station to use
     * @param archive   the archive to use
     */
    void extend(double &start,
                double &end,
                Data::Archive::Access *access = nullptr,
                const Data::SequenceName::Component &station = {},
                const Data::SequenceName::Component &archive = "raw") const;

    /**
     * Add the (re-)averaging this directive requires.
     * 
     * @param continuous    the continuous to averaging required
     * @param full          the full averaging required
     */
    void getAveraging(Data::SequenceMatch::Composite &continuous, Data::SequenceMatch::Composite &full) const;

    /**
     * Test if the directive has been changed.
     * 
     * @return  true if the directive has changed
     */
    bool changed() const;

    /**
     * Commit changes and return the updated data value for the archive.
     * 
     * @param changed   set to true if the value was changed
     * @return          the updated data value
     */
    Data::SequenceValue commit(bool *changed = nullptr);

    bool operator<(const DirectiveContainer &other) const;

    bool sortLessThan(qint64 priority) const;

    bool sortGreaterThan(qint64 priority) const;
};

}
}

#endif
