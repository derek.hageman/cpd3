/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/editdirective.hxx"

#ifndef NDEBUG
#define CHECK_INCOMING_TIMINGS
#endif

using namespace CPD3::Data;

namespace CPD3 {
namespace Editing {

/** @file editing/editdirective.hxx
 * The base handling for edit directives.
 */

EditDirective::EditDirective(double s,
                             double e,
                             Trigger *t,
                             Action *a,
                             const FanoutMode &fT,
                             const FanoutMode &fA, bool aFrag) : output(nullptr),
                                                                 start(s),
                                                                 end(e),
                                                                 trigger(t),
                                                                 action(a),
                                                                 triggerFanout(fT),
                                                                 actionFanout(fA),
                                                                 actionFragment(aFrag),
                                                                 triggerData(),
                                                                 triggerDispatch(),
                                                                 triggerMux(),
                                                                 triggerBypass(nullptr),
                                                                 actionData(),
                                                                 actionDispatch(),
                                                                 actionOutputMutex(),
                                                                 actionMux(),
                                                                 actionPlaceholder(nullptr),
                                                                 skipTargets()
{
    if (action->mustSynchronizeOutput())
        actionOutputMutex.reset(new std::mutex);

    triggerBypass = triggerMux.createSimple();
    actionPlaceholder = actionMux.createPlaceholder();
}

EditDirective::~EditDirective()
{
    for (const auto &data : triggerData) {
        data.second->trigger->signalTerminate();
        data.second->trigger->finalize();
    }
    for (const auto &data : actionData) {
        data.second->action->signalTerminate();
        data.second->action->finalize();
    }
}

EditDirective::TriggerData::TriggerData()
        : trigger(), pending(), stream(), advanceTime(FP::undefined())
{ }


void EditDirective::signalTerminate()
{
    for (const auto &data : triggerData) {
        data.second->trigger->signalTerminate();
    }
    for (const auto &data : actionData) {
        data.second->action->signalTerminate();
    }
}

bool EditDirective::shouldRunDetached() const
{
    if (trigger) {
        if (trigger->shouldRunDetached())
            return true;
    } else {
        if (triggerData.begin()->second->trigger->shouldRunDetached())
            return true;
    }

    if (action) {
        if (action->shouldRunDetached())
            return true;
    } else {
        if (actionData.begin()->second->action->shouldRunDetached())
            return true;
    }
    return false;
}

bool EditDirective::mustSynchronizeOutput() const
{
    if (action) {
        if (action->mustSynchronizeOutput())
            return true;
    } else {
        if (actionData.begin()->second->action->mustSynchronizeOutput())
            return true;
    }
    return false;
}

Data::SequenceName::Set EditDirective::requestedInputs() const
{
    Data::SequenceName::Set result;
    if (trigger) {
        Util::merge(trigger->requestedInputs(), result);
    } else {
        Util::merge(triggerData.begin()->second->trigger->requestedInputs(), result);
    }

    if (action) {
        Util::merge(action->requestedInputs(), result);
    } else {
        Util::merge(actionData.begin()->second->action->requestedInputs(), result);
    }
    return result;
}

Data::SequenceName::Set EditDirective::predictedOutputs() const
{
    Data::SequenceName::Set result;
    if (action) {
        Util::merge(action->predictedOutputs(), result);
    } else {
        Util::merge(actionData.begin()->second->action->predictedOutputs(), result);
    }
    return result;
}

void EditDirective::flattenFanout(SequenceName &unit, const FanoutMode &fanout)
{
    if (!(fanout & Fanout_Station)) {
        unit.setStation(std::string());
    }

    if (!(fanout & Fanout_Archive)) {
        unit.setArchive(std::string());
    } else if ((fanout & Fanout_FlattenMeta) && unit.isMeta()) {
        unit.clearMeta();
    }

    if (!(fanout & Fanout_Variable)) {
        unit.setVariable(std::string());
    }

    if (!(fanout & Fanout_Flavors)) {
        unit.clearFlavors();
    } else if (fanout & Fanout_FlattenStatistics) {
        auto flavors = unit.getFlavors();
        flavors.erase(SequenceName::flavor_stats);
        flavors.erase(SequenceName::flavor_cover);
        flavors.erase(SequenceName::flavor_end);
        unit.setFlavors(flavors);
    }
}

EditDirective::TriggerDispatch &EditDirective::getTriggerTarget(const Data::SequenceName &name)
{
    auto target = triggerDispatch.find(name);
    if (target != triggerDispatch.end())
        return target->second;

    SequenceName fanout = name;
    flattenFanout(fanout, triggerFanout);

    auto data = triggerData.find(fanout);
    if (data == triggerData.end()) {
        std::unique_ptr<TriggerData> add(new TriggerData);
        if (trigger) {
            add->trigger = std::move(trigger);
            trigger.reset();
        } else {
            Q_ASSERT(!triggerData.empty());
            add->trigger.reset(triggerData.cbegin()->second->trigger->clone());
        }
        add->pendingAdvance = FP::undefined();
        add->stream = triggerMux.createSimple();
        add->actionBypass = actionMux.createSimple();
        data = triggerData.emplace(fanout, std::move(add)).first;
    }


    Q_ASSERT(actionDispatch.count(name) == 0);
    fanout = name;
    flattenFanout(fanout, actionFanout);

    auto action = actionData.find(fanout);
    if (action == actionData.end()) {
        std::unique_ptr<ActionData> add(new ActionData);
        if (this->action) {
            add->action = std::move(this->action);
            this->action.reset();
        } else {
            Q_ASSERT(!actionData.empty());
            add->action.reset(actionData.cbegin()->second->action->clone());
        }

        add->bypass.reset(new ActionOutput(this, add.get()));
        if (!actionFragment)
            add->output.reset(new ActionOutput(this, add.get()));
        else
            add->output.reset(new ActionOutputFragment(this, add.get(), start, end));

        add->action->setTarget(add->output.get());
        add->action->initialize();

        action = actionData.emplace(fanout, std::move(add)).first;
    }

    TriggerDispatch triggerInfo;
    ActionDispatch actionInfo;

    {
        QList<EditDataTarget *> inputs;
        QList<EditDataTarget *> outputs;
        QList<EditDataModification *> modifiers;

        action->second->action->unhandledInput(name, inputs, outputs, modifiers);

        if (outputs.empty()) {
            triggerInfo.unaffectedByAction = true;
            inputs.append(action->second->bypass.get());
        } else {
            triggerInfo.unaffectedByAction = false;
            inputs.append(outputs);
        }

        std::copy(modifiers.begin(), modifiers.end(), Util::back_emplacer(actionInfo.modifiers));
        std::copy(inputs.begin(), inputs.end(), Util::back_emplacer(actionInfo.targets));
    }
    actionInfo.data = action->second.get();

    actionDispatch.emplace(name, std::move(actionInfo));


    auto targets = data->second->trigger->unhandledInput(name);
    std::copy(targets.begin(), targets.end(), Util::back_emplacer(triggerInfo.targets));
    triggerInfo.data = data->second.get();

    return triggerDispatch.emplace(name, std::move(triggerInfo)).first->second;
}

void EditDirective::dispatchValueToTrigger(const SequenceValue &value)
{
    auto target = getTriggerTarget(value.getName());

    for (auto t : target.targets) {
        t->incomingSequenceValue(value);
    }

    auto data = target.data;

    /* If it's not affected by the action, then we can treat it as
     * "passing" the trigger (via a special bypass slot), since the dispatch
     * will then immediately route it to the final output. */
    if (target.unaffectedByAction) {
        if (triggerBypass->incomingValue(value))
            dispatchTriggerPassed();
        return;
    }

    data->pendingAdvance = FP::undefined();
    if (data->pending.empty()) {
        data->trigger->incomingDataAdvance(value.getStart());
        if (data->trigger->isReady(value.getEnd())) {
            processTriggerValue(value, data);
        } else {
            data->pending.emplace_back(value);
        }
    } else {
        data->pending.emplace_back(value);
    }
}

void EditDirective::dispatchValueToTrigger(SequenceValue &&value)
{
    auto target = getTriggerTarget(value.getName());

    for (auto t : target.targets) {
        t->incomingSequenceValue(value);
    }

    auto data = target.data;

    /* If it's not affected by the action, then we can treat it as
     * "passing" the trigger (via a special bypass slot), since the dispatch
     * will then immediately route it to the final output. */
    if (target.unaffectedByAction) {
        if (triggerBypass->incomingValue(std::move(value)))
            dispatchTriggerPassed();
        return;
    }

    data->pendingAdvance = FP::undefined();
    if (data->pending.empty()) {
        data->trigger->incomingDataAdvance(value.getStart());
        if (data->trigger->isReady(value.getEnd())) {
            processTriggerValue(std::move(value), data);
        } else {
            data->pending.emplace_back(std::move(value));
        }
    } else {
        data->pending.emplace_back(std::move(value));
    }
}

void EditDirective::dispatchValueToAction(SequenceValue &&value)
{
    auto target = actionDispatch.find(value.getName());
    /* Will already have been added during trigger lookup */
    Q_ASSERT(target != actionDispatch.end());

    if (actionFragment) {
        if (FP::defined(start) && (!FP::defined(value.getStart()) || value.getStart() < start)) {
            SequenceValue before = value;
            before.setEnd(start);
            target->second.data->bypass->incomingData(std::move(before));
        }
        if (FP::defined(end) && (!FP::defined(value.getEnd()) || value.getEnd() > end)) {
            SequenceValue after = value;
            after.setStart(end);
            target->second.data->afterFragments.emplace_back(std::move(after));
        }
    }

    for (auto t : target->second.modifiers) {
        t->modifySequenceValue(value);
    }

    for (auto t : target->second.targets) {
        t->incomingSequenceValue(value);
    }
}


void EditDirective::processPendingTriggerValues()
{
    for (const auto &td : triggerData) {
        TriggerData *data = td.second.get();

        double advanceTime = 0;
        auto flushEnd = data->pending.begin();
        for (auto pendingEnd = data->pending.end(); flushEnd != pendingEnd; ++flushEnd) {
            if (!data->trigger->isReady(flushEnd->getEnd()))
                break;
            advanceTime = flushEnd->getStart();
            processTriggerValue(std::move(*flushEnd), data);
        }
        if (flushEnd != data->pending.begin()) {
            if (FP::defined(advanceTime)) {
                if (data->stream->advance(advanceTime))
                    dispatchTriggerPassed();
            }

            data->pendingAdvance = FP::undefined();
            if (flushEnd == data->pending.end())
                data->pending.clear();
            else
                data->pending.erase(data->pending.begin(), flushEnd);
        }

        advanceTime = data->advanceTime;
        if (FP::defined(advanceTime)) {
            data->advanceTime = FP::undefined();

            if (actionOutputMutex) {
                std::lock_guard<std::mutex> lock(*actionOutputMutex);
                data->actionBypass->advance(advanceTime);
            } else {
                data->actionBypass->advance(advanceTime);
            }
        }
    }
}

void EditDirective::processTriggerValue(const SequenceValue &value, TriggerData *data)
{
    data->advanceTime = value.getStart();

    if (!data->trigger->appliesTo(value.getStart(), value.getEnd())) {
        if (actionOutputMutex) {
            std::lock_guard<std::mutex> lock(*actionOutputMutex);
            data->actionBypass->incomingValue(value);
        } else {
            data->actionBypass->incomingValue(value);
        }
        return;
    }
    if (data->stream->incomingValue(value))
        dispatchTriggerPassed();
}

void EditDirective::processTriggerValue(SequenceValue &&value, TriggerData *data)
{
    data->advanceTime = value.getStart();

    if (!data->trigger->appliesTo(value.getStart(), value.getEnd())) {
        if (actionOutputMutex) {
            std::lock_guard<std::mutex> lock(*actionOutputMutex);
            data->actionBypass->incomingValue(std::move(value));
        } else {
            data->actionBypass->incomingValue(std::move(value));
        }
        return;
    }
    if (data->stream->incomingValue(std::move(value)))
        dispatchTriggerPassed();
}

void EditDirective::advanceTrigger(double time)
{
    if (triggerBypass)
        triggerBypass->advance(time);

    for (const auto &td : triggerData) {
        TriggerData *data = td.second.get();
        data->trigger->incomingDataAdvance(time);

        if (data->pending.empty()) {
            /* The complication with pending advance comes from the fact that
             * we might have a trigger (buffered) that can't advance to the
             * current time.  If that's the case we still want to advance it
             * if incoming data stops applying to the action, so we remember
             * a time in the past we got, then keep trying that until it's ready
             * then advance to that time.  If we get any data values pending
             * we use those instead. */
            if (data->trigger->isReady(time)) {
                data->trigger->advance(time);

                Q_ASSERT(Range::compareStart(data->advanceTime, time) <= 0);
                data->advanceTime = time;
                data->pendingAdvance = FP::undefined();
                data->stream->advance(time);
            } else if (!FP::defined(data->pendingAdvance)) {
                data->pendingAdvance = time;
            } else if (data->trigger->isReady(data->pendingAdvance)) {
                Q_ASSERT(Range::compareStart(data->pendingAdvance, time) <= 0);

                data->trigger->advance(data->pendingAdvance);
                data->stream->advance(data->pendingAdvance);
                data->pendingAdvance = FP::undefined();
            }
        }
    }

    dispatchTriggerPassed();

    processPendingTriggerValues();

    double actionAdvanceTime = triggerMux.getCurrentAdvance();
    if (FP::defined(actionAdvanceTime))
        advanceAction(actionAdvanceTime);
}

void EditDirective::completeTrigger()
{
    for (const auto &td : triggerData) {
        td.second->trigger->finalize();
    }
    processPendingTriggerValues();
    triggerMux.finishAll();
    triggerBypass = nullptr;
#ifndef NDEBUG
    for (const auto &td : triggerData) {
        td.second->stream = NULL;
    }
#endif
    dispatchTriggerPassed();
}

void EditDirective::advanceAction(double time)
{
    for (const auto &data : actionData) {
        while (!data.second->afterFragments.empty()) {
            Q_ASSERT(FP::defined(time));
            Q_ASSERT(FP::defined(data.second->afterFragments.front().getStart()));
            if (data.second->afterFragments.front().getStart() > time)
                break;

            data.second->bypass->incomingData(std::move(data.second->afterFragments.front()));
            data.second->afterFragments.pop_front();
        }

        data.second->bypass->incomingAdvance(time);
        data.second->action->incomingDataAdvance(time);
    }
}

void EditDirective::dispatchTriggerPassed()
{
    SequenceValue::Transfer values;
    triggerMux.output(values);
    if (values.empty())
        return;
    double time = values.back().getStart();
    for (auto &v : values) {
        dispatchValueToAction(std::move(v));
    }
    if (!FP::defined(time))
        return;
    advanceAction(time);
}

void EditDirective::completeAction()
{
    for (const auto &data : actionData) {
        {
            SequenceValue::Transfer output;
            Util::append(std::move(data.second->afterFragments), output);
            data.second->afterFragments.clear();
            data.second->bypass->incomingData(std::move(output));
        }
        data.second->action->finalize();
    }
}

void EditDirective::finalize()
{
    completeTrigger();
    triggerData.clear();
    triggerDispatch.clear();

    completeAction();
    actionData.clear();
    actionDispatch.clear();

    /* Action has stopped now, so no more synchronization needed */
    actionMux.finishAll();
    actionPlaceholder = nullptr;
    output->incomingData(actionMux.output<SequenceValue::Transfer>());
}

void EditDirective::unlink()
{
    completeTrigger();
    if (actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*actionOutputMutex);
        for (const auto &td : triggerData) {
            td.second->actionBypass->end();
        }
    } else {
        for (const auto &td : triggerData) {
            td.second->actionBypass->end();
        }
    }
    triggerData.clear();
    triggerDispatch.clear();

    completeAction();
    for (const auto &data : actionData) {
        data.second->bypass->unlink();
        data.second->output->unlink();
    }
    actionData.clear();
    actionDispatch.clear();
}


void EditDirective::incomingData(const SequenceValue &value)
{
    double advanceTime = value.getStart();
#ifdef CHECK_INCOMING_TIMINGS
    Q_ASSERT(Range::intersects(start, end, advanceTime, value.getEnd()));
#endif

    dispatchValueToTrigger(value);
    if (FP::defined(advanceTime))
        incomingAdvance(advanceTime);
}

void EditDirective::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    double advanceTime = values.back().getStart();
    for (const auto &add : values) {
#ifdef CHECK_INCOMING_TIMINGS
        Q_ASSERT(Range::intersects(start, end, add.getStart(), add.getEnd()));
#endif

        dispatchValueToTrigger(add);
    }
    if (FP::defined(advanceTime))
        incomingAdvance(advanceTime);
}

void EditDirective::incomingData(SequenceValue &&value)
{
    double advanceTime = value.getStart();
#ifdef CHECK_INCOMING_TIMINGS
    Q_ASSERT(Range::intersects(start, end, advanceTime, value.getEnd()));
#endif

    dispatchValueToTrigger(std::move(value));
    if (FP::defined(advanceTime))
        incomingAdvance(advanceTime);
}

void EditDirective::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    double advanceTime = values.back().getStart();
    for (auto &add : values) {
#ifdef CHECK_INCOMING_TIMINGS
        Q_ASSERT(Range::intersects(start, end, add.getStart(), add.getEnd()));
#endif

        dispatchValueToTrigger(std::move(add));
    }
    if (FP::defined(advanceTime))
        incomingAdvance(advanceTime);
}

void EditDirective::incomingAdvance(double time)
{
    advanceTrigger(time);

    double outputAdvanceTime;
    if (actionOutputMutex) {
        std::unique_lock<std::mutex> lock(*actionOutputMutex);
        actionPlaceholder->advance(time);
        SequenceValue::Transfer values;
        actionMux.output(values);
        outputAdvanceTime = actionMux.getCurrentAdvance();
        lock.unlock();
        output->incomingData(std::move(values));
    } else {
        actionPlaceholder->advance(time);
        output->incomingData(actionMux.output<SequenceValue::Transfer>());
        outputAdvanceTime = actionMux.getCurrentAdvance();
    }

    if (FP::defined(outputAdvanceTime))
        output->incomingAdvance(outputAdvanceTime);
}

EditDirective::Multiplexer::Simple *EditDirective::lookupSkip(void *origin)
{
    auto target = skipTargets.find(origin);
    if (target != skipTargets.end())
        return target->second;

    if (actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*actionOutputMutex);
        target = skipTargets.emplace(origin, actionMux.createSimple()).first;
    } else {
        target = skipTargets.emplace(origin, actionMux.createSimple()).first;
    }
    return target->second;
}

void EditDirective::incomingSkipped(const SequenceValue &value, void *origin)
{
    Multiplexer::Simple *target = lookupSkip(origin);
    if (actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*actionOutputMutex);
        target->incomingValue(value);
    } else {
        target->incomingValue(value);
    }
}

void EditDirective::incomingSkipped(const SequenceValue::Transfer &values, void *origin)
{
    Multiplexer::Simple *target = lookupSkip(origin);
    if (actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*actionOutputMutex);
        target->incoming(values);
    } else {
        target->incoming(values);
    }
}

void EditDirective::incomingSkipped(SequenceValue &&value, void *origin)
{
    Multiplexer::Simple *target = lookupSkip(origin);
    if (actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*actionOutputMutex);
        target->incomingValue(std::move(value));
    } else {
        target->incomingValue(std::move(value));
    }
}

void EditDirective::incomingSkipped(SequenceValue::Transfer &&values, void *origin)
{
    Multiplexer::Simple *target = lookupSkip(origin);
    if (actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*actionOutputMutex);
        target->incoming(std::move(values));
    } else {
        target->incoming(std::move(values));
    }
}

void EditDirective::incomingSkipAdvance(double time, void *origin)
{
    auto target = skipTargets.find(origin);
    if (target == skipTargets.end())
        return;

    if (actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*actionOutputMutex);
        target->second->advance(time);
    } else {
        target->second->advance(time);
    }
}

void EditDirective::incomingSkipEnd(void *origin)
{
    auto target = skipTargets.find(origin);
    if (target == skipTargets.end())
        return;

    if (actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*actionOutputMutex);
        target->second->end();
    } else {
        target->second->end();
    }
    skipTargets.erase(target);
}


EditDirective::ActionOutput::ActionOutput(EditDirective *p, ActionData *d) : parent(p),
                                                                             data(d),
                                                                             stream(parent->actionMux
                                                                                          .createSimple())
{ }

EditDirective::ActionOutput::~ActionOutput() = default;

void EditDirective::ActionOutput::incomingSequenceValue(const SequenceValue &value)
{
    if (parent->actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*parent->actionOutputMutex);
        stream->incomingValue(value);
    } else {
        stream->incomingValue(value);
    }
}

void EditDirective::ActionOutput::incomingData(const SequenceValue &value)
{
    if (parent->actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*parent->actionOutputMutex);
        stream->incomingValue(value);
    } else {
        stream->incomingValue(value);
    }
}

void EditDirective::ActionOutput::incomingData(const SequenceValue::Transfer &values)
{
    if (parent->actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*parent->actionOutputMutex);
        stream->incoming(values);
    } else {
        stream->incoming(values);
    }
}

void EditDirective::ActionOutput::incomingData(SequenceValue &&value)
{
    if (parent->actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*parent->actionOutputMutex);
        stream->incomingValue(std::move(value));
    } else {
        stream->incomingValue(std::move(value));
    }
}

void EditDirective::ActionOutput::incomingData(SequenceValue::Transfer &&values)
{
    if (parent->actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*parent->actionOutputMutex);
        stream->incoming(std::move(values));
    } else {
        stream->incoming(std::move(values));
    }
}

void EditDirective::ActionOutput::incomingAdvance(double time)
{
    if (parent->actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*parent->actionOutputMutex);
        stream->advance(time);
    } else {
        stream->advance(time);
    }
}

void EditDirective::ActionOutput::unlink()
{
    if (parent->actionOutputMutex) {
        std::lock_guard<std::mutex> lock(*parent->actionOutputMutex);
        stream->end();
    } else {
        stream->end();
    }
    stream = nullptr;
}


EditDirective::ActionOutputFragment::ActionOutputFragment(EditDirective *p,
                                                          ActionData *d,
                                                          double s,
                                                          double e) : ActionOutput(p, d),
                                                                      start(s),
                                                                      end(e)
{ }

EditDirective::ActionOutputFragment::~ActionOutputFragment() = default;

void EditDirective::ActionOutputFragment::modifySequenceValue(SequenceValue &value)
{
    if (FP::defined(start) && (!FP::defined(value.getStart()) || value.getStart() < start))
        value.setStart(start);
    if (FP::defined(end) && (!FP::defined(value.getEnd()) || value.getEnd() > end))
        value.setEnd(end);
}

void EditDirective::ActionOutputFragment::incomingSequenceValue(const SequenceValue &value)
{
    SequenceValue mod = value;
    modifySequenceValue(mod);
    if (Range::compareStartEnd(mod.getStart(), mod.getEnd()) > 0)
        return;
    ActionOutput::incomingSequenceValue(std::move(mod));
}

void EditDirective::ActionOutputFragment::incomingData(const SequenceValue &value)
{
    SequenceValue mod(value);
    modifySequenceValue(mod);
    if (Range::compareStartEnd(mod.getStart(), mod.getEnd()) > 0)
        return;
    ActionOutput::incomingData(mod);
}

void EditDirective::ActionOutputFragment::incomingData(const SequenceValue::Transfer &values)
{
    SequenceValue::Transfer mod = values;
    for (auto v = mod.begin(), endV = mod.end(); v != endV;) {
        modifySequenceValue(*v);
        if (Range::compareStartEnd(v->getStart(), v->getEnd()) > 0) {
            v = mod.erase(v);
            endV = mod.end();
        } else {
            ++v;
        }
    }
    ActionOutput::incomingData(mod);
}

void EditDirective::ActionOutputFragment::incomingData(SequenceValue &&value)
{
    modifySequenceValue(value);
    if (Range::compareStartEnd(value.getStart(), value.getEnd()) > 0)
        return;
    ActionOutput::incomingData(std::move(value));
}

void EditDirective::ActionOutputFragment::incomingData(SequenceValue::Transfer &&values)
{
    for (auto v = values.begin(), endV = values.end(); v != endV;) {
        modifySequenceValue(*v);
        if (Range::compareStartEnd(v->getStart(), v->getEnd()) > 0) {
            v = values.erase(v);
            endV = values.end();
        } else {
            ++v;
        }
    }
    ActionOutput::incomingData(std::move(values));
}

void EditDirective::ActionOutputFragment::incomingAdvance(double time)
{
    Q_ASSERT(FP::defined(time));
    if (FP::defined(start) && time < start)
        time = start;
    ActionOutput::incomingAdvance(time);
}

}
}
