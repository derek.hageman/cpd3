/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGTRIGGERGENERICVALUE_H
#define CPD3EDITINGTRIGGERGENERICVALUE_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QSet>

#include "editing/editing.hxx"

#include "core/timeutils.hxx"
#include "datacore/stream.hxx"
#include "editing/trigger.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/segment.hxx"

namespace CPD3 {
namespace Editing {

/**
 * The base class for single value stream handling triggers.
 */
class CPD3EDITING_EXPORT TriggerValueGeneric : public TriggerSegmenting, public EditDataTarget {
    Data::SequenceMatch::OrderedLookup selection;
    std::string path;
    Data::ValueSegment::Stream reader;

    void convertSegments(const Data::ValueSegment::Transfer &segments);

public:
    /**
     * Create a value trigger.
     * 
     * @param input     the input value selection
     * @param path      the path to read from within the values
     */
    TriggerValueGeneric(const Data::SequenceMatch::OrderedLookup &input,
                        const std::string &path = std::string());

    virtual ~TriggerValueGeneric();

    virtual void finalize();

    virtual QList<EditDataTarget *> unhandledInput(const Data::SequenceName &unit);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

    virtual Data::SequenceName::Set requestedInputs() const;

protected:
    TriggerValueGeneric(const TriggerValueGeneric &other);

    /**
     * Convert the value into a truth value.
     * 
     * @param value the value to convert
     * @return      the converted truth value
     */
    virtual bool convert(const Data::Variant::Read &value) = 0;
};

/**
 * A trigger that returns true when the input has all the given flags.
 */
class CPD3EDITING_EXPORT TriggerHasFlags : public TriggerValueGeneric {
    Data::Variant::Flags flags;
public:
    /**
     * Create a has flags trigger.
     * 
     * @param flags     the flags to require
     * @param input     the input value selection
     * @param path      the path within the value
     */
    TriggerHasFlags(const Data::Variant::Flags &flags,
                    const Data::SequenceMatch::OrderedLookup &input,
                    const std::string &path = std::string());

    virtual ~TriggerHasFlags();

    Trigger *clone() const;

protected:
    TriggerHasFlags(const TriggerHasFlags &other);

    virtual bool convert(const Data::Variant::Read &value);
};

/**
 * A trigger that returns true when the input has lacks the given flags.
 */
class CPD3EDITING_EXPORT TriggerLacksFlags : public TriggerValueGeneric {
    Data::Variant::Flags flags;
public:
    /**
     * Create a lacks flags trigger.
     * 
     * @param flags     the flags to exclude
     * @param input     the input value selection
     * @param path      the path within the value
     */
    TriggerLacksFlags(const Data::Variant::Flags &flags,
                      const Data::SequenceMatch::OrderedLookup &input,
                      const std::string &path = std::string());

    virtual ~TriggerLacksFlags();

    Trigger *clone() const;

protected:
    TriggerLacksFlags(const TriggerLacksFlags &other);

    virtual bool convert(const Data::Variant::Read &value);
};

/**
 * A trigger that returns true when the value exists with any type.
 */
class CPD3EDITING_EXPORT TriggerExists : public TriggerValueGeneric {
public:
    /**
     * Create a exists trigger.
     * 
     * @param input     the input value selection
     * @param path      the path within the value
     */
    TriggerExists(const Data::SequenceMatch::OrderedLookup &input,
                  const std::string &path = std::string());

    virtual ~TriggerExists();

    Trigger *clone() const;

protected:
    TriggerExists(const TriggerExists &other);

    virtual bool convert(const Data::Variant::Read &value);
};

}
}

#endif
