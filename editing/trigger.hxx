/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGTRIGGER_H
#define CPD3EDITINGTRIGGER_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QList>
#include <QVector>

#include "editing/editing.hxx"

#include "core/timeutils.hxx"
#include "datacore/stream.hxx"
#include "editing/editdatatarget.hxx"

namespace CPD3 {
namespace Editing {

/**
 * The base class for edit triggering. 
 */
class CPD3EDITING_EXPORT Trigger {
    bool inv;
public:
    Trigger();

    virtual ~Trigger();

    /**
     * Test if the trigger applies to the given range.  This also advances
     * the trigger's backend to the start.
     * 
     * @param start     the start time
     * @param end       the end time
     * @return          true if the trigger is in effect
     */
    inline bool appliesTo(double start, double end)
    { return process(start, end) != inverted(); }

    /** @see appliesTo(double, double) */
    template<typename T>
    inline bool appliesTo(const T &value)
    { return appliesTo(value.getStart(), value.getEnd()); }

    /**
     * Advance the trigger to the given time.
     * 
     * @param           time the time to advance to
     */
    virtual void advance(double time);

    /**
     * Called at the end of processing to allow the trigger to generate
     * any final data.
     */
    virtual void finalize();

    /**
     * Test if the trigger is ready to process data ending at the given time.
     * 
     * @param end       the time time to check
     * @return          true if the trigger is ready
     */
    virtual bool isReady(double end) const = 0;

    /**
     * Duplicate the trigger.  This does NOT duplicate any state it may have,
     * just the condition the trigger reflects.
     * 
     * @return          a new trigger
     */
    virtual Trigger *clone() const = 0;

    /**
     * Test if the trigger is never active in the given range.  This is
     * used by the main chain for simplification.
     * 
     * @param start     the start of the range
     * @param end       the end of the range
     */
    virtual bool isNeverActive(double start, double end) const;

    /**
     * Test if the trigger should run detached from the main processing chain.
     * That is, if this returns true then the trigger should cause the edit
     * to be processed in another thread.  This is primarily useful for
     * script evaluations that may take a long time to calculate.
     * <br>
     * The default implementation always returns false.
     * 
     * @return          true if the trigger should be detached
     */
    virtual bool shouldRunDetached() const;

    /**
     * Extend the data processing range as required.  This is used to allow
     * triggers to ensure they have a full buffer for processing.
     * 
     * @param start     the input start time and output extended time
     * @param end       the input end time and output extended time
     */
    virtual void extendProcessing(double &start, double &end) const;

    /**
     * Register an new input.  This is called when a new unit is seen and
     * should return a list of targets for that unit.  Ownership is retained
     * by the callee.
     * 
     * @param unit      the unit to test
     * @return          the list of targets for the unit
     */
    virtual QList<EditDataTarget *> unhandledInput(const Data::SequenceName &unit);

    /**
     * Called when to indicate that the input data stream (all targets) has
     * advanced to a certain time.  No data will be generated before this time.
     * <br>
     * This will be called after every set of incoming data before any
     * of the evaluation functions (e.x. isReady(double) are called).
     * 
     * @param time      the time to advance until
     */
    virtual void incomingDataAdvance(double time);

    /**
     * Test if the trigger is inverted.  This is reflected in the application,
     * so it does not need to be applied separately.
     * 
     * @return          true if the trigger is inverted
     */
    inline bool inverted() const
    { return inv; }

    /**
     * Set the trigger's inverted state.
     * 
     * @param inverted  the new inversion state
     */
    inline void setInverted(bool inverted)
    { inv = inverted; }

    /**
     * Abort execution.
     */
    virtual void signalTerminate();

    /**
     * Returns any additional inputs requested by the trigger.  These may
     * not necessarily be available in the final input.
     * 
     * @return  the requested inputs
     */
    virtual Data::SequenceName::Set requestedInputs() const;

protected:
    Trigger(const Trigger &other);

    /**
     * Process the result of a range.  This also advances
     * the trigger's backend to the start.  This does not need to consider
     * the inversion state of the trigger.
     * 
     * @param start     the start time
     * @param end       the end time
     * @return          true if the trigger is in effect
     */
    virtual bool process(double start, double end) = 0;
};

/**
 * A trigger that is always active.
 */
class CPD3EDITING_EXPORT TriggerAlways : public Trigger {
public:
    TriggerAlways();

    virtual ~TriggerAlways();

    virtual bool isNeverActive(double start, double end) const;

    virtual bool isReady(double end) const;

    virtual Trigger *clone() const;

protected:
    virtual bool process(double start, double end);
};

/**
 * A composite trigger that requires all children to be active.
 */
class CPD3EDITING_EXPORT TriggerAND : public Trigger {
    std::vector<Trigger *> components;
public:
    /**
     * Create a new logical AND trigger.  This takes ownership of all
     * components.
     * 
     * @param components    the components
     */
    TriggerAND(const std::vector<Trigger *> &components);

    /**
     * Create a new logical AND trigger.  This takes ownership of all
     * components.
     * 
     * @param components    the components
     */
    TriggerAND(const QList<Trigger *> &components);

    /**
     * Create a new logical AND trigger.  This takes ownership of all
     * components.
     * 
     * @param components    the components
     */
    TriggerAND(const QVector<Trigger *> &components);

    virtual ~TriggerAND();

    virtual void advance(double time);

    virtual void finalize();

    virtual bool isReady(double end) const;

    virtual bool isNeverActive(double start, double end) const;

    virtual bool shouldRunDetached() const;

    virtual void extendProcessing(double &start, double &end) const;

    virtual QList<EditDataTarget *> unhandledInput(const Data::SequenceName &unit);

    virtual void incomingDataAdvance(double time);

    virtual Trigger *clone() const;

    virtual void signalTerminate();

    virtual Data::SequenceName::Set requestedInputs() const;

protected:
    TriggerAND(const TriggerAND &other);

    virtual bool process(double start, double end);
};

/**
 * A composite trigger that requires any child to be active.
 */
class CPD3EDITING_EXPORT TriggerOR : public Trigger {
    std::vector<Trigger *> components;
public:
    /**
     * Create a new logical OR trigger.  This takes ownership of all
     * components.
     * 
     * @param components    the components
     */
    TriggerOR(const std::vector<Trigger *> &components);

    /**
     * Create a new logical OR trigger.  This takes ownership of all
     * components.
     * 
     * @param components    the components
     */
    TriggerOR(const QList<Trigger *> &components);

    /**
     * Create a new logical OR trigger.  This takes ownership of all
     * components.
     * 
     * @param components    the components
     */
    TriggerOR(const QVector<Trigger *> &components);

    virtual ~TriggerOR();

    virtual void advance(double time);

    virtual void finalize();

    virtual bool isReady(double end) const;

    virtual bool isNeverActive(double start, double end) const;

    virtual bool shouldRunDetached() const;

    virtual void extendProcessing(double &start, double &end) const;

    virtual QList<EditDataTarget *> unhandledInput(const Data::SequenceName &unit);

    virtual void incomingDataAdvance(double time);

    virtual Trigger *clone() const;

    virtual void signalTerminate();

    virtual Data::SequenceName::Set requestedInputs() const;

protected:
    TriggerOR(const TriggerOR &other);

    virtual bool process(double start, double end);
};

/**
 * A trigger that extends the bounds of another trigger.
 */
class CPD3EDITING_EXPORT TriggerExtending : public Trigger {
    Trigger *trigger;

    Time::LogicalTimeUnit beforeUnit;
    int beforeCount;
    bool beforeAlign;

    Time::LogicalTimeUnit afterUnit;
    int afterCount;
    bool afterAlign;
public:
    /**
     * Create an extending trigger.  This takes ownership of the base 
     * trigger.
     * 
     * @param trigger   the base trigger to extend
     */
    TriggerExtending(Trigger *trigger);

    virtual ~TriggerExtending();

    virtual void advance(double time);

    virtual void finalize();

    virtual bool isReady(double end) const;

    virtual bool isNeverActive(double start, double end) const;

    virtual bool shouldRunDetached() const;

    virtual void extendProcessing(double &start, double &end) const;

    virtual QList<EditDataTarget *> unhandledInput(const Data::SequenceName &unit);

    virtual void incomingDataAdvance(double time);

    virtual Trigger *clone() const;

    virtual void signalTerminate();

    virtual Data::SequenceName::Set requestedInputs() const;

    /**
     * Set the time extension of the before bound.  That is, this causes
     * the trigger to be active before the normal times.
     * 
     * @param unit      the extension unit
     * @param count     the number of units to extend by
     * @param align     true if the extension is aligned
     */
    inline void setBefore(Time::LogicalTimeUnit unit, int count = 1, int align = false)
    {
        Q_ASSERT(count >= 0);
        beforeUnit = unit;
        beforeCount = count;
        beforeAlign = align;
    }

    /**
     * Set the time extension of the after bound.  That is, this causes
     * the trigger to be active after the normal times.
     * 
     * @param unit      the extension unit
     * @param count     the number of units to extend by
     * @param align     true if the extension is aligned
     */
    inline void setAfter(Time::LogicalTimeUnit unit, int count = 1, int align = false)
    {
        Q_ASSERT(count >= 0);
        afterUnit = unit;
        afterCount = count;
        afterAlign = align;
    }

protected:
    TriggerExtending(const TriggerExtending &other);

    virtual bool process(double start, double end);
};

/**
 * The base for triggers that work by generating truth segments.
 */
class CPD3EDITING_EXPORT TriggerSegmenting : public Trigger {
    struct Segment {
        double start;
        double end;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }
    };

    Segment *segments;
    size_t segmentsSize;
    size_t segmentsCapacity;

    double segmentsReady;
    bool completed;

    void discardUntil(double time);

public:
    TriggerSegmenting();

    virtual ~TriggerSegmenting();

    virtual void advance(double time);

    virtual void finalize();

    virtual bool isReady(double end) const;

protected:
    TriggerSegmenting(const TriggerSegmenting &other);

    virtual bool process(double start, double end);

    /**
     * Emit a segment.  This also advances the internal ready threshold
     * to the start of the segment.  This may only be called in start time
     * ascending order.
     * 
     * @param start     the start time the segment is effective at
     * @param end       the end time of the segment
     * @param value     the truth value of the segment
     */
    void emitSegment(double start, double end, bool value);

    /**
     * Emit an advance.  This advances the ready time until the specified time.
     * 
     * @param time      the time the segmenter has completed
     */
    void emitAdvance(double time);
};

}
}

#endif
