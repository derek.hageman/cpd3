/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGACTIONFLOWCORRECTION_H
#define CPD3EDITINGACTIONFLOWCORRECTION_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QList>

#include "editing/editing.hxx"

#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "editing/action.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/segment.hxx"
#include "core/merge.hxx"

namespace CPD3 {
namespace Editing {

/**
 * An editing action that applies a flow correction.  This reverses an
 * already applied calibration polynomial, applies a new one and updates
 * the flows and dependent parameters based on the new flow.
 */
class CPD3EDITING_EXPORT ActionFlowCorrection : public Action {
    Calibration before;
    Calibration after;
    double min;
    double max;

    Data::SequenceMatch::OrderedLookup flowSelection;
    Data::SequenceMatch::Composite accumulatorSelection;
    Data::SequenceMatch::Composite spotSelection;
    Data::SequenceMatch::Composite valueSelection;
    bool ignoreAccumulatedCut;

    typedef StreamMultiplexer<Data::SequenceValue> Multixpler;

    class FlowTarget : public EditDataTarget {
        ActionFlowCorrection *parent;
    public:
        double ratio;
        double ratioEnd;

        FlowTarget(ActionFlowCorrection *p);

        virtual ~FlowTarget();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class FlowTarget;

    FlowTarget flowTarget;

    class SpotTarget : public EditDataTarget {
        ActionFlowCorrection *parent;
    public:
        SpotTarget(ActionFlowCorrection *p);

        virtual ~SpotTarget();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class SpotTarget;

    SpotTarget spotTarget;

    struct AccumulatorShared {
        double priorOriginalValue;
        double priorReintegratedValue;

        inline AccumulatorShared() : priorOriginalValue(0.0), priorReintegratedValue(0.0)
        { }
    };

    Data::SequenceName::Map<std::unique_ptr<AccumulatorShared>> accumulatorShared;

    class AccumulatorTarget : public EditDataTarget {
        ActionFlowCorrection *parent;
        Data::SequenceName unit;
        AccumulatorShared *shared;

        Data::SequenceSegment::Stream reader;
        Multixpler::Simple *output;

        bool handleSegment(Data::SequenceSegment &&segment);

        void handleSegments(Data::SequenceSegment::Transfer &&segments,
                            bool ignoreFlush = false);

    public:
        AccumulatorTarget(ActionFlowCorrection *p, const Data::SequenceName &u,
                          AccumulatorShared *s);

        virtual ~AccumulatorTarget();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);

        void incomingDataAdvance(double time);

        void finish();
    };

    friend class LengthTarget;

    Data::SequenceName::Map<std::unique_ptr<AccumulatorTarget>> accumulatorTargets;

    class ValueTarget : public EditDataTarget {
        ActionFlowCorrection *parent;
        Data::SequenceName unit;
        Data::SequenceSegment::Stream reader;
        Multixpler::Simple *output;

        bool handleSegment(Data::SequenceSegment &&segment);

        void handleSegments(Data::SequenceSegment::Transfer &&segments,
                            bool ignoreFlush = false);

    public:
        ValueTarget(ActionFlowCorrection *p, const Data::SequenceName &u);

        virtual ~ValueTarget();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);

        void incomingDataAdvance(double time);

        void finish();
    };

    friend class ValueTarget;

    Data::SequenceName::Map<std::unique_ptr<ValueTarget>> valueTargets;

    Multixpler mux;
    Multixpler::Simple *basicTarget;

    void flushOutput();

    double recalibrate(double input) const;

public:
    /**
     * Create a new flow correction action.
     * 
     * @param before            the original (wrong) calibration
     * @param after             the updated (correct) calibration
     * @param flowSelection     the selection of flow (usually Q_$INST)
     * @param accumulatorSelection the selection of accumulators (usually (L|Qt)_$INST)
     * @param spotSelection     the selection of spot parameters (usually ZSPOT_$INST)
     * @param valueSelection    the selection of values (usually (Bb?[aes][0-9A-Z]*_$INST)|(N[0-9]*_$INST) )
     * @param ignoreAccumulatedCut if set then ignore the cut size on the accumulated values
     * @param min               the minimum range of the uncalibrated value
     * @param max               the maximum range of the uncalibrated value
     */
    ActionFlowCorrection(const Calibration &before,
                         const Calibration &after,
                         const Data::SequenceMatch::OrderedLookup &flowSelection,
                         const Data::SequenceMatch::Composite &accumulatorSelection,
                         const Data::SequenceMatch::Composite &spotSelection,
                         const Data::SequenceMatch::Composite &valueSelection,
                         bool ignoreAccumulatedCut = true,
                         double min = FP::undefined(),
                         double max = FP::undefined());

    /**
     * Create a new flow correction.
     * 
     * @param before            the original (wrong) calibration
     * @param after             the updated (correct) calibration
     * @param instrument        the instrument code to correct (e.x. "A11")
     * @param station           the station to restrict to
     * @param archive           the archive to restrict to
     * @param ignoreAccumulated if set then ignore the cut size on the accumulated values
     * @param min               the minimum range of the uncalibrated value
     * @param max               the maximum range of the uncalibrated value
     */
    ActionFlowCorrection(const Calibration &before,
                         const Calibration &after,
                         const Data::SequenceName::Component &instrument,
                         const Data::SequenceName::Component &station = {},
                         const Data::SequenceName::Component &archive = {},
                         bool ignoreAccumulated = true,
                         double min = FP::undefined(),
                         double max = FP::undefined());

    virtual ~ActionFlowCorrection();

	ActionFlowCorrection &operator=(const ActionFlowCorrection &) = delete;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void finalize();

    virtual Action *clone() const;

    virtual Data::SequenceName::Set requestedInputs() const;

protected:
    ActionFlowCorrection(const ActionFlowCorrection &other);
};

/**
 * An editing action that applies a spot size correction.  This alters the
 * accumulated sample length and resulting values based on the ration between
 * the old and new spot sizes. 
 */
class CPD3EDITING_EXPORT ActionSpotCorrection : public Action {
    double before;
    double after;
    Data::SequenceMatch::Composite lengthSelection;
    Data::SequenceMatch::OrderedLookup spotSelection;
    Data::SequenceMatch::Composite valueSelection;

    class LengthTarget : public EditDataTarget {
        ActionSpotCorrection *parent;
    public:
        LengthTarget(ActionSpotCorrection *p);

        virtual ~LengthTarget();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class LengthTarget;

    LengthTarget lengthTarget;

    class SpotTarget : public EditDataTarget {
        ActionSpotCorrection *parent;
    public:
        SpotTarget(ActionSpotCorrection *p);

        virtual ~SpotTarget();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class SpotTarget;

    SpotTarget spotTarget;

    class ValueTarget : public EditDataTarget {
        ActionSpotCorrection *parent;
    public:
        ValueTarget(ActionSpotCorrection *p);

        virtual ~ValueTarget();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class ValueTarget;

    ValueTarget valueTarget;

    double spotArea;
    double spotAreaEnd;

    double correctionFactor(double time) const;

public:
    /**
     * Create a new spot correction action.
     * 
     * @param before            the original (wrong) area
     * @param after             the updated (correct) area
     * @param lengthSelection   the selection of length (usually L_$INST)
     * @param spotSelection     the selection of spot parameters (usually ZSPOT_$INST)
     * @param valueSelection    the selection of values (usually (Bb?[aes][0-9A-Z]*_$INST)|(N[0-9]*_$INST) )
     */
    ActionSpotCorrection(double before,
                         double after,
                         const Data::SequenceMatch::Composite &lengthSelection,
                         const Data::SequenceMatch::OrderedLookup &spotSelection,
                         const Data::SequenceMatch::Composite &valueSelection);

    /**
     * Create a new spot correction.
     * 
     * @param before            the original (wrong) area
     * @param after             the updated (correct) area
     * @param instrument        the instrument code to correct (e.x. "A11")
     * @param station           the station to restrict to
     * @param archive           the archive to restrict to
     */
    ActionSpotCorrection(double before,
                         double after,
                         const Data::SequenceName::Component &instrument,
                         const Data::SequenceName::Component &station = {},
                         const Data::SequenceName::Component &archive = {});

    virtual ~ActionSpotCorrection();

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual Action *clone() const;

    virtual Data::SequenceName::Set requestedInputs() const;

protected:
    ActionSpotCorrection(const ActionSpotCorrection &other);
};

/**
 * An editing action that applies a spot size correction.  This alters the
 * accumulated sample length and resulting values based on the ration between
 * the old and new spot sizes.   This version works for multiple spot
 * instruments.
 */
class CPD3EDITING_EXPORT ActionMultiSpotCorrection : public Action {
    std::vector<double> before;
    std::vector<double> after;
    Data::SequenceMatch::Composite lengthSelection;
    Data::SequenceMatch::OrderedLookup spotSelection;
    Data::SequenceMatch::OrderedLookup indexSelection;
    Data::SequenceMatch::Composite valueSelection;

    class LengthTarget : public EditDataTarget {
        ActionMultiSpotCorrection *parent;
    public:
        LengthTarget(ActionMultiSpotCorrection *p);

        virtual ~LengthTarget();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class LengthTarget;

    LengthTarget lengthTarget;

    class SpotTarget : public EditDataTarget {
        ActionMultiSpotCorrection *parent;
    public:
        SpotTarget(ActionMultiSpotCorrection *p);

        virtual ~SpotTarget();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class SpotTarget;

    SpotTarget spotTarget;

    class IndexTarget : public EditDataTarget {
        ActionMultiSpotCorrection *parent;
    public:
        IndexTarget(ActionMultiSpotCorrection *p);

        virtual ~IndexTarget();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class IndexTarget;

    IndexTarget indexTarget;

    class ValueTarget : public EditDataTarget {
        ActionMultiSpotCorrection *parent;
    public:
        ValueTarget(ActionMultiSpotCorrection *p);

        virtual ~ValueTarget();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class ValueTarget;

    ValueTarget valueTarget;

    qint64 index;

    double spotArea;
    double spotAreaEnd;

    double effectiveArea() const;

    double inputArea() const;

    double correctionFactor(double time) const;

public:
    /**
     * Create a new spot correction action.
     * 
     * @param before            the original (wrong) areas
     * @param after             the updated (correct) areas
     * @param lengthSelection   the selection of length (usually L_$INST)
     * @param spotSelection     the selection of spot parameters (usually ZSPOT_$INST)
     * @param indexSelection    the selection of spot index (usually Fn_$INST)
     * @param valueSelection    the selection of values (usually (Bb?[aes][0-9A-Z]*_$INST)|(N[0-9]*_$INST) )
     */
    ActionMultiSpotCorrection(const std::vector<double> &before,
                              const std::vector<double> &after,
                              const Data::SequenceMatch::Composite &lengthSelection,
                              const Data::SequenceMatch::OrderedLookup &spotSelection,
                              const Data::SequenceMatch::OrderedLookup &indexSelection,
                              const Data::SequenceMatch::Composite &valueSelection);

    /**
     * Create a new spot correction.
     * 
     * @param before            the original (wrong) areas
     * @param after             the updated (correct) areas
     * @param instrument        the instrument code to correct (e.x. "A11")
     * @param station           the station to restrict to
     * @param archive           the archive to restrict to
     */
    ActionMultiSpotCorrection(const std::vector<double> &before,
                              const std::vector<double> &after,
                              const Data::SequenceName::Component &instrument,
                              const Data::SequenceName::Component &station = {},
                              const Data::SequenceName::Component &archive = {});

    virtual ~ActionMultiSpotCorrection();

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual Action *clone() const;

    virtual Data::SequenceName::Set requestedInputs() const;

protected:
    ActionMultiSpotCorrection(const ActionMultiSpotCorrection &other);
};

}
}

#endif
