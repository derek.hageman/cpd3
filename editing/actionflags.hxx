/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGACTIONFLAGS_H
#define CPD3EDITINGACTIONFLAGS_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QList>
#include <QRegExp>

#include "editing/editing.hxx"

#include "core/number.hxx"
#include "editing/action.hxx"

namespace CPD3 {
namespace Editing {

/**
 * The general case for adding a flag to the input variables.
 */
class CPD3EDITING_EXPORT ActionAddFlag
        : public Action, public EditDataTarget, public EditDataModification {
    Data::SequenceMatch::Composite selection;
    Data::Variant::Flags flags;
    Data::Variant::Root description;
    quint64 bits;
public:
    /**
     * Create a new flags adding action.
     * 
     * @param selection     the units to apply to
     * @param flags         the flags to add
     * @param description   the metadata description set
     * @param bits          the metadata bits set for the flags
     */
    ActionAddFlag(const Data::SequenceMatch::Composite &selection,
                  const Data::Variant::Flags &flags,
                  const Data::Variant::Read &description = Data::Variant::Read::empty(),
                  quint64 bits = INTEGER::undefined());

    /**
     * Create a new flags adding action.
     * 
     * @param selection     the units to apply to
     * @param flag          the flag to add
     * @param description   the metadata description set
     * @param bits          the metadata bits set for the flags
     */
    ActionAddFlag(const Data::SequenceMatch::Composite &selection,
                  const Data::Variant::Flag &flag,
                  const Data::Variant::Read &description = Data::Variant::Read::empty(),
                  quint64 bits = INTEGER::undefined());

    virtual ~ActionAddFlag();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

    virtual void modifySequenceValue(Data::SequenceValue &value);

protected:
    ActionAddFlag(const ActionAddFlag &other);
};


/**
 * A simple case for removing literal flags from the input.
 */
class CPD3EDITING_EXPORT ActionRemoveFlag : public Action, public EditDataTarget {
    Data::SequenceMatch::Composite selection;
    Data::Variant::Flags flags;
public:
    /**
     * Create a new flags removing action.
     * 
     * @param selection     the units to apply to
     * @param flags         the flags to remove
     */
    ActionRemoveFlag(const Data::SequenceMatch::Composite &selection,
                     const Data::Variant::Flags &flags);

    /**
     * Create a new flags removing action.
     * 
     * @param selection     the units to apply to
     * @param flag          the flag to remove
     */
    ActionRemoveFlag(const Data::SequenceMatch::Composite &selection,
                     const Data::Variant::Flag &flag);

    virtual ~ActionRemoveFlag();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

protected:
    ActionRemoveFlag(const ActionRemoveFlag &other);
};


/**
 * An implementation that removes all matching flags from the input.
 */
class CPD3EDITING_EXPORT ActionRemoveMatchingFlags : public Action, public EditDataTarget {
    Data::SequenceMatch::Composite selection;
    QList<QRegExp> matchers;
    bool checkAll;

    class MetadataUpdate : public EditDataTarget {
        ActionRemoveMatchingFlags *parent;
    public:
        MetadataUpdate(ActionRemoveMatchingFlags *p);

        virtual ~MetadataUpdate();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class MetadataUpdate;

    MetadataUpdate metadataUpdate;
    Data::Variant::Flags metadataMatchedFlags;

    void incomingMetadata(const Data::SequenceValue &value);

public:
    /**
     * Create a new flags removing action.
     * 
     * @param selection     the units to apply to
     * @param matchers      the list of regular expressions to match
     * @param checkAll      if set then check all flags instead of just those specified in the metadata
     */
    ActionRemoveMatchingFlags(const Data::SequenceMatch::Composite &selection,
                              const QList<QRegExp> &matchers,
                              bool checkAll = false);

    virtual ~ActionRemoveMatchingFlags();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

protected:
    ActionRemoveMatchingFlags(const ActionRemoveMatchingFlags &other);
};

}
}

#endif
