/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGTRIGGERBUFFER_H
#define CPD3EDITINGTRIGGERBUFFER_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QList>
#include <QVector>

#include "editing/editing.hxx"

#include "core/timeutils.hxx"
#include "core/range.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/variant/composite.hxx"
#include "editing/triggerinput.hxx"

namespace CPD3 {
namespace Editing {

/**
 * A manger for trigger buffers.  This handles maintaining a buffer of values
 * ahead and behind of an "active" time.  It then calls 
 * convert( const QVector<BufferType> &, const BufferType & ) to convert the 
 * buffer to an output.
 * <br>
 * The buffer type must provide getStart() and getEnd() methods.
 * <br>
 * Both bounds (forward and backward) are inclusive with respect to the
 * start times.  This is required so that a zero bound is consistent with
 * the idea that it doesn't extend at all.
 * 
 * @param BufferType        the type contained in the buffer
 * @param OutputType        the type returned by the convert process
 */
template<typename BufferType, typename OutputType>
class TriggerBuffer {
    Time::LogicalTimeUnit beforeUnit;
    int beforeCount;
    bool beforeAlign;

    Time::LogicalTimeUnit afterUnit;
    int afterCount;
    bool afterAlign;

    Time::LogicalTimeUnit gapUnit;
    int gapCount;
    bool gapAlign;

    bool requireFull;

    double latestTime;
    bool completed;

    QVector<BufferType> buffer;
    int activeIndex;

    bool bufferBeginInitialized;
    bool beforeSatisfied;
    double bufferBeginTime;

    void applyBeforeBound()
    {
        Q_ASSERT(!buffer.empty());

        double discardTime =
                Time::logical(buffer.at(activeIndex).getStart(), beforeUnit, beforeCount,
                              beforeAlign, false);
        Q_ASSERT(Range::compareStart(buffer.at(activeIndex).getStart(), discardTime) >= 0);

        typename QVector<BufferType>::iterator begin = buffer.begin();
        typename QVector<BufferType>::iterator
                rm = Range::heuristicUpperBound(begin, begin + activeIndex, discardTime);
        Q_ASSERT(rm != buffer.end());
        if (rm == begin)
            return;
        if (FP::equal((rm - 1)->getStart(), discardTime)) {
            --rm;
            if (rm == begin)
                return;
        } else {
            Q_ASSERT(Range::compareStart((rm - 1)->getStart(), discardTime) < 0);
        }
        Q_ASSERT(Range::compareStart(discardTime, rm->getStart()) <= 0);

        activeIndex -= (rm - begin);
        buffer.erase(begin, rm);

        Q_ASSERT(activeIndex >= 0);
        Q_ASSERT(activeIndex < buffer.size());
    }

    void dumpWholeBuffer(QVector<OutputType> &result)
    {
        /* At this point, we know we don't have anything possible after,
         * so we just move the active until we've output everything, discarding
         * the ones outside the before window as we move forward. */
        for (; activeIndex < buffer.size(); activeIndex++) {
            applyBeforeBound();
            result.append(convert(buffer, buffer.at(activeIndex)));
        }
    }

    void processGap(QVector<OutputType> &result)
    {
        if (gapCount < 0)
            return;
        if (buffer.isEmpty())
            return;
        double checkTime = Time::logical(buffer.last().getEnd(), gapUnit, gapCount, gapAlign, true);
        if (Range::compareStartEnd(latestTime, checkTime) <= 0)
            return;
        if (!requireFull)
            dumpWholeBuffer(result);
        buffer.clear();
        activeIndex = 0;
        beforeSatisfied = false;
        bufferBeginInitialized = false;
    }

    double calculateAfterSatisfactionTime() const
    {
        Q_ASSERT(!buffer.isEmpty());

        double originTime = buffer.at(activeIndex).getStart();
        double requiredTime = Time::logical(originTime, afterUnit, afterCount, afterAlign, true);
        /* Special to catch the degenerate round up that would otherwise snap
         * to a full unit immediately after the first value.  */
        if (afterAlign &&
                afterCount == 0 &&
                FP::defined(requiredTime) &&
                requiredTime == originTime) {
            requiredTime = Time::logical(requiredTime, afterUnit, 1, afterAlign, true);
        }
        return requiredTime;
    }

    void processCompleted(QVector<OutputType> &result)
    {
        /* Since we've already output everything that's been completed,
         * we need to move the active until it's inside the after window (it
         * can't be output).  While moving it we output it if the before
         * is satisfied (either because there is enough data or because
         * we're not requiring a full buffer). */

        for (; activeIndex < buffer.size(); activeIndex++) {
            if (Range::compareStart(calculateAfterSatisfactionTime(), latestTime) > 0)
                return;

            applyBeforeBound();

            if (!beforeSatisfied) {
                /* Should only enter here after we've seen a value 
                 * (the gap would clear the buffer entirely and an advance
                 * wouldn't be able to add data), so we can
                 * always have an initialized buffer start. */
                Q_ASSERT(bufferBeginInitialized);
                Q_ASSERT(FP::defined(bufferBeginTime));

                double checkTime =
                        Time::logical(buffer.at(activeIndex).getStart(), beforeUnit, beforeCount,
                                      beforeAlign, false);
                if (Range::compareStart(checkTime, bufferBeginTime) >= 0)
                    beforeSatisfied = true;
            }

            if (beforeSatisfied || !requireFull) {
                result.append(convert(buffer, buffer.at(activeIndex)));
            }
        }
    }

    void processCompletedPartial(QVector<OutputType> &result)
    {
        for (; activeIndex < buffer.size(); activeIndex++) {
            if (Range::compareStart(calculateAfterSatisfactionTime(), latestTime) >= 0)
                break;

            applyBeforeBound();

            if (!beforeSatisfied) {
                /* Should only enter here after we've seen a value 
                 * (the gap would clear the buffer entirely and an advance
                 * wouldn't be able to add data), so we can
                 * always have an initialized buffer start. */
                Q_ASSERT(bufferBeginInitialized);
                Q_ASSERT(FP::defined(bufferBeginTime));

                double checkTime =
                        Time::logical(buffer.at(activeIndex).getStart(), beforeUnit, beforeCount,
                                      beforeAlign, false);
                if (Range::compareStart(checkTime, bufferBeginTime) >= 0)
                    beforeSatisfied = true;
            }

            if (beforeSatisfied || !requireFull) {
                result.append(convert(buffer, buffer.at(activeIndex)));
            }
        }
    }

public:
    TriggerBuffer() : beforeUnit(Time::Minute),
                      beforeCount(0),
                      beforeAlign(false),
                      afterUnit(Time::Minute),
                      afterCount(0),
                      afterAlign(false),
                      gapUnit(Time::Minute),
                      gapCount(-1),
                      gapAlign(false),
                      requireFull(false),
                      latestTime(FP::undefined()),
                      completed(false),
                      buffer(),
                      activeIndex(0),
                      bufferBeginInitialized(false),
                      beforeSatisfied(false),
                      bufferBeginTime(FP::undefined())
    { }

    virtual ~TriggerBuffer() = default;

    /**
     * Add an incoming value to the buffer and return all completed
     * outputs.
     * 
     * @param add       the value to add
     * @return          the completed values
     */
    QVector<OutputType> incomingBufferData(const BufferType &add)
    {
        Q_ASSERT(!completed);
        Q_ASSERT(Range::compareStart(add.getStart(), latestTime) >= 0);
        Q_ASSERT(Range::compareStartEnd(add.getStart(), add.getEnd()) <= 0);
        /* Required so that we can output as soon as we see a start time */
        Q_ASSERT(buffer.empty() ||
                         Range::compareStart(add.getStart(), buffer.last().getStart()) > 0);
        latestTime = add.getStart();

        QVector<OutputType> result;

        /* Process gap first, so the completed isn't updated before we
         * dump/clear due to gaps. */
        processGap(result);

        if (!beforeSatisfied && !bufferBeginInitialized) {
            bufferBeginInitialized = true;
            bufferBeginTime = latestTime;
            if (!FP::defined(bufferBeginTime)) {
                beforeSatisfied = true;
            }
        }

        if (!buffer.empty() && Range::compareStartEnd(latestTime, buffer.last().getEnd()) < 0) {
            buffer.last().setEnd(latestTime);
        }

        /* Check if the new value is included in the current after bound,
         * and if it is, then add it before doing the output.  Otherwise
         * do the output then add it. */
        if (activeIndex >= buffer.size() ||
                Range::compareStart(calculateAfterSatisfactionTime(), latestTime) >= 0) {
            buffer.push_back(add);
            processCompleted(result);
        } else {
            processCompletedPartial(result);
            buffer.push_back(add);
            processCompleted(result);
        }
        return result;
    }

    /**
     * Advance the data time to the given start time.
     * 
     * @param time      the time to advance to
     * @return          the completed values
     */
    QVector<OutputType> incomingBufferAdvance(double time)
    {
        Q_ASSERT(!completed);
        if (FP::equal(time, latestTime))
            return QVector<OutputType>();
        Q_ASSERT(Range::compareStart(time, latestTime) > 0);
        latestTime = time;

        QVector<OutputType> result;
        processGap(result);
        if (!buffer.empty())
            processCompletedPartial(result);
        return result;
    }

    /**
     * Signal the end of incoming data and generate any final outputs.
     * 
     * @return          the completed values
     */
    QVector<OutputType> endBufferData()
    {
        completed = true;

        QVector<OutputType> result;
        if (!requireFull)
            dumpWholeBuffer(result);
        buffer.clear();
        activeIndex = 0;
        beforeSatisfied = false;
        bufferBeginInitialized = false;
        return result;
    }

    /**
     * Test if all segments before a given end time have been completed.
     * 
     * @param end       the end time to check
     * @return          true if the data is ready up until the end
     */
    bool isBufferReady(double end) const
    {
        if (activeIndex >= buffer.size())
            return Range::compareStartEnd(latestTime, end) >= 0 || completed;
        return Range::compareStartEnd(buffer.at(activeIndex).getStart(), end) >= 0 || completed;
    }

    /**
     * Get how far the buffer output (via convert calls) has advanced.  No
     * convert calls with a start time before this will be made.
     * 
     * @return  the advanced time or undefined
     */
    double bufferAdvancedTime() const
    {
        if (activeIndex >= buffer.size())
            return latestTime;
        return buffer.at(activeIndex).getStart();
    }


    /**
     * Add all values in a container to the buffer.
     * 
     * @param values    the values to add
     * @return          the resulting outputs
     */
    template<template<typename ContainerType> class Container>
    QVector<OutputType> incomingBufferData(const Container<BufferType> &values)
    {
        QVector<OutputType> result;
        for (typename Container<BufferType>::const_iterator add = values.begin(),
                endAdd = values.end(); add != endAdd; ++add) {
            QVector<OutputType> rv(incomingBufferData(*add));
            if (!rv.isEmpty())
                result << rv;
        }
        return result;
    }


    /**
     * Copy all parameters (but no state) from another buffer.
     * 
     * @param other the buffer to copy parameters from
     */
    void copyBufferParameters(const TriggerBuffer<BufferType, OutputType> &other)
    {
        beforeUnit = other.beforeUnit;
        beforeCount = other.beforeCount;
        beforeAlign = other.beforeAlign;

        afterUnit = other.afterUnit;
        afterCount = other.afterCount;
        afterAlign = other.afterAlign;

        gapUnit = other.gapUnit;
        gapCount = other.gapCount;
        gapAlign = other.gapAlign;

        requireFull = other.requireFull;
    }

    /**
     * Set the amount of time retained before the active segment.
     * 
     * @param unit      the extension unit
     * @param count     the number of units to extend by
     * @param align     true if the extension is aligned
     */
    void setBefore(Time::LogicalTimeUnit unit, int count = 1, int align = false)
    {
        Q_ASSERT(count >= 0);
        beforeUnit = unit;
        beforeCount = -count;
        beforeAlign = align;
    }

    /**
     * Set the amount of time required after the active segment.
     * 
     * @param unit      the extension unit
     * @param count     the number of units to extend by
     * @param align     true if the extension is aligned
     */
    void setAfter(Time::LogicalTimeUnit unit, int count = 1, int align = false)
    {
        Q_ASSERT(count >= 0);
        afterUnit = unit;
        afterCount = count;
        afterAlign = align;
    }

    /**
     * Set the amount of time required for a gap to be triggered.  If the
     * count is negative then gaps are never generated.
     * 
     * @param unit      the extension unit
     * @param count     the number of units to require or negative to disable gaps
     * @param align     true if the extension is aligned
     */
    void setGap(Time::LogicalTimeUnit unit, int count = 1, int align = false)
    {
        gapUnit = unit;
        gapCount = count;
        gapAlign = align;
    }

    /**
     * Enable or disable the requirement of a full buffer for processing.
     * When enabled, incomplete segments are generated whenever the buffer
     * bounds are less than the full before and after range instead of
     * converting the partial buffer.
     * 
     * @param enable    enable or disable the full requirement
     */
    void setRequireFull(bool enable)
    { requireFull = enable; }

    /**
     * Extend the processing bounds so that the buffer could fill.
     * 
     * @param start     the input and output start time
     * @param end       the input and output end time
     */
    void extendBufferProcessing(double &start, double &end) const
    {
        start = Time::logical(start, beforeUnit, beforeCount, beforeAlign, false);
        end = Time::logical(end, afterUnit, afterCount, afterAlign, true);
    }

    /**
     * Set the parameters of the buffer from configuration setting.
     * 
     * @param config    the configuration to set
     */
    void configureBuffer(const Data::Variant::Read &config)
    {
        if (config["Interval"].exists()) {
            int count = 0;
            bool align = false;
            Time::LogicalTimeUnit unit =
                    Data::Variant::Composite::toTimeInterval(config["Interval"], &count, &align);
            if (count >= 0) {
                setBefore(unit, count, align);
                setAfter(unit, count, align);
            }
        }
        if (config["Before"].exists()) {
            int count = 0;
            bool align = false;
            Time::LogicalTimeUnit unit =
                    Data::Variant::Composite::toTimeInterval(config["Before"], &count, &align);
            if (count >= 0) {
                setBefore(unit, count, align);
            }
        }
        if (config["After"].exists()) {
            int count = 0;
            bool align = false;
            Time::LogicalTimeUnit unit =
                    Data::Variant::Composite::toTimeInterval(config["After"], &count, &align);
            if (count >= 0) {
                setAfter(unit, count, align);
            }
        }
        if (config["Gap"].exists()) {
            if (config["Gap"].getType() == Data::Variant::Type::Boolean) {
                if (config["Gap"].toBool()) {
                    setGap(Time::Minute, 0, false);
                } else {
                    setGap(Time::Minute, -1, false);
                }
            } else {
                int count = 0;
                bool align = false;
                Time::LogicalTimeUnit unit =
                        Data::Variant::Composite::toTimeInterval(config["Gap"], &count, &align);
                setGap(unit, count, align);
            }
        }
    }

protected:
    /**
     * Called by the internal processing to convert the buffer and the active
     * element to an output.
     * 
     * @param buffer        the current buffer
     * @param active        the active element
     * @return              the converted output
     */
    virtual OutputType convert(const QVector<BufferType> &buffer, const BufferType &active) = 0;
};


/**
 * A base class for simple single input buffers.
 */
class CPD3EDITING_EXPORT TriggerInputBuffer
        : public TriggerInput, private TriggerBuffer<TriggerInput::Result, TriggerInput::Result> {
    TriggerInput *input;
    QVector<Result> result;

    void processPending();

public:
    /**
     * Create a buffer input.  This takes ownership of the wrapped
     * input.
     * 
     * @param input     the input to the function
     * @param config    the buffer configuration
     */
    TriggerInputBuffer(TriggerInput *input,
                       const Data::Variant::Read &config = Data::Variant::Read::empty());

    virtual ~TriggerInputBuffer();

    virtual void extendProcessing(double &start, double &end) const;

    virtual double advancedTime() const;

    virtual bool isReady(double end) const;

    virtual void incomingDataAdvance(double time);

    virtual QList<EditDataTarget *> unhandledInput(const Data::SequenceName &unit);

    virtual QVector<Result> process();

    virtual void finalize();

protected:
    TriggerInputBuffer(const TriggerInputBuffer &other);

    virtual Result convert(const QVector<Result> &buffer, const Result &active) = 0;
};

/**
 * A buffering input that takes the simple mean of all values in the
 * range.
 */
class CPD3EDITING_EXPORT TriggerInputMean : public TriggerInputBuffer {
public:
    /**
     * Create a mean input.  This takes ownership of the wrapped
     * input.
     * 
     * @param input     the input to the function
     * @param config    the buffer configuration
     */
    TriggerInputMean(TriggerInput *input,
                     const Data::Variant::Read &config = Data::Variant::Read::empty());

    virtual ~TriggerInputMean();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputMean(const TriggerInputMean &other);

    virtual Result convert(const QVector<Result> &buffer, const Result &active);
};

/**
 * A buffering input that takes the simple standard deviation of all values 
 * in the range.
 */
class CPD3EDITING_EXPORT TriggerInputSD : public TriggerInputBuffer {
public:
    /**
     * Create a SD input.  This takes ownership of the wrapped
     * input.
     * 
     * @param input     the input to the function
     * @param config    the buffer configuration
     */
    TriggerInputSD(TriggerInput *input,
                   const Data::Variant::Read &config = Data::Variant::Read::empty());

    virtual ~TriggerInputSD();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputSD(const TriggerInputSD &other);

    virtual Result convert(const QVector<Result> &buffer, const Result &active);
};


/**
 * A buffering input that takes a quantile of the input buffer.
 */
class CPD3EDITING_EXPORT TriggerInputQuantile : public TriggerInputBuffer {
    double quantile;
public:
    /**
     * Create a quantile input.  This takes ownership of the wrapped
     * input.
     * 
     * @param input     the input to the function
     * @param quantile  the quantile to take
     * @param config    the buffer configuration
     */
    TriggerInputQuantile(TriggerInput *input,
                         double quantile = 0.5,
                         const Data::Variant::Read &config = Data::Variant::Read::empty());

    virtual ~TriggerInputQuantile();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputQuantile(const TriggerInputQuantile &other);

    virtual Result convert(const QVector<Result> &buffer, const Result &active);
};

/**
 * A buffering input that generates the slope of the least squares line.
 */
class CPD3EDITING_EXPORT TriggerInputSlope : public TriggerInputBuffer {
public:
    /**
     * Create a slope input.  This takes ownership of the wrapped
     * input.
     * 
     * @param input     the input to the function
     * @param config    the buffer configuration
     */
    TriggerInputSlope(TriggerInput *input,
                      const Data::Variant::Read &config = Data::Variant::Read::empty());

    virtual ~TriggerInputSlope();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputSlope(const TriggerInputSlope &other);

    virtual Result convert(const QVector<Result> &buffer, const Result &active);
};

/**
 * A buffering input that returns the time length of the input buffer.  This is
 * just the end time minus the start time without considering the relative
 * coverage.
 */
class CPD3EDITING_EXPORT TriggerInputLength : public TriggerInputBuffer {
public:
    /**
     * Create a length input.  This takes ownership of the wrapped
     * input.
     * 
     * @param input     the input to the function
     * @param config    the buffer configuration
     */
    TriggerInputLength(TriggerInput *input,
                       const Data::Variant::Read &config = Data::Variant::Read::empty());

    virtual ~TriggerInputLength();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputLength(const TriggerInputLength &other);

    virtual Result convert(const QVector<Result> &buffer, const Result &active);
};


}
}

#endif
