/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <thread>
#include <QLoggingCategory>

#include "editing/actionscriptgeneral.hxx"
#include "luascript/libs/streamvalue.hxx"
#include "luascript/libs/sequencesegment.hxx"

using namespace CPD3::Data;


Q_LOGGING_CATEGORY(log_editing_actionscriptgeneral, "cpd3.editing.actionscriptgeneral",
                   QtWarningMsg)

namespace CPD3 {
namespace Editing {

/** @file editing/actionscriptgeneral.hxx
 * Editing actions that us a general purpose script processor.
 */


ActionScriptSequenceSegmentGeneral::ActionScriptSequenceSegmentGeneral(Data::SequenceMatch::OrderedLookup outputs,
                                                                       Data::SequenceMatch::OrderedLookup inputs,
                                                                       std::string code,
                                                                       bool implicitMeta) : outputs(
        outputs),
                                                                                            inputs(std::move(
                                                                                                    inputs)),
                                                                                            code(std::move(
                                                                                                    code)),
                                                                                            implicitMeta(
                                                                                                    implicitMeta),
                                                                                            registeredOutputs(
                                                                                                    std::move(
                                                                                                            outputs)),
                                                                                            buffer(*this)
{ }

ActionScriptSequenceSegmentGeneral::~ActionScriptSequenceSegmentGeneral()
{ finalize(); }

ActionScriptSequenceSegmentGeneral::ActionScriptSequenceSegmentGeneral(const ActionScriptSequenceSegmentGeneral &other)
        : Action(other), outputs(other.outputs), inputs(other.inputs), code(other.code),
          implicitMeta(other.implicitMeta), registeredOutputs(outputs), buffer(*this)
{ }

Action *ActionScriptSequenceSegmentGeneral::clone() const
{ return new ActionScriptSequenceSegmentGeneral(*this); }

bool ActionScriptSequenceSegmentGeneral::mustSynchronizeOutput() const
{ return true; }

void ActionScriptSequenceSegmentGeneral::unhandledInput(const SequenceName &name,
                                                        QList<EditDataTarget *> &inputs,
                                                        QList<EditDataTarget *> &outputs,
                                                        QList<EditDataModification *> &)
{
    if (implicitMeta && name.isMeta()) {
        auto nometa = name.fromMeta();
        std::lock_guard<std::mutex> lock(buffer.mutex);
        if (this->inputs.matches(nometa) || this->registeredOutputs.matches(nometa))
            inputs.append(this);
        return;
    }

    std::lock_guard<std::mutex> lock(buffer.mutex);
    if (registeredOutputs.registerInput(name)) {
        outputs.append(this);
        return;
    }
    if (this->inputs.matches(name)) {
        inputs.append(this);
        return;
    }
}

SequenceName::Set ActionScriptSequenceSegmentGeneral::requestedInputs() const
{ return inputs.knownInputs(); }

SequenceName::Set ActionScriptSequenceSegmentGeneral::predictedOutputs() const
{ return outputs.knownInputs(); }

void ActionScriptSequenceSegmentGeneral::run()
{
    Lua::Engine engine;

    Lua::Engine::Frame root(engine);
    buffer.pushLuaController(root);
    auto controller = root.back();
    auto environment = root.pushSandboxEnvironment();
    environment.set("data", controller);
    {
        Lua::Engine::Assign assign(root, environment, "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_editing_actionscriptgeneral) << entry[i].toOutputString();
            }
        }));
    }

    {
        Lua::Engine::Call call(root);
        if (!call.pushChunk(code, environment)) {
            qCDebug(log_editing_actionscriptgeneral) << "Error parsing sequence segment code:"
                                                     << call.errorDescription();
            call.clearError();

            {
                std::lock_guard<std::mutex> lock(buffer.mutex);
                if (buffer.state == Buffer::State::Active)
                    buffer.state = Buffer::State::Ended;
            }
            double time = buffer.finish(call, controller);

            std::lock_guard<std::mutex> lock(buffer.mutex);
            buffer.state = Buffer::State::Finished;
            buffer.advance = time;
            buffer.notify.notify_all();
            return;
        }
        if (!call.execute()) {
            engine.clearError();
            {
                std::lock_guard<std::mutex> lock(buffer.mutex);
                if (buffer.state == Buffer::State::Active)
                    buffer.state = Buffer::State::Ended;
            }
            double time = buffer.finish(call, controller);

            std::lock_guard<std::mutex> lock(buffer.mutex);
            buffer.state = Buffer::State::Finished;
            buffer.advance = time;
            buffer.notify.notify_all();
            return;
        }
    }

    double time = buffer.finish(root, controller);

    std::lock_guard<std::mutex> lock(buffer.mutex);
    buffer.state = Buffer::State::Finished;
    buffer.advance = time;
    buffer.notify.notify_all();
}

void ActionScriptSequenceSegmentGeneral::incomingSequenceValue(const SequenceValue &value)
{
    auto add = reader.add(value);
    if (add.empty())
        return;
    {
        std::lock_guard<std::mutex> lock(buffer.mutex);
        if (buffer.state == Buffer::State::Finished)
            return;
        Util::append(std::move(add), buffer.incoming);
        buffer.advance = FP::undefined();
    }
    buffer.notify.notify_all();
}

void ActionScriptSequenceSegmentGeneral::incomingDataAdvance(double time)
{
    auto add = reader.advance(time);
    {
        std::unique_lock<std::mutex> lock(buffer.mutex);
        if (buffer.state == Buffer::State::Finished) {
            /* Keep advancing even if the script code finishes, since we don't have
             * a separate end condition */
            if (Range::compareStart(time, buffer.advance) <= 0)
                return;
            lock.unlock();
            outputAdvance(time);
            return;
        }
        Util::append(std::move(add), buffer.incoming);
        buffer.advance = time;
    }
    buffer.notify.notify_all();
}

void ActionScriptSequenceSegmentGeneral::finalize()
{
    auto add = reader.finish();
    {
        std::lock_guard<std::mutex> lock(buffer.mutex);
        if (buffer.state == Buffer::State::Finished)
            return;
        Util::append(std::move(add), buffer.incoming);
        buffer.state = Buffer::State::Ended;
    }

    buffer.notify.notify_all();

    {
        std::unique_lock<std::mutex> lock(buffer.mutex);
        buffer.notify.wait(lock, [this] { return buffer.state == Buffer::State::Finished; });
    }
}

void ActionScriptSequenceSegmentGeneral::initialize()
{
    {
        std::lock_guard<std::mutex> lock(buffer.mutex);
        if (buffer.state == Buffer::State::Initialize)
            buffer.state = Buffer::State::Active;
    }
    std::thread(std::bind(&ActionScriptSequenceSegmentGeneral::run, this)).detach();
}

void ActionScriptSequenceSegmentGeneral::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(buffer.mutex);
        if (buffer.state != Buffer::State::Active)
            return;
        buffer.state = Buffer::State::Ended;
    }
    buffer.notify.notify_all();
}

ActionScriptSequenceSegmentGeneral::Buffer::Buffer(ActionScriptSequenceSegmentGeneral &parent)
        : parent(parent), state(State::Initialize), advance(FP::undefined())
{ }

ActionScriptSequenceSegmentGeneral::Buffer::~Buffer() = default;

bool ActionScriptSequenceSegmentGeneral::Buffer::pushNext(Lua::Engine::Frame &target)
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        switch (state) {
        case State::Initialize:
            Q_ASSERT(false);
            return false;
        case State::Active:
            if (!incoming.empty()) {
                target.pushData<Lua::Libs::SequenceSegment>(std::move(incoming.front()));
                incoming.pop_front();
                return true;
            }
            if (FP::defined(advance)) {
                target.push(advance);
                advance = FP::undefined();
                return true;
            }
            notify.wait(lock);
            break;
        case State::Ended:
            if (!incoming.empty()) {
                target.pushData<Lua::Libs::SequenceSegment>(std::move(incoming.front()));
                incoming.pop_front();
                return true;
            }
            return false;
        case State::Finished:
            return false;
        }
    }
}

void ActionScriptSequenceSegmentGeneral::Buffer::outputReady(Lua::Engine::Frame &frame,
                                                             const Lua::Engine::Reference &ref)
{
    SequenceName::Set outputNames;
    {
        std::lock_guard<std::mutex> lock(mutex);
        outputNames = parent.registeredOutputs.knownInputs();
    }
    parent.outputData(extract(frame, ref, outputNames));
}

void ActionScriptSequenceSegmentGeneral::Buffer::advanceReady(double time)
{ parent.outputAdvance(time); }

void ActionScriptSequenceSegmentGeneral::Buffer::endReady()
{ }


ActionScriptSequenceValueGeneral::ActionScriptSequenceValueGeneral(SequenceMatch::OrderedLookup selection,
                                                                   std::string code) : selection(
        std::move(selection)), code(std::move(code)), buffer(*this)
{ }

ActionScriptSequenceValueGeneral::~ActionScriptSequenceValueGeneral()
{ finalize(); }

ActionScriptSequenceValueGeneral::ActionScriptSequenceValueGeneral(const ActionScriptSequenceValueGeneral &other)
        : Action(other), selection(other.selection), code(other.code), buffer(*this)
{ }

Action *ActionScriptSequenceValueGeneral::clone() const
{ return new ActionScriptSequenceValueGeneral(*this); }

bool ActionScriptSequenceValueGeneral::mustSynchronizeOutput() const
{ return true; }

void ActionScriptSequenceValueGeneral::unhandledInput(const SequenceName &name,
                                                      QList<EditDataTarget *> &,
                                                      QList<EditDataTarget *> &outputs,
                                                      QList<EditDataModification *> &)
{
    if (!selection.matches(name))
        return;
    outputs.append(this);
}

SequenceName::Set ActionScriptSequenceValueGeneral::requestedInputs() const
{ return selection.knownInputs(); }

void ActionScriptSequenceValueGeneral::run()
{
    Lua::Engine engine;

    Lua::Engine::Frame root(engine);
    buffer.pushLuaController(root);
    auto controller = root.back();
    auto environment = root.pushSandboxEnvironment();
    environment.set("data", controller);
    {
        Lua::Engine::Assign assign(root, environment, "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_editing_actionscriptgeneral) << entry[i].toOutputString();
            }
        }));
    }

    {
        Lua::Engine::Call call(root);
        if (!call.pushChunk(code, environment)) {
            qCDebug(log_editing_actionscriptgeneral) << "Error parsing sequence value code:"
                                                     << call.errorDescription();
            call.clearError();

            {
                std::lock_guard<std::mutex> lock(buffer.mutex);
                if (buffer.state == Buffer::State::Active)
                    buffer.state = Buffer::State::Ended;
            }
            double time = buffer.finish(call, controller);

            std::lock_guard<std::mutex> lock(buffer.mutex);
            buffer.state = Buffer::State::Finished;
            buffer.advance = time;
            buffer.notify.notify_all();
            return;
        }
        if (!call.execute()) {
            engine.clearError();
            {
                std::lock_guard<std::mutex> lock(buffer.mutex);
                if (buffer.state == Buffer::State::Active)
                    buffer.state = Buffer::State::Ended;
            }
            double time = buffer.finish(call, controller);

            std::lock_guard<std::mutex> lock(buffer.mutex);
            buffer.state = Buffer::State::Finished;
            buffer.advance = time;
            buffer.notify.notify_all();
            return;
        }
    }

    double time = buffer.finish(root, controller);

    std::lock_guard<std::mutex> lock(buffer.mutex);
    buffer.state = Buffer::State::Finished;
    buffer.advance = time;
    buffer.notify.notify_all();
}

void ActionScriptSequenceValueGeneral::incomingSequenceValue(const SequenceValue &value)
{
    {
        std::lock_guard<std::mutex> lock(buffer.mutex);
        if (buffer.state == Buffer::State::Finished)
            return;
        buffer.incoming.emplace_back(value);
        buffer.advance = FP::undefined();
    }
    buffer.notify.notify_all();
}

void ActionScriptSequenceValueGeneral::incomingDataAdvance(double time)
{
    {
        std::unique_lock<std::mutex> lock(buffer.mutex);
        if (buffer.state == Buffer::State::Finished) {
            /* Keep advancing even if the script code finishes, since we don't have
             * a separate end condition */
            if (Range::compareStart(time, buffer.advance) <= 0)
                return;
            lock.unlock();
            outputAdvance(time);
            return;
        }
        buffer.advance = time;
    }
    buffer.notify.notify_all();
}

void ActionScriptSequenceValueGeneral::finalize()
{
    {
        std::lock_guard<std::mutex> lock(buffer.mutex);
        if (buffer.state == Buffer::State::Finished)
            return;
        buffer.state = Buffer::State::Ended;
    }

    buffer.notify.notify_all();

    {
        std::unique_lock<std::mutex> lock(buffer.mutex);
        buffer.notify.wait(lock, [this] { return buffer.state == Buffer::State::Finished; });
    }
}

void ActionScriptSequenceValueGeneral::initialize()
{
    {
        std::lock_guard<std::mutex> lock(buffer.mutex);
        if (buffer.state == Buffer::State::Initialize)
            buffer.state = Buffer::State::Active;
    }
    std::thread(std::bind(&ActionScriptSequenceValueGeneral::run, this)).detach();
}

void ActionScriptSequenceValueGeneral::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(buffer.mutex);
        if (buffer.state != Buffer::State::Active)
            return;
        buffer.state = Buffer::State::Ended;
    }
    buffer.notify.notify_all();
}

ActionScriptSequenceValueGeneral::Buffer::Buffer(ActionScriptSequenceValueGeneral &parent) : parent(
        parent), state(State::Initialize), advance(FP::undefined())
{ }

ActionScriptSequenceValueGeneral::Buffer::~Buffer() = default;

bool ActionScriptSequenceValueGeneral::Buffer::pushNext(Lua::Engine::Frame &target)
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        switch (state) {
        case State::Initialize:
            Q_ASSERT(false);
            return false;
        case State::Active:
            if (!incoming.empty()) {
                target.pushData<Lua::Libs::SequenceValue>(std::move(incoming.front()));
                incoming.pop_front();
                return true;
            }
            if (FP::defined(advance)) {
                target.push(advance);
                advance = FP::undefined();
                return true;
            }
            notify.wait(lock);
            break;
        case State::Ended:
            if (!incoming.empty()) {
                target.pushData<Lua::Libs::SequenceValue>(std::move(incoming.front()));
                incoming.pop_front();
                return true;
            }
            return false;
        case State::Finished:
            return false;
        }
    }
}

void ActionScriptSequenceValueGeneral::Buffer::outputReady(Lua::Engine::Frame &frame,
                                                           const Lua::Engine::Reference &ref)
{ parent.outputData(extract(frame, ref)); }

void ActionScriptSequenceValueGeneral::Buffer::advanceReady(double time)
{ parent.outputAdvance(time); }

void ActionScriptSequenceValueGeneral::Buffer::endReady()
{ }


}
}
