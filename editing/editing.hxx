/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITING_H
#define CPD3EDITING_H

#include <QtCore/QtGlobal>

#if defined(cpd3editing_EXPORTS)
#   define CPD3EDITING_EXPORT Q_DECL_EXPORT
#else
#   define CPD3EDITING_EXPORT Q_DECL_IMPORT
#endif

#endif
