/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGDIRECTIVE_H
#define CPD3EDITINGDIRECTIVE_H

#include "core/first.hxx"

#include <mutex>
#include <memory>
#include <vector>
#include <unordered_map>
#include <QtGlobal>

#include "editing/editing.hxx"
#include "editing/trigger.hxx"
#include "editing/action.hxx"
#include "datacore/stream.hxx"
#include "core/merge.hxx"

namespace CPD3 {
namespace Editing {

/**
 * A single directive in an editing chain.  This implements the internal
 * handling of the trigger and action.
 * <br>
 * This implements the dispatch and basic synchronization involved in the
 * dual fanouts for both the trigger and action.  That is, this has an
 * implementation that does the dispatch and fanout to copies of the trigger,
 * then recombines that output, and dispatches it again to the actions.  It
 * does NOT implement the implicit thread input/output synchronization that
 * some types of action may require.  That is, for any action that requires
 * a synchronized output the directive containing it MUST be the last one
 * that executes on the thread because it cannot handle being connected
 * to another skip dispatch.
 */
class CPD3EDITING_EXPORT EditDirective : public EditDispatchSkipTarget {
    EditDispatchTarget *output;

    double start;
    double end;

    std::unique_ptr<Trigger> trigger;
    std::unique_ptr<Action> action;
public:
    enum FanoutModeValues {
        /** Fanout different stations. */
                Fanout_Station = 0x01,

        /** Fanout different archives. */
                Fanout_Archive = 0x02,

        /** Fanout different variables. */
                Fanout_Variable = 0x04,

        /** Fanout different sets of flavors. */
                Fanout_Flavors = 0x08,

        /** Flatten statistics (cover, stats, end) flavors instead of fanning
         * them out. */
                Fanout_FlattenStatistics = 0x10,


        /** Flatten metadata into the base archive. */
                Fanout_FlattenMeta = 0x20,

        /** The default fanout actions. */
                Fanout_Default = Fanout_Station |
                Fanout_Archive |
                Fanout_Flavors |
                Fanout_FlattenStatistics |
                Fanout_FlattenMeta,

        /** No fanout at all */
                Fanout_None = 0,
    };
    Q_DECLARE_FLAGS(FanoutMode, FanoutModeValues)
private:
    typedef StreamMultiplexer<Data::SequenceValue> Multiplexer;

    FanoutMode triggerFanout;
    FanoutMode actionFanout;
    bool actionFragment;

    struct TriggerData {
        std::unique_ptr<Trigger> trigger;
        std::deque<Data::SequenceValue> pending;
        double pendingAdvance;

        Multiplexer::Simple *stream;
        Multiplexer::Simple *actionBypass;

        double advanceTime;

        TriggerData();
    };

    Data::SequenceName::Map<std::unique_ptr<TriggerData>> triggerData;

    struct TriggerDispatch {
        std::vector<EditDataTarget *> targets;
        bool unaffectedByAction;
        TriggerData *data;
    };
    Data::SequenceName::Map<TriggerDispatch> triggerDispatch;

    Multiplexer triggerMux;
    Multiplexer::Simple *triggerBypass;


    struct ActionData;

    class ActionOutput : public EditDispatchTarget, public EditDataTarget {
        EditDirective *parent;
        ActionData *data;
    public:
        Multiplexer::Simple *stream;

        ActionOutput(EditDirective *p, ActionData *d);

        virtual ~ActionOutput();

        void incomingData(const Data::SequenceValue &value) override;

        void incomingData(const Data::SequenceValue::Transfer &values) override;

        void incomingData(Data::SequenceValue &&value) override;

        void incomingData(Data::SequenceValue::Transfer &&values) override;

        void incomingSequenceValue(const Data::SequenceValue &value) override;

        void incomingAdvance(double time) override;

        void unlink();
    };

    class ActionOutputFragment : public ActionOutput {
        double start;
        double end;

        void modifySequenceValue(Data::SequenceValue &value);

    public:
        ActionOutputFragment(EditDirective *p, ActionData *d, double s, double e);

        virtual ~ActionOutputFragment();

        void incomingData(const Data::SequenceValue &value) override;

        void incomingData(const Data::SequenceValue::Transfer &values) override;

        void incomingData(Data::SequenceValue &&value) override;

        void incomingData(Data::SequenceValue::Transfer &&values) override;

        void incomingSequenceValue(const Data::SequenceValue &value) override;

        void incomingAdvance(double time) override;
    };

    friend class ActionOutput;

    struct ActionData {
        std::unique_ptr<Action> action;
        std::unique_ptr<ActionOutput> bypass;
        std::unique_ptr<ActionOutput> output;
        std::deque<Data::SequenceValue> afterFragments;
    };

    Data::SequenceName::Map<std::unique_ptr<ActionData>> actionData;

    struct ActionDispatch {
        std::vector<EditDataModification *> modifiers;
        std::vector<EditDataTarget *> targets;
        ActionData *data;
    };
    Data::SequenceName::Map<ActionDispatch> actionDispatch;

    std::unique_ptr<std::mutex> actionOutputMutex;
    Multiplexer actionMux;
    Multiplexer::Placeholder *actionPlaceholder;
    std::unordered_map<void *, Multiplexer::Simple *> skipTargets;

    TriggerDispatch &getTriggerTarget(const Data::SequenceName &name);

    void dispatchValueToTrigger(const Data::SequenceValue &value);

    void dispatchValueToTrigger(Data::SequenceValue &&value);

    void dispatchValueToAction(Data::SequenceValue &&value);

    void processPendingTriggerValues();

    void processTriggerValue(const Data::SequenceValue &value, TriggerData *data);

    void processTriggerValue(Data::SequenceValue &&value, TriggerData *data);

    void advanceTrigger(double time);

    void advanceAction(double time);

    void completeTrigger();

    void dispatchTriggerPassed();

    void completeAction();

    Multiplexer::Simple *lookupSkip(void *origin);

    static void flattenFanout(Data::SequenceName &unit, const FanoutMode &fanout);

public:

    /**
     * Create the edit directive.  The bounds define data affected by the edit.
     * 
     * @param start     the start of the directive
     * @param end       the end of the directive
     * @param trigger   the trigger of the directive
     * @param action    the action performed by the directive
     * @param fanoutTrigger the fanout mode applied to the trigger
     * @param fanoutAction  the fanout mode applied to the action
     * @param fragmentAction    fragment values going into the action to be only within the directive time
     */
    EditDirective(double start,
                  double end,
                  Trigger *trigger,
                  Action *action,
                  const FanoutMode &fanoutTrigger = Fanout_Default,
                  const FanoutMode &fanoutAction = Fanout_Default,
                  bool fragmentAction = false);

    virtual ~EditDirective();

    /**
     * Set the directive output.  This must be invoked before any data is
     * received by the directive.
     */
    inline void setOutput(EditDispatchTarget *output)
    { this->output = output; }

    /**
     * Get the start time of the directive.
     * 
     * @return the start time
     */
    inline double getStart() const
    { return start; }

    /**
     * Get the end time of the directive.
     * 
     * @return the end time
     */
    inline double getEnd() const
    { return end; }

    /**
     * Finalize the directive.  This ends the directive and outputs
     * any pending data.
     */
    void finalize();

    /**
     * Unlink the edit directive.  This removes all active processing from it
     * and switches it into a pure skip target.  This is normally used by
     * the dispatcher when the data has moved past the directive.
     */
    void unlink();

    /**
     * Terminate the edit directive processing.
     */
    void signalTerminate();


    void incomingData(const Data::SequenceValue &value) override;

    void incomingData(const Data::SequenceValue::Transfer &values) override;

    void incomingData(Data::SequenceValue &&value) override;

    void incomingData(Data::SequenceValue::Transfer &&values) override;

    void incomingAdvance(double time) override;

    void incomingSkipped(const Data::SequenceValue &value, void *origin) override;

    void incomingSkipped(const Data::SequenceValue::Transfer &values, void *origin) override;

    void incomingSkipped(Data::SequenceValue &&value, void *origin) override;

    void incomingSkipped(Data::SequenceValue::Transfer &&values, void *origin) override;

    void incomingSkipAdvance(double time, void *origin) override;

    void incomingSkipEnd(void *origin) override;

    /**
     * Test if the directive should run detached from the main processing chain.
     * That is, if this returns true then the trigger should cause the edit
     * to be processed in another thread.  This is primarily useful for
     * script evaluations that may take a long time to calculate.
     * <br>
     * The default implementation always returns false.
     * 
     * @return          true if the trigger should be detached
     */
    virtual bool shouldRunDetached() const;

    /**
     * Test if the directive requires synchronization.  This is required for any
     * actions that run on a separate thread (e.x. blocking script processing).
     * If this returns false then the output can be re-multiplexed without
     * threading synchronization.  That is, this must return true if the
     * action can produce output outside of while it's blocked in an
     * incoming value handler.
     * <br>
     * The default implementation always returns false.
     * 
     * @return      true if the output needs re-synchronization
     */
    virtual bool mustSynchronizeOutput() const;

    /**
    * Returns any additional inputs requested by the directive.  These may
    * not necessarily be available in the final input.  This is generally
    * unnecessary as most actions should only operate on existing data.
    *
    * @return  the requested inputs
    */
    virtual Data::SequenceName::Set requestedInputs() const;

    /**
     * Get any outputs generated by the directive.  This is generally
     * unnecessary as most actions should only operate on existing data.
     * 
     * @return  the output units
     */
    virtual Data::SequenceName::Set predictedOutputs() const;
};

}
}

Q_DECLARE_OPERATORS_FOR_FLAGS(CPD3::Editing::EditDirective::FanoutMode)

#endif
