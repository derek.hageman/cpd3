/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGTRIGGERAVERAGE_H
#define CPD3EDITINGTRIGGERAVERAGE_H

#include "core/first.hxx"

#include <QtGlobal>

#include "editing/editing.hxx"

#include "editing/triggermerge.hxx"
#include "editing/triggerbuffer.hxx"
#include "datacore/sequencematch.hxx"


namespace CPD3 {
namespace Editing {

/**
 * An input that handles full weighted averaging of the base input.
 */
class CPD3EDITING_EXPORT TriggerInputAverage : public TriggerInputMerger<
        Internal::InputMergedFixed<2>, TriggerInput>,
                                               private TriggerBuffer<Internal::InputMergedFixed<2>,
                                                                     TriggerInput::Result> {
    QVector<Result> result;

    void processPending();

public:
    /**
     * Create a average input.
     * 
     * @param input     the input value selection
     * @param config    the buffer configuration
     * @param path      the path with the values to read from
     */
    TriggerInputAverage(const Data::SequenceMatch::OrderedLookup &input,
                        const Data::Variant::Read &config = Data::Variant::Read::empty(),
                        const std::string &path = std::string());

    virtual ~TriggerInputAverage();

    virtual TriggerInput *clone() const;

    virtual QVector<Result> process();

    virtual void extendProcessing(double &start, double &end) const;

    virtual double advancedTime() const;

    virtual bool isReady(double end) const;

    virtual void incomingDataAdvance(double time);

    virtual void finalize();

protected:
    TriggerInputAverage(const TriggerInputAverage &other);

    virtual TriggerInput::Result convert(const QVector<Internal::InputMergedFixed<2> > &buffer,
                                         const Internal::InputMergedFixed<2> &active);
};

}
}
#endif
