/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGACTIONUNIT_H
#define CPD3EDITINGACTIONUNIT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QList>
#include <QRegExp>

#include "editing/editing.hxx"

#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "editing/action.hxx"
#include "datacore/sequencematch.hxx"

namespace CPD3 {
namespace Editing {

/**
 * An action that sets components of the incoming data to those of another.
 */
class CPD3EDITING_EXPORT ActionUnitSet : public Action, public EditDataTarget {
    Data::SequenceMatch::Composite selection;
    Data::SequenceName target;
    bool applyToMeta;
public:
    enum Component {
        /** Set the station. */
                Apply_Station = 0x1, /** Set the archive. */
                Apply_Archive = 0x2, /** Set the variable. */
                Apply_Variable = 0x4, /** Set the flavors. */
                Apply_Flavors = 0x8,

        /** Apply to all components. */
                Apply_All = Apply_Station | Apply_Archive | Apply_Variable | Apply_Flavors,

        /** Do not apply anything. */
                Apply_None = 0x0,
    };
    Q_DECLARE_FLAGS(Components, Component)
private:
    Components actions;

    class MetadataUpdate : public EditDataTarget {
        ActionUnitSet *parent;
    public:
        MetadataUpdate(ActionUnitSet *p);

        virtual ~MetadataUpdate();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class MetadataUpdate;

    MetadataUpdate metadataUpdate;
    Data::SequenceName metadataTarget;

    void incomingMetadata(const Data::SequenceValue &value);

public:
    /**
     * Create a new unit setting action.
     * 
     * @param selection     the selection to apply to
     * @param target        the target unit to set to
     * @param components    the components to change
     * @param applyToMeta   if set then also change metadata
     */
    ActionUnitSet(const Data::SequenceMatch::Composite &selection, const Data::SequenceName &target,
                  const Components &components = Apply_All,
                  bool applyToMeta = true);

    virtual ~ActionUnitSet();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

protected:
    ActionUnitSet(const ActionUnitSet &other);
};

/**
 * An action that duplicates the input data by changing the unit.
 */
class CPD3EDITING_EXPORT ActionUnitDuplicate : public ActionUnitSet {
    class Forwarder : public EditDataTarget {
        ActionUnitDuplicate *parent;
    public:
        Forwarder(ActionUnitDuplicate *p);

        virtual ~Forwarder();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class Forwarder;

    Forwarder forwarder;
public:
    /**
     * Create a new unit duplicating action.
     * 
     * @param selection     the selection to apply to
     * @param target        the target unit to set to
     * @param components    the components to change
     * @param applyToMeta   if set then also duplicate metadata
     */
    ActionUnitDuplicate(const Data::SequenceMatch::Composite &selection, const Data::SequenceName &target,
                        const ActionUnitSet::Components &components = ActionUnitSet::Apply_All,
                        bool applyToMeta = true);

    virtual ~ActionUnitDuplicate();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

protected:
    ActionUnitDuplicate(const ActionUnitDuplicate &other);
};


/**
 * An action that preforms regular expression replacement on the input units.
 */
class CPD3EDITING_EXPORT ActionUnitReplace : public Action, public EditDataTarget {
    Data::SequenceMatch::Composite selection;
    QRegExp inputStation;
    QString outputStation;
    QRegExp inputArchive;
    QString outputArchive;
    QRegExp inputVariable;
    QString outputVariable;
    QRegExp inputFlavor;
    QString outputFlavor;
    bool applyToMeta;

    class MetadataUpdate : public EditDataTarget {
        ActionUnitReplace *parent;
    public:
        MetadataUpdate(ActionUnitReplace *p);

        virtual ~MetadataUpdate();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class MetadataUpdate;

    MetadataUpdate metadataUpdate;

    void modifyUnit(Data::SequenceName &unit);

    void incomingMetadata(const Data::SequenceValue &value);

public:
    /**
     * Create a new unit setting action.  Even if the selection matches a unit
     * only the components that match are altered.  That is, if the selection
     * matches a unit but a component doesn't match, is isn't changed.  Each
     * flavor is matched independently.
     * <br>
     * The captures of the components are available as \1,\2, etc.
     * 
     * @param selection         the selection to apply to
     * @param inputStation      the input station matcher
     * @param outputStation     the output station     
     * @param inputArchive      the input archive matcher
     * @param outputArchive     the output archive
     * @param inputVariable     the input variable matcher
     * @param outputVariable    the output variable
     * @param inputFlavor       the input flavors matcher
     * @param outputFlavor      the output flavor
     * @param applyToMeta       if set then metadata is applied by changing to normal and back otherwise it goes through the normal path
     */
    ActionUnitReplace(const Data::SequenceMatch::Composite &selection,
                      const QRegExp &inputStation,
                      const QString &outputStation,
                      const QRegExp &inputArchive,
                      const QString &outputArchive,
                      const QRegExp &inputVariable,
                      const QString &outputVariable,
                      const QRegExp &inputFlavor,
                      const QString &outputFlavor,
                      bool applyToMeta = true);

    virtual ~ActionUnitReplace();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

protected:
    ActionUnitReplace(const ActionUnitReplace &other);
};

/**
 * An action that duplicates the input data and does replacement on the input.
 */
class CPD3EDITING_EXPORT ActionUnitReplaceDuplicate : public ActionUnitReplace {
    class Forwarder : public EditDataTarget {
        ActionUnitReplaceDuplicate *parent;
    public:
        Forwarder(ActionUnitReplaceDuplicate *p);

        virtual ~Forwarder();

        virtual void incomingSequenceValue(const Data::SequenceValue &value);
    };

    friend class Forwarder;

    Forwarder forwarder;
public:
    /**
     * Create a new unit duplicating action.  Even if the selection matches a 
     * unit only the components that match are altered.  That is, if the 
     * selection matches a unit but a component doesn't match, is isn't 
     * changed.  Each flavor is matched independently.
     * <br>
     * The captures of the components are available as \1,\2, etc.
     * 
     * @param selection         the selection to apply to
     * @param inputStation      the input station matcher
     * @param outputStation     the output station     
     * @param inputArchive      the input archive matcher
     * @param outputArchive     the output archive
     * @param inputVariable     the input variable matcher
     * @param outputVariable    the output variable
     * @param inputFlavor       the input flavors matcher
     * @param outputFlavor      the output flavor
     * @param applyToMeta       if set then metadata is applied by changing to normal and back otherwise it goes through the normal path
     */
    ActionUnitReplaceDuplicate(const Data::SequenceMatch::Composite &selection,
                               const QRegExp &inputStation,
                               const QString &outputStation,
                               const QRegExp &inputArchive,
                               const QString &outputArchive,
                               const QRegExp &inputVariable,
                               const QString &outputVariable,
                               const QRegExp &inputFlavor,
                               const QString &outputFlavor,
                               bool applyToMeta = true);

    virtual ~ActionUnitReplaceDuplicate();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

protected:
    ActionUnitReplaceDuplicate(const ActionUnitReplaceDuplicate &other);
};


/**
 * An action that modifies flavors of the matched units.  This adds and/or
 * removes flavors from the data.
 */
class CPD3EDITING_EXPORT ActionModifyFlavors : public Action, public EditDataTarget {
    Data::SequenceMatch::Composite selection;
    Data::SequenceName::Flavors addFlavors;
    Data::SequenceName::Flavors removeFlavors;

    bool applyToMeta;
public:
    /**
     * Create a new flavor modifying action.
     * 
     * @param selection     the selection to apply to
     * @param addFlavors    the flavors to add
     * @param removeFlavors the flavors to remove
     * @param applyToMeta   if set then metadata is modified if it also matches the selection, otherwise it is ignored
     */
    ActionModifyFlavors(const Data::SequenceMatch::Composite &selection,
                        const Data::SequenceName::Flavors &addFlavors,
                        const Data::SequenceName::Flavors &removeFlavors,
                        bool applyToMeta = false);

    virtual ~ActionModifyFlavors();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

protected:
    ActionModifyFlavors(const ActionModifyFlavors &other);
};

}
}

Q_DECLARE_OPERATORS_FOR_FLAGS(CPD3::Editing::ActionUnitSet::Components)

#endif
