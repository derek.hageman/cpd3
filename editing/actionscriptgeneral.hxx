/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGACTIONSCRIPTGENERAL_H
#define CPD3EDITINGACTIONSCRIPTGENERAL_H

#include "core/first.hxx"

#include <mutex>
#include <deque>
#include <condition_variable>
#include <QtGlobal>
#include <QList>
#include <QThread>

#include "editing/editing.hxx"

#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "editing/action.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/segment.hxx"
#include "luascript/engine.hxx"
#include "luascript/streambuffer.hxx"

namespace CPD3 {
namespace Editing {

/**
 * An editing action that uses a general script interface to a multisegement
 * processing object.
 */
class CPD3EDITING_EXPORT ActionScriptSequenceSegmentGeneral : public Action, public EditDataTarget {
    Data::SequenceMatch::OrderedLookup outputs;
    Data::SequenceMatch::OrderedLookup inputs;
    std::string code;
    bool implicitMeta;

    Data::SequenceMatch::OrderedLookup registeredOutputs;

    class Buffer : public Lua::StreamBuffer::Segment {
        ActionScriptSequenceSegmentGeneral &parent;
    public:
        enum class State {
            Initialize, Active, Ended, Finished,
        };
        State state;
        double advance;
        std::deque<Data::SequenceSegment> incoming;
        std::mutex mutex;
        std::condition_variable notify;

        Buffer(ActionScriptSequenceSegmentGeneral &parent);

        virtual ~Buffer();

    protected:
        bool pushNext(Lua::Engine::Frame &target) override;

        void outputReady(Lua::Engine::Frame &frame, const Lua::Engine::Reference &ref) override;

        void advanceReady(double time) override;

        void endReady() override;
    };

    friend class Buffer;

    Buffer buffer;

    Data::SequenceSegment::Stream reader;

    void run();

public:
    /**
     * Create a new processing action.
     *
     * @param outputs       the output selection
     * @param inputs        the input only selection
     * @param code          the script code to execute
     * @param implicitMeta  include metadata as an input implicitly
     */
    ActionScriptSequenceSegmentGeneral(Data::SequenceMatch::OrderedLookup outputs,
                                       Data::SequenceMatch::OrderedLookup inputs,
                                       std::string code,
                                       bool implicitMeta = true);

    virtual ~ActionScriptSequenceSegmentGeneral();

    void unhandledInput(const Data::SequenceName &name,
                        QList<EditDataTarget *> &inputs,
                        QList<EditDataTarget *> &outputs,
                        QList<EditDataModification *> &modifiers) override;

    void incomingSequenceValue(const Data::SequenceValue &value) override;

    void incomingDataAdvance(double time) override;

    void finalize() override;

    bool mustSynchronizeOutput() const override;

    void initialize() override;

    void signalTerminate() override;

    Data::SequenceName::Set requestedInputs() const override;

    Data::SequenceName::Set predictedOutputs() const override;

    Action *clone() const override;

protected:
    ActionScriptSequenceSegmentGeneral(const ActionScriptSequenceSegmentGeneral &other);
};

/**
 * An editing action that uses a general script interface to a value
 * processing object.
 */
class CPD3EDITING_EXPORT ActionScriptSequenceValueGeneral : public Action, public EditDataTarget {
    Data::SequenceMatch::OrderedLookup selection;
    std::string code;

    class Buffer : public Lua::StreamBuffer::Value {
        ActionScriptSequenceValueGeneral &parent;
    public:
        enum class State {
            Initialize, Active, Ended, Finished,
        };
        State state;
        double advance;
        std::deque<Data::SequenceValue> incoming;
        std::mutex mutex;
        std::condition_variable notify;

        Buffer(ActionScriptSequenceValueGeneral &parent);

        virtual ~Buffer();

    protected:
        bool pushNext(Lua::Engine::Frame &target) override;

        void outputReady(Lua::Engine::Frame &frame, const Lua::Engine::Reference &ref) override;

        void advanceReady(double time) override;

        void endReady() override;
    };

    friend class Buffer;

    Buffer buffer;

    void run();

public:
    /**
     * Create a new processing action.
     *
     * @param selection     the input selection to the processor
     * @param code          the script code to execute
     */
    ActionScriptSequenceValueGeneral(Data::SequenceMatch::OrderedLookup selection,
                                     std::string code);

    virtual ~ActionScriptSequenceValueGeneral();

    void unhandledInput(const Data::SequenceName &name,
                        QList<EditDataTarget *> &inputs,
                        QList<EditDataTarget *> &outputs,
                        QList<EditDataModification *> &modifiers) override;

    void incomingSequenceValue(const Data::SequenceValue &value) override;

    void incomingDataAdvance(double time) override;

    void finalize() override;

    bool mustSynchronizeOutput() const override;

    void initialize() override;

    void signalTerminate() override;

    Data::SequenceName::Set requestedInputs() const override;

    Action *clone() const override;

protected:
    ActionScriptSequenceValueGeneral(const ActionScriptSequenceValueGeneral &other);
};


}
}

#endif
