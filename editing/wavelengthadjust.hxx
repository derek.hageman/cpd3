/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGWAVELENGTHADJUST_H
#define CPD3EDITINGWAVELENGTHADJUST_H

#include "core/first.hxx"

#include <vector>
#include <map>
#include <memory>
#include <QtGlobal>
#include <QList>
#include <QPair>
#include <QSet>
#include <QDataStream>
#include <QVector>

#include "editing/editing.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/wavelength.hxx"
#include "smoothing/baseline.hxx"

namespace CPD3 {
namespace Editing {

class WavelengthAdjust;

CPD3EDITING_EXPORT QDataStream &operator<<(QDataStream &stream, const WavelengthAdjust *adj);

CPD3EDITING_EXPORT QDataStream &operator>>(QDataStream &stream, WavelengthAdjust *&adj);

/**
 * An interface to adjust data to different wavelengths.
 */
class CPD3EDITING_EXPORT WavelengthAdjust {
    class Input {
    public:
        double wavelength;
        std::vector<std::unique_ptr<CPD3::Smoothing::BaselineSmoother>> smoothers;
        bool isSimple;
        CPD3::Data::Variant::PathElement::MatrixIndex matrixShape;
        double validDistance;
        std::unique_ptr<CPD3::Data::DynamicDouble> validDistanceInput;

        Input();

        Input(const Input &other);

        virtual ~Input();

        bool isValid(double forWavelength, double adjustDistance) const;
    };

    class InputDynamic : public Input {
    public:
        CPD3::Data::SequenceName unit;

        InputDynamic(const CPD3::Data::SequenceName &u);

        InputDynamic(const InputDynamic &other);

        virtual ~InputDynamic();

        void reshapeArray(std::size_t newSize, CPD3::Smoothing::BaselineSmoother *baseSmoother);

        void reshapeMatrix(const CPD3::Data::Variant::PathElement::MatrixIndex &sz,
                           CPD3::Smoothing::BaselineSmoother *baseSmoother);

        void reshapeSimple();
    };

    class InputFixed : public Input {
    public:
        std::unique_ptr<CPD3::Data::DynamicInput> input;
        CPD3::Data::SequenceName::Set usedUnits;
        std::unique_ptr<CPD3::Data::DynamicDouble> fixedWavelengthInput;

        InputFixed();

        InputFixed(const InputFixed &other);

        virtual ~InputFixed();
    };

    enum AngstromSearchMode {
        AngstromSearch_General, AngstromSearch_Fallback,
    };

    struct AngstromInput {
        double wavelength;
        std::unique_ptr<CPD3::Smoothing::BaselineSmoother> smoother;
        std::unique_ptr<CPD3::Data::DynamicInput> input;
        CPD3::Data::SequenceName::Set usedUnits;
        double validDistance;
        std::unique_ptr<CPD3::Data::DynamicDouble> fixedWavelengthInput;
        std::unique_ptr<CPD3::Data::DynamicDouble> validDistanceInput;
        bool isFallback;

        AngstromInput();

        AngstromInput(const AngstromInput &other);

        virtual ~AngstromInput();

        bool isValid(double forWavelength, double angstromDistance, AngstromSearchMode mode) const;
    };


    std::multimap<double, Input *> inputLookup;
    std::vector<std::unique_ptr<InputFixed>> inputFixed;
    CPD3::Data::SequenceName::Map<std::unique_ptr<InputDynamic>> inputDynamic;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> dynamicOperate;
    std::unique_ptr<CPD3::Smoothing::BaselineSmoother> dynamicSmoother;
    std::unique_ptr<CPD3::Data::DynamicDouble> dynamicValidDistance;

    std::multimap<double, AngstromInput *> angstromLookup;
    std::vector<std::unique_ptr<AngstromInput>> inputAngstrom;

    AngstromInput *angstromForWavelength
            (double wavelength, double angstromDistance, AngstromSearchMode mode) const;

    Input *inputNearestWavelength(double wavelength, double adjustDistance) const;

    InputDynamic *lookupDynamic(const CPD3::Data::SequenceName &unit);

    static CPD3::Data::Variant::Root
            adjustFromSingle(double target, double origin, const Input *input, double angstrom);

    struct ConvertAngstrom {
        double wavelength;
        double angstromDistance;
        AngstromSearchMode mode;

        ConvertAngstrom(double wl, double ad, AngstromSearchMode m);

        bool isValid(const AngstromInput *input) const;

        bool outsidePossible(const AngstromInput *input, double target) const;

        inline AngstromInput *invalid() const
        { return NULL; }

        AngstromInput *convert(AngstromInput *input, double target) const;
    };

    struct CalculateAngstrom {
        bool isValid(const Input *input) const;

        inline CPD3::Data::Variant::Read invalid() const
        { return CPD3::Data::Variant::Read::empty(); }

        CPD3::Data::Variant::Read convert(const Input *lower,
                                          const Input *upper,
                                          double target) const;
    };

    struct ReturnNearest {
        double wavelength;
        double adjustDistance;

        ReturnNearest(double wl, double ad);

        bool isValid(const Input *input) const;

        inline Input *invalid() const
        { return NULL; }

        inline bool outsidePossible(const Input *input, double target) const
        {
            Q_UNUSED(input);
            Q_UNUSED(target);
            return false;
        }

        inline Input *convert(Input *input, double target) const
        {
            Q_UNUSED(target);
            return input;
        }
    };

    struct Interpolate {
        double wavelength;
        double adjustDistance;

        Interpolate(double wl, double ad);

        bool isValid(const Input *input) const;

        inline CPD3::Data::Variant::Read invalid() const
        { return CPD3::Data::Variant::Root(FP::undefined()).read(); }

        CPD3::Data::Variant::Read convert(const Input *lower,
                                          const Input *upper,
                                          double target) const;
    };

    void rebuildLookups();

    WavelengthAdjust();

    void configure(const CPD3::Data::ValueSegment::Transfer &config,
                   double start,
                   double end, const CPD3::Data::Variant::Read &defaultConfig);

public:
    WavelengthAdjust(const WavelengthAdjust &other);

    WavelengthAdjust &operator=(const WavelengthAdjust &other);

    /**
     * Construct a wavelength adjuster that used the given input and the
     * angstrom/linear interpolation of those inputs to the output wavelength.
     * <br>
     * This takes ownership of the input specification and smoother
     * 
     * @param input     the input specification
     * @param smoother  the smoother applied to all inputs or NULL for none
     */
    static WavelengthAdjust *fromInput(CPD3::Data::DynamicSequenceSelection *input,
                                       CPD3::Smoothing::BaselineSmoother *smoother = nullptr);

    /**
     * Construct a wavelength adjuster from the given configuration.
     * 
     * @param config    the configuration
     * @param unit      the unit within the configuration
     * @param path      the path within the configuration
     * @param start     the effective start time
     * @param end       the effective end time
     * @param defaultConfig the default configuration when none exists
     */
    static WavelengthAdjust *fromConfiguration(CPD3::Data::SequenceSegment::Transfer &config,
                                               const CPD3::Data::SequenceName &unit,
                                               const QString &path = QString(),
                                               double start = FP::undefined(),
                                               double end = FP::undefined(),
                                               const CPD3::Data::Variant::Read &defaultConfig = CPD3::Data::Variant::Read::empty());

    /**
     * Construct a wavelength adjuster from the given configuration.
     * 
     * @param config    the configuration
     * @param unit      the unit within the configuration
     * @param path      the path within the configuration
     * @param start     the effective start time
     * @param end       the effective end time
     * @param defaultConfig the default configuration when none exists
     */
    static WavelengthAdjust *fromConfiguration(const CPD3::Data::ValueSegment::Transfer &config,
                                               const QString &path = QString(),
                                               double start = FP::undefined(),
                                               double end = FP::undefined(),
                                               const CPD3::Data::Variant::Read &defaultConfig = CPD3::Data::Variant::Read::empty());

    ~WavelengthAdjust();

    /**
     * Add a fallback angstrom exponent input.
     * <br>
     * This takes ownership of all parameters.
     * 
     * @param angstromFallback  the angstrom exponent to use
     * @param wavelength        the wavelength it's valid at
     * @param validDistance     the maximum wavelength distance it's valid for
     * @param smoother          the smoother applied
     */
    void addFallbackAngstrom(CPD3::Data::DynamicInput *angstromFallback,
                             CPD3::Data::DynamicDouble *wavelength = nullptr,
                             CPD3::Data::DynamicDouble *validDistance = nullptr,
                             CPD3::Smoothing::BaselineSmoother *smoother = nullptr);

    /**
     * Update the adjuster from some input data.
     * 
     * @param data      the input data
     */
    void incoming(CPD3::Data::SequenceSegment &data);

    /**
     * Update the adjuster from some input metadata.
     * 
     * @param data      the input metadata
     */
    void incomingMeta(CPD3::Data::SequenceSegment &data);

    /**
     * Get the wavelength of an input.
     * 
     * @return the input wavelength
     */
    double getWavelength(const CPD3::Data::SequenceName &unit) const;

    /**
     * Get the angstrom exponent at a given wavelength.
     * 
     * @param wavelength    the wavelength target
     * @param bracketing    the bracketing behavior if this is an exact match
     * @param angstromDistance  the maximum distance to use for fixed angstrom exponents
     * @param excludeFallback   don't include fallbacks in the output
     */
    CPD3::Data::Variant::Read getAngstrom(double wavelength,
                                          CPD3::Data::Wavelength::BracketingExactBehavior bracketing = CPD3::Data::Wavelength::BracketingExactBehavior::ExcludeExact,
                                          double angstromDistance = FP::undefined(),
                                          bool excludeFallback = false) const;

    /**
     * Get the angstrom exponent for a given input.
     * 
     * @param unit          the input unit
     * @param bracketing    the bracketing behavior if this is an exact match
     * @param angstromDistance  the maximum distance to use for fixed angstrom exponents
     * @param excludeFallback   don't include fallbacks in the output
     */
    CPD3::Data::Variant::Read getAngstrom(const CPD3::Data::SequenceName &unit,
                                          CPD3::Data::Wavelength::BracketingExactBehavior bracketing = CPD3::Data::Wavelength::BracketingExactBehavior::ExcludeExact,
                                          double angstromDistance = FP::undefined(),
                                          bool excludeFallback = false) const;

    /**
     * Get the adjusted value at a given wavelength.
     * 
     * @param wavelength    the wavelength target
     * @param angstromDistance  the maximum distance to use for fixed angstrom exponents
     * @param adjustDistance    the maximum distance to adjust wavelengths from their origin
     * @return              the adjusted value
     */
    CPD3::Data::Variant::Read getAdjusted(double wavelength,
                                          double angstromDistance = FP::undefined(),
                                          double adjustDistance = FP::undefined()) const;

    /**
     * Register an input for this list of variables.  Returns true if it could
     * be used.
     * 
     * @param input     the input unit to test
     * @param adjust    if not NULL then set to true if this is an adjustable input
     * @return          true if the input is used
     */
    bool registerInput(const CPD3::Data::SequenceName &input, bool *adjust = nullptr);

    /**
     * Register a set of expected matched parameters.  This allows the selection
     * to determine the used inputs before they actually appear in the data
     * stream.  If any component is empty is is treated as unknown and required
     * to be exact before a component can be used.  That is specifying a
     * station and archive will allow all inputs that specify exactly a single
     * variable to be known.  The flavors are considered unknown.
     * 
     * @param station   the station to expect or empty for unknown
     * @param archive   the archive to expect or empty for unknown
     * @param variable  the variable to expect or empty for unknown
     */
    void registerExpectedFlavorless(const Data::SequenceName::Component &station = {},
                                    const Data::SequenceName::Component &archive = {},
                                    const Data::SequenceName::Component &variable = {});

    /**
     * The same as registerExpectedFlavorless( const QString &, const QString &, 
     * const QString & ) except that the flavors are known.
     * 
     * @param station       the station to expect or empty for unknown
     * @param archive       the archive to expect or empty for unknown
     * @param variable      the variable to expect or empty for unknown
     * @param flavors       the flavors to expect
     * @see registerExpectedFlavorless( const QString &, const QString &, const QString & )
     */
    void registerExpected(const Data::SequenceName::Component &station,
                          const Data::SequenceName::Component &archive,
                          const Data::SequenceName::Component &variable,
                          const Data::SequenceName::Flavors &flavors);

    /**
     * The same as registerExpected( const QString &, const QString &, 
     * const QString &, const QSet<QString> & ) except that the flavors are 
     * known only as a set of possibilities
     * 
     * @param station       the station to expect or empty for unknown
     * @param archive       the archive to expect or empty for unknown
     * @param variable      the variable to expect or empty for unknown
     * @param flavors       the sets of flavors to expect
     * @see registerExpectedFlavorless( const QString &, const QString &, const QString & )
     */
    void registerExpected(const Data::SequenceName::Component &station = {},
                          const Data::SequenceName::Component &archive = {},
                          const Data::SequenceName::Component &variable = {},
                          const std::unordered_set<
                                  Data::SequenceName::Flavors> &flavors = CPD3::Data::SequenceName::defaultDataFlavors());

    /**
     * Get all known input values.
     * 
     * @return  a list of known inputs that this input uses
     */
    CPD3::Data::SequenceName::Set getAllUnits() const;

    /**
     * Get all known times when the "structure" of this operation might change.
     * Generally this should include times when the actual variables being
     * operated on would be changed.
     * 
     * @return  a list of known changed times
     */
    QSet<double> getChangedPoints() const;

    /**
     * Get the start time needed to fully spin up the wavelength adjuster
     * given an initial time.
     * 
     * @param initial   the initial start time
     * @return          the time to begin so that the adjuster is running by the initial time
     */
    double spinupTime(double initial) const;

    friend CPD3EDITING_EXPORT QDataStream
            &operator<<(QDataStream &stream, const WavelengthAdjust *adj);

    friend CPD3EDITING_EXPORT QDataStream &operator>>(QDataStream &stream, WavelengthAdjust *&adj);

};

class WavelengthTracker;

CPD3EDITING_EXPORT QDataStream &operator<<(QDataStream &stream, const WavelengthTracker &track);

CPD3EDITING_EXPORT QDataStream &operator>>(QDataStream &stream, WavelengthTracker &track);

/**
 * A simple interface for tracking wavelengths in incoming data.  This is
 * normally paired with a basic filter.
 */
class CPD3EDITING_EXPORT WavelengthTracker {
    CPD3::Data::SequenceName::Map<double> wavelengths;
public:
    WavelengthTracker();

    /**
     * Update the tracker from some input metadata.
     * 
     * @param data      the input metadata
     * @param operate   the units the operate matcher has selected
     */
    void incomingMeta(CPD3::Data::SequenceSegment &data,
                      const CPD3::Data::SequenceName::Set &operate);

    /**
     * Update the tracker with a single input of metadata.
     * 
     * @param data      the input metadata
     * @param variable  the variable to update
     */
    void incomingMeta(const CPD3::Data::Variant::Read &data,
                      const CPD3::Data::SequenceName &variable);

    /**
     * Get the wavelength of a tracked unit.
     * 
     * @param unit      the unit to fetch
     */
    double get(const CPD3::Data::SequenceName &unit) const;

    friend CPD3EDITING_EXPORT QDataStream
            &operator<<(QDataStream &stream, const WavelengthTracker &track);

    friend CPD3EDITING_EXPORT QDataStream
            &operator>>(QDataStream &stream, WavelengthTracker &track);
};

}
}

#endif
