/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGACTIONSCRIPTPROCESSOR_H
#define CPD3EDITINGACTIONSCRIPTPROCESSOR_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QList>
#include <QVector>

#include "editing/editing.hxx"

#include "core/number.hxx"
#include "core/merge.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "editing/action.hxx"
#include "luascript/engine.hxx"
#include "luascript/streambuffer.hxx"
#include "luascript/fanoutcontroller.hxx"

namespace CPD3 {
namespace Editing {

/**
 * An editing action that calls into a general value processor to handle
 * the execution.
 */
class CPD3EDITING_EXPORT ActionScriptSequenceValueProcessor : public Action, public EditDataTarget {
    Data::SequenceMatch::OrderedLookup selection;
    std::string code;

    class Buffer : public Lua::StreamBuffer::Value {
        ActionScriptSequenceValueProcessor &parent;
    public:
        enum class State {
            Empty, Advance, Value
        };
        State state;
        double advance;
        Data::SequenceValue next;

        Buffer(ActionScriptSequenceValueProcessor &parent);

        virtual ~Buffer();

    protected:
        bool pushNext(Lua::Engine::Frame &target) override;

        void outputReady(Lua::Engine::Frame &frame, const Lua::Engine::Reference &ref) override;

        void advanceReady(double time) override;

        void endReady() override;
    };

    friend class Buffer;

    Lua::Engine engine;
    Lua::Engine::Frame root;
    Buffer buffer;

    Lua::Engine::Reference controller;
    Lua::Engine::Table environment;
    Lua::Engine::Reference invoke;

    void init();

    void process();

public:
    /**
     * Create a new processing action.
     * 
     * @param selection     the input selection to the processor
     * @param code          the script code to execute
     */
    ActionScriptSequenceValueProcessor(Data::SequenceMatch::OrderedLookup selection,
                                       std::string code);

    virtual ~ActionScriptSequenceValueProcessor();

    void unhandledInput(const Data::SequenceName &name,
                        QList<EditDataTarget *> &inputs,
                        QList<EditDataTarget *> &outputs,
                        QList<EditDataModification *> &modifiers) override;

    void incomingSequenceValue(const Data::SequenceValue &value) override;

    void incomingDataAdvance(double time) override;

    void finalize() override;

    bool shouldRunDetached() const override;

    Data::SequenceName::Set requestedInputs() const override;

    Action *clone() const override;

protected:
    ActionScriptSequenceValueProcessor(const ActionScriptSequenceValueProcessor &other);
};

/**
 * An editing action that calls into a multisegment processor to handle
 * the execution.
 */
class CPD3EDITING_EXPORT ActionScriptSequenceSegmentProcessor
        : public Action, public EditDataTarget {
    Data::SequenceMatch::OrderedLookup outputs;
    Data::SequenceMatch::OrderedLookup inputs;
    std::string code;
    bool implicitMeta;

    Data::SequenceMatch::OrderedLookup registeredOutputs;

    class Buffer : public Lua::StreamBuffer::Segment {
        ActionScriptSequenceSegmentProcessor &parent;
        std::size_t offset;
    public:
        double advance;
        Data::SequenceSegment::Transfer pending;

        Buffer(ActionScriptSequenceSegmentProcessor &parent);

        virtual ~Buffer();

    protected:
        bool pushNext(Lua::Engine::Frame &target) override;

        void outputReady(Lua::Engine::Frame &frame, const Lua::Engine::Reference &ref) override;

        void advanceReady(double time) override;

        void endReady() override;
    };

    friend class Buffer;

    Lua::Engine engine;
    Lua::Engine::Frame root;
    Buffer buffer;

    Lua::Engine::Reference controller;
    Lua::Engine::Table environment;
    Lua::Engine::Reference invoke;

    Data::SequenceSegment::Stream reader;

    void init();

    void process(bool ignoreEnd = true);

public:
    /**
     * Create a new processing action.
     * 
     * @param outputs       the output selection
     * @param inputs        the input only selection
     * @param code          the script code to execute
     * @param implicitMeta  include metadata as an input implicitly
     */
    ActionScriptSequenceSegmentProcessor(Data::SequenceMatch::OrderedLookup outputs,
                                         Data::SequenceMatch::OrderedLookup inputs,
                                         std::string code,
                                         bool implicitMeta = true);

    virtual ~ActionScriptSequenceSegmentProcessor();

    void unhandledInput(const Data::SequenceName &name,
                        QList<EditDataTarget *> &inputs,
                        QList<EditDataTarget *> &outputs,
                        QList<EditDataModification *> &modifiers) override;

    void incomingSequenceValue(const Data::SequenceValue &value) override;

    void incomingDataAdvance(double time) override;

    void finalize() override;

    bool shouldRunDetached() const override;

    Data::SequenceName::Set requestedInputs() const override;

    Data::SequenceName::Set predictedOutputs() const override;

    Action *clone() const override;

protected:
    ActionScriptSequenceSegmentProcessor(const ActionScriptSequenceSegmentProcessor &other);
};


/**
 * An editing action that uses a script data fanout controller as the data
 * interface.
 */
class CPD3EDITING_EXPORT ActionScriptFanoutProcessor : public Action, public EditDataTarget {
    std::string code;
    bool processMetadata;

    typedef StreamMultiplexer<Data::SequenceValue> Multiplexer;

    class BaseTarget : public Lua::FanoutController::Target {
    public:
        BaseTarget();

        virtual ~BaseTarget();

        virtual bool incomingSequenceValue(const Data::SequenceValue &value) = 0;

        virtual bool incomingDataAdvance(double time);

        virtual void finalize();
    };

    class ForegroundTarget : public BaseTarget {
        class Buffer : public Lua::StreamBuffer::Segment {
            ForegroundTarget &parent;
            std::size_t offset;
        public:
            double advance;
            Data::SequenceSegment::Transfer pending;

            Buffer(ForegroundTarget &parent);

            virtual ~Buffer();

        protected:
            bool pushNext(Lua::Engine::Frame &target) override;

            void outputReady(Lua::Engine::Frame &frame, const Lua::Engine::Reference &ref) override;

            void advanceReady(double time) override;

            void endReady() override;
        };

        friend class Buffer;

        ActionScriptFanoutProcessor &parent;
        Buffer buffer;
        Multiplexer::Simple *sink;
        bool sinkReady;

        Data::SequenceName::Set outputs;
        Data::SequenceSegment::Stream reader;

        void process(bool ignoreEnd = true);

    public:
        ForegroundTarget(ActionScriptFanoutProcessor &parent);

        virtual ~ForegroundTarget();

        bool incomingSequenceValue(const Data::SequenceValue &value) override;

        bool incomingDataAdvance(double time) override;

        void finalize() override;

        void error();

        void addedDispatch(Lua::Engine::Frame &frame,
                           const Lua::Engine::Reference &controller,
                           const Data::SequenceName &name);

    protected:
        bool initializeCall(Lua::Engine::Frame &arguments,
                            const Lua::Engine::Reference &controller,
                            const Lua::Engine::Reference &key,
                            const std::vector<std::shared_ptr<Target>> &background,
                            Lua::Engine::Table &context) override;

        void processSaved(Lua::Engine::Frame &saved,
                          const Lua::Engine::Reference &controller,
                          const Lua::Engine::Reference &key,
                          const std::vector<std::shared_ptr<Target>> &background,
                          Lua::Engine::Table &context) override;
    };

    friend class ForegroundTarget;

    class BackgroundTarget : public BaseTarget {
        Data::SequenceSegment::Stream reader;

        friend class ActionScriptFanoutProcessor::ForegroundTarget;

    public:
        BackgroundTarget();

        virtual ~BackgroundTarget();

        bool incomingSequenceValue(const Data::SequenceValue &value) override;

    protected:
        bool initializeCall(Lua::Engine::Frame &arguments,
                            const Lua::Engine::Reference &controller,
                            const Lua::Engine::Reference &key,
                            const std::vector<std::shared_ptr<Target>> &background,
                            Lua::Engine::Table &context) override;
    };

    class BypassTarget : public BaseTarget {
        Multiplexer::Simple *output;
    public:
        BypassTarget(ActionScriptFanoutProcessor &parent);

        virtual ~BypassTarget();

        bool incomingSequenceValue(const Data::SequenceValue &value) override;
    };

    friend class BypassTarget;

    class Fanout : public Lua::FanoutController {
        ActionScriptFanoutProcessor &parent;
    public:
        Fanout(ActionScriptFanoutProcessor &parent);

        virtual ~Fanout();

    protected:
        std::shared_ptr<Target> createTarget(const Lua::Engine::Output &type) override;

        void addedDispatch(Lua::Engine::Frame &frame,
                           const Lua::Engine::Reference &controller,
                           const Data::SequenceName &name,
                           std::vector<std::shared_ptr<Target>> &targets) override;
    };

    Lua::Engine engine;
    Lua::Engine::Frame root;
    Fanout fanout;

    Multiplexer mux;
    Multiplexer::Simple *bypass;
    bool inError;

    Lua::Engine::Reference controller;

    void init();

    void error();

public:
    /**
     * Create a new script demultiplexer action.
     *
     * @param code              the script code to execute
     * @param processMetadata   also process metadata
     */
    ActionScriptFanoutProcessor(std::string code, bool processMetadata = false);

    virtual ~ActionScriptFanoutProcessor();

    void unhandledInput(const Data::SequenceName &name,
                        QList<EditDataTarget *> &inputs,
                        QList<EditDataTarget *> &outputs,
                        QList<EditDataModification *> &modifiers) override;

    void incomingSequenceValue(const Data::SequenceValue &value) override;

    void incomingDataAdvance(double time) override;

    void finalize() override;

    bool shouldRunDetached() const override;

    Action *clone() const override;

protected:
    ActionScriptFanoutProcessor(const ActionScriptFanoutProcessor &other);
};

}
}

#endif
