/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/triggerperiodic.hxx"
#include "core/range.hxx"

namespace CPD3 {
namespace Editing {

/** @file editing/triggerperiodic.hxx
 * Triggers based on periodic enabling (e.x. specific minutes within the
 * hour).
 */

TriggerPeriodic::TriggerPeriodic(Time::LogicalTimeUnit unit, int count, bool aligned)
        : intervalUnit(unit),
          intervalCount(count),
          intervalAligned(aligned),
          intervalStartTime(FP::undefined()),
          intervalEndTime(FP::undefined())
{
    Q_ASSERT(count > 0);
}

TriggerPeriodic::~TriggerPeriodic()
{ }

TriggerPeriodic::TriggerPeriodic(const TriggerPeriodic &other) : Trigger(other),
                                                                 intervalUnit(other.intervalUnit),
                                                                 intervalCount(other.intervalCount),
                                                                 intervalAligned(
                                                                         other.intervalAligned),
                                                                 intervalStartTime(FP::undefined()),
                                                                 intervalEndTime(FP::undefined())
{ }

bool TriggerPeriodic::isReady(double end) const
{
    Q_UNUSED(end);
    return true;
}

void TriggerPeriodic::setInterval(double time, double &start, double &end) const
{
    start = Time::floor(time, intervalUnit, intervalCount);
    Q_ASSERT(FP::defined(start));
    end = Time::logical(start, intervalUnit, intervalCount, intervalAligned, true);
    Q_ASSERT(FP::defined(end));
    if (start >= end)
        end = start + 1;
}

bool TriggerPeriodic::updateInterval(double time)
{
    if (!FP::defined(time))
        return false;
    if (!FP::defined(intervalStartTime)) {
        setInterval(time, intervalStartTime, intervalEndTime);
        return true;
    }
    Q_ASSERT(FP::defined(intervalEndTime));
    if (time < intervalEndTime)
        return false;
    for (int counter = 0; counter < 1000000; counter++) {
        intervalStartTime = intervalEndTime;
        intervalEndTime =
                Time::logical(intervalStartTime, intervalUnit, intervalCount, intervalAligned,
                              true);
        Q_ASSERT(FP::defined(intervalEndTime));
        if (intervalStartTime >= intervalEndTime)
            intervalEndTime = intervalStartTime + 1;
        if (intervalEndTime > time)
            return true;
    }
    setInterval(time, intervalStartTime, intervalEndTime);
    return true;
}

bool TriggerPeriodic::testMultipleIntervals(double start, double end, double &intervalStart) const
{
    if (!FP::defined(start) || !FP::defined(end))
        return true;
    double testEnd = 0;
    setInterval(start, intervalStart, testEnd);
    if (testEnd <= end)
        return true;
    return false;
}


TriggerPeriodicMoment::TriggerPeriodicMoment(Time::LogicalTimeUnit intervalUnit,
                                             int intervalCount,
                                             bool intervalAligned,
                                             Time::LogicalTimeUnit u,
                                             const QSet<int> &m) : TriggerPeriodic(intervalUnit,
                                                                                   intervalCount,
                                                                                   intervalAligned),
                                                                   momentUnit(u),
                                                                   moments(m.values()),
                                                                   segments(NULL),
                                                                   firstCheckSegment(NULL)
{
    std::sort(moments.begin(), moments.end());

    if (moments.isEmpty())
        return;
    segments = (Segment *) malloc(sizeof(Segment) * (size_t) moments.size());
}

TriggerPeriodicMoment::~TriggerPeriodicMoment()
{
    if (segments != NULL)
        free(segments);
}

TriggerPeriodicMoment::TriggerPeriodicMoment(const TriggerPeriodicMoment &other) : TriggerPeriodic(
        other),
                                                                                   momentUnit(
                                                                                           other.momentUnit),
                                                                                   moments(other.moments),
                                                                                   segments(NULL),
                                                                                   firstCheckSegment(
                                                                                           NULL)
{
    if (moments.isEmpty())
        return;
    segments = (Segment *) malloc(sizeof(Segment) * (size_t) moments.size());
}

Trigger *TriggerPeriodicMoment::clone() const
{ return new TriggerPeriodicMoment(*this); }

bool TriggerPeriodicMoment::isNeverActive(double start, double end) const
{
    if (moments.isEmpty())
        return true;

    double testStart = 0;
    if (testMultipleIntervals(start, end, testStart))
        return false;
    for (QList<int>::const_iterator m = moments.constBegin(), endM = moments.constEnd();
            m != endM;
            ++m) {
        double checkStart = Time::logical(testStart, momentUnit, *m, false, false);
        Q_ASSERT(FP::defined(checkStart));
        double checkEnd = Time::logical(checkStart, momentUnit, 1, false, true);
        Q_ASSERT(FP::defined(checkEnd));
        if (Range::intersects(start, end, checkStart, checkEnd))
            return false;
    }
    return true;
}

bool TriggerPeriodicMoment::process(double start, double end)
{
    if (segments == NULL)
        return false;

    if (updateInterval(start)) {
        Segment *set = segments;
        for (QList<int>::const_iterator add = moments.constBegin(), endAdd = moments.constEnd();
                add != endAdd;
                ++add, ++set) {
            set->start = Time::logical(getIntervalStart(), momentUnit, *add, false, false);
            set->end = Time::logical(set->start, momentUnit, 1, false, true);
        }
        firstCheckSegment = segments;
    }

    Segment *segmentsEnd = segments + (size_t) moments.size();

    for (; ;) {
        Segment *next = firstCheckSegment + 1;
        if (next == segmentsEnd)
            break;
        if (Range::compareStart(start, next->start) < 0)
            break;
        firstCheckSegment = next;
    }

    for (Segment *check = firstCheckSegment; check != segmentsEnd; ++check) {
        if (Range::compareStartEnd(check->start, end) >= 0)
            break;
        if (Range::intersects(check->start, check->end, start, end))
            return true;
    }

    /* Within this interval so if it hasn't hit a a moment, it won't hit
     * one. */
    if (Range::compareEnd(end, getIntervalEnd()) <= 0)
        return false;

    double nextBegin = getIntervalEnd();
    double nextEnd =
            Time::logical(nextBegin, getIntervalUnit(), getIntervalCount(), getIntervalAligned(),
                          true);
    Q_ASSERT(FP::defined(nextEnd));

    /* Encompasses more than a single interval so we know it must overlap
     * at least one moment (since we've already handled the no moments at
     * all case) */
    if (Range::compareEnd(end, nextEnd) > 0)
        return true;

    /* Evaluate the "future" wrapped moments to see if one would hit */
    for (QList<int>::const_iterator add = moments.constBegin(), endAdd = moments.constEnd();
            add != endAdd;
            ++add) {
        double checkStart = Time::logical(nextBegin, momentUnit, *add, false, false);
        double checkEnd = Time::logical(checkStart, momentUnit, 1, false, true);

        if (Range::compareStartEnd(checkStart, end) >= 0)
            break;
        if (Range::intersects(checkStart, checkEnd, start, end))
            return true;
    }
    return false;
}


TriggerPeriodicRange::TriggerPeriodicRange(Time::LogicalTimeUnit intervalUnit,
                                           int intervalCount,
                                           bool intervalAligned,
                                           Time::LogicalTimeUnit tsu,
                                           int tsc,
                                           bool tsa,
                                           Time::LogicalTimeUnit teu,
                                           int tec,
                                           bool tea) : TriggerPeriodic(intervalUnit, intervalCount,
                                                                       intervalAligned),
                                                       triggerStartUnit(tsu),
                                                       triggerStartCount(tsc),
                                                       triggerStartAlign(tsa),
                                                       triggerEndUnit(teu),
                                                       triggerEndCount(tec),
                                                       triggerEndAlign(tea),
                                                       triggerStartTime(FP::undefined()),
                                                       triggerEndTime(FP::undefined())
{ }

TriggerPeriodicRange::~TriggerPeriodicRange()
{ }

TriggerPeriodicRange::TriggerPeriodicRange(const TriggerPeriodicRange &other) : TriggerPeriodic(
        other),
                                                                                triggerStartUnit(
                                                                                        other.triggerStartUnit),
                                                                                triggerStartCount(
                                                                                        other.triggerStartCount),
                                                                                triggerStartAlign(
                                                                                        other.triggerStartAlign),
                                                                                triggerEndUnit(
                                                                                        other.triggerEndUnit),
                                                                                triggerEndCount(
                                                                                        other.triggerEndCount),
                                                                                triggerEndAlign(
                                                                                        other.triggerEndAlign),
                                                                                triggerStartTime(
                                                                                        FP::undefined()),
                                                                                triggerEndTime(
                                                                                        FP::undefined())
{ }

Trigger *TriggerPeriodicRange::clone() const
{ return new TriggerPeriodicRange(*this); }

bool TriggerPeriodicRange::isNeverActive(double start, double end) const
{
    double testStart = 0;
    if (testMultipleIntervals(start, end, testStart))
        return false;

    double checkStart = testStart;
    if (triggerStartCount >= 0) {
        checkStart =
                Time::logical(testStart, triggerStartUnit, triggerStartCount, triggerStartAlign,
                              false);
    }

    double checkEnd =
            Time::logical(testStart, getIntervalUnit(), getIntervalCount(), getIntervalAligned(),
                          true);
    if (triggerEndCount >= 0) {
        checkEnd = Time::logical(testStart, triggerEndUnit, triggerEndCount, triggerEndAlign, true);
    }

    /* Normal range, so it's active if it intersects at all */
    if (Range::compareStartEnd(checkStart, checkEnd) < 0)
        return !Range::intersects(start, end, checkStart, checkEnd);

    /* Reversed range, so if the check interval is entirely within the gap then
     * it's not active */
    if (Range::compareStart(checkStart, start) <= 0 && Range::compareEnd(checkEnd, end) >= 0)
        return true;
    return false;
}

bool TriggerPeriodicRange::process(double start, double end)
{
    if (updateInterval(start)) {
        if (triggerStartCount >= 0) {
            triggerStartTime =
                    Time::logical(getIntervalStart(), triggerStartUnit, triggerStartCount,
                                  triggerStartAlign, false);
        } else {
            triggerStartTime = getIntervalStart();
        }
        if (triggerEndCount >= 0) {
            triggerEndTime = Time::logical(getIntervalStart(), triggerEndUnit, triggerEndCount,
                                           triggerEndAlign, false);
        } else {
            triggerEndTime = getIntervalEnd();
        }
    }

    if (Range::compareStartEnd(triggerStartTime, triggerEndTime) < 0) {
        if (Range::intersects(start, end, triggerStartTime, triggerEndTime))
            return true;
    } else {
        if (Range::intersects(start, end, getIntervalStart(), triggerEndTime))
            return true;
        if (Range::intersects(start, end, triggerStartTime, getIntervalEnd()))
            return true;
    }

    /* Within this interval so if it hasn't hit the range we're done */
    if (Range::compareEnd(end, getIntervalEnd()) <= 0)
        return false;

    double nextBegin = getIntervalEnd();
    double nextEnd =
            Time::logical(nextBegin, getIntervalUnit(), getIntervalCount(), getIntervalAligned(),
                          true);
    Q_ASSERT(FP::defined(nextEnd));

    /* Encompasses more than a single interval so we know it intersects the
     * interval sometime */
    if (Range::compareEnd(end, nextEnd) > 0)
        return true;

    double checkStart;
    if (triggerStartCount >= 0) {
        checkStart =
                Time::logical(nextBegin, triggerStartUnit, triggerStartCount, triggerStartAlign,
                              false);
    } else {
        checkStart = nextBegin;
    }

    double checkEnd;
    if (triggerEndCount >= 0) {
        checkEnd =
                Time::logical(nextBegin, triggerEndUnit, triggerEndCount, triggerEndAlign, false);
    } else {
        checkEnd = getIntervalEnd();
    }

    if (Range::compareStartEnd(checkStart, checkEnd) < 0) {
        if (Range::intersects(start, end, checkStart, checkEnd))
            return true;
    } else {
        if (Range::intersects(start, end, nextBegin, checkEnd))
            return true;
        if (Range::intersects(start, end, checkStart, nextEnd))
            return true;
    }

    return false;
}

}
}
