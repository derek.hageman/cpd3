/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGCORE_H
#define CPD3EDITINGCORE_H

#include "core/first.hxx"

#include <unordered_map>
#include <vector>
#include <mutex>
#include <memory>
#include <condition_variable>
#include <QtGlobal>
#include <QList>

#include "editing/editing.hxx"
#include "editing/directivecontainer.hxx"
#include "editing/directivedispatch.hxx"
#include "editing/editdirective.hxx"
#include "datacore/stream.hxx"
#include "datacore/processingstage.hxx"
#include "core/waitutils.hxx"
#include "core/merge.hxx"

namespace CPD3 {
namespace Editing {

/**
 * A step in the editing chain.  This processes all directives between other
 * stages of the editing pipeline.
 */
class CPD3EDITING_EXPORT EditCore : public Data::ProcessingStage {
    class Worker : public Data::StreamSink, public Data::StreamSource {
        friend class EditCore;

        EditCore &core;

        std::thread thread;
        std::mutex mutex;
        std::mutex egressLock;
        std::condition_variable request;
        std::condition_variable response;
        Data::SequenceValue::Transfer incoming;
        bool incomingEnded;

        enum {
            NotStarted, Running, Terminated, Completed,
        } state;

        std::vector<std::unique_ptr<EditDirective>> directives;
        std::vector<std::unique_ptr<DirectiveDispatch>> dispatchers;

        class FinalTarget : public EditDispatchSkipTarget {
            Worker *worker;
            Data::StreamSink *next;

            typedef StreamMultiplexer<Data::SequenceValue> Multiplexer;

            Multiplexer mux;
            Multiplexer::Simple *primary;
            std::unordered_map<void *, Multiplexer::Simple *> dispatch;
            bool synchronize;

            Multiplexer::Simple *lookupSkip(void *origin);

            friend class Worker;

        public:
            FinalTarget(Worker *worker);

            virtual ~FinalTarget();

            void incomingData(const Data::SequenceValue &value) override;

            void incomingData(const Data::SequenceValue::Transfer &values) override;

            void incomingData(Data::SequenceValue &&value) override;

            void incomingData(Data::SequenceValue::Transfer &&values) override;

            void incomingAdvance(double time) override;

            void incomingSkipped(const Data::SequenceValue &value, void *origin) override;

            void incomingSkipped(const Data::SequenceValue::Transfer &values,
                                 void *origin) override;

            void incomingSkipped(Data::SequenceValue &&value, void *origin) override;

            void incomingSkipped(Data::SequenceValue::Transfer &&values, void *origin) override;

            void incomingSkipAdvance(double time, void *origin) override;

            void incomingSkipEnd(void *origin) override;

            void finalize();

            inline void setNext(Data::StreamSink *n)
            { next = n; }
        };

        friend class FinalTarget;

        FinalTarget finalTarget;

        bool synchronousFinalOutput;
        Data::StreamSink *egress;

        void finalizeEnd();

        void stall(std::unique_lock<std::mutex> &lock);

    public:
        Worker(EditCore &core, std::vector<std::unique_ptr<EditDirective>> &&directives);

        virtual ~Worker();

        virtual void run();

        void incomingData(const Data::SequenceValue::Transfer &values) override;

        void incomingData(Data::SequenceValue::Transfer &&values) override;

        void incomingData(const Data::SequenceValue &value) override;

        void incomingData(Data::SequenceValue &&value) override;

        void endData() override;

        void signalTerminate();

        void start();

        void setEgress(Data::StreamSink *egress) override;

        Threading::Signal<> finished;
    };

    std::vector<std::unique_ptr<Worker>> workers;

    class SynchronizerOutput : public Data::AsyncProcessingStage {
    public:
        SynchronizerOutput();

        virtual ~SynchronizerOutput();

    protected:
        virtual void process(Data::SequenceValue::Transfer &&incoming);
    };

    std::unique_ptr<SynchronizerOutput> synchronizerOutput;

    Worker *input;
    Data::StreamSource *egressController;

    std::condition_variable externalWait;

    void createWorker(std::vector<std::unique_ptr<EditDirective>> &&directives);

    void connectWorkers();

public:
    /**
     * Create a new editing core.  This takes ownership of all the directives.
     * The directives list cannot be empty.
     * 
     * @param directives    the directives to apply
     */
    EditCore(std::vector<std::unique_ptr<EditDirective>> &&directives);

    virtual ~EditCore();

    void incomingData(const Data::SequenceValue::Transfer &values) override;

    void incomingData(Data::SequenceValue::Transfer &&values) override;

    void incomingData(const Data::SequenceValue &value) override;

    void incomingData(Data::SequenceValue &&value) override;

    void endData() override;

    void setEgress(Data::StreamSink *egress) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

    void serialize(QDataStream &stream) override;

    bool isFinished() override;

    bool wait(double timeout = FP::undefined()) override;

    void signalTerminate() override;

    void start() override;
};

}
}

#endif
