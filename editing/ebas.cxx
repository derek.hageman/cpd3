/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/ebas.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Editing {

/** @file editing/ebas.hxx
 * EBAS constants related to editing.
 */

namespace {
enum Type {
    Valid, Invalid, Missing, Hidden,
};

class FlagInfo {
public:
    int code;
    Type type;
    QString description;

    FlagInfo(qint64 c, Type t, const QString &d) : code(c), type(t), description(d)
    { }

    FlagInfo() : code(-1), type(Valid), description()
    { }
};

class AllFlags {
    void a(qint64 code, Type type, const QString &description)
    {
        data.insert(code, FlagInfo(code, type, description));
    }

public:
    QHash<int, FlagInfo> data;

    AllFlags()
    {
        a(999, Missing, "Missing measurement, unspecified reason");
        a(990, Missing,
          "Precipitation not measured due to snow-fall. Needed for historic data, should not be needed for new data");
        a(980, Missing, "Missing due to calibration or zero/span check");
        a(900, Hidden, "Hidden and invalidated by data originator");
        a(899, Missing, "Measurement undefined, unspecified reason");
        a(890, Missing, "Concentration in precipitation undefined, no precipitation");
        a(799, Invalid,
          "Measurement missing (unspecified reason), data element contains estimated value");
        a(798, Valid,
          "Measurement missing (unspecified reason), data element contains estimated value. Considered valid.");
        a(797, Valid, "Data element taken from co-located instrument");
        a(784, Invalid, "Low precipitation, concentration estimated");
        a(783, Missing, "Low precipitation, concentration unknown");
        a(782, Valid, "Low precipitation, concentration estimated");
        a(781, Valid, "Value below detection limit, data element contains detection limit");
        a(780, Valid,
          "Value below detection or quantification limit, data element contains estimated or measured value.");
        a(771, Valid, "Value above range, data element contains upper range limit");
        a(770, Valid, "Value above range, data element contains estimated value");
        a(750, Missing, "H+ not measured in alkaline sample");
        a(741, Valid,
          "Non refractory AMS concentrations. Don't include compounds that volatalises above 600 deg C");
        a(740, Valid, "Probably biased gas/particle ratio");
        a(701, Invalid,
          "Less accurate than usual, unspecified reason. (Used only with old data, for new data see groups 6 and 5)");
        a(699, Invalid, "Mechanical problem, unspecified reason");
        a(682, Invalid, "Invalid due to calibration or zero/span check");
        a(681, Invalid, "Low data capture");
        a(680, Valid, "Undefined wind direction");
        a(679, Valid, "Unspecified meteorological condition");
        a(678, Valid, "Hurricane");
        a(677, Invalid, "Icing or hoar frost in the intake");
        a(676, Valid, "station inside cloud (visibility < 1000 m)");
        a(675, Valid, "no visibility data available");
        a(670, Invalid, "Incomplete data acquisition for multi-component data sets");
        a(669, Invalid, "Moist or wet filter, invalid");
        a(668, Valid, "Moist or wet filter, valid");
        a(666, Invalid, "Filter damaged, invalid");
        a(665, Valid, "Filter damaged, valid");
        a(664, Invalid, "Instrument flow(s) too far off target value, considered invalid");
        a(663, Invalid, "Too high sampling flow, data considered invalid");
        a(662, Valid, "Too high sampling flow, data considered valid");
        a(660, Valid, "Unspecified sampling anomaly, considered valid");
        a(659, Invalid, "Unspecified sampling anomaly");
        a(658, Invalid, "Too small air volume");
        a(657, Valid, "Precipitation collector overflow. Heavy rain shower (squall)");
        a(656, Valid, "Wet-only collector failure, operated as bulk collector");
        a(655, Valid,
          "Two samples mixed due to late servicing of sampler. Estimated value created by averaging");
        a(654, Valid, "Sampling period longer than normal by 10-15%, observed values reported");
        a(653, Valid, "Sampling period shorter than normal by 10-15%, observed values reported");
        a(652, Valid, "Construction/acitivity nearby");
        a(651, Valid, "Agricultural activity nearby");
        a(650, Valid, "Precipitation collector failure");
        a(649, Valid, "Temporary power fail has affected sampler operation");
        a(648, Valid, "Snow sampler");
        a(644, Valid, "Low instrument precision and/or calibration issues");
        a(641, Invalid, "Aerosol filters installed incorrectly");
        a(640, Valid, "Instrument internal relative humidity above 40%");
        a(635, Invalid, "Internal temperatures too far off target value, considered invalid");
        a(599, Invalid, "Unspecified contamination or local influence");
        a(593, Invalid, "Industrial contamination, considered invalid");
        a(591, Invalid, "Agricultural contamination, considered invalid");
        a(578, Invalid,
          "Large sea salt contribution (ratio between marine and excess sulphate is larger than 2.0). Used for old data only. For newer data use 451/450.");
        a(568, Invalid, "Dust contamination, considered invalid");
        a(567, Invalid, "Insect contamination, considered invalid");
        a(566, Invalid, "Bird droppings, considered invalid");
        a(565, Invalid, "Pollen and/or leaf contamination, considered invalid");
        a(559, Valid, "Unspecified contamination or local influence, but considered valid");
        a(558, Valid, "Dust contamination, but considered valid");
        a(557, Valid, "Insect contamination, but considered valid");
        a(556, Valid, "Bird droppings, but considered valid");
        a(555, Valid, "Pollen and/or leaf contamination, but considered valid");
        a(549, Invalid, "Impure chemicals");
        a(541, Invalid, "Gold trap passiviated by unknown compound");
        a(540, Invalid, "Spectral interference in laboratory analysis");
        a(534, Invalid, "Wrong coated denuder used");
        a(533, Invalid, "Filters mixed up; incorrect analysis");
        a(532, Valid, "Data less accurate than normal due to high field blank value");
        a(531, Valid, "Low recovery, analysis inaccurate");
        a(530, Invalid, "Invalid due to too low or too high recovery");
        a(521, Valid,
          "Bactericide was added to sample for storage under warm climate. Considered valid");
        a(499, Valid, "Inconsistent with another unspecified measurement");
        a(498, Valid, "Gold trap inconsistency in mercury monitor");
        a(478, Invalid, "Invalid due to inconsistency discovered through ion balance calculations");
        a(477, Invalid, "Invalid due to inconsistency between measured and estimated conductivity");
        a(476, Valid,
          "Inconsistency discovered through ion balance calculations, but considerd valid");
        a(475, Valid,
          "Inconsistency between measured and estimated conductivity, but considerd valid");
        a(470, Valid,
          "Particulate mass concentration higher than parallell mass concentration measurement with higher cut off i.e PM1_mass > PM25_mass and PM25_mass > PM10_mass");
        a(460, Invalid, "Contamination suspected");
        a(459, Invalid, "Extreme value, unspecified error");
        a(458, Valid,
          "Extremely high value, outside four times standard deviation in a lognormal distribution");
        a(457, Valid,
          "Extremely low value, outside four times standard deviation in a lognormal distribution");
        a(456, Invalid, "Invalidated by data originator");
        a(452, Invalid, "Invalid due to large uncertainty");
        a(451, Invalid, "Invalid due to large sea salt contribution");
        a(450, Valid, "Considerable sea salt contribution, but considered valid");
        a(440, Valid, "Reconstructed or recalculated data");
        a(420, Valid, "Preliminary data");
        a(410, Valid, "Sahara dust event");
        a(395, Invalid, "Data completeness less than 90%");
        a(394, Valid, "Data completeness less than 90%");
        a(393, Invalid, "Data completeness less than 75%");
        a(392, Valid, "Data completeness less than 75%");
        a(391, Invalid, "Data completeness less than 50%");
        a(390, Valid, "Data completeness less than 50%");
        a(382, Valid, "More than 75% of the measurements are below detection limit");
        a(380, Valid, "More than 50% of the measurements are below detection limit");
        a(370, Valid,
          "For monthly averages with samples partly in two months, the number of days in each month is used for weighing the sample");
        a(299, Valid, "Inconsistent with another unspecified measurement");
        a(298, Valid, "Gold trap inconsistency in mercury monitor");
        a(278, Invalid, "Invalid due to inconsistency discovered through ion balance calculations");
        a(277, Invalid, "Invalid due to inconsistency between measured and estimated conductivity");
        a(276, Valid,
          "Inconsistency discovered through ion balance calculations, but considered valid");
        a(275, Valid,
          "Inconsistency between measured and estimated conductivity, but considered valid");
        a(260, Invalid, "Contamination suspected");
        a(259, Invalid, "Unspecified error expected");
        a(258, Valid,
          "Extremely high value, outside four times standard deviation in a log-normal distribution");
        a(257, Valid,
          "Extremely low value, outside four times standard deviation in a log-normal distribution");
        a(256, Invalid, "Invalidated by database co-ordinator");
        a(251, Invalid, "Invalid due to large sea salt contribution");
        a(250, Valid, "Considerable sea salt contribution, but considered valid");
        a(249, Valid, "Apparent typing error corrected. Valid measurement");
        a(248, Valid,
          "Illegal flag was removed by the database co-ordinator. Lost flag information (used for historic data only).");
        a(247, Valid,
          "Overlapping sample interval was corrected by the database co-ordinator. Possible wrong sample time (used for historic data only)");
        a(220, Valid, "Preliminary data");
        a(211, Valid,
          "Irregular data checked and accepted by database co-ordinator. Valid measurement");
        a(210, Valid,
          "Episode data checked and accepted by database co-ordinator. Valid measurement");
        a(191, Valid, "Data not truncation corrected - Valid measurement");
        a(190, Valid, "Not corrected for cross-sensitivity to particle scattering");
        a(189, Valid,
          "Possible local contamination indicated by wind from contaminated sector (auto)");
        a(188, Valid, "Possible local contamination indicated by low wind speed (auto)");
        a(187, Valid,
          "Possible local contamination indicated by occurence of new particles (auto)");
        a(186, Valid, "Possible local contamination indicated by single scattering albedo (auto)");
        a(185, Valid, "Possible local contamination indicated by wind direction or velocity");
        a(147, Valid,
          "Below theoretical detection limit or formal Q/A limit, but a value has been measured and reported and is considered valid");
        a(120, Valid, "Sample reanalysed with similar results. Valid measurement");
        a(111, Valid, "Irregular data checked and accepted by data originator. Valid measurement");
        a(110, Valid, "Episode data checked and accepted by data originator. Valid measurement");
        a(103, Valid, "CV of replicate ALPHA samplers > 15 %;. Valid measurement");
        a(102, Valid, "CV of replicate diffusion tubes > 30 %. Valid measurement");
        a(101, Valid, "Denuder capture efficiency < 75%. Valid measurement");
        a(100, Valid, "Checked by data originator. Valid measurement");
    }
};

static AllFlags flagData;
}

Variant::Root EBAS::description(int flag)
{
    QHash<int, FlagInfo>::const_iterator info = flagData.data.constFind(flag);
    if (info == flagData.data.constEnd())
        return Variant::Root();
    return Variant::Root(info.value().description);
}

Variant::Root EBAS::description(const QString &flag)
{
    if (!flag.startsWith("EBASFlag"))
        return Variant::Root();
    bool ok = false;
    int code = flag.mid(8).toInt(&ok);
    if (!ok || code <= 0 || code > 999)
        return Variant::Root();
    return description(code);
}

QHash<int, QString> EBAS::getAllDescriptions(FlagType type)
{
    Type check = Valid;
    switch (type) {
    case Flag_Valid:
        check = Valid;
        break;
    case Flag_Invalid:
        check = Invalid;
        break;
    case Flag_Missing:
        check = Missing;
        break;
    case Flag_Hidden:
        check = Hidden;
        break;
    }

    QHash<int, QString> result;
    for (QHash<int, FlagInfo>::const_iterator info = flagData.data.constBegin(),
            endInfo = flagData.data.constEnd(); info != endInfo; ++info) {
        if (info.value().type != check)
            continue;
        result.insert(info.value().code, info.value().description);
    }
    return result;
}


}
}
