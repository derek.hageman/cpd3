/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGENGINE_H
#define CPD3EDITINGENGINE_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QList>
#include <QSet>
#include <QHash>
#include <QList>
#include <QString>

#include "editing/editing.hxx"
#include "editing/editdirective.hxx"
#include "editing/directivecontainer.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/archive/access.hxx"

namespace CPD3 {
namespace Editing {

namespace Internal {
struct EditingOutputSelection {
    double start;
    double end;
    Data::SequenceMatch::OrderedLookup selection;

    inline double getStart() const
    { return start; }

    inline double getEnd() const
    { return end; }
};
}

/**
 * An engine that handles the complete generation of processed edited
 * data.
 */
class CPD3EDITING_EXPORT EditingEngine : public Data::ExternalConverter {
    double startTime;
    double endTime;
    Data::SequenceName::Component station;
    QString profile;
    Data::SequenceName::Component archive;

    Data::SequenceName::Component outputArchive;
    Data::SequenceName::Component outputAverages;

    Data::Archive::Selection::List inputSelections;
    Data::Archive::Access *access;
    std::unique_ptr<Data::Archive::Access> localAccess;
    Data::Archive::Access::StreamHandle activeRead;
    std::vector<std::unique_ptr<Data::ProcessingStage> > chain;

    Data::StreamSink *initialEgress;

    class OutputFilter : public Data::StreamSink {
        EditingEngine *engine;

        class DispatchBase {
        public:
            DispatchBase();

            virtual ~DispatchBase();

            virtual bool handle(Data::SequenceValue &incoming) = 0;
        };

        class DispatchDiscard : public DispatchBase {
        public:
            DispatchDiscard();

            virtual ~DispatchDiscard();

            virtual bool handle(Data::SequenceValue &incoming);
        };

        friend class DispatchDiscard;

        class DispatchPass : public DispatchBase {
            OutputFilter *filter;
            double start;
            double end;
        public:
            DispatchPass(OutputFilter *f);

            virtual ~DispatchPass();

            virtual bool handle(Data::SequenceValue &incoming);
        };

        friend class DispatchPass;

        class DispatchMetaPass : public DispatchBase {
            OutputFilter *filter;
            double start;
            double end;
        public:
            DispatchMetaPass(OutputFilter *f);

            virtual ~DispatchMetaPass();

            virtual bool handle(Data::SequenceValue &incoming);
        };

        friend class DispatchMetaPass;

        Data::SequenceName::Map<std::unique_ptr<DispatchBase>> dispatch;

        DispatchBase *getDispatch(const Data::SequenceValue &value);
    public:
        Data::StreamSink *egress;
        std::vector<Internal::EditingOutputSelection> selection;

        OutputFilter(EditingEngine *e);

        virtual ~OutputFilter();

        void incomingData(const Data::SequenceValue::Transfer &values) override;

        void incomingData(Data::SequenceValue::Transfer &&values) override;

        void incomingData(const Data::SequenceValue &value) override;

        void incomingData(Data::SequenceValue &&value) override;

        void endData() override;

        inline double getStart()
        { return engine->getStart(); }

        inline double getEnd()
        { return engine->getEnd(); }
    };

    friend class OutputFilter;

    OutputFilter outputFilter;

    bool instantiateDirectives(QList<DirectiveContainer>::const_iterator beginInstance,
                               QList<DirectiveContainer>::const_iterator endInstance,
                               Data::SequenceMatch::Composite **variablesContinuous = NULL,
                               Data::SequenceMatch::Composite **variablesFull = NULL);

    struct Context {
        std::unique_ptr<QObject> ref;
        std::mutex mutex;
        bool valid;
        std::uint_fast32_t held;

        Context();
    };

    std::shared_ptr<Context> context;

    void createChain();

    void checkAllFinished(const std::shared_ptr<Context> &context);

public:
    /**
     * Create a new editing engine.
     * 
     * @param start     the start of the editing period
     * @param eend       the end of the editing period
     * @param station   the station to generate for
     * @param profile   the profile identification
     * @param access    the archive access to use
     */
    EditingEngine(double start, double eend, const Data::SequenceName::Component &station,
                  const QString &profile = QString::fromLatin1("aerosol"),
                  Data::Archive::Access *access = nullptr);

    virtual ~EditingEngine();

    void setEgress(Data::StreamSink *egress) override;

    void incomingData(const Util::ByteView &data) override;

    void endData() override;

    void signalTerminate() override;

    bool isFinished() override;

    void start() override;

    bool wait(double timeout = FP::undefined()) override;

    Data::SequenceName::Set predictedOutputs() override;

    /**
     * Execute a purge on the archive.  This will remove all data that will
     * be output by the engine.  Only available after starting the engine
     * with a supplied access and a write lock.
     *
     * @param progress  the progress signal generated
     */
    void purgeArchive(const Threading::Signal<double> &progress = Threading::Signal<double>());

    /**
     * Get the start time the engine was created with.
     * 
     * @return the start time
     */
    inline double getStart() const
    { return startTime; }

    /**
     * Get the end time the engine was created with.
     * 
     * @return the end time
     */
    inline double getEnd() const
    { return endTime; }
};

}
}

#endif
