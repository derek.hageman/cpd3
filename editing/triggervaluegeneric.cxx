/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/triggervaluegeneric.hxx"

namespace CPD3 {
namespace Editing {

/** @file editing/triggervaluegeneric.hxx
 * Triggers based on generic value streams.  These triggers work with the
 * raw segment stream instead of any buffering and conversion to
 * single double streams.
 */

TriggerValueGeneric::TriggerValueGeneric(const Data::SequenceMatch::OrderedLookup &input,
                                         const std::string &p) : selection(input), path(p), reader()
{ }

TriggerValueGeneric::~TriggerValueGeneric() = default;

TriggerValueGeneric::TriggerValueGeneric(const TriggerValueGeneric &other) : TriggerSegmenting(
        other), selection(other.selection), path(other.path), reader()
{ }

QList<EditDataTarget *> TriggerValueGeneric::unhandledInput(const Data::SequenceName &unit)
{
    if (!selection.matches(unit))
        return QList<EditDataTarget *>();
    QList<EditDataTarget *> result;
    result.append(this);
    return result;
}

void TriggerValueGeneric::convertSegments(const Data::ValueSegment::Transfer &segments)
{
    if (segments.empty())
        return;
    for (const auto &add : segments) {
        TriggerSegmenting::emitSegment(add.getStart(), add.getEnd(), convert(add[path]));
    }
}

void TriggerValueGeneric::incomingSequenceValue(const Data::SequenceValue &value)
{ convertSegments(reader.add(value)); }

void TriggerValueGeneric::incomingDataAdvance(double time)
{
    convertSegments(reader.advance(time));
    TriggerSegmenting::emitAdvance(time);
}

void TriggerValueGeneric::finalize()
{
    convertSegments(reader.finish());
    TriggerSegmenting::finalize();
}

Data::SequenceName::Set TriggerValueGeneric::requestedInputs() const
{ return selection.knownInputs(); }


TriggerHasFlags::TriggerHasFlags(const Data::Variant::Flags &fl,
                                 const Data::SequenceMatch::OrderedLookup &input,
                                 const std::string &path) : TriggerValueGeneric(input, path),
                                                            flags(fl)
{ }

TriggerHasFlags::~TriggerHasFlags() = default;

TriggerHasFlags::TriggerHasFlags(const TriggerHasFlags &other) = default;

Trigger *TriggerHasFlags::clone() const
{ return new TriggerHasFlags(*this); }

bool TriggerHasFlags::convert(const Data::Variant::Read &value)
{ return value.testFlags(flags); }

TriggerLacksFlags::TriggerLacksFlags(const Data::Variant::Flags &fl,
                                     const Data::SequenceMatch::OrderedLookup &input,
                                     const std::string &path) : TriggerValueGeneric(input, path),
                                                                flags(fl)
{ }

TriggerLacksFlags::~TriggerLacksFlags() = default;

TriggerLacksFlags::TriggerLacksFlags(const TriggerLacksFlags &other) = default;

Trigger *TriggerLacksFlags::clone() const
{ return new TriggerLacksFlags(*this); }

bool TriggerLacksFlags::convert(const Data::Variant::Read &value)
{ return !value.testAnyFlags(flags); }

TriggerExists::TriggerExists(const Data::SequenceMatch::OrderedLookup &input,
                             const std::string &path) : TriggerValueGeneric(input, path)
{ }

TriggerExists::~TriggerExists() = default;

TriggerExists::TriggerExists(const TriggerExists &other) = default;

Trigger *TriggerExists::clone() const
{ return new TriggerExists(*this); }

bool TriggerExists::convert(const Data::Variant::Read &value)
{ return value.exists(); }

}
}
