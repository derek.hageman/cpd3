/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/editcore.hxx"
#include "core/environment.hxx"
#include "core/threadpool.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Editing {

/** @file editing/editcore.hxx
 * The implementation of the core handler that applies edits to a data stream.
 */

static const int maximumDirectivesPerWorker = 32;

EditCore::EditCore(std::vector<std::unique_ptr<EditDirective>> &&directives)
        : workers(), synchronizerOutput(), input(nullptr), egressController(nullptr), externalWait()
{
    Q_ASSERT(!directives.empty());

    std::vector<std::unique_ptr<EditDirective>> activeSet;
    bool synchronizeRequired = false;
    for (auto &add : directives) {
        if ((add->shouldRunDetached() ||
                synchronizeRequired || activeSet.size() >= maximumDirectivesPerWorker) &&
                !activeSet.empty()) {
            createWorker(std::move(activeSet));
            activeSet.clear();
        }

        synchronizeRequired = add->mustSynchronizeOutput();
        activeSet.emplace_back(std::move(add));
    }
    if (!activeSet.empty()) {
        createWorker(std::move(activeSet));
    }

    connectWorkers();
}

void EditCore::createWorker(std::vector<std::unique_ptr<EditDirective>> &&directives)
{
    workers.emplace_back(new Worker(*this, std::move(directives)));
}

void EditCore::connectWorkers()
{
    Q_ASSERT(!workers.empty());

    input = workers.front().get();

    if (!workers.back()->directives.back()->mustSynchronizeOutput()) {
        workers.back()->synchronousFinalOutput = true;
        egressController = workers.back().get();
    } else {
        synchronizerOutput.reset(new SynchronizerOutput);
        egressController = synchronizerOutput.get();
        workers.back()->finalTarget.setNext(synchronizerOutput.get());
    }

    for (auto w = workers.begin(), next = w + 1, end = workers.end();
            next != end;
            w = next, ++next) {
        (*w)->finalTarget.setNext(next->get());
    }
}

EditCore::~EditCore()
{
    for (const auto &w : workers) {
        w->signalTerminate();
    }
    externalWait.notify_all();
    wait();
}

void EditCore::incomingData(const SequenceValue::Transfer &values)
{ input->incomingData(values); }

void EditCore::incomingData(SequenceValue::Transfer &&values)
{ input->incomingData(std::move(values)); }

void EditCore::incomingData(const SequenceValue &value)
{ input->incomingData(value); }

void EditCore::incomingData(SequenceValue &&value)
{ input->incomingData(std::move(value)); }

void EditCore::endData()
{ input->endData(); }

SequenceName::Set EditCore::requestedInputs()
{
    SequenceName::Set result;
    for (const auto &w : workers) {
        for (const auto &d : w->directives) {
            Util::merge(d->requestedInputs(), result);
        }
    }
    return result;
}

SequenceName::Set EditCore::predictedOutputs()
{
    SequenceName::Set result;
    for (const auto &w : workers) {
        for (const auto &d : w->directives) {
            Util::merge(d->predictedOutputs(), result);
        }
    }
    return result;
}

void EditCore::serialize(QDataStream &)
{
    Q_ASSERT(false);
}

void EditCore::signalTerminate()
{
    for (const auto &w : workers) {
        w->signalTerminate();
    }
    if (synchronizerOutput)
        synchronizerOutput->signalTerminate();
}

void EditCore::start()
{
    if (synchronizerOutput) {
        synchronizerOutput->finished = finished;
    } else if (!workers.empty()) {
        workers.back()->finished = finished;
    } else {
        finished();
        return;
    }

    for (const auto &w : workers) {
        w->start();
    }
    if (synchronizerOutput)
        synchronizerOutput->start();
}

bool EditCore::isFinished()
{
    for (const auto &w : workers) {
        std::lock_guard<std::mutex> lock(w->mutex);
        if (w->state != Worker::Completed)
            return false;
    }
    if (synchronizerOutput && !synchronizerOutput->isFinished())
        return false;
    return true;
}

bool EditCore::wait(double timeout)
{
    if (!FP::defined(timeout)) {
        for (const auto &w : workers) {
            std::unique_lock<std::mutex> lock(w->mutex);
            for (;;) {
                if (w->state == Worker::Completed)
                    break;
                externalWait.wait(lock);
            }
        }
        if (synchronizerOutput)
            synchronizerOutput->wait();
        return true;
    }

    using Clock = std::chrono::steady_clock;
    auto endTime = Clock::now() + std::chrono::duration<double>(timeout);

    for (const auto &w : workers) {
        std::unique_lock<std::mutex> lock(w->mutex);
        for (;;) {
            if (w->state == Worker::Completed)
                break;
            if (endTime <= Clock::now())
                return false;
            externalWait.wait_until(lock, endTime);
        }
    }

    if (synchronizerOutput) {
        auto remaining = std::chrono::duration_cast<std::chrono::duration<double>>(
                endTime - Clock::now()).count();
        if (remaining <= 0)
            return synchronizerOutput->isFinished();
        if (!synchronizerOutput->wait(remaining))
            return false;
    }

    return true;
}

void EditCore::setEgress(Data::StreamSink *egress)
{ egressController->setEgress(egress); }


EditCore::Worker::Worker(EditCore &core, std::vector<std::unique_ptr<EditDirective>> &&d) : core(
        core),
                                                                                            incomingEnded(
                                                                                                    false),
                                                                                            state(NotStarted),
                                                                                            directives(
                                                                                                    std::move(
                                                                                                            d)),
                                                                                            dispatchers(),
                                                                                            finalTarget(
                                                                                                    this),
                                                                                            synchronousFinalOutput(
                                                                                                 false),
                                                                                            egress(nullptr)
{

    Q_ASSERT(!directives.empty());
    for (auto begin = directives.begin(), end = directives.end(); begin != end; ++begin) {
        std::vector<EditDirective *> selected;
        for (auto add = begin; add != end; ++add) {
            selected.push_back(add->get());
        }
        dispatchers.emplace_back(new DirectiveDispatch(std::move(selected), &finalTarget));
    }
    auto dis = dispatchers.begin() + 1;
    for (auto dir = directives.begin(), next = dir + 1, end = directives.end();
            dir != end;
            dir = next, ++next, ++dis) {
        if (next == end) {
            (*dir)->setOutput(&finalTarget);
        } else {
            (*dir)->setOutput(dis->get());
        }
    }
}

EditCore::Worker::~Worker()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state == Running)
            state = Terminated;
    }
    request.notify_all();
    if (thread.joinable())
        thread.join();
}

void EditCore::Worker::finalizeEnd()
{
    for (const auto &dir : directives) {
        dir->finalize();
    }

    if (finalTarget.next)
        finalTarget.finalize();
}

void EditCore::Worker::run()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!lock)
            lock.lock();
        std::unique_lock<std::mutex> egressLocker(egressLock);

        bool localEnded = false;

        SequenceValue::Transfer toProcess;
        switch (state) {
        case NotStarted:
        case Completed:
            return;

        case Terminated: {
            lock.unlock();

            for (const auto &dir : directives) {
                dir->signalTerminate();
            }
            /* Only do the finalize if we have an output, so we don't try
             * dumping values into nothing */
            if (!synchronousFinalOutput || egress) {
                finalizeEnd();
            }

            egressLocker.unlock();
            lock.lock();
            state = Completed;
            incoming.clear();
            response.notify_all();

            bool doEnd = false;
            if (synchronousFinalOutput) {
                egressLocker.lock();
                doEnd = egress && (finalTarget.next != egress);
                finalTarget.next = egress;
            }

            core.externalWait.notify_all();

            lock.unlock();
            if (doEnd) {
                Q_ASSERT(egressLocker.owns_lock());
                egress->endData();
            }
            finished();
            return;
        }

        case Running:
            if (synchronousFinalOutput) {
                finalTarget.next = egress;

                if (!egress) {
                    egressLocker.unlock();
                    request.wait(lock);
                    continue;
                }
            }

            if (!incoming.empty() || incomingEnded) {
                localEnded = incomingEnded;
                toProcess = std::move(incoming);
                incoming.clear();
                break;
            }

            egressLocker.unlock();
            request.wait(lock);
            continue;
        }
        lock.unlock();

        static constexpr std::size_t advanceChunkSize = 8192;

        std::size_t remaining = static_cast<std::size_t>(toProcess.size());
        if (remaining > stallThreshold)
            response.notify_all();

        if (remaining != 0) {
            if (remaining <= advanceChunkSize) {
                dispatchers.front()->incomingData(std::move(toProcess));

                for (const auto &disp : dispatchers) {
                    disp->processAdvance();
                }
            } else {
                for (auto begin = toProcess.begin(), end = toProcess.end(); begin != end;) {
                    auto add = std::min(remaining, advanceChunkSize);
                    remaining -= add;
                    auto next = begin + add;

                    {
                        SequenceValue::Transfer chunk;
                        std::move(begin, next, Util::back_emplacer(chunk));
                        dispatchers.front()->incomingData(std::move(chunk));
                    }

                    for (const auto &disp : dispatchers) {
                        disp->processAdvance();
                    }

                    begin = next;
                }
            }
        }

        if (localEnded) {
            finalizeEnd();

            egressLocker.unlock();
            lock.lock();
            state = Completed;
            lock.unlock();

            core.externalWait.notify_all();
            response.notify_all();
            finished();
            return;
        }
    }
}

void EditCore::Worker::setEgress(StreamSink *set)
{
    Q_ASSERT(synchronousFinalOutput);

    std::unique_lock<std::mutex> lock(mutex);
    std::unique_lock<std::mutex> egressLocker(egressLock);
    if (state == Completed || state == Terminated) {
        for (;;) {
            if (state == Completed)
                break;
            egressLocker.unlock();
            core.externalWait.wait(lock);
            egressLocker.lock();
        }
        lock.unlock();

        if (set && set != egress) {
            egress->endData();
        }
        egress = set;
        return;
    }
    egress = set;
    request.notify_all();
}

void EditCore::Worker::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Q_ASSERT(!incomingEnded);
        Util::append(values, incoming);
    }
    request.notify_all();
}

void EditCore::Worker::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Q_ASSERT(!incomingEnded);
        Util::append(std::move(values), incoming);
    }
    request.notify_all();
}

void EditCore::Worker::incomingData(const SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Q_ASSERT(!incomingEnded);
        incoming.emplace_back(value);
    }
    request.notify_all();
}

void EditCore::Worker::incomingData(SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Q_ASSERT(!incomingEnded);
        incoming.emplace_back(std::move(value));
    }
    request.notify_all();
}

void EditCore::Worker::endData()
{
    std::lock_guard<std::mutex> lock(mutex);
    incomingEnded = true;
    request.notify_all();
}

void EditCore::Worker::stall(std::unique_lock<std::mutex> &lock)
{
    while (incoming.size() > stallThreshold) {
        switch (state) {
        case NotStarted:
        case Running:
            break;
        case Terminated:
        case Completed:
            return;
        }

        response.wait(lock);
    }
}

void EditCore::Worker::signalTerminate()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (state != Completed)
        state = Terminated;
    request.notify_all();
    response.notify_all();
}

void EditCore::Worker::start()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(state == NotStarted);
        state = Running;
    }
    thread = std::thread(std::bind(&Worker::run, this));
}

EditCore::Worker::FinalTarget::FinalTarget(Worker *w) : worker(w),
                                                        next(nullptr),
                                                        mux(),
                                                        primary(nullptr),
                                                        dispatch(),
                                                        synchronize(false)
{
    primary = mux.createSimple();
}

EditCore::Worker::FinalTarget::~FinalTarget() = default;

void EditCore::Worker::FinalTarget::incomingData(const SequenceValue &value)
{
    Q_ASSERT(next);
    if (synchronize) {
        std::unique_lock<std::mutex> lock(worker->mutex);
        SequenceValue::Transfer result;
        if (primary->incomingValue(value))
            mux.output(result);
        lock.unlock();
        next->incomingData(std::move(result));
    } else {
        if (primary->incomingValue(value))
            next->incomingData(mux.output<SequenceValue::Transfer>());
    }
}

void EditCore::Worker::FinalTarget::incomingData(const SequenceValue::Transfer &values)
{
    Q_ASSERT(next);
    if (synchronize) {
        std::unique_lock<std::mutex> lock(worker->mutex);
        SequenceValue::Transfer result;
        if (primary->incoming(values))
            mux.output(result);
        lock.unlock();
        next->incomingData(std::move(result));
    } else {
        if (primary->incoming(values))
            next->incomingData(mux.output<SequenceValue::Transfer>());
    }
}

void EditCore::Worker::FinalTarget::incomingData(SequenceValue &&value)
{
    Q_ASSERT(next);
    if (synchronize) {
        std::unique_lock<std::mutex> lock(worker->mutex);
        SequenceValue::Transfer result;
        if (primary->incomingValue(std::move(value)))
            mux.output(result);
        lock.unlock();
        next->incomingData(std::move(result));
    } else {
        if (primary->incomingValue(value))
            next->incomingData(mux.output<SequenceValue::Transfer>());
    }
}

void EditCore::Worker::FinalTarget::incomingData(SequenceValue::Transfer &&values)
{
    Q_ASSERT(next);
    if (synchronize) {
        std::unique_lock<std::mutex> lock(worker->mutex);
        SequenceValue::Transfer result;
        if (primary->incoming(std::move(values)))
            mux.output(result);
        lock.unlock();
        next->incomingData(std::move(result));
    } else {
        if (primary->incoming(values))
            next->incomingData(mux.output<SequenceValue::Transfer>());
    }
}

void EditCore::Worker::FinalTarget::incomingAdvance(double time)
{
    Q_ASSERT(next);
    if (synchronize) {
        std::unique_lock<std::mutex> lock(worker->mutex);
        SequenceValue::Transfer result;
        if (primary->advance(time))
            mux.output(result);
        lock.unlock();
        next->incomingData(result);
    } else {
        if (primary->advance(time))
            next->incomingData(mux.output<SequenceValue::Transfer>());
    }
}

EditCore::Worker::FinalTarget::Multiplexer::Simple *EditCore::Worker::FinalTarget::lookupSkip(void *origin)
{
    auto target = dispatch.find(origin);
    if (target != dispatch.end())
        return target->second;

    if (synchronize) {
        std::lock_guard<std::mutex> lock(worker->mutex);
        target = dispatch.emplace(origin, mux.createSimple()).first;
    } else {
        target = dispatch.emplace(origin, mux.createSimple()).first;
    }
    return target->second;
}

void EditCore::Worker::FinalTarget::incomingSkipped(const SequenceValue &value, void *origin)
{
    Multiplexer::Simple *target = lookupSkip(origin);
    Q_ASSERT(next);
    if (synchronize) {
        std::unique_lock<std::mutex> lock(worker->mutex);
        SequenceValue::Transfer result;
        if (target->incomingValue(value))
            mux.output(result);
        lock.unlock();
        next->incomingData(std::move(result));
    } else {
        if (target->incomingValue(value))
            next->incomingData(mux.output<SequenceValue::Transfer>());
    }
}

void EditCore::Worker::FinalTarget::incomingSkipped(const SequenceValue::Transfer &values,
                                                    void *origin)
{
    Multiplexer::Simple *target = lookupSkip(origin);
    Q_ASSERT(next);
    if (synchronize) {
        std::unique_lock<std::mutex> lock(worker->mutex);
        SequenceValue::Transfer result;
        if (target->incoming(values))
            mux.output(result);
        lock.unlock();
        next->incomingData(std::move(result));
    } else {
        if (target->incoming(values))
            next->incomingData(mux.output<SequenceValue::Transfer>());
    }
}

void EditCore::Worker::FinalTarget::incomingSkipped(SequenceValue &&value, void *origin)
{
    Multiplexer::Simple *target = lookupSkip(origin);
    Q_ASSERT(next);
    if (synchronize) {
        std::unique_lock<std::mutex> lock(worker->mutex);
        SequenceValue::Transfer result;
        if (target->incomingValue(std::move(value)))
            mux.output(result);
        lock.unlock();
        next->incomingData(std::move(result));
    } else {
        if (target->incomingValue(value))
            next->incomingData(mux.output<SequenceValue::Transfer>());
    }
}

void EditCore::Worker::FinalTarget::incomingSkipped(SequenceValue::Transfer &&values, void *origin)
{
    Multiplexer::Simple *target = lookupSkip(origin);
    Q_ASSERT(next);
    if (synchronize) {
        std::unique_lock<std::mutex> lock(worker->mutex);
        SequenceValue::Transfer result;
        if (target->incoming(std::move(values)))
            mux.output(result);
        lock.unlock();
        next->incomingData(std::move(result));
    } else {
        if (target->incoming(values))
            next->incomingData(mux.output<SequenceValue::Transfer>());
    }
}

void EditCore::Worker::FinalTarget::incomingSkipAdvance(double time, void *origin)
{
    Multiplexer::Simple *target = lookupSkip(origin);
    Q_ASSERT(next);
    if (synchronize) {
        std::unique_lock<std::mutex> lock(worker->mutex);
        SequenceValue::Transfer result;
        if (target->advance(time))
            mux.output(result);
        lock.unlock();
        next->incomingData(std::move(result));
    } else {
        if (target->advance(time))
            next->incomingData(mux.output<SequenceValue::Transfer>());
    }
}

void EditCore::Worker::FinalTarget::incomingSkipEnd(void *origin)
{
    auto check = dispatch.find(origin);
    if (check == dispatch.end())
        return;
    Q_ASSERT(next);
    auto target = check->second;
    dispatch.erase(check);
    if (synchronize) {
        std::unique_lock<std::mutex> lock(worker->mutex);
        SequenceValue::Transfer result;
        if (target->end())
            mux.output(result);
        lock.unlock();
        next->incomingData(std::move(result));
    } else {
        if (target->end())
            next->incomingData(mux.output<SequenceValue::Transfer>());
    }
}

void EditCore::Worker::FinalTarget::finalize()
{
    Q_ASSERT(next);
    mux.finishAll();
    next->incomingData(mux.output<SequenceValue::Transfer>());
    next->endData();
}


EditCore::SynchronizerOutput::SynchronizerOutput() = default;

EditCore::SynchronizerOutput::~SynchronizerOutput() = default;

void EditCore::SynchronizerOutput::process(SequenceValue::Transfer &&values)
{ egress->incomingData(std::move(values)); }


}
}
