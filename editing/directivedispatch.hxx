/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGDIRECTIVEDISPATCH_H
#define CPD3EDITINGDIRECTIVEDISPATCH_H

#include "core/first.hxx"

#include <vector>
#include <unordered_map>
#include <QtGlobal>
#include <QString>
#include <QHash>

#include "editing/editing.hxx"
#include "datacore/stream.hxx"
#include "editing/editdirective.hxx"

namespace CPD3 {
namespace Editing {

/**
 * This class handles dispatch within a stack of edit directives.
 * <br>
 * This class functions by maintaining a list of effective directives and
 * a set of bounds for which they are valid.  When it sees data that does
 * not fall within the bounds, it recalculates the effective directives.
 * Since most data is short non-overlapping values, this allows it to
 * avoid sending data to most edits most of the time.
 * <br>
 * The first bound is a start time upper limit that defines the latest
 * start time the effective set is valid for.  This bound is set to the next
 * start time of any directive that has already been passed in the input
 * checked start time.  That is, this is the first time a new directive
 * could come into effect.
 * <br>
 * The second bound is a range for which the end time is valid for.  That is,
 * the current effective set is not valid if an end time is seen that falls
 * outside of this range.  This range is set to the narrowest range defined
 * by the set of all start and ends of directives.  That is, take all starts
 * and end of all directives and find the closest upper and lower limits
 * that contain the inspected end time.
 */
class CPD3EDITING_EXPORT DirectiveDispatch : public EditDispatchTarget {
    std::vector<EditDirective *> allDirectives;
    EditDispatchSkipTarget *finalOutput;
    EditDispatchSkipTarget *originalFirst;
    EditDispatchSkipTarget *target;
    std::unordered_map<EditDirective *, EditDispatchSkipTarget *> skipInputs;
    bool isSkipTarget;

    double recalculateStartUpper;
    double recalculateEndLower;
    double recalculateEndUpper;
    int recalculateEndLowerThreshold;

    double advanceTime;

#ifndef NDEBUG
    double lastTime;
#endif

    bool shouldRecalculate(double start, double end) const;

    void recalculateDirectives(double start, double end);

public:
    /**
     * Create a new edit directive dispatcher.
     * 
     * @param directives    the directives
     * @param finalOutput   the final output of the chain
     */
    DirectiveDispatch(const std::vector<EditDirective *> &directives,
                      EditDispatchSkipTarget *finalOutput);

    virtual ~DirectiveDispatch();

    void incomingData(const Data::SequenceValue &value) override;

    void incomingData(const Data::SequenceValue::Transfer &values) override;

    void incomingData(Data::SequenceValue &&value) override;

    void incomingData(Data::SequenceValue::Transfer &&values) override;

    void incomingAdvance(double time) override;

    /**
     * Process the pending advance if any.
     */
    void processAdvance();
};

}
}

#endif
