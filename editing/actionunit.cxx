/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/actionunit.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Editing {

/** @file editing/actionunit.hxx
 * Edit directive actions that modify the data units.
 */


ActionUnitSet::ActionUnitSet(const SequenceMatch::Composite &sel, const SequenceName &t,
                             const Components &c,
                             bool aM) : selection(sel),
                                        target(t),
                                        applyToMeta(aM),
                                        actions(c),
                                        metadataUpdate(this),
                                        metadataTarget(t.isMeta() ? t : t.toMeta())
{ }

ActionUnitSet::~ActionUnitSet() = default;

ActionUnitSet::ActionUnitSet(const ActionUnitSet &other) : Action(other),
                                                           selection(other.selection),
                                                           target(other.target),
                                                           applyToMeta(other.applyToMeta),
                                                           actions(other.actions),
                                                           metadataUpdate(this),
                                                           metadataTarget(other.metadataTarget)
{ }

Action *ActionUnitSet::clone() const
{ return new ActionUnitSet(*this); }

void ActionUnitSet::unhandledInput(const SequenceName &unit,
                                   QList<EditDataTarget *> &inputs,
                                   QList<EditDataTarget *> &outputs,
                                   QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    Q_UNUSED(modifiers);
    if (unit.isMeta()) {
        if (!applyToMeta)
            return;
        if (!selection.matches(unit.fromMeta()))
            return;
        outputs.append(&metadataUpdate);
    } else {
        if (!selection.matches(unit))
            return;
        outputs.append(this);
    }
}

void ActionUnitSet::incomingDataAdvance(double time)
{ outputAdvance(time); }

void ActionUnitSet::incomingSequenceValue(const SequenceValue &value)
{
    SequenceIdentity id = value.getIdentity();
    if (actions & Apply_Station)
        id.setStation(target.getStation());
    if (actions & Apply_Archive)
        id.setArchive(target.getArchive());
    if (actions & Apply_Variable)
        id.setVariable(target.getVariable());
    if (actions & Apply_Flavors)
        id.getName().setFlavorsString(target.getFlavorsString());
    outputData(SequenceValue(std::move(id), value.root()));
}

ActionUnitSet::MetadataUpdate::MetadataUpdate(ActionUnitSet *p) : parent(p)
{ }

ActionUnitSet::MetadataUpdate::~MetadataUpdate() = default;

void ActionUnitSet::MetadataUpdate::incomingSequenceValue(const SequenceValue &value)
{ parent->incomingMetadata(value); }

void ActionUnitSet::incomingMetadata(const SequenceValue &value)
{
    SequenceIdentity id = value.getIdentity();
    if (actions & Apply_Station)
        id.setStation(metadataTarget.getStation());
    if (actions & Apply_Archive)
        id.setArchive(metadataTarget.getArchive());
    if (actions & Apply_Variable)
        id.setVariable(metadataTarget.getVariable());
    if (actions & Apply_Flavors)
        id.getName().setFlavorsString(metadataTarget.getFlavorsString());
    outputData(SequenceValue(std::move(id), value.root()));
}


ActionUnitDuplicate::ActionUnitDuplicate(const SequenceMatch::Composite &selection,
                                         const SequenceName &target,
                                         const ActionUnitSet::Components &components,
                                         bool applyToMeta) : ActionUnitSet(selection, target,
                                                                           components, applyToMeta),
                                                             forwarder(this)
{ }

ActionUnitDuplicate::~ActionUnitDuplicate() = default;

ActionUnitDuplicate::ActionUnitDuplicate(const ActionUnitDuplicate &other) : ActionUnitSet(other),
                                                                             forwarder(this)
{ }

Action *ActionUnitDuplicate::clone() const
{ return new ActionUnitDuplicate(*this); }

void ActionUnitDuplicate::unhandledInput(const SequenceName &unit,
                                         QList<EditDataTarget *> &inputs,
                                         QList<EditDataTarget *> &outputs,
                                         QList<EditDataModification *> &modifiers)
{
    ActionUnitSet::unhandledInput(unit, inputs, outputs, modifiers);
    if (!outputs.isEmpty())
        inputs.append(&forwarder);
}

ActionUnitDuplicate::Forwarder::Forwarder(ActionUnitDuplicate *p) : parent(p)
{ }

ActionUnitDuplicate::Forwarder::~Forwarder() = default;

void ActionUnitDuplicate::Forwarder::incomingSequenceValue(const SequenceValue &value)
{ parent->outputData(value); }


ActionUnitReplace::ActionUnitReplace(const SequenceMatch::Composite &sel,
                                     const QRegExp &inStation,
                                     const QString &outStation,
                                     const QRegExp &inArchive,
                                     const QString &outArchive,
                                     const QRegExp &inVariable,
                                     const QString &outVariable,
                                     const QRegExp &inFlavor,
                                     const QString &outFlavor,
                                     bool aM) : selection(sel),
                                                inputStation(inStation),
                                                outputStation(outStation),
                                                inputArchive(inArchive),
                                                outputArchive(outArchive),
                                                inputVariable(inVariable),
                                                outputVariable(outVariable),
                                                inputFlavor(inFlavor),
                                                outputFlavor(outFlavor),
                                                applyToMeta(aM),
                                                metadataUpdate(this)
{
    inputStation.setCaseSensitivity(Qt::CaseInsensitive);
    inputArchive.setCaseSensitivity(Qt::CaseInsensitive);
    inputFlavor.setCaseSensitivity(Qt::CaseInsensitive);
}

ActionUnitReplace::ActionUnitReplace(const ActionUnitReplace &other) : Action(other),
                                                                       selection(other.selection),
                                                                       inputStation(
                                                                               other.inputStation),
                                                                       outputStation(
                                                                               other.outputStation),
                                                                       inputArchive(
                                                                               other.inputArchive),
                                                                       outputArchive(
                                                                               other.outputArchive),
                                                                       inputVariable(
                                                                               other.inputVariable),
                                                                       outputVariable(
                                                                               other.outputVariable),
                                                                       inputFlavor(
                                                                               other.inputFlavor),
                                                                       outputFlavor(
                                                                               other.outputFlavor),
                                                                       applyToMeta(
                                                                               other.applyToMeta),
                                                                       metadataUpdate(this)
{ }

ActionUnitReplace::~ActionUnitReplace() = default;

Action *ActionUnitReplace::clone() const
{ return new ActionUnitReplace(*this); }

void ActionUnitReplace::unhandledInput(const SequenceName &unit,
                                       QList<EditDataTarget *> &inputs,
                                       QList<EditDataTarget *> &outputs,
                                       QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    Q_UNUSED(modifiers);

    if (applyToMeta && unit.isMeta()) {
        if (!selection.matches(unit.fromMeta()))
            return;
        outputs.append(&metadataUpdate);
        return;
    }
    if (!selection.matches(unit))
        return;
    outputs.append(this);
}

void ActionUnitReplace::incomingDataAdvance(double time)
{ outputAdvance(time); }

void ActionUnitReplace::modifyUnit(SequenceName &unit)
{
    if (inputStation.isValid() && !inputStation.pattern().isEmpty() && !outputStation.isEmpty()) {
        QString updated(unit.getStationQString());
        updated.replace(inputStation, outputStation);
        unit.setStation(updated);
    }
    if (inputArchive.isValid() && !inputArchive.pattern().isEmpty() && !outputArchive.isEmpty()) {
        QString updated(unit.getArchiveQString());
        updated.replace(inputArchive, outputArchive);
        unit.setArchive(updated);
    }
    if (inputVariable.isValid() &&
            !inputVariable.pattern().isEmpty() &&
            !outputVariable.isEmpty()) {
        QString updated(unit.getVariableQString());
        updated.replace(inputVariable, outputVariable);
        unit.setVariable(updated);
    }
    if (inputFlavor.isValid() && !inputFlavor.pattern().isEmpty()) {
        Data::SequenceName::Flavors original(unit.getFlavors());
        Data::SequenceName::Flavors result;
        result.reserve(original.size());
        for (const auto &fl : original) {
            QString updated(QString::fromStdString(fl));
            updated.replace(inputFlavor, outputFlavor);
            if (updated.isEmpty())
                continue;
            result.insert(updated.toStdString());
        }
        unit.setFlavors(std::move(result));
    }
}

void ActionUnitReplace::incomingSequenceValue(const SequenceValue &value)
{
    SequenceIdentity id = value.getIdentity();
    modifyUnit(id.getName());
    outputData(SequenceValue(std::move(id), value.root()));
}

void ActionUnitReplace::incomingMetadata(const SequenceValue &value)
{
    SequenceIdentity id = value.getIdentity();
    Q_ASSERT(id.getName().isMeta());
    id.getName().clearMeta();
    modifyUnit(id.getName());
    id.getName().setMeta();
    outputData(SequenceValue(std::move(id), value.root()));
}

ActionUnitReplace::MetadataUpdate::MetadataUpdate(ActionUnitReplace *p) : parent(p)
{ }

ActionUnitReplace::MetadataUpdate::~MetadataUpdate() = default;

void ActionUnitReplace::MetadataUpdate::incomingSequenceValue(const SequenceValue &value)
{ parent->incomingMetadata(value); }


ActionUnitReplaceDuplicate::ActionUnitReplaceDuplicate(const SequenceMatch::Composite &selection,
                                                       const QRegExp &inputStation,
                                                       const QString &outputStation,
                                                       const QRegExp &inputArchive,
                                                       const QString &outputArchive,
                                                       const QRegExp &inputVariable,
                                                       const QString &outputVariable,
                                                       const QRegExp &inputFlavor,
                                                       const QString &outputFlavor,
                                                       bool applyToMeta) : ActionUnitReplace(
        selection, inputStation, outputStation, inputArchive, outputArchive, inputVariable,
        outputVariable, inputFlavor, outputFlavor, applyToMeta), forwarder(this)
{ }

ActionUnitReplaceDuplicate::~ActionUnitReplaceDuplicate() = default;

ActionUnitReplaceDuplicate::ActionUnitReplaceDuplicate(const ActionUnitReplaceDuplicate &other)
        : ActionUnitReplace(other), forwarder(this)
{ }

Action *ActionUnitReplaceDuplicate::clone() const
{ return new ActionUnitReplaceDuplicate(*this); }

void ActionUnitReplaceDuplicate::unhandledInput(const SequenceName &unit,
                                                QList<EditDataTarget *> &inputs,
                                                QList<EditDataTarget *> &outputs,
                                                QList<EditDataModification *> &modifiers)
{
    ActionUnitReplace::unhandledInput(unit, inputs, outputs, modifiers);
    if (!outputs.isEmpty())
        inputs.append(&forwarder);
}

ActionUnitReplaceDuplicate::Forwarder::Forwarder(ActionUnitReplaceDuplicate *p) : parent(p)
{ }

ActionUnitReplaceDuplicate::Forwarder::~Forwarder() = default;

void ActionUnitReplaceDuplicate::Forwarder::incomingSequenceValue(const SequenceValue &value)
{ parent->outputData(value); }


ActionModifyFlavors::ActionModifyFlavors(const SequenceMatch::Composite &sel,
                                         const Data::SequenceName::Flavors &add,
                                         const Data::SequenceName::Flavors &remove,
                                         bool aM) : selection(sel),
                                                    addFlavors(add),
                                                    removeFlavors(remove),
                                                    applyToMeta(aM)
{ }

ActionModifyFlavors::~ActionModifyFlavors() = default;

ActionModifyFlavors::ActionModifyFlavors(const ActionModifyFlavors &other) : Action(other),
                                                                             selection(
                                                                                     other.selection),
                                                                             addFlavors(
                                                                                     other.addFlavors),
                                                                             removeFlavors(
                                                                                     other.removeFlavors),
                                                                             applyToMeta(
                                                                                     other.applyToMeta)
{ }

Action *ActionModifyFlavors::clone() const
{ return new ActionModifyFlavors(*this); }

void ActionModifyFlavors::unhandledInput(const SequenceName &unit,
                                         QList<EditDataTarget *> &inputs,
                                         QList<EditDataTarget *> &outputs,
                                         QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    Q_UNUSED(modifiers);

    if (unit.isMeta()) {
        if (!applyToMeta)
            return;
        if (!selection.matches(unit.fromMeta()))
            return;
    } else {
        if (!selection.matches(unit))
            return;
    }
    outputs.append(this);
}

void ActionModifyFlavors::incomingDataAdvance(double time)
{ outputAdvance(time); }

void ActionModifyFlavors::incomingSequenceValue(const SequenceValue &value)
{
    SequenceIdentity id = value.getIdentity();
    auto flavors = id.getFlavors();
    for (const auto &rem : removeFlavors) {
        flavors.erase(rem);
    }
    Util::merge(addFlavors, flavors);
    id.setFlavors(std::move(flavors));
    outputData(SequenceValue(std::move(id), value.root()));
}


}
}
