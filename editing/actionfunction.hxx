/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGACTIONFUNCTION_H
#define CPD3EDITINGACTIONFUNCTION_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QList>
#include <QRegExp>

#include "editing/editing.hxx"

#include "core/number.hxx"
#include "editing/action.hxx"
#include "datacore/segment.hxx"
#include "datacore/sequencematch.hxx"

namespace CPD3 {
namespace Editing {

/**
 * The base class for actions that apply a function of a single extra input to data.
 */
class CPD3EDITING_EXPORT ActionSingleInputFunction
        : public Action, public EditDataTarget, public EditDataModification {
    Data::SequenceMatch::Composite valueSelection;
    Data::SequenceMatch::OrderedLookup inputSelection;
    Calibration inputCalibration;
    std::string inputPath;
    Data::SequenceSegment::Stream reader;
    Data::SequenceName::Set processUnits;
    Data::SequenceName::Set inputUnits;

    void handleSegments(Data::SequenceSegment::Transfer &&segments);

public:
    /**
     * Create a new function action.
     * 
     * @param valueSelection    the value to apply to
     * @param inputSelection    the input to fetch
     * @param calibration       the calibration to apply to the input
     * @param path              the path to fetch the input from
     */
    ActionSingleInputFunction(const Data::SequenceMatch::Composite &valueSelection,
                              const Data::SequenceMatch::OrderedLookup &inputSelection,
                              const Calibration &calibration = Calibration(),
                              const std::string &path = std::string());

    virtual ~ActionSingleInputFunction();

    void unhandledInput(const Data::SequenceName &unit,
                        QList<EditDataTarget *> &inputs,
                        QList<EditDataTarget *> &outputs,
                        QList<EditDataModification *> &modifiers) override;

    void incomingSequenceValue(const Data::SequenceValue &value) override;

    void incomingDataAdvance(double time) override;

    void finalize() override;

    Data::SequenceName::Set requestedInputs() const override;

    void modifySequenceValue(Data::SequenceValue &value) override;

protected:
    ActionSingleInputFunction(const ActionSingleInputFunction &other);

    /**
     * Apply the function to a data value.
     *
     * @param value         the input value
     * @param parameter     the input parameter
     * @return              the function applied to the input
     */
    virtual double applyFunction(double value, double parameter) = 0;
};

/**
 * An action that adds a single value to another.
 */
class CPD3EDITING_EXPORT ActionAdd : public ActionSingleInputFunction {
public:
    /**
     * Create a new function action.
     *
     * @param valueSelection    the value to apply to
     * @param inputSelection    the input to fetch
     * @param calibration       the calibration to apply to the input
     * @param path              the path to fetch the input from
     */
    ActionAdd(const Data::SequenceMatch::Composite &valueSelection,
              const Data::SequenceMatch::OrderedLookup &inputSelection,
              const Calibration &calibration = Calibration(),
              const std::string &path = std::string());

    virtual ~ActionAdd();

    Action *clone() const override;

protected:
    ActionAdd(const ActionAdd &other);

    double applyFunction(double value, double parameter) override;
};

/**
 * An action that subtracts a single value to another.
 */
class CPD3EDITING_EXPORT ActionSubtract : public ActionSingleInputFunction {
public:
    /**
     * Create a new function action.
     *
     * @param valueSelection    the value to apply to
     * @param inputSelection    the input to fetch
     * @param calibration       the calibration to apply to the input
     * @param path              the path to fetch the input from
     */
    ActionSubtract(const Data::SequenceMatch::Composite &valueSelection,
                   const Data::SequenceMatch::OrderedLookup &inputSelection,
                   const Calibration &calibration = Calibration(),
                   const std::string &path = std::string());

    virtual ~ActionSubtract();

    Action *clone() const override;

protected:
    ActionSubtract(const ActionSubtract &other);

    double applyFunction(double value, double parameter) override;
};

/**
 * An action that multiplies a single value by another.
 */
class CPD3EDITING_EXPORT ActionMultiply : public ActionSingleInputFunction {
public:
    /**
     * Create a new function action.
     *
     * @param valueSelection    the value to apply to
     * @param inputSelection    the input to fetch
     * @param calibration       the calibration to apply to the input
     * @param path              the path to fetch the input from
     */
    ActionMultiply(const Data::SequenceMatch::Composite &valueSelection,
                   const Data::SequenceMatch::OrderedLookup &inputSelection,
                   const Calibration &calibration = Calibration(),
                   const std::string &path = std::string());

    virtual ~ActionMultiply();

    Action *clone() const override;

protected:
    ActionMultiply(const ActionMultiply &other);

    double applyFunction(double value, double parameter) override;
};

/**
 * An action that divides a single value by another.
 */
class CPD3EDITING_EXPORT ActionDivide : public ActionSingleInputFunction {
public:
    /**
     * Create a new function action.
     *
     * @param valueSelection    the value to apply to
     * @param inputSelection    the input to fetch
     * @param calibration       the calibration to apply to the input
     * @param path              the path to fetch the input from
     */
    ActionDivide(const Data::SequenceMatch::Composite &valueSelection,
                 const Data::SequenceMatch::OrderedLookup &inputSelection,
                 const Calibration &calibration = Calibration(),
                 const std::string &path = std::string());

    virtual ~ActionDivide();

    Action *clone() const override;

protected:
    ActionDivide(const ActionDivide &other);

    double applyFunction(double value, double parameter) override;
};

/**
 * An action that inverts a single value with another as the numerator.
 */
class CPD3EDITING_EXPORT ActionInvert : public ActionSingleInputFunction {
public:
    /**
     * Create a new function action.
     *
     * @param valueSelection    the value to apply to
     * @param inputSelection    the input to fetch
     * @param calibration       the calibration to apply to the input
     * @param path              the path to fetch the input from
     */
    ActionInvert(const Data::SequenceMatch::Composite &valueSelection,
                 const Data::SequenceMatch::OrderedLookup &inputSelection,
                 const Calibration &calibration = Calibration(),
                 const std::string &path = std::string());

    virtual ~ActionInvert();

    Action *clone() const override;

protected:
    ActionInvert(const ActionInvert &other);

    double applyFunction(double value, double parameter) override;
};

/**
 * An action that that assigns another value directly.
 */
class CPD3EDITING_EXPORT ActionAssign : public ActionSingleInputFunction {
public:
    /**
     * Create a new function action.
     *
     * @param valueSelection    the value to apply to
     * @param inputSelection    the input to fetch
     * @param calibration       the calibration to apply to the input
     * @param path              the path to fetch the input from
     */
    ActionAssign(const Data::SequenceMatch::Composite &valueSelection,
                 const Data::SequenceMatch::OrderedLookup &inputSelection,
                 const Calibration &calibration = Calibration(),
                 const std::string &path = std::string());

    virtual ~ActionAssign();

    Action *clone() const override;

protected:
    ActionAssign(const ActionAssign &other);

    double applyFunction(double value, double parameter) override;
};


}
}

#endif
