/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGTRIGGERCONDITION_H
#define CPD3EDITINGTRIGGERCONDITION_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QList>
#include <QVector>

#include "editing/editing.hxx"

#include "core/timeutils.hxx"
#include "datacore/stream.hxx"
#include "editing/trigger.hxx"
#include "editing/triggermerge.hxx"
#include "editing/triggerinput.hxx"

namespace CPD3 {
namespace Editing {

/**
 * The base class for conditional (dependent on input data) edit triggers.
 * This provides the base implementation provides the utilties to combine
 * a number of inputs into segments and calls a processing function to
 * convert those inputs into a boolean value.
 * 
 * @param N the number of inputs to the condition
 */
template<int N>
class TriggerCondition : public TriggerInputMerger<Internal::InputMergedFixed<N>,
                                                   TriggerSegmenting> {

    void processAllReady()
    {
        QVector<Internal::InputMergedFixed<N> > completed
                (TriggerInputMerger<Internal::InputMergedFixed<N>,
                                    TriggerSegmenting>::takeAllReady());
        for (typename QVector<Internal::InputMergedFixed<N> >::const_iterator
                add = completed.constBegin(), endAdd = completed.constEnd(); add != endAdd; ++add) {
            TriggerSegmenting::emitSegment(add->getStart(), add->getEnd(), convert(add->values));
        }
    }

public:
    virtual ~TriggerCondition() = default;

    virtual void finalize()
    {
        TriggerInputMerger<Internal::InputMergedFixed<N>, TriggerSegmenting>::finalize();
        processAllReady();
        TriggerSegmenting::finalize();
    }

    virtual bool isReady(double end) const
    {
        if (!TriggerInputMerger<Internal::InputMergedFixed<N>, TriggerSegmenting>::isReady(end))
            return false;
        return TriggerSegmenting::isReady(end);
    }

    virtual void incomingDataAdvance(double time)
    {
        TriggerInputMerger<Internal::InputMergedFixed<N>, TriggerSegmenting>::incomingDataAdvance(
                time);
        processAllReady();
        double adv = TriggerInputMerger<Internal::InputMergedFixed<N>,
                                        TriggerSegmenting>::advancedTime();
        if (FP::defined(adv))
            TriggerSegmenting::emitAdvance(adv);
    }

protected:
    /**
     * Create a new conditional input.  The inputs must be the same length
     * as the declared size.
     * 
     * @param inputs    the inputs to the condition
     */
    TriggerCondition(const QVector<TriggerInput *> &inputs) : TriggerInputMerger<
            Internal::InputMergedFixed<N>, TriggerSegmenting>(inputs)
    {
        Q_ASSERT(inputs.size() == N);
    }

    TriggerCondition(const TriggerCondition<N> &other) : TriggerInputMerger<
            Internal::InputMergedFixed<N>, TriggerSegmenting>(other)
    { }

    virtual bool process(double start, double end)
    {
        processAllReady();
        return TriggerSegmenting::process(start, end);
    }

    /**
     * Convert the merged data into a boolean value.
     * 
     * @param values    the merged values
     * @return          the truth value from evaluating the condition
     */
    virtual bool convert(const double values[N]) = 0;
};

/**
 * A less than inequality trigger.  True when the left is less than the
 * right.
 */
class CPD3EDITING_EXPORT TriggerLessThan : public TriggerCondition<2> {
public:
    /**
     * Create a new trigger.
     * 
     * @param left  the left hand side
     * @param right the right hand side
     */
    TriggerLessThan(TriggerInput *left, TriggerInput *right);

    virtual ~TriggerLessThan();

    virtual Trigger *clone() const;

protected:
    TriggerLessThan(const TriggerLessThan &other);

    virtual bool convert(const double values[2]);
};

/**
 * A less than inequality trigger.  True when the left is less than or equal to
 * the right.
 */
class CPD3EDITING_EXPORT TriggerLessThanEqual : public TriggerCondition<2> {
public:
    /**
     * Create a new trigger.
     * 
     * @param left  the left hand side
     * @param right the right hand side
     */
    TriggerLessThanEqual(TriggerInput *left, TriggerInput *right);

    virtual ~TriggerLessThanEqual();

    virtual Trigger *clone() const;

protected:
    TriggerLessThanEqual(const TriggerLessThanEqual &other);

    virtual bool convert(const double values[2]);
};

/**
 * A less than inequality trigger.  True when the greater is less than the
 * right.
 */
class CPD3EDITING_EXPORT TriggerGreaterThan : public TriggerCondition<2> {
public:
    /**
     * Create a new trigger.
     * 
     * @param left  the left hand side
     * @param right the right hand side
     */
    TriggerGreaterThan(TriggerInput *left, TriggerInput *right);

    virtual ~TriggerGreaterThan();

    virtual Trigger *clone() const;

protected:
    TriggerGreaterThan(const TriggerGreaterThan &other);

    virtual bool convert(const double values[2]);
};

/**
 * A less than inequality trigger.  True when the left is greater than or 
 * equal to the right.
 */
class CPD3EDITING_EXPORT TriggerGreaterThanEqual : public TriggerCondition<2> {
public:
    /**
     * Create a new trigger.
     * 
     * @param left  the left hand side
     * @param right the right hand side
     */
    TriggerGreaterThanEqual(TriggerInput *left, TriggerInput *right);

    virtual ~TriggerGreaterThanEqual();

    virtual Trigger *clone() const;

protected:
    TriggerGreaterThanEqual(const TriggerGreaterThanEqual &other);

    virtual bool convert(const double values[2]);
};

/**
 * An equality test trigger.  True when the inputs are equal.
 */
class CPD3EDITING_EXPORT TriggerEqual : public TriggerCondition<2> {
public:
    /**
     * Create a new trigger.
     * 
     * @param left  the left hand side
     * @param right the right hand side
     */
    TriggerEqual(TriggerInput *left, TriggerInput *right);

    virtual ~TriggerEqual();

    virtual Trigger *clone() const;

protected:
    TriggerEqual(const TriggerEqual &other);

    virtual bool convert(const double values[2]);
};

/**
 * An not equal test trigger.  True when the inputs are not equal.
 */
class CPD3EDITING_EXPORT TriggerNotEqual : public TriggerCondition<2> {
public:
    /**
     * Create a new trigger.
     * 
     * @param left  the left hand side
     * @param right the right hand side
     */
    TriggerNotEqual(TriggerInput *left, TriggerInput *right);

    virtual ~TriggerNotEqual();

    virtual Trigger *clone() const;

protected:
    TriggerNotEqual(const TriggerNotEqual &other);

    virtual bool convert(const double values[2]);
};

/**
 * A range test trigger.  True when the middle input is greater than
 * the start and less than the end.
 */
class CPD3EDITING_EXPORT TriggerInsideRange : public TriggerCondition<3> {
public:
    /**
     * Create a new trigger.
     * 
     * @param start     the lower bound
     * @param middle    the middle test
     * @param end       the upper bound
     */
    TriggerInsideRange(TriggerInput *start, TriggerInput *middle, TriggerInput *end);

    virtual ~TriggerInsideRange();

    virtual Trigger *clone() const;

protected:
    TriggerInsideRange(const TriggerInsideRange &other);

    virtual bool convert(const double values[3]);
};

/**
 * A range test trigger.  True when the middle input is less than
 * the start or greater than the end.
 */
class CPD3EDITING_EXPORT TriggerOutsideRange : public TriggerCondition<3> {
public:
    /**
     * Create a new trigger.
     * 
     * @param start     the lower bound
     * @param middle    the middle test
     * @param end       the upper bound
     */
    TriggerOutsideRange(TriggerInput *start, TriggerInput *middle, TriggerInput *end);

    virtual ~TriggerOutsideRange();

    virtual Trigger *clone() const;

protected:
    TriggerOutsideRange(const TriggerOutsideRange &other);

    virtual bool convert(const double values[3]);
};

/**
 * A range test trigger.  True when the middle is inside the start and end
 * range as defined by the modular bounds.  That is, values are wrapped
 * between the limits of the range (e.x. a wind angle).
 */
class CPD3EDITING_EXPORT TriggerInsideModularRange : public TriggerCondition<5> {
public:
    /**
     * Create a new trigger.
     * 
     * @param start     the lower bound
     * @param middle    the middle test
     * @param end       the upper bound
     * @param lower     the lower modular limit
     * @param upper     the upper modular limit
     */
    TriggerInsideModularRange(TriggerInput *start,
                              TriggerInput *middle,
                              TriggerInput *end,
                              TriggerInput *lower,
                              TriggerInput *upper);

    virtual ~TriggerInsideModularRange();

    virtual Trigger *clone() const;

protected:
    TriggerInsideModularRange(const TriggerInsideModularRange &other);

    virtual bool convert(const double values[5]);
};

/**
 * A range test trigger.  True when the middle is outside the start and end
 * range as defined by the modular bounds.  That is, values are wrapped
 * between the limits of the range (e.x. a wind angle).
 */
class CPD3EDITING_EXPORT TriggerOutsideModularRange : public TriggerCondition<5> {
public:
    /**
     * Create a new trigger.
     * 
     * @param start     the lower bound
     * @param middle    the middle test
     * @param end       the upper bound
     * @param lower     the lower modular limit
     * @param upper     the upper modular limit
     */
    TriggerOutsideModularRange(TriggerInput *start,
                               TriggerInput *middle,
                               TriggerInput *end,
                               TriggerInput *lower,
                               TriggerInput *upper);

    virtual ~TriggerOutsideModularRange();

    virtual Trigger *clone() const;

protected:
    TriggerOutsideModularRange(const TriggerOutsideModularRange &other);

    virtual bool convert(const double values[5]);
};


/**
 * The base class for simple conditions that just transform the output
 * of a single input into a boolean segment.
 */
class CPD3EDITING_EXPORT TriggerSimpleCondition : public TriggerSegmenting {
    TriggerInput *input;

    void processAllReady();

public:
    virtual ~TriggerSimpleCondition();

    virtual bool isReady(double end) const;

    virtual void finalize();

    virtual void extendProcessing(double &start, double &end) const;

    virtual QList<EditDataTarget *> unhandledInput(const Data::SequenceName &unit);

    virtual void incomingDataAdvance(double time);

protected:
    /**
     * Create the condition.  This takes ownership of the input
     * 
     * @param input the input
     */
    TriggerSimpleCondition(TriggerInput *input);

    TriggerSimpleCondition(const TriggerSimpleCondition &other);

    virtual bool process(double start, double end);

    /**
     * Convert the value into a truth value.
     * 
     * @param value the value to convert
     * @return      the converted truth value
     */
    virtual bool convert(double value) = 0;
};

/**
 * A defined test trigger.  Returns true if the input is defined.
 */
class CPD3EDITING_EXPORT TriggerDefined : public TriggerSimpleCondition {
public:
    /**
     * Create a new trigger.
     * 
     * @param input     the input to test
     */
    TriggerDefined(TriggerInput *input);

    virtual ~TriggerDefined();

    virtual Trigger *clone() const;

protected:
    TriggerDefined(const TriggerDefined &other);

    virtual bool convert(double value);
};

/**
 * An undefined test trigger.  Returns true if the input is not defined.
 */
class CPD3EDITING_EXPORT TriggerUndefined : public TriggerSimpleCondition {
public:
    /**
     * Create a new trigger.
     * 
     * @param input     the input to test
     */
    TriggerUndefined(TriggerInput *input);

    virtual ~TriggerUndefined();

    virtual Trigger *clone() const;

protected:
    TriggerUndefined(const TriggerUndefined &other);

    virtual bool convert(double value);
};

}
}

#endif
