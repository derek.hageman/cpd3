/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "editing/triggercondition.hxx"

namespace CPD3 {
namespace Editing {

/** @file editing/triggercondition.hxx
 * Conditional (dependent on input value) triggers.
 */


TriggerLessThan::TriggerLessThan(TriggerInput *left, TriggerInput *right) : TriggerCondition<2>(
        QVector<TriggerInput *>() << left << right)
{ }

TriggerLessThan::TriggerLessThan(const TriggerLessThan &other) : TriggerCondition<2>(other)
{ }

TriggerLessThan::~TriggerLessThan()
{ }

Trigger *TriggerLessThan::clone() const
{ return new TriggerLessThan(*this); }

bool TriggerLessThan::convert(const double values[2])
{
    if (!FP::defined(values[0]) || !FP::defined(values[1]))
        return false;
    return values[0] < values[1];
}

TriggerLessThanEqual::TriggerLessThanEqual(TriggerInput *left, TriggerInput *right)
        : TriggerCondition<2>(QVector<TriggerInput *>() << left << right)
{ }

TriggerLessThanEqual::TriggerLessThanEqual(const TriggerLessThanEqual &other) : TriggerCondition<2>(
        other)
{ }

TriggerLessThanEqual::~TriggerLessThanEqual()
{ }

Trigger *TriggerLessThanEqual::clone() const
{ return new TriggerLessThanEqual(*this); }

bool TriggerLessThanEqual::convert(const double values[2])
{
    if (!FP::defined(values[0]) || !FP::defined(values[1]))
        return false;
    return values[0] <= values[1];
}

TriggerGreaterThan::TriggerGreaterThan(TriggerInput *left, TriggerInput *right) : TriggerCondition<
        2>(QVector<TriggerInput *>() << left << right)
{ }

TriggerGreaterThan::TriggerGreaterThan(const TriggerGreaterThan &other) : TriggerCondition<2>(other)
{ }

TriggerGreaterThan::~TriggerGreaterThan()
{ }

Trigger *TriggerGreaterThan::clone() const
{ return new TriggerGreaterThan(*this); }

bool TriggerGreaterThan::convert(const double values[2])
{
    if (!FP::defined(values[0]) || !FP::defined(values[1]))
        return false;
    return values[0] > values[1];
}

TriggerGreaterThanEqual::TriggerGreaterThanEqual(TriggerInput *left, TriggerInput *right)
        : TriggerCondition<2>(QVector<TriggerInput *>() << left << right)
{ }

TriggerGreaterThanEqual::TriggerGreaterThanEqual(const TriggerGreaterThanEqual &other)
        : TriggerCondition<2>(other)
{ }

TriggerGreaterThanEqual::~TriggerGreaterThanEqual()
{ }

Trigger *TriggerGreaterThanEqual::clone() const
{ return new TriggerGreaterThanEqual(*this); }

bool TriggerGreaterThanEqual::convert(const double values[2])
{
    if (!FP::defined(values[0]) || !FP::defined(values[1]))
        return false;
    return values[0] >= values[1];
}

TriggerEqual::TriggerEqual(TriggerInput *left, TriggerInput *right) : TriggerCondition<2>(
        QVector<TriggerInput *>() << left << right)
{ }

TriggerEqual::TriggerEqual(const TriggerEqual &other) : TriggerCondition<2>(other)
{ }

TriggerEqual::~TriggerEqual()
{ }

Trigger *TriggerEqual::clone() const
{ return new TriggerEqual(*this); }

bool TriggerEqual::convert(const double values[2])
{
    if (!FP::defined(values[0]) || !FP::defined(values[1]))
        return false;
    return values[0] == values[1];
}

TriggerNotEqual::TriggerNotEqual(TriggerInput *left, TriggerInput *right) : TriggerCondition<2>(
        QVector<TriggerInput *>() << left << right)
{ }

TriggerNotEqual::TriggerNotEqual(const TriggerNotEqual &other) : TriggerCondition<2>(other)
{ }

TriggerNotEqual::~TriggerNotEqual()
{ }

Trigger *TriggerNotEqual::clone() const
{ return new TriggerNotEqual(*this); }

bool TriggerNotEqual::convert(const double values[2])
{
    if (!FP::defined(values[0]) || !FP::defined(values[1]))
        return false;
    return values[0] != values[1];
}

TriggerInsideRange::TriggerInsideRange(TriggerInput *start, TriggerInput *middle, TriggerInput *end)
        : TriggerCondition<3>(QVector<TriggerInput *>() << middle << start << end)
{ }

TriggerInsideRange::TriggerInsideRange(const TriggerInsideRange &other) : TriggerCondition<3>(other)
{ }

TriggerInsideRange::~TriggerInsideRange()
{ }

Trigger *TriggerInsideRange::clone() const
{ return new TriggerInsideRange(*this); }

bool TriggerInsideRange::convert(const double values[3])
{
    if (!FP::defined(values[0]) || !FP::defined(values[1]) || !FP::defined(values[2]))
        return false;
    return values[1] < values[0] && values[0] < values[2];
}

TriggerOutsideRange::TriggerOutsideRange(TriggerInput *start,
                                         TriggerInput *middle,
                                         TriggerInput *end) : TriggerCondition<3>(
        QVector<TriggerInput *>() << middle << start << end)
{ }

TriggerOutsideRange::TriggerOutsideRange(const TriggerOutsideRange &other) : TriggerCondition<3>(
        other)
{ }

TriggerOutsideRange::~TriggerOutsideRange()
{ }

Trigger *TriggerOutsideRange::clone() const
{ return new TriggerOutsideRange(*this); }

bool TriggerOutsideRange::convert(const double values[3])
{
    if (!FP::defined(values[0]) || !FP::defined(values[1]) || !FP::defined(values[2]))
        return false;
    return values[0] < values[1] || values[0] > values[2];
}


static double limitIncomingModulus(double value, double lower, double upper, double range)
{
    Q_ASSERT(range > 0.0);
    if (value < lower) {
        value += (double) abs((int) floor((value - lower) / range)) * range;
    }
    if (value >= upper) {
        value -= (double) abs((int) floor((value - lower) / range)) * range;
    }
    return value;
}

TriggerInsideModularRange::TriggerInsideModularRange(TriggerInput *start,
                                                     TriggerInput *middle,
                                                     TriggerInput *end,
                                                     TriggerInput *lower,
                                                     TriggerInput *upper) : TriggerCondition<5>(
        QVector<TriggerInput *>() << middle << start << end << lower << upper)
{ }

TriggerInsideModularRange::TriggerInsideModularRange(const TriggerInsideModularRange &other)
        : TriggerCondition<5>(other)
{ }

TriggerInsideModularRange::~TriggerInsideModularRange()
{ }

Trigger *TriggerInsideModularRange::clone() const
{ return new TriggerInsideModularRange(*this); }

bool TriggerInsideModularRange::convert(const double values[5])
{
    if (!FP::defined(values[0]) ||
            !FP::defined(values[1]) ||
            !FP::defined(values[2]) ||
            !FP::defined(values[3]) ||
            !FP::defined(values[4]))
        return false;
    double lower = values[3];
    double upper = values[4];
    double range = upper - lower;
    if (range <= 0.0)
        return false;
    double v = limitIncomingModulus(values[0], lower, upper, range);
    double min = values[1];
    double max = values[2];
    if (min < max)
        return min < v && v < max;
    return v > min || v < max;
}

TriggerOutsideModularRange::TriggerOutsideModularRange(TriggerInput *start,
                                                       TriggerInput *middle,
                                                       TriggerInput *end,
                                                       TriggerInput *lower,
                                                       TriggerInput *upper) : TriggerCondition<5>(
        QVector<TriggerInput *>() << middle << start << end << lower << upper)
{ }

TriggerOutsideModularRange::TriggerOutsideModularRange(const TriggerOutsideModularRange &other)
        : TriggerCondition<5>(other)
{ }

TriggerOutsideModularRange::~TriggerOutsideModularRange()
{ }

Trigger *TriggerOutsideModularRange::clone() const
{ return new TriggerOutsideModularRange(*this); }

bool TriggerOutsideModularRange::convert(const double values[5])
{
    if (!FP::defined(values[0]) ||
            !FP::defined(values[1]) ||
            !FP::defined(values[2]) ||
            !FP::defined(values[3]) ||
            !FP::defined(values[4]))
        return false;
    double lower = values[3];
    double upper = values[4];
    double range = upper - lower;
    if (range <= 0.0)
        return false;
    double v = limitIncomingModulus(values[0], lower, upper, range);
    double min = values[1];
    double max = values[2];
    if (min < max)
        return v < min || v > max;
    return v > max && v < min;
}


TriggerSimpleCondition::TriggerSimpleCondition(TriggerInput *in) : input(in)
{ }

TriggerSimpleCondition::TriggerSimpleCondition(const TriggerSimpleCondition &other)
        : TriggerSegmenting(other), input(other.input->clone())
{ }

TriggerSimpleCondition::~TriggerSimpleCondition()
{ delete input; }

bool TriggerSimpleCondition::isReady(double end) const
{ return TriggerSegmenting::isReady(end) && input->isReady(end); }

void TriggerSimpleCondition::finalize()
{
    input->finalize();
    processAllReady();
    TriggerSegmenting::finalize();
}

void TriggerSimpleCondition::extendProcessing(double &start, double &end) const
{ input->extendProcessing(start, end); }

QList<EditDataTarget *> TriggerSimpleCondition::unhandledInput(const Data::SequenceName &unit)
{ return input->unhandledInput(unit); }

void TriggerSimpleCondition::incomingDataAdvance(double time)
{
    input->incomingDataAdvance(time);
    processAllReady();
    double adv = input->advancedTime();
    if (FP::defined(adv)) {
        TriggerSegmenting::emitAdvance(adv);
    }
}

bool TriggerSimpleCondition::process(double start, double end)
{
    processAllReady();
    return TriggerSegmenting::process(start, end);
}

void TriggerSimpleCondition::processAllReady()
{
    QVector<TriggerInput::Result> incoming(input->process());
    for (QVector<TriggerInput::Result>::const_iterator add = incoming.constBegin(),
            endAdd = incoming.constEnd(); add != endAdd; ++add) {
        TriggerSegmenting::emitSegment(add->getStart(), add->getEnd(), convert(add->getValue()));
    }
}

TriggerDefined::TriggerDefined(TriggerInput *input) : TriggerSimpleCondition(input)
{ }

TriggerDefined::TriggerDefined(const TriggerDefined &other) : TriggerSimpleCondition(other)
{ }

TriggerDefined::~TriggerDefined()
{ }

Trigger *TriggerDefined::clone() const
{ return new TriggerDefined(*this); }

bool TriggerDefined::convert(double value)
{ return FP::defined(value); }

TriggerUndefined::TriggerUndefined(TriggerInput *input) : TriggerSimpleCondition(input)
{ }

TriggerUndefined::TriggerUndefined(const TriggerUndefined &other) : TriggerSimpleCondition(other)
{ }

TriggerUndefined::~TriggerUndefined()
{ }

Trigger *TriggerUndefined::clone() const
{ return new TriggerUndefined(*this); }

bool TriggerUndefined::convert(double value)
{ return !FP::defined(value); }

}
}
