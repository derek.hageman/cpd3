/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <vector>
#include <algorithm>

#include "editing/triggerbuffer.hxx"
#include "algorithms/statistics.hxx"
#include "algorithms/leastsquares.hxx"

namespace CPD3 {
namespace Editing {

/** @file editing/triggerbuffer.hxx
 * Routines for handling the buffering components of trigger inputs.
 */


TriggerInputBuffer::TriggerInputBuffer(TriggerInput *i, const Data::Variant::Read &c) : input(i),
                                                                                        result()
{
    configureBuffer(c);
}

TriggerInputBuffer::TriggerInputBuffer(const TriggerInputBuffer &other) : TriggerInput(other),
                                                                          TriggerBuffer<
                                                                                  TriggerInput::Result,
                                                                                  TriggerInput::Result>(),
                                                                          input(other.input
                                                                                     ->clone()),
                                                                          result()
{
    copyBufferParameters(other);
}

TriggerInputBuffer::~TriggerInputBuffer()
{
    delete input;
}

double TriggerInputBuffer::advancedTime() const
{ return bufferAdvancedTime(); }

bool TriggerInputBuffer::isReady(double end) const
{ return isBufferReady(end) && input->isReady(end); }

void TriggerInputBuffer::extendProcessing(double &start, double &end) const
{
    extendBufferProcessing(start, end);
    input->extendProcessing(start, end);
}

QList<EditDataTarget *> TriggerInputBuffer::unhandledInput(const Data::SequenceName &unit)
{ return input->unhandledInput(unit); }

void TriggerInputBuffer::incomingDataAdvance(double time)
{
    input->incomingDataAdvance(time);

    processPending();
    double adv = input->advancedTime();
    if (FP::defined(adv)) {
        result << incomingBufferAdvance(adv);
    }
}

void TriggerInputBuffer::processPending()
{
    QVector<Result> add(input->process());
    if (add.isEmpty())
        return;
    result << incomingBufferData(add);

    /* Special handling so we catch constant values */
    if (!FP::defined(add.last().getEnd())) {
        if (input->isReady(FP::undefined()))
            result << endBufferData();
    }
}

QVector<TriggerInput::Result> TriggerInputBuffer::process()
{
    processPending();
    if (result.isEmpty())
        return result;
    QVector<Result> ret(result);
    result.clear();
    return ret;
}

void TriggerInputBuffer::finalize()
{
    input->finalize();
    processPending();
    result << endBufferData();
}


TriggerInputMean::TriggerInputMean(TriggerInput *input, const Data::Variant::Read &config)
        : TriggerInputBuffer(input, config)
{ }

TriggerInputMean::~TriggerInputMean()
{ }

TriggerInputMean::TriggerInputMean(const TriggerInputMean &other) : TriggerInputBuffer(other)
{ }

TriggerInput *TriggerInputMean::clone() const
{ return new TriggerInputMean(*this); }

TriggerInput::Result TriggerInputMean::convert(const QVector<Result> &buffer, const Result &active)
{
    size_t count = 0;
    double sum = 0;
    for (QVector<Result>::const_iterator add = buffer.constBegin(), endAdd = buffer.constEnd();
            add != endAdd;
            ++add) {
        if (!FP::defined(add->value))
            continue;
        count++;
        sum += add->value;
    }
    if (count == 0)
        return Result(active.start, active.end, FP::undefined());
    return Result(active.start, active.end, sum / (double) count);
}

TriggerInputSD::TriggerInputSD(TriggerInput *input, const Data::Variant::Read &config)
        : TriggerInputBuffer(
        input, config)
{ }

TriggerInputSD::~TriggerInputSD()
{ }

TriggerInputSD::TriggerInputSD(const TriggerInputSD &other) : TriggerInputBuffer(other)
{ }

TriggerInput *TriggerInputSD::clone() const
{ return new TriggerInputSD(*this); }

TriggerInput::Result TriggerInputSD::convert(const QVector<Result> &buffer, const Result &active)
{
    size_t count = 0;
    double sum = 0;
    for (QVector<Result>::const_iterator add = buffer.constBegin(), endAdd = buffer.constEnd();
            add != endAdd;
            ++add) {
        if (!FP::defined(add->value))
            continue;
        count++;
        sum += add->value;
    }
    if (count < 2)
        return Result(active.start, active.end, FP::undefined());
    double mean = sum / (double) count;


    count = 0;
    sum = 0;
    for (QVector<Result>::const_iterator add = buffer.constBegin(), endAdd = buffer.constEnd();
            add != endAdd;
            ++add) {
        if (!FP::defined(add->value))
            continue;
        count++;
        double v = add->value - mean;
        sum += v * v;
    }
    Q_ASSERT(count >= 2);
    return Result(active.start, active.end, sqrt(sum / (double) (count - 1)));
}


TriggerInputQuantile::TriggerInputQuantile(TriggerInput *input,
                                           double q,
                                           const Data::Variant::Read &config)
        : TriggerInputBuffer(input, config), quantile(q)
{
    quantile = qBound(0.0, quantile, 1.0);
}

TriggerInputQuantile::~TriggerInputQuantile()
{ }

TriggerInputQuantile::TriggerInputQuantile(const TriggerInputQuantile &other) : TriggerInputBuffer(
        other), quantile(other.quantile)
{ }

TriggerInput *TriggerInputQuantile::clone() const
{ return new TriggerInputQuantile(*this); }

TriggerInput::Result TriggerInputQuantile::convert(const QVector<Result> &buffer,
                                                   const Result &active)
{
    std::vector<double> values;
    for (QVector<Result>::const_iterator add = buffer.constBegin(), endAdd = buffer.constEnd();
            add != endAdd;
            ++add) {
        if (!FP::defined(add->value))
            continue;
        values.push_back(add->value);
    }
    std::sort(values.begin(), values.end());
    return Result(active.start, active.end, Algorithms::Statistics::quantile(values, quantile));
}


TriggerInputSlope::TriggerInputSlope(TriggerInput *input, const Data::Variant::Read &config)
        : TriggerInputBuffer(input, config)
{ }

TriggerInputSlope::~TriggerInputSlope()
{ }

TriggerInputSlope::TriggerInputSlope(const TriggerInputSlope &other) : TriggerInputBuffer(other)
{ }

TriggerInput *TriggerInputSlope::clone() const
{ return new TriggerInputSlope(*this); }

TriggerInput::Result TriggerInputSlope::convert(const QVector<Result> &buffer, const Result &active)
{
    if (buffer.size() < 2)
        return Result(active.start, active.end, FP::undefined());
    QVector<double> x;
    QVector<double> y;
    for (QVector<Result>::const_iterator add = buffer.constBegin(), endAdd = buffer.constEnd();
            add != endAdd;
            ++add) {
        if (!FP::defined(add->value))
            continue;
        double ax;
        if (!FP::defined(add->start)) {
            ax = add->end;
            if (!FP::defined(ax))
                continue;
        } else if (!FP::defined(add->end)) {
            ax = add->start;
        } else {
            ax = (add->end + add->start) * 0.5;
        }
        x.append(ax);
        y.append(add->value);
    }
    if (x.size() < 2)
        return Result(active.start, active.end, FP::undefined());
    Algorithms::BasicOrdinaryLeastSquares ols(x, y);
    return Result(active.start, active.end, ols.slope());
}


TriggerInputLength::TriggerInputLength(TriggerInput *input, const Data::Variant::Read &config)
        : TriggerInputBuffer(input, config)
{ }

TriggerInputLength::~TriggerInputLength()
{ }

TriggerInputLength::TriggerInputLength(const TriggerInputLength &other) : TriggerInputBuffer(other)
{ }

TriggerInput *TriggerInputLength::clone() const
{ return new TriggerInputLength(*this); }

TriggerInput::Result TriggerInputLength::convert(const QVector<Result> &buffer,
                                                 const Result &active)
{
    if (buffer.empty())
        return Result(active.start, active.end, FP::undefined());
    double s = buffer.first().getStart();
    double e = buffer.last().getEnd();
    if (!FP::defined(e))
        e = buffer.last().getStart();
    if (!FP::defined(s) || !FP::defined(e) || s > e)
        return Result(active.start, active.end, FP::undefined());
    return Result(active.start, active.end, e - s);
}

}
}
