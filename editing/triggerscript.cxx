/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "editing/triggerscript.hxx"
#include "luascript/libs/sequencesegment.hxx"


Q_LOGGING_CATEGORY(log_editing_triggerscript, "cpd3.editing.triggerscript", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace Editing {

/** @file editing/triggerscript.hxx
 * Triggering based on script code evaluation.
 */

void TriggerScript::init()
{
    buffer.pushExternalController(root);
    controller = root.back();
    environment = root.pushSandboxEnvironment();
    {
        Lua::Engine::Assign assign(root, environment, "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_editing_triggerscript) << entry[i].toOutputString();
            }
        }));
    }
    environment.set("control", controller);
    if (!root.pushChunk(code, environment)) {
        qCDebug(log_editing_triggerscript) << "Error parsing trigger script code:"
                                           << root.errorDescription();
        root.clearError();
        return;
    }
    invoke = root.back();
}

TriggerScript::TriggerScript(std::string code, SequenceMatch::OrderedLookup selection) : code(
        std::move(code)),
                                                                                         selection(
                                                                                                 selection),
                                                                                         engine(),
                                                                                         root(engine),
                                                                                         buffer(*this),
                                                                                         inputs(std::move(
                                                                                                 selection))
{ init(); }

TriggerScript::~TriggerScript() = default;

TriggerScript::TriggerScript(const TriggerScript &other) : TriggerSegmenting(other),
                                                           code(other.code),
                                                           selection(other.selection),
                                                           engine(),
                                                           root(engine),
                                                           buffer(*this),
                                                           inputs(selection)
{ init(); }

Trigger *TriggerScript::clone() const
{ return new TriggerScript(*this); }

bool TriggerScript::shouldRunDetached() const
{ return true; }

QList<EditDataTarget *> TriggerScript::unhandledInput(const Data::SequenceName &name)
{
    QList<EditDataTarget *> result;
    if (inputs.registerInput(name))
        result.append(this);
    return result;
}

SequenceName::Set TriggerScript::requestedInputs() const
{ return inputs.knownInputs(); }

namespace {
class OutputSegment : public Lua::Libs::SequenceSegment {
    bool result;
public:

    OutputSegment() : result(false)
    { }

    virtual ~OutputSegment() = default;

    explicit OutputSegment(const CPD3::Data::SequenceSegment &segment) : Lua::Libs::SequenceSegment(
            segment), result(false)
    { }

    explicit OutputSegment(CPD3::Data::SequenceSegment &&segment) : Lua::Libs::SequenceSegment(
            std::move(segment)), result(false)
    { }

    void setResult(bool r)
    { result = r; }

    bool getResult() const
    { return result; }
};
}

void TriggerScript::process(bool ignoreEnd)
{
    for (;;) {
        Lua::Engine::Frame local(root);
        if (!buffer.pushExternal(local, controller, ignoreEnd))
            return;
        auto current = local.back();
        environment.set("data", current);

        Lua::Engine::Call call(local);
        call.push(invoke);
        if (!call.execute(1)) {
            call.clearError();
            return;
        }
        if (!call.back().isNil()) {
            if (auto seg = current.toData<OutputSegment>()) {
                seg->setResult(call.back().toBoolean());
            }
        }
    }
}

void TriggerScript::incomingSequenceValue(const SequenceValue &value)
{
    Util::append(reader.add(value), buffer.pending);
    process();
}

void TriggerScript::incomingDataAdvance(double time)
{
    Util::append(reader.advance(time), buffer.pending);
    buffer.advance = time;
    process();
}

void TriggerScript::finalize()
{
    Util::append(reader.finish(), buffer.pending);
    process(false);
    buffer.finish(root, controller);
}

void TriggerScript::bufferEnd()
{ TriggerSegmenting::finalize(); }

TriggerScript::Buffer::Buffer(TriggerScript &parent) : parent(parent),
                                                       offset(0),
                                                       advance(FP::undefined())
{ }

TriggerScript::Buffer::~Buffer() = default;

bool TriggerScript::Buffer::pushNext(Lua::Engine::Frame &target)
{
    if (offset < pending.size()) {
        target.pushData<OutputSegment>(std::move(pending[offset]));
        ++offset;
        return true;
    }
    offset = 0;
    pending.clear();

    if (FP::defined(advance)) {
        target.push(advance);
        advance = FP::undefined();
        return true;
    }

    return false;
}

double TriggerScript::Buffer::getStart(Lua::Engine::Frame &frame, const Lua::Engine::Reference &ref)
{
    auto value = ref.toData<OutputSegment>();
    if (!value)
        return FP::undefined();
    return value->get().getStart();
}

void TriggerScript::Buffer::convertFromLua(Lua::Engine::Frame &frame)
{
    if (frame.back().toData<OutputSegment>())
        return;
    auto value = Lua::Libs::SequenceSegment::extract(frame, frame.back());
    frame.pop();
    frame.pushData<OutputSegment>(std::move(value));
}

void TriggerScript::Buffer::outputReady(Lua::Engine::Frame &frame,
                                        const Lua::Engine::Reference &ref)
{
    auto value = ref.toData<OutputSegment>();
    if (!value)
        return;

    parent.emitSegment(value->get().getStart(), value->get().getEnd(), value->getResult());
}

void TriggerScript::Buffer::advanceReady(double time)
{ parent.emitAdvance(time); }

void TriggerScript::Buffer::endReady()
{ parent.bufferEnd(); }

bool TriggerScript::Buffer::bufferAssigned(Lua::Engine::Frame &frame,
                                           const Lua::Engine::Reference &target,
                                           const Lua::Engine::Reference &value)
{
    if (value.getType() != Lua::Engine::Reference::Type::Boolean)
        return false;
    auto output = target.toData<OutputSegment>();
    if (!output)
        return false;
    output->setResult(value.toBoolean());
    return true;
}


}
}
