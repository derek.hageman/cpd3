/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGTRIGGERSCRIPT_H
#define CPD3EDITINGTRIGGERSCRIPT_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QSet>

#include "editing/editing.hxx"

#include "core/timeutils.hxx"
#include "datacore/stream.hxx"
#include "editing/trigger.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/segment.hxx"
#include "luascript/streambuffer.hxx"

namespace CPD3 {
namespace Editing {

/**
 * A trigger based on simple script segment evaluation.
 */
class CPD3EDITING_EXPORT TriggerScript : public TriggerSegmenting, public EditDataTarget {
    std::string code;
    Data::SequenceMatch::OrderedLookup selection;

    class Buffer : public Lua::StreamBuffer {
        TriggerScript &parent;
        std::size_t offset;
    public:
        double advance;
        Data::SequenceSegment::Transfer pending;

        Buffer(TriggerScript &parent);

        virtual ~Buffer();

    protected:
        bool pushNext(Lua::Engine::Frame &target) override;

        double getStart(Lua::Engine::Frame &frame, const Lua::Engine::Reference &ref) override;

        void convertFromLua(Lua::Engine::Frame &frame) override;

        void outputReady(Lua::Engine::Frame &frame, const Lua::Engine::Reference &ref) override;

        void advanceReady(double time) override;

        void endReady() override;

        bool bufferAssigned(Lua::Engine::Frame &frame,
                            const Lua::Engine::Reference &target,
                            const Lua::Engine::Reference &value) override;
    };

    friend class Buffer;

    Lua::Engine engine;
    Lua::Engine::Frame root;
    Buffer buffer;

    Data::SequenceMatch::OrderedLookup inputs;

    Lua::Engine::Reference controller;
    Lua::Engine::Table environment;
    Lua::Engine::Reference invoke;

    Data::SequenceSegment::Stream reader;

    void init();

    void process(bool ignoreEnd = true);

    void bufferEnd();

public:
    /**
     * Create a script trigger.
     * 
     * @param code      the script code
     * @param input     the input value selection
     */
    TriggerScript(std::string code, Data::SequenceMatch::OrderedLookup selection);

    virtual ~TriggerScript();

    bool shouldRunDetached() const override;

    void finalize() override;

    QList<EditDataTarget *> unhandledInput(const Data::SequenceName &name) override;

    void incomingDataAdvance(double time) override;

    void incomingSequenceValue(const Data::SequenceValue &value) override;

    Trigger *clone() const override;

    Data::SequenceName::Set requestedInputs() const override;

protected:
    TriggerScript(const TriggerScript &other);
};

}
}

#endif
