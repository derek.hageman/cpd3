/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGACTIONPOLY_H
#define CPD3EDITINGACTIONPOLY_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QList>

#include "editing/editing.hxx"

#include "core/number.hxx"
#include "editing/action.hxx"
#include "algorithms/model.hxx"

namespace CPD3 {
namespace Editing {

/**
 * An action that applies a calibration polynomial.
 */
class CPD3EDITING_EXPORT ActionApplyPoly
        : public Action, public EditDataTarget, public EditDataModification {
    Data::SequenceMatch::Composite selection;
    Calibration poly;
public:
    /**
     * Create a new polynomial application action
     * 
     * @param selection     the units to apply to
     * @param poly          the polynomial to apply
     */
    ActionApplyPoly(const Data::SequenceMatch::Composite &selection, const Calibration &poly);

    virtual ~ActionApplyPoly();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

    void modifySequenceValue(Data::SequenceValue &value) override;

protected:
    ActionApplyPoly(const ActionApplyPoly &other);
};

/**
 * An action that inverts a calibration polynomial.
 */
class CPD3EDITING_EXPORT ActionInversePoly : public Action, public EditDataTarget {
    Data::SequenceMatch::Composite selection;
    Calibration poly;
    double min;
    double max;
public:
    /**
     * Create a new polynomial application action
     * 
     * @param selection     the units to apply to
     * @param poly          the polynomial to invert
     * @param min           the minimum range of the output
     * @param max           the maximum range of the output
     */
    ActionInversePoly(const Data::SequenceMatch::Composite &selection,
                      const Calibration &poly,
                      double min = FP::undefined(),
                      double max = FP::undefined());

    virtual ~ActionInversePoly();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

protected:
    ActionInversePoly(const ActionInversePoly &other);
};

/**
 * An action that inverts a calibration polynomial then applies a new one.
 */
class CPD3EDITING_EXPORT ActionReapplyPoly : public Action, public EditDataTarget {
    Data::SequenceMatch::Composite selection;
    Calibration to;
    Calibration from;
    double min;
    double max;
public:
    /**
     * Create a new polynomial application action
     * 
     * @param selection     the units to apply to
     * @param to            the new polynomial
     * @param from          the original polynomial
     * @param min           the minimum range of the uncalibrated value
     * @param max           the maximum range of the uncalibrated value
     */
    ActionReapplyPoly(const Data::SequenceMatch::Composite &selection,
                      const Calibration &to,
                      const Calibration &from,
                      double min = FP::undefined(),
                      double max = FP::undefined());

    virtual ~ActionReapplyPoly();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

protected:
    ActionReapplyPoly(const ActionReapplyPoly &other);
};

/**
 * An action that applies multiple calibration polynomials interpolated in time.
 */
class CPD3EDITING_EXPORT ActionMultiPoly : public Action, public EditDataTarget {
    Data::SequenceMatch::Composite selection;
    std::vector<std::unique_ptr<Algorithms::Model>> coefficients;

public:
    enum Interpolation {
        Linear, CSpline, Sin, Cos, Sigmoid
    };
    enum TimeSelection {
        Middle, Start, End
    };
private:
    TimeSelection timeSelection;
    bool extrapolate;
    double startTime;
    double endTime;

    double apply(double start, double end, double value);

public:
    /**
     * Create a new polynomial application action.
     *
     * @param selection     the units to apply to
     * @param poly          the polynomials to apply at the points to apply them
     * @param interpolation the interpolation method
     * @param timeSelection the time of each value to use
     * @param extrapolate   when set continue model evaluation beyond the bounds
     * @param k0            the first model constant
     * @param k1            the second model constant
     */
    ActionMultiPoly(const Data::SequenceMatch::Composite &selection,
                    const QMap<double, Calibration> &poly,
                    Interpolation method,
                    TimeSelection timeSelection,
                    bool extrapolate = false,
                    double k0 = FP::undefined(),
                    double k1 = FP::undefined());

    virtual ~ActionMultiPoly();

    ActionMultiPoly &operator=(const ActionMultiPoly &) = delete;

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

protected:
    ActionMultiPoly(const ActionMultiPoly &other);
};

}
}

#endif
