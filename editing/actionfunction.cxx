/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/actionfunction.hxx"
#include "datacore/variant/composite.hxx"
#include "datacore/segmentprocessingstage.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Editing {

/** @file editing/actionfunction.hxx
 * Edit directive actions that apply simple functions to data.
 */



ActionSingleInputFunction::ActionSingleInputFunction(const SequenceMatch::Composite &vs,
                                                     const SequenceMatch::OrderedLookup &is,
                                                     const Calibration &cal,
                                                     const std::string &path) : valueSelection(vs),
                                                                                inputSelection(is),
                                                                                inputCalibration(cal),
                                                                                inputPath(path),
                                                                                reader(),
                                                                                processUnits(),
                                                                                inputUnits()
{ }

ActionSingleInputFunction::ActionSingleInputFunction(const ActionSingleInputFunction &other)
        : Action(other),
          valueSelection(other.valueSelection),
          inputSelection(other.inputSelection),
          inputCalibration(other.inputCalibration),
          inputPath(other.inputPath),
          reader(),
          processUnits(),
          inputUnits()
{ }

ActionSingleInputFunction::~ActionSingleInputFunction() = default;

void ActionSingleInputFunction::unhandledInput(const SequenceName &unit,
                                               QList<EditDataTarget *> &inputs,
                                               QList<EditDataTarget *> &outputs,
                                               QList<EditDataModification *> &modifiers)
{
    if (valueSelection.matches(unit)) {
        processUnits.insert(unit);
        outputs.append(this);
        return;
    }
    if (inputSelection.matches(unit)) {
        inputUnits.insert(unit);
        inputs.append(this);
    }
    if (unit.isMeta() && valueSelection.matches(unit.fromMeta())) {
        modifiers.append(this);
    }
}

void ActionSingleInputFunction::incomingSequenceValue(const Data::SequenceValue &value)
{ handleSegments(reader.add(value)); }

void ActionSingleInputFunction::incomingDataAdvance(double time)
{
    handleSegments(reader.advance(time));
    outputAdvance(time);
}

void ActionSingleInputFunction::finalize()
{ handleSegments(reader.finish()); }

Data::SequenceName::Set ActionSingleInputFunction::requestedInputs() const
{ return inputSelection.knownInputs(); }

void ActionSingleInputFunction::handleSegments(SequenceSegment::Transfer &&segments)
{
    if (segments.empty() || processUnits.empty())
        return;
    for (auto &s : segments) {
        double arg = FP::undefined();
        for (const auto &u : inputUnits) {
            double v = s.getValue(u).getPath(inputPath).toDouble();
            v = inputCalibration.apply(v);
            if (!FP::defined(v))
                continue;
            arg = v;
            break;
        }

        for (const auto &u : processUnits) {
            auto result = Variant::Composite::applyTransform(s.getValue(u),
                                                             [this, arg](const Variant::Read &input,
                                                                         Variant::Write &output) {
                                                                 output.setReal(applyFunction(
                                                                         input.toReal(), arg));
                                                             });
            outputData(SequenceValue({u, s.getStart(), s.getEnd()}, std::move(result)));
        }
    }
}

void ActionSingleInputFunction::modifySequenceValue(Data::SequenceValue &value)
{
    SegmentProcessingStage::clearPropagatedSmoothing(value.write().metadata("Smoothing"));
}


ActionAdd::ActionAdd(const Data::SequenceMatch::Composite &valueSelection,
                     const Data::SequenceMatch::OrderedLookup &inputSelection,
                     const Calibration &calibration, const std::string &path)
        : ActionSingleInputFunction(valueSelection,
                                    inputSelection, calibration,
                                    path)
{ }

ActionAdd::ActionAdd(const ActionAdd &other) = default;

ActionAdd::~ActionAdd() = default;

Action *ActionAdd::clone() const
{ return new ActionAdd(*this); }

double ActionAdd::applyFunction(double value, double parameter)
{
    if (!FP::defined(value) || !FP::defined(parameter))
        return FP::undefined();
    return value + parameter;
}


ActionSubtract::ActionSubtract(const Data::SequenceMatch::Composite &valueSelection,
                               const Data::SequenceMatch::OrderedLookup &inputSelection,
                               const Calibration &calibration, const std::string &path)
        : ActionSingleInputFunction(valueSelection,
                                    inputSelection,
                                    calibration, path)
{ }

ActionSubtract::ActionSubtract(const ActionSubtract &other) = default;

ActionSubtract::~ActionSubtract() = default;

Action *ActionSubtract::clone() const
{ return new ActionSubtract(*this); }

double ActionSubtract::applyFunction(double value, double parameter)
{
    if (!FP::defined(value) || !FP::defined(parameter))
        return FP::undefined();
    return value - parameter;
}


ActionMultiply::ActionMultiply(const Data::SequenceMatch::Composite &valueSelection,
                               const Data::SequenceMatch::OrderedLookup &inputSelection,
                               const Calibration &calibration, const std::string &path)
        : ActionSingleInputFunction(valueSelection,
                                    inputSelection,
                                    calibration, path)
{ }

ActionMultiply::ActionMultiply(const ActionMultiply &other) = default;

ActionMultiply::~ActionMultiply() = default;

Action *ActionMultiply::clone() const
{ return new ActionMultiply(*this); }

double ActionMultiply::applyFunction(double value, double parameter)
{
    if (!FP::defined(value) || !FP::defined(parameter))
        return FP::undefined();
    return value * parameter;
}


ActionDivide::ActionDivide(const Data::SequenceMatch::Composite &valueSelection,
                           const Data::SequenceMatch::OrderedLookup &inputSelection,
                           const Calibration &calibration, const std::string &path)
        : ActionSingleInputFunction(valueSelection,
                                    inputSelection,
                                    calibration, path)
{ }

ActionDivide::ActionDivide(const ActionDivide &other) = default;

ActionDivide::~ActionDivide() = default;

Action *ActionDivide::clone() const
{ return new ActionDivide(*this); }

double ActionDivide::applyFunction(double value, double parameter)
{
    if (!FP::defined(value) || !FP::defined(parameter) || parameter == 0.0)
        return FP::undefined();
    return value / parameter;
}


ActionInvert::ActionInvert(const Data::SequenceMatch::Composite &valueSelection,
                           const Data::SequenceMatch::OrderedLookup &inputSelection,
                           const Calibration &calibration, const std::string &path)
        : ActionSingleInputFunction(valueSelection,
                                    inputSelection,
                                    calibration, path)
{ }

ActionInvert::ActionInvert(const ActionInvert &other) = default;

ActionInvert::~ActionInvert() = default;

Action *ActionInvert::clone() const
{ return new ActionInvert(*this); }

double ActionInvert::applyFunction(double value, double parameter)
{
    if (!FP::defined(value) || !FP::defined(parameter) || value == 0.0)
        return FP::undefined();
    return parameter / value;
}


ActionAssign::ActionAssign(const Data::SequenceMatch::Composite &valueSelection,
                           const Data::SequenceMatch::OrderedLookup &inputSelection,
                           const Calibration &calibration,
                           const std::string &path) : ActionSingleInputFunction(valueSelection,
                                                                                inputSelection,
                                                                                calibration, path)
{ }

ActionAssign::ActionAssign(const ActionAssign &other) = default;

ActionAssign::~ActionAssign() = default;

Action *ActionAssign::clone() const
{ return new ActionAssign(*this); }

double ActionAssign::applyFunction(double, double parameter)
{ return parameter; }

}
}
