/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/directivedispatch.hxx"
#include "core/util.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Editing {

/** @file editing/directivedispatch.hxx
 * The routines to handle edit directive data dispatch.
 */


DirectiveDispatch::DirectiveDispatch(const std::vector<EditDirective *> &directives,
                                     EditDispatchSkipTarget *fin)
        : allDirectives(),
          finalOutput(fin), originalFirst(nullptr), target(nullptr),
          skipInputs(),
          isSkipTarget(false),
          recalculateStartUpper(FP::undefined()),
          recalculateEndLower(FP::undefined()),
          recalculateEndUpper(FP::undefined()), recalculateEndLowerThreshold(0),
          advanceTime(FP::undefined())
#ifndef NDEBUG
        ,
          lastTime(FP::undefined())
#endif
{
    Q_ASSERT(!directives.empty());
    EditDirective *input = nullptr;
    for (auto add : directives) {
        allDirectives.push_back(add);
        Q_ASSERT(skipInputs.count(add) == 0);
        if (input)
            skipInputs.emplace(add, input);
        input = add;
    }
    originalFirst = allDirectives[0];

    recalculateDirectives(FP::undefined(), FP::undefined());
}

DirectiveDispatch::~DirectiveDispatch() = default;

void DirectiveDispatch::incomingData(const SequenceValue &value)
{
#ifndef NDEBUG
    {
        double currentTime = value.getStart();
        Q_ASSERT(Range::compareStart(currentTime, lastTime) >= 0);
        lastTime = currentTime;
    }
#endif
    if (shouldRecalculate(value.getStart(), value.getEnd()))
        recalculateDirectives(value.getStart(), value.getEnd());

    if (isSkipTarget) {
        target->incomingSkipped(value, this);
    } else {
        target->incomingData(value);
    }
    Q_ASSERT(Range::compareStart(advanceTime, value.getStart()) <= 0);
    advanceTime = value.getStart();
}

void DirectiveDispatch::incomingData(SequenceValue &&value)
{
#ifndef NDEBUG
    {
        double currentTime = value.getStart();
        Q_ASSERT(Range::compareStart(currentTime, lastTime) >= 0);
        lastTime = currentTime;
    }
#endif
    if (shouldRecalculate(value.getStart(), value.getEnd()))
        recalculateDirectives(value.getStart(), value.getEnd());

    double start = value.getStart();

    if (isSkipTarget) {
        target->incomingSkipped(std::move(value), this);
    } else {
        target->incomingData(std::move(value));
    }
    Q_ASSERT(Range::compareStart(advanceTime, start) <= 0);
    advanceTime = start;
}

void DirectiveDispatch::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    auto endValues = values.end();
    auto beginSend = values.begin();

#ifndef NDEBUG
    for (auto v = beginSend; v != endValues; ++v) {
        double currentTime = v->getStart();
        Q_ASSERT(Range::compareStart(currentTime, lastTime) >= 0);
        lastTime = currentTime;
    }
#endif

    Q_ASSERT(Range::compareStart(advanceTime, values.back().getStart()) <= 0);
    advanceTime = values.back().getStart();

    for (auto v = beginSend; v != endValues; ++v) {
        if (shouldRecalculate(v->getStart(), v->getEnd())) {
            if (beginSend != v) {
                SequenceValue::Transfer copy;
                std::copy(beginSend, v, Util::back_emplacer(copy));
                if (isSkipTarget) {
                    target->incomingSkipped(std::move(copy), this);
                } else {
                    target->incomingData(std::move(copy));
                }
            }
            beginSend = v;
            recalculateDirectives(v->getStart(), v->getEnd());
        }
    }

    if (beginSend == endValues)
        return;

    if (beginSend == values.begin()) {
        if (isSkipTarget) {
            target->incomingSkipped(values, this);
        } else {
            target->incomingData(values);
        }
        return;
    }

    SequenceValue::Transfer copy;
    std::copy(beginSend, endValues, Util::back_emplacer(copy));
    if (isSkipTarget) {
        target->incomingSkipped(std::move(copy), this);
    } else {
        target->incomingData(std::move(copy));
    }
}

void DirectiveDispatch::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    auto endValues = values.end();
    auto beginSend = values.begin();

#ifndef NDEBUG
    for (auto v = beginSend; v != endValues; ++v) {
        double currentTime = v->getStart();
        Q_ASSERT(Range::compareStart(currentTime, lastTime) >= 0);
        lastTime = currentTime;
    }
#endif

    Q_ASSERT(Range::compareStart(advanceTime, values.back().getStart()) <= 0);
    advanceTime = values.back().getStart();

    for (auto v = beginSend; v != endValues; ++v) {
        if (shouldRecalculate(v->getStart(), v->getEnd())) {
            if (beginSend != v) {
                SequenceValue::Transfer copy;
                std::move(beginSend, v, Util::back_emplacer(copy));
                if (isSkipTarget) {
                    target->incomingSkipped(std::move(copy), this);
                } else {
                    target->incomingData(std::move(copy));
                }
            }
            beginSend = v;
            recalculateDirectives(v->getStart(), v->getEnd());
        }
    }

    if (beginSend == endValues)
        return;

    if (beginSend == values.begin()) {
        if (isSkipTarget) {
            target->incomingSkipped(std::move(values), this);
        } else {
            target->incomingData(std::move(values));
        }
        return;
    }

    SequenceValue::Transfer copy;
    std::move(beginSend, endValues, Util::back_emplacer(copy));
    if (isSkipTarget) {
        target->incomingSkipped(std::move(copy), this);
    } else {
        target->incomingData(std::move(copy));
    }
}

void DirectiveDispatch::incomingAdvance(double time)
{
    Q_ASSERT(Range::compareStart(advanceTime, time) <= 0);
    advanceTime = time;
}

void DirectiveDispatch::processAdvance()
{
    if (!FP::defined(advanceTime))
        return;

    /* If the advance moves us past the end of the original first, then
     * we can unlink it right now, so it doesn't hold up the
     * data stream (if we see no further data because it's bypassing
     * everything) */
    if (!allDirectives.empty() && allDirectives[0] == originalFirst) {
        EditDirective *dir = allDirectives[0];
        if (FP::defined(dir->getEnd()) && dir->getEnd() <= advanceTime) {
            dir->unlink();
            auto si = skipInputs.find(dir);
            if (si != skipInputs.end()) {
                Q_ASSERT(si->second);
                si->second->incomingSkipEnd(this);
                skipInputs.erase(si);
            }
            allDirectives.erase(allDirectives.begin());

            recalculateDirectives(advanceTime, FP::undefined());
        }
    }

    originalFirst->incomingAdvance(advanceTime);

    for (const auto &si : skipInputs) {
        if (!si.second)
            continue;
        si.second->incomingSkipAdvance(advanceTime, this);
    }
    finalOutput->incomingSkipAdvance(advanceTime, this);

    advanceTime = FP::undefined();
}

bool DirectiveDispatch::shouldRecalculate(double start, double end) const
{
    if (Range::compareStartEnd(start, recalculateStartUpper) >= 0)
        return true;
    if (Range::compareStartEnd(recalculateEndLower, end) >= recalculateEndLowerThreshold)
        return true;
    if (Range::compareEnd(end, recalculateEndUpper) > 0)
        return true;
    return false;
}

void DirectiveDispatch::recalculateDirectives(double start, double end)
{
    recalculateStartUpper = FP::undefined();
    recalculateEndLower = FP::undefined();
    recalculateEndUpper = FP::undefined();
    recalculateEndLowerThreshold = 0;
    target = nullptr;
    isSkipTarget = false;

    for (auto dir = allDirectives.begin(); dir != allDirectives.end();) {
        double dirEnd = (*dir)->getEnd();

        /* No longer possibly active, so remove it */
        if (Range::compareStartEnd(start, dirEnd) >= 0) {
            std::ptrdiff_t offset = dir - allDirectives.begin();
            if (*dir == originalFirst)
                (*dir)->unlink();
            auto si = skipInputs.find(*dir);
            if (si != skipInputs.end()) {
                Q_ASSERT(si->second);
                si->second->incomingSkipEnd(this);
                skipInputs.erase(si);
            }
            dir = allDirectives.erase(allDirectives.begin() + offset);
            continue;
        }
        double dirStart = (*dir)->getStart();

        if (Range::compareStart(dirStart, start) > 0) {
            /* If this directive is not yet activated by the start time we need
             * to see when the start time would necessitate it's activation and
             * set the recalculate bound to that. */
            if (Range::compareStartEnd(dirStart, recalculateStartUpper) < 0)
                recalculateStartUpper = dirStart;
        }

        if (Range::compareStartEnd(dirStart, end) >= 0) {
            /* If the directive starts after the end of the current range then
             * we can set the upper limit based on the start time. */
            if (Range::compareStartEnd(dirStart, recalculateEndUpper) < 0)
                recalculateEndUpper = dirStart;
        } else {
            /* Otherwise it starts before the end of the current range so we
             * set the lower limit. */
            if (Range::compareStart(dirStart, recalculateEndLower) > 0) {
                recalculateEndLower = dirStart;
                recalculateEndLowerThreshold = 0;
            }
        }

        if (Range::compareEnd(dirEnd, end) <= 0) {
            /* If the directive ends before the end of the current range then
             * we can set the lower bound based on the end time, unless
             * the end is infinite.  Meaning both our current end and the
             * directive end at infinity, so we can't possibly move the lower
             * valid bound up to it. */
            if (FP::defined(dirEnd) && Range::compareStartEnd(recalculateEndLower, dirEnd) < 0) {
                recalculateEndLower = dirEnd;
                recalculateEndLowerThreshold = 1;
            }
        } else {
            /* Otherwise it ends after the current end time, so we can set
             * the upper bound based on the end time. */
            if (Range::compareEnd(dirEnd, recalculateEndUpper) < 0)
                recalculateEndUpper = dirEnd;
        }


        /* If the directive is not active then continue */
        if (!Range::intersects(start, end, dirStart, dirEnd)) {
            ++dir;
            continue;
        }

        /* If it is active then we may need to change the start activation
         * time to when it would deactivate. */
        if (Range::compareEnd(dirEnd, recalculateStartUpper) < 0)
            recalculateStartUpper = dirEnd;

        if (Range::compareStart(dirStart, start) > 0) {
            /* If it is active and the start time is still before it (so
             * this is looking forward) then we can set the end recalculation
             * time to when the directive actually starts.  So if we stop
             * looking forward to it (large end time moving backwards) we
             * catch the inactivation. */
            if (Range::compareStart(recalculateEndLower, dirStart) < 0) {
                recalculateEndLower = dirStart;
                recalculateEndLowerThreshold = 0;
            }
        }


        if (!target) {
            auto check = skipInputs.find(*dir);
            if (check == skipInputs.end()) {
                target = originalFirst;
                isSkipTarget = false;
            } else {
                isSkipTarget = true;
                target = check->second;
            }
        }

        ++dir;
    }

    Q_ASSERT(!shouldRecalculate(start, end));
    if (!target) {
        target = finalOutput;
        isSkipTarget = true;
    }
}

}
}
