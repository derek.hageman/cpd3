/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/actionflowcorrection.hxx"
#include "datacore/archive/selection.hxx"
#include "datacore/variant/composite.hxx"
#include "core/util.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Editing {

/** @file editing/actionflowcorrection.hxx
 * Flow correction related actions.
 */

const static SequenceName saveFlowRatio("", "", "_ZFLOWRATIO");

ActionFlowCorrection::ActionFlowCorrection(const Calibration &b,
                                           const Calibration &a,
                                           const SequenceMatch::OrderedLookup &flowSel,
                                           const SequenceMatch::Composite &accumulatorSel,
                                           const SequenceMatch::Composite &spotSel,
                                           const SequenceMatch::Composite &valueSel,
                                           bool ignoreAccumulated,
                                           double mn,
                                           double mx) : before(b),
                                                        after(a),
                                                        min(mn),
                                                        max(mx),
                                                        flowSelection(flowSel),
                                                        accumulatorSelection(accumulatorSel),
                                                        spotSelection(spotSel),
                                                        valueSelection(valueSel),
                                                        ignoreAccumulatedCut(ignoreAccumulated),
                                                        flowTarget(this),
                                                        spotTarget(this),
                                                        accumulatorTargets(),
                                                        valueTargets(),
                                                        mux(),
                                                        basicTarget(nullptr)
{
    basicTarget = mux.createSimple();
}

ActionFlowCorrection::ActionFlowCorrection(const Calibration &before,
                                           const Calibration &after,
                                           const SequenceName::Component &instrument,
                                           const SequenceName::Component &station,
                                           const SequenceName::Component &archive,
                                           bool ignoreAccumulated,
                                           double min,
                                           double max) : ActionFlowCorrection(before, after,
                                                                              SequenceMatch::OrderedLookup(
                                                                                      SequenceMatch::Element(
                                                                                              {},
                                                                                              Archive::Selection::excludeMetaArchiveMatcher(),
                                                                                              "Q1?_" +
                                                                                                      instrument,
                                                                                              {},
                                                                                              {SequenceName::flavor_cover,
                                                                                               SequenceName::flavor_stats})),
                                                                              SequenceMatch::Composite(
                                                                                      SequenceMatch::Element(
                                                                                              {},
                                                                                              Archive::Selection::excludeMetaArchiveMatcher(),
                                                                                              "(?:(?:L1?)|(?:Qt1?))_" +
                                                                                                      instrument,
                                                                                              {},
                                                                                              {SequenceName::flavor_cover,
                                                                                               SequenceName::flavor_stats})),
                                                                              SequenceMatch::Composite(
                                                                                      SequenceMatch::Element(
                                                                                              {},
                                                                                              Archive::Selection::excludeMetaArchiveMatcher(),
                                                                                              "ZSPOT_" +
                                                                                                      instrument,
                                                                                              {},
                                                                                              {SequenceName::flavor_cover,
                                                                                               SequenceName::flavor_stats})),
                                                                              SequenceMatch::Composite(
                                                                                      SequenceMatch::Element(
                                                                                              {},
                                                                                              Archive::Selection::excludeMetaArchiveMatcher(),
                                                                                              "(?:(?:Bb?[aes][BGRQ0-9]*)|(?:X[BGRQ0-9]*)|(?:N[0-9]*))_" +
                                                                                                      instrument,
                                                                                              {},
                                                                                              {SequenceName::flavor_cover,
                                                                                               SequenceName::flavor_stats})),
                                                                              ignoreAccumulated,
                                                                              min, max)
{
    flowSelection.registerExpected(station, archive, "Q_" + instrument);
    flowSelection.registerExpected(station, archive, "Q1_" + instrument);
}

ActionFlowCorrection::~ActionFlowCorrection() = default;

ActionFlowCorrection::ActionFlowCorrection(const ActionFlowCorrection &other) : Action(other),
                                                                                before(other.before),
                                                                                after(other.after),
                                                                                min(other.min),
                                                                                max(other.max),
                                                                                flowSelection(
                                                                                        other.flowSelection),
                                                                                accumulatorSelection(
                                                                                        other.accumulatorSelection),
                                                                                spotSelection(
                                                                                        other.spotSelection),
                                                                                valueSelection(
                                                                                        other.valueSelection),
                                                                                ignoreAccumulatedCut(
                                                                                        other.ignoreAccumulatedCut),
                                                                                flowTarget(this),
                                                                                spotTarget(this),
                                                                                accumulatorTargets(),
                                                                                valueTargets(),
                                                                                mux(),
                                                                                basicTarget(nullptr)
{
    basicTarget = mux.createSimple();
}

Action *ActionFlowCorrection::clone() const
{ return new ActionFlowCorrection(*this); }

void ActionFlowCorrection::unhandledInput(const SequenceName &unit,
                                          QList<EditDataTarget *> &inputs,
                                          QList<EditDataTarget *> &outputs,
                                          QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    Q_UNUSED(modifiers);

    if (flowSelection.matches(unit)) {
        outputs.append(&flowTarget);
        return;
    }
    if (spotSelection.matches(unit)) {
        outputs.append(&spotTarget);
        return;
    }
    if (valueSelection.matches(unit)) {
        Q_ASSERT(valueTargets.count(unit) == 0);

        std::unique_ptr<ValueTarget> target(new ValueTarget(this, unit));
        outputs.append(target.get());
        valueTargets.emplace(unit, std::move(target));
        return;
    }
    if (accumulatorSelection.matches(unit)) {
        Q_ASSERT(accumulatorTargets.count(unit) == 0);

        Util::ReferenceCopy<SequenceName> accumulatorUnit(unit);
        if (ignoreAccumulatedCut) {
            accumulatorUnit->removeFlavors(SequenceName::cutSizeFlavors());
        }
        auto shared = accumulatorShared.find(accumulatorUnit());
        if (shared == accumulatorShared.end()) {
            shared = accumulatorShared.emplace(accumulatorUnit(),
                                               std::unique_ptr<AccumulatorShared>(
                                                       new AccumulatorShared)).first;
        }
        Q_ASSERT(shared->second);

        std::unique_ptr<AccumulatorTarget>
                target(new AccumulatorTarget(this, unit, shared->second.get()));
        outputs.append(target.get());
        accumulatorTargets.emplace(unit, std::move(target));
        return;
    }
}

void ActionFlowCorrection::incomingDataAdvance(double time)
{
    basicTarget->advance(time);
    for (const auto &v : accumulatorTargets) {
        v.second->incomingDataAdvance(time);
    }
    for (const auto &v : valueTargets) {
        v.second->incomingDataAdvance(time);
    }
    outputData(mux.output<SequenceValue::Transfer>());
    outputAdvance(time);
}

void ActionFlowCorrection::finalize()
{
    for (const auto &v : accumulatorTargets) {
        v.second->finish();
    }
    accumulatorTargets.clear();

    for (const auto &v : valueTargets) {
        v.second->finish();
    }
    valueTargets.clear();

    mux.finishAll();
    basicTarget = nullptr;

    outputData(mux.output<SequenceValue::Transfer>());
}

void ActionFlowCorrection::flushOutput()
{ outputData(mux.output<SequenceValue::Transfer>()); }

SequenceName::Set ActionFlowCorrection::requestedInputs() const
{ return flowSelection.knownInputs(); }

double ActionFlowCorrection::recalibrate(double input) const
{
    return after.apply(before.inverse(input, Calibration::BestInRange, min, max));
}

ActionFlowCorrection::FlowTarget::FlowTarget(ActionFlowCorrection *p) : parent(p),
                                                                        ratio(),
                                                                        ratioEnd(FP::undefined())
{ }

ActionFlowCorrection::FlowTarget::~FlowTarget() = default;

void ActionFlowCorrection::FlowTarget::incomingSequenceValue(const SequenceValue &value)
{
    double original = value.read().toDouble();
    double result = parent->recalibrate(original);
    if (FP::defined(original) && FP::defined(result) && original != 0.0 && result != 0.0) {
        ratio = result / original;
        ratioEnd = value.getEnd();

        SequenceValue sendValue
                ({saveFlowRatio, value.getStart(), value.getEnd(), value.getPriority()},
                 Variant::Root(ratio));
        for (const auto &v : parent->accumulatorTargets) {
            v.second->incomingSequenceValue(sendValue);
        }
        for (const auto &v : parent->valueTargets) {
            v.second->incomingSequenceValue(sendValue);
        }
    }


    if (parent->basicTarget
              ->incomingValue(SequenceValue(value.getIdentity(), Variant::Root(result)))) {
        parent->flushOutput();
    }
}

ActionFlowCorrection::AccumulatorTarget::AccumulatorTarget(ActionFlowCorrection *p,
                                                           const SequenceName &u,
                                                           AccumulatorShared *s) : parent(p),
                                                                                   unit(u),
                                                                                   shared(s),
                                                                                   reader(),
                                                                                   output(parent->mux
                                                                                                .createSimple())
{ }

ActionFlowCorrection::AccumulatorTarget::~AccumulatorTarget() = default;

bool ActionFlowCorrection::AccumulatorTarget::handleSegment(SequenceSegment &&segment)
{
    double original = segment.value(unit).toDouble();
    if (!FP::defined(original))
        return false;

    if (original < shared->priorOriginalValue) {
        shared->priorOriginalValue = 0.0;
        shared->priorReintegratedValue = 0.0;
    }
    Q_ASSERT(FP::defined(shared->priorOriginalValue));
    Q_ASSERT(FP::defined(shared->priorReintegratedValue));

    double ratio = segment.value(saveFlowRatio).toDouble();
    if (!FP::defined(ratio)) {
        if (FP::defined(parent->flowTarget.ratio) &&
                Range::compareStartEnd(segment.getStart(), parent->flowTarget.ratioEnd) < 0)
            ratio = parent->flowTarget.ratio;
        else
            ratio = 1.0;
    }
    if (ratio == 0.0)
        ratio = 1.0;

    double delta = original - shared->priorOriginalValue;
    delta *= ratio;

    shared->priorReintegratedValue += delta;
    shared->priorOriginalValue = original;

    return output->incomingValue(
            SequenceValue(SequenceIdentity(unit, segment.getStart(), segment.getEnd()),
                          Variant::Root(shared->priorReintegratedValue)));
}

void ActionFlowCorrection::AccumulatorTarget::handleSegments(SequenceSegment::Transfer &&segments,
                                                             bool ignoreFlush)
{
    bool doFlush = false;
    for (auto &seg : segments) {
        if (handleSegment(std::move(seg)))
            doFlush = true;
    }
    if (doFlush && !ignoreFlush)
        parent->flushOutput();
}

void ActionFlowCorrection::AccumulatorTarget::incomingSequenceValue(const SequenceValue &value)
{ handleSegments(reader.add(value)); }

void ActionFlowCorrection::AccumulatorTarget::incomingDataAdvance(double time)
{
    handleSegments(reader.advance(time), true);
    output->advance(time);
}

void ActionFlowCorrection::AccumulatorTarget::finish()
{ handleSegments(reader.finish()); }

ActionFlowCorrection::ValueTarget::ValueTarget(ActionFlowCorrection *p, const SequenceName &u)
        : parent(p), unit(u), reader(), output(parent->mux.createSimple())
{ }

ActionFlowCorrection::ValueTarget::~ValueTarget() = default;

bool ActionFlowCorrection::ValueTarget::handleSegment(SequenceSegment &&segment)
{
    auto result = segment.value(unit);
    if (!result.exists())
        return false;

    double original = result.toDouble();
    if (FP::defined(original)) {
        double ratio = segment.value(saveFlowRatio).toDouble();
        if (!FP::defined(ratio)) {
            if (FP::defined(parent->flowTarget.ratio) &&
                    Range::compareStartEnd(segment.getStart(), parent->flowTarget.ratioEnd) < 0)
                ratio = parent->flowTarget.ratio;
            else
                ratio = 1.0;
        }
        if (ratio == 0.0)
            ratio = 1.0;

        original /= ratio;

        return output->incomingValue(
                SequenceValue(SequenceIdentity(unit, segment.getStart(), segment.getEnd()),
                              Variant::Root(original)));
    }

    return output->incomingValue(
            SequenceValue(SequenceIdentity(unit, segment.getStart(), segment.getEnd()),
                          Variant::Root(result)));
}

void ActionFlowCorrection::ValueTarget::handleSegments(SequenceSegment::Transfer &&segments,
                                                       bool ignoreFlush)
{
    bool doFlush = false;
    for (auto &seg : segments) {
        if (handleSegment(std::move(seg)))
            doFlush = true;
    }
    if (doFlush && !ignoreFlush)
        parent->flushOutput();
}

void ActionFlowCorrection::ValueTarget::incomingSequenceValue(const SequenceValue &value)
{ handleSegments(reader.add(value)); }

void ActionFlowCorrection::ValueTarget::incomingDataAdvance(double time)
{
    handleSegments(reader.advance(time), true);
    output->advance(time);
}

void ActionFlowCorrection::ValueTarget::finish()
{ handleSegments(reader.finish()); }

ActionFlowCorrection::SpotTarget::SpotTarget(ActionFlowCorrection *p) : parent(p)
{ }

ActionFlowCorrection::SpotTarget::~SpotTarget() = default;

void ActionFlowCorrection::SpotTarget::incomingSequenceValue(const SequenceValue &value)
{
    Variant::Root result = value.root();

    if (FP::defined(value.getStart()) &&
            FP::defined(value.getEnd()) &&
            value.getStart() < value.getEnd()) {
        double elapsed = value.getEnd() - value.getStart();

        double originalQt = result["Qt"].toDouble();
        if (FP::defined(originalQt)) {
            double flow = originalQt;
            flow /= elapsed;
            flow *= 60000.0;
            flow = parent->recalibrate(flow);
            flow *= elapsed;
            flow /= 60000.0;

            result["Qt"].setDouble(flow);
        }

        double area = result["Area"].toDouble();
        double length = result["L"].toDouble();
        if (!FP::defined(area) &&
                FP::defined(originalQt) &&
                FP::defined(length) &&
                originalQt > 0.0 &&
                length > 0.0) {
            area = (originalQt / length) * 1E6;
        }

        if (FP::defined(area) && FP::defined(length) && area > 0.0) {
            double flow = length * (area * 1E-6);
            flow /= elapsed;
            flow *= 60000.0;
            flow = parent->recalibrate(flow);
            flow *= elapsed;
            flow /= 60000.0;
            flow /= (area * 1E-6);

            result["L"].setDouble(flow);
        }
    }

    if (parent->basicTarget->incomingValue(SequenceValue(value.getIdentity(), std::move(result)))) {
        parent->flushOutput();
    }
}


ActionSpotCorrection::ActionSpotCorrection(double b,
                                           double a,
                                           const Data::SequenceMatch::Composite &lengthSel,
                                           const Data::SequenceMatch::OrderedLookup &spotSel,
                                           const Data::SequenceMatch::Composite &valueSel) : before(
        b),
                                                                                             after(a),
                                                                                             lengthSelection(
                                                                                                     lengthSel),
                                                                                             spotSelection(
                                                                                                     spotSel),
                                                                                             valueSelection(
                                                                                                     valueSel),
                                                                                             lengthTarget(
                                                                                                     this),
                                                                                             spotTarget(
                                                                                                     this),
                                                                                             valueTarget(
                                                                                                     this),
                                                                                             spotArea(
                                                                                                     FP::undefined()),
                                                                                             spotAreaEnd(
                                                                                                     FP::undefined())
{ }

ActionSpotCorrection::ActionSpotCorrection(double before,
                                           double after,
                                           const SequenceName::Component &instrument,
                                           const SequenceName::Component &station,
                                           const SequenceName::Component &archive)
        : ActionSpotCorrection(before, after, SequenceMatch::Composite(
                                                                                                  SequenceMatch::Element(
                                                                                                          {},
                                                                                                          Archive::Selection::excludeMetaArchiveMatcher(),
                                                                                                          "L1?_" +
                                                                                                                  instrument,
                                                                                                          {},
                                                                                                          {SequenceName::flavor_cover,
                                                                                                           SequenceName::flavor_stats})),
                               SequenceMatch::OrderedLookup(
                                                                                                  SequenceMatch::Element(
                                                                                                          {},
                                                                                                          Archive::Selection::excludeMetaArchiveMatcher(),
                                                                                                          "ZSPOT_" +
                                                                                                                  instrument,
                                                                                                          {},
                                                                                                          {SequenceName::flavor_cover,
                                                                                                           SequenceName::flavor_stats})),
                               SequenceMatch::Composite(
                                                                                                  SequenceMatch::Element(
                                                                                                          {},
                                                                                                          Archive::Selection::excludeMetaArchiveMatcher(),
                                                                                                          "(?:(?:Bb?[aes][BGRQ0-9]*)|(?:X[BGRQ0-9]*)|(?:N[0-9]*))_" +
                                                                                                                  instrument,
                                                                                                          {},
                                                                                                          {SequenceName::flavor_cover,
                                                                                                           SequenceName::flavor_stats})))
{
    spotSelection.registerExpected(station, archive);
}

ActionSpotCorrection::~ActionSpotCorrection() = default;

ActionSpotCorrection::ActionSpotCorrection(const ActionSpotCorrection &other) : Action(other),
                                                                                before(other.before),
                                                                                after(other.after),
                                                                                lengthSelection(
                                                                                        other.lengthSelection),
                                                                                spotSelection(
                                                                                        other.spotSelection),
                                                                                valueSelection(
                                                                                        other.valueSelection),
                                                                                lengthTarget(this),
                                                                                spotTarget(this),
                                                                                valueTarget(this),
                                                                                spotArea(
                                                                                        FP::undefined()),
                                                                                spotAreaEnd(
                                                                                        FP::undefined())
{ }

Action *ActionSpotCorrection::clone() const
{ return new ActionSpotCorrection(*this); }

void ActionSpotCorrection::unhandledInput(const SequenceName &unit,
                                          QList<EditDataTarget *> &inputs,
                                          QList<EditDataTarget *> &outputs,
                                          QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    Q_UNUSED(modifiers);

    if (lengthSelection.matches(unit)) {
        outputs.append(&lengthTarget);
        return;
    }
    if (spotSelection.matches(unit)) {
        outputs.append(&spotTarget);
        return;
    }
    if (valueSelection.matches(unit)) {
        outputs.append(&valueTarget);
        return;
    }
}

void ActionSpotCorrection::incomingDataAdvance(double time)
{ outputAdvance(time); }

double ActionSpotCorrection::correctionFactor(double time) const
{
    if (!FP::defined(after) || after == 0.0)
        return 1.0;
    if (FP::defined(before) && before != 0.0)
        return after / before;
    if (FP::defined(spotArea) && spotArea != 0.0 && Range::compareStartEnd(time, spotAreaEnd) < 0)
        return after / spotArea;
    return 1.0;
}

SequenceName::Set ActionSpotCorrection::requestedInputs() const
{ return spotSelection.knownInputs(); }

ActionSpotCorrection::SpotTarget::SpotTarget(ActionSpotCorrection *p) : parent(p)
{ }

ActionSpotCorrection::SpotTarget::~SpotTarget() = default;

void ActionSpotCorrection::SpotTarget::incomingSequenceValue(const SequenceValue &value)
{
    Variant::Root result = value.root();

    double original = result["Area"].toDouble();
    if (FP::defined(original)) {
        result["Area"].setDouble(parent->after);

        parent->spotArea = original;
        parent->spotAreaEnd = value.getEnd();
    }

    double length = result["L"].toDouble();
    if (FP::defined(length)) {
        length /= parent->correctionFactor(value.getEnd());

        result["L"].setDouble(length);
    }

    parent->outputData(SequenceValue(value.getIdentity(), std::move(result)));
}

ActionSpotCorrection::LengthTarget::LengthTarget(ActionSpotCorrection *p) : parent(p)
{ }

ActionSpotCorrection::LengthTarget::~LengthTarget() = default;

void ActionSpotCorrection::LengthTarget::incomingSequenceValue(const SequenceValue &value)
{
    double length = value.read().toDouble();
    if (!FP::defined(length)) {
        parent->outputData(value);
        return;
    }

    length /= parent->correctionFactor(value.getEnd());
    parent->outputData(SequenceValue(value.getIdentity(), Variant::Root(length)));
}

ActionSpotCorrection::ValueTarget::ValueTarget(ActionSpotCorrection *p) : parent(p)
{ }

ActionSpotCorrection::ValueTarget::~ValueTarget() = default;

void ActionSpotCorrection::ValueTarget::incomingSequenceValue(const SequenceValue &value)
{
    double factor = parent->correctionFactor(value.getEnd());
    auto result = Variant::Composite::applyTransform(value.read(),
                                                     [factor](const Variant::Read &input,
                                                              Variant::Write &output) {
                                                         auto v = input.toReal();
                                                         if (FP::defined(v))
                                                             v *= factor;
                                                         output.setReal(v);
                                                     });
    parent->outputData(SequenceValue(value.getIdentity(), std::move(result)));
}


ActionMultiSpotCorrection::ActionMultiSpotCorrection(const std::vector<double> &b,
                                                     const std::vector<double> &a,
                                                     const Data::SequenceMatch::Composite &lengthSel,
                                                     const Data::SequenceMatch::OrderedLookup &spotSel,
                                                     const Data::SequenceMatch::OrderedLookup &indexSel,
                                                     const Data::SequenceMatch::Composite &valueSel)
        : before(b),
          after(a),
          lengthSelection(lengthSel),
          spotSelection(spotSel),
          indexSelection(indexSel),
          valueSelection(valueSel),
          lengthTarget(this),
          spotTarget(this),
          indexTarget(this),
          valueTarget(this),
          index(INTEGER::undefined()),
          spotArea(FP::undefined()),
          spotAreaEnd(FP::undefined())
{ }

ActionMultiSpotCorrection::ActionMultiSpotCorrection(const std::vector<double> &before,
                                                     const std::vector<double> &after,
                                                     const SequenceName::Component &instrument,
                                                     const SequenceName::Component &station,
                                                     const SequenceName::Component &archive)
        : ActionMultiSpotCorrection(before, after, SequenceMatch::Composite(
        SequenceMatch::Element({}, Archive::Selection::excludeMetaArchiveMatcher(),
                               "L1?_" + instrument, {},
                               {SequenceName::flavor_cover, SequenceName::flavor_stats})),
                                    SequenceMatch::OrderedLookup(SequenceMatch::Element({},
                                                                                        Archive::Selection::excludeMetaArchiveMatcher(),
                                                                                        "ZSPOT_" +
                                                                                                instrument,
                                                                                        {},
                                                                                        {SequenceName::flavor_cover,
                                                                                         SequenceName::flavor_stats})),
                                    SequenceMatch::OrderedLookup(SequenceMatch::Element({},
                                                                                        Archive::Selection::excludeMetaArchiveMatcher(),
                                                                                        "Fn_" +
                                                                                                instrument,
                                                                                        {},
                                                                                        {SequenceName::flavor_cover,
                                                                                         SequenceName::flavor_stats})),
                                    SequenceMatch::Composite(SequenceMatch::Element({},
                                                                                    Archive::Selection::excludeMetaArchiveMatcher(),
                                                                                    "(?:(?:Bb?[aes][BGRQ0-9]*)|(?:X[BGRQ0-9]*)|(?:N[0-9]*))_" +
                                                                                            instrument,
                                                                                    {},
                                                                                    {SequenceName::flavor_cover,
                                                                                     SequenceName::flavor_stats})))
{
    spotSelection.registerExpected(station, archive);
    indexSelection.registerExpected(station, archive);
}

ActionMultiSpotCorrection::~ActionMultiSpotCorrection() = default;

ActionMultiSpotCorrection::ActionMultiSpotCorrection(const ActionMultiSpotCorrection &other)
        : Action(other),
          before(other.before),
          after(other.after),
          lengthSelection(other.lengthSelection),
          spotSelection(other.spotSelection),
          indexSelection(other.indexSelection),
          valueSelection(other.valueSelection),
          lengthTarget(this),
          spotTarget(this),
          indexTarget(this),
          valueTarget(this),
          index(INTEGER::undefined()),
          spotArea(FP::undefined()),
          spotAreaEnd(FP::undefined())
{ }

Action *ActionMultiSpotCorrection::clone() const
{ return new ActionMultiSpotCorrection(*this); }

void ActionMultiSpotCorrection::unhandledInput(const SequenceName &unit,
                                               QList<EditDataTarget *> &inputs,
                                               QList<EditDataTarget *> &outputs,
                                               QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(modifiers);

    if (indexSelection.matches(unit)) {
        inputs.append(&indexTarget);
    }
    if (lengthSelection.matches(unit)) {
        outputs.append(&lengthTarget);
        return;
    }
    if (spotSelection.matches(unit)) {
        outputs.append(&spotTarget);
        return;
    }
    if (valueSelection.matches(unit)) {
        outputs.append(&valueTarget);
        return;
    }
}

void ActionMultiSpotCorrection::incomingDataAdvance(double time)
{ outputAdvance(time); }

double ActionMultiSpotCorrection::effectiveArea() const
{
    if (!INTEGER::defined(index) || index < 0 || (size_t) index >= after.size())
        return FP::undefined();
    return after[(size_t) index];
}

double ActionMultiSpotCorrection::inputArea() const
{
    if (!INTEGER::defined(index) || index < 0 || (size_t) index >= before.size())
        return FP::undefined();
    return before[(size_t) index];
}

double ActionMultiSpotCorrection::correctionFactor(double time) const
{
    double area = effectiveArea();
    if (!FP::defined(area) || area == 0.0)
        return 1.0;
    double target = inputArea();
    if (FP::defined(target) && target != 0.0)
        return area / target;
    if (FP::defined(spotArea) && spotArea != 0.0 && Range::compareStartEnd(time, spotAreaEnd) < 0)
        return area / spotArea;
    return 1.0;
}

SequenceName::Set ActionMultiSpotCorrection::requestedInputs() const
{
    SequenceName::Set result;
    Util::merge(spotSelection.knownInputs(), result);
    Util::merge(indexSelection.knownInputs(), result);
    return result;
}

ActionMultiSpotCorrection::SpotTarget::SpotTarget(ActionMultiSpotCorrection *p) : parent(p)
{ }

ActionMultiSpotCorrection::SpotTarget::~SpotTarget() = default;

void ActionMultiSpotCorrection::SpotTarget::incomingSequenceValue(const SequenceValue &value)
{
    auto result = value.root();

    qint64 index = result["Fn"].toInt64();
    if (INTEGER::defined(index)) {
        parent->index = index - 1;
    }

    double original = result["Area"].toDouble();
    double target = parent->effectiveArea();
    if (FP::defined(original)) {
        if (FP::defined(target)) {
            result["Area"].setDouble(target);
        }

        parent->spotArea = original;
        parent->spotAreaEnd = value.getEnd();
    }

    double length = result["L"].toDouble();
    if (FP::defined(length)) {
        length /= parent->correctionFactor(value.getEnd());

        result["L"].setDouble(length);
    }

    parent->outputData(SequenceValue(value.getIdentity(), std::move(result)));
}

ActionMultiSpotCorrection::IndexTarget::IndexTarget(ActionMultiSpotCorrection *p) : parent(p)
{ }

ActionMultiSpotCorrection::IndexTarget::~IndexTarget() = default;

void ActionMultiSpotCorrection::IndexTarget::incomingSequenceValue(const SequenceValue &value)
{
    qint64 i = value.read().toInt64();
    if (INTEGER::defined(i))
        parent->index = i - 1;
}

ActionMultiSpotCorrection::LengthTarget::LengthTarget(ActionMultiSpotCorrection *p) : parent(p)
{ }

ActionMultiSpotCorrection::LengthTarget::~LengthTarget() = default;

void ActionMultiSpotCorrection::LengthTarget::incomingSequenceValue(const SequenceValue &value)
{
    double length = value.read().toDouble();
    if (!FP::defined(length)) {
        parent->outputData(value);
        return;
    }

    length /= parent->correctionFactor(value.getEnd());
    parent->outputData(SequenceValue(value.getIdentity(), Variant::Root(length)));
}

ActionMultiSpotCorrection::ValueTarget::ValueTarget(ActionMultiSpotCorrection *p) : parent(p)
{ }

ActionMultiSpotCorrection::ValueTarget::~ValueTarget() = default;

void ActionMultiSpotCorrection::ValueTarget::incomingSequenceValue(const SequenceValue &value)
{
    double factor = parent->correctionFactor(value.getEnd());
    auto result = Variant::Composite::applyTransform(value.read(),
                                                     [factor](const Variant::Read &input,
                                                              Variant::Write &output) {
                                                         auto v = input.toReal();
                                                         if (FP::defined(v))
                                                             v *= factor;
                                                         output.setReal(v);
                                                     });
    parent->outputData(SequenceValue(value.getIdentity(), std::move(result)));
}


}
}
