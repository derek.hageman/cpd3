/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/trigger.hxx"
#include "core/range.hxx"

namespace CPD3 {
namespace Editing {

/** @file editing/trigger.hxx
 * The handling for the triggering of edit directives.
 */

Trigger::Trigger() : inv(false)
{ }

Trigger::~Trigger()
{ }

Trigger::Trigger(const Trigger &other) : inv(other.inv)
{ }

void Trigger::advance(double time)
{ Q_UNUSED(time); }

void Trigger::finalize()
{ }

bool Trigger::isNeverActive(double start, double end) const
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    return false;
}

void Trigger::extendProcessing(double &start, double &end) const
{
    Q_UNUSED(start);
    Q_UNUSED(end);
}

bool Trigger::shouldRunDetached() const
{ return false; }

QList<EditDataTarget *> Trigger::unhandledInput(const Data::SequenceName &unit)
{
    Q_UNUSED(unit);
    return QList<EditDataTarget *>();
}

void Trigger::incomingDataAdvance(double time)
{ Q_UNUSED(time); }

void Trigger::signalTerminate()
{ }

Data::SequenceName::Set Trigger::requestedInputs() const
{ return Data::SequenceName::Set(); }


TriggerAlways::TriggerAlways()
{ }

TriggerAlways::~TriggerAlways()
{ }

bool TriggerAlways::isReady(double end) const
{
    Q_UNUSED(end);
    return true;
}

bool TriggerAlways::isNeverActive(double start, double end) const
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    return inverted();
}

Trigger *TriggerAlways::clone() const
{
    TriggerAlways *t = new TriggerAlways;
    Q_ASSERT(t);
    t->setInverted(inverted());
    return t;
}

bool TriggerAlways::process(double start, double end)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    return true;
}


TriggerAND::TriggerAND(const std::vector<Trigger *> &cmp) : components(cmp)
{ }

TriggerAND::TriggerAND(const QList<Trigger *> &cmp) : components()
{
    components.reserve(cmp.size());
    for (QList<Trigger *>::const_iterator add = cmp.constBegin(), endAdd = cmp.constEnd();
            add != endAdd;
            ++add) {
        components.push_back(*add);
    }
}

TriggerAND::TriggerAND(const QVector<Trigger *> &cmp) : components(cmp.toStdVector())
{ }

TriggerAND::~TriggerAND()
{ qDeleteAll(components); }

TriggerAND::TriggerAND(const TriggerAND &other) : Trigger(other), components()
{
    components.reserve(other.components.size());
    for (std::vector<Trigger *>::const_iterator cmp = other.components.begin(),
            endCmp = other.components.end(); cmp != endCmp; ++cmp) {
        components.push_back((*cmp)->clone());
    }
}

Trigger *TriggerAND::clone() const
{ return new TriggerAND(*this); }

bool TriggerAND::process(double start, double end)
{
    for (std::vector<Trigger *>::iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        if (!(*cmp)->appliesTo(start, end)) {
            if (FP::defined(start)) {
                for (++cmp; cmp != endCmp; ++cmp) {
                    (*cmp)->advance(start);
                }
            }
            return false;
        }
    }
    return true;
}

void TriggerAND::advance(double time)
{
    for (std::vector<Trigger *>::iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        (*cmp)->advance(time);
    }
}

void TriggerAND::finalize()
{
    for (std::vector<Trigger *>::iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        (*cmp)->finalize();
    }
}

bool TriggerAND::isReady(double end) const
{
    for (std::vector<Trigger *>::const_iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        if (!(*cmp)->isReady(end))
            return false;
    }
    return true;
}

bool TriggerAND::isNeverActive(double start, double end) const
{
    for (std::vector<Trigger *>::const_iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        if ((*cmp)->isNeverActive(start, end))
            return !inverted();
    }
    return false;
}

bool TriggerAND::shouldRunDetached() const
{
    for (std::vector<Trigger *>::const_iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        if ((*cmp)->shouldRunDetached())
            return true;
    }
    return false;
}

void TriggerAND::extendProcessing(double &start, double &end) const
{
    double extendedStart = start;
    double extendedEnd = end;
    for (std::vector<Trigger *>::const_iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        double passStart = start;
        double passEnd = end;
        (*cmp)->extendProcessing(start, end);
        if (Range::compareStart(passStart, extendedStart) < 0)
            extendedStart = passStart;
        if (Range::compareEnd(passEnd, extendedEnd) > 0)
            extendedEnd = passEnd;
    }
    start = extendedStart;
    end = extendedEnd;
}

QList<EditDataTarget *> TriggerAND::unhandledInput(const Data::SequenceName &unit)
{
    QList<EditDataTarget *> result;
    for (std::vector<Trigger *>::iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        result.append((*cmp)->unhandledInput(unit));
    }
    return result;
}

void TriggerAND::incomingDataAdvance(double time)
{
    for (std::vector<Trigger *>::iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        (*cmp)->incomingDataAdvance(time);
    }
}

void TriggerAND::signalTerminate()
{
    for (std::vector<Trigger *>::iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        (*cmp)->signalTerminate();
    }
}

Data::SequenceName::Set TriggerAND::requestedInputs() const
{
    Data::SequenceName::Set result;
    for (const auto &cmp : components) {
        Util::merge(cmp->requestedInputs(), result);
    }
    return result;
}

TriggerOR::TriggerOR(const std::vector<Trigger *> &cmp) : components(cmp)
{ }

TriggerOR::TriggerOR(const QList<Trigger *> &cmp) : components()
{
    components.reserve(cmp.size());
    for (QList<Trigger *>::const_iterator add = cmp.constBegin(), endAdd = cmp.constEnd();
            add != endAdd;
            ++add) {
        components.push_back(*add);
    }
}

TriggerOR::TriggerOR(const QVector<Trigger *> &cmp) : components(cmp.toStdVector())
{ }

TriggerOR::~TriggerOR()
{ qDeleteAll(components); }

TriggerOR::TriggerOR(const TriggerOR &other) : Trigger(other), components()
{
    components.reserve(other.components.size());
    for (std::vector<Trigger *>::const_iterator cmp = other.components.begin(),
            endCmp = other.components.end(); cmp != endCmp; ++cmp) {
        components.push_back((*cmp)->clone());
    }
}

Trigger *TriggerOR::clone() const
{ return new TriggerOR(*this); }

bool TriggerOR::process(double start, double end)
{
    for (std::vector<Trigger *>::iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        if ((*cmp)->appliesTo(start, end)) {
            if (FP::defined(start)) {
                for (++cmp; cmp != endCmp; ++cmp) {
                    (*cmp)->advance(start);
                }
            }
            return true;
        }
    }
    return false;
}

void TriggerOR::advance(double time)
{
    for (std::vector<Trigger *>::iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        (*cmp)->advance(time);
    }
}

void TriggerOR::finalize()
{
    for (std::vector<Trigger *>::iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        (*cmp)->finalize();
    }
}

bool TriggerOR::isReady(double end) const
{
    for (std::vector<Trigger *>::const_iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        if (!(*cmp)->isReady(end))
            return false;
    }
    return true;
}

bool TriggerOR::isNeverActive(double start, double end) const
{
    for (std::vector<Trigger *>::const_iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        if (!(*cmp)->isNeverActive(start, end))
            return false;
    }
    return !inverted();
}

bool TriggerOR::shouldRunDetached() const
{
    for (std::vector<Trigger *>::const_iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        if ((*cmp)->shouldRunDetached())
            return true;
    }
    return false;
}

void TriggerOR::extendProcessing(double &start, double &end) const
{
    double extendedStart = start;
    double extendedEnd = end;
    for (std::vector<Trigger *>::const_iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        double passStart = start;
        double passEnd = end;
        (*cmp)->extendProcessing(start, end);
        if (Range::compareStart(passStart, extendedStart) < 0)
            extendedStart = passStart;
        if (Range::compareEnd(passEnd, extendedEnd) > 0)
            extendedEnd = passEnd;
    }
    start = extendedStart;
    end = extendedEnd;
}

QList<EditDataTarget *> TriggerOR::unhandledInput(const Data::SequenceName &unit)
{
    QList<EditDataTarget *> result;
    for (std::vector<Trigger *>::iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        result.append((*cmp)->unhandledInput(unit));
    }
    return result;
}

void TriggerOR::incomingDataAdvance(double time)
{
    for (std::vector<Trigger *>::iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        (*cmp)->incomingDataAdvance(time);
    }
}

void TriggerOR::signalTerminate()
{
    for (std::vector<Trigger *>::iterator cmp = components.begin(), endCmp = components.end();
            cmp != endCmp;
            ++cmp) {
        (*cmp)->signalTerminate();
    }
}

Data::SequenceName::Set TriggerOR::requestedInputs() const
{
    Data::SequenceName::Set result;
    for (const auto &cmp : components) {
        Util::merge(cmp->requestedInputs(), result);
    }
    return result;
}


/* Note that this is a bit counter intuitive in how the extensions are
 * applied.  Since our polarity is looking for truth segments, we want
 * to actually apply the "before" unit to the end, so we move the end
 * we inspect up to a time when it might become true.  The same is
 * true for the start. */
TriggerExtending::TriggerExtending(Trigger *t) : trigger(t),
                                                 beforeUnit(Time::Minute),
                                                 beforeCount(0),
                                                 beforeAlign(false),
                                                 afterUnit(Time::Minute),
                                                 afterCount(0),
                                                 afterAlign(false)
{
    Q_ASSERT(trigger != NULL);
}

TriggerExtending::~TriggerExtending()
{
    delete trigger;
}

TriggerExtending::TriggerExtending(const TriggerExtending &other) : Trigger(other),
                                                                    trigger(other.trigger->clone()),
                                                                    beforeUnit(other.beforeUnit),
                                                                    beforeCount(other.beforeCount),
                                                                    beforeAlign(other.beforeAlign),
                                                                    afterUnit(other.afterUnit),
                                                                    afterCount(other.afterCount),
                                                                    afterAlign(other.afterAlign)
{ }

Trigger *TriggerExtending::clone() const
{ return new TriggerExtending(*this); }

void TriggerExtending::finalize()
{ trigger->finalize(); }

QList<EditDataTarget *> TriggerExtending::unhandledInput(const Data::SequenceName &unit)
{ return trigger->unhandledInput(unit); }

void TriggerExtending::incomingDataAdvance(double time)
{ trigger->incomingDataAdvance(time); }

bool TriggerExtending::process(double start, double end)
{
    start = Time::logical(start, afterUnit, afterCount, afterAlign, false, -1);
    end = Time::logical(end, beforeUnit, beforeCount, beforeAlign, true);
    Q_ASSERT(Range::compareStartEnd(start, end) <= 0);
    return trigger->appliesTo(start, end);
}

void TriggerExtending::advance(double time)
{
    trigger->advance(Time::logical(time, afterUnit, afterCount, afterAlign, false, -1));
}

bool TriggerExtending::isReady(double end) const
{
    end = Time::logical(end, beforeUnit, beforeCount, beforeAlign, true);
    return trigger->isReady(end);
}

bool TriggerExtending::isNeverActive(double start, double end) const
{
    start = Time::logical(start, afterUnit, afterCount, afterAlign, false, -1);
    end = Time::logical(end, beforeUnit, beforeCount, beforeAlign, true);
    return trigger->isNeverActive(start, end);
}

bool TriggerExtending::shouldRunDetached() const
{ return trigger->shouldRunDetached(); }

void TriggerExtending::extendProcessing(double &start, double &end) const
{
    trigger->extendProcessing(start, end);
    start = Time::logical(start, afterUnit, afterCount, afterAlign, false, -1);
    end = Time::logical(end, beforeUnit, beforeCount, beforeAlign, true);
}

void TriggerExtending::signalTerminate()
{ trigger->signalTerminate(); }

Data::SequenceName::Set TriggerExtending::requestedInputs() const
{ return trigger->requestedInputs(); }


TriggerSegmenting::TriggerSegmenting() : segments(NULL),
                                         segmentsSize(0),
                                         segmentsCapacity(15),
                                         segmentsReady(FP::undefined()),
                                         completed(false)
{
    segments = (Segment *) malloc(segmentsCapacity * sizeof(Segment));
    Q_ASSERT(segments);
}

TriggerSegmenting::TriggerSegmenting(const TriggerSegmenting &other) : Trigger(other),
                                                                       segments(NULL),
                                                                       segmentsSize(0),
                                                                       segmentsCapacity(15),
                                                                       segmentsReady(
                                                                               FP::undefined()),
                                                                       completed(false)
{
    segments = (Segment *) malloc(segmentsCapacity * sizeof(Segment));
    Q_ASSERT(segments);
}

TriggerSegmenting::~TriggerSegmenting()
{
    free(segments);
}

void TriggerSegmenting::emitSegment(double start, double end, bool value)
{
    Q_ASSERT(Range::compareStartEnd(start, end) <= 0);
    Q_ASSERT(Range::compareStart(start, segmentsReady) >= 0);
    segmentsReady = start;

    if (segmentsSize != 0) {
        Segment *last = segments + (segmentsSize - 1);
        Q_ASSERT(Range::compareStart(start, last->start) >= 0);
        if (Range::compareStartEnd(start, last->end) <= 0) {
            if (!value) {
                last->end = start;
            } else {
                last->end = end;
            }
            return;
        }
    }

    if (!value)
        return;

    if (segmentsSize >= segmentsCapacity) {
        segmentsCapacity = INTEGER::growAlloc(segmentsSize + 1);
        segments = (Segment *) realloc(segments, segmentsCapacity * sizeof(Segment));
        Q_ASSERT(segments);
    }

    Segment *target = segments + segmentsSize;
    segmentsSize++;

    target->start = start;
    target->end = end;
}

void TriggerSegmenting::emitAdvance(double time)
{
    Q_ASSERT(Range::compareStart(time, segmentsReady) >= 0);
    Q_ASSERT(segmentsSize == 0 ||
                     Range::compareStart(time, (segments + segmentsSize - 1)->start) >= 0);
    segmentsReady = time;
}

void TriggerSegmenting::discardUntil(double time)
{
    if (segmentsSize == 0)
        return;
    if (Range::compareStart(time, segments->start) <= 0)
        return;

    if (segmentsSize == 1) {
        if (Range::compareStartEnd(time, segments->end) >= 0) {
            segmentsSize = 0;
        }
        return;
    }
    Q_ASSERT(segmentsSize > 1);

    size_t shift = 0;
    if (Range::compareStart(time, segments[1].start) <= 0 &&
            Range::compareStartEnd(time, segments[0].end) >= 0) {
        shift = 1;
    } else {
        Segment *endSeg = segments + segmentsSize;
        Segment *endDiscard = Range::lowerBound(segments, endSeg, time);
        if (endDiscard == endSeg) {
            if (Range::compareStartEnd(time, (endSeg - 1)->end) >= 0) {
                segmentsSize = 0;
                return;
            }
            shift = segmentsSize - 1;
        } else if (endDiscard == segments) {
            return;
        } else {
            if (Range::compareStartEnd(time, (endDiscard - 1)->end) >= 0) {
                shift = endDiscard - segments;
            } else {
                shift = (endDiscard - segments) - 1;
                if (shift == 0)
                    return;
            }
        }
    }

    Q_ASSERT(shift != 0);
    segmentsSize -= shift;
    memmove(segments, segments + shift, segmentsSize * sizeof(Segment));
}

void TriggerSegmenting::advance(double time)
{
    Q_ASSERT(completed || Range::compareStart(segmentsReady, time) >= 0);

    discardUntil(time);
}

bool TriggerSegmenting::process(double start, double end)
{
    Q_ASSERT(Range::compareStartEnd(start, end) <= 0);
    Q_ASSERT(completed || Range::compareStartEnd(segmentsReady, end) >= 0);

    discardUntil(start);

    if (segmentsSize == 0)
        return false;

    return Range::intersects(start, end, segments->start, segments->end);
}

void TriggerSegmenting::finalize()
{
    completed = true;
}

bool TriggerSegmenting::isReady(double end) const
{
    if (completed)
        return true;
    return Range::compareStartEnd(segmentsReady, end) >= 0;
}


}
}
