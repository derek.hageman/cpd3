/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/triggeraverage.hxx"

namespace CPD3 {
namespace Editing {

/** @file editing/triggeraverage.hxx
 * Handlers for weighted averaging of input data for triggering.
 */

enum {
    Average_Value = 0, Average_Cover = 1,
};

static QVector<TriggerInput *> buildAverageInput(const Data::SequenceMatch::OrderedLookup &input,
                                                 const std::string &path)
{
    QVector<TriggerInput *> result;

    Data::SequenceMatch::OrderedLookup sel(input);
    sel.excludeFlavors({Data::SequenceName::flavor_cover});

    result.append(new TriggerInputValue(sel, path));
    Q_ASSERT(result.last());

    sel = input;
    sel.requireFlavors({Data::SequenceName::flavor_cover});
    result.append(new TriggerInputValue(sel));
    Q_ASSERT(result.last());

    return result;
}

TriggerInputAverage::TriggerInputAverage(const Data::SequenceMatch::OrderedLookup &input,
                                         const Data::Variant::Read &config,
                                         const std::string &path) : TriggerInputMerger<
        Internal::InputMergedFixed<2>, TriggerInput>(buildAverageInput(input, path)),
                                                                TriggerBuffer<
                                                                        Internal::InputMergedFixed<
                                                                                2>,
                                                                        TriggerInput::Result>(),
                                                                result()
{
    configureBuffer(config);
}

TriggerInputAverage::TriggerInputAverage(const TriggerInputAverage &other) : TriggerInputMerger<
        Internal::InputMergedFixed<2>, TriggerInput>(other),
                                                                             TriggerBuffer<
                                                                                     Internal::InputMergedFixed<
                                                                                             2>,
                                                                                     TriggerInput::Result>(),
                                                                             result()
{
    copyBufferParameters(other);
}

TriggerInputAverage::~TriggerInputAverage() = default;

TriggerInput *TriggerInputAverage::clone() const
{ return new TriggerInputAverage(*this); }

double TriggerInputAverage::advancedTime() const
{ return bufferAdvancedTime(); }

bool TriggerInputAverage::isReady(double end) const
{
    return isBufferReady(end) &&
            TriggerInputMerger<Internal::InputMergedFixed<2>, TriggerInput>::isReady(end);
}

void TriggerInputAverage::extendProcessing(double &start, double &end) const
{
    extendBufferProcessing(start, end);
    TriggerInputMerger<Internal::InputMergedFixed<2>, TriggerInput>::extendProcessing(start, end);
}

void TriggerInputAverage::incomingDataAdvance(double time)
{
    TriggerInputMerger<Internal::InputMergedFixed<2>, TriggerInput>::incomingDataAdvance(time);

    processPending();
    double adv = TriggerInputMerger<Internal::InputMergedFixed<2>, TriggerInput>::advancedTime();
    if (FP::defined(adv)) {
        result << incomingBufferAdvance(adv);
    }
}

void TriggerInputAverage::processPending()
{
    QVector<Internal::InputMergedFixed<2> > add(takeAllReady());
    if (add.isEmpty())
        return;
    result << incomingBufferData(add);
}

QVector<TriggerInput::Result> TriggerInputAverage::process()
{
    processPending();
    if (result.isEmpty())
        return result;
    QVector<Result> ret(result);
    result.clear();
    return ret;
}

void TriggerInputAverage::finalize()
{
    TriggerInputMerger<Internal::InputMergedFixed<2>, TriggerInput>::finalize();
    processPending();
    result << endBufferData();
}

TriggerInput::Result TriggerInputAverage::convert(const QVector<
        Internal::InputMergedFixed<2> > &buffer, const Internal::InputMergedFixed<2> &active)
{
    double totalTime = 0.0;
    double sum = 0.0;
    for (QVector<Internal::InputMergedFixed<2> >::const_iterator add = buffer.constBegin(),
            endAdd = buffer.constEnd(); add != endAdd; ++add) {
        double value = add->values[Average_Value];
        if (!FP::defined(value))
            continue;

        if (!FP::defined(add->start) || !FP::defined(add->end) || add->end < add->start) {
            /* An undefined duration means we can't weight them correctly,
             * so just assume they're all equal weight. */
            sum = 0.0;
            totalTime = 0.0;
            for (add = buffer.constBegin(), endAdd = buffer.constEnd(); add != endAdd; ++add) {
                value = add->values[Average_Value];
                if (!FP::defined(value))
                    continue;
                double cover = add->values[Average_Cover];
                if (!FP::defined(cover)) {
                    totalTime += 1.0;
                    sum += value;
                } else if (cover > 0.0) {
                    sum += value * cover;
                    totalTime += add->values[Average_Cover];
                }
            }

            if (totalTime != 0.0) {
                return Result(active.start, active.end, sum / totalTime);
            } else {
                return Result(active.start, active.end, FP::undefined());
            }
        }
        double duration = add->end - add->start;

        double cover = add->values[Average_Cover];
        if (!FP::defined(cover)) {
            sum += value * duration;
            totalTime += duration;
        } else if (cover > 0.0) {
            double tAdd = duration * cover;
            sum += value * tAdd;
            totalTime += tAdd;
        }
    }

    if (totalTime != 0.0) {
        return Result(active.start, active.end, sum / totalTime);
    }

    return Result(active.start, active.end, FP::undefined());
}


}
}
