/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGACTION_H
#define CPD3EDITINGACTION_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QList>
#include <QVector>

#include "editing/editing.hxx"

#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "editing/editdatatarget.hxx"
#include "datacore/sequencematch.hxx"

namespace CPD3 {
namespace Editing {

/**
 * The base class that applies the action involved in an edit directive.
 */
class CPD3EDITING_EXPORT Action {
    EditDispatchTarget *next;
public:
    Action();

    virtual ~Action();

    /**
     * Register an new input.  This is called when a new unit is seen and should
     * set the outputs to handle the dispatch.  Ownership is retained
     * by the callee.
     * <br>
     * Modifiers are called before any other handling.  If no outputs are set
     * then the value bypasses the action, otherwise the action must produce
     * it was output.
     * 
     * @param unit      the new unit
     * @param inputs    the list of always sent to input targets
     * @param outputs   the list of output targets
     * @param modifiers the targets that are immediately applied to modify the value
     */
    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers) = 0;

    /**
     * Called when to indicate that the input data stream (all targets) has
     * advanced to a certain time.  No data will be generated before this time.
     * 
     * @param time      the time to advance until
     */
    virtual void incomingDataAdvance(double time);

    /**
     * Finalize and finish the action.
     */
    virtual void finalize();

    /**
     * Test if the action requires synchronization.  This is required for any
     * actions that run on a separate thread (e.x. blocking script processing).
     * If this returns false then the output can be re-multiplexed without
     * threading synchronization.  That is, this must return true if the
     * action can produce output outside of while it's blocked in an
     * incoming value handler.
     * <br>
     * The default implementation always returns false.
     * 
     * @return      true if the output needs re-synchronization
     */
    virtual bool mustSynchronizeOutput() const;

    /**
    * Test if the action should run detached from the main processing chain.
    * That is, if this returns true then the action should cause the edit
    * to be processed in another thread.  This is primarily useful for
    * script evaluations that may take a long time to calculate.
    * <br>
    * The default implementation always returns false.
    *
    * @return          true if the trigger should be detached
    */
    virtual bool shouldRunDetached() const;

    /**
     * Set the action's target.
     * 
     * @param target    the target
     */
    inline void setTarget(EditDispatchTarget *target)
    { next = target; }

    /**
     * Output a processed data value.
     * 
     * @param value     the value to output
     */
    inline void outputData(const Data::SequenceValue &value)
    { next->incomingData(value); }

    /** @see outputData(const Data::SequenceValue &) */
    inline void outputData(Data::SequenceValue &&value)
    { next->incomingData(std::move(value)); }

    /**
     * Output processed data values.
     * 
     * @param values    the values to output
     */
    inline void outputData(const Data::SequenceValue::Transfer &values)
    {
        if (values.empty())
            return;
        next->incomingData(values);
    }

    /** @see outputData(const Data::SequenceValue::Transfer &) */
    inline void outputData(Data::SequenceValue::Transfer &&values)
    {
        if (values.empty())
            return;
        next->incomingData(std::move(values));
    }

    /**
     * Output an advance in time.
     * 
     * @param time      the time to advance to
     */
    inline void outputAdvance(double time)
    { next->incomingAdvance(time); }

    /**
     * Duplicate the action.  This does NOT duplicate any state it may have,
     * just the operation the action reflects.
     * 
     * @return          a new action
     */
    virtual Action *clone() const = 0;

    /**
     * Abort execution.
     */
    virtual void signalTerminate();

    /**
     * Start execution.
     */
    virtual void initialize();

    /**
     * Returns any additional inputs requested by the action.  These may
     * not necessarily be available in the final input.  This is generally
     * unnecessary as most actions should only operate on existing data.
     * 
     * @return  the requested inputs
     */
    virtual Data::SequenceName::Set requestedInputs() const;

    /**
     * Get any outputs generated by the action.  This is generally
     * unnecessary as most actions should only operate on existing data.
     * 
     * @return  the output units
     */
    virtual Data::SequenceName::Set predictedOutputs() const;

protected:
    Action(const Action &other);
};

/**
 * A NOOP action.  This does nothing to the data.
 */
class CPD3EDITING_EXPORT ActionNOOP : public Action {
public:
    /**
     * Create a new NOOP action.
     */
    ActionNOOP();

    virtual ~ActionNOOP();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);
};

/**
 * An invalidate action.  This sets any values it applies to to undefined.
 */
class CPD3EDITING_EXPORT ActionInvalidate : public Action, public EditDataTarget {
    Data::SequenceMatch::Composite selection;
    bool preserveType;
public:
    /**
     * Create a new invalidate action.
     * 
     * @param selection     the units to invalidate
     * @param preserveType  if set then preserve the type if possible (set doubles and integers to an undefined code)
     */
    ActionInvalidate(const Data::SequenceMatch::Composite &selection, bool preserveType = true);

    virtual ~ActionInvalidate();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

protected:
    ActionInvalidate(const ActionInvalidate &other);
};

/**
 * An remove action.  This discard everything it matches.
 */
class CPD3EDITING_EXPORT ActionRemove : public Action, public EditDataTarget {
    Data::SequenceMatch::Composite selection;
public:
    /**
     * Create a new remove action.
     * 
     * @param selection     the units to remove
     */
    ActionRemove(const Data::SequenceMatch::Composite &selection);

    virtual ~ActionRemove();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

protected:
    ActionRemove(const ActionRemove &other);
};

/**
 * An action that wraps incoming values to a modular range.
 */
class CPD3EDITING_EXPORT ActionWrapModular
        : public Action, public EditDataTarget, public EditDataModification {
    Data::SequenceMatch::Composite selection;
    double lower;
    double upper;
    double range;

    double apply(double value) const;

public:
    /**
     * Create a new modular wrapping action
     * 
     * @param selection     the units to apply to
     * @param lower         the lower bound
     * @param upper         the upper bound
     */
    ActionWrapModular(const Data::SequenceMatch::Composite &selection, double lower, double upper);

    virtual ~ActionWrapModular();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

    void modifySequenceValue(Data::SequenceValue &value) override;

protected:
    ActionWrapModular(const ActionWrapModular &other);
};

/**
 * An action that overlays a value at a given path.
 */
class CPD3EDITING_EXPORT ActionOverlayValue : public Action, public EditDataTarget {
    Data::SequenceMatch::Composite selection;
    Data::Variant::Root value;
    std::string path;
public:
    /**
     * Create a new overlay action.
     * 
     * @param selection     the units to apply to
     * @param value         the value to overlay
     * @param path          the path to overlay at
     */
    ActionOverlayValue(const Data::SequenceMatch::Composite &selection,
                       const Data::Variant::Read &value,
                       const std::string &path = std::string());

    virtual ~ActionOverlayValue();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

    virtual void incomingDataAdvance(double time);

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

protected:
    ActionOverlayValue(const ActionOverlayValue &other);
};

/**
 * An action that overlays metadata at a given path.
 */
class CPD3EDITING_EXPORT ActionOverlayMetadata : public ActionOverlayValue {
    Data::SequenceMatch::Composite selection;
public:
    /**
     * Create a new overlay action.
     * 
     * @param selection     the units to apply to
     * @param value         the value to overlay
     * @param path          the path to overlay at
     */
    ActionOverlayMetadata(const Data::SequenceMatch::Composite &selection,
                          const Data::Variant::Read &value,
                          const std::string &path = std::string());

    virtual ~ActionOverlayMetadata();

    virtual Action *clone() const;

    virtual void unhandledInput(const Data::SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers);

protected:
    ActionOverlayMetadata(const ActionOverlayMetadata &other);
};

}
}

#endif
