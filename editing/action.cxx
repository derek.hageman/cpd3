/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/action.hxx"
#include "datacore/variant/composite.hxx"
#include "datacore/segmentprocessingstage.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Editing {

/** @file editing/action.hxx
 * The execution of the operations of edit directives.
 */


Action::Action() : next(nullptr)
{ }

Action::~Action() = default;

Action::Action(const Action &) : next(nullptr)
{ }

void Action::incomingDataAdvance(double time)
{ Q_UNUSED(time); }

void Action::finalize()
{ }

bool Action::mustSynchronizeOutput() const
{ return false; }

bool Action::shouldRunDetached() const
{ return false; }

void Action::signalTerminate()
{ }

void Action::initialize()
{ }

Data::SequenceName::Set Action::requestedInputs() const
{ return Data::SequenceName::Set(); }

Data::SequenceName::Set Action::predictedOutputs() const
{ return Data::SequenceName::Set(); }


ActionNOOP::ActionNOOP() = default;

ActionNOOP::~ActionNOOP() = default;

Action *ActionNOOP::clone() const
{ return new ActionNOOP; }

void ActionNOOP::unhandledInput(const SequenceName &unit,
                                QList<EditDataTarget *> &inputs,
                                QList<EditDataTarget *> &outputs,
                                QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(unit);
    Q_UNUSED(inputs);
    Q_UNUSED(outputs);
    Q_UNUSED(modifiers);
}


ActionInvalidate::ActionInvalidate(const SequenceMatch::Composite &sel, bool preserve) : selection(
        sel), preserveType(preserve)
{ }

ActionInvalidate::~ActionInvalidate() = default;

ActionInvalidate::ActionInvalidate(const ActionInvalidate &other) = default;

Action *ActionInvalidate::clone() const
{ return new ActionInvalidate(*this); }

void ActionInvalidate::unhandledInput(const SequenceName &unit,
                                      QList<EditDataTarget *> &inputs,
                                      QList<EditDataTarget *> &outputs,
                                      QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    Q_UNUSED(modifiers);
    if (!selection.matches(unit))
        return;
    outputs.append(this);
}

void ActionInvalidate::incomingDataAdvance(double time)
{ outputAdvance(time); }

void ActionInvalidate::incomingSequenceValue(const SequenceValue &value)
{
    if (preserveType) {
        Variant::Root result(value.root());
        Variant::Composite::invalidate(result.write());
        outputData(SequenceValue(value.getIdentity(), std::move(result)));
        return;
    }

    outputData(SequenceValue(value.getIdentity(), Variant::Root()));
}


ActionRemove::ActionRemove(const SequenceMatch::Composite &sel) : selection(sel)
{ }

ActionRemove::~ActionRemove() = default;

ActionRemove::ActionRemove(const ActionRemove &other) = default;

Action *ActionRemove::clone() const
{ return new ActionRemove(*this); }

void ActionRemove::unhandledInput(const SequenceName &unit,
                                  QList<EditDataTarget *> &inputs,
                                  QList<EditDataTarget *> &outputs,
                                  QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    Q_UNUSED(modifiers);
    if (!selection.matches(unit))
        return;
    outputs.append(this);
}

void ActionRemove::incomingDataAdvance(double time)
{ outputAdvance(time); }

void ActionRemove::incomingSequenceValue(const SequenceValue &)
{ }


ActionWrapModular::ActionWrapModular(const SequenceMatch::Composite &sel, double l, double u)
        : selection(sel), lower(l), upper(u)
{
    if (!FP::defined(lower) || !FP::defined(upper) || lower >= upper) {
        range = 0.0;
    } else {
        range = upper - lower;
    }
}

ActionWrapModular::~ActionWrapModular() = default;

ActionWrapModular::ActionWrapModular(const ActionWrapModular &other) = default;

Action *ActionWrapModular::clone() const
{ return new ActionWrapModular(*this); }

void ActionWrapModular::unhandledInput(const SequenceName &unit, QList<EditDataTarget *> &,
                                       QList<EditDataTarget *> &outputs,
                                       QList<EditDataModification *> &modifiers)
{
    if (selection.matches(unit)) {
        outputs.append(this);
        return;
    }
    if (unit.isMeta() && selection.matches(unit.fromMeta())) {
        modifiers.append(this);
    }
}

void ActionWrapModular::incomingDataAdvance(double time)
{ outputAdvance(time); }

double ActionWrapModular::apply(double value) const
{
    if (!FP::defined(value))
        return value;
    if (range <= 0.0)
        return value;
    if (value < lower) {
        value += (double) abs((int) floor((value - lower) / range)) * range;
    }
    if (value >= upper) {
        value -= (double) abs((int) floor((value - lower) / range)) * range;
    }
    return value;
}

void ActionWrapModular::incomingSequenceValue(const SequenceValue &value)
{
    auto result = Variant::Composite::applyTransform(value.read(),
                                                     [this](const Variant::Read &input,
                                                            Variant::Write &output) {
                                                         output.setReal(apply(input.toReal()));
                                                     });
    outputData(SequenceValue(value.getIdentity(), std::move(result)));
}

void ActionWrapModular::modifySequenceValue(Data::SequenceValue &value)
{
    SegmentProcessingStage::clearPropagatedSmoothing(value.write().metadata("Smoothing"));
}


ActionOverlayValue::ActionOverlayValue(const SequenceMatch::Composite &sel,
                                       const Data::Variant::Read &v,
                                       const std::string &p) : selection(sel), value(v), path(p)
{ }

ActionOverlayValue::~ActionOverlayValue() = default;

ActionOverlayValue::ActionOverlayValue(const ActionOverlayValue &other) = default;

Action *ActionOverlayValue::clone() const
{ return new ActionOverlayValue(*this); }

void ActionOverlayValue::unhandledInput(const SequenceName &unit,
                                        QList<EditDataTarget *> &inputs,
                                        QList<EditDataTarget *> &outputs,
                                        QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    Q_UNUSED(modifiers);
    if (!selection.matches(unit))
        return;
    outputs.append(this);
}

void ActionOverlayValue::incomingDataAdvance(double time)
{ outputAdvance(time); }

void ActionOverlayValue::incomingSequenceValue(const SequenceValue &value)
{
    if (path.empty()) {
        outputData(SequenceValue(value.getIdentity(),
                                 Variant::Root::overlay(value.root(), this->value)));
        return;
    }

    auto merged = Variant::Root::overlay(Variant::Root(value.read().getPath(path)), this->value);

    Variant::Root output = value.root();
    output[path].set(merged);
    outputData(SequenceValue(value.getIdentity(), std::move(output)));
}


ActionOverlayMetadata::ActionOverlayMetadata(const SequenceMatch::Composite &sel,
                                             const Data::Variant::Read &v,
                                             const std::string &p) : ActionOverlayValue(sel, v, p),
                                                                 selection(sel)
{ }

ActionOverlayMetadata::~ActionOverlayMetadata() = default;

ActionOverlayMetadata::ActionOverlayMetadata(const ActionOverlayMetadata &other) = default;

Action *ActionOverlayMetadata::clone() const
{ return new ActionOverlayMetadata(*this); }

void ActionOverlayMetadata::unhandledInput(const SequenceName &unit,
                                           QList<EditDataTarget *> &inputs,
                                           QList<EditDataTarget *> &outputs,
                                           QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    Q_UNUSED(modifiers);
    if (!unit.isMeta())
        return;
    if (!selection.matches(unit.fromMeta()))
        return;
    outputs.append(this);
}

}
}
