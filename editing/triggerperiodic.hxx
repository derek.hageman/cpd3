/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGTRIGGERPERIODIC_H
#define CPD3EDITINGTRIGGERPERIODIC_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QSet>

#include "editing/editing.hxx"

#include "core/timeutils.hxx"
#include "datacore/stream.hxx"
#include "editing/trigger.hxx"

namespace CPD3 {
namespace Editing {

/**
 * The base class for periodic triggers.  This manages the maintenance of
 * an outer interval of time.
 */
class CPD3EDITING_EXPORT TriggerPeriodic : public Trigger {
    Time::LogicalTimeUnit intervalUnit;
    int intervalCount;
    bool intervalAligned;

    double intervalStartTime;
    double intervalEndTime;

    void setInterval(double time, double &start, double &end) const;

public:
    /**
     * Create a periodic trigger.
     * 
     * @param intervalUnit      the outer time interval unit
     * @param intervalCount     the outer time interval count
     * @param intervalAligned   the outer time interval alignment state
     */
    TriggerPeriodic(Time::LogicalTimeUnit intervalUnit,
                    int intervalCount = 1,
                    bool intervalAligned = false);

    virtual ~TriggerPeriodic();

    virtual bool isReady(double end) const;

protected:
    TriggerPeriodic(const TriggerPeriodic &other);

    /**
     * Get the current start time of the periodic interval.
     * 
     * @return      the start time
     */
    inline double getIntervalStart() const
    { return intervalStartTime; }

    /**
     * Get the current end time of the periodic interval.
     * 
     * @return      the start time
     */
    inline double getIntervalEnd() const
    { return intervalEndTime; }

    /**
     * Get the interval unit.
     * 
     * @return the interval unit
     */
    inline Time::LogicalTimeUnit getIntervalUnit() const
    { return intervalUnit; }

    /**
     * Get the interval count.
     * 
     * @return the interval count
     */
    inline int getIntervalCount() const
    { return intervalCount; }

    /**
     * Get the interval alignment.
     * 
     * @return the interval alignment
     */
    inline bool getIntervalAligned() const
    { return intervalAligned; }

    /**
     * Update the interval.
     * 
     * @param time  the time to advance to
     * @return      true if the interval was moved
     */
    bool updateInterval(double time);

    /**
     * Test if the given range spans multiple intervals and return the
     * start time of the first interval.  This is primarily used to determine
     * if a trigger would never activate.
     * 
     * @param start     the start time to test
     * @param end       the end time to test
     * @param intervalStart the output start of the first interval
     * @return          true if the period spans multiple intervals
     */
    bool testMultipleIntervals(double start, double end, double &intervalStart) const;
};

/**
 * A trigger that activates at specific moments within a larger time interval.
 */
class CPD3EDITING_EXPORT TriggerPeriodicMoment : public TriggerPeriodic {
    Time::LogicalTimeUnit momentUnit;
    QList<int> moments;

    struct Segment {
        double start;
        double end;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }
    };

    Segment *segments;
    Segment *firstCheckSegment;
public:
    /**
     * Create a moment trigger.
     * 
     * @param intervalUnit      the outer time interval unit
     * @param intervalCount     the outer time interval count
     * @param intervalAligned   the outer time interval alignment state
     * @param momentUnit        the unit the moments are specified in
     * @param moments           the moments to activate at
     */
    TriggerPeriodicMoment(Time::LogicalTimeUnit intervalUnit,
                          int intervalCount,
                          bool intervalAligned,
                          Time::LogicalTimeUnit momentUnit,
                          const QSet<int> &moments);

    virtual ~TriggerPeriodicMoment();

    virtual Trigger *clone() const;

    virtual bool isNeverActive(double start, double end) const;

protected:
    TriggerPeriodicMoment(const TriggerPeriodicMoment &other);

    virtual bool process(double start, double end);
};

/**
 * A trigger that activates for a range of time within a larger interval.
 */
class CPD3EDITING_EXPORT TriggerPeriodicRange : public TriggerPeriodic {
    Time::LogicalTimeUnit triggerStartUnit;
    int triggerStartCount;
    bool triggerStartAlign;

    Time::LogicalTimeUnit triggerEndUnit;
    int triggerEndCount;
    bool triggerEndAlign;

    double triggerStartTime;
    double triggerEndTime;
public:
    /**
     * Create a range trigger.
     * 
     * @param intervalUnit      the outer time interval unit
     * @param intervalCount     the outer time interval count
     * @param intervalAligned   the outer time interval alignment state
     * @param triggerStartUnit  the unit the trigger start is specified in
     * @param triggerStartCount the number of units the trigger start is offset by
     * @param triggerStartAlign the alignment state of the trigger start
     * @param triggerEndUnit    the unit the trigger end is specified in
     * @param triggerEndCount   the number of units the trigger end is offset by
     * @param triggerEndAlign   the alignment state of the trigger end
     */
    TriggerPeriodicRange(Time::LogicalTimeUnit intervalUnit,
                         int intervalCount,
                         bool intervalAligned,
                         Time::LogicalTimeUnit triggerStartUnit,
                         int triggerStartCount,
                         bool triggerStartAlign,
                         Time::LogicalTimeUnit triggerEndUnit,
                         int triggerEndCount,
                         bool triggerEndAlign);

    virtual ~TriggerPeriodicRange();

    virtual Trigger *clone() const;

    virtual bool isNeverActive(double start, double end) const;

protected:
    TriggerPeriodicRange(const TriggerPeriodicRange &other);

    virtual bool process(double start, double end);
};

}
}

#endif
