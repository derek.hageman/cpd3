/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "editing/actionscriptprocessor.hxx"
#include "luascript/libs/streamvalue.hxx"
#include "luascript/libs/sequencesegment.hxx"
#include "luascript/libs/sequencename.hxx"

using namespace CPD3::Data;


Q_LOGGING_CATEGORY(log_editing_actionscriptprocessor, "cpd3.editing.actionscriptprocessor",
                   QtWarningMsg)

namespace CPD3 {
namespace Editing {

void ActionScriptSequenceValueProcessor::init()
{
    buffer.pushExternalController(root);
    controller = root.back();
    environment = root.pushSandboxEnvironment();
    {
        Lua::Engine::Assign assign(root, environment, "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_editing_actionscriptprocessor) << entry[i].toOutputString();
            }
        }));
    }
    environment.set("control", controller);
    if (!root.pushChunk(code, environment)) {
        qCDebug(log_editing_actionscriptprocessor) << "Error parsing sequence value processor code:"
                                                   << root.errorDescription();
        root.clearError();
        return;
    }
    invoke = root.back();
}

ActionScriptSequenceValueProcessor::ActionScriptSequenceValueProcessor(SequenceMatch::OrderedLookup selection,
                                                                       std::string code)
        : selection(std::move(selection)),
          code(std::move(code)),
          engine(),
          root(engine),
          buffer(*this)
{ init(); }

ActionScriptSequenceValueProcessor::~ActionScriptSequenceValueProcessor() = default;

ActionScriptSequenceValueProcessor::ActionScriptSequenceValueProcessor(const ActionScriptSequenceValueProcessor &other)
        : Action(other),
          selection(other.selection),
          code(other.code),
          engine(),
          root(engine),
          buffer(*this)
{ init(); }

Action *ActionScriptSequenceValueProcessor::clone() const
{ return new ActionScriptSequenceValueProcessor(*this); }

bool ActionScriptSequenceValueProcessor::shouldRunDetached() const
{ return true; }

void ActionScriptSequenceValueProcessor::unhandledInput(const SequenceName &name,
                                                        QList<EditDataTarget *> &,
                                                        QList<EditDataTarget *> &outputs,
                                                        QList<EditDataModification *> &)
{
    if (!selection.matches(name))
        return;
    outputs.append(this);
}

SequenceName::Set ActionScriptSequenceValueProcessor::requestedInputs() const
{ return selection.knownInputs(); }

void ActionScriptSequenceValueProcessor::process()
{
    for (;;) {
        {
            Lua::Engine::Frame local(root);
            if (!buffer.pushExternal(local, controller, true))
                return;
            environment.set("data", local.back());
        }
        {
            Lua::Engine::Call call(root);
            call.push(invoke);
            if (!call.execute()) {
                call.clearError();
                return;
            }
        }
    }
}

void ActionScriptSequenceValueProcessor::incomingSequenceValue(const SequenceValue &value)
{
    if (!invoke.isAssigned())
        return;
    buffer.state = Buffer::State::Value;
    buffer.next = value;
    process();
}

void ActionScriptSequenceValueProcessor::incomingDataAdvance(double time)
{
    if (!invoke.isAssigned())
        return;
    buffer.state = Buffer::State::Advance;
    buffer.advance = time;
    process();
}

void ActionScriptSequenceValueProcessor::finalize()
{
    if (!invoke.isAssigned())
        return;
    buffer.finish(root, controller);
}

ActionScriptSequenceValueProcessor::Buffer::Buffer(ActionScriptSequenceValueProcessor &parent)
        : parent(parent), state(State::Empty), advance(FP::undefined())
{ }

ActionScriptSequenceValueProcessor::Buffer::~Buffer() = default;

bool ActionScriptSequenceValueProcessor::Buffer::pushNext(Lua::Engine::Frame &target)
{
    switch (state) {
    case State::Empty:
        return false;
    case State::Advance:
        state = State::Empty;
        target.push(advance);
        return true;
    case State::Value:
        state = State::Empty;
        target.pushData<Lua::Libs::SequenceValue>(std::move(next));
        return true;
    }
    Q_ASSERT(false);
    return false;
}

void ActionScriptSequenceValueProcessor::Buffer::outputReady(Lua::Engine::Frame &frame,
                                                             const Lua::Engine::Reference &ref)
{ parent.outputData(extract(frame, ref)); }

void ActionScriptSequenceValueProcessor::Buffer::advanceReady(double time)
{ parent.outputAdvance(time); }

void ActionScriptSequenceValueProcessor::Buffer::endReady()
{ }


void ActionScriptSequenceSegmentProcessor::init()
{
    buffer.pushExternalController(root);
    controller = root.back();
    environment = root.pushSandboxEnvironment();
    environment.set("control", controller);
    {
        Lua::Engine::Assign assign(root, environment, "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_editing_actionscriptprocessor) << entry[i].toOutputString();
            }
        }));
    }
    if (!root.pushChunk(code, environment)) {
        qCDebug(log_editing_actionscriptprocessor)
            << "Error parsing sequence segment processor code:" << root.errorDescription();
        root.clearError();
        return;
    }
    invoke = root.back();
}

ActionScriptSequenceSegmentProcessor::ActionScriptSequenceSegmentProcessor(SequenceMatch::OrderedLookup outputs,
                                                                           SequenceMatch::OrderedLookup inputs,
                                                                           std::string code,
                                                                           bool implicitMeta)
        : outputs(outputs),
          inputs(std::move(inputs)),
          code(std::move(code)),
          implicitMeta(implicitMeta),
          registeredOutputs(std::move(outputs)),
          engine(),
          root(engine),
          buffer(*this)
{ init(); }

ActionScriptSequenceSegmentProcessor::~ActionScriptSequenceSegmentProcessor() = default;

ActionScriptSequenceSegmentProcessor::ActionScriptSequenceSegmentProcessor(const ActionScriptSequenceSegmentProcessor &other)
        : Action(other),
          outputs(other.outputs),
          inputs(other.inputs),
          code(other.code),
          implicitMeta(other.implicitMeta),
          registeredOutputs(outputs),
          engine(),
          root(engine),
          buffer(*this)
{ init(); }

Action *ActionScriptSequenceSegmentProcessor::clone() const
{ return new ActionScriptSequenceSegmentProcessor(*this); }

bool ActionScriptSequenceSegmentProcessor::shouldRunDetached() const
{ return true; }

void ActionScriptSequenceSegmentProcessor::unhandledInput(const SequenceName &unit,
                                                          QList<EditDataTarget *> &inputs,
                                                          QList<EditDataTarget *> &outputs,
                                                          QList<EditDataModification *> &)
{
    if (implicitMeta && unit.isMeta()) {
        auto nometa = unit.fromMeta();
        if (this->inputs.matches(nometa) || this->outputs.matches(nometa))
            inputs.append(this);
        return;
    }

    if (registeredOutputs.registerInput(unit)) {
        outputs.append(this);
        return;
    }

    if (this->inputs.matches(unit)) {
        inputs.append(this);
        return;
    }
}

SequenceName::Set ActionScriptSequenceSegmentProcessor::requestedInputs() const
{ return inputs.knownInputs(); }

SequenceName::Set ActionScriptSequenceSegmentProcessor::predictedOutputs() const
{ return outputs.knownInputs(); }

void ActionScriptSequenceSegmentProcessor::process(bool ignoreEnd)
{
    for (;;) {
        {
            Lua::Engine::Frame local(root);
            if (!buffer.pushExternal(local, controller, ignoreEnd))
                return;
            environment.set("data", local.back());
        }
        {
            Lua::Engine::Call call(root);
            call.push(invoke);
            if (!call.execute()) {
                call.clearError();
                return;
            }
        }
    }
}

void ActionScriptSequenceSegmentProcessor::incomingSequenceValue(const SequenceValue &value)
{
    if (!invoke.isAssigned())
        return;
    Util::append(reader.add(value), buffer.pending);
    process();
}

void ActionScriptSequenceSegmentProcessor::incomingDataAdvance(double time)
{
    if (!invoke.isAssigned())
        return;
    Util::append(reader.advance(time), buffer.pending);
    buffer.advance = time;
    process();
}

void ActionScriptSequenceSegmentProcessor::finalize()
{
    if (!invoke.isAssigned())
        return;
    Util::append(reader.finish(), buffer.pending);
    process(false);
    buffer.finish(root, controller);
}

ActionScriptSequenceSegmentProcessor::Buffer::Buffer(ActionScriptSequenceSegmentProcessor &parent)
        : parent(parent), offset(0), advance(FP::undefined())
{ }

ActionScriptSequenceSegmentProcessor::Buffer::~Buffer() = default;

bool ActionScriptSequenceSegmentProcessor::Buffer::pushNext(Lua::Engine::Frame &target)
{
    if (offset < pending.size()) {
        target.pushData<Lua::Libs::SequenceSegment>(std::move(pending[offset]));
        ++offset;
        return true;
    }
    offset = 0;
    pending.clear();

    if (FP::defined(advance)) {
        target.push(advance);
        advance = FP::undefined();
        return true;
    }

    return false;
}

void ActionScriptSequenceSegmentProcessor::Buffer::outputReady(Lua::Engine::Frame &frame,
                                                               const Lua::Engine::Reference &ref)
{ parent.outputData(extract(frame, ref, parent.registeredOutputs.knownInputs())); }

void ActionScriptSequenceSegmentProcessor::Buffer::advanceReady(double time)
{ parent.outputAdvance(time); }

void ActionScriptSequenceSegmentProcessor::Buffer::endReady()
{ }


void ActionScriptFanoutProcessor::init()
{
    bypass = mux.createSimple();

    fanout.pushController(root);
    controller = root.back();

    auto environment = root.pushSandboxEnvironment();
    {
        Lua::Engine::Assign assign(root, environment, "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_editing_actionscriptprocessor) << entry[i].toOutputString();
            }
        }));
    }
    environment.set("fanout", controller);
    {
        Lua::Engine::Call call(root);
        if (!call.pushChunk(code, environment)) {
            qCDebug(log_editing_actionscriptprocessor) << "Error parsing fanout processor code:"
                                                       << call.errorDescription();
            call.clearError();
            return;
        }
        if (!call.execute()) {
            qCDebug(log_editing_actionscriptprocessor) << "Error initializing fanout action code:"
                                                       << call.errorDescription();
            call.clearError();
            return;
        }
    }

    inError = false;
}

ActionScriptFanoutProcessor::ActionScriptFanoutProcessor(std::string code, bool processMetadata)
        : code(std::move(code)),
          processMetadata(processMetadata),
          engine(),
          root(engine),
          fanout(*this),
          inError(true)
{ init(); }

ActionScriptFanoutProcessor::~ActionScriptFanoutProcessor() = default;

ActionScriptFanoutProcessor::ActionScriptFanoutProcessor(const ActionScriptFanoutProcessor &other)
        : Action(other),
          code(other.code),
          processMetadata(other.processMetadata),
          engine(),
          root(engine),
          fanout(*this),
          inError(true)
{ init(); }

Action *ActionScriptFanoutProcessor::clone() const
{ return new ActionScriptFanoutProcessor(*this); }

void ActionScriptFanoutProcessor::error()
{
    if (inError)
        return;
    inError = true;

    mux.creationComplete();

    for (const auto &base : fanout.allTargets()) {
        auto target = dynamic_cast<ForegroundTarget *>(base.get());
        if (!target)
            continue;
        target->error();
    }

    mux.output();
}

void ActionScriptFanoutProcessor::unhandledInput(const Data::SequenceName &name,
                                                 QList<EditDataTarget *> &,
                                                 QList<EditDataTarget *> &outputs,
                                                 QList<EditDataModification *> &)
{
    if (!processMetadata && name.isMeta())
        return;
    outputs.append(this);
}

void ActionScriptFanoutProcessor::incomingSequenceValue(const Data::SequenceValue &value)
{
    if (inError)
        return;

    bool doOutput = false;
    for (const auto &base : fanout.dispatch(root, controller, value.getName())) {
        doOutput = static_cast<BaseTarget *>(base.get())->incomingSequenceValue(value) || doOutput;
    }
    if (doOutput)
        outputData(mux.output());
}

void ActionScriptFanoutProcessor::incomingDataAdvance(double time)
{
    if (inError) {
        bypass->advance(time);
        mux.output();
        outputAdvance(mux.getCurrentAdvance());
        return;
    }

    bool doOutput = false;
    for (const auto &base : fanout.allTargets()) {
        doOutput = static_cast<BaseTarget *>(base.get())->incomingDataAdvance(time) || doOutput;
    }
    doOutput = bypass->advance(time) || doOutput;

    if (doOutput)
        outputData(mux.output());
}

void ActionScriptFanoutProcessor::finalize()
{
    if (inError)
        return;

    for (const auto &base : fanout.allTargets()) {
        static_cast<BaseTarget *>(base.get())->finalize();
    }

    mux.creationComplete();
    bypass->end();
    bypass = nullptr;

    outputData(mux.output());
}

bool ActionScriptFanoutProcessor::shouldRunDetached() const
{ return true; }

ActionScriptFanoutProcessor::Fanout::Fanout(ActionScriptFanoutProcessor &parent) : parent(parent)
{ }

ActionScriptFanoutProcessor::Fanout::~Fanout() = default;

std::shared_ptr<Lua::FanoutController::Target> ActionScriptFanoutProcessor::Fanout::createTarget(
        const Lua::Engine::Output &type)
{
    if (type.toBoolean())
        return std::make_shared<BackgroundTarget>();
    return std::make_shared<ForegroundTarget>(parent);
}

void ActionScriptFanoutProcessor::Fanout::addedDispatch(Lua::Engine::Frame &frame,
                                                        const Lua::Engine::Reference &controller,
                                                        const Data::SequenceName &name,
                                                        std::vector<std::shared_ptr<
                                                                Lua::FanoutController::Target>> &targets)
{
    bool haveBackground = false;
    for (const auto &check : targets) {
        if (dynamic_cast<BackgroundTarget *>(check.get())) {
            haveBackground = true;
            break;
        }
    }
    if (haveBackground) {
        targets.emplace_back(std::make_shared<BypassTarget>(parent));
        return;
    }
    bool haveForeground = false;
    for (const auto &check : targets) {
        if (auto fg = dynamic_cast<ForegroundTarget *>(check.get())) {
            fg->addedDispatch(frame, controller, name);
            haveForeground = true;
        }
    }
    if (!haveForeground) {
        targets.emplace_back(std::make_shared<BypassTarget>(parent));
    }
}

ActionScriptFanoutProcessor::BaseTarget::BaseTarget() = default;

ActionScriptFanoutProcessor::BaseTarget::~BaseTarget() = default;

bool ActionScriptFanoutProcessor::BaseTarget::incomingDataAdvance(double)
{ return false; }

void ActionScriptFanoutProcessor::BaseTarget::finalize()
{ }


ActionScriptFanoutProcessor::BackgroundTarget::BackgroundTarget() = default;

ActionScriptFanoutProcessor::BackgroundTarget::~BackgroundTarget() = default;

bool ActionScriptFanoutProcessor::BackgroundTarget::incomingSequenceValue(const Data::SequenceValue &value)
{
    reader.add(value);
    return false;
}

bool ActionScriptFanoutProcessor::BackgroundTarget::initializeCall(Lua::Engine::Frame &arguments,
                                                                   const Lua::Engine::Reference &,
                                                                   const Lua::Engine::Reference &,
                                                                   const std::vector<
                                                                           std::shared_ptr<
                                                                                   Lua::FanoutController::Target>> &,
                                                                   Lua::Engine::Table &)
{
    arguments.clear();
    return false;
}

ActionScriptFanoutProcessor::BypassTarget::BypassTarget(ActionScriptFanoutProcessor &parent)
        : output(parent.bypass)
{ }

ActionScriptFanoutProcessor::BypassTarget::~BypassTarget() = default;

bool ActionScriptFanoutProcessor::BypassTarget::incomingSequenceValue(const Data::SequenceValue &value)
{ return output->incomingValue(value); }

ActionScriptFanoutProcessor::ForegroundTarget::ForegroundTarget(ActionScriptFanoutProcessor &parent)
        : parent(parent), buffer(*this), sink(parent.mux.createSimple()), sinkReady(false)
{ }

ActionScriptFanoutProcessor::ForegroundTarget::~ForegroundTarget() = default;

void ActionScriptFanoutProcessor::ForegroundTarget::process(bool ignoreEnd)
{
    if (!sink)
        return;

    Lua::Engine::Frame local(parent.root);
    pushSaved(local, parent.controller, 2);
    auto bcontroller = local.back();
    pushSaved(local, parent.controller, 0);
    auto invoke = local.back();

    for (;;) {
        if (!sink)
            return;

        Lua::Engine::Call call(local);
        call.push(invoke);
        if (!buffer.pushExternal(call, bcontroller, ignoreEnd))
            return;
        if (!call.execute()) {
            error();
            call.clearError();
            return;
        }
    }
}

bool ActionScriptFanoutProcessor::ForegroundTarget::incomingSequenceValue(const Data::SequenceValue &value)
{
    if (!sink)
        return false;

    Util::append(reader.add(value), buffer.pending);
    sinkReady = false;
    process();
    return sinkReady;
}

bool ActionScriptFanoutProcessor::ForegroundTarget::incomingDataAdvance(double time)
{
    if (!sink)
        return false;

    Util::append(reader.advance(time), buffer.pending);
    buffer.advance = time;
    sinkReady = false;
    process();
    return sinkReady;
}

void ActionScriptFanoutProcessor::ForegroundTarget::finalize()
{
    if (!sink)
        return;

    Util::append(reader.finish(), buffer.pending);
    process(false);

    Lua::Engine::Frame local(parent.root);
    pushSaved(local, parent.controller, 2);
    auto bcontroller = local.back();
    buffer.finish(local, bcontroller);

    if (sink) {
        sink->end();
        sink = nullptr;
    }
}

void ActionScriptFanoutProcessor::ForegroundTarget::error()
{
    if (!sink)
        return;
    sink->end();
    sink = nullptr;
}

bool ActionScriptFanoutProcessor::ForegroundTarget::initializeCall(Lua::Engine::Frame &arguments,
                                                                   const Lua::Engine::Reference &controller,
                                                                   const Lua::Engine::Reference &key,
                                                                   const std::vector<
                                                                           std::shared_ptr<
                                                                                   Lua::FanoutController::Target>> &background,
                                                                   Lua::Engine::Table &context)
{
    for (const auto &check : background) {
        auto btarget = dynamic_cast<BackgroundTarget *>(check.get());
        if (!btarget)
            continue;
        reader.overlay(btarget->reader);
    }

    buffer.advance = parent.mux.getCurrentAdvance();
    reader.advance(buffer.advance);

    buffer.pushExternalController(arguments);
    context.set("bcontrol", arguments.back());

    arguments.push(key);

    arguments.propagate(2);
    return true;
}

void ActionScriptFanoutProcessor::ForegroundTarget::processSaved(Lua::Engine::Frame &saved,
                                                                 const Lua::Engine::Reference &,
                                                                 const Lua::Engine::Reference &,
                                                                 const std::vector<std::shared_ptr<
                                                                         Lua::FanoutController::Target>> &,
                                                                 Lua::Engine::Table &context)
{
    saved.resize(2);
    if (saved[0].isNil()) {
        error();
        return;
    }
    saved.push(context, "bcontrol");
}

void ActionScriptFanoutProcessor::ForegroundTarget::addedDispatch(Lua::Engine::Frame &frame,
                                                                  const Lua::Engine::Reference &controller,
                                                                  const Data::SequenceName &name)
{
    Lua::Engine::Call call(frame);
    pushSaved(call, controller, 1);
    if (call.back().isNil()) {
        outputs.insert(name);
        return;
    }
    call.pushData<Lua::Libs::SequenceName>(name);
    if (!call.executeVariable()) {
        error();
        call.clearError();
        return;
    }
    for (std::size_t i = 0, max = call.size(); i < max; i++) {
        outputs.insert(Lua::Libs::SequenceName::extract(call, call[i]));
    }
}

ActionScriptFanoutProcessor::ForegroundTarget::Buffer::Buffer(ForegroundTarget &parent) : parent(
        parent), offset(0), advance(FP::undefined())
{ }

ActionScriptFanoutProcessor::ForegroundTarget::Buffer::~Buffer() = default;

bool ActionScriptFanoutProcessor::ForegroundTarget::Buffer::pushNext(Lua::Engine::Frame &target)
{
    if (offset < pending.size()) {
        target.pushData<Lua::Libs::SequenceSegment>(std::move(pending[offset]));
        ++offset;
        return true;
    }
    offset = 0;
    pending.clear();

    if (FP::defined(advance)) {
        target.push(advance);
        advance = FP::undefined();
        return true;
    }

    return false;
}

void ActionScriptFanoutProcessor::ForegroundTarget::Buffer::outputReady(Lua::Engine::Frame &frame,
                                                                        const Lua::Engine::Reference &ref)
{
    if (!parent.sink)
        return;
    parent.sinkReady =
            parent.sink->incoming(extract(frame, ref, parent.outputs)) || parent.sinkReady;
}

void ActionScriptFanoutProcessor::ForegroundTarget::Buffer::advanceReady(double time)
{ parent.sinkReady = parent.sink->advance(time) || parent.sinkReady; }

void ActionScriptFanoutProcessor::ForegroundTarget::Buffer::endReady()
{
    parent.sinkReady = true;
    parent.sink->end();
    parent.sink = nullptr;
}


}
}
