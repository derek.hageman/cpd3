/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cstdint>
#include <string>
#include <QLoggingCategory>

#include "editing/editingengine.hxx"
#include "editing/editcore.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"
#include "core/memory.hxx"
#include "smoothing/editingengine.hxx"
#include "smoothing/contamfilter.hxx"


Q_LOGGING_CATEGORY(log_editing_engine, "cpd3.editing.engine", QtWarningMsg)

using namespace CPD3::Data;
using namespace CPD3::Smoothing;
using namespace CPD3::Editing::Internal;

namespace CPD3 {
namespace Editing {

/** @file editing/editingengine.hxx
 * The implementation of the generation of processed edited data.
 */


EditingEngine::EditingEngine(double start,
                             double eend,
                             const SequenceName::Component &station,
                             const QString &profile,
                             Data::Archive::Access *access) : startTime(start),
                                                              endTime(eend),
                                                              station(station),
                                                              profile(profile.toLower()),
                                                              archive("raw"),
                                                              outputArchive("clean"),
                                                              outputAverages("avgh"),
                                                              inputSelections(),
                                                              access(access),
                                                              localAccess(),
                                                              activeRead(),
                                                              chain(),
                                                              initialEgress(nullptr),
                                                              outputFilter(this)
{ }

EditingEngine::~EditingEngine()
{
    /*
     * FIXME: These are still QObjects (for now), so make sure their owning thread is the one
     * that deletes them.
     */
    if (context) {
        context->ref.release()->deleteLater();
        for (;;) {
            {
                std::lock_guard<std::mutex> lock(context->mutex);
                context->valid = false;
                if (!context->held)
                    break;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }
    for (auto &f : chain) {
        f->signalTerminate();
        f->wait();
    }
}

void EditingEngine::incomingData(const Util::ByteView &)
{ }

void EditingEngine::endData()
{ }

void EditingEngine::setEgress(Data::StreamSink *egress)
{
    if (chain.empty()) {
        initialEgress = egress;
        return;
    }
    if (egress == outputFilter.egress)
        return;

    Q_ASSERT(access);

    chain.back()->setEgress(nullptr);
    outputFilter.egress = egress;
    if (!egress)
        return;
    chain.back()->setEgress(&outputFilter);
}

void EditingEngine::signalTerminate()
{
    if (!access)
        return;
    if (activeRead)
        activeRead->signalTerminate();
    access->signalTerminate();
    for (const auto &f : chain) {
        f->signalTerminate();
    }
}

bool EditingEngine::isFinished()
{
    if (!access)
        return false;
    if (activeRead && !activeRead->isComplete())
        return false;
    for (const auto &f : chain) {
        if (!f->isFinished())
            return false;
    }
    return true;
}


bool EditingEngine::wait(double timeout)
{
    if (!access)
        return false;
    if (FP::defined(timeout) && timeout <= 0.0)
        return isFinished();

    if (!FP::defined(timeout)) {
        if (activeRead && !activeRead->wait())
            return false;
        for (const auto &f : chain) {
            if (!f->wait())
                return false;
        }
        return true;
    }

    ElapsedTimer st;
    st.start();

    if (activeRead && !activeRead->wait(timeout))
        return false;
    for (const auto &f : chain) {
        int remaining = static_cast<int>(timeout * 1000.0) - st.elapsed();
        if (remaining < 0)
            remaining = 0;
        if (!f->wait(remaining / 1000.0))
            return false;
    }
    return true;
}

void EditingEngine::checkAllFinished(const std::shared_ptr<Context> &context)
{
    {
        std::lock_guard<std::mutex> lock(context->mutex);
        if (!context->valid)
            return;
        context->held++;
    }
    bool invalidate = false;
    if (isFinished()) {
        invalidate = true;
        finished();
    }
    {
        std::lock_guard<std::mutex> lock(context->mutex);
        context->held--;
        if (invalidate)
            context->valid = false;
    }
}

bool EditingEngine::instantiateDirectives(QList<DirectiveContainer>::const_iterator beginInstance,
                                          QList<DirectiveContainer>::const_iterator endInstance,
                                          SequenceMatch::Composite **variablesContinuous,
                                          SequenceMatch::Composite **variablesFull)
{
    std::vector<std::unique_ptr<EditDirective>> directives;
    SequenceMatch::Composite addContinuous;
    SequenceMatch::Composite addFull;
    for (; beginInstance != endInstance; ++beginInstance) {
        auto add = beginInstance->instantiate(startTime, endTime, station);
        if (add.empty())
            continue;
        Util::append(std::move(add), directives);

        beginInstance->getAveraging(addContinuous, addFull);
    }
    if (directives.empty())
        return false;

    auto core = new EditCore(std::move(directives));
    Q_ASSERT(core);
    chain.emplace_back(core);

    if (variablesContinuous && addContinuous.valid())
        *variablesContinuous = new SequenceMatch::Composite(addContinuous);
    if (variablesFull && addFull.valid())
        *variablesFull = new SequenceMatch::Composite(addFull);

    return true;
}

namespace {
class ArchiveContaminationFilter : public EditingContaminationFilter {
    Archive::Access &access;
public:
    ArchiveContaminationFilter(Archive::Access &access,
                               const QString &id,
                               Data::SequenceMatch::Composite *variables = nullptr)
            : EditingContaminationFilter(id, variables), access(access)
    { }

    virtual ~ArchiveContaminationFilter() = default;

    virtual Data::DynamicSequenceSelection *systemContaminationAffected(const SequenceName::Component &station,
                                                                        const QString &type = QString::fromLatin1(
                                                                                "aerosol"),
                                                                        double start = FP::undefined(),
                                                                        double end = FP::undefined())
    {
        auto config = ValueSegment::Stream::read(
                {start, end, {station}, {"configuration"}, {"contamination"}}, &access);
        return DynamicSequenceSelection::fromConfiguration(config, QString("Affected/%1").arg(type),
                                                           start, end);
    }
};

class ArchiveContaminationFinal : public EditingContaminationFinal {
    Archive::Access &access;
public:
    ArchiveContaminationFinal(Archive::Access &access, const QString &id)
            : EditingContaminationFinal(id), access(access)
    { }

    virtual ~ArchiveContaminationFinal() = default;

    virtual Data::DynamicSequenceSelection *systemContaminationAffected(const SequenceName::Component &station,
                                                                        const QString &type = QString::fromLatin1(
                                                                                "aerosol"),
                                                                        double start = FP::undefined(),
                                                                        double end = FP::undefined())
    {
        auto config = ValueSegment::Stream::read(
                {start, end, {station}, {"configuration"}, {"contamination"}}, &access);
        return DynamicSequenceSelection::fromConfiguration(config, QString("Affected/%1").arg(type),
                                                           start, end);
    }
};
}

/* These exist to make different implementations of the sort happy, so just ignore
 * when they're not used. */
#pragma GCC diagnostic ignored "-Wunused-function"

static bool operator<(const DirectiveContainer &a, qint64 b)
{ return a.sortLessThan(b); }

static bool operator>(const DirectiveContainer &a, qint64 b)
{ return a.sortGreaterThan(b); }

static bool operator<(qint64 a, const DirectiveContainer &b)
{ return b.sortGreaterThan(a); }

static bool operator>(qint64 a, const DirectiveContainer &b)
{ return b.sortLessThan(a); }

void EditingEngine::createChain()
{
    Q_ASSERT(!activeRead);

    if (!access) {
        localAccess.reset(new Archive::Access);
        access = localAccess.get();
    }
    Archive::Access::ReadLock lock(*access);

    auto config = ValueSegment::Stream::read(
            Archive::Selection(startTime, endTime, {station}, {"configuration"}, {"editing"}),
            access);
    for (auto seg = config.begin(); seg != config.end();) {
        if (!Range::intersects(seg->getStart(), seg->getEnd(), startTime, endTime)) {
            seg = config.erase(seg);
            continue;
        }
        ++seg;
    }

    std::vector<EditingOutputSelection> outputSelections;
    Archive::Selection::List editSelections;
    std::map<std::int_fast64_t, ValueSegment::Transfer> filterConfig;

    editSelections.push_back(
            Archive::Selection(startTime, endTime, {station}, {"edits"}, {profile.toStdString()}));
    editSelections.back().includeMetaArchive = false;

    QString contaminationProfile(profile);
    bool disableContamination = false;
    bool disableAveraging = false;
    double inputStart = startTime;
    double inputEnd = endTime;

    /* Go through each configuration segment and initialize what we need
     * to load */
    for (const auto &segment: config) {
        auto cfg = segment.getValue().hash("Profiles").hash(profile);

        std::unordered_set<Data::SequenceName::Component> addEdits;
        for (const auto &add: cfg["LoadEdits"].toChildren().keys()) {
            addEdits.insert(add);
        }
        if (!addEdits.empty()) {
            for (const auto &addProfile : addEdits) {
                editSelections.push_back(
                        Archive::Selection(startTime, endTime, {station}, {"edits"}, {addProfile}));
                editSelections.back().includeMetaArchive = false;
            }
        }

        if (!cfg["Output/Archive"].toString().empty()) {
            outputArchive = cfg["Output/Archive"].toQString().toLower().toStdString();
        }
        if (!cfg["Output/Averages"].toString().empty()) {
            outputAverages = cfg["Output/Averages"].toQString().toLower().toStdString();
        }
        if (!cfg["Contamination"].toString().empty()) {
            contaminationProfile = cfg["Contamination"].toQString();
        }
        if (cfg["Output/AverageContaminatedData"].exists()) {
            disableContamination = cfg["Output/AverageContaminatedData"].toBool();
        }
        if (cfg["Output/DisableAverage"].exists()) {
            disableAveraging = cfg["Output/DisableAverage"].toBool();
        }

        EditingOutputSelection outSel;
        outSel.start = segment.getStart();
        if (Range::compareStart(outSel.start, startTime) < 0)
            outSel.start = startTime;
        outSel.end = segment.getEnd();
        if (Range::compareEnd(outSel.end, endTime) > 0)
            outSel.end = endTime;
        outSel.selection = SequenceMatch::OrderedLookup(cfg["Output/Data"]);
        outSel.selection.registerExpected(station, outputArchive);
        outSel.selection.registerExpected(station, outputAverages);

        if (cfg["Input/Data"].exists()) {
            SequenceMatch::Composite sel(cfg["Input/Data"]);
            auto selections = sel.toArchiveSelections({station}, {archive});
            for (auto &mod : selections) {
                mod.start = outSel.start;
                mod.end = outSel.end;
#ifndef NDEBUG
                if (mod.variables.empty() ||
                        std::find(mod.variables.begin(), mod.variables.end(), std::string()) !=
                                mod.variables.end()) {
                    qCDebug(log_editing_engine) << "Global variable input detected:" << mod
                                                << "from" << sel;
                }
#endif
            }
            Util::append(std::move(selections), inputSelections);
        }

        outputSelections.emplace_back(std::move(outSel));

        /* Add filters for the segment */
        for (auto add : cfg["Filters"].toHash()) {
            bool ok = false;
            long long p = QString::fromStdString(add.first).toLongLong(&ok);
            if (!ok || p <= 0) {
                qCDebug(log_editing_engine) << "Invalid filter priority at"
                                            << Logging::range(segment.getStart(), segment.getEnd())
                                            << ":" << add.first;
                continue;
            }
            QString componentName(add.second["Component"].toQString());
            if (componentName.isEmpty())
                continue;

            auto target = filterConfig.find(p);
            if (target == filterConfig.end()) {
                target = filterConfig.emplace(p, ValueSegment::Transfer()).first;
            }

            if (!target->second.empty()) {
                if (target->second.back()["Component"].toQString() != componentName) {
                    qCDebug(log_editing_engine) << "Filter priority " << p
                                                << "changed from component"
                                                << target->second.back()["Component"].toQString()
                                                << "at"
                                                << Logging::time(target->second.back().getEnd())
                                                << "to" << componentName << "at"
                                                << Logging::time(segment.getStart());
                }
            }

            target->second
                  .emplace_back(segment.getStart(), segment.getEnd(), Variant::Root(add.second));
        }
    }
    for (const auto &sel : outputSelections) {
        auto selections = sel.selection.toArchiveSelections({station}, {archive});
        for (auto &mod : selections) {
            mod.start = sel.start;
            mod.end = sel.end;
#ifndef NDEBUG
            if (mod.variables.empty() ||
                    std::find(mod.variables.begin(), mod.variables.end(), std::string()) !=
                            mod.variables.end()) {
                qCDebug(log_editing_engine) << "Global variable input detected:" << mod << "from"
                                            << sel.selection;
            }
#endif
        }
        Util::append(std::move(selections), inputSelections);
    }
    if (disableAveraging) {
        disableContamination = true;
    }

    /* Second pass we extend what we're loading from the filters */
    for (const auto &fc: filterConfig) {
        Q_ASSERT(!fc.second.empty());

        QString componentName(fc.second.back()["Component"].toQString());
        Q_ASSERT(!componentName.isEmpty());

        QObject *component = ComponentLoader::create(componentName);
        if (!component) {
            qCWarning(log_editing_engine) << "Can't load component" << componentName;
            continue;
        }
        ProcessingStageComponent *filterComponent;
        if (!(filterComponent = qobject_cast<ProcessingStageComponent *>(component))) {
            qCWarning(log_editing_engine) << "Component" << componentName
                                          << "is not a general filter";
            continue;
        }

        double loadStart = startTime;
        double loadEnd = endTime;
        filterComponent->extendGeneralFilterEditing(loadStart, loadEnd, station, archive,
                                                    ValueSegment::withPath(fc.second, "Parameters"),
                                                    access);

        if (Range::compareStart(loadStart, inputStart) < 0)
            inputStart = loadStart;
        if (Range::compareEnd(loadEnd, inputEnd) > 0)
            inputEnd = loadEnd;
    }
    /* Modify the directives we're loading from the filter extensions */
    for (auto &mod : editSelections) {
        mod.start = inputStart;
        mod.end = inputEnd;
    }

    /* Load edit directives */
    QList<DirectiveContainer> rawDirectives;
    {
        StreamSink::Iterator load;
        access->readStream(editSelections, &load)->detach();
        while (load.hasNext()) {
            rawDirectives.append(DirectiveContainer(load.next()));
        }
    }
    std::stable_sort(rawDirectives.begin(), rawDirectives.end());

    /* Extend input data loading based on the directives */
    for (QList<DirectiveContainer>::const_iterator dir = rawDirectives.constBegin(),
            endDir = rawDirectives.constEnd(); dir != endDir; ++dir) {
        double loadStart = startTime;
        double loadEnd = endTime;
        dir->extend(loadStart, loadEnd, access, station, archive);
        if (Range::compareStart(loadStart, inputStart) < 0)
            inputStart = loadStart;
        if (Range::compareEnd(loadEnd, inputEnd) > 0)
            inputEnd = loadEnd;
    }

    /* Add the initial directives before the first averaging */
    QList<DirectiveContainer>::const_iterator beginInstance = rawDirectives.constBegin();
    QList<DirectiveContainer>::const_iterator
            endInstance = std::upper_bound(beginInstance, rawDirectives.constEnd(), (qint64) 0);
    int totalEditCores = 0;
    if (instantiateDirectives(beginInstance, endInstance)) {
        ++totalEditCores;
    }
    beginInstance = endInstance;


    DynamicTimeInterval::Variable hourlyAverage;
    hourlyAverage.set(Time::Hour, 1, true);

    DynamicTimeInterval *averageInterval = nullptr;
    DynamicDouble *averageCover = nullptr;

    if (!disableAveraging) {
        averageInterval =
                DynamicTimeInterval::fromConfiguration(config, "Profiles/" + profile + "/Average",
                                                       startTime, endTime, true, &hourlyAverage);
        averageCover = DynamicDoubleOption::fromConfiguration(config, "Profiles/" +
                profile +
                "/RequiredCoverage", startTime, endTime);

        /* Add the first averaging step */
        if (disableContamination) {
            chain.emplace_back(new EditingContaminationFilter(new DynamicSequenceSelection::None));
        } else {
            chain.emplace_back(new ArchiveContaminationFilter(*access, contaminationProfile));
        }
        chain.emplace_back(
                new EditingEngineFullCalculate(averageInterval->clone(), averageCover->clone(),
                                               new SequenceMatch::Composite(
                                                       SequenceMatch::Element::SpecialMatch::Data)));
    }

    SequenceMatch::Composite *variablesContinuous = nullptr;
    SequenceMatch::Composite *variablesFull = nullptr;

    /* Add each filter and any edits that go before it */
    int nProcessingComponents = 0;
    for (const auto &fc : filterConfig) {
        if (!disableAveraging) {
            /* Add the averaging that went after the prior filter */
            if (variablesContinuous) {
                chain.emplace_back(new EditingEngineContinuousRecalculate(averageInterval->clone(),
                                                                          averageCover->clone(),
                                                                          variablesContinuous));
                variablesContinuous = nullptr;
            }
            if (variablesFull) {
                if (disableContamination) {
                    chain.emplace_back(
                            new EditingContaminationFilter(new DynamicSequenceSelection::None,
                                                           new SequenceMatch::Composite(
                                                                   *variablesFull)));
                } else {
                    chain.emplace_back(new ArchiveContaminationFilter(*access, contaminationProfile,
                                                                      new SequenceMatch::Composite(
                                                                              *variablesFull)));
                }
                chain.emplace_back(new EditingEngineFullCalculate(averageInterval->clone(),
                                                                  averageCover->clone(),
                                                                  variablesFull));
                variablesFull = nullptr;
            }
        }

        /* Add any edits that go before the filter */
        endInstance = std::upper_bound(beginInstance, rawDirectives.constEnd(), fc.first);
        if (beginInstance != endInstance) {
            if (instantiateDirectives(beginInstance, endInstance,
                                      disableAveraging ? nullptr : &variablesContinuous,
                                      disableAveraging ? nullptr : &variablesFull)) {
                ++totalEditCores;
            }
            beginInstance = endInstance;

            /* Re-average again if needed */
            if (variablesContinuous) {
                chain.emplace_back(new EditingEngineContinuousRecalculate(averageInterval->clone(),
                                                                          averageCover->clone(),
                                                                          variablesContinuous));
                variablesContinuous = nullptr;
            }
            if (variablesFull) {
                if (disableContamination) {
                    chain.emplace_back(
                            new EditingContaminationFilter(new DynamicSequenceSelection::None,
                                                           new SequenceMatch::Composite(
                                                                   *variablesFull)));
                } else {
                    chain.emplace_back(new ArchiveContaminationFilter(*access, contaminationProfile,
                                                                      new SequenceMatch::Composite(
                                                                              *variablesFull)));
                }
                chain.emplace_back(new EditingEngineFullCalculate(averageInterval->clone(),
                                                                  averageCover->clone(),
                                                                  variablesFull));
                variablesFull = nullptr;
            }
        }

        Q_ASSERT(!fc.second.empty());

        QString componentName(fc.second.back()["Component"].toQString());
        Q_ASSERT(!componentName.isEmpty());

        QObject *component = ComponentLoader::create(componentName);
        if (!component) {
            qCWarning(log_editing_engine) << "Can't load component" << componentName;
            continue;
        }
        ProcessingStageComponent *filterComponent;
        if (!(filterComponent = qobject_cast<ProcessingStageComponent *>(component))) {
            qCWarning(log_editing_engine) << "Component" << componentName
                                          << "is not a general filter";
            continue;
        }

        ProcessingStage *filter =
                filterComponent->createGeneralFilterEditing(inputStart, inputEnd, station, archive,
                                                            ValueSegment::withPath(fc.second,
                                                                                   "Parameters"));
        if (!filter) {
            qCWarning(log_editing_engine) << "Can't create general filter for" << componentName;
            continue;
        }

        chain.emplace_back(filter);
        nProcessingComponents++;

        /* Set reaveraging */
        if (!disableAveraging) {
            SequenceMatch::Composite addContinuous;
            SequenceMatch::Composite addFull;
            for (const auto &seg: fc.second) {
                auto params = seg.getValue()["Average"];

                if (params["Continuous"].exists())
                    addContinuous.append(
                            SequenceMatch::Composite(params["Continuous"], {}, {}, {}, {}, {}, {}));
                if (params["Full"].exists())
                    addFull.append(
                            SequenceMatch::Composite(params["Full"], {}, {}, {}, {}, {}, {}));
            }

            if (addContinuous.valid())
                variablesContinuous = new SequenceMatch::Composite(addContinuous);
            if (addFull.valid())
                variablesFull = new SequenceMatch::Composite(addFull);
        }
    }

    /* Add final directives */
    if (beginInstance != rawDirectives.constEnd()) {
        if (!disableAveraging) {
            /* Re-average */
            if (variablesContinuous) {
                chain.emplace_back(new EditingEngineContinuousRecalculate(averageInterval->clone(),
                                                                          averageCover->clone(),
                                                                          variablesContinuous));
                variablesContinuous = nullptr;
            }
            if (variablesFull) {
                if (disableContamination) {
                    chain.emplace_back(
                            new EditingContaminationFilter(new DynamicSequenceSelection::None,
                                                           new SequenceMatch::Composite(
                                                                   *variablesFull)));
                } else {
                    chain.emplace_back(new ArchiveContaminationFilter(*access, contaminationProfile,
                                                                      new SequenceMatch::Composite(
                                                                              *variablesFull)));
                }
                chain.emplace_back(new EditingEngineFullCalculate(averageInterval->clone(),
                                                                  averageCover->clone(),
                                                                  variablesFull));
                variablesFull = nullptr;
            }
        }

        if (instantiateDirectives(beginInstance, rawDirectives.constEnd(),
                                  disableAveraging ? nullptr : &variablesContinuous,
                                  disableAveraging ? nullptr : &variablesFull)) {
            ++totalEditCores;
        }
    }

    if (!disableAveraging) {
        /* Add the final averaging/stats */
        if (disableContamination) {
            chain.emplace_back(new EditingContaminationFinal(new DynamicSequenceSelection::None));
        } else {
            chain.emplace_back(new ArchiveContaminationFinal(*access, contaminationProfile));
        }
        chain.emplace_back(new EditingEngineFinal(averageInterval->clone(), averageCover->clone(),
                                                  variablesContinuous, variablesFull, outputArchive,
                                                  outputAverages));
    }

    if (chain.empty()) {
        chain.emplace_back(ProcessingStage::createNOOP());
    }

    /* Set input selections */
    {
        SequenceName::Set requestInputs;
        for (const auto &filter: chain) {
            Util::merge(filter->requestedInputs(), requestInputs);
        }
        for (const auto &add: requestInputs) {
            inputSelections.emplace_back(add, startTime, endTime);
        }
    }

    for (auto &mod : inputSelections) {
        mod.start = inputStart;
        mod.end = inputEnd;
        mod.includeMetaArchive = true;
        mod.includeDefaultStation = true;
#ifndef NDEBUG
        if (mod.variables.empty() ||
                std::find(mod.variables.begin(), mod.variables.end(), std::string()) !=
                        mod.variables.end()) {
            qCDebug(log_editing_engine) << "Global variable read detected:" << mod;
        }
#endif
        for (auto lf = mod.lacksFlavors.begin(); lf != mod.lacksFlavors.end();) {
            if (*lf == "cover" || *lf == "end") {
                lf = mod.lacksFlavors.erase(lf);
                continue;
            }

            ++lf;
        }
    }

    for (auto &mod : outputSelections) {
        mod.selection.registerExpected(station, outputArchive + "_meta");
        mod.selection.registerExpected(station, outputAverages + "_meta");
    }

    delete averageInterval;
    delete averageCover;

    outputFilter.selection = std::move(outputSelections);

    Memory::release_unused();

    qCDebug(log_editing_engine) << "Starting editing with" << inputSelections.size()
                                << "input selection(s) " << outputFilter.selection.size()
                                << "output selection(s), and" << chain.size()
                                << "stage(s) composed of" << nProcessingComponents
                                << "filter(s) and" << rawDirectives.size() << "edit directive(s) in"
                                << totalEditCores << "section(s)";
}

SequenceName::Set EditingEngine::predictedOutputs()
{
    if (chain.empty())
        createChain();

    SequenceName::Set result;
    for (const auto &sel : outputFilter.selection) {
        Util::merge(sel.selection.knownInputs(), result);
    }
    return result;
}

void EditingEngine::start()
{
    context = std::make_shared<Context>();

    if (chain.empty())
        createChain();

    Q_ASSERT(access);
    Q_ASSERT(!chain.empty());

    StreamSource *prior;
    StreamSink *readerTarget = nullptr;
    if (!inputSelections.empty()) {
        activeRead = access->readStream(inputSelections);
        activeRead->complete
                  .connect(context->ref.get(),
                           std::bind(&EditingEngine::checkAllFinished, this, context), true);
        prior = activeRead.get();
    } else {
        prior = nullptr;
    }
    for (const auto &filter : chain) {
        filter->finished
              .connect(context->ref.get(),
                       std::bind(&EditingEngine::checkAllFinished, this, context), true);
        filter->start();
        if (activeRead && prior == activeRead.get())
            readerTarget = filter.get();
        else if (prior)
            prior->setEgress(filter.get());
        else
            filter->endData();
        prior = filter.get();
    }

    /* Do this last because the reader is likely spooling up the query and
     * we can instantiate everything else instead of blocking on the
     * first setEgress call. */
    if (readerTarget)
        activeRead->setEgress(readerTarget);

    if (initialEgress) {
        outputFilter.egress = initialEgress;
        prior->setEgress(&outputFilter);
        initialEgress = nullptr;
    }

    // FIXME: Appears like Qt signals sometimes get lost (maybe thread moving?)
    auto failsafe = new QTimer(context->ref.get());
    failsafe->setSingleShot(false);
    failsafe->setInterval(500);
    QObject::connect(failsafe, &QTimer::timeout, context->ref.get(),
                     std::bind(&EditingEngine::checkAllFinished, this, context),
                     Qt::QueuedConnection);
    failsafe->start();

    checkAllFinished(context);
}

void EditingEngine::purgeArchive(const Threading::Signal<double> &progress)
{
    /* A local archive means we only have a read lock, so this can't be valid */
    Q_ASSERT(!localAccess);
    Q_ASSERT(access);

    Archive::Selection::List readSelections;
    for (const auto &sel : outputFilter.selection) {
        auto vs = sel.selection.toArchiveSelections({station}, {outputArchive, outputAverages});
        for (auto &add : vs) {
            add.start = sel.getStart();
            add.end = sel.getEnd();
            add.includeDefaultStation = false;
            add.includeMetaArchive = true;
            readSelections.emplace_back(std::move(add));
        }
    }
    if (readSelections.empty())
        return;

    for (;;) {
        Archive::Access::WriteLock lock(*access, false);
        StreamSink::Iterator read;
        access->readStream(readSelections, &read)->detach();

        class MatchSelection : public Internal::EditingOutputSelection {
            SequenceName::Map<bool> hit;
        public:
            MatchSelection(const Internal::EditingOutputSelection &base)
                    : Internal::EditingOutputSelection(base)
            { }

            bool test(const SequenceName &name)
            {
                auto check = hit.find(name);
                if (check != hit.end())
                    return check->second;

                bool result = selection.matches(name);
                hit.emplace(name, result);
                return result;
            }
        };

        std::deque<MatchSelection>
                selections(outputFilter.selection.begin(), outputFilter.selection.end());

        /* This is "safe" because if we're seeing a value, the stream has already finished
         * reading the block it belongs to (for no forward split ones), so removal
         * is still ok.  Addition is still ok, because any ones we add, we would modify
         * the same way even if we saw them again, so if the addition results in a duplication
         * we're still ok. */
        SequenceValue::Transfer add;
        SequenceIdentity::Transfer remove;
        for (;;) {
            SequenceValue::Transfer incoming;
            if (read.all(incoming) && incoming.empty())
                break;
            for (auto &v : incoming) {
                while (!selections.empty() &&
                        Range::compareStartEnd(v.getStart(), selections.front().getEnd()) >= 0) {
                    selections.pop_front();
                }

                auto name = v.getName();
                name.clearMeta();
                if (name.getArchive() != outputArchive && name.getArchive() != outputAverages)
                    continue;

                if (add.size() + remove.size() > StreamSink::stallThreshold) {
                    access->writeSynchronous(add, remove);
                    add.clear();
                    remove.clear();
                    progress(v.getStart());
                }

                bool haveRemoved = false;
                for (auto &sel : selections) {
                    if (Range::compareStartEnd(sel.getStart(), v.getEnd()) >= 0)
                        break;
                    if (!sel.test(name))
                        continue;

                    /* If it matches, then the original is being removed, either completely or
                     * because it's fragmented */
                    if (!haveRemoved) {
                        remove.emplace_back(v);
                        haveRemoved = true;
                    }

                    /* If it starts before the selection, insert a fill for it */
                    if (Range::compareStart(v.getStart(), sel.getStart()) < 0) {
                        SequenceValue fill = v;
                        fill.setEnd(sel.getStart());
                        Q_ASSERT(Range::compareStartEnd(fill.getStart(), fill.getEnd()) < 0);
                        add.emplace_back(std::move(fill));
                    }

                    /* Now move the start to the end of the selection, so the next one will
                     * start where it should */
                    v.setStart(sel.getEnd());

                    /* If it's become zero length, we're done: the value's end is before
                     * the selection's end, so all further ones are removed */
                    if (Range::compareStartEnd(v.getStart(), v.getEnd()) >= 0)
                        break;
                }

                /* Insert the tail, if required */
                if (haveRemoved && Range::compareStartEnd(v.getStart(), v.getEnd()) < 0) {
                    add.emplace_back(std::move(v));
                }
            }
        }

        if (!add.empty() || !remove.empty()) {
            access->writeSynchronous(add, remove);
        }

        if (lock.commit())
            break;
    }
}


EditingEngine::OutputFilter::OutputFilter(EditingEngine *e) : engine(e),
                                                              dispatch(),
                                                              egress(nullptr),
                                                              selection()
{ }

EditingEngine::OutputFilter::~OutputFilter() = default;

EditingEngine::OutputFilter::DispatchBase *EditingEngine::OutputFilter::getDispatch(const SequenceValue &value)
{
    while (!selection.empty() &&
            Range::compareStartEnd(value.getStart(), selection.front().getEnd()) >= 0) {
        selection.erase(selection.begin());
        dispatch.clear();
    }

    auto target = dispatch.find(value.getName());
    if (target != dispatch.end())
        return target->second.get();

    DispatchBase *result;
    if (selection.empty() || !selection.front().selection.matches(value.getName())) {
        result = new DispatchDiscard;
    } else if (value.getName().isMeta()) {
        result = new DispatchMetaPass(this);
    } else {
        result = new DispatchPass(this);
    }

    dispatch.emplace(value.getName(), std::unique_ptr<DispatchBase>(result));
    return result;
}

void EditingEngine::OutputFilter::incomingData(const SequenceValue::Transfer &values)
{
    Q_ASSERT(egress);
    if (values.empty())
        return;

    SequenceValue::Transfer result;
    for (const auto &add : values) {
        SequenceValue v = add;
        if (!getDispatch(v)->handle(v))
            continue;
        result.emplace_back(std::move(v));
    }
    egress->incomingData(std::move(result));
}

void EditingEngine::OutputFilter::incomingData(SequenceValue::Transfer &&values)
{
    Q_ASSERT(egress);
    if (values.empty())
        return;

    SequenceValue::Transfer result;
    for (auto &add : values) {
        SequenceValue v = std::move(add);
        if (!getDispatch(v)->handle(v))
            continue;
        result.emplace_back(std::move(v));
    }
    egress->incomingData(std::move(result));
}

void EditingEngine::OutputFilter::incomingData(const SequenceValue &value)
{
    Q_ASSERT(egress);
    SequenceValue v = value;
    if (!getDispatch(v)->handle(v))
        return;
    egress->incomingData(std::move(v));
}

void EditingEngine::OutputFilter::incomingData(SequenceValue &&value)
{
    Q_ASSERT(egress);
    if (!getDispatch(value)->handle(value))
        return;
    egress->incomingData(std::move(value));
}

void EditingEngine::OutputFilter::endData()
{
    Q_ASSERT(egress != NULL);
    egress->endData();
}

EditingEngine::OutputFilter::DispatchBase::DispatchBase() = default;

EditingEngine::OutputFilter::DispatchBase::~DispatchBase() = default;

EditingEngine::OutputFilter::DispatchDiscard::DispatchDiscard() = default;

EditingEngine::OutputFilter::DispatchDiscard::~DispatchDiscard() = default;

bool EditingEngine::OutputFilter::DispatchDiscard::handle(SequenceValue &)
{ return false; }

EditingEngine::OutputFilter::DispatchPass::DispatchPass(OutputFilter *f) : filter(f),
                                                                           start(f->getStart()),
                                                                           end(f->getEnd())
{ }

EditingEngine::OutputFilter::DispatchPass::~DispatchPass() = default;

bool EditingEngine::OutputFilter::DispatchPass::handle(SequenceValue &incoming)
{
    if (Range::compareStart(incoming.getStart(), start) < 0) {
        if (Range::compareStartEnd(start, incoming.getEnd()) >= 0)
            return false;
        incoming.setStart(start);
        if (Range::compareEnd(incoming.getEnd(), end) > 0)
            incoming.setEnd(end);
    } else if (Range::compareEnd(incoming.getEnd(), end) > 0) {
        if (Range::compareStartEnd(incoming.getStart(), end) >= 0)
            return false;
        incoming.setEnd(end);
    }
    return true;
}


EditingEngine::OutputFilter::DispatchMetaPass::DispatchMetaPass(OutputFilter *f) : filter(f),
                                                                                   start(f->getStart()),
                                                                                   end(f->getEnd())
{ }

EditingEngine::OutputFilter::DispatchMetaPass::~DispatchMetaPass() = default;

bool EditingEngine::OutputFilter::DispatchMetaPass::handle(SequenceValue &incoming)
{
    if (Range::compareStart(incoming.getStart(), start) < 0) {
        if (Range::compareStartEnd(start, incoming.getEnd()) >= 0)
            return false;
        incoming.setStart(start);
        if (Range::compareEnd(incoming.getEnd(), end) > 0)
            incoming.setEnd(end);
    } else if (Range::compareEnd(incoming.getEnd(), end) > 0) {
        if (Range::compareStartEnd(incoming.getStart(), end) >= 0)
            return false;
        incoming.setEnd(end);
    }

    if (incoming.read().isMetadata() && incoming.read().metadata("Processing").exists()) {

        Variant::Write processing = incoming.write().metadata("Processing").toArray().after_back();
        processing.hash("By").setString("editing_engine");
        processing.hash("At").setDouble(Time::time());
        processing.hash("Environment").setString(Environment::describe());
        processing.hash("Revision").setString(Environment::revision());
    }

    return true;
}

EditingEngine::Context::Context() : ref(new QObject), valid(true), held(0)
{ }

}
}
