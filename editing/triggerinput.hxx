/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGTRIGGERINPUT_H
#define CPD3EDITINGTRIGGERINPUT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QList>
#include <QVector>
#include <QDebug>

#include "editing/editing.hxx"

#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/segment.hxx"
#include "core/number.hxx"
#include "editing/editdatatarget.hxx"

namespace CPD3 {
namespace Editing {

/**
 * The base class for edit triggering.  The base implementation always
 * return true (always applies).
 */
class CPD3EDITING_EXPORT TriggerInput {
public:
    TriggerInput();

    virtual ~TriggerInput();

    /**
     * Duplicate the trigger.  This does NOT duplicate any state it may have,
     * just the input conditions.
     * 
     * @return          a new input
     */
    virtual TriggerInput *clone() const = 0;

    /**
     * Extend the data processing range as required.  This is used to allow
     * triggers to ensure they have a full buffer for processing.
     * 
     * @param start     the input start time and output extended time
     * @param end       the input end time and output extended time
     */
    virtual void extendProcessing(double &start, double &end) const;

    /**
     * Register an new input.  This is called when a new unit is seen and
     * should return a list of targets for that unit.  Ownership is retained
     * by the callee.
     * 
     * @param unit      the unit to test
     * @return          the list of targets for the unit
     */
    virtual QList<EditDataTarget *> unhandledInput(const Data::SequenceName &unit);

    /**
     * Called when to indicate that the input data stream (all targets) has
     * advanced to a certain time.  No data will be generated before this time.
     * <br>
     * This will be called after every set of incoming data before any
     * of the evaluation functions (e.x. isReady(double) are called).
     * 
     * @param time      the time to advance until
     */
    virtual void incomingDataAdvance(double time);

    /**
     * Get how far the input has advanced until.  This can be used to determine
     * a bound that a parent input wrapping it can advance until.
     * 
     * @return          the maximum advance time or undefined
     */
    virtual double advancedTime() const;

    /**
     * Test if the input is ready to process data ending at the given time.
     * That is, test if there are no more updates possible before the given
     * end time.
     * 
     * @param end       the time time to check
     * @return          true if the trigger is ready
     */
    virtual bool isReady(double end) const = 0;

    /**
     * The result type of the input.  List of results are produced in
     * response to incoming data.  Once a result has been produced, no
     * further updates will occur before the start time.
     */
    struct Result {
        double start;
        double end;
        double value;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline double getValue() const
        { return value; }

        inline void setStart(double s)
        { start = s; }

        inline void setEnd(double e)
        { end = e; }

        /**
         * Create a new result.
         * 
         * @param s     the start time
         * @param e     the end time
         * @param v     the value
         */
        inline Result(double s, double e, double v) : start(s), end(e), value(v)
        { }

        inline Result() : start(FP::undefined()), end(FP::undefined()), value(FP::undefined())
        { }

        inline bool operator==(const Result &other) const
        {
            return FP::equal(start, other.start) &&
                    FP::equal(end, other.end) &&
                    FP::equal(value, other.value);
        }

        inline bool operator!=(const Result &other) const
        {
            return !FP::equal(start, other.start) ||
                    !FP::equal(end, other.end) ||
                    !FP::equal(value, other.value);
        }
    };

    /**
     * Process and generate all pending results.
     * 
     * @return          a list of results
     */
    virtual QVector<Result> process() = 0;

    /**
     * Called at the end of processing to allow the input to generate
     * any final data.
     */
    virtual void finalize();

    /**
     * Returns any additional inputs requested by the trigger.  These may
     * not necessarily be available in the final input.
     * 
     * @return  the requested inputs
     */
    virtual Data::SequenceName::Set requestedInputs() const;

protected:
    TriggerInput(const TriggerInput &other);
};

CPD3EDITING_EXPORT QDebug operator<<(QDebug stream, const TriggerInput::Result &v);

/**
 * A constant value for a trigger input.
 */
class CPD3EDITING_EXPORT TriggerInputConstant : public TriggerInput {
    double value;
    bool emitted;
public:
    /**
     * Create a constant input.
     * 
     * @param value     the constant value
     */
    TriggerInputConstant(double value);

    virtual ~TriggerInputConstant();

    virtual TriggerInput *clone() const;

    virtual bool isReady(double end) const;

    virtual QVector<Result> process();

protected:
    TriggerInputConstant(const TriggerInputConstant &other);
};

/**
 * A value read from the input stream.
 */
class CPD3EDITING_EXPORT TriggerInputValue : public TriggerInput, public EditDataTarget {
    Data::SequenceMatch::OrderedLookup selection;
    std::string path;
    Data::ValueSegment::Stream reader;
    double latestTime;
    bool completed;

    QVector<Result> result;

    void convertSegments(const Data::ValueSegment::Transfer &segments);

public:
    /**
     * Create a value input.
     * 
     * @param input     the input value selection
     * @param path      the path to read from within the values
     */
    TriggerInputValue(const Data::SequenceMatch::OrderedLookup &input,
                      const std::string &path = std::string());

    virtual ~TriggerInputValue();

    virtual TriggerInput *clone() const;

    virtual double advancedTime() const;

    virtual bool isReady(double end) const;

    virtual QList<EditDataTarget *> unhandledInput(const Data::SequenceName &unit);

    virtual QVector<Result> process();

    virtual void incomingSequenceValue(const Data::SequenceValue &value);

    virtual void incomingDataAdvance(double time);

    virtual void finalize();

    virtual Data::SequenceName::Set requestedInputs() const;

protected:
    TriggerInputValue(const TriggerInputValue &other);
};

/**
 * A base class for simple single input functions of another input.
 */
class CPD3EDITING_EXPORT TriggerInputFunction : public TriggerInput {
    TriggerInput *input;
public:
    /**
     * Create a function input.  This takes ownership of the wrapped
     * input.
     * 
     * @param input     the input to the function
     */
    TriggerInputFunction(TriggerInput *input);

    virtual ~TriggerInputFunction();

    virtual void extendProcessing(double &start, double &end) const;

    virtual double advancedTime() const;

    virtual bool isReady(double end) const;

    virtual QList<EditDataTarget *> unhandledInput(const Data::SequenceName &unit);

    virtual void incomingDataAdvance(double time);

    virtual QVector<Result> process();

    virtual void finalize();

    virtual Data::SequenceName::Set requestedInputs() const;

protected:
    TriggerInputFunction(const TriggerInputFunction &other);

    /**
     * Apply the function to a value.
     * 
     * @param value     the input value
     * @return          the function applied to the value
     */
    virtual double apply(double value) = 0;
};

/**
 * Apply sin(x) to an input.
 */
class CPD3EDITING_EXPORT TriggerInputSin : public TriggerInputFunction {
public:
    TriggerInputSin(TriggerInput *input);

    virtual ~TriggerInputSin();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputSin(const TriggerInputSin &other);

    virtual double apply(double value);
};

/**
 * Apply cos(x) to an input.
 */
class CPD3EDITING_EXPORT TriggerInputCos : public TriggerInputFunction {
public:
    TriggerInputCos(TriggerInput *input);

    virtual ~TriggerInputCos();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputCos(const TriggerInputCos &other);

    virtual double apply(double value);
};

/**
 * Apply ln(x) to an input.
 */
class CPD3EDITING_EXPORT TriggerInputLog : public TriggerInputFunction {
public:
    TriggerInputLog(TriggerInput *input);

    virtual ~TriggerInputLog();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputLog(const TriggerInputLog &other);

    virtual double apply(double value);
};

/**
 * Apply log10(x) to an input.
 */
class CPD3EDITING_EXPORT TriggerInputLog10 : public TriggerInputFunction {
public:
    TriggerInputLog10(TriggerInput *input);

    virtual ~TriggerInputLog10();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputLog10(const TriggerInputLog10 &other);

    virtual double apply(double value);
};

/**
 * Apply e^(x) to an input.
 */
class CPD3EDITING_EXPORT TriggerInputExp : public TriggerInputFunction {
public:
    TriggerInputExp(TriggerInput *input);

    virtual ~TriggerInputExp();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputExp(const TriggerInputExp &other);

    virtual double apply(double value);
};

/**
 * Apply |x| to an input.
 */
class CPD3EDITING_EXPORT TriggerInputAbs : public TriggerInputFunction {
public:
    TriggerInputAbs(TriggerInput *input);

    virtual ~TriggerInputAbs();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputAbs(const TriggerInputAbs &other);

    virtual double apply(double value);
};

/**
 * Apply a calibration polynomial to an input.
 */
class CPD3EDITING_EXPORT TriggerInputPoly : public TriggerInputFunction {
    Calibration poly;
public:
    /**
     * Create a calibration polynomial input. 
     * 
     * @param input     the input
     * @param poly      the polynomial to apply
     */
    TriggerInputPoly(TriggerInput *input, const Calibration &poly);

    virtual ~TriggerInputPoly();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputPoly(const TriggerInputPoly &other);

    virtual double apply(double value);
};

/**
 * Invert a calibration polynomial to an input.
 */
class CPD3EDITING_EXPORT TriggerInputPolyInvert : public TriggerInputFunction {
    Calibration poly;
    double min;
    double max;
public:
    /**
     * Create a calibration polynomial inversion input. 
     * 
     * @param input     the input
     * @param poly      the polynomial to invert
     * @param min       the minimum range of the output
     * @param max       the maximum range of the output
     */
    TriggerInputPolyInvert(TriggerInput *input,
                           const Calibration &poly,
                           double min = FP::undefined(),
                           double max = FP::undefined());

    virtual ~TriggerInputPolyInvert();

    virtual TriggerInput *clone() const;

protected:
    TriggerInputPolyInvert(const TriggerInputPolyInvert &other);

    virtual double apply(double value);
};

}
}

Q_DECLARE_TYPEINFO(CPD3::Editing::TriggerInput::Result, Q_MOVABLE_TYPE);

#endif
