/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3EDITINGEBAS_H
#define CPD3EDITINGEBAS_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QHash>

#include "editing/editing.hxx"

#include "datacore/variant/root.hxx"

namespace CPD3 {
namespace Editing {

/**
 * An interface defining constants related to the EBAS database.
 */
class CPD3EDITING_EXPORT EBAS {
public:

    /**
     * Get the description for a given EBAS flag.
     * 
     * @param flag  the flag code
     * @return      the description information
     */
    static Data::Variant::Root description(int flag);

    /**
     * Get the description for a given EBAS flag.
     * 
     * @param flag  the flag code
     * @return      the description information
     */
    static Data::Variant::Root description(const QString &flag);

    /**
     * The types of EBAS flags.
     */
    enum FlagType {
        Flag_Valid, Flag_Invalid, Flag_Missing, Flag_Hidden,
    };

    /**
     * Get all the valid flags.
     * 
     * @return      all valid flags
     */
    static QHash<int, QString> getAllDescriptions(FlagType type);
};

}
}

#endif
