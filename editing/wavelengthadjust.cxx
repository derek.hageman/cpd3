/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <cmath>

#include "editing/wavelengthadjust.hxx"

using namespace CPD3::Data;
using namespace CPD3::Smoothing;

namespace CPD3 {
namespace Editing {

/** @file editing/wavelengthadjust.hxx
 * Handling for wavelength adjustment.  This exists in the editing system
 * because it depends on smoothing and various other complex features.
 * <br>
 * Wavelength interpolation is done in log-log space when possible (all inputs
 * positive) using the formula:
 * <code>output = e^(ln(lowerInput) + (ln(upperInput) - ln(lowerInput)) * (ln(targetWavelength / lowerWavelength) / ln(upperWavelength / lowerWavelength)))</code>
 * Or when any input is negative or zero in linear space:
 * <code>output =  lowerInput + (upperInput - lowerInput) * (targetWavelength - lowerWavelength) / (upperWavelength - lowerWavelength))</code>
 * When extrapolating using only an angstrom exponent from a single wavelength
 * the formula used is:
 * <code>output = input * ((inputWavelength/targetWavelength) ^ angstromExponent)</code>
 * Angstrom exponent calculation is done using the formula:
 * <code>output = ln(lowerInput/upperInput) / ln(upperWavelength/lowerWavelength)</code>
 */


WavelengthAdjust::WavelengthAdjust()
        : inputLookup(),
          inputFixed(),
          inputDynamic(),
          dynamicOperate(nullptr),
          dynamicSmoother(nullptr),
          dynamicValidDistance(nullptr),
          angstromLookup(),
          inputAngstrom()
{ }

void WavelengthAdjust::configure(const ValueSegment::Transfer &config,
                                 double start, double end, const Variant::Read &defaultConfig)
{
    dynamicOperate.reset(DynamicSequenceSelection::fromConfiguration(config, "Input", start, end));

    auto defaultSmoothing = defaultConfig["Smoothing"];
    if (!defaultSmoothing.exists()) {
        Variant::Root temp;
        temp["Type"].setString("Latest");
        defaultSmoothing = temp.read();
    }
    auto angstromSmoothing = defaultConfig["Angstrom/Smoothing"];
    if (!angstromSmoothing.exists()) {
        Variant::Root temp;
        temp["Type"].setString("Latest");
        angstromSmoothing = temp.read();
    }

    dynamicSmoother.reset(
            BaselineSmoother::fromConfiguration(defaultSmoothing, config, "Smoothing", start, end));

    dynamicValidDistance.reset(
            DynamicDoubleOption::fromConfiguration(config, "ValidDistance", start, end));

    std::unordered_set<std::string> children;
    for (const auto &c : config) {
        Util::merge(c["Fixed"].toHash().keys(), children);
    }
    for (const auto &child : children) {
        std::unique_ptr<InputFixed> input(new InputFixed);
        input->input
             .reset(DynamicInput::fromConfiguration(config, QString("Fixed/%1/Input").arg(
                     QString::fromStdString(child)), start, end));
        input->smoothers.resize(1);
        input->smoothers
             .back()
             .reset(BaselineSmoother::fromConfiguration(defaultSmoothing, config,
                                                        QString("Fixed/%1/Smoothing").arg(
                                                                QString::fromStdString(child)),
                                                        start, end));
        input->fixedWavelengthInput
             .reset(DynamicDoubleOption::fromConfiguration(config,
                                                           QString("Fixed/%1/Wavelength").arg(
                                                                   QString::fromStdString(child)),
                                                           start, end));
        input->validDistanceInput
             .reset(DynamicDoubleOption::fromConfiguration(config,
                                                           QString("Fixed/%1/ValidDistance").arg(
                                                                   QString::fromStdString(child)),
                                                           start, end));
        input->wavelength = input->fixedWavelengthInput->get(FP::undefined(), FP::undefined());
        input->validDistance = input->validDistanceInput->get(FP::undefined(), FP::undefined());
        inputFixed.emplace_back(std::move(input));
    }

    children.clear();
    for (const auto &c : config) {
        Util::merge(c["Angstrom"].toHash().keys(), children);
    }
    for (const auto &child : children) {
        std::unique_ptr<AngstromInput> input(new AngstromInput);
        input->input
             .reset(DynamicInput::fromConfiguration(config, QString("Angstrom/%1/Input").arg(
                     QString::fromStdString(child)), start, end));
        input->smoother
             .reset(BaselineSmoother::fromConfiguration(angstromSmoothing, config,
                                                        QString("Angstrom/%1/Smoothing").arg(
                                                                QString::fromStdString(child)),
                                                        start, end));
        input->fixedWavelengthInput
             .reset(DynamicDoubleOption::fromConfiguration(config,
                                                           QString("Angstrom/%1/Wavelength").arg(
                                                                   QString::fromStdString(child)),
                                                           start, end));
        input->validDistanceInput
             .reset(DynamicDoubleOption::fromConfiguration(config,
                                                           QString("Angstrom/%1/ValidDistance").arg(
                                                                   QString::fromStdString(child)),
                                                           start, end));
        input->wavelength = input->fixedWavelengthInput->get(FP::undefined(), FP::undefined());
        input->validDistance = input->validDistanceInput->get(FP::undefined(), FP::undefined());
        input->isFallback = false;
        inputAngstrom.emplace_back(std::move(input));
    }

    children.clear();
    for (const auto &c : config) {
        Util::merge(c["AngstromFallback"].toHash().keys(), children);
    }
    for (const auto &child : children) {
        std::unique_ptr<AngstromInput> input(new AngstromInput);
        input->input
             .reset(DynamicInput::fromConfiguration(config,
                                                    QString("AngstromFallback/%1/Input").arg(
                                                            QString::fromStdString(child)), start,
                                                    end));
        input->smoother
             .reset(BaselineSmoother::fromConfiguration(angstromSmoothing, config,
                                                        QString("AngstromFallback/%1/Smoothing").arg(
                                                                QString::fromStdString(child)),
                                                        start, end));
        input->fixedWavelengthInput
             .reset(DynamicDoubleOption::fromConfiguration(config,
                                                           QString("AngstromFallback/%1/Wavelength")
                                                                   .arg(QString::fromStdString(
                                                                           child)), start, end));
        input->validDistanceInput
             .reset(DynamicDoubleOption::fromConfiguration(config,
                                                           QString("AngstromFallback/%1/ValidDistance")
                                                                   .arg(QString::fromStdString(
                                                                           child)), start, end));
        input->wavelength = input->fixedWavelengthInput->get(FP::undefined(), FP::undefined());
        input->validDistance = input->validDistanceInput->get(FP::undefined(), FP::undefined());
        input->isFallback = true;
        inputAngstrom.emplace_back(std::move(input));
    }

    rebuildLookups();
}

WavelengthAdjust *WavelengthAdjust::fromConfiguration(SequenceSegment::Transfer &config,
                                                      const SequenceName &unit,
                                                      const QString &path,
                                                      double start,
                                                      double end,
                                                      const Variant::Read &defaultConfig)
{
    ValueSegment::Transfer local;
    for (auto &s : config) {
        local.emplace_back(s.getStart(), s.getEnd(), Variant::Root(s[unit][path]));
    }
    WavelengthAdjust *result = new WavelengthAdjust;
    Q_ASSERT(result);
    result->configure(local, start, end, defaultConfig);
    return result;
}

WavelengthAdjust *WavelengthAdjust::fromConfiguration(const ValueSegment::Transfer &config,
                                                      const QString &path,
                                                      double start,
                                                      double end,
                                                      const Variant::Read &defaultConfig)
{
    WavelengthAdjust *result = new WavelengthAdjust;
    Q_ASSERT(result);
    if (!path.isEmpty()) {
        result->configure(ValueSegment::withPath(config, path), start, end, defaultConfig);
    } else {
        result->configure(config, start, end, defaultConfig);
    }
    return result;
}

WavelengthAdjust *WavelengthAdjust::fromInput(DynamicSequenceSelection *input,
                                              BaselineSmoother *smoother)
{
    WavelengthAdjust *result = new WavelengthAdjust;
    Q_ASSERT(result);
    result->dynamicOperate.reset(input);
    if (!smoother)
        smoother = new BaselineLatest;
    result->dynamicSmoother.reset(smoother);
    result->dynamicValidDistance.reset(new DynamicPrimitive<double>::Constant(FP::undefined()));

    return result;
}

void WavelengthAdjust::addFallbackAngstrom(DynamicInput *angstromFallback,
                                           DynamicDouble *wavelength,
                                           DynamicDouble *validDistance,
                                           BaselineSmoother *smoother)
{
    Q_ASSERT(angstromFallback);

    std::unique_ptr<AngstromInput> input(new AngstromInput);
    input->input.reset(angstromFallback);
    if (!smoother) {
        input->smoother.reset(new BaselineLatest);
    } else {
        input->smoother.reset(smoother);
    }
    if (wavelength) {
        input->fixedWavelengthInput.reset(wavelength);
        if (validDistance) {
            input->validDistanceInput.reset(validDistance);
        } else {
            input->validDistanceInput
                 .reset(new DynamicPrimitive<double>::Constant(FP::undefined()));
        }
    } else {
        Q_ASSERT(!validDistance);
        input->fixedWavelengthInput.reset(new DynamicPrimitive<double>::Constant(1));
        input->validDistanceInput.reset(new DynamicPrimitive<double>::Constant(FP::undefined()));
    }
    input->wavelength = input->fixedWavelengthInput->get(FP::undefined(), FP::undefined());
    input->validDistance = input->validDistanceInput->get(FP::undefined(), FP::undefined());
    input->isFallback = true;
    inputAngstrom.emplace_back(std::move(input));

    rebuildLookups();
}


WavelengthAdjust::~WavelengthAdjust() = default;

void WavelengthAdjust::rebuildLookups()
{
    inputLookup.clear();
    for (const auto &input : inputFixed) {
        if (!FP::defined(input->wavelength) || input->wavelength <= 0.0) {
            input->wavelength = FP::undefined();
            continue;
        }
        inputLookup.emplace(input->wavelength, static_cast<Input *>(input.get()));
    }
    for (const auto &input : inputDynamic) {
        if (!FP::defined(input.second->wavelength) || input.second->wavelength <= 0.0) {
            input.second->wavelength = FP::undefined();
            continue;
        }
        inputLookup.emplace(input.second->wavelength, static_cast<Input *>(input.second.get()));
    }

    angstromLookup.clear();
    for (const auto &input : inputAngstrom) {
        if (!FP::defined(input->wavelength) || input->wavelength <= 0.0) {
            input->wavelength = FP::undefined();
            continue;
        }
        angstromLookup.emplace(input->wavelength, static_cast<AngstromInput *>(input.get()));
    }
}

WavelengthAdjust::WavelengthAdjust(const WavelengthAdjust &other)
        : inputLookup(),
          inputFixed(),
          inputDynamic(),
          dynamicOperate(other.dynamicOperate->clone()),
          dynamicSmoother(other.dynamicSmoother->clone()),
          dynamicValidDistance(other.dynamicValidDistance->clone()),
          angstromLookup(),
          inputAngstrom()
{
    for (const auto &input : other.inputFixed) {
        inputFixed.emplace_back(new InputFixed(*input));
    }
    for (const auto &add : other.inputDynamic) {
        inputDynamic.emplace(add.first,
                             std::unique_ptr<InputDynamic>(new InputDynamic(*add.second)));
    }
    for (const auto &input : other.inputAngstrom) {
        inputAngstrom.emplace_back(new AngstromInput(*input));
    }
    rebuildLookups();
}

WavelengthAdjust &WavelengthAdjust::operator=(const WavelengthAdjust &other)
{
    if (&other == this)
        return *this;
    dynamicOperate.reset(other.dynamicOperate->clone());
    dynamicSmoother.reset(other.dynamicSmoother->clone());
    dynamicValidDistance.reset(other.dynamicValidDistance->clone());
    inputFixed.clear();
    for (const auto &input : other.inputFixed) {
        inputFixed.emplace_back(new InputFixed(*input));
    }
    inputDynamic.clear();
    for (const auto &add : other.inputDynamic) {
        inputDynamic.emplace(add.first,
                             std::unique_ptr<InputDynamic>(new InputDynamic(*add.second)));
    }
    inputAngstrom.clear();
    for (const auto &input : other.inputAngstrom) {
        inputAngstrom.emplace_back(new AngstromInput(*input));
    }
    rebuildLookups();
    return *this;
}

namespace {
QDataStream &operator<<(QDataStream &stream, const Variant::PathElement::MatrixIndex &index)
{
    Serialize::container(stream, index, [&](std::size_t add) {
        stream << static_cast<quint32>(add);
    });
    return stream;
}

QDataStream &operator>>(QDataStream &stream, Variant::PathElement::MatrixIndex &index)
{
    Deserialize::container(stream, index, [&]() {
        quint32 add = 0;
        stream >> add;
        return static_cast<std::size_t>(add);
    });
    return stream;
}
}

QDataStream &operator<<(QDataStream &stream, const WavelengthAdjust *adj)
{
    if (!adj) {
        stream << static_cast<quint8>(0);
        return stream;
    }
    stream << static_cast<quint8>(1) << adj->dynamicOperate << adj->dynamicSmoother
           << adj->dynamicValidDistance;

    Serialize::container(stream, adj->inputFixed,
                         [&](const std::unique_ptr<WavelengthAdjust::InputFixed> &input) {
                             stream << input->wavelength << input->isSimple << input->matrixShape
                                    << input->input << input->usedUnits
                                    << input->fixedWavelengthInput << input->validDistance
                                    << input->validDistanceInput << input->smoothers;
                         });

    stream << static_cast<quint32>(adj->inputDynamic.size());
    for (const auto &input : adj->inputDynamic) {
        stream << input.second->wavelength << input.second->isSimple << input.second->matrixShape
               << input.second->unit << input.second->smoothers;
    }

    Serialize::container(stream, adj->inputAngstrom,
                         [&](const std::unique_ptr<WavelengthAdjust::AngstromInput> &input) {
                             stream << input->wavelength << input->smoother << input->input
                                    << input->usedUnits << input->validDistance
                                    << input->fixedWavelengthInput << input->validDistanceInput
                                    << input->isFallback;
                         });

    return stream;
}

QDataStream &operator>>(QDataStream &stream, WavelengthAdjust *&adj)
{
    quint8 type;
    stream >> type;
    if (type == 0) {
        adj = nullptr;
        return stream;
    }
    adj = new WavelengthAdjust;

    stream >> adj->dynamicOperate >> adj->dynamicSmoother >> adj->dynamicValidDistance;

    Q_ASSERT(adj->inputFixed.empty());
    Deserialize::container(stream, adj->inputFixed, [&]() {
        std::unique_ptr<WavelengthAdjust::InputFixed> input(new WavelengthAdjust::InputFixed);
        Q_ASSERT(input->smoothers.empty());

        stream >> input->wavelength >> input->isSimple >> input->matrixShape >> input->input
               >> input->usedUnits >> input->fixedWavelengthInput >> input->validDistance
               >> input->validDistanceInput >> input->smoothers;

        return std::move(input);
    });

    quint32 n = 0;
    stream >> n;
    Q_ASSERT(adj->inputDynamic.empty());
    for (quint32 i = 0; i < n; i++) {
        std::unique_ptr<WavelengthAdjust::InputDynamic>
                input(new WavelengthAdjust::InputDynamic(SequenceName()));
        Q_ASSERT(input->smoothers.empty());
        stream >> input->wavelength >> input->isSimple >> input->matrixShape >> input->unit
               >> input->smoothers;

        adj->inputDynamic.emplace(input->unit, std::move(input));
    }

    Q_ASSERT(adj->inputAngstrom.empty());
    Deserialize::container(stream, adj->inputAngstrom, [&]() {
        std::unique_ptr<WavelengthAdjust::AngstromInput> input(new WavelengthAdjust::AngstromInput);
        stream >> input->wavelength >> input->smoother >> input->input >> input->usedUnits
               >> input->validDistance >> input->fixedWavelengthInput >> input->validDistanceInput
               >> input->isFallback;
        return std::move(input);
    });

    adj->rebuildLookups();
    return stream;
}


WavelengthAdjust::Input::Input() : wavelength(FP::undefined()),
                                   smoothers(),
                                   isSimple(true),
                                   matrixShape(),
                                   validDistance(FP::undefined()),
                                   validDistanceInput(nullptr)
{ }

WavelengthAdjust::Input::Input(const Input &other) : wavelength(other.wavelength),
                                                     smoothers(),
                                                     isSimple(other.isSimple),
                                                     matrixShape(other.matrixShape),
                                                     validDistance(other.validDistance),
                                                     validDistanceInput(
                                                             other.validDistanceInput->clone())
{
    smoothers.reserve(other.smoothers.size());
    for (const auto &add : other.smoothers) {
        smoothers.emplace_back(add->clone());
    }
}

WavelengthAdjust::Input::~Input() = default;

bool WavelengthAdjust::Input::isValid(double forWavelength, double adjustDistance) const
{
    Q_ASSERT(FP::defined(wavelength));

    if (FP::defined(forWavelength)) {
        if (FP::defined(adjustDistance)) {
            if (std::fabs(wavelength - forWavelength) > adjustDistance)
                return false;
        }
        if (FP::defined(validDistance)) {
            if (std::fabs(wavelength - forWavelength) > validDistance)
                return false;
        }
    }

    for (const auto &check : smoothers) {
        double v = check->value();
        if (!FP::defined(v))
            continue;
        return true;
    }
    return false;
}

WavelengthAdjust::InputDynamic::InputDynamic(const SequenceName &u) : Input(), unit(u)
{ }

WavelengthAdjust::InputDynamic::InputDynamic(const InputDynamic &other) = default;

WavelengthAdjust::InputDynamic::~InputDynamic() = default;

void WavelengthAdjust::InputDynamic::reshapeArray(std::size_t sz, BaselineSmoother *baseSmoother)
{
    isSimple = false;
    if (sz == smoothers.size())
        return;
    matrixShape.clear();
    if (sz < 1)
        sz = 1;
    for (std::size_t i = smoothers.size(); i < sz; i++) {
        smoothers.emplace_back(baseSmoother->clone());
    }
    smoothers.resize(sz);
}

void WavelengthAdjust::InputDynamic::reshapeMatrix(const Variant::PathElement::MatrixIndex &sz,
                                                   BaselineSmoother *baseSmoother)
{
    isSimple = false;
    if (matrixShape == sz)
        return;
    matrixShape = sz;
    std::size_t n = 1;
    for (auto add : sz) {
        if (add <= 0)
            continue;
        n *= add;
    }
    if (n < 1)
        n = 1;
    for (std::size_t i = smoothers.size(); i < n; i++) {
        smoothers.emplace_back(baseSmoother->clone());
    }
    smoothers.resize(n);
}

void WavelengthAdjust::InputDynamic::reshapeSimple()
{
    isSimple = true;
    if (smoothers.size() == 1)
        return;
    matrixShape.clear();
    smoothers.resize(1);
}

WavelengthAdjust::InputFixed::InputFixed()
        : Input(), input(nullptr), usedUnits(), fixedWavelengthInput(nullptr)
{ }

WavelengthAdjust::InputFixed::InputFixed(const InputFixed &other) : Input(other),
                                                                    input(other.input->clone()),
                                                                    usedUnits(other.usedUnits),
                                                                    fixedWavelengthInput(
                                                                            other.fixedWavelengthInput
                                                                                 ->clone())
{ }

WavelengthAdjust::InputFixed::~InputFixed() = default;

WavelengthAdjust::AngstromInput::AngstromInput() : wavelength(FP::undefined()),
                                                   smoother(nullptr),
                                                   input(nullptr),
                                                   usedUnits(),
                                                   validDistance(FP::undefined()),
                                                   fixedWavelengthInput(nullptr),
                                                   validDistanceInput(nullptr),
                                                   isFallback(false)
{ }

WavelengthAdjust::AngstromInput::AngstromInput(const AngstromInput &other) : wavelength(
        other.wavelength),
                                                                             smoother(other.smoother
                                                                                           ->clone()),
                                                                             input(other.input
                                                                                        ->clone()),
                                                                             usedUnits(
                                                                                     other.usedUnits),
                                                                             validDistance(
                                                                                     other.validDistance),
                                                                             fixedWavelengthInput(
                                                                                     other.fixedWavelengthInput
                                                                                          ->clone()),
                                                                             validDistanceInput(
                                                                                     other.validDistanceInput
                                                                                          ->clone()),
                                                                             isFallback(
                                                                                     other.isFallback)
{ }

WavelengthAdjust::AngstromInput::~AngstromInput() = default;

bool WavelengthAdjust::AngstromInput::isValid(double forWavelength,
                                              double angstromDistance,
                                              AngstromSearchMode mode) const
{
    Q_ASSERT(FP::defined(wavelength));

    switch (mode) {
    case AngstromSearch_General:
        if (isFallback)
            return false;
        break;
    case AngstromSearch_Fallback:
        break;
    }

    if (FP::defined(angstromDistance)) {
        Q_ASSERT(FP::defined(forWavelength));
        if (std::fabs(wavelength - forWavelength) > angstromDistance)
            return false;
    }
    if (FP::defined(validDistance)) {
        Q_ASSERT(FP::defined(forWavelength));
        if (std::fabs(wavelength - forWavelength) > validDistance)
            return false;
    }

    double v = smoother->value();
    if (!FP::defined(v) || v <= 0.0)
        return false;
    return true;
}


WavelengthAdjust::InputDynamic *WavelengthAdjust::lookupDynamic(const SequenceName &unit)
{
    auto check = inputDynamic.find(unit);
    if (check != inputDynamic.end())
        return check->second.get();

    std::unique_ptr<InputDynamic> add(new InputDynamic(unit));
    add->smoothers.resize(1);
    add->smoothers.back().reset(dynamicSmoother->clone());
    add->validDistanceInput.reset(dynamicValidDistance->clone());
    add->validDistance = add->validDistanceInput->get(FP::undefined(), FP::undefined());
    return inputDynamic.emplace(unit, std::move(add)).first->second.get();
}

void WavelengthAdjust::incoming(SequenceSegment &data)
{
    for (const auto &input : inputFixed) {
        input->smoothers[0]->add(input->input->get(data), data.getStart(), data.getEnd());
    }
    for (const auto &input : inputAngstrom) {
        input->smoother->add(input->input->get(data), data.getStart(), data.getEnd());
    }

    for (const auto &n : dynamicOperate->get(data)) {
        InputDynamic *input = lookupDynamic(n);
        auto value = data.getValue(n);
        switch (value.getType()) {
        case Variant::Type::Array:
            input->reshapeArray(value.toArray().size(), dynamicSmoother.get());
            for (std::size_t i = 0, max = value.toArray().size(); i < max; i++) {
                if (i >= input->smoothers.size())
                    break;
                input->smoothers[i]->add(value.array(i).toDouble(), data.getStart(), data.getEnd());
            }
            break;
        case Variant::Type::Matrix:
            input->reshapeMatrix(value.toMatrix().shape(), dynamicSmoother.get());
            for (std::size_t i = 0, max = value.toMatrix().size(); i < max; i++) {
                if ((size_t) i >= input->smoothers.size())
                    break;
                input->smoothers[i]->add(value.array(i).toDouble(), data.getStart(), data.getEnd());
            }
            break;
        default:
            input->reshapeSimple();
            input->smoothers[0]->add(value.toDouble(), data.getStart(), data.getEnd());
            break;
        }
    }
}

template<typename Map, typename Key, typename Value>
static void removeInstance(Map &map, const Key &key, const Value &value)
{
    auto check = map.lower_bound(key);
    auto end = map.end();
    if (check == end)
        return;
    for (; check != end && check->first == key; ++check) {
        if (check->second == value) {
            map.erase(check);
            return;
        }
    }
}

void WavelengthAdjust::incomingMeta(SequenceSegment &data)
{
    for (const auto &input : inputFixed) {
        input->validDistance = input->validDistanceInput->get(data);

        double newWavelength = input->fixedWavelengthInput->get(data);
        if (!FP::defined(newWavelength) || newWavelength <= 0.0) {
            for (const auto &check : input->usedUnits) {
                newWavelength = data[check.toMeta()].metadata("Wavelength").toDouble();
                if (FP::defined(newWavelength))
                    break;
            }
        }

        if (!FP::defined(newWavelength) || newWavelength <= 0.0) {
            if (FP::defined(input->wavelength)) {
                removeInstance(inputLookup, input->wavelength, static_cast<Input *>(input.get()));
            }
            input->wavelength = FP::undefined();
        } else if (!FP::defined(input->wavelength)) {
            inputLookup.emplace(newWavelength, static_cast<Input *>(input.get()));
            input->wavelength = newWavelength;
        } else if (newWavelength != input->wavelength) {
            removeInstance(inputLookup, input->wavelength, static_cast<Input *>(input.get()));
            inputLookup.emplace(newWavelength, static_cast<Input *>(input.get()));
            input->wavelength = newWavelength;
        }
    }

    for (const auto &input : inputAngstrom) {
        input->validDistance = input->validDistanceInput->get(data);

        double newWavelength = input->fixedWavelengthInput->get(data);
        if (!FP::defined(newWavelength) || newWavelength <= 0.0) {
            for (const auto &check : input->usedUnits) {
                newWavelength = data[check.toMeta()].metadata("Wavelength").toDouble();
                if (FP::defined(newWavelength))
                    break;
            }
        }

        if (!FP::defined(newWavelength) || newWavelength <= 0.0) {
            if (FP::defined(input->wavelength)) {
                removeInstance(angstromLookup, input->wavelength,
                               static_cast<AngstromInput *>(input.get()));
            }
            input->wavelength = FP::undefined();
        } else if (!FP::defined(input->wavelength)) {
            angstromLookup.emplace(newWavelength, static_cast<AngstromInput *>(input.get()));
            input->wavelength = newWavelength;
        } else if (newWavelength != input->wavelength) {
            removeInstance(angstromLookup, input->wavelength,
                           static_cast<AngstromInput *>(input.get()));
            angstromLookup.emplace(newWavelength, static_cast<AngstromInput *>(input.get()));
            input->wavelength = newWavelength;
        }
    }

    for (auto n : dynamicOperate->get(data)) {
        InputDynamic *input = lookupDynamic(n);
        n.setMeta();

        input->validDistance = input->validDistanceInput->get(data);

        if (!data.exists(n)) {
            if (FP::defined(input->wavelength)) {
                removeInstance(inputLookup, input->wavelength, static_cast<Input *>(input));
            }
            input->wavelength = FP::undefined();
            /* Keep the existing shape */
            continue;
        }

        auto d = data[n];
        double newWavelength = d.metadata("Wavelength").toDouble();
        if (!FP::defined(newWavelength) || newWavelength <= 0.0) {
            newWavelength = d.metadata("Children").metadata("Wavelength").toDouble();
        }

        if (!FP::defined(newWavelength) || newWavelength <= 0.0) {
            if (FP::defined(input->wavelength)) {
                removeInstance(inputLookup, input->wavelength, static_cast<Input *>(input));
            }
            input->wavelength = FP::undefined();
        } else if (!FP::defined(input->wavelength)) {
            inputLookup.emplace(newWavelength, static_cast<Input *>(input));
            input->wavelength = newWavelength;
        } else if (newWavelength != input->wavelength) {
            removeInstance(inputLookup, input->wavelength, static_cast<Input *>(input));
            inputLookup.emplace(newWavelength, static_cast<Input *>(input));
            input->wavelength = newWavelength;
        }

        switch (d.getType()) {
        case Variant::Type::Array: {
            qint64 sz = d.metadata("Count").toInt64();
            if (INTEGER::defined(sz)) {
                input->reshapeArray(static_cast<std::size_t>(sz), dynamicSmoother.get());
            } else {
                input->isSimple = false;
            }
            break;
        }
        case Variant::Type::Matrix: {
            input->isSimple = false;
            if (d.metadataMatrix("Size").toArray().size() > 0) {
                Variant::PathElement::MatrixIndex shape;
                for (auto add : d.metadataMatrix("Size").toArray()) {
                    qint64 test = add.toInt64();
                    if (!INTEGER::defined(test) || test <= 0)
                        continue;
                    shape.emplace_back(test);
                }
                input->reshapeMatrix(shape, dynamicSmoother.get());
            } else {
                input->isSimple = false;
                if (input->matrixShape.empty())
                    input->matrixShape.emplace_back(1);
            }
            break;
        }
        default:
            input->reshapeSimple();
            break;
        }
    }
}

bool WavelengthAdjust::registerInput(const SequenceName &unit, bool *adjust)
{
    bool result = dynamicOperate->registerInput(unit);
    for (const auto &input : inputFixed) {
        if (input->input->registerInput(unit)) {
            input->usedUnits.insert(unit);
            result = true;
        }
    }
    if (adjust)
        *adjust = result;
    for (const auto &input : inputAngstrom) {
        if (input->input->registerInput(unit)) {
            input->usedUnits.insert(unit);
            result = true;
        }
    }
    return result;
}

void WavelengthAdjust::registerExpectedFlavorless(const SequenceName::Component &station,
                                                  const SequenceName::Component &archive,
                                                  const SequenceName::Component &variable)
{
    dynamicOperate->registerExpectedFlavorless(station, archive, variable);
    for (const auto &input : inputFixed) {
        input->input->registerExpectedFlavorless(station, archive, variable);
        Util::merge(input->input->getUsedInputs(), input->usedUnits);
    }
    for (const auto &input : inputAngstrom) {
        input->input->registerExpectedFlavorless(station, archive, variable);
        Util::merge(input->input->getUsedInputs(), input->usedUnits);
    }
}

void WavelengthAdjust::registerExpected(const SequenceName::Component &station,
                                        const SequenceName::Component &archive,
                                        const SequenceName::Component &variable,
                                        const SequenceName::Flavors &flavors)
{
    dynamicOperate->registerExpected(station, archive, variable, flavors);
    for (const auto &input : inputFixed) {
        input->input->registerExpected(station, archive, variable, flavors);
        Util::merge(input->input->getUsedInputs(), input->usedUnits);
    }
    for (const auto &input : inputAngstrom) {
        input->input->registerExpected(station, archive, variable, flavors);
        Util::merge(input->input->getUsedInputs(), input->usedUnits);
    }
}

void WavelengthAdjust::registerExpected(const SequenceName::Component &station,
                                        const SequenceName::Component &archive,
                                        const SequenceName::Component &variable,
                                        const std::unordered_set<SequenceName::Flavors> &flavors)
{
    dynamicOperate->registerExpected(station, archive, variable, flavors);
    for (const auto &input : inputFixed) {
        input->input->registerExpected(station, archive, variable, flavors);
        Util::merge(input->input->getUsedInputs(), input->usedUnits);
    }
    for (const auto &input : inputAngstrom) {
        input->input->registerExpected(station, archive, variable, flavors);
        Util::merge(input->input->getUsedInputs(), input->usedUnits);
    }
}

SequenceName::Set WavelengthAdjust::getAllUnits() const
{
    SequenceName::Set result;
    Util::merge(dynamicOperate->getAllUnits(), result);
    for (const auto &input : inputFixed) {
        Util::merge(input->input->getUsedInputs(), result);
    }
    for (const auto &input : inputAngstrom) {
        Util::merge(input->input->getUsedInputs(), result);
    }
    return result;
}

QSet<double> WavelengthAdjust::getChangedPoints() const
{
    QSet<double> result;
    Util::merge(dynamicOperate->getChangedPoints(), result);
    for (const auto &input : inputFixed) {
        Util::merge(input->input->getChangedPoints(), result);
        Util::merge(input->fixedWavelengthInput->getChangedPoints(), result);
        Util::merge(input->validDistanceInput->getChangedPoints(), result);
    }
    for (const auto &input : inputAngstrom) {
        Util::merge(input->input->getChangedPoints(), result);
        Util::merge(input->fixedWavelengthInput->getChangedPoints(), result);
        Util::merge(input->validDistanceInput->getChangedPoints(), result);
    }
    return result;
}

double WavelengthAdjust::spinupTime(double initial) const
{
    if (!FP::defined(initial))
        return initial;
    double result = initial;
    double check = dynamicSmoother->spinupTime(initial);
    if (FP::defined(check) && check < result)
        result = check;
    for (const auto &input : inputFixed) {
        for (const auto &smoother : input->smoothers) {
            check = smoother->spinupTime(initial);
            if (FP::defined(check) && check < result)
                result = check;
        }
    }
    for (const auto &input : inputAngstrom) {
        check = input->smoother->spinupTime(initial);
        if (FP::defined(check) && check < result)
            result = check;
    }
    return result;
}

WavelengthAdjust::ConvertAngstrom::ConvertAngstrom(double wl, double ad, AngstromSearchMode m)
        : wavelength(wl), angstromDistance(ad), mode(m)
{ }

bool WavelengthAdjust::ConvertAngstrom::isValid(const AngstromInput *input) const
{ return input->isValid(wavelength, angstromDistance, mode); }

bool WavelengthAdjust::ConvertAngstrom::outsidePossible(const AngstromInput *input,
                                                        double target) const
{
    Q_UNUSED(input);
    if (!FP::defined(angstromDistance))
        return false;
    return fabs(target - wavelength) > angstromDistance;
}

WavelengthAdjust::AngstromInput *WavelengthAdjust::ConvertAngstrom::convert(AngstromInput *input,
                                                                            double target) const
{
    Q_UNUSED(target);
    return input;
}

WavelengthAdjust::AngstromInput *WavelengthAdjust::angstromForWavelength(double wavelength,
                                                                         double angstromDistance,
                                                                         AngstromSearchMode mode) const
{
    Q_ASSERT(FP::defined(wavelength));
    return Wavelength::nearest(wavelength, angstromLookup,
                               ConvertAngstrom(wavelength, angstromDistance, mode));
}

static double angstromSingle(double wl, double wu, double vl, double vu)
{
    if (!FP::defined(vl) || vl <= 0.0)
        return FP::undefined();
    if (!FP::defined(vu) || vu <= 0.0)
        return FP::undefined();
    return log(vl / vu) / log(wu / wl);
}

bool WavelengthAdjust::CalculateAngstrom::isValid(const Input *input) const
{
    if (!input->isValid(FP::undefined(), FP::undefined()))
        return false;
    for (const auto &check : input->smoothers) {
        double v = check->value();
        if (!FP::defined(v))
            continue;
        if (v <= 0.0)
            continue;
        return true;
    }
    return false;
}

Variant::Read WavelengthAdjust::CalculateAngstrom::convert(const Input *lower,
                                                           const Input *upper,
                                                           double target) const
{
    Q_UNUSED(target);
    double wl = lower->wavelength;
    double wu = upper->wavelength;
    Q_ASSERT(FP::defined(wl) && wl > 0.0);
    Q_ASSERT(FP::defined(wu) && wu > 0.0);
    Q_ASSERT(wl != wu);

    if (lower->isSimple && upper->isSimple) {
        return Variant::Root(angstromSingle(wl, wu, lower->smoothers[0]->value(),
                                            upper->smoothers[0]->value())).read();
    }

    Variant::Write result = Variant::Write::empty();
    if (!lower->matrixShape.empty() && lower->matrixShape == upper->matrixShape) {
        result.setType(Variant::Type::Matrix);
        result.toMatrix().reshape(lower->matrixShape);
    } else {
        result.setType(Variant::Type::Array);
    }
    for (std::size_t i = 0, max = std::min(lower->smoothers.size(), upper->smoothers.size());
            i < max;
            i++) {
        double vl = lower->smoothers[i]->value();
        if (!FP::defined(vl) || vl <= 0.0)
            continue;
        double vu = upper->smoothers[i]->value();
        if (!FP::defined(vu) || vu <= 0.0)
            continue;
        result.array(i).setDouble(log(vl / vu) / log(wu / wl));
    }
    return result;
}

Variant::Read WavelengthAdjust::getAngstrom(double wavelength,
                                            Wavelength::BracketingExactBehavior bracketing,
                                            double angstromDistance,
                                            bool excludeFallback) const
{
    if (!FP::defined(wavelength) || wavelength <= 0.0)
        return Variant::Read::empty();

    AngstromInput
            *direct = angstromForWavelength(wavelength, angstromDistance, AngstromSearch_General);
    if (direct)
        return Variant::Root(direct->smoother->value()).read();

    if (bracketing == Wavelength::BracketingExactBehavior::ExactTwice)
        bracketing = Wavelength::BracketingExactBehavior::ExcludeExact;

    static const CalculateAngstrom convert = CalculateAngstrom();
    Variant::Read result(Wavelength::bracketing(wavelength, inputLookup, convert, bracketing));
    if (result.exists() &&
            (result.getType() != Variant::Type::Real || FP::defined(result.toReal())))
        return result;

    if (!excludeFallback) {
        direct = angstromForWavelength(wavelength, angstromDistance, AngstromSearch_Fallback);
        if (direct)
            return Variant::Root(direct->smoother->value()).read();
    }

    return Variant::Read::empty();
}


double WavelengthAdjust::getWavelength(const SequenceName &unit) const
{
    {
        auto check = inputDynamic.find(unit);
        if (check != inputDynamic.end()) {
            return check->second->wavelength;
        }
    }
    {
        for (const auto &check : inputFixed) {
            if (check->usedUnits.count(unit) != 0)
                return check->wavelength;
        }
    }
    {
        for (const auto &check : inputAngstrom) {
            if (check->usedUnits.count(unit) != 0)
                return check->wavelength;
        }
    }

    return FP::undefined();
}

Variant::Read WavelengthAdjust::getAngstrom(const SequenceName &unit,
                                            Wavelength::BracketingExactBehavior bracketing,
                                            double angstromDistance,
                                            bool excludeFallback) const
{
    return getAngstrom(getWavelength(unit), bracketing, angstromDistance, excludeFallback);
}

WavelengthAdjust::ReturnNearest::ReturnNearest(double wl, double ad) : wavelength(wl),
                                                                       adjustDistance(ad)
{ }

bool WavelengthAdjust::ReturnNearest::isValid(const WavelengthAdjust::Input *input) const
{ return input->isValid(wavelength, adjustDistance); }

WavelengthAdjust::Input *WavelengthAdjust::inputNearestWavelength(double wavelength,
                                                                  double adjustDistance) const
{
    return Wavelength::nearest(wavelength, inputLookup, ReturnNearest(wavelength, adjustDistance));
}

Variant::Root WavelengthAdjust::adjustFromSingle(double target,
                                                 double origin,
                                                 const Input *input,
                                                 double angstrom)
{
    Q_ASSERT(FP::defined(target));
    Q_ASSERT(FP::defined(origin));
    Q_ASSERT(input->isValid(FP::undefined(), FP::undefined()));

    if (input->isSimple) {
        double value = input->smoothers[0]->value();
        if (!FP::defined(value) || !FP::defined(angstrom))
            return Variant::Root(FP::undefined());
        if (target <= 0 || origin <= 0)
            return Variant::Root(FP::undefined());
        return Variant::Root(value * std::pow((origin / target), angstrom));
    }

    Variant::Root result;
    if (!input->matrixShape.empty()) {
        result.write().setType(Variant::Type::Matrix);
        result.write().toMatrix().reshape(input->matrixShape);
    } else {
        result.write().setType(Variant::Type::Array);
    }
    for (size_t i = 0, max = input->smoothers.size(); i < max; i++) {
        double value = input->smoothers[i]->value();
        if (!FP::defined(value) || !FP::defined(angstrom))
            continue;
        if (target <= 0 || origin <= 0)
            continue;
        result.write().array(i).setDouble(value * pow((origin / target), angstrom));
    }
    return result;
}

static double interpolateSingle(double target, double wl, double wu, double vl, double vu)
{
    Q_ASSERT(FP::defined(wl) && wl > 0.0);
    Q_ASSERT(FP::defined(wu) && wu > 0.0);
    Q_ASSERT(wl != wu);

    if (!FP::defined(vu) || !FP::defined(vu))
        return FP::undefined();

    if (vl > 0.0 && vu > 0.0) {
        double f = log(target / wl) / log(wu / wl);
        double ll = log(vl);
        double lu = log(vu);
        return exp(ll + (lu - ll) * f);
    }

    double f = (target - wl) / (wu - wl);
    return vl + (vu - vl) * f;
}

WavelengthAdjust::Interpolate::Interpolate(double wl, double ad) : wavelength(wl),
                                                                   adjustDistance(ad)
{ }

Variant::Read WavelengthAdjust::Interpolate::convert(const Input *lower,
                                                     const Input *upper,
                                                     double target) const
{
    if (lower == upper) {
        if (lower->isSimple) {
            return Variant::Root(lower->smoothers[0]->value()).read();
        }
        Variant::Write result = Variant::Write::empty();
        if (!lower->matrixShape.empty() && lower->matrixShape == upper->matrixShape) {
            result.setType(Variant::Type::Matrix);
            result.toMatrix().reshape(lower->matrixShape);
        } else {
            result.setType(Variant::Type::Array);
        }
        for (std::size_t i = 0, max = lower->smoothers.size(); i < max; i++) {
            result.array(i).setDouble(lower->smoothers[i]->value());
        }
        return result;
    }

    if (!lower->isValid(wavelength, adjustDistance) &&
            !upper->isValid(wavelength, adjustDistance)) {
        if (lower->isSimple && upper->isSimple) {
            return Variant::Root(FP::undefined());
        }
        Variant::Write result = Variant::Write::empty();
        if (!lower->matrixShape.empty() && lower->matrixShape == upper->matrixShape) {
            result.setType(Variant::Type::Matrix);
            result.toMatrix().reshape(lower->matrixShape);
        } else {
            result.setType(Variant::Type::Array);
        }
        for (std::size_t i = 0, max = std::min(lower->smoothers.size(), upper->smoothers.size());
                i < max;
                i++) {
            result.array(i).setDouble(FP::undefined());
        }
        return result;
    }

    double wl = lower->wavelength;
    double wu = upper->wavelength;

    if (lower->isSimple && upper->isSimple) {
        return Variant::Root(interpolateSingle(target, wl, wu, lower->smoothers[0]->value(),
                                               upper->smoothers[0]->value())).read();
    }

    Variant::Write result = Variant::Write::empty();
    if (!lower->matrixShape.empty() && lower->matrixShape == upper->matrixShape) {
        result.setType(Variant::Type::Matrix);
        result.toMatrix().reshape(lower->matrixShape);
    } else {
        result.setType(Variant::Type::Array);
    }
    for (std::size_t i = 0, max = std::min(lower->smoothers.size(), upper->smoothers.size());
            i < max;
            i++) {
        result.array(i)
              .setDouble(interpolateSingle(target, wl, wu, lower->smoothers[i]->value(),
                                           upper->smoothers[i]->value()));
    }
    return result;
}

bool WavelengthAdjust::Interpolate::isValid(const WavelengthAdjust::Input *input) const
{ return input->isValid(FP::undefined(), FP::undefined()); }

Variant::Read WavelengthAdjust::getAdjusted(double wavelength,
                                            double angstromDistance,
                                            double adjustDistance) const
{
    if (!FP::defined(wavelength) || wavelength <= 0.0)
        return Variant::Root(FP::undefined()).read();

    AngstromInput
            *direct = angstromForWavelength(wavelength, angstromDistance, AngstromSearch_General);
    if (direct) {
        Input *origin = inputNearestWavelength(wavelength, adjustDistance);
        if (origin) {
            return adjustFromSingle(wavelength, origin->wavelength, origin,
                                    direct->smoother->value());
        }
    }

    Variant::Read result =
            Wavelength::bracketing(wavelength, inputLookup, Interpolate(wavelength, adjustDistance),
                                   Wavelength::BracketingExactBehavior::ExactTwice);
    if (result.exists() &&
            (result.getType() != Variant::Type::Real || FP::defined(result.toReal())))
        return result;

    direct = angstromForWavelength(wavelength, angstromDistance, AngstromSearch_Fallback);
    if (direct) {
        Input *origin = inputNearestWavelength(wavelength, adjustDistance);
        if (origin) {
            return adjustFromSingle(wavelength, origin->wavelength, origin,
                                    direct->smoother->value());
        }
    }

    return Variant::Read::empty();
}


WavelengthTracker::WavelengthTracker() : wavelengths()
{ }

void WavelengthTracker::incomingMeta(SequenceSegment &data, const SequenceName::Set &operate)
{
    for (const auto &base : operate) {
        auto meta = base.toMeta();
        if (!data.exists(meta)) {
            wavelengths.erase(base);
            continue;
        }
        Util::insert_or_assign(wavelengths, base,
                               data[std::move(meta)].metadata("Wavelength").toDouble());
    }
}

void WavelengthTracker::incomingMeta(const Variant::Read &data, const SequenceName &value)
{
    if (!data.exists()) {
        wavelengths.erase(value);
        return;
    }
    Util::insert_or_assign(wavelengths, value, data.metadata("Wavelength").toDouble());
}

double WavelengthTracker::get(const SequenceName &unit) const
{
    auto check = wavelengths.find(unit);
    if (check == wavelengths.end())
        return FP::undefined();
    return check->second;
}

QDataStream &operator<<(QDataStream &stream, const WavelengthTracker &track)
{
    stream << track.wavelengths;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, WavelengthTracker &track)
{
    stream >> track.wavelengths;
    return stream;
}

}
}
