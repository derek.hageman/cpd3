/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/directivecontainer.hxx"
#include "core/environment.hxx"
#include "core/util.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/variant/composite.hxx"

#include "editing/ebas.hxx"
#include "editing/actionflags.hxx"
#include "editing/actionflowcorrection.hxx"
#include "editing/actionfunction.hxx"
#include "editing/actionscriptgeneral.hxx"
#include "editing/actionscriptprocessor.hxx"
#include "editing/actionunit.hxx"
#include "editing/actionpoly.hxx"
#include "editing/triggeraverage.hxx"
#include "editing/triggercondition.hxx"
#include "editing/triggerperiodic.hxx"
#include "editing/triggerscript.hxx"
#include "editing/triggervaluegeneric.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Editing {

/** @file editing/directivecontainer.hxx
 * The general container for edit directives.
 */


DirectiveContainer::DirectiveContainer() : original(), value()
{ }

DirectiveContainer::DirectiveContainer(const DirectiveContainer &) = default;

DirectiveContainer &DirectiveContainer::operator=(const DirectiveContainer &) = default;

DirectiveContainer::DirectiveContainer(DirectiveContainer &&) = default;

DirectiveContainer &DirectiveContainer::operator=(DirectiveContainer &&) = default;

DirectiveContainer::DirectiveContainer(const SequenceValue &archive) : original(archive),
                                                                       value(archive)
{ }

DirectiveContainer::DirectiveContainer(const SequenceName &target, const DirectiveContainer &other)
        : original(other.value), value(other.value)
{
    original.setUnit(SequenceName());
    value.setUnit(target);
    value.write().hash("History").remove();
}

bool DirectiveContainer::isActive() const
{
    return !value.read().hash("Disabled").toBool();
}

static SequenceMatch::OrderedLookup toSelection(const Variant::Read &config,
                                                const SequenceName::Component &station,
                                                const SequenceName::Component &archive)
{
    /* We need to exclude the metadata archive by default, since edits rarely 
     * want that explicitly. */
    SequenceMatch::OrderedLookup sel(config, {}, SequenceMatch::Element::PatternList(
            QString::fromStdString(Archive::Selection::excludeMetaArchiveMatcher())), {});
    if (!sel.valid()) {
        sel = SequenceMatch::OrderedLookup(SequenceMatch::Element({}, QString::fromStdString(
                Archive::Selection::excludeMetaArchiveMatcher()), {}));
    }
    sel.registerExpected(station, archive);
    return sel;
}

static Variant::Flags toFlags(const Variant::Read &config)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        const auto &flag = config.toString();
        if (flag.empty())
            return Variant::Flags();
        return Variant::Flags{flag};
    }
    default:
        break;
    }
    return config.toFlags();
}

static SequenceName::Flavors toFlavors(const Variant::Read &config)
{ return SequenceName::toFlavors(config); }

static bool defaultTrue(const Variant::Read &config, bool def = true)
{
    if (config.getType() != Variant::Type::Boolean)
        return def;
    return config.toBool();
}

static TriggerInput *createInput(const Variant::Read &config,
                                 const SequenceName::Component &station,
                                 const SequenceName::Component &archive);

static QList<TriggerInput *> createMultiInput(const Variant::Read &config,
                                              const SequenceName::Component &station,
                                              const SequenceName::Component &archive)
{
    QList<TriggerInput *> components;
    for (auto add : config.toChildren()) {
        components.append(createInput(add, station, archive));
    }
    return components;
}

static TriggerInput *createInput(const Variant::Read &config,
                                 const SequenceName::Component &station,
                                 const SequenceName::Component &archive)
{
    switch (config.getType()) {
    case Variant::Type::Integer: {
        qint64 i = config.toInt64();
        if (!INTEGER::defined(i))
            return new TriggerInputConstant(FP::undefined());
        return new TriggerInputConstant((double) i);
    }
    case Variant::Type::Real:
        return new TriggerInputConstant(config.toDouble());
    case Variant::Type::String:
        return new TriggerInputValue(toSelection(config, station, archive));
    case Variant::Type::Array: {
        QList<TriggerInput *> components;
        for (auto add : config.toArray()) {
            components.append(createInput(add, station, archive));
        }
        return new TriggerInputFirstValid(components);
    }
    default:
        break;
    }

    const auto &type = config.hash("Type").toString();
    if (type.empty()) {
        return new TriggerInputValue(toSelection(config.hash("Value"), station, archive));
    }

    if (Util::equal_insensitive(type, "Constant")) {
        return new TriggerInputConstant(config.hash("Value").toDouble());
    } else if (Util::equal_insensitive(type, "sin")) {
        return new TriggerInputSin(createInput(config.hash("Input"), station, archive));
    } else if (Util::equal_insensitive(type, "cos")) {
        return new TriggerInputCos(createInput(config.hash("Input"), station, archive));
    } else if (Util::equal_insensitive(type, "log", "ln")) {
        return new TriggerInputLog(createInput(config.hash("Input"), station, archive));
    } else if (Util::equal_insensitive(type, "log10")) {
        return new TriggerInputLog10(createInput(config.hash("Input"), station, archive));
    } else if (Util::equal_insensitive(type, "exp")) {
        return new TriggerInputExp(createInput(config.hash("Input"), station, archive));
    } else if (Util::equal_insensitive(type, "abs", "Absolute", "AbsoluteValue")) {
        return new TriggerInputAbs(createInput(config.hash("Input"), station, archive));
    } else if (Util::equal_insensitive(type, "poly", "Polynomial", "Cal", "Calibration")) {
        return new TriggerInputPoly(createInput(config.hash("Input"), station, archive),
                                    Variant::Composite::toCalibration(config.hash("Calibration")));
    } else if (Util::equal_insensitive(type, "PolyInvert", "PolynomialInvert", "InvertCal",
                                       "InvertCalibration")) {
        return new TriggerInputPolyInvert(createInput(config.hash("Input"), station, archive),
                                          Variant::Composite::toCalibration(
                                                  config.hash("Calibration")),
                                          config.hash("Minimum").toDouble(),
                                          config.hash("Maximum").toDouble());
    } else if (Util::equal_insensitive(type, "Mean")) {
        return new TriggerInputMean(createInput(config.hash("Input"), station, archive), config);
    } else if (Util::equal_insensitive(type, "sd", "StandardDeviation")) {
        return new TriggerInputSD(createInput(config.hash("Input"), station, archive), config);
    } else if (Util::equal_insensitive(type, "Quantile")) {
        return new TriggerInputQuantile(createInput(config.hash("Input"), station, archive),
                                        config.hash("Quantile").toDouble(), config);
    } else if (Util::equal_insensitive(type, "Median")) {
        return new TriggerInputQuantile(createInput(config.hash("Input"), station, archive), 0.5,
                                        config);
    } else if (Util::equal_insensitive(type, "Minimum", "min")) {
        return new TriggerInputQuantile(createInput(config.hash("Input"), station, archive), 0.0,
                                        config);
    } else if (Util::equal_insensitive(type, "Maximum", "max")) {
        return new TriggerInputQuantile(createInput(config.hash("Input"), station, archive), 1.0,
                                        config);
    } else if (Util::equal_insensitive(type, "Slope")) {
        return new TriggerInputSlope(createInput(config.hash("Input"), station, archive), config);
    } else if (Util::equal_insensitive(type, "Length", "Duration", "Elapsed")) {
        return new TriggerInputLength(createInput(config.hash("Input"), station, archive), config);
    } else if (Util::equal_insensitive(type, "Average", "Smoothed")) {
        /* Special handling, so we don't automatically exclude the coverage data */
        auto excludeNoCover = Util::to_qstringlist(SequenceName::defaultLacksFlavors());
        excludeNoCover.removeAll(QString::fromStdString(SequenceName::flavor_cover));
        SequenceMatch::OrderedLookup inputSel(config.hash("Value"), {}, {QString::fromStdString(
                Archive::Selection::excludeMetaArchiveMatcher())}, {}, {}, excludeNoCover);
        if (!inputSel.valid()) {
            inputSel = SequenceMatch::OrderedLookup(SequenceMatch::Element({},
                                                                           QString::fromStdString(
                                                                                   Archive::Selection::excludeMetaArchiveMatcher()),
                                                                           {}));
        }
        inputSel.registerExpected(station, archive);
        return new TriggerInputAverage(inputSel, config, config.hash("Path").toString());
    } else if (Util::equal_insensitive(type, "Sum", "Add")) {
        return new TriggerInputSum(createMultiInput(config.hash("Inputs"), station, archive),
                                   defaultTrue(config.hash("RequireAll")));
    } else if (Util::equal_insensitive(type, "Difference", "Subtract")) {
        return new TriggerInputDifference(createMultiInput(config.hash("Inputs"), station, archive),
                                          defaultTrue(config.hash("RequireAll")));
    } else if (Util::equal_insensitive(type, "Product", "Multiply")) {
        return new TriggerInputProduct(createMultiInput(config.hash("Inputs"), station, archive),
                                       defaultTrue(config.hash("RequireAll")));
    } else if (Util::equal_insensitive(type, "Quotient", "Divide")) {
        return new TriggerInputQuotient(createMultiInput(config.hash("Inputs"), station, archive),
                                        defaultTrue(config.hash("RequireAll")));
    } else if (Util::equal_insensitive(type, "Power")) {
        return new TriggerInputPower(createMultiInput(config.hash("Inputs"), station, archive),
                                     defaultTrue(config.hash("RequireAll")));
    } else if (Util::equal_insensitive(type, "Largest")) {
        return new TriggerInputLargest(createMultiInput(config.hash("Inputs"), station, archive),
                                       defaultTrue(config.hash("RequireAll")));
    } else if (Util::equal_insensitive(type, "Smallest")) {
        return new TriggerInputSmallest(createMultiInput(config.hash("Inputs"), station, archive),
                                        defaultTrue(config.hash("RequireAll")));
    } else if (Util::equal_insensitive(type, "First", "FirstValid", "Valid")) {
        return new TriggerInputFirstValid(
                createMultiInput(config.hash("Inputs"), station, archive));
    }

    return new TriggerInputValue(toSelection(config.hash("Value"), station, archive),
                                 config.hash("Path").toString());
}

Trigger *DirectiveContainer::createTrigger(const Variant::Read &config,
                                           const SequenceName::Component &station,
                                           const SequenceName::Component &archive)
{
    switch (config.getType()) {
    case Variant::Type::Empty:
        return new TriggerAlways;
    case Variant::Type::Boolean:
        if (config.toBool()) {
            return new TriggerAlways;
        } else {
            Trigger *result = new TriggerAlways;
            result->setInverted(true);
            return result;
        }
    case Variant::Type::Array: {
        QList<Trigger *> components;
        for (auto add : config.toArray()) {
            components.append(createTrigger(add, station, archive));
        }
        return new TriggerOR(components);
    }
    default:
        break;
    }

    Trigger *result;

    const auto &type = config.hash("Type").toString();
    if (Util::equal_insensitive(type, "OR", "Any")) {
        QList<Trigger *> components;
        for (auto add : config.hash("Components").toArray()) {
            components.append(createTrigger(add, station, archive));
        }
        result = new TriggerOR(components);
    } else if (Util::equal_insensitive(type, "And", "All")) {
        QList<Trigger *> components;
        for (auto add : config.hash("Components").toArray()) {
            components.append(createTrigger(add, station, archive));
        }
        result = new TriggerAND(components);
    } else if (Util::equal_insensitive(type, "Never", "Disable")) {
        result = new TriggerAlways;
        result->setInverted(true);
    } else if (Util::equal_insensitive(type, "Periodic", "Moment", "Instant")) {
        int intervalCount = 1;
        bool intervalAligned = false;
        Time::LogicalTimeUnit intervalUnit =
                Variant::Composite::toTimeInterval(config.hash("Interval"), &intervalCount,
                                                   &intervalAligned);

        QSet<int> moments;
        for (auto add : config.hash("Moments").toChildren()) {
            qint64 i = add.toInt64();
            if (!INTEGER::defined(i) || i < 0)
                continue;
            moments.insert((int) i);
        }
        if (moments.empty()) {
            qint64 i = config.hash("Moments").toInt64();
            if (INTEGER::defined(i) && i >= 0)
                moments.insert((int) i);
        }

        result = new TriggerPeriodicMoment(intervalUnit, intervalCount, intervalAligned,
                                           Variant::Composite::toTimeInterval(
                                                   config.hash("MomentUnit")), moments);
    } else if (Util::equal_insensitive(type, "PeriodicRange", "TimeRange")) {
        int intervalCount = 1;
        bool intervalAligned = false;
        Time::LogicalTimeUnit intervalUnit =
                Variant::Composite::toTimeInterval(config.hash("Interval"), &intervalCount,
                                                   &intervalAligned);

        int startCount = 1;
        bool startAligned = false;
        Time::LogicalTimeUnit startUnit =
                Variant::Composite::toTimeInterval(config.hash("Start"), &startCount,
                                                   &startAligned);

        int endCount = 1;
        bool endAligned = false;
        Time::LogicalTimeUnit endUnit =
                Variant::Composite::toTimeInterval(config.hash("End"), &endCount, &endAligned);

        result = new TriggerPeriodicRange(intervalUnit, intervalCount, intervalAligned, startUnit,
                                          startCount, startAligned, endUnit, endCount, endAligned);
    } else if (Util::equal_insensitive(type, "Less", "Lessthan")) {
        result = new TriggerLessThan(createInput(config.hash("Left"), station, archive),
                                     createInput(config.hash("Right"), station, archive));
    } else if (Util::equal_insensitive(type, "LessEqual", "LessThanOrEqual")) {
        result = new TriggerLessThanEqual(createInput(config.hash("Left"), station, archive),
                                          createInput(config.hash("Right"), station, archive));
    } else if (Util::equal_insensitive(type, "Greater", "GreaterThan")) {
        result = new TriggerGreaterThan(createInput(config.hash("Left"), station, archive),
                                        createInput(config.hash("Right"), station, archive));
    } else if (Util::equal_insensitive(type, "GreaterEqual", "GreaterThanOrEqual")) {
        result = new TriggerGreaterThanEqual(createInput(config.hash("Left"), station, archive),
                                             createInput(config.hash("Right"), station, archive));
    } else if (Util::equal_insensitive(type, "Equal", "EqualTo")) {
        result = new TriggerEqual(createInput(config.hash("Left"), station, archive),
                                  createInput(config.hash("Right"), station, archive));
    } else if (Util::equal_insensitive(type, "NotEqual", "NotEqualTo")) {
        result = new TriggerNotEqual(createInput(config.hash("Left"), station, archive),
                                     createInput(config.hash("Right"), station, archive));
    } else if (Util::equal_insensitive(type, "Range", "InsideRange")) {
        result = new TriggerInsideRange(createInput(config.hash("Start"), station, archive),
                                        createInput(config.hash("Value"), station, archive),
                                        createInput(config.hash("End"), station, archive));
    } else if (Util::equal_insensitive(type, "ModularRange", "InsideModularRange")) {
        result = new TriggerInsideModularRange(createInput(config.hash("Start"), station, archive),
                                               createInput(config.hash("Value"), station, archive),
                                               createInput(config.hash("End"), station, archive),
                                               createInput(config.hash("Modulus").hash("Start"),
                                                           station, archive),
                                               createInput(config.hash("Modulus").hash("End"),
                                                           station, archive));
    } else if (Util::equal_insensitive(type, "OutsideRange")) {
        result = new TriggerOutsideRange(createInput(config.hash("Start"), station, archive),
                                         createInput(config.hash("Value"), station, archive),
                                         createInput(config.hash("End"), station, archive));
    } else if (Util::equal_insensitive(type, "OutsideModularRange")) {
        result = new TriggerOutsideModularRange(createInput(config.hash("Start"), station, archive),
                                                createInput(config.hash("Value"), station, archive),
                                                createInput(config.hash("End"), station, archive),
                                                createInput(config.hash("Modulus").hash("Start"),
                                                            station, archive),
                                                createInput(config.hash("Modulus").hash("End"),
                                                            station, archive));
    } else if (Util::equal_insensitive(type, "HasFlag", "HasFlags", "Flag", "Flags")) {
        auto flags = toFlags(config.hash("Flags"));
        Util::merge(toFlags(config.hash("Flag")), flags);
        result = new TriggerHasFlags(flags, toSelection(config.hash("Input"), station, archive),
                                     config.hash("Path").toString());
    } else if (Util::equal_insensitive(type, "Lacksflag", "LacksFlags")) {
        auto flags = toFlags(config.hash("Flags"));
        Util::merge(toFlags(config.hash("Flag")), flags);
        result = new TriggerLacksFlags(flags, toSelection(config.hash("Input"), station, archive),
                                       config.hash("Path").toString());
    } else if (Util::equal_insensitive(type, "Exists")) {
        result = new TriggerExists(toSelection(config.hash("Input"), station, archive),
                                   config.hash("Path").toString());
    } else if (Util::equal_insensitive(type, "Defined")) {
        result = new TriggerDefined(createInput(config.hash("Value"), station, archive));
    } else if (Util::equal_insensitive(type, "Undefined")) {
        result = new TriggerUndefined(createInput(config.hash("Value"), station, archive));
    } else if (Util::equal_insensitive(type, "Script")) {
        result = new TriggerScript(config.hash("Code").toString(),
                                   toSelection(config.hash("Input"), station, archive));
    } else {
        result = new TriggerAlways;
    }
    Q_ASSERT(result);


    if (config.hash("Invert").exists())
        result->setInverted(config.hash("Invert").toBool());
    else if (config.hash("Inverted").exists())
        result->setInverted(config.hash("Inverted").toBool());

    if (config.hash("Extend").exists()) {
        TriggerExtending *ext = new TriggerExtending(result);
        Q_ASSERT(ext);
        result = ext;

        int count = 1;
        bool align = false;
        Time::LogicalTimeUnit
                unit = Variant::Composite::toTimeInterval(config.hash("Extend"), &count, &align);

        int beforeCount = count;
        bool beforeAlign = align;
        Time::LogicalTimeUnit beforeUnit = unit;
        if (config.hash("Extend").hash("Before").exists()) {
            beforeUnit = Variant::Composite::toTimeInterval(config.hash("Extend").hash("Before"),
                                                            &beforeCount, &beforeAlign);
        }
        if (beforeCount >= 0)
            ext->setBefore(beforeUnit, beforeCount, beforeAlign);

        int afterCount = count;
        bool afterAlign = align;
        Time::LogicalTimeUnit afterUnit = unit;
        if (config.hash("Extend").hash("After").exists()) {
            afterUnit = Variant::Composite::toTimeInterval(config.hash("Extend").hash("After"),
                                                           &afterCount, &afterAlign);
        }
        if (afterCount >= 0)
            ext->setAfter(afterUnit, afterCount, afterAlign);
    }

    return result;
}

static Action *createAction(const Variant::Read &config,
                            const SequenceName::Component &station,
                            const SequenceName::Component &archive,
                            EditDirective::FanoutMode *defaultFanout = nullptr,
                            bool *defaultFragment = nullptr)
{
    const auto &type = config.hash("Type").toString();

    if (Util::equal_insensitive(type, "Remove")) {
        return new ActionRemove(toSelection(config.hash("Selection"), station, archive));
    } else if (Util::equal_insensitive(type, "Poly", "Polynomial", "Cal", "Calibration")) {
        return new ActionApplyPoly(toSelection(config.hash("Selection"), station, archive),
                                   Variant::Composite::toCalibration(config.hash("Calibration")));
    } else if (Util::equal_insensitive(type, "PolyInvert", "PolynomialInvert", "InvertCal",
                                       "InvertCalibration")) {
        return new ActionInversePoly(toSelection(config.hash("Selection"), station, archive),
                                     Variant::Composite::toCalibration(config.hash("Calibration")),
                                     config.hash("Minimum").toDouble(),
                                     config.hash("Maximum").toDouble());
    } else if (Util::equal_insensitive(type, "Recalibrate")) {
        return new ActionReapplyPoly(toSelection(config.hash("Selection"), station, archive),
                                     Variant::Composite::toCalibration(config.hash("Calibration")),
                                     Variant::Composite::toCalibration(config.hash("Original")),
                                     config.hash("Minimum").toDouble(),
                                     config.hash("Maximum").toDouble());
    } else if (Util::equal_insensitive(type, "Wrap", "Modular", "Modulus")) {
        return new ActionWrapModular(toSelection(config.hash("Selection"), station, archive),
                                     config.hash("Start").toDouble(),
                                     config.hash("End").toDouble());
    } else if (Util::equal_insensitive(type, "Overlay", "Set")) {
        return new ActionOverlayValue(toSelection(config.hash("Selection"), station, archive),
                                      config.hash("Value"), config.hash("Path").toString());
    } else if (Util::equal_insensitive(type, "Meta", "Metadata", "OverlayMeta",
                                       "OverlayMetadata")) {
        if (defaultFragment)
            *defaultFragment = true;
        return new ActionOverlayMetadata(toSelection(config.hash("Selection"), station, archive),
                                         config.hash("Value"), config.hash("Path").toString());
    } else if (Util::equal_insensitive(type, "Serial", "SetSerial")) {
        const auto &instrument = config.hash("Instrument").toString();
        if (defaultFragment)
            *defaultFragment = true;
        if (!instrument.empty()) {
            SequenceMatch::OrderedLookup sel{SequenceMatch::Element("[^_]+_" + instrument,
                                                                    Archive::Selection::excludeMetaArchiveMatcher(),
                                                                    {}, {},
                                                                    {SequenceName::flavor_cover,
                                                                     SequenceName::flavor_stats})};
            sel.registerExpected(station, archive);
            return new ActionOverlayMetadata(sel, config.hash("SerialNumber"),
                                             "^Source/SerialNumber");
        } else {
            return new ActionOverlayMetadata(
                    toSelection(config.hash("Selection"), station, archive),
                    config.hash("SerialNumber"), "^Source/SerialNumber");
        }
    } else if (Util::equal_insensitive(type, "Flag", "Addflag", "AddFlags")) {
        auto flags = toFlags(config.hash("Flags"));
        Util::merge(toFlags(config.hash("Flag")), flags);
        return new ActionAddFlag(toSelection(config.hash("Selection"), station, archive), flags,
                                 config.hash("Description"), config.hash("Bits").toInt64());
    } else if (Util::equal_insensitive(type, "RemoveFlag", "RemoveFlags")) {
        auto flags = toFlags(config.hash("Flags"));
        Util::merge(toFlags(config.hash("Flag")), flags);
        return new ActionRemoveFlag(toSelection(config.hash("Selection"), station, archive), flags);
    } else if (Util::equal_insensitive(type, "RemoveAnyFlag", "RemoveAnyFlags",
                                       "RemoveMatchingFlags")) {
        QList<QRegExp> matchers;

        Qt::CaseSensitivity cs =
                config.hash("CaseSensitive").toBool() ? Qt::CaseSensitive : Qt::CaseInsensitive;

        for (const auto &add : config.hash("Flags").toChildren().keys()) {
            matchers.append(QRegExp(QString::fromStdString(add), cs));
        }

        return new ActionRemoveMatchingFlags(
                toSelection(config.hash("Selection"), station, archive), matchers,
                config.hash("CheckAll").toBool());
    } else if (Util::equal_insensitive(type, "Contaminate", "Contam")) {
        Variant::Read description = Variant::Root(
                QObject::tr("Data flagged as contaminated by the station mentor")).read();
        qint64 bits = 0x0002;
        std::string origin = "Mentor";

        if (config.hash("Description").exists())
            description = config.hash("Description");
        if (INTEGER::defined(config.hash("Bits").toInt64()))
            bits = config.hash("Bits").toInt64();
        if (config.hash("Origin").exists())
            origin = config.hash("Origin").toString();

        SequenceMatch::Composite sel{SequenceMatch::Element{{}, {}, "F1_.+", {},
                                                            {SequenceName::flavor_cover,
                                                             SequenceName::flavor_stats}}};
        return new ActionAddFlag(sel, Variant::Flags{std::string("Contaminate") + origin},
                                 description, bits);
    } else if (Util::equal_insensitive(type, "Uncontaminate", "ClearContam")) {
        SequenceMatch::Composite sel{SequenceMatch::Element{{}, {}, "F1_.+", {},
                                                            {SequenceName::flavor_cover,
                                                             SequenceName::flavor_stats}}};
        return new ActionRemoveMatchingFlags(sel, QList<QRegExp>()
                << QRegExp("Contaminat.*", Qt::CaseInsensitive));
    } else if (Util::equal_insensitive(type, "FlowCorrection", "FlowCalibration")) {
        bool ignoreAccumulatedCut = defaultTrue(config.hash("CombineCut"));
        if (ignoreAccumulatedCut && defaultFanout)
            *defaultFanout &= ~EditDirective::Fanout_Flavors;

        const auto &instrument = config.hash("Instrument").toString();
        if (!instrument.empty()) {
            return new ActionFlowCorrection(
                    Variant::Composite::toCalibration(config.hash("Original")),
                    Variant::Composite::toCalibration(config.hash("Calibration")), instrument,
                    station, archive, ignoreAccumulatedCut, config.hash("Minimum").toDouble(),
                    config.hash("Maximum").toDouble());
        } else {
            return new ActionFlowCorrection(
                    Variant::Composite::toCalibration(config.hash("Original")),
                    Variant::Composite::toCalibration(config.hash("Calibration")),
                    toSelection(config.hash("Flow"), station, archive),
                    toSelection(config.hash("Accumulator"), station, archive),
                    toSelection(config.hash("Spot"), station, archive),
                    toSelection(config.hash("Selection"), station, archive), ignoreAccumulatedCut,
                    config.hash("Minimum").toDouble(), config.hash("Maximum").toDouble());
        }
    } else if (Util::equal_insensitive(type, "Spot", "SpotSize")) {
        const auto &instrument = config.hash("Instrument").toString();
        if (!instrument.empty()) {
            return new ActionSpotCorrection(config.hash("Original").toDouble(),
                                            config.hash("Corrected").toDouble(), instrument,
                                            station, archive);
        } else {
            return new ActionSpotCorrection(config.hash("Original").toDouble(),
                                            config.hash("Corrected").toDouble(),
                                            toSelection(config.hash("Length"), station, archive),
                                            toSelection(config.hash("Spot"), station, archive),
                                            toSelection(config.hash("Selection"), station,
                                                        archive));
        }
    } else if (Util::equal_insensitive(type, "MultiSpot", "MultiSpotSize", "CLAPSpot")) {
        std::vector<double> before;
        for (auto v : config.hash("Original").toArray()) {
            before.push_back(v.toDouble());
        }
        std::vector<double> after;
        for (auto v : config.hash("Corrected").toArray()) {
            after.push_back(v.toDouble());
        }

        const auto &instrument = config.hash("Instrument").toString();
        if (!instrument.empty()) {
            return new ActionMultiSpotCorrection(before, after, instrument, station, archive);
        } else {
            return new ActionMultiSpotCorrection(before, after,
                                                 toSelection(config.hash("Length"), station,
                                                             archive),
                                                 toSelection(config.hash("Spot"), station, archive),
                                                 toSelection(config.hash("Index"), station,
                                                             archive),
                                                 toSelection(config.hash("Selection"), station,
                                                             archive));
        }
    } else if (Util::equal_insensitive(type, "Unit", "SetUnit", "Duplicate")) {
        ActionUnitSet::Components operations = 0;
        SequenceName target;
        if (config.hash("Station").exists()) {
            operations |= ActionUnitSet::Apply_Station;
            target.setStation(config.hash("Station").toString());
        }
        if (config.hash("Archive").exists()) {
            operations |= ActionUnitSet::Apply_Archive;
            target.setArchive(config.hash("Archive").toString());
        }
        if (config.hash("Variable").exists()) {
            operations |= ActionUnitSet::Apply_Variable;
            target.setVariable(config.hash("Variable").toString());
        }
        if (config.hash("Flavors").exists()) {
            operations |= ActionUnitSet::Apply_Flavors;
            target.setFlavors(toFlavors(config.hash("Flavors")));
        }

        if (Util::equal_insensitive(type, "Duplicate")) {
            return new ActionUnitDuplicate(toSelection(config.hash("Selection"), station, archive),
                                           target, operations,
                                           defaultTrue(config.hash("ApplyToMetadata")));
        } else {
            return new ActionUnitSet(toSelection(config.hash("Selection"), station, archive),
                                     target, operations,
                                     defaultTrue(config.hash("ApplyToMetadata")));
        }
    } else if (Util::equal_insensitive(type, "UnitReplace", "Translate", "DuplicateTranslate")) {
        auto from = config.hash("From");
        QRegExp fromStation;
        if (from.hash("Station").exists())
            fromStation = QRegExp(from.hash("Station").toQString(), Qt::CaseInsensitive);
        QRegExp fromArchive;
        if (from.hash("Archive").exists())
            fromArchive = QRegExp(from.hash("Archive").toQString(), Qt::CaseInsensitive);
        QRegExp fromVariable;
        if (from.hash("Variable").exists())
            fromVariable = QRegExp(from.hash("Variable").toQString(), Qt::CaseSensitive);
        QRegExp fromFlavor;
        if (from.hash("Flavor").exists())
            fromFlavor = QRegExp(from.hash("Flavor").toQString(), Qt::CaseInsensitive);

        auto to = config.hash("To");
        QString toStation(to.hash("Station").toQString());
        QString toArchive(to.hash("Archive").toQString());
        QString toVariable(to.hash("Variable").toQString());
        QString toFlavor(to.hash("Flavor").toQString());

        if (Util::equal_insensitive(type, "DuplicateTranslate")) {
            return new ActionUnitReplaceDuplicate(
                    toSelection(config.hash("Selection"), station, archive), fromStation, toStation,
                    fromArchive, toArchive, fromVariable, toVariable, fromFlavor, toFlavor,
                    defaultTrue(config.hash("ApplyToMetadata")));
        } else {
            return new ActionUnitReplace(toSelection(config.hash("Selection"), station, archive),
                                         fromStation, toStation, fromArchive, toArchive,
                                         fromVariable, toVariable, fromFlavor, toFlavor,
                                         defaultTrue(config.hash("ApplyToMetadata")));
        }
    } else if (Util::equal_insensitive(type, "Flavors")) {
        return new ActionModifyFlavors(toSelection(config.hash("Selection"), station, archive),
                                       toFlavors(config.hash("Add")),
                                       toFlavors(config.hash("Remove")),
                                       config.hash("ApplyToMetadata").toBool());
    } else if (Util::equal_insensitive(type, "SetCut", "Cut")) {
        SequenceName::Flavors cut;
        if (!config.hash("Cut").toString().empty())
            cut.insert(config.hash("Cut").toString());

        return new ActionModifyFlavors(toSelection(config.hash("Selection"), station, archive), cut,
                                       SequenceName::cutSizeFlavors(),
                                       config.hash("ApplyToMetadata").toBool());
    } else if (Util::equal_insensitive(type, "MultiPoly", "MultiPolynomial", "MultiCal",
                                       "MultiCalibration")) {
        const auto &interType = config.hash("Interpolation").toString();
        ActionMultiPoly::Interpolation interpolation = ActionMultiPoly::Linear;
        double k0 = FP::undefined();
        double k1 = FP::undefined();
        if (Util::equal_insensitive(interType, "CSpline", "CubicSpline")) {
            interpolation = ActionMultiPoly::CSpline;
            k0 = config.hash("ClampStart").toDouble();
            k1 = config.hash("ClampEnd").toDouble();
        } else if (Util::equal_insensitive(interType, "sin")) {
            interpolation = ActionMultiPoly::Sin;
            k0 = config.hash("SinPower").toDouble();
        } else if (Util::equal_insensitive(interType, "cos")) {
            interpolation = ActionMultiPoly::Sin;
            k0 = config.hash("CosPower").toDouble();
        } else if (Util::equal_insensitive(interType, "sigmoid")) {
            interpolation = ActionMultiPoly::Sigmoid;
            k0 = config.hash("SigmoidSlope").toDouble();
        }

        const auto &timePointType = config.hash("Time").toString();
        ActionMultiPoly::TimeSelection time = ActionMultiPoly::Middle;
        if (Util::equal_insensitive(timePointType, "Start", "Begin"))
            time = ActionMultiPoly::Start;
        else if (Util::equal_insensitive(timePointType, "End"))
            time = ActionMultiPoly::End;

        QMap<double, Calibration> polys;
        for (auto add : config.hash("Calibrations").toKeyframe()) {
            if (!FP::defined(add.first) || !add.second.exists())
                continue;
            polys.insert(add.first, Variant::Composite::toCalibration(add.second));
        }
        return new ActionMultiPoly(toSelection(config.hash("Selection"), station, archive), polys,
                                   interpolation, time, config.hash("Extrapolate").toBool(), k0,
                                   k1);
    } else if (Util::equal_insensitive(type, "Arithmetic", "Math", "Function")) {
        auto values = toSelection(config.hash("Selection"), station, archive);
        auto inputs = toSelection(config.hash("Inputs"), station, archive);
        Calibration cal = Variant::Composite::toCalibration(config.hash("Calibration"));
        const auto &path = config.hash("Path").toString();

        const auto &t = config.hash("Function").toString();
        if (Util::equal_insensitive(t, "Subtract", "Minus"))
            return new ActionSubtract(values, inputs, cal, path);
        else if (Util::equal_insensitive(t, "Multiply", "Times"))
            return new ActionMultiply(values, inputs, cal, path);
        else if (Util::equal_insensitive(t, "Divide"))
            return new ActionDivide(values, inputs, cal, path);
        else if (Util::equal_insensitive(t, "Invert", "Inverse"))
            return new ActionInvert(values, inputs, cal, path);
        else if (Util::equal_insensitive(t, "Assign", "Set", "Equal"))
            return new ActionAssign(values, inputs, cal, path);
        return new ActionAdd(values, inputs, cal, path);
    } else if (Util::equal_insensitive(type, "ScriptValue", "ScriptValues")) {
        return new ActionScriptSequenceValueProcessor(
                toSelection(config.hash("Selection"), station, archive),
                config.hash("Code").toString());
    } else if (Util::equal_insensitive(type, "ScriptSegment", "ScriptSegments", "Script")) {
        auto outputs = toSelection(config.hash("Selection"), station, archive);
        auto inputs = outputs;

        if (config.hash("Outputs").exists())
            outputs = toSelection(config.hash("Outputs"), station, archive);
        if (config.hash("Inputs").exists())
            inputs = toSelection(config.hash("Inputs"), station, archive);

        return new ActionScriptSequenceSegmentProcessor(outputs, inputs,
                                                        config.hash("Code").toString(), defaultTrue(
                        config.hash("ImplicitMetadata")));
    } else if (Util::equal_insensitive(type, "ScriptDemultiplexer", "Demultiplexer")) {
        if (defaultFanout)
            *defaultFanout = EditDirective::Fanout_None;
        return new ActionScriptFanoutProcessor(config.hash("Code").toString());
    } else if (Util::equal_insensitive(type, "ScriptGeneralValue", "ScriptGeneralValues")) {
        return new ActionScriptSequenceValueGeneral(
                toSelection(config.hash("Selection"), station, archive),
                config.hash("Code").toString());
    } else if (Util::equal_insensitive(type, "ScriptGeneralSegment", "ScriptGeneralSegments")) {
        auto outputs = toSelection(config.hash("Selection"), station, archive);
        auto inputs = outputs;

        if (config.hash("Outputs").exists())
            outputs = toSelection(config.hash("Outputs"), station, archive);
        if (config.hash("Inputs").exists())
            inputs = toSelection(config.hash("Inputs"), station, archive);

        return new ActionScriptSequenceSegmentGeneral(outputs, inputs,
                                                      config.hash("Code").toString(),
                                                      defaultTrue(config.hash("ImplicitMetadata")));
    } else if (Util::equal_insensitive(type, "NOOP", "None")) {
        return new ActionNOOP;
    } else if (Util::equal_insensitive(type, "AbnormalDataEpisode")) {
        if (defaultFanout)
            *defaultFanout = EditDirective::Fanout_None;
        SequenceMatch::Composite flagsSelection
                {SequenceMatch::Element({}, {}, SequenceName::Component("F1_.*"), {},
                                        {SequenceName::flavor_cover, SequenceName::flavor_stats})};
        return new ActionAddFlag(flagsSelection, "EBASFlag110", EBAS::description(110));
    }

    return new ActionInvalidate(toSelection(config.hash("Selection"), station, archive),
                                defaultTrue(config.hash("PreserveType")));
}

EditDirective::FanoutMode DirectiveContainer::toFanout(const Variant::Read &config,
                                                       EditDirective::FanoutMode defaultFanout)
{
    if (!config.exists())
        return defaultFanout;
    EditDirective::FanoutMode result = 0;
    if (defaultTrue(config.hash("Station"), (defaultFanout & EditDirective::Fanout_Station) != 0))
        result |= EditDirective::Fanout_Station;
    if (defaultTrue(config.hash("Archive"), (defaultFanout & EditDirective::Fanout_Archive) != 0))
        result |= EditDirective::Fanout_Archive;
    if (defaultTrue(config.hash("Variable"), (defaultFanout & EditDirective::Fanout_Variable) != 0))
        result |= EditDirective::Fanout_Variable;
    if (defaultTrue(config.hash("Flavors"), (defaultFanout & EditDirective::Fanout_Flavors) != 0))
        result |= EditDirective::Fanout_Flavors;

    if (defaultTrue(config.hash("FlattenStatistics"),
                    (defaultFanout & EditDirective::Fanout_FlattenStatistics) != 0))
        result |= EditDirective::Fanout_FlattenStatistics;
    if (defaultTrue(config.hash("FlattenMetadata"),
                    (defaultFanout & EditDirective::Fanout_FlattenMeta) != 0))
        result |= EditDirective::Fanout_FlattenMeta;

    return result;
}

std::vector<std::unique_ptr<EditDirective>> DirectiveContainer::instantiate(double start,
                                                                            double end,
                                                                            const SequenceName::Component &station,
                                                                            const SequenceName::Component &archive) const
{
    std::vector<std::unique_ptr<EditDirective>> result;
    if (!isActive())
        return std::move(result);
    auto params = value.read().hash("Parameters");

    Trigger *trigger = createTrigger(params.hash("Trigger"), station, archive);
    if (trigger->isNeverActive(start, end)) {
        delete trigger;
        return std::move(result);
    }

    EditDirective::FanoutMode triggerFanout = toFanout(params.hash("Trigger").hash("Fanout"));

    EditDirective::FanoutMode actionFanout = EditDirective::Fanout_Default;
    bool actionFragment = params.hash("Action").hash("Fragment").toBool();
    Action *action =
            createAction(params.hash("Action"), station, archive, &actionFanout, &actionFragment);
    actionFanout = toFanout(params.hash("Action").hash("Fanout"), actionFanout);

    result.emplace_back(
            new EditDirective(getStart(), getEnd(), trigger, action, triggerFanout, actionFanout,
                              actionFragment));

    auto ebasFlags = toFlags(params.hash("EBASFlags"));
    if (!ebasFlags.empty()) {
        SequenceMatch::Composite flagsSelection
                {SequenceMatch::Element({}, {}, SequenceName::Component("F1_.*"), {},
                                        {SequenceName::flavor_cover, SequenceName::flavor_stats})};

        for (const auto &flag : ebasFlags) {
            if (flag.empty())
                continue;
            result.emplace_back(new EditDirective(getStart(), getEnd(), trigger->clone(),
                                                  new ActionAddFlag(flagsSelection, flag,
                                                                    EBAS::description(
                                                                            QString::fromStdString(
                                                                                    flag))),
                                                  triggerFanout, EditDirective::Fanout_None));
        }
    }

    return std::move(result);
}

void DirectiveContainer::extend(double &start,
                                double &end,
                                Data::Archive::Access *access,
                                const SequenceName::Component &station,
                                const SequenceName::Component &archive) const
{
    auto params = value.read().hash("Extend");
    double t = params.hash("Start").toDouble();
    if (FP::defined(t) && Range::compareStart(start, t) > 0)
        start = t;
    t = params.hash("End").toDouble();
    if (FP::defined(t) && Range::compareEnd(end, t) < 0)
        end = t;

    if (access && params.hash("Data").exists()) {
        SequenceMatch::Composite sel(params.hash("Data"));
        Archive::Selection::Match stations;
        if (!station.empty())
            stations.emplace_back(station);
        Archive::Selection::Match archives;
        if (!archive.empty())
            archives.emplace_back(archive);
        auto selections = sel.toArchiveSelections(stations, archives);
        if (!selections.empty()) {
            for (auto &mod : selections) {
                mod.start = start;
                mod.end = end;
                mod.includeMetaArchive = false;
                mod.includeDefaultStation = false;
            }
            StreamSink::Iterator data;
            auto reader = access->readStream(selections, &data);
            while (data.hasNext()) {
                auto value = data.next();
                if (!sel.matches(value.getName()))
                    continue;
                if (params.hash("Data").hash("Start").toBool() &&
                        FP::defined(value.getStart()) &&
                        Range::compareStart(start, value.getStart()) < 0)
                    start = value.getStart();
                if (params.hash("Data").hash("End").toBool()) {
                    for (;;) {
                        if (sel.matches(value.getName())) {
                            if (FP::defined(value.getEnd()) &&
                                    Range::compareEnd(start, value.getEnd()) > 0)
                                end = value.getEnd();
                        }
                        if (!data.hasNext())
                            break;
                        value = data.next();
                    }
                }
                break;
            }
            data.abort();
            reader->signalTerminate();
            data.wait();
        }
    }
}

void DirectiveContainer::getAveraging(SequenceMatch::Composite &continuous,
                                      SequenceMatch::Composite &full) const
{
    auto params = value.read().hash("Average");
    if (!params.exists())
        return;

    if (params.hash("Continuous").exists()) {
        continuous.append(
                SequenceMatch::Composite(params.hash("Continuous"), {}, {}, {}, {}, {}, {}));
    }
    if (params.hash("Full").exists()) {
        full.append(SequenceMatch::Composite(params.hash("Full"), {}, {}, {}, {}, {}, {}));
    }
}

static Variant::Root historyBase()
{
    Variant::Root result;

    auto wr = result.write();
    wr.hash("At").setDouble(Time::time());
    wr.hash("Environment").setString(Environment::describe());
    wr.hash("Revision").setString(Environment::revision());
    wr.hash("User").setString(Environment::user());

    return result;
}

bool DirectiveContainer::changed() const
{
    if (!original.isValid())
        return true;
    if (original.getUnit().getVariable() != value.getUnit().getVariable())
        return true;
    if (!FP::equal(value.getStart(), original.getStart()) ||
            !FP::equal(value.getEnd(), original.getEnd()))
        return true;
    if (original.read().hash("Parameters") != value.read().hash("Parameters"))
        return true;
    if (original.read().hash("Extend") != value.read().hash("Extend"))
        return true;
    if (original.read().hash("Author") != value.read().hash("Author"))
        return true;
    if (original.read().hash("Comment") != value.read().hash("Comment"))
        return true;
    if (original.read().hash("Priority") != value.read().hash("Priority"))
        return true;
    if (original.read().hash("SystemInternal") != value.read().hash("SystemInternal"))
        return true;
    if (!original.read().hash("Disabled").toBool() && value.read().hash("Disabled").toBool())
        return true;
    if (original.read().hash("Disabled").toBool() && !value.read().hash("Disabled").toBool())
        return true;
    return false;
}

SequenceValue DirectiveContainer::commit(bool *changed)
{
    if (changed)
        *changed = false;

    SequenceValue result(value);

    Variant::Write history = result.write().hash("History");

    if (!original.isValid()) {
        auto add = historyBase().write();
        add.hash("Type").setString("Created");
        if (original.read().hash("History").exists())
            add.hash("Prehistory").set(original.read().hash("History"));
        history.toArray().after_back().set(add);

        if (changed)
            *changed = true;
        return result;
    }

    if (original.getUnit().getVariable() != result.getUnit().getVariable()) {
        auto add = historyBase().write();
        add.hash("Type").setString("ProfileChanged");
        add.hash("OriginalProfile").setString(original.getUnit().getVariable());
        history.toArray().after_back().set(add);

        if (changed)
            *changed = true;
    }

    if (!FP::equal(result.getStart(), original.getStart()) ||
            !FP::equal(result.getEnd(), original.getEnd())) {
        auto add = historyBase().write();
        add.hash("Type").setString("BoundsChanged");
        add.hash("OriginalBounds").hash("Start").setDouble(original.getStart());
        add.hash("OriginalBounds").hash("End").setDouble(original.getEnd());
        add.hash("RevisedBounds").hash("Start").setDouble(value.getStart());
        add.hash("RevisedBounds").hash("End").setDouble(value.getEnd());
        history.toArray().after_back().set(add);

        if (changed)
            *changed = true;
    }

    if (original.read().hash("Parameters") != result.read().hash("Parameters")) {
        auto add = historyBase().write();
        add.hash("Type").setString("ParametersChanged");
        add.hash("OriginalParameters").set(original.read().hash("Parameters"));
        history.toArray().after_back().set(add);

        if (changed)
            *changed = true;
    }

    if (original.read().hash("Extend") != result.read().hash("Extend")) {
        auto add = historyBase().write();
        add.hash("Type").setString("ExtendChanged");
        add.hash("OriginalExtend").set(original.read().hash("Extend"));
        history.toArray().after_back().set(add);

        if (changed)
            *changed = true;
    }

    if (original.read().hash("Author") != result.read().hash("Author")) {
        auto add = historyBase().write();
        add.hash("Type").setString("AuthorChanged");
        add.hash("OriginalAuthor").set(original.read().hash("Author"));
        history.toArray().after_back().set(add);

        if (changed)
            *changed = true;
    }

    if (original.read().hash("Comment") != result.read().hash("Comment")) {
        auto add = historyBase().write();
        add.hash("Type").setString("CommentChanged");
        add.hash("OriginalComment").set(original.getValue().hash("Comment"));
        history.toArray().after_back().set(add);

        if (changed)
            *changed = true;
    }

    if (original.read().hash("Priority") != result.read().hash("Priority")) {
        auto add = historyBase().write();
        add.hash("Type").setString("PriorityChanged");
        add.hash("OriginalPriority").set(original.read().hash("Priority"));
        history.toArray().after_back().set(add);

        if (changed)
            *changed = true;
    }

    if (original.read().hash("SystemInternal") != result.read().hash("SystemInternal")) {
        auto add = historyBase().write();
        add.hash("Type").setString("SystemInternalChanged");
        add.hash("OriginalSystemInternal").set(original.read().hash("SystemInternal"));
        history.toArray().after_back().set(add);

        if (changed)
            *changed = true;
    }

    if (!original.read().hash("Disabled").toBool() && result.read().hash("Disabled").toBool()) {
        auto add = historyBase().write();
        add.hash("Type").setString("Disabled");
        history.toArray().after_back().set(add);

        if (changed)
            *changed = true;
    }

    if (original.read().hash("Disabled").toBool() && !result.read().hash("Disabled").toBool()) {
        auto add = historyBase().write();
        add.hash("Type").setString("Enabled");
        history.toArray().after_back().set(add);

        if (changed)
            *changed = true;
    }

    return result;
}

bool DirectiveContainer::operator<(const DirectiveContainer &other) const
{
    qint64 pa = value.read().hash("Priority").toInt64();
    qint64 pb = other.value.read().hash("Priority").toInt64();

    if (!INTEGER::defined(pa)) {
        if (INTEGER::defined(pb))
            return true;
    } else if (!INTEGER::defined(pb)) {
        return false;
    } else if (pa != pb) {
        return pa < pb;
    }

    if (!FP::defined(value.getStart())) {
        if (FP::defined(other.value.getStart()))
            return true;
    } else if (!FP::defined(other.value.getStart())) {
        return false;
    } else if (value.getStart() != other.value.getStart()) {
        return value.getStart() < other.value.getStart();
    }

    return value.getPriority() < other.value.getPriority();
}

bool DirectiveContainer::sortLessThan(qint64 pb) const
{
    qint64 pa = value.read().hash("Priority").toInt64();
    if (!INTEGER::defined(pa)) {
        if (INTEGER::defined(pb))
            return true;
    } else if (!INTEGER::defined(pb)) {
        return false;
    } else if (pa != pb) {
        return pa < pb;
    }
    return false;
}

bool DirectiveContainer::sortGreaterThan(qint64 pa) const
{
    qint64 pb = value.read().hash("Priority").toInt64();
    if (!INTEGER::defined(pa)) {
        if (INTEGER::defined(pb))
            return true;
    } else if (!INTEGER::defined(pb)) {
        return false;
    } else if (pa != pb) {
        return pa < pb;
    }
    return false;
}


}
}
