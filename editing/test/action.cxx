/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/action.hxx"
#include "datacore/stream.hxx"
#include "datacore/staticmultiplexer.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

class Dispatch : public EditDispatchTarget {
    Action *action;

    struct Target {
        QList<EditDataTarget *> targets;
        QList<EditDataModification *> modifiers;
        int muxId;

        Target() : targets(), modifiers(), muxId(0)
        { }
    };

    QHash<SequenceName, Target> d;

    StaticMultiplexer outputMux;

    void handleCompleted(const SequenceValue::Transfer &add)
    {
        Util::append(add, output);
    }

public:
    SequenceValue::Transfer output;

    Dispatch() : action(NULL), d(), outputMux()
    { }

    virtual ~Dispatch()
    { }

    void initialize(Action *a)
    {
        output.clear();
        d.clear();
        action = a;

        outputMux.clear(true);
        outputMux.setStreams(2);

        action->setTarget(this);
    }

    void value(SequenceValue value)
    {
        QHash<SequenceName, Target>::iterator target = d.find(value.getUnit());
        if (target == d.end()) {
            QList<EditDataTarget *> inputs;
            QList<EditDataTarget *> outputs;
            QList<EditDataModification *> modifiers;

            action->unhandledInput(value.getUnit(), inputs, outputs, modifiers);

            Target add;
            add.targets = inputs;
            add.targets.append(outputs);
            add.modifiers = modifiers;

            if (outputs.isEmpty()) {
                add.muxId = 0;
            } else {
                add.muxId = -1;
            }

            target = d.insert(value.getUnit(), add);
        }

        for (QList<EditDataModification *>::const_iterator
                m = target.value().modifiers.constBegin(),
                end = target.value().modifiers.constEnd(); m != end; ++m) {
            (*m)->modifySequenceValue(value);
        }
        for (QList<EditDataTarget *>::const_iterator t = target.value().targets.constBegin(),
                end = target.value().targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(value);
        }
        if (target.value().muxId >= 0) {
            handleCompleted(outputMux.incoming(target.value().muxId, value));
        }
    }

    void value(const SequenceName &unit, const Variant::Read &v, double start, double end)
    { value(SequenceValue(unit, Variant::Root(v), start, end)); }

    void value(const SequenceName &unit, double v, double start, double end)
    { value(SequenceValue(unit, Variant::Root(v), start, end)); }

    void advance(double time)
    {
        action->incomingDataAdvance(time);
        handleCompleted(outputMux.advance(0, time));
    }

    void finish()
    {
        action->finalize();
        handleCompleted(outputMux.finish());
    }

    void incomingData(const Data::SequenceValue &value) override
    { handleCompleted(outputMux.incoming(1, value)); }

    void incomingData(const Data::SequenceValue::Transfer &values) override
    { handleCompleted(outputMux.incoming(1, values)); }

    void incomingAdvance(double time) override
    { handleCompleted(outputMux.advance(1, time)); }

    bool compare(const SequenceValue::Transfer &expected)
    {
        if (expected.size() != output.size())
            return false;

        for (const auto &check : expected) {
            int idx;
            for (idx = output.size() - 1; idx >= 0; --idx) {
                SequenceValue compare(output.at(idx));
                if (compare.getUnit() != check.getUnit())
                    continue;
                if (!FP::equal(compare.getStart(), check.getStart()))
                    continue;
                if (!FP::equal(compare.getEnd(), check.getEnd()))
                    continue;
                if (compare.getValue() != check.getValue())
                    continue;
                output.erase(output.begin() + idx);
                break;
            }
            if (idx < 0)
                return false;
        }

        return output.empty();
    }
};

class TestAction : public QObject {
Q_OBJECT
private slots:

    void noop()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionNOOP;
        Dispatch d;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(a, 7.0, 700, 800);
        d.value(b, 8.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(4.0), 200, 300),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(6.0), 600, 700),
                                                  SequenceValue(a, Variant::Root(7.0), 700, 800),
                                                  SequenceValue(b, Variant::Root(8.0), 800, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(a, 7.0, 700, 800);
        d.value(b, 8.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(4.0), 200, 300),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(6.0), 600, 700),
                                                  SequenceValue(a, Variant::Root(7.0), 700, 800),
                                                  SequenceValue(b, Variant::Root(8.0), 800, 900)}));

        delete act;
    }

    void invalidatePreserve()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionInvalidate(SequenceMatch::Composite(a), true);
        Dispatch d;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(a, 7.0, 700, 800);
        d.value(b, 8.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(FP::undefined()), 100, 200),
                                        SequenceValue(b, Variant::Root(3.0), 100, 200),
                                        SequenceValue(a, Variant::Root(FP::undefined()), 200, 300),
                                        SequenceValue(b, Variant::Root(5.0), 300, 400),
                                        SequenceValue(a, Variant::Root(FP::undefined()), 600, 700),
                                        SequenceValue(a, Variant::Root(FP::undefined()), 700, 800),
                                        SequenceValue(b, Variant::Root(8.0), 800, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(a, 7.0, 700, 800);
        d.value(b, 8.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(FP::undefined()), 100, 200),
                                        SequenceValue(b, Variant::Root(3.0), 100, 200),
                                        SequenceValue(a, Variant::Root(FP::undefined()), 200, 300),
                                        SequenceValue(b, Variant::Root(5.0), 300, 400),
                                        SequenceValue(a, Variant::Root(FP::undefined()), 600, 700),
                                        SequenceValue(a, Variant::Root(FP::undefined()), 700, 800),
                                        SequenceValue(b, Variant::Root(8.0), 800, 900)}));

        delete act;
    }

    void invalidateFull()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionInvalidate(SequenceMatch::Composite(a), false);
        Dispatch d;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(a, 7.0, 700, 800);
        d.value(b, 8.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(), 100, 200),
                                                  SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(), 200, 300),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(), 600, 700),
                                                  SequenceValue(a, Variant::Root(), 700, 800),
                                                  SequenceValue(b, Variant::Root(8.0), 800, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(a, 7.0, 700, 800);
        d.value(b, 8.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(), 100, 200),
                                                  SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(), 200, 300),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(), 600, 700),
                                                  SequenceValue(a, Variant::Root(), 700, 800),
                                                  SequenceValue(b, Variant::Root(8.0), 800, 900)}));

        delete act;
    }

    void remove()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionRemove(SequenceMatch::Composite(a));
        Dispatch d;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(a, 7.0, 700, 800);
        d.value(b, 8.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(b, Variant::Root(8.0), 800, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(a, 7.0, 700, 800);
        d.value(b, 8.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(b, Variant::Root(8.0), 800, 900)}));

        delete act;
    }

    void wrapModular()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionWrapModular(SequenceMatch::Composite(a), 0.0, 4.0);
        Dispatch d;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(a, 7.0, 700, 800);
        d.value(b, 8.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(0.0), 200, 300),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(2.0), 600, 700),
                                                  SequenceValue(a, Variant::Root(3.0), 700, 800),
                                                  SequenceValue(b, Variant::Root(8.0), 800, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(a, 7.0, 700, 800);
        d.value(b, 8.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(0.0), 200, 300),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(2.0), 600, 700),
                                                  SequenceValue(a, Variant::Root(3.0), 700, 800),
                                                  SequenceValue(b, Variant::Root(8.0), 800, 900)}));

        delete act;
    }

    void overlay()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionOverlayValue(SequenceMatch::Composite(a), Variant::Root(-1.0), "A");
        Dispatch d;

        Variant::Root input;
        input.write().hash("A").setDouble(1.0);
        input.write().hash("B").setDouble(2.0);

        Variant::Root output;
        output.write().hash("A").setDouble(-1.0);
        output.write().hash("B").setDouble(2.0);

        d.initialize(act);

        d.value(a, input, 100, 200);
        d.value(b, Variant::Root(1.0), 100, 200);
        d.advance(100);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, output, 100, 200),
                                                  SequenceValue(b, Variant::Root(1.0), 100, 200)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, input, 100, 200);
        d.value(b, Variant::Root(1.0), 100, 200);
        d.advance(100);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, output, 100, 200),
                                                  SequenceValue(b, Variant::Root(1.0), 100, 200)}));

        delete act;
    }

    void overlayMeta()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act =
                new ActionOverlayMetadata(SequenceMatch::Composite(a), Variant::Root(-1.0), "A");
        Dispatch d;

        Variant::Root input;
        input.write().hash("A").setDouble(1.0);
        input.write().hash("B").setDouble(2.0);

        Variant::Root output;
        output.write().hash("A").setDouble(-1.0);
        output.write().hash("B").setDouble(2.0);

        d.initialize(act);

        d.value(a.toMeta(), input, 100, 200);
        d.value(b.toMeta(), Variant::Root(1.0), 100, 200);
        d.value(b, Variant::Root(2.0), 100, 200);
        d.value(a, Variant::Root(3.0), 100, 200);
        d.advance(100);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a.toMeta(), output, 100, 200),
                                                  SequenceValue(b.toMeta(), Variant::Root(1.0), 100,
                                                                200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(3.0), 100, 200)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a.toMeta(), input, 100, 200);
        d.value(b.toMeta(), Variant::Root(1.0), 100, 200);
        d.value(b, Variant::Root(2.0), 100, 200);
        d.value(a, Variant::Root(3.0), 100, 200);
        d.advance(100);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a.toMeta(), output, 100, 200),
                                                  SequenceValue(b.toMeta(), Variant::Root(1.0), 100,
                                                                200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(3.0), 100, 200)}));

        delete act;
    }
};

QTEST_MAIN(TestAction)

#include "action.moc"
