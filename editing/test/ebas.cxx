/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/ebas.hxx"
#include "datacore/stream.hxx"
#include "datacore/staticmultiplexer.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

class TestEBAS : public QObject {
Q_OBJECT
private slots:

    void description()
    {
        QVERIFY(EBAS::description(990).read().exists());
        QVERIFY(EBAS::description("EBASFlag470").read().exists());
    }
};

QTEST_MAIN(TestEBAS)

#include "ebas.moc"
