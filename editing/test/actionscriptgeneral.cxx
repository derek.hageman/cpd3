/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/actionscriptgeneral.hxx"
#include "datacore/stream.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

class Dispatch : public EditDispatchTarget {
    Action *action;

    struct Target {
        QList<EditDataTarget *> targets;
        QList<EditDataModification *> modifiers;
        int muxId;

        Target() : targets(), modifiers(), muxId(0)
        { }
    };

    QHash<SequenceName, Target> d;

    StaticMultiplexer outputMux;

    void handleCompleted(const SequenceValue::Transfer &add)
    {
        Util::append(add, output);
    }

    std::mutex mutex;
public:
    SequenceValue::Transfer output;

    Dispatch() : action(NULL), d(), outputMux(), mutex()
    { }

    virtual ~Dispatch()
    { }

    void initialize(Action *a)
    {
        output.clear();
        d.clear();
        action = a;

        outputMux.clear(true);
        outputMux.setStreams(2);

        action->setTarget(this);
        action->initialize();
    }

    void value(SequenceValue value)
    {
        QHash<SequenceName, Target>::iterator target = d.find(value.getUnit());
        if (target == d.end()) {
            QList<EditDataTarget *> inputs;
            QList<EditDataTarget *> outputs;
            QList<EditDataModification *> modifiers;

            action->unhandledInput(value.getUnit(), inputs, outputs, modifiers);

            Target add;
            add.targets = inputs;
            add.targets.append(outputs);
            add.modifiers = modifiers;

            if (outputs.isEmpty()) {
                add.muxId = 0;
            } else {
                add.muxId = -1;
            }

            target = d.insert(value.getUnit(), add);
        }

        for (QList<EditDataModification *>::const_iterator
                m = target.value().modifiers.constBegin(),
                end = target.value().modifiers.constEnd(); m != end; ++m) {
            (*m)->modifySequenceValue(value);
        }
        for (QList<EditDataTarget *>::const_iterator t = target.value().targets.constBegin(),
                end = target.value().targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(value);
        }
        if (target.value().muxId >= 0) {
            std::lock_guard<std::mutex> lock(mutex);
            handleCompleted(outputMux.incoming(target.value().muxId, value));
        }
    }

    void value(const SequenceName &unit, const Variant::Read &v, double start, double end)
    { value(SequenceValue(unit, Variant::Root(v), start, end)); }

    void value(const SequenceName &unit, double v, double start, double end)
    { value(SequenceValue(unit, Variant::Root(v), start, end)); }

    void advance(double time)
    {
        action->incomingDataAdvance(time);
        handleCompleted(outputMux.advance(0, time));
    }

    void finish()
    {
        action->finalize();
        std::lock_guard<std::mutex> lock(mutex);
        handleCompleted(outputMux.finish());
    }

    void incomingData(const Data::SequenceValue &value) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        handleCompleted(outputMux.incoming(1, value));
    }

    void incomingData(const Data::SequenceValue::Transfer &values) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        handleCompleted(outputMux.incoming(1, values));
    }

    void incomingAdvance(double time) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        handleCompleted(outputMux.advance(1, time));
    }

    bool compare(const SequenceValue::Transfer &expected)
    {
        if (expected.size() != output.size())
            return false;

        for (const auto &check : expected) {
            int idx;
            for (idx = output.size() - 1; idx >= 0; --idx) {
                SequenceValue compare(output.at(idx));
                if (compare.getUnit() != check.getUnit())
                    continue;
                if (!FP::equal(compare.getStart(), check.getStart()))
                    continue;
                if (!FP::equal(compare.getEnd(), check.getEnd()))
                    continue;
                if (compare.getValue() != check.getValue())
                    continue;
                output.erase(output.begin() + idx);
                break;
            }
            if (idx < 0)
                return false;
        }

        return output.empty();
    }
};

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestActionScriptGeneral : public QObject {
Q_OBJECT

    static bool contains(const SequenceValue::Transfer &d, const SequenceValue &v)
    { return std::find(d.begin(), d.end(), v) != d.end(); }

private slots:

    void sequenceSegment()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionScriptSequenceSegmentGeneral(SequenceMatch::OrderedLookup(a),
                                                             SequenceMatch::OrderedLookup(), R"EOF(
for val in data do
    val.a = val.a + 0.5;
end
)EOF");
        Dispatch d;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 700, 800);
        d.value(a, 8.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(4.5), 200, 300),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(6.5), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 700, 800),
                                                  SequenceValue(a, Variant::Root(8.5), 800, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);
        QTest::qSleep(50);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);
        QTest::qSleep(50);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);
        QTest::qSleep(50);

        d.advance(600);
        QTest::qSleep(50);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 700, 800);
        d.value(a, 8.0, 800, 900);
        QTest::qSleep(100);

        d.finish();
        QTest::qSleep(50);

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(4.5), 200, 300),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(6.5), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 700, 800),
                                                  SequenceValue(a, Variant::Root(8.5), 800, 900)}));

        delete act;
    }

    void values()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionScriptSequenceValueGeneral(SequenceMatch::OrderedLookup(a), R"EOF(
for val in data do
    val.value = val.value + 0.5;
end
)EOF");
        Dispatch d;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 700, 800);
        d.value(a, 8.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(4.5), 200, 300),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(6.5), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 700, 800),
                                                  SequenceValue(a, Variant::Root(8.5), 800, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);
        QTest::qSleep(50);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);
        QTest::qSleep(50);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);
        QTest::qSleep(50);

        d.advance(600);
        QTest::qSleep(50);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 700, 800);
        d.value(a, 8.0, 800, 900);
        QTest::qSleep(50);

        d.finish();
        QTest::qSleep(50);

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(4.5), 200, 300),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(6.5), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 700, 800),
                                                  SequenceValue(a, Variant::Root(8.5), 800, 900)}));

        delete act;
    }

    void valuesRelease()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionScriptSequenceValueGeneral(SequenceMatch::OrderedLookup(a), R"EOF(
data.autorelease = 10;
for val in data do
    val.start = val.start - 10.0;
end
)EOF");
        Dispatch d;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 700, 800);
        d.value(a, 8.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.0), 90, 200),
                                                  SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(4.0), 190, 300),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(6.0), 590, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 700, 800),
                                                  SequenceValue(a, Variant::Root(8.0), 790, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);
        QTest::qSleep(50);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);
        QTest::qSleep(50);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);
        QTest::qSleep(50);

        d.advance(600);
        QTest::qSleep(50);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 700, 800);
        d.value(a, 8.0, 800, 900);
        QTest::qSleep(50);

        d.finish();
        QTest::qSleep(50);

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.0), 90, 200),
                                                  SequenceValue(b, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(4.0), 190, 300),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(6.0), 590, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 700, 800),
                                                  SequenceValue(a, Variant::Root(8.0), 790, 900)}));

        delete act;
    }

    void sequenceSegmentError()
    {
        Logging::suppressGlobalForTesting();

        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionScriptSequenceSegmentGeneral(SequenceMatch::OrderedLookup(a),
                                                             SequenceMatch::OrderedLookup(), R"EOF(
for val in data do
    val.a = val.a + 0.5;
    error();
end
)EOF");
        Dispatch d;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 700, 800);
        d.value(a, 8.0, 800, 900);

        d.finish();
        delete act;

        QVERIFY(contains(d.output, SequenceValue(b, Variant::Root(3.0), 100, 200)));
        QVERIFY(contains(d.output, SequenceValue(b, Variant::Root(5.0), 300, 400)));
        QVERIFY(contains(d.output, SequenceValue(b, Variant::Root(7.0), 700, 800)));
    }

    void valuesError()
    {
        Logging::suppressGlobalForTesting();

        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionScriptSequenceValueGeneral(SequenceMatch::OrderedLookup(a), R"EOF(
for val in data do
    val.value = val.value + 0.5;
    error();
end
)EOF");
        Dispatch d;

        d.initialize(act);

        d.value(a, 2.0, 100, 200);
        d.value(b, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(b, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 700, 800);
        d.value(a, 8.0, 800, 900);

        d.finish();
        delete act;

        QVERIFY(contains(d.output, SequenceValue(b, Variant::Root(3.0), 100, 200)));
        QVERIFY(contains(d.output, SequenceValue(b, Variant::Root(5.0), 300, 400)));
        QVERIFY(contains(d.output, SequenceValue(b, Variant::Root(7.0), 700, 800)));
    }
};

QTEST_MAIN(TestActionScriptGeneral)

#include "actionscriptgeneral.moc"
