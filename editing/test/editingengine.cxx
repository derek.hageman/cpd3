/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>
#include <QTemporaryFile>

#include "editing/editingengine.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

class TestTarget : public StreamSink {
public:
    SequenceValue::Transfer result;
    bool ended;

    TestTarget() : result(), ended(false)
    { }

    virtual ~TestTarget()
    { }

    void incomingData(const SequenceValue::Transfer &values) override
    {
        Q_ASSERT(!ended);
        Util::append(values, result);
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        Q_ASSERT(!ended);
        Util::append(std::move(values), result);
    }

    void incomingData(const SequenceValue &value) override
    {
        Q_ASSERT(!ended);
        result.emplace_back(value);
    }

    void incomingData(SequenceValue &&value) override
    {
        Q_ASSERT(!ended);
        result.emplace_back(std::move(value));
    }

    virtual void endData()
    {
        ended = true;
    }

    bool compare(const SequenceValue::Transfer &expected)
    {
        if (expected.size() != result.size())
            return false;

        for (const auto &check : expected) {
            int idx;
            for (idx = result.size() - 1; idx >= 0; --idx) {
                SequenceValue compare(result.at(idx));
                if (compare.getUnit() != check.getUnit())
                    continue;
                if (!FP::equal(compare.getStart(), check.getStart()))
                    continue;
                if (!FP::equal(compare.getEnd(), check.getEnd()))
                    continue;
                if (!compare.getValue().exists()) {
                    if (check.getValue().exists())
                        continue;
                } else {
                    if (compare.getValue() != check.getValue())
                        continue;
                }
                result.erase(result.begin() + idx);
                break;
            }
            if (idx < 0) {
                qDebug() << "Failed to match" << check;
                return false;
            }
        }

        if (!result.empty()) {
            qDebug() << "Unmatched values" << result;
            return false;
        }
        return true;
    }
};

class TestEditingEngine : public QObject {
Q_OBJECT

    typedef SequenceValue DV;
    typedef SequenceName DVU;

    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void empty()
    {
        EditingEngine engine(1356998400, 1388534400, "brw");
        TestTarget target;
        engine.start();
        engine.setEgress(&target);
        QVERIFY(engine.wait(30000));

        QVERIFY(target.ended);
        QVERIFY(target.result.empty());
    }

    void basic()
    {
        SequenceValue::Transfer values;
        double den = ((900 / 1013.25) * (273.15 / (25.0 + 273.15)));
        double expected = (10.0 / den);
        {
            SequenceValue::Transfer add;
            {
                Variant::Root v;

                v["Profiles/aerosol/Output/Data"] = "BsG_S11";
                v["Profiles/aerosol/Filters/1000/Component"] = "corr_stp";
                v["Profiles/aerosol/Filters/1000/Parameters/S11_raw/CorrectConcentrations"] =
                        ":raw:BsG_S11:-cover:-stats";
                v["Profiles/aerosol/Filters/1000/Parameters/S11_raw/Flags"] =
                        ":raw:F1_S11:-cover:-stats";
                v["Profiles/aerosol/Filters/1000/Parameters/S11_raw/SampleT"] =
                        ":raw:T_S11:-cover:-stats";
                v["Profiles/aerosol/Filters/1000/Parameters/S11_raw/SampleP"] =
                        ":raw:P_S11:-cover:-stats";
                v["Profiles/aerosol/Filters/1000/Parameters/S11_raw/StandardT"] = 273.15;
                v["Profiles/aerosol/Filters/1000/Parameters/S11_raw/StandardP"] = 1013.25;
                v["Profiles/aerosol/Filters/1000/Parameters/S11_cont/CorrectConcentrations"] =
                        ":cont:BsG_S11:-cover:-stats";
                v["Profiles/aerosol/Filters/1000/Parameters/S11_cont/Flags"] =
                        ":cont:F1_S11:-cover:-stats";
                v["Profiles/aerosol/Filters/1000/Parameters/S11_cont/SampleT"] =
                        ":cont:T_S11:-cover:-stats";
                v["Profiles/aerosol/Filters/1000/Parameters/S11_cont/SampleP"] =
                        ":cont:P_S11:-cover:-stats";
                v["Profiles/aerosol/Filters/1000/Parameters/S11_cont/StandardT"] = 273.15;
                v["Profiles/aerosol/Filters/1000/Parameters/S11_cont/StandardP"] = 1013.25;
                v["Profiles/aerosol/Filters/1000/Parameters/S11_avg/CorrectConcentrations"] =
                        ":avg:BsG_S11:-cover:-stats";
                v["Profiles/aerosol/Filters/1000/Parameters/S11_avg/Flags"] =
                        ":avg:F1_S11:-cover:-stats";
                v["Profiles/aerosol/Filters/1000/Parameters/S11_avg/SampleT"] =
                        ":avg:T_S11:-cover:-stats";
                v["Profiles/aerosol/Filters/1000/Parameters/S11_avg/SampleP"] =
                        ":avg:P_S11:-cover:-stats";
                v["Profiles/aerosol/Filters/1000/Parameters/S11_avg/StandardT"] = 273.15;
                v["Profiles/aerosol/Filters/1000/Parameters/S11_avg/StandardP"] = 1013.25;
                add.emplace_back(DV(DVU("brw", "configuration", "editing"), v, FP::undefined(),
                                    FP::undefined()));

                v.write().setEmpty();
                v["Parameters/Trigger/Type"] = "Always";
                v["Parameters/Action/Type"] = "Invalidate";
                v["Parameters/Action/Selection"] = "BsG_S11";
                add.emplace_back(DV(DVU("brw", "edits", "aerosol"), v, 1356998400 + 3600,
                                    1356998400 + 3600 * 2));
            }

            for (double start = 1356998400; start < 1356998400 + 86400; start += 60) {
                double end = start + 60;

                values.emplace_back(
                        DV(DVU("brw", "raw", "BsG_S11"), Variant::Root(10.0), start, end));
                add.emplace_back(DV(DVU("brw", "raw", "T_S11"), Variant::Root(25.0), start, end));
                add.emplace_back(DV(DVU("brw", "raw", "P_S11"), Variant::Root(900.0), start, end));
                add.emplace_back(
                        DV(DVU("brw", "raw", "F1_S11"), Variant::Root(Variant::Flags()), start,
                           end));
            }
            Util::append(values, add);

            Archive::Access(databaseFile).writeSynchronous(add);

            /* Since we're not setting the flavors, the cover, etc will be
             * excluded */
            for (auto &v : values) {
                v.setUnit(DVU("brw", "clean", "BsG_S11"));
                if (Range::intersects(v.getStart(), v.getEnd(), 1356998400 + 3600,
                                      1356998400 + 3600 * 2)) {
                    v.write().setDouble(FP::undefined());
                    continue;
                }
                v.write().setDouble(expected);
            }
            for (double start = 1356998400; start < 1356998400 + 86400; start += 3600) {
                double end = start + 3600;
                double v = expected;
                if (Range::intersects(start, end, 1356998400 + 3600, 1356998400 + 3600 * 2)) {
                    v = FP::undefined();
                }
                values.emplace_back(
                        DV(DVU("brw", "avgh", "BsG_S11"), Variant::Root(v), start, end));
            }
        }

        EditingEngine engine(1356998400, 1388534400, "brw");
        TestTarget target;
        engine.start();
        engine.setEgress(&target);
        QVERIFY(engine.wait(30000));

        QVERIFY(target.ended);
        QCOMPARE((int) target.result.size(), 24 * 60 + 24);
        QVERIFY(target.compare(values));
    }

    void editFinal()
    {
        {
            SequenceValue::Transfer add;
            Variant::Root v;

            v["Profiles/aerosol/Output/Data"] = "BsG_S11";
            v["Profiles/aerosol/Filters/1000/Component"] = "corr_stp";
            v["Profiles/aerosol/Filters/1000/Parameters/S11_raw/CorrectConcentrations"] =
                    ":raw:BsG_S11:-cover:-stats";
            v["Profiles/aerosol/Filters/1000/Parameters/S11_raw/Flags"] =
                    ":raw:F1_S11:-cover:-stats";
            v["Profiles/aerosol/Filters/1000/Parameters/S11_raw/SampleT"] =
                    ":raw:T_S11:-cover:-stats";
            v["Profiles/aerosol/Filters/1000/Parameters/S11_raw/SampleP"] =
                    ":raw:P_S11:-cover:-stats";
            v["Profiles/aerosol/Filters/1000/Parameters/S11_raw/StandardT"] = 273.15;
            v["Profiles/aerosol/Filters/1000/Parameters/S11_raw/StandardP"] = 1013.25;
            v["Profiles/aerosol/Filters/1000/Parameters/S11_cont/CorrectConcentrations"] =
                    ":cont:BsG_S11:-cover:-stats";
            v["Profiles/aerosol/Filters/1000/Parameters/S11_cont/Flags"] =
                    ":cont:F1_S11:-cover:-stats";
            v["Profiles/aerosol/Filters/1000/Parameters/S11_cont/SampleT"] =
                    ":cont:T_S11:-cover:-stats";
            v["Profiles/aerosol/Filters/1000/Parameters/S11_cont/SampleP"] =
                    ":cont:P_S11:-cover:-stats";
            v["Profiles/aerosol/Filters/1000/Parameters/S11_cont/StandardT"] = 273.15;
            v["Profiles/aerosol/Filters/1000/Parameters/S11_cont/StandardP"] = 1013.25;
            v["Profiles/aerosol/Filters/1000/Parameters/S11_avg/CorrectConcentrations"] =
                    ":avg:BsG_S11:-cover:-stats";
            v["Profiles/aerosol/Filters/1000/Parameters/S11_avg/Flags"] =
                    ":avg:F1_S11:-cover:-stats";
            v["Profiles/aerosol/Filters/1000/Parameters/S11_avg/SampleT"] =
                    ":avg:T_S11:-cover:-stats";
            v["Profiles/aerosol/Filters/1000/Parameters/S11_avg/SampleP"] =
                    ":avg:P_S11:-cover:-stats";
            v["Profiles/aerosol/Filters/1000/Parameters/S11_avg/StandardT"] = 273.15;
            v["Profiles/aerosol/Filters/1000/Parameters/S11_avg/StandardP"] = 1013.25;
            v["Profiles/aerosol/Filters/1000/Average/Full"] = "F1_S11";
            v["Profiles/aerosol/Filters/1000/Average/Continuous"] = "P_S11";
            add.emplace_back(DV(DVU("brw", "configuration", "editing"), v, FP::undefined(),
                                FP::undefined()));

            v.write().setEmpty();
            v["Parameters/Trigger/Type"] = "Always";
            v["Parameters/Action/Type"] = "Remove";
            v["Parameters/Action/Selection"] = "BsG_S11";
            v["Priority"].setInt64(2000);
            add.emplace_back(DV(DVU("brw", "edits", "aerosol"), v, 1356998400 + 3600,
                                1356998400 + 3600 * 2));

            for (double start = 1356998400; start < 1356998400 + 86400; start += 60) {
                double end = start + 60;

                add.emplace_back(DV(DVU("brw", "raw", "BsG_S11"), Variant::Root(10.0), start, end));
                add.emplace_back(DV(DVU("brw", "raw", "T_S11"), Variant::Root(25.0), start, end));
                add.emplace_back(DV(DVU("brw", "raw", "P_S11"), Variant::Root(900.0), start, end));
                add.emplace_back(
                        DV(DVU("brw", "raw", "F1_S11"), Variant::Root(Variant::Flags()), start,
                           end));
            }

            Archive::Access(databaseFile).writeSynchronous(add);
        }

        EditingEngine engine(1356998400, 1388534400, "brw");
        TestTarget target;
        engine.start();
        engine.setEgress(&target);
        QVERIFY(engine.wait(30000));

        QVERIFY(target.ended);
        QCOMPARE((int) target.result.size(), 23 * 60 + 23);
    }

    void contaminationRemove()
    {
        SequenceValue::Transfer values;
        {
            SequenceValue::Transfer add;
            {
                Variant::Root v;

                v["Profiles/aerosol/Output/Data"] = "(BsG|F1)_S11";
                v["Profiles/aerosol/RequiredCoverage"] = 0.5;
                add.emplace_back(DV(DVU("brw", "configuration", "editing"), v, FP::undefined(),
                                    FP::undefined()));

                v.write().setEmpty();
                v["Affected/aerosol"] = "BsG_S11";
                add.emplace_back(
                        DV(DVU("brw", "configuration", "contamination"), v, FP::undefined(),
                           FP::undefined()));
            }

            for (double start = 1356998400; start < 1356998400 + 86400; start += 60) {
                double end = start + 60;

                values.emplace_back(
                        DV(DVU("brw", "raw", "BsG_S11"), Variant::Root(10.0), start, end));

                Variant::Flags flags;
                if (start >= 1356998400 + 86400 / 2) {
                    if ((int) start % 3600 >= 300)
                        flags.insert("Contaminated");
                }
                values.emplace_back(
                        DV(DVU("brw", "raw", "F1_S11"), Variant::Root(flags), start, end));
            }
            Util::append(values, add);

            Archive::Access(databaseFile).writeSynchronous(add);

            /* Since we're not setting the flavors, the cover, etc will be
             * excluded */
            for (auto &v : values) {
                v.setArchive("clean");
            }
            for (double start = 1356998400; start < 1356998400 + 86400; start += 3600) {
                double end = start + 3600;
                double v = 10.0;
                Variant::Flags flags;
                if (start >= 1356998400 + 86400 / 2) {
                    v = FP::undefined();
                    flags.insert("Contaminated");
                }
                values.emplace_back(
                        DV(DVU("brw", "avgh", "BsG_S11"), Variant::Root(v), start, end));
                values.emplace_back(
                        DV(DVU("brw", "avgh", "F1_S11"), Variant::Root(flags), start, end));
            }
        }

        EditingEngine engine(1356998400, 1388534400, "brw");
        TestTarget target;
        engine.start();
        engine.setEgress(&target);
        QVERIFY(engine.wait(30000));

        QVERIFY(target.ended);
        QCOMPARE((int) target.result.size(), 24 * 60 * 2 + 24 * 2);
        QVERIFY(target.compare(values));
    }

    void purge()
    {
        Archive::Access access(databaseFile);
        {
            SequenceValue::Transfer add;
            {
                Variant::Root v;

                v["Profiles/aerosol/Output/Data"] = "(BsG|F1)_S11";
                add.emplace_back(
                        DV(DVU("brw", "configuration", "editing"), v, FP::undefined(), 5000));

                v["Profiles/aerosol/Output/Data"] = "BsG_S11";
                add.emplace_back(
                        DV(DVU("brw", "configuration", "editing"), v, 5000, FP::undefined()));
            }

            add.emplace_back(
                    DV(DVU("brw", "clean", "F1_S11"), Variant::Root(Variant::Flags()), 1000, 8000));
            add.emplace_back(DV(DVU("brw", "clean", "BsG_S11"), Variant::Root(1.0), 1000, 8000));
            add.emplace_back(DV(DVU("brw", "clean", "BsG_S11"), Variant::Root(2.0), 1100, 3000));
            add.emplace_back(
                    DV(DVU("brw", "clean", "F1_S11"), Variant::Root(Variant::Flags()), 6000, 7000));
            add.emplace_back(DV(DVU("brw", "clean", "BsG_S11"), Variant::Root(3.0), 6000, 7000));
            add.emplace_back(DV(DVU("brw", "clean", "BsG_S11"), Variant::Root(4.0), 6100, 8100));

            add.emplace_back(
                    DV(DVU("brw", "raw", "F1_S11"), Variant::Root(Variant::Flags()), 3000, 4000));
            add.emplace_back(DV(DVU("brw", "raw", "BsG_S11"), Variant::Root(0.0), 3000, 4000));

            access.writeSynchronous(add);
        }

        TestTarget target;
        for (;;) {
            Archive::Access::WriteLock lock(access);
            target.result.clear();
            target.ended = false;
            EditingEngine engine(2000, 7500, "brw", "aerosol", &access);
            engine.start();
            engine.purgeArchive();
            engine.setEgress(&target);
            QVERIFY(engine.wait(30000));
            if (lock.commit())
                break;
        }

        {
            SequenceValue::Transfer values;
            values.emplace_back(
                    DV(DVU("brw", "clean", "F1_S11"), Variant::Root(Variant::Flags()), 3000, 4000));
            values.emplace_back(DV(DVU("brw", "clean", "BsG_S11"), Variant::Root(0.0), 3000, 4000));

            values.emplace_back(
                    DV(DVU("brw", "avgh", "F1_S11"), Variant::Root(Variant::Flags()), 2000, 3600));
            values.emplace_back(DV(DVU("brw", "avgh", "BsG_S11"), Variant::Root(0.0), 2000, 3600));
            values.emplace_back(
                    DV(DVU("brw", "avgh", "F1_S11"), Variant::Root(Variant::Flags()), 3600, 7200));
            values.emplace_back(DV(DVU("brw", "avgh", "BsG_S11"), Variant::Root(0.0), 3600, 7200));

            QVERIFY(target.ended);
            QVERIFY(target.compare(values));
        }

        target.result = access.readSynchronous(
                Archive::Selection{FP::undefined(), FP::undefined(), {"brw"}, {"clean"}});
        {
            SequenceValue::Transfer values;
            values.emplace_back(
                    DV(DVU("brw", "clean", "F1_S11"), Variant::Root(Variant::Flags()), 1000, 2000));
            values.emplace_back(DV(DVU("brw", "clean", "BsG_S11"), Variant::Root(1.0), 1000, 2000));
            values.emplace_back(DV(DVU("brw", "clean", "BsG_S11"), Variant::Root(2.0), 1100, 2000));
            values.emplace_back(
                    DV(DVU("brw", "clean", "F1_S11"), Variant::Root(Variant::Flags()), 5000, 8000));
            values.emplace_back(
                    DV(DVU("brw", "clean", "F1_S11"), Variant::Root(Variant::Flags()), 6000, 7000));
            values.emplace_back(DV(DVU("brw", "clean", "BsG_S11"), Variant::Root(4.0), 7500, 8100));
            values.emplace_back(DV(DVU("brw", "clean", "BsG_S11"), Variant::Root(1.0), 7500, 8000));

            QVERIFY(target.compare(values));
        }
    }
};

QTEST_MAIN(TestEditingEngine)

#include "editingengine.moc"
