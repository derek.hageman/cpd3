/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/triggervaluegeneric.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

static void sendValue(Trigger *input,
                      QHash<SequenceName, QList<EditDataTarget *> > &dispatch,
                      const SequenceName &unit, const Variant::Root &value,
                      double start,
                      double end)
{
    QHash<SequenceName, QList<EditDataTarget *> >::iterator target = dispatch.find(unit);
    if (target == dispatch.end()) {
        target = dispatch.insert(unit, input->unhandledInput(unit));
    }
    for (QList<EditDataTarget *>::const_iterator t = target.value().constBegin(),
            endT = target.value().constEnd(); t != endT; ++t) {
        (*t)->incomingSequenceValue(SequenceValue(unit, value, start, end));
    }
}

class TestTriggerValueGeneric : public QObject {
Q_OBJECT
private slots:

    void hasFlags()
    {
        SequenceName u("brw", "raw", "1");
        Trigger *i = new TriggerHasFlags(Variant::Flags{"TF"}, SequenceMatch::OrderedLookup(u));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u, Variant::Root(Variant::Flags()), 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u, Variant::Root(Variant::Flags{"TF"}), 200, 400);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));

        sendValue(i, d, u, Variant::Root(Variant::Flags{"FF"}), 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(i->appliesTo(200, 300));
        i->advance(200);
        QVERIFY(i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u, Variant::Root(Variant::Flags()), 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u, Variant::Root(Variant::Flags{"TF"}), 200, 400);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));

        sendValue(i, d, u, Variant::Root(Variant::Flags{"FF"}), 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(i->appliesTo(200, 300));
        i->advance(200);
        QVERIFY(i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));

        delete i;
    }

    void lacksFlags()
    {
        SequenceName u("brw", "raw", "1");
        Trigger *i = new TriggerLacksFlags(Variant::Flags{"TF"}, SequenceMatch::OrderedLookup(u));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u, Variant::Root(Variant::Flags()), 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u, Variant::Root(Variant::Flags{"TF"}), 200, 400);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));

        sendValue(i, d, u, Variant::Root(Variant::Flags{"FF"}), 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));
        i->advance(200);
        QVERIFY(!i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u, Variant::Root(Variant::Flags()), 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u, Variant::Root(Variant::Flags{"TF"}), 200, 400);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(i->appliesTo(100, 200));

        sendValue(i, d, u, Variant::Root(Variant::Flags{"FF"}), 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));
        i->advance(200);
        QVERIFY(!i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));

        delete i;
    }

    void exists()
    {
        SequenceName u("brw", "raw", "1");
        Trigger *i = new TriggerExists(SequenceMatch::OrderedLookup(u));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u, Variant::Root(Variant::Flags()), 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u, Variant::Root(), 200, 400);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));

        sendValue(i, d, u, Variant::Root(FP::undefined()), 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));
        i->advance(200);
        QVERIFY(!i->appliesTo(200, 300));

        sendValue(i, d, u, Variant::Root(3.0), 500, 600);
        i->incomingDataAdvance(600);
        QVERIFY(i->isReady(600));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));
        QVERIFY(!i->appliesTo(400, 500));

        i->finalize();
        QVERIFY(!i->appliesTo(400, 500));
        QVERIFY(i->appliesTo(500, 600));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u, Variant::Root(Variant::Flags()), 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u, Variant::Root(), 200, 400);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));

        sendValue(i, d, u, Variant::Root(FP::undefined()), 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));
        i->advance(200);
        QVERIFY(!i->appliesTo(200, 300));

        sendValue(i, d, u, Variant::Root(3.0), 500, 600);
        i->incomingDataAdvance(600);
        QVERIFY(i->isReady(600));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));
        QVERIFY(!i->appliesTo(400, 500));

        i->finalize();
        QVERIFY(!i->appliesTo(400, 500));
        QVERIFY(i->appliesTo(500, 600));

        delete i;
    }
};

QTEST_MAIN(TestTriggerValueGeneric)

#include "triggervaluegeneric.moc"
