/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/directivedispatch.hxx"
#include "editing/editdirective.hxx"
#include "editing/action.hxx"
#include "editing/trigger.hxx"
#include "datacore/stream.hxx"
#include "datacore/staticmultiplexer.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

class Target : public EditDispatchSkipTarget {
    StaticMultiplexer mux;
    QHash<void *, int> lookup;

    int toStream(void *origin)
    {
        int target = lookup.value(origin, -1);
        if (target > 0)
            return target;
        target = mux.addStream();
        lookup.insert(origin, target);
        return target;
    }

public:
    SequenceValue::Transfer output;

    Target() : mux(1), lookup(), output()
    { }

    virtual ~Target()
    { }

    void incomingData(const Data::SequenceValue &value) override
    { Util::append(mux.incoming(0, value), output); }

    void incomingData(const Data::SequenceValue::Transfer &values) override
    { Util::append(mux.incoming(0, values), output); }

    void incomingAdvance(double time) override
    { Util::append(mux.advance(0, time), output); }

    void incomingSkipped(const Data::SequenceValue &value, void *origin) override
    { Util::append(mux.incoming(toStream(origin), value), output); }

    void incomingSkipped(const Data::SequenceValue::Transfer &values, void *origin) override
    { Util::append(mux.incoming(toStream(origin), values), output); }

    void incomingSkipAdvance(double time, void *origin) override
    {
        int target = lookup.value(origin, -1);
        if (target <= 0)
            return;
        Util::append(mux.advance(target, time), output);
    }

    void incomingSkipEnd(void *origin) override
    {
        int target = lookup.value(origin, -1);
        if (target <= 0)
            return;
        Util::append(mux.finish(target), output);
    }

    void finish()
    { Util::append(mux.finish(), output); }

    bool compare(const SequenceValue::Transfer &expected)
    {
        if (expected.size() != output.size())
            return false;

        for (const auto &check : expected) {
            int idx;
            for (idx = output.size() - 1; idx >= 0; --idx) {
                SequenceValue compare(output.at(idx));
                if (compare.getUnit() != check.getUnit())
                    continue;
                if (!FP::equal(compare.getStart(), check.getStart()))
                    continue;
                if (!FP::equal(compare.getEnd(), check.getEnd()))
                    continue;
                if (compare.getValue() != check.getValue())
                    continue;
                output.erase(output.begin() + idx);
                break;
            }
            if (idx < 0)
                return false;
        }

        return output.empty();
    }
};

class TestDirectiveDispatch : public QObject {
Q_OBJECT

private slots:

    void basic()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");

        Target target;
        std::vector<EditDirective *> directives;
        directives.insert(directives.begin(), new EditDirective(1000, 2000, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                a))));
        directives.front()->setOutput(&target);
        DirectiveDispatch dispatch(directives, &target);

        dispatch.incomingData(SequenceValue(a, Variant::Root(1.0), 100, 200));
        dispatch.incomingData(SequenceValue(a, Variant::Root(2.0), 200, 300));
        dispatch.incomingData(SequenceValue(b, Variant::Root(3.0), 200, 300));
        dispatch.processAdvance();
        QVERIFY(!target.output.empty());
        QVERIFY(target.output.back().getStart() >= 100.0);
        dispatch.incomingData(SequenceValue(a, Variant::Root(4.0), 300, 1500));
        dispatch.incomingData(SequenceValue(b, Variant::Root(5.0), 300, 1500));
        dispatch.incomingData(SequenceValue(a, Variant::Root(6.0), 400, 500));
        dispatch.incomingData(SequenceValue(b, Variant::Root(7.0), 400, 2100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(8.0), 800, 1000));
        dispatch.incomingData(SequenceValue(b, Variant::Root(9.0), 900, 1000));
        dispatch.incomingData(SequenceValue(a, Variant::Root(9.0), 1000, 1500));
        dispatch.incomingData(SequenceValue(a, Variant::Root(10.0), 1200, 1300));
        dispatch.incomingData(SequenceValue(b, Variant::Root(11.0), 1200, 2000));
        dispatch.incomingData(SequenceValue(b, Variant::Root(12.0), 1300, 2100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(13.0), 1500, 2100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(14.0), 1800, 1900));
        dispatch.incomingData(SequenceValue(b, Variant::Root(15.0), 1800, 1900));
        dispatch.incomingData(SequenceValue(a, Variant::Root(16.0), 2000, 2100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(17.0), 2100, 2200));
        dispatch.incomingData(SequenceValue(b, Variant::Root(18.0), 2200, 2300));
        dispatch.processAdvance();
        QVERIFY(target.output.back().getStart() >= 2000.0);
        dispatch.incomingAdvance(2500);
        dispatch.processAdvance();
        QVERIFY(target.output.back().getStart() >= 2200.0);

        for (auto d : directives) {
            d->finalize();
            delete d;
        }

        target.finish();

        QVERIFY(target.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                        SequenceValue(a, Variant::Root(2.0), 200, 300),
                                        SequenceValue(b, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(5.0), 300, 1500),
                                        SequenceValue(a, Variant::Root(6.0), 400, 500),
                                        SequenceValue(b, Variant::Root(7.0), 400, 2100),
                                        SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                        SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                        SequenceValue(b, Variant::Root(11.0), 1200, 2000),
                                        SequenceValue(b, Variant::Root(12.0), 1300, 2100),
                                        SequenceValue(b, Variant::Root(15.0), 1800, 1900),
                                        SequenceValue(a, Variant::Root(16.0), 2000, 2100),
                                        SequenceValue(a, Variant::Root(17.0), 2100, 2200),
                                        SequenceValue(b, Variant::Root(18.0), 2200, 2300)}));
    }

    void batch()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");

        Target target;
        std::vector<EditDirective *> directives;
        directives.insert(directives.begin(), new EditDirective(1000, 2000, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                a))));
        directives.front()->setOutput(&target);
        DirectiveDispatch dispatch(directives, &target);

        dispatch.incomingData(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                        SequenceValue(a, Variant::Root(2.0), 200, 300),
                                        SequenceValue(b, Variant::Root(3.0), 200, 300),
                                        SequenceValue(a, Variant::Root(4.0), 300, 1500),
                                        SequenceValue(b, Variant::Root(5.0), 300, 1500),
                                        SequenceValue(a, Variant::Root(6.0), 400, 500),
                                        SequenceValue(b, Variant::Root(7.0), 400, 2100)});
        dispatch.incomingAdvance(700);
        dispatch.incomingData(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                        SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                        SequenceValue(a, Variant::Root(9.0), 1000, 1500),
                                        SequenceValue(a, Variant::Root(10.0), 1200, 1300),
                                        SequenceValue(b, Variant::Root(11.0), 1200, 2000),
                                        SequenceValue(b, Variant::Root(12.0), 1300, 2100),
                                        SequenceValue(a, Variant::Root(13.0), 1500, 2100),
                                        SequenceValue(a, Variant::Root(14.0), 1800, 1900),
                                        SequenceValue(b, Variant::Root(15.0), 1800, 1900),
                                        SequenceValue(a, Variant::Root(16.0), 2000, 2100)});
        dispatch.incomingData(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(17.0), 2100, 2200),
                                        SequenceValue(b, Variant::Root(18.0), 2200, 2300)});
        dispatch.incomingAdvance(2500);

        QVERIFY(!target.output.empty());

        for (auto d : directives) {
            d->finalize();
            delete d;
        }

        target.finish();

        QVERIFY(target.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                        SequenceValue(a, Variant::Root(2.0), 200, 300),
                                        SequenceValue(b, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(5.0), 300, 1500),
                                        SequenceValue(a, Variant::Root(6.0), 400, 500),
                                        SequenceValue(b, Variant::Root(7.0), 400, 2100),
                                        SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                        SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                        SequenceValue(b, Variant::Root(11.0), 1200, 2000),
                                        SequenceValue(b, Variant::Root(12.0), 1300, 2100),
                                        SequenceValue(b, Variant::Root(15.0), 1800, 1900),
                                        SequenceValue(a, Variant::Root(16.0), 2000, 2100),
                                        SequenceValue(a, Variant::Root(17.0), 2100, 2200),
                                        SequenceValue(b, Variant::Root(18.0), 2200, 2300)}));
    }

    void chain()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");
        SequenceName c("bnd", "raw", "c");

        Target target;
        std::vector<EditDirective *> directives;
        directives.insert(directives.begin(), new EditDirective(3000, 4000, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                b))));
        directives.front()->setOutput(&target);
        DirectiveDispatch mid(directives, &target);
        directives.insert(directives.begin(), new EditDirective(1000, 2000, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                a))));
        directives.front()->setOutput(&mid);
        DirectiveDispatch dispatch(directives, &target);

        dispatch.incomingData(SequenceValue(a, Variant::Root(1.0), 100, 200));
        dispatch.incomingData(SequenceValue(b, Variant::Root(2.0), 100, 200));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-1.0), 100, 200));
        dispatch.incomingData(SequenceValue(a, Variant::Root(3.0), 200, 1500));
        dispatch.incomingData(SequenceValue(b, Variant::Root(4.0), 200, 1500));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-2.0), 200, 1500));
        dispatch.incomingData(SequenceValue(a, Variant::Root(5.0), 300, 2100));
        dispatch.incomingData(SequenceValue(b, Variant::Root(6.0), 300, 2100));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-3.0), 300, 2100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(7.0), 300, 3500));
        dispatch.incomingData(SequenceValue(b, Variant::Root(8.0), 300, 3500));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-4.0), 300, 3500));
        dispatch.processAdvance();
        mid.processAdvance();
        QVERIFY(!target.output.empty());
        QVERIFY(target.output.back().getStart() >= 300.0);
        dispatch.incomingData(SequenceValue(a, Variant::Root(9.0), 400, 4100));
        dispatch.incomingData(SequenceValue(b, Variant::Root(10.0), 400, 4100));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-5.0), 400, 4100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(11.0), 1100, 1900));
        dispatch.incomingData(SequenceValue(b, Variant::Root(12.0), 1100, 1900));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-6.0), 1100, 1900));
        dispatch.incomingData(SequenceValue(a, Variant::Root(13.0), 1200, 2100));
        dispatch.incomingData(SequenceValue(b, Variant::Root(14.0), 1200, 2100));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-7.0), 1200, 2100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(15.0), 1300, 3500));
        dispatch.incomingData(SequenceValue(b, Variant::Root(16.0), 1300, 3500));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-8.0), 1300, 3500));
        dispatch.incomingData(SequenceValue(a, Variant::Root(17.0), 1400, 4100));
        dispatch.incomingData(SequenceValue(b, Variant::Root(18.0), 1400, 4100));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-9.0), 1400, 4100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(19.0), 2100, 2200));
        dispatch.incomingData(SequenceValue(b, Variant::Root(20.0), 2100, 2200));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-10.0), 2100, 2200));
        dispatch.incomingData(SequenceValue(a, Variant::Root(21.0), 2200, 3500));
        dispatch.incomingData(SequenceValue(b, Variant::Root(22.0), 2200, 3500));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-11.0), 2200, 3500));
        dispatch.incomingData(SequenceValue(a, Variant::Root(23.0), 2300, 4100));
        dispatch.incomingData(SequenceValue(b, Variant::Root(24.0), 2300, 4100));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-12.0), 2300, 4100));
        dispatch.processAdvance();
        mid.processAdvance();
        QVERIFY(target.output.back().getStart() >= 2000.0);
        dispatch.incomingData(SequenceValue(a, Variant::Root(25.0), 3100, 3200));
        dispatch.incomingData(SequenceValue(b, Variant::Root(26.0), 3100, 3200));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-13.0), 3100, 3200));
        dispatch.incomingData(SequenceValue(a, Variant::Root(27.0), 3100, 4100));
        dispatch.incomingData(SequenceValue(b, Variant::Root(28.0), 3100, 4100));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-14.0), 3100, 4100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(29.0), 4100, 4200));
        dispatch.incomingData(SequenceValue(b, Variant::Root(30.0), 4100, 4200));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-15.0), 4100, 4200));
        dispatch.processAdvance();
        mid.processAdvance();
        QVERIFY(target.output.back().getStart() >= 3100.0);

        dispatch.incomingAdvance(4500);
        dispatch.processAdvance();
        mid.processAdvance();
        QVERIFY(target.output.back().getStart() >= 4100.0);

        QVERIFY(!target.output.empty());

        for (auto d : directives) {
            d->finalize();
            delete d;
        }

        target.finish();

        QVERIFY(target.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                        SequenceValue(b, Variant::Root(2.0), 100, 200),
                                        SequenceValue(c, Variant::Root(-1.0), 100, 200),
                                        SequenceValue(b, Variant::Root(4.0), 200, 1500),
                                        SequenceValue(c, Variant::Root(-2.0), 200, 1500),
                                        SequenceValue(b, Variant::Root(6.0), 300, 2100),
                                        SequenceValue(c, Variant::Root(-3.0), 300, 2100),
                                        SequenceValue(c, Variant::Root(-4.0), 300, 3500),
                                        SequenceValue(c, Variant::Root(-5.0), 400, 4100),
                                        SequenceValue(b, Variant::Root(12.0), 1100, 1900),
                                        SequenceValue(c, Variant::Root(-6.0), 1100, 1900),
                                        SequenceValue(b, Variant::Root(14.0), 1200, 2100),
                                        SequenceValue(c, Variant::Root(-7.0), 1200, 2100),
                                        SequenceValue(c, Variant::Root(-8.0), 1300, 3500),
                                        SequenceValue(c, Variant::Root(-9.0), 1400, 4100),
                                        SequenceValue(a, Variant::Root(19.0), 2100, 2200),
                                        SequenceValue(b, Variant::Root(20.0), 2100, 2200),
                                        SequenceValue(c, Variant::Root(-10.0), 2100, 2200),
                                        SequenceValue(a, Variant::Root(21.0), 2200, 3500),
                                        SequenceValue(c, Variant::Root(-11.0), 2200, 3500),
                                        SequenceValue(a, Variant::Root(23.0), 2300, 4100),
                                        SequenceValue(c, Variant::Root(-12.0), 2300, 4100),
                                        SequenceValue(a, Variant::Root(25.0), 3100, 3200),
                                        SequenceValue(c, Variant::Root(-13.0), 3100, 3200),
                                        SequenceValue(a, Variant::Root(27.0), 3100, 4100),
                                        SequenceValue(c, Variant::Root(-14.0), 3100, 4100),
                                        SequenceValue(a, Variant::Root(29.0), 4100, 4200),
                                        SequenceValue(b, Variant::Root(30.0), 4100, 4200),
                                        SequenceValue(c, Variant::Root(-15.0), 4100,
                                                                     4200)}));
    }

    void chainReverse()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");
        SequenceName c("bnd", "raw", "c");

        Target target;
        std::vector<EditDirective *> directives;
        directives.insert(directives.begin(), new EditDirective(1000, 2000, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                a))));
        directives.front()->setOutput(&target);
        DirectiveDispatch mid(directives, &target);
        directives.insert(directives.begin(), new EditDirective(3000, 4000, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                b))));
        directives.front()->setOutput(&mid);
        DirectiveDispatch dispatch(directives, &target);

        dispatch.incomingData(SequenceValue(a, Variant::Root(1.0), 100, 200));
        dispatch.incomingData(SequenceValue(b, Variant::Root(2.0), 100, 200));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-1.0), 100, 200));
        dispatch.incomingData(SequenceValue(a, Variant::Root(3.0), 200, 1500));
        dispatch.incomingData(SequenceValue(b, Variant::Root(4.0), 200, 1500));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-2.0), 200, 1500));
        dispatch.incomingData(SequenceValue(a, Variant::Root(5.0), 300, 2100));
        dispatch.incomingData(SequenceValue(b, Variant::Root(6.0), 300, 2100));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-3.0), 300, 2100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(7.0), 300, 3500));
        dispatch.incomingData(SequenceValue(b, Variant::Root(8.0), 300, 3500));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-4.0), 300, 3500));
        dispatch.incomingData(SequenceValue(a, Variant::Root(9.0), 400, 4100));
        dispatch.incomingData(SequenceValue(b, Variant::Root(10.0), 400, 4100));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-5.0), 400, 4100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(11.0), 1100, 1900));
        dispatch.incomingData(SequenceValue(b, Variant::Root(12.0), 1100, 1900));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-6.0), 1100, 1900));
        dispatch.incomingData(SequenceValue(a, Variant::Root(13.0), 1200, 2100));
        dispatch.incomingData(SequenceValue(b, Variant::Root(14.0), 1200, 2100));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-7.0), 1200, 2100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(15.0), 1300, 3500));
        dispatch.incomingData(SequenceValue(b, Variant::Root(16.0), 1300, 3500));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-8.0), 1300, 3500));
        dispatch.incomingData(SequenceValue(a, Variant::Root(17.0), 1400, 4100));
        dispatch.incomingData(SequenceValue(b, Variant::Root(18.0), 1400, 4100));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-9.0), 1400, 4100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(19.0), 2100, 2200));
        dispatch.incomingData(SequenceValue(b, Variant::Root(20.0), 2100, 2200));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-10.0), 2100, 2200));
        dispatch.incomingData(SequenceValue(a, Variant::Root(21.0), 2200, 3500));
        dispatch.incomingData(SequenceValue(b, Variant::Root(22.0), 2200, 3500));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-11.0), 2200, 3500));
        dispatch.incomingData(SequenceValue(a, Variant::Root(23.0), 2300, 4100));
        dispatch.incomingData(SequenceValue(b, Variant::Root(24.0), 2300, 4100));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-12.0), 2300, 4100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(25.0), 3100, 3200));
        dispatch.incomingData(SequenceValue(b, Variant::Root(26.0), 3100, 3200));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-13.0), 3100, 3200));
        dispatch.incomingData(SequenceValue(a, Variant::Root(27.0), 3100, 4100));
        dispatch.incomingData(SequenceValue(b, Variant::Root(28.0), 3100, 4100));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-14.0), 3100, 4100));
        dispatch.incomingData(SequenceValue(a, Variant::Root(29.0), 4100, 4200));
        dispatch.incomingData(SequenceValue(b, Variant::Root(30.0), 4100, 4200));
        dispatch.incomingData(SequenceValue(c, Variant::Root(-15.0), 4100, 4200));


        dispatch.incomingAdvance(4500);

        QVERIFY(!target.output.empty());

        for (auto d : directives) {
            d->finalize();
            delete d;
        }

        target.finish();

        QVERIFY(target.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                        SequenceValue(b, Variant::Root(2.0), 100, 200),
                                        SequenceValue(c, Variant::Root(-1.0), 100, 200),
                                        SequenceValue(b, Variant::Root(4.0), 200, 1500),
                                        SequenceValue(c, Variant::Root(-2.0), 200, 1500),
                                        SequenceValue(b, Variant::Root(6.0), 300, 2100),
                                        SequenceValue(c, Variant::Root(-3.0), 300, 2100),
                                        SequenceValue(c, Variant::Root(-4.0), 300, 3500),
                                        SequenceValue(c, Variant::Root(-5.0), 400, 4100),
                                        SequenceValue(b, Variant::Root(12.0), 1100, 1900),
                                        SequenceValue(c, Variant::Root(-6.0), 1100, 1900),
                                        SequenceValue(b, Variant::Root(14.0), 1200, 2100),
                                        SequenceValue(c, Variant::Root(-7.0), 1200, 2100),
                                        SequenceValue(c, Variant::Root(-8.0), 1300, 3500),
                                        SequenceValue(c, Variant::Root(-9.0), 1400, 4100),
                                        SequenceValue(a, Variant::Root(19.0), 2100, 2200),
                                        SequenceValue(b, Variant::Root(20.0), 2100, 2200),
                                        SequenceValue(c, Variant::Root(-10.0), 2100, 2200),
                                        SequenceValue(a, Variant::Root(21.0), 2200, 3500),
                                        SequenceValue(c, Variant::Root(-11.0), 2200, 3500),
                                        SequenceValue(a, Variant::Root(23.0), 2300, 4100),
                                        SequenceValue(c, Variant::Root(-12.0), 2300, 4100),
                                        SequenceValue(a, Variant::Root(25.0), 3100, 3200),
                                        SequenceValue(c, Variant::Root(-13.0), 3100, 3200),
                                        SequenceValue(a, Variant::Root(27.0), 3100, 4100),
                                        SequenceValue(c, Variant::Root(-14.0), 3100, 4100),
                                        SequenceValue(a, Variant::Root(29.0), 4100, 4200),
                                        SequenceValue(b, Variant::Root(30.0), 4100, 4200),
                                        SequenceValue(c, Variant::Root(-15.0), 4100,
                                                                     4200)}));
    }

    void chainBatch()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");
        SequenceName c("bnd", "raw", "c");

        Target target;
        std::vector<EditDirective *> directives;
        directives.insert(directives.begin(), new EditDirective(3000, 4000, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                b))));
        directives.front()->setOutput(&target);
        DirectiveDispatch mid(directives, &target);
        directives.insert(directives.begin(), new EditDirective(1000, 2000, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                a))));
        directives.front()->setOutput(&mid);
        DirectiveDispatch dispatch(directives, &target);

        dispatch.incomingData(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                        SequenceValue(b, Variant::Root(2.0), 100, 200),
                                        SequenceValue(c, Variant::Root(-1.0), 100, 200),
                                        SequenceValue(a, Variant::Root(3.0), 200, 1500),
                                        SequenceValue(b, Variant::Root(4.0), 200, 1500),
                                        SequenceValue(c, Variant::Root(-2.0), 200, 1500),
                                        SequenceValue(a, Variant::Root(5.0), 300, 2100),
                                        SequenceValue(b, Variant::Root(6.0), 300, 2100),
                                        SequenceValue(c, Variant::Root(-3.0), 300, 2100),
                                        SequenceValue(a, Variant::Root(7.0), 300, 3500)});
        dispatch.incomingData(
                SequenceValue::Transfer{SequenceValue(b, Variant::Root(8.0), 300, 3500),
                                        SequenceValue(c, Variant::Root(-4.0), 300, 3500),
                                        SequenceValue(a, Variant::Root(9.0), 400, 4100),
                                        SequenceValue(b, Variant::Root(10.0), 400, 4100),
                                        SequenceValue(c, Variant::Root(-5.0), 400, 4100),
                                        SequenceValue(a, Variant::Root(11.0), 1100, 1900),
                                        SequenceValue(b, Variant::Root(12.0), 1100, 1900),
                                        SequenceValue(c, Variant::Root(-6.0), 1100, 1900),
                                        SequenceValue(a, Variant::Root(13.0), 1200, 2100),
                                        SequenceValue(b, Variant::Root(14.0), 1200, 2100),
                                        SequenceValue(c, Variant::Root(-7.0), 1200, 2100),
                                        SequenceValue(a, Variant::Root(15.0), 1300, 3500),
                                        SequenceValue(b, Variant::Root(16.0), 1300, 3500),
                                        SequenceValue(c, Variant::Root(-8.0), 1300, 3500),
                                        SequenceValue(a, Variant::Root(17.0), 1400, 4100),
                                        SequenceValue(b, Variant::Root(18.0), 1400, 4100)});
        dispatch.incomingData(
                SequenceValue::Transfer{SequenceValue(c, Variant::Root(-9.0), 1400, 4100),
                                        SequenceValue(a, Variant::Root(19.0), 2100, 2200),
                                        SequenceValue(b, Variant::Root(20.0), 2100, 2200),
                                        SequenceValue(c, Variant::Root(-10.0), 2100, 2200),
                                        SequenceValue(a, Variant::Root(21.0), 2200, 3500),
                                        SequenceValue(b, Variant::Root(22.0), 2200, 3500),
                                        SequenceValue(c, Variant::Root(-11.0), 2200, 3500),
                                        SequenceValue(a, Variant::Root(23.0), 2300, 4100),
                                        SequenceValue(b, Variant::Root(24.0), 2300, 4100),
                                        SequenceValue(c, Variant::Root(-12.0), 2300, 4100),
                                        SequenceValue(a, Variant::Root(25.0), 3100, 3200),
                                        SequenceValue(b, Variant::Root(26.0), 3100, 3200),
                                        SequenceValue(c, Variant::Root(-13.0), 3100, 3200),
                                        SequenceValue(a, Variant::Root(27.0), 3100, 4100),
                                        SequenceValue(b, Variant::Root(28.0), 3100, 4100)});
        dispatch.incomingData(
                SequenceValue::Transfer{SequenceValue(c, Variant::Root(-14.0), 3100, 4100),
                                        SequenceValue(a, Variant::Root(29.0), 4100, 4200),
                                        SequenceValue(b, Variant::Root(30.0), 4100, 4200),
                                        SequenceValue(c, Variant::Root(-15.0), 4100, 4200)});


        dispatch.incomingAdvance(4500);

        QVERIFY(!target.output.empty());

        for (auto d : directives) {
            d->finalize();
            delete d;
        }

        target.finish();

        QVERIFY(target.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                        SequenceValue(b, Variant::Root(2.0), 100, 200),
                                        SequenceValue(c, Variant::Root(-1.0), 100, 200),
                                        SequenceValue(b, Variant::Root(4.0), 200, 1500),
                                        SequenceValue(c, Variant::Root(-2.0), 200, 1500),
                                        SequenceValue(b, Variant::Root(6.0), 300, 2100),
                                        SequenceValue(c, Variant::Root(-3.0), 300, 2100),
                                        SequenceValue(c, Variant::Root(-4.0), 300, 3500),
                                        SequenceValue(c, Variant::Root(-5.0), 400, 4100),
                                        SequenceValue(b, Variant::Root(12.0), 1100, 1900),
                                        SequenceValue(c, Variant::Root(-6.0), 1100, 1900),
                                        SequenceValue(b, Variant::Root(14.0), 1200, 2100),
                                        SequenceValue(c, Variant::Root(-7.0), 1200, 2100),
                                        SequenceValue(c, Variant::Root(-8.0), 1300, 3500),
                                        SequenceValue(c, Variant::Root(-9.0), 1400, 4100),
                                        SequenceValue(a, Variant::Root(19.0), 2100, 2200),
                                        SequenceValue(b, Variant::Root(20.0), 2100, 2200),
                                        SequenceValue(c, Variant::Root(-10.0), 2100, 2200),
                                        SequenceValue(a, Variant::Root(21.0), 2200, 3500),
                                        SequenceValue(c, Variant::Root(-11.0), 2200, 3500),
                                        SequenceValue(a, Variant::Root(23.0), 2300, 4100),
                                        SequenceValue(c, Variant::Root(-12.0), 2300, 4100),
                                        SequenceValue(a, Variant::Root(25.0), 3100, 3200),
                                        SequenceValue(c, Variant::Root(-13.0), 3100, 3200),
                                        SequenceValue(a, Variant::Root(27.0), 3100, 4100),
                                        SequenceValue(c, Variant::Root(-14.0), 3100, 4100),
                                        SequenceValue(a, Variant::Root(29.0), 4100, 4200),
                                        SequenceValue(b, Variant::Root(30.0), 4100, 4200),
                                        SequenceValue(c, Variant::Root(-15.0), 4100,
                                                                     4200)}));
    }

    void activationSequence()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");

        Target target;
        std::vector<EditDirective *> directives;
        directives.insert(directives.begin(),
                          new EditDirective(1900, FP::undefined(), new TriggerAlways,
                                            new ActionRemove(SequenceMatch::Composite(b))));
        directives.front()->setOutput(&target);
        DirectiveDispatch mid(directives, &target);
        directives.insert(directives.begin(), new EditDirective(1000, 2000, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                a))));
        directives.front()->setOutput(&mid);
        DirectiveDispatch dispatch(directives, &target);

        dispatch.incomingData(SequenceValue(a, Variant::Root(1.0), 100, 4000));
        dispatch.incomingData(SequenceValue(a, Variant::Root(2.0), 200, 300));
        dispatch.incomingData(SequenceValue(b, Variant::Root(3.0), 300, 400));

        for (auto d : directives) {
            d->finalize();
            delete d;
        }

        target.finish();

        QVERIFY(target.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.0), 200, 300),
                                        SequenceValue(b, Variant::Root(3.0), 300, 400)}));
    }

    void infiniteFirst()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");
        SequenceName c("bnd", "raw", "c");

        Target target;
        std::vector<EditDirective *> directives;
        directives.insert(directives.begin(),
                          new EditDirective(FP::undefined(), FP::undefined(), new TriggerAlways,
                                            new ActionRemove(SequenceMatch::Composite(b))));
        directives.front()->setOutput(&target);
        DirectiveDispatch mid(directives, &target);
        directives.insert(directives.begin(), new EditDirective(1000, 2000, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                c))));
        directives.front()->setOutput(&mid);
        DirectiveDispatch dispatch(directives, &target);

        dispatch.incomingData(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 4000),
                                        SequenceValue(a, Variant::Root(2.0), 100, 200),
                                        SequenceValue(c, Variant::Root(3.0), 200, 300),
                                        SequenceValue(c, Variant::Root(4.0), 900, 1100),
                                        SequenceValue(c, Variant::Root(5.0), 1100, 1200),
                                        SequenceValue(c, Variant::Root(6.0), 3000, 4000)});

        for (auto d : directives) {
            d->finalize();
            delete d;
        }

        target.finish();

        QVERIFY(target.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 4000),
                                        SequenceValue(a, Variant::Root(2.0), 100, 200),
                                        SequenceValue(c, Variant::Root(3.0), 200, 300),
                                        SequenceValue(c, Variant::Root(6.0), 3000, 4000)}));
    }

    void endLowerStartExactAlignment()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");
        SequenceName c("bnd", "raw", "c");

        Target target;
        std::vector<EditDirective *> directives;
        directives.insert(directives.begin(), new EditDirective(100, 2000, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                b))));
        directives.front()->setOutput(&target);
        DirectiveDispatch mid(directives, &target);
        directives.insert(directives.begin(), new EditDirective(500, 600, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                c))));
        directives.front()->setOutput(&mid);
        DirectiveDispatch dispatch(directives, &target);

        dispatch.incomingData(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 4000),
                                        SequenceValue(a, Variant::Root(2.0), 100, 200),
                                        SequenceValue(b, Variant::Root(3.0), 200, 300),
                                        SequenceValue(c, Variant::Root(4.0), 200, 550),
                                        SequenceValue(c, Variant::Root(5.0), 400, 500),
                                        SequenceValue(c, Variant::Root(6.0), 700, 800)});

        for (auto d : directives) {
            d->finalize();
            delete d;
        }

        target.finish();

        QVERIFY(target.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 4000),
                                        SequenceValue(a, Variant::Root(2.0), 100, 200),
                                        SequenceValue(c, Variant::Root(5.0), 400, 500),
                                        SequenceValue(c, Variant::Root(6.0), 700, 800)}));
    }

    void endLowerEndExactAlignment()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");
        SequenceName c("bnd", "raw", "c");

        Target target;
        std::vector<EditDirective *> directives;
        directives.insert(directives.begin(), new EditDirective(510, 550, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                b))));
        directives.front()->setOutput(&target);
        DirectiveDispatch mid(directives, &target);
        directives.insert(directives.begin(), new EditDirective(500, 600, new TriggerAlways,
                                                                new ActionRemove(
                                                                        SequenceMatch::Composite(
                                                                                c))));
        directives.front()->setOutput(&mid);
        DirectiveDispatch dispatch(directives, &target);

        dispatch.incomingData(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 4000),
                                        SequenceValue(a, Variant::Root(2.0), 100, 200),
                                        SequenceValue(c, Variant::Root(4.0), 400, 700),
                                        SequenceValue(b, Variant::Root(5.0), 500, 550),
                                        SequenceValue(c, Variant::Root(6.0), 700, 800)});

        for (auto d : directives) {
            d->finalize();
            delete d;
        }

        target.finish();

        QVERIFY(target.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 4000),
                                        SequenceValue(a, Variant::Root(2.0), 100, 200),
                                        SequenceValue(c, Variant::Root(6.0), 700, 800)}));
    }
};

QTEST_MAIN(TestDirectiveDispatch)

#include "directivedispatch.moc"
