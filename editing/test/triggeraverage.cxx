/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/triggeraverage.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

typedef TriggerInput::Result TR;

namespace QTest {
template<>
char *toString(const QVector<TR> &segs)
{
    QByteArray ba;
    for (int i = 0, max = segs.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        ba += "[" +
                QByteArray::number(segs[i].start) +
                ":" +
                QByteArray::number(segs[i].end) +
                "]=" +
                QByteArray::number(segs[i].value);
    }
    return qstrdup(ba.data());
}
}

static void sendValue(TriggerInput *input,
                      QHash<SequenceName, QList<EditDataTarget *> > &dispatch,
                      const SequenceName &unit,
                      double value,
                      double start,
                      double end)
{
    QHash<SequenceName, QList<EditDataTarget *> >::iterator target = dispatch.find(unit);
    if (target == dispatch.end()) {
        target = dispatch.insert(unit, input->unhandledInput(unit));
    }
    for (QList<EditDataTarget *>::const_iterator t = target.value().constBegin(),
            endT = target.value().constEnd(); t != endT; ++t) {
        (*t)->incomingSequenceValue(SequenceValue(unit, Variant::Root(value), start, end));
    }
}

class TestTriggerAverage : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        SequenceName v("brw", "raw", "1");
        SequenceName c("brw", "raw", "1", {"cover"});
        Variant::Root config;
        config["Interval/Unit"].setString("Second");
        config["Interval/Count"].setInt64(1000);
        TriggerInput *i = new TriggerInputAverage(SequenceMatch::OrderedLookup(SequenceMatch::Element("brw", "raw", "1")), config);

        QHash<SequenceName, QList<EditDataTarget *> > d;
        QVector<TR> r;
        QVector<TR> e;

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, v, 10, 1000, 1500);
        sendValue(i, d, c, 1.0, 1000, 1500);
        sendValue(i, d, v, 20, 1500, 2000);
        sendValue(i, d, c, 0.6, 1500, 2000);

        i->incomingDataAdvance(2001);
        r << i->process();
        QVERIFY(i->isReady(1500));
        e << TR(1000, 1500, 13.75);
        QCOMPARE(r, e);

        sendValue(i, d, v, 10, 10000, 10500);
        sendValue(i, d, c, 1.0, 10000, 10500);
        i->incomingDataAdvance(10001);
        r << i->process();
        QVERIFY(i->isReady(2000));
        e << TR(1500, 2000, 13.75);
        QCOMPARE(r, e);

        sendValue(i, d, v, 20, 10500, 11000);
        i->incomingDataAdvance(11001);
        r << i->process();
        QVERIFY(i->isReady(10500));
        e << TR(10000, 10500, 15.0);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        QVERIFY(i->isReady(20000));
        e << TR(10500, 11000, 15.0);
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;
        e.clear();
        d.clear();

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, v, 10, 1000, 1500);
        sendValue(i, d, c, 1.0, 1000, 1500);
        sendValue(i, d, v, 20, 1500, 2000);
        sendValue(i, d, c, 0.6, 1500, 2000);

        i->incomingDataAdvance(2001);
        r << i->process();
        QVERIFY(i->isReady(1500));
        e << TR(1000, 1500, 13.75);
        QCOMPARE(r, e);

        sendValue(i, d, v, 10, 10000, 10500);
        sendValue(i, d, c, 1.0, 10000, 10500);
        i->incomingDataAdvance(10001);
        r << i->process();
        QVERIFY(i->isReady(2000));
        e << TR(1500, 2000, 13.75);
        QCOMPARE(r, e);

        sendValue(i, d, v, 20, 10500, 11000);
        i->incomingDataAdvance(11001);
        r << i->process();
        QVERIFY(i->isReady(10500));
        e << TR(10000, 10500, 15.0);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        QVERIFY(i->isReady(20000));
        e << TR(10500, 11000, 15.0);
        QCOMPARE(r, e);

        delete i;
    }
};

QTEST_MAIN(TestTriggerAverage)

#include "triggeraverage.moc"
