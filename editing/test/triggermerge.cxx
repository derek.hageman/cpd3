/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/triggermerge.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

typedef TriggerInput::Result TR;

namespace QTest {
template<>
char *toString(const QVector<TR> &segs)
{
    QByteArray ba;
    for (int i = 0, max = segs.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        ba += "[" +
                QByteArray::number(segs[i].start) +
                ":" +
                QByteArray::number(segs[i].end) +
                "]=" +
                QByteArray::number(segs[i].value);
    }
    return qstrdup(ba.data());
}
}

static void sendValue(TriggerInput *input,
                      QHash<SequenceName, QList<EditDataTarget *> > &dispatch,
                      const SequenceName &unit,
                      double value,
                      double start,
                      double end)
{
    QHash<SequenceName, QList<EditDataTarget *> >::iterator target = dispatch.find(unit);
    if (target == dispatch.end()) {
        target = dispatch.insert(unit, input->unhandledInput(unit));
    }
    for (QList<EditDataTarget *>::const_iterator t = target.value().constBegin(),
            endT = target.value().constEnd(); t != endT; ++t) {
        (*t)->incomingSequenceValue(SequenceValue(unit, Variant::Root(value), start, end));
    }
}

class TestTriggerMerge : public QObject {
Q_OBJECT
private slots:

    void sum()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        TriggerInput *i = new TriggerInputSum(QVector<TriggerInput *>() <<
                                                      new TriggerInputValue(
                                                              SequenceMatch::OrderedLookup(u1)) <<
                                                      new TriggerInputValue(SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;
        QVector<TR> r;
        QVector<TR> e;

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        e << TR(100, 200, 30);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 11, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 21, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, 32);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 12, 300, 500);
        sendValue(i, d, u1, 13, 400, 600);
        sendValue(i, d, u2, 22, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, 33);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, 35);
        e << TR(500, 600, 35);
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;
        e.clear();
        d.clear();

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        QVERIFY(i->isReady(101));
        e << TR(100, 200, 30);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 11, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 21, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, 32);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 12, 300, 500);
        sendValue(i, d, u1, 13, 400, 600);
        sendValue(i, d, u2, 22, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, 33);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, 35);
        e << TR(500, 600, 35);
        QCOMPARE(r, e);

        delete i;
    }

    void difference()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        TriggerInput *i = new TriggerInputDifference(QVector<TriggerInput *>() <<
                                                             new TriggerInputValue(
                                                                     SequenceMatch::OrderedLookup(u1)) <<
                                                             new TriggerInputValue(
                                                                     SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;
        QVector<TR> r;
        QVector<TR> e;

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        QVERIFY(i->isReady(101));
        e << TR(100, 200, -10);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 11, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 21, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, -10);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 12, 300, 500);
        sendValue(i, d, u1, 13, 400, 600);
        sendValue(i, d, u2, 22, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, -9);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, -9);
        e << TR(500, 600, -9);
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;
        e.clear();
        d.clear();

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        QVERIFY(i->isReady(101));
        e << TR(100, 200, -10);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 11, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 21, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, -10);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 12, 300, 500);
        sendValue(i, d, u1, 13, 400, 600);
        sendValue(i, d, u2, 22, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, -9);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, -9);
        e << TR(500, 600, -9);
        QCOMPARE(r, e);

        delete i;
    }

    void product()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        TriggerInput *i = new TriggerInputProduct(QVector<TriggerInput *>() <<
                                                          new TriggerInputValue(
                                                                  SequenceMatch::OrderedLookup(u1)) <<
                                                          new TriggerInputValue(
                                                                  SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;
        QVector<TR> r;
        QVector<TR> e;

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        QVERIFY(i->isReady(101));
        e << TR(100, 200, 200);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 11, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 21, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, 231);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 12, 300, 500);
        sendValue(i, d, u1, 13, 400, 600);
        sendValue(i, d, u2, 22, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, 252);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, 286);
        e << TR(500, 600, 286);
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;
        e.clear();
        d.clear();

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        QVERIFY(i->isReady(101));
        e << TR(100, 200, 200);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 11, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 21, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, 231);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 12, 300, 500);
        sendValue(i, d, u1, 13, 400, 600);
        sendValue(i, d, u2, 22, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, 252);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, 286);
        e << TR(500, 600, 286);
        QCOMPARE(r, e);

        delete i;
    }

    void quotient()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        TriggerInput *i = new TriggerInputQuotient(QVector<TriggerInput *>() <<
                                                           new TriggerInputValue(
                                                                   SequenceMatch::OrderedLookup(u1)) <<
                                                           new TriggerInputValue(
                                                                   SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;
        QVector<TR> r;
        QVector<TR> e;

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        QVERIFY(i->isReady(101));
        e << TR(100, 200, 10.0 / 20.0);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 11, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 21, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, 11.0 / 21.0);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 12, 300, 500);
        sendValue(i, d, u1, 13, 400, 600);
        sendValue(i, d, u2, 22, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, 12.0 / 21.0);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, 13.0 / 22.0);
        e << TR(500, 600, 13.0 / 22.0);
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;
        e.clear();
        d.clear();

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        QVERIFY(i->isReady(101));
        e << TR(100, 200, 10.0 / 20.0);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 11, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 21, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, 11.0 / 21.0);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 12, 300, 500);
        sendValue(i, d, u1, 13, 400, 600);
        i->incomingDataAdvance(400);
        sendValue(i, d, u2, 22, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, 12.0 / 21.0);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, 13.0 / 22.0);
        e << TR(500, 600, 13.0 / 22.0);
        QCOMPARE(r, e);

        delete i;
    }

    void power()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        TriggerInput *i = new TriggerInputPower(QVector<TriggerInput *>() <<
                                                        new TriggerInputValue(
                                                                SequenceMatch::OrderedLookup(u1)) <<
                                                        new TriggerInputValue(
                                                                SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;
        QVector<TR> r;
        QVector<TR> e;

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 2, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 3, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        QVERIFY(i->isReady(101));
        e << TR(100, 200, 8);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 3, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 4, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, 81);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 5, 300, 500);
        sendValue(i, d, u1, 6, 400, 600);
        sendValue(i, d, u2, 7, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, 625);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, 279936);
        e << TR(500, 600, 279936);
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;
        e.clear();
        d.clear();

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 2, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 3, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        QVERIFY(i->isReady(101));
        e << TR(100, 200, 8);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 3, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 4, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, 81);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 5, 300, 500);
        sendValue(i, d, u1, 6, 400, 600);
        i->incomingDataAdvance(400);
        sendValue(i, d, u2, 7, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, 625);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, 279936);
        e << TR(500, 600, 279936);
        QCOMPARE(r, e);

        delete i;
    }

    void firstvalid()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        TriggerInput *i = new TriggerInputFirstValid(QVector<TriggerInput *>() <<
                                                             new TriggerInputValue(
                                                                     SequenceMatch::OrderedLookup(u1)) <<
                                                             new TriggerInputValue(
                                                                     SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;
        QVector<TR> r;
        QVector<TR> e;

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        i->incomingDataAdvance(101);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QCOMPARE(i->advancedTime(), 100.0);
        QVERIFY(i->isReady(100));

        i->incomingDataAdvance(201);
        r << i->process();
        QCOMPARE(i->advancedTime(), 201.0);
        QVERIFY(i->isReady(100));
        e << TR(100, 200, 10);
        QCOMPARE(r, e);

        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(301);
        r << i->process();
        QCOMPARE(i->advancedTime(), 300.0);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->isReady(301));
        QCOMPARE(r, e);

        sendValue(i, d, u1, 11, 350, 390);
        i->incomingDataAdvance(351);
        r << i->process();
        QCOMPARE(i->advancedTime(), 351.0);
        QVERIFY(i->isReady(351));
        QVERIFY(!i->isReady(352));
        QVERIFY(!i->isReady(380));
        e << TR(300, 350, 20);
        e << TR(350, 390, 11);
        QCOMPARE(r, e);

        sendValue(i, d, u1, FP::undefined(), 380, 390);
        i->incomingDataAdvance(380);
        QCOMPARE(i->advancedTime(), 380.0);
        QVERIFY(i->isReady(380));
        QVERIFY(!i->isReady(381));
        r << i->process();
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(380, 390, 20.0);
        e << TR(390, 400, 20.0);
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;
        e.clear();
        d.clear();

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        i->incomingDataAdvance(101);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QCOMPARE(i->advancedTime(), 100.0);
        QVERIFY(i->isReady(100));

        i->incomingDataAdvance(201);
        r << i->process();
        QCOMPARE(i->advancedTime(), 201.0);
        QVERIFY(i->isReady(100));
        e << TR(100, 200, 10);
        QCOMPARE(r, e);

        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(301);
        r << i->process();
        QCOMPARE(i->advancedTime(), 300.0);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->isReady(301));
        QCOMPARE(r, e);

        sendValue(i, d, u1, 11, 350, 390);
        i->incomingDataAdvance(351);
        r << i->process();
        QCOMPARE(i->advancedTime(), 351.0);
        QVERIFY(i->isReady(351));
        QVERIFY(!i->isReady(352));
        QVERIFY(!i->isReady(380));
        e << TR(300, 350, 20);
        e << TR(350, 390, 11);
        QCOMPARE(r, e);

        sendValue(i, d, u1, FP::undefined(), 380, 390);
        i->incomingDataAdvance(380);
        QCOMPARE(i->advancedTime(), 380.0);
        QVERIFY(i->isReady(380));
        QVERIFY(!i->isReady(381));
        r << i->process();
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(380, 390, 20.0);
        e << TR(390, 400, 20.0);
        QCOMPARE(r, e);

        delete i;
    }

    void largest()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        TriggerInput *i = new TriggerInputLargest(QVector<TriggerInput *>() <<
                                                          new TriggerInputValue(
                                                                  SequenceMatch::OrderedLookup(u1)) <<
                                                          new TriggerInputValue(
                                                                  SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;
        QVector<TR> r;
        QVector<TR> e;

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        e << TR(100, 200, 20);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 31, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 21, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, 31);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 12, 300, 500);
        sendValue(i, d, u1, 13, 400, 600);
        sendValue(i, d, u2, 22, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, 21);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, 22);
        e << TR(500, 600, 22);
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;
        e.clear();
        d.clear();

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        e << TR(100, 200, 20);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 31, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 21, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, 31);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 12, 300, 500);
        sendValue(i, d, u1, 13, 400, 600);
        sendValue(i, d, u2, 22, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, 21);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, 22);
        e << TR(500, 600, 22);
        QCOMPARE(r, e);

        delete i;
    }

    void smallest()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        TriggerInput *i = new TriggerInputSmallest(QVector<TriggerInput *>() <<
                                                           new TriggerInputValue(
                                                                   SequenceMatch::OrderedLookup(u1)) <<
                                                           new TriggerInputValue(
                                                                   SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;
        QVector<TR> r;
        QVector<TR> e;

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        e << TR(100, 200, 10);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 31, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 21, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, 21);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 12, 300, 500);
        sendValue(i, d, u1, 13, 400, 600);
        sendValue(i, d, u2, 22, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, 12);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, 13);
        e << TR(500, 600, 13);
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;
        e.clear();
        d.clear();

        r = i->process();
        QCOMPARE(r.size(), 0);

        sendValue(i, d, u1, 10, 100, 200);
        QVERIFY(!i->isReady(100));
        QVERIFY(!FP::defined(i->advancedTime()));
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));
        QCOMPARE(i->advancedTime(), 100.0);

        r << i->process();
        QCOMPARE(r.size(), 0);
        i->incomingDataAdvance(101);
        r << i->process();
        e << TR(100, 200, 10);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 31, 200, 300);
        QVERIFY(!i->isReady(200));
        sendValue(i, d, u2, 21, 200, 500);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QCOMPARE(r, e);

        QVERIFY(i->isReady(200));
        i->incomingDataAdvance(201);
        r << i->process();
        e << TR(200, 300, 21);
        QCOMPARE(r, e);

        sendValue(i, d, u1, 12, 300, 500);
        sendValue(i, d, u1, 13, 400, 600);
        sendValue(i, d, u2, 22, 400, 600);
        i->incomingDataAdvance(400);
        r << i->process();
        QVERIFY(i->isReady(400));
        e << TR(300, 400, 12);
        QCOMPARE(r, e);

        i->finalize();
        r << i->process();
        e << TR(400, 500, 13);
        e << TR(500, 600, 13);
        QCOMPARE(r, e);

        delete i;
    }

    void constant()
    {
        SequenceName u1("brw", "raw", "1");
        TriggerInput *i = new TriggerInputSum(QVector<TriggerInput *>() <<
                                                      new TriggerInputValue(
                                                              SequenceMatch::OrderedLookup(u1)) <<
                                                      new TriggerInputConstant(20.0));

        QHash<SequenceName, QList<EditDataTarget *> > d;
        QVector<TR> r;
        QVector<TR> e;

        r = i->process();
        QCOMPARE(r.size(), 0);

        QVERIFY(!i->isReady(100));
        sendValue(i, d, u1, 10, 100, 200);
        r << i->process();
        e << TR(FP::undefined(), 100, FP::undefined());
        sendValue(i, d, u1, 11, 150, 200);
        r << i->process();
        QVERIFY(i->isReady(100));
        e << TR(100, 150, 30);
        sendValue(i, d, u1, 12, 200, 300);
        e << TR(150, 200, 31);
        r << i->process();
        QVERIFY(i->isReady(150));
        QVERIFY(!r.isEmpty());

        i->finalize();
        r << i->process();
        e << TR(200, 300, 32);
        e << TR(300, FP::undefined(), FP::undefined());
        QCOMPARE(r, e);

        delete i;
    }

    void constantMissingInput()
    {
        SequenceName u1("brw", "raw", "1");
        TriggerInput *i = new TriggerInputSum(QVector<TriggerInput *>() <<
                                                      new TriggerInputValue(
                                                              SequenceMatch::OrderedLookup(u1)) <<
                                                      new TriggerInputConstant(20.0));

        QHash<SequenceName, QList<EditDataTarget *> > d;
        QVector<TR> r;
        QVector<TR> e;

        r = i->process();
        QCOMPARE(r.size(), 0);

        QVERIFY(!i->isReady(100));
        r << i->process();
        i->incomingDataAdvance(100);
        r << i->process();
        i->incomingDataAdvance(10000);
        i->incomingDataAdvance(10000);
        i->incomingDataAdvance(10000);
        r << i->process();
        QVERIFY(i->isReady(100));

        i->finalize();
        r << i->process();
        e << TR(FP::undefined(), 100, FP::undefined());
        e << TR(100, 10000, FP::undefined());
        e << TR(10000, FP::undefined(), FP::undefined());
        QCOMPARE(r, e);

        delete i;
    }
};

QTEST_MAIN(TestTriggerMerge)

#include "triggermerge.moc"
