/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/wavelengthadjust.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "smoothing/baseline.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;

class TestWavelengthAdjust : public QObject {
Q_OBJECT
private slots:

    void dynamic()
    {
        WavelengthAdjust *adj = WavelengthAdjust::fromInput(
                new DynamicSequenceSelection::Match(QString(), QString(), "Bs[BGR]_S11"),
                new BaselineLatest);

        SequenceName BsB("bnd", "raw", "BsB_S11");
        SequenceName BsG("bnd", "raw", "BsG_S11");
        SequenceName BsR("bnd", "raw", "BsR_S11");

        QVERIFY(!adj->registerInput(SequenceName("bnd", "raw", "BaG_A11")));
        QVERIFY(adj->registerInput(BsB));
        QVERIFY(adj->registerInput(BsG));
        QVERIFY(adj->registerInput(BsR));
        QCOMPARE(adj->getAllUnits(), (SequenceName::Set{BsB, BsG, BsR}));

        SequenceSegment data;
        Variant::Root meta;
        meta.write().metadataReal("Wavelength").setDouble(450.0);
        data.setValue(BsB.toMeta(), meta);
        meta.write().setEmpty();
        meta.write().metadataReal("Wavelength").setDouble(550.0);
        data.setValue(BsG.toMeta(), meta);
        meta.write().setEmpty();
        meta.write().metadataReal("Wavelength").setDouble(700.0);
        data.setValue(BsR.toMeta(), meta);
        data.setStart(10.0);
        data.setEnd(FP::undefined());
        adj->incomingMeta(data);

        QCOMPARE(adj->getWavelength(BsB), 450.0);
        QCOMPARE(adj->getWavelength(BsG), 550.0);
        QCOMPARE(adj->getWavelength(BsR), 700.0);

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(8.0));
        data.setValue(BsR, Variant::Root(6.0));
        data.setStart(10.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAngstrom(450.0).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAngstrom(400.0).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAngstrom(500.0).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAngstrom(550.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(600.0).toDouble(), 1.19289939822588);
        QCOMPARE(adj->getAngstrom(700.0).toDouble(), 1.19289939822588);
        QCOMPARE(adj->getAngstrom(750.0).toDouble(), 1.19289939822588);
        QCOMPARE(adj->getAngstrom(BsB).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAngstrom(BsG).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(BsR).toDouble(), 1.19289939822588);

        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 10.0);
        QCOMPARE(adj->getAdjusted(550.0).toDouble(), 8.0);
        QCOMPARE(adj->getAdjusted(700.0).toDouble(), 6.0);
        QCOMPARE(adj->getAdjusted(400.0).toDouble(), 11.3993746763798);
        QCOMPARE(adj->getAdjusted(500.0).toDouble(), 8.89443132640780);
        QCOMPARE(adj->getAdjusted(600.0).toDouble(), 7.21127463252649);
        QCOMPARE(adj->getAdjusted(750.0).toDouble(), 5.52596511915908);

        {
            WavelengthAdjust *adj2;
            {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << adj;
                }
                {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    stream >> adj2;
                }
            }
            QCOMPARE(adj2->getAllUnits(), (SequenceName::Set{BsB, BsG, BsR}));

            QCOMPARE(adj2->getAngstrom(450.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(400.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(500.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAdjusted(400.0).toDouble(), 11.3993746763798);
            QCOMPARE(adj2->getAdjusted(500.0).toDouble(), 8.89443132640780);
            QCOMPARE(adj2->getAdjusted(600.0).toDouble(), 7.21127463252649);
            QCOMPARE(adj2->getAdjusted(750.0).toDouble(), 5.52596511915908);

            *adj2 = *adj;
            QCOMPARE(adj2->getAngstrom(450.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(400.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(500.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAdjusted(400.0).toDouble(), 11.3993746763798);
            QCOMPARE(adj2->getAdjusted(500.0).toDouble(), 8.89443132640780);
            QCOMPARE(adj2->getAdjusted(600.0).toDouble(), 7.21127463252649);
            QCOMPARE(adj2->getAdjusted(750.0).toDouble(), 5.52596511915908);

            WavelengthAdjust adj3(*adj2);
            delete adj2;

            QCOMPARE(adj3.getAngstrom(450.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj3.getAngstrom(400.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj3.getAngstrom(500.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj3.getAdjusted(400.0).toDouble(), 11.3993746763798);
            QCOMPARE(adj3.getAdjusted(500.0).toDouble(), 8.89443132640780);
            QCOMPARE(adj3.getAdjusted(600.0).toDouble(), 7.21127463252649);
            QCOMPARE(adj3.getAdjusted(750.0).toDouble(), 5.52596511915908);
        }

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(-8.0));
        data.setValue(BsR, Variant::Root(6.0));
        data.setStart(20.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAngstrom(400.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(450.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(550.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(600.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(750.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(800.0).toDouble(), 1.15615155538170);

        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 10.0);
        QCOMPARE(adj->getAdjusted(550.0).toDouble(), -8.0);
        QCOMPARE(adj->getAdjusted(700.0).toDouble(), 6.0);
        QCOMPARE(adj->getAdjusted(400.0).toDouble(), 19.0);
        QCOMPARE(adj->getAdjusted(500.0).toDouble(), 1.0);
        QCOMPARE(adj->getAdjusted(600.0).toDouble(), -3.33333333333333);
        QCOMPARE(adj->getAdjusted(750.0).toDouble(), 10.6666666666667);

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(8.0));
        data.setValue(BsR, Variant::Root(FP::undefined()));
        data.setStart(20.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAngstrom(400.0).toDouble(), 1.1119887276032148);
        QCOMPARE(adj->getAngstrom(450.0).toDouble(), 1.1119887276032148);
        QCOMPARE(adj->getAngstrom(550.0).toDouble(), 1.1119887276032148);
        QCOMPARE(adj->getAngstrom(600.0).toDouble(), 1.1119887276032148);
        QCOMPARE(adj->getAngstrom(750.0).toDouble(), 1.1119887276032148);
        QCOMPARE(adj->getAngstrom(800.0).toDouble(), 1.1119887276032148);

        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 10.0);
        QCOMPARE(adj->getAdjusted(550.0).toDouble(), 8.0);
        QCOMPARE(adj->getAdjusted(700.0).toDouble(), 6.11822519087006);
        QCOMPARE(adj->getAdjusted(400.0).toDouble(), 11.3993746763798);
        QCOMPARE(adj->getAdjusted(500.0).toDouble(), 8.89443132640780);
        QCOMPARE(adj->getAdjusted(600.0).toDouble(), 7.26222220826863);
        QCOMPARE(adj->getAdjusted(750.0).toDouble(), 5.66639298578371);

        delete adj;
    }

    void dynamicLimit()
    {
        Variant::Root config;
        config["Input"].setString("Bs[BGR]_S11");
        config["ValidDistance"].setDouble(70.0);
        WavelengthAdjust *adj = WavelengthAdjust::fromConfiguration(
                ValueSegment::Transfer{ValueSegment(FP::undefined(), FP::undefined(), config)});

        SequenceName BsB("bnd", "raw", "BsB_S11");
        SequenceName BsG("bnd", "raw", "BsG_S11");
        SequenceName BsR("bnd", "raw", "BsR_S11");

        QVERIFY(!adj->registerInput(SequenceName("bnd", "raw", "BaG_A11")));
        QVERIFY(adj->registerInput(BsB));
        QVERIFY(adj->registerInput(BsG));
        QVERIFY(adj->registerInput(BsR));
        QCOMPARE(adj->getAllUnits(), (SequenceName::Set{BsB, BsG, BsR}));

        SequenceSegment data;
        Variant::Root meta;
        meta.write().metadataReal("Wavelength").setDouble(450.0);
        data.setValue(BsB.toMeta(), meta);
        meta.write().setEmpty();
        meta.write().metadataReal("Wavelength").setDouble(550.0);
        data.setValue(BsG.toMeta(), meta);
        meta.write().setEmpty();
        meta.write().metadataReal("Wavelength").setDouble(700.0);
        data.setValue(BsR.toMeta(), meta);
        data.setStart(10.0);
        data.setEnd(FP::undefined());
        adj->incomingMeta(data);
        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(8.0));
        data.setValue(BsR, Variant::Root(6.0));
        data.setStart(10.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 10.0);
        QCOMPARE(adj->getAdjusted(550.0).toDouble(), 8.0);
        QCOMPARE(adj->getAdjusted(700.0).toDouble(), 6.0);
        QCOMPARE(adj->getAdjusted(400.0).toDouble(), 11.3993746763798);
        QCOMPARE(adj->getAdjusted(500.0).toDouble(), 8.89443132640780);
        QCOMPARE(adj->getAdjusted(600.0).toDouble(), 7.21127463252649);
        QCOMPARE(adj->getAdjusted(750.0).toDouble(), 5.52596511915908);

        {
            WavelengthAdjust *adj2;
            {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << adj;
                }
                {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    stream >> adj2;
                }
            }
            QCOMPARE(adj2->getAllUnits(), (SequenceName::Set{BsB, BsG, BsR}));

            QCOMPARE(adj2->getAdjusted(400.0).toDouble(), 11.3993746763798);
            QCOMPARE(adj2->getAdjusted(500.0).toDouble(), 8.89443132640780);
            QCOMPARE(adj2->getAdjusted(600.0).toDouble(), 7.21127463252649);
            QCOMPARE(adj2->getAdjusted(750.0).toDouble(), 5.52596511915908);

            *adj2 = *adj;
            QCOMPARE(adj2->getAdjusted(400.0).toDouble(), 11.3993746763798);
            QCOMPARE(adj2->getAdjusted(500.0).toDouble(), 8.89443132640780);
            QCOMPARE(adj2->getAdjusted(600.0).toDouble(), 7.21127463252649);
            QCOMPARE(adj2->getAdjusted(750.0).toDouble(), 5.52596511915908);

            WavelengthAdjust adj3(*adj2);
            delete adj2;

            QCOMPARE(adj3.getAdjusted(400.0).toDouble(), 11.3993746763798);
            QCOMPARE(adj3.getAdjusted(500.0).toDouble(), 8.89443132640780);
            QCOMPARE(adj3.getAdjusted(600.0).toDouble(), 7.21127463252649);
            QCOMPARE(adj3.getAdjusted(750.0).toDouble(), 5.52596511915908);
        }

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(-8.0));
        data.setValue(BsR, Variant::Root(6.0));
        data.setStart(20.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 10.0);
        QCOMPARE(adj->getAdjusted(550.0).toDouble(), -8.0);
        QCOMPARE(adj->getAdjusted(700.0).toDouble(), 6.0);
        QCOMPARE(adj->getAdjusted(400.0).toDouble(), 19.0);
        QCOMPARE(adj->getAdjusted(500.0).toDouble(), 1.0);
        QCOMPARE(adj->getAdjusted(600.0).toDouble(), -3.33333333333333);
        QCOMPARE(adj->getAdjusted(750.0).toDouble(), 10.6666666666667);

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(8.0));
        data.setValue(BsR, Variant::Root(FP::undefined()));
        data.setStart(20.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 10.0);
        QCOMPARE(adj->getAdjusted(550.0).toDouble(), 8.0);
        QVERIFY(!FP::defined(adj->getAdjusted(700.0).toReal()));
        QCOMPARE(adj->getAdjusted(400.0).toDouble(), 11.3993746763798);
        QCOMPARE(adj->getAdjusted(500.0).toDouble(), 8.89443132640780);
        QCOMPARE(adj->getAdjusted(600.0).toDouble(), 7.26222220826863);
        QVERIFY(!FP::defined(adj->getAdjusted(750.0).toReal()));

        delete adj;
    }

    void fixed()
    {
        Variant::Root config;
        config["Fixed/Blue/Input"].setString("::BsB_S11:=");
        config["Fixed/Blue/Wavelength"].setDouble(450.0);
        config["Fixed/Blue/Smoothing/Type"].setString("Latest");
        config["Fixed/Green/Input"].setString("::BsG_S11:=");
        config["Fixed/Green/Smoothing/Type"].setString("Latest");
        config["Fixed/Red/Input"].setString("::BsR_S11:=");
        config["Fixed/Red/Smoothing/Type"].setString("Latest");
        WavelengthAdjust *adj = WavelengthAdjust::fromConfiguration(
                ValueSegment::Transfer{ValueSegment(FP::undefined(), FP::undefined(), config)});

        SequenceName BsB("bnd", "raw", "BsB_S11");
        SequenceName BsG("bnd", "raw", "BsG_S11");
        SequenceName BsR("bnd", "raw", "BsR_S11");

        QVERIFY(!adj->registerInput(SequenceName("bnd", "raw", "BaG_A11")));
        adj->registerExpected("bnd", "raw");
        QCOMPARE(adj->getAllUnits(), (SequenceName::Set{BsB, BsG, BsR}));
        bool check = false;
        QVERIFY(adj->registerInput(BsB, &check));
        QVERIFY(check);
        QVERIFY(adj->registerInput(BsG));
        QVERIFY(adj->registerInput(BsR));

        SequenceSegment data;
        Variant::Root meta;
        meta.write().metadataReal("Wavelength").setDouble(550.0);
        data.setValue(BsG.toMeta(), meta);
        meta.write().setEmpty();
        meta.write().metadataReal("Wavelength").setDouble(700.0);
        data.setValue(BsR.toMeta(), meta);
        data.setStart(10.0);
        data.setEnd(FP::undefined());
        adj->incomingMeta(data);

        QCOMPARE(adj->getWavelength(BsB), 450.0);
        QCOMPARE(adj->getWavelength(BsG), 550.0);
        QCOMPARE(adj->getWavelength(BsR), 700.0);

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(8.0));
        data.setValue(BsR, Variant::Root(6.0));
        data.setStart(10.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAngstrom(450.0).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAngstrom(400.0).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAngstrom(500.0).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAngstrom(550.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(600.0).toDouble(), 1.19289939822588);
        QCOMPARE(adj->getAngstrom(700.0).toDouble(), 1.19289939822588);
        QCOMPARE(adj->getAngstrom(750.0).toDouble(), 1.19289939822588);
        QCOMPARE(adj->getAngstrom(BsB).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAngstrom(BsG).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(BsR).toDouble(), 1.19289939822588);

        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 10.0);
        QCOMPARE(adj->getAdjusted(550.0).toDouble(), 8.0);
        QCOMPARE(adj->getAdjusted(700.0).toDouble(), 6.0);
        QCOMPARE(adj->getAdjusted(400.0).toDouble(), 11.3993746763798);
        QCOMPARE(adj->getAdjusted(500.0).toDouble(), 8.89443132640780);
        QCOMPARE(adj->getAdjusted(600.0).toDouble(), 7.21127463252649);
        QCOMPARE(adj->getAdjusted(750.0).toDouble(), 5.52596511915908);

        {
            WavelengthAdjust *adj2;
            {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << adj;
                }
                {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    stream >> adj2;
                }
            }
            QCOMPARE(adj2->getAllUnits(), (SequenceName::Set{BsB, BsG, BsR}));

            QCOMPARE(adj2->getAngstrom(450.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(400.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(500.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAdjusted(400.0).toDouble(), 11.3993746763798);
            QCOMPARE(adj2->getAdjusted(500.0).toDouble(), 8.89443132640780);
            QCOMPARE(adj2->getAdjusted(600.0).toDouble(), 7.21127463252649);
            QCOMPARE(adj2->getAdjusted(750.0).toDouble(), 5.52596511915908);

            *adj2 = *adj;
            QCOMPARE(adj2->getAngstrom(450.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(400.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(500.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAdjusted(400.0).toDouble(), 11.3993746763798);
            QCOMPARE(adj2->getAdjusted(500.0).toDouble(), 8.89443132640780);
            QCOMPARE(adj2->getAdjusted(600.0).toDouble(), 7.21127463252649);
            QCOMPARE(adj2->getAdjusted(750.0).toDouble(), 5.52596511915908);

            WavelengthAdjust adj3(*adj2);
            delete adj2;

            QCOMPARE(adj3.getAngstrom(450.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj3.getAngstrom(400.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj3.getAngstrom(500.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj3.getAdjusted(400.0).toDouble(), 11.3993746763798);
            QCOMPARE(adj3.getAdjusted(500.0).toDouble(), 8.89443132640780);
            QCOMPARE(adj3.getAdjusted(600.0).toDouble(), 7.21127463252649);
            QCOMPARE(adj3.getAdjusted(750.0).toDouble(), 5.52596511915908);
        }

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(-8.0));
        data.setValue(BsR, Variant::Root(6.0));
        data.setStart(20.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAngstrom(400.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(450.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(550.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(600.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(750.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(800.0).toDouble(), 1.15615155538170);

        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 10.0);
        QCOMPARE(adj->getAdjusted(550.0).toDouble(), -8.0);
        QCOMPARE(adj->getAdjusted(700.0).toDouble(), 6.0);
        QCOMPARE(adj->getAdjusted(400.0).toDouble(), 19.0);
        QCOMPARE(adj->getAdjusted(500.0).toDouble(), 1.0);
        QCOMPARE(adj->getAdjusted(600.0).toDouble(), -3.33333333333333);
        QCOMPARE(adj->getAdjusted(750.0).toDouble(), 10.6666666666667);

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(-5.0));
        data.setValue(BsR, Variant::Root(-6.0));
        data.setStart(20.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        delete adj;
    }

    void angstrom()
    {
        Variant::Root config;
        config["Fixed/Blue/Input"].setString("::BsB_S11:=");
        config["Fixed/Blue/Smoothing/Type"].setString("Latest");
        config["Smoothing/Type"].setString("Latest");
        config["Input"].setString("::Bs[GR]_S11:=");
        config["Angstrom/ZA1/Input"].setString("::ZA1_S11:=");
        config["Angstrom/ZA1/Smoothing/Type"].setString("Latest");
        config["Angstrom/ZA1/ValidDistance"].setDouble(100);
        config["Angstrom/ZA1/Wavelength"].setDouble(500);
        config["AngstromFallback/ZA2/Input"].setString("::ZA2_S11:=");
        config["AngstromFallback/ZA2/Smoothing/Type"].setString("Latest");
        WavelengthAdjust *adj = WavelengthAdjust::fromConfiguration(
                ValueSegment::Transfer{ValueSegment(FP::undefined(), FP::undefined(), config)});

        SequenceName BsB("bnd", "raw", "BsB_S11");
        SequenceName BsG("bnd", "raw", "BsG_S11");
        SequenceName BsR("bnd", "raw", "BsR_S11");
        SequenceName ZA1("bnd", "raw", "ZA1_S11");
        SequenceName ZA2("bnd", "raw", "ZA2_S11");

        QVERIFY(!adj->registerInput(SequenceName("bnd", "raw", "BaG_A11")));
        adj->registerExpected("bnd", "raw");
        QCOMPARE(adj->getAllUnits(), (SequenceName::Set{BsB, ZA1, ZA2}));

        bool check = false;
        QVERIFY(adj->registerInput(BsB, &check));
        QVERIFY(check);
        QVERIFY(adj->registerInput(BsG));
        QVERIFY(adj->registerInput(BsR));
        check = true;
        QVERIFY(adj->registerInput(ZA1, &check));
        QVERIFY(!check);
        QVERIFY(adj->registerInput(ZA2));

        SequenceSegment data;
        Variant::Root meta;
        meta.write().metadataReal("Wavelength").setDouble(450.0);
        data.setValue(BsB.toMeta(), meta);
        meta.write().setEmpty();
        meta.write().metadataReal("Wavelength").setDouble(550.0);
        data.setValue(BsG.toMeta(), meta);
        meta.write().setEmpty();
        meta.write().metadataReal("Wavelength").setDouble(700.0);
        data.setValue(BsR.toMeta(), meta);
        meta.write().setEmpty();
        meta.write().metadataReal("Wavelength").setDouble(650.0);
        data.setValue(ZA2.toMeta(), meta);
        data.setStart(10.0);
        data.setEnd(FP::undefined());
        adj->incomingMeta(data);

        QCOMPARE(adj->getWavelength(BsB), 450.0);
        QCOMPARE(adj->getWavelength(BsG), 550.0);
        QCOMPARE(adj->getWavelength(BsR), 700.0);
        QCOMPARE(adj->getWavelength(ZA1), 500.0);
        QCOMPARE(adj->getWavelength(ZA2), 650.0);

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(8.0));
        data.setValue(ZA1, Variant::Root(0.75));
        data.setStart(10.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAngstrom(500.0).toDouble(), 0.75);
        QCOMPARE(adj->getAngstrom(450.0).toDouble(), 0.75);
        QCOMPARE(adj->getAngstrom(450.0, Wavelength::BracketingExactBehavior::ExcludeExact, 25.0)
                    .toDouble(),
                 1.11198872760321);
        QCOMPARE(adj->getAngstrom(550.0).toDouble(), 0.75);
        QCOMPARE(adj->getAngstrom(350.0).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAngstrom(650.0).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 10.0);
        QCOMPARE(adj->getAdjusted(550.0).toDouble(), 8.0);
        QCOMPARE(adj->getAdjusted(460.0).toDouble(), 9.83650940587021);
        QCOMPARE(adj->getAdjusted(560.0).toDouble(), 7.89261618715919);
        QCOMPARE(adj->getAdjusted(300.0).toDouble(), 15.6968133850279);
        QCOMPARE(adj->getAdjusted(700.0).toDouble(), 6.11822519087006);

        {
            WavelengthAdjust *adj2;
            {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << adj;
                }
                {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    stream >> adj2;
                }
            }
            QCOMPARE(adj2->getAllUnits(), (SequenceName::Set{BsB, BsG, BsR, ZA1, ZA2}));

            QCOMPARE(adj2->getAngstrom(500.0).toDouble(), 0.75);
            QCOMPARE(adj2->getAngstrom(450.0).toDouble(), 0.75);
            QCOMPARE(adj2->getAngstrom(450.0, Wavelength::BracketingExactBehavior::ExcludeExact,
                                       25.0).toDouble(),
                     1.11198872760321);
            QCOMPARE(adj2->getAngstrom(550.0).toDouble(), 0.75);
            QCOMPARE(adj2->getAngstrom(350.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(650.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAdjusted(450.0).toDouble(), 10.0);
            QCOMPARE(adj2->getAdjusted(550.0).toDouble(), 8.0);
            QCOMPARE(adj2->getAdjusted(460.0).toDouble(), 9.83650940587021);
            QCOMPARE(adj2->getAdjusted(560.0).toDouble(), 7.89261618715919);
            QCOMPARE(adj2->getAdjusted(300.0).toDouble(), 15.6968133850279);
            QCOMPARE(adj2->getAdjusted(700.0).toDouble(), 6.11822519087006);

            *adj2 = *adj;
            QCOMPARE(adj2->getAngstrom(500.0).toDouble(), 0.75);
            QCOMPARE(adj2->getAngstrom(450.0).toDouble(), 0.75);
            QCOMPARE(adj2->getAngstrom(450.0, Wavelength::BracketingExactBehavior::ExcludeExact,
                                       25.0).toDouble(),
                     1.11198872760321);
            QCOMPARE(adj2->getAngstrom(550.0).toDouble(), 0.75);
            QCOMPARE(adj2->getAngstrom(350.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(650.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAdjusted(450.0).toDouble(), 10.0);
            QCOMPARE(adj2->getAdjusted(550.0).toDouble(), 8.0);
            QCOMPARE(adj2->getAdjusted(460.0).toDouble(), 9.83650940587021);
            QCOMPARE(adj2->getAdjusted(560.0).toDouble(), 7.89261618715919);
            QCOMPARE(adj2->getAdjusted(300.0).toDouble(), 15.6968133850279);
            QCOMPARE(adj2->getAdjusted(700.0).toDouble(), 6.11822519087006);

            WavelengthAdjust adj3(*adj2);
            delete adj2;

            QCOMPARE(adj3.getAngstrom(500.0).toDouble(), 0.75);
            QCOMPARE(adj3.getAngstrom(450.0).toDouble(), 0.75);
            QCOMPARE(
                    adj3.getAngstrom(450.0, Wavelength::BracketingExactBehavior::ExcludeExact, 25.0)
                        .toDouble(),
                    1.11198872760321);
            QCOMPARE(adj3.getAngstrom(550.0).toDouble(), 0.75);
            QCOMPARE(adj3.getAngstrom(350.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj3.getAngstrom(650.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj3.getAdjusted(450.0).toDouble(), 10.0);
            QCOMPARE(adj3.getAdjusted(550.0).toDouble(), 8.0);
            QCOMPARE(adj3.getAdjusted(460.0).toDouble(), 9.83650940587021);
            QCOMPARE(adj3.getAdjusted(560.0).toDouble(), 7.89261618715919);
            QCOMPARE(adj3.getAdjusted(300.0).toDouble(), 15.6968133850279);
            QCOMPARE(adj3.getAdjusted(700.0).toDouble(), 6.11822519087006);
        }

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(12.0));
        data.setValue(ZA1, Variant::Root(0.75));
        data.setValue(ZA2, Variant::Root(0.5));
        data.setStart(20.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAngstrom(500.0).toDouble(), 0.75);
        QCOMPARE(adj->getAngstrom(700.0).toDouble(), 0.5);
        QCOMPARE(adj->getAngstrom(300.0).toDouble(), 0.5);
        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 12.0);
        QCOMPARE(adj->getAdjusted(300.0).toDouble(), 14.6969384566991);
        QCOMPARE(adj->getAdjusted(460.0).toDouble(), 11.8038112870443);

        delete adj;
    }

    void dynamicAngstromFallback()
    {
        WavelengthAdjust *adj = WavelengthAdjust::fromInput(
                new DynamicSequenceSelection::Match(QString(), QString(), "Bs[BGR]_S11"),
                new BaselineLatest);

        adj->addFallbackAngstrom(new DynamicInput::Constant(1.5));

        SequenceName BsB("bnd", "raw", "BsB_S11");
        SequenceName BsG("bnd", "raw", "BsG_S11");
        SequenceName BsR("bnd", "raw", "BsR_S11");

        QVERIFY(!adj->registerInput(SequenceName("bnd", "raw", "BaG_A11")));
        QVERIFY(adj->registerInput(BsB));
        QVERIFY(adj->registerInput(BsG));
        QVERIFY(adj->registerInput(BsR));
        QCOMPARE(adj->getAllUnits(), (SequenceName::Set{BsB, BsG, BsR}));

        SequenceSegment data;
        Variant::Root meta;
        meta.write().metadataReal("Wavelength").setDouble(450.0);
        data.setValue(BsB.toMeta(), meta);
        meta.write().setEmpty();
        meta.write().metadataReal("Wavelength").setDouble(550.0);
        data.setValue(BsG.toMeta(), meta);
        meta.write().setEmpty();
        meta.write().metadataReal("Wavelength").setDouble(700.0);
        data.setValue(BsR.toMeta(), meta);
        data.setStart(10.0);
        data.setEnd(FP::undefined());
        adj->incomingMeta(data);

        QCOMPARE(adj->getWavelength(BsB), 450.0);
        QCOMPARE(adj->getWavelength(BsG), 550.0);
        QCOMPARE(adj->getWavelength(BsR), 700.0);

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(8.0));
        data.setValue(BsR, Variant::Root(6.0));
        data.setStart(10.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAngstrom(450.0).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAngstrom(400.0).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAngstrom(500.0).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAngstrom(550.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(600.0).toDouble(), 1.19289939822588);
        QCOMPARE(adj->getAngstrom(700.0).toDouble(), 1.19289939822588);
        QCOMPARE(adj->getAngstrom(750.0).toDouble(), 1.19289939822588);
        QCOMPARE(adj->getAngstrom(BsB).toDouble(), 1.11198872760321);
        QCOMPARE(adj->getAngstrom(BsG).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(BsR).toDouble(), 1.19289939822588);

        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 10.0);
        QCOMPARE(adj->getAdjusted(550.0).toDouble(), 8.0);
        QCOMPARE(adj->getAdjusted(700.0).toDouble(), 6.0);
        QCOMPARE(adj->getAdjusted(400.0).toDouble(), 11.3993746763798);
        QCOMPARE(adj->getAdjusted(500.0).toDouble(), 8.89443132640780);
        QCOMPARE(adj->getAdjusted(600.0).toDouble(), 7.21127463252649);
        QCOMPARE(adj->getAdjusted(750.0).toDouble(), 5.52596511915908);

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(FP::undefined()));
        data.setValue(BsG, Variant::Root(8.0));
        data.setValue(BsR, Variant::Root(FP::undefined()));
        data.setStart(11.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAngstrom(450.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(400.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(500.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(550.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(600.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(700.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(750.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(BsB).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(BsG).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(BsR).toDouble(), 1.5);

        QVERIFY(!FP::defined(
                adj->getAngstrom(450.0, Wavelength::BracketingExactBehavior::ExcludeExact,
                                 FP::undefined(), true)
                   .toReal()));

        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 10.8097400574546);
        QCOMPARE(adj->getAdjusted(550.0).toDouble(), 8.0);
        QCOMPARE(adj->getAdjusted(700.0).toDouble(), 5.57169020840406);
        QCOMPARE(adj->getAdjusted(400.0).toDouble(), 12.8986433395144);
        QCOMPARE(adj->getAdjusted(500.0).toDouble(), 9.22951786389734);
        QCOMPARE(adj->getAdjusted(600.0).toDouble(), 7.02113212354648);
        QCOMPARE(adj->getAdjusted(750.0).toDouble(), 5.02391318632236);

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(8.0));
        data.setValue(BsR, Variant::Root(6.0));
        data.setStart(12.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        {
            WavelengthAdjust *adj2;
            {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << adj;
                }
                {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    stream >> adj2;
                }
            }
            QCOMPARE(adj2->getAllUnits(), (SequenceName::Set{BsB, BsG, BsR}));

            QCOMPARE(adj2->getAngstrom(450.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(400.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(500.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAdjusted(400.0).toDouble(), 11.3993746763798);
            QCOMPARE(adj2->getAdjusted(500.0).toDouble(), 8.89443132640780);
            QCOMPARE(adj2->getAdjusted(600.0).toDouble(), 7.21127463252649);
            QCOMPARE(adj2->getAdjusted(750.0).toDouble(), 5.52596511915908);

            *adj2 = *adj;
            QCOMPARE(adj2->getAngstrom(450.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(400.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAngstrom(500.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj2->getAdjusted(400.0).toDouble(), 11.3993746763798);
            QCOMPARE(adj2->getAdjusted(500.0).toDouble(), 8.89443132640780);
            QCOMPARE(adj2->getAdjusted(600.0).toDouble(), 7.21127463252649);
            QCOMPARE(adj2->getAdjusted(750.0).toDouble(), 5.52596511915908);

            WavelengthAdjust adj3(*adj2);
            delete adj2;

            QCOMPARE(adj3.getAngstrom(450.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj3.getAngstrom(400.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj3.getAngstrom(500.0).toDouble(), 1.11198872760321);
            QCOMPARE(adj3.getAdjusted(400.0).toDouble(), 11.3993746763798);
            QCOMPARE(adj3.getAdjusted(500.0).toDouble(), 8.89443132640780);
            QCOMPARE(adj3.getAdjusted(600.0).toDouble(), 7.21127463252649);
            QCOMPARE(adj3.getAdjusted(750.0).toDouble(), 5.52596511915908);
        }

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(FP::undefined()));
        data.setValue(BsG, Variant::Root(8.0));
        data.setValue(BsR, Variant::Root(FP::undefined()));
        data.setStart(13.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAngstrom(450.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(400.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(500.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(550.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(600.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(700.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(750.0).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(BsB).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(BsG).toDouble(), 1.5);
        QCOMPARE(adj->getAngstrom(BsR).toDouble(), 1.5);

        QVERIFY(!FP::defined(
                adj->getAngstrom(450.0, Wavelength::BracketingExactBehavior::ExcludeExact,
                                 FP::undefined(), true)
                   .toReal()));

        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 10.8097400574546);
        QCOMPARE(adj->getAdjusted(550.0).toDouble(), 8.0);
        QCOMPARE(adj->getAdjusted(700.0).toDouble(), 5.57169020840406);
        QCOMPARE(adj->getAdjusted(400.0).toDouble(), 12.8986433395144);
        QCOMPARE(adj->getAdjusted(500.0).toDouble(), 9.22951786389734);
        QCOMPARE(adj->getAdjusted(600.0).toDouble(), 7.02113212354648);
        QCOMPARE(adj->getAdjusted(750.0).toDouble(), 5.02391318632236);

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(-8.0));
        data.setValue(BsR, Variant::Root(6.0));
        data.setStart(20.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        QCOMPARE(adj->getAngstrom(400.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(450.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(550.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(600.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(750.0).toDouble(), 1.15615155538170);
        QCOMPARE(adj->getAngstrom(800.0).toDouble(), 1.15615155538170);

        QCOMPARE(adj->getAdjusted(450.0).toDouble(), 10.0);
        QCOMPARE(adj->getAdjusted(550.0).toDouble(), -8.0);
        QCOMPARE(adj->getAdjusted(700.0).toDouble(), 6.0);
        QCOMPARE(adj->getAdjusted(400.0).toDouble(), 19.0);
        QCOMPARE(adj->getAdjusted(500.0).toDouble(), 1.0);
        QCOMPARE(adj->getAdjusted(600.0).toDouble(), -3.33333333333333);
        QCOMPARE(adj->getAdjusted(750.0).toDouble(), 10.6666666666667);

        data = SequenceSegment();
        data.setValue(BsB, Variant::Root(10.0));
        data.setValue(BsG, Variant::Root(-5.0));
        data.setValue(BsR, Variant::Root(-6.0));
        data.setStart(20.0);
        data.setEnd(FP::undefined());
        adj->incoming(data);

        delete adj;
    }

    void wavelengthTracking()
    {
        WavelengthTracker track;

        SequenceName BsB("bnd", "raw", "BsB_S11");
        SequenceName BsG("bnd", "raw", "BsG_S11");
        SequenceName BsR("bnd", "raw", "BsR_S11");

        SequenceSegment data;
        Variant::Root meta;
        meta.write().metadataReal("Wavelength").setDouble(450.0);
        data.setValue(BsB.toMeta(), meta);
        meta.write().setEmpty();
        meta.write().metadataReal("Wavelength").setDouble(550.0);
        data.setValue(BsG.toMeta(), meta);
        meta.write().setEmpty();
        meta.write().metadataReal("Wavelength").setDouble(700.0);
        data.setValue(BsR.toMeta(), meta);
        data.setStart(10.0);
        data.setEnd(FP::undefined());
        track.incomingMeta(data, SequenceName::Set{BsB, BsG, BsR});

        QCOMPARE(track.get(BsB), 450.0);
        QCOMPARE(track.get(BsG), 550.0);
        QCOMPARE(track.get(BsR), 700.0);
        QVERIFY(!FP::defined(track.get(SequenceName("bnd", "raw", "T_S11"))));

        {
            WavelengthTracker track2;
            {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << track;
                }
                {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    stream >> track2;
                }
            }

            QCOMPARE(track2.get(BsB), 450.0);
            QCOMPARE(track2.get(BsG), 550.0);
            QCOMPARE(track2.get(BsR), 700.0);
        }
    }
};

QTEST_MAIN(TestWavelengthAdjust)

#include "wavelengthadjust.moc"
