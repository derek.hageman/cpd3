/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QObject>
#include <QTest>

#include "editing/triggerinput.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

class TestTriggerInput : public QObject {
Q_OBJECT
private slots:

    void constant()
    {
        TriggerInput *i = new TriggerInputConstant(1.0);

        QVERIFY(i->isReady(100.0));
        QVERIFY(i->isReady(200.0));

        QVector<TriggerInput::Result> r(i->process());
        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, 1.0);

        TriggerInput *j = i->clone();
        delete i;
        r = j->process();
        r << j->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, 1.0);

        delete j;
    }

    void functionSin()
    {
        TriggerInput *i = new TriggerInputSin(new TriggerInputConstant(1.0));

        QVERIFY(i->isReady(100.0));
        QVERIFY(i->isReady(200.0));

        QVector<TriggerInput::Result> r(i->process());
        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, sin(1.0));

        TriggerInput *j = i->clone();
        delete i;
        r = j->process();
        r << j->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, sin(1.0));

        delete j;
    }

    void functionCos()
    {
        TriggerInput *i = new TriggerInputCos(new TriggerInputConstant(1.0));

        QVERIFY(i->isReady(100.0));
        QVERIFY(i->isReady(200.0));

        QVector<TriggerInput::Result> r(i->process());
        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, cos(1.0));

        TriggerInput *j = i->clone();
        delete i;
        r = j->process();
        r << j->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, cos(1.0));

        delete j;
    }

    void functionLog()
    {
        TriggerInput *i = new TriggerInputLog(new TriggerInputConstant(3.0));

        QVERIFY(i->isReady(100.0));
        QVERIFY(i->isReady(200.0));

        QVector<TriggerInput::Result> r(i->process());
        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, log(3.0));

        TriggerInput *j = i->clone();
        delete i;
        r = j->process();
        r << j->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, log(3.0));

        delete j;
    }

    void functionLog10()
    {
        TriggerInput *i = new TriggerInputLog10(new TriggerInputConstant(3.0));

        QVERIFY(i->isReady(100.0));
        QVERIFY(i->isReady(200.0));

        QVector<TriggerInput::Result> r(i->process());
        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, log10(3.0));

        TriggerInput *j = i->clone();
        delete i;
        r = j->process();
        r << j->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, log10(3.0));

        delete j;
    }

    void functionExp()
    {
        TriggerInput *i = new TriggerInputExp(new TriggerInputConstant(3.0));

        QVERIFY(i->isReady(100.0));
        QVERIFY(i->isReady(200.0));

        QVector<TriggerInput::Result> r(i->process());
        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, exp(3.0));

        TriggerInput *j = i->clone();
        delete i;
        r = j->process();
        r << j->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, exp(3.0));

        delete j;
    }

    void functionAbs()
    {
        TriggerInput *i = new TriggerInputAbs(new TriggerInputConstant(-2.0));

        QVERIFY(i->isReady(100.0));
        QVERIFY(i->isReady(200.0));

        QVector<TriggerInput::Result> r(i->process());
        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, 2.0);

        TriggerInput *j = i->clone();
        delete i;
        r = j->process();
        r << j->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, 2.0);

        delete j;
    }

    void functionPoly()
    {
        TriggerInput *i = new TriggerInputPoly(new TriggerInputConstant(1.0),
                                               Calibration(QVector<double>() << 0.25 << 0.5));
        TriggerInput *ii = new TriggerInputPolyInvert(new TriggerInputConstant(0.75), Calibration(
                QVector<double>() << 0.25 << 0.5));

        QVERIFY(i->isReady(100.0));
        QVERIFY(i->isReady(200.0));

        QVector<TriggerInput::Result> r(i->process());
        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, 0.75);

        r = ii->process();
        ii->finalize();
        r << ii->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, 1.0);

        TriggerInput *j = i->clone();
        delete i;
        r = j->process();
        r << j->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, 0.75);

        delete j;

        j = ii->clone();
        delete ii;
        r = j->process();
        r << j->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(!FP::defined(r[0].start));
        QVERIFY(!FP::defined(r[0].end));
        QCOMPARE(r[0].value, 1.0);

        delete j;
    }

    void valueInput()
    {
        SequenceName u("bnd", "raw", "BsG_S11");
        TriggerInput *i = new TriggerInputValue(SequenceMatch::OrderedLookup(u));
        QVector<TriggerInput::Result> r;
        QVector<TriggerInput::Result> e;

        QList<EditDataTarget *> target(i->unhandledInput(u));
        target.append(i->unhandledInput(u.withFlavor("pm1")));
        QCOMPARE(target.size(), 1);

        r = i->process();
        QVERIFY(!i->isReady(100));
        target[0]->incomingSequenceValue(SequenceValue(u, Variant::Root(1.0), 100, 200));
        i->incomingDataAdvance(100);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));

        target[0]->incomingSequenceValue(SequenceValue(u, Variant::Root(2.0), 200, 300));
        i->incomingDataAdvance(200);
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(i->isReady(101));
        QVERIFY(!i->isReady(201));

        i->incomingDataAdvance(201);
        r << i->process();
        QCOMPARE(r.size(), 2);
        QVERIFY(i->isReady(201));

        target[0]->incomingSequenceValue(SequenceValue(u, Variant::Root(3.0), 300, 400));
        i->incomingDataAdvance(300);
        r << i->process();
        QCOMPARE(r.size(), 2);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->isReady(301));

        i->incomingDataAdvance(400);
        QVERIFY(i->isReady(400));
        QVERIFY(!i->isReady(401));
        r << i->process();
        QCOMPARE(r.size(), 3);

        target[0]->incomingSequenceValue(SequenceValue(u, Variant::Root(4.0), 400, 500));
        i->incomingDataAdvance(400);
        r << i->process();
        QCOMPARE(r.size(), 3);
        QVERIFY(i->isReady(400));
        QVERIFY(!i->isReady(401));

        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 4);
        QVERIFY(i->isReady(700));

        e <<
                TriggerInput::Result(100.0, 200.0, 1.0) <<
                TriggerInput::Result(200.0, 300.0, 2.0) <<
                TriggerInput::Result(300.0, 400.0, 3.0) <<
                TriggerInput::Result(400.0, 500.0, 4.0);
        QCOMPARE(r, e);


        TriggerInput *j = i->clone();
        delete i;
        i = j;

        target = i->unhandledInput(u);
        target.append(i->unhandledInput(u.withFlavor("pm1")));
        QCOMPARE(target.size(), 1);

        r = i->process();
        QVERIFY(!i->isReady(100));
        target[0]->incomingSequenceValue(SequenceValue(u, Variant::Root(1.0), 100, 200));
        i->incomingDataAdvance(100);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(i->isReady(100));
        QVERIFY(!i->isReady(101));

        target[0]->incomingSequenceValue(SequenceValue(u, Variant::Root(2.0), 200, 300));
        i->incomingDataAdvance(200);
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(i->isReady(101));
        QVERIFY(!i->isReady(201));

        i->incomingDataAdvance(201);
        r << i->process();
        QCOMPARE(r.size(), 2);
        QVERIFY(i->isReady(201));

        target[0]->incomingSequenceValue(SequenceValue(u, Variant::Root(3.0), 300, 400));
        i->incomingDataAdvance(300);
        r << i->process();
        QCOMPARE(r.size(), 2);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->isReady(301));

        i->incomingDataAdvance(400);
        QVERIFY(i->isReady(400));
        QVERIFY(!i->isReady(401));
        r << i->process();
        QCOMPARE(r.size(), 3);

        target[0]->incomingSequenceValue(SequenceValue(u, Variant::Root(4.0), 400, 500));
        i->incomingDataAdvance(400);
        r << i->process();
        QCOMPARE(r.size(), 3);
        QVERIFY(i->isReady(400));
        QVERIFY(!i->isReady(401));

        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 4);
        QVERIFY(i->isReady(700));

        delete i;

        QCOMPARE(r, e);
    }
};

QTEST_MAIN(TestTriggerInput)

#include "triggerinput.moc"
