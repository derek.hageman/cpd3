/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/actionflags.hxx"
#include "datacore/stream.hxx"
#include "datacore/staticmultiplexer.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

class Dispatch : public EditDispatchTarget {
    Action *action;

    struct Target {
        QList<EditDataTarget *> targets;
        QList<EditDataModification *> modifiers;
        int muxId;

        Target() : targets(), modifiers(), muxId(0)
        { }
    };

    QHash<SequenceName, Target> d;

    StaticMultiplexer outputMux;

    void handleCompleted(const SequenceValue::Transfer &add)
    {
        Util::append(add, output);
    }

public:
    SequenceValue::Transfer output;

    Dispatch() : action(NULL), d(), outputMux()
    { }

    virtual ~Dispatch()
    { }

    void initialize(Action *a)
    {
        output.clear();
        d.clear();
        action = a;

        outputMux.clear(true);
        outputMux.setStreams(2);

        action->setTarget(this);
    }

    void value(SequenceValue value)
    {
        QHash<SequenceName, Target>::iterator target = d.find(value.getUnit());
        if (target == d.end()) {
            QList<EditDataTarget *> inputs;
            QList<EditDataTarget *> outputs;
            QList<EditDataModification *> modifiers;

            action->unhandledInput(value.getUnit(), inputs, outputs, modifiers);

            Target add;
            add.targets = inputs;
            add.targets.append(outputs);
            add.modifiers = modifiers;

            if (outputs.isEmpty()) {
                add.muxId = 0;
            } else {
                add.muxId = -1;
            }

            target = d.insert(value.getUnit(), add);
        }

        for (QList<EditDataModification *>::const_iterator
                m = target.value().modifiers.constBegin(),
                end = target.value().modifiers.constEnd(); m != end; ++m) {
            (*m)->modifySequenceValue(value);
        }
        for (QList<EditDataTarget *>::const_iterator t = target.value().targets.constBegin(),
                end = target.value().targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(value);
        }
        if (target.value().muxId >= 0) {
            handleCompleted(outputMux.incoming(target.value().muxId, value));
        }
    }

    void value(const SequenceName &unit, const Variant::Read &v, double start, double end)
    { value(SequenceValue(unit, Variant::Root(v), start, end)); }

    void value(const SequenceName &unit, const Variant::Flags &v, double start, double end)
    { value(SequenceValue(unit, Variant::Root(v), start, end)); }

    void value(const SequenceName &unit, double v, double start, double end)
    { value(SequenceValue(unit, Variant::Root(v), start, end)); }

    void advance(double time)
    {
        action->incomingDataAdvance(time);
        handleCompleted(outputMux.advance(0, time));
    }

    void finish()
    {
        action->finalize();
        handleCompleted(outputMux.finish());
    }

    void incomingData(const Data::SequenceValue &value) override
    { handleCompleted(outputMux.incoming(1, value)); }

    void incomingData(const Data::SequenceValue::Transfer &values) override
    { handleCompleted(outputMux.incoming(1, values)); }

    void incomingAdvance(double time) override
    { handleCompleted(outputMux.advance(1, time)); }

    bool compare(const SequenceValue::Transfer &expected)
    {
        if (expected.size() != output.size())
            return false;

        for (const auto &check : expected) {
            int idx;
            for (idx = output.size() - 1; idx >= 0; --idx) {
                SequenceValue compare(output.at(idx));
                if (compare.getUnit() != check.getUnit())
                    continue;
                if (!FP::equal(compare.getStart(), check.getStart()))
                    continue;
                if (!FP::equal(compare.getEnd(), check.getEnd()))
                    continue;
                if (compare.getValue() != check.getValue())
                    continue;
                output.erase(output.begin() + idx);
                break;
            }
            if (idx < 0)
                return false;
        }

        return output.empty();
    }
};

class TestActionFlags : public QObject {
Q_OBJECT
private slots:

    void addFlag()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionAddFlag(SequenceMatch::Composite(a), Variant::Flags{"C", "D"},
                                        Variant::Root("Foobar"));
        Dispatch d;

        d.initialize(act);

        Variant::Root metaIn;
        metaIn.write().metadataFlags("A").setString("1111");
        metaIn.write().metadataSingleFlag("B").hash("Description").setString("2222");
        metaIn.write().metadataSingleFlag("C").hash("Origin").toArray().after_back().setString("3");

        Variant::Root metaOut = metaIn;
        metaOut.write().metadataSingleFlag("C").hash("Description").setString("Foobar");
        metaOut.write()
               .metadataSingleFlag("C")
               .hash("Origin")
               .toArray()
               .after_back()
               .setString("editing");
        metaOut.write().metadataSingleFlag("D").hash("Description").setString("Foobar");
        metaOut.write()
               .metadataSingleFlag("D")
               .hash("Origin")
               .toArray()
               .after_back()
               .setString("editing");

        d.value(a.toMeta(), metaIn, 100, 200);
        d.value(b.toMeta(), metaIn, 100, 200);
        d.advance(100);

        d.value(a, Variant::Flags{"A"}, 200, 300);
        d.value(b, 1.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a.toMeta(), metaOut, 100, 200),
                                                  SequenceValue(b.toMeta(), metaIn, 100, 200),
                                                  SequenceValue(a, Variant::Root(
                                                          Variant::Flags{"A", "C", "D"}), 200, 300),
                                                  SequenceValue(b, Variant::Root(1.0), 300, 400)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a.toMeta(), metaIn, 100, 200);
        d.value(b.toMeta(), metaIn, 100, 200);
        d.advance(100);

        d.value(a, Variant::Flags{"A"}, 200, 300);
        d.value(b, 1.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a.toMeta(), metaOut, 100, 200),
                                                  SequenceValue(b.toMeta(), metaIn, 100, 200),
                                                  SequenceValue(a, Variant::Root(
                                                          Variant::Flags{"A", "C", "D"}), 200, 300),
                                                  SequenceValue(b, Variant::Root(1.0), 300, 400)}));

        delete act;
    }

    void removeFlag()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionRemoveFlag(SequenceMatch::Composite(a), Variant::Flags{"C", "D"});
        Dispatch d;

        d.initialize(act);

        d.value(a, Variant::Flags{"A"}, 200, 300);
        d.value(a, Variant::Flags{"A", "C"}, 300, 400);
        d.value(a, Variant::Flags{"A", "C", "D", "E"}, 400, 500);
        d.value(b, 1.0, 400, 500);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{
                SequenceValue(a, Variant::Root(Variant::Flags{"A"}), 200, 300),
                SequenceValue(a, Variant::Root(Variant::Flags{"A"}), 300, 400),
                SequenceValue(a, Variant::Root(Variant::Flags{"A", "E"}), 400, 500),
                SequenceValue(b, Variant::Root(1.0), 400, 500)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, Variant::Flags{"A"}, 200, 300);
        d.value(a, Variant::Flags{"A", "C"}, 300, 400);
        d.value(a, Variant::Flags{"A", "C", "D", "E"}, 400, 500);
        d.value(b, 1.0, 400, 500);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{
                SequenceValue(a, Variant::Root(Variant::Flags{"A"}), 200, 300),
                SequenceValue(a, Variant::Root(Variant::Flags{"A"}), 300, 400),
                SequenceValue(a, Variant::Root(Variant::Flags{"A", "E"}), 400, 500),
                SequenceValue(b, Variant::Root(1.0), 400, 500)}));

        delete act;
    }

    void removeMatchingFlag()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionRemoveMatchingFlags(SequenceMatch::Composite(a),
                                                    QList<QRegExp>() << QRegExp("(C|D)"), true);
        Dispatch d;

        d.initialize(act);

        d.value(a, Variant::Flags{"A"}, 200, 300);
        d.value(a, Variant::Flags{"A", "C"}, 300, 400);
        d.value(a, Variant::Flags{"A", "C", "D", "E"}, 400, 500);
        d.value(b, 1.0, 400, 500);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{
                SequenceValue(a, Variant::Root(Variant::Flags{"A"}), 200, 300),
                SequenceValue(a, Variant::Root(Variant::Flags{"A"}), 300, 400),
                SequenceValue(a, Variant::Root(Variant::Flags{"A", "E"}), 400, 500),
                SequenceValue(b, Variant::Root(1.0), 400, 500)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, Variant::Flags{"A"}, 200, 300);
        d.value(a, Variant::Flags{"A", "C"}, 300, 400);
        d.value(a, Variant::Flags{"A", "C", "D", "E"}, 400, 500);
        d.value(b, 1.0, 400, 500);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{
                SequenceValue(a, Variant::Root(Variant::Flags{"A"}), 200, 300),
                SequenceValue(a, Variant::Root(Variant::Flags{"A"}), 300, 400),
                SequenceValue(a, Variant::Root(Variant::Flags{"A", "E"}), 400, 500),
                SequenceValue(b, Variant::Root(1.0), 400, 500)}));

        delete act;
    }

    void removeMatchingFlagMeta()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        Action *act = new ActionRemoveMatchingFlags(SequenceMatch::Composite(a),
                                                    QList<QRegExp>() << QRegExp("(C|D)"), false);
        Dispatch d;

        Variant::Root meta;
        meta.write().metadataSingleFlag("A").hash("Description").setString("1");
        meta.write().metadataSingleFlag("C").hash("Description").setString("2");
        meta.write().metadataSingleFlag("D").hash("Description").setString("3");
        meta.write().metadataSingleFlag("E").hash("Description").setString("4");

        d.initialize(act);

        d.value(a.toMeta(), meta, 100, 1000);
        d.value(a, Variant::Flags{"A"}, 200, 300);
        d.value(a, Variant::Flags{"A", "C"}, 300, 400);
        d.value(a, Variant::Flags{"A", "C", "D", "E"}, 400, 500);
        d.value(b, 1.0, 400, 500);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a.toMeta(), meta, 100, 1000),
                                                  SequenceValue(a,
                                                                Variant::Root(Variant::Flags{"A"}),
                                                                200, 300), SequenceValue(a,
                                                                                         Variant::Root(
                                                                                                 Variant::Flags{
                                                                                                         "A"}),
                                                                                         300, 400),
                                                  SequenceValue(a, Variant::Root(
                                                          Variant::Flags{"A", "E"}), 400, 500),
                                                  SequenceValue(b, Variant::Root(1.0), 400, 500)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a.toMeta(), meta, 100, 1000);
        d.value(a, Variant::Flags{"A"}, 200, 300);
        d.value(a, Variant::Flags{"A", "C"}, 300, 400);
        d.value(a, Variant::Flags{"A", "C", "D", "E"}, 400, 500);
        d.value(b, 1.0, 400, 500);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a.toMeta(), meta, 100, 1000),
                                                  SequenceValue(a,
                                                                Variant::Root(Variant::Flags{"A"}),
                                                                200, 300), SequenceValue(a,
                                                                                         Variant::Root(
                                                                                                 Variant::Flags{
                                                                                                         "A"}),
                                                                                         300, 400),
                                                  SequenceValue(a, Variant::Root(
                                                          Variant::Flags{"A", "E"}), 400, 500),
                                                  SequenceValue(b, Variant::Root(1.0), 400, 500)}));

        delete act;
    }
};

QTEST_MAIN(TestActionFlags)

#include "actionflags.moc"
