/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/triggerperiodic.hxx"

using namespace CPD3;
using namespace CPD3::Editing;

class TestTriggerPeriodic : public QObject {
Q_OBJECT
private slots:

    void moment()
    {
        Trigger *i = new TriggerPeriodicMoment(Time::Minute, 1, true, Time::Second,
                                               QSet<int>() << 1 << 2 << 10 << 30);

        QVERIFY(i->isReady(60));

        QVERIFY(!i->appliesTo(0, 1));
        QVERIFY(i->appliesTo(1, 2));
        QVERIFY(i->appliesTo(1, 4));
        QVERIFY(i->appliesTo(1, 5));
        QVERIFY(i->appliesTo(1, 120));
        QVERIFY(i->appliesTo(2, 3));
        QVERIFY(!i->appliesTo(3, 4));
        QVERIFY(!i->appliesTo(4, 9));
        QVERIFY(!i->appliesTo(9, 10));
        QVERIFY(i->appliesTo(10, 30));
        QVERIFY(i->appliesTo(30, 61));
        QVERIFY(!i->appliesTo(32, 60));
        QVERIFY(i->appliesTo(55, 62));
        QVERIFY(i->appliesTo(55, 65));
        QVERIFY(i->appliesTo(55, 120));
        QVERIFY(i->appliesTo(61, 62));
        QVERIFY(i->appliesTo(61, 300));
        QVERIFY(i->appliesTo(70, 300));
        QVERIFY(i->appliesTo(70, 71));
        QVERIFY(i->appliesTo(70, 90));
        QVERIFY(i->appliesTo(71, 300));
        QVERIFY(!i->appliesTo(71, 80));

        i->finalize();

        Trigger *j = i->clone();
        delete i;
        i = j;

        QVERIFY(!i->appliesTo(0, 1));
        QVERIFY(i->appliesTo(1, 2));
        QVERIFY(i->appliesTo(1, 4));
        QVERIFY(i->appliesTo(1, 5));
        QVERIFY(i->appliesTo(1, 120));
        QVERIFY(i->appliesTo(2, 3));
        QVERIFY(!i->appliesTo(3, 4));
        QVERIFY(!i->appliesTo(4, 9));
        QVERIFY(!i->appliesTo(9, 10));
        QVERIFY(i->appliesTo(10, 30));
        QVERIFY(i->appliesTo(30, 61));
        QVERIFY(!i->appliesTo(32, 60));
        QVERIFY(i->appliesTo(55, 62));
        QVERIFY(i->appliesTo(55, 65));
        QVERIFY(i->appliesTo(55, 120));
        QVERIFY(i->appliesTo(61, 62));
        QVERIFY(i->appliesTo(61, 300));
        QVERIFY(i->appliesTo(70, 300));
        QVERIFY(i->appliesTo(70, 71));
        QVERIFY(i->appliesTo(70, 90));
        QVERIFY(i->appliesTo(71, 300));
        QVERIFY(!i->appliesTo(71, 80));

        delete i;
    }

    void rangeSimple()
    {
        Trigger *i = new TriggerPeriodicRange(Time::Minute, 1, true, Time::Second, 10, false,
                                              Time::Second, 20, false);

        QVERIFY(i->isReady(60));

        QVERIFY(!i->appliesTo(0, 1));
        QVERIFY(!i->appliesTo(1, 9));
        QVERIFY(i->appliesTo(10, 11));
        QVERIFY(i->appliesTo(10, 20));
        QVERIFY(i->appliesTo(10, 60));
        QVERIFY(i->appliesTo(10, 65));
        QVERIFY(i->appliesTo(10, 70));
        QVERIFY(i->appliesTo(10, 300));
        QVERIFY(i->appliesTo(11, 12));
        QVERIFY(i->appliesTo(11, 25));
        QVERIFY(!i->appliesTo(20, 21));
        QVERIFY(!i->appliesTo(30, 59));
        QVERIFY(!i->appliesTo(40, 65));
        QVERIFY(i->appliesTo(40, 71));
        QVERIFY(i->appliesTo(40, 90));
        QVERIFY(i->appliesTo(40, 300));
        QVERIFY(!i->appliesTo(50, 60));
        QVERIFY(!i->appliesTo(60, 70));
        QVERIFY(i->appliesTo(70, 71));
        QVERIFY(i->appliesTo(70, 80));
        QVERIFY(i->appliesTo(70, 100));

        i->finalize();

        Trigger *j = i->clone();
        delete i;
        i = j;

        QVERIFY(!i->appliesTo(0, 1));
        QVERIFY(!i->appliesTo(1, 9));
        QVERIFY(i->appliesTo(10, 11));
        QVERIFY(i->appliesTo(10, 20));
        QVERIFY(i->appliesTo(10, 60));
        QVERIFY(i->appliesTo(10, 65));
        QVERIFY(i->appliesTo(10, 70));
        QVERIFY(i->appliesTo(10, 300));
        QVERIFY(i->appliesTo(11, 12));
        QVERIFY(i->appliesTo(11, 25));
        QVERIFY(!i->appliesTo(20, 21));
        QVERIFY(!i->appliesTo(30, 59));
        QVERIFY(!i->appliesTo(40, 65));
        QVERIFY(i->appliesTo(40, 71));
        QVERIFY(i->appliesTo(40, 90));
        QVERIFY(i->appliesTo(40, 300));
        QVERIFY(!i->appliesTo(50, 60));
        QVERIFY(!i->appliesTo(60, 70));
        QVERIFY(i->appliesTo(70, 71));
        QVERIFY(i->appliesTo(70, 80));
        QVERIFY(i->appliesTo(70, 100));

        delete i;
    }

    void rangeReversed()
    {

        Trigger *i = new TriggerPeriodicRange(Time::Minute, 1, true, Time::Second, 50, false,
                                              Time::Second, 10, false);

        QVERIFY(i->isReady(60));

        QVERIFY(i->appliesTo(0, 10));
        QVERIFY(i->appliesTo(1, 9));
        QVERIFY(i->appliesTo(2, 59));
        QVERIFY(i->appliesTo(3, 61));
        QVERIFY(!i->appliesTo(10, 20));
        QVERIFY(!i->appliesTo(20, 50));
        QVERIFY(i->appliesTo(50, 51));
        QVERIFY(i->appliesTo(50, 60));
        QVERIFY(i->appliesTo(51, 61));
        QVERIFY(i->appliesTo(51, 72));
        QVERIFY(i->appliesTo(60, 61));
        QVERIFY(i->appliesTo(61, 75));
        QVERIFY(i->appliesTo(65, 70));
        QVERIFY(!i->appliesTo(70, 72));
        QVERIFY(i->appliesTo(71, 300));

        i->finalize();

        Trigger *j = i->clone();
        delete i;
        i = j;

        QVERIFY(i->appliesTo(0, 10));
        QVERIFY(i->appliesTo(1, 9));
        QVERIFY(i->appliesTo(2, 59));
        QVERIFY(i->appliesTo(3, 61));
        QVERIFY(!i->appliesTo(10, 20));
        QVERIFY(!i->appliesTo(20, 50));
        QVERIFY(i->appliesTo(50, 51));
        QVERIFY(i->appliesTo(50, 60));
        QVERIFY(i->appliesTo(51, 61));
        QVERIFY(i->appliesTo(51, 72));
        QVERIFY(i->appliesTo(60, 61));
        QVERIFY(i->appliesTo(61, 75));
        QVERIFY(i->appliesTo(65, 70));
        QVERIFY(!i->appliesTo(70, 72));
        QVERIFY(i->appliesTo(71, 300));

        delete i;
    }
};

QTEST_MAIN(TestTriggerPeriodic)

#include "triggerperiodic.moc"
