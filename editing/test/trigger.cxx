/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "editing/trigger.hxx"

using namespace CPD3;
using namespace CPD3::Editing;

class TestSegmenter : public TriggerSegmenting {
public:
    TestSegmenter()
    { }

    virtual ~TestSegmenter()
    { }

    virtual Trigger *clone() const
    {
        Q_ASSERT(false);
        return NULL;
    }

    void add(double start, double end, bool v)
    {
        emitSegment(start, end, v);
    }

    void add(double time)
    {
        emitAdvance(time);
    }
};

class TestTrigger : public QObject {
Q_OBJECT
private slots:

    void always()
    {
        Trigger *trigger = new TriggerAlways;

        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        {
            double s = 100.0;
            double e = 200.0;
            trigger->extendProcessing(s, e);
            QCOMPARE(s, 100.0);
            QCOMPARE(e, 200.0);
        }
        QVERIFY(trigger->isReady(200));
        QVERIFY(trigger->appliesTo(100, 200));
        trigger->advance(200.0);

        Trigger *cl = trigger->clone();
        delete trigger;
        trigger = cl;

        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(300));
        QVERIFY(trigger->appliesTo(200, 300));
        trigger->advance(300.0);

        trigger->setInverted(true);
        QVERIFY(trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(400));
        QVERIFY(!trigger->appliesTo(300, 400));
        trigger->advance(400.0);

        cl = trigger->clone();
        delete trigger;
        trigger = cl;

        QVERIFY(trigger->isNeverActive(FP::undefined(), FP::undefined()));
        {
            double s = 100.0;
            double e = 200.0;
            trigger->extendProcessing(s, e);
            QCOMPARE(s, 100.0);
            QCOMPARE(e, 200.0);
        }
        QVERIFY(trigger->isReady(500));
        QVERIFY(!trigger->appliesTo(400, 500));
        trigger->advance(400.0);

        delete trigger;
    }

    void compositeAND()
    {
        Trigger *trigger =
                new TriggerAND(QVector<Trigger *>() << (new TriggerAlways) << (new TriggerAlways));

        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        {
            double s = 100.0;
            double e = 200.0;
            trigger->extendProcessing(s, e);
            QCOMPARE(s, 100.0);
            QCOMPARE(e, 200.0);
        }
        QVERIFY(trigger->isReady(200));
        QVERIFY(trigger->appliesTo(100, 200));
        trigger->advance(200.0);

        Trigger *cl = trigger->clone();
        delete trigger;
        trigger = cl;

        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(300));
        QVERIFY(trigger->appliesTo(200, 300));
        trigger->advance(300.0);

        trigger->setInverted(true);
        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(400));
        QVERIFY(!trigger->appliesTo(300, 400));
        trigger->advance(400.0);

        cl = trigger->clone();
        delete trigger;
        trigger = cl;

        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        {
            double s = 100.0;
            double e = 200.0;
            trigger->extendProcessing(s, e);
            QCOMPARE(s, 100.0);
            QCOMPARE(e, 200.0);
        }
        QVERIFY(trigger->isReady(500));
        QVERIFY(!trigger->appliesTo(400, 500));
        trigger->advance(400.0);

        delete trigger;


        trigger = new TriggerAlways;
        trigger->setInverted(true);
        trigger = new TriggerAND(QVector<Trigger *>() << trigger << (new TriggerAlways));

        QVERIFY(trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(200));
        QVERIFY(!trigger->appliesTo(100, 200));
        trigger->advance(200.0);

        cl = trigger->clone();
        delete trigger;
        trigger = cl;

        QVERIFY(trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(300));
        QVERIFY(!trigger->appliesTo(200, 300));
        trigger->advance(300.0);

        trigger->setInverted(true);
        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(400));
        QVERIFY(trigger->appliesTo(300, 400));
        trigger->advance(400.0);

        cl = trigger->clone();
        delete trigger;
        trigger = cl;

        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(500));
        QVERIFY(trigger->appliesTo(400, 500));
        trigger->advance(400.0);

        delete trigger;
    }

    void compositeOR()
    {
        Trigger *trigger =
                new TriggerOR(QVector<Trigger *>() << (new TriggerAlways) << (new TriggerAlways));

        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        {
            double s = 100.0;
            double e = 200.0;
            trigger->extendProcessing(s, e);
            QCOMPARE(s, 100.0);
            QCOMPARE(e, 200.0);
        }
        QVERIFY(trigger->isReady(200));
        QVERIFY(trigger->appliesTo(100, 200));
        trigger->advance(200.0);

        Trigger *cl = trigger->clone();
        delete trigger;
        trigger = cl;

        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(300));
        QVERIFY(trigger->appliesTo(200, 300));
        trigger->advance(300.0);

        trigger->setInverted(true);
        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(400));
        QVERIFY(!trigger->appliesTo(300, 400));
        trigger->advance(400.0);

        cl = trigger->clone();
        delete trigger;
        trigger = cl;

        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        {
            double s = 100.0;
            double e = 200.0;
            trigger->extendProcessing(s, e);
            QCOMPARE(s, 100.0);
            QCOMPARE(e, 200.0);
        }
        QVERIFY(trigger->isReady(500));
        QVERIFY(!trigger->appliesTo(400, 500));
        trigger->advance(400.0);

        delete trigger;


        trigger = new TriggerAlways;
        trigger->setInverted(true);
        trigger = new TriggerOR(QVector<Trigger *>() << trigger);

        QVERIFY(trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(200));
        QVERIFY(!trigger->appliesTo(100, 200));
        trigger->advance(200.0);

        cl = trigger->clone();
        delete trigger;
        trigger = cl;

        QVERIFY(trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(300));
        QVERIFY(!trigger->appliesTo(200, 300));
        trigger->advance(300.0);

        trigger->setInverted(true);
        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(400));
        QVERIFY(trigger->appliesTo(300, 400));
        trigger->advance(400.0);

        cl = trigger->clone();
        delete trigger;
        trigger = cl;

        QVERIFY(!trigger->isNeverActive(FP::undefined(), FP::undefined()));
        QVERIFY(trigger->isReady(500));
        QVERIFY(trigger->appliesTo(400, 500));
        trigger->advance(400.0);

        delete trigger;
    }

    void segmenterEmpty()
    {
        TestSegmenter s;
        s.add(200);
        QVERIFY(s.isReady(200));
        QVERIFY(!s.isReady(201));
        QVERIFY(!s.appliesTo(FP::undefined(), 100));
        QVERIFY(!s.appliesTo(100, 200));
        s.advance(150);
        s.add(300);
        QVERIFY(s.isReady(201));
        QVERIFY(!s.isReady(301));
        QVERIFY(!s.appliesTo(200, 300));
        s.finalize();
        QVERIFY(s.isReady(400));
        QVERIFY(!s.appliesTo(300, 400));
        QVERIFY(!s.appliesTo(400, FP::undefined()));
    }

    void segmenterBasic()
    {
        TestSegmenter s;
        s.add(100, 200, true);
        QVERIFY(s.isReady(100));
        QVERIFY(!s.isReady(101));
        QVERIFY(!s.appliesTo(FP::undefined(), 100));
        QVERIFY(!s.appliesTo(50, 90));

        s.add(200, 300, true);
        QVERIFY(s.isReady(200));
        QVERIFY(!s.isReady(201));
        s.add(250);
        QVERIFY(s.isReady(201));
        QVERIFY(s.isReady(250));
        QVERIFY(!s.isReady(251));

        QVERIFY(s.appliesTo(50, 150));
        QVERIFY(s.appliesTo(100, 150));
        QVERIFY(s.appliesTo(101, 150));
        QVERIFY(s.appliesTo(150, 250));
        QVERIFY(s.appliesTo(200, 250));
        QVERIFY(s.appliesTo(201, 250));

        s.add(250, 400, false);
        QVERIFY(s.isReady(250));
        QVERIFY(!s.isReady(251));
        s.add(300);
        QVERIFY(s.isReady(300));

        QVERIFY(s.appliesTo(201, 250));
        QVERIFY(s.appliesTo(201, 300));
        QVERIFY(s.appliesTo(249, 300));
        QVERIFY(!s.appliesTo(250, 300));
        QVERIFY(!s.appliesTo(251, 300));
        s.advance(260);
        QVERIFY(!s.appliesTo(260, 290));

        s.add(400);
        s.add(400, 500, false);
        s.add(500, 600, true);
        s.add(550, 700, false);
        s.add(700, 800, true);
        s.add(800);
        QVERIFY(s.isReady(800));

        QVERIFY(!s.appliesTo(400, 500));
        QVERIFY(s.appliesTo(400, 501));
        QVERIFY(s.appliesTo(550, 750));
        QVERIFY(!s.appliesTo(550, 650));
        QVERIFY(s.appliesTo(701, 702));
    }

    void segmenterBulk()
    {
        TestSegmenter s;

        for (int i = 0; i < 1000; i++) {
            s.add((double) i, (double) i + 0.9, (i / 100) % 2 == 0);
        }

        QVERIFY(s.appliesTo(10, 50));
        QVERIFY(s.appliesTo(50, 100));
        QVERIFY(s.appliesTo(50, 150));
        QVERIFY(!s.appliesTo(100, 150));
        QVERIFY(!s.appliesTo(150, 200));
        QVERIFY(s.appliesTo(150, 250));

        QVERIFY(s.appliesTo(800, 890));
    }
};

QTEST_MAIN(TestTrigger)

#include "trigger.moc"
