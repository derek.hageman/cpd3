/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/directivecontainer.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

class TestDirectiveContainer : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        Variant::Root config;
        config["Parameters/Action/Type"].setString("Invalidate");
        DirectiveContainer
                cont(SequenceValue(SequenceName("brw", "edits", "aerosol"), config, 1000, 2000));

        QCOMPARE(cont.getStart(), 1000.0);
        QCOMPARE(cont.getEnd(), 2000.0);
        QVERIFY(cont.isActive());

        auto directives = cont.instantiate();
        QVERIFY(directives.size() > 0);

        QCOMPARE(directives.front()->getStart(), 1000.0);
        QCOMPARE(directives.front()->getEnd(), 2000.0);

        DirectiveContainer cont2(cont);

        QCOMPARE(cont2.getStart(), 1000.0);
        QCOMPARE(cont2.getEnd(), 2000.0);
        QVERIFY(cont2.isActive());

        directives = cont2.instantiate();
        QVERIFY(directives.size() > 0);

        QCOMPARE(directives.front()->getStart(), 1000.0);
        QCOMPARE(directives.front()->getEnd(), 2000.0);
    }

    void ebasFlag()
    {
        Variant::Root config;
        config["Parameters/Action/Type"].setString("Invalidate");
        config["Parameters/EBASFlags"].setString("EBASFlag990");
        DirectiveContainer
                cont(SequenceValue(SequenceName("brw", "edits", "aerosol"), config, 1000, 2000));

        QCOMPARE(cont.getStart(), 1000.0);
        QCOMPARE(cont.getEnd(), 2000.0);
        QVERIFY(cont.isActive());

        auto directives = cont.instantiate();
        QVERIFY(directives.size() > 1);

        QCOMPARE(directives.front()->getStart(), 1000.0);
        QCOMPARE(directives.front()->getEnd(), 2000.0);
    }
};

QTEST_MAIN(TestDirectiveContainer)

#include "directivecontainer.moc"
