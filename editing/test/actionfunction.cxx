/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/actionfunction.hxx"
#include "datacore/stream.hxx"
#include "datacore/staticmultiplexer.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

class Dispatch : public EditDispatchTarget {
    Action *action;

    struct Target {
        QList<EditDataTarget *> targets;
        QList<EditDataModification *> modifiers;
        int muxId;

        Target() : targets(), modifiers(), muxId(0)
        { }
    };

    QHash<SequenceName, Target> d;

    StaticMultiplexer outputMux;

    void handleCompleted(const SequenceValue::Transfer &add)
    {
        Util::append(add, output);
    }

public:
    SequenceValue::Transfer output;

    Dispatch() : action(NULL), d(), outputMux()
    { }

    virtual ~Dispatch()
    { }

    void initialize(Action *a)
    {
        output.clear();
        d.clear();
        action = a;

        outputMux.clear(true);
        outputMux.setStreams(2);

        action->setTarget(this);
    }

    void value(SequenceValue value)
    {
        QHash<SequenceName, Target>::iterator target = d.find(value.getUnit());
        if (target == d.end()) {
            QList<EditDataTarget *> inputs;
            QList<EditDataTarget *> outputs;
            QList<EditDataModification *> modifiers;

            action->unhandledInput(value.getUnit(), inputs, outputs, modifiers);

            Target add;
            add.targets = inputs;
            add.targets.append(outputs);
            add.modifiers = modifiers;

            if (outputs.isEmpty()) {
                add.muxId = 0;
            } else {
                add.muxId = -1;
            }

            target = d.insert(value.getUnit(), add);
        }

        for (QList<EditDataModification *>::const_iterator
                m = target.value().modifiers.constBegin(),
                end = target.value().modifiers.constEnd(); m != end; ++m) {
            (*m)->modifySequenceValue(value);
        }
        for (QList<EditDataTarget *>::const_iterator t = target.value().targets.constBegin(),
                end = target.value().targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(value);
        }
        if (target.value().muxId >= 0) {
            handleCompleted(outputMux.incoming(target.value().muxId, value));
        }
    }

    void value(const SequenceName &unit, const Variant::Read &v, double start, double end)
    { value(SequenceValue(unit, Variant::Root(v), start, end)); }

    void value(const SequenceName &unit, double v, double start, double end)
    { value(SequenceValue(unit, Variant::Root(v), start, end)); }

    void advance(double time)
    {
        action->incomingDataAdvance(time);
        handleCompleted(outputMux.advance(0, time));
    }

    void finish()
    {
        action->finalize();
        handleCompleted(outputMux.finish());
    }

    void incomingData(const Data::SequenceValue &value) override
    { handleCompleted(outputMux.incoming(1, value)); }

    void incomingData(const Data::SequenceValue::Transfer &values) override
    { handleCompleted(outputMux.incoming(1, values)); }

    void incomingAdvance(double time) override
    { handleCompleted(outputMux.advance(1, time)); }

    bool compare(const SequenceValue::Transfer &expected)
    {
        if (expected.size() != output.size())
            return false;

        for (const auto &check : expected) {
            int idx;
            for (idx = output.size() - 1; idx >= 0; --idx) {
                SequenceValue compare(output.at(idx));
                if (compare.getUnit() != check.getUnit())
                    continue;
                if (!FP::equal(compare.getStart(), check.getStart()))
                    continue;
                if (!FP::equal(compare.getEnd(), check.getEnd()))
                    continue;
                if (compare.getValue() != check.getValue())
                    continue;
                output.erase(output.begin() + idx);
                break;
            }
            if (idx < 0)
                return false;
        }

        return output.empty();
    }
};

class TestActionFunction : public QObject {
Q_OBJECT
private slots:

    void add()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        SequenceName c("brw", "raw", "c");
        Action *act = new ActionAdd(SequenceMatch::Composite(a), SequenceMatch::OrderedLookup(b),
                                    Calibration(QList<double>() << 0.5 << 1.0));
        Dispatch d;

        d.initialize(act);

        d.value(a, 1.0, 100, 200);
        d.value(b, 2.0, 100, 200);
        d.value(c, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(c, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 600, 800);
        d.value(a, 8.0, 700, 800);
        d.value(c, 9.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(3.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(c, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(FP::undefined()),
                                                                200, 300),
                                                  SequenceValue(c, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(13.5), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 600, 800),
                                                  SequenceValue(a, Variant::Root(15.5), 700, 800),
                                                  SequenceValue(c, Variant::Root(9.0), 800, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, 1.0, 100, 200);
        d.value(b, 2.0, 100, 200);
        d.value(c, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(c, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 600, 800);
        d.value(a, 8.0, 700, 800);
        d.value(c, 9.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(3.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(c, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(FP::undefined()),
                                                                200, 300),
                                                  SequenceValue(c, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(13.5), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 600, 800),
                                                  SequenceValue(a, Variant::Root(15.5), 700, 800),
                                                  SequenceValue(c, Variant::Root(9.0), 800, 900)}));

        delete act;
    }

    void subtract()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        SequenceName c("brw", "raw", "c");
        Action *act = new ActionSubtract(SequenceMatch::Composite(a), SequenceMatch::OrderedLookup(b),
                                         Calibration(QList<double>() << 0.5 << 1.0));
        Dispatch d;

        d.initialize(act);

        d.value(a, 1.0, 100, 200);
        d.value(b, 2.0, 100, 200);
        d.value(c, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(c, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 600, 800);
        d.value(a, 8.0, 700, 800);
        d.value(c, 9.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(-1.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(c, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(FP::undefined()),
                                                                200, 300),
                                                  SequenceValue(c, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(-1.5), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 600, 800),
                                                  SequenceValue(a, Variant::Root(0.5), 700, 800),
                                                  SequenceValue(c, Variant::Root(9.0), 800, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, 1.0, 100, 200);
        d.value(b, 2.0, 100, 200);
        d.value(c, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(c, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 600, 800);
        d.value(a, 8.0, 700, 800);
        d.value(c, 9.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(-1.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(c, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(FP::undefined()),
                                                                200, 300),
                                                  SequenceValue(c, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(-1.5), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 600, 800),
                                                  SequenceValue(a, Variant::Root(0.5), 700, 800),
                                                  SequenceValue(c, Variant::Root(9.0), 800, 900)}));

        delete act;
    }

    void multiply()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        SequenceName c("brw", "raw", "c");
        Action *act = new ActionMultiply(SequenceMatch::Composite(a), SequenceMatch::OrderedLookup(b),
                                         Calibration(QList<double>() << 0.5 << 1.0));
        Dispatch d;

        d.initialize(act);

        d.value(a, 1.0, 100, 200);
        d.value(b, 2.0, 100, 200);
        d.value(c, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(c, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 600, 800);
        d.value(a, 8.0, 700, 800);
        d.value(c, 9.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(c, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(FP::undefined()),
                                                                200, 300),
                                                  SequenceValue(c, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(45.0), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 600, 800),
                                                  SequenceValue(a, Variant::Root(60.0), 700, 800),
                                                  SequenceValue(c, Variant::Root(9.0), 800, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, 1.0, 100, 200);
        d.value(b, 2.0, 100, 200);
        d.value(c, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(c, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 600, 800);
        d.value(a, 8.0, 700, 800);
        d.value(c, 9.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(c, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(FP::undefined()),
                                                                200, 300),
                                                  SequenceValue(c, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(45.0), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 600, 800),
                                                  SequenceValue(a, Variant::Root(60.0), 700, 800),
                                                  SequenceValue(c, Variant::Root(9.0), 800, 900)}));

        delete act;
    }

    void divide()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        SequenceName c("brw", "raw", "c");
        Action *act = new ActionDivide(SequenceMatch::Composite(a), SequenceMatch::OrderedLookup(b),
                                       Calibration(QList<double>() << 0.5 << 1.0));
        Dispatch d;

        d.initialize(act);

        d.value(a, 1.0, 100, 200);
        d.value(b, 2.0, 100, 200);
        d.value(c, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(c, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 600, 800);
        d.value(a, 8.0, 700, 800);
        d.value(c, 9.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(0.4), 100, 200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(c, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(FP::undefined()),
                                                                200, 300),
                                                  SequenceValue(c, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(0.8), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 600, 800),
                                                  SequenceValue(a, Variant::Root(8.0 / 7.5), 700,
                                                                800),
                                                  SequenceValue(c, Variant::Root(9.0), 800, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, 1.0, 100, 200);
        d.value(b, 2.0, 100, 200);
        d.value(c, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(c, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 600, 800);
        d.value(a, 8.0, 700, 800);
        d.value(c, 9.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(0.4), 100, 200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(c, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(FP::undefined()),
                                                                200, 300),
                                                  SequenceValue(c, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(0.8), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 600, 800),
                                                  SequenceValue(a, Variant::Root(8.0 / 7.5), 700,
                                                                800),
                                                  SequenceValue(c, Variant::Root(9.0), 800, 900)}));

        delete act;
    }

    void invert()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        SequenceName c("brw", "raw", "c");
        Action *act = new ActionInvert(SequenceMatch::Composite(a), SequenceMatch::OrderedLookup(b),
                                       Calibration(QList<double>() << 0.5 << 1.0));
        Dispatch d;

        d.initialize(act);

        d.value(a, 1.0, 100, 200);
        d.value(b, 2.0, 100, 200);
        d.value(c, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(c, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 600, 800);
        d.value(a, 8.0, 700, 800);
        d.value(c, 9.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(c, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(FP::undefined()),
                                                                200, 300),
                                                  SequenceValue(c, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(1.25), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 600, 800),
                                                  SequenceValue(a, Variant::Root(7.5 / 8.0), 700,
                                                                800),
                                                  SequenceValue(c, Variant::Root(9.0), 800, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, 1.0, 100, 200);
        d.value(b, 2.0, 100, 200);
        d.value(c, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(c, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 600, 800);
        d.value(a, 8.0, 700, 800);
        d.value(c, 9.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(c, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(FP::undefined()),
                                                                200, 300),
                                                  SequenceValue(c, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(1.25), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 600, 800),
                                                  SequenceValue(a, Variant::Root(7.5 / 8.0), 700,
                                                                800),
                                                  SequenceValue(c, Variant::Root(9.0), 800, 900)}));

        delete act;
    }

    void assign()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        SequenceName c("brw", "raw", "c");
        Action *act = new ActionAssign(SequenceMatch::Composite(a), SequenceMatch::OrderedLookup(b),
                                       Calibration(QList<double>() << 0.5 << 1.0));
        Dispatch d;

        d.initialize(act);

        d.value(a, 1.0, 100, 200);
        d.value(b, 2.0, 100, 200);
        d.value(c, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(c, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 600, 800);
        d.value(a, 8.0, 700, 800);
        d.value(c, 9.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(c, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(FP::undefined()),
                                                                200, 300),
                                                  SequenceValue(c, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(7.5), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 600, 800),
                                                  SequenceValue(a, Variant::Root(7.5), 700, 800),
                                                  SequenceValue(c, Variant::Root(9.0), 800, 900)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, 1.0, 100, 200);
        d.value(b, 2.0, 100, 200);
        d.value(c, 3.0, 100, 200);
        d.advance(100);

        d.value(a, 4.0, 200, 300);
        d.value(c, 5.0, 300, 400);
        d.advance(500);

        d.advance(600);
        d.value(a, 6.0, 600, 700);
        d.value(b, 7.0, 600, 800);
        d.value(a, 8.0, 700, 800);
        d.value(c, 9.0, 800, 900);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(a, Variant::Root(2.5), 100, 200),
                                                  SequenceValue(b, Variant::Root(2.0), 100, 200),
                                                  SequenceValue(c, Variant::Root(3.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(FP::undefined()),
                                                                200, 300),
                                                  SequenceValue(c, Variant::Root(5.0), 300, 400),
                                                  SequenceValue(a, Variant::Root(7.5), 600, 700),
                                                  SequenceValue(b, Variant::Root(7.0), 600, 800),
                                                  SequenceValue(a, Variant::Root(7.5), 700, 800),
                                                  SequenceValue(c, Variant::Root(9.0), 800, 900)}));

        delete act;
    }
};

QTEST_MAIN(TestActionFunction)

#include "actionfunction.moc"
