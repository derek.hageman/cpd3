/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/editdirective.hxx"
#include "editing/triggercondition.hxx"
#include "editing/triggerinput.hxx"
#include "editing/triggerbuffer.hxx"
#include "editing/actionscriptprocessor.hxx"
#include "datacore/stream.hxx"
#include "datacore/staticmultiplexer.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

class Target : public EditDispatchTarget {
public:
    SequenceValue::Transfer output;

    Target()
    { }

    virtual ~Target()
    { }

    void incomingData(const SequenceValue &value) override
    { output.emplace_back(value); }

    void incomingData(const SequenceValue::Transfer &values) override
    { Util::append(values, output); }

    void incomingAdvance(double time) override
    { Q_UNUSED(time); }

    bool compare(const SequenceValue::Transfer &expected)
    {
        if (expected.size() != output.size())
            return false;

        for (const auto &check : expected) {
            int idx;
            for (idx = output.size() - 1; idx >= 0; --idx) {
                SequenceValue compare(output.at(idx));
                if (compare.getUnit() != check.getUnit())
                    continue;
                if (!FP::equal(compare.getStart(), check.getStart()))
                    continue;
                if (!FP::equal(compare.getEnd(), check.getEnd()))
                    continue;
                if (compare.getValue() != check.getValue())
                    continue;
                output.erase(output.begin() + idx);
                break;
            }
            if (idx < 0)
                return false;
        }

        return output.empty();
    }
};

class TestEditDirective : public QObject {
Q_OBJECT

    static SequenceValue DV(const SequenceName &name, double v, double start, double end)
    { return SequenceValue(name, Variant::Root(v), start, end); }

private slots:

    void basic()
    {
        SequenceName u1("bnd", "raw", "a");
        SequenceName u2("bnd", "raw", "b");
        SequenceName u3("sgp", "raw", "a");

        Target target;
        EditDirective *e = new EditDirective(FP::undefined(), FP::undefined(), new TriggerAlways,
                                             new ActionRemove(SequenceMatch::Composite(u1)));
        e->setOutput(&target);

        e->incomingData(DV(u1, 1.0, 100, 200));
        e->incomingData(DV(u2, 2.0, 100, 200));
        e->incomingData(DV(u3, 3.0, 100, 200));
        e->incomingAdvance(110);

        QVERIFY(!target.output.empty());

        e->incomingData(DV(u1, 4.0, 200, 300));
        e->incomingData(DV(u2, 5.0, 200, 300));
        e->incomingData(DV(u3, 6.0, 200, 300));

        e->incomingSkipped(DV(u1, 7.0, 300, 400), this);
        e->incomingSkipped(DV(u2, 8.0, 300, 400), this);
        e->incomingSkipped(DV(u3, 9.0, 300, 400), this);

        e->incomingAdvance(310);
        e->incomingSkipAdvance(310, this);

        e->incomingData(DV(u1, 10.0, 400, 500));
        e->incomingData(DV(u2, 11.0, 400, 500));
        e->incomingData(DV(u3, 12.0, 400, 500));

        e->finalize();

        QVERIFY(target.compare(SequenceValue::Transfer{DV(u2, 2.0, 100, 200), DV(u3, 3.0, 100, 200),
                                                       DV(u2, 5.0, 200, 300), DV(u3, 6.0, 200, 300),
                                                       DV(u1, 7.0, 300, 400), DV(u2, 8.0, 300, 400),
                                                       DV(u3, 9.0, 300, 400),
                                                       DV(u2, 11.0, 400, 500),
                                                       DV(u3, 12.0, 400, 500)}));

        delete e;
    }

    void triggered()
    {
        SequenceName a1("bnd", "raw", "a");
        SequenceName b1("bnd", "raw", "b");
        SequenceName a2("sgp", "raw", "a");
        SequenceName b2("sgp", "raw", "b");

        Target target;
        EditDirective *e = new EditDirective(FP::undefined(), FP::undefined(), new TriggerLessThan(
                new TriggerInputValue(SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("a"))),
                new TriggerInputValue(SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("b")))),
                                             new ActionScriptSequenceSegmentProcessor(
                                                     SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("a")),
                                                     SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("a|b")), R"EOF(
if not data:exists('a') then
    control:erase(data);
    return;
end
data.a = data.a + data.b
)EOF"));
        e->setOutput(&target);

        e->incomingData(DV(a1, 1.0, 100, 200));
        e->incomingData(DV(b1, 2.0, 100, 200));

        e->incomingData(DV(a1, 2.0, 200, 300));
        e->incomingData(DV(b1, 1.0, 200, 300));

        e->incomingData(DV(a1, 1.0, 300, 400));
        e->incomingData(DV(b1, 2.0, 300, 400));
        e->incomingData(DV(a2, 2.5, 300, 400));
        e->incomingData(DV(b2, 1.5, 300, 400));

        e->incomingData(DV(a1, 2.0, 400, 500));
        e->incomingData(DV(b1, 1.0, 400, 500));
        e->incomingData(DV(a2, 1.5, 400, 500));
        e->incomingData(DV(b2, 2.5, 400, 500));

        e->incomingSkipped(DV(a1, 1.0, 500, 600), this);
        e->incomingSkipped(DV(b1, 2.0, 500, 600), this);

        e->finalize();

        QVERIFY(target.compare(SequenceValue::Transfer{DV(a1, 3.0, 100, 200), DV(b1, 2.0, 100, 200),
                                                       DV(a1, 2.0, 200, 300), DV(b1, 1.0, 200, 300),
                                                       DV(a1, 3.0, 300, 400), DV(b1, 2.0, 300, 400),
                                                       DV(a2, 2.5, 300, 400), DV(b2, 1.5, 300, 400),
                                                       DV(a1, 2.0, 400, 500), DV(b1, 1.0, 400, 500),
                                                       DV(a2, 4.0, 400, 500), DV(b2, 2.5, 400, 500),
                                                       DV(a1, 1.0, 500, 600),
                                                       DV(b1, 2.0, 500, 600)}));

        delete e;
    }

    void buffer()
    {
        SequenceName a1("bnd", "raw", "a");
        SequenceName b1("bnd", "raw", "b");
        SequenceName a2("sgp", "raw", "a");
        SequenceName b2("sgp", "raw", "b");

        Variant::Root config;
        config["Interval"].setInt64(100);

        Target target;
        EditDirective *e = new EditDirective(FP::undefined(), FP::undefined(), new TriggerLessThan(
                new TriggerInputValue(SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("a"))),
                new TriggerInputQuantile(
                        new TriggerInputValue(SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("b"))), 0.5,
                        config)), new ActionRemove(SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("a"))));
        e->setOutput(&target);

        e->incomingData(DV(a1, 1.0, 100, 200));
        e->incomingData(DV(b1, 2.0, 100, 200));

        e->incomingSkipped(DV(a1, 1.5, 200, 300), this);

        e->incomingData(DV(a1, 2.0, 400, 500));
        e->incomingData(DV(b1, 1.0, 400, 500));

        e->incomingSkipped(DV(b1, 2.5, 300, 400), this);

        e->incomingData(DV(a1, 2.0, 500, 600));
        e->incomingData(DV(b1, 1.0, 500, 600));

        e->incomingData(DV(a1, 2.0, 600, 700));
        e->incomingData(DV(b1, 3.0, 600, 700));

        e->incomingData(DV(a1, 2.0, 700, 800));
        e->incomingData(DV(b1, 3.0, 700, 800));

        e->incomingData(DV(a1, 2.0, 800, 900));
        e->incomingData(DV(b1, 3.0, 800, 900));
        e->incomingData(DV(a2, 2.0, 800, 900));
        e->incomingData(DV(b2, 1.0, 800, 900));

        e->incomingData(DV(a2, 2.0, 900, 1000));
        e->incomingData(DV(b2, 1.0, 900, 1000));

        e->incomingData(DV(a2, 2.0, 1000, 1100));
        e->incomingData(DV(b2, 1.0, 1000, 1100));

        e->incomingData(DV(a2, 2.0, 1100, 1200));
        e->incomingData(DV(b2, 1.0, 1100, 1200));

        e->incomingData(DV(a2, 2.0, 1200, 1300));
        e->incomingData(DV(b2, 3.0, 1200, 1300));

        e->incomingData(DV(a2, 2.0, 1300, 1400));
        e->incomingData(DV(b2, 3.0, 1300, 1400));

        e->incomingData(DV(a2, 2.0, 1400, 1500));
        e->incomingData(DV(b2, 3.0, 1400, 1500));

        e->finalize();

        QVERIFY(target.compare(SequenceValue::Transfer{DV(b1, 2.0, 100, 200), DV(a1, 1.5, 200, 300),
                                                       DV(b1, 2.5, 300, 400), DV(a1, 2.0, 400, 500),
                                                       DV(b1, 1.0, 400, 500), DV(a1, 2.0, 500, 600),
                                                       DV(b1, 1.0, 500, 600), DV(b1, 3.0, 600, 700),
                                                       DV(b1, 3.0, 700, 800), DV(b1, 3.0, 800, 900),
                                                       DV(a2, 2.0, 800, 900), DV(b2, 1.0, 800, 900),
                                                       DV(a2, 2.0, 900, 1000),
                                                       DV(b2, 1.0, 900, 1000),
                                                       DV(a2, 2.0, 1000, 1100),
                                                       DV(b2, 1.0, 1000, 1100),
                                                       DV(a2, 2.0, 1100, 1200),
                                                       DV(b2, 1.0, 1100, 1200),
                                                       DV(b2, 3.0, 1200, 1300),
                                                       DV(b2, 3.0, 1300, 1400),
                                                       DV(b2, 3.0, 1400, 1500)}));


        delete e;
    }
};

QTEST_MAIN(TestEditDirective)

#include "editdirective.moc"
