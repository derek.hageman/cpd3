/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/actionflowcorrection.hxx"
#include "datacore/stream.hxx"
#include "datacore/staticmultiplexer.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

class Dispatch : public EditDispatchTarget {
    Action *action;

    struct Target {
        QList<EditDataTarget *> targets;
        QList<EditDataModification *> modifiers;
        int muxId;

        Target() : targets(), modifiers(), muxId(0)
        { }
    };

    QHash<SequenceName, Target> d;

    StaticMultiplexer outputMux;

    void handleCompleted(const SequenceValue::Transfer &add)
    {
        Util::append(add, output);
    }

public:
    SequenceValue::Transfer output;

    Dispatch() : action(NULL), d(), outputMux()
    { }

    virtual ~Dispatch()
    { }

    void initialize(Action *a)
    {
        output.clear();
        d.clear();
        action = a;

        outputMux.clear(true);
        outputMux.setStreams(2);

        action->setTarget(this);
    }

    void value(SequenceValue value)
    {
        QHash<SequenceName, Target>::iterator target = d.find(value.getUnit());
        if (target == d.end()) {
            QList<EditDataTarget *> inputs;
            QList<EditDataTarget *> outputs;
            QList<EditDataModification *> modifiers;

            action->unhandledInput(value.getUnit(), inputs, outputs, modifiers);

            Target add;
            add.targets = inputs;
            add.targets.append(outputs);
            add.modifiers = modifiers;

            if (outputs.isEmpty()) {
                add.muxId = 0;
            } else {
                add.muxId = -1;
            }

            target = d.insert(value.getUnit(), add);
        }

        for (QList<EditDataModification *>::const_iterator
                m = target.value().modifiers.constBegin(),
                end = target.value().modifiers.constEnd(); m != end; ++m) {
            (*m)->modifySequenceValue(value);
        }
        for (QList<EditDataTarget *>::const_iterator t = target.value().targets.constBegin(),
                end = target.value().targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(value);
        }
        if (target.value().muxId >= 0) {
            handleCompleted(outputMux.incoming(target.value().muxId, value));
        }
    }

    void value(const SequenceName &unit, const Variant::Read &v, double start, double end)
    { value(SequenceValue(unit, Variant::Root(v), start, end)); }

    void value(const SequenceName &unit, double v, double start, double end)
    { value(SequenceValue(unit, Variant::Root(v), start, end)); }

    void advance(double time)
    {
        action->incomingDataAdvance(time);
        handleCompleted(outputMux.advance(0, time));
    }

    void finish()
    {
        action->finalize();
        handleCompleted(outputMux.finish());
    }

    void incomingData(const Data::SequenceValue &value) override
    { handleCompleted(outputMux.incoming(1, value)); }

    void incomingData(const Data::SequenceValue::Transfer &values) override
    { handleCompleted(outputMux.incoming(1, values)); }

    void incomingAdvance(double time) override
    { handleCompleted(outputMux.advance(1, time)); }

    bool compare(const SequenceValue::Transfer &expected)
    {
        if (expected.size() != output.size())
            return false;

        for (const auto &check : expected) {
            int idx;
            for (idx = output.size() - 1; idx >= 0; --idx) {
                SequenceValue compare(output.at(idx));
                if (compare.getUnit() != check.getUnit())
                    continue;
                if (!FP::equal(compare.getStart(), check.getStart()))
                    continue;
                if (!FP::equal(compare.getEnd(), check.getEnd()))
                    continue;
                if (compare.getValue() != check.getValue()) {
                    if (compare.getValue().getType() == Variant::Type::Real &&
                            check.getValue().getType() == Variant::Type::Real) {
                        double a = compare.getValue().toDouble();
                        double b = check.getValue().toDouble();
                        if (!FP::defined(a)) {
                            if (FP::defined(b))
                                continue;
                        } else if (!FP::defined(b)) {
                            continue;
                        } else {
                            double eps = qMax(fabs(a), fabs(b));
                            if (eps == 0.0)
                                eps = 1.0;
                            else
                                eps *= 1E-6;
                            if (fabs(a - b) > eps) {
                                continue;
                            }
                        }
                    } else {
                        continue;
                    }
                }
                output.erase(output.begin() + idx);
                break;
            }
            if (idx < 0) {
                return false;
            }
        }

        return output.empty();
    }
};

class TestActionFlowCorrection : public QObject {
Q_OBJECT
private slots:

    void calibrationCorrection()
    {
        SequenceName Q("brw", "raw", "Q_A11");
        SequenceName L("brw", "raw", "L_A11");
        SequenceName Qt("brw", "raw", "Qt_A11");
        SequenceName ZSPOT("brw", "raw", "ZSPOT_A11");
        SequenceName Ba1("brw", "raw", "BaB_A11");
        SequenceName Ba2("brw", "raw", "Ba1_A11");
        SequenceName BaX("brw", "raw", "Ba1_A12");

        double vraw = 1.0;
        double area = 12.5;
        Calibration before(QVector<double>() << 0.0 << 2.0);
        Calibration after(QVector<double>() << 0.25 << 1.5);
        double startTime = 100;
        double endTime1 = 400;
        double endTime2 = 800;
        double dT = 100.0;

        Action *act = new ActionFlowCorrection(before, after, "A11", "brw", "raw");
        Dispatch d;

        QVERIFY(act->requestedInputs().count(Q) != 0);

        d.initialize(act);

        Variant::Root spot;
        spot["Area"].setDouble(area);
        spot["Qt"].setDouble((endTime1 - startTime) * before.apply(vraw) / 60000.0);
        spot["L"].setDouble(spot["Qt"].toDouble() / (area * 1E-6));
        d.value(ZSPOT, spot, startTime, endTime1);

        Variant::Root modifiedSpot;
        modifiedSpot["Area"].setDouble(area);
        modifiedSpot["Qt"].setDouble((endTime1 - startTime) * after.apply(vraw) / 60000.0);
        modifiedSpot["L"].setDouble(modifiedSpot["Qt"].toDouble() / (area * 1E-6));
        SequenceValue::Transfer expected;
        expected.emplace_back(ZSPOT, modifiedSpot, startTime, endTime1);

        double accumulateBefore = 0.0;
        double accumulateAfter = 0.0;
        for (double time = startTime + dT; time < endTime1; startTime = time, time += dT) {
            double beforeFlow = before.apply(vraw);
            double afterFlow = after.apply(vraw);
            accumulateBefore += beforeFlow * dT / 60000.0;
            accumulateAfter += afterFlow * dT / 60000.0;

            d.value(Q, beforeFlow, startTime, time);
            expected.emplace_back(Q, Variant::Root(afterFlow), startTime, time);

            d.value(Qt, accumulateBefore, startTime, time);
            expected.emplace_back(Qt, Variant::Root(accumulateAfter), startTime, time);

            d.value(L, accumulateBefore / (area * 1E-6), startTime, time);
            expected.emplace_back(L, Variant::Root(accumulateAfter / (area * 1E-6)), startTime,
                                  time);

            d.value(Ba1, 1.0, startTime, time);
            d.value(Ba2, 2.0, startTime, time);
            d.value(BaX, -1.0, startTime, time);

            expected.emplace_back(Ba1, Variant::Root(1.0 / (afterFlow / beforeFlow)), startTime,
                                  time);
            expected.emplace_back(Ba2, Variant::Root(2.0 / (afterFlow / beforeFlow)), startTime,
                                  time);
            expected.emplace_back(BaX, Variant::Root(-1.0), startTime, time);
        }

        spot = Variant::Root();
        spot["Area"].setDouble(area);
        spot["Qt"].setDouble((endTime2 - startTime) * before.apply(vraw) / 60000.0);
        spot["L"].setDouble(spot["Qt"].toDouble() / (area * 1E-6));
        d.value(ZSPOT, spot, startTime, endTime2);

        modifiedSpot = Variant::Root();
        modifiedSpot["Area"].setDouble(area);
        modifiedSpot["Qt"].setDouble((endTime2 - startTime) * after.apply(vraw) / 60000.0);
        modifiedSpot["L"].setDouble(modifiedSpot["Qt"].toDouble() / (area * 1E-6));
        expected.emplace_back(ZSPOT, modifiedSpot, startTime, endTime2);

        accumulateBefore = 0.0;
        accumulateAfter = 0.0;
        for (double time = startTime + dT; time < endTime2; startTime = time, time += dT) {
            double beforeFlow = before.apply(vraw);
            double afterFlow = after.apply(vraw);
            accumulateBefore += beforeFlow * dT / 60000.0;
            accumulateAfter += afterFlow * dT / 60000.0;

            d.value(Q, beforeFlow, startTime, time);
            expected.emplace_back(Q, Variant::Root(afterFlow), startTime, time);

            d.value(Qt, accumulateBefore, startTime, time);
            expected.emplace_back(Qt, Variant::Root(accumulateAfter), startTime, time);

            d.value(L, accumulateBefore / (area * 1E-6), startTime, time);
            expected.emplace_back(L, Variant::Root(accumulateAfter / (area * 1E-6)), startTime,
                                  time);

            d.value(Ba1, 2.0, startTime, time);
            d.value(Ba2, 3.0, startTime, time);
            d.value(BaX, -1.5, startTime, time);

            expected.emplace_back(Ba1, Variant::Root(2.0 / (afterFlow / beforeFlow)), startTime,
                                  time);
            expected.emplace_back(Ba2, Variant::Root(3.0 / (afterFlow / beforeFlow)), startTime,
                                  time);
            expected.emplace_back(BaX, Variant::Root(-1.5), startTime, time);
        }

        d.finish();

        QVERIFY(d.compare(expected));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);
        startTime = 100.0;

        spot = Variant::Root();
        spot["Area"].setDouble(area);
        spot["Qt"].setDouble((endTime1 - startTime) * before.apply(vraw) / 60000.0);
        spot["L"].setDouble(spot["Qt"].toDouble() / (area * 1E-6));
        d.value(ZSPOT, spot, startTime, endTime1);

        accumulateBefore = 0.0;
        accumulateAfter = 0.0;
        for (double time = startTime + dT; time < endTime1; startTime = time, time += dT) {
            double beforeFlow = before.apply(vraw);
            double afterFlow = after.apply(vraw);
            accumulateBefore += beforeFlow * dT / 60000.0;
            accumulateAfter += afterFlow * dT / 60000.0;

            d.value(Q, beforeFlow, startTime, time);

            d.value(Qt, accumulateBefore, startTime, time);

            d.value(L, accumulateBefore / (area * 1E-6), startTime, time);

            d.value(Ba1, 1.0, startTime, time);
            d.value(Ba2, 2.0, startTime, time);
            d.value(BaX, -1.0, startTime, time);
        }

        spot = Variant::Root();
        spot["Area"].setDouble(area);
        spot["Qt"].setDouble((endTime2 - startTime) * before.apply(vraw) / 60000.0);
        spot["L"].setDouble(spot["Qt"].toDouble() / (area * 1E-6));
        d.value(ZSPOT, spot, startTime, endTime2);

        accumulateBefore = 0.0;
        accumulateAfter = 0.0;
        for (double time = startTime + dT; time < endTime2; startTime = time, time += dT) {
            double beforeFlow = before.apply(vraw);
            double afterFlow = after.apply(vraw);
            accumulateBefore += beforeFlow * dT / 60000.0;
            accumulateAfter += afterFlow * dT / 60000.0;

            d.value(Q, beforeFlow, startTime, time);

            d.value(Qt, accumulateBefore, startTime, time);

            d.value(L, accumulateBefore / (area * 1E-6), startTime, time);

            d.value(Ba1, 2.0, startTime, time);
            d.value(Ba2, 3.0, startTime, time);
            d.value(BaX, -1.5, startTime, time);
        }

        d.finish();

        QVERIFY(d.compare(expected));

        delete act;
    }

    void spotCorrection()
    {
        SequenceName L("brw", "raw", "L_A11");
        SequenceName ZSPOT("brw", "raw", "ZSPOT_A11");
        SequenceName Ba1("brw", "raw", "BaB_A11");
        SequenceName Ba2("brw", "raw", "Ba1_A11");
        SequenceName BaX("brw", "raw", "Ba1_A12");

        double spotBefore = 17.25;
        double spotAfter = 18.75;

        Action *act = new ActionSpotCorrection(spotBefore, spotAfter, "A11", "brw", "raw");
        Dispatch d;

        QVERIFY(act->requestedInputs().count(ZSPOT) != 0);

        d.initialize(act);

        Variant::Root spot;
        spot["Area"].setDouble(spotBefore);
        spot["L"].setDouble(500.0);
        d.value(ZSPOT, spot, 100, 600);

        Variant::Root modifiedSpot;
        modifiedSpot["Area"].setDouble(spotAfter);
        modifiedSpot["L"].setDouble(500.0 / (spotAfter / spotBefore));

        d.value(L, 500.0, 100, 200);
        d.value(Ba1, 10.0, 100, 200);
        d.value(Ba2, 20.0, 100, 200);
        d.value(BaX, 30.0, 200, 300);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(ZSPOT, modifiedSpot, 100, 600),
                                                  SequenceValue(L, Variant::Root(
                                                          500.0 / (spotAfter / spotBefore)), 100,
                                                                200), SequenceValue(Ba1,
                                                                                    Variant::Root(
                                                                                            10.0 *
                                                                                                    (spotAfter /
                                                                                                            spotBefore)),
                                                                                    100, 200),
                                                  SequenceValue(Ba2, Variant::Root(
                                                          20.0 * (spotAfter / spotBefore)), 100,
                                                                200),
                                                  SequenceValue(BaX, Variant::Root(30.0), 200,
                                                                300)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(ZSPOT, spot, 100, 600);
        d.value(L, 500.0, 100, 200);
        d.value(Ba1, 10.0, 100, 200);
        d.value(Ba2, 20.0, 100, 200);
        d.value(BaX, 30.0, 200, 300);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(ZSPOT, modifiedSpot, 100, 600),
                                                  SequenceValue(L, Variant::Root(
                                                          500.0 / (spotAfter / spotBefore)), 100,
                                                                200), SequenceValue(Ba1,
                                                                                    Variant::Root(
                                                                                            10.0 *
                                                                                                    (spotAfter /
                                                                                                            spotBefore)),
                                                                                    100, 200),
                                                  SequenceValue(Ba2, Variant::Root(
                                                          20.0 * (spotAfter / spotBefore)), 100,
                                                                200),
                                                  SequenceValue(BaX, Variant::Root(30.0), 200,
                                                                300)}));

        delete act;
    }

    void multiSpotCorrection()
    {
        SequenceName Fn("brw", "raw", "Fn_A11");
        SequenceName L("brw", "raw", "L_A11");
        SequenceName ZSPOT("brw", "raw", "ZSPOT_A11");
        SequenceName Ba1("brw", "raw", "BaB_A11");
        SequenceName Ba2("brw", "raw", "Ba1_A11");
        SequenceName BaX("brw", "raw", "Ba1_A12");

        std::vector<double> spotBefore;
        std::vector<double> spotAfter;

        spotBefore.push_back(17.25);
        spotAfter.push_back(18.75);

        spotBefore.push_back(16.5);
        spotAfter.push_back(22.5);

        Action *act = new ActionMultiSpotCorrection(spotBefore, spotAfter, "A11", "brw", "raw");
        Dispatch d;

        QVERIFY(act->requestedInputs().count(ZSPOT) != 0);
        QVERIFY(act->requestedInputs().count(Fn) != 0);

        d.initialize(act);

        d.value(Fn, Variant::Root((qint64) 1), 100, 300);

        Variant::Root spot1;
        spot1["Area"].setDouble(spotBefore[0]);
        spot1["L"].setDouble(500.0);
        spot1["Fn"].setInt64(1);
        d.value(ZSPOT, spot1, 100, 300);

        Variant::Root modifiedSpot1;
        modifiedSpot1["Area"].setDouble(spotAfter[0]);
        modifiedSpot1["L"].setDouble(500.0 / (spotAfter[0] / spotBefore[0]));
        modifiedSpot1["Fn"].setInt64(1);

        d.value(L, 500.0, 100, 200);
        d.value(Ba1, 10.0, 100, 200);
        d.value(Ba2, 20.0, 100, 200);
        d.value(BaX, 30.0, 200, 300);


        d.value(Fn, Variant::Root((qint64) 2), 300, 500);

        Variant::Root spot2;
        spot2["Area"].setDouble(spotBefore[1]);
        spot2["L"].setDouble(450.0);
        spot2["Fn"].setInt64(2);
        d.value(ZSPOT, spot2, 300, 500);

        Variant::Root modifiedSpot2;
        modifiedSpot2["Area"].setDouble(spotAfter[1]);
        modifiedSpot2["L"].setDouble(450.0 / (spotAfter[1] / spotBefore[1]));
        modifiedSpot2["Fn"].setInt64(2);

        d.value(L, 450.0, 300, 500);
        d.value(Ba1, 11.0, 300, 500);
        d.value(Ba2, 21.0, 300, 500);
        d.value(BaX, 31.0, 400, 500);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(Fn, Variant::Root((qint64) 1), 100, 300),
                                        SequenceValue(ZSPOT, modifiedSpot1, 100, 300),
                                        SequenceValue(L, Variant::Root(500.0 /
                                                          (spotAfter[0] / spotBefore[0])), 100, 200),
                                        SequenceValue(Ba1, Variant::Root(10.0 *
                                                          (spotAfter[0] / spotBefore[0])), 100, 200),
                                        SequenceValue(Ba2, Variant::Root(20.0 *
                                                          (spotAfter[0] / spotBefore[0])), 100, 200),
                                        SequenceValue(BaX, Variant::Root(30.0), 200, 300),
                                        SequenceValue(Fn, Variant::Root((qint64) 2), 300, 500),
                                        SequenceValue(ZSPOT, modifiedSpot2, 300, 500),
                                        SequenceValue(L, Variant::Root(450.0 /
                                                          (spotAfter[1] / spotBefore[1])), 300, 500),
                                        SequenceValue(Ba1, Variant::Root(11.0 *
                                                          (spotAfter[1] / spotBefore[1])), 300, 500),
                                        SequenceValue(Ba2, Variant::Root(21.0 *
                                                          (spotAfter[1] / spotBefore[1])), 300, 500),
                                        SequenceValue(BaX, Variant::Root(31.0), 400, 500)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(Fn, Variant::Root((qint64) 1), 100, 300);
        d.value(ZSPOT, spot1, 100, 300);

        d.value(L, 500.0, 100, 200);
        d.value(Ba1, 10.0, 100, 200);
        d.value(Ba2, 20.0, 100, 200);
        d.value(BaX, 30.0, 200, 300);


        d.value(Fn, Variant::Root((qint64) 2), 300, 500);
        d.value(ZSPOT, spot2, 300, 500);

        d.value(L, 450.0, 300, 500);
        d.value(Ba1, 11.0, 300, 500);
        d.value(Ba2, 21.0, 300, 500);
        d.value(BaX, 31.0, 400, 500);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(Fn, Variant::Root((qint64) 1), 100, 300),
                                        SequenceValue(ZSPOT, modifiedSpot1, 100, 300),
                                        SequenceValue(L, Variant::Root(500.0 /
                                                          (spotAfter[0] / spotBefore[0])), 100, 200),
                                        SequenceValue(Ba1, Variant::Root(10.0 *
                                                          (spotAfter[0] / spotBefore[0])), 100, 200),
                                        SequenceValue(Ba2, Variant::Root(20.0 *
                                                          (spotAfter[0] / spotBefore[0])), 100, 200),
                                        SequenceValue(BaX, Variant::Root(30.0), 200, 300),
                                        SequenceValue(Fn, Variant::Root((qint64) 2), 300, 500),
                                        SequenceValue(ZSPOT, modifiedSpot2, 300, 500),
                                        SequenceValue(L, Variant::Root(450.0 /
                                                          (spotAfter[1] / spotBefore[1])), 300, 500),
                                        SequenceValue(Ba1, Variant::Root(11.0 *
                                                          (spotAfter[1] / spotBefore[1])), 300, 500),
                                        SequenceValue(Ba2, Variant::Root(21.0 *
                                                          (spotAfter[1] / spotBefore[1])), 300, 500),
                                        SequenceValue(BaX, Variant::Root(31.0), 400, 500)}));

        delete act;
    }
};

QTEST_MAIN(TestActionFlowCorrection)

#include "actionflowcorrection.moc"
