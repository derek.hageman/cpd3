/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/triggerbuffer.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

struct TS {
    double start;
    double end;
    quint32 value;

    inline double getStart() const
    { return start; }

    inline double getEnd() const
    { return end; }

    inline void setStart(double v)
    { start = v; }

    inline void setEnd(double v)
    { end = v; }

    bool operator==(const TS &other) const
    {
        return FP::equal(start, other.start) && FP::equal(end, other.end) && value == other.value;
    }

    TS() : start(FP::undefined()), end(FP::undefined()), value(-1)
    { }

    TS(double s, double e, quint32 v) : start(s), end(e), value(v)
    { }
};
namespace QTest {
template<>
char *toString(const QVector<TS> &segs)
{
    QByteArray ba;
    for (int i = 0, max = segs.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        ba += "[" +
                QByteArray::number(segs[i].start) +
                ":" +
                QByteArray::number(segs[i].end) +
                "]=0x" +
                QByteArray::number(segs[i].value, 16);
    }
    return qstrdup(ba.data());
}
}

struct TA {
    QVector<TS> add;
    double advance;

    QVector<TS> result;

    TA() : add(), advance(FP::undefined())
    { }

    TA(const QVector<TS> &a, const QVector<TS> &r = QVector<TS>()) : add(a),
                                                                     advance(FP::undefined()),
                                                                     result(r)
    { }

    TA(const TS &a, const QVector<TS> &r = QVector<TS>())
            : add(), advance(FP::undefined()), result(r)
    { add << a; }

    TA(double a, const QVector<TS> &r = QVector<TS>()) : add(), advance(a), result(r)
    { }
};

Q_DECLARE_METATYPE(QList<TA>);

class TestBufferConverter : public TriggerBuffer<TS, TS> {
public:
    TestBufferConverter()
    { }

protected:
    virtual TS convert(const QVector<TS> &buffer, const TS &input)
    {
        TS output(input);
        for (QVector<TS>::const_iterator add = buffer.constBegin(), endAdd = buffer.constEnd();
                add != endAdd;
                ++add) {
            output.value |= add->value;
        }
        return output;
    }
};

class TestTriggerBuffer : public QObject {
Q_OBJECT
private slots:

    void buffer()
    {
        QFETCH(int, before);
        QFETCH(int, after);
        QFETCH(int, gap);
        QFETCH(bool, requireAll);
        QFETCH(QList<TA>, actions);

        TestBufferConverter converter;
        converter.setBefore(Time::Second, before);
        converter.setAfter(Time::Second, after);
        converter.setGap(Time::Second, gap);
        converter.setRequireFull(requireAll);

        QVector<TS> finalResult;
        QVector<TS> finalExpected;
        for (QList<TA>::const_iterator action = actions.constBegin(), end = actions.constEnd();
                action != end;
                ++action) {
            QVector<TS> result;
            if (!action->add.isEmpty()) {
                result << converter.incomingBufferData(action->add);
                QCOMPARE(result, action->result);
            }
            if (FP::defined(action->advance)) {
                result << converter.incomingBufferAdvance(action->advance);
                QCOMPARE(result, action->result);
            }
            finalResult << result;
            finalExpected << action->result;
        }

        finalResult << converter.endBufferData();
        QCOMPARE(finalResult, finalExpected);
    }

    void buffer_data()
    {
        QTest::addColumn<int>("before");
        QTest::addColumn<int>("after");
        QTest::addColumn<int>("gap");
        QTest::addColumn<bool>("requireAll");
        QTest::addColumn<QList<TA> >("actions");

        QTest::newRow("Empty") << 10 << 10 << -1 << true << (QList<TA>());
        QTest::newRow("Single") <<
                10 <<
                10 <<
                -1 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x1)) <<
                        TA(QVector<TS>(), QVector<TS>() << TS(1, 2, 0x1)));
        QTest::newRow("Two Gap") <<
                10 <<
                10 <<
                1 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x1)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x2), QVector<TS>() << TS(1, 2, 0x1)) <<
                        TA(QVector<TS>(), QVector<TS>() << TS(4, 5, 0x2)));

        QTest::newRow("Two Merge") <<
                10 <<
                10 <<
                -1 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x1)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x2)) <<
                        TA(QVector<TS>(), QVector<TS>() << TS(1, 2, 0x3) << TS(3, 4, 0x3)));
        QTest::newRow("Two Merge Advance") <<
                10 <<
                10 <<
                -1 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x1)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x2)) <<
                        TA(12, QVector<TS>() << TS(1, 2, 0x3)) <<
                        TA(QVector<TS>(), QVector<TS>() << TS(3, 4, 0x3)));
        QTest::newRow("Two Merge Advance All") <<
                10 <<
                10 <<
                -1 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x1)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x2)) <<
                        TA(14, QVector<TS>() << TS(1, 2, 0x3) << TS(3, 4, 0x3)));

        QTest::newRow("Two Gap Merge") <<
                10 <<
                10 <<
                0 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x1)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x2)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x4),
                           QVector<TS>() << TS(1, 2, 0x3) << TS(2, 3, 0x3)) <<
                        TA(QVector<TS>() << TS(5, 6, 0x8)) <<
                        TA(17, QVector<TS>() << TS(4, 5, 0xC) << TS(5, 6, 0xC)));

        QTest::newRow("Two Skip Merge") <<
                2 <<
                2 <<
                -1 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04), QVector<TS>() << TS(1, 2, 0x07)) <<
                        TA(QVector<TS>() << TS(5, 6, 0x10),
                           QVector<TS>() << TS(2, 3, 0x07) << TS(3, 4, 0x17)) <<
                        TA(QVector<TS>() << TS(6, 7, 0x20)) <<
                        TA(QVector<TS>() << TS(7, 8, 0x40), QVector<TS>() << TS(5, 6, 0x74)) <<
                        TA(QVector<TS>(), QVector<TS>() << TS(6, 7, 0x70) << TS(7, 8, 0x70)));

        QTest::newRow("Completed Check") <<
                2 <<
                1 <<
                -1 <<
                true <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x08), QVector<TS>() << TS(3, 4, 0x0F)) <<
                        TA(QVector<TS>() << TS(5, 6, 0x10), QVector<TS>() << TS(4, 5, 0x1e)));

        QTest::newRow("Completed Advance") <<
                2 <<
                1 <<
                -1 <<
                true <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04)) <<
                        TA(4) <<
                        TA(4.1, QVector<TS>() << TS(3, 4, 0x07)) <<
                        TA(QVector<TS>() << TS(5, 6, 0x10)));

        QTest::newRow("Multiple") <<
                2 <<
                1 <<
                -1 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02), QVector<TS>() << TS(1, 2, 0x03)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04), QVector<TS>() << TS(2, 3, 0x07)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x08), QVector<TS>() << TS(3, 4, 0x0F)) <<
                        TA(QVector<TS>() << TS(5, 6, 0x10), QVector<TS>() << TS(4, 5, 0x1e)) <<
                        TA(QVector<TS>(), QVector<TS>() << TS(5, 6, 0x1C)));

        QTest::newRow("Multiple Gap") <<
                2 <<
                2 <<
                0 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04), QVector<TS>() << TS(1, 2, 0x07)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x08), QVector<TS>() << TS(2, 3, 0x0F)) <<
                        TA(QVector<TS>() << TS(6, 7, 0x10),
                           QVector<TS>() << TS(3, 4, 0x0F) << QVector<TS>() << TS(4, 5, 0x0E)) <<
                        TA(QVector<TS>() << TS(7, 8, 0x20)) <<
                        TA(QVector<TS>() << TS(8, 9, 0x40), QVector<TS>() << TS(6, 7, 0x70)) <<
                        TA(QVector<TS>(),
                           QVector<TS>() << TS(7, 8, 0x70) << QVector<TS>() << TS(8, 9, 0x70)));

        QTest::newRow("Multiple Gap Advance") <<
                2 <<
                2 <<
                0 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04), QVector<TS>() << TS(1, 2, 0x07)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x08), QVector<TS>() << TS(2, 3, 0x0F)) <<
                        TA(6, QVector<TS>() << TS(3, 4, 0x0F) << QVector<TS>() << TS(4, 5, 0x0E)) <<
                        TA(QVector<TS>() << TS(7, 8, 0x10)) <<
                        TA(QVector<TS>() << TS(8, 9, 0x20)) <<
                        TA(QVector<TS>(),
                           QVector<TS>() << TS(7, 8, 0x30) << QVector<TS>() << TS(8, 9, 0x30)));

        QTest::newRow("Gap Completed") <<
                1 <<
                1 <<
                1 <<
                true <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x04), QVector<TS>() << TS(2, 3, 0x03)) <<
                        TA(QVector<TS>() << TS(5, 6, 0x08), QVector<TS>() << TS(4, 5, 0x0C)) <<
                        TA(QVector<TS>() << TS(8, 9, 0x10)) <<
                        TA(QVector<TS>() << TS(9, 10, 0x20)) <<
                        TA(QVector<TS>() << TS(10, 11, 0x40), QVector<TS>() << TS(9, 10, 0x70)));

        QTest::newRow("Gap Completed Advance") <<
                1 <<
                1 <<
                1 <<
                true <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x04), QVector<TS>() << TS(2, 3, 0x03)) <<
                        TA(QVector<TS>() << TS(5, 6, 0x08), QVector<TS>() << TS(4, 5, 0x0C)) <<
                        TA(8) <<
                        TA(QVector<TS>() << TS(8, 9, 0x10)) <<
                        TA(QVector<TS>() << TS(9, 10, 0x20)) <<
                        TA(QVector<TS>() << TS(10, 11, 0x40), QVector<TS>() << TS(9, 10, 0x70)));

        QTest::newRow("Before Only") <<
                2 <<
                0 <<
                -1 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01), QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02), QVector<TS>() << TS(2, 3, 0x03)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04), QVector<TS>() << TS(3, 4, 0x07)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x08), QVector<TS>() << TS(4, 5, 0x0E)) <<
                        TA(QVector<TS>() << TS(5, 6, 0x10), QVector<TS>() << TS(5, 6, 0x1C)) <<
                        TA(QVector<TS>() << TS(7, 8, 0x20), QVector<TS>() << TS(7, 8, 0x30)) <<
                        TA(QVector<TS>() << TS(8, 9, 0x40), QVector<TS>() << TS(8, 9, 0x60)));

        QTest::newRow("Before Only Gap") <<
                2 <<
                0 <<
                0 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01), QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02), QVector<TS>() << TS(2, 3, 0x03)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04), QVector<TS>() << TS(3, 4, 0x07)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x08), QVector<TS>() << TS(4, 5, 0x0E)) <<
                        TA(QVector<TS>() << TS(6, 7, 0x10), QVector<TS>() << TS(6, 7, 0x10)) <<
                        TA(QVector<TS>() << TS(7, 8, 0x20), QVector<TS>() << TS(7, 8, 0x30)) <<
                        TA(QVector<TS>() << TS(8, 9, 0x40), QVector<TS>() << TS(8, 9, 0x70)) <<
                        TA(QVector<TS>() << TS(9, 10, 0x80), QVector<TS>() << TS(9, 10, 0xE0)));

        QTest::newRow("Before Only Completed") <<
                2 <<
                0 <<
                -1 <<
                true <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04), QVector<TS>() << TS(3, 4, 0x07)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x08), QVector<TS>() << TS(4, 5, 0x0E)) <<
                        TA(QVector<TS>() << TS(5, 6, 0x10), QVector<TS>() << TS(5, 6, 0x1C)));

        QTest::newRow("Before Only Completed Gap") <<
                2 <<
                0 <<
                0 <<
                true <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04), QVector<TS>() << TS(3, 4, 0x07)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x08), QVector<TS>() << TS(4, 5, 0x0E)) <<
                        TA(QVector<TS>() << TS(6, 7, 0x10)) <<
                        TA(QVector<TS>() << TS(7, 8, 0x20)) <<
                        TA(QVector<TS>() << TS(8, 9, 0x40), QVector<TS>() << TS(8, 9, 0x70)));

        QTest::newRow("After Only") <<
                0 <<
                2 <<
                -1 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04), QVector<TS>() << TS(1, 2, 0x07)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x08), QVector<TS>() << TS(2, 3, 0x0E)) <<
                        TA(QVector<TS>() << TS(5, 6, 0x10), QVector<TS>() << TS(3, 4, 0x1C)) <<
                        TA(QVector<TS>() << TS(7, 8, 0x20),
                           QVector<TS>() << TS(4, 5, 0x18) << TS(5, 6, 0x30)) <<
                        TA(QVector<TS>() << TS(8, 9, 0x40)) <<
                        TA(QVector<TS>() << TS(9, 10, 0x80), QVector<TS>() << TS(7, 8, 0xE0)) <<
                        TA(QVector<TS>(), QVector<TS>() << TS(8, 9, 0xC0) << TS(9, 10, 0x80)));

        QTest::newRow("After Only Gap") <<
                0 <<
                2 <<
                0 <<
                false <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04), QVector<TS>() << TS(1, 2, 0x07)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x08), QVector<TS>() << TS(2, 3, 0x0E)) <<
                        TA(QVector<TS>() << TS(6, 7, 0x10),
                           QVector<TS>() << TS(3, 4, 0x0C) << TS(4, 5, 0x08)) <<
                        TA(QVector<TS>() << TS(7, 8, 0x20)) <<
                        TA(QVector<TS>() << TS(8, 9, 0x40), QVector<TS>() << TS(6, 7, 0x70)) <<
                        TA(QVector<TS>() << TS(9, 10, 0x80), QVector<TS>() << TS(7, 8, 0xE0)) <<
                        TA(QVector<TS>(), QVector<TS>() << TS(8, 9, 0xC0) << TS(9, 10, 0x80)));

        QTest::newRow("After Only Completed") <<
                0 <<
                2 <<
                -1 <<
                true <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04), QVector<TS>() << TS(1, 2, 0x07)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x08), QVector<TS>() << TS(2, 3, 0x0E)) <<
                        TA(QVector<TS>() << TS(5, 6, 0x10), QVector<TS>() << TS(3, 4, 0x1C)) <<
                        TA(QVector<TS>() << TS(7, 8, 0x20),
                           QVector<TS>() << TS(4, 5, 0x18) << TS(5, 6, 0x30)) <<
                        TA(QVector<TS>() << TS(8, 9, 0x40)) <<
                        TA(QVector<TS>() << TS(9, 10, 0x80), QVector<TS>() << TS(7, 8, 0xE0)));

        QTest::newRow("After Only Completed Gap") <<
                0 <<
                2 <<
                0 <<
                true <<
                (QList<TA>() <<
                        TA(QVector<TS>() << TS(1, 2, 0x01)) <<
                        TA(QVector<TS>() << TS(2, 3, 0x02)) <<
                        TA(QVector<TS>() << TS(3, 4, 0x04), QVector<TS>() << TS(1, 2, 0x07)) <<
                        TA(QVector<TS>() << TS(4, 5, 0x08), QVector<TS>() << TS(2, 3, 0x0E)) <<
                        TA(QVector<TS>() << TS(6, 7, 0x10)) <<
                        TA(QVector<TS>() << TS(7, 8, 0x20)) <<
                        TA(QVector<TS>() << TS(8, 9, 0x40), QVector<TS>() << TS(6, 7, 0x70)) <<
                        TA(QVector<TS>() << TS(9, 10, 0x80), QVector<TS>() << TS(7, 8, 0xE0)));
    }

    void mean()
    {
        Data::Variant::Root config;
        config["Interval/Unit"].setString("Second");
        config["Interval/Count"].setInt64(10);
        SequenceName u("bnd", "raw", "BsG_S11");
        TriggerInput *i = new TriggerInputMean(new TriggerInputValue(SequenceMatch::OrderedLookup(u)), config);

        QVector<TriggerInput::Result> r;
        QVector<TriggerInput::Result> e;

        QList<EditDataTarget *> targets(i->unhandledInput(u));
        targets.append(i->unhandledInput(u.withFlavor("pm1")));

        r = i->process();
        QVERIFY(!i->isReady(10));
        QVERIFY(!i->isReady(20));
        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(1.0), 10, 20));
        }
        i->incomingDataAdvance(10);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(20));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(2.0), 20, 30));
        }
        i->incomingDataAdvance(20);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(30));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(3.0), 30, 40));
        }
        i->incomingDataAdvance(30);
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(i->isReady(20));

        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 3);
        QVERIFY(i->isReady(40));

        e <<
                TriggerInput::Result(10.0, 20.0, 1.5) <<
                TriggerInput::Result(20.0, 30.0, 2.0) <<
                TriggerInput::Result(30.0, 40.0, 2.5);
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;

        targets = i->unhandledInput(u);
        targets.append(i->unhandledInput(u.withFlavor("pm1")));

        r = i->process();
        QVERIFY(!i->isReady(10));
        QVERIFY(!i->isReady(20));
        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(1.0), 10, 20));
        }
        i->incomingDataAdvance(10);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(20));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(2.0), 20, 30));
        }
        i->incomingDataAdvance(20);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(30));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(3.0), 30, 40));
        }
        i->incomingDataAdvance(30);
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(i->isReady(20));

        i->incomingDataAdvance(31.0);
        r << i->process();
        QCOMPARE(r.size(), 2);
        QVERIFY(i->isReady(30));

        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 3);
        QVERIFY(i->isReady(40));

        QCOMPARE(r, e);

        delete i;
    }

    void sd()
    {
        Data::Variant::Root config;
        config["Interval/Unit"].setString("Second");
        config["Interval/Count"].setInt64(10);
        SequenceName u("bnd", "raw", "BsG_S11");
        TriggerInput *i = new TriggerInputSD(new TriggerInputValue(SequenceMatch::OrderedLookup(u)), config);

        QVector<TriggerInput::Result> r;
        QVector<TriggerInput::Result> e;

        QList<EditDataTarget *> targets(i->unhandledInput(u));
        targets.append(i->unhandledInput(u.withFlavor("pm1")));

        r = i->process();
        QVERIFY(!i->isReady(10));
        QVERIFY(!i->isReady(20));
        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(1.0), 10, 20));
        }
        i->incomingDataAdvance(10);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(20));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(2.0), 20, 30));
        }
        i->incomingDataAdvance(20);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(30));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(3.0), 30, 40));
        }
        i->incomingDataAdvance(30);
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(i->isReady(20));

        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 3);
        QVERIFY(i->isReady(40));

        e <<
                TriggerInput::Result(10.0, 20.0, sqrt(0.5)) <<
                TriggerInput::Result(20.0, 30.0, 1.0) <<
                TriggerInput::Result(30.0, 40.0, sqrt(0.5));
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;

        targets = i->unhandledInput(u);
        targets.append(i->unhandledInput(u.withFlavor("pm1")));

        r = i->process();
        QVERIFY(!i->isReady(10));
        QVERIFY(!i->isReady(20));
        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(1.0), 10, 20));
        }
        i->incomingDataAdvance(10);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(20));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(2.0), 20, 30));
        }
        i->incomingDataAdvance(20);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(30));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(3.0), 30, 40));
        }
        i->incomingDataAdvance(30);
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(i->isReady(20));

        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 3);
        QVERIFY(i->isReady(40));

        QCOMPARE(r, e);

        delete i;
    }

    void quantile()
    {
        Data::Variant::Root config;
        config["Interval/Unit"].setString("Second");
        config["Interval/Count"].setInt64(10);
        SequenceName u("bnd", "raw", "BsG_S11");
        TriggerInput *i =
                new TriggerInputQuantile(new TriggerInputValue(SequenceMatch::OrderedLookup(u)), 0.75, config);

        QVector<TriggerInput::Result> r;
        QVector<TriggerInput::Result> e;

        QList<EditDataTarget *> targets(i->unhandledInput(u));
        targets.append(i->unhandledInput(u.withFlavor("pm1")));

        r = i->process();
        QVERIFY(!i->isReady(10));
        QVERIFY(!i->isReady(20));
        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(1.0), 10, 20));
        }
        i->incomingDataAdvance(10);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(20));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(2.0), 20, 30));
        }
        i->incomingDataAdvance(20);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(30));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(3.0), 30, 40));
        }
        i->incomingDataAdvance(30);
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(i->isReady(20));

        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 3);
        QVERIFY(i->isReady(40));

        e <<
                TriggerInput::Result(10.0, 20.0, 1.75) <<
                TriggerInput::Result(20.0, 30.0, 2.5) <<
                TriggerInput::Result(30.0, 40.0, 2.75);
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;

        targets = i->unhandledInput(u);
        targets.append(i->unhandledInput(u.withFlavor("pm1")));

        r = i->process();
        QVERIFY(!i->isReady(10));
        QVERIFY(!i->isReady(20));
        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(1.0), 10, 20));
        }
        i->incomingDataAdvance(10);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(20));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(2.0), 20, 30));
        }
        i->incomingDataAdvance(20);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(30));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(3.0), 30, 40));
        }
        i->incomingDataAdvance(30);
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(i->isReady(20));

        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 3);
        QVERIFY(i->isReady(40));

        QCOMPARE(r, e);

        delete i;
    }

    void slope()
    {
        Data::Variant::Root config;
        config["Interval/Unit"].setString("Second");
        config["Interval/Count"].setInt64(10);
        SequenceName u("bnd", "raw", "BsG_S11");
        TriggerInput
                *i = new TriggerInputSlope(new TriggerInputValue(SequenceMatch::OrderedLookup(u)), config);

        QVector<TriggerInput::Result> r;
        QVector<TriggerInput::Result> e;

        QList<EditDataTarget *> targets(i->unhandledInput(u));
        targets.append(i->unhandledInput(u.withFlavor("pm1")));

        r = i->process();
        QVERIFY(!i->isReady(10));
        QVERIFY(!i->isReady(20));
        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(10.0), 10, 20));
        }
        i->incomingDataAdvance(10);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(20));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(20.0), 20, 30));
        }
        i->incomingDataAdvance(20);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(30));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(30.0), 30, 40));
        }
        i->incomingDataAdvance(30);
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(i->isReady(20));

        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 3);
        QVERIFY(i->isReady(40));

        e <<
                TriggerInput::Result(10.0, 20.0, 1.0) <<
                TriggerInput::Result(20.0, 30.0, 1.0) <<
                TriggerInput::Result(30.0, 40.0, 1.0);
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;

        targets = i->unhandledInput(u);
        targets.append(i->unhandledInput(u.withFlavor("pm1")));

        r = i->process();
        QVERIFY(!i->isReady(10));
        QVERIFY(!i->isReady(20));
        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(10.0), 10, 20));
        }
        i->incomingDataAdvance(10);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(20));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(20.0), 20, 30));
        }
        i->incomingDataAdvance(20);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(30));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(30.0), 30, 40));
        }
        i->incomingDataAdvance(30);
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(i->isReady(20));

        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 3);
        QVERIFY(i->isReady(40));

        QCOMPARE(r, e);

        delete i;
    }

    void length()
    {
        Data::Variant::Root config;
        config["Interval/Unit"].setString("Second");
        config["Interval/Count"].setInt64(10);
        SequenceName u("bnd", "raw", "BsG_S11");
        TriggerInput
                *i = new TriggerInputLength(new TriggerInputValue(SequenceMatch::OrderedLookup(u)), config);

        QVector<TriggerInput::Result> r;
        QVector<TriggerInput::Result> e;

        QList<EditDataTarget *> targets(i->unhandledInput(u));
        targets.append(i->unhandledInput(u.withFlavor("pm1")));

        r = i->process();
        QVERIFY(!i->isReady(10));
        QVERIFY(!i->isReady(20));
        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(10.0), 10, 20));
        }
        i->incomingDataAdvance(10);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(20));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(20.0), 20, 30));
        }
        i->incomingDataAdvance(20);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(30));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(30.0), 30, 40));
        }
        i->incomingDataAdvance(30);
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(i->isReady(20));

        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 3);
        QVERIFY(i->isReady(40));

        e <<
                TriggerInput::Result(10.0, 20.0, 20.0) <<
                TriggerInput::Result(20.0, 30.0, 30.0) <<
                TriggerInput::Result(30.0, 40.0, 20.0);
        QCOMPARE(r, e);

        TriggerInput *j = i->clone();
        delete i;
        i = j;

        targets = i->unhandledInput(u);
        targets.append(i->unhandledInput(u.withFlavor("pm1")));

        r = i->process();
        QVERIFY(!i->isReady(10));
        QVERIFY(!i->isReady(20));
        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(10.0), 10, 20));
        }
        i->incomingDataAdvance(10);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(20));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(20.0), 20, 30));
        }
        i->incomingDataAdvance(20);
        r << i->process();
        QCOMPARE(r.size(), 0);
        QVERIFY(!i->isReady(30));

        for (QList<EditDataTarget *>::const_iterator t = targets.constBegin(),
                end = targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(SequenceValue(u, Variant::Root(30.0), 30, 40));
        }
        i->incomingDataAdvance(30);
        r << i->process();
        QCOMPARE(r.size(), 1);
        QVERIFY(i->isReady(20));

        i->finalize();
        r << i->process();
        QCOMPARE(r.size(), 3);
        QVERIFY(i->isReady(40));

        QCOMPARE(r, e);

        delete i;
    }
};

QTEST_MAIN(TestTriggerBuffer)

#include "triggerbuffer.moc"
