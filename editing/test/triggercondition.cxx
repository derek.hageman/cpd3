/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/triggercondition.hxx"
#include "editing/triggerbuffer.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

static void sendValue(Trigger *input,
                      QHash<SequenceName, QList<EditDataTarget *> > &dispatch,
                      const SequenceName &unit,
                      double value,
                      double start,
                      double end)
{
    QHash<SequenceName, QList<EditDataTarget *> >::iterator target = dispatch.find(unit);
    if (target == dispatch.end()) {
        target = dispatch.insert(unit, input->unhandledInput(unit));
    }
    for (QList<EditDataTarget *>::const_iterator t = target.value().constBegin(),
            endT = target.value().constEnd(); t != endT; ++t) {
        (*t)->incomingSequenceValue(SequenceValue(unit, Variant::Root(value), start, end));
    }
}

class TestTriggerCondition : public QObject {
Q_OBJECT
private slots:

    void lessThan()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        Trigger *i = new TriggerLessThan(new TriggerInputValue(SequenceMatch::OrderedLookup(u1)),
                                         new TriggerInputValue(SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 20, 200, 300);
        sendValue(i, d, u2, 10, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(i->appliesTo(100, 200));

        sendValue(i, d, u1, 20, 300, 400);
        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));

        i->advance(200);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 20, 200, 300);
        sendValue(i, d, u2, 10, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(i->appliesTo(100, 200));

        sendValue(i, d, u1, 20, 300, 400);
        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));

        i->advance(200);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));

        delete i;
    }

    void lessThanEqual()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        Trigger *i = new TriggerLessThanEqual(new TriggerInputValue(SequenceMatch::OrderedLookup(u1)),
                                              new TriggerInputValue(SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 20, 200, 300);
        sendValue(i, d, u2, 10, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(i->appliesTo(100, 200));

        sendValue(i, d, u1, 20, 300, 400);
        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));

        i->advance(200);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 20, 200, 300);
        sendValue(i, d, u2, 10, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(i->appliesTo(100, 200));

        sendValue(i, d, u1, 20, 300, 400);
        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));

        i->advance(200);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));

        delete i;
    }

    void greaterThan()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        Trigger *i = new TriggerGreaterThan(new TriggerInputValue(SequenceMatch::OrderedLookup(u1)),
                                            new TriggerInputValue(SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 20, 200, 300);
        sendValue(i, d, u2, 10, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(!i->appliesTo(100, 200));

        sendValue(i, d, u1, 20, 300, 400);
        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(i->appliesTo(200, 300));

        i->advance(200);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 20, 200, 300);
        sendValue(i, d, u2, 10, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(!i->appliesTo(100, 200));

        sendValue(i, d, u1, 20, 300, 400);
        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(i->appliesTo(200, 300));

        i->advance(200);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));

        delete i;
    }

    void greaterThanEqual()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        Trigger *i = new TriggerGreaterThanEqual(new TriggerInputValue(SequenceMatch::OrderedLookup(u1)),
                                                 new TriggerInputValue(SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 20, 200, 300);
        sendValue(i, d, u2, 10, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(!i->appliesTo(100, 200));

        sendValue(i, d, u1, 20, 300, 400);
        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(i->appliesTo(200, 300));

        i->advance(200);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 20, 200, 300);
        sendValue(i, d, u2, 10, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(!i->appliesTo(100, 200));

        sendValue(i, d, u1, 20, 300, 400);
        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(i->appliesTo(200, 300));

        i->advance(200);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));

        delete i;
    }

    void equal()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        Trigger *i = new TriggerEqual(new TriggerInputValue(SequenceMatch::OrderedLookup(u1)),
                                      new TriggerInputValue(SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 20, 200, 300);
        sendValue(i, d, u2, 10, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(!i->appliesTo(100, 200));

        sendValue(i, d, u1, 20, 300, 400);
        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));

        i->advance(200);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 20, 200, 300);
        sendValue(i, d, u2, 10, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(!i->appliesTo(100, 200));

        sendValue(i, d, u1, 20, 300, 400);
        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));

        i->advance(200);
        QVERIFY(i->isReady(300));
        QVERIFY(!i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));

        delete i;
    }

    void notEqual()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        Trigger *i = new TriggerNotEqual(new TriggerInputValue(SequenceMatch::OrderedLookup(u1)),
                                         new TriggerInputValue(SequenceMatch::OrderedLookup(u2)));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 20, 200, 300);
        sendValue(i, d, u2, 10, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(i->appliesTo(100, 200));

        sendValue(i, d, u1, 20, 300, 400);
        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(i->appliesTo(200, 300));

        i->advance(200);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 20, 200, 300);
        sendValue(i, d, u2, 10, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(i->appliesTo(100, 200));

        sendValue(i, d, u1, 20, 300, 400);
        sendValue(i, d, u2, 20, 300, 400);
        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(i->appliesTo(200, 300));

        i->advance(200);
        QVERIFY(i->isReady(300));
        QVERIFY(i->appliesTo(200, 300));

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));

        delete i;
    }

    void insideRange()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        SequenceName u3("brw", "raw", "3");
        Trigger *i = new TriggerInsideRange(new TriggerInputValue(SequenceMatch::OrderedLookup(u1)),
                                            new TriggerInputValue(SequenceMatch::OrderedLookup(u2)),
                                            new TriggerInputValue(SequenceMatch::OrderedLookup(u3)));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        sendValue(i, d, u3, 30, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 10, 200, 300);
        sendValue(i, d, u2, 40, 200, 300);
        sendValue(i, d, u3, 20, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(i->appliesTo(100, 200));

        sendValue(i, d, u1, 10, 300, 400);
        sendValue(i, d, u2, 0, 300, 400);
        sendValue(i, d, u3, 20, 300, 400);

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        sendValue(i, d, u3, 30, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 10, 200, 300);
        sendValue(i, d, u2, 40, 200, 300);
        sendValue(i, d, u3, 20, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(i->appliesTo(100, 200));

        sendValue(i, d, u1, 10, 300, 400);
        sendValue(i, d, u2, 0, 300, 400);
        sendValue(i, d, u3, 20, 300, 400);

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));

        delete i;
    }

    void outsideRange()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        SequenceName u3("brw", "raw", "3");
        Trigger *i = new TriggerOutsideRange(new TriggerInputValue(SequenceMatch::OrderedLookup(u1)),
                                             new TriggerInputValue(SequenceMatch::OrderedLookup(u2)),
                                             new TriggerInputValue(SequenceMatch::OrderedLookup(u3)));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        sendValue(i, d, u3, 30, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 10, 200, 300);
        sendValue(i, d, u2, 40, 200, 300);
        sendValue(i, d, u3, 20, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(!i->appliesTo(100, 200));

        sendValue(i, d, u1, 10, 300, 400);
        sendValue(i, d, u2, 0, 300, 400);
        sendValue(i, d, u3, 20, 300, 400);

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        sendValue(i, d, u3, 30, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u1, 10, 200, 300);
        sendValue(i, d, u2, 40, 200, 300);
        sendValue(i, d, u3, 20, 200, 300);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(!i->appliesTo(100, 200));

        sendValue(i, d, u1, 10, 300, 400);
        sendValue(i, d, u2, 0, 300, 400);
        sendValue(i, d, u3, 20, 300, 400);

        i->finalize();
        QVERIFY(i->isReady(400));
        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));

        delete i;
    }

    void insideModularRange()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        SequenceName u3("brw", "raw", "3");
        SequenceName u4("brw", "raw", "4");
        SequenceName u5("brw", "raw", "5");
        Trigger *i = new TriggerInsideModularRange(new TriggerInputValue(SequenceMatch::OrderedLookup(u1)),
                                                   new TriggerInputValue(SequenceMatch::OrderedLookup(u2)),
                                                   new TriggerInputValue(SequenceMatch::OrderedLookup(u3)),
                                                   new TriggerInputValue(SequenceMatch::OrderedLookup(u4)),
                                                   new TriggerInputValue(SequenceMatch::OrderedLookup(u5)));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        sendValue(i, d, u3, 30, 100, 200);
        sendValue(i, d, u4, 0, 100, 400);
        sendValue(i, d, u5, 40, 100, 400);

        sendValue(i, d, u1, 10, 200, 300);
        sendValue(i, d, u2, 35, 200, 300);
        sendValue(i, d, u3, 30, 200, 300);

        sendValue(i, d, u1, 10, 300, 400);
        sendValue(i, d, u2, 5, 300, 400);
        sendValue(i, d, u3, 30, 300, 400);

        sendValue(i, d, u1, -5, 400, 800);
        sendValue(i, d, u3, 50, 400, 800);
        sendValue(i, d, u4, -40, 400, FP::undefined());
        sendValue(i, d, u5, 100, 400, FP::undefined());
        sendValue(i, d, u2, 10, 400, 500);

        sendValue(i, d, u2, 35, 500, 600);

        sendValue(i, d, u2, 110, 600, 700);

        sendValue(i, d, u2, -50, 700, 800);

        sendValue(i, d, u1, 90, 800, FP::undefined());
        sendValue(i, d, u3, -30, 800, FP::undefined());
        sendValue(i, d, u2, 95, 800, 900);

        sendValue(i, d, u2, -35, 900, 1000);

        sendValue(i, d, u2, -20, 1000, 1100);

        sendValue(i, d, u2, 80, 1100, 1200);

        sendValue(i, d, u2, FP::undefined(), 1200, 1300);

        i->finalize();
        QVERIFY(i->isReady(1200));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));
        QVERIFY(i->appliesTo(400, 500));
        QVERIFY(i->appliesTo(500, 600));
        QVERIFY(!i->appliesTo(600, 700));
        QVERIFY(!i->appliesTo(700, 800));
        QVERIFY(i->appliesTo(800, 900));
        QVERIFY(i->appliesTo(900, 1000));
        QVERIFY(!i->appliesTo(1000, 1100));
        QVERIFY(!i->appliesTo(1100, 1200));
        QVERIFY(!i->appliesTo(1200, 1300));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        sendValue(i, d, u3, 30, 100, 200);
        sendValue(i, d, u4, 0, 100, 400);
        sendValue(i, d, u5, 40, 100, 400);

        sendValue(i, d, u1, 10, 200, 300);
        sendValue(i, d, u2, 35, 200, 300);
        sendValue(i, d, u3, 30, 200, 300);

        sendValue(i, d, u1, 10, 300, 400);
        sendValue(i, d, u2, 5, 300, 400);
        sendValue(i, d, u3, 30, 300, 400);

        sendValue(i, d, u1, -5, 400, 800);
        sendValue(i, d, u3, 50, 400, 800);
        sendValue(i, d, u4, -40, 400, FP::undefined());
        sendValue(i, d, u5, 100, 400, FP::undefined());
        sendValue(i, d, u2, 10, 400, 500);

        sendValue(i, d, u2, 35, 500, 600);

        sendValue(i, d, u2, 110, 600, 700);

        sendValue(i, d, u2, -50, 700, 800);

        sendValue(i, d, u1, 90, 800, FP::undefined());
        sendValue(i, d, u3, -30, 800, FP::undefined());
        sendValue(i, d, u2, 95, 800, 900);

        sendValue(i, d, u2, -35, 900, 1000);

        sendValue(i, d, u2, -20, 1000, 1100);

        sendValue(i, d, u2, 80, 1100, 1200);

        sendValue(i, d, u2, FP::undefined(), 1200, 1300);

        i->finalize();
        QVERIFY(i->isReady(1200));
        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));
        QVERIFY(i->appliesTo(400, 500));
        QVERIFY(i->appliesTo(500, 600));
        QVERIFY(!i->appliesTo(600, 700));
        QVERIFY(!i->appliesTo(700, 800));
        QVERIFY(i->appliesTo(800, 900));
        QVERIFY(i->appliesTo(900, 1000));
        QVERIFY(!i->appliesTo(1000, 1100));
        QVERIFY(!i->appliesTo(1100, 1200));
        QVERIFY(!i->appliesTo(1200, 1300));

        delete i;
    }

    void outsideModularRange()
    {
        SequenceName u1("brw", "raw", "1");
        SequenceName u2("brw", "raw", "2");
        SequenceName u3("brw", "raw", "3");
        SequenceName u4("brw", "raw", "4");
        SequenceName u5("brw", "raw", "5");
        Trigger *i = new TriggerOutsideModularRange(new TriggerInputValue(SequenceMatch::OrderedLookup(u1)),
                                                    new TriggerInputValue(SequenceMatch::OrderedLookup(u2)),
                                                    new TriggerInputValue(SequenceMatch::OrderedLookup(u3)),
                                                    new TriggerInputValue(SequenceMatch::OrderedLookup(u4)),
                                                    new TriggerInputValue(SequenceMatch::OrderedLookup(u5)));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        sendValue(i, d, u3, 30, 100, 200);
        sendValue(i, d, u4, 0, 100, 400);
        sendValue(i, d, u5, 40, 100, 400);

        sendValue(i, d, u1, 10, 200, 300);
        sendValue(i, d, u2, 35, 200, 300);
        sendValue(i, d, u3, 30, 200, 300);

        sendValue(i, d, u1, 10, 300, 400);
        sendValue(i, d, u2, 5, 300, 400);
        sendValue(i, d, u3, 30, 300, 400);

        sendValue(i, d, u1, -5, 400, 800);
        sendValue(i, d, u3, 50, 400, 800);
        sendValue(i, d, u4, -40, 400, FP::undefined());
        sendValue(i, d, u5, 100, 400, FP::undefined());
        sendValue(i, d, u2, 10, 400, 500);

        sendValue(i, d, u2, 35, 500, 600);

        sendValue(i, d, u2, 110, 600, 700);

        sendValue(i, d, u2, -50, 700, 800);

        sendValue(i, d, u1, 90, 800, FP::undefined());
        sendValue(i, d, u3, -30, 800, FP::undefined());
        sendValue(i, d, u2, 95, 800, 900);

        sendValue(i, d, u2, -35, 900, 1000);

        sendValue(i, d, u2, -20, 1000, 1100);

        sendValue(i, d, u2, 80, 1100, 1200);

        sendValue(i, d, u2, FP::undefined(), 1200, 1300);

        i->finalize();
        QVERIFY(i->isReady(1200));
        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));
        QVERIFY(!i->appliesTo(400, 500));
        QVERIFY(!i->appliesTo(500, 600));
        QVERIFY(i->appliesTo(600, 700));
        QVERIFY(i->appliesTo(700, 800));
        QVERIFY(!i->appliesTo(800, 900));
        QVERIFY(!i->appliesTo(900, 1000));
        QVERIFY(i->appliesTo(1000, 1100));
        QVERIFY(i->appliesTo(1100, 1200));
        QVERIFY(!i->appliesTo(1200, 1300));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u1, 10, 100, 200);
        sendValue(i, d, u2, 20, 100, 200);
        sendValue(i, d, u3, 30, 100, 200);
        sendValue(i, d, u4, 0, 100, 400);
        sendValue(i, d, u5, 40, 100, 400);

        sendValue(i, d, u1, 10, 200, 300);
        sendValue(i, d, u2, 35, 200, 300);
        sendValue(i, d, u3, 30, 200, 300);

        sendValue(i, d, u1, 10, 300, 400);
        sendValue(i, d, u2, 5, 300, 400);
        sendValue(i, d, u3, 30, 300, 400);

        sendValue(i, d, u1, -5, 400, 800);
        sendValue(i, d, u3, 50, 400, 800);
        sendValue(i, d, u4, -40, 400, FP::undefined());
        sendValue(i, d, u5, 100, 400, FP::undefined());
        sendValue(i, d, u2, 10, 400, 500);

        sendValue(i, d, u2, 35, 500, 600);

        sendValue(i, d, u2, 110, 600, 700);

        sendValue(i, d, u2, -50, 700, 800);

        sendValue(i, d, u1, 90, 800, FP::undefined());
        sendValue(i, d, u3, -30, 800, FP::undefined());
        sendValue(i, d, u2, 95, 800, 900);

        sendValue(i, d, u2, -35, 900, 1000);

        sendValue(i, d, u2, -20, 1000, 1100);

        sendValue(i, d, u2, 80, 1100, 1200);

        sendValue(i, d, u2, FP::undefined(), 1200, 1300);

        i->finalize();
        QVERIFY(i->isReady(1200));
        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));
        QVERIFY(!i->appliesTo(400, 500));
        QVERIFY(!i->appliesTo(500, 600));
        QVERIFY(i->appliesTo(600, 700));
        QVERIFY(i->appliesTo(700, 800));
        QVERIFY(!i->appliesTo(800, 900));
        QVERIFY(!i->appliesTo(900, 1000));
        QVERIFY(i->appliesTo(1000, 1100));
        QVERIFY(i->appliesTo(1100, 1200));
        QVERIFY(!i->appliesTo(1200, 1300));

        delete i;
    }

    void defined()
    {
        SequenceName u("brw", "raw", "1");
        Trigger *i = new TriggerDefined(new TriggerInputValue(SequenceMatch::OrderedLookup(u)));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u, 10, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));

        sendValue(i, d, u, FP::undefined(), 300, 400);
        sendValue(i, d, u, 20, 400, 500);
        i->finalize();

        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));
        QVERIFY(i->appliesTo(400, 500));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u, 10, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));

        sendValue(i, d, u, FP::undefined(), 300, 400);
        sendValue(i, d, u, 20, 400, 500);
        i->finalize();

        QVERIFY(i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(!i->appliesTo(300, 400));
        QVERIFY(i->appliesTo(400, 500));

        delete i;
    }

    void undefined()
    {
        SequenceName u("brw", "raw", "1");
        Trigger *i = new TriggerUndefined(new TriggerInputValue(SequenceMatch::OrderedLookup(u)));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u, 10, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));

        sendValue(i, d, u, FP::undefined(), 300, 400);
        sendValue(i, d, u, 20, 400, 500);
        i->finalize();

        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));
        QVERIFY(!i->appliesTo(400, 500));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u, 10, 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        i->incomingDataAdvance(300);
        QVERIFY(i->isReady(300));

        sendValue(i, d, u, FP::undefined(), 300, 400);
        sendValue(i, d, u, 20, 400, 500);
        i->finalize();

        QVERIFY(!i->appliesTo(100, 200));
        QVERIFY(!i->appliesTo(200, 300));
        QVERIFY(i->appliesTo(300, 400));
        QVERIFY(!i->appliesTo(400, 500));

        delete i;
    }
};

QTEST_MAIN(TestTriggerCondition)

#include "triggercondition.moc"
