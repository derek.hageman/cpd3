/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/triggerscript.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

static void sendValue(Trigger *input,
                      QHash<SequenceName, QList<EditDataTarget *> > &dispatch,
                      const SequenceName &unit, const Variant::Root &value,
                      double start,
                      double end)
{
    QHash<SequenceName, QList<EditDataTarget *> >::iterator target = dispatch.find(unit);
    if (target == dispatch.end()) {
        target = dispatch.insert(unit, input->unhandledInput(unit));
    }
    for (QList<EditDataTarget *>::const_iterator t = target.value().constBegin(),
            endT = target.value().constEnd(); t != endT; ++t) {
        (*t)->incomingSequenceValue(SequenceValue(unit, value, start, end));
    }
}

class TestTriggerScript : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        SequenceName u("brw", "raw", "a");
        SequenceName v("brw", "raw", "b");
        Trigger *i = new TriggerScript("return data.a > 10", SequenceMatch::OrderedLookup(u));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u, Variant::Root(50), 100, 200);
        sendValue(i, d, v, Variant::Root(1), 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u, Variant::Root(20), 200, 400);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(i->appliesTo(100, 200));

        sendValue(i, d, u, Variant::Root(5), 300, 400);
        sendValue(i, d, u, Variant::Root(6), 400, 500);
        sendValue(i, d, u, Variant::Root(50), 600, 700);

        QVERIFY(i->isReady(400));
        QVERIFY(i->appliesTo(200, 400));

        i->incomingDataAdvance(600);
        QVERIFY(i->isReady(600));
        QVERIFY(!i->appliesTo(300, 400));
        QVERIFY(!i->appliesTo(400, 500));

        i->finalize();
        QVERIFY(i->appliesTo(600, 700));

        Trigger *j = i->clone();
        delete i;
        i = j;
        d.clear();

        sendValue(i, d, u, Variant::Root(50), 100, 200);
        sendValue(i, d, v, Variant::Root(1), 100, 200);
        i->incomingDataAdvance(100);
        QVERIFY(i->isReady(100));

        sendValue(i, d, u, Variant::Root(20), 200, 400);
        i->incomingDataAdvance(200);
        QVERIFY(i->isReady(200));
        QVERIFY(i->appliesTo(100, 200));

        sendValue(i, d, u, Variant::Root(5), 300, 400);
        sendValue(i, d, u, Variant::Root(6), 400, 500);
        sendValue(i, d, u, Variant::Root(50), 600, 700);

        QVERIFY(i->isReady(400));
        QVERIFY(i->appliesTo(200, 400));

        i->incomingDataAdvance(600);
        QVERIFY(i->isReady(600));
        QVERIFY(!i->appliesTo(300, 400));
        QVERIFY(!i->appliesTo(400, 500));

        i->finalize();
        QVERIFY(i->appliesTo(600, 700));

        delete i;
    }

    void error()
    {
        Logging::suppressGlobalForTesting();

        SequenceName u("brw", "raw", "a");
        SequenceName v("brw", "raw", "b");
        Trigger *i =
                new TriggerScript("error(); return data.a > 10", SequenceMatch::OrderedLookup(u));

        QHash<SequenceName, QList<EditDataTarget *> > d;

        sendValue(i, d, u, Variant::Root(50), 100, 200);
        sendValue(i, d, v, Variant::Root(1), 100, 200);
        i->incomingDataAdvance(100);

        sendValue(i, d, u, Variant::Root(20), 200, 400);
        i->incomingDataAdvance(200);

        sendValue(i, d, u, Variant::Root(5), 300, 400);
        sendValue(i, d, u, Variant::Root(6), 400, 500);
        sendValue(i, d, u, Variant::Root(50), 600, 700);

        i->incomingDataAdvance(600);

        i->finalize();

        delete i;
    }
};

QTEST_MAIN(TestTriggerScript)

#include "triggerscript.moc"
