/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QList>

#include "editing/actionunit.hxx"
#include "datacore/stream.hxx"
#include "datacore/staticmultiplexer.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

class Dispatch : public EditDispatchTarget {
    Action *action;

    struct Target {
        QList<EditDataTarget *> targets;
        QList<EditDataModification *> modifiers;
        int muxId;

        Target() : targets(), modifiers(), muxId(0)
        { }
    };

    QHash<SequenceName, Target> d;

    StaticMultiplexer outputMux;

    void handleCompleted(const SequenceValue::Transfer &add)
    {
        Util::append(add, output);
    }

public:
    SequenceValue::Transfer output;

    Dispatch() : action(NULL), d(), outputMux()
    { }

    virtual ~Dispatch()
    { }

    void initialize(Action *a)
    {
        output.clear();
        d.clear();
        action = a;

        outputMux.clear(true);
        outputMux.setStreams(2);

        action->setTarget(this);
    }

    void value(SequenceValue value)
    {
        QHash<SequenceName, Target>::iterator target = d.find(value.getUnit());
        if (target == d.end()) {
            QList<EditDataTarget *> inputs;
            QList<EditDataTarget *> outputs;
            QList<EditDataModification *> modifiers;

            action->unhandledInput(value.getUnit(), inputs, outputs, modifiers);

            Target add;
            add.targets = inputs;
            add.targets.append(outputs);
            add.modifiers = modifiers;

            if (outputs.isEmpty()) {
                add.muxId = 0;
            } else {
                add.muxId = -1;
            }

            target = d.insert(value.getUnit(), add);
        }

        for (QList<EditDataModification *>::const_iterator
                m = target.value().modifiers.constBegin(),
                end = target.value().modifiers.constEnd(); m != end; ++m) {
            (*m)->modifySequenceValue(value);
        }
        for (QList<EditDataTarget *>::const_iterator t = target.value().targets.constBegin(),
                end = target.value().targets.constEnd(); t != end; ++t) {
            (*t)->incomingSequenceValue(value);
        }
        if (target.value().muxId >= 0) {
            handleCompleted(outputMux.incoming(target.value().muxId, value));
        }
    }

    void value(const SequenceName &unit, const Variant::Read &v, double start, double end)
    { value(SequenceValue(unit, Variant::Root(v), start, end)); }

    void value(const SequenceName &unit, double v, double start, double end)
    { value(SequenceValue(unit, Variant::Root(v), start, end)); }

    void advance(double time)
    {
        action->incomingDataAdvance(time);
        handleCompleted(outputMux.advance(0, time));
    }

    void finish()
    {
        action->finalize();
        handleCompleted(outputMux.finish());
    }

    void incomingData(const Data::SequenceValue &value) override
    { handleCompleted(outputMux.incoming(1, value)); }

    void incomingData(const Data::SequenceValue::Transfer &values) override
    { handleCompleted(outputMux.incoming(1, values)); }

    void incomingAdvance(double time) override
    { handleCompleted(outputMux.advance(1, time)); }

    bool compare(const SequenceValue::Transfer &expected)
    {
        if (expected.size() != output.size())
            return false;

        for (const auto &check : expected) {
            int idx;
            for (idx = output.size() - 1; idx >= 0; --idx) {
                SequenceValue compare(output.at(idx));
                if (compare.getUnit() != check.getUnit())
                    continue;
                if (!FP::equal(compare.getStart(), check.getStart()))
                    continue;
                if (!FP::equal(compare.getEnd(), check.getEnd()))
                    continue;
                if (compare.getValue() != check.getValue())
                    continue;
                output.erase(output.begin() + idx);
                break;
            }
            if (idx < 0)
                return false;
        }

        return output.empty();
    }
};

class TestActionUnit : public QObject {
Q_OBJECT
private slots:

    void setUnit()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        SequenceName c("sgp", "clean", "c", {"pm1"});
        Action *act = new ActionUnitSet(SequenceMatch::Composite(a), c);
        Dispatch d;

        d.initialize(act);

        d.value(a.toMeta(), 1.0, 100, 200);
        d.value(b.toMeta(), 2.0, 100, 200);
        d.value(a, 3.0, 200, 300);
        d.value(b, 4.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(c.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(b.toMeta(), Variant::Root(2.0), 100, 200),
                                        SequenceValue(c, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(4.0), 300, 400)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a.toMeta(), 1.0, 100, 200);
        d.value(b.toMeta(), 2.0, 100, 200);
        d.value(a, 3.0, 200, 300);
        d.value(b, 4.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(c.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(b.toMeta(), Variant::Root(2.0), 100, 200),
                                        SequenceValue(c, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(4.0), 300, 400)}));

        delete act;
    }

    void setUnitNone()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        SequenceName c("sgp", "clean", "c", {"pm1"});
        Action *act = new ActionUnitSet(SequenceMatch::Composite(a), c, ActionUnitSet::Apply_None, true);
        Dispatch d;

        d.initialize(act);

        d.value(a.toMeta(), 1.0, 100, 200);
        d.value(b.toMeta(), 2.0, 100, 200);
        d.value(a, 3.0, 200, 300);
        d.value(b, 4.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(a.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(b.toMeta(), Variant::Root(2.0), 100, 200),
                                        SequenceValue(a, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(4.0), 300, 400)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a.toMeta(), 1.0, 100, 200);
        d.value(b.toMeta(), 2.0, 100, 200);
        d.value(a, 3.0, 200, 300);
        d.value(b, 4.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(a.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(b.toMeta(), Variant::Root(2.0), 100, 200),
                                        SequenceValue(a, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(4.0), 300, 400)}));

        delete act;
    }

    void duplicateUnit()
    {
        SequenceName a("brw", "raw", "a");
        SequenceName b("brw", "raw", "b");
        SequenceName c("sgp", "clean", "c", {"pm1"});
        Action *act = new ActionUnitDuplicate(SequenceMatch::Composite(a), c);
        Dispatch d;

        d.initialize(act);

        d.value(a.toMeta(), 1.0, 100, 200);
        d.value(b.toMeta(), 2.0, 100, 200);
        d.value(a, 3.0, 200, 300);
        d.value(b, 4.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(a.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(c.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(b.toMeta(), Variant::Root(2.0), 100, 200),
                                        SequenceValue(a, Variant::Root(3.0), 200, 300),
                                        SequenceValue(c, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(4.0), 300, 400)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a.toMeta(), 1.0, 100, 200);
        d.value(b.toMeta(), 2.0, 100, 200);
        d.value(a, 3.0, 200, 300);
        d.value(b, 4.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(a.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(c.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(b.toMeta(), Variant::Root(2.0), 100, 200),
                                        SequenceValue(a, Variant::Root(3.0), 200, 300),
                                        SequenceValue(c, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(4.0), 300, 400)}));

        delete act;
    }

    void replaceUnit()
    {
        SequenceName a("brw", "raw", "a", {"10", "1"});
        SequenceName b("brw", "raw", "b");
        SequenceName c("sgp", "clean", "Qa", {"0"});
        Action *act =
                new ActionUnitReplace(SequenceMatch::Composite(a), QRegExp("brW"), "sgp", QRegExp("raw"),
                                      "clean", QRegExp("(a)"), "Q\\1", QRegExp("^(?:1)(.*)$"),
                                      "\\1");
        Dispatch d;

        d.initialize(act);

        d.value(a.toMeta(), 1.0, 100, 200);
        d.value(b.toMeta(), 2.0, 100, 200);
        d.value(a, 3.0, 200, 300);
        d.value(b, 4.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(c.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(b.toMeta(), Variant::Root(2.0), 100, 200),
                                        SequenceValue(c, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(4.0), 300, 400)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a.toMeta(), 1.0, 100, 200);
        d.value(b.toMeta(), 2.0, 100, 200);
        d.value(a, 3.0, 200, 300);
        d.value(b, 4.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(c.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(b.toMeta(), Variant::Root(2.0), 100, 200),
                                        SequenceValue(c, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(4.0), 300, 400)}));

        delete act;
    }

    void replaceDuplicateUnit()
    {
        SequenceName a("brw", "raw", "a", {"10", "1"});
        SequenceName b("brw", "raw", "b");
        SequenceName c("sgp", "clean", "Qa", {"0"});
        Action *act = new ActionUnitReplaceDuplicate(SequenceMatch::Composite(a), QRegExp("brW"), "sgp",
                                                     QRegExp("raw"), "clean", QRegExp("(a)"),
                                                     "Q\\1", QRegExp("^(?:1)(.*)$"), "\\1");
        Dispatch d;

        d.initialize(act);

        d.value(a.toMeta(), 1.0, 100, 200);
        d.value(b.toMeta(), 2.0, 100, 200);
        d.value(a, 3.0, 200, 300);
        d.value(b, 4.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(a.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(c.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(b.toMeta(), Variant::Root(2.0), 100, 200),
                                        SequenceValue(a, Variant::Root(3.0), 200, 300),
                                        SequenceValue(c, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(4.0), 300, 400)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a.toMeta(), 1.0, 100, 200);
        d.value(b.toMeta(), 2.0, 100, 200);
        d.value(a, 3.0, 200, 300);
        d.value(b, 4.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(
                SequenceValue::Transfer{SequenceValue(a.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(c.toMeta(), Variant::Root(1.0), 100, 200),
                                        SequenceValue(b.toMeta(), Variant::Root(2.0), 100, 200),
                                        SequenceValue(a, Variant::Root(3.0), 200, 300),
                                        SequenceValue(c, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(4.0), 300, 400)}));

        delete act;
    }

    void modifyFlavors()
    {
        SequenceName a("brw", "raw", "a", {"pm10", "stats"});
        SequenceName b("brw", "raw", "b");
        SequenceName c("brw", "raw", "a", {"pm1", "stats"});
        Action *act = new ActionModifyFlavors(SequenceMatch::Composite(a), {"pm1"}, {"pm10"});
        Dispatch d;

        d.initialize(act);

        d.value(a, 3.0, 200, 300);
        d.value(b, 4.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(c, Variant::Root(3.0), 200, 300),
                                                  SequenceValue(b, Variant::Root(4.0), 300, 400)}));

        Action *copy = act->clone();
        delete act;
        act = copy;

        d.initialize(act);

        d.value(a, 3.0, 200, 300);
        d.value(b, 4.0, 300, 400);

        d.finish();

        QVERIFY(d.compare(SequenceValue::Transfer{SequenceValue(c, Variant::Root(3.0), 200, 300),
                                                  SequenceValue(b, Variant::Root(4.0), 300, 400)}));

        delete act;
    }
};

QTEST_MAIN(TestActionUnit)

#include "actionunit.moc"
