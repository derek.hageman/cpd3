/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QObject>
#include <QTest>
#include <QList>

#include "editing/editcore.hxx"
#include "core/waitutils.hxx"
#include "datacore/stream.hxx"
#include "editing/triggerscript.hxx"
#include "editing/triggercondition.hxx"
#include "editing/triggerinput.hxx"
#include "editing/actionscriptgeneral.hxx"
#include "editing/actionscriptprocessor.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Editing;
using namespace CPD3::Data;

class TestTarget : public StreamSink {
    std::mutex mutex;
public:
    SequenceValue::Transfer result;
    bool ended;

    TestTarget() : mutex(), result(), ended(false)
    { }

    virtual ~TestTarget()
    { }

    void incomingData(const SequenceValue::Transfer &values) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(!ended);
        if (values.empty())
            return;
        Q_ASSERT(result.empty() ||
                         Range::compareStart(result.back().getStart(), values.front().getStart()) <=
                                 0);
        Util::append(values, result);
    }

    void endData() override
    {
        std::lock_guard<std::mutex> lock(mutex);
        ended = true;
    }

    double getLastStart()
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (result.empty())
            return FP::undefined();
        return result.back().getStart();
    }

    bool compare(const SequenceValue::Transfer &expected)
    {
        if (expected.size() != result.size())
            return false;

        for (const auto &check : expected) {
            int idx;
            for (idx = result.size() - 1; idx >= 0; --idx) {
                SequenceValue compare(result.at(idx));
                if (compare.getUnit() != check.getUnit())
                    continue;
                if (!FP::equal(compare.getStart(), check.getStart()))
                    continue;
                if (!FP::equal(compare.getEnd(), check.getEnd()))
                    continue;
                if (compare.getValue() != check.getValue())
                    continue;
                result.erase(result.begin() + idx);
                break;
            }
            if (idx < 0)
                return false;
        }

        return result.empty();
    }
};

class TestEditCore : public QObject {
Q_OBJECT
private slots:

    void empty()
    {
        SequenceName a("bnd", "raw", "a");

        std::vector<std::unique_ptr<EditDirective>> directives;
        directives.insert(directives.begin(), std::unique_ptr<EditDirective>(
                new EditDirective(1000, 2000, new TriggerAlways, new ActionRemove(SequenceMatch::Composite(a)))));

        EditCore core(std::move(directives));
        TestTarget target;
        core.start();
        core.setEgress(&target);
        core.endData();
        QVERIFY(core.wait(30000));

        QVERIFY(target.ended);
        QVERIFY(target.result.empty());
    }

    void basic()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");

        std::vector<std::unique_ptr<EditDirective>> directives;
        directives.insert(directives.begin(), std::unique_ptr<EditDirective>(
                new EditDirective(1000, 2000, new TriggerAlways, new ActionRemove(SequenceMatch::Composite(a)))));

        EditCore core(std::move(directives));
        TestTarget target;
        core.start();
        core.setEgress(&target);

        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(2.0), 200, 300),
                                                  SequenceValue(b, Variant::Root(3.0), 200, 300),
                                                  SequenceValue(a, Variant::Root(4.0), 300, 1500),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 1500),
                                                  SequenceValue(a, Variant::Root(6.0), 400, 500),
                                                  SequenceValue(b, Variant::Root(7.0), 400, 2100)});
        QTest::qSleep(50);
        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                                  SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                                  SequenceValue(a, Variant::Root(9.0), 1000, 1500),
                                                  SequenceValue(a, Variant::Root(10.0), 1200, 1300),
                                                  SequenceValue(b, Variant::Root(11.0), 1200, 2000),
                                                  SequenceValue(b, Variant::Root(12.0), 1300, 2100),
                                                  SequenceValue(a, Variant::Root(13.0), 1500, 2100),
                                                  SequenceValue(a, Variant::Root(14.0), 1800, 1900),
                                                  SequenceValue(b, Variant::Root(15.0), 1800, 1900),
                                                  SequenceValue(a, Variant::Root(16.0), 2000,
                                                                2100)});
        do {
            QTest::qSleep(50);
        } while (Range::compareStart(target.getLastStart(), 1800) < 0);
        QVERIFY(Range::compareStart(target.getLastStart(), 1800) >= 0);
        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(17.0), 2100, 2200),
                                                  SequenceValue(b, Variant::Root(18.0), 2200,
                                                                2300)});

        core.endData();
        QVERIFY(core.wait(30000));

        QVERIFY(target.ended);
        QVERIFY(target.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                        SequenceValue(a, Variant::Root(2.0), 200, 300),
                                        SequenceValue(b, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(5.0), 300, 1500),
                                        SequenceValue(a, Variant::Root(6.0), 400, 500),
                                        SequenceValue(b, Variant::Root(7.0), 400, 2100),
                                        SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                        SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                        SequenceValue(b, Variant::Root(11.0), 1200, 2000),
                                        SequenceValue(b, Variant::Root(12.0), 1300, 2100),
                                        SequenceValue(b, Variant::Root(15.0), 1800, 1900),
                                        SequenceValue(a, Variant::Root(16.0), 2000, 2100),
                                        SequenceValue(a, Variant::Root(17.0), 2100, 2200),
                                        SequenceValue(b, Variant::Root(18.0), 2200, 2300)}));
    }

    void split()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");

        std::vector<std::unique_ptr<EditDirective>> directives;
        for (int i = 1000; i < 2000; i += 10) {
            directives.insert(directives.begin(), std::unique_ptr<EditDirective>(
                    new EditDirective(i, i + 10, new TriggerAlways, new ActionRemove(SequenceMatch::Composite(a)))));
        }

        EditCore core(std::move(directives));
        TestTarget target;
        core.start();
        core.setEgress(&target);

        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(2.0), 200, 300),
                                                  SequenceValue(b, Variant::Root(3.0), 200, 300),
                                                  SequenceValue(a, Variant::Root(4.0), 300, 1500),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 1500),
                                                  SequenceValue(a, Variant::Root(6.0), 400, 500),
                                                  SequenceValue(b, Variant::Root(7.0), 400, 2100)});
        QTest::qSleep(50);
        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                                  SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                                  SequenceValue(a, Variant::Root(9.0), 1000, 1500),
                                                  SequenceValue(a, Variant::Root(10.0), 1200, 1300),
                                                  SequenceValue(b, Variant::Root(11.0), 1200, 2000),
                                                  SequenceValue(b, Variant::Root(12.0), 1300, 2100),
                                                  SequenceValue(a, Variant::Root(13.0), 1500, 2100),
                                                  SequenceValue(a, Variant::Root(14.0), 1800, 1900),
                                                  SequenceValue(b, Variant::Root(15.0), 1800, 1900),
                                                  SequenceValue(a, Variant::Root(16.0), 2000,
                                                                2100)});
        ElapsedTimer to;
        to.start();
        do {
            QTest::qSleep(50);
        } while (Range::compareStart(target.getLastStart(), 1800) < 0 && to.elapsed() < 30000);
        QVERIFY(Range::compareStart(target.getLastStart(), 1800) >= 0);
        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(17.0), 2100, 2200),
                                                  SequenceValue(b, Variant::Root(18.0), 2200,
                                                                2300)});
        to.start();
        do {
            QTest::qSleep(50);
        } while (Range::compareStart(target.getLastStart(), 2000) < 0 && to.elapsed() < 30000);
        QVERIFY(Range::compareStart(target.getLastStart(), 2000) >= 0);

        core.endData();
        QVERIFY(core.wait(30000));

        QVERIFY(target.ended);
        QVERIFY(target.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                        SequenceValue(a, Variant::Root(2.0), 200, 300),
                                        SequenceValue(b, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(5.0), 300, 1500),
                                        SequenceValue(a, Variant::Root(6.0), 400, 500),
                                        SequenceValue(b, Variant::Root(7.0), 400, 2100),
                                        SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                        SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                        SequenceValue(b, Variant::Root(11.0), 1200, 2000),
                                        SequenceValue(b, Variant::Root(12.0), 1300, 2100),
                                        SequenceValue(b, Variant::Root(15.0), 1800, 1900),
                                        SequenceValue(a, Variant::Root(16.0), 2000, 2100),
                                        SequenceValue(a, Variant::Root(17.0), 2100, 2200),
                                        SequenceValue(b, Variant::Root(18.0), 2200, 2300)}));
    }

    void skip()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");

        std::vector<std::unique_ptr<EditDirective>> directives;
        for (int i = 0; i < 3; i++) {
            int min = (i * 2 + 1) * 1000;
            int max = (i * 2 + 2) * 1000;
            SequenceName u = (i % 2 == 0 ? a : b);
            for (int j = min; j < max; j += 100) {
                directives.emplace_back(
                        new EditDirective(j, j + 100, new TriggerAlways, new ActionRemove(SequenceMatch::Composite(u))));
            }
        }

        EditCore core(std::move(directives));
        TestTarget target;
        core.start();
        core.setEgress(&target);

        SequenceValue::Transfer expected;
        for (int i = 100; i < 6000; i += 10) {
            SequenceValue v((i / 100) % 2 ? a : b, Variant::Root(i / 100.0), i, i + 10);
            core.incomingData(SequenceValue::Transfer({v}));

            if ((i / 1000) % 2 != 1 || v.getUnit() != ((i / 2000) % 2 == 0 ? a : b))
                expected.emplace_back(v);

            if (i % 1000 == 990) {
                double time = i - 400;
                ElapsedTimer to;
                to.start();
                do {
                    QTest::qSleep(50);
                } while (Range::compareStart(target.getLastStart(), time) < 0 &&
                        to.elapsed() < 30000);
                QVERIFY(Range::compareStart(target.getLastStart(), time) >= 0);
            }
        }

        core.endData();
        QVERIFY(core.wait(30000));

        QVERIFY(target.ended);
        QVERIFY(target.compare(expected));
    }

    void synchronize()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");

        std::vector<std::unique_ptr<EditDirective>> directives;
        directives.emplace_back(new EditDirective(1000, 2000,
                                                  new TriggerScript("return data.a > 1.0",
                                                                    SequenceMatch::OrderedLookup(
                                                                            a)),
                                                  new ActionRemove(SequenceMatch::Composite(a))));
        directives.emplace_back(new EditDirective(1000, 2000, new TriggerAlways,
                                                  new ActionScriptSequenceValueGeneral(
                                                          SequenceMatch::OrderedLookup(SequenceMatch::Element::SpecialMatch::All),
                                                          "for value in data do value.value = value.value + 0.5; end")));

        EditCore core(std::move(directives));
        TestTarget target;
        core.start();
        core.setEgress(&target);

        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(2.0), 200, 300),
                                                  SequenceValue(b, Variant::Root(3.0), 200, 300),
                                                  SequenceValue(a, Variant::Root(4.0), 300, 1500),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 1500),
                                                  SequenceValue(a, Variant::Root(6.0), 400, 500),
                                                  SequenceValue(b, Variant::Root(7.0), 400, 2100)});
        QTest::qSleep(50);
        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                                  SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                                  SequenceValue(a, Variant::Root(9.0), 1000, 1500),
                                                  SequenceValue(a, Variant::Root(10.0), 1200, 1300),
                                                  SequenceValue(b, Variant::Root(11.0), 1200, 2000),
                                                  SequenceValue(b, Variant::Root(12.0), 1300, 2100),
                                                  SequenceValue(a, Variant::Root(13.0), 1500, 2100),
                                                  SequenceValue(a, Variant::Root(14.0), 1800, 1900),
                                                  SequenceValue(b, Variant::Root(15.0), 1800, 1900),
                                                  SequenceValue(a, Variant::Root(16.0), 2000,
                                                                2100)});
        do {
            QTest::qSleep(50);
        } while (Range::compareStart(target.getLastStart(), 1800) < 0);
        QVERIFY(Range::compareStart(target.getLastStart(), 1800) >= 0);
        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(17.0), 2100, 2200),
                                                  SequenceValue(b, Variant::Root(18.0), 2200,
                                                                2300)});

        core.endData();
        QVERIFY(core.wait(30000));

        QVERIFY(target.ended);
        QVERIFY(target.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                        SequenceValue(a, Variant::Root(2.0), 200, 300),
                                        SequenceValue(b, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(5.5), 300, 1500),
                                        SequenceValue(a, Variant::Root(6.0), 400, 500),
                                        SequenceValue(b, Variant::Root(7.5), 400, 2100),
                                        SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                        SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                        SequenceValue(b, Variant::Root(11.5), 1200, 2000),
                                        SequenceValue(b, Variant::Root(12.5), 1300, 2100),
                                        SequenceValue(b, Variant::Root(15.5), 1800, 1900),
                                        SequenceValue(a, Variant::Root(16.0), 2000, 2100),
                                        SequenceValue(a, Variant::Root(17.0), 2100, 2200),
                                        SequenceValue(b, Variant::Root(18.0), 2200, 2300)}));
    }

    void triggerCondition()
    {
        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");

        std::vector<std::unique_ptr<EditDirective>> directives;
        directives.emplace_back(new EditDirective(1000, 2000,
                                                  new TriggerGreaterThan(new TriggerInputValue(SequenceMatch::OrderedLookup(a)),
                                                                         new TriggerInputConstant(
                                                                                 1.0)),
                                                  new ActionRemove(SequenceMatch::Composite(a))));

        EditCore core(std::move(directives));
        TestTarget target;
        core.start();
        core.setEgress(&target);

        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(2.0), 200, 300),
                                                  SequenceValue(b, Variant::Root(3.0), 200, 300),
                                                  SequenceValue(a, Variant::Root(4.0), 300, 1500),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 1500),
                                                  SequenceValue(a, Variant::Root(6.0), 400, 500),
                                                  SequenceValue(b, Variant::Root(7.0), 400, 2100)});
        QTest::qSleep(50);
        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                                  SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                                  SequenceValue(a, Variant::Root(9.0), 1000, 1500),
                                                  SequenceValue(a, Variant::Root(10.0), 1200, 1300),
                                                  SequenceValue(b, Variant::Root(11.0), 1200, 2000),
                                                  SequenceValue(b, Variant::Root(12.0), 1300, 2100),
                                                  SequenceValue(a, Variant::Root(13.0), 1500, 2100),
                                                  SequenceValue(a, Variant::Root(14.0), 1800, 1900),
                                                  SequenceValue(b, Variant::Root(15.0), 1800, 1900),
                                                  SequenceValue(a, Variant::Root(16.0), 2000,
                                                                2100)});
        do {
            QTest::qSleep(50);
        } while (Range::compareStart(target.getLastStart(), 1800) < 0);
        QVERIFY(Range::compareStart(target.getLastStart(), 1800) >= 0);
        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(17.0), 2100, 2200),
                                                  SequenceValue(b, Variant::Root(18.0), 2200,
                                                                2300)});

        core.endData();
        QVERIFY(core.wait(30000));

        QVERIFY(target.ended);
        QVERIFY(target.compare(
                SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                        SequenceValue(a, Variant::Root(2.0), 200, 300),
                                        SequenceValue(b, Variant::Root(3.0), 200, 300),
                                        SequenceValue(b, Variant::Root(5.0), 300, 1500),
                                        SequenceValue(a, Variant::Root(6.0), 400, 500),
                                        SequenceValue(b, Variant::Root(7.0), 400, 2100),
                                        SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                        SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                        SequenceValue(b, Variant::Root(11.0), 1200, 2000),
                                        SequenceValue(b, Variant::Root(12.0), 1300, 2100),
                                        SequenceValue(b, Variant::Root(15.0), 1800, 1900),
                                        SequenceValue(a, Variant::Root(16.0), 2000, 2100),
                                        SequenceValue(a, Variant::Root(17.0), 2100, 2200),
                                        SequenceValue(b, Variant::Root(18.0), 2200, 2300)}));
    }

    void scriptSequenceSegmentError()
    {
        Logging::suppressGlobalForTesting();

        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");

        std::vector<std::unique_ptr<EditDirective>> directives;
        directives.insert(directives.begin(), std::unique_ptr<EditDirective>(
                new EditDirective(1000, 2000, new TriggerAlways,
                                  new ActionScriptSequenceSegmentGeneral(
                                          SequenceMatch::OrderedLookup(a),
                                          SequenceMatch::OrderedLookup(b),
                                          "fot not a in data; endless",
                                          false))));

        EditCore core(std::move(directives));
        TestTarget target;
        core.start();
        core.setEgress(&target);

        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(2.0), 200, 300),
                                                  SequenceValue(b, Variant::Root(3.0), 200, 300),
                                                  SequenceValue(a, Variant::Root(4.0), 300, 1500),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 1500),
                                                  SequenceValue(a, Variant::Root(6.0), 400, 500),
                                                  SequenceValue(b, Variant::Root(7.0), 400, 2100)});
        QTest::qSleep(50);
        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                                  SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                                  SequenceValue(a, Variant::Root(9.0), 1000, 1500),
                                                  SequenceValue(a, Variant::Root(10.0), 1200, 1300),
                                                  SequenceValue(b, Variant::Root(11.0), 1200, 2000),
                                                  SequenceValue(b, Variant::Root(12.0), 1300, 2100),
                                                  SequenceValue(a, Variant::Root(13.0), 1500, 2100),
                                                  SequenceValue(a, Variant::Root(14.0), 1800, 1900),
                                                  SequenceValue(b, Variant::Root(15.0), 1800, 1900),
                                                  SequenceValue(a, Variant::Root(16.0), 2000,
                                                                2100)});

        core.endData();
        QVERIFY(core.wait(30000));
    }

    void scriptValueError()
    {
        Logging::suppressGlobalForTesting();

        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");

        std::vector<std::unique_ptr<EditDirective>> directives;
        directives.insert(directives.begin(), std::unique_ptr<EditDirective>(
                new EditDirective(1000, 2000, new TriggerAlways,
                                  new ActionScriptSequenceValueGeneral(
                                          SequenceMatch::OrderedLookup(a),
                                          "fot not a in data; endless"))));

        EditCore core(std::move(directives));
        TestTarget target;
        core.start();
        core.setEgress(&target);

        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(2.0), 200, 300),
                                                  SequenceValue(b, Variant::Root(3.0), 200, 300),
                                                  SequenceValue(a, Variant::Root(4.0), 300, 1500),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 1500),
                                                  SequenceValue(a, Variant::Root(6.0), 400, 500),
                                                  SequenceValue(b, Variant::Root(7.0), 400, 2100)});
        QTest::qSleep(50);
        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                                  SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                                  SequenceValue(a, Variant::Root(9.0), 1000, 1500),
                                                  SequenceValue(a, Variant::Root(10.0), 1200, 1300),
                                                  SequenceValue(b, Variant::Root(11.0), 1200, 2000),
                                                  SequenceValue(b, Variant::Root(12.0), 1300, 2100),
                                                  SequenceValue(a, Variant::Root(13.0), 1500, 2100),
                                                  SequenceValue(a, Variant::Root(14.0), 1800, 1900),
                                                  SequenceValue(b, Variant::Root(15.0), 1800, 1900),
                                                  SequenceValue(a, Variant::Root(16.0), 2000,
                                                                2100)});

        core.endData();
        QVERIFY(core.wait(30000));
    }

    void scriptFanoutError()
    {
        Logging::suppressGlobalForTesting();

        SequenceName a("bnd", "raw", "a");
        SequenceName b("bnd", "raw", "b");

        std::vector<std::unique_ptr<EditDirective>> directives;
        directives.insert(directives.begin(), std::unique_ptr<EditDirective>(
                new EditDirective(1000, 2000, new TriggerAlways,
                                  new ActionScriptFanoutProcessor("aaa = { ! ~= ! }"))));

        EditCore core(std::move(directives));
        TestTarget target;
        core.start();
        core.setEgress(&target);

        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(1.0), 100, 200),
                                                  SequenceValue(a, Variant::Root(2.0), 200, 300),
                                                  SequenceValue(b, Variant::Root(3.0), 200, 300),
                                                  SequenceValue(a, Variant::Root(4.0), 300, 1500),
                                                  SequenceValue(b, Variant::Root(5.0), 300, 1500),
                                                  SequenceValue(a, Variant::Root(6.0), 400, 500),
                                                  SequenceValue(b, Variant::Root(7.0), 400, 2100)});
        QTest::qSleep(50);
        core.incomingData(SequenceValue::Transfer{SequenceValue(a, Variant::Root(8.0), 800, 1000),
                                                  SequenceValue(b, Variant::Root(9.0), 900, 1000),
                                                  SequenceValue(a, Variant::Root(9.0), 1000, 1500),
                                                  SequenceValue(a, Variant::Root(10.0), 1200, 1300),
                                                  SequenceValue(b, Variant::Root(11.0), 1200, 2000),
                                                  SequenceValue(b, Variant::Root(12.0), 1300, 2100),
                                                  SequenceValue(a, Variant::Root(13.0), 1500, 2100),
                                                  SequenceValue(a, Variant::Root(14.0), 1800, 1900),
                                                  SequenceValue(b, Variant::Root(15.0), 1800, 1900),
                                                  SequenceValue(a, Variant::Root(16.0), 2000,
                                                                2100)});

        core.endData();
        QVERIFY(core.wait(30000));
    }
};

QTEST_MAIN(TestEditCore)

#include "editcore.moc"
