/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/editdatatarget.hxx"

namespace CPD3 {
namespace Editing {

/** @file editing/editdatatarget.hxx
 * The data target for handling dispatch in the editing chain.
 */

EditDataTarget::EditDataTarget() = default;

EditDataTarget::~EditDataTarget() = default;

EditDataModification::EditDataModification() = default;

EditDataModification::~EditDataModification() = default;

EditDispatchTarget::EditDispatchTarget() = default;

EditDispatchTarget::~EditDispatchTarget() = default;

void EditDispatchTarget::incomingData(Data::SequenceValue &&value)
{ incomingData(value); }

void EditDispatchTarget::incomingData(const Data::SequenceValue::Transfer &values)
{
    for (const auto &v : values) {
        incomingData(v);
    }
}

void EditDispatchTarget::incomingData(Data::SequenceValue::Transfer &&values)
{
    for (auto &v : values) {
        incomingData(std::move(v));
    }
}

void EditDispatchTarget::incomingAdvance(double)
{ }


EditDispatchSkipTarget::EditDispatchSkipTarget() = default;

EditDispatchSkipTarget::~EditDispatchSkipTarget() = default;

void EditDispatchSkipTarget::incomingSkipped(Data::SequenceValue &&value, void *origin)
{ incomingSkipped(value, origin); }

void EditDispatchSkipTarget::incomingSkipped(const Data::SequenceValue::Transfer &values,
                                             void *origin)
{
    for (const auto &v : values) {
        incomingSkipped(v, origin);
    }
}

void EditDispatchSkipTarget::incomingSkipped(Data::SequenceValue::Transfer &&values, void *origin)
{
    for (auto &v : values) {
        incomingSkipped(std::move(v), origin);
    }
}

void EditDispatchSkipTarget::incomingSkipAdvance(double, void *)
{ }

void EditDispatchSkipTarget::incomingSkipEnd(void *)
{ }

}
}
