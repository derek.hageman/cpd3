/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "editing/actionflags.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Editing {

/** @file editing/actionflags.hxx
 * Edit directive actions that manipulate flags fields.
 */


ActionAddFlag::ActionAddFlag(const SequenceMatch::Composite &sel,
                             const Data::Variant::Flags &fl,
                             const Variant::Read &desc,
                             quint64 b) : selection(sel), flags(fl), description(desc), bits(b)
{ }

ActionAddFlag::ActionAddFlag(const SequenceMatch::Composite &sel,
                             const Data::Variant::Flag &fl,
                             const Variant::Read &desc,
                             quint64 b) : selection(sel), flags{fl}, description(desc), bits(b)
{ }

ActionAddFlag::~ActionAddFlag() = default;

ActionAddFlag::ActionAddFlag(const ActionAddFlag &other) = default;

Action *ActionAddFlag::clone() const
{ return new ActionAddFlag(*this); }

void ActionAddFlag::unhandledInput(const Data::SequenceName &unit,
                                   QList<EditDataTarget *> &inputs,
                                   QList<EditDataTarget *> &outputs,
                                   QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    if (unit.isMeta()) {
        if (!selection.matches(unit.fromMeta()))
            return;
        modifiers.append(this);
        return;
    }
    if (!selection.matches(unit))
        return;
    outputs.append(this);
}

void ActionAddFlag::incomingDataAdvance(double time)
{ outputAdvance(time); }

void ActionAddFlag::incomingSequenceValue(const Data::SequenceValue &value)
{
    auto result = value.read().toFlags();
    Util::merge(flags, result);
    outputData(SequenceValue(value.getIdentity(), Variant::Root(std::move(result))));
}

void ActionAddFlag::modifySequenceValue(Data::SequenceValue &value)
{
    for (const auto &add : flags) {
        if (description.read().exists()) {
            value.write().metadataSingleFlag(add).hash("Description").set(description);
        }
        if (INTEGER::defined(bits)) {
            value.write().metadataSingleFlag(add).hash("Bits").setInt64(bits);
        }

        bool hit = false;
        for (auto check : value.read().metadataSingleFlag(add).hash("Origin").toArray()) {
            if (check.toString() == "editing") {
                hit = true;
                break;
            }
        }
        if (hit)
            continue;

        value.write()
             .metadataSingleFlag(add)
             .hash("Origin")
             .toArray()
             .after_back()
             .setString("editing");
    }
}


ActionRemoveFlag::ActionRemoveFlag(const SequenceMatch::Composite &sel,
                                   const Data::Variant::Flags &fl) : selection(sel), flags(fl)
{ }

ActionRemoveFlag::ActionRemoveFlag(const SequenceMatch::Composite &sel,
                                   const Data::Variant::Flag &fl) : selection(sel), flags{fl}
{ }

ActionRemoveFlag::~ActionRemoveFlag() = default;

ActionRemoveFlag::ActionRemoveFlag(const ActionRemoveFlag &other) = default;

Action *ActionRemoveFlag::clone() const
{ return new ActionRemoveFlag(*this); }

void ActionRemoveFlag::unhandledInput(const SequenceName &unit,
                                      QList<EditDataTarget *> &inputs,
                                      QList<EditDataTarget *> &outputs,
                                      QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(inputs);
    Q_UNUSED(modifiers);
    if (!selection.matches(unit))
        return;
    outputs.append(this);
}

void ActionRemoveFlag::incomingDataAdvance(double time)
{ outputAdvance(time); }

void ActionRemoveFlag::incomingSequenceValue(const SequenceValue &value)
{
    auto result = value.read().toFlags();
    for (const auto &rem : flags) {
        result.erase(rem);
    }
    outputData(SequenceValue(value.getIdentity(), Variant::Root(std::move(result))));
}


ActionRemoveMatchingFlags::ActionRemoveMatchingFlags(const SequenceMatch::Composite &sel,
                                                     const QList<QRegExp> &m,
                                                     bool c) : selection(sel),
                                                               matchers(m),
                                                               checkAll(c),
                                                               metadataUpdate(this),
                                                               metadataMatchedFlags()
{ }

ActionRemoveMatchingFlags::~ActionRemoveMatchingFlags() = default;

ActionRemoveMatchingFlags::ActionRemoveMatchingFlags(const ActionRemoveMatchingFlags &other)
        : Action(other),
          selection(other.selection),
          matchers(other.matchers),
          checkAll(other.checkAll),
          metadataUpdate(this),
          metadataMatchedFlags()
{ }

Action *ActionRemoveMatchingFlags::clone() const
{ return new ActionRemoveMatchingFlags(*this); }

void ActionRemoveMatchingFlags::unhandledInput(const SequenceName &unit,
                                               QList<EditDataTarget *> &inputs,
                                               QList<EditDataTarget *> &outputs,
                                               QList<EditDataModification *> &modifiers)
{
    Q_UNUSED(modifiers);
    if (checkAll || !unit.isMeta()) {
        if (!selection.matches(unit))
            return;
        outputs.append(this);
        return;
    }

    Q_ASSERT(unit.isMeta());
    if (!selection.matches(unit.fromMeta()))
        return;
    inputs.append(&metadataUpdate);
}

void ActionRemoveMatchingFlags::incomingDataAdvance(double time)
{ outputAdvance(time); }

void ActionRemoveMatchingFlags::incomingSequenceValue(const SequenceValue &value)
{
    auto result = value.read().toFlags();
    if (!checkAll) {
        for (const auto &rem : metadataMatchedFlags) {
            result.erase(rem);
        }
    } else {
        for (auto f = result.begin(); f != result.end();) {
            bool hit = false;
            for (const auto &check : matchers) {
                if (!check.exactMatch(QString::fromStdString(*f)))
                    continue;
                hit = true;
                break;
            }

            if (!hit) {
                ++f;
                continue;
            }

            f = result.erase(f);
        }
    }

    outputData(SequenceValue(value.getIdentity(), Variant::Root(std::move(result))));
}

void ActionRemoveMatchingFlags::incomingMetadata(const SequenceValue &value)
{
    for (auto flag : value.read().toMetadataSingleFlag()) {
        bool hit = false;
        for (const auto &check : matchers) {
            if (!check.exactMatch(QString::fromStdString(flag.first)))
                continue;
            hit = true;
            break;
        }

        if (!hit)
            continue;

        metadataMatchedFlags.insert(flag.first);
    }
}

ActionRemoveMatchingFlags::MetadataUpdate::MetadataUpdate(ActionRemoveMatchingFlags *p) : parent(p)
{ }

ActionRemoveMatchingFlags::MetadataUpdate::~MetadataUpdate() = default;

void ActionRemoveMatchingFlags::MetadataUpdate::incomingSequenceValue(const SequenceValue &value)
{ parent->incomingMetadata(value); }

}
}
