/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACORE_DYNAMICINPUT_HXX
#define CPD3DATACORE_DYNAMICINPUT_HXX

#include "core/first.hxx"

#include <memory>
#include <deque>
#include <QtGlobal>
#include <QDataStream>
#include <QList>
#include <QSet>
#include <QDebug>

#include "datacore.hxx"
#include "core/util.hxx"
#include "segment.hxx"
#include "stream.hxx"
#include "sequencematch.hxx"
#include "valueoptionparse.hxx"
#include "core/component.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Data {

class DynamicInput;

class DynamicInputOption;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const DynamicInput *tiv);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, DynamicInput *&tiv);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const DynamicInput *tiv);

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                            const std::unique_ptr<DynamicInput> &tiv);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                            std::unique_ptr<DynamicInput> &tiv);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const std::unique_ptr<DynamicInput> &tiv);

/**
 * A single possibly time dependent input floating point value.
 */
class CPD3DATACORE_EXPORT DynamicInput {
public:
    virtual ~DynamicInput();

    /**
     * Get the value at the given time.  This will advance the time, so no
     * requests before this time may be made after the call.
     * 
     * @param time  the time
     * @return      the value at the given time
     */
    virtual double get(double time) = 0;

    /**
     * Get the value at the given time.
     * 
     * @param time  the time
     * @return      the value at the given time
     */
    virtual double getConst(double time) const = 0;

    /**
     * Get the value for the given data segment (possibly using values in it).
     * This will advance the time, so no requests before the start time may be 
     * made after the call.
     * 
     * @param in    the data segments
     * @return      the resulting value
     */
    virtual double get(SequenceSegment &in) = 0;

    /** @see get(DataMultiSegment &) */
    virtual double get(const SequenceSegment &in) = 0;

    /**
     * Get the value for the given data segment (possibly using values in it).
     * 
     * @param in    the data segments
     * @return      the resulting value
     */
    virtual double getConst(SequenceSegment &in) const = 0;

    /** @see getConst(DataMultiSegment &) const */
    virtual double getConst(const SequenceSegment &in) const = 0;

    /**
     * Like get(DataMultiSegment &) except it does not convert the value
     * to a double if applicable.
     * 
     * @param in    the data segments
     * @return      the resulting value
     */
    virtual Variant::Read getValue(SequenceSegment &in);

    /** @see getValue(DataMultiSegment &) */
    virtual Variant::Read getValue(const SequenceSegment &in);

    /**
     * Like getConst(DataMultiSegment &) except it does not convert the value
     * to a double if applicable.
     * 
     * @param in    the data segments
     * @return      the resulting value
     */
    virtual Variant::Read getValueConst(SequenceSegment &in) const;

    /** @see getValueConst(DataMultiSegment &) const */
    virtual Variant::Read getValueConst(const SequenceSegment &in) const;

    /**
     * Register an input for this value series.  Returns true if it could be
     * used as an input.
     * 
     * @param input     the input unit to test
     * @return          true if the input is used
     */
    virtual bool registerInput(const SequenceName &input);

    /**
     * Register a set of expected matched parameters.  This allows the selection
     * to determine the used inputs before they actually appear in the data
     * stream.  If any component is empty is is treated as unknown and required
     * to be exact before a component can be used.  That is specifying a
     * station and archive will allow all inputs that specify exactly a single
     * variable to be known.  The flavors are considered unknown.
     * 
     * @param station   the station to expect or empty for unknown
     * @param archive   the archive to expect or empty for unknown
     * @param variable  the variable to expect or empty for unknown
     */
    virtual void registerExpectedFlavorless(const SequenceName::Component &station = {},
                                            const SequenceName::Component &archive = {},
                                            const SequenceName::Component &variable = {});

    /**
     * The same as registerExpectedFlavorless( const QString &, const QString &, 
     * const QString & ) except that the flavors are known.
     * 
     * @param station       the station to expect or empty for unknown
     * @param archive       the archive to expect or empty for unknown
     * @param variable      the variable to expect or empty for unknown
     * @param flavors       the flavors to except
     * @see registerExpectedFlavorless( const QString &, const QString &, const QString & )
     */
    virtual void registerExpected(const SequenceName::Component &station,
                                  const SequenceName::Component &archive,
                                  const SequenceName::Component &variable,
                                  const SequenceName::Flavors &flavors);

    /**
     * The same as registerExpectedFlavorless( const QString &, const QString &, 
     * const QString &, const QSet<QString> & ) except that the flavors are 
     * known only as a set of possibilities.
     * 
     * @param station       the station to expect or empty for unknown
     * @param archive       the archive to expect or empty for unknown
     * @param variable      the variable to expect or empty for unknown
     * @param flavors       the flavors to except
     * @see registerExpectedFlavorless( const QString &, const QString &, const QString & )
     */
    virtual void registerExpected(const SequenceName::Component &station = {},
                                  const SequenceName::Component &archive = {},
                                  const SequenceName::Component &variable = {},
                                  const std::unordered_set<
                                          SequenceName::Flavors> &flavors = SequenceName::defaultDataFlavors());

    /**
     * Get all known input values.
     * 
     * @return  a list of known inputs that this input uses
     */
    virtual SequenceName::Set getUsedInputs() const;

    /**
     * Get all known times when the "structure" of this input might change.
     * Generally this should return times when the outside description of it
     * would need to be updated.
     * 
     * @return  a list of known changed times
     */
    virtual QSet<double> getChangedPoints() const;

    /**
     * Duplicate this object, including polymorphism.  Allocates a new object
     * that must be deleted by the caller.
     * 
     * @return      the copy
     */
    virtual DynamicInput *clone() const = 0;

    /**
     * Returns a string representation of this input at the given times.
     * 
     * @param start the start time to describe
     * @param end   the end time to describe
     * @param whenUndefined the value returned when the input would be always undefined
     * @return      a string
     */
    virtual QString describe(double start,
                             double end,
                             const QString &whenUndefined = QString::fromLatin1("Undefined")) const;

    /**
     * Returns a double value representing the input at the given times.  If
     * the input is variable then this will return undefined.
     * 
     * @param start the start time to describe
     * @param end   the end time to describe   
     * @return      the constant value of the input
     */
    virtual double constant(double start, double end) const;

    template<typename T>
    inline QString describe(const T &in,
                            const QString &whenUndefined = QString::fromLatin1("Undefined")) const
    { return describe(in.getStart(), in.getEnd(), whenUndefined); }

    template<typename T>
    inline double constant(const T &in) const
    { return constant(in.getStart(), in.getEnd()); }

    /** @see getConst( DataMultiSegment & ) */
    inline double get(SequenceSegment &in) const
    { return getConst(in); }

    /** @see getConst( const DataMultiSegment & ) */
    inline double get(const SequenceSegment &in) const
    { return getConst(in); }

    /** @see getConst( double ) */
    inline double get(double time) const
    { return getConst(time); }

    /** @see getValueConst( DataMultiSegment & ) */
    inline Variant::Read getValue(SequenceSegment &in) const
    { return getValueConst(in); }

    /** @see getValueConst( DataMultiSegment & ) */
    inline Variant::Read getValue(const SequenceSegment &in) const
    { return getValueConst(in); }

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicInput *tiv);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, DynamicInput *&tiv);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const DynamicInput *tiv);


    /**
     * Create a time dependent input from a list of configuration values.  This
     * looks up the given unit each segment and defines the input based on that
     * lookup and the path within that Value.  Finally if the start and end are
     * given then the result is trimmed to only segments within that range.  The
     * start and end are optional and only used for optimization.
     * <br>
     * This allocates a TimeInputValue with new that the caller must delete.
     *
     * @param config    the configuration segments
     * @param unit      the unit to lookup within the segments
     * @param path      the path within the resulting value to use
     * @param start     the optional start time to optimize for
     * @param end       the optional end time to optimize for
     * @param defaultStation    the default station or empty for all
     * @param defaultArchive    the default archive or empty for all
     * @param defaultVariable   the default variable or empty for all
     * @return          a newly allocated TimeInputValue
     */
    static DynamicInput *fromConfiguration(SequenceSegment::Transfer &config,
                                           const SequenceName &unit,
                                           const QString &path = QString(),
                                           double start = FP::undefined(),
                                           double end = FP::undefined(),
                                           const SequenceName::Component &defaultStation = {},
                                           const SequenceName::Component &defaultArchive = {},
                                           const SequenceName::Component &defaultVariable = {});

    /**
     * Create a time dependent input from a list of configuration values.  This
     * looks up the given path within each segment and defines the input based on
     * that.  Finally if the start and end are given then the result is trimmed to
     * only segments within that range.  The start and end are optional and only
     * used for optimization.
     * <br>
     * This allocates a TimeInputValue with new that the caller must delete.
     *
     * @param config    the configuration segments
     * @param path      the path within the resulting value to use
     * @param start     the optional start time to optimize for
     * @param end       the optional end time to optimize for
     * @param defaultStation    the default station or empty for all
     * @param defaultArchive    the default archive or empty for all
     * @param defaultVariable   the default variable or empty for all
     * @return          a newly allocated TimeInputValue
     */
    static DynamicInput *fromConfiguration(const ValueSegment::Transfer &config,
                                           const QString &path = QString(),
                                           double start = FP::undefined(),
                                           double end = FP::undefined(),
                                           const SequenceName::Component &defaultStation = {},
                                           const SequenceName::Component &defaultArchive = {},
                                           const SequenceName::Component &defaultVariable = {});

    class Constant;

    class Calibration;

    class Basic;

    class Variable;

protected:
    virtual void serialize(QDataStream &stream) const = 0;

    virtual void printLog(QDebug &stream) const;
};

/**
 * A thin wrapper in for a constant value in the time dependent interface.
 */
class CPD3DATACORE_EXPORT DynamicInput::Constant : public DynamicInput {
    double value;
public:
    /**
     * Create an input that is always undefined.
     */
    Constant();

    /**
     * Create an input that always reports the given value.
     */
    explicit Constant(double v);

    virtual ~Constant();

    double get(double time) override;

    double getConst(double time) const override;

    double get(SequenceSegment &in) override;

    double get(const SequenceSegment &in) override;

    double getConst(SequenceSegment &in) const override;

    double getConst(const SequenceSegment &in) const override;

    QString describe(double start,
                     double end,
                     const QString &whenUndefined = QString::fromLatin1(
                             "Undefined")) const override;

    double constant(double start, double end) const override;

    DynamicInput *clone() const override;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicInput *tiv);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, DynamicInput *&tiv);

protected:
    explicit Constant(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    void printLog(QDebug &stream) const override;
};

/**
 * A wrapper around another input that applies a calibration to it.
 */
class CPD3DATACORE_EXPORT DynamicInput::Calibration : public DynamicInput {
    std::unique_ptr<DynamicInput> origin;
    CPD3::Calibration cal;
public:
    Calibration() = delete;

    /**
     * Create a calibrating wrapper.  This takes ownership of the wrapped input.
     *
     * @param source    the input value to wrap
     */
    Calibration(DynamicInput *source, const CPD3::Calibration &calibration);

    /**
     * Create a calibrating wrapper.  This takes ownership of the wrapped input.
     *
     * @param source    the input value to wrap
     */
    Calibration(std::unique_ptr<DynamicInput> &&source, const CPD3::Calibration &calibration);

    virtual ~Calibration();

    double get(double time) override;

    double getConst(double time) const override;

    double get(SequenceSegment &in) override;

    double get(const SequenceSegment &in) override;

    double getConst(SequenceSegment &in) const override;

    double getConst(const SequenceSegment &in) const override;

    Variant::Read getValue(SequenceSegment &in) override;

    Variant::Read getValue(const SequenceSegment &in) override;

    Variant::Read getValueConst(SequenceSegment &in) const override;

    Variant::Read getValueConst(const SequenceSegment &in) const override;

    QString describe(double start,
                     double end,
                     const QString &whenUndefined = QString::fromLatin1(
                             "Undefined")) const override;

    bool registerInput(const SequenceName &input) override;

    void registerExpectedFlavorless(const SequenceName::Component &station = {},
                                    const SequenceName::Component &archive = {},
                                    const SequenceName::Component &variable = {}) override;

    void registerExpected(const SequenceName::Component &station,
                          const SequenceName::Component &archive,
                          const SequenceName::Component &variable,
                          const SequenceName::Flavors &flavors) override;

    void registerExpected(const SequenceName::Component &station = {},
                          const SequenceName::Component &archive = {},
                          const SequenceName::Component &variable = {},
                          const std::unordered_set<
                                  SequenceName::Flavors> &flavors = SequenceName::defaultDataFlavors()) override;

    SequenceName::Set getUsedInputs() const override;

    QSet<double> getChangedPoints() const override;

    double constant(double start, double end) const override;

    DynamicInput *clone() const override;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicInput *tiv);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, DynamicInput *&tiv);

protected:
    explicit Calibration(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    void printLog(QDebug &stream) const override;
};


/**
 * A thin wrapper for a single variable lookup in the time dependent interface.
 */
class CPD3DATACORE_EXPORT DynamicInput::Basic : public DynamicInput {
    SequenceName unit;
public:
    Basic() = delete;

    /**
     * Create an input that always lookup the given value.
     *
     * @param u     the sequence name
     */
    explicit Basic(const SequenceName &u);

    virtual ~Basic();

    double get(double time) override;

    double getConst(double time) const override;

    double get(SequenceSegment &in) override;

    double get(const SequenceSegment &in) override;

    double getConst(SequenceSegment &in) const override;

    double getConst(const SequenceSegment &in) const override;

    Variant::Read getValue(SequenceSegment &in) override;

    Variant::Read getValue(const SequenceSegment &in) override;

    Variant::Read getValueConst(SequenceSegment &in) const override;

    Variant::Read getValueConst(const SequenceSegment &in) const override;

    bool registerInput(const SequenceName &input) override;

    SequenceName::Set getUsedInputs() const override;

    QString describe(double start,
                     double end,
                     const QString &whenUndefined = QString::fromLatin1(
                             "Undefined")) const override;

    DynamicInput *clone() const override;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicInput *tiv);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, DynamicInput *&tiv);

protected:
    explicit Basic(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    void printLog(QDebug &stream) const override;
};

class DynamicInputOption;

class CPD3DATACORE_EXPORT DynamicInput::Variable : public DynamicInput {
    struct ConstantOverlay {
        double start;
        double end;
        double constant;
        bool first;

        inline ConstantOverlay(double set, bool isFirst, double setStart, double setEnd) : start(
                setStart), end(setEnd), constant(set), first(isFirst)
        { }

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }
    };

    struct VariableOverlay {
        double start;
        double end;
        SequenceMatch::OrderedLookup selection;
        std::string path;

        inline VariableOverlay(SequenceMatch::OrderedLookup set, std::string setPath,
                               double setStart,
                               double setEnd) : start(setStart),
                                                end(setEnd),
                                                selection(std::move(set)),
                                                path(std::move(setPath))
        { }

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }
    };

    struct CalibrationOverlay {
        double start;
        double end;
        CPD3::Calibration calibration;

        inline CalibrationOverlay(CPD3::Calibration set, double setStart, double setEnd) : start(
                setStart), end(setEnd), calibration(std::move(set))
        { }

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }
    };

    struct InputSegment {
        double start;
        double end;
        double constant;
        bool constantFirst;
        SequenceMatch::OrderedLookup variableSelect;
        std::string path;
        CPD3::Calibration calibration;

        InputSegment();

        InputSegment(const InputSegment &other);

        InputSegment &operator=(const InputSegment &other);

        InputSegment(InputSegment &&other);

        InputSegment &operator=(InputSegment &&other);

        InputSegment(const InputSegment &other, double setStart, double setEnd);

        InputSegment(const InputSegment &under,
                     const InputSegment &over,
                     double setStart,
                     double setEnd);

        InputSegment(const InputSegment &under,
                     const ConstantOverlay &over,
                     double setStart,
                     double setEnd);

        InputSegment(const InputSegment &under,
                     const VariableOverlay &over,
                     double setStart,
                     double setEnd);

        InputSegment(const InputSegment &under,
                     const CalibrationOverlay &over,
                     double setStart,
                     double setEnd);

        InputSegment(const ConstantOverlay &other, double setStart, double setEnd);

        InputSegment(const VariableOverlay &other, double setStart, double setEnd);

        InputSegment(const CalibrationOverlay &other, double setStart, double setEnd);

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }
    };

    std::deque<InputSegment> segments;

public:
    Variable();

    virtual ~Variable();

    Variable(const DynamicInput::Variable &other);

    DynamicInput::Variable &operator=(const DynamicInput::Variable &other);

    Variable(DynamicInput::Variable &&other);

    DynamicInput::Variable &operator=(DynamicInput::Variable &&other);

    double get(double time) override;

    double getConst(double time) const override;

    double get(SequenceSegment &in) override;

    double get(const SequenceSegment &in) override;

    double getConst(SequenceSegment &in) const override;

    double getConst(const SequenceSegment &in) const override;

    Variant::Read getValue(SequenceSegment &in) override;

    Variant::Read getValue(const SequenceSegment &in) override;

    Variant::Read getValueConst(SequenceSegment &in) const override;

    Variant::Read getValueConst(const SequenceSegment &in) const override;

    bool registerInput(const SequenceName &input) override;

    void registerExpectedFlavorless(const SequenceName::Component &station,
                                    const SequenceName::Component &archive,
                                    const SequenceName::Component &variable) override;

    void registerExpected(const SequenceName::Component &station,
                          const SequenceName::Component &archive,
                          const SequenceName::Component &variable,
                          const SequenceName::Flavors &flavors) override;

    void registerExpected(const SequenceName::Component &station,
                          const SequenceName::Component &archive,
                          const SequenceName::Component &variable,
                          const std::unordered_set<SequenceName::Flavors> &flavors) override;

    SequenceName::Set getUsedInputs() const override;

    QSet<double> getChangedPoints() const override;

    QString describe(double start,
                     double end,
                     const QString &whenUndefined = QString::fromLatin1(
                             "Undefined")) const override;

    double constant(double start, double end) const override;

    DynamicInput *clone() const override;

    /**
     * Clear all input segments.
     */
    void clear();

    /**
     * Set the oinput to always be the given constant value.
     *
     * @param constant  the value to set
     */
    void set(double constant);

    /**
     * Set the input to always lookup the given variable selection,
     * apply the given calibration to it, but return the failover value if there
     * are no valid values.
     *
     * @param variableSelect    the variable selection to use
     * @param calibration       the calibration to apply to the variable input
     * @param failoverConstant  the value to report if there is no valid variable
     * @param path              the path to look up
     */
    void set(const SequenceMatch::OrderedLookup &variableSelect,
             const CPD3::Calibration &calibration,
             double failoverConstant = FP::undefined(), const std::string &path = std::string());

    /**
     * Set the input to use the given constant between the given values.  If it is
     * set as a failover constant then the variable input is tried first, otherwise
     * it will always report this constant.
     *
     * @param start         the start time to apply the constant for
     * @param end           the end time to apply the constant until
     * @param constant      the value to report
     * @param isFailover    if true then try this constant only after the variables
     */
    void set(double start, double end, double constant, bool isFailover = false);

    /**
     * Set the input to use the given variable selection and calibration between
     * the given times.  If there are no valid values then the failover value is
     * returned.
     *
     * @param start             the start time to apply this input type for
     * @param end               the end time to apply this input type until
     * @param variableSelect    the variable selection to use
     * @param calibration       the calibration to apply to variables
     * @param failoverConstant  the value to use if no variable is valid
     * @param path              the path to look up
     */
    void set(double start,
             double end,
             const SequenceMatch::OrderedLookup &variableSelect,
             const CPD3::Calibration &calibration,
             double failoverConstant = FP::undefined(), const std::string &path = std::string());

    /**
     * Set the calibration for variables between the given times.
     *
     * @param start         the start time to apply the calibration
     * @param end           the end time to apply the calibration until
     * @param calibration   the calibration to apply
     */
    void set(double start, double end, const CPD3::Calibration &calibration);

    /**
     * Set the input to the configuration defined by the given value between
     * the given times.  This replaces entirely any configuration within those
     * times.
     *
     * @param start     the start of the replace
     * @param end       the end of the replace
     * @param v         the value to replace with
     * @param defaultStation    the default station or empty for all
     * @param defaultArchive    the default archive or empty for all
     * @param defaultVariable   the default variable or empty for all
     */
    void set(double start,
             double end, const Variant::Read &v,
             const SequenceName::Component &defaultStation = {},
             const SequenceName::Component &defaultArchive = {},
             const SequenceName::Component &defaultVariable = {});

    /**
     * Set the input to always use the configuration given by the value
     *
     * @param v     the configuration value
     * @param defaultStation    the default station or empty for all
     * @param defaultArchive    the default archive or empty for all
     * @param defaultVariable   the default variable or empty for all
     */
    void set(const Variant::Read &v,
             const SequenceName::Component &defaultStation = {},
             const SequenceName::Component &defaultArchive = {},
             const SequenceName::Component &defaultVariable = {});

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicInput *tiv);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, DynamicInput *&tiv);

    friend class DynamicInputOption;

    friend class DynamicInput;

protected:
    explicit Variable(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    void printLog(QDebug &stream) const override;

private:
    static DynamicInput *reduceConfigurationSegments(const DynamicInput::Variable &icv,
                                                     double start,
                                                     double end);
};


/** 
 * A single floating point input only value.  Generally this can be
 * either a constant value or a variable.
 */
class CPD3DATACORE_EXPORT DynamicInputOption
        : public ComponentOptionBase, virtual public ComponentOptionValueParsable {
Q_OBJECT

    Q_INTERFACES(CPD3::Data::ComponentOptionValueParsable)

    DynamicInput::Variable input;
public:
    DynamicInputOption(const QString &argumentName,
                       const QString &displayName,
                       const QString &description,
                       const QString &defaultBehavior,
                       int sortPriority = 0);

    DynamicInputOption(const DynamicInputOption &other);

    virtual ~DynamicInputOption();

    void reset() override;

    ComponentOptionBase *clone() const override;

    /**
     * Allocate a new input to read from.  The input allocated this way must
     * be deleted by the caller.  Note that inputs created this way do not reflect
     * changes to this input options wrapper after they are created.
     *
     * @return a new input
     */
    DynamicInput *getInput() const;

    /**
     * Clear all input segments from this options wrapper.
     */
    void clear();

    /**
     * Set the options wrapper to always be the given constant value.
     *
     * @param constant  the value to set
     */
    void set(double constant);

    /**
     * Set the options wrapper to always lookup the given variable selection,
     * apply the given calibration to it, but return the failover value if there
     * are no valid values.
     *
     * @param variableSelect    the variable selection to use
     * @param calibration       the calibration to apply to the variable input
     * @param failoverConstant  the value to report if there is no valid variable
     * @param path              the path within values to lookup
     */
    void set(const SequenceMatch::OrderedLookup &variableSelect,
             const CPD3::Calibration &calibration,
             double failoverConstant = FP::undefined(), const std::string &path = std::string());

    /**
     * Set the input to use the given constant between the given values.  If it is
     * set as a failover constant then the variable input is tried first, otherwise
     * it will always report this constant.
     *
     * @param start         the start time to apply the constant for
     * @param end           the end time to apply the constant until
     * @param constant      the value to report
     * @param isFailover    if true then try this constant only after the variables
     */
    void set(double start, double end, double constant, bool isFailover = false);

    /**
     * Set the input to use the given variable selection and calibration between
     * the given times.  If there are no valid values then the failover value is
     * returned.
     *
     * @param start             the start time to apply this input type for
     * @param end               the end time to apply this input type until
     * @param variableSelect    the variable selection to use
     * @param calibration       the calibration to apply to variables
     * @param failoverConstant  the value to use if no variable is valid
     * @param path              the path within values to lookup
     */
    void set(double start,
             double end,
             const SequenceMatch::OrderedLookup &variableSelect,
             const CPD3::Calibration &calibration,
             double failoverConstant = FP::undefined(), const std::string &path = std::string());

    /**
     * Set the calibration for variables between the given times.
     *
     * @param start         the start time to apply the calibration
     * @param end           the end time to apply the calibration until
     * @param calibration   the calibration to apply
     */
    void set(double start, double end, const CPD3::Calibration &calibration);

    /**
     * Set the input to the parameters specified by the given value for all time.
     *
     * @param value         the Value to apply
     */
    void set(const Variant::Read &value);

    /**
     * Set the input to the parameters specified by the given value for the given
     * time range.
     *
     * @param start         the start time to apply the Value
     * @param end           the end time to apply the Value until
     * @param value         the Value to apply
     */
    void set(double start, double end, const Variant::Read &value);

    bool parseFromValue(const Variant::Read &value) override;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicInput *tiv);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, DynamicInput *&tiv);
};

}
}

#endif
