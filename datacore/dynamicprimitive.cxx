/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "dynamicprimitive.hxx"


namespace CPD3 {
namespace Data {

/** @file datacore/filterprimitive.hxx
 * Provides basic options for filters requiring primitive types as inputs.
 */


DynamicStringOption::DynamicStringOption(const QString &argumentName,
                                         const QString &displayName,
                                         const QString &description,
                                         const QString &defaultBehavior,
                                         int sortPriority) : ComponentOptionBase(argumentName,
                                                                                 displayName,
                                                                                 description,
                                                                                 defaultBehavior,
                                                                                 sortPriority),
                                                             operate()
{ }

DynamicStringOption::DynamicStringOption(const DynamicStringOption &other) : ComponentOptionBase(
        other), operate(other.operate)
{ }

DynamicStringOption::~DynamicStringOption() = default;

/**
 * Set the option to the given value for the given time range.
 * 
 * @param value the value to set
 * @param start the start time the value is effective at
 * @param end   the end time the value is effective until
 */
void DynamicStringOption::set(const QString &value, double start, double end)
{
    optionSet = true;
    operate.set(value, start, end);
}

void DynamicStringOption::clear()
{ operate.clear(); }

void DynamicStringOption::reset()
{
    optionSet = false;
    operate.clear();
}

ComponentOptionBase *DynamicStringOption::clone() const
{
    return new DynamicStringOption(*this);
}

DynamicString *DynamicStringOption::getInput() const
{
    auto result = new DynamicPrimitive<QString>::Variable(operate);
    result->simplify();
    DynamicString *check = result->compressedType();
    if (check != NULL) {
        delete result;
        return check;
    }
    return result;
}

bool DynamicStringOption::parseFromValue(const Variant::Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Array: {
        for (auto v : value.toArray()) {
            set(v.hash("Value").toQString(), v.hash("Start").toDouble(), v.hash("End").toDouble());
        }
        break;
    }
    case Variant::Type::Hash:
        set(value["Value"].toQString(), value["Start"].toDouble(), value["End"].toDouble());
        break;
    default:
        set(value.toQString());
        break;
    }
    return true;
}


DynamicBoolOption::DynamicBoolOption(const QString &argumentName,
                                     const QString &displayName,
                                     const QString &description,
                                     const QString &defaultBehavior,
                                     int sortPriority) : ComponentOptionBase(argumentName,
                                                                             displayName,
                                                                             description,
                                                                             defaultBehavior,
                                                                             sortPriority),
                                                         operate()
{
    operate.set(false);
}

DynamicBoolOption::DynamicBoolOption(const DynamicBoolOption &other) : ComponentOptionBase(other),
                                                                       operate(other.operate)
{ }

DynamicBoolOption::~DynamicBoolOption() = default;

/**
 * Set the option to the given value for the given time range.
 * 
 * @param value the value to set
 * @param start the start time the value is effective at
 * @param end   the end time the value is effective until
 */
void DynamicBoolOption::set(bool value, double start, double end)
{
    optionSet = true;
    operate.set(value, start, end);
}

void DynamicBoolOption::clear()
{
    operate.clear();
    operate.set(false);
}

void DynamicBoolOption::reset()
{
    optionSet = false;
    operate.clear();
}

ComponentOptionBase *DynamicBoolOption::clone() const
{ return new DynamicBoolOption(*this); }

DynamicBool *DynamicBoolOption::getInput() const
{
    auto result = new DynamicPrimitive<bool>::Variable(operate);
    result->simplify();
    DynamicBool *check = result->compressedType();
    if (check != NULL) {
        delete result;
        return check;
    }
    return result;
}

bool DynamicBoolOption::parseFromValue(const Variant::Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Array: {
        for (auto v : value.toArray()) {
            set(v.hash("Value").toBool(), v.hash("Start").toDouble(), v.hash("End").toDouble());
        }
        break;
    }
    case Variant::Type::Hash:
        set(value["Value"].toBool(), value["Start"].toDouble(), value["End"].toDouble());
        break;
    default:
        set(value.toBool());
        break;
    }
    return true;
}


DynamicDoubleOption::DynamicDoubleOption(const QString &argumentName,
                                         const QString &displayName,
                                         const QString &description,
                                         const QString &defaultBehavior,
                                         int sortPriority) : ComponentOptionBase(argumentName,
                                                                                 displayName,
                                                                                 description,
                                                                                 defaultBehavior,
                                                                                 sortPriority),
                                                             operate(),
                                                             max(FP::undefined()),
                                                             min(FP::undefined()),
                                                             maxInclusive(true),
                                                             minInclusive(true),
                                                             allowUndefined(false)
{
    operate.set(FP::undefined());
}

DynamicDoubleOption::DynamicDoubleOption(const DynamicDoubleOption &other) : ComponentOptionBase(
        other),
                                                                             operate(other.operate),
                                                                             max(other.max),
                                                                             min(other.min),
                                                                             maxInclusive(
                                                                                     other.maxInclusive),
                                                                             minInclusive(
                                                                                     other.minInclusive),
                                                                             allowUndefined(
                                                                                     other.allowUndefined)
{ }

DynamicDoubleOption::~DynamicDoubleOption() = default;

void DynamicDoubleOption::setMaximum(double m, bool inclusive)
{
    max = m;
    maxInclusive = inclusive;
}

void DynamicDoubleOption::setMinimum(double m, bool inclusive)
{
    min = m;
    minInclusive = inclusive;
}

double DynamicDoubleOption::getMaximum() const
{ return max; }

double DynamicDoubleOption::getMinimum() const
{ return min; }

bool DynamicDoubleOption::getMaximumInclusive() const
{ return maxInclusive; }

bool DynamicDoubleOption::getMinimumInclusive() const
{ return minInclusive; }

void DynamicDoubleOption::setAllowUndefined(bool a)
{ allowUndefined = a; }

bool DynamicDoubleOption::getAllowUndefined() const
{ return allowUndefined; }

bool DynamicDoubleOption::isValid(double check) const
{
    if (!FP::defined(check))
        return allowUndefined;

    if (FP::defined(min)) {
        if (minInclusive) {
            if (check < min)
                return false;
        } else {
            if (check <= min)
                return false;
        }
    }

    if (FP::defined(max)) {
        if (maxInclusive) {
            if (check > max)
                return false;
        } else {
            if (check >= max)
                return false;
        }
    }

    return true;
}

/**
 * Set the option to the given value for the given time range.
 * 
 * @param value the value to set
 * @param start the start time the value is effective at
 * @param end   the end time the value is effective until
 */
void DynamicDoubleOption::set(double value, double start, double end)
{
    optionSet = true;
    operate.set(value, start, end);
}

void DynamicDoubleOption::clear()
{
    operate.clear();
    operate.set(FP::undefined());
}

void DynamicDoubleOption::reset()
{
    optionSet = false;
    operate.clear();
}

ComponentOptionBase *DynamicDoubleOption::clone() const
{
    return new DynamicDoubleOption(*this);
}

DynamicDouble *DynamicDoubleOption::getInput() const
{
    auto result = new DynamicPrimitive<double>::Variable(operate);
    result->simplify();
    DynamicDouble *check = result->compressedType();
    if (check != NULL) {
        delete result;
        return check;
    }
    return result;
}

bool DynamicDoubleOption::parseFromValue(const Variant::Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Array: {
        for (auto v : value.toArray()) {
            set(Variant::Composite::toNumber(v.hash("Value")), v.hash("Start").toDouble(),
                v.hash("End").toDouble());
        }
        break;
    }
    case Variant::Type::Hash:
        set(Variant::Composite::toNumber(value["Value"]), value["Start"].toDouble(),
            value["End"].toDouble());
        break;
    default:
        set(value.toDouble());
        break;
    }
    return true;
}


DynamicIntegerOption::DynamicIntegerOption(const QString &argumentName,
                                           const QString &displayName,
                                           const QString &description,
                                           const QString &defaultBehavior,
                                           int sortPriority) : ComponentOptionBase(argumentName,
                                                                                   displayName,
                                                                                   description,
                                                                                   defaultBehavior,
                                                                                   sortPriority),
                                                               operate(),
                                                               max(INTEGER::undefined()),
                                                               min(INTEGER::undefined()),
                                                               allowUndefined(false)
{
    operate.set(false);
}

DynamicIntegerOption::DynamicIntegerOption(const DynamicIntegerOption &other) : ComponentOptionBase(
        other),
                                                                                operate(other.operate),
                                                                                max(other.max),
                                                                                min(other.min),
                                                                                allowUndefined(
                                                                                        other.allowUndefined)
{ }

DynamicIntegerOption::~DynamicIntegerOption() = default;

void DynamicIntegerOption::setMaximum(qint64 m)
{ max = m; }

void DynamicIntegerOption::setMinimum(qint64 m)
{ min = m; }

qint64 DynamicIntegerOption::getMaximum() const
{ return max; }

qint64 DynamicIntegerOption::getMinimum() const
{ return min; }

void DynamicIntegerOption::setAllowUndefined(bool a)
{ allowUndefined = a; }

bool DynamicIntegerOption::getAllowUndefined() const
{ return allowUndefined; }

bool DynamicIntegerOption::isValid(qint64 check) const
{
    if (!INTEGER::defined(check))
        return allowUndefined;

    if (INTEGER::defined(min)) {
        if (check < min)
            return false;
    }

    if (INTEGER::defined(max)) {
        if (check > max)
            return false;
    }

    return true;
}

/**
 * Set the option to the given value for the given time range.
 * 
 * @param value the value to set
 * @param start the start time the value is effective at
 * @param end   the end time the value is effective until
 */
void DynamicIntegerOption::set(qint64 value, double start, double end)
{
    optionSet = true;
    operate.set(value, start, end);
}

void DynamicIntegerOption::clear()
{
    operate.clear();
    operate.set(false);
}


void DynamicIntegerOption::reset()
{
    optionSet = false;
    operate.clear();
}

ComponentOptionBase *DynamicIntegerOption::clone() const
{
    return new DynamicIntegerOption(*this);
}

DynamicInteger *DynamicIntegerOption::getInput() const
{
    auto result = new DynamicPrimitive<qint64>::Variable(operate);
    result->simplify();
    DynamicInteger *check = result->compressedType();
    if (check != NULL) {
        delete result;
        return check;
    }
    return result;
}

static qint64 variantToInteger(const Variant::Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Integer:
        return value.toInteger();
    case Variant::Type::Real: {
        double f = value.toReal();
        if (!FP::defined(f))
            return INTEGER::undefined();
        return static_cast<qint64>(std::round(f));
    }
    default:
        break;
    }
    return INTEGER::undefined();
}

bool DynamicIntegerOption::parseFromValue(const Variant::Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Array: {
        for (auto v : value.toArray()) {
            set(variantToInteger(v.hash("Value")), v.hash("Start").toDouble(),
                v.hash("End").toDouble());
        }
        break;
    }
    case Variant::Type::Hash:
        set(variantToInteger(value["Value"]), value["Start"].toDouble(), value["End"].toDouble());
        break;
    default:
        set(value.toInt64());
        break;
    }
    return true;
}


DynamicCalibrationOption::DynamicCalibrationOption(const QString &argumentName,
                                                   const QString &displayName,
                                                   const QString &description,
                                                   const QString &defaultBehavior,
                                                   int sortPriority) : ComponentOptionBase(
        argumentName, displayName, description, defaultBehavior, sortPriority), operate()
{ }

DynamicCalibrationOption::DynamicCalibrationOption(const DynamicCalibrationOption &other)
        : ComponentOptionBase(other), operate(other.operate)
{ }

DynamicCalibrationOption::~DynamicCalibrationOption() = default;

bool DynamicCalibrationOption::isValid(const Calibration &cal) const
{
    Q_UNUSED(cal);
    return true;
}

void DynamicCalibrationOption::reset()
{
    optionSet = false;
    operate.clear();
}

/**
 * Set the option to the given value for the given time range.
 * 
 * @param value the value to set
 * @param start the start time the value is effective at
 * @param end   the end time the value is effective until
 */
void DynamicCalibrationOption::set(const Calibration &value, double start, double end)
{
    optionSet = true;
    operate.set(value, start, end);
}

void DynamicCalibrationOption::clear()
{ operate.clear(); }

ComponentOptionBase *DynamicCalibrationOption::clone() const
{
    return new DynamicCalibrationOption(*this);
}

DynamicCalibration *DynamicCalibrationOption::getInput() const
{
    auto result = new DynamicPrimitive<Calibration>::Variable(operate);
    result->simplify();
    DynamicCalibration *check = result->compressedType();
    if (check != NULL) {
        delete result;
        return check;
    }
    return result;
}

bool DynamicCalibrationOption::parseFromValue(const Variant::Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Array: {
        for (auto v : value.toArray()) {
            set(Variant::Composite::toCalibration(v.hash("Value")), v.hash("Start").toDouble(),
                v.hash("End").toDouble());
        }
        break;
    }
    case Variant::Type::Hash:
        set(Variant::Composite::toCalibration(value["Value"]), value["Start"].toDouble(),
            value["End"].toDouble());
        break;
    default:
        set(Variant::Composite::toCalibration(value));
        break;
    }
    return true;
}


}
}
