/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACORE_SEQUENCEAUTHORIZATION_HXX
#define CPD3DATACORE_SEQUENCEAUTHORIZATION_HXX

#include "core/first.hxx"

#include <memory>
#include <vector>
#include <cstdint>
#include <unordered_map>

#include "datacore.hxx"
#include "stream.hxx"
#include "sequencematch.hxx"
#include "segment.hxx"
#include "archive/selection.hxx"
#include "core/stream.hxx"

namespace CPD3 {
namespace Data {

class CPD3DATACORE_EXPORT SequenceFilter {
protected:
    class CPD3DATACORE_EXPORT MatcherData {
    public:
        virtual ~MatcherData() = default;

        virtual void passed(const SequenceValue &value) = 0;

        virtual void passed(const ArchiveValue &value) = 0;

        virtual void passed(const ArchiveErasure &value) = 0;

        virtual double overrideModified(double modifiedAfter,
                                        const SequenceMatch::Element &matcher);

        virtual MatcherData *clone() = 0;
    };

    virtual std::unique_ptr<MatcherData> createMatchData(const Variant::Read &config);

private:

    bool defaultAccept;

    struct Segment : Time::Bounds {
        enum Outcome {
            Accept, Reject, UniversalAccept, UniversalReject, System
        };

        Outcome defaultOutcome;

        struct Stage {
            std::int_fast64_t priority;
            SequenceMatch::Element matcher;
            bool accept;
            bool universal;
            std::unique_ptr<MatcherData> data;

            Stage();

            Stage(const Stage &other);

            Stage &operator=(const Stage &other);

            Stage(Stage &&);

            Stage &operator=(Stage &&);

            Stage(const Variant::Read &config,
                  std::unique_ptr<MatcherData> &&data,
                  const CPD3::Data::SequenceMatch::Element::PatternList &defaultStation = {},
                  const CPD3::Data::SequenceMatch::Element::PatternList &defaultArchive = {},
                  const CPD3::Data::SequenceMatch::Element::PatternList &defaultVariable = {});
        };

        std::vector<Stage> stages;

        struct EvaluationResult {
            Outcome outcome;
            std::vector<MatcherData *> data;

            EvaluationResult();

            EvaluationResult(const EvaluationResult &);

            EvaluationResult &operator=(const EvaluationResult &);

            EvaluationResult(EvaluationResult &&);

            EvaluationResult &operator=(EvaluationResult &&);
        };

        SequenceName::Map <EvaluationResult> result;

        Segment();

        Segment(const Segment &other);

        Segment &operator=(const Segment &);

        Segment(Segment &&);

        Segment &operator=(Segment &&);

        const EvaluationResult &evaluate(const SequenceName &name);
    };

    ConfigurationStream<Segment> segments;

    Segment configureSegment(double start,
                             double end, const Variant::Read &config,
                             const SequenceMatch::Element::PatternList &defaultStation,
                             const SequenceMatch::Element::PatternList &defaultArchive,
                             const SequenceMatch::Element::PatternList &defaultVariable);

    struct AcceptResult {
        bool anyHit;
        bool anyAccepted;
        bool anyRejected;
        bool anySystem;
        enum {
            None, Accept, Reject
        } universal;
        double firstStart;
        double lastEnd;
        std::vector<MatcherData *> data;

        AcceptResult();

        void integrate(const SequenceName &name, Segment &segment);

        bool accepted(double start, double end, bool defaultAccept) const;
    };

public:
    SequenceFilter();

    virtual ~SequenceFilter();

    SequenceFilter(const SequenceFilter &);

    SequenceFilter &operator=(const SequenceFilter &);

    SequenceFilter(SequenceFilter &&);

    SequenceFilter &operator=(SequenceFilter &&);

    /**
     * Create a filter from a configuration.
     *
     * @param config    the configuration
     * @param defaultStation    the default station pattern
     * @param defaultArchive    the default archive pattern
     * @param defaultVariable   the default variable pattern
     */
    void configure(const ValueSegment::Transfer &config,
                   const CPD3::Data::SequenceMatch::Element::PatternList &defaultStation = {},
                   const CPD3::Data::SequenceMatch::Element::PatternList &defaultArchive = {},
                   const CPD3::Data::SequenceMatch::Element::PatternList &defaultVariable = {});


    /**
     * Create a filter from a configuration.
     *
     * @param config    the configuration
     * @param defaultStation    the default station pattern
     * @param defaultArchive    the default archive pattern
     * @param defaultVariable   the default variable pattern
     */
    void configure(const Variant::Read &config,
                   const CPD3::Data::SequenceMatch::Element::PatternList &defaultStation = {},
                   const CPD3::Data::SequenceMatch::Element::PatternList &defaultArchive = {},
                   const CPD3::Data::SequenceMatch::Element::PatternList &defaultVariable = {});

    /**
     * Set if the filter accepts values by default.
     *
     * @param accept    enable default accept
     */
    void setDefaultAccept(bool accept);

    /**
     * Convert the filter to the list of archive selections it could
     * possibly accept.  This does NOT include "everything" if the default
     * is to accept values, only those explicitly accepted.
     *
     * @param defaultStations   the list of default stations
     * @param defaultArchives   the list of default archives
     * @param defaultVariable   the list of default variables
     * @param modifiedAfter     the default modified after time
     * @return  the list of archive selections accepted by the filter
     */
    Archive::Selection::List toArchiveSelection(const Archive::Selection::Match &defaultStations = {},
                                                const Archive::Selection::Match &defaultArchives = {},
                                                const Archive::Selection::Match &defaultVariables = {},
                                                double modifiedAfter = FP::undefined()) const;

    /**
     * Advance the filter to the given time.
     *
     * @param time  the time to advance to
     */
    void advance(double time);

    /**
     * Test if the given name would be accepted.
     *
     * @param name      the sequence name to test
     * @param start     the start time to check
     * @param end       the end time to check
     * @return          true if the sequence name is accepted
     */
    bool accept(const SequenceName &name,
                double start = FP::undefined(),
                double end = FP::undefined());

    /**
     * Test if a value is accepted by the authorization.
     *
     * @param value     the value
     * @return          true if it is accepted
     */
    bool accept(const SequenceValue &value);

    /**
     * Test if a value is accepted by the authorization.
     *
     * @param value     the value
     * @return          true if it is accepted
     */
    bool accept(const ArchiveValue &value);

    /**
     * Test if a value is accepted by the authorization.
     *
     * @param value     the value
     * @return          true if it is accepted
     */
    bool accept(const ArchiveErasure &value);

    /**
     * Test if a value is accepted by the authorization and advance to the value's start time.
     *
     * @param value     the value
     * @return          true if it is accepted
     */
    inline bool acceptAdvancing(const SequenceValue &value)
    {
        advance(value.getStart());
        return accept(value);
    }

    /**
     * Test if a value is accepted by the authorization and advance to the value's start time.
     *
     * @param value     the value
     * @return          true if it is accepted
     */
    inline bool acceptAdvancing(const ArchiveValue &value)
    {
        advance(value.getStart());
        return accept(value);
    }

    /**
     * Test if a value is accepted by the authorization and advance to the value's start time.
     *
     * @param value     the value
     * @return          true if it is accepted
     */
    inline bool acceptAdvancing(const ArchiveErasure &value)
    {
        advance(value.getStart());
        return accept(value);
    }
};

}
}

#endif //CPD3DATACORE_SEQUENCEAUTHORIZATION_HXX
