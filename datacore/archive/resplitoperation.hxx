/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREARCHIVE_RESPLIT_HXX
#define CPD3DATACOREARCHIVE_RESPLIT_HXX

#include "core/first.hxx"

#include <memory>

#include "datacore/datacore.hxx"

#include "datacore/datacore.hxx"
#include "core/threading.hxx"
#include "datacore/archive/structure.hxx"

namespace CPD3 {
namespace Data {
namespace Archive {

/**
 * An archive resplit operation.  This converts the archive from one
 * splitting method to another.
 */
class CPD3DATACORE_EXPORT ResplitOperation {
    Database::Connection *connection;
    Structure::Layout &source;
    Structure::Layout &target;

    enum class State {
        Initialize, ConvertSequence, ConvertErasure, ReleaseOldTables, Terminating, Complete, Failed
    } state;

    std::mutex mutex;
    std::condition_variable notify;
    std::thread thread;

    void run();

    std::unordered_set<std::string> releaseSequence;
    std::unordered_set<std::string> releaseErasure;

    bool isTerminated();

    bool convertSequence();

    bool convertErasure();

    bool releaseOld();

    bool executeConvert(const Structure::Identity &sourceIdentity,
                        const std::string &sourceTable,
                        const Structure::Identity &targetIdentity,
                        const std::string &targetTable);

public:
    ResplitOperation(Database::Connection *connection,
                     Structure::Layout &source,
                     Structure::Layout &target);

    ~ResplitOperation();

    /**
     * Abort the operation.
     */
    void signalTerminate();

    /**
     * Start the operation.
     */
    void start();

    /**
     * Wait for the operation to complete.
     *
     * @param timeout   the maximum time to wait
     * @return          true if the operation has completed
     */
    bool wait(double timeout = FP::undefined());

    /**
     * Test if the global operation has completed.
     *
     * @return          true if the operation has completed
     */
    bool isComplete();

    /**
     * Test if the operation completed successfully.
     *
     * @return          true if the operation completed and was sucessful
     */
    bool isOk();

    /**
     * A signal generated from the thread once it has completed work.
     */
    Threading::Signal<> complete;
};

}
}
}

#endif //CPD3DATACOREARCHIVE_RESPLIT_HXX
