/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include <algorithm>

#include <QByteArray>
#include <QBuffer>
#include <QDataStream>
#include <QDebug>

#include "datacore/archive/stringindex.hxx"
#include "datacore/archive/structure.hxx"
#include "datacore/archive/initialization.hxx"
#include "database/connection.hxx"
#include "database/tabledefinition.hxx"
#include "datacore/stream.hxx"
#include "core/compression.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::Archive;

Database::Connection::SynchronousResult initialize(Database::Connection *connection,
                                                   int versionCode = 0x08)
{
    static constexpr int systemIdentifier = 1;

    if (connection->hasTable("CPD3DBVersion"))
        return Database::Connection::Synchronous_Abort;

    Database::TableDefinition table("CPD3DBVersion");
    table.column("system", Database::TableDefinition::Integer, false);
    table.column("version", Database::TableDefinition::Integer, false);
    table.primaryKey("system");
    connection->createTable(table);

    connection->insert("CPD3DBVersion", {"system", "version"})
              .synchronous({{"system",  systemIdentifier},
                            {"version", versionCode}});

    return Database::Connection::Synchronous_Ok;
}

QByteArray constructSequence(StringIndex &flavors, const ArchiveValue::Transfer &values)
{
    ArchiveValue::Transfer sorted(values);

    std::stable_sort(sorted.begin(), sorted.end(),
                     [](const ArchiveValue &a, const ArchiveValue &b) {
                         if (!FP::defined(a.getModified()))
                             return FP::defined(b.getModified());
                         if (!FP::defined(b.getModified()))
                             return false;
                         return a.getModified() < b.getModified();
                     });

    QList <quint32> offsets;

    QByteArray data;
    QBuffer bfr(&data);
    bfr.open(QIODevice::WriteOnly);
    bfr.seek(sizeof(quint32) + sizeof(quint32) * sorted.size());
    QDataStream stream(&bfr);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setVersion(QDataStream::Qt_4_5);

    for (const auto &v : sorted) {
        offsets.append((quint32) bfr.pos());

        quint8 flags = 0;
        if (v.isRemoteReferenced())
            flags |= Structure::Existing::Flag_RemoteReferenced;

        stream << v.getStart();
        stream << v.getEnd();
        stream << static_cast<quint16>(flavors.insert(v.getName().getFlavorsString()));
        stream << v.getModified();
        stream << static_cast<qint32>(v.getPriority());
        stream << flags;
        stream << v.getValue();
    }

    bfr.seek(0);
    stream << offsets;

    data = BlockCompressor().compress(data).toQByteArray();
    if (data.isEmpty()) {
        qDebug() << "Failed to compress data";
        return QByteArray();
    }
    return data;
}

QByteArray constructErasure(StringIndex &flavors, const ArchiveErasure::Transfer &values)
{
    ArchiveErasure::Transfer sorted(values);

    std::stable_sort(sorted.begin(), sorted.end(),
                     [](const ArchiveErasure &a, const ArchiveErasure &b) {
                         if (!FP::defined(a.getModified()))
                             return FP::defined(b.getModified());
                         if (!FP::defined(b.getModified()))
                             return false;
                         return a.getModified() < b.getModified();
                     });

    QList <quint32> offsets;

    QByteArray data;
    QBuffer bfr(&data);
    bfr.open(QIODevice::WriteOnly);
    QDataStream stream(&bfr);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setVersion(QDataStream::Qt_4_5);

    for (const auto &v : sorted) {
        stream << v.getStart();
        stream << v.getEnd();
        stream << static_cast<quint16>(flavors.insert(v.getName().getFlavorsString()));
        stream << v.getModified();
        stream << static_cast<qint32>(v.getPriority());
    }

    data = BlockCompressor().compress(data).toQByteArray();
    if (data.isEmpty()) {
        qDebug() << "Failed to compress data";
        return QByteArray();
    }
    return data;
}

Structure::Identity insertIdentity(Structure::Layout &layout,
                                   IdentityIndex &index,
                                   const SequenceName &name)
{
    Structure::Identity tableID;
    if (layout.splitStation()) {
        Q_ASSERT(!name.getStation().empty());
        tableID.station = index.station.insert(name.getStation());
    }
    if (layout.splitArchive()) {
        Q_ASSERT(!name.getArchive().empty());
        tableID.archive = index.archive.insert(name.getArchive());
    }
    if (layout.splitVariable()) {
        Q_ASSERT(!name.getVariable().empty());
        tableID.variable = index.variable.insert(name.getVariable());
    }
    return tableID;
}

Structure::Identity sequenceTable(Database::Connection *db,
                                  Structure::Layout &layout,
                                  IdentityIndex &index,
                                  const SequenceName &name)
{
    Structure::Identity tableID = insertIdentity(layout, index, name);
    std::string tableName = layout.sequenceTable(tableID);
    if (!db->hasTable(tableName))
        Initialization::createSequenceTable(db, layout, tableID);
    return tableID;
}

Structure::Identity erasureTable(Database::Connection *db,
                                 Structure::Layout &layout,
                                 IdentityIndex &index,
                                 const SequenceName &name)
{
    Structure::Identity tableID = insertIdentity(layout, index, name);
    std::string tableName = layout.erasureTable(tableID);
    if (!db->hasTable(tableName))
        Initialization::createErasureTable(db, layout, tableID);
    return tableID;
}

void sequenceInsert(Database::Connection *db,
                    Structure::Layout &layout,
                    IdentityIndex &index,
                    Structure::Block block,
                    const ArchiveValue::Transfer &values)
{
    Q_ASSERT(!values.empty());

    auto id = sequenceTable(db, layout, index, values.front().getName());
    if (!layout.splitStation()) {
        id.station = index.station.insert(values.front().getName().getStation());
    }
    if (!layout.splitArchive()) {
        id.archive = index.archive.insert(values.front().getName().getArchive());
    }
    if (!layout.splitVariable()) {
        id.variable = index.variable.insert(values.front().getName().getVariable());
    }
    auto op = layout.sequenceWrite(id);

    auto binds = op.binds();
    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    binds.emplace(Structure::Column_Data, constructSequence(index.flavor, values));
    double modified = values.front().getModified();
    for (const auto &v : values) {
        if (v.getModified() > modified)
            modified = v.getModified();
    }
    binds.emplace(Structure::Column_Modified, layout.targetModified(modified));

    op.statement().synchronous(binds);
}

void erasureInsert(Database::Connection *db,
                   Structure::Layout &layout,
                   IdentityIndex &index,
                   Structure::Block block,
                   const ArchiveErasure::Transfer &values)
{
    Q_ASSERT(!values.empty());

    auto id = erasureTable(db, layout, index, values.front().getName());
    if (!layout.splitStation()) {
        id.station = index.station.insert(values.front().getName().getStation());
    }
    if (!layout.splitArchive()) {
        id.archive = index.archive.insert(values.front().getName().getArchive());
    }
    if (!layout.splitVariable()) {
        id.variable = index.variable.insert(values.front().getName().getVariable());
    }
    auto op = layout.erasureWrite(id);

    auto binds = op.binds();
    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    binds.emplace(Structure::Column_Data, constructErasure(index.flavor, values));
    double modified = values.front().getModified();
    for (const auto &v : values) {
        if (v.getModified() > modified)
            modified = v.getModified();
    }
    binds.emplace(Structure::Column_Modified, layout.targetModified(modified));

    op.statement().synchronous(binds);
}

ArchiveValue::Transfer readSequence(StringIndex &flavors,
                                    const SequenceName &baseName,
                                    const Util::ByteView &input)
{
    if (input.empty())
        return ArchiveValue::Transfer();
    QByteArray data = BlockDecompressor().decompress(input).toQByteArray();
    if (data.isEmpty()) {
        qDebug() << "Decompression failed";
        return ArchiveValue::Transfer();
    }

    QBuffer bfr(&data);
    bfr.open(QIODevice::ReadOnly);
    QDataStream stream(&bfr);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setVersion(QDataStream::Qt_4_5);

    QList <quint32> offsets;
    stream >> offsets;

    if (offsets.isEmpty()) {
        qDebug() << "Empty offsets";
        return ArchiveValue::Transfer();
    }

    ArchiveValue::Transfer result;
    for (auto i : offsets) {
        bfr.seek(i);

        double start;
        stream >> start;
        double end;
        stream >> end;
        quint16 flavorsIndex;
        stream >> flavorsIndex;
        auto flavorsString = flavors.lookup(flavorsIndex);
        double modified;
        stream >> modified;
        qint32 priority;
        stream >> priority;
        quint8 flags;
        stream >> flags;
        Variant::Root val;
        stream >> val;

        SequenceName name(baseName);
        name.setFlavorsString(flavorsString);

        result.push_back(ArchiveValue(name, val, start, end, (int) priority, modified,
                                      (flags & Structure::Existing::Flag_RemoteReferenced)));
    }

    return result;
}

ArchiveErasure::Transfer readErasure(StringIndex &flavors,
                                     const SequenceName &baseName,
                                     const Util::ByteView &input)
{
    if (input.empty())
        return ArchiveErasure::Transfer();
    QByteArray data = BlockDecompressor().decompress(input).toQByteArray();
    if (data.isEmpty()) {
        qDebug() << "Decompression failed";
        return ArchiveErasure::Transfer();
    }
    if (data.size() % Structure::Erasure::Total_Size != 0) {
        qDebug() << "Invalid erasure size";
        return ArchiveErasure::Transfer();
    }

    QBuffer bfr(&data);
    bfr.open(QIODevice::ReadOnly);
    QDataStream stream(&bfr);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setVersion(QDataStream::Qt_4_5);

    ArchiveErasure::Transfer result;
    for (int i = 0, endI = data.size(); i < endI; i += Structure::Erasure::Total_Size) {
        bfr.seek(i);

        double start;
        stream >> start;
        double end;
        stream >> end;
        quint16 flavorsIndex;
        stream >> flavorsIndex;
        auto flavorsString = flavors.lookup(flavorsIndex);
        double modified;
        stream >> modified;
        qint32 priority;
        stream >> priority;

        SequenceName name(baseName);
        name.setFlavorsString(flavorsString);

        result.push_back(ArchiveErasure(name, start, end, (int) priority, modified));
    }

    return result;
}


Database::Types::Variant loadSequence(Structure::Layout &layout,
                      IdentityIndex &index,
                      Structure::Block block,
                      const SequenceName &name)
{
    std::string whereBlock = Structure::Column_Block;
    whereBlock += " = :";
    whereBlock += Structure::Column_Block;

    auto op = layout.sequenceRead(Structure::Identity(index.station.lookup(name.getStation()),
                                                      index.archive.lookup(name.getArchive()),
                                                      index.variable.lookup(name.getVariable())),
                                  whereBlock);
    auto binds = op.binds();
    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    return op.statement().single(binds, false);
}

Database::Types::Variant loadErasure(Structure::Layout &layout,
                     IdentityIndex &index,
                     Structure::Block block,
                     const SequenceName &name)
{
    std::string whereBlock = Structure::Column_Block;
    whereBlock += " = :";
    whereBlock += Structure::Column_Block;

    auto op = layout.erasureRead(Structure::Identity(index.station.lookup(name.getStation()),
                                                     index.archive.lookup(name.getArchive()),
                                                     index.variable.lookup(name.getVariable())),
                                 whereBlock);
    auto binds = op.binds();
    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    return op.statement().single(binds, false);
}

bool compareSequence(SequenceValue::Transfer result,
                     SequenceValue::Transfer expected,
                     bool sorted = false)
{
    if (sorted && !result.empty()) {
        double time = result.front().getStart();
        for (const auto &check : result) {
            if (Range::compareStart(check.getStart(), time) < 0) {
                qDebug() << "Time not ascending";
                return false;
            }
            time = check.getStart();
        }
    }
    for (auto rv = result.begin(); rv != result.end();) {
        auto check = std::find_if(expected.begin(), expected.end(), [=](const SequenceValue &e) {
            if (!FP::equal(e.getStart(), rv->getStart()))
                return false;
            if (!FP::equal(e.getEnd(), rv->getEnd()))
                return false;
            if (e.getName() != rv->getName())
                return false;
            if (e.getPriority() != rv->getPriority())
                return false;
            return e.getValue() == rv->getValue();
        });
        if (check == expected.end()) {
            ++rv;
            continue;
        }

        rv = result.erase(rv);
        expected.erase(check);
    }

    if (!result.empty()) {
        qDebug() << "Unmatched result values:" << result;
        return false;
    }
    if (!expected.empty()) {
        qDebug() << "Unmatched expected values:" << expected;
        return false;
    }
    return true;
}

bool compareSequence(ArchiveValue::Transfer result,
                     SequenceValue::Transfer expected,
                     bool sorted = false)
{
    if (sorted && !result.empty()) {
        double time = result.front().getStart();
        for (const auto &check : result) {
            if (Range::compareStart(check.getStart(), time) < 0) {
                qDebug() << "Time not ascending";
                return false;
            }
            time = check.getStart();
        }
    }
    for (auto rv = result.begin(); rv != result.end();) {
        auto check = std::find_if(expected.begin(), expected.end(), [=](const SequenceValue &e) {
            if (!FP::equal(e.getStart(), rv->getStart()))
                return false;
            if (!FP::equal(e.getEnd(), rv->getEnd()))
                return false;
            if (e.getName() != rv->getName())
                return false;
            if (e.getPriority() != rv->getPriority())
                return false;
            return e.getValue() == rv->getValue();
        });
        if (check == expected.end()) {
            ++rv;
            continue;
        }

        rv = result.erase(rv);
        expected.erase(check);
    }

    if (!result.empty()) {
        qDebug() << "Unmatched result values:" << result;
        return false;
    }
    if (!expected.empty()) {
        qDebug() << "Unmatched expected values:" << expected;
        return false;
    }
    return true;
}

bool compareArchive(ArchiveValue::Transfer result,
                    ArchiveValue::Transfer expected,
                    bool sorted = false)
{
    if (sorted && !result.empty()) {
        double time = result.front().getStart();
        for (const auto &check : result) {
            if (Range::compareStart(check.getStart(), time) < 0) {
                qDebug() << "Time not ascending";
                return false;
            }
            time = check.getStart();
        }
    }
    for (auto rv = result.begin(); rv != result.end();) {
        auto check = std::find_if(expected.begin(), expected.end(), [=](const ArchiveValue &e) {
            if (!FP::equal(e.getStart(), rv->getStart()))
                return false;
            if (!FP::equal(e.getEnd(), rv->getEnd()))
                return false;
            if (e.getName() != rv->getName())
                return false;
            if (e.getPriority() != rv->getPriority())
                return false;
            if (e.isRemoteReferenced() != rv->isRemoteReferenced())
                return false;
            if (!FP::equal(e.getModified(), rv->getModified()))
                return false;
            return e.getValue() == rv->getValue();
        });
        if (check == expected.end()) {
            ++rv;
            continue;
        }

        rv = result.erase(rv);
        expected.erase(check);
    }

    if (!result.empty()) {
        qDebug() << "Unmatched result values:" << result;
        return false;
    }
    if (!expected.empty()) {
        qDebug() << "Unmatched expected values:" << expected;
        return false;
    }
    return true;
}

bool compareErasure(ArchiveErasure::Transfer result,
                    ArchiveErasure::Transfer expected,
                    bool sorted = false)
{
    if (sorted && !result.empty()) {
        double time = result.front().getStart();
        for (const auto &check : result) {
            if (Range::compareStart(check.getStart(), time) < 0) {
                qDebug() << "Time not ascending";
                return false;
            }
            time = check.getStart();
        }
    }
    for (auto rv = result.begin(); rv != result.end();) {
        auto check = std::find_if(expected.begin(), expected.end(), [=](const ArchiveErasure &e) {
            if (!FP::equal(e.getStart(), rv->getStart()))
                return false;
            if (!FP::equal(e.getEnd(), rv->getEnd()))
                return false;
            if (e.getName() != rv->getName())
                return false;
            if (e.getPriority() != rv->getPriority())
                return false;
            if (!FP::equal(e.getModified(), rv->getModified()))
                return false;
            return true;
        });
        if (check == expected.end()) {
            ++rv;
            continue;
        }

        rv = result.erase(rv);
        expected.erase(check);
    }

    if (!result.empty()) {
        qDebug() << "Unmatched result values:" << result;
        return false;
    }
    if (!expected.empty()) {
        qDebug() << "Unmatched expected values:" << expected;
        return false;
    }
    return true;
}

bool checkSequence(Structure::Layout &layout,
                   IdentityIndex &index,
                   Structure::Block block,
                   const SequenceValue::Transfer &expected)
{
    Q_ASSERT(!expected.empty());

    SequenceName baseName = expected.front().getName();
    baseName.clearFlavors();
    auto data = loadSequence(layout, index, block, baseName);
    if (!data.isSet()) {
        qDebug() << "Invalid database result";
        return false;
    }
    return compareSequence(readSequence(index.flavor, baseName, data.toBytes()), expected);
}

bool checkArchive(Structure::Layout &layout,
                  IdentityIndex &index,
                  Structure::Block block,
                  const ArchiveValue::Transfer &expected)
{
    Q_ASSERT(!expected.empty());

    SequenceName baseName = expected.front().getName();
    baseName.clearFlavors();
    auto data = loadSequence(layout, index, block, baseName);
    if (!data.isSet()) {
        qDebug() << "Invalid database result";
        return false;
    }
    return compareArchive(readSequence(index.flavor, baseName, data.toBytes()), expected);
}

bool checkErasure(Structure::Layout &layout,
                  IdentityIndex &index,
                  Structure::Block block,
                  const ArchiveErasure::Transfer &expected)
{
    Q_ASSERT(!expected.empty());

    SequenceName baseName = expected.front().getName();
    baseName.clearFlavors();
    auto data = loadErasure(layout, index, block, baseName);
    if (!data.isSet()) {
        qDebug() << "Invalid database result";
        return false;
    }
    return compareErasure(readErasure(index.flavor, baseName, data.toBytes()), expected);
}

double sequenceModified(Structure::Layout &layout,
                        IdentityIndex &index,
                        Structure::Block block,
                        const SequenceName &name)
{
    std::string whereBlock = Structure::Column_Block;
    whereBlock += " = :";
    whereBlock += Structure::Column_Block;

    auto op = layout.sequenceIndex(Structure::Identity(index.station.lookup(name.getStation()),
                                                       index.archive.lookup(name.getArchive()),
                                                       index.variable.lookup(name.getVariable())),
                                   whereBlock);
    auto binds = op.binds();
    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    auto result = op.statement().extract(binds);
    if (result.size() < 2)
        return FP::undefined();
    return layout.modifiedValue(result[1]);
}

double erasureModified(Structure::Layout &layout,
                       IdentityIndex &index,
                       Structure::Block block,
                       const SequenceName &name)
{
    std::string whereBlock = Structure::Column_Block;
    whereBlock += " = :";
    whereBlock += Structure::Column_Block;

    auto op = layout.erasureIndex(Structure::Identity(index.station.lookup(name.getStation()),
                                                      index.archive.lookup(name.getArchive()),
                                                      index.variable.lookup(name.getVariable())),
                                  whereBlock);
    auto binds = op.binds();
    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    auto result = op.statement().extract(binds);
    if (result.size() < 2)
        return FP::undefined();
    return layout.modifiedValue(result[1]);
}