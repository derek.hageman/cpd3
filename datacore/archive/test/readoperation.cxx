/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QTest>
#include <QTemporaryFile>

#include "datacore/archive/readoperation.hxx"

#include "common.cxx"

class BlockingSequenceSink final : public StreamSink {
public:
    std::uint_fast64_t counter;
    bool ended;

    BlockingSequenceSink() : counter(0), ended(false)
    { }

    virtual ~BlockingSequenceSink() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    {
        counter += values.size();
        QTest::qSleep(1000);
    }

    virtual void endData()
    { ended = true; }
};

class TestReadOperation : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    Database::Storage archiveStorage() const
    {
        return Database::Storage::sqlite(databaseFile.fileName().toUtf8().constData());
    }

private slots:

    void initTestCase()
    {
        QVERIFY(databaseFile.open());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void basic()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0,
                                     600.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0,
                                     2000.0,
                                     0, 601.0, false),});

        StreamSink::Buffer result;
        {
            ReadSequence read(layout, index, {Selection()});
            read.start();
            read.setEgress(&result);
            read.wait();
        }

        QVERIFY(result.ended());
        QVERIFY(compareSequence(result.values(), SequenceValue::Transfer{
                                        SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                                      1000.0, 0),
                                        SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                                      1001.0, 0),
                                        SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0, 0),
                                        SequenceValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0,
                                                      2000.0, 0),},
                                true));
    }

    void empty()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceTable(&db, layout, index, {"bnd", "raw", "BsG_S11"});

        StreamSink::Buffer result;
        {
            ReadSequence read(layout, index, {Selection()});
            read.start();
            read.setEgress(&result);
            read.wait();
        }

        QVERIFY(result.ended());
        QVERIFY(result.values().empty());
    }

    void terminate()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        for (int i = 0; i < 10; i++) {
            sequenceInsert(&db, layout, index, i,
                           {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                         Structure::Block_Interval * i + 1000.0,
                                         Structure::Block_Interval * i + 2000.0, 0, 600.0,
                                         false),});
        }
        BlockingSequenceSink result;
        {
            ReadSequence read(layout, index, {Selection()});
            read.setEgress(&result);
            read.start();
            QTest::qSleep(50);
            read.signalTerminate();
            read.wait();
        }

        QVERIFY(result.ended);
        QVERIFY(result.counter < 10);

        {
            ReadSequence read(layout, index, {Selection()});
            read.start();
            QTest::qSleep(50);
            read.signalTerminate();
            read.wait();
        }
    }

    void multipleInput()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8 | 0x1 | 0x2 | 0x4)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        index.station.insert("brw");
        index.archive.insert("clean");
        index.variable.insert("BsB_S11");

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 1000.0, 2000.0,
                                     0,
                                     600.0, false),});
        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"thd", "raw", "BsG_S11"}, Variant::Root(3.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"thd", "raw", "BsG_S11"}, Variant::Root(4.0), 1000.0, 2000.0,
                                     0,
                                     600.0, false),});

        StreamSink::Buffer result;
        {
            ReadSequence read(layout, index, {Selection(500, 2500, {"bnd", "thd"})});
            read.start();
            read.setEgress(&result);
            read.wait();
        }

        QVERIFY(result.ended());
        QVERIFY(compareSequence(result.values(), SequenceValue::Transfer{
                                        SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                                      1000.0, 0),
                                        SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 1000.0, 2000.0, 0),
                                        SequenceValue({"thd", "raw", "BsG_S11"}, Variant::Root(3.0), FP::undefined(),
                                                      1000.0, 0),
                                        SequenceValue({"thd", "raw", "BsG_S11"}, Variant::Root(4.0), 1000.0, 2000.0, 0),},
                                true));
    }

    void finalStreamFilter()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 2000.0, 3000.0,
                                     0,
                                     600.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 2000.0, 3000.0,
                                     -1,
                                     601.0, false),});

        StreamSink::Buffer result;
        {
            ReadSequence read(layout, index,
                              {Selection(500.0, 1500.0, {}, {}, {}, {}, {}, {}, 1000.0),
                               Selection(1500.0, 4000.0, {}, {}, {}, {}, {}, {}, 400.0)});
            read.start();
            read.setEgress(&result);
            read.wait();
        }

        QVERIFY(result.ended());
        QVERIFY(compareSequence(result.values(), SequenceValue::Transfer{
                                        SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 2000.0, 3000.0, 0),
                                        SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 2000.0, 3000.0, -1),},
                                true));
    }

    void bulk()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        SequenceValue::Transfer expected;
        SequenceName baseName("bnd", "raw", "BsG_S11");
        for (int i = 0; i < 1000; i++) {
            ArchiveValue::Transfer contents;
            double startTime = i * Structure::Block_Interval;
            for (int j = 0; j < 10; j++) {
                ArchiveValue
                        value(baseName, Variant::Root(i * j), startTime + j, startTime + j + 1, 0,
                              i * j + 1000.0, false);
                expected.emplace_back(value);
                contents.emplace_back(std::move(value));
            }
            sequenceInsert(&db, layout, index, i, contents);
        }

        StreamSink::Buffer result;
        {
            ReadSequence read(layout, index, {Selection()});
            read.start();
            read.setEgress(&result);
            read.wait();
        }

        QVERIFY(result.ended());
        QVERIFY(compareSequence(result.values(), expected, true));
    }

    void readArchive()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0,
                                     600.0, true),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0,
                                     2000.0,
                                     0, 601.0, false),});

        ArchiveSink::Buffer result;
        {
            ReadArchive read(layout, index, {Selection()});
            read.start();
            read.setEgress(&result);
            read.wait();
        }

        QVERIFY(result.ended());
        QVERIFY(compareArchive(result.values(), ArchiveValue::Transfer{
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(), 1000.0,
                             0,
                             500.0, false),
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(), 1001.0,
                             0,
                             501.0, false),
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0, 0,
                             600.0, true),
                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0, 2000.0,
                             0,
                             601.0, false),}, true));
    }

    void readErasure()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0,
                                     600.0, true),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0,
                                     2000.0,
                                     0, 601.0, false),});
        erasureInsert(&db, layout, index, 0,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1200.0, 2300.0, 0, 700.0)});

        ErasureSink::Buffer result;
        {
            ReadErasure read(layout, index, {Selection()});
            read.start();
            read.setEgress(&result);
            read.wait();
        }

        QVERIFY(result.ended());
        QVERIFY(compareArchive(result.archiveValues(), ArchiveValue::Transfer{
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(), 1000.0,
                             0,
                             500.0, false),
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(), 1001.0,
                             0,
                             501.0, false),
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0, 0,
                             600.0, true),
                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0, 2000.0,
                             0,
                             601.0, false),}, true));
        QVERIFY(compareErasure(result.erasureValues(), ArchiveErasure::Transfer{
                ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1200.0, 2300.0, 0, 700.0)}, true));
    }

    void emptyErasure()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0,
                                     600.0, true),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0,
                                     2000.0,
                                     0, 601.0, false),});

        ErasureSink::Buffer result;
        {
            ReadErasure read(layout, index, {Selection()});
            read.start();
            read.setEgress(&result);
            read.wait();
        }

        QVERIFY(result.ended());
        QVERIFY(compareArchive(result.archiveValues(), ArchiveValue::Transfer{
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(), 1000.0,
                             0,
                             500.0, false),
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(), 1001.0,
                             0,
                             501.0, false),
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0, 0,
                             600.0, true),
                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0, 2000.0,
                             0,
                             601.0, false),}, true));
        QVERIFY(result.erasureValues().empty());
    }

    void availableNames()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 1000.0, 2000.0,
                                     0,
                                     600.0, true),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(2.0), 1001.0,
                                     2000.0,
                                     0, 601.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"thd", "clean", "BsB_S11"}, Variant::Root(3.0), 1000.0,
                                     2000.0, 0,
                                     700.0, true),
                        ArchiveValue({"thd", "clean", "BsB_S11", {"pm10"}}, Variant::Root(4.0),
                                     1001.0,
                                     2000.0, 0, 701.0, false),});

        {
            Available::Names read(layout, index, {Selection()});
            read.start();
            read.wait();
            QCOMPARE(read.result(), (SequenceName::Set{{"bnd", "raw",   "BsG_S11"},
                                                       {"thd", "clean", "BsB_S11"}}));
        }
        {
            Available::Names read(layout, index, {Selection()}, Available::IndexOnly);
            read.start();
            read.wait();
            QCOMPARE(read.result(), (SequenceName::Set{{"bnd", "raw",   "BsG_S11"},
                                                       {"thd", "clean", "BsB_S11"}}));
        }
        {
            Available::Names read(layout, index, {Selection()}, Available::ReadAll);
            read.start();
            read.wait();
            QCOMPARE(read.result(), (SequenceName::Set{{"bnd", "raw",   "BsG_S11"},
                                                       {"bnd", "raw",   "BsG_S11", {"pm1"}},
                                                       {"thd", "clean", "BsB_S11"},
                                                       {"thd", "clean", "BsB_S11", {"pm10"}}}));
        }
        {
            Available::Names read(layout, index,
                                  {Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {}, {},
                                             {}, 650.0)});
            read.start();
            read.wait();
            QCOMPARE(read.result(), (SequenceName::Set{{"thd", "clean", "BsB_S11"}}));
        }
        {
            Available::Names read(layout, index,
                                  {Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {"pm10"},
                                             {}, {}, 650.0)}, Available::ReadAll);
            read.start();
            read.wait();
            QCOMPARE(read.result(), (SequenceName::Set{{"thd", "clean", "BsB_S11", {"pm10"}}}));
        }
        {
            Available::Names read(layout, index,
                                  {Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {}, {},
                                             {"pm25"}, 650.0)}, Available::ReadAll);
            read.start();
            read.wait();
            QVERIFY(read.result().empty());
        }
    }

    void availableComponents()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 1000.0, 2000.0,
                                     0,
                                     600.0, true),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(2.0), 1001.0,
                                     2000.0,
                                     0, 601.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"thd", "clean", "BsB_S11"}, Variant::Root(3.0), 1000.0,
                                     2000.0, 0,
                                     700.0, true),
                        ArchiveValue({"thd", "clean", "BsB_S11", {"pm10"}}, Variant::Root(4.0),
                                     1001.0,
                                     2000.0, 0, 701.0, false),});

        QCOMPARE(Available::Components::allStations(index),
                 (SequenceName::ComponentSet{"bnd", "thd"}));
        QCOMPARE(Available::Components::allArchives(index),
                 (SequenceName::ComponentSet{"raw", "clean"}));
        QCOMPARE(Available::Components::allVariables(index),
                 (SequenceName::ComponentSet{"BsG_S11", "BsB_S11"}));
        QCOMPARE(Available::Components::allFlavors(index),
                 (SequenceName::ComponentSet{"pm1", "pm10"}));

        {
            Available::Components read(layout, index, {Selection()});
            read.start();
            read.wait();
            QCOMPARE(read.stations(), (SequenceName::ComponentSet{"bnd", "thd"}));
            QCOMPARE(read.archives(), (SequenceName::ComponentSet{"raw", "clean"}));
            QCOMPARE(read.variables(), (SequenceName::ComponentSet{"BsG_S11", "BsB_S11"}));
        }
        {
            Available::Components read(layout, index, {Selection()}, Available::IndexOnly);
            read.start();
            read.wait();
            QCOMPARE(read.stations(), (SequenceName::ComponentSet{"bnd", "thd"}));
            QCOMPARE(read.archives(), (SequenceName::ComponentSet{"raw", "clean"}));
            QCOMPARE(read.variables(), (SequenceName::ComponentSet{"BsG_S11", "BsB_S11"}));
        }
        {
            Available::Components read(layout, index, {Selection()}, Available::ReadAll);
            read.start();
            read.wait();
            QCOMPARE(read.stations(), (SequenceName::ComponentSet{"bnd", "thd"}));
            QCOMPARE(read.archives(), (SequenceName::ComponentSet{"raw", "clean"}));
            QCOMPARE(read.variables(), (SequenceName::ComponentSet{"BsG_S11", "BsB_S11"}));
            QCOMPARE(read.flavors(), (SequenceName::ComponentSet{"pm1", "pm10"}));
        }
        {
            Available::Components read(layout, index,
                                       {Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {},
                                                  {}, {}, 650.0)});
            read.start();
            read.wait();
            QCOMPARE(read.stations(), (SequenceName::ComponentSet{"thd"}));
            QCOMPARE(read.archives(), (SequenceName::ComponentSet{"clean"}));
            QCOMPARE(read.variables(), (SequenceName::ComponentSet{"BsB_S11"}));
        }
        {
            Available::Components read(layout, index,
                                       {Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {},
                                                  {}, {"pm10"}, 650.0)}, Available::ReadAll);
            read.start();
            read.wait();
            QCOMPARE(read.stations(), (SequenceName::ComponentSet{"thd"}));
            QCOMPARE(read.archives(), (SequenceName::ComponentSet{"clean"}));
            QCOMPARE(read.variables(), (SequenceName::ComponentSet{"BsB_S11"}));
            QCOMPARE(read.flavors(), (SequenceName::ComponentSet{"pm10"}));
        }
        {
            Available::Components read(layout, index,
                                       {Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {},
                                                  {}, {"pm25"})});
            read.start();
            read.wait();
            QCOMPARE(read.stations(), (SequenceName::ComponentSet{"bnd", "thd"}));
            QCOMPARE(read.archives(), (SequenceName::ComponentSet{"raw", "clean"}));
            QCOMPARE(read.variables(), (SequenceName::ComponentSet{"BsG_S11", "BsB_S11"}));
            QVERIFY(read.flavors().empty());
        }
        {
            Available::Components read(layout, index,
                                       {Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {},
                                                  {}, {"pm25"}, 650.0)}, Available::ReadAll);
            read.start();
            read.wait();
            QVERIFY(read.stations().empty());
            QVERIFY(read.archives().empty());
            QVERIFY(read.variables().empty());
            QVERIFY(read.flavors().empty());
        }
    }

    void availableExists()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 1000.0, 2000.0,
                                     0,
                                     600.0, true),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(2.0), 1001.0,
                                     2000.0,
                                     0, 601.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"thd", "clean", "BsB_S11"}, Variant::Root(3.0), 1000.0,
                                     2000.0, 0,
                                     700.0, true),
                        ArchiveValue({"thd", "clean", "BsB_S11", {"pm10"}}, Variant::Root(4.0),
                                     1001.0,
                                     2000.0, 0, 701.0, false),});
        {
            Available::Exists read(layout, index, {Selection()});
            read.start();
            read.wait();
            QVERIFY(read.result());
        }
        {
            Available::Exists read(layout, index, {Selection()}, Available::IndexOnly);
            read.start();
            read.wait();
            QVERIFY(read.result());
        }
        {
            Available::Exists read(layout, index, {Selection()}, Available::ReadAll);
            read.start();
            read.wait();
            QVERIFY(read.result());
        }
        {
            Available::Exists read(layout, index,
                                   {Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {}, {},
                                              {}, 650.0)});
            read.start();
            read.wait();
            QVERIFY(read.result());
        }
        {
            Available::Exists read(layout, index,
                                   {Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {}, {},
                                              {"pm10"}, 650.0)}, Available::ReadAll);
            read.start();
            read.wait();
            QVERIFY(read.result());
        }
        {
            Available::Exists read(layout, index,
                                   {Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {}, {},
                                              {"pm25"})});
            read.start();
            read.wait();
            QVERIFY(read.result());
        }
        {
            Available::Exists read(layout, index,
                                   {Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {}, {},
                                              {}, 750.0)});
            read.start();
            read.wait();
            QVERIFY(!read.result());
        }
        {
            Available::Exists read(layout, index,
                                   {Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {}, {},
                                              {"pm25"}, 650.0)}, Available::ReadAll);
            read.start();
            read.wait();
            QVERIFY(!read.result());
        }
    }

    void basicLegacy()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x1)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0,
                                     600.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0,
                                     2000.0,
                                     0, 601.0, false),});

        StreamSink::Buffer result;
        {
            ReadSequence read(layout, index, {Selection()});
            read.start();
            read.setEgress(&result);
            read.wait();
        }

        QVERIFY(result.ended());
        QVERIFY(compareSequence(result.values(), SequenceValue::Transfer{
                                        SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                                      1000.0, 0),
                                        SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                                      1001.0, 0),
                                        SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0, 0),
                                        SequenceValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0,
                                                      2000.0, 0),},
                                true));
    }

    void readErasureLegacy()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x1)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0,
                                     600.0, true),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0,
                                     2000.0,
                                     0, 601.0, false),});
        erasureInsert(&db, layout, index, 0,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1200.0, 2300.0, 0, 700.0)});

        ErasureSink::Buffer result;
        {
            ReadErasure read(layout, index, {Selection()});
            read.start();
            read.setEgress(&result);
            read.wait();
        }

        QVERIFY(result.ended());
        QVERIFY(compareArchive(result.archiveValues(), ArchiveValue::Transfer{
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(), 1000.0,
                             0,
                             500.0, false),
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(), 1001.0,
                             0,
                             501.0, false),
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0, 0,
                             600.0, true),
                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0, 2000.0,
                             0,
                             601.0, false),}, true));
        QVERIFY(compareErasure(result.erasureValues(), ArchiveErasure::Transfer{
                ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1200.0, 2300.0, 0, 700.0)}, true));
    }
};

QTEST_APPLESS_MAIN(TestReadOperation)

#include "readoperation.moc"
