/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QTest>
#include <QTemporaryFile>

#include "datacore/archive/sequencefilter.hxx"

#include "common.cxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::Archive;

class TestSequenceFilter : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    Database::Storage archiveStorage() const
    {
        return Database::Storage::sqlite(databaseFile.fileName().toUtf8().constData());
    }

    static Database::Connection::SynchronousResult initialize(Database::Connection *connection,
                                                              int versionCode = 0x08)
    {
        static constexpr int systemIdentifier = 1;

        if (connection->hasTable("CPD3DBVersion"))
            return Database::Connection::Synchronous_Abort;

        Database::TableDefinition table("CPD3DBVersion");
        table.column("system", Database::TableDefinition::Integer, false);
        table.column("version", Database::TableDefinition::Integer, false);
        table.primaryKey("system");
        connection->createTable(table);

        connection->insert("CPD3DBVersion", {"system", "version"})
                  .synchronous({{"system",  systemIdentifier},
                                {"version", versionCode}});

        return Database::Connection::Synchronous_Ok;
    }

    static const uchar *rModified(double modified)
    {
        static QByteArray data;

        data.clear();
        QBuffer bfr(&data);
        bfr.open(QIODevice::WriteOnly);
        QDataStream stream(&bfr);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);
        stream << modified;

        return reinterpret_cast<const uchar *>(data.constData());
    }

    static bool hasBind(const Database::Types::Binds &binds,
                        const std::string &prefix,
                        Structure::Identity::index id)
    {
        for (const auto &check : binds) {
            if (!std::equal(prefix.begin(), prefix.end(), check.first.begin()))
                continue;
            auto v = check.second.toInteger();
            if (!INTEGER::defined(v) || v == 0)
                continue;
            if (id == static_cast<Structure::Identity::index>(v))
                return true;
        }
        return false;
    }

private slots:

    void initTestCase()
    {
        QVERIFY(databaseFile.open());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void everything()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        Initialization::createSequenceTable(&db, layout, Structure::Identity(0, 0, 0));

        auto inputs = SequenceFilter::Input::sequence(layout, index, {Selection()});

        QCOMPARE((int) inputs.size(), 1);

        QCOMPARE(inputs[0]->name, SequenceName("", "", ""));
        QCOMPARE(inputs[0]->identity, Structure::Identity(0, 0, 0));

        QVERIFY(inputs[0]->stream.buildIndexWhere().empty());
        {
            Database::Types::Binds binds;
            inputs[0]->stream.mergeIndexBinds(binds);
            QVERIFY(binds.empty());
        }
        QVERIFY(!FP::defined(inputs[0]->stream.modifiedAfter()));
        QVERIFY(inputs[0]->stream.firstBlock() < 0);
        QVERIFY(inputs[0]->stream.lastBlock() < 0);
        QVERIFY(inputs[0]->stream.block(-1));
        QVERIFY(inputs[0]->stream.block(1));
        QVERIFY(inputs[0]->stream.identity(Structure::Identity(1, 2, 3)));
        QVERIFY(inputs[0]->stream
                         .final(1, 1000.0, Structure::Identity(1, 2, 3),
                                SequenceName("s1", "a1", "v1")));

        QVERIFY(!FP::defined(inputs[0]->block.modifiedAfter()));
        QVERIFY(inputs[0]->block.flavors(""));
        QVERIFY(inputs[0]->block.flavors("f1"));
        QVERIFY(inputs[0]->block.bounds(1000, 2000));
        QVERIFY(inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1", {"f1"})));
        QVERIFY(inputs[0]->block.final(1000, 2000, rModified(999), SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[0]->block
                         .final(1000, 2000, rModified(999),
                                SequenceName("s1", "a1", "v1", {"f1"})));

        inputs[0]->advance(2000.0);
    }

    void everythingSplit()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8 | 0x1)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        auto s1 = index.station.insert("s1");
        auto s2 = index.station.insert("s2");

        Initialization::createSequenceTable(&db, layout, Structure::Identity(s1, 0, 0));
        Initialization::createSequenceTable(&db, layout, Structure::Identity(s2, 0, 0));

        auto inputs = SequenceFilter::Input::sequence(layout, index, {Selection()});

        QCOMPARE((int) inputs.size(), 2);

        std::size_t i1;
        std::size_t i2;
        if (inputs[0]->identity == Structure::Identity(s1, 0, 0)) {
            i1 = 0;
            i2 = 1;
        } else {
            i1 = 1;
            i2 = 0;
        }

        QCOMPARE(inputs[i1]->name, SequenceName("s1", "", ""));
        QCOMPARE(inputs[i1]->identity, Structure::Identity(s1, 0, 0));

        QVERIFY(inputs[i1]->stream.buildIndexWhere().empty());
        {
            Database::Types::Binds binds;
            inputs[i1]->stream.mergeIndexBinds(binds);
            QVERIFY(binds.empty());
        }
        QVERIFY(!FP::defined(inputs[i1]->stream.modifiedAfter()));
        QVERIFY(inputs[i1]->stream.firstBlock() < 0);
        QVERIFY(inputs[i1]->stream.lastBlock() < 0);
        QVERIFY(inputs[i1]->stream.block(-1));
        QVERIFY(inputs[i1]->stream.block(1));
        QVERIFY(inputs[i1]->stream.identity(Structure::Identity(s1, 2, 3)));
        QVERIFY(inputs[i1]->stream
                          .final(1, 1000.0, Structure::Identity(s1, 2, 3),
                                 SequenceName("s1", "a1", "v1")));

        QVERIFY(!FP::defined(inputs[i1]->block.modifiedAfter()));
        QVERIFY(inputs[i1]->block.flavors(""));
        QVERIFY(inputs[i1]->block.flavors("f1"));
        QVERIFY(inputs[i1]->block.bounds(1000, 2000));
        QVERIFY(inputs[i1]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[i1]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1", {"f1"})));
        QVERIFY(inputs[i1]->block
                          .final(1000, 2000, rModified(999), SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[i1]->block
                          .final(1000, 2000, rModified(999),
                                 SequenceName("s1", "a1", "v1", {"f1"})));

        inputs[i2]->advance(2000.0);

        QCOMPARE(inputs[i2]->name, SequenceName("s2", "", ""));
        QCOMPARE(inputs[i2]->identity, Structure::Identity(s2, 0, 0));

        QVERIFY(inputs[i2]->stream.buildIndexWhere().empty());
        {
            Database::Types::Binds binds;
            inputs[i2]->stream.mergeIndexBinds(binds);
            QVERIFY(binds.empty());
        }
        QVERIFY(!FP::defined(inputs[i2]->stream.modifiedAfter()));
        QVERIFY(inputs[i2]->stream.firstBlock() < 0);
        QVERIFY(inputs[i2]->stream.lastBlock() < 0);
        QVERIFY(inputs[i2]->stream.block(-1));
        QVERIFY(inputs[i2]->stream.block(1));
        QVERIFY(inputs[i2]->stream.identity(Structure::Identity(s2, 2, 3)));
        QVERIFY(inputs[i2]->stream
                          .final(1, 1000.0, Structure::Identity(s2, 2, 3),
                                 SequenceName("s1", "a1", "v1")));

        QVERIFY(!FP::defined(inputs[i2]->block.modifiedAfter()));
        QVERIFY(inputs[i2]->block.flavors(""));
        QVERIFY(inputs[i2]->block.flavors("f1"));
        QVERIFY(inputs[i2]->block.bounds(1000, 2000));
        QVERIFY(inputs[i2]->block.final(1000, 2000, 999, SequenceName("s2", "a1", "v1")));
        QVERIFY(inputs[i2]->block.final(1000, 2000, 999, SequenceName("s2", "a1", "v1", {"f1"})));
        QVERIFY(inputs[i2]->block
                          .final(1000, 2000, rModified(999), SequenceName("s2", "a1", "v1")));
        QVERIFY(inputs[i2]->block
                          .final(1000, 2000, rModified(999),
                                 SequenceName("s2", "a1", "v1", {"f1"})));

        inputs[i2]->advance(2000.0);
    }

    void singleSelection()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        auto s1 = index.station.insert("s1");
        auto sX = index.station.insert("sx");

        auto a1 = index.archive.insert("a1");
        auto aX = index.archive.insert("ax");

        auto v1 = index.variable.insert("v1");
        auto vX = index.variable.insert("vX");

        index.flavor.insert("f1");
        index.flavor.insert("fx");

        Initialization::createSequenceTable(&db, layout, Structure::Identity(0, 0, 0));

        auto inputs = SequenceFilter::Input::sequence(layout, index,
                                                      {Selection(1000, 2000, {"s1"}, {"a1"}, {"v1"},
                                                                 {}, {"fX"}, {}, 900.0)});

        QCOMPARE((int) inputs.size(), 1);

        QCOMPARE(inputs[0]->name, SequenceName("", "", ""));
        QCOMPARE(inputs[0]->identity, Structure::Identity(0, 0, 0));

        QVERIFY(!inputs[0]->stream.buildIndexWhere().empty());
        {
            Database::Types::Binds binds;
            inputs[0]->stream.mergeIndexBinds(binds);
            QVERIFY(!binds.empty());
            QVERIFY(hasBind(binds, Structure::Column_Station, s1));
            QVERIFY(hasBind(binds, Structure::Column_Archive, a1));
            QVERIFY(hasBind(binds, Structure::Column_Variable, v1));
        }
        QCOMPARE(inputs[0]->stream.modifiedAfter(), 900.0);
        QCOMPARE((int) inputs[0]->stream.firstBlock(), 0);
        QCOMPARE((int) inputs[0]->stream.lastBlock(), 0);
        QVERIFY(inputs[0]->stream.block(-1));
        QVERIFY(inputs[0]->stream.block(0));
        QVERIFY(inputs[0]->stream.identity(Structure::Identity(s1, a1, v1)));
#if 0
        QVERIFY(!inputs[0]->stream.block(2));
        QVERIFY(!inputs[0]->stream.identity(Structure::Identity(sX, a1, v1)));
        QVERIFY(!inputs[0]->stream.identity(Structure::Identity(s1, aX, v1)));
        QVERIFY(!inputs[0]->stream.identity(Structure::Identity(s1, a1, vX)));
#endif
        QVERIFY(inputs[0]->stream
                         .final(0, 1000.0, Structure::Identity(s1, a1, v1),
                                SequenceName("s1", "a1", "v1")));
        QVERIFY(!inputs[0]->stream
                          .final(0, 400.0, Structure::Identity(s1, a1, v1),
                                 SequenceName("s1", "a1", "v1")));
        QVERIFY(!inputs[0]->stream
                          .final(0, 1000.0, Structure::Identity(sX, a1, v1),
                                 SequenceName("sX", "a1", "v1")));
        QVERIFY(!inputs[0]->stream
                          .final(0, 1000.0, Structure::Identity(s1, aX, v1),
                                 SequenceName("s1", "aX", "v1")));
        QVERIFY(!inputs[0]->stream
                          .final(0, 1000.0, Structure::Identity(s1, a1, vX),
                                 SequenceName("s1", "a1", "vX")));
        QVERIFY(!inputs[0]->stream
                          .final(2, 1000.0, Structure::Identity(s1, a1, v1),
                                 SequenceName("s1", "a1", "v1")));

        QCOMPARE(inputs[0]->block.modifiedAfter(), 900.0);
        QVERIFY(inputs[0]->block.flavors(""));
        QVERIFY(inputs[0]->block.flavors("f1"));
        QVERIFY(!inputs[0]->block.flavors("fx"));
        QVERIFY(!inputs[0]->block.flavors("f1 fx"));
        QVERIFY(inputs[0]->block.bounds(1000, 2000));
        QVERIFY(!inputs[0]->block.bounds(3000, 4000));
        QVERIFY(inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1", {"f1"})));
        QVERIFY(inputs[0]->block.final(1000, 2000, rModified(999), SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[0]->block
                         .final(1000, 2000, rModified(999),
                                SequenceName("s1", "a1", "v1", {"f1"})));
        QVERIFY(!inputs[0]->block.final(1000, 2000, 500, SequenceName("s1", "a1", "v1")));
        QVERIFY(!inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1", {"fX"})));
        QVERIFY(!inputs[0]->block
                          .final(1000, 2000, rModified(500), SequenceName("s1", "a1", "v1")));
        QVERIFY(!inputs[0]->block.final(1000, 2000, 999, SequenceName("sX", "aX", "vX")));
        QVERIFY(!inputs[0]->block.final(2000, 3000, 999, SequenceName("s1", "a1", "v1")));

        inputs[0]->advance(3000.0);
    }

    void combineSelections()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8 | 0x1)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        auto s1 = index.station.insert("s1");
        auto sX = index.station.insert("sx");

        auto a1 = index.archive.insert("a1");
        auto a2 = index.archive.insert("a2");
        auto aX = index.archive.insert("ax");

        auto v1 = index.variable.insert("v1");
        auto vX = index.variable.insert("vX");

        index.flavor.insert("f1");
        index.flavor.insert("fx");

        Initialization::createSequenceTable(&db, layout, Structure::Identity(s1, 0, 0));

        auto inputs = SequenceFilter::Input::sequence(layout, index,
                                                      {Selection(1000, 2000, {"s1"}, {"a1"}, {"v1"},
                                                                 {}, {"fX"}, {}),
                                                       Selection(1000, 2000, {"s1"}, {"a2"},
                                                                 {"v1"})});

        QCOMPARE((int) inputs.size(), 1);

        QCOMPARE(inputs[0]->name, SequenceName("s1", "", ""));
        QCOMPARE(inputs[0]->identity, Structure::Identity(s1, 0, 0));

        QVERIFY(!inputs[0]->stream.buildIndexWhere().empty());
        {
            Database::Types::Binds binds;
            inputs[0]->stream.mergeIndexBinds(binds);
            QVERIFY(!binds.empty());
            QVERIFY(!hasBind(binds, Structure::Column_Station, s1));
            QVERIFY(hasBind(binds, Structure::Column_Archive, a1));
            QVERIFY(hasBind(binds, Structure::Column_Archive, a2));
            QVERIFY(hasBind(binds, Structure::Column_Variable, v1));
        }
        QVERIFY(!FP::defined(inputs[0]->stream.modifiedAfter()));
        QCOMPARE((int) inputs[0]->stream.firstBlock(), 0);
        QCOMPARE((int) inputs[0]->stream.lastBlock(), 0);
        QVERIFY(inputs[0]->stream.block(-1));
        QVERIFY(inputs[0]->stream.block(0));
        QVERIFY(inputs[0]->stream.identity(Structure::Identity(s1, a1, v1)));
        QVERIFY(inputs[0]->stream.identity(Structure::Identity(s1, a2, v1)));
#if 0
        QVERIFY(!inputs[0]->stream.block(2));
        QVERIFY(!inputs[0]->stream.identity(Structure::Identity(sX, a1, v1)));
        QVERIFY(!inputs[0]->stream.identity(Structure::Identity(s1, aX, v1)));
        QVERIFY(!inputs[0]->stream.identity(Structure::Identity(s1, a1, vX)));
#endif
        QVERIFY(inputs[0]->stream
                         .final(0, 1000.0, Structure::Identity(s1, a1, v1),
                                SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[0]->stream
                         .final(0, 1000.0, Structure::Identity(s1, a2, v1),
                                SequenceName("s1", "a2", "v1")));
        QVERIFY(!inputs[0]->stream
                          .final(0, 1000.0, Structure::Identity(sX, a1, v1),
                                 SequenceName("sX", "a1", "v1")));
        QVERIFY(!inputs[0]->stream
                          .final(0, 1000.0, Structure::Identity(s1, aX, v1),
                                 SequenceName("s1", "aX", "v1")));
        QVERIFY(!inputs[0]->stream
                          .final(0, 1000.0, Structure::Identity(s1, a1, vX),
                                 SequenceName("s1", "a1", "vX")));
        QVERIFY(!inputs[0]->stream
                          .final(2, 1000.0, Structure::Identity(s1, a1, v1),
                                 SequenceName("s1", "a1", "v1")));

        QVERIFY(!FP::defined(inputs[0]->block.modifiedAfter()));
        QVERIFY(inputs[0]->block.flavors(""));
        QVERIFY(inputs[0]->block.flavors("f1"));
        QVERIFY(inputs[0]->block.flavors("fx"));
        QVERIFY(inputs[0]->block.bounds(1000, 2000));
        QVERIFY(!inputs[0]->block.bounds(3000, 4000));
        QVERIFY(inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1", {"f1"})));
        QVERIFY(inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a2", "v1")));
        QVERIFY(inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a2", "v1", {"f1"})));
        QVERIFY(inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a2", "v1", {"fx"})));
        QVERIFY(inputs[0]->block.final(1000, 2000, rModified(999), SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[0]->block
                         .final(1000, 2000, rModified(999),
                                SequenceName("s1", "a1", "v1", {"f1"})));
        QVERIFY(!inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1", {"fX"})));
        QVERIFY(!inputs[0]->block.final(1000, 2000, 999, SequenceName("sX", "aX", "vX")));
        QVERIFY(!inputs[0]->block.final(2000, 3000, 999, SequenceName("s1", "a1", "v1")));

        inputs[0]->advance(3000.0);
    }

    void separateSelections()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        auto s1 = index.station.insert("s1");
        auto s2 = index.station.insert("s2");
        auto sX = index.station.insert("sx");

        auto a1 = index.archive.insert("a1");
        auto a2 = index.archive.insert("a2");
        auto aX = index.archive.insert("ax");

        auto v1 = index.variable.insert("v1");
        auto vX = index.variable.insert("vX");

        index.flavor.insert("f1");
        index.flavor.insert("fx");

        Initialization::createSequenceTable(&db, layout, Structure::Identity(0, 0, 0));

        auto inputs = SequenceFilter::Input::sequence(layout, index,
                                                      {Selection(FP::undefined(), 2000, {"s1"},
                                                                 {"a1"}, {"v1"}, {}, {"fX"}, {},
                                                                 900.0),
                                                       Selection(1500, FP::undefined(), {"s2"},
                                                                 {"a2"}, {"v1"})});

        QCOMPARE((int) inputs.size(), 1);

        QCOMPARE(inputs[0]->name, SequenceName("", "", ""));
        QCOMPARE(inputs[0]->identity, Structure::Identity(0, 0, 0));

        QVERIFY(!inputs[0]->stream.buildIndexWhere().empty());
        {
            Database::Types::Binds binds;
            inputs[0]->stream.mergeIndexBinds(binds);
            QVERIFY(!binds.empty());
            QVERIFY(hasBind(binds, Structure::Column_Station, s1));
            QVERIFY(hasBind(binds, Structure::Column_Station, s2));
            QVERIFY(hasBind(binds, Structure::Column_Archive, a1));
            QVERIFY(hasBind(binds, Structure::Column_Archive, a2));
            QVERIFY(hasBind(binds, Structure::Column_Variable, v1));
        }
        QVERIFY(!FP::defined(inputs[0]->stream.modifiedAfter()));
        QVERIFY(inputs[0]->stream.firstBlock() < 0);
        QVERIFY(inputs[0]->stream.lastBlock() < 0);
        QVERIFY(inputs[0]->stream.block(-1));
        QVERIFY(inputs[0]->stream.block(0));
        QVERIFY(inputs[0]->stream.identity(Structure::Identity(s1, a1, v1)));
        QVERIFY(inputs[0]->stream.identity(Structure::Identity(s2, a2, v1)));
#if 0
        QVERIFY(!inputs[0]->stream.identity(Structure::Identity(sX, a1, v1)));
        QVERIFY(!inputs[0]->stream.identity(Structure::Identity(s1, aX, v1)));
        QVERIFY(!inputs[0]->stream.identity(Structure::Identity(s1, a1, vX)));
        QVERIFY(!inputs[0]->stream.identity(Structure::Identity(s1, a2, v1)));
        QVERIFY(!inputs[0]->stream.identity(Structure::Identity(s2, a1, v1)));
#endif
        QVERIFY(inputs[0]->stream
                         .final(0, 1000.0, Structure::Identity(s1, a1, v1),
                                SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[0]->stream
                         .final(-1, 1000.0, Structure::Identity(s1, a1, v1),
                                SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[0]->stream
                         .final(0, 500.0, Structure::Identity(s2, a2, v1),
                                SequenceName("s2", "a2", "v1")));
        QVERIFY(!inputs[0]->stream
                          .final(0, 1000.0, Structure::Identity(sX, a1, v1),
                                 SequenceName("sX", "a1", "v1")));
        QVERIFY(!inputs[0]->stream
                          .final(0, 1000.0, Structure::Identity(s1, aX, v1),
                                 SequenceName("s1", "aX", "v1")));
        QVERIFY(!inputs[0]->stream
                          .final(0, 1000.0, Structure::Identity(s1, a1, vX),
                                 SequenceName("s1", "a1", "vX")));
        QVERIFY(!inputs[0]->stream
                          .final(0, 500, Structure::Identity(s1, a1, v1),
                                 SequenceName("s1", "a1", "v1")));
        QVERIFY(!inputs[0]->stream
                          .final(1, 1000.0, Structure::Identity(s1, a1, v1),
                                 SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[0]->stream
                         .final(1, 1000.0, Structure::Identity(s2, a2, v1),
                                SequenceName("s2", "a2", "v1")));

        QVERIFY(!FP::defined(inputs[0]->block.modifiedAfter()));
        QVERIFY(inputs[0]->block.flavors(""));
        QVERIFY(inputs[0]->block.flavors("f1"));
        QVERIFY(inputs[0]->block.flavors("fx"));
        QVERIFY(inputs[0]->block.bounds(FP::undefined(), FP::undefined()));
        QVERIFY(inputs[0]->block.bounds(FP::undefined(), 1000.0));
        QVERIFY(inputs[0]->block.bounds(3000.0, FP::undefined()));
        QVERIFY(inputs[0]->block.bounds(1000, 2000));
        QVERIFY(inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1", {"f1"})));
        QVERIFY(!inputs[0]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1", {"fx"})));
        QVERIFY(!inputs[0]->block.final(1000, 2000, 400, SequenceName("s1", "a1", "v1", {"f1"})));
        QVERIFY(inputs[0]->block.final(1000, 2000, rModified(999), SequenceName("s1", "a1", "v1")));
        QVERIFY(!inputs[0]->block
                          .final(1000, 2000, rModified(400), SequenceName("s1", "a1", "v1")));
        QVERIFY(!inputs[0]->block
                          .final(FP::undefined(), 1400, 999, SequenceName("s2", "a2", "v1")));
        QVERIFY(inputs[0]->block.final(1000, 2000, 999, SequenceName("s2", "a2", "v1")));
        QVERIFY(inputs[0]->block.final(1900, FP::undefined(), 999, SequenceName("s2", "a2", "v1")));
        QVERIFY(inputs[0]->block.final(1000, 2000, 400.0, SequenceName("s2", "a2", "v1")));
        QVERIFY(inputs[0]->block.final(1000, 2000, rModified(400), SequenceName("s2", "a2", "v1")));
        QVERIFY(inputs[0]->block.final(3000, FP::undefined(), 999, SequenceName("s2", "a2", "v1")));
        QVERIFY(!inputs[0]->block
                          .final(3000, FP::undefined(), 999, SequenceName("s1", "a1", "v1")));

        inputs[0]->advance(3000.0);
    }

    void eliminateSelections()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8 | 0x1)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        auto s1 = index.station.insert("s1");
        auto s2 = index.station.insert("s2");

        auto a1 = index.archive.insert("a1");
        auto a2 = index.archive.insert("a2");

        auto v1 = index.variable.insert("v1");

        index.flavor.insert("f1");
        index.flavor.insert("fx");

        Initialization::createSequenceTable(&db, layout, Structure::Identity(s1, 0, 0));
        Initialization::createSequenceTable(&db, layout, Structure::Identity(s2, 0, 0));

        auto inputs = SequenceFilter::Input::sequence(layout, index,
                                                      {Selection(FP::undefined(), 2000, {"s1"},
                                                                 {"a1"}, {"v1"}, {}, {}, {"f1"},
                                                                 900.0),
                                                       Selection(1500, FP::undefined(), {"s2"},
                                                                 {"a2"}, {"v1"})});

        QCOMPARE((int) inputs.size(), 2);

        std::size_t i1;
        std::size_t i2;
        if (inputs[0]->identity == Structure::Identity(s1, 0, 0)) {
            i1 = 0;
            i2 = 1;
        } else {
            i1 = 1;
            i2 = 0;
        }

        QCOMPARE(inputs[i1]->name, SequenceName("s1", "", ""));
        QCOMPARE(inputs[i1]->identity, Structure::Identity(s1, 0, 0));
        QCOMPARE(inputs[i2]->name, SequenceName("s2", "", ""));
        QCOMPARE(inputs[i2]->identity, Structure::Identity(s2, 0, 0));

        QVERIFY(!inputs[i1]->stream.buildIndexWhere().empty());
        QVERIFY(!inputs[i2]->stream.buildIndexWhere().empty());
        {
            Database::Types::Binds binds;
            inputs[i1]->stream.mergeIndexBinds(binds);
            QVERIFY(!binds.empty());
            QVERIFY(!hasBind(binds, Structure::Column_Station, s1));
            QVERIFY(!hasBind(binds, Structure::Column_Station, s2));
            QVERIFY(hasBind(binds, Structure::Column_Archive, a1));
            QVERIFY(!hasBind(binds, Structure::Column_Archive, a2));
            QVERIFY(!hasBind(binds, Structure::Column_Variable, v1));

            binds.clear();
            inputs[i2]->stream.mergeIndexBinds(binds);
            QVERIFY(!binds.empty());
            QVERIFY(!hasBind(binds, Structure::Column_Station, s1));
            QVERIFY(!hasBind(binds, Structure::Column_Station, s2));
            QVERIFY(!hasBind(binds, Structure::Column_Archive, a1));
            QVERIFY(hasBind(binds, Structure::Column_Archive, a2));
            QVERIFY(!hasBind(binds, Structure::Column_Variable, v1));
        }
        QCOMPARE(inputs[i1]->stream.modifiedAfter(), 900.0);
        QVERIFY(!FP::defined(inputs[i2]->stream.modifiedAfter()));
        QVERIFY(inputs[i1]->stream.firstBlock() < 0);
        QCOMPARE((int) inputs[i1]->stream.lastBlock(), 0);
        QCOMPARE((int) inputs[i2]->stream.firstBlock(), 0);
        QVERIFY(inputs[i2]->stream.lastBlock() < 0);
        QVERIFY(inputs[i1]->stream.block(-1));
        QVERIFY(inputs[i1]->stream.block(0));
        QVERIFY(inputs[i2]->stream.block(-1));
        QVERIFY(inputs[i2]->stream.block(0));
        QVERIFY(inputs[i2]->stream.block(1));
        QVERIFY(inputs[i1]->stream.identity(Structure::Identity(s1, a1, v1)));
        QVERIFY(inputs[i2]->stream.identity(Structure::Identity(s2, a2, v1)));
#if 0
        QVERIFY(!inputs[i1]->stream.identity(Structure::Identity(s2, a2, v1)));
        QVERIFY(!inputs[i2]->stream.identity(Structure::Identity(s1, a1, v1)));
#endif
        QVERIFY(inputs[i1]->stream
                          .final(0, 1000.0, Structure::Identity(s1, a1, v1),
                                 SequenceName("s1", "a1", "v1")));
        QVERIFY(!inputs[i1]->stream
                           .final(0, 1000.0, Structure::Identity(s2, a2, v1),
                                  SequenceName("s2", "a2", "v1")));
        QVERIFY(!inputs[i2]->stream
                           .final(0, 1000.0, Structure::Identity(s1, a1, v1),
                                  SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[i2]->stream
                          .final(0, 1000.0, Structure::Identity(s2, a2, v1),
                                 SequenceName("s2", "a2", "v1")));
        QVERIFY(!inputs[i1]->stream
                           .final(0, 400.0, Structure::Identity(s1, a1, v1),
                                  SequenceName("s1", "a1", "v1")));
        QVERIFY(inputs[i2]->stream
                          .final(0, 400.0, Structure::Identity(s2, a2, v1),
                                 SequenceName("s2", "a2", "v1")));

        QCOMPARE(inputs[i1]->block.modifiedAfter(), 900.0);
        QVERIFY(!FP::defined(inputs[i2]->block.modifiedAfter()));
        QVERIFY(!inputs[i1]->block.flavors(""));
        QVERIFY(inputs[i1]->block.flavors("f1"));
        QVERIFY(!inputs[i1]->block.flavors("fx"));
        QVERIFY(!inputs[i1]->block.flavors("f1 fx"));
        QVERIFY(inputs[i2]->block.flavors(""));
        QVERIFY(inputs[i2]->block.flavors("f1"));
        QVERIFY(inputs[i2]->block.flavors("fx"));
        QVERIFY(inputs[i1]->block.bounds(FP::undefined(), FP::undefined()));
        QVERIFY(inputs[i1]->block.bounds(FP::undefined(), 1000.0));
        QVERIFY(!inputs[i1]->block.bounds(3000.0, FP::undefined()));
        QVERIFY(inputs[i1]->block.bounds(1000, 2000));
        QVERIFY(inputs[i2]->block.bounds(FP::undefined(), FP::undefined()));
        QVERIFY(!inputs[i2]->block.bounds(FP::undefined(), 1000.0));
        QVERIFY(inputs[i2]->block.bounds(3000.0, FP::undefined()));
        QVERIFY(inputs[i2]->block.bounds(1000, 2000));
        QVERIFY(inputs[i1]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1", {"f1"})));
        QVERIFY(!inputs[i1]->block.final(1000, 2000, 500, SequenceName("s1", "a1", "v1", {"f1"})));
        QVERIFY(!inputs[i1]->block.final(2000, 2000, 999, SequenceName("s1", "a1", "v1", {"f1"})));
        QVERIFY(!inputs[i1]->block.final(1000, 2000, 999, SequenceName("s2", "a2", "v1")));
        QVERIFY(inputs[i2]->block.final(1000, 2000, 999, SequenceName("s2", "a2", "v1")));
        QVERIFY(!inputs[i2]->block.final(1000, 2000, 999, SequenceName("s1", "a1", "v1", {"f1"})));
        QVERIFY(!inputs[i2]->block.final(500, 1000, 999, SequenceName("s2", "a2", "v1")));
        QVERIFY(inputs[i2]->block.final(2000, 2000, 999, SequenceName("s2", "a2", "v1")));

        inputs[0]->advance(3000.0);

        QVERIFY(inputs[i2]->block.final(3000, 4000, 999, SequenceName("s2", "a2", "v1")));
    }
};

QTEST_APPLESS_MAIN(TestSequenceFilter)

#include "sequencefilter.moc"
