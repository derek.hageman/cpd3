/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QTest>
#include <QTemporaryFile>

#include "datacore/archive/writeoperation.hxx"

#include "common.cxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::Archive;

class TestWriteOperation : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    Database::Storage archiveStorage() const
    {
        return Database::Storage::sqlite(databaseFile.fileName().toUtf8().constData());
    }

private slots:

    void initTestCase()
    {
        QVERIFY(databaseFile.open());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void basic()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, true),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0,
                                     600.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0,
                                     2000.0,
                                     0, 601.0, false),});
        erasureInsert(&db, layout, index, -1,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 2000.0, 0,
                                      700.0)});
        erasureInsert(&db, layout, index, 0,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 2000.0, 3000.0, 0, 701.0)});

        {
            WriteSequence write(&db, layout, index);

            write.add(ArchiveValue(
                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.1), 1000.0, 2000.0, 0,
                                 6000.0,
                                 false)));
            write.add(ArchiveValue(
                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(4.1), FP::undefined(),
                                 2000.0, 0,
                                 6001.0, false)));
            write.remove(
                    ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 1001.0, 0, 7000.0));

            write.complete();
        }

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(),
                                           1000.0, 0, 500.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(4.1),
                                           FP::undefined(),
                                           2000.0, 0, 6001.0, true),}));
        QCOMPARE(sequenceModified(layout, index, -1, {"bnd", "raw", "BsG_S11"}), 6001.0);
        QVERIFY(checkArchive(layout, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.1), 1000.0,
                                           2000.0, 0,
                                           6000.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                           1001.0,
                                           2000.0, 0, 601.0, false),}));
        QCOMPARE(sequenceModified(layout, index, 0, {"bnd", "raw", "BsG_S11"}), 6000.0);

        QVERIFY(checkErasure(layout, index, -1,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 1001.0, 0,
                                             7000.0),}));
        QCOMPARE(erasureModified(layout, index, -1, {"bnd", "raw", "BsG_S11"}), 7000.0);
        QVERIFY(checkErasure(layout, index, 0,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 2000.0, 3000.0, 0,
                                             701.0),}));
        QCOMPARE(erasureModified(layout, index, 0, {"bnd", "raw", "BsG_S11"}), 701.0);
    }

    void basicLegacy()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x1)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, true),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0,
                                     600.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0,
                                     2000.0,
                                     0, 601.0, false),});
        erasureInsert(&db, layout, index, -1,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 2000.0, 0,
                                      700.0)});
        erasureInsert(&db, layout, index, 0,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 2000.0, 3000.0, 0, 701.0)});

        {
            WriteSequence write(&db, layout, index);

            write.add(ArchiveValue(
                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.1), 1000.0, 2000.0, 0,
                                 6000.0,
                                 false)));
            write.add(ArchiveValue(
                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(4.1), FP::undefined(),
                                 2000.0, 0,
                                 6001.0, false)));
            write.remove(
                    ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 1001.0, 0, 7000.0));

            write.complete();
        }

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(),
                                           1000.0, 0, 500.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(4.1),
                                           FP::undefined(),
                                           2000.0, 0, 6001.0, true),}));
        QCOMPARE(sequenceModified(layout, index, -1, {"bnd", "raw", "BsG_S11"}), 6001.0);
        QVERIFY(checkArchive(layout, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.1), 1000.0,
                                           2000.0, 0,
                                           6000.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                           1001.0,
                                           2000.0, 0, 601.0, false),}));
        QCOMPARE(sequenceModified(layout, index, 0, {"bnd", "raw", "BsG_S11"}), 6000.0);

        QVERIFY(checkErasure(layout, index, -1,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 1001.0, 0,
                                             7000.0),}));
        QCOMPARE(erasureModified(layout, index, -1, {"bnd", "raw", "BsG_S11"}), 7000.0);
        QVERIFY(checkErasure(layout, index, 0,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 2000.0, 3000.0, 0,
                                             701.0),}));
        QCOMPARE(erasureModified(layout, index, 0, {"bnd", "raw", "BsG_S11"}), 701.0);
    }

    void multipleGlobal()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, true),});
        sequenceInsert(&db, layout, index, -2,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), FP::undefined(),
                                     1001.0,
                                     1, 501.0, false),});

        erasureInsert(&db, layout, index, -1,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 2000.0, 0, 700.0),
                       ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 2000.0, 1,
                                      701.0),});
        erasureInsert(&db, layout, index, -2,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 2000.0, 2,
                                      702.0)});

        {
            WriteSequence write(&db, layout, index);

            write.add(ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.1), FP::undefined(),
                                   1001.0, 1,
                                   5000.0, false));
            write.remove(
                    ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 2000.0, 2, 7000.0));

            write.complete();
        }

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(),
                                           1000.0, 0, 500.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(),
                                           1001.0, 0, 501.0, true),}));
        QCOMPARE(sequenceModified(layout, index, -1, {"bnd", "raw", "BsG_S11"}), 501.0);
        QVERIFY(checkArchive(layout, index, -2,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.1),
                                           FP::undefined(),
                                           1001.0, 1, 5000.0, false),}));
        QCOMPARE(sequenceModified(layout, index, -2, {"bnd", "raw", "BsG_S11"}), 5000.0);

        QVERIFY(checkErasure(layout, index, -1,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 2000.0, 0,
                                             700.0),
                              ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 2000.0, 1,
                                             701.0),}));
        QCOMPARE(erasureModified(layout, index, -1, {"bnd", "raw", "BsG_S11"}), 701.0);
        QVERIFY(checkErasure(layout, index, -2,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 2000.0, 2,
                                             7000.0),}));
        QCOMPARE(erasureModified(layout, index, -2, {"bnd", "raw", "BsG_S11"}), 7000.0);
    }

    void emptyCreate()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8 | 0x1)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        {
            WriteSequence write(&db, layout, index);

            write.add(ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                   1001.0, 0,
                                   500.0, false));
            write.add(ArchiveValue({"thd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                   1001.0, 0,
                                   501.0, false));
            write.add(ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 2000.0, 3000.0, 0,
                                   502.0,
                                   false));

            write.complete();
        }

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(),
                                           1001.0, 0, 500.0, false),}));
        QCOMPARE(sequenceModified(layout, index, -1, {"bnd", "raw", "BsG_S11"}), 500.0);
        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"thd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(),
                                           1001.0, 0, 501.0, false),}));
        QCOMPARE(sequenceModified(layout, index, -1, {"thd", "raw", "BsG_S11"}), 501.0);
        QVERIFY(checkArchive(layout, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 2000.0,
                                           3000.0, 0,
                                           502.0, false),}));
        QCOMPARE(sequenceModified(layout, index, 0, {"bnd", "raw", "BsG_S11"}), 502.0);
    }

    void emptyRemove()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -2,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1001.0,
                                     1, 501.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 1001.0, 2000.0,
                                     0,
                                     601.0, false),});
        sequenceInsert(&db, layout, index, 1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0),
                                     Structure::Block_Interval + 1001.0,
                                     Structure::Block_Interval + 2000.0, 0,
                                     701.0, false),});

        erasureInsert(&db, layout, index, 2, {ArchiveErasure({"bnd", "raw", "BsG_S11"},
                                                             Structure::Block_Interval * 2 + 1001.0,
                                                             Structure::Block_Interval * 2 + 2000.0,
                                                             0, 702.0)});


        {
            WriteSequence write(&db, layout, index);

            write.remove(
                    SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.1), FP::undefined(),
                                  1001.0,
                                  1));
            write.remove(
                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.1), 1001.0, 2000.0, 0,
                                 6000.0,
                                 false));

            write.add(ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.1),
                                   Structure::Block_Interval * 2 + 1001.0,
                                   Structure::Block_Interval * 2 + 2000.0, 0, 6001.0, false));

            write.complete();
        }

        QVERIFY(checkArchive(layout, index, 1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0),
                                           Structure::Block_Interval + 1001.0,
                                           Structure::Block_Interval + 2000.0, 0,
                                           701.0, false),}));
        QCOMPARE(sequenceModified(layout, index, 1, {"bnd", "raw", "BsG_S11"}), 701.0);
        QVERIFY(checkArchive(layout, index, 2,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.1),
                                           Structure::Block_Interval * 2 + 1001.0,
                                           Structure::Block_Interval * 2 + 2000.0,
                                           0, 6001.0, true),}));
        QCOMPARE(sequenceModified(layout, index, 2, {"bnd", "raw", "BsG_S11"}), 6001.0);

        QVERIFY(loadSequence(layout, index, -2, {"bnd", "raw", "BsG_S11"}).isEmpty());
        QVERIFY(loadSequence(layout, index, 0, {"bnd", "raw", "BsG_S11"}).isEmpty());
        QVERIFY(loadErasure(layout, index, 2, {"bnd", "raw", "BsG_S11"}).isEmpty());
    }

    void emptyRemoveErasureGlobal()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        erasureInsert(&db, layout, index, -1,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 2000.0, 0,
                                      501.0),});

        {
            WriteSequence write(&db, layout, index);

            write.add(ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                   2000.0, 0,
                                   6000.0, false));

            write.complete();
        }

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(),
                                           2000.0, 0, 6000.0, true),}));
        QCOMPARE(sequenceModified(layout, index, -1, {"bnd", "raw", "BsG_S11"}), 6000.0);

        QVERIFY(loadErasure(layout, index, -1, {"bnd", "raw", "BsG_S11"}).isEmpty());
    }

    void selectGlobalTarget()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -2,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, false),});

        {
            WriteSequence write(&db, layout, index);

            write.add(ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.1), FP::undefined(),
                                   1001.0, 1,
                                   6001.0, false));

            write.complete();
        }

        QVERIFY(checkArchive(layout, index, -2,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(),
                                           1001.0, 0, 501.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.1),
                                           FP::undefined(),
                                           1001.0, 1, 6001.0, false)}));
        QCOMPARE(sequenceModified(layout, index, -2, {"bnd", "raw", "BsG_S11"}), 6001.0);
    }

    void bulkUnsorted()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        ArchiveValue::Transfer operation;
        std::vector<ArchiveValue::Transfer> expected;
        SequenceName baseName("bnd", "raw", "BsG_S11");
        for (int i = 0; i < 1000; i++) {
            ArchiveValue::Transfer contents;
            double startTime = i * Structure::Block_Interval;
            expected.emplace_back();
            for (int j = 0; j < 10; j++) {
                ArchiveValue
                        value(baseName, Variant::Root(i * j), startTime + j, startTime + j + 1, 0,
                              i * j + 1000.0, false);
                contents.push_back(value);

                value.setModified(i * j + 1001.0);
                value.setRoot(Variant::Root(i * j + 1));

                expected.back().push_back(value);
                operation.emplace_back(std::move(value));
            }
            sequenceInsert(&db, layout, index, i, contents);
        }

        {
            WriteSequence write(&db, layout, index);
            write.add(operation);
            write.complete();
        }

        for (int i = 0; i < 1000; i++) {
            QVERIFY(checkArchive(layout, index, i, expected[i]));
        }
    }

    void bulkSorted()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        ArchiveValue::Transfer operation;
        std::vector<ArchiveValue::Transfer> expected;
        SequenceName baseName("bnd", "raw", "BsG_S11");
        for (int i = 0; i < 1000; i++) {
            ArchiveValue::Transfer contents;
            double startTime = i * Structure::Block_Interval;
            expected.emplace_back();
            for (int j = 0; j < 10; j++) {
                ArchiveValue
                        value(baseName, Variant::Root(i * j), startTime + j, startTime + j + 1, 0,
                              i * j + 1000.0, false);
                contents.push_back(value);

                value.setModified(i * j + 1001.0);
                value.setRoot(Variant::Root(i * j + 1));

                expected.back().push_back(value);
                operation.emplace_back(std::move(value));
            }
            sequenceInsert(&db, layout, index, i, contents);
        }

        {
            WriteSequence write(&db, layout, index, true);
            write.add(operation);
            write.complete();
        }

        for (int i = 0; i < 1000; i++) {
            QVERIFY(checkArchive(layout, index, i, expected[i]));
        }
    }

    void synchronize()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, true),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), FP::undefined(),
                                     1001.0,
                                     1, 502.0, true),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(4.0), 1000.0, 2000.0,
                                     0,
                                     600.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(5.0), 1001.0,
                                     2000.0,
                                     0, 601.0, false),});
        erasureInsert(&db, layout, index, -1,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 2000.0, 0,
                                      700.0)});
        erasureInsert(&db, layout, index, 0,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 2000.0, 3000.0, 0, 701.0)});

        {
            WriteSynchronize write(&db, layout, index);

            write.add(ArchiveValue(
                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.1), FP::undefined(),
                                 1001.0, 0,
                                 5001.0, true)));
            write.remove(
                    ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 1000.0, 0, 400.0));
            write.remove(
                    ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 1001.0, 1, 6001.0));
            write.add(ArchiveValue(
                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.1), FP::undefined(),
                                 2000.0, 0,
                                 7001.0, true)));
            write.add(ArchiveValue(
                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.1), 2000.0, 3000.0, 0,
                                 401.0,
                                 true)));
            write.add(ArchiveValue(
                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(4.1), 1000.0, 2000.0, 0,
                                 8001.0,
                                 true)));

            write.complete();
        }

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(),
                                           1000.0, 0, 500.0, true),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.1),
                                           FP::undefined(),
                                           1001.0, 0, 5001.0, true),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.1),
                                           FP::undefined(),
                                           2000.0, 0, 7001.0, true)}));
        QCOMPARE(sequenceModified(layout, index, -1, {"bnd", "raw", "BsG_S11"}), 7001.0);
        QVERIFY(checkArchive(layout, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(4.1), 1000.0,
                                           2000.0, 0,
                                           8001.0, true),
                              ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(5.0),
                                           1001.0,
                                           2000.0, 0, 601.0, false),}));
        QCOMPARE(sequenceModified(layout, index, 0, {"bnd", "raw", "BsG_S11"}), 8001.0);

        QVERIFY(checkErasure(layout, index, -1,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 1001.0, 1,
                                             6001.0),}));
        QCOMPARE(erasureModified(layout, index, -1, {"bnd", "raw", "BsG_S11"}), 6001.0);
        QVERIFY(checkErasure(layout, index, 0,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 2000.0, 3000.0, 0,
                                             701.0),}));
        QCOMPARE(erasureModified(layout, index, 0, {"bnd", "raw", "BsG_S11"}), 701.0);
    }

    void flagRemoteReferenced()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);


        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, true),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), FP::undefined(),
                                     1001.0,
                                     1, 502.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(4.0), 1000.0, 2000.0,
                                     0,
                                     600.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(5.0), 1001.0,
                                     2000.0,
                                     0, 601.0, true),
                        ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(6.0), 1002.0,
                                     2000.0,
                                     0, 602.0, false),});

        {
            FlagRemoteReferenced write(&db, layout, index);

            write.add(ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                   1000.0, 0,
                                   500.0, false));
            write.add(ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                   1001.0, 0,
                                   501.0, true));
            write.add(ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(5.0), 1001.0,
                                   2000.0, 0,
                                   601.0, true));
            write.add(ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(6.0), 1002.0,
                                   2000.0, 0,
                                   602.0, false));

            write.complete();
        }

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(),
                                           1000.0, 0, 500.0, true),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(),
                                           1001.0, 0, 501.0, true),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0),
                                           FP::undefined(),
                                           1001.0, 1, 502.0, false)}));
        QCOMPARE(sequenceModified(layout, index, -1, {"bnd", "raw", "BsG_S11"}), 502.0);

        QVERIFY(checkArchive(layout, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(4.0), 1000.0,
                                           2000.0, 0,
                                           600.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(5.0),
                                           1001.0,
                                           2000.0, 0, 601.0, true),
                              ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(6.0),
                                           1002.0,
                                           2000.0, 0, 602.0, true)}));
        QCOMPARE(sequenceModified(layout, index, 0, {"bnd", "raw", "BsG_S11"}), 602.0);
    }
};

QTEST_APPLESS_MAIN(TestWriteOperation)

#include "writeoperation.moc"
