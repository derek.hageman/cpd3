/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QTemporaryFile>

#include "datacore/archive/structure.hxx"
#include "datacore/archive/initialization.hxx"
#include "database/connection.hxx"
#include "database/storage.hxx"
#include "database/tabledefinition.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::Archive;

class TestStructure : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    Database::Storage archiveStorage() const
    {
        return Database::Storage::sqlite(databaseFile.fileName().toUtf8().constData());
    }

    static Database::Connection::SynchronousResult initialize(Database::Connection *connection,
                                                              int versionCode = 1)
    {
        static constexpr int systemIdentifier = 1;

        if (connection->hasTable("CPD3DBVersion"))
            return Database::Connection::Synchronous_Abort;

        Database::TableDefinition table("CPD3DBVersion");
        table.column("system", Database::TableDefinition::Integer, false);
        table.column("version", Database::TableDefinition::Integer, false);
        table.primaryKey("system");
        connection->createTable(table);

        connection->insert("CPD3DBVersion", {"system", "version"})
                  .synchronous({{"system",  systemIdentifier},
                                {"version", versionCode}});

        return Database::Connection::Synchronous_Ok;
    }

private slots:

    void initTestCase()
    {
        QVERIFY(databaseFile.open());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void version1()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 1)));
        auto trans = db.transaction();
        Structure::Layout layout(&db);

        QVERIFY(!layout.splitStation());
        QVERIFY(!layout.splitArchive());
        QVERIFY(!layout.splitVariable());

        {
            Database::TableDefinition table("CPD3Data");
            table.integer<std::uint16_t>("station", false);
            table.integer<std::uint16_t>("archive", false);
            table.integer<std::uint16_t>("variable", false);
            table.integer<std::int16_t>("tblock", false);
            table.real("lastModified", false);
            table.bytes("data", false);
            table.primaryKey({"station", "archive", "variable", "tblock"});
            db.createTable(table);
        }
        QVERIFY(db.hasTable("CPD3Data"));
        db.insert("CPD3Data", {"station", "archive", "variable", "tblock", "lastModified", "data"})
          .synchronous({{"station",      static_cast<std::int_fast64_t>(1)},
                        {"archive",      static_cast<std::int_fast64_t>(2)},
                        {"variable",     static_cast<std::int_fast64_t>(3)},
                        {"tblock",       static_cast<std::int_fast64_t>(4)},
                        {"lastModified", 1000.0},
                        {"data",         Util::ByteArray("12345")}});

        {
            Database::TableDefinition table("CPD3Deleted");
            table.integer<std::uint16_t>("station", false);
            table.integer<std::uint16_t>("archive", false);
            table.integer<std::uint16_t>("variable", false);
            table.integer<std::int16_t>("tblock", false);
            table.real("lastModified", false);
            table.bytes("data", false);
            table.primaryKey({"station", "archive", "variable", "tblock"});
            db.createTable(table);
        }
        QVERIFY(db.hasTable("CPD3Deleted"));
        db.insert("CPD3Deleted",
                  {"station", "archive", "variable", "tblock", "lastModified", "data"})
          .synchronous({{"station",      static_cast<std::int_fast64_t>(5)},
                        {"archive",      static_cast<std::int_fast64_t>(6)},
                        {"variable",     static_cast<std::int_fast64_t>(7)},
                        {"tblock",       static_cast<std::int_fast64_t>(8)},
                        {"lastModified", 2000.0},
                        {"data",         Util::ByteArray("6789")}});

        QCOMPARE((int) layout.startBlock(1000), 0);
        QCOMPARE((int) layout.startBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.startBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.startBlock(604800), 1);
        QVERIFY((int) layout.startBlock(FP::undefined()) < 0);

        QCOMPARE((int) layout.endBlock(1000), 0);
        QCOMPARE((int) layout.endBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.endBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.endBlock(604800), 0);
        QCOMPARE((int) layout.endBlock(604800 + 604800), 1);
        QVERIFY((int) layout.endBlock(FP::undefined()) < 0);

        QCOMPARE((int) layout.targetBlock(1000, 2000), 0);
        QCOMPARE((int) layout.targetBlock(1000 + 604800, 2000 + 604800), 1);
        QVERIFY((int) layout.targetBlock(1000, 2000 + 604800) < 0);

        QCOMPARE((int) layout.targetBlock(1000), 0);
        QCOMPARE((int) layout.targetBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.targetBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.targetBlock(604800), 1);
        QVERIFY((int) layout.targetBlock(FP::undefined()) < 0);

        QCOMPARE(layout.blockStart(1), 604800.0);
        QCOMPARE(layout.blockStart(2), 604800.0 * 2.0);
        QVERIFY(!FP::defined(layout.blockStart(-1)));

        QCOMPARE(layout.blockEnd(1), 604800.0 * 2.0);
        QCOMPARE(layout.blockEnd(2), 604800.0 * 3.0);
        QVERIFY(!FP::defined(layout.blockEnd(-1)));

        QCOMPARE(layout.targetModified(1000.0001).toReal(), 1000.001);
        QCOMPARE(layout.modifiedQuery(1000.0001).toReal(), 1000.000);
        QCOMPARE(layout.modifiedValue(Database::Types::Variant(1000.000)), 1000.000);
        QCOMPARE(layout.modifiedThreshold(1000.0001), 1000.000);


        QCOMPARE(layout.sequenceTable(Structure::Identity(0, 0, 0)), std::string("CPD3Data"));
        QCOMPARE(layout.sequencePossible(), (std::unordered_set<Structure::Identity>{{0, 0, 0}}));

        QCOMPARE(layout.sequenceUnique({0, 0, 0}),
                 (std::unordered_set<Structure::Identity>{{1, 2, 3}}));
        QCOMPARE(layout.sequenceUnique({0, 0, 0}, false, true, true),
                 (std::unordered_set<Structure::Identity>{{0, 2, 3}}));
        QCOMPARE(layout.sequenceUnique({1, 0, 0}),
                 (std::unordered_set<Structure::Identity>{{1, 2, 3}}));

        auto q = layout.sequenceRead({1, 2, 3});
        auto r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("12345"));
        QCOMPARE((int) r[1].toInteger(), 4);

        q = layout.sequenceWrite({100, 200, 300}, Structure::Layout::WriteMode::KnownNew);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9997.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXY"));
            q.statement().synchronous(b);
        }
        q = layout.sequenceRead({100, 200, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXY"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.sequenceWrite({100, 200, 300}, Structure::Layout::WriteMode::Any);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9998.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXZ"));
            q.statement().synchronous(b);
        }
        q = layout.sequenceRead({100, 200, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXZ"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.sequenceWrite({100, 200, 300}, Structure::Layout::WriteMode::KnownExisting);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9999.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXX"));
            q.statement().synchronous(b);
        }
        q = layout.sequenceRead({100, 200, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXX"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.sequenceDelete({100, 200, 300}, "tblock = -1");
        q.statement().synchronous(q.binds());

        q = layout.sequenceDelete({100, 200, 300});
        q.statement().synchronous(q.binds());

        q = layout.sequenceSize({1, 2, 3});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 5);
        QCOMPARE((int) r[1].toInteger(), 4);

        q = layout.sequenceSize({1, 2, 3}, "tblock = 4");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 5);
        QCOMPARE((int) r[1].toInteger(), 4);

        q = layout.sequenceMinimum({1, 2, 3});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 1);
        QCOMPARE((int) r[0].toInteger(), 4);

        q = layout.sequenceIndex({1, 2, 3});
        auto ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 5);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE(r[1].toReal(), 1000.0);
        QCOMPARE((int) r[2].toInteger(), 1);
        QCOMPARE((int) r[3].toInteger(), 2);
        QCOMPARE((int) r[4].toInteger(), 3);

        q = layout.sequenceIndex({0, 0, 0});
        ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 5);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE(r[1].toReal(), 1000.0);
        QCOMPARE((int) r[2].toInteger(), 1);
        QCOMPARE((int) r[3].toInteger(), 2);
        QCOMPARE((int) r[4].toInteger(), 3);


        QCOMPARE(layout.erasureTable(Structure::Identity(0, 0, 0)), std::string("CPD3Deleted"));
        QCOMPARE(layout.erasurePossible(), (std::unordered_set<Structure::Identity>{{0, 0, 0}}));

        QCOMPARE(layout.erasureUnique({0, 0, 0}),
                 (std::unordered_set<Structure::Identity>{{5, 6, 7}}));
        QCOMPARE(layout.erasureUnique({0, 0, 0}, false, true, true),
                 (std::unordered_set<Structure::Identity>{{0, 6, 7}}));
        QCOMPARE(layout.erasureUnique({5, 0, 0}),
                 (std::unordered_set<Structure::Identity>{{5, 6, 7}}));

        q = layout.erasureRead({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("6789"));
        QCOMPARE((int) r[1].toInteger(), 8);

        q = layout.erasureWrite({100, 200, 300}, Structure::Layout::WriteMode::KnownNew);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9997.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXY"));
            q.statement().synchronous(b);
        }
        q = layout.erasureRead({100, 200, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXY"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.erasureWrite({100, 200, 300}, Structure::Layout::WriteMode::Any);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9998.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXZ"));
            q.statement().synchronous(b);
        }
        q = layout.erasureRead({100, 200, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXZ"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.erasureWrite({100, 200, 300}, Structure::Layout::WriteMode::KnownExisting);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9999.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXX"));
            q.statement().synchronous(b);
        }
        q = layout.erasureRead({100, 200, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXX"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.erasureDelete({100, 200, 300}, "tblock = -1");
        q.statement().synchronous(q.binds());

        q = layout.erasureDelete({100, 200, 300});
        q.statement().synchronous(q.binds());

        q = layout.erasureSize({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 8);

        q = layout.erasureSize({5, 6, 7}, "tblock = 8");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 8);

        q = layout.erasureMinimum({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 1);
        QCOMPARE((int) r[0].toInteger(), 8);

        q = layout.erasureIndex({5, 6, 7});
        ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 5);
        QCOMPARE((int) r[0].toInteger(), 8);
        QCOMPARE(r[1].toReal(), 2000.0);
        QCOMPARE((int) r[2].toInteger(), 5);
        QCOMPARE((int) r[3].toInteger(), 6);
        QCOMPARE((int) r[4].toInteger(), 7);

        q = layout.erasureIndex({0, 0, 0});
        ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 5);
        QCOMPARE((int) r[0].toInteger(), 8);
        QCOMPARE(r[1].toReal(), 2000.0);
        QCOMPARE((int) r[2].toInteger(), 5);
        QCOMPARE((int) r[3].toInteger(), 6);
        QCOMPARE((int) r[4].toInteger(), 7);
    }

    void version2_unsplit()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        Structure::Layout layout(&db);

        QVERIFY(!layout.splitStation());
        QVERIFY(!layout.splitArchive());
        QVERIFY(!layout.splitVariable());

        Initialization::createSequenceTable(&db, layout, Structure::Identity(0, 0, 0));
        QVERIFY(db.hasTable("CPD3Sequence"));
        db.insert("CPD3Sequence",
                  {"station", "archive", "variable", "tblock", "lastModified", "data"})
          .synchronous({{"station",      static_cast<std::int_fast64_t>(1)},
                        {"archive",      static_cast<std::int_fast64_t>(2)},
                        {"variable",     static_cast<std::int_fast64_t>(3)},
                        {"tblock",       static_cast<std::int_fast64_t>(4)},
                        {"lastModified", static_cast<std::int_fast64_t>(1000)},
                        {"data",         Util::ByteArray("12345")}});

        Initialization::createErasureTable(&db, layout, Structure::Identity(0, 0, 0));
        QVERIFY(db.hasTable("CPD3Erasure"));
        db.insert("CPD3Erasure",
                  {"station", "archive", "variable", "tblock", "lastModified", "data"})
          .synchronous({{"station",      static_cast<std::int_fast64_t>(5)},
                        {"archive",      static_cast<std::int_fast64_t>(6)},
                        {"variable",     static_cast<std::int_fast64_t>(7)},
                        {"tblock",       static_cast<std::int_fast64_t>(8)},
                        {"lastModified", static_cast<std::int_fast64_t>(2000)},
                        {"data",         Util::ByteArray("6789")}});

        QCOMPARE((int) layout.startBlock(1000), 0);
        QCOMPARE((int) layout.startBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.startBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.startBlock(604800), 1);
        QVERIFY((int) layout.startBlock(FP::undefined()) < 0);

        QCOMPARE((int) layout.endBlock(1000), 0);
        QCOMPARE((int) layout.endBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.endBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.endBlock(604800), 0);
        QCOMPARE((int) layout.endBlock(604800 + 604800), 1);
        QVERIFY((int) layout.endBlock(FP::undefined()) < 0);

        QCOMPARE((int) layout.targetBlock(1000, 2000), 0);
        QCOMPARE((int) layout.targetBlock(1000 + 604800, 2000 + 604800), 1);
        QVERIFY((int) layout.targetBlock(1000, 2000 + 604800) < 0);

        QCOMPARE((int) layout.targetBlock(1000), 0);
        QCOMPARE((int) layout.targetBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.targetBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.targetBlock(604800), 1);
        QVERIFY((int) layout.targetBlock(FP::undefined()) < 0);

        QCOMPARE(layout.blockStart(1), 604800.0);
        QCOMPARE(layout.blockStart(2), 604800.0 * 2.0);
        QVERIFY(!FP::defined(layout.blockStart(-1)));

        QCOMPARE(layout.blockEnd(1), 604800.0 * 2.0);
        QCOMPARE(layout.blockEnd(2), 604800.0 * 3.0);
        QVERIFY(!FP::defined(layout.blockEnd(-1)));

        QCOMPARE((int) layout.targetModified(1000.0001).toInteger(), 1000001);
        QCOMPARE((int) layout.modifiedQuery(1000.0001).toInteger(), 1000000);
        QCOMPARE(layout.modifiedValue(Database::Types::Variant(static_cast<std::int_fast64_t>(1000000))), 1000.000);
        QCOMPARE(layout.modifiedThreshold(1000.0001), 1000.000);


        QCOMPARE(layout.sequenceTable(Structure::Identity(0, 0, 0)), std::string("CPD3Sequence"));
        QCOMPARE(layout.sequencePossible(), (std::unordered_set<Structure::Identity>{{0, 0, 0}}));

        QCOMPARE(layout.sequenceUnique({0, 0, 0}),
                 (std::unordered_set<Structure::Identity>{{1, 2, 3}}));
        QCOMPARE(layout.sequenceUnique({0, 0, 0}, false, true, true),
                 (std::unordered_set<Structure::Identity>{{0, 2, 3}}));
        QCOMPARE(layout.sequenceUnique({1, 0, 0}),
                 (std::unordered_set<Structure::Identity>{{1, 2, 3}}));

        auto q = layout.sequenceRead({1, 2, 3});
        auto r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("12345"));
        QCOMPARE((int) r[1].toInteger(), 4);

        q = layout.sequenceWrite({100, 200, 300}, Structure::Layout::WriteMode::KnownNew);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9998.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXY"));
            q.statement().synchronous(b);
        }
        q = layout.sequenceRead({100, 200, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXY"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.sequenceWrite({100, 200, 300}, Structure::Layout::WriteMode::Any);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9998.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXZ"));
            q.statement().synchronous(b);
        }
        q = layout.sequenceRead({100, 200, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXZ"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.sequenceWrite({100, 200, 300}, Structure::Layout::WriteMode::KnownExisting);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9999.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXX"));
            q.statement().synchronous(b);
        }
        q = layout.sequenceRead({100, 200, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXX"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.sequenceDelete({100, 200, 300}, "tblock = -1");
        q.statement().synchronous(q.binds());

        q = layout.sequenceDelete({100, 200, 300});
        q.statement().synchronous(q.binds());

        q = layout.sequenceSize({1, 2, 3});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 5);
        QCOMPARE((int) r[1].toInteger(), 4);

        q = layout.sequenceSize({1, 2, 3}, "tblock = 4");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 5);
        QCOMPARE((int) r[1].toInteger(), 4);

        q = layout.sequenceMinimum({1, 2, 3});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 1);
        QCOMPARE((int) r[0].toInteger(), 4);

        q = layout.sequenceIndex({1, 2, 3});
        auto ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 5);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 1000);
        QCOMPARE((int) r[2].toInteger(), 1);
        QCOMPARE((int) r[3].toInteger(), 2);
        QCOMPARE((int) r[4].toInteger(), 3);

        q = layout.sequenceIndex({0, 0, 0});
        ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 5);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 1000);
        QCOMPARE((int) r[2].toInteger(), 1);
        QCOMPARE((int) r[3].toInteger(), 2);
        QCOMPARE((int) r[4].toInteger(), 3);


        QCOMPARE(layout.erasureTable(Structure::Identity(0, 0, 0)), std::string("CPD3Erasure"));
        QCOMPARE(layout.erasurePossible(), (std::unordered_set<Structure::Identity>{{0, 0, 0}}));

        QCOMPARE(layout.erasureUnique({0, 0, 0}),
                 (std::unordered_set<Structure::Identity>{{5, 6, 7}}));
        QCOMPARE(layout.erasureUnique({0, 0, 0}, false, true, true),
                 (std::unordered_set<Structure::Identity>{{0, 6, 7}}));
        QCOMPARE(layout.erasureUnique({5, 0, 0}),
                 (std::unordered_set<Structure::Identity>{{5, 6, 7}}));

        q = layout.erasureRead({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("6789"));
        QCOMPARE((int) r[1].toInteger(), 8);

        q = layout.erasureWrite({100, 200, 300}, Structure::Layout::WriteMode::KnownNew);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9997.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXY"));
            q.statement().synchronous(b);
        }
        q = layout.erasureRead({100, 200, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXY"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.erasureWrite({100, 200, 300}, Structure::Layout::WriteMode::Any);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9998.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXZ"));
            q.statement().synchronous(b);
        }
        q = layout.erasureRead({100, 200, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXZ"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.erasureWrite({100, 200, 300}, Structure::Layout::WriteMode::KnownExisting);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9999.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXX"));
            q.statement().synchronous(b);
        }
        q = layout.erasureRead({100, 200, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXX"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.erasureDelete({100, 200, 300}, "tblock = -1");
        q.statement().synchronous(q.binds());

        q = layout.erasureDelete({100, 200, 300});
        q.statement().synchronous(q.binds());

        q = layout.erasureSize({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 8);

        q = layout.erasureSize({5, 6, 7}, "tblock = 8");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 8);

        q = layout.erasureMinimum({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 1);
        QCOMPARE((int) r[0].toInteger(), 8);

        q = layout.erasureIndex({5, 6, 7});
        ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 5);
        QCOMPARE((int) r[0].toInteger(), 8);
        QCOMPARE((int) r[1].toInteger(), 2000);
        QCOMPARE((int) r[2].toInteger(), 5);
        QCOMPARE((int) r[3].toInteger(), 6);
        QCOMPARE((int) r[4].toInteger(), 7);

        q = layout.erasureIndex({0, 0, 0});
        ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 5);
        QCOMPARE((int) r[0].toInteger(), 8);
        QCOMPARE((int) r[1].toInteger(), 2000);
        QCOMPARE((int) r[2].toInteger(), 5);
        QCOMPARE((int) r[3].toInteger(), 6);
        QCOMPARE((int) r[4].toInteger(), 7);
    }

    void version2_fullsplit()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8 | 0x1 | 0x2 | 0x4)));
        auto trans = db.transaction();
        Structure::Layout layout(&db);

        QVERIFY(layout.splitStation());
        QVERIFY(layout.splitArchive());
        QVERIFY(layout.splitVariable());

        Initialization::createSequenceTable(&db, layout, Structure::Identity(1, 2, 3));
        QVERIFY(db.hasTable("CPD3Sequence_s1_a2_v3"));
        db.insert("CPD3Sequence_s1_a2_v3", {"tblock", "lastModified", "data"})
          .synchronous({{"tblock",       static_cast<std::int_fast64_t>(4)},
                        {"lastModified", static_cast<std::int_fast64_t>(1000)},
                        {"data",         Util::ByteArray("12345")}});

        Initialization::createErasureTable(&db, layout, Structure::Identity(5, 6, 7));
        QVERIFY(db.hasTable("CPD3Erasure_s5_a6_v7"));
        db.insert("CPD3Erasure_s5_a6_v7", {"tblock", "lastModified", "data"})
          .synchronous({{"tblock",       static_cast<std::int_fast64_t>(8)},
                        {"lastModified", static_cast<std::int_fast64_t>(2000)},
                        {"data",         Util::ByteArray("6789")}});

        QCOMPARE((int) layout.startBlock(1000), 0);
        QCOMPARE((int) layout.startBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.startBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.startBlock(604800), 1);
        QVERIFY((int) layout.startBlock(FP::undefined()) < 0);

        QCOMPARE((int) layout.endBlock(1000), 0);
        QCOMPARE((int) layout.endBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.endBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.endBlock(604800), 0);
        QCOMPARE((int) layout.endBlock(604800 + 604800), 1);
        QVERIFY((int) layout.endBlock(FP::undefined()) < 0);

        QCOMPARE((int) layout.targetBlock(1000, 2000), 0);
        QCOMPARE((int) layout.targetBlock(1000 + 604800, 2000 + 604800), 1);
        QVERIFY((int) layout.targetBlock(1000, 2000 + 604800) < 0);

        QCOMPARE((int) layout.targetBlock(1000), 0);
        QCOMPARE((int) layout.targetBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.targetBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.targetBlock(604800), 1);
        QVERIFY((int) layout.targetBlock(FP::undefined()) < 0);

        QCOMPARE(layout.blockStart(1), 604800.0);
        QCOMPARE(layout.blockStart(2), 604800.0 * 2.0);
        QVERIFY(!FP::defined(layout.blockStart(-1)));

        QCOMPARE(layout.blockEnd(1), 604800.0 * 2.0);
        QCOMPARE(layout.blockEnd(2), 604800.0 * 3.0);
        QVERIFY(!FP::defined(layout.blockEnd(-1)));

        QCOMPARE((int) layout.targetModified(1000.0001).toInteger(), 1000001);
        QCOMPARE((int) layout.modifiedQuery(1000.0001).toInteger(), 1000000);
        QCOMPARE(layout.modifiedValue(Database::Types::Variant(static_cast<std::int_fast64_t>(1000000))), 1000.000);
        QCOMPARE(layout.modifiedThreshold(1000.0001), 1000.000);


        QCOMPARE(layout.sequenceTable(Structure::Identity(1, 2, 3)),
                 std::string("CPD3Sequence_s1_a2_v3"));
        QCOMPARE(layout.sequencePossible(), (std::unordered_set<Structure::Identity>{{1, 2, 3}}));

        QCOMPARE(layout.sequenceUnique({1, 2, 3}),
                 (std::unordered_set<Structure::Identity>{{1, 2, 3}}));
        QCOMPARE(layout.sequenceUnique({1, 2, 3}, false, true, true),
                 (std::unordered_set<Structure::Identity>{{1, 2, 3}}));

        auto q = layout.sequenceRead({1, 2, 3});
        auto r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("12345"));
        QCOMPARE((int) r[1].toInteger(), 4);

        q = layout.sequenceWrite({1, 2, 3}, Structure::Layout::WriteMode::KnownNew);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9997.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXY"));
            q.statement().synchronous(b);
        }
        q = layout.sequenceRead({1, 2, 3}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXY"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.sequenceWrite({1, 2, 3}, Structure::Layout::WriteMode::Any);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9998.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXZ"));
            q.statement().synchronous(b);
        }
        q = layout.sequenceRead({1, 2, 3}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXZ"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.sequenceWrite({1, 2, 3}, Structure::Layout::WriteMode::KnownExisting);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9999.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXX"));
            q.statement().synchronous(b);
        }
        q = layout.sequenceRead({1, 2, 3}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXX"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.sequenceDelete({1, 2, 3}, "tblock = -1");
        q.statement().synchronous(q.binds());

        q = layout.sequenceSize({1, 2, 3});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 5);
        QCOMPARE((int) r[1].toInteger(), 4);

        q = layout.sequenceSize({1, 2, 3}, "tblock = 4");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 5);
        QCOMPARE((int) r[1].toInteger(), 4);

        q = layout.sequenceMinimum({1, 2, 3});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 1);
        QCOMPARE((int) r[0].toInteger(), 4);

        q = layout.sequenceIndex({1, 2, 3});
        auto ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 1000);


        QCOMPARE(layout.erasureTable(Structure::Identity(5, 6, 7)),
                 std::string("CPD3Erasure_s5_a6_v7"));
        QCOMPARE(layout.erasurePossible(), (std::unordered_set<Structure::Identity>{{5, 6, 7}}));

        QCOMPARE(layout.erasureUnique({5, 6, 7}),
                 (std::unordered_set<Structure::Identity>{{5, 6, 7}}));
        QCOMPARE(layout.erasureUnique({5, 6, 7}, false, true, true),
                 (std::unordered_set<Structure::Identity>{{5, 6, 7}}));

        q = layout.erasureRead({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("6789"));
        QCOMPARE((int) r[1].toInteger(), 8);

        q = layout.erasureWrite({5, 6, 7}, Structure::Layout::WriteMode::KnownNew);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9997.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXY"));
            q.statement().synchronous(b);
        }
        q = layout.erasureRead({5, 6, 7}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXY"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.erasureWrite({5, 6, 7}, Structure::Layout::WriteMode::Any);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9998.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXZ"));
            q.statement().synchronous(b);
        }
        q = layout.erasureRead({5, 6, 7}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXZ"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.erasureWrite({5, 6, 7}, Structure::Layout::WriteMode::KnownExisting);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9999.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXX"));
            q.statement().synchronous(b);
        }
        q = layout.erasureRead({5, 6, 7}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXX"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.erasureDelete({5, 6, 7}, "tblock = -1");
        q.statement().synchronous(q.binds());

        q = layout.erasureSize({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 8);

        q = layout.erasureSize({5, 6, 7}, "tblock = 8");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 8);

        q = layout.erasureMinimum({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 1);
        QCOMPARE((int) r[0].toInteger(), 8);

        q = layout.erasureIndex({5, 6, 7});
        ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 8);
        QCOMPARE((int) r[1].toInteger(), 2000);

        for (Structure::Identity::index i = 100; i < 120; i++) {
            Structure::Identity id(i, i, i);
            Initialization::createSequenceTable(&db, layout, id);
            QVERIFY(layout.sequencePossible().count(id) == 1);
        }
    }

    void version2_partialsplit()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8 | 0x1 | 0x2)));
        auto trans = db.transaction();
        Structure::Layout layout(&db);

        QVERIFY(layout.splitStation());
        QVERIFY(layout.splitArchive());
        QVERIFY(!layout.splitVariable());

        Initialization::createSequenceTable(&db, layout, Structure::Identity(1, 2, 0));
        QVERIFY(db.hasTable("CPD3Sequence_s1_a2"));
        db.insert("CPD3Sequence_s1_a2", {"variable", "tblock", "lastModified", "data"})
          .synchronous({{"variable",     static_cast<std::int_fast64_t>(3)},
                        {"tblock",       static_cast<std::int_fast64_t>(4)},
                        {"lastModified", static_cast<std::int_fast64_t>(1000)},
                        {"data",         Util::ByteArray("12345")}});

        Initialization::createErasureTable(&db, layout, Structure::Identity(5, 6, 0));
        QVERIFY(db.hasTable("CPD3Erasure_s5_a6"));
        db.insert("CPD3Erasure_s5_a6", {"variable", "tblock", "lastModified", "data"})
          .synchronous({{"variable",     static_cast<std::int_fast64_t>(7)},
                        {"tblock",       static_cast<std::int_fast64_t>(8)},
                        {"lastModified", static_cast<std::int_fast64_t>(2000)},
                        {"data",         Util::ByteArray("6789")}});

        QCOMPARE((int) layout.startBlock(1000), 0);
        QCOMPARE((int) layout.startBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.startBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.startBlock(604800), 1);
        QVERIFY((int) layout.startBlock(FP::undefined()) < 0);

        QCOMPARE((int) layout.endBlock(1000), 0);
        QCOMPARE((int) layout.endBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.endBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.endBlock(604800), 0);
        QCOMPARE((int) layout.endBlock(604800 + 604800), 1);
        QVERIFY((int) layout.endBlock(FP::undefined()) < 0);

        QCOMPARE((int) layout.targetBlock(1000, 2000), 0);
        QCOMPARE((int) layout.targetBlock(1000 + 604800, 2000 + 604800), 1);
        QVERIFY((int) layout.targetBlock(1000, 2000 + 604800) < 0);

        QCOMPARE((int) layout.targetBlock(1000), 0);
        QCOMPARE((int) layout.targetBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.targetBlock(1000 + 604800), 1);
        QCOMPARE((int) layout.targetBlock(604800), 1);
        QVERIFY((int) layout.targetBlock(FP::undefined()) < 0);

        QCOMPARE(layout.blockStart(1), 604800.0);
        QCOMPARE(layout.blockStart(2), 604800.0 * 2.0);
        QVERIFY(!FP::defined(layout.blockStart(-1)));

        QCOMPARE(layout.blockEnd(1), 604800.0 * 2.0);
        QCOMPARE(layout.blockEnd(2), 604800.0 * 3.0);
        QVERIFY(!FP::defined(layout.blockEnd(-1)));

        QCOMPARE((int) layout.targetModified(1000.0001).toInteger(), 1000001);
        QCOMPARE((int) layout.modifiedQuery(1000.0001).toInteger(), 1000000);
        QCOMPARE(layout.modifiedValue(Database::Types::Variant(static_cast<std::int_fast64_t>(1000000))), 1000.000);
        QCOMPARE(layout.modifiedThreshold(1000.0001), 1000.000);


        QCOMPARE(layout.sequenceTable(Structure::Identity(1, 2, 0)),
                 std::string("CPD3Sequence_s1_a2"));
        QCOMPARE(layout.sequencePossible(), (std::unordered_set<Structure::Identity>{{1, 2, 0}}));

        QCOMPARE(layout.sequenceUnique({1, 2, 0}),
                 (std::unordered_set<Structure::Identity>{{1, 2, 3}}));
        QCOMPARE(layout.sequenceUnique({1, 2, 0}, false, true, true),
                 (std::unordered_set<Structure::Identity>{{1, 2, 3}}));
        QCOMPARE(layout.sequenceUnique({1, 2, 3}),
                 (std::unordered_set<Structure::Identity>{{1, 2, 3}}));
        QCOMPARE(layout.sequenceUnique({1, 2, 99}), (std::unordered_set<Structure::Identity>()));

        auto q = layout.sequenceRead({1, 2, 3});
        auto r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("12345"));
        QCOMPARE((int) r[1].toInteger(), 4);

        q = layout.sequenceWrite({1, 2, 300}, Structure::Layout::WriteMode::KnownNew);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9997.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXY"));
            q.statement().synchronous(b);
        }
        q = layout.sequenceRead({1, 2, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXY"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.sequenceWrite({1, 2, 300}, Structure::Layout::WriteMode::Any);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9997.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXZ"));
            q.statement().synchronous(b);
        }
        q = layout.sequenceRead({1, 2, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXZ"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.sequenceWrite({1, 2, 300}, Structure::Layout::WriteMode::KnownExisting);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9999.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXX"));
            q.statement().synchronous(b);
        }
        q = layout.sequenceRead({1, 2, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXX"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.sequenceDelete({1, 2, 300}, "tblock = -1");
        q.statement().synchronous(q.binds());

        q = layout.sequenceDelete({1, 2, 300});
        q.statement().synchronous(q.binds());

        q = layout.sequenceSize({1, 2, 3});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 5);
        QCOMPARE((int) r[1].toInteger(), 4);

        q = layout.sequenceSize({1, 2, 3}, "tblock = 4");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 5);
        QCOMPARE((int) r[1].toInteger(), 4);

        q = layout.sequenceMinimum({1, 2, 3});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 1);
        QCOMPARE((int) r[0].toInteger(), 4);

        q = layout.sequenceIndex({1, 2, 3});
        auto ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 3);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 1000);
        QCOMPARE((int) r[2].toInteger(), 3);

        q = layout.sequenceIndex({1, 2, 0});
        ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 3);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 1000);
        QCOMPARE((int) r[2].toInteger(), 3);


        QCOMPARE(layout.erasureTable(Structure::Identity(5, 6, 0)),
                 std::string("CPD3Erasure_s5_a6"));
        QCOMPARE(layout.erasurePossible(), (std::unordered_set<Structure::Identity>{{5, 6, 0}}));

        QCOMPARE(layout.erasureUnique({5, 6, 0}),
                 (std::unordered_set<Structure::Identity>{{5, 6, 7}}));
        QCOMPARE(layout.erasureUnique({5, 6, 0}, false, true, true),
                 (std::unordered_set<Structure::Identity>{{5, 6, 7}}));
        QCOMPARE(layout.erasureUnique({5, 6, 7}),
                 (std::unordered_set<Structure::Identity>{{5, 6, 7}}));
        QCOMPARE(layout.erasureUnique({5, 6, 99}), (std::unordered_set<Structure::Identity>()));

        q = layout.erasureRead({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("6789"));
        QCOMPARE((int) r[1].toInteger(), 8);

        q = layout.erasureWrite({5, 6, 300}, Structure::Layout::WriteMode::KnownNew);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9997.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXY"));
            q.statement().synchronous(b);
        }
        q = layout.erasureRead({5, 6, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXY"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.erasureWrite({5, 6, 300}, Structure::Layout::WriteMode::Any);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9998.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXZ"));
            q.statement().synchronous(b);
        }
        q = layout.erasureRead({5, 6, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXZ"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.erasureWrite({5, 6, 300}, Structure::Layout::WriteMode::KnownExisting);
        {
            auto b = q.binds();
            b.emplace(Structure::Column_Block, (qlonglong) -1);
            b.emplace(Structure::Column_Modified, (double) 9999.0);
            b.emplace(Structure::Column_Data, Util::ByteArray("XXXX"));
            q.statement().synchronous(b);
        }
        q = layout.erasureRead({5, 6, 300}, "tblock = -1");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("XXXX"));
        QCOMPARE((int) r[1].toInteger(), -1);

        q = layout.erasureDelete({5, 6, 300}, "tblock = -1");
        q.statement().synchronous(q.binds());

        q = layout.erasureDelete({5, 6, 300});
        q.statement().synchronous(q.binds());

        q = layout.erasureSize({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 8);

        q = layout.erasureSize({5, 6, 7}, "tblock = 8");
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 8);

        q = layout.erasureMinimum({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 1);
        QCOMPARE((int) r[0].toInteger(), 8);

        q = layout.erasureIndex({5, 6, 7});
        ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 3);
        QCOMPARE((int) r[0].toInteger(), 8);
        QCOMPARE((int) r[1].toInteger(), 2000);
        QCOMPARE((int) r[2].toInteger(), 7);

        q = layout.erasureIndex({5, 6, 0});
        ar = q.statement().execute(q.binds()).all();
        QCOMPARE((int) ar.size(), 1);
        r = ar[0];
        QCOMPARE((int) r.size(), 3);
        QCOMPARE((int) r[0].toInteger(), 8);
        QCOMPARE((int) r[1].toInteger(), 2000);
        QCOMPARE((int) r[2].toInteger(), 7);
    }
};

QTEST_APPLESS_MAIN(TestStructure)

#include "structure.moc"
