/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QTest>
#include <QTemporaryFile>

#include "datacore/archive/resplitoperation.hxx"
#include "datacore/archive/initialization.hxx"
#include "core/qtcompat.hxx"

#include "common.cxx"

class TestResplitOperation : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    Database::Storage archiveStorage() const
    {
        return Database::Storage::sqlite(databaseFile.fileName().toUtf8().constData());
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        QVERIFY(databaseFile.open());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void resplitSeparate()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout source(&db);

        sequenceInsert(&db, source, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0, 0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0, 0, 501.0, false),});
        sequenceInsert(&db, source, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0, 600.0, false),});
        sequenceInsert(&db, source, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsB_S11"}, Variant::Root(4.0), 1000.0, 2000.0,
                                     0, 601.0, false),});
        erasureInsert(&db, source, index, 0,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0, 700.0),});

        QVERIFY(checkArchive(source, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(), 1000.0, 0, 500.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(), 1001.0, 0, 501.0, false),}));
        QCOMPARE(sequenceModified(source, index, -1, {"bnd", "raw", "BsG_S11"}), 501.0);
        QVERIFY(checkArchive(source, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                           2000.0, 0, 600.0, false),}));
        QCOMPARE(sequenceModified(source, index, 0, {"bnd", "raw", "BsG_S11"}), 600.0);
        QVERIFY(checkArchive(source, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsB_S11"}, Variant::Root(4.0), 1000.0,
                                           2000.0, 0, 601.0, false),}));
        QCOMPARE(sequenceModified(source, index, 0, {"bnd", "raw", "BsB_S11"}), 601.0);

        QVERIFY(checkErasure(source, index, 0,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0,
                                             700.0),}));
        QCOMPARE(erasureModified(source, index, 0, {"bnd", "raw", "BsG_S11"}), 700.0);


        Structure::Layout target(&db, true, true, true);

        {
            ResplitOperation op(&db, source, target);
            op.start();
            op.wait();
            QVERIFY(op.isOk());
        }

        QVERIFY(!db.hasTable(source.sequenceTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));
        QVERIFY(!db.hasTable(source.erasureTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));

        QVERIFY(db.hasTable(target.sequenceTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));
        QVERIFY(db.hasTable(target.sequenceTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsB_S11")})));
        QVERIFY(db.hasTable(target.erasureTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));

        QVERIFY(checkArchive(target, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(), 1000.0, 0, 500.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(), 1001.0, 0, 501.0, false),}));
        QCOMPARE(sequenceModified(target, index, -1, {"bnd", "raw", "BsG_S11"}), 501.0);
        QVERIFY(checkArchive(target, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                           2000.0, 0, 600.0, false),}));
        QCOMPARE(sequenceModified(target, index, 0, {"bnd", "raw", "BsG_S11"}), 600.0);
        QVERIFY(checkArchive(target, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsB_S11"}, Variant::Root(4.0), 1000.0,
                                           2000.0, 0, 601.0, false),}));
        QCOMPARE(sequenceModified(target, index, 0, {"bnd", "raw", "BsB_S11"}), 601.0);

        QVERIFY(checkErasure(target, index, 0,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0,
                                             700.0),}));
        QCOMPARE(erasureModified(target, index, 0, {"bnd", "raw", "BsG_S11"}), 700.0);

    }

    void resplitCombine()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0xF)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout source(&db);

        sequenceInsert(&db, source, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0, 0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0, 0, 501.0, false),});
        sequenceInsert(&db, source, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0, 600.0, false),});
        sequenceInsert(&db, source, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsB_S11"}, Variant::Root(4.0), 1000.0, 2000.0,
                                     0, 601.0, false),});
        erasureInsert(&db, source, index, 0,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0, 700.0),});

        QVERIFY(checkArchive(source, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(), 1000.0, 0, 500.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(), 1001.0, 0, 501.0, false),}));
        QCOMPARE(sequenceModified(source, index, -1, {"bnd", "raw", "BsG_S11"}), 501.0);
        QVERIFY(checkArchive(source, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                           2000.0, 0, 600.0, false),}));
        QCOMPARE(sequenceModified(source, index, 0, {"bnd", "raw", "BsG_S11"}), 600.0);
        QVERIFY(checkArchive(source, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsB_S11"}, Variant::Root(4.0), 1000.0,
                                           2000.0, 0, 601.0, false),}));
        QCOMPARE(sequenceModified(source, index, 0, {"bnd", "raw", "BsB_S11"}), 601.0);

        QVERIFY(checkErasure(source, index, 0,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0,
                                             700.0),}));
        QCOMPARE(erasureModified(source, index, 0, {"bnd", "raw", "BsG_S11"}), 700.0);


        Structure::Layout target(&db, false, false, false);

        {
            ResplitOperation op(&db, source, target);
            op.start();
            op.wait();
            QVERIFY(op.isOk());
        }

        QVERIFY(!db.hasTable(source.sequenceTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));
        QVERIFY(!db.hasTable(source.erasureTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));

        QVERIFY(db.hasTable(target.sequenceTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));
        QVERIFY(db.hasTable(target.erasureTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));

        QVERIFY(checkArchive(target, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(), 1000.0, 0, 500.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(), 1001.0, 0, 501.0, false),}));
        QCOMPARE(sequenceModified(target, index, -1, {"bnd", "raw", "BsG_S11"}), 501.0);
        QVERIFY(checkArchive(target, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                           2000.0, 0, 600.0, false),}));
        QCOMPARE(sequenceModified(target, index, 0, {"bnd", "raw", "BsG_S11"}), 600.0);
        QVERIFY(checkArchive(target, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsB_S11"}, Variant::Root(4.0), 1000.0,
                                           2000.0, 0, 601.0, false),}));
        QCOMPARE(sequenceModified(target, index, 0, {"bnd", "raw", "BsB_S11"}), 601.0);

        QVERIFY(checkErasure(target, index, 0,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0,
                                             700.0),}));
        QCOMPARE(erasureModified(target, index, 0, {"bnd", "raw", "BsG_S11"}), 700.0);
    }

    void resplitSeparatePartial()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout source(&db);

        sequenceInsert(&db, source, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0, 0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0, 0, 501.0, false),});
        sequenceInsert(&db, source, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0, 600.0, false),});
        sequenceInsert(&db, source, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsB_S11"}, Variant::Root(4.0), 1000.0, 2000.0,
                                     0, 601.0, false),});
        erasureInsert(&db, source, index, 0,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0, 700.0),});

        QVERIFY(checkArchive(source, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(), 1000.0, 0, 500.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(), 1001.0, 0, 501.0, false),}));
        QCOMPARE(sequenceModified(source, index, -1, {"bnd", "raw", "BsG_S11"}), 501.0);
        QVERIFY(checkArchive(source, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                           2000.0, 0, 600.0, false),}));
        QCOMPARE(sequenceModified(source, index, 0, {"bnd", "raw", "BsG_S11"}), 600.0);
        QVERIFY(checkArchive(source, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsB_S11"}, Variant::Root(4.0), 1000.0,
                                           2000.0, 0, 601.0, false),}));
        QCOMPARE(sequenceModified(source, index, 0, {"bnd", "raw", "BsB_S11"}), 601.0);

        QVERIFY(checkErasure(source, index, 0,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0,
                                             700.0),}));
        QCOMPARE(erasureModified(source, index, 0, {"bnd", "raw", "BsG_S11"}), 700.0);


        Structure::Layout target(&db, true, true, false);

        {
            ResplitOperation op(&db, source, target);
            op.start();
            op.wait();
            QVERIFY(op.isOk());
        }

        QVERIFY(!db.hasTable(source.sequenceTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));
        QVERIFY(!db.hasTable(source.erasureTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));

        QVERIFY(db.hasTable(target.sequenceTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));
        QVERIFY(db.hasTable(target.sequenceTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsB_S11")})));
        QVERIFY(db.hasTable(target.erasureTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));

        QVERIFY(checkArchive(target, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(), 1000.0, 0, 500.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(), 1001.0, 0, 501.0, false),}));
        QCOMPARE(sequenceModified(target, index, -1, {"bnd", "raw", "BsG_S11"}), 501.0);
        QVERIFY(checkArchive(target, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                           2000.0, 0, 600.0, false),}));
        QCOMPARE(sequenceModified(target, index, 0, {"bnd", "raw", "BsG_S11"}), 600.0);
        QVERIFY(checkArchive(target, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsB_S11"}, Variant::Root(4.0), 1000.0,
                                           2000.0, 0, 601.0, false),}));
        QCOMPARE(sequenceModified(target, index, 0, {"bnd", "raw", "BsB_S11"}), 601.0);

        QVERIFY(checkErasure(target, index, 0,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0,
                                             700.0),}));
        QCOMPARE(erasureModified(target, index, 0, {"bnd", "raw", "BsG_S11"}), 700.0);

    }

    void resplitCombinePartial()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0xF)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout source(&db);

        sequenceInsert(&db, source, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0, 0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0, 0, 501.0, false),});
        sequenceInsert(&db, source, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0, 600.0, false),});
        sequenceInsert(&db, source, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsB_S11"}, Variant::Root(4.0), 1000.0, 2000.0,
                                     0, 601.0, false),});
        erasureInsert(&db, source, index, 0,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0, 700.0),});

        QVERIFY(checkArchive(source, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(), 1000.0, 0, 500.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(), 1001.0, 0, 501.0, false),}));
        QCOMPARE(sequenceModified(source, index, -1, {"bnd", "raw", "BsG_S11"}), 501.0);
        QVERIFY(checkArchive(source, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                           2000.0, 0, 600.0, false),}));
        QCOMPARE(sequenceModified(source, index, 0, {"bnd", "raw", "BsG_S11"}), 600.0);
        QVERIFY(checkArchive(source, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsB_S11"}, Variant::Root(4.0), 1000.0,
                                           2000.0, 0, 601.0, false),}));
        QCOMPARE(sequenceModified(source, index, 0, {"bnd", "raw", "BsB_S11"}), 601.0);

        QVERIFY(checkErasure(source, index, 0,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0,
                                             700.0),}));
        QCOMPARE(erasureModified(source, index, 0, {"bnd", "raw", "BsG_S11"}), 700.0);


        Structure::Layout target(&db, false, false, true);

        {
            ResplitOperation op(&db, source, target);
            op.start();
            op.wait();
            QVERIFY(op.isOk());
        }

        QVERIFY(!db.hasTable(source.sequenceTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));
        QVERIFY(!db.hasTable(source.erasureTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));

        QVERIFY(db.hasTable(target.sequenceTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));
        QVERIFY(db.hasTable(target.erasureTable(
                {index.station.lookup("bnd"), index.archive.lookup("raw"),
                 index.variable.lookup("BsG_S11")})));

        QVERIFY(checkArchive(target, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(), 1000.0, 0, 500.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(), 1001.0, 0, 501.0, false),}));
        QCOMPARE(sequenceModified(target, index, -1, {"bnd", "raw", "BsG_S11"}), 501.0);
        QVERIFY(checkArchive(target, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                           2000.0, 0, 600.0, false),}));
        QCOMPARE(sequenceModified(target, index, 0, {"bnd", "raw", "BsG_S11"}), 600.0);
        QVERIFY(checkArchive(target, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsB_S11"}, Variant::Root(4.0), 1000.0,
                                           2000.0, 0, 601.0, false),}));
        QCOMPARE(sequenceModified(target, index, 0, {"bnd", "raw", "BsB_S11"}), 601.0);

        QVERIFY(checkErasure(target, index, 0,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0,
                                             700.0),}));
        QCOMPARE(erasureModified(target, index, 0, {"bnd", "raw", "BsG_S11"}), 700.0);
    }

    void v1Separate()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 1)));
        auto trans = db.transaction();
        Structure::Layout source(&db);

        QVERIFY(!source.splitStation());
        QVERIFY(!source.splitArchive());
        QVERIFY(!source.splitVariable());

        {
            Database::TableDefinition table("CPD3Data");
            table.integer<std::uint16_t>("station", false);
            table.integer<std::uint16_t>("archive", false);
            table.integer<std::uint16_t>("variable", false);
            table.integer<std::int16_t>("tblock", false);
            table.real("lastModified", false);
            table.bytes("data", false);
            table.primaryKey({"station", "archive", "variable", "tblock"});
            db.createTable(table);
        }
        QVERIFY(db.hasTable("CPD3Data"));
        db.insert("CPD3Data", {"station", "archive", "variable", "tblock", "lastModified", "data"})
          .synchronous({{"station",      static_cast<std::int_fast64_t>(1)},
                        {"archive",      static_cast<std::int_fast64_t>(2)},
                        {"variable",     static_cast<std::int_fast64_t>(3)},
                        {"tblock",       static_cast<std::int_fast64_t>(4)},
                        {"lastModified", 1000.0},
                        {"data",         Util::ByteArray("12345")}});

        {
            Database::TableDefinition table("CPD3Deleted");
            table.integer<std::uint16_t>("station", false);
            table.integer<std::uint16_t>("archive", false);
            table.integer<std::uint16_t>("variable", false);
            table.integer<std::int16_t>("tblock", false);
            table.real("lastModified", false);
            table.bytes("data", false);
            table.primaryKey({"station", "archive", "variable", "tblock"});
            db.createTable(table);
        }
        QVERIFY(db.hasTable("CPD3Deleted"));
        db.insert("CPD3Deleted",
                  {"station", "archive", "variable", "tblock", "lastModified", "data"})
          .synchronous({{"station",      static_cast<std::int_fast64_t>(5)},
                        {"archive",      static_cast<std::int_fast64_t>(6)},
                        {"variable",     static_cast<std::int_fast64_t>(7)},
                        {"tblock",       static_cast<std::int_fast64_t>(8)},
                        {"lastModified", 2000.0},
                        {"data",         Util::ByteArray("6789")}});

        Structure::Layout target(&db, true, true, false);

        {
            ResplitOperation op(&db, source, target);
            op.start();
            op.wait();
            QVERIFY(op.isOk());
        }

        QVERIFY(!db.hasTable(source.sequenceTable({1, 2, 3})));
        QVERIFY(!db.hasTable(source.erasureTable({5, 6, 7})));
        QVERIFY(!db.hasTable("CPD3Data"));
        QVERIFY(!db.hasTable("CPD3Deleted"));

        QVERIFY(db.hasTable(target.sequenceTable({1, 2, 3})));
        QVERIFY(db.hasTable(target.erasureTable({5, 6, 7})));
        QVERIFY(db.hasTable("CPD3Sequence_s1_a2"));
        QVERIFY(db.hasTable("CPD3Erasure_s5_a6"));

        auto q = target.sequenceRead({1, 2, 3});
        auto r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("12345"));
        QCOMPARE((int) r[1].toInteger(), 4);

        q = target.erasureRead({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].toBytes(), Util::ByteArray("6789"));
        QCOMPARE((int) r[1].toInteger(), 8);

        q = target.sequenceIndex({1, 2, 3});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 3);
        QCOMPARE((int) r[0].toInteger(), 4);
        QCOMPARE((int) r[1].toInteger(), 1000001);
        QCOMPARE((int) r[2].toInteger(), 3);

        q = target.erasureIndex({5, 6, 7});
        r = q.statement().extract(q.binds(), true);
        QCOMPARE((int) r.size(), 3);
        QCOMPARE((int) r[0].toInteger(), 8);
        QCOMPARE((int) r[1].toInteger(), 2000001);
        QCOMPARE((int) r[2].toInteger(), 7);
    }
};

QTEST_APPLESS_MAIN(TestResplitOperation)

#include "resplitoperation.moc"
