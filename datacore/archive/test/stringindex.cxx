/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QTemporaryFile>

#include "datacore/archive/stringindex.hxx"
#include "database/connection.hxx"
#include "database/storage.hxx"
#include "database/tabledefinition.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::Archive;

class TestStringIndex : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    Database::Storage archiveStorage() const
    {
        return Database::Storage::sqlite(databaseFile.fileName().toUtf8().constData());
    }

private slots:

    void initTestCase()
    {
        QVERIFY(databaseFile.open());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void basic()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();

        {
            Database::TableDefinition table("TestIndex");
            table.integer<std::uint16_t>("id", false);
            table.primaryKey("id");
            table.column("value", Database::TableDefinition::FiniteString, false, 127);
            table.index("Value", "value");
            db.createTable(table);
        }

        {
            StringIndex index(&db, "TestIndex");

            auto id = index.insert("abc");
            QCOMPARE((int) id, 1);

            QCOMPARE(index.lookup(id), std::string("abc"));
            QCOMPARE((int) index.lookup(std::string("abc")), (int) id);

            id = index.insert("abc");
            QCOMPARE((int) id, 1);

            {
                StringIndex index2(&db, "TestIndex");
                QCOMPARE(index.lookup(id), std::string("abc"));
                QCOMPARE((int) index.lookup(std::string("abc")), (int) id);

                id = index.insert("abc");
                QCOMPARE((int) id, 1);
            }

            id = index.insert("def");
            QCOMPARE((int) id, 2);
            QCOMPARE(index.lookup(id), std::string("def"));
            QCOMPARE((int) index.lookup(std::string("def")), (int) id);
        }

        db.del("TestIndex", "id = 1").synchronous();

        {
            StringIndex index(&db, "TestIndex");

            auto id = index.insert("qwe");
            QCOMPARE((int) id, 1);

            QCOMPARE(index.lookup(id), std::string("qwe"));
            QCOMPARE((int) index.lookup(std::string("qwe")), (int) id);
        }

        db.del("TestIndex", "id = 2").synchronous();

        {
            StringIndex index(&db, "TestIndex");

            auto id = index.insert("zxcv");
            QCOMPARE((int) id, 2);

            QCOMPARE(index.lookup(id), std::string("zxcv"));
            QCOMPARE((int) index.lookup(std::string("zxcv")), (int) id);
        }

        db.insert("TestIndex", {"id", "value"})
          .synchronous({{"id",    (qlonglong) 0x7FFF},
                        {"value", "none"}});

        {
            StringIndex index(&db, "TestIndex");

            QCOMPARE(index.lookup(0x7FFF), std::string("none"));
            QCOMPARE((int) index.lookup(std::string("none")), (int) 0x7FFF);

            auto id = index.insert("none");
            QCOMPARE((int) id, 0x7FFF);

            id = index.insert("iop");
            QCOMPARE((int) id, 3);

            QCOMPARE(index.lookup(id), std::string("iop"));
            QCOMPARE((int) index.lookup(std::string("iop")), (int) id);
        }

        {
            StringIndex index(&db, "TestIndex");

            index.remove(0x7FFF);
            index.lookup(2);
            index.remove(2);

            auto id = index.insert("none");
            QCOMPARE((int) id, 4);

            QCOMPARE(index.lookup(id), std::string("none"));
            QCOMPARE((int) index.lookup(std::string("none")), (int) id);
        }
    }

    void initialize()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();

        IdentityIndex::initialize(&db);
        QVERIFY(db.hasTable("CPD3Stations"));
        QVERIFY(db.hasTable("CPD3Archives"));
        QVERIFY(db.hasTable("CPD3Variables"));
        QVERIFY(db.hasTable("CPD3Flavors"));

        IdentityIndex index(&db);

        auto id = index.station.insert("bnd");
        QCOMPARE((int) id, 1);
        QCOMPARE(index.station.lookup(id), std::string("bnd"));
        QCOMPARE((int) index.station.lookup(std::string("bnd")), (int) id);
        QCOMPARE(db.select("CPD3Stations", {"value"}, "id = 1").single().toString(),
                 std::string("bnd"));

        id = index.archive.insert("raw_meta");
        QCOMPARE((int) id, 1);
        QCOMPARE(index.archive.lookup(id), std::string("raw_meta"));
        QCOMPARE((int) index.archive.lookup(std::string("raw_meta")), (int) id);
        QCOMPARE(db.select("CPD3Archives", {"value"}, "id = 1").single().toString(),
                 std::string("raw_meta"));

        id = index.variable.insert("BsG_S11");
        QCOMPARE((int) id, 1);
        QCOMPARE(index.variable.lookup(id), std::string("BsG_S11"));
        QCOMPARE((int) index.variable.lookup(std::string("BsG_S11")), (int) id);
        QCOMPARE(db.select("CPD3Variables", {"value"}, "id = 1").single().toString(),
                 std::string("BsG_S11"));

        id = index.flavor.insert("");
        QCOMPARE((int) id, 1);
        QCOMPARE(index.flavor.lookup(id), std::string(""));
        QCOMPARE((int) index.flavor.lookup(std::string("")), (int) id);
        QCOMPARE(db.select("CPD3Flavors", {"value"}, "id = 1").single().toString(), std::string(""));
    }
};

QTEST_APPLESS_MAIN(TestStringIndex)

#include "stringindex.moc"
