/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QTest>
#include <QTemporaryFile>

#include "datacore/archive/readblock.hxx"
#include "datacore/archive/sequencefilter.hxx"

#include "common.cxx"

class TestReadBlock : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    Database::Storage archiveStorage() const
    {
        return Database::Storage::sqlite(databaseFile.fileName().toUtf8().constData());
    }

private slots:

    void initTestCase()
    {
        QVERIFY(databaseFile.open());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void sequenceSingle()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        Initialization::createSequenceTable(&db, layout, Structure::Identity(0, 0, 0));
        index.station.insert("bnd");
        index.archive.insert("raw");
        index.variable.insert("BsG_S11");
        QByteArray data = constructSequence(index.flavor, {ArchiveValue({"bnd", "raw", "BsG_S11"},
                                                                        Variant::Root(1.0),
                                                                        1000.0, 2000.0, 0, 500.0, false)});

        auto filter = SequenceFilter::Input::sequence(layout, index, {Selection()});
        QCOMPARE((int) filter.size(), 1);

        auto sequence = ReadBlock::sequence(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                            filter.front()->block);
        QVERIFY(compareSequence(sequence, (SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 1000.0, 2000.0, 0)})));

        auto archive = ReadBlock::archive(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                          filter.front()->block);
        QVERIFY(compareArchive(archive, (ArchiveValue::Transfer{
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 1000.0, 2000.0, 0,
                             500.0,
                             false)})));

        auto names = ReadBlock::names(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                      filter.front()->block);
        QCOMPARE(names, (SequenceName::Set{{"bnd", "raw", "BsG_S11"}}));
    }

    void erasureSingle()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        Initialization::createErasureTable(&db, layout, Structure::Identity(0, 0, 0));
        index.station.insert("bnd");
        index.archive.insert("raw");
        index.variable.insert("BsG_S11");
        QByteArray data = constructErasure(index.flavor,
                                           {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0,
                                                           2000.0, 0, 500.0)});

        auto filter = SequenceFilter::Input::erasure(layout, index, {Selection()});
        QCOMPARE((int) filter.size(), 1);

        auto erasure = ReadBlock::erasure(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                          filter.front()->block);
        QVERIFY(compareErasure(erasure, (ArchiveErasure::Transfer{
                ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0, 500.0)})));
    }

    void sequenceFilter()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        Initialization::createSequenceTable(&db, layout, Structure::Identity(0, 0, 0));
        index.station.insert("bnd");
        index.archive.insert("raw");
        index.variable.insert("BsG_S11");
        QByteArray data = constructSequence(index.flavor, {ArchiveValue({"bnd", "raw", "BsG_S11"},
                                                                        Variant::Root(1.0),
                                                                        1000.0, 2000.0, 0, 500.0, false),
                                                           ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}},
                                                                        Variant::Root(2.0), 2000.0,
                                                                        3000.0, 1, 600.0,
                                                                        true), ArchiveValue(
                        {"bnd", "raw", "BsG_S11", {"pm1", "pm10"}}, Variant::Root(3.0), 3000.0,
                        4000.0, 0, 800.0, false)});

        auto filter = SequenceFilter::Input::sequence(layout, index,
                                                      {Selection(1500.0, 2500.0, {"bnd"}, {"raw"},
                                                                 {"BsG_S11"}, {"pm1"}, {}, {},
                                                                 510.0)});
        QCOMPARE((int) filter.size(), 1);

        auto sequence = ReadBlock::sequence(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                            filter.front()->block);
        QVERIFY(compareSequence(sequence, (SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(2.0), 2000.0,
                              3000.0,
                              1)})));

        auto archive = ReadBlock::archive(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                          filter.front()->block);
        QVERIFY(compareArchive(archive, (ArchiveValue::Transfer{
                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(2.0), 2000.0, 3000.0,
                             1,
                             600.0, true)})));

        auto names = ReadBlock::names(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                      filter.front()->block);
        QCOMPARE(names, (SequenceName::Set{{"bnd", "raw", "BsG_S11", {"pm1"}}}));


        filter = SequenceFilter::Input::sequence(layout, index,
                                                 {Selection(500.0, 5000.0, {"bnd"}, {"raw"},
                                                            {"BsG_S11"}, {}, {"pm10"}, {}, 400.0)});
        QCOMPARE((int) filter.size(), 1);

        sequence = ReadBlock::sequence(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                       filter.front()->block);
        QVERIFY(compareSequence(sequence, (SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 1000.0, 2000.0, 0),
                SequenceValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(2.0), 2000.0,
                              3000.0,
                              1)})));

        archive = ReadBlock::archive(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                     filter.front()->block);
        QVERIFY(compareArchive(archive, (ArchiveValue::Transfer{
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 1000.0, 2000.0, 0,
                             500.0,
                             false),
                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(2.0), 2000.0, 3000.0,
                             1,
                             600.0, true)})));

        names = ReadBlock::names(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                 filter.front()->block);
        QCOMPARE(names, (SequenceName::Set{{"bnd", "raw", "BsG_S11", {"pm1"}},
                                           {"bnd", "raw", "BsG_S11"}}));


        filter = SequenceFilter::Input::sequence(layout, index,
                                                 {Selection(3000.0, FP::undefined(), {"bnd"},
                                                            {"raw"}, {"BsG_S11"}, {}, {}, {"pm1"}),
                                                  Selection(FP::undefined(), FP::undefined(),
                                                            {"bnd"}, {"raw"}, {"BsG_S11"}, {}, {},
                                                            {""})});
        QCOMPARE((int) filter.size(), 1);

        sequence = ReadBlock::sequence(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                       filter.front()->block);
        QVERIFY(compareSequence(sequence, (SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 1000.0, 2000.0, 0)})));

        archive = ReadBlock::archive(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                     filter.front()->block);
        QVERIFY(compareArchive(archive, (ArchiveValue::Transfer{
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 1000.0, 2000.0, 0,
                             500.0,
                             false)})));

        names = ReadBlock::names(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                 filter.front()->block);
        QCOMPARE(names, (SequenceName::Set{{"bnd", "raw", "BsG_S11"}}));
    }

    void erasureFilter()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        Initialization::createErasureTable(&db, layout, Structure::Identity(0, 0, 0));
        index.station.insert("bnd");
        index.archive.insert("raw");
        index.variable.insert("BsG_S11");
        QByteArray data = constructErasure(index.flavor,
                                           {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0,
                                                           2000.0, 0, 500.0),
                                            ArchiveErasure({"bnd", "raw", "BsG_S11", {"pm1"}},
                                                           2000.0, 3000.0, 1, 600.0),
                                            ArchiveErasure(
                                                    {"bnd", "raw", "BsG_S11", {"pm1", "pm10"}},
                                                    3000.0, 4000.0, 0, 800.0)});

        auto filter = SequenceFilter::Input::erasure(layout, index,
                                                     {Selection(1500.0, 2500.0, {"bnd"}, {"raw"},
                                                                {"BsG_S11"}, {"pm1"}, {}, {},
                                                                510.0)});
        QCOMPARE((int) filter.size(), 1);

        auto erasure = ReadBlock::erasure(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                          filter.front()->block);
        QVERIFY(compareErasure(erasure, (ArchiveErasure::Transfer{
                ArchiveErasure({"bnd", "raw", "BsG_S11", {"pm1"}}, 2000.0, 3000.0, 1, 600.0)})));


        filter = SequenceFilter::Input::erasure(layout, index,
                                                {Selection(500.0, 5000.0, {"bnd"}, {"raw"},
                                                           {"BsG_S11"}, {}, {"pm10"}, {}, 400.0)});
        QCOMPARE((int) filter.size(), 1);

        erasure = ReadBlock::erasure(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                     filter.front()->block);
        QVERIFY(compareErasure(erasure, (ArchiveErasure::Transfer{
                ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0, 500.0),
                ArchiveErasure({"bnd", "raw", "BsG_S11", {"pm1"}}, 2000.0, 3000.0, 1, 600.0)})));


        filter = SequenceFilter::Input::erasure(layout, index,
                                                {Selection(3000.0, FP::undefined(), {"bnd"},
                                                           {"raw"}, {"BsG_S11"}, {}, {}, {"pm1"}),
                                                 Selection(FP::undefined(), FP::undefined(),
                                                           {"bnd"}, {"raw"}, {"BsG_S11"}, {}, {},
                                                           {""})});
        QCOMPARE((int) filter.size(), 1);

        erasure = ReadBlock::erasure(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                     filter.front()->block);
        QVERIFY(compareErasure(erasure, (ArchiveErasure::Transfer{
                ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0, 500.0)})));
    }

    void sequenceBulk()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        Initialization::createSequenceTable(&db, layout, Structure::Identity(0, 0, 0));
        index.station.insert("bnd");
        index.archive.insert("avgh");
        index.variable.insert("BsG_S11");

        ArchiveValue::Transfer allArchive;
        SequenceValue::Transfer allSequence;
        for (int i = 0; i < 1000; i++) {
            SequenceName name("bnd", "avgh", "BsG_S11");
            switch (i % 3) {
            case 1:
                name.setFlavors({"stats"});
                break;
            case 2:
                name.setFlavors({"cover"});
                break;
            default:
                break;
            }
            allArchive.push_back(
                    ArchiveValue(name, Variant::Root((qint64) i), 1000.0 + i / 10, 2000.0 + i / 5,
                                 i % 3,
                                 500 + i % 100, i % 20 == 0));
            allSequence.push_back(SequenceValue(allArchive.back()));
        }

        QByteArray data = constructSequence(index.flavor, allArchive);

        auto filter = SequenceFilter::Input::sequence(layout, index, {Selection()});
        QCOMPARE((int) filter.size(), 1);

        auto sequence = ReadBlock::sequence(data, {"bnd", "avgh", "BsG_S11"}, index.flavor,
                                            filter.front()->block);
        QVERIFY(compareSequence(sequence, allSequence));

        auto archive = ReadBlock::archive(data, {"bnd", "avgh", "BsG_S11"}, index.flavor,
                                          filter.front()->block);
        QVERIFY(compareArchive(archive, allArchive));

        auto names = ReadBlock::names(data, {"bnd", "avgh", "BsG_S11"}, index.flavor,
                                      filter.front()->block);
        QCOMPARE(names, (SequenceName::Set{{"bnd", "avgh", "BsG_S11"},
                                           {"bnd", "avgh", "BsG_S11", {"stats"}},
                                           {"bnd", "avgh", "BsG_S11", {"cover"}}}));
    }

    void erasureBulk()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        Initialization::createErasureTable(&db, layout, Structure::Identity(0, 0, 0));
        index.station.insert("bnd");
        index.archive.insert("avgh");
        index.variable.insert("BsG_S11");

        ArchiveErasure::Transfer allErasure;
        for (int i = 0; i < 1000; i++) {
            SequenceName name("bnd", "avgh", "BsG_S11");
            switch (i % 3) {
            case 1:
                name.setFlavors({"stats"});
                break;
            case 2:
                name.setFlavors({"cover"});
                break;
            default:
                break;
            }
            allErasure.push_back(
                    ArchiveErasure(name, 1000.0 + i / 10, 2000.0 + i / 5, i % 3, 500 + i % 100));
        }

        QByteArray data = constructErasure(index.flavor, allErasure);

        auto filter = SequenceFilter::Input::erasure(layout, index, {Selection()});
        QCOMPARE((int) filter.size(), 1);

        auto erasure = ReadBlock::erasure(data, {"bnd", "avgh", "BsG_S11"}, index.flavor,
                                          filter.front()->block);
        QVERIFY(compareErasure(erasure, allErasure));
    }

    void sequenceModifedTwo()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        Initialization::createSequenceTable(&db, layout, Structure::Identity(0, 0, 0));
        index.station.insert("bnd");
        index.archive.insert("raw");
        index.variable.insert("BsG_S11");
        QByteArray data = constructSequence(index.flavor, {ArchiveValue({"bnd", "raw", "BsG_S11"},
                                                                        Variant::Root(1.0),
                                                                        1000.0, 2000.0, 0, 500.0, false),
                                                           ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}},
                                                                        Variant::Root(2.0), 2000.0,
                                                                        3000.0, 1, 600.0,
                                                                        true)});

        auto filter = SequenceFilter::Input::sequence(layout, index,
                                                      {Selection(1500.0, 2500.0, {"bnd"}, {"raw"},
                                                                 {"BsG_S11"}, {"pm1"}, {}, {},
                                                                 510.0)});
        QCOMPARE((int) filter.size(), 1);

        auto sequence = ReadBlock::sequence(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                            filter.front()->block);
        QVERIFY(compareSequence(sequence, (SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(2.0), 2000.0,
                              3000.0,
                              1)})));

#if 0
        auto archive = ReadBlock::archive(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                          filter.front()->block);
        QVERIFY(compareArchive(archive, (ArchiveValue::Transfer{
                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(2.0), 2000.0, 3000.0, 1,
                             600.0, true)})));

        auto names = ReadBlock::names(data, {"bnd", "raw", "BsG_S11"}, index.flavor,
                                      filter.front()->block);
        QCOMPARE(names, (SequenceName::Set{{"bnd", "raw", "BsG_S11", {"pm1"}}}));
#endif
    }
};

QTEST_APPLESS_MAIN(TestReadBlock)

#include "readblock.moc"
