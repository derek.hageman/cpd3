/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <thread>
#include <future>
#include <QTest>
#include <QTemporaryFile>

#include "datacore/archive/access.hxx"
#include "core/waitutils.hxx"

#include "common.cxx"

class TestAccess : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    Database::Storage archiveStorage() const
    {
        return Database::Storage::sqlite(databaseFile.fileName().toUtf8().constData());
    }

private slots:

    void initTestCase()
    {
        QVERIFY(databaseFile.open());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void empty()
    {
        {
            Access a(archiveStorage());

            a.signalTerminate();
            a.waitForLocks();
        }

        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        Structure::Layout layout(&db);
    }

    void locks()
    {
        Access a(archiveStorage());

        {
            Access::ReadLock l1(a);
        }
        {
            Access::ReadLock l1(a);
            Access::ReadLock l2(a);
            l1.release();
        }
        {
            Access::WriteLock l1(a);
            {
                Access::ReadLock l2(a);
                Access::WriteLock l3(a, false);
                QVERIFY(l3.commit());
                l2.release();
            }
            QVERIFY(l1.commit());
        }

        a.waitForLocks();
    }

    void blockingLocks()
    {
        Access a(archiveStorage());

        {
            std::promise<void> p1;
            auto f1 = p1.get_future();
            auto t1 = std::thread([&] {
                Access::WriteLock l1(a);
                p1.set_value();
                Access::ReadLock l2(a);
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                l2.release();
                l1.commit();
            });
            f1.get();

            std::promise<void> p2;
            auto f2 = p2.get_future();
            std::promise<void> p3;
            auto f3 = p3.get_future();
            auto t2 = std::thread([&] {
                p2.set_value();
                Access::WriteLock l3(a);
                p3.set_value();
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                l3.abort();
            });

            f2.get();
            QVERIFY(!a.waitForLocks(0.0));
            QVERIFY(f3.wait_for(std::chrono::milliseconds(10)) == std::future_status::timeout);

            f3.get();
            QVERIFY(!a.waitForLocks(0.0));
            t2.join();
            QVERIFY(a.waitForLocks(0.0));
            t1.join();
        }

        {
            std::promise<void> p1;
            auto f1 = p1.get_future();
            auto t1 = std::thread([&] {
                Access::ReadLock l1(a);
                p1.set_value();
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            });
            f1.get();

            std::promise<void> p2;
            auto f2 = p2.get_future();
            std::promise<void> p3;
            auto f3 = p3.get_future();
            auto t2 = std::thread([&] {
                p2.set_value();
                Access::ReadLock l2(a, true, true);
                p3.set_value();
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            });

            f2.get();
            QVERIFY(!a.waitForLocks(0.0));
            QVERIFY(f3.wait_for(std::chrono::milliseconds(10)) == std::future_status::timeout);

            QVERIFY(a.waitForLocks());
            QVERIFY(f3.wait_for(std::chrono::milliseconds(1)) == std::future_status::ready);
            f3.get();

            t1.join();
            t2.join();
        }
    }

    void writeSynchronousAddSequence()
    {
        Access a(archiveStorage());

        QVERIFY(a.writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                              1000.0, 0),
                SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 1000, 2000.0, 0),}));

        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction(Database::Connection::Transaction_ReadOnly);
        Structure::Layout layout(&db);
        IdentityIndex index(&db);

        QVERIFY(checkSequence(layout, index, -1,
                              {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0)}));
        QVERIFY(checkSequence(layout, index, 0,
                              {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 1000,
                                             2000.0,
                                             0)}));
    }

    void writeSynchronousAddArchive()
    {
        Access a(archiveStorage());

        QVERIFY(a.writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(), 1000.0,
                             0,
                             500.0, true),
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 1000, 2000.0, 0, 501.0,
                             false),}));

        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction(Database::Connection::Transaction_ReadOnly);
        Structure::Layout layout(&db);
        IdentityIndex index(&db);

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(),
                                           1000.0, 0, 500.0, true)}));
        QVERIFY(checkSequence(layout, index, 0,
                              {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 1000,
                                            2000.0, 0,
                                            501.0, false)}));
    }

    void writeSynchronousAddRemoveSequence()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);
                Structure::Layout layout(&db);

                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(),
                                             1001.0, 0, 501.0, true),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),});
                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        QVERIFY(a.writeSynchronous(SequenceValue::Transfer{
                                           SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.1), FP::undefined(),
                                                         1000.0, 0),
                                           SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.1), 1000, 2000.0, 0),},
                                   ArchiveErasure::Transfer{
                                           ArchiveErasure({"bnd", "raw", "BsG_S11"},
                                                          FP::undefined(), 1001.0, 0, 600.0),
                                           ArchiveErasure({"bnd", "raw", "BsG_S11", {"pm1"}},
                                                          1001.0, 2000.0, 0, 601.0),}));

        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction(Database::Connection::Transaction_ReadOnly);
        Structure::Layout layout(&db);
        IdentityIndex index(&db);

        QVERIFY(checkSequence(layout, index, -1,
                              {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.1),
                                             FP::undefined(),
                                             1000.0, 0)}));
        QVERIFY(checkSequence(layout, index, 0,
                              {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.1), 1000,
                                             2000.0,
                                             0)}));
        QVERIFY(checkErasure(layout, index, -1,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 1001.0, 0,
                                             600.0)}));
    }

    void writeSynchronousAddRemoveSequenceAlternate()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);
                Structure::Layout layout(&db);

                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(),
                                             1001.0, 0, 501.0, true),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),});
                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        QVERIFY(a.writeSynchronous(SequenceValue::Transfer{
                                           SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.1), FP::undefined(),
                                                         1000.0, 0),
                                           SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.1), 1000, 2000.0, 0),},
                                   SequenceValue::Transfer{SequenceValue({"bnd", "raw", "BsG_S11"},
                                                                         Variant::Root(2.0),
                                                                         FP::undefined(), 1001.0, 0),
                                                           SequenceValue({"bnd", "raw", "BsG_S11", {"pm1"}},
                                                                         Variant::Root(4.0), 1001.0,
                                                                         2000.0, 0),}));

        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction(Database::Connection::Transaction_ReadOnly);
        Structure::Layout layout(&db);
        IdentityIndex index(&db);

        QVERIFY(checkSequence(layout, index, -1,
                              {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.1),
                                             FP::undefined(),
                                             1000.0, 0)}));
        QVERIFY(checkSequence(layout, index, 0,
                              {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.1), 1000,
                                             2000.0,
                                             0)}));
    }

    void writeSynchronousAddRemoveArchive()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);
                Structure::Layout layout(&db);

                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(),
                                             1001.0, 0, 501.0, true),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),});
                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        QVERIFY(a.writeSynchronous(ArchiveValue::Transfer{
                                           ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.1), FP::undefined(), 1000.0,
                                                        0,
                                                        600.0, false),
                                           ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.1), 1000, 2000.0, 0, 601.0,
                                                        true),},
                                   ArchiveErasure::Transfer{
                                           ArchiveErasure({"bnd", "raw", "BsG_S11"},
                                                          FP::undefined(), 1001.0, 0, 602.0),
                                           ArchiveErasure({"bnd", "raw", "BsG_S11", {"pm1"}},
                                                          1001.0, 2000.0, 0, 603.0),}));

        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction(Database::Connection::Transaction_ReadOnly);
        Structure::Layout layout(&db);
        IdentityIndex index(&db);

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.1),
                                           FP::undefined(),
                                           1000.0, 0, 600.0, false)}));
        QVERIFY(checkArchive(layout, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.1), 1000,
                                           2000.0, 0,
                                           601.0, true)}));
        QVERIFY(checkErasure(layout, index, -1,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 1001.0, 0,
                                             602.0)}));
    }

    void writeSynchronousRemoveArchive()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);
                Structure::Layout layout(&db);

                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(),
                                             1001.0, 0, 501.0, true),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),});
                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        QVERIFY(a.writeSynchronous(ArchiveErasure::Transfer{
                ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 1001.0, 0, 600.0),
                ArchiveErasure({"bnd", "raw", "BsG_S11", {"pm1"}}, 1001.0, 2000.0, 0, 601.0),}));

        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction(Database::Connection::Transaction_ReadOnly);
        Structure::Layout layout(&db);
        IdentityIndex index(&db);

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(),
                                           1000.0, 0, 500.0, false)}));
        QVERIFY(checkArchive(layout, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                           2000.0, 0,
                                           600.0, false)}));
        QVERIFY(checkErasure(layout, index, -1,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, FP::undefined(), 1001.0, 0,
                                             600.0)}));
    }

    void writeSynchronousRemoveIdentity()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);
                Structure::Layout layout(&db);

                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(),
                                             1001.0, 0, 501.0, false),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),});
                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        QVERIFY(a.removeSynchronous(SequenceIdentity::Transfer{
                SequenceIdentity({"bnd", "raw", "BsG_S11"}, FP::undefined(), 1001.0, 0),
                SequenceIdentity({"bnd", "raw", "BsG_S11", {"pm1"}}, 1001.0, 2000.0,
                                 0),}));

        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction(Database::Connection::Transaction_ReadOnly);
        Structure::Layout layout(&db);
        IdentityIndex index(&db);

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(),
                                           1000.0, 0, 500.0, false)}));
        QVERIFY(checkArchive(layout, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                           2000.0, 0,
                                           600.0, false)}));
    }

    void readSynchronous()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);
                Structure::Layout layout(&db);

                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(),
                                             1001.0, 0, 501.0, true),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),});
                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        QVERIFY(compareSequence(a.readSynchronous(Selection()),
                                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                               FP::undefined(), 1000.0, 0),
                                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                               FP::undefined(), 1001.0, 0),
                                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0),
                                               1000.0,
                                               2000.0, 0),
                                 SequenceValue({"bnd", "raw", "BsG_S11", {"pm1"}},
                                               Variant::Root(4.0),
                                               1001.0, 2000.0, 0)}, true));
    }

    void readSynchronousArchive()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);
                Structure::Layout layout(&db);

                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(),
                                             1001.0, 0, 501.0, true),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),});
                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        QVERIFY(compareArchive(a.readSynchronousArchive(Selection()),
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(),
                                             1001.0, 0, 501.0, true),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),}, true));
    }

    void externalconverter()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);
                Structure::Layout layout(&db);

                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(),
                                             1001.0, 0, 501.0, true),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),});
                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        {
            StreamSink::Buffer result;

            {
                auto ctx = a.readStream(Selection());
                QVERIFY(!ctx->wait(0.050));
                QVERIFY(!ctx->isComplete());
                ctx->setEgress(&result);
                QVERIFY(ctx->wait());
                QVERIFY(ctx->isComplete());
            }

            QVERIFY(result.ended());
            QVERIFY(compareSequence(result.values(),
                                    {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                                   FP::undefined(), 1000.0, 0),
                                     SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                                   FP::undefined(), 1001.0, 0),
                                     SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0),
                                                   1000.0,
                                                   2000.0, 0),
                                     SequenceValue({"bnd", "raw", "BsG_S11", {"pm1"}},
                                                   Variant::Root(4.0),
                                                   1001.0, 2000.0, 0)}, true));
        }

        {
            StreamSink::Iterator result;

            a.readStream(Selection(), &result)->detach();

            QVERIFY(result.wait());
            SequenceValue::Transfer check;
            QVERIFY(result.all(check));
            QVERIFY(compareSequence(check,
                                    {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                                   FP::undefined(), 1000.0, 0),
                                     SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                                   FP::undefined(), 1001.0, 0),
                                     SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0),
                                                   1000.0, 2000.0, 0),
                                     SequenceValue({"bnd", "raw", "BsG_S11", {"pm1"}},
                                                   Variant::Root(4.0), 1001.0, 2000.0, 0)}, true));
        }
    }

    void archiveSource()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);
                Structure::Layout layout(&db);

                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(),
                                             1001.0, 0, 501.0, true),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),});
                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        {
            ArchiveSink::Buffer result;

            {
                auto ctx = a.readArchive(Selection());
                QVERIFY(!ctx->wait(0.050));
                QVERIFY(!ctx->isComplete());
                ctx->setEgress(&result);
                QVERIFY(ctx->wait());
                QVERIFY(ctx->isComplete());
            }

            QVERIFY(result.ended());
            QVERIFY(compareArchive(result.values(),
                                   {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                                 FP::undefined(), 1000.0, 0, 500.0, false),
                                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                                 FP::undefined(), 1001.0, 0, 501.0, true),
                                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0),
                                                 1000.0,
                                                 2000.0, 0, 600.0, false),
                                    ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}},
                                                 Variant::Root(4.0),
                                                 1001.0, 2000.0, 0, 601.0, false),}, true));
        }

        {
            ArchiveSink::Iterator result;

            a.readArchive(Selection(), &result)->detach();

            QVERIFY(a.waitForLocks());
            QVERIFY(result.wait(0.0));
            ArchiveValue::Transfer check;
            QVERIFY(result.all(check));
            QVERIFY(compareArchive(check,
                                   {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                                 FP::undefined(), 1000.0, 0, 500.0, false),
                                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                                 FP::undefined(), 1001.0, 0, 501.0, true),
                                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0),
                                                 1000.0, 2000.0, 0, 600.0, false),
                                    ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}},
                                                 Variant::Root(4.0), 1001.0, 2000.0, 0, 601.0,
                                                 false),}, true));
        }
    }

    void erasureSource()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);
                Structure::Layout layout(&db);

                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(),
                                             1001.0, 0, 501.0, true),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),});
                erasureInsert(&db, layout, index, 0,
                              {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1002.0, 2000.0, 0,
                                              700.0)});

                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        {
            ErasureSink::Buffer result;

            {
                auto ctx = a.readErasure(Selection());
                QVERIFY(!ctx->wait(0.050));
                QVERIFY(!ctx->isComplete());
                ctx->setEgress(&result);
                QVERIFY(ctx->wait());
                QVERIFY(ctx->isComplete());
            }

            QVERIFY(result.ended());
            QVERIFY(compareArchive(result.archiveValues(),
                                   {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                                 FP::undefined(), 1000.0, 0, 500.0, false),
                                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                                 FP::undefined(), 1001.0, 0, 501.0, true),
                                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0),
                                                 1000.0,
                                                 2000.0, 0, 600.0, false),
                                    ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}},
                                                 Variant::Root(4.0),
                                                 1001.0, 2000.0, 0, 601.0, false),}, true));
            QVERIFY(compareErasure(result.erasureValues(),
                                   {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1002.0, 2000.0, 0,
                                                   700.0)}, true));
        }

        {
            ErasureSink::Iterator result;

            a.readErasure(Selection(), &result)->detach();

            QVERIFY(result.wait());
            ArchiveValue::Transfer archive;
            ArchiveErasure::Transfer erasure;
            QVERIFY(result.all(archive, erasure));
            QVERIFY(compareArchive(archive,
                                   {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                                 FP::undefined(), 1000.0, 0, 500.0, false),
                                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                                 FP::undefined(), 1001.0, 0, 501.0, true),
                                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0),
                                                 1000.0, 2000.0, 0, 600.0, false),
                                    ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}},
                                                 Variant::Root(4.0), 1001.0, 2000.0, 0, 601.0,
                                                 false),}, true));
            QVERIFY(compareErasure(erasure,
                                   {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1002.0, 2000.0, 0,
                                                   700.0)}, true));
        }
    }

    void writeReplace()
    {
        Access a(archiveStorage());

        auto op = a.writeReplace();
        op->incomingData(
                SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                              1000.0, 0));
        op->incomingData(
                SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 1000, 2000.0, 0));

        QVERIFY(!op->wait(0.050));

        op->endData();

        QVERIFY(op->wait());


        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction(Database::Connection::Transaction_ReadOnly);
        Structure::Layout layout(&db);
        IdentityIndex index(&db);

        QVERIFY(checkSequence(layout, index, -1,
                              {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0)}));
        QVERIFY(checkSequence(layout, index, 0,
                              {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 1000,
                                             2000.0,
                                             0)}));
    }

    void writeSynchronize()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);
                Structure::Layout layout(&db);

                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(),
                                             1001.0, 0, 501.0, true),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),});
                erasureInsert(&db, layout, index, 0,
                              {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1002.0, 2000.0, 0, 700.0),
                               ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1003.0, 2000.0, 0,
                                              701.0),});
                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        {
            auto op = a.writeSynchronize();
            op->incomingValue(
                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.1), FP::undefined(),
                                 1000.0, 0,
                                 5000.0, true));
            op->incomingValue(ArchiveValue::Transfer{
                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.1), FP::undefined(),
                                 1001.0, 0,
                                 400.0, true)});

            op->incomingErasure(
                    ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1002.0, 2000.0, 0, 400.0));
            op->incomingErasure(ArchiveErasure::Transfer{
                    ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1003.0, 2000.0, 0, 7001.0)});

            op->incomingValue(
                    ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.1), 1004.0, 2000.0, 0,
                                 800.0,
                                 true));

            QTest::qSleep(100);

            op->endStream();
        }

        a.waitForLocks();


        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction(Database::Connection::Transaction_ReadOnly);
        Structure::Layout layout(&db);
        IdentityIndex index(&db);

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.1),
                                           FP::undefined(),
                                           1000.0, 0, 5000.0, true),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(),
                                           1001.0, 0, 501.0, true)}));
        QVERIFY(checkArchive(layout, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                           2000.0, 0,
                                           600.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                           1001.0,
                                           2000.0, 0, 601.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.1), 1004.0,
                                           2000.0, 0,
                                           800.0, true)}));
        QVERIFY(checkErasure(layout, index, 0,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1002.0, 2000.0, 0, 700.0),
                              ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1003.0, 2000.0, 0,
                                             7001.0),}));
    }

    void writeRemoteReferenced()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);
                Structure::Layout layout(&db);

                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(),
                                             1001.0, 0, 501.0, true),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),
                                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),});
                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        auto op = a.writeRemoteReferenced();
        op->incomingValue(ArchiveValue::Transfer{
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(), 1000.0,
                             0,
                             500.0, false),
                ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(), 1001.0,
                             0,
                             501.0, true),});
        op->incomingData(
                ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0), 1001.0, 2000.0,
                             0,
                             601.0, false));

        QVERIFY(!op->wait(0.050));

        QVERIFY(a.waitForLocks());

        op->endData();

        QVERIFY(op->wait());

        QVERIFY(a.waitForLocks());


        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction(Database::Connection::Transaction_ReadOnly);
        Structure::Layout layout(&db);
        IdentityIndex index(&db);

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(),
                                           1000.0, 0, 500.0, true),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(),
                                           1001.0, 0, 501.0, true),}));
        QVERIFY(checkArchive(layout, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                           2000.0, 0,
                                           600.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                           1001.0,
                                           2000.0, 0, 601.0, true),}));
    }

    void availableAll()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);

                index.station.insert("bnd");
                index.station.insert("thd");
                index.station.insert("_");
                index.archive.insert("raw");
                index.archive.insert("clean");
                index.archive.insert("clean_meta");
                index.variable.insert("BsG_S11");
                index.variable.insert("BsB_S11");
                index.flavor.insert("");
                index.flavor.insert("pm1");
                index.flavor.insert("pm1 cover");

                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        QCOMPARE(a.availableStations(), (SequenceName::ComponentSet{"bnd", "thd"}));
        QCOMPARE(a.availableStations({".*"}), (SequenceName::ComponentSet{"bnd", "thd", "_"}));
        QCOMPARE(a.availableArchives(), (SequenceName::ComponentSet{"raw", "clean", "clean_meta"}));
        QCOMPARE(a.availableArchives({"raw", "clean"}),
                 (SequenceName::ComponentSet{"raw", "clean"}));
        QCOMPARE(a.availableVariables(), (SequenceName::ComponentSet{"BsG_S11", "BsB_S11"}));
        QCOMPARE(a.availableVariables({"Bs[Gb]_S11"}), (SequenceName::ComponentSet{"BsG_S11"}));
        {
            Access::ReadLock l(a);
            QCOMPARE(a.availableFlavors(), (SequenceName::ComponentSet{"pm1", "cover"}));
            QCOMPARE(a.availableFlavors({"PM10?"}), (SequenceName::ComponentSet{"pm1"}));
        }
        {
            auto r = a.availableSelected();
            QCOMPARE(r.stations, (SequenceName::ComponentSet{"bnd", "thd"}));
            QCOMPARE(r.archives, (SequenceName::ComponentSet{"raw", "clean", "clean_meta"}));
            QCOMPARE(r.variables, (SequenceName::ComponentSet{"BsG_S11", "BsB_S11"}));
            QCOMPARE(r.flavors, (SequenceName::ComponentSet{"pm1", "cover"}));
        }

        {
            auto f1 = a.availableStationsFuture();
            auto f2 = a.availableStationsFuture({".*"});
            QCOMPARE(f1.get(), (SequenceName::ComponentSet{"bnd", "thd"}));
            QCOMPARE(f2.get(), (SequenceName::ComponentSet{"bnd", "thd", "_"}));
        }
        {
            auto f1 = a.availableArchivesFuture();
            auto f2 = a.availableArchivesFuture({"raw", "clean"});
            QCOMPARE(f1.get(), (SequenceName::ComponentSet{"raw", "clean", "clean_meta"}));
            QCOMPARE(f2.get(), (SequenceName::ComponentSet{"raw", "clean"}));
        }
        {
            auto f2 = a.availableVariablesFuture({"Bs[Gb]_S11"});
            auto f1 = a.availableVariablesFuture();
            QCOMPARE(f1.get(), (SequenceName::ComponentSet{"BsG_S11", "BsB_S11"}));
            QCOMPARE(f2.get(), (SequenceName::ComponentSet{"BsG_S11"}));
        }
        {
            Access::ReadLock l(a);
            auto f1 = a.availableFlavorsFuture();
            auto f2 = a.availableFlavorsFuture({"PM10?"});
            l.release();
            QCOMPARE(f1.get(), (SequenceName::ComponentSet{"pm1", "cover"}));
            QCOMPARE(f2.get(), (SequenceName::ComponentSet{"pm1"}));
        }
        {
            auto f = a.availableSelectedFuture();
            auto r = f.get();
            QCOMPARE(r.stations, (SequenceName::ComponentSet{"bnd", "thd"}));
            QCOMPARE(r.archives, (SequenceName::ComponentSet{"raw", "clean", "clean_meta"}));
            QCOMPARE(r.variables, (SequenceName::ComponentSet{"BsG_S11", "BsB_S11"}));
            QCOMPARE(r.flavors, (SequenceName::ComponentSet{"pm1", "cover"}));
        }
    }

    void availableSelection()
    {
        {
            Database::Connection db(archiveStorage());
            QVERIFY(db.start());
            QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
            for (;;) {
                auto trans = db.transaction();
                IdentityIndex::initialize(&db);
                IdentityIndex index(&db);
                Structure::Layout layout(&db);

                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                             FP::undefined(),
                                             1000.0, 0, 500.0, false),});
                sequenceInsert(&db, layout, index, -1,
                               {ArchiveValue({"bnd", "clean", "BsG_S11"}, Variant::Root(2.0),
                                             FP::undefined(), 1001.0, 0, 501.0, true),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"bnd", "raw", "BsB_S11"}, Variant::Root(3.0), 1000.0,
                                             2000.0,
                                             0, 600.0, false),});
                sequenceInsert(&db, layout, index, 0,
                               {ArchiveValue({"thd", "raw", "BsG_S11", {"pm1"}}, Variant::Root(4.0),
                                             1001.0,
                                             2000.0, 0, 601.0, false),});
                if (trans.end())
                    break;
            }
        }

        Access a(archiveStorage());

        QCOMPARE(a.availableNames(Selection()), (SequenceName::Set{{"bnd", "raw",   "BsG_S11"},
                                                                   {"bnd", "clean", "BsG_S11"},
                                                                   {"bnd", "raw",   "BsB_S11"},
                                                                   {"thd", "raw",   "BsG_S11"},}));
        QCOMPARE(a.availableNames(Selection(), Archive::Access::IndexOnly),
                 (SequenceName::Set{{"bnd", "raw",   "BsG_S11"},
                                    {"bnd", "clean", "BsG_S11"},
                                    {"bnd", "raw",   "BsB_S11"},
                                    {"thd", "raw",   "BsG_S11"},}));

        QVERIFY(a.availableExists(Selection(SequenceName{"bnd", "raw", "BsG_S11"})));
        QVERIFY(a.availableExists(Selection(SequenceName{"bnd", "raw", "BsG_S11"}),
                                  Archive::Access::IndexOnly));
        QVERIFY(!a.availableExists(Selection(SequenceName{"thd", "clean", "BsG_S11"})));

        {
            auto f = a.availableNamesFuture(Selection(), Archive::Access::ReadAll);
            QCOMPARE(f.get(), (SequenceName::Set{{"bnd", "raw",   "BsG_S11"},
                                                 {"bnd", "clean", "BsG_S11"},
                                                 {"bnd", "raw",   "BsB_S11"},
                                                 {"thd", "raw",   "BsG_S11", {"pm1"}},}));
        }

        {
            auto r = a.availableSelected(Selection());
            QCOMPARE(r.stations, (SequenceName::ComponentSet{"bnd", "thd"}));
            QCOMPARE(r.archives, (SequenceName::ComponentSet{"raw", "clean"}));
            QCOMPARE(r.variables, (SequenceName::ComponentSet{"BsG_S11", "BsB_S11"}));
        }

        {
            Access::ReadLock l(a);
            auto f = a.availableSelectedFuture(Selection(), Archive::Access::ReadAll);
            l.release();
            auto r = f.get();
            QCOMPARE(r.stations, (SequenceName::ComponentSet{"bnd", "thd"}));
            QCOMPARE(r.archives, (SequenceName::ComponentSet{"raw", "clean"}));
            QCOMPARE(r.variables, (SequenceName::ComponentSet{"BsG_S11", "BsB_S11"}));
            QCOMPARE(r.flavors, (SequenceName::ComponentSet{"pm1"}));
        }

        {
            auto f = a.availableExistsFuture(
                    Selection(SequenceName{"thd", "raw", "BsG_S11", {"pm1"}}),
                    Archive::Access::ReadAll);
            auto r = f.get();
            QVERIFY(r);
        }
    }
};

QTEST_APPLESS_MAIN(TestAccess)

#include "access.moc"
