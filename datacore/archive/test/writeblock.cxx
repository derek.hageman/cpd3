/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QTest>
#include <QTemporaryFile>

#include "datacore/archive/writeblock.hxx"

#include "common.cxx"

class TestWriteBlock : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    Database::Storage archiveStorage() const
    {
        return Database::Storage::sqlite(databaseFile.fileName().toUtf8().constData());
    }

private slots:

    void initTestCase()
    {
        QVERIFY(databaseFile.open());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void basicSingle()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);

        SequenceName baseName("bnd", "raw", "BsG_S11");

        QByteArray sequenceData = constructSequence(index.flavor,
                                                    {ArchiveValue(baseName, Variant::Root(1.0),
                                                                  1000.0,
                                                                  2000.0, 0, 500.0, false),
                                                     ArchiveValue(baseName, Variant::Root(2.0),
                                                                  1000.0,
                                                                  2000.0, 1, 501.0, true),
                                                     ArchiveValue(baseName, Variant::Root(3.0),
                                                                  1001.0,
                                                                  2000.0, 0, 502.0, false),
                                                     ArchiveValue(baseName.withFlavor("pm1"),
                                                                  Variant::Root(3.0), 1001.0,
                                                                  2000.0, 0,
                                                                  502.0, false)});
        QByteArray erasureData = constructErasure(index.flavor,
                                                  {ArchiveErasure(baseName, 2000.0, 3000.0, 0,
                                                                  600.0),
                                                   ArchiveErasure(baseName, 2000.0, 3000.0, 1,
                                                                  601.0),
                                                   ArchiveErasure(baseName, 2001.0, 3000.0, 0,
                                                                  603.0)});

        WriteBlock::Context context(index.flavor);

        context.add(ArchiveValue(baseName, Variant::Root(1.1), 1000.0, 2000.0, 0, 5000.0, false));
        context.remove(ArchiveErasure(baseName, 1000.0, 2000.0, 0, 4000.0));
        context.add(ArchiveValue(baseName.withFlavor("pm10"), Variant::Root(4.0), 1000.0, 2000.0, 0,
                                 5001.0,
                                 false));
        context.add(ArchiveValue(baseName, Variant::Root(5.0), 2000.0, 3000.0, 1, 5002.0, false));
        context.remove(ArchiveErasure(baseName, 1000.0, 2000.0, 1, 6001.0));
        context.remove(ArchiveErasure(baseName, 1001.0, 2000.0, 0, 6002.0));
        context.remove(ArchiveErasure(baseName, 2001.0, 3000.0, 0, 6003.0));


        WriteBlock::Sequence sequence(sequenceData);
        WriteBlock::Erasure erasure(erasureData);
        context.writeStageOne();
        erasure.writeStageTwo(context);
        sequence.writeStageThree(context);
        context.writeStageFour();
        erasure.writeStageFive(context);
        sequence.inject(context);
        erasure.inject(context);

        QVERIFY(sequence.isModified());
        QVERIFY(erasure.isModified());

        auto sequenceResult = readSequence(index.flavor, baseName, sequence.outputData());
        auto erasureResult = readErasure(index.flavor, baseName, erasure.outputData());


        QVERIFY(compareArchive(sequenceResult,
                               {ArchiveValue(baseName, Variant::Root(1.1), 1000.0, 2000.0, 0,
                                             5000.0,
                                             false),
                                ArchiveValue(baseName.withFlavor("pm1"), Variant::Root(3.0), 1001.0,
                                             2000.0,
                                             0, 502.0, false),
                                ArchiveValue(baseName.withFlavor("pm10"), Variant::Root(4.0),
                                             1000.0,
                                             2000.0, 0, 5001.0, false),
                                ArchiveValue(baseName, Variant::Root(5.0), 2000.0, 3000.0, 1,
                                             5002.0,
                                             true)}));

        QVERIFY(compareErasure(erasureResult, {ArchiveErasure(baseName, 2000.0, 3000.0, 0, 600.0),
                                               ArchiveErasure(baseName, 1000.0, 2000.0, 1, 6001.0),
                                               ArchiveErasure(baseName, 2001.0, 3000.0, 0,
                                                              6003.0)}));

        QCOMPARE(sequence.latestModified(), 5002.0);
        QCOMPARE(erasure.latestModified(), 6003.0);
    }

    void basicGlobal()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);

        SequenceName baseName("bnd", "raw", "BsG_S11");

        QByteArray sequenceData1 = constructSequence(index.flavor,
                                                     {ArchiveValue(baseName, Variant::Root(1.0),
                                                                   1000.0,
                                                                   2000.0, 0, 500.0, false),
                                                      ArchiveValue(baseName, Variant::Root(2.0),
                                                                   1000.0,
                                                                   2000.0, 1, 501.0, true)});
        QByteArray sequenceData2 = constructSequence(index.flavor,
                                                     {ArchiveValue(baseName, Variant::Root(3.0),
                                                                   1001.0,
                                                                   2000.0, 0, 502.0, false),
                                                      ArchiveValue(baseName.withFlavor("pm1"),
                                                                   Variant::Root(3.0), 1001.0,
                                                                   2000.0, 0,
                                                                   502.0, false)});

        QByteArray erasureData1 = constructErasure(index.flavor,
                                                   {ArchiveErasure(baseName, 2000.0, 3000.0, 0,
                                                                   600.0),
                                                    ArchiveErasure(baseName, 2000.0, 3000.0, 1,
                                                                   601.0)});
        QByteArray erasureData2 = constructErasure(index.flavor,
                                                   {ArchiveErasure(baseName, 2001.0, 3000.0, 0,
                                                                   603.0)});

        WriteBlock::Context context(index.flavor);

        context.add(ArchiveValue(baseName, Variant::Root(1.1), 1000.0, 2000.0, 0, 5000.0, false));
        context.remove(ArchiveErasure(baseName, 1000.0, 2000.0, 0, 4000.0));
        context.add(ArchiveValue(baseName.withFlavor("pm10"), Variant::Root(4.0), 1000.0, 2000.0, 0,
                                 5001.0,
                                 false));
        context.add(ArchiveValue(baseName, Variant::Root(5.0), 2000.0, 3000.0, 1, 5002.0, false));
        context.remove(ArchiveErasure(baseName, 1000.0, 2000.0, 1, 6001.0));
        context.remove(ArchiveErasure(baseName, 1001.0, 2000.0, 0, 6002.0));
        context.remove(ArchiveErasure(baseName, 2001.0, 3000.0, 0, 6003.0));


        WriteBlock::Sequence sequence1(sequenceData1);
        WriteBlock::Erasure erasure1(erasureData1);
        WriteBlock::Sequence sequence2(sequenceData2);
        WriteBlock::Erasure erasure2(erasureData2);
        context.writeStageOne();
        erasure1.writeStageTwo(context);
        erasure2.writeStageTwo(context);
        sequence1.writeStageThree(context);
        sequence2.writeStageThree(context);
        context.writeStageFour();
        erasure1.writeStageFive(context);
        erasure2.writeStageFive(context);
        sequence2.inject(context);
        erasure2.inject(context);

        QVERIFY(sequence2.isModified());
        QVERIFY(erasure2.isModified());
        QVERIFY(sequence1.isModified());
        QVERIFY(erasure1.isModified());


        auto sequenceResult = readSequence(index.flavor, baseName, sequence1.outputData());
        auto erasureResult = readErasure(index.flavor, baseName, erasure1.outputData());

        QVERIFY(compareArchive(sequenceResult, {}));
        QVERIFY(sequence1.outputData().empty());

        QVERIFY(compareErasure(erasureResult,
                               {ArchiveErasure(baseName, 2000.0, 3000.0, 0, 600.0)}));


        sequenceResult = readSequence(index.flavor, baseName, sequence2.outputData());
        erasureResult = readErasure(index.flavor, baseName, erasure2.outputData());

        QVERIFY(compareArchive(sequenceResult,
                               {ArchiveValue(baseName, Variant::Root(1.1), 1000.0, 2000.0, 0,
                                             5000.0,
                                             false),
                                ArchiveValue(baseName.withFlavor("pm1"), Variant::Root(3.0), 1001.0,
                                             2000.0,
                                             0, 502.0, false),
                                ArchiveValue(baseName.withFlavor("pm10"), Variant::Root(4.0),
                                             1000.0,
                                             2000.0, 0, 5001.0, false),
                                ArchiveValue(baseName, Variant::Root(5.0), 2000.0, 3000.0, 1,
                                             5002.0,
                                             true)}));

        QVERIFY(compareErasure(erasureResult, {ArchiveErasure(baseName, 1000.0, 2000.0, 1, 6001.0),
                                               ArchiveErasure(baseName, 2001.0, 3000.0, 0,
                                                              6003.0)}));
    }

    void basicNOOP()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);

        SequenceName baseName("bnd", "raw", "BsG_S11");

        QByteArray sequenceData = constructSequence(index.flavor,
                                                    {ArchiveValue(baseName, Variant::Root(1.0),
                                                                  1000.0,
                                                                  2000.0, 0, 500.0, false)});
        QByteArray erasureData = constructErasure(index.flavor,
                                                  {ArchiveErasure(baseName, 2000.0, 3000.0, 0,
                                                                  600.0)});

        WriteBlock::Context context(index.flavor);

        context.remove(ArchiveErasure(baseName, 1000.0, 2000.0, 1, 4000.0));

        WriteBlock::Sequence sequence(sequenceData);
        WriteBlock::Erasure erasure(erasureData);
        context.writeStageOne();
        erasure.writeStageTwo(context);
        sequence.writeStageThree(context);
        context.writeStageFour();
        erasure.writeStageFive(context);
        sequence.inject(context);
        erasure.inject(context);

        QVERIFY(!sequence.isModified());
        QVERIFY(!erasure.isModified());
    }

    void basicEmptyErasure()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);

        SequenceName baseName("bnd", "raw", "BsG_S11");

        QByteArray sequenceData = constructSequence(index.flavor,
                                                    {ArchiveValue(baseName, Variant::Root(1.0),
                                                                  1000.0,
                                                                  2000.0, 0, 500.0, false)});
        QByteArray erasureData = constructErasure(index.flavor,
                                                  {ArchiveErasure(baseName, 2000.0, 3000.0, 0,
                                                                  600.0)});

        WriteBlock::Context context(index.flavor);

        context.add(ArchiveValue(baseName, Variant::Root(2.0), 2000.0, 3000.0, 0, 5000.0, false));
        context.remove(ArchiveErasure(baseName, 1000.0, 2000.0, 0, 6000.0));

        WriteBlock::Sequence sequence(sequenceData);
        WriteBlock::Erasure erasure(erasureData);
        context.writeStageOne();
        erasure.writeStageTwo(context);
        sequence.writeStageThree(context);
        context.writeStageFour();
        erasure.writeStageFive(context);
        sequence.inject(context);
        erasure.inject(context);

        QVERIFY(sequence.isModified());
        QVERIFY(erasure.isModified());


        auto sequenceResult = readSequence(index.flavor, baseName, sequence.outputData());
        auto erasureResult = readErasure(index.flavor, baseName, erasure.outputData());

        QVERIFY(compareArchive(sequenceResult,
                               {ArchiveValue(baseName, Variant::Root(2.0), 2000.0, 3000.0, 0,
                                             5000.0,
                                             true)}));

        QVERIFY(compareErasure(erasureResult, {}));
        QVERIFY(erasure.outputData().empty());
    }

    void basicErasureNoModify()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);

        SequenceName baseName("bnd", "raw", "BsG_S11");

        QByteArray sequenceData = constructSequence(index.flavor,
                                                    {ArchiveValue(baseName, Variant::Root(1.0),
                                                                  1000.0,
                                                                  2000.0, 0, 500.0, false)});
        QByteArray erasureData = constructErasure(index.flavor,
                                                  {ArchiveErasure(baseName, 2000.0, 3000.0, 0,
                                                                  600.0)});

        WriteBlock::Context context(index.flavor);

        context.remove(SequenceIdentity(baseName, 2000.0, 3000.0, 0));

        WriteBlock::Sequence sequence(sequenceData);
        WriteBlock::Erasure erasure(erasureData);
        context.writeStageOne();
        erasure.writeStageTwo(context);
        sequence.writeStageThree(context);
        context.writeStageFour();
        erasure.writeStageFive(context);
        sequence.inject(context);
        erasure.inject(context);

        QVERIFY(!sequence.isModified());
        QVERIFY(!erasure.isModified());


        auto sequenceResult = readSequence(index.flavor, baseName, sequence.outputData());
        auto erasureResult = readErasure(index.flavor, baseName, erasure.outputData());

        QVERIFY(compareArchive(sequenceResult,
                               {ArchiveValue(baseName, Variant::Root(1.0), 1000.0, 2000.0, 0, 500.0,
                                             false)}));

        QVERIFY(compareErasure(erasureResult,
                               {ArchiveErasure(baseName, 2000.0, 3000.0, 0, 600.0)}));
    }

    void synchronizeBasic()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);

        SequenceName baseName("bnd", "raw", "BsG_S11");

        QByteArray sequenceData = constructSequence(index.flavor,
                                                    {ArchiveValue(baseName, Variant::Root(1.0),
                                                                  1000.0,
                                                                  2000.0, 0, 500.0, false),
                                                     ArchiveValue(baseName, Variant::Root(2.0),
                                                                  1000.0,
                                                                  2000.0, 1, 501.0, true),
                                                     ArchiveValue(baseName, Variant::Root(3.0),
                                                                  1001.0,
                                                                  2000.0, 0, 502.0, false),
                                                     ArchiveValue(baseName, Variant::Root(4.0),
                                                                  1001.0,
                                                                  2000.0, 1, 503.0, false),
                                                     ArchiveValue(baseName, Variant::Root(5.0),
                                                                  1002.0,
                                                                  2000.0, 0, 504.0, false)});
        QByteArray erasureData = constructErasure(index.flavor,
                                                  {ArchiveErasure(baseName, 2000.0, 3000.0, 0,
                                                                  600.0),
                                                   ArchiveErasure(baseName, 2000.0, 3000.0, 1,
                                                                  601.0),
                                                   ArchiveErasure(baseName, 2001.0, 3000.0, 0,
                                                                  602.0),
                                                   ArchiveErasure(baseName, 2001.0, 3000.0, 1,
                                                                  603.0),
                                                   ArchiveErasure(baseName, 2002.0, 3000.0, 0,
                                                                  604.0)});

        WriteBlock::Context context(index.flavor);

        context.add(ArchiveValue(baseName, Variant::Root(1.1), 2000.0, 3000.0, 0, 300.0, true));
        context.add(ArchiveValue(baseName, Variant::Root(2.1), 2000.0, 3000.0, 1, 5000.0, true));
        context.add(ArchiveValue(baseName, Variant::Root(3.1), 3001.0, 4000.0, 0, 5001.0, true));
        context.add(ArchiveValue(baseName, Variant::Root(4.1), 3002.0, 4000.0, 0, 5002.0, true));
        context.add(ArchiveValue(baseName, Variant::Root(5.1), 3003.0, 4000.0, 0, 301.0, true));
        context.add(ArchiveValue(baseName, Variant::Root(6.1), 1001.0, 2000.0, 0, 302.0, true));
        context.add(ArchiveValue(baseName, Variant::Root(7.1), 1001.0, 2000.0, 1, 5003.0, true));

        context.remove(ArchiveErasure(baseName, 1000.0, 2000.0, 0, 400.0));
        context.remove(ArchiveErasure(baseName, 1000.0, 2000.0, 1, 6000.0));
        context.remove(ArchiveErasure(baseName, 3002.0, 4000.0, 0, 401.0));
        context.remove(ArchiveErasure(baseName, 3003.0, 4000.0, 0, 6002.0));
        context.remove(ArchiveErasure(baseName, 2001.0, 3000.0, 0, 402.0));
        context.remove(ArchiveErasure(baseName, 2001.0, 3000.0, 1, 6003.0));


        WriteBlock::Sequence sequence(sequenceData);
        WriteBlock::Erasure erasure(erasureData);
        context.synchronizeStageOne();
        erasure.synchronizeStageTwo(context);
        sequence.synchronizeStageThree(context);
        sequence.inject(context);
        erasure.inject(context);

        QVERIFY(sequence.isModified());
        QVERIFY(erasure.isModified());

        auto sequenceResult = readSequence(index.flavor, baseName, sequence.outputData());
        auto erasureResult = readErasure(index.flavor, baseName, erasure.outputData());


        QVERIFY(compareArchive(sequenceResult,
                               {ArchiveValue(baseName, Variant::Root(1.0), 1000.0, 2000.0, 0, 500.0,
                                             true),
                                ArchiveValue(baseName, Variant::Root(3.0), 1001.0, 2000.0, 0, 502.0,
                                             true),
                                ArchiveValue(baseName, Variant::Root(5.0), 1002.0, 2000.0, 0, 504.0,
                                             false),
                                ArchiveValue(baseName, Variant::Root(2.1), 2000.0, 3000.0, 1,
                                             5000.0, true),
                                ArchiveValue(baseName, Variant::Root(3.1), 3001.0, 4000.0, 0,
                                             5001.0, true),
                                ArchiveValue(baseName, Variant::Root(4.1), 3002.0, 4000.0, 0,
                                             5002.0, true),
                                ArchiveValue(baseName, Variant::Root(7.1), 1001.0, 2000.0, 1,
                                             5003.0,
                                             true)}));

        QVERIFY(compareErasure(erasureResult, {ArchiveErasure(baseName, 2000.0, 3000.0, 0, 600.0),
                                               ArchiveErasure(baseName, 2001.0, 3000.0, 0, 602.0),
                                               ArchiveErasure(baseName, 2001.0, 3000.0, 1, 6003.0),
                                               ArchiveErasure(baseName, 2002.0, 3000.0, 0, 604.0),
                                               ArchiveErasure(baseName, 1000.0, 2000.0, 1, 6000.0),
                                               ArchiveErasure(baseName, 3003.0, 4000.0, 0,
                                                              6002.0)}));
    }

    void synchronizeGlobal()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);

        SequenceName baseName("bnd", "raw", "BsG_S11");

        QByteArray sequenceData1 = constructSequence(index.flavor,
                                                     {ArchiveValue(baseName, Variant::Root(1.0),
                                                                   1000.0,
                                                                   2000.0, 0, 500.0, false),
                                                      ArchiveValue(baseName, Variant::Root(2.0),
                                                                   1000.0,
                                                                   2000.0, 1, 501.0, true)});
        QByteArray sequenceData2 = constructSequence(index.flavor,
                                                     {ArchiveValue(baseName, Variant::Root(3.0),
                                                                   1001.0,
                                                                   2000.0, 0, 502.0, false),
                                                      ArchiveValue(baseName, Variant::Root(4.0),
                                                                   1001.0,
                                                                   2000.0, 1, 503.0, false),
                                                      ArchiveValue(baseName, Variant::Root(5.0),
                                                                   1002.0,
                                                                   2000.0, 0, 504.0, false)});
        QByteArray erasureData1 = constructErasure(index.flavor,
                                                   {ArchiveErasure(baseName, 2000.0, 3000.0, 0,
                                                                   600.0),
                                                    ArchiveErasure(baseName, 2000.0, 3000.0, 1,
                                                                   601.0),
                                                    ArchiveErasure(baseName, 2001.0, 3000.0, 0,
                                                                   602.0)});
        QByteArray erasureData2 = constructErasure(index.flavor,
                                                   {ArchiveErasure(baseName, 2001.0, 3000.0, 1,
                                                                   603.0),
                                                    ArchiveErasure(baseName, 2002.0, 3000.0, 0,
                                                                   604.0)});

        WriteBlock::Context context(index.flavor);

        context.add(ArchiveValue(baseName, Variant::Root(1.1), 2000.0, 3000.0, 0, 300.0, true));
        context.add(ArchiveValue(baseName, Variant::Root(2.1), 2000.0, 3000.0, 1, 5000.0, true));
        context.add(ArchiveValue(baseName, Variant::Root(3.1), 3001.0, 4000.0, 0, 5001.0, true));
        context.add(ArchiveValue(baseName, Variant::Root(4.1), 3002.0, 4000.0, 0, 5002.0, true));
        context.add(ArchiveValue(baseName, Variant::Root(5.1), 3003.0, 4000.0, 0, 301.0, true));
        context.add(ArchiveValue(baseName, Variant::Root(6.1), 1001.0, 2000.0, 0, 302.0, true));
        context.add(ArchiveValue(baseName, Variant::Root(7.1), 1001.0, 2000.0, 1, 5003.0, true));

        context.remove(ArchiveErasure(baseName, 1000.0, 2000.0, 0, 400.0));
        context.remove(ArchiveErasure(baseName, 1000.0, 2000.0, 1, 6000.0));
        context.remove(ArchiveErasure(baseName, 3002.0, 4000.0, 0, 401.0));
        context.remove(ArchiveErasure(baseName, 3003.0, 4000.0, 0, 6002.0));
        context.remove(ArchiveErasure(baseName, 2001.0, 3000.0, 0, 402.0));
        context.remove(ArchiveErasure(baseName, 2001.0, 3000.0, 1, 6003.0));


        WriteBlock::Sequence sequence1(sequenceData1);
        WriteBlock::Sequence sequence2(sequenceData2);
        WriteBlock::Erasure erasure1(erasureData1);
        WriteBlock::Erasure erasure2(erasureData2);
        context.synchronizeStageOne();
        erasure1.synchronizeStageTwo(context);
        erasure2.synchronizeStageTwo(context);
        sequence1.synchronizeStageThree(context);
        sequence2.synchronizeStageThree(context);
        sequence1.inject(context);
        erasure1.inject(context);

        QVERIFY(sequence1.isModified());
        QVERIFY(erasure1.isModified());
        QVERIFY(sequence2.isModified());
        QVERIFY(erasure2.isModified());


        auto sequenceResult = readSequence(index.flavor, baseName, sequence1.outputData());
        auto erasureResult = readErasure(index.flavor, baseName, erasure1.outputData());

        QVERIFY(compareArchive(sequenceResult,
                               {ArchiveValue(baseName, Variant::Root(1.0), 1000.0, 2000.0, 0, 500.0,
                                             true),
                                ArchiveValue(baseName, Variant::Root(2.1), 2000.0, 3000.0, 1,
                                             5000.0, true),
                                ArchiveValue(baseName, Variant::Root(3.1), 3001.0, 4000.0, 0,
                                             5001.0, true),
                                ArchiveValue(baseName, Variant::Root(4.1), 3002.0, 4000.0, 0,
                                             5002.0, true),
                                ArchiveValue(baseName, Variant::Root(7.1), 1001.0, 2000.0, 1,
                                             5003.0,
                                             true)}));

        QVERIFY(compareErasure(erasureResult, {ArchiveErasure(baseName, 2000.0, 3000.0, 0, 600.0),
                                               ArchiveErasure(baseName, 2001.0, 3000.0, 0, 602.0),
                                               ArchiveErasure(baseName, 1000.0, 2000.0, 1, 6000.0),
                                               ArchiveErasure(baseName, 3003.0, 4000.0, 0,
                                                              6002.0)}));


        sequenceResult = readSequence(index.flavor, baseName, sequence2.outputData());
        erasureResult = readErasure(index.flavor, baseName, erasure2.outputData());

        QVERIFY(compareArchive(sequenceResult,
                               {ArchiveValue(baseName, Variant::Root(3.0), 1001.0, 2000.0, 0, 502.0,
                                             true),
                                ArchiveValue(baseName, Variant::Root(5.0), 1002.0, 2000.0, 0, 504.0,
                                             false)}));

        QVERIFY(compareErasure(erasureResult, {ArchiveErasure(baseName, 2001.0, 3000.0, 1, 6003.0),
                                               ArchiveErasure(baseName, 2002.0, 3000.0, 0,
                                                              604.0)}));
    }

    void synchronizeNOOP()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);

        SequenceName baseName("bnd", "raw", "BsG_S11");

        QByteArray sequenceData = constructSequence(index.flavor,
                                                    {ArchiveValue(baseName, Variant::Root(1.0),
                                                                  1000.0,
                                                                  2000.0, 0, 500.0, true)});
        QByteArray erasureData = constructErasure(index.flavor,
                                                  {ArchiveErasure(baseName, 2000.0, 3000.0, 0,
                                                                  600.0)});

        WriteBlock::Context context(index.flavor);

        context.add(ArchiveValue(baseName, Variant::Root(1.1), 1000.0, 2000.0, 0, 300.0, true));

        context.remove(ArchiveErasure(baseName, 2000.0, 3000.0, 0, 400.0));


        WriteBlock::Sequence sequence(sequenceData);
        WriteBlock::Erasure erasure(erasureData);
        context.synchronizeStageOne();
        erasure.synchronizeStageTwo(context);
        sequence.synchronizeStageThree(context);
        sequence.inject(context);
        erasure.inject(context);

        QVERIFY(!sequence.isModified());
        QVERIFY(!erasure.isModified());
    }

    void synchronizeEmptyErasure()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);

        SequenceName baseName("bnd", "raw", "BsG_S11");

        QByteArray sequenceData = constructSequence(index.flavor,
                                                    {ArchiveValue(baseName, Variant::Root(1.0),
                                                                  1000.0,
                                                                  2000.0, 0, 500.0, false)});
        QByteArray erasureData = constructErasure(index.flavor,
                                                  {ArchiveErasure(baseName, 2000.0, 3000.0, 0,
                                                                  600.0)});

        WriteBlock::Context context(index.flavor);

        context.add(ArchiveValue(baseName, Variant::Root(1.1), 1000.0, 2000.0, 0, 300.0, true));
        context.add(ArchiveValue(baseName, Variant::Root(2.1), 2000.0, 3000.0, 0, 6000.0, true));


        WriteBlock::Sequence sequence(sequenceData);
        WriteBlock::Erasure erasure(erasureData);
        context.synchronizeStageOne();
        erasure.synchronizeStageTwo(context);
        sequence.synchronizeStageThree(context);
        sequence.inject(context);
        erasure.inject(context);

        QVERIFY(sequence.isModified());
        QVERIFY(erasure.isModified());

        auto sequenceResult = readSequence(index.flavor, baseName, sequence.outputData());


        QVERIFY(compareArchive(sequenceResult,
                               {ArchiveValue(baseName, Variant::Root(1.0), 1000.0, 2000.0, 0, 500.0,
                                             true),
                                ArchiveValue(baseName, Variant::Root(2.1), 2000.0, 3000.0, 0,
                                             6000.0,
                                             true)}));

        QVERIFY(erasure.outputData().empty());
    }

    void synchronizeEmptySequence()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);

        SequenceName baseName("bnd", "raw", "BsG_S11");

        QByteArray sequenceData = constructSequence(index.flavor,
                                                    {ArchiveValue(baseName, Variant::Root(1.0),
                                                                  1000.0,
                                                                  2000.0, 0, 500.0, false)});
        QByteArray erasureData;

        WriteBlock::Context context(index.flavor);

        context.remove(ArchiveErasure(baseName, 1000.0, 2000.0, 0, 6000.0));

        WriteBlock::Sequence sequence(sequenceData);
        WriteBlock::Erasure erasure(erasureData);
        context.synchronizeStageOne();
        erasure.synchronizeStageTwo(context);
        sequence.synchronizeStageThree(context);
        sequence.inject(context);
        erasure.inject(context);

        QVERIFY(sequence.isModified());
        QVERIFY(erasure.isModified());

        auto erasureResult = readErasure(index.flavor, baseName, erasure.outputData());

        QVERIFY(sequence.outputData().empty());

        QVERIFY(compareErasure(erasureResult,
                               {ArchiveErasure(baseName, 1000.0, 2000.0, 0, 6000.0)}));
    }

    void flagRemoteReferenceBasic()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);

        SequenceName baseName("bnd", "raw", "BsG_S11");

        QByteArray sequenceData = constructSequence(index.flavor,
                                                    {ArchiveValue(baseName, Variant::Root(1.0),
                                                                  1000.0,
                                                                  2000.0, 0, 500.0, false),
                                                     ArchiveValue(baseName, Variant::Root(2.0),
                                                                  1001.0,
                                                                  2000.0, 0, 501.0, false)});

        WriteBlock::Sequence::RemoteReferenceContext context(index.flavor);

        context.reference(
                ArchiveValue(baseName, Variant::Root(1.1), 1000.0, 2000.0, 0, 500.0, false));
        context.reference(
                ArchiveValue(baseName, Variant::Root(1.2), 1000.0, 2000.0, 1, 500.0, false));

        WriteBlock::Sequence sequence(sequenceData);
        sequence.remoteReferenced(context);

        QVERIFY(sequence.isModified());

        auto sequenceResult = readSequence(index.flavor, baseName, sequence.outputData());

        QVERIFY(compareArchive(sequenceResult,
                               {ArchiveValue(baseName, Variant::Root(1.0), 1000.0, 2000.0, 0, 500.0,
                                             true),
                                ArchiveValue(baseName, Variant::Root(2.0), 1001.0, 2000.0, 0, 501.0,
                                             false)}));
    }

    void flagRemoteReferenceNOOP()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);

        SequenceName baseName("bnd", "raw", "BsG_S11");

        QByteArray sequenceData = constructSequence(index.flavor,
                                                    {ArchiveValue(baseName, Variant::Root(1.0),
                                                                  1000.0,
                                                                  2000.0, 0, 500.0, true),
                                                     ArchiveValue(baseName, Variant::Root(2.0),
                                                                  1001.0,
                                                                  2000.0, 0, 501.0, false)});

        WriteBlock::Sequence::RemoteReferenceContext context(index.flavor);

        context.reference(
                ArchiveValue(baseName, Variant::Root(1.1), 1000.0, 2000.0, 0, 500.0, false));

        WriteBlock::Sequence sequence(sequenceData);
        sequence.remoteReferenced(context);

        QVERIFY(!sequence.isModified());
    }

    void flagRemoteReferenceEmpty()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);

        SequenceName baseName("bnd", "raw", "BsG_S11");

        QByteArray sequenceData;

        WriteBlock::Sequence::RemoteReferenceContext context(index.flavor);

        context.reference(
                ArchiveValue(baseName, Variant::Root(1.1), 1000.0, 2000.0, 0, 500.0, false));

        WriteBlock::Sequence sequence(sequenceData);
        sequence.remoteReferenced(context);

        QVERIFY(!sequence.isModified());
    }
};

QTEST_APPLESS_MAIN(TestWriteBlock)

#include "writeblock.moc"
