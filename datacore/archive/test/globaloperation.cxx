/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QTest>
#include <QTemporaryFile>

#include "datacore/archive/globaloperation.hxx"
#include "datacore/archive/initialization.hxx"
#include "core/qtcompat.hxx"

#include "common.cxx"

class TestGlobalOperation : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    Database::Storage archiveStorage() const
    {
        return Database::Storage::sqlite(databaseFile.fileName().toUtf8().constData());
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        QVERIFY(databaseFile.open());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void findUnused()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        FindUnusedIndexOperation::IndexContents contents;
        contents.station.emplace(index.station.insert("bnd"));
        contents.station.emplace(index.station.insert("alt"));
        contents.archive.emplace(index.archive.insert("raw"));
        contents.archive.emplace(index.archive.insert("clean"));
        contents.variable.emplace(index.variable.insert("BsG_S11"));
        contents.variable.emplace(index.variable.insert("BsB_S11"));
        contents.flavor.emplace(index.flavor.insert(""));
        contents.flavor.emplace(index.flavor.insert("pm1"));

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0,
                                     600.0, false),});
        erasureInsert(&db, layout, index, 0,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0, 700.0),});

        FindUnusedIndexOperation::EmptyIndices emptySequence;
        FindUnusedIndexOperation::EmptyIndices emptyErasure;
        {
            FindUnusedIndexOperation read(layout, contents);
            read.start();
            read.wait();
            contents = read.remainingContents();
            emptySequence = read.emptySequences();
            emptyErasure = read.emptyErasures();
        }

        QCOMPARE((int) contents.station.size(), 1);
        QCOMPARE((int) contents.archive.size(), 1);
        QCOMPARE((int) contents.variable.size(), 1);
        QCOMPARE((int) contents.flavor.size(), 1);

        QVERIFY((int) contents.station.count(index.station.lookup("alt")) == 1);
        QVERIFY((int) contents.archive.count(index.archive.lookup("clean")) == 1);
        QVERIFY((int) contents.variable.count(index.variable.lookup("BsB_S11")) == 1);
        QVERIFY((int) contents.flavor.count(index.flavor.lookup("pm1")) == 1);

        QVERIFY(emptySequence.empty());
        QVERIFY(emptyErasure.empty());
    }

    void findUnusedTable()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0xF)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        FindUnusedIndexOperation::IndexContents contents;
        contents.station.emplace(index.station.insert("bnd"));
        contents.station.emplace(index.station.insert("alt"));
        contents.archive.emplace(index.archive.insert("raw"));
        contents.archive.emplace(index.archive.insert("clean"));
        contents.variable.emplace(index.variable.insert("BsG_S11"));
        contents.variable.emplace(index.variable.insert("BsB_S11"));
        contents.flavor.emplace(index.flavor.insert(""));
        contents.flavor.emplace(index.flavor.insert("pm1"));

        Structure::Identity idRemove(index.station.lookup("alt"), index.archive.lookup("clean"),
                                     index.variable.lookup("BsB_S11"));

        Initialization::createSequenceTable(&db, layout, idRemove);
        Initialization::createErasureTable(&db, layout, idRemove);

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0,
                                     600.0, false),});

        FindUnusedIndexOperation::EmptyIndices emptySequence;
        FindUnusedIndexOperation::EmptyIndices emptyErasure;
        {
            FindUnusedIndexOperation op(layout, contents);
            op.start();
            op.wait();
            contents = op.remainingContents();
            emptySequence = op.emptySequences();
            emptyErasure = op.emptyErasures();
        }

        QCOMPARE((int) contents.station.size(), 1);
        QCOMPARE((int) contents.archive.size(), 1);
        QCOMPARE((int) contents.variable.size(), 1);
        QCOMPARE((int) contents.flavor.size(), 1);

        QVERIFY(contents.station.count(index.station.lookup("alt")) == 1);
        QVERIFY(contents.archive.count(index.archive.lookup("clean")) == 1);
        QVERIFY(contents.variable.count(index.variable.lookup("BsB_S11")) == 1);
        QVERIFY(contents.flavor.count(index.flavor.lookup("pm1")) == 1);

        QCOMPARE((int) emptySequence.size(), 1);
        QCOMPARE((int) emptyErasure.size(), 1);

        QVERIFY(emptySequence.count(idRemove) == 1);
        QVERIFY(emptyErasure.count(idRemove) == 1);
    }

    void recompress()
    {
        Database::Connection db(archiveStorage());
        QVERIFY(db.start());
        QVERIFY(db.synchronousTransaction(std::bind(&initialize, &db, 0x8)));
        auto trans = db.transaction();
        IdentityIndex::initialize(&db);
        IdentityIndex index(&db);
        Structure::Layout layout(&db);

        sequenceInsert(&db, layout, index, -1,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(),
                                     1000.0,
                                     0, 500.0, false),
                        ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(),
                                     1001.0,
                                     0, 501.0, false),});
        sequenceInsert(&db, layout, index, 0,
                       {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0, 2000.0,
                                     0,
                                     600.0, false),});
        erasureInsert(&db, layout, index, 0,
                      {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0, 700.0),});

        {
            RecompressOperation op(layout);
            op.start();
            op.wait();
        }
        {
            RecompressOperation op(layout, 100.0);
            op.start();
            op.wait();
        }
        {
            RecompressOperation op(layout, FP::undefined(), 1000.0);
            op.start();
            op.wait();
        }
        {
            RecompressOperation op(layout, 100.0, 1000.0);
            op.start();
            op.wait();
        }
        {
            RecompressOperation op(layout, 50.0, 60.0);
            op.start();
            op.wait();
        }

        QVERIFY(checkArchive(layout, index, -1,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0),
                                           FP::undefined(),
                                           1000.0, 0, 500.0, false),
                              ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0),
                                           FP::undefined(),
                                           1001.0, 0, 501.0, false),}));
        QCOMPARE(sequenceModified(layout, index, -1, {"bnd", "raw", "BsG_S11"}), 501.0);
        QVERIFY(checkArchive(layout, index, 0,
                             {ArchiveValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 1000.0,
                                           2000.0, 0,
                                           600.0, false),}));
        QCOMPARE(sequenceModified(layout, index, 0, {"bnd", "raw", "BsG_S11"}), 600.0);

        QVERIFY(checkErasure(layout, index, 0,
                             {ArchiveErasure({"bnd", "raw", "BsG_S11"}, 1000.0, 2000.0, 0,
                                             700.0),}));
        QCOMPARE(erasureModified(layout, index, 0, {"bnd", "raw", "BsG_S11"}), 700.0);
    }
};

QTEST_APPLESS_MAIN(TestGlobalOperation)

#include "globaloperation.moc"
