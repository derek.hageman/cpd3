/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "selection.hxx"

#include <QRegularExpression>
#include <QDebugStateSaver>

#include "core/range.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"

namespace CPD3 {
namespace Data {
namespace Archive {

Selection::Selection() : start(FP::undefined()),
                         end(FP::undefined()),
                         stations(),
                         archives(),
                         variables(),
                         hasFlavors(),
                         lacksFlavors(),
                         exactFlavors(),
                         modifiedAfter(FP::undefined()),
                         includeDefaultStation(true),
                         includeMetaArchive(true)
{ }

Selection::Selection(double setStart,
                     double setEnd,
                     const Match &setStations,
                     const Match &setArchives,
                     const Match &setVariables,
                     const Match &setHasFlavors,
                     const Match &setLacksFlavors,
                     const Match &setExactFlavors,
                     double setModifiedAfter) : start(setStart),
                                                end(setEnd),
                                                stations(setStations),
                                                archives(setArchives),
                                                variables(setVariables),
                                                hasFlavors(setHasFlavors),
                                                lacksFlavors(setLacksFlavors),
                                                exactFlavors(setExactFlavors),
                                                modifiedAfter(setModifiedAfter),
                                                includeDefaultStation(true),
                                                includeMetaArchive(true)
{ }

Selection::Selection(const SequenceName &name, double start, double end, double setModifiedAfter)
        : start(start),
          end(end),
          stations{QRegularExpression::escape(name.getStationQString()).toStdString()},
          archives{QRegularExpression::escape(name.getArchiveQString()).toStdString()},
          variables{QRegularExpression::escape(name.getVariableQString()).toStdString()},
          hasFlavors(),
          lacksFlavors(),
          exactFlavors(),
          modifiedAfter(setModifiedAfter),
          includeDefaultStation(true),
          includeMetaArchive(true)
{
    auto flavors = name.getFlavors();
    if (flavors.empty()) {
        exactFlavors.emplace_back();
    } else {
        for (const auto &f : flavors) {
            exactFlavors.emplace_back(
                    QRegularExpression::escape(QString::fromStdString(f)).toStdString());
        }
    }
}

Selection::Selection(const Selection &other) = default;

Selection &Selection::operator=(const Selection &other) = default;

Selection::Selection(Selection &&other) = default;

Selection &Selection::operator=(Selection &&other) = default;

Selection Selection::withDefaultStation(bool include) const
{
    Selection result(*this);
    result.includeDefaultStation = include;
    return result;
}

std::string Selection::excludeMetaArchiveMatcher()
{
    /* Qt (prior to the 5.0 series) does not support negative lookbehind,
     * so the simple form of ".*(?<!_meta)" can't be used (even in 5.0 it's not
     * supported in QRegExp only QRegularExpression).  So we basically
     * brute force it, which works since this is a whole string match
     * (read: no repeat matches) and this is at the end.  We can't use
     * lookahead assertions because the quantifies are greedy (so the
     * ".*" in ".*(?!_meta)" ends up consuming a "_meta" and the
     * expression matches anyway). */
    static const std::string match = "(?:.{0,5}|.+"
                                     "(?:[^_].{4}|"
                                     "(?:_[^m].{3}|"
                                     "(?:_m[^e].{2}|"
                                     "(?:_me[^t].{1}|"
                                     "(?:_met[^a]"
                                     "))))))";
    return match;
}

Selection Selection::withMetaArchive(bool include) const
{
    Selection result(*this);
    result.includeMetaArchive = include;
    return result;
}


static void serializeMatch(QDataStream &stream, const Archive::Selection::Match &match)
{
    stream << static_cast<quint32>(match.size());
    for (const auto &i : match) {
        stream << i;
    }
}

QDataStream &operator<<(QDataStream &stream, const Archive::Selection &value)
{
    stream << value.start << value.end;
    serializeMatch(stream, value.stations);
    serializeMatch(stream, value.archives);
    serializeMatch(stream, value.variables);
    serializeMatch(stream, value.hasFlavors);
    serializeMatch(stream, value.lacksFlavors);
    serializeMatch(stream, value.exactFlavors);
    stream << value.modifiedAfter << value.includeDefaultStation << value.includeMetaArchive;
    return stream;
}

static void deserializeMatch(QDataStream &stream, Archive::Selection::Match &match)
{
    quint32 n = 0;
    stream >> n;
    for (quint32 i = 0; i < n; ++i) {
        std::string add;
        stream >> add;
        match.emplace_back(std::move(add));
    }
}


QDataStream &operator>>(QDataStream &stream, Archive::Selection &value)
{
    stream >> value.start >> value.end;
    deserializeMatch(stream, value.stations);
    deserializeMatch(stream, value.archives);
    deserializeMatch(stream, value.variables);
    deserializeMatch(stream, value.hasFlavors);
    deserializeMatch(stream, value.lacksFlavors);
    deserializeMatch(stream, value.exactFlavors);
    stream >> value.modifiedAfter >> value.includeDefaultStation >> value.includeMetaArchive;
    return stream;
}

static void listOrEmpty(QDebug &stream, const Archive::Selection::Match &list)
{
    if (list.size() == 1 && list.begin()->empty()) {
        stream << "-NULL-";
    } else {
        std::vector<std::string> sorted = list;
        std::sort(sorted.begin(), sorted.end());
        stream << sorted;
    }
}

QDebug operator<<(QDebug stream, const Archive::Selection &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "Selection(bounds=" << Logging::range(v.start, v.end) << ",stations=[";
    listOrEmpty(stream, v.stations);
    stream << "],archives=[";
    listOrEmpty(stream, v.archives);
    stream << "],variables=[";
    listOrEmpty(stream, v.variables);
    stream << "],hasFlavors=[";
    listOrEmpty(stream, v.hasFlavors);
    stream << "],lacksFlavors=[";
    listOrEmpty(stream, v.lacksFlavors);
    stream << "],exactFlavors=[";
    listOrEmpty(stream, v.exactFlavors);
    stream << "],modifiedAfter=" << Logging::time(v.modifiedAfter);
    if (!v.includeDefaultStation && !v.includeMetaArchive) {
        stream << ",NoDefaultStation|NoMetaArchive)";
    } else if (!v.includeMetaArchive) {
        stream << ",NoMetaArchive)";
    } else if (!v.includeDefaultStation) {
        stream << ",NoDefaultStation)";
    } else {
        stream << ")";
    }
    return stream;
}

}
}
}