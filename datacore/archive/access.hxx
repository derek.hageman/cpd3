/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREARCHIVE_ACCESS_HXX
#define CPD3DATACOREARCHIVE_ACCESS_HXX

#include "core/first.hxx"

#include <functional>
#include <memory>
#include <cstdint>
#include <unordered_set>
#include <condition_variable>
#include <mutex>
#include <future>

#include "datacore/datacore.hxx"

#include "datacore/stream.hxx"
#include "core/threading.hxx"
#include "selection.hxx"

class QFile;

namespace CPD3 {

namespace Database {
class Storage;

class Connection;
}

namespace Data {
namespace Archive {

struct IdentityIndex;

class ReadSequence;

class ReadArchive;

class ReadErasure;

namespace Structure {
class Layout;
}

/**
 * The general archive access interface.  This provides locking and actual access
 * to the data stored in the archive.
 */
class CPD3DATACORE_EXPORT Access final {
    std::unique_ptr<Database::Connection> connection;

    std::mutex lockMutex;
    std::condition_variable lockState;
    bool isWriteLock;
    bool isSerializableLock;
    std::uint_fast64_t nextLock;
    std::unordered_set<std::uint_fast64_t> activeLocks;

    class TransactionContext;

    std::unique_ptr<TransactionContext> activeTransaction;

    void beginTransaction(bool serializable, bool write);

    bool endTransaction();

    bool isTerminated();

public:
    /**
     * Create access to the default archive.
     */
    Access();

    /**
     * Create access to the specified archive.
     *
     * @param storage   the archive
     */
    explicit Access(const Database::Storage &storage);

    /**
     * Create access to the archive on a specific connection.
     *
     * @param connection    the database connection
     */
    explicit Access(Database::Connection *connection);

    /**
     * Create access to the archive on a SQLite database.
     *
     * @param database      the database file name
     */
    explicit Access(const QFile &database);

    ~Access();

    /**
     * Terminate the connection and start the abort of any transactions in
     * progress.
     */
    void signalTerminate();

    /**
     * Wait for all locks to release.
     *
     * @param timeout   the maximum time to wait
     * @return          true if all locks where clear
     */
    bool waitForLocks(double timeout = FP::undefined());


    /**
     * A read only lock on the archive access.  This may be acquired inside
     * a write lock.
     */
    class CPD3DATACORE_EXPORT ReadLock final {
        Access *access;
        std::uint_fast64_t id;
        bool exclusive;
    public:
        /**
         * Create the read lock.
         *
         * @param access        the access to lock
         * @param serializable  ensure that the lock is serializable with respect to other serializable locks
         * @param exclusive     wait for any existing locks to release before acquiring this one
         */
        ReadLock(Access &access, bool serializable = false, bool exclusive = false);

        ~ReadLock();

        ReadLock(ReadLock &&other);

        ReadLock &operator=(ReadLock &&other);

        void swap(ReadLock &other);

        ReadLock() = delete;

        ReadLock &operator=(const ReadLock &) = delete;

        /**
         * Release the read lock.
         */
        void release();
    };

    friend class ReadLock;

    /**
     * A write lock on the archive access.  Due to the fact that write locks can fail
     * to commit, nested ones must be protected by an outermost one that is released
     * last.  That is, ending the first write lock while others are still held
     * will produce unexpected results.  Destruction without committing or aborting
     * results in an abort, but is not recommended.
     */
    class CPD3DATACORE_EXPORT WriteLock {
        Access *access;
        std::uint_fast64_t id;
        bool exclusive;
    public:
        /**
         * Create the write lock
         *
         * @param access        the access to lock
         * @param exclusive     wait for any existing locks to release before acquiring this one
         */
        WriteLock(Access &access, bool exclusive = true);

        ~WriteLock();

        WriteLock(WriteLock &&other);

        WriteLock &operator=(WriteLock &&other);

        void swap(WriteLock &other);

        /**
         * Abort the lock, rolling back any changes made.
         */
        void abort();

        /**
         * Commit all changes made by the lock.
         *
         * @return  true if changes where successfully comitted
         */
        bool commit();

        WriteLock() = delete;

        WriteLock &operator=(const WriteLock &) = delete;
    };

    friend class WriteLock;

    /**
     * Perform a synchronous write operation on the archive.  The write lock held
     * while this is performed is not exclusive.
     * <br>
     * The operation must be able to be repeated, if the write lock fails to commit.
     *
     * @param operation     the operation, returning false aborts all attempts to write
     * @return              true if the operation succeeded
     */
    bool writeSynchronous(const std::function<bool()> &operation);

    /**
     * Perform a synchronous write, with a direct replacement of the specified values.  If no
     * existing locks are held, the write will be retried if required.
     *
     * @param values        the values to write or replace
     * @param sorted        true if the values are already sorted
     * @return              true if the write succeeded
     */
    bool writeSynchronous(const SequenceValue::Transfer &values, bool sorted = false);

    /**
     * Perform a synchronous write, with a direct replacement of the specified values.  If no
     * existing locks are held, the write will be retried if required.
     *
     * @param values        the values to write or replace
     * @param sorted        true if the values are already sorted
     * @return              true if the write succeeded
     */
    bool writeSynchronous(const ArchiveValue::Transfer &values, bool sorted = false);

    /**
     * Perform a synchronous write, with a direct replacement of the specified values.  If no
     * existing locks are held, the write will be retried if required.
     *
     * @param values        the values to write or replace
     * @param remove        the values to remove
     * @return              true if the write succeeded
     */
    bool writeSynchronous(const SequenceValue::Transfer &values,
                          const ArchiveErasure::Transfer &remove);

    /**
     * Perform a synchronous write, with a direct replacement of the specified values.  If no
     * existing locks are held, the write will be retried if required.
     *
     * @param values        the values to write or replace
     * @param remove        the values to remove
     * @return              true if the write succeeded
     */
    bool writeSynchronous(const SequenceValue::Transfer &values,
                          const SequenceValue::Transfer &remove);

    /**
     * Perform a synchronous write, with a direct replacement of the specified values.  If no
     * existing locks are held, the write will be retried if required.
     *
     * @param values        the values to write or replace
     * @param remove        the values to remove
     * @return              true if the write succeeded
     */
    bool writeSynchronous(const SequenceValue::Transfer &values,
                          const SequenceIdentity::Transfer &remove);

    /**
     * Perform a synchronous write, with a direct replacement of the specified values.  If no
     * existing locks are held, the write will be retried if required.
     *
     * @param values        the values to write or replace
     * @param remove        the values to remove
     * @return              true if the write succeeded
     */
    bool writeSynchronous(const ArchiveValue::Transfer &values,
                          const ArchiveErasure::Transfer &remove);

    /**
     * Perform a synchronous write, removing the specified values.  If no
     * existing locks are held, the write will be retried if required.
     *
     * @param remove        the values to remove
     * @param sorted        true if the values are already sorted
     * @return              true if the write succeeded
     */
    bool writeSynchronous(const ArchiveErasure::Transfer &remove, bool sorted = false);

    /**
     * Perform a synchronous write, removing the specified values.  If no
     * existing locks are held, the write will be retried if required.
     *
     * @param remove        the values to remove
     * @param sorted        true if the values are already sorted
     * @return              true if the write succeeded
     */
    bool removeSynchronous(const SequenceIdentity::Transfer &remove, bool sorted = false);

    /**
     * Perform a synchronous write, removing the specified values.  If no
     * existing locks are held, the write will be retried if required.
     *
     * @param remove        the values to remove
     * @param sorted        true if the values are already sorted
     * @return              true if the write succeeded
     */
    bool removeSynchronous(const SequenceValue::Transfer &remove, bool sorted = false);

    /**
     * Perform a synchronous read, returning the resulting values.
     *
     * @param selections    the specification of what to read
     * @return              the data covered by the specification
     */
    SequenceValue::Transfer readSynchronous(const Selection::List &selections);

    /** @see readSynchronous(const Selection::List &) */
    template<class Container>
    inline SequenceValue::Transfer readSynchronous(const Container &selections)
    { return this->readSynchronous(Selection::List(selections.begin(), selections.end())); }

    /** @see readSynchronous(const Selection::List &) */
    inline SequenceValue::Transfer readSynchronous(const Selection &selection)
    { return this->readSynchronous(Selection::List{selection}); }


    /**
     * Perform a synchronous read of full archive values, returning the resulting values.
     *
     * @param selections    the specification of what to read
     * @return              the data covered by the specification
     */
    ArchiveValue::Transfer readSynchronousArchive(const Selection::List &selections);

    /** @see readSynchronousArchive(const Selection::List &) */
    template<class Container>
    inline ArchiveValue::Transfer readSynchronousArchive(const Container &selections)
    {
        return this->readSynchronousArchive(Selection::List(selections.begin(), selections.end()));
    }

    /** @see readSynchronousArchive(const Selection::List &) */
    inline ArchiveValue::Transfer readSynchronousArchive(const Selection &selection)
    { return this->readSynchronousArchive(Selection::List{selection}); }


    /**
     * An asynchronous stream reading context.
     */
    class CPD3DATACORE_EXPORT StreamContext final
            : public CPD3::Data::StreamSource, public std::enable_shared_from_this<StreamContext> {
        std::unique_ptr<ReadLock> lock;
        std::unique_ptr<ReadSequence> operation;

        StreamContext(ReadLock *lock, ReadSequence *operation);

        friend class Access;

    public:
        StreamContext() = delete;

        StreamContext(const StreamContext &) = delete;

        StreamContext &operator=(const StreamContext &) = delete;

        virtual ~StreamContext();

        /**
         * Abort the read.
         */
        void signalTerminate();

        /**
         * Wait for completion.
         *
         * @param timeout   the maximum time to wait
         * @return          true if the context completed
         */
        bool wait(double timeout = FP::undefined());

        /**
         * Test if the operation has completed.
         *
         * @return      true if the context has completed
         */
        bool isComplete();

        /**
         * Detach the context, running in the background even if no further references
         * to it remain.
         */
        void detach();

        void setEgress(StreamSink *sink) override;

        /**
         * A signal generated from the read thread once it has completed work.
         */
        Threading::Signal<> complete;
    };

    /**
     * A pointer to a stream context.
     */
    typedef std::shared_ptr<StreamContext> StreamHandle;

    /**
     * Create an asynchronous read.
     *
     * @param selections    the specification of what to read
     * @param sink          the initial sink if any
     * @return              the read context
     */
    StreamHandle readStream(const Selection::List &selections, StreamSink *sink = nullptr);

    /** @see readStream(const Selection::List &, StreamSink *) */
    template<class Container>
    inline StreamHandle readStream(const Container &selections, StreamSink *sink = nullptr)
    { return this->readStream(Selection::List(selections.begin(), selections.end()), sink); }

    /** @see readStream(const Selection::List &, StreamSink *) */
    inline StreamHandle readStream(const Selection &selection, StreamSink *sink = nullptr)
    { return this->readStream(Selection::List{selection}, sink); }


    /**
     * An asynchronous full archive value reading context.
     */
    class CPD3DATACORE_EXPORT ArchiveContext final
            : public CPD3::Data::ArchiveSource,
              public std::enable_shared_from_this<ArchiveContext> {
        std::unique_ptr<ReadLock> lock;
        std::unique_ptr<ReadArchive> operation;

        ArchiveContext(ReadLock *lock, ReadArchive *operation);

        friend class Access;

    public:
        ArchiveContext() = delete;

        ArchiveContext(const ArchiveContext &) = delete;

        ArchiveContext &operator=(const ArchiveContext &) = delete;

        virtual ~ArchiveContext();

        /**
         * Abort the read.
         */
        void signalTerminate();

        /**
         * Wait for completion.
         *
         * @param timeout   the maximum time to wait
         * @return          true if the context completed
         */
        bool wait(double timeout = FP::undefined());

        /**
         * Test if the operation has completed.
         *
         * @return      true if the context has completed
         */
        bool isComplete();

        /**
         * Detach the context, running in the background even if no further references
         * to it remain.
         */
        void detach();

        void setEgress(ArchiveSink *sink) override;

        /**
         * A signal generated from the read thread once it has completed work.
         */
        Threading::Signal<> complete;
    };

    /**
     * A pointer to an archive reading context.
     */
    typedef std::shared_ptr<ArchiveContext> ArchiveHandle;

    /**
     * Create an asynchronous archive read.
     *
     * @param selections    the specification of what to read
     * @param sink          the initial sink, if any
     * @return              the read context
     */
    ArchiveHandle readArchive(const Selection::List &selections, ArchiveSink *sink = nullptr);

    /** @see readArchive(const Selection::List &, ArchiveSink *) */
    template<class Container>
    inline ArchiveHandle readArchive(const Container &selections, ArchiveSink *sink = nullptr)
    { return this->readArchive(Selection::List(selections.begin(), selections.end()), sink); }

    /** @see readArchive(const Selection::List &, ArchiveSink *) */
    inline ArchiveHandle readArchive(const Selection &selection, ArchiveSink *sink = nullptr)
    { return this->readArchive(Selection::List{selection}, sink); }


    /**
     * An asynchronous archive value and erasure reading context.
     */
    class CPD3DATACORE_EXPORT ErasureContext final
            : public CPD3::Data::ErasureSource,
              public std::enable_shared_from_this<ErasureContext> {
        std::unique_ptr<ReadLock> lock;
        std::unique_ptr<ReadErasure> operation;

        ErasureContext(ReadLock *lock, ReadErasure *operation);

        friend class Access;

    public:
        ErasureContext() = delete;

        ErasureContext(const ErasureContext &) = delete;

        ErasureContext &operator=(const ErasureContext &) = delete;

        virtual ~ErasureContext();

        /**
         * Abort the read.
         */
        void signalTerminate();

        /**
         * Wait for completion.
         *
         * @param timeout   the maximum time to wait
         * @return          true if the context completed
         */
        bool wait(double timeout = FP::undefined());

        /**
         * Test if the operation has completed.
         *
         * @return      true if the context has completed
         */
        bool isComplete();

        /**
         * Detach the context, running in the background even if no further references
         * to it remain.
         */
        void detach();

        void setEgress(ErasureSink *sink) override;

        /**
         * A signal generated from the read thread once it has completed work.
         */
        Threading::Signal<> complete;
    };

    /**
     * A pointer to an erasure reading context.
     */
    typedef std::shared_ptr<ErasureContext> ErasureHandle;

    /**
     * Create an asynchronous erasure read.
     *
     * @param selections    the specification of what to read
     * @param sink          the initial sink, if any
     * @return              the read context
     */
    ErasureHandle readErasure(const Selection::List &selections, ErasureSink *sink = nullptr);

    /** @see readErasure(const Selection::List &, ErasureSink *) */
    template<class Container>
    inline ErasureHandle readErasure(const Container &selections, ErasureSink *sink = nullptr)
    { return this->readErasure(Selection::List(selections.begin(), selections.end()), sink); }

    /** @see readErasure(const Selection::List &, ErasureSink *) */
    inline ErasureHandle readErasure(const Selection &selection, ErasureSink *sink = nullptr)
    { return this->readErasure(Selection::List{selection}, sink); }

    /**
     * A general base class implementing a retrying write operation running on
     * a background thread.  The most common use of this is to implement
     * write operations that buffer incoming data, then transfer that data
     * to a processing area, and finally attempt to write the processing data
     * into the archive.
     * <br>
     * This class manages the locking and retries for the write operations, so
     * derived classes generally only need to implement the transfer buffers
     * and the actual write operation.
     */
    class CPD3DATACORE_EXPORT BackgroundWrite : public std::enable_shared_from_this<
            BackgroundWrite> {
    private:
        Access &access;
        bool exclusive;

        std::mutex mutex;
        std::condition_variable notify;
        enum {
            State_Inactive,
            State_Executing_Once,
            State_Executing_Again,
            State_Executing_Terminate,
            State_Terminated,
        } state;

        static void run(const std::shared_ptr<BackgroundWrite> &self);

        friend class Access;

    public:
        /**
         * Create the write operation.
         *
         * @param access        the archive access
         * @param exclusive     use an exclusive write lock
         */
        BackgroundWrite(Access &access, bool exclusive = true);

        virtual ~BackgroundWrite();

        /**
         * Abort the write.
         */
        void signalTerminate();

        /**
         * Wait for any active writes to complete.
         *
         * @param timeout   the maximum time to wait
         * @return          true if the write has completed
         */
        bool wait(double timeout = FP::undefined());

        /**
         * A signal generated from the thread once it has completed work.
         */
        Threading::Signal<> complete;

    protected:
        /**
         * Wake the background write to perform work.
         *
         * @param withLock      called with the thread lock held before actually performing the wake, must return true to wake the worker
         * @param stall         called with the thread lock held, delaying the wake operation until it returns false
         */
        void wake(const std::function<bool()> &withLock = std::function<bool()>(),
                  const std::function<bool()> &stall = std::function<bool()>());

        /**
         * The result of the actual write operation.
         */
        enum Result {
            /** Operation successful, do not retry unless the commit fails */
                    Success,

            /** Operation successful, but repeat it */
                    Again,

            /** Operation failed, abort the lock and repeat */
                    Retry,

            /** Operation failed, abort the lock and do not repeat */
                    Abort,
        };

        /**
         * Perform the write operation.  This is called without the thread lock, but
         * with the archive write lock.
         *
         * @param access    the archive access
         * @return          the result of the operation
         */
        virtual Result operation(Access &access) = 0;

        /**
         * Called with the thread lock held before the operation.
         * <br>
         * The default implementation always returns true.
         *
         * @return          true to proceed with the write
         */
        virtual bool prepare();

        /**
         * Called after the operation with the thread lock, when it has succeeded.
         * <br>
         * The default implementation does nothing.
         */
        virtual void success();

        /**
         * Called after the operation with the thread lock, when it has failed.
         * <br>
         * The default implementation does nothing.
         */
        virtual void failure();

        /**
         * Called during the wait, to test if the wait is complete beyond the
         * current status of the thread.  For example, can return the
         * incoming end state to delay the wait until all data have been
         * received.
         * <br>
         * The default implementation always returns true.
         *
         * @return  true if the wait is complete and the write has finished
         */
        virtual bool isWaitComplete();
    };


    /**
     * An asynchronous write operation performing direct value replacement
     * or addition.
     */
    class CPD3DATACORE_EXPORT ReplaceWriteContext final
            : public CPD3::Data::StreamSink, public BackgroundWrite {
        bool sorted;

        bool ended;
        bool finished;
        SequenceValue::Transfer incoming;
        SequenceValue::Transfer processing;

        ReplaceWriteContext(Access &access, bool exclusive, bool sorted);

        friend class Access;

        bool stall() const;

    public:
        virtual ~ReplaceWriteContext();

        void incomingData(const SequenceValue::Transfer &values) override;

        void incomingData(SequenceValue::Transfer &&values) override;

        void incomingData(const SequenceValue &value) override;

        void incomingData(SequenceValue &&value) override;

        void endData() override;

    protected:
        virtual bool prepare();

        virtual Result operation(Access &access);

        virtual void success();

        virtual bool isWaitComplete();
    };

    friend class ReplaceWriteContext;

    /**
     * A pointer to an asynchronous write replace operation.
     */
    typedef std::shared_ptr<ReplaceWriteContext> ReplaceHandle;

    /**
     * Create an asynchronous replacing write operation.
     *
     * @param exclusive     use an exclusive lock on the archive
     * @param sorted        true if the incoming values are sorted
     * @return              the write operation
     */
    ReplaceHandle writeReplace(bool exclusive = true, bool sorted = true);


    /**
     * An asynchronous write operation performing synchronization semantics.  This
     * ensures that the incoming value only replace existing ones if they are
     * newer and sets the remote flag regardless.
     */
    class CPD3DATACORE_EXPORT SynchronizationWriteContext final
            : public CPD3::Data::ErasureSink, public BackgroundWrite {
        bool sorted;

        bool ended;
        bool finished;
        bool wasNearStall;
        ArchiveValue::Transfer incomingArchiveValues;
        ArchiveValue::Transfer processingArchiveValues;
        ArchiveErasure::Transfer incomingErasureValues;
        ArchiveErasure::Transfer processingErasureValues;

        SynchronizationWriteContext(Access &access, bool exclusive, bool sorted);

        friend class Access;

        bool stall() const;

        bool nearStall() const;

    public:
        virtual ~SynchronizationWriteContext();


        /**
         * Handle incoming data.
         *
         * @param values    the incoming values
         */
        void incomingValue(const ArchiveValue::Transfer &values);

        /** @see incomingValue(const ArchiveValue::Transfer &) */
        void incomingValue(ArchiveValue::Transfer &&values);

        virtual void incomingValue(const ArchiveValue &value);

        virtual void incomingValue(ArchiveValue &&value);


        /**
         * Handle incoming erasures.
         *
         * @param values    the incoming erasures
         */
        void incomingErasure(const ArchiveErasure::Transfer &values);

        /** @see incomingErasure(const ArchiveErasure::Transfer &) */
        void incomingErasure(ArchiveErasure::Transfer &&values);

        virtual void incomingErasure(const ArchiveErasure &erasure);

        virtual void incomingErasure(ArchiveErasure &&erasure);


        /**
         * Handle incoming values and erasures.
         *
         * @param values    the values
         * @param erasure   the erasures
         */
        void incoming(const ArchiveValue::Transfer &values,
                      const ArchiveErasure::Transfer &erasure);

        /** @see incoming(const ArchiveValue::Transfer &, const ArchiveErasure::Transfer &) */
        void incoming(ArchiveValue::Transfer &&values, ArchiveErasure::Transfer &&erasure);

        virtual void endStream();

        /**
         * Test if the writer is near a stall and should attempt to halt the incoming
         * data before a hard stall is required.
         *
         * @return  true if near a forced stall
         */
        bool softStalled();

        /**
         * A signal emitted when the soft stall has cleared.  However, newer values may have
         * made it so the stall has returned, even if this is emitted.
         */
        Threading::Signal<> softStallCleared;

    protected:
        virtual bool prepare();

        virtual Result operation(Access &access);

        virtual void success();

        virtual bool isWaitComplete();
    };

    /**
     * A pointer to a synchronization write operation
     */
    typedef std::shared_ptr<SynchronizationWriteContext> SynchronizationHandle;

    /**
     * Create a synchronization write operation.
     *
     * @param exclusive     use an exclusive lock on the archive
     * @param sorted        true if the incoming values are sorted
     * @return              the write operation
     */
    SynchronizationHandle writeSynchronize(bool exclusive = true, bool sorted = true);


    /**
     * An asynchronous write operation that only sets the remote flag for
     * the incoming values.  This does not actually write the data and is used
     * for flag the local data as having been synchronized while generating
     * data being sent to a remote target.
     */
    class CPD3DATACORE_EXPORT RemoteReferencedWriteContext final
            : public CPD3::Data::SynchronizeLocalSink, public BackgroundWrite {
        bool sorted;

        bool ended;
        bool finished;
        SequenceIdentity::Transfer incoming;
        SequenceIdentity::Transfer processing;

        RemoteReferencedWriteContext(Access &access, bool exclusive, bool sorted);

        friend class Access;

        bool stall() const;

    public:
        virtual ~RemoteReferencedWriteContext();

        void incomingData(const SequenceIdentity::Transfer &values) override;

        void incomingData(SequenceIdentity::Transfer &&values) override;

        void incomingData(const SequenceIdentity &value) override;

        void incomingData(SequenceIdentity &&value) override;

        void endData() override;


        /** @see incomingData(const SequenceIdentity::Transfer &) */
        void incomingValue(const ArchiveValue::Transfer &values);

        /** @see incomingData(SequenceIdentity::Transfer &&) */
        void incomingValue(ArchiveValue::Transfer &&values);

        /** @see incomingData(const SequenceIdentity &) */
        void incomingValue(const ArchiveValue &value);

        /** @see incomingData(SequenceIdentity &&) */
        void incomingValue(ArchiveValue &&value);


        /** @see incomingData(const SequenceIdentity::Transfer &) */
        void incomingValue(const SequenceValue::Transfer &values);

        /** @see incomingData(SequenceIdentity::Transfer &&) */
        void incomingValue(SequenceValue::Transfer &&values);

        /** @see incomingData(const SequenceIdentity &) */
        void incomingValue(const SequenceValue &value);

        /** @see incomingData(SequenceIdentity &&) */
        void incomingValue(SequenceValue &&value);

    protected:
        virtual bool prepare();

        virtual Result operation(Access &access);

        virtual void success();

        virtual bool isWaitComplete();
    };

    /**
     * A pointer to an asynchronous remote flag operation.
     */
    typedef std::shared_ptr<RemoteReferencedWriteContext> RemoteReferencedHandle;

    /**
     * Create a remote flag write operation.
     *
     * @param exclusive     use an exclusive lock on the archive
     * @param sorted        true if the incoming values are sorted
     * @return              the write operation
     */
    RemoteReferencedHandle writeRemoteReferenced(bool exclusive = true, bool sorted = true);


    /**
     * A future for an available components operation.
     */
    typedef std::future<SequenceName::ComponentSet> FutureComponents;

    /**
     * Get the available stations, optionally matching them against a set
     * of regular expressions.
     * <br>
     * This does not access the archive and reflects only the stations possibly defined.
     *
     * @param filter    a set of regular expressions to match against, if not empty
     * @return          the available stations
     */
    SequenceName::ComponentSet availableStations(const Selection::Match &filter = Selection::Match());

    /**
     * Get the available stations, optionally matching them against a set
     * of regular expressions.
     * <br>
     * This does not access the archive and reflects only the stations possibly defined.
     *
     * @param filter    a set of regular expressions to match against, if not empty
     * @return          a future with the available stations
     */
    FutureComponents availableStationsFuture(const Selection::Match &filter = Selection::Match());

    /**
     * Get the available archives, optionally matching them against a set
     * of regular expressions.
     * <br>
     * This does not access the archive and reflects only the archives possibly defined.
     *
     * @param filter    a set of regular expressions to match against, if not empty
     * @return          the available archives
     */
    SequenceName::ComponentSet availableArchives(const Selection::Match &filter = Selection::Match());

    /**
     * Get the available archives, optionally matching them against a set
     * of regular expressions.
     * <br>
     * This does not access the archive and reflects only the archives possibly defined.
     *
     * @param filter    a set of regular expressions to match against, if not empty
     * @return          a future with the available archives
     */
    FutureComponents availableArchivesFuture(const Selection::Match &filter = Selection::Match());

    /**
     * Get the available variables, optionally matching them against a set
     * of regular expressions.
     * <br>
     * This does not access the archive and reflects only the variables possibly defined.
     *
     * @param filter    a set of regular expressions to match against, if not empty
     * @return          the available variables
     */
    SequenceName::ComponentSet availableVariables(const Selection::Match &filter = Selection::Match());

    /**
     * Get the available variables, optionally matching them against a set
     * of regular expressions.
     * <br>
     * This does not access the archive and reflects only the variables possibly defined.
     *
     * @param filter    a set of regular expressions to match against, if not empty
     * @return          a future with the available variables
     */
    FutureComponents availableVariablesFuture(const Selection::Match &filter = Selection::Match());

    /**
     * Get the available flavors, optionally matching them against a set
     * of regular expressions.
     * <br>
     * This does not access the archive and reflects only the flavors possibly defined.
     *
     * @param filter    a set of regular expressions to match against, if not empty
     * @return          the available flavors
     */
    SequenceName::ComponentSet availableFlavors(const Selection::Match &filter = Selection::Match());

    /**
     * Get the available flavors, optionally matching them against a set
     * of regular expressions.
     * <br>
     * This does not access the archive and reflects only the flavors possibly defined.
     *
     * @param filter    a set of regular expressions to match against, if not empty
     * @return          a future with the available flavors
     */
    FutureComponents availableFlavorsFuture(const Selection::Match &filter = Selection::Match());


    /**
     * The types of granularity for availability checking.
     */
    enum AvailableGranularity {
        /**
         * Read the archive in full detail, including flavors. */
                ReadAll,

        /**
         * Only read the spanning blocks in detail, using the index for the remainder.
         * Flavors are not available or checked in non-spanning blocks.
         */
                SpanningOnly,

        /**
         * Read the index only.  No flavor information is available and all spanning
         * blocks are considered to be matched.
         */
                IndexOnly
    };

    /**
     * A future with available sequence names.
     */
    typedef std::future<SequenceName::Set> FutureNames;

    /**
     * Get the available sequence names matching the specified selections.
     * <br>
     * Note that for inexact matching, flavors are ignored.
     *
     * @param selections    the selections to match
     * @param exact         use exact matching, reading the archive instead of only the index
     * @return              the set of matched names
     */
    SequenceName::Set availableNames(const Selection::List &selections,
                                     AvailableGranularity granularity = SpanningOnly);

    /** @see availableNames(const Selection::List &, bool) */
    inline SequenceName::Set availableNames(const Selection &selection,
                                            AvailableGranularity granularity = SpanningOnly)
    { return availableNames(Selection::List{selection}, granularity); }

    /**
     * Get the available sequence names matching the specified selections.
     * <br>
     * Note that for inexact matching, flavors are ignored.
     *
     * @param selections    the selections to match
     * @param exact         use exact matching, reading the archive instead of only the index
     * @return              a future with the set of matched names
     */
    FutureNames availableNamesFuture(const Selection::List &selections,
                                     AvailableGranularity granularity = SpanningOnly);

    /** @see availableNamesFuture(const Selection::List &, bool) */
    inline FutureNames availableNamesFuture(const Selection &selection,
                                            AvailableGranularity granularity = SpanningOnly)
    { return availableNamesFuture(Selection::List{selection}, granularity); }


    /**
     * A container for the components of the sequence names resulting from
     * an available query operation.
     */
    struct SelectedComponents {
        /** The station names. */
        SequenceName::ComponentSet stations;

        /** The archive names. */
        SequenceName::ComponentSet archives;

        /** The variable names. */
        SequenceName::ComponentSet variables;

        /** The individual flavor names. */
        SequenceName::ComponentSet flavors;
    };

    /**
     * A future with available components.
     */
    typedef std::future<SelectedComponents> FutureSelected;

    /**
     * Get the available sequence name components matching the specified selections.
     * <br>
     * Note that for inexact matching, flavors are not returned.
     *
     * @param selections    the selections to match
     * @param exact         use exact matching, reading the archive instead of only the index
     * @return              the sets of matched components
     */
    SelectedComponents availableSelected(const Selection::List &selections,
                                         AvailableGranularity granularity = SpanningOnly);

    /** @see availableSelected(const Selection::List &, bool) */
    inline SelectedComponents availableSelected(const Selection &selection,
                                                AvailableGranularity granularity = SpanningOnly)
    { return availableSelected(Selection::List{selection}, granularity); }

    /**
     * Get the available sequence name components matching the specified selections.
     * <br>
     * Note that for inexact matching, flavors are not returned.
     *
     * @param selections    the selections to match
     * @param exact         use exact matching, reading the archive instead of only the index
     * @return              a future with the sets of matched components
     */
    FutureSelected availableSelectedFuture(const Selection::List &selections,
                                           AvailableGranularity granularity = SpanningOnly);

    /** @see availableSelectedFuture(const Selection::List &, bool) */
    inline FutureSelected availableSelectedFuture(const Selection &selection,
                                                  AvailableGranularity granularity = SpanningOnly)
    { return availableSelectedFuture(Selection::List{selection}, granularity); }

    /**
     * Get all available sequence name components.
     *
     * @return              all available sequence name components.
     */
    SelectedComponents availableSelected();

    /**
     * Get all available sequence name components.
     *
     * @return              all available sequence name components.
     */
    FutureSelected availableSelectedFuture();

    /**
     * A future with existing check results.
     */
    typedef std::future<bool> FutureExists;

    /**
     * Test if the selection results in any data possible.
     * <br>
     * Note that for inexact matching, flavors are ignored.
     *
     * @param selections    the selections to match
     * @param exact         use exact matching, reading the archive instead of only the index
     * @return              true if anything matches
     */
    bool availableExists(const Selection::List &selections,
                         AvailableGranularity granularity = SpanningOnly);

    /** @see availableExists(const Selection::List &, bool) */
    inline bool availableExists(const Selection &selection,
                                AvailableGranularity granularity = SpanningOnly)
    { return availableExists(Selection::List{selection}, granularity); }

    /**
     * Test if the selection results in any data possible.
     * <br>
     * Note that for inexact matching, flavors are ignored.
     *
     * @param selections    the selections to match
     * @param exact         use exact matching, reading the archive instead of only the index
     * @return              a future that will be tru if there are any matches
     */
    FutureExists availableExistsFuture(const Selection::List &selections,
                                       AvailableGranularity granularity = SpanningOnly);

    /** @see availableSelectedFuture(const Selection::List &, bool) */
    inline FutureExists availableExistsFuture(const Selection &selection,
                                              AvailableGranularity granularity = SpanningOnly)
    { return availableExistsFuture(Selection::List{selection}, granularity); }
};

inline void swap(Access::ReadLock &lhs, Access::ReadLock &rhs)
{ lhs.swap(rhs); }

inline void swap(Access::WriteLock &lhs, Access::WriteLock &rhs)
{ lhs.swap(rhs); }

}
}
}

#endif //CPD3DATACOREARCHIVE_ACCESS_HXX
