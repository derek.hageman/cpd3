/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QtEndian>
#include <QLoggingCategory>

#include "globaloperation.hxx"
#include "initialization.hxx"

#include "core/compression.hxx"


Q_LOGGING_CATEGORY(log_datacore_archive_globaloperation, "cpd3.datacore.archive.globaloperation",
                   QtWarningMsg)

namespace CPD3 {
namespace Data {
namespace Archive {

GlobalOperation::BlockOperation::BlockOperation(GlobalOperation::IndexRead &index,
                                                Structure::Block block,
                                                const Structure::Identity &identity) : index(index),
                                                                                       operation(
                                                                                               index.operation),
                                                                                       block(block),
                                                                                       identity(
                                                                                               identity),
                                                                                       state(State_Initialize)
{ }

GlobalOperation::BlockOperation::~BlockOperation() = default;

void GlobalOperation::BlockOperation::start()
{
    Q_ASSERT(state == State_Initialize);
    state = State_CreateContext;

    operation.createRun.run(std::bind(&BlockOperation::createContext, this));
}

void GlobalOperation::BlockOperation::createContext()
{
    Q_ASSERT(state == State_CreateContext);
    state = State_ReadData;

    auto binds = index.blockRead.binds();
    if (index.identity.station == 0)
        binds.emplace(Structure::Column_Station, static_cast<std::int_fast64_t>(identity.station));
    if (index.identity.archive == 0)
        binds.emplace(Structure::Column_Archive, static_cast<std::int_fast64_t>(identity.archive));
    if (index.identity.variable == 0)
        binds.emplace(Structure::Column_Variable, static_cast<std::int_fast64_t>(identity.variable));

    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));

    readDataContext = index.blockRead.statement().execute(binds, true);

    readDataContext.result
                   .connect(
                           std::bind(&BlockOperation::readDataResult, this, std::placeholders::_1));
    readDataContext.complete.connect(std::bind(&BlockOperation::readDataComplete, this));
    return readDataContext.begin();
}

void GlobalOperation::BlockOperation::readDataResult(const Database::Types::Results &results)
{
    Q_ASSERT(state == State_ReadData);
    state = State_Process;

    readDataContext.finish();
    readDataContext = Database::Context();

    Q_ASSERT(!results.empty());
    operation.processRun
             .run(std::bind(&BlockOperation::processData, this, results.front().toBytes()));
}

void GlobalOperation::BlockOperation::readDataComplete()
{
    if (state != State_ReadData)
        return;
    state = State_Complete;
}

void GlobalOperation::BlockOperation::completed()
{
    Q_ASSERT(state == State_Process);
    state = State_Complete;
    return index.completeBlock(this);
}


GlobalOperation::BlockRewrite::BlockRewrite(GlobalOperation::IndexRead &index,
                                            Structure::Block block,
                                            double modified,
                                            const Structure::Identity &identity) : BlockOperation(
        index, block, identity), block(block), modified(modified), identity(identity)
{ }

GlobalOperation::BlockRewrite::~BlockRewrite() = default;

void GlobalOperation::BlockRewrite::processData(const Util::ByteArray &data)
{

    writeDataContext = createWrite(data, block, modified, identity);
    writeDataContext.complete.connect(std::bind(&BlockOperation::completed, this));
    return writeDataContext.begin();
}


GlobalOperation::IndexRead::IndexRead(GlobalOperation &operation,
                                      const Structure::Identity &identity,
                                      Database::Context &&context,
                                      Structure::Layout::DatabaseAccess &&blockRead) : operation(
        operation),
                                                                                       identity(
                                                                                               identity),
                                                                                       state(State_Initialize),
                                                                                       indexContext(
                                                                                               std::move(
                                                                                                       context)),
                                                                                       blockRead(
                                                                                               std::move(
                                                                                                       blockRead))
{ }

GlobalOperation::IndexRead::~IndexRead() = default;

void GlobalOperation::IndexRead::start()
{
    indexContext.result.connect(std::bind(&IndexRead::indexResult, this, std::placeholders::_1));
    indexContext.complete.connect(std::bind(&IndexRead::indexComplete, this));

    {
        std::lock_guard<std::mutex> lock(operation.mutex);
        Q_ASSERT(state == State_Initialize);
        state = State_ReadingIndex;
    }
    indexContext.begin();
}

void GlobalOperation::IndexRead::indexResult(const Database::Types::Results &results)
{
    auto rptr = results.cbegin();

    Q_ASSERT(rptr != results.cend());
    Structure::Block block = static_cast<Structure::Block>(rptr->toInteger());
    ++rptr;

    Q_ASSERT(rptr != results.cend());
    double modified = operation.layout.modifiedValue(*rptr);
    ++rptr;

    Structure::Identity identity = this->identity;
    if (!operation.layout.splitStation()) {
        Q_ASSERT(rptr != results.cend());
        Q_ASSERT(identity.station == 0 ||
                         identity.station ==
                                 static_cast<Structure::Identity::index>(rptr->toInteger()));
        identity.station = static_cast<Structure::Identity::index>(rptr->toInteger());
        ++rptr;
    }
    if (!operation.layout.splitArchive()) {
        Q_ASSERT(rptr != results.cend());
        Q_ASSERT(identity.archive == 0 ||
                         identity.archive ==
                                 static_cast<Structure::Identity::index>(rptr->toInteger()));
        identity.archive = static_cast<Structure::Identity::index>(rptr->toInteger());
        ++rptr;
    }
    if (!operation.layout.splitVariable()) {
        Q_ASSERT(rptr != results.cend());
        Q_ASSERT(identity.variable == 0 ||
                         identity.variable ==
                                 static_cast<Structure::Identity::index>(rptr->toInteger()));
        identity.variable = static_cast<Structure::Identity::index>(rptr->toInteger());
        ++rptr;
    }

    Q_ASSERT(identity.station != 0);
    Q_ASSERT(identity.archive != 0);
    Q_ASSERT(identity.variable != 0);

    BlockOperation *add = createBlock(block, modified, identity);
    if (!add)
        return indexContext.next();

    bool doAdvance;
    {
        std::lock_guard<std::mutex> lock(operation.mutex);
        Q_ASSERT(state == State_ReadingIndex);
        blocks.emplace(add, std::unique_ptr<BlockOperation>(add));
        doAdvance = shouldAdvanceIndex();
        if (!doAdvance) {
            state = State_IndexPaused;
        }

        add->start();
    }

    if (doAdvance)
        indexContext.next();
}

std::string GlobalOperation::IndexRead::whereBlockRead()
{
    std::string result = Structure::Column_Block;
    result += " = :";
    result += Structure::Column_Block;
    return result;
}

void GlobalOperation::IndexRead::indexComplete()
{
    std::lock_guard<std::mutex> lock(operation.mutex);
    Q_ASSERT(state != State_Complete);
    state = State_Complete;
    /* Must retain the lock, since we can be deleted otherwise */
    operation.notify.notify_all();
}

bool GlobalOperation::IndexRead::shouldAdvanceIndex()
{
    static constexpr std::size_t maxRunningBlocks = 32;
    return blocks.size() < maxRunningBlocks;
}

void GlobalOperation::IndexRead::completeBlock(BlockOperation *block)
{
    std::lock_guard<std::mutex> lock(operation.mutex);
    bool wake = false;
    if (blocks.size() == 1) {
        Q_ASSERT(blocks.begin()->first == block);
        Q_ASSERT(blocks.begin()->second.get() == block);
        blocks.clear();

        /* Only avoid the wake if we're just waiting for the index to read more,
         * if we're complete or paused, we need to launch another one */
        wake = (state != State_ReadingIndex);
    } else {
        auto check = blocks.find(block);
        Q_ASSERT(check != blocks.end());
        Q_ASSERT(check->second.get() == block);
        blocks.erase(check);

        /* Wake if we're paused but can advance (so we can unpause) */
        if (state == State_IndexPaused && shouldAdvanceIndex())
            wake = true;
    }

    /* Must retain the lock, since we can be deleted otherwise */
    if (wake)
        operation.notify.notify_all();
}

void GlobalOperation::IndexRead::signalTerminate()
{
    indexContext.finish();
}

bool GlobalOperation::IndexRead::process(std::unique_lock<std::mutex> &lock, bool &complete)
{
    switch (state) {
    case State_Initialize:
        Q_ASSERT(false);
        return false;
    case State_ReadingIndex:
        return false;
    case State_IndexPaused:
        if (!shouldAdvanceIndex())
            return false;
        state = State_ReadingIndex;
        break;
    case State_Complete:
        if (!blocks.empty())
            return false;
        complete = true;
        return false;
    }

    Q_ASSERT(state == State_ReadingIndex);

    lock.unlock();

    indexContext.next();

    lock.lock();
    return true;
}


GlobalOperation::GlobalOperation(Structure::Layout &layout) : layout(layout),
                                                              state(State_Initialize)
{ }

GlobalOperation::~GlobalOperation()
{ shutdownDestroy(); }

void GlobalOperation::shutdownDestroy()
{
    if (thread.joinable()) {
        signalTerminate();
        thread.join();
    }
}

void GlobalOperation::start()
{
    Q_ASSERT(state == State_Initialize);
    Q_ASSERT(!thread.joinable());
    thread = std::thread(std::bind(&GlobalOperation::run, this));
}

void GlobalOperation::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state != State_Running && state != State_Initialize)
            return;
        state = State_TerminateRequested;
    }
    notify.notify_all();
}

bool GlobalOperation::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, mutex, notify,
                                     [this] { return state == State_Complete; });
}

bool GlobalOperation::isComplete()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == State_Complete;
}

void GlobalOperation::run()
{
    auto completed = complete.defer();

    for (const auto &identity : layout.sequencePossible()) {
        IndexRead *add = createSequence(identity);
        if (!add)
            continue;
        indices.emplace_back(add);
        add->start();
    }
    for (const auto &identity : layout.erasurePossible()) {
        IndexRead *add = createErasure(identity);
        if (!add)
            continue;
        indices.emplace_back(add);
        add->start();
    }

    std::unique_lock<std::mutex> lock(mutex);
    Q_ASSERT(state == State_Initialize || state == State_TerminateRequested);

    if (indices.empty()) {
        Q_ASSERT(lock.owns_lock());
        state = State_Complete;
        notify.notify_all();
        return;
    }

    if (state != State_TerminateRequested)
        state = State_Running;
    for (;;) {
        Q_ASSERT(lock.owns_lock());

        if (state == State_Finishing)
            break;

        if (state == State_TerminateRequested) {
            state = State_Terminating;

            lock.unlock();
            terminateIndices();
            lock.lock();
            continue;
        }

        if (processIndices(lock))
            continue;

        notify.wait(lock);
    }
    Q_ASSERT(indices.empty());
    Q_ASSERT(lock.owns_lock());

    state = State_Complete;
    notify.notify_all();
}

void GlobalOperation::terminateIndices()
{
    for (const auto &index : indices) {
        index->signalTerminate();
    }
}

bool GlobalOperation::processIndices(std::unique_lock<std::mutex> &lock)
{
    if (indices.empty()) {
        state = State_Finishing;
        return true;
    }

    bool unlocked = false;

    for (auto index = indices.begin(); index != indices.end();) {
        bool complete = false;
        if ((*index)->process(lock, complete))
            unlocked = true;

        Q_ASSERT(lock.owns_lock());

        if (complete) {
            index = indices.erase(index);
            unlocked = true;
        } else {
            ++index;
        }
    }

    return unlocked;
}


FindUnusedIndexOperation::SequenceBlock::SequenceBlock(FindUnusedIndexOperation::IndexSequence &index,
                                                       FindUnusedIndexOperation &operation,
                                                       Structure::Block block,
                                                       const Structure::Identity &identity)
        : BlockOperation(index, block, identity), operation(operation), identity(identity)
{
    /* If we get created at all, we know there's a reference to the station, archive, and
     * variable.  So insert those now */
    std::lock_guard<std::mutex> lock(operation.resultMutex);
    operation.remaining.station.erase(identity.station);
    operation.remaining.archive.erase(identity.archive);
    operation.remaining.variable.erase(identity.variable);
}

FindUnusedIndexOperation::SequenceBlock::~SequenceBlock() = default;

static std::vector<quint32> readSequenceOffsets(const Util::ByteArray &uncompressed)
{

    const uchar *ptr = uncompressed.data<const uchar *>();
    quint32 n = qFromLittleEndian<quint32>(ptr);

    if (n == 0)
        return std::vector<quint32>();
    if (n >= static_cast<quint32>(uncompressed.size() / 4)) {
        qCWarning(log_datacore_archive_globaloperation) << "Data block to small for offset list";
        return std::vector<quint32>();
    }

    std::vector<quint32> offsets;
    offsets.reserve(n);
    ptr += 4;
    for (const uchar *end = ptr + 4 * n; ptr < end; ptr += 4) {
        offsets.emplace_back(qFromLittleEndian<quint32>(ptr));
    }

    return offsets;
}

void FindUnusedIndexOperation::SequenceBlock::processData(const Util::ByteArray &data)
{
    Util::ByteArray uncompressed;
    if (!BlockDecompressor().decompress(data, uncompressed)) {
        qCWarning(log_datacore_archive_globaloperation) << "Decompression failed for data block";
        return completed();
    }
    if (uncompressed.empty())
        return completed();
    if (uncompressed.size() < 4) {
        qCWarning(log_datacore_archive_globaloperation) << "Data block to small for index";
        return completed();
    }

    auto offsets = readSequenceOffsets(uncompressed);
    if (offsets.empty())
        return completed();
    if (offsets.back() > (uncompressed.size() - Structure::Existing::Minimum_Size)) {
        qCWarning(log_datacore_archive_globaloperation) << "Data block to small for offsets";
        return completed();
    }

    const uchar *ptr = uncompressed.data<const uchar *>();

    IndexContents::Component seenFlavors;
    for (auto offset : offsets) {
        Q_ASSERT(offset + Structure::Existing::Minimum_Size <=
                         static_cast<quint32>(uncompressed.size()));
        const uchar *dataPtr = ptr + offset + Structure::Existing::Offset_Flavors;
        quint16 flavors = qFromLittleEndian<quint16>(dataPtr);

        seenFlavors.emplace(flavors);
    }

    {
        std::lock_guard<std::mutex> lock(operation.resultMutex);
        for (auto rem : seenFlavors) {
            operation.remaining.flavor.erase(rem);
        }
    }

    return completed();
}

FindUnusedIndexOperation::IndexSequence::IndexSequence(FindUnusedIndexOperation &operation,
                                                       const Structure::Identity &identity)
        : IndexRead(operation, identity, createIndexContext(operation, identity),
                    createBlockRead(operation, identity)),
          operation(operation),
          identity(identity),
          haveSeenBlocks(false)
{ }

FindUnusedIndexOperation::IndexSequence::~IndexSequence()
{
    if (haveSeenBlocks)
        return;
    std::lock_guard<std::mutex> lock(operation.resultMutex);
    operation.absentSequences.insert(identity);
}

GlobalOperation::BlockOperation *FindUnusedIndexOperation::IndexSequence::createBlock(Structure::Block block,
                                                                                      double,
                                                                                      const Structure::Identity &identity)
{
    haveSeenBlocks = true;
    return new SequenceBlock(*this, operation, block, identity);
}

Database::Context FindUnusedIndexOperation::IndexSequence::createIndexContext(
        FindUnusedIndexOperation &operation,
        const Structure::Identity &identity)
{
    auto index = operation.getLayout().sequenceIndex(identity);
    return index.statement().execute(index.binds());
}

Structure::Layout::DatabaseAccess FindUnusedIndexOperation::IndexSequence::createBlockRead(
        FindUnusedIndexOperation &operation,
        const Structure::Identity &identity)
{ return operation.getLayout().sequenceRead(identity, whereBlockRead()); }

FindUnusedIndexOperation::ErasureBlock::ErasureBlock(FindUnusedIndexOperation::IndexErasure &index,
                                                     FindUnusedIndexOperation &operation,
                                                     Structure::Block block,
                                                     const Structure::Identity &identity)
        : BlockOperation(index, block, identity), operation(operation), identity(identity)
{
    /* If we get created at all, we know there's a reference to the station, archive, and
     * variable.  So insert those now */
    std::lock_guard<std::mutex> lock(operation.resultMutex);
    operation.remaining.station.erase(identity.station);
    operation.remaining.archive.erase(identity.archive);
    operation.remaining.variable.erase(identity.variable);
}

FindUnusedIndexOperation::ErasureBlock::~ErasureBlock() = default;

void FindUnusedIndexOperation::ErasureBlock::processData(const Util::ByteArray &data)
{
    Util::ByteArray uncompressed;
    if (!BlockDecompressor().decompress(data, uncompressed)) {
        qCWarning(log_datacore_archive_globaloperation) << "Decompression failed for data block";
        return completed();
    }
    if (uncompressed.empty())
        return completed();
    if (uncompressed.size() < 4) {
        qCWarning(log_datacore_archive_globaloperation) << "Data block to small for index";
        return completed();
    }

    const uchar *ptr = uncompressed.data<const uchar *>();
    const uchar *endPtr = ptr + uncompressed.size();

    IndexContents::Component seenFlavors;
    for (; ptr != endPtr; ptr += Structure::Erasure::Total_Size) {
        Q_ASSERT(ptr < endPtr);
        const uchar *dataPtr = ptr + Structure::Erasure::Offset_Flavors;
        quint16 flavors = qFromLittleEndian<quint16>(dataPtr);

        seenFlavors.emplace(flavors);
    }

    {
        std::lock_guard<std::mutex> lock(operation.resultMutex);
        for (auto rem : seenFlavors) {
            operation.remaining.flavor.erase(rem);
        }
    }

    return completed();
}

FindUnusedIndexOperation::IndexErasure::IndexErasure(FindUnusedIndexOperation &operation,
                                                     const Structure::Identity &identity)
        : IndexRead(operation, identity, createIndexContext(operation, identity),
                    createBlockRead(operation, identity)),
          operation(operation),
          identity(identity),
          haveSeenBlocks(false)
{ }

FindUnusedIndexOperation::IndexErasure::~IndexErasure()
{
    if (haveSeenBlocks)
        return;
    std::lock_guard<std::mutex> lock(operation.resultMutex);
    operation.absentErasures.insert(identity);
}

GlobalOperation::BlockOperation *FindUnusedIndexOperation::IndexErasure::createBlock(Structure::Block block,
                                                                                     double,
                                                                                     const Structure::Identity &identity)
{
    haveSeenBlocks = true;
    return new ErasureBlock(*this, operation, block, identity);
}

Database::Context FindUnusedIndexOperation::IndexErasure::createIndexContext(
        FindUnusedIndexOperation &operation,
        const Structure::Identity &identity)
{
    auto index = operation.getLayout().erasureIndex(identity);
    return index.statement().execute(index.binds());
}

Structure::Layout::DatabaseAccess FindUnusedIndexOperation::IndexErasure::createBlockRead(
        FindUnusedIndexOperation &operation,
        const Structure::Identity &identity)
{ return operation.getLayout().erasureRead(identity, whereBlockRead()); }

FindUnusedIndexOperation::FindUnusedIndexOperation(Structure::Layout &layout,
                                                   const FindUnusedIndexOperation::IndexContents &contents)
        : GlobalOperation(layout), remaining(contents)
{ }

FindUnusedIndexOperation::~FindUnusedIndexOperation()
{ shutdownDestroy(); }

GlobalOperation::IndexRead *FindUnusedIndexOperation::createSequence(const Structure::Identity &identity)
{ return new IndexSequence(*this, identity); }

GlobalOperation::IndexRead *FindUnusedIndexOperation::createErasure(const Structure::Identity &identity)
{ return new IndexErasure(*this, identity); }

FindUnusedIndexOperation::IndexContents FindUnusedIndexOperation::remainingContents() const
{ return remaining; }

FindUnusedIndexOperation::EmptyIndices FindUnusedIndexOperation::emptySequences() const
{ return absentSequences; }

FindUnusedIndexOperation::EmptyIndices FindUnusedIndexOperation::emptyErasures() const
{ return absentErasures; }


RecompressOperation::SequenceBlock::SequenceBlock(GlobalOperation::IndexRead &index,
                                                  Structure::Block block,
                                                  double modified,
                                                  const Structure::Identity &identity)
        : BlockRewrite(index, block, modified, identity)
{ }

RecompressOperation::SequenceBlock::~SequenceBlock() = default;

Database::Context RecompressOperation::SequenceBlock::createWrite(const Util::ByteArray &data,
                                                                  Structure::Block block,
                                                                  double modified,
                                                                  const Structure::Identity &identity)
{
    qCDebug(log_datacore_archive_globaloperation) << "Recompressing sequence" << identity.station
                                                  << identity.archive << identity.variable << block;

    auto write = getLayout().sequenceWrite(identity, Structure::Layout::WriteMode::KnownExisting);
    auto binds = write.binds();
    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    binds.emplace(Structure::Column_Modified, getLayout().targetModified(modified));
    {
        Util::ByteArray uncompressed;
        if (!BlockDecompressor().decompress(data, uncompressed)) {
            qCWarning(log_datacore_archive_globaloperation)
                << "Decompression failed for data block";
            return Database::Context();
        }

        Util::ByteArray
                recompressed = BlockCompressor(BlockCompressor::LZ4HC).compress(uncompressed);
        if (recompressed.empty()) {
            qCWarning(log_datacore_archive_globaloperation)
                << "Recompression failed for data block";
            return Database::Context();
        }

        binds.emplace(Structure::Column_Data, std::move(recompressed));
    }
    return write.statement().execute(binds);
}

static std::string modifiedBoundsWhere(double modifiedLower, double modifiedUpper)
{
    std::string where;
    if (FP::defined(modifiedLower)) {
        if (FP::defined(modifiedUpper)) {
            if (!where.empty())
                where += " AND ";
            where += "( ";
            where += Structure::Column_Modified;
            where += " BETWEEN :modifiedLower AND :modifiedUpper )";
        } else {
            where += "( ";
            where += Structure::Column_Modified;
            where += " >= :modifiedLower )";
        }
    } else if (FP::defined(modifiedUpper)) {
        where += "( ";
        where += Structure::Column_Modified;
        where += " <= :modifiedUpper )";
    }
    return where;
}

static void modifiedBoundsBinds(double modifiedLower,
                                double modifiedUpper,
                                const Structure::Layout &layout,
                                Database::Types::Binds &binds)
{
    if (FP::defined(modifiedLower))
        binds.emplace("modifiedLower", layout.modifiedQuery(modifiedLower));
    if (FP::defined(modifiedUpper))
        binds.emplace("modifiedUpper", layout.targetModified(modifiedUpper));
}

RecompressOperation::IndexSequence::IndexSequence(RecompressOperation &operation,
                                                  const Structure::Identity &identity) : IndexRead(
        operation, identity,
        createIndexContext(operation, identity, operation.modifiedLower, operation.modifiedUpper),
        createBlockRead(operation, identity))
{ }

RecompressOperation::IndexSequence::~IndexSequence() = default;

GlobalOperation::BlockOperation *RecompressOperation::IndexSequence::createBlock(Structure::Block block,
                                                                                 double modified,
                                                                                 const Structure::Identity &identity)
{ return new SequenceBlock(*this, block, modified, identity); }

Database::Context RecompressOperation::IndexSequence::createIndexContext(RecompressOperation &operation,
                                                                         const Structure::Identity &identity,
                                                                         double modifiedLower,
                                                                         double modifiedUpper)
{
    auto index = operation.getLayout()
                          .sequenceIndex(identity,
                                         modifiedBoundsWhere(modifiedLower, modifiedUpper));
    auto binds = index.binds();
    modifiedBoundsBinds(modifiedLower, modifiedUpper, operation.getLayout(), binds);
    return index.statement().execute(binds);
}

Structure::Layout::DatabaseAccess RecompressOperation::IndexSequence::createBlockRead(
        RecompressOperation &operation,
        const Structure::Identity &identity)
{ return operation.getLayout().sequenceRead(identity, whereBlockRead()); }

RecompressOperation::ErasureBlock::ErasureBlock(GlobalOperation::IndexRead &index,
                                                Structure::Block block,
                                                double modified,
                                                const Structure::Identity &identity) : BlockRewrite(
        index, block, modified, identity)
{ }

RecompressOperation::ErasureBlock::~ErasureBlock() = default;

Database::Context RecompressOperation::ErasureBlock::createWrite(const Util::ByteArray &data,
                                                                 Structure::Block block,
                                                                 double modified,
                                                                 const Structure::Identity &identity)
{
    qCDebug(log_datacore_archive_globaloperation) << "Recompressing erasure" << identity.station
                                                  << identity.archive << identity.variable << block;

    auto write = getLayout().erasureWrite(identity, Structure::Layout::WriteMode::KnownExisting);
    auto binds = write.binds();
    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    binds.emplace(Structure::Column_Modified, getLayout().targetModified(modified));
    {
        Util::ByteArray uncompressed;
        if (!BlockDecompressor().decompress(data, uncompressed)) {
            qCWarning(log_datacore_archive_globaloperation)
                << "Decompression failed for data block";
            return Database::Context();
        }

        Util::ByteArray
                recompressed = BlockCompressor(BlockCompressor::LZ4HC).compress(uncompressed);
        if (recompressed.empty()) {
            qCWarning(log_datacore_archive_globaloperation)
                << "Recompression failed for data block";
            return Database::Context();
        }

        binds.emplace(Structure::Column_Data, std::move(recompressed));
    }
    return write.statement().execute(binds);
}


RecompressOperation::IndexErasure::IndexErasure(RecompressOperation &operation,
                                                const Structure::Identity &identity) : IndexRead(
        operation, identity,
        createIndexContext(operation, identity, operation.modifiedLower, operation.modifiedUpper),
        createBlockRead(operation, identity))
{ }

RecompressOperation::IndexErasure::~IndexErasure() = default;

GlobalOperation::BlockOperation *RecompressOperation::IndexErasure::createBlock(Structure::Block block,
                                                                                double modified,
                                                                                const Structure::Identity &identity)
{ return new ErasureBlock(*this, block, modified, identity); }

Database::Context RecompressOperation::IndexErasure::createIndexContext(RecompressOperation &operation,
                                                                        const Structure::Identity &identity,
                                                                        double modifiedLower,
                                                                        double modifiedUpper)
{
    auto index = operation.getLayout()
                          .erasureIndex(identity,
                                        modifiedBoundsWhere(modifiedLower, modifiedUpper));
    auto binds = index.binds();
    modifiedBoundsBinds(modifiedLower, modifiedUpper, operation.getLayout(), binds);
    return index.statement().execute(binds);
}

Structure::Layout::DatabaseAccess RecompressOperation::IndexErasure::createBlockRead(
        RecompressOperation &operation,
        const Structure::Identity &identity)
{ return operation.getLayout().erasureRead(identity, whereBlockRead()); }

RecompressOperation::RecompressOperation(Structure::Layout &layout,
                                         double modifiedLower,
                                         double modifiedUpper) : GlobalOperation(layout),
                                                                 modifiedLower(modifiedLower),
                                                                 modifiedUpper(modifiedUpper)
{ }

RecompressOperation::~RecompressOperation()
{ shutdownDestroy(); }

GlobalOperation::IndexRead *RecompressOperation::createSequence(const Structure::Identity &identity)
{ return new IndexSequence(*this, identity); }

GlobalOperation::IndexRead *RecompressOperation::createErasure(const Structure::Identity &identity)
{ return new IndexErasure(*this, identity); }

}
}
}