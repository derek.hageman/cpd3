/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <cstring>
#include <limits>
#include <QtEndian>
#include <QDataStream>
#include <QLoggingCategory>

#include "writeblock.hxx"
#include "structure.hxx"
#include "stringindex.hxx"
#include "core/compression.hxx"
#include "core/timeutils.hxx"
#include "core/qtcompat.hxx"


Q_LOGGING_CATEGORY(log_datacore_archive_writeblock, "cpd3.datacore.archive.writeblock",
                   QtWarningMsg)


namespace CPD3 {
namespace Data {
namespace Archive {
namespace WriteBlock {


static double readDouble(const uchar *ptr)
{
    quint64 i = qFromLittleEndian<quint64>(ptr);
    double d;
    std::memcpy(&d, &i, 8);
    return d;
}

static void writeDouble(double v, uchar *ptr)
{
    quint64 i;
    std::memcpy(&i, &v, 8);
    qToLittleEndian<quint64>(i, ptr);
}


Context::Identity::Identity(double s, double e, std::int_fast32_t p, Structure::Identity::index f)
        : start(s), end(e), priority(p), flavors(f)
{
    if (!FP::defined(start))
        start = std::numeric_limits<double>::lowest();
    if (!FP::defined(end))
        end = std::numeric_limits<double>::lowest();
}

double Context::Identity::getStart() const
{
    if (start == std::numeric_limits<double>::lowest())
        return FP::undefined();
    return start;
}

double Context::Identity::getEnd() const
{
    if (end == std::numeric_limits<double>::lowest())
        return FP::undefined();
    return end;
}

Context::Context(StringIndex &f) : flavors(f), values(), erasures()
{ }

Context::~Context() = default;

Context::ValueData::ValueData(const SequenceValue &value) : modified(FP::undefined()),
                                                            remoteReferenced(false),
                                                            value(value.root())
{ }

void Context::add(const SequenceValue &value)
{
    values.emplace(Identity(value.getStart(), value.getEnd(), value.getPriority(),
                            flavors.insert(value.getName().getFlavorsString())), ValueData(value));
}

Context::ValueData::ValueData(const ArchiveValue &value) : modified(value.getModified()),
                                                           remoteReferenced(
                                                                   value.isRemoteReferenced()),
                                                           value(value.root())
{ }

void Context::add(const ArchiveValue &value)
{
    values.emplace(Identity(value.getStart(), value.getEnd(), value.getPriority(),
                            flavors.insert(value.getName().getFlavorsString())), ValueData(value));
}

Context::ErasureData::ErasureData(const SequenceIdentity &erase) : modified(FP::undefined()),
                                                                   remoteReferenced(false)
{ }

void Context::remove(const SequenceIdentity &erase)
{
    erasures.emplace(Identity(erase.getStart(), erase.getEnd(), erase.getPriority(),
                              flavors.insert(erase.getName().getFlavorsString())),
                     ErasureData(erase));
}

void Context::remove(const SequenceValue &erase)
{ remove(erase.getIdentity()); }

/* Always set it to the current time, unless it's explicitly an erasure */
Context::ErasureData::ErasureData(const ArchiveValue &erase) : modified(FP::undefined()),
                                                               remoteReferenced(
                                                                       erase.isRemoteReferenced())
{ }

void Context::remove(const ArchiveValue &erase)
{
    erasures.emplace(Identity(erase.getStart(), erase.getEnd(), erase.getPriority(),
                              flavors.insert(erase.getName().getFlavorsString())),
                     ErasureData(erase));
}

Context::ErasureData::ErasureData(const ArchiveErasure &erase) : modified(erase.getModified()),
                                                                 remoteReferenced(false)
{ }

void Context::remove(const ArchiveErasure &erase)
{
    erasures.emplace(Identity(erase.getStart(), erase.getEnd(), erase.getPriority(),
                              flavors.insert(erase.getName().getFlavorsString())),
                     ErasureData(erase));
}


quint8 Sequence::Element::toFlags() const
{
    quint8 flags = 0;
    if (remoteReferenced)
        flags |= Structure::Existing::Flag_RemoteReferenced;
    return flags;
}

void Sequence::Element::fromFlags(std::uint_fast8_t flags)
{
    remoteReferenced = (flags & Structure::Existing::Flag_RemoteReferenced) != 0;
}

Sequence::Sequence(const Util::ByteView &data) : modified(false)
{
    if (data.empty())
        return;
    if (!BlockDecompressor().decompress(data, raw)) {
        qCWarning(log_datacore_archive_writeblock) << "Decompression failed for data block";
        return;
    }
    if (raw.empty())
        return;
    if (raw.size() < 4) {
        qCWarning(log_datacore_archive_writeblock) << "Data block to small for index";
        return;
    }

    std::vector<quint32> offsets;
    {
        const uchar *ptr = raw.data<const uchar *>();
        quint32 n = qFromLittleEndian<quint32>(ptr);
        if (n == 0)
            return;
        if (n >= static_cast<quint32>(raw.size() / 4)) {
            qCWarning(log_datacore_archive_writeblock) << "Data block to small for offset list";
            return;
        }

        offsets.reserve(n);
        ptr += 4;
        for (const uchar *end = ptr + 4 * n; ptr < end; ptr += 4) {
            offsets.emplace_back(qFromLittleEndian<quint32>(ptr));
        }
    }

    Q_ASSERT(!offsets.empty());
    if (offsets.back() > (raw.size() - Structure::Existing::Minimum_Size)) {
        qCWarning(log_datacore_archive_writeblock) << "Data block to small for offsets";
        return;
    }

    const uchar *ptr = raw.data<const uchar *>();
    for (auto oi = offsets.cbegin(), endO = offsets.cend(); oi != endO; ++oi) {
        std::size_t offset = *oi;
        Q_ASSERT(
                offset + Structure::Existing::Minimum_Size <= static_cast<std::size_t>(raw.size()));

        const uchar *dataPtr = ptr + offset;
        double start = readDouble(dataPtr);
        dataPtr += 8;
        double end = readDouble(dataPtr);
        dataPtr += 8;
        Structure::Identity::index flavors = qFromLittleEndian<quint16>(dataPtr);
        dataPtr += 2;
        double modified = readDouble(dataPtr);
        dataPtr += 8;
        std::int_fast32_t priority = qFromLittleEndian<qint32>(dataPtr);
        dataPtr += 4;
        std::uint_fast8_t flags = qFromLittleEndian<quint8>(dataPtr);

        if (!FP::defined(modified)) {
            qCWarning(log_datacore_archive_writeblock)
                << "Data block has an invalid modified time at" << Logging::range(start, end);
            continue;
        }

        Element e;
        e.modified = modified;
        e.fromFlags(flags);

        offset += Structure::Existing::Offset_Value;
        e.valueOffset = offset;
        auto nextOI = oi + 1;
        if (nextOI == endO) {
            e.valueLength = static_cast<std::size_t>(raw.size()) - offset;
        } else {
            e.valueLength = *nextOI - offset;
        }

        this->data.emplace(Context::Identity(start, end, priority, flavors), std::move(e));
    }
}

Sequence::Sequence() : modified(false)
{ }

Sequence::~Sequence() = default;

Sequence::Sequence(Sequence &&other) = default;

Sequence &Sequence::operator=(Sequence &&other) = default;


Util::ByteArray Sequence::outputData() const
{
    if (data.empty())
        return {};

    std::vector<Lookup::const_iterator> sorted;
    for (auto ci = data.cbegin(), endCI = data.cend(); ci != endCI; ++ci) {
        sorted.emplace_back(ci);
    }
    std::sort(sorted.begin(), sorted.end(),
              [](const Lookup::const_iterator &a, const Lookup::const_iterator &b) -> bool {
                  return a->second.modified < b->second.modified;
              });

    Util::ByteArray input;
    input.resize(4 * sorted.size() + 4);
    qToLittleEndian<quint32>(static_cast<quint32>(sorted.size()),
                             reinterpret_cast<uchar *>(input.data()));

    std::size_t offsetIndex = 0;
    for (const auto &item : sorted) {
        std::size_t offset = static_cast<std::size_t>(input.size());
        input.resize(input.size() + Structure::Existing::Offset_Value + item->second.valueLength);

        qToLittleEndian<quint32>(static_cast<quint32>(offset),
                                 reinterpret_cast<uchar *>(input.data()) + 4 + (4 * offsetIndex));
        ++offsetIndex;

        uchar *ptr = reinterpret_cast<uchar *>(input.data()) + offset;
        writeDouble(item->first.getStart(), ptr);
        ptr += 8;
        writeDouble(item->first.getEnd(), ptr);
        ptr += 8;
        qToLittleEndian<quint16>(static_cast<quint16>(item->first.flavors), ptr);
        ptr += 2;
        writeDouble(item->second.modified, ptr);
        ptr += 8;
        qToLittleEndian<qint32>(static_cast<qint32>(item->first.priority), ptr);
        ptr += 4;
        qToLittleEndian<quint8>(item->second.toFlags(), ptr);
        ptr += 1;

        Q_ASSERT((static_cast<std::ptrdiff_t>(ptr - reinterpret_cast<uchar *>(input.data())) -
                offset) == Structure::Existing::Offset_Value);
        Q_ASSERT(reinterpret_cast<uchar *>(input.data()) + input.size() >=
                         ptr + item->second.valueLength);
        Q_ASSERT(item->second.valueOffset + item->second.valueLength <=
                         static_cast<std::size_t>(raw.size()));

        std::memcpy(ptr, raw.data(item->second.valueOffset), item->second.valueLength);
    }

    return BlockCompressor(BlockCompressor::LZ4).compress(input);
}

double Sequence::latestModified() const
{
    double m = FP::undefined();
    for (const auto &e : data) {
        double add = e.second.modified;
        Q_ASSERT(FP::defined(add));
        if (!FP::defined(m)) {
            m = add;
            continue;
        }
        if (add > m)
            m = add;
    }
    return m;
}

void Sequence::inject(const Context &ctx)
{
    if (ctx.values.empty())
        return;
    modified = true;

    Util::ByteArray::Device dev(raw);
    dev.open(QIODevice::WriteOnly);
    QDataStream stream(&dev);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setVersion(QDataStream::Qt_4_5);
    dev.seek(static_cast<qint64>(raw.size()));

    for (const auto &add : ctx.values) {
        Q_ASSERT(data.count(add.first) == 0);

        std::size_t offset = static_cast<std::size_t>(raw.size());
        stream << add.second.value;

        Element e;
        e.modified = add.second.modified;
        if (!FP::defined(e.modified))
            e.modified = Time::time();
        e.remoteReferenced = add.second.remoteReferenced;
        e.valueOffset = offset;
        e.valueLength = static_cast<std::size_t>(raw.size()) - offset;

        data.emplace(add.first, std::move(e));
    }
}

Sequence::RemoteReferenceContext::RemoteReferenceContext(StringIndex &flavors) : Context(flavors),
                                                                                 refs()
{ }

Sequence::RemoteReferenceContext::~RemoteReferenceContext() = default;

void Sequence::RemoteReferenceContext::reference(const SequenceIdentity &value)
{
    refs.emplace(value.getStart(), value.getEnd(), value.getPriority(),
                 flavors.insert(value.getName().getFlavorsString()));
}

void Sequence::RemoteReferenceContext::reference(const SequenceValue &value)
{ reference(value.getIdentity()); }

void Sequence::RemoteReferenceContext::reference(const ArchiveValue &value)
{ reference(value.getIdentity()); }

void Sequence::remoteReferenced(const RemoteReferenceContext &ctx)
{
    for (const auto &add : ctx.refs) {
        auto m = data.find(add);
        if (m == data.end())
            continue;

        if (m->second.remoteReferenced)
            continue;

        m->second.remoteReferenced = true;
        modified = true;
    }
}


Erasure::Erasure(const Util::ByteView &data) : modified(false)
{
    if (data.empty())
        return;
    Util::ByteArray raw;
    if (!BlockDecompressor().decompress(data, raw)) {
        qCWarning(log_datacore_archive_writeblock) << "Decompression failed for data block";
        return;
    }
    if (raw.empty())
        return;
    if (raw.size() % Structure::Erasure::Total_Size != 0) {
        qCWarning(log_datacore_archive_writeblock) << "Invalid decompressed size";
        return;
    }

    const uchar *dataPtr = raw.data<const uchar *>();
    const uchar *endPtr = dataPtr + raw.size();

    for (; dataPtr != endPtr;) {
        Q_ASSERT(dataPtr < endPtr);

        double start = readDouble(dataPtr);
        dataPtr += 8;
        double end = readDouble(dataPtr);
        dataPtr += 8;
        Structure::Identity::index flavors = qFromLittleEndian<quint16>(dataPtr);
        dataPtr += 2;
        double modified = readDouble(dataPtr);
        dataPtr += 8;
        std::int_fast32_t priority = qFromLittleEndian<qint32>(dataPtr);
        dataPtr += 4;

        if (!FP::defined(modified)) {
            qCWarning(log_datacore_archive_writeblock)
                << "Data block has an invalid modified time at" << Logging::range(start, end);
            continue;
        }

        Element e;
        e.modified = modified;

        this->data.emplace(Context::Identity(start, end, priority, flavors), std::move(e));
    }
}

Erasure::Erasure() : modified(false)
{ }

Erasure::~Erasure() = default;

Erasure::Erasure(Erasure &&other) = default;

Erasure &Erasure::operator=(Erasure &&other) = default;

Util::ByteArray Erasure::outputData() const
{
    if (data.empty())
        return {};

    std::vector<Lookup::const_iterator> sorted;
    for (auto ci = data.cbegin(), endCI = data.cend(); ci != endCI; ++ci) {
        sorted.emplace_back(ci);
    }
    std::sort(sorted.begin(), sorted.end(),
              [](const Lookup::const_iterator &a, const Lookup::const_iterator &b) -> bool {
                  return a->second.modified < b->second.modified;
              });

    Util::ByteArray input;
    input.resize(sorted.size() * Structure::Erasure::Total_Size);
    uchar *ptr = reinterpret_cast<uchar *>(input.data());
    for (const auto &item : sorted) {
        writeDouble(item->first.getStart(), ptr);
        ptr += 8;
        writeDouble(item->first.getEnd(), ptr);
        ptr += 8;
        qToLittleEndian<quint16>(static_cast<quint16>(item->first.flavors), ptr);
        ptr += 2;
        writeDouble(item->second.modified, ptr);
        ptr += 8;
        qToLittleEndian<qint32>(static_cast<qint32>(item->first.priority), ptr);
        ptr += 4;
    }
    Q_ASSERT(ptr == (reinterpret_cast<uchar *>(input.data()) + input.size()));

    return BlockCompressor(BlockCompressor::LZ4).compress(input);
}

double Erasure::latestModified() const
{
    double m = FP::undefined();
    for (const auto &e : data) {
        double add = e.second.modified;
        Q_ASSERT(FP::defined(add));
        if (!FP::defined(m)) {
            m = add;
            continue;
        }
        if (add > m)
            m = add;
    }
    return m;
}

void Erasure::inject(const Context &ctx)
{
    if (ctx.erasures.empty())
        return;
    modified = true;

    for (const auto &add : ctx.erasures) {
        Q_ASSERT(data.count(add.first) == 0);

        Element e;
        e.modified = add.second.modified;
        if (!FP::defined(e.modified))
            e.modified = Time::time();

        data.emplace(add.first, std::move(e));
    }
}


/*
 * Write process:
 *  Remove erasures matching sequence to be written
 *  Step through existing erasures:
 *      Incoming sequence matches -> Remove from existing erasures -> Flag networked
 *  Step through existing sequence:
 *      Incoming erasure matches -> Remove -> Add to erasure list if networked
 *      Existing value matches incoming -> Remove existing (needs rewrite)
 *      Preserve target for new (smallest)
 *  Step through existing erasures (erasure list non-empty):
 *     Existing erasure matches incoming (now filtered) -> Update modified
 *     No existing -> No action (write later)
 *     Preserve target of new (smallest)
 *  Write out remaining to smallest targets
 */

void Context::writeStageOne()
{
    /* Remove any incoming erasures that have values for them */
    for (const auto &v : values) {
        erasures.erase(v.first);
    }
    /* For writes, this is a flag indicating that it should be written, so force it to
     * unset */
    for (auto &e : erasures) {
        e.second.remoteReferenced = false;
    }
}

void Erasure::writeStageTwo(Context &ctx)
{
    /* Remove any erasures that have values that are being inserted, and flag those
     * values as remote referenced */
    for (auto &v : ctx.values) {
        auto e = data.find(v.first);
        if (e == data.end())
            continue;
        data.erase(e);
        modified = true;
        v.second.remoteReferenced = true;
    }

    /* Set the remote referenced flag for any incoming erasures that already have
     * erasures, so they get preserved and updated if required */
    for (auto &e : ctx.erasures) {
        if (data.count(e.first) == 0)
            continue;
        e.second.remoteReferenced = true;
    }
}

void Sequence::writeStageThree(Context &ctx)
{
    /* Remove any values that are being erased, and flag the erasures if the existing
     * value had a remote reference */
    for (auto &e : ctx.erasures) {
        auto v = data.find(e.first);
        if (v == data.end())
            continue;

        if (v->second.remoteReferenced)
            e.second.remoteReferenced = true;

        data.erase(v);
        modified = true;
    }

    /* Remove any values that are being replaced (they'll be rewritten by an inject) */
    for (auto &i : ctx.values) {
        Q_ASSERT(ctx.erasures.count(i.first) == 0);

        auto l = data.find(i.first);
        if (l == data.end())
            continue;

        /* Once a value has been remote referenced, it's always so */
        if (l->second.remoteReferenced)
            i.second.remoteReferenced = true;

        data.erase(l);
        modified = true;
    }
}

void Context::writeStageFour()
{
    /* Remove any erasures that are not remote referenced, since we only need to write
     * out the ones that where */
    for (auto e = erasures.begin(); e != erasures.end();) {
        if (e->second.remoteReferenced) {
            ++e;
            continue;
        }

        e = erasures.erase(e);
    }
}

void Erasure::writeStageFive(Context &ctx)
{
    /* Update any existing erasures */
    for (auto i = ctx.erasures.begin(); i != ctx.erasures.end();) {
        auto l = data.find(i->first);

        /* No existing, so no action (full inject) */
        if (l == data.end()) {
            ++i;
            continue;
        }

        /* Local matches, so update it */

        Q_ASSERT(FP::defined(l->second.modified));

        /* If it has no modified time, then it's just a duplication request
         * (removing something that was already marked as removed), so no
         * action required.  If the modified time matches, then it's also
         * a duplicate, so nothing needed. */
        if (FP::defined(i->second.modified) && l->second.modified != i->second.modified) {
            l->second.modified = i->second.modified;
            modified = true;
        }

        i = ctx.erasures.erase(i);
    }
}


/*
 * *
 * Synchronize process:
 *  Select newest from incoming between sequence and erasure
 *  Step through existing erasures:
 *      Incoming sequence older -> Remove incoming sequence (implicit network flag)
 *      Incoming sequence newer -> Remove local erasure (implicit network flag)
 *      Local exists but older -> Update -> Remove from incoming (implicit sequence does not exist)
 *      Local exists but newer -> Remove from incoming (implicit sequence does not exist)
 *      Local does not exist -> Preserve (needs compare with local sequence)
 *  Step through existing sequence:
 *      Incoming erasure older -> Flag network -> Remove incoming erasure
 *      Incoming erasure newer -> Remove -> Preserve incoming erasure
 *      Incoming erasure with no match -> Preserve erasure (implicit network flag)
 *      Incoming sequence older -> Flag network -> Remove from incoming (handled)
 *      Incoming sequence newer -> Remove local (rewrite)
 *      Local does not exist -> Preserve on incoming (write out later)
 *      Preserve target for new (smallest)
 *  Write out remaining to smallest targets
 */

void Context::synchronizeStageOne()
{
    /* Remove any erasures that are older than values */
    for (auto &v : values) {
        v.second.remoteReferenced = true;
        if (!FP::defined(v.second.modified))
            v.second.modified = Time::time();

        auto e = erasures.find(v.first);
        if (e == erasures.end())
            continue;

        if (!FP::defined(e->second.modified) || e->second.modified > v.second.modified)
            continue;
        erasures.erase(e);
    }

    /* Remove any values that are older than erasures */
    for (auto &e : erasures) {
        e.second.remoteReferenced = true;
        if (!FP::defined(e.second.modified))
            e.second.modified = Time::time();

        auto v = values.find(e.first);
        if (v == values.end())
            continue;
        Q_ASSERT(FP::defined(v->second.modified));

        if (v->second.modified >= e.second.modified)
            continue;
        values.erase(v);
    }
}

void Erasure::synchronizeStageTwo(Context &ctx)
{
    /* Compare incoming values against the local erasures */
    for (auto v = ctx.values.begin(); v != ctx.values.end();) {
        auto e = data.find(v->first);

        /* No local erasure, so no action required */
        if (e == data.end()) {
            Q_ASSERT(v->second.remoteReferenced);
            ++v;
            continue;
        }

        Q_ASSERT(FP::defined(v->second.modified));
        Q_ASSERT(FP::defined(e->second.modified));

        /* Incoming newer, so remove the local erasure */
        if (v->second.modified >= e->second.modified) {
            Q_ASSERT(v->second.remoteReferenced);
            data.erase(e);
            modified = true;
            ++v;
            continue;
        }

        /* Incoming older, so remove the incoming (the erasure already indicates
         * a remote reference flag) */
        Q_ASSERT(v->second.modified < e->second.modified);

        v = ctx.values.erase(v);
    }

    /* Compare incoming erasures against local */
    for (auto i = ctx.erasures.begin(); i != ctx.erasures.end();) {
        auto l = data.find(i->first);

        /* No local erasure, do nothing yet, since it still needs to be compared
         * against the sequence values */
        if (l == data.end()) {
            ++i;
            continue;
        }

        Q_ASSERT(FP::defined(i->second.modified));
        Q_ASSERT(FP::defined(l->second.modified));

        /* If there's a local erasure, we know there's no local value so we can
         * act on it fully now (we don't need it to compare to the values anymore) */
        Q_ASSERT(ctx.values.count(l->first) == 0);

        /* Local newer, so remove the incoming since we don't need to do anything */
        if (l->second.modified >= i->second.modified) {
            i = ctx.erasures.erase(i);
            continue;
        }

        /* Incoming newer, so update the local and remove from the incoming */
        Q_ASSERT(l->second.modified < i->second.modified);

        l->second.modified = i->second.modified;
        i = ctx.erasures.erase(i);
        modified = true;
    }
}

void Sequence::synchronizeStageThree(Context &ctx)
{
    /* Compare incoming erasures with local values */
    for (auto e = ctx.erasures.begin(); e != ctx.erasures.end();) {
        auto v = data.find(e->first);

        /* No local value, so no action required */
        if (v == data.end()) {
            ++e;
            continue;
        }

        Q_ASSERT(FP::defined(v->second.modified));
        Q_ASSERT(FP::defined(e->second.modified));

        /* Erasure newer, so remove the local value */
        if (v->second.modified < e->second.modified) {
            data.erase(v);
            modified = true;
            ++e;
            continue;
        }

        /* Local newer, so flag it referenced if required and drop the erasure */
        Q_ASSERT(v->second.modified >= e->second.modified);

        if (!v->second.remoteReferenced) {
            v->second.remoteReferenced = true;
            modified = true;
        }

        e = ctx.erasures.erase(e);
    }
    /* Any erasures that have survived are completely new: we've already done direct
     * replacement on any existing ones and now we've checked them against the values */

    /* Compare incoming values with local */
    for (auto i = ctx.values.begin(); i != ctx.values.end();) {
        auto l = data.find(i->first);

        Q_ASSERT(ctx.erasures.count(i->first) == 0);

        /* No local value, so no action required */
        if (l == data.end()) {
            ++i;
            continue;
        }

        Q_ASSERT(FP::defined(i->second.modified));
        Q_ASSERT(FP::defined(l->second.modified));

        /* Local newer, so remove the incoming */
        if (l->second.modified >= i->second.modified) {
            /* Flag it referenced if it wasn't */
            if (!l->second.remoteReferenced) {
                l->second.remoteReferenced = true;
                modified = true;
            }

            i = ctx.values.erase(i);
            continue;
        }

        /* Incoming newer, so remove it from local for the inject later; we already
         * know there's no erasure for it, since the value itself exists */
        Q_ASSERT(l->second.modified < i->second.modified);
        Q_ASSERT(ctx.erasures.count(l->first) == 0);
        Q_ASSERT(i->second.remoteReferenced);

        data.erase(l);
        modified = true;
        ++i;
    }
}


}
}
}
}