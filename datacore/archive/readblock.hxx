/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREARCHIVE_READBLOCK_HXX
#define CPD3DATACOREARCHIVE_READBLOCK_HXX

#include "core/first.hxx"

#include <unordered_set>
#include <QByteArray>

#include "datacore/datacore.hxx"
#include "datacore/stream.hxx"
#include "datacore/stream.hxx"

namespace CPD3 {
namespace Data {
namespace Archive {

class StringIndex;

namespace SequenceFilter {
class Block;
}

namespace ReadBlock {

/**
 * Read the sequence values from a data block.
 *
 * @param data          the raw block data
 * @param baseName      the base sequence name of the block
 * @param flavors       the flavors index
 * @param filter        the filter to apply
 * @return              the resulting values
 */
CPD3DATACORE_EXPORT SequenceValue::Transfer sequence(const Util::ByteView &data,
                                                     const SequenceName &baseName,
                                                     StringIndex &flavors,
                                                     SequenceFilter::Block &filter);

/**
 * Read the full archive values from a data block.
 *
 * @param data          the raw block data
 * @param baseName      the base sequence name of the block
 * @param flavors       the flavors index
 * @param filter        the filter to apply
 * @return              the resulting values
 */
CPD3DATACORE_EXPORT ArchiveValue::Transfer archive(const Util::ByteView &data,
                                                   const SequenceName &baseName,
                                                   StringIndex &flavors,
                                                   SequenceFilter::Block &filter);

/**
 * Read the unique sequence names contained in a data block.
 *
 * @param data          the raw block data
 * @param baseName      the base sequence name of the block
 * @param flavors       the flavors index
 * @param filter        the filter to apply
 * @return              the resulting sequence names
 */
CPD3DATACORE_EXPORT SequenceName::Set names(const Util::ByteView &data,
                                            const SequenceName &baseName,
                                            StringIndex &flavors,
                                            SequenceFilter::Block &filter);

/**
 * Read the erasure values from a data block.
 *
 * @param data          the raw block data
 * @param baseName      the base sequence name of the block
 * @param flavors       the flavors index
 * @param filter        the filter to apply
 * @return              the resulting erasures
 */
CPD3DATACORE_EXPORT ArchiveErasure::Transfer erasure(const Util::ByteView &data,
                                                     const SequenceName &baseName,
                                                     StringIndex &flavors,
                                                     SequenceFilter::Block &filter);

}


}
}
}

#endif //CPD3DATACOREARCHIVE_READBLOCK_HXX
