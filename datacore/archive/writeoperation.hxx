/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREARCHIVE_WRITEOPERATION_HXX
#define CPD3DATACOREARCHIVE_WRITEOPERATION_HXX

#include "core/first.hxx"

#include <unordered_map>
#include <map>
#include <memory>
#include <mutex>
#include <condition_variable>

#include "datacore/datacore.hxx"
#include "datacore/stream.hxx"
#include "core/number.hxx"
#include "structure.hxx"
#include "writeblock.hxx"

namespace CPD3 {

namespace Database {
class Connection;
}

namespace Data {
namespace Archive {

struct IdentityIndex;

namespace Structure {
class Layout;
}

namespace WriteBlock {
class Context;
}

/**
 * The base class for write operations.  This handles coordination between the blocks
 * that need to be written and their threading.
 */
class CPD3DATACORE_EXPORT WriteOperation {

    Database::Connection *connection;
    Structure::Layout &layout;
    IdentityIndex &index;

protected:

    /**
     * The interface for operations performed on a single (positive) block of data.
     */
    struct BlockOperations;

    friend struct BlockOperations;

    /**
     * The interface for operations performed on a global (negative) set of blocks.
     */
    struct GlobalOperations;

    friend struct GlobalOperations;

    typedef std::shared_ptr<WriteBlock::Context> BlockContext;

private:

    struct Sequence {
        BlockContext global;
        std::shared_ptr<BlockOperations> blockOperations;
        std::map<Structure::Block, BlockContext> blocks;
    };

    bool sorted;

    std::size_t inFlight;
    std::mutex flightMutex;
    std::condition_variable flightNotify;
    Threading::PoolRun operationRun;

    std::unordered_map<Structure::Identity, std::shared_ptr<Sequence>> working;
    std::unordered_map<SequenceName, std::shared_ptr<Sequence>> lookup;

    WriteBlock::Context &context(Sequence &sequence, double start, double end);

    void completeWrite();

    void launchGlobal(const std::shared_ptr<GlobalOperations> &operations,
                      const BlockContext &context);

    void launchBlock(const std::shared_ptr<BlockOperations> &operations,
                     Structure::Block block,
                     const BlockContext &context);

protected:
    /**
     * Create the write operation.
     *
     * @param connection    the archive connection
     * @param layout        the archive layout
     * @param index         the archive index
     * @param sorted        true if incoming values are time sorted
     */
    WriteOperation(Database::Connection *connection,
                   Structure::Layout &layout,
                   IdentityIndex &index,
                   bool sorted = false);

    /**
     * Get the write context for a given name, start, and end of that a value should
     * be placed into.
     *
     * @param name          the sequence name
     * @param start         the value start
     * @param end           the value end
     * @return              the target context
     */
    WriteBlock::Context &context(const SequenceName &name, double start, double end);

    /**
     * Create a context for handling values.
     *
     * @return          the value context
     */
    virtual BlockContext createContext();

    /**
     * Advance the write operation to the specified time, indicating that no
     * further values will start before that time.
     *
     * @param time  the time to advance to
     */
    void advance(double time);

    /**
     * Get the archive layout.
     *
     * @return      the archive layout
     */
    inline const Structure::Layout &getLayout() const
    { return layout; }

    /**
     * Get the archive index.
     *
     * @return      the archive index
     */
    inline IdentityIndex &getIndex()
    { return index; }

    /**
     * Perform the actual block write.
     *
     * @param operations    the operations handle
     * @param block         the block number
     * @param context       the block context
     */
    virtual void writeBlock(const BlockOperations &operations,
                            Structure::Block block,
                            WriteBlock::Context &context) = 0;

    /**
     * Perform the actual global write.
     *
     * @param operations    the operations handle
     * @param context       the global context
     */
    virtual void writeGlobal(const GlobalOperations &operations, WriteBlock::Context &context) = 0;

public:
    virtual ~WriteOperation();

    WriteOperation() = delete;

    WriteOperation(const WriteOperation &) = delete;

    WriteOperation &operator=(const WriteOperation &) = delete;

    /**
     * Complete the operation, launching all remaining blocks and waiting for completion.
     */
    void complete();

};


/**
 * The write operation for conventional sequences of data.
 */
class CPD3DATACORE_EXPORT WriteSequence final : public WriteOperation {
public:
    /**
     * Create the write operation.
     *
     * @param connection    the archive connection
     * @param layout        the archive layout
     * @param index         the archive index
     * @param sorted        true if incoming values are time sorted
     */
    WriteSequence(Database::Connection *connection,
                  Structure::Layout &layout,
                  IdentityIndex &index,
                  bool sorted = false);

    virtual ~WriteSequence();

    /**
     * Add or modify a value.
     *
     * @param value the value
     */
    void add(const SequenceValue &value);

    /**
     * Add or modify a value.
     *
     * @param value the value
     */
    void add(const ArchiveValue &value);

    /**
     * Add or modify the values defined by a sequential iterator range.
     *
     * @tparam Iterator the iterator type
     * @param begin     the start iterator
     * @param end       the end iterator
     */
    template<typename Iterator>
    void add(Iterator begin, Iterator end)
    {
        for (; begin != end; ++begin) {
            add(*begin);
        }
    }

    /**
     * Add or modify all values in a container.
     *
     * @tparam Container    the container type
     * @param container     the container
     */
    template<typename Container>
    void add(const Container &container)
    { add(container.begin(), container.end()); }


    /**
     * Remove a value.
     *
     * @param erase the value to remove
     */
    void remove(const SequenceIdentity &erase);

    /**
     * Remove a value.
     *
     * @param erase the value to remove
     */
    void remove(const SequenceValue &erase);

    /**
     * Remove a value.
     *
     * @param erase the value to remove
     */
    void remove(const ArchiveValue &erase);

    /**
     * Remove a value.
     *
     * @param erase the value to remove
     */
    void remove(const ArchiveErasure &erase);

    /**
     * Remove the values defined by a sequential iterator range.
     *
     * @tparam Iterator the iterator type
     * @param begin     the start iterator
     * @param end       the end iterator
     */
    template<typename Iterator>
    void remove(Iterator begin, Iterator end)
    {
        for (; begin != end; ++begin) {
            remove(*begin);
        }
    }

    /**
     * Remove all values in a container.
     *
     * @tparam Container    the container type
     * @param container     the container
     */
    template<typename Container>
    void remove(const Container &container)
    { remove(container.begin(), container.end()); }

protected:
    void writeBlock(const BlockOperations &operations,
                            Structure::Block block,
                            WriteBlock::Context &context) override;

    void writeGlobal(const GlobalOperations &operations, WriteBlock::Context &context) override;
};


/**
 * The write operation for synchronization operations.  This will not overwrite local
 * values if they are modified after the incoming ones, but will flag them as networked.
 */
class CPD3DATACORE_EXPORT WriteSynchronize final : public WriteOperation {
public:
    /**
     * Create the write operation.
     *
     * @param connection    the archive connection
     * @param layout        the archive layout
     * @param index         the archive index
     * @param sorted        true if incoming values are time sorted
     */
    WriteSynchronize(Database::Connection *connection,
                     Structure::Layout &layout,
                     IdentityIndex &index,
                     bool sorted = false);

    virtual ~WriteSynchronize();

    /**
     * Add or modify a value.
     *
     * @param value the value
     */
    void add(const ArchiveValue &value);

    /**
     * Add or modify the values defined by a sequential iterator range.
     *
     * @tparam Iterator the iterator type
     * @param begin     the start iterator
     * @param end       the end iterator
     */
    template<typename Iterator>
    void add(Iterator begin, Iterator end)
    {
        for (; begin != end; ++begin) {
            add(*begin);
        }
    }

    /**
     * Add or modify all values in a container.
     *
     * @tparam Container    the container type
     * @param container     the container
     */
    template<typename Container>
    void add(const Container &container)
    { add(container.begin(), container.end()); }


    /**
     * Remove a value.
     *
     * @param erase the value to remove
     */
    void remove(const ArchiveErasure &erase);

    /**
     * Remove the values defined by a sequential iterator range.
     *
     * @tparam Iterator the iterator type
     * @param begin     the start iterator
     * @param end       the end iterator
     */
    template<typename Iterator>
    void remove(Iterator begin, Iterator end)
    {
        for (; begin != end; ++begin) {
            remove(*begin);
        }
    }

    /**
     * Remove all values in a container.
     *
     * @tparam Container    the container type
     * @param container     the container
     */
    template<typename Container>
    void remove(const Container &container)
    { remove(container.begin(), container.end()); }

protected:
    void writeBlock(const BlockOperations &operations,
                            Structure::Block block,
                            WriteBlock::Context &context) override;

    void writeGlobal(const GlobalOperations &operations, WriteBlock::Context &context) override;
};


/**
 * A write operation that simply sets the remote referenced flag on existing values.  This
 * is used when sending data that will be remotely referenced, to ensure that local
 * erasures are retained.
 */
class CPD3DATACORE_EXPORT FlagRemoteReferenced final : public WriteOperation {
public:
    /**
     * Create the write operation.
     *
     * @param connection    the archive connection
     * @param layout        the archive layout
     * @param index         the archive index
     * @param sorted        true if incoming values are time sorted
     */
    FlagRemoteReferenced(Database::Connection *connection,
                         Structure::Layout &layout,
                         IdentityIndex &index,
                         bool sorted = false);

    virtual ~FlagRemoteReferenced();

    /**
     * Modify a value.
     *
     * @param value the value
     */
    void add(const SequenceValue &value);

    /**
     * Modify a value.
     *
     * @param value the value
     */
    void add(const ArchiveValue &value);

    /**
     * Modify a value.
     *
     * @param value the value
     */
    void add(const SequenceIdentity &value);

    /**
     * Modify the values defined by a sequential iterator range.
     *
     * @tparam Iterator the iterator type
     * @param begin     the start iterator
     * @param end       the end iterator
     */
    template<typename Iterator>
    void add(Iterator begin, Iterator end)
    {
        for (; begin != end; ++begin) {
            add(*begin);
        }
    }

    /**
     * Modify all values in a container.
     *
     * @tparam Container    the container type
     * @param container     the container
     */
    template<typename Container>
    void add(const Container &container)
    { add(container.begin(), container.end()); }

protected:
    BlockContext createContext() override;

    void writeBlock(const BlockOperations &operations,
                            Structure::Block block,
                            WriteBlock::Context &context) override;

    void writeGlobal(const GlobalOperations &operations, WriteBlock::Context &context) override;
};

}
}
}

#endif //CPD3DATACOREARCHIVE_WRITEOPERATION_HXX
