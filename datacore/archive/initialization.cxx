/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "initialization.hxx"
#include "database/connection.hxx"
#include "database/tabledefinition.hxx"

using namespace CPD3::Database;

namespace CPD3 {
namespace Data {
namespace Archive {

namespace Initialization {

static void defineTable(const Structure::Layout &layout, TableDefinition &table)
{
    std::vector<std::string> pKey;

    if (!layout.splitArchive()) {
        table.integer<Structure::Identity::strict>(Structure::Column_Archive, false);
        pKey.emplace_back(Structure::Column_Archive);
    }
    if (!layout.splitStation()) {
        table.integer<Structure::Identity::strict>(Structure::Column_Station, false);
        pKey.emplace_back(Structure::Column_Station);
    }
    if (!layout.splitVariable()) {
        table.integer<Structure::Identity::strict>(Structure::Column_Variable, false);
        pKey.emplace_back(Structure::Column_Variable);
    }

    table.integer<Structure::StrictBlock>(Structure::Column_Block, false);
    pKey.emplace_back(Structure::Column_Block);

    table.integer<std::int64_t>(Structure::Column_Modified, false);
    table.bytes(Structure::Column_Data, false);

    table.primaryKey(pKey);
}

static void defineIndices(const Structure::Layout &layout, TableDefinition &table)
{
    if (!layout.splitArchive() && !layout.splitStation()) {
        std::vector<std::string> iKey{Structure::Column_Station};

        if (!layout.splitVariable())
            iKey.emplace_back(Structure::Column_Variable);

        iKey.emplace_back(Structure::Column_Block);

        iKey.emplace_back(Structure::Column_Archive);

        table.index("Station", iKey);
    }
    if ((!layout.splitArchive() || !layout.splitStation()) && !layout.splitVariable()) {
        std::vector<std::string> iKey{Structure::Column_Variable};

        if (!layout.splitStation())
            iKey.emplace_back(Structure::Column_Station);

        iKey.emplace_back(Structure::Column_Block);

        if (!layout.splitArchive())
            iKey.emplace_back(Structure::Column_Archive);

        table.index("Variable", iKey);
    }
}

void createSequenceTable(Connection *connection,
                         const Structure::Layout &layout,
                         const Structure::Identity &identity)
{
    TableDefinition table(layout.sequenceTable(identity));
    defineTable(layout, table);
    defineIndices(layout, table);
    connection->createTable(table);
}

void createErasureTable(Connection *connection,
                        const Structure::Layout &layout,
                        const Structure::Identity &identity)
{
    TableDefinition table(layout.erasureTable(identity));
    defineTable(layout, table);
    connection->createTable(table);
}

void createStringIndexTable(Database::Connection *connection, const std::string &name)
{
    TableDefinition table(name);

    table.integer<Structure::Identity::strict>("id", false);
    table.primaryKey("id");

    table.column("value", TableDefinition::FiniteString, false, 127);
    table.index("Value", "value");

    connection->createTable(table);
}


}

}
}
}