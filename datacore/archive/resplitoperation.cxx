/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QLoggingCategory>

#include "core/qtcompat.hxx"
#include "resplitoperation.hxx"
#include "initialization.hxx"


Q_LOGGING_CATEGORY(log_datacore_archive_resplitoperation, "cpd3.datacore.archive.resplitoperation",
                   QtWarningMsg)

namespace CPD3 {
namespace Data {
namespace Archive {


ResplitOperation::ResplitOperation(Database::Connection *connection,
                                   Structure::Layout &source,
                                   Structure::Layout &target) : connection(connection),
                                                                source(source),
                                                                target(target),
                                                                state(State::Initialize)
{ }

ResplitOperation::~ResplitOperation()
{
    if (thread.joinable()) {
        signalTerminate();
        thread.join();
    }
}

void ResplitOperation::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state == State::Complete || state == State::Failed)
            return;
        state = State::Terminating;
    }
    notify.notify_all();
}

void ResplitOperation::start()
{
    Q_ASSERT(state == State::Initialize);
    Q_ASSERT(!thread.joinable());
    thread = std::thread(std::bind(&ResplitOperation::run, this));
}

bool ResplitOperation::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, mutex, notify, [this] {
        return state == State::Complete || state == State::Failed;
    });
}

bool ResplitOperation::isComplete()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == State::Complete || state == State::Failed;
}

bool ResplitOperation::isOk()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == State::Complete;
}

bool ResplitOperation::isTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == State::Terminating;
}

void ResplitOperation::run()
{
    auto completed = complete.defer();

    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state == State::Terminating) {
            state = State::Failed;
            notify.notify_all();
            return;
        }
        state = State::ConvertSequence;
    }

    if (!convertSequence()) {
        qCDebug(log_datacore_archive_resplitoperation) << "Sequence conversion failure";
        std::lock_guard<std::mutex> lock(mutex);
        state = State::Failed;
        notify.notify_all();
        return;
    }

    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state == State::Terminating) {
            state = State::Failed;
            notify.notify_all();
            return;
        }
        state = State::ConvertErasure;
    }

    if (!convertErasure()) {
        qCDebug(log_datacore_archive_resplitoperation) << "Erasure conversion failure";
        std::lock_guard<std::mutex> lock(mutex);
        state = State::Failed;
        notify.notify_all();
        return;
    }

    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state == State::Terminating) {
            state = State::Failed;
            notify.notify_all();
            return;
        }
        state = State::ReleaseOldTables;
    }

    if (!releaseOld()) {
        qCDebug(log_datacore_archive_resplitoperation) << "Release failure";
        std::lock_guard<std::mutex> lock(mutex);
        state = State::Failed;
        notify.notify_all();
        return;
    }

    std::lock_guard<std::mutex> lock(mutex);
    state = State::Complete;
    notify.notify_all();
}

bool ResplitOperation::executeConvert(const Structure::Identity &sourceIdentity,
                                      const std::string &sourceTable,
                                      const Structure::Identity &targetIdentity,
                                      const std::string &targetTable)
{
    Database::Types::Outputs sourceColumns;
    Database::Types::Outputs targetColumns;
    std::string where;
    Database::Types::Binds binds;

    if (!target.splitStation()) {
        targetColumns.emplace_back(Structure::Column_Station);

        if (!source.splitStation()) {
            sourceColumns.emplace_back(Structure::Column_Station);
        } else {
            sourceColumns.emplace_back(std::string(":") + Structure::Column_Station);

            Q_ASSERT(sourceIdentity.station != 0);
            binds.emplace(Structure::Column_Station,
                          static_cast<std::int_fast64_t>(sourceIdentity.station));
        }
    } else {
        if (!source.splitStation()) {
            if (!where.empty())
                where += " AND ";
            where += " ( ";
            where += Structure::Column_Station;
            where += " = :";
            where += Structure::Column_Station;
            where += " ) ";

            Q_ASSERT(targetIdentity.station != 0);
            binds.emplace(Structure::Column_Station,
                          static_cast<std::int_fast64_t>(targetIdentity.station));
        } else {
            Q_ASSERT(sourceIdentity.station == targetIdentity.station);
        }
    }

    if (!target.splitArchive()) {
        targetColumns.emplace_back(Structure::Column_Archive);

        if (!source.splitArchive()) {
            sourceColumns.emplace_back(Structure::Column_Archive);
        } else {
            sourceColumns.emplace_back(std::string(":") + Structure::Column_Archive);

            Q_ASSERT(sourceIdentity.archive != 0);
            binds.emplace(Structure::Column_Archive,
                          static_cast<std::int_fast64_t>(sourceIdentity.archive));
        }
    } else {
        if (!source.splitArchive()) {
            if (!where.empty())
                where += " AND ";
            where += " ( ";
            where += Structure::Column_Archive;
            where += " = :";
            where += Structure::Column_Archive;
            where += " ) ";

            Q_ASSERT(targetIdentity.archive != 0);
            binds.emplace(Structure::Column_Archive,
                          static_cast<std::int_fast64_t>(targetIdentity.archive));
        } else {
            Q_ASSERT(sourceIdentity.archive == targetIdentity.archive);
        }
    }

    if (!target.splitVariable()) {
        targetColumns.emplace_back(Structure::Column_Variable);

        if (!source.splitVariable()) {
            sourceColumns.emplace_back(Structure::Column_Variable);
        } else {
            sourceColumns.emplace_back(std::string(":") + Structure::Column_Variable);

            Q_ASSERT(sourceIdentity.variable != 0);
            binds.emplace(Structure::Column_Variable,
                          static_cast<std::int_fast64_t>(sourceIdentity.variable));
        }
    } else {
        if (!source.splitVariable()) {
            if (!where.empty())
                where += " AND ";
            where += " ( ";
            where += Structure::Column_Variable;
            where += " = :";
            where += Structure::Column_Variable;
            where += " ) ";

            Q_ASSERT(targetIdentity.variable != 0);
            binds.emplace(Structure::Column_Variable,
                          static_cast<std::int_fast64_t>(targetIdentity.variable));
        } else {
            Q_ASSERT(sourceIdentity.variable == targetIdentity.variable);
        }
    }

    targetColumns.emplace_back(Structure::Column_Block);
    sourceColumns.emplace_back(Structure::Column_Block);

    targetColumns.emplace_back(Structure::Column_Data);
    sourceColumns.emplace_back(Structure::Column_Data);

    targetColumns.emplace_back(Structure::Column_Modified);
    sourceColumns.emplace_back(target.modifiedInsertSelect(source));

    connection->insertSelect(targetTable, targetColumns, sourceTable, sourceColumns, where)
              .synchronous(binds);

    return true;
}

bool ResplitOperation::convertSequence()
{
    qCDebug(log_datacore_archive_resplitoperation) << "Starting sequence conversion";

    std::unordered_set<std::string> createdTables;

    for (const auto &sourceIdentity : source.sequencePossible()) {
        if (isTerminated())
            return false;
        auto sourceTable = source.sequenceTable(sourceIdentity);
        releaseSequence.insert(sourceTable);

        for (const auto &targetIdentity : source.sequenceUnique(sourceIdentity,
                                                                target.splitStation(),
                                                                target.splitArchive(),
                                                                target.splitVariable())) {
            if (isTerminated())
                return false;

            auto targetTable = target.sequenceTable(targetIdentity);
            if (sourceTable == targetTable) {
                qCWarning(log_datacore_archive_resplitoperation)
                    << "Resplit failed due to identical sequence tables" << sourceTable << "into"
                    << targetTable;
                return false;
            }
            Q_ASSERT(releaseSequence.count(targetTable) == 0);

            if (!createdTables.count(targetTable)) {
                qCDebug(log_datacore_archive_resplitoperation) << "Creating sequence table"
                                                               << targetTable;

                createdTables.insert(targetTable);
                if (connection->hasTable(targetTable))
                    connection->removeTable(targetTable);
                Initialization::createSequenceTable(connection, target, targetIdentity);
            }

            qCDebug(log_datacore_archive_resplitoperation) << "Converting sequence"
                                                           << targetIdentity.station
                                                           << targetIdentity.archive
                                                           << targetIdentity.variable;

            if (!executeConvert(sourceIdentity, sourceTable, targetIdentity, targetTable))
                return false;
        }
    }

    return true;
}

bool ResplitOperation::convertErasure()
{
    qCDebug(log_datacore_archive_resplitoperation) << "Starting erasure conversion";

    std::unordered_set<std::string> createdTables;

    for (const auto &sourceIdentity : source.erasurePossible()) {
        if (isTerminated())
            return false;
        auto sourceTable = source.erasureTable(sourceIdentity);
        releaseErasure.insert(sourceTable);

        for (const auto &targetIdentity : source.erasureUnique(sourceIdentity,
                                                               target.splitStation(),
                                                               target.splitArchive(),
                                                               target.splitVariable())) {
            if (isTerminated())
                return false;

            auto targetTable = target.erasureTable(targetIdentity);
            if (sourceTable == targetTable) {
                qCWarning(log_datacore_archive_resplitoperation)
                    << "Resplit failed due to identical erasure tables" << sourceTable << "into"
                    << targetTable;
                return false;
            }
            Q_ASSERT(releaseErasure.count(targetTable) == 0);

            if (!createdTables.count(targetTable)) {
                qCDebug(log_datacore_archive_resplitoperation) << "Creating erasure table"
                                                               << targetTable;

                createdTables.insert(targetTable);
                if (connection->hasTable(targetTable))
                    connection->removeTable(targetTable);
                Initialization::createErasureTable(connection, target, targetIdentity);
            }

            qCDebug(log_datacore_archive_resplitoperation) << "Converting erasure"
                                                           << targetIdentity.station
                                                           << targetIdentity.archive
                                                           << targetIdentity.variable;

            if (!executeConvert(sourceIdentity, sourceTable, targetIdentity, targetTable))
                return false;
        }
    }

    return true;
}

bool ResplitOperation::releaseOld()
{
    for (const auto &rem : releaseSequence) {
        if (isTerminated())
            return false;
        connection->removeTable(rem);
    }
    for (const auto &rem : releaseErasure) {
        if (isTerminated())
            return false;
        connection->removeTable(rem);
    }
    return true;
}

}
}
}