/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREARCHIVE_WRITEBLOCK_HXX
#define CPD3DATACOREARCHIVE_WRITEBLOCK_HXX

#include "core/first.hxx"

#include <cstddef>
#include <cstdint>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <functional>

#include "datacore/datacore.hxx"
#include "datacore/stream.hxx"
#include "datacore/stream.hxx"
#include "core/number.hxx"
#include "core/util.hxx"

namespace CPD3 {
namespace Data {
namespace Archive {

class StringIndex;

namespace WriteBlock {

class Sequence;

class Erasure;

/**
 * The shared context for a single main index write.  This context is used to track
 * the shared state between all block in a single write group.  For example, a
 * single context is used for a single station, archive, variable pair and all
 * global block, or one with a single non-global block.
 * <br>
 * In general the context is used between all the "stages" of various operations,
 * which are executed in order to handle the changed needed to sequences and
 * erasures by the complete operation.
 */
class CPD3DATACORE_EXPORT Context {
    friend class Sequence;

    friend class Erasure;

    /**
     * The unique identity of a value inside the context scope.
     */
    struct Identity {
        /**
         * The start time.
         */
        double start;

        /**
         * The end time.
         */
        double end;

        /**
         * The priority.
         */
        std::int_fast32_t priority;

        /**
         * The flavors index.
         */
        std::uint_fast16_t flavors;

        struct hash {
            inline std::size_t operator()(const Identity &identity) const
            {
                std::size_t h = std::hash<std::uint_fast16_t>()(identity.flavors);
                h = INTEGER::mix(h, std::hash<std::int_fast32_t>()(identity.priority));
                if (FP::defined(identity.start)) {
                    h = INTEGER::mix(h, std::hash<double>()(identity.start));
                }
                if (FP::defined(identity.end)) {
                    h = INTEGER::mix(h, std::hash<double>()(identity.end));
                }
                return h;
            }
        };

        inline bool operator==(const Identity &other) const
        {
            return flavors == other.flavors &&
                    priority == other.priority &&
                    FP::equal(start, other.start) &&
                    FP::equal(end, other.end);
        }

        inline bool operator!=(const Identity &other) const
        {
            return flavors != other.flavors ||
                    priority != other.priority ||
                    !FP::equal(start, other.start) ||
                    !FP::equal(end, other.end);
        }

        Identity(double start, double end, std::int_fast32_t priority, std::uint_fast16_t flavors);

        double getStart() const;

        double getEnd() const;
    };

protected:

    /**
     * The flavors index.
     */
    StringIndex &flavors;

private:

    /**
     * The data associated with a unique identity for a value add request.
     */
    struct ValueData {
        /**
         * The modification time.
         */
        double modified;

        /**
         * The remote referenced flag.
         */
        bool remoteReferenced;

        /**
         * The actual value data.
         */
        Variant::Root value;

        explicit ValueData(const SequenceValue &value);

        explicit ValueData(const ArchiveValue &value);
    };

    /**
     * The unique values in the context.
     */
    std::unordered_map<Identity, ValueData, Identity::hash> values;

    /**
     * The data associated with a unique identity for a value remove request.
     */
    struct ErasureData {
        /**
         * The modification time.
         */
        double modified;

        /**
         * The remote referenced flag.
         */
        bool remoteReferenced;

        explicit ErasureData(const SequenceIdentity &erase);

        explicit ErasureData(const ArchiveValue &erase);

        explicit ErasureData(const ArchiveErasure &erase);
    };

    /**
     * The unique values in the context.
     */
    std::unordered_map<Identity, ErasureData, Identity::hash> erasures;

public:
    Context(StringIndex &flavors);

    Context() = delete;

    virtual ~Context();

    /**
     * Add or modify a value.
     *
     * @param value the value
     */
    void add(const SequenceValue &value);

    /**
     * Add or modify a value.
     *
     * @param value the value
     */
    void add(const ArchiveValue &value);

    /**
     * Add or modify the values defined by a sequential iterator range.
     *
     * @tparam Iterator the iterator type
     * @param begin     the start iterator
     * @param end       the end iterator
     */
    template<typename Iterator>
    void add(Iterator begin, Iterator end)
    {
        for (; begin != end; ++begin) {
            add(*begin);
        }
    }

    /**
     * Add or modify all values in a container.
     *
     * @tparam Container    the container type
     * @param container     the container
     */
    template<typename Container>
    void add(const Container &container)
    { add(container.begin(), container.end()); }


    /**
     * Remove a value.
     *
     * @param erase the value to remove
     */
    void remove(const SequenceIdentity &erase);

    /**
     * Remove a value.
     *
     * @param erase the value to remove
     */
    void remove(const SequenceValue &erase);

    /**
     * Remove a value.
     *
     * @param erase the value to remove
     */
    void remove(const ArchiveValue &erase);

    /**
     * Remove a value.
     *
     * @param erase the value to remove
     */
    void remove(const ArchiveErasure &erase);

    /**
     * Remove the values defined by a sequential iterator range.
     *
     * @tparam Iterator the iterator type
     * @param begin     the start iterator
     * @param end       the end iterator
     */
    template<typename Iterator>
    void remove(Iterator begin, Iterator end)
    {
        for (; begin != end; ++begin) {
            remove(*begin);
        }
    }

    /**
     * Remove all values in a container.
     *
     * @tparam Container    the container type
     * @param container     the container
     */
    template<typename Container>
    void remove(const Container &container)
    { remove(container.begin(), container.end()); }


    /**
     * Test if the context has any (remaining) sequence values to be altered.
     * @return
     */
    inline bool hasSequence() const
    { return !values.empty(); }

    /**
     * Test if the context has any (remaining) erasure values to be altered.
     * @return
     */
    inline bool hasErasure() const
    { return !erasures.empty(); }


    /**
     * Perform the first stage of a write operation.
     */
    void writeStageOne();

    /**
     * Perform the fourth stage of a write operation.
     */
    void writeStageFour();


    /**
     * Perform the first stage of a synchronize operation.
     */
    void synchronizeStageOne();
};

/**
 * A single block of sequence values for writing.
 */
class CPD3DATACORE_EXPORT Sequence final {
    bool modified;
    Util::ByteArray raw;

    struct Element {
        double modified;
        bool remoteReferenced;
        std::size_t valueOffset;
        std::size_t valueLength;

        quint8 toFlags() const;

        void fromFlags(uint_fast8_t flags);
    };

    typedef std::unordered_map<Context::Identity, Element, Context::Identity::hash> Lookup;
    Lookup data;
public:
    /**
     * Create a sequence block associated with existing data.
     *
     * @param data  the block data
     */
    explicit Sequence(const Util::ByteView &data);

    /**
     * Create an empty new sequence block.
     */
    Sequence();

    ~Sequence();

    Sequence(Sequence &&other);

    Sequence &operator=(Sequence &&other);

    Sequence(const Sequence &) = delete;

    Sequence &operator=(const Sequence &) = delete;

    /**
     * Test if the block has been modified by the write.
     *
     * @return  true if the block has been modified
     */
    inline bool isModified() const
    { return modified; }

    /**
     * Get the output data to be written.
     *
     * @return  the output data
     */
    Util::ByteArray outputData() const;

    /**
     * Get the latest modified time of any value in the block.
     *
     * @return  the latest modified time
     */
    double latestModified() const;


    /**
     * Integrate the contents of the context into the block, so that
     * they are contained in the output data.
     *
     * @param ctx   the source context
     */
    void inject(const Context &ctx);


    /**
     * A context used to handle remote reference flagging without data
     * modification.
     */
    class CPD3DATACORE_EXPORT RemoteReferenceContext : public Context {
        std::unordered_set<Context::Identity, Context::Identity::hash> refs;

        friend class Sequence;

    public:
        RemoteReferenceContext(StringIndex &flavors);

        RemoteReferenceContext() = delete;

        virtual ~RemoteReferenceContext();

        /**
         * Indicate that a value is remotely referenced now.
         *
         * @param value the value that is remotely referenced
         */
        void reference(const SequenceIdentity &value);

        /**
         * Indicate that a value is remotely referenced now.
         *
         * @param value the value that is remotely referenced
         */
        void reference(const SequenceValue &value);

        /**
         * Indicate that a value is remotely referenced now.
         *
         * @param value the value that is remotely referenced
         */
        void reference(const ArchiveValue &value);
    };

    /**
     * Integrate the remote references into the block, so that it
     * now has remote references for any values in the context.
     *
     * @param ctx   the remote source context
     */
    void remoteReferenced(const RemoteReferenceContext &ctx);


    /**
     * Perform the third stage of a write operation.
     *
     * @param ctx   the shared context
     */
    void writeStageThree(Context &ctx);


    /**
     * Perform the the stage of a synchronize operation.
     *
     * @param ctx   the shared context
     */
    void synchronizeStageThree(Context &ctx);
};

/**
 * A single block of erasure values for writing.
 */
class CPD3DATACORE_EXPORT Erasure final {
    bool modified;
    struct Element {
        double modified;
    };
    typedef std::unordered_map<Context::Identity, Element, Context::Identity::hash> Lookup;
    Lookup data;
public:
    /**
     * Create a erasure block associated with existing data.
     *
     * @param data  the block data
     */
    explicit Erasure(const Util::ByteView &data);

    /**
     * Create an empty new erasure block.
     */
    Erasure();

    ~Erasure();

    Erasure(Erasure &&other);

    Erasure &operator=(Erasure &&other);

    Erasure(const Erasure &) = delete;

    Erasure &operator=(const Erasure &) = delete;

    /**
     * Test if the block has been modified by the write.
     *
     * @return  true if the block has been modified
     */
    inline bool isModified() const
    { return modified; }

    /**
     * Get the output data to be written.
     *
     * @return  the output data
     */
    Util::ByteArray outputData() const;

    /**
     * Get the latest modified time of any value in the block.
     *
     * @return  the latest modified time
     */
    double latestModified() const;


    /**
     * Integrate the contents of the context into the block, so that
     * they are contained in the output data.
     *
     * @param ctx   the source context
     */
    void inject(const Context &ctx);


    /**
     * Perform the second stage of a write operation.
     *
     * @param ctx   the shared context
     */
    void writeStageTwo(Context &ctx);

    /**
     * Perform the fifth stage of a write operation.
     *
     * @param ctx   the shared context
     */
    void writeStageFive(Context &ctx);


    /**
     * Perform the second stage of a synchronize operation.
     *
     * @param ctx   the shared context
     */
    void synchronizeStageTwo(Context &ctx);
};


}
}
}
}

#endif //CPD3DATACOREARCHIVE_WRITEBLOCK_HXX
