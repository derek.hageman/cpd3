/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <cstring>
#include <algorithm>
#include <QtEndian>
#include <QLoggingCategory>

#include "sequencefilter.hxx"
#include "datacore/sequencematch.hxx"

Q_LOGGING_CATEGORY(log_datacore_archive_sequencefilter, "cpd3.datacore.archive.sequencefilter",
                   QtWarningMsg)

namespace CPD3 {
namespace Data {
namespace Archive {
namespace SequenceFilter {


static void addWhereSet(const std::unordered_set<Structure::Identity::index> &keys,
                        const char *column,
                        std::string &where)
{
    if (keys.empty())
        return;
    if (!where.empty())
        where += " AND ";

    where += "( ";
    for (std::size_t i = 0, max = keys.size(); i < max; ++i) {
        if (i != 0)
            where += " OR ";

        where += " ( ";
        where += column;
        where += " = :";
        where += column;
        where += "_";
        where += std::to_string(i);
        where += " ) ";
    }
    where += ")";
}

std::string Stream::buildIndexWhere() const
{
    std::string where;

    addWhereSet(archives, Structure::Column_Archive, where);
    addWhereSet(stations, Structure::Column_Station, where);
    addWhereSet(variables, Structure::Column_Variable, where);

    if (firstBlock() >= 0) {
        if (!where.empty())
            where += " AND ";
        where += "( ( ";
        where += Structure::Column_Block;
        where += " < 0 ) OR ( ";
        where += Structure::Column_Block;
        if (lastBlock() >= 0) {
            Q_ASSERT(firstBlock() <= lastBlock());
            where += " BETWEEN :firstBlock AND :lastBlock ) )";
        } else {
            where += " >= :firstBlock ) )";
        }
    } else if (lastBlock() >= 0) {
        if (!where.empty())
            where += " AND ";
        where += "( ";
        where += Structure::Column_Block;
        where += " <= :lastBlock )";
    }

    if (FP::defined(modifiedAfter())) {
        if (!where.empty())
            where += " AND ";
        where += "( ";
        where += Structure::Column_Modified;
        where += " > :modifiedAfter )";
    }

    return where;
}

static void mergeWhereSet(const std::unordered_set<Structure::Identity::index> &keys,
                          const char *column,
                          Database::Types::Binds &binds)
{
    if (keys.empty())
        return;
    std::size_t i = 0;
    for (const auto &add : keys) {
        std::string name = column;
        name += "_";
        name += std::to_string(i);
        ++i;
        binds.emplace(name, static_cast<std::int_fast64_t>(add));
    }
}

void Stream::mergeIndexBinds(Database::Types::Binds &binds) const
{
    mergeWhereSet(stations, Structure::Column_Station, binds);
    mergeWhereSet(archives, Structure::Column_Archive, binds);
    mergeWhereSet(variables, Structure::Column_Variable, binds);

    if (firstBlock() >= 0)
        binds.emplace("firstBlock", static_cast<std::int_fast64_t>(firstBlock()));
    if (lastBlock() >= 0)
        binds.emplace("lastBlock", static_cast<std::int_fast64_t>(lastBlock()));

    if (FP::defined(modifiedAfter()))
        binds.emplace("modifiedAfter", layout.modifiedQuery(modifiedAfter()));
}

Structure::Block Stream::firstBlock() const
{ return firstPossibleBlock; }

Structure::Block Stream::lastBlock() const
{ return lastPossibleBlock; }

double Stream::modifiedAfter() const
{ return firstPossibleModified; }

static bool insideBlockRange(Structure::Block block, Structure::Block first, Structure::Block last)
{
    if (block < 0)
        return true;
    if (first >= 0 && block < first)
        return false;
    if (last >= 0 && block > last)
        return false;
    return true;
}

bool Stream::block(Structure::Block) const
{
#if 1
    /* Queries constructed already restrict the block access */
    return true;
#else
    return insideBlockRange(block, firstBlock(), lastBlock());
#endif
}

bool Stream::identity(const Structure::Identity &) const
{
#if 1
    /* Queries constructed already restrict the identity access */
    return true;
#else
    if (identity.station != 0 && !stations.empty() && stations.count(identity.station) == 0)
        return false;
    if (identity.archive != 0 && !archives.empty() && archives.count(identity.archive) == 0)
        return false;
    if (identity.variable != 0 && !variables.empty() && variables.count(identity.variable) == 0)
        return false;
    return true;
#endif
}

Stream::Dispatch::const_iterator Stream::lookup(const Structure::Identity &identity,
                                                const SequenceName &name)
{
    auto v = dispatch.find(identity);
    if (v != dispatch.end())
        return v;

    std::vector<Selection *> result;
    int i = 0;
    for (const auto &sel : selections) {
        i++;
        if (!sel->match->matches(name))
            continue;
        result.emplace_back(sel.get());
    }
    return std::move(dispatch.emplace(std::make_pair(identity, std::move(result))).first);
}

bool Stream::final(Structure::Block block,
                   double modified,
                   const Structure::Identity &identity,
                   const SequenceName &name)
{
    /* Since we merge selections based on matching time bounds, we can
     * just do the block check for timing.  That is, since each
     * selection will have exactly the same time bounds for all its
     * components, we can simply do the block check rather than
     * querying the individual components for matching times */
#if 0
    double blockStart = layout.blockStart(block);
    double blockEnd = layout.blockEnd(block);
#endif

    std::lock_guard<std::mutex> lock(mutex);

    auto check = lookup(identity, name);
    for (auto selection : check->second) {
        if (!selection->acceptBlock(block))
            continue;
        if (!selection->acceptModified(modified))
            continue;
#if 0
        if (block >= 0) {
            if (!selection->acceptTimes(name, blockStart, blockEnd))
                continue;
        }
#endif
        return true;
    }

    return false;
}

void Stream::advance(double time)
{
    std::lock_guard<std::mutex> lock(mutex);
    for (const auto &sel : selections) {
        sel->match->advance(time);
    }
}

bool Stream::Selection::acceptBlock(Structure::Block block) const
{ return insideBlockRange(block, firstBlock, lastBlock); }

bool Stream::Selection::acceptModified(double modified) const
{
    if (!FP::defined(modifiedAfter))
        return true;
    return modified > modifiedAfter;
}

bool Stream::Selection::acceptTimes(const SequenceName &name, double start, double end) const
{
    const SequenceMatch::Basic &m = *match;
    return m.matches(name, start, end);
}

Stream::Selection::Selection(const Stream::Selection &other) : firstBlock(other.firstBlock),
                                                               lastBlock(other.lastBlock),
                                                               modifiedAfter(other.modifiedAfter),
                                                               match(other.match->clone())
{ }

Stream::Selection &Stream::Selection::operator=(const Stream::Selection &other)
{
    firstBlock = other.firstBlock;
    lastBlock = other.lastBlock;
    modifiedAfter = other.modifiedAfter;
    match = other.match->clone();
    return *this;
}

static SequenceMatch::Basic::CompileOptions selectionMatchOptions()
{
    SequenceMatch::Basic::CompileOptions options;
    options.ignoreFlavors = true;
    return options;
}

Stream::Selection::Selection(Structure::Layout &layout, const Archive::Selection::List &selections)
        : firstBlock(layout.startBlock(selections.front().start)),
          lastBlock(layout.endBlock(selections.front().end)),
          modifiedAfter(layout.modifiedThreshold(selections.front().modifiedAfter)),
          match(SequenceMatch::Basic::compile(selections, selections.front().start,
                                              selections.front().end, selectionMatchOptions()))
{ }

Stream::Selection::Selection() = default;

Stream::Selection::Selection(Stream::Selection &&) = default;

Stream::Selection &Stream::Selection::operator=(Stream::Selection &&) = default;

Stream::Stream(Structure::Layout &layout) : layout(layout)
{ }


double Block::modifiedAfter() const
{ return firstPossibleModified; }

bool Block::bounds(double start, double end) const
{ return Range::intersects(start, end, firstPossibleStart, lastPossibleEnd); }

bool Block::flavors(const SequenceName::Component &flavors)
{
    std::lock_guard<std::mutex> lock(mutex);
    auto check = flavorsHit.find(flavors);
    if (check != flavorsHit.end())
        return check->second;

    SequenceName name;
    name.setFlavorsString(flavors);
    bool result = flavorsMatch->matches(name);
    flavorsHit.emplace(std::make_pair(flavors, result));
    return result;
}

Block::Dispatch::const_iterator Block::lookup(const SequenceName &name)
{
    auto v = dispatch.find(name);
    if (v != dispatch.end())
        return v;

    std::vector<Selection *> result;
    for (const auto &sel : selections) {
        if (!sel->match->matches(name))
            continue;
        result.emplace_back(sel.get());
    }
    return std::move(dispatch.emplace(std::make_pair(name, std::move(result))).first);
}

bool Block::final(double start, double end, double modified, const SequenceName &name)
{
    std::lock_guard<std::mutex> lock(mutex);
    auto check = lookup(name);
    for (auto selection : check->second) {
        if (!selection->acceptModified(modified))
            continue;
        if (!selection->acceptTimes(name, start, end))
            continue;
        return true;
    }
    return false;
}

static double readDouble(const uchar *ptr)
{
    quint64 i = qFromLittleEndian<quint64>(ptr);
    double d;
    std::memcpy(&d, &i, 8);
    return d;
}

bool Block::final(double start, double end, const uchar *modifiedPtr, const SequenceName &name)
{
    bool haveModified = false;
    double modified = 0;

    std::lock_guard<std::mutex> lock(mutex);
    auto check = lookup(name);
    for (auto selection : check->second) {
        if (FP::defined(selection->modifiedAfter)) {
            if (!haveModified) {
                modified = readDouble(modifiedPtr);
                haveModified = true;
            }
            if (modified <= selection->modifiedAfter)
                continue;
        }

        if (!selection->acceptTimes(name, start, end))
            continue;
        return true;
    }
    return false;
}

void Block::advance(double time)
{
    std::lock_guard<std::mutex> lock(mutex);
    flavorsMatch->advance(time);
    for (const auto &sel : selections) {
        sel->match->advance(time);
    }
}

bool Block::Selection::acceptModified(double modified) const
{
    if (!FP::defined(modifiedAfter))
        return true;
    return modified > modifiedAfter;
}

bool Block::Selection::acceptTimes(const SequenceName &name, double start, double end) const
{
#if 1
    /* Since we already know that the name can hit the matcher, and since
     * we only receive selections that all have the same bounds as inputs to
     * the matcher, we can just check the times */
    return Range::intersects(start, end, this->start, this->end);
#else
    const SequenceMatch::Basic &m = *match;
    return m.matches(name, start, end);
#endif
}

Block::Selection::Selection(const Block::Selection &other) : start(other.start),
                                                             end(other.end),
                                                             modifiedAfter(other.modifiedAfter),
                                                             match(other.match->clone())
{ }

Block::Selection &Block::Selection::operator=(const Block::Selection &other)
{
    start = other.start;
    end = other.end;
    modifiedAfter = other.modifiedAfter;
    match = other.match->clone();
    return *this;
}

Block::Selection::Selection(Structure::Layout &, const Archive::Selection::List &selections)
        : start(selections.front().start),
          end(selections.front().end),
          modifiedAfter(selections.front().modifiedAfter),
          match(SequenceMatch::Basic::compile(selections, start, end))
{ }

Block::Selection::Selection() = default;

Block::Selection::Selection(Block::Selection &&) = default;

Block::Selection &Block::Selection::operator=(Block::Selection &&) = default;

Block::Block() = default;


std::vector<std::unique_ptr<Input>> Input::sequence(Structure::Layout &layout,
                                                    IdentityIndex &index,
                                                    const Selection::List &selections,
                                                    std::unique_ptr<
                                                            ForgeLoopbackInput> *forgeLoopback)
{ return generate(layout, index, selections, layout.sequencePossible(), forgeLoopback); }

std::vector<std::unique_ptr<Input>> Input::erasure(Structure::Layout &layout,
                                                   IdentityIndex &index,
                                                   const Selection::List &selections)
{ return generate(layout, index, selections, layout.erasurePossible()); }

void Input::advance(double time)
{
    block.advance(time);
    stream.advance(time);
}

namespace {

class IdentityHandler {
protected:
    Structure::Layout &layout;
    IdentityIndex &index;
    Structure::Identity identity;

    virtual bool isSplit() const = 0;

    virtual Structure::Identity::index getID() const = 0;

    virtual StringIndex &getIndex() = 0;

    virtual bool selectionMatchsAll(const Selection &selection) const = 0;

    virtual std::vector<SequenceMatch::Element> compileSelections(const std::vector<
            Selection> &selections) const = 0;

    virtual SequenceName selectionMatchingName(const SequenceName::Component &component) const = 0;

private:
    Structure::Identity::index id;
    SequenceName::Component component;
    bool matchesAll;

    typedef std::unordered_set<Structure::Identity::index> PossibleComponents;
    PossibleComponents possibleComponents;

    bool firstSplit(Selection::List &selections)
    {
        Q_ASSERT(id != 0);
        component = getIndex().lookup(id);
        /* If this table is residual (i.e. not dropped from a purged name), just
         * ignore it */
        if (component.empty())
            return false;

        for (auto sel = selections.begin(); sel != selections.end();) {
            auto filter = compileSelections(Selection::List{*sel});
            if (SequenceMatch::Element::matchesAny(filter, selectionMatchingName(component))) {
                ++sel;
                continue;
            }

            sel = selections.erase(sel);
        }

        /* Actual table filtering is handled by the prefilter */
        return true;
    }

    bool firstUnsplit(Selection::List &)
    {
        Q_ASSERT(id == 0);
        return true;
    }

    bool secondSplit(const Selection::List &)
    {
        Q_ASSERT(possibleComponents.empty());
        return true;
    }

    bool secondUnsplit(const Selection::List &selections)
    {
        Q_ASSERT(id == 0);
        Q_ASSERT(possibleComponents.empty());

        /* Check to see if we've got a global matcher, in which case
         * we don't need to filter further down */
        for (const auto &check : selections) {
            if (selectionMatchsAll(check)) {
                Q_ASSERT(possibleComponents.empty());
                matchesAll = true;
                return true;
            }
        }

        auto allComponents = getIndex().lookup();
        if (allComponents.empty())
            return false;

        auto filter = compileSelections(selections);
        for (const auto &check : allComponents) {
            if (!SequenceMatch::Element::matchesAny(filter, selectionMatchingName(check.second)))
                continue;
            possibleComponents.emplace(check.first);
        }
        if (possibleComponents.size() == allComponents.size()) {
            possibleComponents.clear();
            matchesAll = true;
            return true;
        }

        return !possibleComponents.empty();
    }

public:

    IdentityHandler(Structure::Layout &layout, IdentityIndex &index) : layout(layout),
                                                                       index(index),
                                                                       identity(),
                                                                       id(0),
                                                                       component(),
                                                                       matchesAll(false)
    { }

    virtual ~IdentityHandler() = default;

    bool first(const Structure::Identity &identity, Selection::List &selections)
    {
        this->identity = identity;
        id = getID();
        if (isSplit()) {
            return firstSplit(selections);
        } else {
            return firstUnsplit(selections);
        }
    }

    bool second(const Selection::List &selections)
    {
        if (isSplit()) {
            return secondSplit(selections);
        } else {
            return secondUnsplit(selections);
        }
    }

    inline const SequenceName::Component &finalComponentName() const
    { return component; }

    typedef std::vector<PossibleComponents> ComponentSets;

    ComponentSets componentSets() const
    {
        if (isSplit() || matchesAll)
            return ComponentSets{PossibleComponents()};
        std::vector<Structure::Identity::index>
                sorted(possibleComponents.begin(), possibleComponents.end());
        std::sort(sorted.begin(), sorted.end());
        return subdivideComponentSets(std::move(sorted));
    }

private:
    static ComponentSets subdivideComponentSets(const std::vector<
            Structure::Identity::index> &input)
    {
        static constexpr std::size_t maximumSize = 32;
        if (input.size() <= maximumSize)
            return ComponentSets{PossibleComponents(input.begin(), input.end())};

        ComponentSets result;
        auto first = input.begin();
        auto mid = first + (input.size() / 2);
        auto end = input.end();

        {
            auto sd = subdivideComponentSets(std::vector<Structure::Identity::index>(first, mid));
            Q_ASSERT(!sd.empty());
            std::move(sd.begin(), sd.end(), Util::back_emplacer(result));
        }
        {
            auto sd = subdivideComponentSets(std::vector<Structure::Identity::index>(mid, end));
            Q_ASSERT(!sd.empty());
            std::move(sd.begin(), sd.end(), Util::back_emplacer(result));
        }

        return result;
    }
};

class IdentityHandlerStation : public IdentityHandler {
public:
    IdentityHandlerStation(Structure::Layout &layout, IdentityIndex &index) : IdentityHandler(
            layout, index)
    { }

    virtual ~IdentityHandlerStation() = default;

protected:
    virtual bool isSplit() const
    { return layout.splitStation(); }

    virtual Structure::Identity::index getID() const
    { return identity.station; }

    virtual StringIndex &getIndex()
    { return index.station; }

    virtual bool selectionMatchsAll(const Selection &selection) const
    { return selection.stations.empty(); }

    virtual std::vector<SequenceMatch::Element> compileSelections(const std::vector<
            Selection> &selections) const
    {
        SequenceMatch::Basic::CompileOptions options;
        options.ignoreFlavors = true;
        options.ignoreArchive = true;
        options.ignoreVariable = true;
        return SequenceMatch::Basic::compileToElements(selections, options);
    }

    virtual SequenceName selectionMatchingName(const SequenceName::Component &component) const
    { return SequenceName(component, {}, {}); }
};

class IdentityHandlerArchive : public IdentityHandler {
public:
    IdentityHandlerArchive(Structure::Layout &layout, IdentityIndex &index) : IdentityHandler(
            layout, index)
    { }

    virtual ~IdentityHandlerArchive() = default;

protected:
    virtual bool isSplit() const
    { return layout.splitArchive(); }

    virtual Structure::Identity::index getID() const
    { return identity.archive; }

    virtual StringIndex &getIndex()
    { return index.archive; }

    virtual bool selectionMatchsAll(const Selection &selection) const
    { return selection.archives.empty(); }

    virtual std::vector<SequenceMatch::Element> compileSelections(const std::vector<
            Selection> &selections) const
    {
        SequenceMatch::Basic::CompileOptions options;
        options.ignoreFlavors = true;
        options.ignoreStation = true;
        options.ignoreVariable = true;
        return SequenceMatch::Basic::compileToElements(selections, options);
    }

    virtual SequenceName selectionMatchingName(const SequenceName::Component &component) const
    { return SequenceName({}, component, {}); }
};

class IdentityHandlerVariable : public IdentityHandler {
public:
    IdentityHandlerVariable(Structure::Layout &layout, IdentityIndex &index) : IdentityHandler(
            layout, index)
    { }

    virtual ~IdentityHandlerVariable() = default;

protected:
    virtual bool isSplit() const
    { return layout.splitVariable(); }

    virtual Structure::Identity::index getID() const
    { return identity.variable; }

    virtual StringIndex &getIndex()
    { return index.variable; }

    virtual bool selectionMatchsAll(const Selection &selection) const
    { return selection.variables.empty(); }

    virtual std::vector<SequenceMatch::Element> compileSelections(const std::vector<
            Selection> &selections) const
    {
        SequenceMatch::Basic::CompileOptions options;
        options.ignoreFlavors = true;
        options.ignoreStation = true;
        options.ignoreArchive = true;
        return SequenceMatch::Basic::compileToElements(selections, options);
    }

    virtual SequenceName selectionMatchingName(const SequenceName::Component &component) const
    { return SequenceName({}, {}, component); }
};

struct SelectionMerge {
    double start;
    double end;
    double modifiedAfter;

    struct hash {
        inline std::size_t operator()(const SelectionMerge &merge) const
        {
            std::size_t h = 0;
            if (FP::defined(merge.start)) {
                h ^= std::hash<double>()(merge.start);
            }
            if (FP::defined(merge.end)) {
                h = INTEGER::mix(h, std::hash<double>()(merge.end));
            }
            if (FP::defined(merge.modifiedAfter)) {
                h = INTEGER::mix(h, std::hash<double>()(merge.modifiedAfter));
            }
            return h;
        }
    };

    inline bool operator==(const SelectionMerge &other) const
    {
        return FP::equal(start, other.start) &&
                FP::equal(end, other.end) &&
                FP::equal(modifiedAfter, other.modifiedAfter);
    }

    inline bool operator!=(const SelectionMerge &other) const
    {
        return !FP::equal(start, other.start) ||
                !FP::equal(end, other.end) ||
                !FP::equal(modifiedAfter, other.modifiedAfter);
    }

    SelectionMerge(const Selection &selection) : start(selection.getStart()),
                                                 end(selection.getEnd()),
                                                 modifiedAfter(selection.modifiedAfter)
    { }
};

}

std::vector<std::unique_ptr<Input>> Input::generate(Structure::Layout &layout,
                                                    IdentityIndex &index,
                                                    const Selection::List &selections,
                                                    const std::unordered_set<
                                                            Structure::Identity> &possible,
                                                    std::unique_ptr<
                                                            ForgeLoopbackInput> *forgeLoopback)
{
    if (possible.empty())
        return std::vector<std::unique_ptr<Input>>();
    if (selections.empty())
        return std::vector<std::unique_ptr<Input>>();

    std::string forgeLoopBackProgram;
    std::unordered_set<std::pair<StringIndex::index, StringIndex::index>> forgeLoopbackSkip;
    std::unordered_set<StringIndex::index> forgeAlwaysVariables;
    if (forgeLoopback && layout.splitStation() && layout.splitArchive()) {
        QByteArray p(qgetenv("CPD3_FORGE_LOOPBACK"));
        if (!p.isEmpty()) {
            auto parts = p.split(':');
            if (parts.size() >= 2) {
                std::unordered_set<StringIndex::index> loopbackStations;
                std::unordered_set<StringIndex::index> loopbackArchives;

                loopbackArchives.emplace(index.archive.lookup("raw", false));
                loopbackArchives.emplace(index.archive.lookup("raw_meta", false));
                loopbackArchives.emplace(index.archive.lookup("clean", false));
                loopbackArchives.emplace(index.archive.lookup("clean_meta", false));
                loopbackArchives.emplace(index.archive.lookup("avgh", false));
                loopbackArchives.emplace(index.archive.lookup("avgh_meta", false));
                loopbackArchives.emplace(index.archive.lookup("passed", false));
                loopbackArchives.emplace(index.archive.lookup("passed_meta", false));
                loopbackArchives.emplace(index.archive.lookup("events", false));
                loopbackArchives.emplace(index.archive.lookup("events_meta", false));

                if (layout.splitVariable())
                    forgeAlwaysVariables.emplace(index.variable.lookup("alias", false));

                for (const auto &station: parts[1].split(',')) {
                    auto id = index.station.lookup(station.toLower().toStdString(), false);
                    if (id == 0) {
                        continue;
                    }
                    loopbackStations.emplace(id);
                }

                qCDebug(log_datacore_archive_sequencefilter) << "Using Forge loopback" << parts[0]
                                                             << "for" << loopbackStations.size()
                                                             << "station(s)";

                for (const auto &id: possible) {
                    if (!loopbackStations.count(id.station))
                        continue;
                    if (!loopbackArchives.count(id.archive))
                        continue;
                    if (forgeAlwaysVariables.count(id.variable))
                        continue;
                    auto forgeID = std::pair<StringIndex::index, StringIndex::index>(id.station,
                                                                                     id.archive);
                    if (forgeLoopbackSkip.count(forgeID))
                        continue;

                    Selection::List effectiveSelections = selections;
                    IdentityHandlerStation station(layout, index);
                    if (!station.first(id, effectiveSelections))
                        continue;
                    IdentityHandlerArchive archive(layout, index);
                    if (!archive.first(id, effectiveSelections))
                        continue;
                    IdentityHandlerVariable variable(layout, index);
                    if (!variable.first(id, effectiveSelections))
                        continue;
                    if (effectiveSelections.empty())
                        continue;

                    forgeLoopbackSkip.emplace(std::move(forgeID));

                    if (!*forgeLoopback) {
                        forgeLoopback->reset(new ForgeLoopbackInput);
                        (*forgeLoopback)->program = parts[0].toStdString();
                    }
                    for (auto sel: effectiveSelections) {
                        sel.stations.clear();
                        sel.stations.push_back(index.station.lookup(id.station));
                        sel.archives.clear();
                        sel.archives.push_back(index.archive.lookup(id.archive));
                        (*forgeLoopback)->selections.push_back(sel);
                    }
                }
            }
        }
    }

    std::vector<std::unique_ptr<Input>> result;

    /* Do the filtering/handling for each table */
    for (const auto &id : possible) {
        if (forgeLoopbackSkip.count(
                std::pair<StringIndex::index, StringIndex::index>(id.station, id.archive)) &&
                !forgeAlwaysVariables.count(id.variable))
            continue;
        /* Start the handling for the table, and skip it if it can't
         * apply to the selections */
        Selection::List effectiveSelections = selections;
        IdentityHandlerStation station(layout, index);
        if (!station.first(id, effectiveSelections))
            continue;
        IdentityHandlerArchive archive(layout, index);
        if (!archive.first(id, effectiveSelections))
            continue;
        IdentityHandlerVariable variable(layout, index);
        if (!variable.first(id, effectiveSelections))
            continue;

        if (effectiveSelections.empty())
            continue;

        /* Second stage, generate selection sets and skip if none are possible */
        if (!station.second(effectiveSelections))
            continue;
        if (!archive.second(effectiveSelections))
            continue;
        if (!variable.second(effectiveSelections))
            continue;


        SequenceName name(station.finalComponentName(), archive.finalComponentName(),
                          variable.finalComponentName());

        /* Do a filter step to see if this table can possibly hit any of the selections */
        if (layout.splitStation() || layout.splitArchive() || layout.splitVariable()) {
            SequenceMatch::Basic::CompileOptions options;
            options.ignoreFlavors = true;
            options.ignoreStation = !layout.splitStation();
            options.ignoreArchive = !layout.splitArchive();
            options.ignoreVariable = !layout.splitVariable();
            if (!SequenceMatch::Element::matchesAny(
                    SequenceMatch::Basic::compileToElements(effectiveSelections, options), name))
                continue;
        }

        /* Calculate maximum bounds that need to be queried */
        double firstPossibleStart = effectiveSelections.front().start;
        double lastPossibleEnd = effectiveSelections.front().end;
        double firstPossibleModified = effectiveSelections.front().modifiedAfter;
        for (auto sel = effectiveSelections.begin() + 1, endSel = effectiveSelections.end();
                sel != endSel;
                ++sel) {
            if (Range::compareStart(sel->start, firstPossibleStart) < 0)
                firstPossibleStart = sel->start;
            if (Range::compareEnd(sel->end, lastPossibleEnd) > 0)
                lastPossibleEnd = sel->end;
            if (Range::compareStart(sel->modifiedAfter, firstPossibleModified) < 0)
                firstPossibleModified = sel->modifiedAfter;
        }

        /* Create the final level flavors filter */
        SequenceMatch::Basic::CompileOptions flavorsMatchOptions;
        flavorsMatchOptions.ignoreStation = true;
        flavorsMatchOptions.ignoreArchive = true;
        flavorsMatchOptions.ignoreVariable = true;
        auto flavorsMatch =
                SequenceMatch::Basic::compile(effectiveSelections, FP::undefined(), FP::undefined(),
                                              flavorsMatchOptions);

        /* Merge selections that result in single groupings of inputs */
        std::unordered_map<SelectionMerge, Selection::List, SelectionMerge::hash> mergedSelections;
        for (auto &sel : effectiveSelections) {
            SelectionMerge mergeID = sel;
            mergedSelections[mergeID].emplace_back(std::move(sel));
        }

        /* For each broken down set of possible components, create an input */
        auto stationIdentity = station.componentSets();
        auto archiveIdentity = archive.componentSets();
        auto variableIdentity = variable.componentSets();
        for (const auto &sID : stationIdentity) {
            for (const auto &aID : archiveIdentity) {
                for (const auto &vID : variableIdentity) {
                    result.emplace_back(new Input(layout, id, name));
                    std::unique_ptr<Input> &input = result.back();

                    input->stream.firstPossibleBlock = layout.startBlock(firstPossibleStart);
                    input->stream.lastPossibleBlock = layout.endBlock(lastPossibleEnd);
                    input->stream.firstPossibleModified =
                            layout.modifiedThreshold(firstPossibleModified);
                    input->stream.stations = sID;
                    input->stream.archives = aID;
                    input->stream.variables = vID;
                    for (const auto &sel : mergedSelections) {
                        input->stream
                             .selections
                             .emplace_back(new Stream::Selection(layout, sel.second));
                    }

                    input->block.firstPossibleStart = firstPossibleStart;
                    input->block.lastPossibleEnd = lastPossibleEnd;
                    input->block.firstPossibleModified = firstPossibleModified;
                    input->block.flavorsMatch =
                            std::unique_ptr<SequenceMatch::Basic>(flavorsMatch->clone());
                    for (const auto &sel : mergedSelections) {
                        input->block
                             .selections
                             .emplace_back(new Block::Selection(layout, sel.second));
                    }
                }
            }
        }
    }

    return result;
}

Input::Input(Structure::Layout &layout,
             const Structure::Identity &identity,
             const SequenceName &name) : identity(identity), name(name), stream(layout), block()
{ }


}
}
}
}