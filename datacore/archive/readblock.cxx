/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <vector>
#include <cstring>
#include <algorithm>
#include <unordered_map>
#include <cstdint>
#include <QtEndian>
#include <QDataStream>
#include <QLoggingCategory>

#include "readblock.hxx"

#include "core/compression.hxx"
#include "sequencefilter.hxx"
#include "stringindex.hxx"
#include "structure.hxx"


Q_LOGGING_CATEGORY(log_datacore_archive_readblock, "cpd3.datacore.archive.readblock", QtWarningMsg)


namespace CPD3 {
namespace Data {
namespace Archive {

static double readDouble(const uchar *ptr)
{
    quint64 i = qFromLittleEndian<quint64>(ptr);
    double d;
    std::memcpy(&d, &i, 8);
    return d;
}


namespace {

class NameCache {
    const SequenceName &baseName;
    StringIndex &flavorsIndex;
    SequenceFilter::Block &blockFilter;
    std::unordered_map<StringIndex::index, SequenceName> flavorsLookup;
public:

    NameCache(const SequenceName &name, StringIndex &flavors, SequenceFilter::Block &filter)
            : baseName(name), flavorsIndex(flavors), blockFilter(filter)
    { }

    SequenceName lookup(StringIndex::index idFlavors)
    {
        auto check = flavorsLookup.find(idFlavors);
        if (check != flavorsLookup.end())
            return check->second;

        auto flavors = flavorsIndex.lookup(idFlavors);
        if (!blockFilter.flavors(flavors))
            return flavorsLookup.emplace(idFlavors, SequenceName()).first->second;

        SequenceName n = baseName;
        n.setFlavorsString(flavors);
        return flavorsLookup.emplace(idFlavors, std::move(n)).first->second;
    }
};


struct ArchiveModifiedIterator {
    const uchar *origin;
    std::vector<quint32>::const_iterator position;

public:

    using self_type = ArchiveModifiedIterator;
    using difference_type = std::vector<quint32>::const_iterator::difference_type;
    using value_type = double;
    using pointer = value_type *;
    using reference = value_type &;
    using iterator_category = std::vector<quint32>::const_iterator::iterator_category;

    ArchiveModifiedIterator(const uchar *o, const std::vector<quint32>::const_iterator &p) : origin(
            o + Structure::Existing::Offset_Modified), position(p)
    { }

    ArchiveModifiedIterator() = default;

    self_type operator++()
    {
        self_type i = *this;
        ++position;
        return i;
    }

    self_type operator++(int)
    {
        ++position;
        return *this;
    }

    self_type operator--()
    {
        self_type i = *this;
        --position;
        return i;
    }

    self_type operator--(int)
    {
        --position;
        return *this;
    }

    self_type operator+(difference_type add) const
    {
        self_type i = *this;
        i.position += add;
        return i;
    }

    self_type &operator+=(difference_type add)
    {
        position += add;
        return *this;
    }

    self_type operator-(difference_type add) const
    {
        self_type i = *this;
        i.position -= add;
        return i;
    }

    difference_type operator-(const self_type &other) const
    {
        return (position - other.position);
    }

    self_type &operator-=(difference_type add)
    {
        position -= add;
        return *this;
    }

    value_type operator[](difference_type n) const
    { return readDouble(origin + position[n]); }

    value_type operator*() const
    { return readDouble(origin + (*position)); }

    bool operator==(const self_type &rhs)
    { return position == rhs.position; }

    bool operator!=(const self_type &rhs)
    { return position != rhs.position; }

    inline std::vector<quint32>::const_iterator get() const
    { return position; }

#ifdef Q_CC_MSVC
    inline bool operator<(const ArchiveModifiedIterator &other)
    { return position < other.position; }
    inline bool operator>(const ArchiveModifiedIterator &other)
    { return position > other.position; }
    inline bool operator<=(const ArchiveModifiedIterator &other)
    { return position <= other.position; }
    inline bool operator>=(const ArchiveModifiedIterator &other)
    { return position >= other.position; }
#endif
};

}

static std::vector<quint32> readSequenceOffsets(const Util::ByteArray &uncompressed)
{

    const uchar *ptr = uncompressed.data<const uchar *>();
    quint32 n = qFromLittleEndian<quint32>(ptr);

    if (n == 0)
        return std::vector<quint32>();
    if (n >= static_cast<quint32>(uncompressed.size() / 4)) {
        qCWarning(log_datacore_archive_readblock) << "Data block to small for offset list";
        return std::vector<quint32>();
    }

    std::vector<quint32> offsets;
    offsets.reserve(n);
    ptr += 4;
    for (const uchar *end = ptr + 4 * n; ptr < end; ptr += 4) {
        offsets.emplace_back(qFromLittleEndian<quint32>(ptr));
    }

    return offsets;
}

SequenceValue::Transfer ReadBlock::sequence(const Util::ByteView &data,
                                            const SequenceName &baseName,
                                            StringIndex &flavors,
                                            SequenceFilter::Block &filter)
{
    SequenceValue::Transfer result;

    Util::ByteArray uncompressed;
    if (!BlockDecompressor().decompress(data, uncompressed)) {
        qCWarning(log_datacore_archive_readblock) << "Decompression failed for data block in"
                                                  << baseName;
        return result;
    }
    if (uncompressed.empty())
        return result;
    if (uncompressed.size() < 4) {
        qCWarning(log_datacore_archive_readblock) << "Data block to small for index in" << baseName;
        return result;
    }

    auto offsets = readSequenceOffsets(uncompressed);
    if (offsets.empty())
        return result;
    if (offsets.back() > (uncompressed.size() - Structure::Existing::Minimum_Size)) {
        qCWarning(log_datacore_archive_readblock) << "Data block to small for offsets in"
                                                  << baseName;
        return result;
    }

    const uchar *ptr = uncompressed.data<const uchar *>();

    auto offset = offsets.cbegin();
    auto offsetEnd = offsets.cend();
    if (FP::defined(filter.modifiedAfter())) {
        offset = std::upper_bound(ArchiveModifiedIterator(ptr, offset),
                                  ArchiveModifiedIterator(ptr, offsetEnd),
                                  filter.modifiedAfter()).get();
        if (offset == offsetEnd)
            return result;
    }

    NameCache cache(baseName, flavors, filter);

    Util::ByteView::Device dev(uncompressed);
    dev.open(QIODevice::ReadOnly);
    QDataStream stream(&dev);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setVersion(QDataStream::Qt_4_5);

    for (; offset != offsetEnd; ++offset) {
        Q_ASSERT(*offset + Structure::Existing::Minimum_Size <=
                         static_cast<quint32>(uncompressed.size()));

        const uchar *dataPtr = ptr + *offset;
        double start = readDouble(dataPtr);
        dataPtr += 8;
        double end = readDouble(dataPtr);

        if (!filter.bounds(start, end))
            continue;
        dataPtr += 8;

        SequenceName name = cache.lookup(qFromLittleEndian<quint16>(dataPtr));
        if (!name.isValid())
            continue;
        dataPtr += 2;

        if (!filter.final(start, end, dataPtr, name))
            continue;
        dataPtr += 8;

        qint32 priority = qFromLittleEndian<qint32>(dataPtr);

        dev.seek((qint64) (*offset) + (qint64) Structure::Existing::Offset_Value);
        Variant::Root v = Variant::Root::deserialize(stream);

        result.emplace_back(
                SequenceIdentity(std::move(name), start, end, static_cast<int>(priority)),
                std::move(v));
    }

    return result;
}

ArchiveValue::Transfer ReadBlock::archive(const Util::ByteView &data,
                                          const SequenceName &baseName,
                                          StringIndex &flavors,
                                          SequenceFilter::Block &filter)
{
    ArchiveValue::Transfer result;

    Util::ByteArray uncompressed;
    if (!BlockDecompressor().decompress(data, uncompressed)) {
        qCWarning(log_datacore_archive_readblock) << "Decompression failed for data block in"
                                                  << baseName;
        return result;
    }
    if (uncompressed.empty())
        return result;
    if (uncompressed.size() < 4) {
        qCWarning(log_datacore_archive_readblock) << "Data block to small for index in" << baseName;
        return result;
    }

    auto offsets = readSequenceOffsets(uncompressed);
    if (offsets.empty())
        return result;
    if (offsets.back() > (uncompressed.size() - Structure::Existing::Minimum_Size)) {
        qCWarning(log_datacore_archive_readblock) << "Data block to small for offsets in"
                                                  << baseName;
        return result;
    }

    const uchar *ptr = uncompressed.data<const uchar *>();

    auto offset = offsets.cbegin();
    auto offsetEnd = offsets.cend();
    if (FP::defined(filter.modifiedAfter())) {
        offset = std::upper_bound(ArchiveModifiedIterator(ptr, offset),
                                  ArchiveModifiedIterator(ptr, offsetEnd),
                                  filter.modifiedAfter()).get();
        if (offset == offsetEnd)
            return result;
    }

    NameCache cache(baseName, flavors, filter);

    Util::ByteView::Device dev(uncompressed);
    dev.open(QIODevice::ReadOnly);
    QDataStream stream(&dev);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setVersion(QDataStream::Qt_4_5);

    for (; offset != offsetEnd; ++offset) {
        Q_ASSERT(*offset + Structure::Existing::Minimum_Size <=
                         static_cast<quint32>(uncompressed.size()));

        const uchar *dataPtr = ptr + *offset;
        double start = readDouble(dataPtr);
        dataPtr += 8;
        double end = readDouble(dataPtr);

        if (!filter.bounds(start, end))
            continue;
        dataPtr += 8;

        SequenceName name = cache.lookup(qFromLittleEndian<quint16>(dataPtr));
        if (!name.isValid())
            continue;
        dataPtr += 2;

        double modified = readDouble(dataPtr);
        dataPtr += 8;
        Q_ASSERT(!FP::defined(filter.modifiedAfter()) || modified > filter.modifiedAfter());
        if (!filter.final(start, end, modified, name))
            continue;

        int_fast32_t priority = qFromLittleEndian<qint32>(dataPtr);
        dataPtr += 4;

        uint_fast8_t flags = qFromLittleEndian<quint8>(dataPtr);

        dev.seek((qint64) (*offset) + (qint64) Structure::Existing::Offset_Value);
        Variant::Root v = Variant::Root::deserialize(stream);

        result.emplace_back(
                SequenceIdentity(std::move(name), start, end, static_cast<int>(priority)),
                std::move(v), modified, (flags & Structure::Existing::Flag_RemoteReferenced) != 0);
    }

    return result;
}

SequenceName::Set ReadBlock::names(const Util::ByteView &data,
                                   const SequenceName &baseName,
                                   StringIndex &flavors,
                                   SequenceFilter::Block &filter)
{
    SequenceName::Set result;

    Util::ByteArray uncompressed;
    if (!BlockDecompressor().decompress(data, uncompressed)) {
        qCWarning(log_datacore_archive_readblock) << "Decompression failed for data block in"
                                                  << baseName;
        return result;
    }
    if (uncompressed.empty())
        return result;
    if (uncompressed.size() < 4) {
        qCWarning(log_datacore_archive_readblock) << "Data block to small for index in" << baseName;
        return result;
    }

    auto offsets = readSequenceOffsets(uncompressed);
    if (offsets.empty())
        return result;
    if (offsets.back() > (uncompressed.size() - Structure::Existing::Minimum_Size)) {
        qCWarning(log_datacore_archive_readblock) << "Data block to small for offsets in"
                                                  << baseName;
        return result;
    }

    const uchar *ptr = uncompressed.data<const uchar *>();

    auto offset = offsets.cbegin();
    auto offsetEnd = offsets.cend();
    if (FP::defined(filter.modifiedAfter())) {
        offset = std::upper_bound(ArchiveModifiedIterator(ptr, offset),
                                  ArchiveModifiedIterator(ptr, offsetEnd),
                                  filter.modifiedAfter()).get();
        if (offset == offsetEnd)
            return result;
    }

    NameCache cache(baseName, flavors, filter);

    for (; offset != offsetEnd; ++offset) {
        Q_ASSERT(*offset + Structure::Existing::Minimum_Size <=
                         static_cast<quint32>(uncompressed.size()));

        const uchar *dataPtr = ptr + *offset;
        double start = readDouble(dataPtr);
        dataPtr += 8;
        double end = readDouble(dataPtr);

        if (!filter.bounds(start, end))
            continue;
        dataPtr += 8;

        SequenceName name = cache.lookup(qFromLittleEndian<quint16>(dataPtr));
        if (!name.isValid())
            continue;
        if (result.find(name) != result.end())
            continue;
        dataPtr += 2;

        if (!filter.final(start, end, dataPtr, name))
            continue;

        result.emplace(std::move(name));
    }

    return result;
}


namespace {
struct ErasureModifiedIterator {
    const uchar *ptr;
public:

    using self_type = ErasureModifiedIterator;
    using difference_type = std::ptrdiff_t;
    using value_type = double;
    using pointer = value_type *;
    using reference = value_type &;
    using iterator_category = std::random_access_iterator_tag;

    ErasureModifiedIterator(const uchar *p) : ptr(p + Structure::Erasure::Offset_Modified)
    { }

    ErasureModifiedIterator() = default;

    self_type operator++()
    {
        self_type i = *this;
        ptr += Structure::Erasure::Total_Size;
        return i;
    }

    self_type operator++(int)
    {
        ptr += Structure::Erasure::Total_Size;
        return *this;
    }

    self_type operator--()
    {
        self_type i = *this;
        ptr -= Structure::Erasure::Total_Size;
        return i;
    }

    self_type operator--(int)
    {
        ptr -= Structure::Erasure::Total_Size;
        return *this;
    }

    self_type operator+(difference_type add) const
    {
        self_type i = *this;
        i.ptr += Structure::Erasure::Total_Size * add;
        return i;
    }

    self_type &operator+=(difference_type add)
    {
        ptr += Structure::Erasure::Total_Size * add;
        return *this;
    }

    self_type operator-(difference_type add) const
    {
        self_type i = *this;
        i.ptr -= Structure::Erasure::Total_Size * add;
        return i;
    }

    difference_type operator-(const self_type &other) const
    {
        return (ptr - other.ptr) / Structure::Erasure::Total_Size;
    }

    self_type &operator-=(difference_type add)
    {
        ptr -= Structure::Erasure::Total_Size * add;
        return *this;
    }

    value_type operator[](difference_type n) const
    { return readDouble(ptr + Structure::Erasure::Total_Size * n); }

    value_type operator*() const
    { return readDouble(ptr); }

    bool operator==(const self_type &rhs)
    { return ptr == rhs.ptr; }

    bool operator!=(const self_type &rhs)
    { return ptr != rhs.ptr; }

    const uchar *origin() const
    { return ptr - Structure::Erasure::Offset_Modified; }

#ifdef Q_CC_MSVC
    inline bool operator<(const ErasureModifiedIterator &other)
    { return ptr < other.ptr; }
    inline bool operator>(const ErasureModifiedIterator &other)
    { return ptr > other.ptr; }
    inline bool operator<=(const ErasureModifiedIterator &other)
    { return ptr <= other.ptr; }
    inline bool operator>=(const ErasureModifiedIterator &other)
    { return ptr >= other.ptr; }
#endif
};
}

ArchiveErasure::Transfer ReadBlock::erasure(const Util::ByteView &data,
                                            const SequenceName &baseName,
                                            StringIndex &flavors,
                                            SequenceFilter::Block &filter)
{
    ArchiveErasure::Transfer result;

    Util::ByteArray uncompressed;
    if (!BlockDecompressor().decompress(data, uncompressed)) {
        qCWarning(log_datacore_archive_readblock) << "Decompression failed for data block in"
                                                  << baseName;
        return result;
    }
    if (uncompressed.empty())
        return result;
    if (uncompressed.size() % Structure::Erasure::Total_Size != 0) {
        qCWarning(log_datacore_archive_readblock) << "Invalid decompressed size in" << baseName;
        return result;
    }

    const uchar *dataPtr = uncompressed.data<const uchar *>();
    const uchar *endPtr = dataPtr + uncompressed.size();

    if (FP::defined(filter.modifiedAfter())) {
        auto offset =
                std::upper_bound(ErasureModifiedIterator(dataPtr), ErasureModifiedIterator(endPtr),
                                 filter.modifiedAfter());
        dataPtr = offset.origin();
        if (dataPtr == endPtr)
            return result;
    }

    NameCache cache(baseName, flavors, filter);

    for (; dataPtr != endPtr;) {
        Q_ASSERT(dataPtr < endPtr);

        double start = readDouble(dataPtr);
        dataPtr += 8;
        double end = readDouble(dataPtr);

        if (!filter.bounds(start, end)) {
            dataPtr += (Structure::Erasure::Total_Size - Structure::Erasure::Offset_End);
            continue;
        }
        dataPtr += 8;

        SequenceName name = cache.lookup(qFromLittleEndian<quint16>(dataPtr));
        if (!name.isValid()) {
            dataPtr += (Structure::Erasure::Total_Size - Structure::Erasure::Offset_Flavors);
            continue;
        }
        dataPtr += 2;

        double modified = readDouble(dataPtr);
        dataPtr += 8;
        Q_ASSERT(!FP::defined(filter.modifiedAfter()) || modified > filter.modifiedAfter());
        if (!filter.final(start, end, modified, name)) {
            dataPtr += 4;
            continue;
        }

        std::int_fast32_t priority = qFromLittleEndian<qint32>(dataPtr);
        dataPtr += 4;

        result.emplace_back(
                SequenceIdentity(std::move(name), start, end, static_cast<int>(priority)),
                modified);
    }

    return result;
}

}
}
}