/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QRegularExpression>
#include <QFile>
#include <QLoggingCategory>

#include "access.hxx"
#include "stringindex.hxx"
#include "writeoperation.hxx"
#include "readoperation.hxx"

#include "database/connection.hxx"
#include "database/util.hxx"
#include "database/tabledefinition.hxx"
#include "core/waitutils.hxx"
#include "core/threadpool.hxx"
#include "core/util.hxx"
#include "structure.hxx"


Q_LOGGING_CATEGORY(log_datacore_archive_access, "cpd3.datacore.archive.access", QtWarningMsg)


namespace CPD3 {
namespace Data {
namespace Archive {

class Access::TransactionContext {
    Database::Connection::Transaction transaction;
    std::unique_ptr<Structure::Layout> activeLayout;
    std::unique_ptr<IdentityIndex> activeIdentity;

    static Database::Connection::Transaction begin(Database::Connection *connection,
                                                   bool serializable,
                                                   bool write)
    {
        int flags = 0;
        if (!write)
            flags |= Database::Connection::Transaction_ReadOnly;
        if (serializable)
            flags |= Database::Connection::Transaction_Serializable;
        return Database::Connection::Transaction(*connection, flags);
    }

public:
    TransactionContext(Database::Connection *connection, bool serializable, bool write)
            : transaction(begin(connection, serializable, write)),
              activeLayout(new Structure::Layout(connection)),
              activeIdentity(new IdentityIndex(connection))
    {
        if (transaction.inError()) {
            qCWarning(log_datacore_archive_access) << "Transaction initialization failed";
        }
    }

    ~TransactionContext()
    {
        activeLayout.reset();
        activeIdentity.reset();
    }

    void abort()
    {
        transaction.abort();
    }

    bool end()
    {
        activeLayout.reset();
        activeIdentity.reset();
        return transaction.end();
    }

    Structure::Layout &layout()
    { return *activeLayout; }

    IdentityIndex &identity()
    { return *activeIdentity; }
};

Access::Access() : Access(Database::Util::getDefaultDatabase())
{ }

Access::Access(const Database::Storage &storage) : Access(new Database::Connection(storage))
{ }

Access::Access(const QFile &database) : Access(
        new Database::Connection(Database::Storage::sqlite(database.fileName().toStdString())))
{ }

static Database::Connection::RequiredResult initialize(Database::Connection *connection)
{
    static constexpr int systemIdentifier = 1;
    static constexpr int systemVersion_V1 = 1;
    static constexpr int systemVersion_V2 = 0x8;
    static constexpr int systemVersion_V2_Mask = 0xF;

    if (!connection->hasTable("CPD3DBVersion")) {
        Database::TableDefinition table("CPD3DBVersion");
        table.column("system", Database::TableDefinition::Integer, false);
        table.column("version", Database::TableDefinition::Integer, false);
        table.primaryKey("system");
        connection->createTable(table);
    }
    Q_ASSERT(connection->hasTable("CPD3DBVersion"));

    auto version = connection->select("CPD3DBVersion", {"version"}, "system = :system")
                             .single({{"system", systemIdentifier}})
                             .toInteger();
    if (INTEGER::defined(version)) {
        if (version == systemVersion_V1) {
            Q_ASSERT(connection->hasTable("CPD3Stations"));
            Q_ASSERT(connection->hasTable("CPD3Archives"));
            Q_ASSERT(connection->hasTable("CPD3Variables"));
            Q_ASSERT(connection->hasTable("CPD3Flavors"));
        } else if ((version & systemVersion_V2) && (version & systemVersion_V2_Mask) == version) {
            Q_ASSERT(connection->hasTable("CPD3Stations"));
            Q_ASSERT(connection->hasTable("CPD3Archives"));
            Q_ASSERT(connection->hasTable("CPD3Variables"));
            Q_ASSERT(connection->hasTable("CPD3Flavors"));
        } else {
            qFatal("Archive version (%d) not supported", static_cast<int>(version));
            return Database::Connection::Required_Failed;
        }
    } else {
        connection->insert("CPD3DBVersion", {"system", "version"})
                  .synchronous({{"system",  systemIdentifier},
                                {"version", systemVersion_V2}});
        IdentityIndex::initialize(connection);
    }

    return Database::Connection::Required_Ok;
}

Access::Access(Database::Connection *conn) : connection(conn),
                                             isWriteLock(false),
                                             isSerializableLock(false),
                                             nextLock(0),
                                             activeLocks(),
                                             activeTransaction()
{
    if (!connection->start()) {
        qCCritical(log_datacore_archive_access)
            << "Unable to access the database, all operations are invalid";
        return;
    }

    connection->requiredTransaction(std::bind(&initialize, connection.get()));
}

Access::~Access()
{
    waitForLocks();
    connection->signalTerminate();
    connection->wait();

    activeTransaction.reset();
    connection.reset();
}

void Access::signalTerminate()
{ connection->signalTerminate(); }

bool Access::waitForLocks(double timeout)
{
    return Threading::waitForTimeout(timeout, lockMutex, lockState,
                                     [this] { return nextLock == 0; });
}

bool Access::isTerminated()
{ return connection->isFinished(); }

bool Access::endTransaction()
{
    nextLock = 0;
    isWriteLock = false;
    isSerializableLock = false;

    bool result = false;
    if (activeTransaction) {
        result = activeTransaction->end();
        activeTransaction.reset();
    }

    lockState.notify_all();

    return result;
}

void Access::beginTransaction(bool serializable, bool write)
{
    Q_ASSERT(activeLocks.empty());
    Q_ASSERT(!activeTransaction);

    nextLock = 2;

    if (write)
        serializable = true;

    activeTransaction = std::unique_ptr<TransactionContext>(
            new TransactionContext(connection.get(), serializable, write));

    isWriteLock = write;
    isSerializableLock = serializable;
}


Access::ReadLock::ReadLock(Access &a, bool serializable, bool excl) : access(&a),
                                                                      id(0),
                                                                      exclusive(excl)
{
    std::unique_lock<std::mutex> lock(access->lockMutex);
    if (exclusive) {
        access->lockState.wait(lock, [this] { return access->nextLock == 0; });
        Q_ASSERT(access->nextLock == 0);
    }

    if (access->nextLock == 0) {
        id = 1;
        access->beginTransaction(serializable, false);
    } else {
        if (serializable && !access->isSerializableLock) {
            qCWarning(log_datacore_archive_access)
                << "Cannot acquire serializablity in a nested lock";
        }
        id = access->nextLock++;
    }

    access->activeLocks.insert(id);
}

Access::ReadLock::~ReadLock()
{
    release();
}

Access::ReadLock::ReadLock(Access::ReadLock &&other) : access(other.access),
                                                       id(other.id),
                                                       exclusive(other.exclusive)
{
    other.id = 0;
}

Access::ReadLock &Access::ReadLock::operator=(Access::ReadLock &&other)
{
    if (&other == this)
        return *this;
    this->~ReadLock();
    Q_ASSERT(id == 0);
    access = other.access;
    id = other.id;
    other.id = 0;
    exclusive = other.exclusive;
    return *this;
}

void Access::ReadLock::swap(Access::ReadLock &rhs)
{
    using std::swap;
    swap(this->access, rhs.access);
    swap(this->id, rhs.id);
    swap(this->exclusive, rhs.exclusive);
}

void Access::ReadLock::release()
{
    if (id == 0)
        return;
    std::lock_guard<std::mutex> lock(access->lockMutex);

    access->activeLocks.erase(id);
    id = 0;

    if (!access->activeLocks.empty()) {
        if (exclusive) {
            qCritical(log_datacore_archive_access) << "Exclusive lock not released on destroy";
        }
        return;
    }

    if (!access->endTransaction()) {
        qCDebug(log_datacore_archive_access) << "Read only transaction commit failed";
    }
}


Access::WriteLock::WriteLock(Access &a, bool excl) : access(&a), id(0), exclusive(excl)
{
    std::unique_lock<std::mutex> lock(access->lockMutex);
#if 0
    if (!exclusive && access->nextLock != 0 && !access->isWriteLock) {
        qCWarning(log_datacore_archive_access) << "Forcing exclusive write access because a read lock is already held";
        exclusive = true;
    }
#endif
    if (exclusive) {
        access->lockState.wait(lock, [this] { return access->nextLock == 0; });
        Q_ASSERT(access->nextLock == 0);
    }

    if (access->nextLock == 0) {
        id = 1;
        access->beginTransaction(true, true);
    } else {
        if (!access->isWriteLock) {
            qFatal("Cannot acquire write access in a nested lock");
        } else if (!access->isSerializableLock) {
            qCWarning(log_datacore_archive_access)
                << "Cannot acquire serializablity in a nested lock";
        }
        id = access->nextLock++;
    }

    access->activeLocks.insert(id);
}

Access::WriteLock::~WriteLock()
{
    if (id != 0) {
        //qCDebug(log_datacore_archive_access) << "Aborting write lock with destruction";
        abort();
    }
}

void Access::WriteLock::abort()
{
    if (id == 0)
        return;
    std::lock_guard<std::mutex> lock(access->lockMutex);

    access->activeLocks.erase(id);
    id = 0;

    if (access->activeTransaction) {
        access->activeTransaction->abort();
    }

    if (access->activeLocks.empty()) {
        access->endTransaction();
    }
}

bool Access::WriteLock::commit()
{
    if (id == 0)
        return false;
    std::lock_guard<std::mutex> lock(access->lockMutex);

    access->activeLocks.erase(id);
    id = 0;

    /* If we're still locked, assume this is managed by some other outer lock */
    if (!access->activeLocks.empty()) {
        if (exclusive) {
            qCritical(log_datacore_archive_access) << "Exclusive lock not released on commit";
        }
        return true;
    }

    return access->endTransaction();
}

Access::WriteLock::WriteLock(Access::WriteLock &&other) : access(other.access),
                                                          id(other.id),
                                                          exclusive(other.exclusive)
{
    other.id = 0;
}

Access::WriteLock &Access::WriteLock::operator=(Access::WriteLock &&other)
{
    if (&other == this)
        return *this;
    this->~WriteLock();
    Q_ASSERT(id == 0);
    access = other.access;
    id = other.id;
    other.id = 0;
    exclusive = other.exclusive;
    return *this;
}

void Access::WriteLock::swap(Access::WriteLock &rhs)
{
    using std::swap;
    swap(this->access, rhs.access);
    swap(this->id, rhs.id);
    swap(this->exclusive, rhs.exclusive);
}


bool Access::writeSynchronous(const std::function<bool()> &operation)
{
    for (;;) {
        if (isTerminated())
            return false;

        WriteLock lock(*this, false);

        if (!operation()) {
            lock.abort();
            return false;
        }

        if (lock.commit())
            break;
    }

    return true;
}

bool Access::writeSynchronous(const SequenceValue::Transfer &values, bool sorted)
{
    return writeSynchronous([&] {
        WriteSequence op
                (connection.get(), activeTransaction->layout(), activeTransaction->identity(),
                 sorted);
        op.add(values);
        op.complete();
        return true;
    });
}

bool Access::writeSynchronous(const ArchiveValue::Transfer &values, bool sorted)
{
    return writeSynchronous([&] {
        WriteSequence op
                (connection.get(), activeTransaction->layout(), activeTransaction->identity(),
                 sorted);
        op.add(values);
        op.complete();
        return true;
    });
}

bool Access::writeSynchronous(const SequenceValue::Transfer &values,
                              const ArchiveErasure::Transfer &remove)
{
    return writeSynchronous([&] {
        WriteSequence op
                (connection.get(), activeTransaction->layout(), activeTransaction->identity(),
                 false);
        op.add(values);
        op.remove(remove);
        op.complete();
        return true;
    });
}

bool Access::writeSynchronous(const SequenceValue::Transfer &values,
                              const SequenceValue::Transfer &remove)
{
    return writeSynchronous([&] {
        WriteSequence op
                (connection.get(), activeTransaction->layout(), activeTransaction->identity(),
                 false);
        op.add(values);
        op.remove(remove);
        op.complete();
        return true;
    });
}

bool Access::writeSynchronous(const SequenceValue::Transfer &values,
                              const SequenceIdentity::Transfer &remove)
{
    return writeSynchronous([&] {
        WriteSequence op
                (connection.get(), activeTransaction->layout(), activeTransaction->identity(),
                 false);
        op.add(values);
        op.remove(remove);
        op.complete();
        return true;
    });
}

bool Access::writeSynchronous(const ArchiveValue::Transfer &values,
                              const ArchiveErasure::Transfer &remove)
{
    return writeSynchronous([&] {
        WriteSequence op
                (connection.get(), activeTransaction->layout(), activeTransaction->identity(),
                 false);
        op.add(values);
        op.remove(remove);
        op.complete();
        return true;
    });
}

bool Access::writeSynchronous(const ArchiveErasure::Transfer &remove, bool sorted)
{
    return writeSynchronous([&] {
        WriteSequence op
                (connection.get(), activeTransaction->layout(), activeTransaction->identity(),
                 sorted);
        op.remove(remove);
        op.complete();
        return true;
    });
}

bool Access::removeSynchronous(const SequenceIdentity::Transfer &remove, bool sorted)
{
    return writeSynchronous([&] {
        WriteSequence op
                (connection.get(), activeTransaction->layout(), activeTransaction->identity(),
                 sorted);
        op.remove(remove);
        op.complete();
        return true;
    });
}

bool Access::removeSynchronous(const SequenceValue::Transfer &remove, bool sorted)
{
    return writeSynchronous([&] {
        WriteSequence op
                (connection.get(), activeTransaction->layout(), activeTransaction->identity(),
                 sorted);
        op.remove(remove);
        op.complete();
        return true;
    });
}


SequenceValue::Transfer Access::readSynchronous(const Selection::List &selections)
{
    ReadLock lock(*this);
    StreamSink::Buffer buffer;
    {
        ReadSequence read(activeTransaction->layout(), activeTransaction->identity(), selections);
        read.setEgress(&buffer);
        read.start();
        read.wait();
    }
    return buffer.take();
}

ArchiveValue::Transfer Access::readSynchronousArchive(const Selection::List &selections)
{
    ReadLock lock(*this, true);
    ArchiveSink::Buffer buffer;
    {
        ReadArchive read(activeTransaction->layout(), activeTransaction->identity(), selections);
        read.setEgress(&buffer);
        read.start();
        read.wait();
    }
    return buffer.take();
}


Access::StreamContext::StreamContext(ReadLock *lock, ReadSequence *operation) : lock(lock),
                                                                                operation(
                                                                                        operation),
                                                                                complete(
                                                                                        operation->complete)
{
    operation->complete.connect([this] {
        this->lock.reset();
    });
    operation->start();
}

Access::StreamContext::~StreamContext()
{
    operation->join();
    /* Force the disconnection, so we know the lock release on complete can't reach us */
    operation->complete.disconnect();
    operation.reset();
    lock.reset();
}

void Access::StreamContext::signalTerminate()
{ operation->signalTerminate(); }

bool Access::StreamContext::wait(double timeout)
{ return operation->wait(timeout); }

bool Access::StreamContext::isComplete()
{ return operation->isComplete(); }

static void detachedStreamContext(const Access::StreamHandle &h)
{ h->wait(); }

void Access::StreamContext::detach()
{ std::thread(detachedStreamContext, shared_from_this()).detach(); }

void Access::StreamContext::setEgress(StreamSink *sink)
{ operation->setEgress(sink); }

Access::StreamHandle Access::readStream(const Selection::List &selections, StreamSink *sink)
{
    ReadLock *lock = new ReadLock(*this);
    ReadSequence *operation =
            new ReadSequence(activeTransaction->layout(), activeTransaction->identity(),
                             selections);
    StreamHandle handle(new StreamContext(lock, operation));
    if (sink)
        handle->setEgress(sink);
    return handle;
}


Access::ArchiveContext::ArchiveContext(ReadLock *lock, ReadArchive *operation) : lock(lock),
                                                                                 operation(
                                                                                         operation),
                                                                                 complete(
                                                                                         operation->complete)
{
    operation->complete.connect([this] {
        this->lock.reset();
    });
    operation->start();
}

Access::ArchiveContext::~ArchiveContext()
{
    operation->join();
    /* Force the disconnection, so we know the lock release on complete can't reach us */
    operation->complete.disconnect();
    operation.reset();
    lock.reset();
}

void Access::ArchiveContext::signalTerminate()
{ operation->signalTerminate(); }

bool Access::ArchiveContext::wait(double timeout)
{ return operation->wait(timeout); }

bool Access::ArchiveContext::isComplete()
{ return operation->isComplete(); }

static void detachedArchiveContext(const Access::ArchiveHandle &h)
{ h->wait(); }

void Access::ArchiveContext::detach()
{ std::thread(detachedArchiveContext, shared_from_this()).detach(); }

void Access::ArchiveContext::setEgress(ArchiveSink *sink)
{ operation->setEgress(sink); }

Access::ArchiveHandle Access::readArchive(const Selection::List &selections, ArchiveSink *sink)
{
    ReadLock *lock = new ReadLock(*this, true);
    ReadArchive *operation =
            new ReadArchive(activeTransaction->layout(), activeTransaction->identity(), selections);
    ArchiveHandle handle(new ArchiveContext(lock, operation));
    if (sink)
        handle->setEgress(sink);
    return handle;
}


Access::ErasureContext::ErasureContext(ReadLock *lock, ReadErasure *operation) : lock(lock),
                                                                                 operation(
                                                                                         operation),
                                                                                 complete(
                                                                                         operation->complete)
{
    operation->complete.connect([this] {
        this->lock.reset();
    });
    operation->start();
}

Access::ErasureContext::~ErasureContext()
{
    operation->join();
    /* Force the disconnection, so we know the lock release on complete can't reach us */
    operation->complete.disconnect();
    operation.reset();
    lock.reset();
}

void Access::ErasureContext::signalTerminate()
{ operation->signalTerminate(); }

bool Access::ErasureContext::wait(double timeout)
{ return operation->wait(timeout); }

bool Access::ErasureContext::isComplete()
{ return operation->isComplete(); }

static void detachedErasureContext(const Access::ErasureHandle &h)
{ h->wait(); }

void Access::ErasureContext::detach()
{ std::thread(detachedErasureContext, shared_from_this()).detach(); }

void Access::ErasureContext::setEgress(ErasureSink *sink)
{ operation->setEgress(sink); }

Access::ErasureHandle Access::readErasure(const Selection::List &selections, ErasureSink *sink)
{
    ReadLock *lock = new ReadLock(*this, true);
    ReadErasure *operation =
            new ReadErasure(activeTransaction->layout(), activeTransaction->identity(), selections);
    ErasureHandle handle(new ErasureContext(lock, operation));
    if (sink)
        handle->setEgress(sink);
    return handle;
}


void Access::BackgroundWrite::run(const std::shared_ptr<Access::BackgroundWrite> &self)
{
    /* Before the lock, so it's called after the lock is released */
    Threading::Signal<>::Deferred inactiveSignal;

    std::unique_lock<std::mutex> lock(self->mutex);
    switch (self->state) {
    case State_Inactive:
        self->state = State_Executing_Once;
        break;
    case State_Executing_Once:
        break;
    case State_Executing_Again:
        /* Can happen if we get a second wake before processing the initial one,
         * so just combine them back together */
        self->state = State_Executing_Once;
        break;
    case State_Executing_Terminate:
        self->state = State_Terminated;
        self->notify.notify_all();
        inactiveSignal = self->complete.defer();
        return;
    case State_Terminated:
        return;
    }

    for (;;) {
        Q_ASSERT(self->state == State_Executing_Once);

        if (!self->prepare()) {
            self->state = State_Inactive;
            self->notify.notify_all();
            if (self->isWaitComplete())
                inactiveSignal = self->complete.defer();
            return;
        }

        lock.unlock();

        BackgroundWrite::Result result;

        {
            WriteLock wr(self->access, self->exclusive);

            result = self->operation(self->access);

            switch (result) {
            case Success:
            case Again:
                if (!wr.commit()) {
                    lock.lock();
                    self->failure();
                    result = Retry;
                } else {
                    lock.lock();
                    self->success();
                }
                break;
            case Retry:
            case Abort:
                wr.abort();

                lock.lock();
                self->failure();
                break;
            }
        }

        Q_ASSERT(lock.owns_lock());

        switch (self->state) {
        case State_Inactive:
            Q_ASSERT(false);
            break;
        case State_Executing_Once:
            switch (result) {
            case Success:
                self->state = State_Inactive;
                self->notify.notify_all();
                if (self->isWaitComplete())
                    inactiveSignal = self->complete.defer();
                return;
            case Again:
            case Retry:
                break;
            case Abort:
                self->state = State_Terminated;
                self->notify.notify_all();
                inactiveSignal = self->complete.defer();
                return;
            }
            break;
        case State_Executing_Again:
            switch (result) {
            case Success:
            case Again:
            case Retry:
                break;
            case Abort:
                self->state = State_Terminated;
                self->notify.notify_all();
                inactiveSignal = self->complete.defer();
                return;
            }
            break;
        case State_Executing_Terminate:
        case State_Terminated:
            self->state = State_Terminated;
            self->notify.notify_all();
            inactiveSignal = self->complete.defer();
            return;
        }

        self->state = State_Executing_Once;
    }
}

Access::BackgroundWrite::BackgroundWrite(Access &access, bool exclusive) : access(access),
                                                                           exclusive(exclusive),
                                                                           mutex(),
                                                                           notify(),
                                                                           state(State_Inactive)
{ }

Access::BackgroundWrite::~BackgroundWrite() = default;

void Access::BackgroundWrite::signalTerminate()
{
    /* Before the lock, so it's called after the lock is released */
    auto inactiveSignal = complete.defer();

    {
        std::lock_guard<std::mutex> lock(mutex);
        switch (state) {
        case State_Inactive:
            state = State_Terminated;
            break;
        case State_Terminated:
            return;
        case State_Executing_Once:
        case State_Executing_Again:
        case State_Executing_Terminate:
            state = State_Executing_Terminate;
            inactiveSignal.never();
            break;
        }
    }
    notify.notify_all();
}

bool Access::BackgroundWrite::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, mutex, notify, [this] {
        if (state == State_Terminated)
            return true;
        return state == State_Inactive && isWaitComplete();
    });
}

void Access::BackgroundWrite::wake(const std::function<bool()> &withLock,
                                   const std::function<bool()> &stall)
{
    {
        std::unique_lock<std::mutex> lock(mutex);

        if (stall) {
            while (state != State_Inactive) {
                switch (state) {
                case State_Inactive:
                    Q_ASSERT(false);
                    break;
                case State_Terminated:
                case State_Executing_Terminate:
                    return;
                case State_Executing_Once:
                case State_Executing_Again:
                    break;
                }

                if (!stall())
                    break;
                notify.wait(lock);
            }
        }

        switch (state) {
        case State_Terminated:
        case State_Executing_Terminate:
            return;
        case State_Inactive:
            if (withLock && !withLock())
                return;
            break;
        case State_Executing_Once:
        case State_Executing_Again:
            if (withLock && !withLock())
                return;
            state = State_Executing_Again;
            return;
        }

        state = State_Executing_Once;
    }
    std::thread(BackgroundWrite::run, shared_from_this()).detach();
}

bool Access::BackgroundWrite::prepare()
{ return true; }

void Access::BackgroundWrite::success()
{ }

void Access::BackgroundWrite::failure()
{ }

bool Access::BackgroundWrite::isWaitComplete()
{ return true; }


Access::ReplaceWriteContext::ReplaceWriteContext(Access &access, bool exclusive, bool sorted)
        : BackgroundWrite(access, exclusive), sorted(sorted), ended(false), finished(false)
{ }

Access::ReplaceWriteContext::~ReplaceWriteContext() = default;

bool Access::ReplaceWriteContext::stall() const
{ return incoming.size() > stallThreshold; }

void Access::ReplaceWriteContext::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(values, incoming);
        return true;
    }, std::bind(&ReplaceWriteContext::stall, this));
}

void Access::ReplaceWriteContext::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(std::move(values), incoming);
        return true;
    }, std::bind(&ReplaceWriteContext::stall, this));
}

void Access::ReplaceWriteContext::incomingData(const SequenceValue &value)
{
    wake([this, &value] {
        incoming.emplace_back(value);
        return true;
    }, std::bind(&ReplaceWriteContext::stall, this));
}

void Access::ReplaceWriteContext::incomingData(SequenceValue &&value)
{
    wake([this, &value] {
        incoming.emplace_back(std::move(value));
        return true;
    }, std::bind(&ReplaceWriteContext::stall, this));
}

void Access::ReplaceWriteContext::endData()
{
    wake([this] {
        ended = true;
        return true;
    });
}

bool Access::ReplaceWriteContext::prepare()
{
    finished = ended;

    if (incoming.empty())
        return !processing.empty();

    Util::append(std::move(incoming), processing);
    incoming.clear();
    return true;
}

Access::BackgroundWrite::Result Access::ReplaceWriteContext::operation(Access &access)
{
    WriteSequence op(access.connection.get(), access.activeTransaction->layout(),
                     access.activeTransaction->identity(), sorted);
    op.add(processing);
    op.complete();
    return finished ? Success : Again;
}

void Access::ReplaceWriteContext::success()
{
    processing.clear();
}

bool Access::ReplaceWriteContext::isWaitComplete()
{ return ended; }

Access::ReplaceHandle Access::writeReplace(bool exclusive, bool sorted)
{ return ReplaceHandle(new ReplaceWriteContext(*this, exclusive, sorted)); }


Access::SynchronizationWriteContext::SynchronizationWriteContext(Access &access,
                                                                 bool exclusive,
                                                                 bool sorted) : BackgroundWrite(
        access, exclusive), sorted(sorted), ended(false), finished(false), wasNearStall(false)
{ }

Access::SynchronizationWriteContext::~SynchronizationWriteContext() = default;

bool Access::SynchronizationWriteContext::stall() const
{ return (incomingArchiveValues.size() + incomingErasureValues.size()) > stallThreshold; }

bool Access::SynchronizationWriteContext::nearStall() const
{
    return static_cast<std::size_t>(incomingArchiveValues.size() + incomingErasureValues.size()) >
            stallThreshold / 2;
}

void Access::SynchronizationWriteContext::incomingValue(const ArchiveValue::Transfer &values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(values, incomingArchiveValues);
        return true;
    }, std::bind(&SynchronizationWriteContext::stall, this));
}

void Access::SynchronizationWriteContext::incomingValue(ArchiveValue::Transfer &&values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(std::move(values), incomingArchiveValues);
        return true;
    }, std::bind(&SynchronizationWriteContext::stall, this));
}

void Access::SynchronizationWriteContext::incomingValue(const ArchiveValue &value)
{
    wake([this, &value] {
        incomingArchiveValues.emplace_back(value);
        return true;
    }, std::bind(&SynchronizationWriteContext::stall, this));
}

void Access::SynchronizationWriteContext::incomingValue(ArchiveValue &&value)
{
    wake([this, &value] {
        incomingArchiveValues.emplace_back(std::move(value));
        return true;
    }, std::bind(&SynchronizationWriteContext::stall, this));
}


void Access::SynchronizationWriteContext::incomingErasure(const ArchiveErasure::Transfer &values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(values, incomingErasureValues);
        return true;
    }, std::bind(&SynchronizationWriteContext::stall, this));
}

void Access::SynchronizationWriteContext::incomingErasure(ArchiveErasure::Transfer &&values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(std::move(values), incomingErasureValues);
        return true;
    }, std::bind(&SynchronizationWriteContext::stall, this));
}

void Access::SynchronizationWriteContext::incomingErasure(const ArchiveErasure &value)
{
    wake([this, &value] {
        incomingErasureValues.emplace_back(value);
        return true;
    }, std::bind(&SynchronizationWriteContext::stall, this));
}

void Access::SynchronizationWriteContext::incomingErasure(ArchiveErasure &&value)
{
    wake([this, &value] {
        incomingErasureValues.emplace_back(std::move(value));
        return true;
    }, std::bind(&SynchronizationWriteContext::stall, this));
}

void Access::SynchronizationWriteContext::incoming(const ArchiveValue::Transfer &values,
                                                   const ArchiveErasure::Transfer &erasure)
{
    if (values.empty())
        return;
    wake([this, &values, &erasure] {
        Util::append(values, incomingArchiveValues);
        Util::append(erasure, incomingErasureValues);
        return true;
    }, std::bind(&SynchronizationWriteContext::stall, this));
}

void Access::SynchronizationWriteContext::incoming(ArchiveValue::Transfer &&values,
                                                   ArchiveErasure::Transfer &&erasure)
{
    if (values.empty())
        return;
    wake([this, &values, &erasure] {
        Util::append(std::move(values), incomingArchiveValues);
        Util::append(std::move(erasure), incomingErasureValues);
        return true;
    }, std::bind(&SynchronizationWriteContext::stall, this));
}

void Access::SynchronizationWriteContext::endStream()
{
    wake([this] {
        ended = true;
        return true;
    });
}

bool Access::SynchronizationWriteContext::softStalled()
{
    bool result = false;
    /* A bit strange, but we just need the lock, so unless this becomes more common
     * ("withLock"), just do a wake that never wakes */
    wake([this, &result] {
        result = nearStall();
        return false;
    });
    return result;
}

bool Access::SynchronizationWriteContext::prepare()
{
    finished = ended;

    wasNearStall = nearStall();

    if (!incomingArchiveValues.empty()) {
        Util::append(std::move(incomingArchiveValues), processingArchiveValues);
        incomingArchiveValues.clear();
    }

    if (!incomingErasureValues.empty()) {
        Util::append(std::move(incomingErasureValues), processingErasureValues);
        incomingErasureValues.clear();
    }

    return !processingArchiveValues.empty() || !processingErasureValues.empty();
}

Access::BackgroundWrite::Result Access::SynchronizationWriteContext::operation(Access &access)
{
    WriteSynchronize op(access.connection.get(), access.activeTransaction->layout(),
                        access.activeTransaction->identity(), sorted);
    if (sorted) {
        auto addArchive = processingArchiveValues.cbegin();
        auto endArchive = processingArchiveValues.cend();
        auto addErasure = processingErasureValues.cbegin();
        auto endErasure = processingErasureValues.cend();
        while (addArchive != endArchive && addErasure != endErasure) {
            if (Range::compareStart(addArchive->getStart(), addErasure->getStart()) <= 0) {
                op.add(*addArchive);
                ++addArchive;
            } else {
                op.remove(*addErasure);
                ++addErasure;
            }
        }
        op.add(addArchive, endArchive);
        op.remove(addErasure, endErasure);
    } else {
        op.add(processingArchiveValues);
        op.remove(processingErasureValues);
    }
    op.complete();

    if (wasNearStall) {
        wasNearStall = false;
        softStallCleared();
    }

    return finished ? Success : Again;
}

void Access::SynchronizationWriteContext::success()
{
    processingArchiveValues.clear();
    processingErasureValues.clear();
}

bool Access::SynchronizationWriteContext::isWaitComplete()
{ return ended; }

Access::SynchronizationHandle Access::writeSynchronize(bool exclusive, bool sorted)
{ return SynchronizationHandle(new SynchronizationWriteContext(*this, exclusive, sorted)); }


Access::RemoteReferencedWriteContext::RemoteReferencedWriteContext(Access &access,
                                                                   bool exclusive,
                                                                   bool sorted) : BackgroundWrite(
        access, exclusive), sorted(sorted), ended(false), finished(false)
{ }

Access::RemoteReferencedWriteContext::~RemoteReferencedWriteContext() = default;

bool Access::RemoteReferencedWriteContext::stall() const
{ return incoming.size() > stallThreshold; }

void Access::RemoteReferencedWriteContext::incomingData(const SequenceIdentity::Transfer &values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(values, incoming);
        return true;
    }, std::bind(&RemoteReferencedWriteContext::stall, this));
}

void Access::RemoteReferencedWriteContext::incomingData(SequenceIdentity::Transfer &&values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(std::move(values), incoming);
        return true;
    }, std::bind(&RemoteReferencedWriteContext::stall, this));
}

void Access::RemoteReferencedWriteContext::incomingData(const SequenceIdentity &value)
{
    wake([this, &value] {
        incoming.emplace_back(value);
        return true;
    }, std::bind(&RemoteReferencedWriteContext::stall, this));
}

void Access::RemoteReferencedWriteContext::incomingData(SequenceIdentity &&value)
{
    wake([this, &value] {
        incoming.emplace_back(std::move(value));
        return true;
    }, std::bind(&RemoteReferencedWriteContext::stall, this));
}

void Access::RemoteReferencedWriteContext::incomingValue(const ArchiveValue::Transfer &values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(values, incoming);
        return true;
    }, std::bind(&RemoteReferencedWriteContext::stall, this));
}

void Access::RemoteReferencedWriteContext::incomingValue(ArchiveValue::Transfer &&values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        for (auto &v : values) {
            incoming.emplace_back(std::move(v));
        }
        return true;
    }, std::bind(&RemoteReferencedWriteContext::stall, this));
}

void Access::RemoteReferencedWriteContext::incomingValue(const ArchiveValue &value)
{ return incomingData(SequenceIdentity(value)); }

void Access::RemoteReferencedWriteContext::incomingValue(ArchiveValue &&value)
{ return incomingData(SequenceIdentity(std::move(value))); }

void Access::RemoteReferencedWriteContext::incomingValue(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(values, incoming);
        return true;
    }, std::bind(&RemoteReferencedWriteContext::stall, this));
}

void Access::RemoteReferencedWriteContext::incomingValue(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    wake([this, &values] {
        Util::append(std::move(values), incoming);
        return true;
    }, std::bind(&RemoteReferencedWriteContext::stall, this));
}

void Access::RemoteReferencedWriteContext::incomingValue(const SequenceValue &value)
{ return incomingData(SequenceIdentity(value)); }

void Access::RemoteReferencedWriteContext::incomingValue(SequenceValue &&value)
{ return incomingData(SequenceIdentity(std::move(value))); }

void Access::RemoteReferencedWriteContext::endData()
{
    wake([this] {
        ended = true;
        return true;
    });
}

bool Access::RemoteReferencedWriteContext::prepare()
{
    finished = ended;

    if (incoming.empty())
        return !processing.empty();

    Util::append(std::move(incoming), processing);
    incoming.clear();
    return true;
}

Access::BackgroundWrite::Result Access::RemoteReferencedWriteContext::operation(Access &access)
{
    FlagRemoteReferenced op(access.connection.get(), access.activeTransaction->layout(),
                            access.activeTransaction->identity(), sorted);
    op.add(processing);
    op.complete();
    return finished ? Success : Again;
}

void Access::RemoteReferencedWriteContext::success()
{
    processing.clear();
}

bool Access::RemoteReferencedWriteContext::isWaitComplete()
{ return ended; }

Access::RemoteReferencedHandle Access::writeRemoteReferenced(bool exclusive, bool sorted)
{ return RemoteReferencedHandle(new RemoteReferencedWriteContext(*this, exclusive, sorted)); }

static void applyAllComponentFilter(SequenceName::ComponentSet &components,
                                    const Selection::Match &filter,
                                    bool caseSensitive = false)
{
    if (filter.empty())
        return;

    std::vector<QRegularExpression> compiled;
    for (const auto &pattern : filter) {
        compiled.emplace_back(QString::fromStdString(pattern),
                              caseSensitive ? QRegularExpression::NoPatternOption
                                            : QRegularExpression::CaseInsensitiveOption);
    }

    for (auto c = components.begin(); c != components.end();) {
        bool hit = false;
        for (auto &re : compiled) {
            if (!Util::exact_match(*c, re))
                continue;
            hit = true;
            break;
        }
        if (!hit) {
            c = components.erase(c);
            continue;
        }
        ++c;
    }
}

SequenceName::ComponentSet Access::availableStations(const Selection::Match &filter)
{
    SequenceName::ComponentSet result;
    {
        ReadLock lock(*this);
        result = Available::Components::allStations(activeTransaction->identity());
    }
    applyAllComponentFilter(result, filter);
    /* Require an explict filter to return the default station */
    if (filter.empty())
        result.erase("_");
    return result;
}

Access::FutureComponents Access::availableStationsFuture(const Selection::Match &filter)
{
    auto lock = std::make_shared<Access::ReadLock>(*this);
    return std::async(std::launch::async, [=]() -> SequenceName::ComponentSet {
        auto result = Available::Components::allStations(activeTransaction->identity());
        lock->release();
        applyAllComponentFilter(result, filter);
        /* Require an explict filter to return the default station */
        if (filter.empty())
            result.erase("_");
        return result;
    });
}

SequenceName::ComponentSet Access::availableArchives(const Selection::Match &filter)
{
    SequenceName::ComponentSet result;
    {
        ReadLock lock(*this);
        result = Available::Components::allArchives(activeTransaction->identity());
    }
    applyAllComponentFilter(result, filter);
    return result;
}

Access::FutureComponents Access::availableArchivesFuture(const Selection::Match &filter)
{
    auto lock = std::make_shared<Access::ReadLock>(*this);
    return std::async(std::launch::async, [=]() -> SequenceName::ComponentSet {
        auto result = Available::Components::allArchives(activeTransaction->identity());
        lock->release();
        applyAllComponentFilter(result, filter);
        return result;
    });
}

SequenceName::ComponentSet Access::availableVariables(const Selection::Match &filter)
{
    SequenceName::ComponentSet result;
    {
        ReadLock lock(*this);
        result = Available::Components::allVariables(activeTransaction->identity());
    }
    applyAllComponentFilter(result, filter, true);
    return result;
}

Access::FutureComponents Access::availableVariablesFuture(const Selection::Match &filter)
{
    auto lock = std::make_shared<Access::ReadLock>(*this);
    return std::async(std::launch::async, [=]() -> SequenceName::ComponentSet {
        auto result = Available::Components::allVariables(activeTransaction->identity());
        lock->release();
        applyAllComponentFilter(result, filter, true);
        return result;
    });
}

SequenceName::ComponentSet Access::availableFlavors(const Selection::Match &filter)
{
    SequenceName::ComponentSet result;
    {
        ReadLock lock(*this);
        result = Available::Components::allFlavors(activeTransaction->identity());
    }
    applyAllComponentFilter(result, filter);
    return result;
}

Access::FutureComponents Access::availableFlavorsFuture(const Selection::Match &filter)
{
    auto lock = std::make_shared<Access::ReadLock>(*this);
    return std::async(std::launch::async, [=]() -> SequenceName::ComponentSet {
        auto result = Available::Components::allFlavors(activeTransaction->identity());
        lock->release();
        applyAllComponentFilter(result, filter);
        return result;
    });
}

static Available::Granularity toAvailableGranularity(Access::AvailableGranularity granularity)
{
    switch (granularity) {
    case Access::ReadAll:
        return Available::ReadAll;
    case Access::SpanningOnly:
        return Available::SpanningOnly;
    case Access::IndexOnly:
        return Available::IndexOnly;
    }
    Q_ASSERT(false);
    return Available::SpanningOnly;
}

SequenceName::Set Access::availableNames(const Selection::List &selections,
                                         AvailableGranularity granularity)
{
    ReadLock lock(*this);
    SequenceName::Set result;
    {
        Available::Names read
                (activeTransaction->layout(), activeTransaction->identity(), selections,
                 toAvailableGranularity(granularity));
        read.start();
        read.wait();
        result = read.result();
    }
    return result;
}

Access::FutureNames Access::availableNamesFuture(const Selection::List &selections,
                                                 AvailableGranularity granularity)
{
    auto lock = std::make_shared<Access::ReadLock>(*this);
    auto read = std::make_shared<Available::Names>(activeTransaction->layout(),
                                                   activeTransaction->identity(), selections,
                                                   toAvailableGranularity(granularity));
    read->start();
    return std::async(std::launch::async, [=]() -> SequenceName::Set {
        read->wait();
        lock->release();
        return read->result();
    });
}

Access::SelectedComponents Access::availableSelected(const Selection::List &selections,
                                                     AvailableGranularity granularity)
{
    ReadLock lock(*this);
    SelectedComponents result;
    {
        Available::Components read
                (activeTransaction->layout(), activeTransaction->identity(), selections,
                 toAvailableGranularity(granularity));
        read.start();
        read.wait();
        result.stations = read.stations();
        result.archives = read.archives();
        result.variables = read.variables();
        result.flavors = read.flavors();
    }
    return result;
}

Access::FutureSelected Access::availableSelectedFuture(const Selection::List &selections,
                                                       AvailableGranularity granularity)
{
    auto lock = std::make_shared<Access::ReadLock>(*this);
    auto read = std::make_shared<Available::Components>(activeTransaction->layout(),
                                                        activeTransaction->identity(), selections,
                                                        toAvailableGranularity(granularity));
    read->start();
    return std::async(std::launch::async, [=]() -> SelectedComponents {
        read->wait();
        lock->release();
        SelectedComponents result;
        result.stations = read->stations();
        result.archives = read->archives();
        result.variables = read->variables();
        result.flavors = read->flavors();
        return result;
    });
}

Access::SelectedComponents Access::availableSelected()
{
    ReadLock lock(*this);
    SelectedComponents result;
    {
        result.stations = Available::Components::allStations(activeTransaction->identity());
        /* Remove the default station, like e do with the normal available */
        result.stations.erase("_");
        result.archives = Available::Components::allArchives(activeTransaction->identity());
        result.variables = Available::Components::allVariables(activeTransaction->identity());
        result.flavors = Available::Components::allFlavors(activeTransaction->identity());
    }
    return result;
}


Access::FutureSelected Access::availableSelectedFuture()
{
    auto lock = std::make_shared<Access::ReadLock>(*this);
    return std::async(std::launch::async, [=]() -> SelectedComponents {
        SelectedComponents result;
        result.stations = Available::Components::allStations(activeTransaction->identity());
        /* Remove the default station, like e do with the normal available */
        result.stations.erase("_");
        result.archives = Available::Components::allArchives(activeTransaction->identity());
        result.variables = Available::Components::allVariables(activeTransaction->identity());
        result.flavors = Available::Components::allFlavors(activeTransaction->identity());
        lock->release();
        return result;
    });
}

bool Access::availableExists(const Selection::List &selections, AvailableGranularity granularity)
{
    ReadLock lock(*this);
    bool result;
    {
        Available::Exists read
                (activeTransaction->layout(), activeTransaction->identity(), selections,
                 toAvailableGranularity(granularity));
        read.start();
        read.wait();
        result = read.result();
    }
    return result;
}

Access::FutureExists Access::availableExistsFuture(const Selection::List &selections,
                                                   AvailableGranularity granularity)
{
    auto lock = std::make_shared<Access::ReadLock>(*this);
    auto read = std::make_shared<Available::Exists>(activeTransaction->layout(),
                                                    activeTransaction->identity(), selections,
                                                    toAvailableGranularity(granularity));
    read->start();
    return std::async(std::launch::async, [=]() -> bool {
        read->wait();
        lock->release();
        return read->result();
    });
}

}
}
}
