/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <vector>
#include <limits>
#include <QLoggingCategory>

#include "writeoperation.hxx"
#include "stringindex.hxx"
#include "structure.hxx"
#include "writeblock.hxx"
#include "initialization.hxx"
#include "core/threadpool.hxx"
#include "database/connection.hxx"


Q_LOGGING_CATEGORY(log_datacore_archive_writeoperation, "cpd3.datacore.archive.writeoperation",
                   QtWarningMsg)

namespace CPD3 {
namespace Data {
namespace Archive {


static constexpr Structure::Block minimumBlock = std::numeric_limits<Structure::StrictBlock>::min();

/*
 * The general structure for writing breaks down the incoming data into block by block
 * operations.  There is a special operation for the global ones (those that are
 * not confined to a single block).  When the incoming data advances past a block (or
 * is completed entirely) then the operation is launched on a worker thread.  This
 * thread is what actually does the block update process.
 *
 * So, a list of un-launched blocks is maintained for each identity, when one
 * is ready it is started then proceeds through the update process.  For simple
 * blocks, this is a read-modify-write cycle on the existing (if any) data.
 * For global blocks, is consists of reading all the global blocks, selecting
 * one to write new values to (if required) then writing out all modified ones
 * (since others may have also been modified by replacement or removal).
 * Pooled threads are not used because we could exhaust the pool
 * waiting and block a database transaction that we're in turn
 * waiting for (e.x. statement creation).
 */

WriteOperation::WriteOperation(Database::Connection *c,
                               Structure::Layout &l,
                               IdentityIndex &i,
                               bool s) : connection(c), layout(l), index(i), sorted(s), inFlight(0)
{ }

WriteOperation::~WriteOperation()
{
    Q_ASSERT(inFlight == 0);
}

WriteBlock::Context &WriteOperation::context(const SequenceName &name, double start, double end)
{
    {
        auto check = lookup.find(name);
        if (check != lookup.end())
            return context(*check->second, start, end);
    }

    Structure::Identity id
            (index.station.insert(name.getStation()), index.archive.insert(name.getArchive()),
             index.variable.insert(name.getVariable()));
    {
        auto check = working.find(id);
        if (check != working.end()) {
            lookup.emplace(name, check->second);
            return context(*check->second, start, end);
        }
    }

    auto target = working.emplace(std::move(id), std::make_shared<Sequence>());
    lookup.emplace(name, target.first->second);
    return context(*target.first->second, start, end);
}

WriteBlock::Context &WriteOperation::context(WriteOperation::Sequence &sequence,
                                             double start,
                                             double end)
{
    Structure::Block block = layout.targetBlock(start, end);
    if (block < 0) {
        if (!sequence.global) {
            sequence.global = createContext();
        }
        return *sequence.global;
    }
    BlockContext &target = sequence.blocks[block];
    if (!target) {
        target = createContext();
    }
    return *target;
}

WriteOperation::BlockContext WriteOperation::createContext()
{
    return std::make_shared<WriteBlock::Context>(index.flavor);
}

void WriteOperation::completeWrite()
{
    /* Have to hold the mutex during the wake, so this isn't destroyed on the notify */
    std::lock_guard<std::mutex> lock(flightMutex);
    Q_ASSERT(inFlight > 0);
    --inFlight;
    if (inFlight != 0)
        return;
    flightNotify.notify_all();
}

struct WriteOperation::GlobalOperations {
    Structure::Layout &layout;

    Structure::Layout::DatabaseAccess readAllSequence;
    Structure::Layout::DatabaseAccess writeSequenceBlock;
    Structure::Layout::DatabaseAccess removeSequenceBlock;
    Structure::Layout::DatabaseAccess readSequenceBlock;
    Structure::Layout::DatabaseAccess sizeSequence;
    Structure::Layout::DatabaseAccess minimumSequence;

    Structure::Layout::DatabaseAccess readAllErasure;
    Structure::Layout::DatabaseAccess writeErasureBlock;
    Structure::Layout::DatabaseAccess removeErasureBlock;
    Structure::Layout::DatabaseAccess readErasureBlock;
    Structure::Layout::DatabaseAccess sizeErasure;
    Structure::Layout::DatabaseAccess minimumErasure;

    GlobalOperations(WriteOperation &operation, const Structure::Identity &id) : layout(
            operation.layout)
    {
        std::string whereAll = Structure::Column_Block;
        whereAll += " < 0";

        std::string whereBlock = Structure::Column_Block;
        whereBlock += " = :";
        whereBlock += Structure::Column_Block;

        if (!operation.connection->hasTable(operation.layout.sequenceTable(id))) {
            Initialization::createSequenceTable(operation.connection, operation.layout, id);
        }
        readAllSequence = operation.layout.sequenceRead(id, whereAll);
        writeSequenceBlock = operation.layout.sequenceWrite(id);
        removeSequenceBlock = operation.layout.sequenceDelete(id, whereBlock);
        readSequenceBlock = operation.layout.sequenceRead(id, whereBlock);
        sizeSequence = operation.layout.sequenceSize(id, whereAll);
        minimumSequence = operation.layout.sequenceMinimum(id);

        if (!operation.connection->hasTable(operation.layout.erasureTable(id))) {
            Initialization::createErasureTable(operation.connection, operation.layout, id);
        }
        readAllErasure = operation.layout.erasureRead(id, whereAll);
        writeErasureBlock = operation.layout.erasureWrite(id);
        removeErasureBlock = operation.layout.erasureDelete(id, whereBlock);
        readErasureBlock = operation.layout.erasureRead(id, whereBlock);
        sizeErasure = operation.layout.erasureSize(id, whereAll);
        minimumErasure = operation.layout.erasureMinimum(id);
    }


    template<typename WriteType>
    struct TargetState {
        typedef std::unique_ptr<WriteType> Pointer;

        Pointer target;
        Structure::Block block;
        std::size_t size;

        TargetState(Structure::Block blk, const Util::ByteView &data) : target(new WriteType(data)),
                                                                    block(blk),
                                                                    size((std::size_t) data.size())
        {
            Q_ASSERT(block < 0);
        }

        TargetState() : target(), block(0), size(0)
        { }

        TargetState(const TargetState &) = delete;

        TargetState &operator=(const TargetState &) = delete;

        TargetState(TargetState &&) = default;

        TargetState &operator=(TargetState &&) = default;

        bool shouldReplace(const TargetState<WriteType> &incoming) const
        {
            Q_ASSERT(incoming.target);

            /* Never try to put it into too-large blocks */
            if (incoming.size >= Structure::Global_SplitSize)
                return false;

            /* If there's not already a target, then use this one */
            if (!target)
                return true;

            if (!incoming.target->isModified()) {
                /* Prefer an already modified one, if possible */
                if (target->isModified())
                    return false;

                /* Neither modified, so only replace if the incoming is smaller */
                return incoming.size < size;
            }

            /* Incoming already modified, so prefer it if the current one isn't */
            if (!target->isModified())
                return true;

            /* Both modified, so only replace if the incoming is smaller */
            return incoming.size < size;
        }
    };

    typedef TargetState<WriteBlock::Sequence> SequenceTarget;
    typedef TargetState<WriteBlock::Erasure> ErasureTarget;

    void foreachSequence(const std::function<void(Structure::Block, const Util::ByteArray &)> &f) const
    {
        Database::Context::Iterator
                it(readAllSequence.statement().execute(readAllSequence.binds()));
        while (it.hasNext()) {
            auto data = it.next();
            if (data.size() < 2)
                continue;
            auto i = data[1].toInteger();
            if (!INTEGER::defined(i)) {
                qCWarning(log_datacore_archive_writeoperation) << "Invalid sequence block";
                continue;
            }
            auto block = static_cast<Structure::Block>(i);
            if (block >= 0) {
                qCWarning(log_datacore_archive_writeoperation) << "Invalid sequence block" << i;
                continue;
            }
            f(block, data[0].takeBytes());
        }
    }

    void foreachErasure(const std::function<void(Structure::Block, const Util::ByteArray &)> &f) const
    {
        std::string cBlock = Structure::Column_Block;
        std::string cData = Structure::Column_Data;
        Database::Context::Iterator it(readAllErasure.statement().execute(readAllErasure.binds()));
        while (it.hasNext()) {
            auto data = it.next();
            if (data.size() < 2)
                continue;
            auto i = data[1].toInteger();
            if (!INTEGER::defined(i)) {
                qCWarning(log_datacore_archive_writeoperation) << "Invalid erasure block";
                continue;
            }
            auto block = static_cast<Structure::Block>(i);
            if (block >= 0) {
                qCWarning(log_datacore_archive_writeoperation) << "Invalid erasure block" << i;
                continue;
            }
            f(block, data[0].takeBytes());
        }
    }

    void acquireTarget(SequenceTarget &target, Structure::Block block) const
    {
        Q_ASSERT(!target.target);

        auto binds = readSequenceBlock.binds();
        binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
        target = SequenceTarget(block, readSequenceBlock.statement().single(binds).takeBytes());
    }

    void acquireTarget(ErasureTarget &target, Structure::Block block) const
    {
        Q_ASSERT(!target.target);

        auto binds = readErasureBlock.binds();
        binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
        target = ErasureTarget(block, readErasureBlock.statement().single(binds).takeBytes());
    }

    void acquireTarget(SequenceTarget &target) const
    {
        if (target.target)
            return;

        auto result = sizeSequence.statement().extract(sizeSequence.binds());
        if (result.size() >= 2) {
            auto size = result[0].toInteger();
            if (INTEGER::defined(size) && size < static_cast<std::int_fast64_t>(Structure::Global_SplitSize)) {
                auto i = result[1].toInteger();
                auto block = static_cast<Structure::Block>(i);
                if (INTEGER::defined(i) && block < 0) {
                    acquireTarget(target, block);
                    return;
                }
            }
        }

        auto min = minimumSequence.statement().single(minimumSequence.binds());
        if (min.isEmpty()) {
            auto i = min.toInteger();
            auto block = static_cast<Structure::Block>(i);
            if (INTEGER::defined(i) && block < 0) {
                if (block <= minimumBlock) {
                    /* No available block, put it in a random one */
                    quint64 rblock = Random::integer() &
                            static_cast<quint64>((-static_cast<qint64>(minimumBlock)) - 1);
                    block = -static_cast<Structure::Block>(rblock);
                    block -= 1;
                } else {
                    /* Put it in the next most negative one */
                    block -= 1;
                }
                Q_ASSERT(block < 0);
                Q_ASSERT(block >= minimumBlock);
                acquireTarget(target, block);
                return;
            }
        }

        /* No global blocks at all, so just put it in -1 */
        acquireTarget(target, -1);
    }

    void acquireTarget(ErasureTarget &target) const
    {
        if (target.target)
            return;

        auto result = sizeErasure.statement().extract(sizeErasure.binds());
        if (result.size() >= 2) {
            auto size = result[0].toInteger();
            if (INTEGER::defined(size) && size < static_cast<std::int_fast64_t>(Structure::Global_SplitSize)) {
                auto i = result[1].toInteger();
                auto block = static_cast<Structure::Block>(i);
                if (INTEGER::defined(i) && block < 0) {
                    acquireTarget(target, block);
                    return;
                }
            }
        }

        auto min = minimumErasure.statement().single(minimumErasure.binds());
        if (!result.empty()) {
            auto i = min.toInteger();
            auto block = static_cast<Structure::Block>(i);
            if (INTEGER::defined(i) && block < 0) {
                if (block <= minimumBlock) {
                    /* No available block, put it in a random one */
                    quint64 rblock = Random::integer() &
                            static_cast<quint64>((-static_cast<qint64>(minimumBlock)) - 1);
                    block = -static_cast<Structure::Block>(rblock);
                    block -= 1;
                } else {
                    /* Put it in the next most negative one */
                    block -= 1;
                }
                Q_ASSERT(block < 0);
                Q_ASSERT(block >= minimumBlock);
                acquireTarget(target, block);
                return;
            }
        }

        /* No global blocks at all, so just put it in -1 */
        acquireTarget(target, -1);
    }

    void commit(SequenceTarget &target) const
    {
        if (!target.target)
            return;
        Q_ASSERT(target.block < 0);

        if (target.target->isModified()) {
            auto data = target.target->outputData();
            if (!data.empty()) {
                auto binds = writeSequenceBlock.binds();
                binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(target.block));
                binds.emplace(Structure::Column_Modified,
                              layout.targetModified(target.target->latestModified()));
                binds.emplace(Structure::Column_Data, std::move(data));
                writeSequenceBlock.statement().synchronous(binds);
            } else if (target.size > 0) {
                auto binds = removeSequenceBlock.binds();
                binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(target.block));
                removeSequenceBlock.statement().synchronous(binds);
            }
        }

        target.target.reset();
        target.block = 0;
        target.size = 0;
    }

    void commit(ErasureTarget &target) const
    {
        if (!target.target)
            return;
        Q_ASSERT(target.block < 0);

        if (target.target->isModified()) {
            auto data = target.target->outputData();
            if (!data.empty()) {
                auto binds = writeErasureBlock.binds();
                binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(target.block));
                binds.emplace(Structure::Column_Modified,
                              layout.targetModified(target.target->latestModified()));
                binds.emplace(Structure::Column_Data, std::move(data));
                writeErasureBlock.statement().synchronous(binds);
            } else if (target.size > 0) {
                auto binds = removeErasureBlock.binds();
                binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(target.block));
                removeErasureBlock.statement().synchronous(binds);
            }
        }

        target.target.reset();
        target.block = 0;
        target.size = 0;
    }
};

void WriteOperation::launchGlobal(const std::shared_ptr<GlobalOperations> &operations,
                                  const BlockContext &context)
{
    {
        std::lock_guard<std::mutex> lock(flightMutex);
        ++inFlight;
    }
    operationRun.run([this, operations, context]() {
        this->writeGlobal(*operations, *context);
        this->completeWrite();
    });
}

struct WriteOperation::BlockOperations {
    Structure::Layout &layout;
    Structure::Layout::DatabaseAccess readSequenceBlock;
    Structure::Layout::DatabaseAccess writeSequenceBlock;
    Structure::Layout::DatabaseAccess removeSequenceBlock;

    Structure::Layout::DatabaseAccess readErasureBlock;
    Structure::Layout::DatabaseAccess writeErasureBlock;
    Structure::Layout::DatabaseAccess removeErasureBlock;

    BlockOperations(WriteOperation &operation, const Structure::Identity &id) : layout(
            operation.layout)
    {
        std::string whereBlock = Structure::Column_Block;
        whereBlock += " = :";
        whereBlock += Structure::Column_Block;

        if (!operation.connection->hasTable(operation.layout.sequenceTable(id))) {
            Initialization::createSequenceTable(operation.connection, operation.layout, id);
        }
        readSequenceBlock = operation.layout.sequenceRead(id, whereBlock);
        writeSequenceBlock = operation.layout.sequenceWrite(id);
        removeSequenceBlock = operation.layout.sequenceDelete(id, whereBlock);

        if (!operation.connection->hasTable(operation.layout.erasureTable(id))) {
            Initialization::createErasureTable(operation.connection, operation.layout, id);
        }
        readErasureBlock = operation.layout.erasureRead(id, whereBlock);
        writeErasureBlock = operation.layout.erasureWrite(id);
        removeErasureBlock = operation.layout.erasureDelete(id, whereBlock);
    }


    struct Data {
        Structure::Block block;
        Util::ByteArray sequenceData;
        Util::ByteArray erasureData;
        bool hadSequence;
        bool hadErasure;

        Data(Structure::Block b, Util::ByteArray sequence, Util::ByteArray erasure) : block(b),
                                                                                      sequenceData(std::move(sequence)),
                                                                                      erasureData(std::move(erasure)),
                                                                                      hadSequence(!sequenceData.empty()),
                                                                                      hadErasure(!erasureData.empty())
        { }

        Data(const Data &) = default;

        Data &operator=(const Data &) = default;

        Data(Data &&) = default;

        Data &operator=(Data &&) = default;
    };

    static Util::ByteArray fetchData(Database::Context::Iterator &it)
    {
        auto r = it.next();
        if (r.empty())
            return {};
        return r.front().takeBytes();
    }

    Data begin(Structure::Block block) const
    {
        auto binds = readSequenceBlock.binds();
        binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
        Database::Context::Iterator sequence(readSequenceBlock.statement().execute(binds, true));

        binds = readErasureBlock.binds();
        binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
        Database::Context::Iterator erasure(readErasureBlock.statement().execute(binds, true));

        return Data(block, fetchData(sequence), fetchData(erasure));
    }

    void complete(const Data &data,
                  WriteBlock::Sequence &&sequence,
                  WriteBlock::Erasure &&erasure) const
    {
        std::vector<Database::Context::Guard> updates;

        if (sequence.isModified()) {
            auto sequenceData = sequence.outputData();
            if (!sequenceData.empty()) {
                auto binds = writeSequenceBlock.binds();
                binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(data.block));
                binds.emplace(Structure::Column_Modified,
                              layout.targetModified(sequence.latestModified()));
                binds.emplace(Structure::Column_Data, std::move(sequenceData));
                updates.emplace_back(writeSequenceBlock.statement().execute(binds));
            } else if (data.hadSequence) {
                auto binds = removeSequenceBlock.binds();
                binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(data.block));
                updates.emplace_back(removeSequenceBlock.statement().execute(binds));
            }
        }
        sequence = WriteBlock::Sequence();

        if (erasure.isModified()) {
            auto erasureData = erasure.outputData();
            if (!erasureData.empty()) {
                auto binds = writeErasureBlock.binds();
                binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(data.block));
                binds.emplace(Structure::Column_Modified,
                              layout.targetModified(erasure.latestModified()));
                binds.emplace(Structure::Column_Data, std::move(erasureData));
                updates.emplace_back(writeErasureBlock.statement().execute(binds));
            } else if (data.hadErasure) {
                auto binds = removeErasureBlock.binds();
                binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(data.block));
                updates.emplace_back(removeErasureBlock.statement().execute(binds));
            }
        }
        erasure = WriteBlock::Erasure();
    }
};

void WriteOperation::launchBlock(const std::shared_ptr<BlockOperations> &operations,
                                 Structure::Block block,
                                 const BlockContext &context)
{
    {
        std::lock_guard<std::mutex> lock(flightMutex);
        ++inFlight;
    }
    operationRun.run([this, operations, block, context]() {
        this->writeBlock(*operations, block, *context);
        this->completeWrite();
    });
}

void WriteOperation::complete()
{
    for (const auto &seq : working) {
        if (seq.second->global) {
            auto operations = std::make_shared<GlobalOperations>(*this, seq.first);
            launchGlobal(operations, seq.second->global);
        }

        if (seq.second->blocks.empty())
            continue;

        if (!seq.second->blockOperations)
            seq.second->blockOperations = std::make_shared<BlockOperations>(*this, seq.first);
        for (const auto &block : seq.second->blocks) {
            launchBlock(seq.second->blockOperations, block.first, block.second);
        }
    }
    working.clear();
    lookup.clear();

    std::unique_lock<std::mutex> lock(flightMutex);
    flightNotify.wait(lock, [this] { return inFlight == 0; });
    Q_ASSERT(inFlight == 0);
}

void WriteOperation::advance(double time)
{
    if (!sorted)
        return;

    Structure::Block block = layout.targetBlock(time);
    if (block <= 0)
        return;

    for (auto &seq : working) {
        while (!seq.second->blocks.empty()) {
            auto lb = seq.second->blocks.begin();
            if (lb->first >= block)
                break;

            if (!seq.second->blockOperations)
                seq.second->blockOperations = std::make_shared<BlockOperations>(*this, seq.first);

            launchBlock(seq.second->blockOperations, lb->first, lb->second);
            seq.second->blocks.erase(lb);
        }
    }
}

WriteSequence::WriteSequence(Database::Connection *connection,
                             Structure::Layout &layout,
                             IdentityIndex &index,
                             bool sorted) : WriteOperation(connection, layout, index, sorted)
{ }

WriteSequence::~WriteSequence()
{
    complete();
}


void WriteSequence::writeGlobal(const GlobalOperations &operations, WriteBlock::Context &context)
{
    context.writeStageOne();

    GlobalOperations::SequenceTarget sequenceTarget;
    GlobalOperations::ErasureTarget erasureTarget;

    operations.foreachErasure([&](Structure::Block block, const Util::ByteArray &erasureData) {
        GlobalOperations::ErasureTarget erasure(block, erasureData);
        erasure.target->writeStageTwo(context);
        if (!erasureTarget.shouldReplace(erasure)) {
            operations.commit(erasure);
        } else {
            operations.commit(erasureTarget);
            erasureTarget = std::move(erasure);
        }
    });

    operations.foreachSequence([&](Structure::Block block, const Util::ByteArray &sequenceData) {
        GlobalOperations::SequenceTarget sequence(block, sequenceData);
        sequence.target->writeStageThree(context);
        if (!sequenceTarget.shouldReplace(sequence)) {
            operations.commit(sequence);
        } else {
            operations.commit(sequenceTarget);
            sequenceTarget = std::move(sequence);
        }
    });

    context.writeStageFour();

    if (context.hasErasure()) {
        /* Invalidate the old target, since we have to make a pass through it again */
        operations.commit(erasureTarget);

        operations.foreachErasure([&](Structure::Block block, const Util::ByteArray &erasureData) {
            GlobalOperations::ErasureTarget erasure(block, erasureData);
            erasure.target->writeStageFive(context);
            if (!erasureTarget.shouldReplace(erasure)) {
                operations.commit(erasure);
            } else {
                operations.commit(erasureTarget);
                erasureTarget = std::move(erasure);
            }
        });
    }

    if (context.hasSequence()) {
        operations.acquireTarget(sequenceTarget);
        sequenceTarget.target->inject(context);
    }

    if (context.hasErasure()) {
        operations.acquireTarget(erasureTarget);
        erasureTarget.target->inject(context);
    }

    operations.commit(sequenceTarget);
    operations.commit(erasureTarget);
}

void WriteSequence::writeBlock(const BlockOperations &operations,
                               Structure::Block block,
                               WriteBlock::Context &context)
{
    BlockOperations::Data d = operations.begin(block);

    WriteBlock::Sequence sequence(d.sequenceData);
    WriteBlock::Erasure erasure(d.erasureData);
    context.writeStageOne();
    erasure.writeStageTwo(context);
    sequence.writeStageThree(context);
    context.writeStageFour();
    erasure.writeStageFive(context);
    sequence.inject(context);
    erasure.inject(context);

    operations.complete(d, std::move(sequence), std::move(erasure));
}

void WriteSequence::add(const SequenceValue &value)
{
    advance(value.getStart());
    context(value.getName(), value.getStart(), value.getEnd()).add(value);
}

void WriteSequence::add(const ArchiveValue &value)
{
    advance(value.getStart());
    context(value.getName(), value.getStart(), value.getEnd()).add(value);
}

void WriteSequence::remove(const SequenceIdentity &erase)
{
    advance(erase.getStart());
    context(erase.getName(), erase.getStart(), erase.getEnd()).remove(erase);
}

void WriteSequence::remove(const SequenceValue &erase)
{
    advance(erase.getStart());
    context(erase.getName(), erase.getStart(), erase.getEnd()).remove(erase);
}

void WriteSequence::remove(const ArchiveValue &erase)
{
    advance(erase.getStart());
    context(erase.getName(), erase.getStart(), erase.getEnd()).remove(erase);
}

void WriteSequence::remove(const ArchiveErasure &erase)
{
    advance(erase.getStart());
    context(erase.getName(), erase.getStart(), erase.getEnd()).remove(erase);
}


WriteSynchronize::WriteSynchronize(Database::Connection *connection,
                                   Structure::Layout &layout,
                                   IdentityIndex &index,
                                   bool sorted) : WriteOperation(connection, layout, index, sorted)
{ }

WriteSynchronize::~WriteSynchronize() = default;

void WriteSynchronize::writeBlock(const WriteOperation::BlockOperations &operations,
                                  Structure::Block block,
                                  WriteBlock::Context &context)
{
    BlockOperations::Data d = operations.begin(block);

    WriteBlock::Sequence sequence(d.sequenceData);
    WriteBlock::Erasure erasure(d.erasureData);
    context.synchronizeStageOne();
    erasure.synchronizeStageTwo(context);
    sequence.synchronizeStageThree(context);
    sequence.inject(context);
    erasure.inject(context);

    operations.complete(d, std::move(sequence), std::move(erasure));
}

void WriteSynchronize::writeGlobal(const WriteOperation::GlobalOperations &operations,
                                   WriteBlock::Context &context)
{
    context.synchronizeStageOne();

    GlobalOperations::SequenceTarget sequenceTarget;
    GlobalOperations::ErasureTarget erasureTarget;

    operations.foreachErasure([&](Structure::Block block, const Util::ByteArray &erasureData) {
        GlobalOperations::ErasureTarget erasure(block, erasureData);
        erasure.target->synchronizeStageTwo(context);
        if (!erasureTarget.shouldReplace(erasure)) {
            operations.commit(erasure);
        } else {
            operations.commit(erasureTarget);
            erasureTarget = std::move(erasure);
        }
    });

    operations.foreachSequence([&](Structure::Block block, const Util::ByteArray &sequenceData) {
        GlobalOperations::SequenceTarget sequence(block, sequenceData);
        sequence.target->synchronizeStageThree(context);
        if (!sequenceTarget.shouldReplace(sequence)) {
            operations.commit(sequence);
        } else {
            operations.commit(sequenceTarget);
            sequenceTarget = std::move(sequence);
        }
    });

    if (context.hasSequence()) {
        operations.acquireTarget(sequenceTarget);
        sequenceTarget.target->inject(context);
    }

    if (context.hasErasure()) {
        operations.acquireTarget(erasureTarget);
        erasureTarget.target->inject(context);
    }

    operations.commit(sequenceTarget);
    operations.commit(erasureTarget);
}

void WriteSynchronize::add(const ArchiveValue &value)
{
    advance(value.getStart());
    context(value.getName(), value.getStart(), value.getEnd()).add(value);
}

void WriteSynchronize::remove(const ArchiveErasure &erase)
{
    advance(erase.getStart());
    context(erase.getName(), erase.getStart(), erase.getEnd()).remove(erase);
}


FlagRemoteReferenced::FlagRemoteReferenced(Database::Connection *connection,
                                           Structure::Layout &layout,
                                           IdentityIndex &index,
                                           bool sorted) : WriteOperation(connection, layout, index,
                                                                         sorted)
{ }

FlagRemoteReferenced::~FlagRemoteReferenced() = default;

WriteOperation::BlockContext FlagRemoteReferenced::createContext()
{ return std::make_shared<WriteBlock::Sequence::RemoteReferenceContext>(getIndex().flavor); }

void FlagRemoteReferenced::writeBlock(const WriteOperation::BlockOperations &operations,
                                      Structure::Block block,
                                      WriteBlock::Context &context)
{
    auto binds = operations.readSequenceBlock.binds();
    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    auto sequenceData = operations.readSequenceBlock.statement().single(binds).takeBytes();
    if (sequenceData.empty())
        return;

    WriteBlock::Sequence sequence(sequenceData);
    sequence.remoteReferenced(static_cast<WriteBlock::Sequence::RemoteReferenceContext &>(context));
    if (!sequence.isModified())
        return;

    sequenceData = sequence.outputData();
    if (sequenceData.empty()) {
        qCWarning(log_datacore_archive_writeoperation) << "Network flag operation emptied data";
        return;
    }

    binds = operations.writeSequenceBlock.binds();
    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    binds.emplace(Structure::Column_Modified,
                  getLayout().targetModified(sequence.latestModified()));
    binds.emplace(Structure::Column_Data, std::move(sequenceData));
    sequence = WriteBlock::Sequence();
    operations.writeSequenceBlock.statement().synchronous(binds);
}

void FlagRemoteReferenced::writeGlobal(const WriteOperation::GlobalOperations &operations,
                                       WriteBlock::Context &context)
{
    operations.foreachSequence([&](Structure::Block block, const Util::ByteArray &sequenceData) {
        GlobalOperations::SequenceTarget sequence(block, sequenceData);
        sequence.target
                ->remoteReferenced(
                        static_cast<WriteBlock::Sequence::RemoteReferenceContext &>(context));
        operations.commit(sequence);
    });
}

void FlagRemoteReferenced::add(const SequenceValue &value)
{
    advance(value.getStart());
    static_cast<WriteBlock::Sequence::RemoteReferenceContext &>(context(value.getName(),
                                                                        value.getStart(),
                                                                        value.getEnd())).reference(
            value);
}

void FlagRemoteReferenced::add(const ArchiveValue &value)
{
    advance(value.getStart());
    static_cast<WriteBlock::Sequence::RemoteReferenceContext &>(context(value.getName(),
                                                                        value.getStart(),
                                                                        value.getEnd())).reference(
            value);
}

void FlagRemoteReferenced::add(const SequenceIdentity &value)
{
    advance(value.getStart());
    static_cast<WriteBlock::Sequence::RemoteReferenceContext &>(context(value.getName(),
                                                                        value.getStart(),
                                                                        value.getEnd())).reference(
            value);
}


}
}
}
