/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREARCHIVE_SELECTION_HXX
#define CPD3DATACOREARCHIVE_SELECTION_HXX

#include "core/first.hxx"

#include <vector>
#include <string>
#include <QDataStream>
#include <QDebug>

#include "datacore/datacore.hxx"
#include "datacore/stream.hxx"
#include "core/number.hxx"
#include "core/qtcompat.hxx"

namespace CPD3 {
namespace Data {

namespace Archive {

/**
 * A single component of selection.
 */
struct CPD3DATACORE_EXPORT Selection {

    typedef std::vector<Selection> List;

    typedef std::vector<SequenceName::Component> Match;

    /** The start time or FP::undefined() for infinite. */
    double start;
    /** The end time or FP::undefined() for infinite. */
    double end;
    /** The stations to match or empty for all. */
    Match stations;
    /** The archives to match or empty for all. */
    Match archives;
    /** The variables to match or empty for all. */
    Match variables;
    /**
     * The flavors the result must have, or empty to ignore.
     * Every entry must match at least one flavor.
     */
    Match hasFlavors;
    /**
     * The flavors the result must NOT have, or empty to ignore.
     * If any flavor matches any entry, then the value is discarded.
     */
    Match lacksFlavors;
    /**
     * The exact set of flavors to match, of empty for any.
     * Every flavor an item has must match an entry.
     */
    Match exactFlavors;
    /** The minimum modification time to match. */
    double modifiedAfter;

    /** Include the default station. */
    bool includeDefaultStation;
    /** Include all metadata archives for the query. */
    bool includeMetaArchive;

    Selection();

    Selection(double setStart,
              double setEnd,
              const Match &setStations = Match(),
              const Match &setArchives = Match(),
              const Match &setVariables = Match(),
              const Match &setHasFlavors = Match(),
              const Match &setLacksFlavors = Match(),
              const Match &setExactFlavors = Match(),
              double setModifiedAfter = FP::undefined());

    explicit Selection(const SequenceName &name,
                       double start = FP::undefined(),
                       double end = FP::undefined(),
                       double setModifiedAfter = FP::undefined());

    Selection(const Selection &other);

    Selection &operator=(const Selection &other);

    Selection(Selection &&other);

    Selection &operator=(Selection &&other);

    /**
     * Create an archive selection with the default station included or not.
     * <br>
     * Note that this handles the case where the incoming expression included all
     * stations (stations list empty) and the result will NOT include the default
     * station in that case.  If the stations list simply matched all stations
     * already (e.x. ".*") then it will still be included.  That is, this only
     * handles the implicit including of the default station.  Similarly, if
     * the station list has had an explicit exclude added then this will not
     * reverse it.  So Selection().withDefaultStation(false).withDefaultStation(true)
     * will produce a result that does NOT include the default station.
     *
     * @param include       include or exclude
     * @return              a Selection with or without the implicit default station
     */
    Selection withDefaultStation(bool include) const;

    /**
     * Create an archive selection with the metadata archives included or not.
     * <br>
     * Note that this handles the case where the incoming expression included all
     * archives (archives list empty) and the result will NOT include the metadata
     * archives in that case.  If the archives list simply matched all archives
     * already (e.x. ".*") then they will still be included.  That is, this only
     * handles the implicit including of the metadata archives.  Similarly, if
     * the archive list has had an explicit exclude added then this will not
     * reverse it.  So Selection().withMetaArchive(false).withMetaArchive(true)
     * will produce a result that does NOT include metadata archives.
     *
     * @param include       include or exclude
     * @return              a Selection with or without the implicit metadata archives
     */
    Selection withMetaArchive(bool include) const;

    /**
     * Get the start time.  Provided for compatibility with range interface.
     *
     * @return the start time.
     */
    inline double getStart() const
    { return start; }

    /**
     * Get the end time.  Provided for compatibility with range interface.
     *
     * @return the end time.
     */
    inline double getEnd() const
    { return end; }

    /**
     * Set the start time.  Provided for compatibility with range interface.
     *
     * @param v the start time
     */
    inline void setStart(double v)
    { start = v; }

    /**
     * Set the end time.  Provided for compatibility with range interface.
     *
     * @param v the end time
     */
    inline void setEnd(double v)
    { end = v; }

    /**
     * Get a string regular expression that explicitly matches non-metadata
     * archives.
     *
     * @return a regular expression that matches non-metadata archives
     */
    static std::string excludeMetaArchiveMatcher();
};

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Selection &value);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, Selection &value);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const Selection &v);

}
}
}

#ifdef CPD3_QTEST

namespace CPD3 {
namespace Data {
namespace Archive {

inline void appendSelectionList(QByteArray &data, const CPD3::Data::Archive::Selection::Match &list)
{
    if (list.size() == 1 && list.begin()->empty()) {
        data += "-NULL-";
    } else {
        std::vector<std::string> sorted(list.begin(), list.end());
        std::sort(sorted.begin(), sorted.end());
        bool first = true;
        for (const auto &add : sorted) {
            if (!first)
                data += ',';
            first = false;
            data += QByteArray::fromStdString(add);
        }
    }
}

inline char *toString(const CPD3::Data::Archive::Selection &v)
{
    QByteArray data = "Selection(bounds=";
    appendRange(data, v.start, v.end);
    data += ",stations=[";
    appendSelectionList(data, v.stations);
    data += "],archives=[";
    appendSelectionList(data, v.archives);
    data += "],variables=[";
    appendSelectionList(data, v.variables);
    data += "],hasFlavors=[";
    appendSelectionList(data, v.hasFlavors);
    data += "],lacksFlavors=[";
    appendSelectionList(data, v.lacksFlavors);
    data += "],exactFlavors=[";
    appendSelectionList(data, v.exactFlavors);
    data += "],modifiedAfter=";
    appendTime(data, v.modifiedAfter);
    if (!v.includeDefaultStation && !v.includeMetaArchive) {
        data += ",NoDefaultStation|NoMetaArchive)";
    } else if (!v.includeMetaArchive) {
        data += ",NoMetaArchive)";
    } else if (!v.includeDefaultStation) {
        data += ",NoDefaultStation)";
    } else {
        data += ")";
    }
    return ::qstrdup(data.data());
}

}
}
}

#endif

#endif //CPD3DATACOREARCHIVE_SELECTION_HXX
