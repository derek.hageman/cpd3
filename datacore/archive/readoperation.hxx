/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREARCHIVE_READOPERATION_HXX
#define CPD3DATACOREARCHIVE_READOPERATION_HXX

#include "core/first.hxx"

#include <memory>
#include <list>
#include <unordered_set>
#include <set>
#include <mutex>
#include <thread>
#include <functional>
#include <condition_variable>
#include <QThread>

#include "datacore/datacore.hxx"
#include "datacore/stream.hxx"
#include "core/number.hxx"
#include "core/merge.hxx"
#include "core/threading.hxx"
#include "database/context.hxx"
#include "database/util.hxx"
#include "io/process.hxx"
#include "structure.hxx"
#include "selection.hxx"
#include "sequencefilter.hxx"

namespace CPD3 {
namespace Data {
namespace Archive {

struct IdentityIndex;

/**
 * The base class for reads from the archive.  This handles the concurrancy
 * and index reading of the archive, leaving the actual value unpacking
 * and handling to its derived classes.
 */
class CPD3DATACORE_EXPORT ReadOperation {
    Structure::Layout &layout;
    IdentityIndex &index;

    std::mutex mutex;
    std::condition_variable notify;
    std::thread thread;

    Threading::PoolRun lookupRun;
    Threading::PoolRun unpackRun;

    enum {
        State_Initialize,
        State_Running,
        State_TerminateRequested,
        State_Terminating,
        State_Finishing,
        State_Complete
    } state;

protected:

    class Stream;

    /**
     * The base definition of a block of data.  This is used as the context
     * for fetching the data from the archive.
     */
    class CPD3DATACORE_EXPORT Block {
        friend class Stream;

        Stream &stream;
        Structure::Block block;
        double modified;
        Structure::Identity identity;

        SequenceName name;
        Database::Context readContext;

        enum {
            State_Initialize,
            State_NameLookup,
            State_ReadData,
            State_Unpack,
            State_Complete,
            State_Failed,
        } state;

        void nameLookup();

        void readResult(const Database::Types::Results &results);

        void readComplete();

        void readUnpack(const Util::ByteArray &data);

        void failBlock();

    protected:
        /**
         * Create the data block.
         *
         * @param stream    the parent data stream
         * @param block     the block index
         * @param modified  the index modified time of the block
         * @param identity  the index identity of the block
         */
        Block(Stream &stream,
              Structure::Block block,
              double modified,
              const Structure::Identity &identity);

        /**
         * Create the data block read query.
         * 
         * @param identity  the final identity of the block
         * @param block     the block index
         * @return          the query to read the block data
         */
        virtual Database::Context createRead(const Structure::Identity &identity,
                                             Structure::Block block) = 0;

        /**
         * Perform the final unpack of the block data.
         * 
         * @param data      the raw block data 
         * @param baseName  the base name of the block
         * @param flavors   the flavors index
         * @param filter    the block filter
         * @param lock      the lock (not held) used by the operation
         * @return          true if the main processing thread needs to be awoken
         */
        virtual bool unpack(const Util::ByteArray &data,
                            const SequenceName &baseName,
                            StringIndex &flavors,
                            SequenceFilter::Block &filter,
                            std::unique_lock<std::mutex> &lock) = 0;

        /**
         * The name short circuit processing.
         * <br>
         * If short circuiting is applied, the lock must be acquired.
         *
         * @parma block     the block index
         * @param name      the block name
         * @param lock      the lock (unheld) used by the operation
         * @return          true if the short circuiting applied and no further processing should be performed
         */
        virtual bool nameProcess(Structure::Block block,
                                 const SequenceName &name,
                                 std::unique_lock<std::mutex> &lock);

        /**
         * Request an abort of the entire read operation.
         *
         * @param lock      the lock used
         */
        void operationAbort(std::unique_lock<std::mutex> &lock);

    public:
        virtual ~Block();

        virtual void start();
    };

    friend class Block;

    /**
     * The base interface for a stream reading from the index.  Each stream 
     * is composed of blocks that define the actual data.
     */
    class CPD3DATACORE_EXPORT Stream {
        friend class ReadOperation;

        ReadOperation &operation;
        std::unique_ptr<SequenceFilter::Input> input;

        Database::Context indexContext;

        typedef std::unique_ptr<Block> BlockPointer;

        enum {
            State_Initialize, State_ReadingIndex, State_IndexPaused, State_Complete,
        } state;
        Structure::Block latestReadBlock;
        Structure::Block readUntilBlock;
        std::multimap<Structure::Block, BlockPointer> blocksInFlight;

        void start();

        void indexResult(const Database::Types::Results &results);

        void indexComplete();

        void completeBlock(Block *block, bool wake = false);

        bool shouldAdvanceIndex();

        Structure::Block beginProcess();

        bool endProcess(std::unique_lock<std::mutex> &lock,
                        Structure::Block threshold,
                        bool &complete);

        void signalTerminate();

    protected:
        /**
         * Create the stream.
         * 
         * @param operation     the parent read operation 
         * @param input         the input stream definition
         */
        Stream(ReadOperation &operation, std::unique_ptr<SequenceFilter::Input> &&input);

        /**
         * Create the index read query.
         * 
         * @return  the index read query 
         */
        virtual Database::Context createIndex() = 0;

        /**
         * Create a block from an index result.
         * 
         * @param block     the block index 
         * @param modified  the modified time from the index
         * @param identity  the identity definition from the index
         * @return          a new block
         */
        virtual std::unique_ptr<Block> createBlock(Structure::Block block,
                                                   double modified,
                                                   const Structure::Identity &identity) = 0;

        /**
         * Advance the stream to the given time.  This is called from the processing
         * thread with the lock held based on how far all block have completed
         * processing and index has read.  That is, the stream is only advanced to the
         * point where no more start times before it will be seen.
         * 
         * @param time      the time to advance to 
         */
        virtual void advanceStream(double time) = 0;

        /**
         * Called on stream completion with the lock held.
         */
        virtual void completeStream() = 0;

        /**
         * Get the input stream.
         * 
         * @return  a reference to the input stream definition 
         */
        inline SequenceFilter::Input &streamInput()
        { return *input; }

    public:
        virtual ~Stream();
    };

    friend class Stream;

    /**
     * Start a stream.  Only valid before the operation is started and normally
     * used as part of construction.
     * 
     * @param stream    the stream to add 
     */
    void startStream(std::unique_ptr<Stream> &&stream);

    /**
     * Create the read operation.
     * 
     * @param layout    the archive layout definition 
     * @param index     the index lookup
     */
    ReadOperation(Structure::Layout &layout, IdentityIndex &index);

    /**
     * Called from the processing thread, with the lock held to process
     * any outptu data.
     * 
     * @param lock      the lock 
     * @return          true if the lock was released at all
     */
    virtual bool processOutput(std::unique_lock<std::mutex> &lock) = 0;

    /**
     * Called from the processing thread, with the lock held to process
     * any output data.
     * 
     * @param lock      the lock 
     * @return          true to wait for a notification
     */
    virtual bool finishOutput(std::unique_lock<std::mutex> &lock) = 0;

    /**
     * Wait a notification.
     * 
     * @param lock      the lock 
     */
    void waitForNotify(std::unique_lock<std::mutex> &lock);

    inline Structure::Layout &operationLayout()
    { return layout; }

    virtual void terminateStreams();

private:

    typedef std::unique_ptr<Stream> StreamPointer;

    std::vector<StreamPointer> streams;

    void run();

    bool processStreams(std::unique_lock<std::mutex> &lock);

public:
    virtual ~ReadOperation();

    /**
     * Abort the read operation.
     */
    void signalTerminate();

    /**
     * Start the read operation.
     */
    void start();

    /**
     * Wait for the read operation to complete.
     * 
     * @param timeout   the maximum time to wait 
     * @return          true if the operation has completed
     */
    bool wait(double timeout = FP::undefined());

    /**
     * Test if the read operation has completed.
     *
     * @return          true if the operation has completed
     */
    bool isComplete();

    /**
     * Explicitly join the thread.  This is provided so references to the signal
     * calling a parent can be cleaned up without the complete() signal ending
     * up in a spinning state (which is safe, but inefficient).
     * <br>
     * This is also used by child class destructors, to make sure the child
     * members are not destroyed while the thread is still active.
     *
     * @param terminate also terminate the thread, if needed
     */
    void join(bool terminate = true);

    /**
     * A signal generated from the read thread once it has completed work.
     */
    Threading::Signal<> complete;

    /**
     * Run with the lock held, if the operation has not yet completed.
     * This also wakes the processing thread after the operation completes.
     *
     * @param operation the operation to perform with the lock held
     * @return          true if the operation was run
     */
    bool runWithLock(const std::function<void()> &operation);
};

/**
 * A simple sequence value read operation.
 */
class CPD3DATACORE_EXPORT ReadSequence final
        : public ReadOperation, public CPD3::Data::StreamSource {

    /* Since we're using unsorted values in bulk, use the native container type */
    typedef StreamMultiplexer<SequenceValue, SequenceValue::Transfer> Multiplexer;
    Multiplexer multiplexer;

    std::mutex sinkMutex;
    StreamSink *sink;

    class SequenceStream;

    class SequenceBlock final : public Block {
        SequenceStream &stream;
    public:
        SequenceBlock(SequenceStream &stream,
                      Structure::Block block,
                      double modified,
                      const Structure::Identity &identity);

        virtual ~SequenceBlock();

    protected:

        Database::Context createRead(const Structure::Identity &identity,
                                             Structure::Block block) override;

        bool unpack(const Util::ByteArray &data,
                            const SequenceName &baseName,
                            StringIndex &flavors,
                            SequenceFilter::Block &filter,
                            std::unique_lock<std::mutex> &lock) override;
    };

    class SequenceStream final : public Stream {
        ReadSequence &operation;
        Multiplexer::Unsorted *target;

        Database::Statement readStatement;
        Database::Types::Binds readBinds;

        friend class SequenceBlock;

    public:
        SequenceStream(ReadSequence &operation, std::unique_ptr<SequenceFilter::Input> &&input);

        virtual ~SequenceStream();

        Database::Context createIndex() override;

        std::unique_ptr<Block> createBlock(Structure::Block block,
                                           double modified,
                                           const Structure::Identity &identity) override;

        void advanceStream(double time) override;

        void completeStream() override;

    protected:
        inline SequenceFilter::Input &input()
        { return streamInput(); }
    };

    friend class SequenceStream;

    class ForgeSequenceLoopBackStream;

    friend class ForgeSequenceLoopBackStream;

    std::unique_ptr<ForgeSequenceLoopBackStream> forgeLoopbackStream;

public:
    /**
     * Create the read operation.
     * 
     * @param layout        the archive layout 
     * @param index         the archive index
     * @param selections    the selections to read from the archive
     */
    ReadSequence(Structure::Layout &layout,
                 IdentityIndex &index,
                 const Selection::List &selections);

    virtual ~ReadSequence();

    void setEgress(StreamSink *sink) override;

protected:

    bool processOutput(std::unique_lock<std::mutex> &lock) override;

    bool finishOutput(std::unique_lock<std::mutex> &lock) override;

    inline Structure::Layout &layout()
    { return operationLayout(); }

    void terminateStreams() override;
};


/**
 * A full archived value read operation.
 */
class CPD3DATACORE_EXPORT ReadArchive final
        : public ReadOperation, public CPD3::Data::ArchiveSource {

    /* Since we're using unsorted values in bulk, use the native container type */
    typedef StreamMultiplexer<ArchiveValue, ArchiveValue::Transfer> Multiplexer;
    Multiplexer multiplexer;

    std::mutex sinkMutex;
    ArchiveSink *sink;

    class ArchiveStream;

    class ArchiveBlock final : public Block {
        ArchiveStream &stream;
    public:
        ArchiveBlock(ArchiveStream &stream,
                     Structure::Block block,
                     double modified,
                     const Structure::Identity &identity);

        virtual ~ArchiveBlock();

    protected:

        Database::Context createRead(const Structure::Identity &identity,
                                             Structure::Block block) override;

        bool unpack(const Util::ByteArray &data,
                            const SequenceName &baseName,
                            StringIndex &flavors,
                            SequenceFilter::Block &filter,
                            std::unique_lock<std::mutex> &lock) override;
    };

    class ArchiveStream final : public Stream {
        ReadArchive &operation;
        Multiplexer::Unsorted *target;

        Database::Statement readStatement;
        Database::Types::Binds readBinds;

        friend class ArchiveBlock;

    public:
        ArchiveStream(ReadArchive &operation, std::unique_ptr<SequenceFilter::Input> &&input);

        virtual ~ArchiveStream();

        Database::Context createIndex() override;

        std::unique_ptr<Block> createBlock(Structure::Block block,
                                           double modified,
                                           const Structure::Identity &identity) override;

        void advanceStream(double time) override;

        void completeStream() override;

    protected:
        inline SequenceFilter::Input &input()
        { return streamInput(); }
    };

    friend class ArchiveStream;

    class ForgeArchiveLoopBackStream;

    friend class ForgeArchiveLoopBackStream;

    std::unique_ptr<ForgeArchiveLoopBackStream> forgeLoopbackStream;

public:
    /**
     * Create the read operation.
     * 
     * @param layout        the archive layout 
     * @param index         the archive index
     * @param selections    the selections to read from the archive
     */
    ReadArchive(Structure::Layout &layout, IdentityIndex &index, const Selection::List &selections);

    virtual ~ReadArchive();

    void setEgress(ArchiveSink *sink) override;

protected:

    bool processOutput(std::unique_lock<std::mutex> &lock) override;

    bool finishOutput(std::unique_lock<std::mutex> &lock) override;

    inline Structure::Layout &layout()
    { return operationLayout(); }

    void terminateStreams() override;
};


/**
 * An archive and erasure read operation.
 */
class CPD3DATACORE_EXPORT ReadErasure final
        : public ReadOperation, public CPD3::Data::ErasureSource {

    struct CombinedValue {
        std::unique_ptr<ArchiveValue> value;
        std::unique_ptr<ArchiveErasure> erasure;

        CombinedValue() = default;

        inline CombinedValue(ArchiveValue &&v) : value(new ArchiveValue(std::move(v))), erasure()
        { }

        inline CombinedValue(ArchiveErasure &&v)
                : value(), erasure(new ArchiveErasure(std::move(v)))
        { }

        CombinedValue(CombinedValue &&other) = default;

        CombinedValue &operator=(CombinedValue &&other) = default;

        inline double getStart() const
        {
            if (value)
                return value->getStart();
            return erasure->getStart();
        }

        inline double getEnd() const
        {
            if (value)
                return value->getEnd();
            return erasure->getEnd();
        }
    };

    typedef std::vector<CombinedValue> Transfer;
    typedef StreamMultiplexer<CombinedValue, Transfer> Multiplexer;
    Multiplexer multiplexer;

    std::mutex sinkMutex;
    ErasureSink *sink;


    class ArchiveStream;

    class ArchiveBlock final : public Block {
        ArchiveStream &stream;
    public:
        ArchiveBlock(ArchiveStream &stream,
                     Structure::Block block,
                     double modified,
                     const Structure::Identity &identity);

        virtual ~ArchiveBlock();

    protected:

        Database::Context createRead(const Structure::Identity &identity,
                                             Structure::Block block) override;

        bool unpack(const Util::ByteArray &data,
                            const SequenceName &baseName,
                            StringIndex &flavors,
                            SequenceFilter::Block &filter,
                            std::unique_lock<std::mutex> &lock) override;
    };

    class ArchiveStream final : public Stream {
        ReadErasure &operation;
        Multiplexer::Unsorted *target;

        Database::Statement readStatement;
        Database::Types::Binds readBinds;

        friend class ArchiveBlock;

    public:
        ArchiveStream(ReadErasure &operation, std::unique_ptr<SequenceFilter::Input> &&input);

        virtual ~ArchiveStream();

        Database::Context createIndex() override;

        std::unique_ptr<Block> createBlock(Structure::Block block,
                                           double modified,
                                           const Structure::Identity &identity) override;

        void advanceStream(double time) override;

        void completeStream() override;

    protected:
        inline SequenceFilter::Input &input()
        { return streamInput(); }
    };

    friend class ArchiveStream;


    class ErasureStream;

    class ErasureBlock final : public Block {
        ErasureStream &stream;
    public:
        ErasureBlock(ErasureStream &stream,
                     Structure::Block block,
                     double modified,
                     const Structure::Identity &identity);

        virtual ~ErasureBlock();

    protected:

        Database::Context createRead(const Structure::Identity &identity,
                                             Structure::Block block) override;

        bool unpack(const Util::ByteArray &data,
                            const SequenceName &baseName,
                            StringIndex &flavors,
                            SequenceFilter::Block &filter,
                            std::unique_lock<std::mutex> &lock) override;
    };

    class ErasureStream final : public Stream {
        ReadErasure &operation;
        Multiplexer::Unsorted *target;

        Database::Statement readStatement;
        Database::Types::Binds readBinds;

        friend class ErasureBlock;

    public:
        ErasureStream(ReadErasure &operation, std::unique_ptr<SequenceFilter::Input> &&input);

        virtual ~ErasureStream();

        Database::Context createIndex() override;

        std::unique_ptr<Block> createBlock(Structure::Block block,
                                           double modified,
                                           const Structure::Identity &identity) override;

        void advanceStream(double time) override;

        void completeStream() override;

    protected:
        inline SequenceFilter::Input &input()
        { return streamInput(); }
    };

    friend class ErasureStream;


    void dispatchValues(Transfer &&values);

public:
    /**
     * Create the read operation.
     * 
     * @param layout        the archive layout 
     * @param index         the archive index
     * @param selections    the selections to read from the archive
     */
    ReadErasure(Structure::Layout &layout, IdentityIndex &index, const Selection::List &selections);

    virtual ~ReadErasure();

    void setEgress(ErasureSink *sink) override;

protected:

    bool processOutput(std::unique_lock<std::mutex> &lock) override;

    bool finishOutput(std::unique_lock<std::mutex> &lock) override;

    inline Structure::Layout &layout()
    { return operationLayout(); }
};


namespace Available {

/**
 * The types of granularity for availability checking.
 */
enum Granularity {
    /**
     * Read the archive in full detail, including flavors. */
            ReadAll,

    /**
     * Only read the spanning blocks in detail, using the index for the remainder.
     * Flavors are not available or checked in non-spanning blocks.
     */
            SpanningOnly,

    /**
     * Read the index only.  No flavor information is available and all spanning
     * blocks are considered to be matched.
     */
            IndexOnly
};

/**
 * A query from the archive of the available sequence names.
 */
class CPD3DATACORE_EXPORT Names final : public ReadOperation {
    Granularity granularity;
    SequenceName::Set output;

    class AvailableStream;

    class AvailableBlock final : public Block {
        AvailableStream &stream;
    public:
        AvailableBlock(AvailableStream &stream,
                       Structure::Block block,
                       double modified,
                       const Structure::Identity &identity);

        virtual ~AvailableBlock();

    protected:

        Database::Context createRead(const Structure::Identity &identity,
                                             Structure::Block block) override;

        bool unpack(const Util::ByteArray &data,
                            const SequenceName &baseName,
                            StringIndex &flavors,
                            SequenceFilter::Block &filter,
                            std::unique_lock<std::mutex> &lock) override;

        bool nameProcess(Structure::Block block,
                                 const SequenceName &name,
                                 std::unique_lock<std::mutex> &lock) override;
    };

    friend class AvailableBlock;

    class AvailableStream final : public Stream {
        Names &operation;

        Database::Statement readStatement;
        Database::Types::Binds readBinds;

        friend class AvailableBlock;

    public:
        AvailableStream(Names &operation, std::unique_ptr<SequenceFilter::Input> &&input);

        virtual ~AvailableStream();

        Database::Context createIndex() override;

        std::unique_ptr<Block> createBlock(Structure::Block block,
                                           double modified,
                                           const Structure::Identity &identity) override;

        void advanceStream(double time) override;

        void completeStream() override;

    protected:
        inline SequenceFilter::Input &input()
        { return streamInput(); }
    };

    friend class AvailableStream;

public:
    /**
     * Create the available query operation.
     * 
     * @param layout        the archive layout 
     * @param index         the archive index
     * @param selections    the selections to read from the archive
     * @param granularity   the read granularity
     */
    Names(Structure::Layout &layout,
          IdentityIndex &index,
          const Selection::List &selections,
          Granularity granularity = SpanningOnly);

    virtual ~Names();

    /**
     * Get the result of the available read operation.
     * 
     * @return          the sequence names matching the selections 
     */
    SequenceName::Set result() const;

protected:

    bool processOutput(std::unique_lock<std::mutex> &lock) override;

    bool finishOutput(std::unique_lock<std::mutex> &lock) override;

    inline Structure::Layout &layout()
    { return operationLayout(); }
};


/**
 * A sequence name component available read operation.  This provides
 * a means of inspecting what components of sequence names match
 * the selections, without separating them down into full sequence names.
 */
class CPD3DATACORE_EXPORT Components final : public ReadOperation {
    Granularity granularity;
    SequenceName::ComponentSet outputStations;
    SequenceName::ComponentSet outputArchives;
    SequenceName::ComponentSet outputVariables;
    SequenceName::ComponentSet outputFlavors;

    class AvailableStream;

    class AvailableBlock final : public Block {
        AvailableStream &stream;
    public:
        AvailableBlock(AvailableStream &stream,
                       Structure::Block block,
                       double modified,
                       const Structure::Identity &identity);

        virtual ~AvailableBlock();

    protected:

        Database::Context createRead(const Structure::Identity &identity,
                                             Structure::Block block) override;

        bool unpack(const Util::ByteArray &data,
                            const SequenceName &baseName,
                            StringIndex &flavors,
                            SequenceFilter::Block &filter,
                            std::unique_lock<std::mutex> &lock) override;

        bool nameProcess(Structure::Block block,
                                 const SequenceName &name,
                                 std::unique_lock<std::mutex> &lock) override;
    };

    friend class AvailableBlock;

    class AvailableStream final : public Stream {
        Components &operation;

        Database::Statement readStatement;
        Database::Types::Binds readBinds;

        friend class AvailableBlock;

    public:
        AvailableStream(Components &operation, std::unique_ptr<SequenceFilter::Input> &&input);

        virtual ~AvailableStream();

        Database::Context createIndex() override;

        std::unique_ptr<Block> createBlock(Structure::Block block,
                                           double modified,
                                           const Structure::Identity &identity) override;

        void advanceStream(double time) override;

        void completeStream() override;

    protected:
        inline SequenceFilter::Input &input()
        { return streamInput(); }
    };

    friend class AvailableStream;

public:
    /**
     * Create the available query operation.
     * 
     * @param layout        the archive layout 
     * @param index         the archive index
     * @param selections    the selections to read from the archive
     * @param granularity   the read granularity
     */
    Components(Structure::Layout &layout,
               IdentityIndex &index,
               const Selection::List &selections,
               Granularity granularity = SpanningOnly);

    virtual ~Components();

    /**
     * Get the stations that matched the selections.
     * 
     * @return  the matching stations 
     */
    SequenceName::ComponentSet stations() const;

    /**
     * Read all possible stations from the archive index.
     * 
     * @param index the archive index 
     * @return      all possible stations
     */
    static SequenceName::ComponentSet allStations(IdentityIndex &index);

    /**
     * Get the archives that matched the selections.
     * 
     * @return  the matching archives 
     */
    SequenceName::ComponentSet archives() const;

    /**
     * Read all possible archives from the archive index.
     * 
     * @param index the archive index 
     * @return      all possible archives
     */
    static SequenceName::ComponentSet allArchives(IdentityIndex &index);

    /**
     * Get the variables that matched the selections.
     * 
     * @return  the matching variables 
     */
    SequenceName::ComponentSet variables() const;

    /**
     * Read all possible variables from the archive index.
     * 
     * @param index the archive index 
     * @return      all possible variables
     */
    static SequenceName::ComponentSet allVariables(IdentityIndex &index);

    /**
     * Get the flavors that matched the selections.
     * <br>
     * This is only valid in exact mode.
     * 
     * @return  the matching flavors 
     */
    SequenceName::ComponentSet flavors() const;

    /**
     * Read all possible flavors from the archive index.
     * 
     * @param index the archive index 
     * @return      all possible flavors
     */
    static SequenceName::ComponentSet allFlavors(IdentityIndex &index);

protected:

    bool processOutput(std::unique_lock<std::mutex> &lock) override;

    bool finishOutput(std::unique_lock<std::mutex> &lock) override;

    inline Structure::Layout &layout()
    { return operationLayout(); }
};


/**
 * A query from the archive of the available sequence names.
 */
class CPD3DATACORE_EXPORT Exists final : public ReadOperation {
    Granularity granularity;
    bool output;

    class AvailableStream;

    class AvailableBlock final : public Block {
        AvailableStream &stream;
    public:
        AvailableBlock(AvailableStream &stream,
                       Structure::Block block,
                       double modified,
                       const Structure::Identity &identity);

        virtual ~AvailableBlock();

    protected:

        Database::Context createRead(const Structure::Identity &identity,
                                             Structure::Block block) override;

        bool unpack(const Util::ByteArray &data,
                            const SequenceName &baseName,
                            StringIndex &flavors,
                            SequenceFilter::Block &filter,
                            std::unique_lock<std::mutex> &lock) override;

        bool nameProcess(Structure::Block block,
                                 const SequenceName &name,
                                 std::unique_lock<std::mutex> &lock) override;
    };

    friend class AvailableBlock;

    class AvailableStream final : public Stream {
        Exists &operation;

        Database::Statement readStatement;
        Database::Types::Binds readBinds;

        friend class AvailableBlock;

    public:
        AvailableStream(Exists &operation, std::unique_ptr<SequenceFilter::Input> &&input);

        virtual ~AvailableStream();

        Database::Context createIndex() override;

        std::unique_ptr<Block> createBlock(Structure::Block block,
                                           double modified,
                                           const Structure::Identity &identity) override;

        void advanceStream(double time) override;

        void completeStream() override;

    protected:
        inline SequenceFilter::Input &input()
        { return streamInput(); }
    };

    friend class AvailableStream;

public:
    /**
     * Create the available query operation.
     *
     * @param layout        the archive layout
     * @param index         the archive index
     * @param selections    the selections to read from the archive
     * @param granularity   the read granularity
     */
    Exists(Structure::Layout &layout,
           IdentityIndex &index,
           const Selection::List &selections,
           Granularity granularity = SpanningOnly);

    virtual ~Exists();

    /**
     * Get the result of the available read operation.
     *
     * @return          true if anything matching the selections existed
     */
    bool result() const;

protected:

    bool processOutput(std::unique_lock<std::mutex> &lock) override;

    bool finishOutput(std::unique_lock<std::mutex> &lock) override;

    inline Structure::Layout &layout()
    { return operationLayout(); }
};

}

}
}
}

#endif //CPD3DATACOREARCHIVE_READOPERATION_HXX
