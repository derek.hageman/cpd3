/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREARCHIVE_STRUCTURE_HXX
#define CPD3DATACOREARCHIVE_STRUCTURE_HXX

#include "core/first.hxx"

#include <cstddef>
#include <cstdint>
#include <string>
#include <memory>
#include <unordered_set>

#include "datacore/datacore.hxx"
#include "database/connection.hxx"
#include "database/util.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Data {
namespace Archive {

namespace Structure {

/**
 * The type used for block indexing.
 */
typedef std::int_fast16_t Block;

/**
 * The minimum width strict type used for block indexing.
 */
typedef std::int16_t StrictBlock;

/**
 * The index identity definition.
 */
struct CPD3DATACORE_EXPORT Identity {
    /**
     * The type used for index identities.
     */
    typedef std::uint_fast16_t index;

    /**
     * The minimum width strict type used for index identities.
     */
    typedef std::uint16_t strict;

    /**
     * The station, or zero for unset or unspecified.
     */
    Identity::index station;

    /**
     * The archive, or zero for unset or unspecified.
     */
    Identity::index archive;

    /**
     * The variable, or zero for unset or unspecified.
     */
    Identity::index variable;

    Identity();

    /**
     * Initialize the identity.
     * @param station   the station
     * @param archive   the archive
     * @param variable  the variable
     */
    Identity(Identity::index station, Identity::index archive, Identity::index variable);

    inline bool operator==(const Identity &other) const
    { return station == other.station && archive == other.archive && variable == other.variable; }

    inline bool operator!=(const Identity &other) const
    { return station != other.station || archive != other.archive || variable != other.variable; }
};


/**
 * The central interface for information about accessing the underlying physical
 * archive.
 */
class CPD3DATACORE_EXPORT Layout {
    class Backend;

    class Backend_V1;

    class Backend_V2;

    friend class Backend;

    std::unique_ptr<Backend> backend;

    static Backend *createBackend(Database::Connection *connection);

public:
    /**
     * Initialize the layout specification.
     *
     * @param connection    the archive connection
     */
    explicit Layout(Database::Connection *connection);

    /**
     * Create a layout with an explicit split configuration.
     *
     * @param connection    the archive connection
     * @param splitStation  split tables on stations
     * @param splitArchive  split tables on archives
     * @param splitVariable split tables on variables
     * @param updateIdentifier update the system identifier on construction
     */
    Layout(Database::Connection *connection,
           bool splitStation,
           bool splitArchive,
           bool splitVariable,
           bool updateIdentifier = true);

    Layout() = delete;

    Layout(const Layout &) = delete;

    Layout &operator=(const Layout &) = delete;


    /**
     * Test if the underlying archive splits data into separate tables for each station.
     *
     * @return  true if each station is split into a different table
     */
    bool splitStation() const;

    /**
     * Test if the underlying archive splits data into separate tables for each archive name.
     *
     * @return  true if each archive name is split into a different table
     */
    bool splitArchive() const;

    /**
     * Test if the underlying archive splits data into separate tables for each variable.
     *
     * @return  true if each variable is split into a different table
     */
    bool splitVariable() const;


    /**
     * Get the destination block number for a value with the specified bounds.
     *
     * @param start     the value start
     * @param end       the value end
     * @return          the destination block number
     */
    Structure::Block targetBlock(double start, double end) const;

    /**
     * Get the destination block that an advance to the specified time permits.
     *
     * @param advance   the advance time
     * @return          the block number corresponding to the advance
     */
    Structure::Block targetBlock(double advance) const;

    /**
     * Get the block a given start time belongs to.
     *
     * @param start     the start time
     * @return          the block number
     */
    Structure::Block startBlock(double start) const;

    /**
     * Get the block a given end time belongs to.
     *
     * @param end       the end time
     * @return          the block number
     */
    Structure::Block endBlock(double end) const;

    /**
     * Get the start time of the specified block number.
     *
     * @param block     the block number
     * @return          the start time of the block
     */
    double blockStart(std::int_fast16_t block) const;

    /**
     * Get the end time of the specified block number.
     *
     * @param block     the block number
     * @return          the end time of the block
     */
    double blockEnd(std::int_fast16_t block) const;


    /**
     * Convert the given modified time into a value suitable for use on the index.
     *
     * @param modified  the modified time
     * @return          the index write value
     */
    Database::Types::Variant targetModified(double modified) const;

    /**
     * Convert the given modified time into a value suitable for querying the index.
     *
     * @param modified  the modified time
     * @return          the index read value
     */
    Database::Types::Variant modifiedQuery(double modified) const;

    /**
     * Convert the given index modified time into a conventional time.
     *
     * @param modified  the index modified value
     * @return          a modified time
     */
    double modifiedValue(const Database::Types::Variant &modified) const;

    /**
     * Apply rounding as needed to a modified value for reading from the index.
     *
     * @param input     the input modified after time
     * @return          the index threshold limit
     */
    double modifiedThreshold(double input) const;

    /**
     * Get the conversion from the original layout to a new one for
     * modified times.
     *
     * @param origin    the original layout
     * @return          the conversion for use in a database insert select
     */
    std::string modifiedInsertSelect(Layout &origin) const;


    /**
     * A container for the access to the backend database.
     */
    class CPD3DATACORE_EXPORT DatabaseAccess final {
        friend class Backend;

        friend class Backend_V1;

        friend class Backend_V2;

        Database::Statement baseStatement;
        Database::Types::Binds baseBinds;

        DatabaseAccess(Database::Statement &&statement, Database::Types::Binds &&binds);

    public:
        DatabaseAccess();

        DatabaseAccess(const DatabaseAccess &);

        DatabaseAccess &operator=(const DatabaseAccess &);

        DatabaseAccess(DatabaseAccess &&);

        DatabaseAccess &operator=(DatabaseAccess &&);


        /**
         * Get the database access statement.
         *
         * @return  the database access statement
         */
        const Database::Statement &statement() const;

        /**
         * Get the binds used in the statement.
         *
         * @return  the base binds used in the statement
         */
        Database::Types::Binds binds() const;
    };


    /**
     * Get the name of the table associated with a specific sequence identity.
     *
     * @param identity  the sequence identity
     * @return          the table name
     */
    std::string sequenceTable(const Identity &identity) const;

    /**
     * Get all possible sequences at the table level.
     *
     * @return          all possible sequence tables
     */
    std::unordered_set<Identity> sequencePossible() const;

    /**
     * Get all unique sequences given a base and uniqueness specification.
     *
     * @param identity  the sequence identity
     * @param station   include the station in the result
     * @param archive   include the archive in the result
     * @param variable  include the variable in the result
     * @return          all possible sequence tables
     */
    std::unordered_set<Identity> sequenceUnique(const Identity &identity,
                                                bool station = true,
                                                bool archive = true,
                                                bool variable = true) const;

    /**
     * Get the data read query for sequences.
     * <br>
     * The query returns: data, tblock
     *
     * @param identity  the identity to read
     * @param where     the where clause
     * @return          the sequence read query
     */
    DatabaseAccess sequenceRead(const Identity &identity,
                                const std::string &where = std::string()) const;

    /**
     * The write mode used for creating an access query.
     */
    enum class WriteMode {
        /**
         * Any or unknown.  This mode allows for update or insert without knowing
         * if the target already exists or not.
         */
                Any,

        /**
         * Known to be a new insert.  This mode means that the target identity is
         * known not to exist in the archive already.
         */
                KnownNew,

        /**
         * Known to exist.  This mode means that the target identity is known to
         * exist in the archive and the write is just an update of the modified
         * and/or data attached to it.
         */
                KnownExisting,
    };

    /**
     * Get the data write query for sequences.
     *
     * @param identity  the identity to write
     * @return          the sequence write query
     */
    DatabaseAccess sequenceWrite(const Identity &identity, WriteMode mode = WriteMode::Any) const;

    /**
     * Get the data delete query for sequences.
     *
     * @param identity  the identity to write
     * @param where     the where clause
     * @return          the sequence delete query
     */
    DatabaseAccess sequenceDelete(const Identity &identity,
                                  const std::string &where = std::string()) const;

    /**
     * Get a query that returns the size of the data block for sequences, sorted
     * in size ascending order (smallest sizes first).
     * <br>
     * The query returns: SIZE(data), block
     *
     * @param identity  the identity to query
     * @param where     the where clause
     * @return          the size query
     */
    DatabaseAccess sequenceSize(const Identity &identity,
                                const std::string &where = std::string()) const;

    /**
     * Get the minimum defined block number for sequences.
     * <br>
     * The query returns a single entry with MINIMUM(block).
     *
     * @param identity  the identity to query
     * @return          the sequence minimum query
     */
    DatabaseAccess sequenceMinimum(const Identity &identity) const;

    /**
     * Get a sequence index read query.  Station, archive, and variable components
     * of the where selection are not automatically inserted if they are not split
     * and the given identity specifies them as zero.
     * <br>
     * The query returns: block, modified, [station if not split], [archive if not split]. [variable if not split]
     *
     * @param identity  the identity to query
     * @param where     the where clause
     * @return          the index read query
     */
    DatabaseAccess sequenceIndex(const Identity &identity,
                                 const std::string &where = std::string()) const;


    /**
     * Get the name of the table associated with a specific erasure identity.
     *
     * @param identity  the erasure identity
     * @return          the table name
     */
    std::string erasureTable(const Identity &identity) const;

    /**
     * Get all possible erasures at the table level.
     *
     * @return          all possible erasure tables
     */
    std::unordered_set<Identity> erasurePossible() const;

    /**
     * Get all unique erasures given a base and uniqueness specification.
     *
     * @param identity  the erasure identity
     * @param station   include the station in the result
     * @param archive   include the archive in the result
     * @param variable  include the variable in the result
     * @return          all possible erasure tables
     */
    std::unordered_set<Identity> erasureUnique(const Identity &identity,
                                               bool station = true,
                                               bool archive = true,
                                               bool variable = true) const;

    /**
     * Get the data read query for erasures.
     * <br>
     * The query returns: data, tblock
     *
     * @param identity  the identity to read
     * @param where     the where clause
     * @return          the erasure read query
     */
    DatabaseAccess erasureRead(const Identity &identity,
                               const std::string &where = std::string()) const;

    /**
     * Get the data write query for erasures.
     *
     * @param identity  the identity to write
     * @return          the erasure write query
     */
    DatabaseAccess erasureWrite(const Identity &identity, WriteMode mode = WriteMode::Any) const;

    /**
     * Get the data delete query for erasures.
     *
     * @param identity  the identity to write
     * @param where     the where clause
     * @return          the erasure delete query
     */
    DatabaseAccess erasureDelete(const Identity &identity,
                                 const std::string &where = std::string()) const;

    /**
     * Get a query that returns the size of the data block for erasures, sorted
     * in size ascending order (smallest sizes first).
     * <br>
     * The query returns: SIZE(data), block
     *
     * @param identity  the identity to query
     * @param where     the where clause
     * @return          the size query
     */
    DatabaseAccess erasureSize(const Identity &identity,
                               const std::string &where = std::string()) const;

    /**
     * Get the minimum defined block number for erasures.
     * <br>
     * The query returns a single entry with MINIMUM(block).
     *
     * @param identity  the identity to query
     * @return          the erasure minimum query
     */
    DatabaseAccess erasureMinimum(const Identity &identity) const;

    /**
     * Get a erasure index read query.  Station, archive, and variable components
     * of the where selection are not automatically inserted if they are not split
     * and the given identity specifies them as zero.
     * <br>
     * The query returns: block, modified, [station if not split], [archive if not split]. [variable if not split]
     *
     * @param identity  the identity to query
     * @param where     the where clause
     * @return          the index read query
     */
    DatabaseAccess erasureIndex(const Identity &identity,
                                const std::string &where = std::string()) const;

private:
    class Backend {
    protected:
        Database::Connection *connection;
    public:
        Backend(Database::Connection *connection);

        virtual ~Backend();

        virtual bool splitStation() const = 0;

        virtual bool splitArchive() const = 0;

        virtual bool splitVariable() const = 0;

        virtual Database::Types::Variant targetModified(double modified) const = 0;

        virtual Database::Types::Variant modifiedQuery(double modified) const = 0;

        virtual double modifiedValue(const Database::Types::Variant &modified) const = 0;

        virtual double modifiedThreshold(double input) const = 0;

        virtual std::string sequenceTable(const Identity &identity) const = 0;

        virtual std::unordered_set<Identity> sequencePossible() const = 0;

        virtual std::unordered_set<Identity> sequenceUnique(const Identity &identity,
                                                            bool station,
                                                            bool archive,
                                                            bool variable) const = 0;

        virtual DatabaseAccess sequenceRead(const Identity &identity,
                                            const std::string &where) const = 0;

        virtual DatabaseAccess sequenceWrite(const Identity &identity, WriteMode mode) const = 0;

        virtual DatabaseAccess sequenceDelete(const Identity &identity,
                                              const std::string &where) const = 0;

        virtual DatabaseAccess sequenceSize(const Identity &identity,
                                            const std::string &where) const = 0;

        virtual DatabaseAccess sequenceMinimum(const Identity &identity) const = 0;

        virtual DatabaseAccess sequenceIndex(const Identity &identity,
                                             const std::string &where) const = 0;


        virtual std::string erasureTable(const Identity &identity) const = 0;

        virtual std::unordered_set<Identity> erasurePossible() const = 0;

        virtual std::unordered_set<Identity> erasureUnique(const Identity &identity,
                                                           bool station,
                                                           bool archive,
                                                           bool variable) const = 0;

        virtual DatabaseAccess erasureRead(const Identity &identity,
                                           const std::string &where) const = 0;

        virtual DatabaseAccess erasureWrite(const Identity &identity, WriteMode mode) const = 0;

        virtual DatabaseAccess erasureDelete(const Identity &identity,
                                             const std::string &where) const = 0;

        virtual DatabaseAccess erasureSize(const Identity &identity,
                                           const std::string &where) const = 0;

        virtual DatabaseAccess erasureMinimum(const Identity &identity) const = 0;

        virtual DatabaseAccess erasureIndex(const Identity &identity,
                                            const std::string &where) const = 0;
    };
};

/**
 * The length of positive blocks, in seconds.
 */
constexpr double Block_Interval = 604800.0;

/**
 * The column name for the station index number.
 */
constexpr const char *Column_Station = "station";
/**
 * The column name for the archive index number.
 */
constexpr const char *Column_Archive = "archive";
/**
 * The column name of the variable index number.
 */
constexpr const char *Column_Variable = "variable";
/**
 * the column name for the latest modified time.
 */
constexpr const char *Column_Modified = "lastModified";
/**
 * The column name for the data blob.
 */
constexpr const char *Column_Data = "data";
/**
 * The column name for the block number.
 */
constexpr const char *Column_Block = "tblock";

/**
 * The size to split global columns at
 */
constexpr std::size_t Global_SplitSize = 16777216;

namespace Existing {

/**
 * The prefix used for sequence tables.
 */
constexpr const char *Table_Prefix = "CPD3Sequence";

enum {
    /**
     * Indicates that there is a remote reference to the value and its erasure should
     * be tracked.
     */
            Flag_RemoteReferenced = 0x01
};


/*
 * Effectively:
 * double start;
 * double end;
 * uint16_t flavors;
 * double modified;
 * int32_t priority;
 * uint8_t flags;
 * uint8_t value[];
 */

constexpr std::size_t Offset_Start = 0;
constexpr std::size_t Offset_End = Offset_Start + 8;
constexpr std::size_t Offset_Flavors = Offset_End + 8;
constexpr std::size_t Offset_Modified = Offset_Flavors + 2;
constexpr std::size_t Offset_Priority = Offset_Modified + 8;
constexpr std::size_t Offset_Flags = Offset_Priority + 4;
constexpr std::size_t Offset_Value = Offset_Flags + 1;
constexpr std::size_t Minimum_Size = Offset_Value + 1;

}

namespace Erasure {

/**
 * The prefix used for erasure tables.
 */
constexpr const char *Table_Prefix = "CPD3Erasure";

/*
 * Effectively:
 * double start;
 * double end;
 * uint16_t flavors;
 * double modified;
 * int32_t priority;
 */

constexpr std::size_t Offset_Start = 0;
constexpr std::size_t Offset_End = Offset_Start + 8;
constexpr std::size_t Offset_Flavors = Offset_End + 8;
constexpr std::size_t Offset_Modified = Offset_Flavors + 2;
constexpr std::size_t Offset_Priority = Offset_Modified + 8;
constexpr std::size_t Total_Size = Offset_Priority + 4;

}

}

}
}
}

namespace std {

template<>
struct hash<CPD3::Data::Archive::Structure::Identity> {
    size_t operator()(const CPD3::Data::Archive::Structure::Identity &identity) const
    {
        std::size_t
                h = std::hash<CPD3::Data::Archive::Structure::Identity::index>()(identity.station);
        h = CPD3::INTEGER::mix(h, std::hash<CPD3::Data::Archive::Structure::Identity::index>()(
                identity.archive));
        h = CPD3::INTEGER::mix(h, std::hash<CPD3::Data::Archive::Structure::Identity::index>()(
                identity.variable));
        return h;
    }
};

}

#endif //CPD3DATACOREARCHIVE_STRUCTURE_HXX
