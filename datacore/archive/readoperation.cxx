/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <cmath>
#include <functional>
#include <QLoggingCategory>

#include "readoperation.hxx"
#include "readblock.hxx"
#include "core/threadpool.hxx"
#include "core/waitutils.hxx"
#include "io/access.hxx"


Q_LOGGING_CATEGORY(log_datacore_archive_readoperation, "cpd3.datacore.archive.readoperation",
                   QtWarningMsg)

namespace CPD3 {
namespace Data {
namespace Archive {

/*
 * Control flow for the read looks like:
 *
 * Stream::indexResult() - Index read from archive thread
 *  Fetch parameters and first stage filtering
 *  Create Block and start on thread pool
 * Block::nameLookup() - Worker thread (non-archive)
 *  (Potentially) blocking lookup, calling back to archive thread
 *  Final index filtering
 *  Either:
 *   Start data read to archive (non-short circuit)
 *   Finish and handle name processing (name short circuit)
 * Block::readResult() - Data read from archive thread
 *  Finish data read query
 *  Start handling on thread pool
 * Block::readUnpack() - Block unpack on worker thread
 *  Handle and complete data
 *
 *
 * Flow control works by managing the indices from a single master thread.
 * This thread sets how far each index will automatically advance based
 * on one block past the earliest index.  So all indices will read until
 * they have passed the one furthest behind (which may have been that
 * index itself).  After that, they await the main thread to change
 * how far they can read and issue an advance.  The main thread itself
 * depends on the output blocking so that it will not continually
 * advance the index if they are too far behind.  Additionally the
 * main thread controls how far the outputs are advanced (for merging)
 * by allowing each stream to advance to the start of the earliest
 * block in flight or to how far it has read in the index if none are.
 * Pooled threads are not used because we could exhaust the pool
 * waiting and block a database transaction that we're in turn
 * waiting for (e.x. statement creation).
 *
 */

void ReadOperation::Stream::start()
{
    indexContext = createIndex();

    indexContext.result.connect(std::bind(&Stream::indexResult, this, std::placeholders::_1));
    indexContext.complete.connect(std::bind(&Stream::indexComplete, this));

    {
        std::lock_guard<std::mutex> lock(operation.mutex);
        Q_ASSERT(state == State_Initialize);
        state = State_ReadingIndex;
    }
    indexContext.begin();
}

void ReadOperation::Stream::indexResult(const Database::Types::Results &results)
{

    auto rptr = results.cbegin();

    Q_ASSERT(rptr != results.cend());
    Structure::Block block = static_cast<Structure::Block>(rptr->toInteger());
    if (!input->stream.block(block)) {
        {
            std::lock_guard<std::mutex> lock(operation.mutex);
            Q_ASSERT(latestReadBlock < 0 || block >= latestReadBlock);
            latestReadBlock = block;
        }
        indexContext.next();
        return;
    }
    ++rptr;

    Q_ASSERT(rptr != results.cend());
    double modified = operation.layout.modifiedValue(*rptr);
    ++rptr;

    Structure::Identity identity = input->identity;
    if (!operation.layout.splitStation()) {
        Q_ASSERT(rptr != results.cend());
        Q_ASSERT(identity.station == 0 ||
                         identity.station ==
                                 static_cast<Structure::Identity::index>(rptr->toInteger()));
        identity.station = static_cast<Structure::Identity::index>(rptr->toInteger());
        ++rptr;
    }
    if (!operation.layout.splitArchive()) {
        Q_ASSERT(rptr != results.cend());
        Q_ASSERT(identity.archive == 0 ||
                         identity.archive ==
                                 static_cast<Structure::Identity::index>(rptr->toInteger()));
        identity.archive = static_cast<Structure::Identity::index>(rptr->toInteger());
        ++rptr;
    }
    if (!operation.layout.splitVariable()) {
        Q_ASSERT(rptr != results.cend());
        Q_ASSERT(identity.variable == 0 ||
                         identity.variable ==
                                 static_cast<Structure::Identity::index>(rptr->toInteger()));
        identity.variable = static_cast<Structure::Identity::index>(rptr->toInteger());
        ++rptr;
    }

    Q_ASSERT(identity.station != 0);
    Q_ASSERT(identity.archive != 0);
    Q_ASSERT(identity.variable != 0);

    if (!input->stream.identity(identity)) {
        {
            std::lock_guard<std::mutex> lock(operation.mutex);
            Q_ASSERT(latestReadBlock < 0 || block >= latestReadBlock);
            latestReadBlock = block;
        }
        indexContext.next();
        return;
    }

    auto add = createBlock(block, modified, identity);
    auto toStart = add.get();
    bool doAdvance;
    {
        std::lock_guard<std::mutex> lock(operation.mutex);
        Q_ASSERT(state == State_ReadingIndex);
        Q_ASSERT(latestReadBlock < 0 || block >= latestReadBlock);
        latestReadBlock = block;
        blocksInFlight.emplace(block, std::move(add));
        doAdvance = shouldAdvanceIndex();
        if (!doAdvance) {
            state = State_IndexPaused;
        }
    }
    /* Safe without locking, since we can never complete, etc outside the
     * database thread, which we're in right now */
    toStart->start();

    if (doAdvance)
        indexContext.next();
}

void ReadOperation::Stream::indexComplete()
{
    std::lock_guard<std::mutex> lock(operation.mutex);
    Q_ASSERT(state != State_Complete);
    state = State_Complete;
    /* Must retain the lock, since we can be deleted otherwise */
    operation.notify.notify_all();
}

void ReadOperation::Stream::completeBlock(ReadOperation::Block *block, bool wake)
{
    if (blocksInFlight.size() == 1) {
        /* Only a single block, so it must be this one */
        Q_ASSERT(blocksInFlight.begin()->second.get() == block);
        blocksInFlight.clear();
        wake = true;
    } else {
        Q_ASSERT(blocksInFlight.size() >= 2);
        auto first = blocksInFlight.begin();
        if (first->second.get() == block) {
            /* If the first block is the one we're removing and the next one
             * changes the outstanding block, wake the processing thread
             * so it can advance */

            auto next = first;
            ++next;
            Q_ASSERT(next != blocksInFlight.end());
            if (next->first != first->first)
                wake = true;

            blocksInFlight.erase(first);
        } else {
            /* Otherwise, just find and remove the block */

            auto check = blocksInFlight.lower_bound(block->block);
            for (;;) {
                Q_ASSERT(check != blocksInFlight.end());
                if (check->second.get() == block) {
                    blocksInFlight.erase(check);
                    break;
                }
                ++check;
            }
        }
    }

    /* Must retain the lock, since we can be deleted otherwise */
    if (wake)
        operation.notify.notify_all();
}

bool ReadOperation::Stream::shouldAdvanceIndex()
{
    /* Always read until we get a block beyond the threshold */
    if (latestReadBlock <= readUntilBlock)
        return true;
    return false;
}

Structure::Block ReadOperation::Stream::beginProcess()
{
    /* Lock already held */

    Structure::Block block;
    if (!blocksInFlight.empty()) {
        block = blocksInFlight.begin()->first;
        Q_ASSERT(block <= latestReadBlock);
    } else {
        block = latestReadBlock;
    }

    if (state != State_Complete && block >= 0) {
        double time = operation.layout.blockStart(block);
        input->advance(time);
        advanceStream(time);
    }

    return block;
}

bool ReadOperation::Stream::endProcess(std::unique_lock<std::mutex> &lock,
                                       Structure::Block threshold,
                                       bool &complete)
{
    if (threshold >= 0) {
        Q_ASSERT(threshold >= readUntilBlock);
        readUntilBlock = threshold;
    }

    switch (state) {
    case State_Initialize:
        Q_ASSERT(false);
        return false;
    case State_ReadingIndex:
        return false;
    case State_IndexPaused:
        if (!shouldAdvanceIndex())
            return false;
        state = State_ReadingIndex;
        break;
    case State_Complete:
        if (!blocksInFlight.empty())
            return false;
        completeStream();
        complete = true;
        return false;
    }

    Q_ASSERT(state == State_ReadingIndex);

    lock.unlock();

    indexContext.next();

    lock.lock();
    return true;
}

void ReadOperation::Stream::signalTerminate()
{
    indexContext.finish();
}

ReadOperation::Stream::Stream(ReadOperation &operation,
                              std::unique_ptr<SequenceFilter::Input> &&input) : operation(
        operation),
                                                                                input(std::move(
                                                                                        input)),
                                                                                indexContext(),
                                                                                state(State_Initialize),
                                                                                latestReadBlock(-1),
                                                                                readUntilBlock(0),
                                                                                blocksInFlight()
{ }

ReadOperation::Stream::~Stream() = default;


void ReadOperation::Block::start()
{
    Q_ASSERT(state == State_Initialize);
    state = State_NameLookup;

    stream.operation.lookupRun.run(std::bind(&Block::nameLookup, this));
}

void ReadOperation::Block::nameLookup()
{
    Q_ASSERT(state == State_NameLookup);

    IdentityIndex &index = stream.operation.index;
    name = SequenceName(index.station.lookup(identity.station),
                        index.archive.lookup(identity.archive),
                        index.variable.lookup(identity.variable));

    if (name.getStation().empty()) {
        qCDebug(log_datacore_archive_readoperation) << "No station found for index"
                                                    << identity.station;
        return failBlock();
    }
    if (name.getArchive().empty()) {
        qCDebug(log_datacore_archive_readoperation) << "No archive found for index"
                                                    << identity.archive;
        return failBlock();
    }
    if (name.getVariable().empty()) {
        qCDebug(log_datacore_archive_readoperation) << "No variable found for index"
                                                    << identity.variable;
        return failBlock();
    }

    if (!stream.input->stream.final(block, modified, identity, name)) {
        return failBlock();
    }

    {
        std::unique_lock<std::mutex> lock(stream.operation.mutex, std::defer_lock);
        if (nameProcess(block, name, lock)) {
            Q_ASSERT(lock.owns_lock());
            state = State_Complete;
            return stream.completeBlock(this);
        }
    }

    state = State_ReadData;

    readContext = createRead(identity, block);

    readContext.result.connect(std::bind(&Block::readResult, this, std::placeholders::_1));
    readContext.complete.connect(std::bind(&Block::readComplete, this));
    return readContext.begin();
}

bool ReadOperation::Block::nameProcess(Structure::Block,
                                       const SequenceName &,
                                       std::unique_lock<std::mutex> &)
{ return false; }

void ReadOperation::Block::readResult(const Database::Types::Results &results)
{
    Q_ASSERT(state == State_ReadData);
    state = State_Unpack;
    readContext.finish();
    readContext = Database::Context();

    Q_ASSERT(!results.empty());
    stream.operation.unpackRun.run(std::bind(&Block::readUnpack, this, results.front().toBytes()));
}

void ReadOperation::Block::readComplete()
{
    if (state != State_ReadData)
        return;
    readContext = Database::Context();
    return failBlock();
}

void ReadOperation::Block::readUnpack(const Util::ByteArray &data)
{
    Q_ASSERT(state == State_Unpack);

    std::unique_lock<std::mutex> lock(stream.operation.mutex, std::defer_lock);
    bool wake = unpack(data, name, stream.operation.index.flavor, stream.input->block, lock);

    if (!lock.owns_lock())
        lock.lock();
    state = State_Complete;
    return stream.completeBlock(this, wake);
}

void ReadOperation::Block::failBlock()
{
    std::lock_guard<std::mutex> lock(stream.operation.mutex);
    state = State_Failed;
    return stream.completeBlock(this);
}

void ReadOperation::Block::operationAbort(std::unique_lock<std::mutex> &lock)
{
    if (!lock.owns_lock())
        lock.lock();
    state = State_Failed;
    if (stream.operation.state != ReadOperation::State_Running)
        return;
    stream.operation.state = ReadOperation::State_TerminateRequested;
    stream.operation.notify.notify_all();
}

ReadOperation::Block::Block(Stream &stream,
                            Structure::Block block,
                            double modified,
                            const Structure::Identity &identity) : stream(stream),
                                                                   block(block),
                                                                   modified(modified),
                                                                   identity(identity),
                                                                   name(),
                                                                   readContext(),
                                                                   state(State_Initialize)
{ }

ReadOperation::Block::~Block() = default;


void ReadOperation::startStream(std::unique_ptr<Stream> &&stream)
{
    Q_ASSERT(state == State_Initialize);
    auto start = stream.get();
    streams.emplace_back(std::move(stream));
    start->start();
}

void ReadOperation::run()
{
    auto completed = complete.defer();

    std::unique_lock<std::mutex> lock(mutex);
    Q_ASSERT(state == State_Initialize || state == State_TerminateRequested);

    if (streams.empty()) {
        while (finishOutput(lock)) {
            Q_ASSERT(lock.owns_lock());
            if (state == State_TerminateRequested) {
                state = State_Terminating;
                break;
            }
            notify.wait(lock);
        }

        Q_ASSERT(lock.owns_lock());
        state = State_Complete;
        notify.notify_all();
        return;
    }

    if (state != State_TerminateRequested)
        state = State_Running;
    bool terminated = false;
    for (;;) {
        Q_ASSERT(lock.owns_lock());

        if (state == State_Finishing)
            break;

        if (state == State_TerminateRequested) {
            state = State_Terminating;
            terminated = true;

            lock.unlock();
            terminateStreams();
            lock.lock();
            continue;
        }

        if (!terminated) {
            if (processOutput(lock))
                continue;
        }
        if (processStreams(lock))
            continue;

        notify.wait(lock);
    }
    Q_ASSERT(streams.empty());
    Q_ASSERT(lock.owns_lock());

    while (finishOutput(lock)) {
        Q_ASSERT(lock.owns_lock());
        if (state == State_TerminateRequested) {
            state = State_Terminating;
            terminated = true;
            break;
        }
        if (terminated)
            break;
        notify.wait(lock);
    }

    Q_ASSERT(lock.owns_lock());
    state = State_Complete;
    notify.notify_all();
}

void ReadOperation::waitForNotify(std::unique_lock<std::mutex> &lock)
{
    notify.wait(lock);
}

bool ReadOperation::runWithLock(const std::function<void()> &operation)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state == State_Complete)
            return false;
        operation();
    }
    notify.notify_all();
    return true;
}


bool ReadOperation::processStreams(std::unique_lock<std::mutex> &lock)
{
    if (streams.empty()) {
        state = State_Finishing;
        return true;
    }

    Structure::Block block = std::numeric_limits<Structure::Block>::max();
    for (const auto &stream : streams) {
        Structure::Block streamPosition = stream->beginProcess();
        if (streamPosition < 0) {
            block = -1;
            continue;
        }
        if (streamPosition < block)
            block = streamPosition;
    }
    Q_ASSERT(block != std::numeric_limits<Structure::Block>::max());

    bool unlocked = false;

    if (block >= 0)
        block = block + 1;

    for (auto stream = streams.begin(); stream != streams.end();) {
        bool complete = false;
        if ((*stream)->endProcess(lock, block, complete))
            unlocked = true;

        Q_ASSERT(lock.owns_lock());

        if (complete) {
            stream = streams.erase(stream);
            unlocked = true;
        } else {
            ++stream;
        }
    }

    return unlocked;
}

void ReadOperation::terminateStreams()
{
    for (const auto &stream : streams) {
        stream->signalTerminate();
    }
}

void ReadOperation::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state != State_Running && state != State_Initialize)
            return;
        state = State_TerminateRequested;
    }
    notify.notify_all();
}

bool ReadOperation::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, mutex, notify,
                                     [this] { return state == State_Complete; });
}

void ReadOperation::join(bool terminate)
{
    if (thread.joinable()) {
        if (terminate)
            signalTerminate();
        thread.join();
    }
}

bool ReadOperation::isComplete()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == State_Complete;
}

ReadOperation::ReadOperation(Structure::Layout &layout, IdentityIndex &index) : layout(layout),
                                                                                index(index),
                                                                                mutex(),
                                                                                notify(),
                                                                                thread(),
                                                                                state(State_Initialize)
{ }

ReadOperation::~ReadOperation()
{ join(); }

void ReadOperation::start()
{
    Q_ASSERT(state == State_Initialize);
    Q_ASSERT(!thread.joinable());
    thread = std::thread(std::bind(&ReadOperation::run, this));
}


namespace {
template<typename MultiplexerTarget>
class ForgeLoopbackStream {
    ReadOperation &operation;
    MultiplexerTarget *target;
    std::shared_ptr<CPD3::IO::Process::Instance> loopbackReader;
    Selection::List selections;
    bool readerComplete;
    bool threadComplete;

    class StreamThread final : public QThread {
        ForgeLoopbackStream &stream;
        std::unique_ptr<CPD3::IO::Process::Stream> selectionWriteStream;
        std::unique_ptr<CPD3::IO::Process::Stream> serializedDataStream;

        void writeSelections()
        {
            auto &backingDevice = *selectionWriteStream;
            auto qioDevice = CPD3::IO::Generic::Backing::qioStream(std::move(selectionWriteStream));
            qioDevice->open(QIODevice::WriteOnly);

            {
                QDataStream qioStream(qioDevice.get());
                qioStream.setByteOrder(QDataStream::LittleEndian);
                qioStream.setVersion(QDataStream::Qt_4_5);

                for (const auto &sel: stream.selections) {
                    qioStream << sel;

                    if (qioStream.status() != QDataStream::Ok) {
                        break;
                    }
                }
            }

            backingDevice.close();
            qioDevice->close();
        }

    public:
        StreamThread(ForgeLoopbackStream &stream) : stream(stream),
                                                    selectionWriteStream(
                                                            stream.loopbackReader->inputStream()),
                                                    serializedDataStream(
                                                            stream.loopbackReader->outputStream())
        { }

    protected:
        void run() override
        {
            auto qioDevice = CPD3::IO::Generic::Backing::qioStream(std::move(serializedDataStream));
            qioDevice->open(QIODevice::ReadOnly);

            writeSelections();

            {
                QDataStream qioStream(qioDevice.get());
                qioStream.setByteOrder(QDataStream::LittleEndian);
                qioStream.setVersion(QDataStream::Qt_4_5);

                for (;;) {
                    ArchiveValue v;
                    qioStream >> v;
                    if (qioStream.status() != QDataStream::Ok) {
                        break;
                    }

                    if (!this->stream.operation.runWithLock([this, &v] {
                        this->stream.target->incomingValue(std::move(v));
                    })) {
                        break;
                    }
                }
            }

            qioDevice->close();
            qioDevice.reset();
            this->stream.operation.runWithLock([this] {
                this->stream.target->end();
                this->stream.threadComplete = true;
            });
        }
    };

    friend class StreamThread;

    std::unique_ptr<StreamThread> streamThread;
public:
    ForgeLoopbackStream(SequenceFilter::ForgeLoopbackInput &&input,
                        ReadOperation &operation,
                        MultiplexerTarget *target) : operation(operation),
                                                     target(target),
                                                     loopbackReader(),
                                                     selections(std::move(input.selections)),
                                                     readerComplete(false),
                                                     threadComplete(false),
                                                     streamThread()
    {
        qCDebug(log_datacore_archive_readoperation) << "Dispatching" << selections.size()
                                                    << "selection(s) to Forge archive loopback";

        IO::Process::Spawn spawn(input.program);
        loopbackReader = spawn.create();

        loopbackReader->exited.connect([this](int, bool) {
            this->operation.runWithLock([this] {
                this->readerComplete = true;
            });
        });

        streamThread.reset(new StreamThread(*this));

        streamThread->start();
        loopbackReader->start();
    }

    ~ForgeLoopbackStream() = default;

    void signalTerminate()
    {
        loopbackReader->terminate();
        streamThread->quit();
    }

    bool finishOutput()
    {
        if (!readerComplete || !threadComplete) {
            return true;
        }
        streamThread->wait();
        return false;
    }
};

}


class ReadSequence::ForgeSequenceLoopBackStream : public ForgeLoopbackStream<
        ReadSequence::Multiplexer::Simple> {
public:
    ForgeSequenceLoopBackStream(SequenceFilter::ForgeLoopbackInput &&input, ReadSequence &operation)
            : ForgeLoopbackStream(std::move(input), operation, operation.multiplexer.createSimple())
    { }

    ~ForgeSequenceLoopBackStream() = default;
};

bool ReadSequence::processOutput(std::unique_lock<std::mutex> &lock)
{
    std::unique_lock<std::mutex> sinkLock(sinkMutex);

    if (!sink) {
        sinkLock.unlock();
        waitForNotify(lock);
        return true;
    }

    SequenceValue::Transfer out;
    multiplexer.output(out);
    if (out.empty())
        return false;
    lock.unlock();

    Q_ASSERT(sink);
    sink->incomingData(std::move(out));
    sinkLock.unlock();

    lock.lock();
    return true;
}

bool ReadSequence::finishOutput(std::unique_lock<std::mutex> &lock)
{
    if (forgeLoopbackStream) {
        if (forgeLoopbackStream->finishOutput()) {
            return true;
        }
        forgeLoopbackStream.reset();
    }

    std::unique_lock<std::mutex> sinkLock(sinkMutex);

    if (!sink) {
        if (multiplexer.isComplete())
            return false;
        sinkLock.unlock();
        return true;
    }

    SequenceValue::Transfer out;
    multiplexer.output(out);

    lock.unlock();

    Q_ASSERT(sink);
    if (!out.empty()) {
        sink->incomingData(std::move(out));
    }
    sink->endData();
    sinkLock.unlock();

    lock.lock();
    Q_ASSERT(multiplexer.isComplete());
    return false;
}

void ReadSequence::setEgress(StreamSink *sink)
{
    if (!runWithLock([this, sink] {
        std::lock_guard<std::mutex> sinkLock(sinkMutex);
        this->sink = sink;
    })) {
        {
            std::lock_guard<std::mutex> sinkLock(sinkMutex);
            if (this->sink == sink)
                return;
            this->sink = sink;
        }
        if (sink)
            sink->endData();
    }
}

void ReadSequence::terminateStreams()
{
    ReadOperation::terminateStreams();
    if (forgeLoopbackStream) {
        forgeLoopbackStream->signalTerminate();
    }
}

ReadSequence::ReadSequence(Structure::Layout &layout,
                           IdentityIndex &index,
                           const Selection::List &selections) : ReadOperation(layout, index),
                                                                multiplexer(),
                                                                sinkMutex(),
                                                                sink(nullptr),
                                                                forgeLoopbackStream()
{
    std::unique_ptr<SequenceFilter::ForgeLoopbackInput> forgeLoopbackInput;
    auto inputs = SequenceFilter::Input::sequence(layout, index, selections, &forgeLoopbackInput);
    for (auto &input: inputs) {
        startStream(std::unique_ptr<Stream>(new SequenceStream(*this, std::move(input))));
    }
    if (forgeLoopbackInput) {
        forgeLoopbackStream.reset(
                new ForgeSequenceLoopBackStream(std::move(*forgeLoopbackInput), *this));
    }
    multiplexer.creationComplete();
}

ReadSequence::~ReadSequence()
{ join(); }


ReadSequence::SequenceBlock::SequenceBlock(SequenceStream &stream,
                                           Structure::Block block,
                                           double modified,
                                           const Structure::Identity &identity) : Block(stream,
                                                                                        block,
                                                                                        modified,
                                                                                        identity),
                                                                                  stream(stream)
{ }

ReadSequence::SequenceBlock::~SequenceBlock() = default;

Database::Context ReadSequence::SequenceBlock::createRead(const Structure::Identity &identity,
                                                          Structure::Block block)
{
    SequenceFilter::Input &input = stream.input();

    auto binds = stream.readBinds;
    if (input.identity.station == 0)
        binds.emplace(Structure::Column_Station, static_cast<std::int_fast64_t>(identity.station));
    if (input.identity.archive == 0)
        binds.emplace(Structure::Column_Archive, static_cast<std::int_fast64_t>(identity.archive));
    if (input.identity.variable == 0)
        binds.emplace(Structure::Column_Variable,
                      static_cast<std::int_fast64_t>(identity.variable));

    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    return stream.readStatement.execute(binds, true);
}

bool ReadSequence::SequenceBlock::unpack(const Util::ByteArray &data,
                                         const SequenceName &baseName,
                                         StringIndex &flavors,
                                         SequenceFilter::Block &filter,
                                         std::unique_lock<std::mutex> &lock)
{
    auto output = ReadBlock::sequence(data, baseName, flavors, filter);
    if (output.empty())
        return false;
    Q_ASSERT(!lock.owns_lock());
    lock.lock();
    return stream.target->incoming(std::move(output));
}


ReadSequence::SequenceStream::SequenceStream(ReadSequence &operation,
                                             std::unique_ptr<SequenceFilter::Input> &&input)
        : Stream(operation, std::move(input)),
          operation(operation),
          target(operation.multiplexer.createUnsorted())
{
    std::string whereBlock = Structure::Column_Block;
    whereBlock += " = :";
    whereBlock += Structure::Column_Block;

    auto read = operation.layout().sequenceRead(this->input().identity, whereBlock);
    readStatement = read.statement();
    readBinds = read.binds();
}

ReadSequence::SequenceStream::~SequenceStream() = default;

Database::Context ReadSequence::SequenceStream::createIndex()
{
    auto index =
            operation.layout().sequenceIndex(input().identity, input().stream.buildIndexWhere());
    auto binds = index.binds();
    input().stream.mergeIndexBinds(binds);
    return index.statement().execute(binds);
}

std::unique_ptr<
        ReadOperation::Block> ReadSequence::SequenceStream::createBlock(Structure::Block block,
                                                                        double modified,
                                                                        const Structure::Identity &identity)
{ return std::unique_ptr<Block>(new SequenceBlock(*this, block, modified, identity)); }

void ReadSequence::SequenceStream::advanceStream(double time)
{ target->advance(time); }

void ReadSequence::SequenceStream::completeStream()
{
    target->end();
    target = nullptr;
}


class ReadArchive::ForgeArchiveLoopBackStream : public ForgeLoopbackStream<
        ReadArchive::Multiplexer::Simple> {
public:
    ForgeArchiveLoopBackStream(SequenceFilter::ForgeLoopbackInput &&input, ReadArchive &operation)
            : ForgeLoopbackStream(std::move(input), operation, operation.multiplexer.createSimple())
    { }

    ~ForgeArchiveLoopBackStream() = default;
};

bool ReadArchive::processOutput(std::unique_lock<std::mutex> &lock)
{
    std::unique_lock<std::mutex> sinkLock(sinkMutex);

    if (!sink) {
        sinkLock.unlock();
        waitForNotify(lock);
        return true;
    }

    ArchiveValue::Transfer out;
    multiplexer.output(out);
    if (out.empty())
        return false;
    lock.unlock();

    Q_ASSERT(sink);
    sink->incomingData(std::move(out));
    sinkLock.unlock();

    lock.lock();
    return true;
}

bool ReadArchive::finishOutput(std::unique_lock<std::mutex> &lock)
{
    if (forgeLoopbackStream) {
        if (forgeLoopbackStream->finishOutput()) {
            return true;
        }
        forgeLoopbackStream.reset();
    }

    std::unique_lock<std::mutex> sinkLock(sinkMutex);

    if (!sink) {
        if (multiplexer.isComplete())
            return false;
        sinkLock.unlock();
        return true;
    }

    ArchiveValue::Transfer out;
    multiplexer.output(out);

    lock.unlock();

    Q_ASSERT(sink);
    if (!out.empty()) {
        sink->incomingData(std::move(out));
    }
    sink->endData();
    sinkLock.unlock();

    lock.lock();
    Q_ASSERT(multiplexer.isComplete());
    return false;
}

void ReadArchive::setEgress(ArchiveSink *sink)
{
    if (!runWithLock([this, sink] {
        std::lock_guard<std::mutex> sinkLock(sinkMutex);
        this->sink = sink;
    })) {
        {
            std::lock_guard<std::mutex> sinkLock(sinkMutex);
            if (this->sink == sink)
                return;
            this->sink = sink;
        }
        if (sink)
            sink->endData();
    }
}

void ReadArchive::terminateStreams()
{
    ReadOperation::terminateStreams();
    if (forgeLoopbackStream) {
        forgeLoopbackStream->signalTerminate();
    }
}

ReadArchive::ReadArchive(Structure::Layout &layout,
                         IdentityIndex &index,
                         const Selection::List &selections) : ReadOperation(layout, index),
                                                              multiplexer(),
                                                              sinkMutex(),
                                                              sink(nullptr),
                                                              forgeLoopbackStream()
{
    std::unique_ptr<SequenceFilter::ForgeLoopbackInput> forgeLoopbackInput;
    auto inputs = SequenceFilter::Input::sequence(layout, index, selections, &forgeLoopbackInput);
    for (auto &input : inputs) {
        startStream(std::unique_ptr<Stream>(new ArchiveStream(*this, std::move(input))));
    }
    if (forgeLoopbackInput) {
        forgeLoopbackStream.reset(
                new ForgeArchiveLoopBackStream(std::move(*forgeLoopbackInput), *this));
    }
    multiplexer.creationComplete();
}

ReadArchive::~ReadArchive()
{ join(); }


ReadArchive::ArchiveBlock::ArchiveBlock(ArchiveStream &stream,
                                        Structure::Block block,
                                        double modified,
                                        const Structure::Identity &identity) : Block(stream, block,
                                                                                     modified,
                                                                                     identity),
                                                                               stream(stream)
{ }

ReadArchive::ArchiveBlock::~ArchiveBlock() = default;

Database::Context ReadArchive::ArchiveBlock::createRead(const Structure::Identity &identity,
                                                        Structure::Block block)
{
    SequenceFilter::Input &input = stream.input();

    auto binds = stream.readBinds;
    if (input.identity.station == 0)
        binds.emplace(Structure::Column_Station, static_cast<std::int_fast64_t>(identity.station));
    if (input.identity.archive == 0)
        binds.emplace(Structure::Column_Archive, static_cast<std::int_fast64_t>(identity.archive));
    if (input.identity.variable == 0)
        binds.emplace(Structure::Column_Variable,
                      static_cast<std::int_fast64_t>(identity.variable));

    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    return stream.readStatement.execute(binds, true);
}

bool ReadArchive::ArchiveBlock::unpack(const Util::ByteArray &data,
                                       const SequenceName &baseName,
                                       StringIndex &flavors,
                                       SequenceFilter::Block &filter,
                                       std::unique_lock<std::mutex> &lock)
{
    auto output = ReadBlock::archive(data, baseName, flavors, filter);
    if (output.empty())
        return false;
    Q_ASSERT(!lock.owns_lock());
    lock.lock();
    return stream.target->incoming(std::move(output));
}


ReadArchive::ArchiveStream::ArchiveStream(ReadArchive &operation,
                                          std::unique_ptr<SequenceFilter::Input> &&input) : Stream(
        operation, std::move(input)),
                                                                                            operation(
                                                                                                    operation),
                                                                                            target(operation
                                                                                                           .multiplexer
                                                                                                           .createUnsorted())
{
    std::string whereBlock = Structure::Column_Block;
    whereBlock += " = :";
    whereBlock += Structure::Column_Block;

    auto read = operation.layout().sequenceRead(this->input().identity, whereBlock);
    readStatement = read.statement();
    readBinds = read.binds();
}

ReadArchive::ArchiveStream::~ArchiveStream() = default;

Database::Context ReadArchive::ArchiveStream::createIndex()
{
    auto index =
            operation.layout().sequenceIndex(input().identity, input().stream.buildIndexWhere());
    auto binds = index.binds();
    input().stream.mergeIndexBinds(binds);
    return index.statement().execute(binds);
}

std::unique_ptr<
        ReadOperation::Block> ReadArchive::ArchiveStream::createBlock(Structure::Block block,
                                                                      double modified,
                                                                      const Structure::Identity &identity)
{ return std::unique_ptr<Block>(new ArchiveBlock(*this, block, modified, identity)); }

void ReadArchive::ArchiveStream::advanceStream(double time)
{ target->advance(time); }

void ReadArchive::ArchiveStream::completeStream()
{
    target->end();
    target = nullptr;
}

bool ReadErasure::processOutput(std::unique_lock<std::mutex> &lock)
{
    std::unique_lock<std::mutex> sinkLock(sinkMutex);

    if (!sink) {
        sinkLock.unlock();
        waitForNotify(lock);
        return true;
    }

    Transfer out;
    multiplexer.output(out);
    if (out.empty())
        return false;
    lock.unlock();

    Q_ASSERT(sink);
    dispatchValues(std::move(out));
    sinkLock.unlock();

    lock.lock();
    return true;
}

bool ReadErasure::finishOutput(std::unique_lock<std::mutex> &lock)
{
    std::unique_lock<std::mutex> sinkLock(sinkMutex);

    if (!sink) {
        if (multiplexer.isComplete())
            return false;
        sinkLock.unlock();
        return true;
    }

    Transfer out;
    multiplexer.output(out);

    lock.unlock();

    Q_ASSERT(sink);
    if (!out.empty()) {
        dispatchValues(std::move(out));
    }
    sink->endStream();
    sinkLock.unlock();

    lock.lock();
    Q_ASSERT(multiplexer.isComplete());
    return false;
}

void ReadErasure::dispatchValues(ReadErasure::Transfer &&values)
{
    for (auto &v : values) {
        if (v.value) {
            sink->incomingValue(std::move(*v.value));
        } else {
            sink->incomingErasure(std::move(*v.erasure));
        }
    }
}

void ReadErasure::setEgress(ErasureSink *sink)
{
    if (!runWithLock([this, sink] {
        std::lock_guard<std::mutex> sinkLock(sinkMutex);
        this->sink = sink;
    })) {
        {
            std::lock_guard<std::mutex> sinkLock(sinkMutex);
            if (this->sink == sink)
                return;
            this->sink = sink;
        }
        if (sink)
            sink->endStream();
    }
}

ReadErasure::ReadErasure(Structure::Layout &layout,
                         IdentityIndex &index,
                         const Selection::List &selections) : ReadOperation(layout, index),
                                                              multiplexer(),
                                                              sinkMutex(),
                                                              sink(nullptr)
{
    {
        auto inputs = SequenceFilter::Input::sequence(layout, index, selections);
        for (auto &input : inputs) {
            startStream(std::unique_ptr<Stream>(new ArchiveStream(*this, std::move(input))));
        }
    }
    {
        auto inputs = SequenceFilter::Input::erasure(layout, index, selections);
        for (auto &input : inputs) {
            startStream(std::unique_ptr<Stream>(new ErasureStream(*this, std::move(input))));
        }
    }
    multiplexer.creationComplete();
}

ReadErasure::~ReadErasure()
{ join(); }

ReadErasure::ArchiveBlock::ArchiveBlock(ArchiveStream &stream,
                                        Structure::Block block,
                                        double modified,
                                        const Structure::Identity &identity) : Block(stream, block,
                                                                                     modified,
                                                                                     identity),
                                                                               stream(stream)
{ }

ReadErasure::ArchiveBlock::~ArchiveBlock() = default;

Database::Context ReadErasure::ArchiveBlock::createRead(const Structure::Identity &identity,
                                                        Structure::Block block)
{
    SequenceFilter::Input &input = stream.input();

    auto binds = stream.readBinds;
    if (input.identity.station == 0)
        binds.emplace(Structure::Column_Station, static_cast<std::int_fast64_t>(identity.station));
    if (input.identity.archive == 0)
        binds.emplace(Structure::Column_Archive, static_cast<std::int_fast64_t>(identity.archive));
    if (input.identity.variable == 0)
        binds.emplace(Structure::Column_Variable,
                      static_cast<std::int_fast64_t>(identity.variable));

    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    return stream.readStatement.execute(binds, true);
}

bool ReadErasure::ArchiveBlock::unpack(const Util::ByteArray &data,
                                       const SequenceName &baseName,
                                       StringIndex &flavors,
                                       SequenceFilter::Block &filter,
                                       std::unique_lock<std::mutex> &lock)
{
    Transfer values;
    {
        auto output = ReadBlock::archive(data, baseName, flavors, filter);
        if (output.empty())
            return false;
        for (auto &value : output) {
            values.emplace_back(std::move(value));
        }
    }

    Q_ASSERT(!lock.owns_lock());
    lock.lock();
    return stream.target->incoming(std::move(values));
}


ReadErasure::ArchiveStream::ArchiveStream(ReadErasure &operation,
                                          std::unique_ptr<SequenceFilter::Input> &&input) : Stream(
        operation, std::move(input)),
                                                                                            operation(
                                                                                                    operation),
                                                                                            target(operation
                                                                                                           .multiplexer
                                                                                                           .createUnsorted())
{
    std::string whereBlock = Structure::Column_Block;
    whereBlock += " = :";
    whereBlock += Structure::Column_Block;

    auto read = operation.layout().sequenceRead(this->input().identity, whereBlock);
    readStatement = read.statement();
    readBinds = read.binds();
}

ReadErasure::ArchiveStream::~ArchiveStream() = default;

Database::Context ReadErasure::ArchiveStream::createIndex()
{
    auto index =
            operation.layout().sequenceIndex(input().identity, input().stream.buildIndexWhere());
    auto binds = index.binds();
    input().stream.mergeIndexBinds(binds);
    return index.statement().execute(binds);
}

std::unique_ptr<
        ReadOperation::Block> ReadErasure::ArchiveStream::createBlock(Structure::Block block,
                                                                      double modified,
                                                                      const Structure::Identity &identity)
{ return std::unique_ptr<Block>(new ArchiveBlock(*this, block, modified, identity)); }

void ReadErasure::ArchiveStream::advanceStream(double time)
{ target->advance(time); }

void ReadErasure::ArchiveStream::completeStream()
{
    target->end();
    target = nullptr;
}

ReadErasure::ErasureBlock::ErasureBlock(ErasureStream &stream,
                                        Structure::Block block,
                                        double modified,
                                        const Structure::Identity &identity) : Block(stream, block,
                                                                                     modified,
                                                                                     identity),
                                                                               stream(stream)
{ }

ReadErasure::ErasureBlock::~ErasureBlock() = default;

Database::Context ReadErasure::ErasureBlock::createRead(const Structure::Identity &identity,
                                                        Structure::Block block)
{
    SequenceFilter::Input &input = stream.input();

    auto binds = stream.readBinds;
    if (input.identity.station == 0)
        binds.emplace(Structure::Column_Station, static_cast<std::int_fast64_t>(identity.station));
    if (input.identity.archive == 0)
        binds.emplace(Structure::Column_Archive, static_cast<std::int_fast64_t>(identity.archive));
    if (input.identity.variable == 0)
        binds.emplace(Structure::Column_Variable,
                      static_cast<std::int_fast64_t>(identity.variable));

    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    return stream.readStatement.execute(binds, true);
}

bool ReadErasure::ErasureBlock::unpack(const Util::ByteArray &data,
                                       const SequenceName &baseName,
                                       StringIndex &flavors,
                                       SequenceFilter::Block &filter,
                                       std::unique_lock<std::mutex> &lock)
{
    Transfer values;
    {
        auto output = ReadBlock::erasure(data, baseName, flavors, filter);
        if (output.empty())
            return false;
        for (auto &value : output) {
            values.emplace_back(std::move(value));
        }
    }

    Q_ASSERT(!lock.owns_lock());
    lock.lock();
    return stream.target->incoming(std::move(values));
}


ReadErasure::ErasureStream::ErasureStream(ReadErasure &operation,
                                          std::unique_ptr<SequenceFilter::Input> &&input) : Stream(
        operation, std::move(input)),
                                                                                            operation(
                                                                                                    operation),
                                                                                            target(operation
                                                                                                           .multiplexer
                                                                                                           .createUnsorted())
{
    std::string whereBlock = Structure::Column_Block;
    whereBlock += " = :";
    whereBlock += Structure::Column_Block;

    auto read = operation.layout().erasureRead(this->input().identity, whereBlock);
    readStatement = read.statement();
    readBinds = read.binds();
}

ReadErasure::ErasureStream::~ErasureStream() = default;

Database::Context ReadErasure::ErasureStream::createIndex()
{
    auto index =
            operation.layout().erasureIndex(input().identity, input().stream.buildIndexWhere());
    auto binds = index.binds();
    input().stream.mergeIndexBinds(binds);
    return index.statement().execute(binds);
}

std::unique_ptr<
        ReadOperation::Block> ReadErasure::ErasureStream::createBlock(Structure::Block block,
                                                                      double modified,
                                                                      const Structure::Identity &identity)
{ return std::unique_ptr<Block>(new ErasureBlock(*this, block, modified, identity)); }

void ReadErasure::ErasureStream::advanceStream(double time)
{ target->advance(time); }

void ReadErasure::ErasureStream::completeStream()
{
    target->end();
    target = nullptr;
}


namespace Available {

bool Names::processOutput(std::unique_lock<std::mutex> &)
{ return false; }

bool Names::finishOutput(std::unique_lock<std::mutex> &)
{ return false; }

SequenceName::Set Names::result() const
{
    Q_ASSERT(const_cast<Names *>(this)->wait(0.0));
    return output;
}

Names::Names(Structure::Layout &layout,
             IdentityIndex &index,
             const Selection::List &selections,
             Granularity granularity) : ReadOperation(layout, index),
                                        granularity(granularity),
                                        output()
{
    auto inputs = SequenceFilter::Input::sequence(layout, index, selections);
    for (auto &input : inputs) {
        startStream(std::unique_ptr<Stream>(new AvailableStream(*this, std::move(input))));
    }
}

Names::~Names()
{ join(); }


Names::AvailableBlock::AvailableBlock(AvailableStream &stream,
                                      Structure::Block block,
                                      double modified,
                                      const Structure::Identity &identity) : Block(stream, block,
                                                                                   modified,
                                                                                   identity),
                                                                             stream(stream)
{ }

Names::AvailableBlock::~AvailableBlock() = default;

Database::Context Names::AvailableBlock::createRead(const Structure::Identity &identity,
                                                    Structure::Block block)
{
    SequenceFilter::Input &input = stream.input();

    auto binds = stream.readBinds;
    if (input.identity.station == 0)
        binds.emplace(Structure::Column_Station, static_cast<std::int_fast64_t>(identity.station));
    if (input.identity.archive == 0)
        binds.emplace(Structure::Column_Archive, static_cast<std::int_fast64_t>(identity.archive));
    if (input.identity.variable == 0)
        binds.emplace(Structure::Column_Variable,
                      static_cast<std::int_fast64_t>(identity.variable));

    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    return stream.readStatement.execute(binds, true);
}

bool Names::AvailableBlock::unpack(const Util::ByteArray &data,
                                   const SequenceName &baseName,
                                   StringIndex &flavors,
                                   SequenceFilter::Block &filter,
                                   std::unique_lock<std::mutex> &lock)
{
    auto output = ReadBlock::names(data, baseName, flavors, filter);
    if (output.empty())
        return false;
    Q_ASSERT(!lock.owns_lock());
    lock.lock();
    stream.operation.output.insert(output.begin(), output.end());
    return false;
}

bool Names::AvailableBlock::nameProcess(Structure::Block block,
                                        const SequenceName &name,
                                        std::unique_lock<std::mutex> &lock)
{
    switch (stream.operation.granularity) {
    case ReadAll:
        return false;
    case SpanningOnly:
        if (block >= 0)
            break;
        return false;
    case IndexOnly:
        break;
    }
    lock.lock();
    stream.operation.output.insert(name);
    return true;
}


Names::AvailableStream::AvailableStream(Names &operation,
                                        std::unique_ptr<SequenceFilter::Input> &&input) : Stream(
        operation, std::move(input)), operation(operation)
{
    if (operation.granularity != IndexOnly) {
        std::string whereBlock = Structure::Column_Block;
        whereBlock += " = :";
        whereBlock += Structure::Column_Block;

        auto read = operation.layout().sequenceRead(this->input().identity, whereBlock);
        readStatement = read.statement();
        readBinds = read.binds();
    }
}

Names::AvailableStream::~AvailableStream() = default;

Database::Context Names::AvailableStream::createIndex()
{
    auto index =
            operation.layout().sequenceIndex(input().identity, input().stream.buildIndexWhere());
    auto binds = index.binds();
    input().stream.mergeIndexBinds(binds);
    return index.statement().execute(binds);
}

std::unique_ptr<ReadOperation::Block> Names::AvailableStream::createBlock(Structure::Block block,
                                                                          double modified,
                                                                          const Structure::Identity &identity)
{ return std::unique_ptr<Block>(new AvailableBlock(*this, block, modified, identity)); }

void Names::AvailableStream::advanceStream(double)
{ }

void Names::AvailableStream::completeStream()
{ }


bool Components::processOutput(std::unique_lock<std::mutex> &)
{ return false; }

bool Components::finishOutput(std::unique_lock<std::mutex> &)
{ return false; }

SequenceName::ComponentSet Components::stations() const
{
    Q_ASSERT(const_cast<Components *>(this)->wait(0.0));
    return outputStations;
}

SequenceName::ComponentSet Components::allStations(IdentityIndex &index)
{
    SequenceName::ComponentSet out;
    auto in = index.station.lookup();
    for (auto &add : in) {
        Q_ASSERT(!add.second.empty());
        out.emplace(std::move(add.second));
    }
    return out;
}

SequenceName::ComponentSet Components::archives() const
{
    Q_ASSERT(const_cast<Components *>(this)->wait(0.0));
    return outputArchives;
}

SequenceName::ComponentSet Components::allArchives(IdentityIndex &index)
{
    SequenceName::ComponentSet out;
    auto in = index.archive.lookup();
    for (auto &add : in) {
        Q_ASSERT(!add.second.empty());
        out.emplace(std::move(add.second));
    }
    return out;
}

SequenceName::ComponentSet Components::variables() const
{
    Q_ASSERT(const_cast<Components *>(this)->wait(0.0));
    return outputVariables;
}

SequenceName::ComponentSet Components::allVariables(IdentityIndex &index)
{
    SequenceName::ComponentSet out;
    auto in = index.variable.lookup();
    for (auto &add : in) {
        Q_ASSERT(!add.second.empty());
        out.emplace(std::move(add.second));
    }
    return out;
}

SequenceName::ComponentSet Components::flavors() const
{
    Q_ASSERT(const_cast<Components *>(this)->wait(0.0));
    return outputFlavors;
}

SequenceName::ComponentSet Components::allFlavors(IdentityIndex &index)
{
    SequenceName::ComponentSet out;
    auto in = index.flavor.lookup();
    for (auto &add : in) {
        auto flavors = Util::split_string(add.second, ' ');
        for (auto &f : flavors) {
            if (f.empty())
                continue;
            out.emplace(std::move(f));
        }
    }
    return out;
}

Components::Components(Structure::Layout &layout,
                       IdentityIndex &index,
                       const Selection::List &selections,
                       Granularity granularity) : ReadOperation(layout, index),
                                                  granularity(granularity),
                                                  outputStations(),
                                                  outputArchives(),
                                                  outputVariables(),
                                                  outputFlavors()
{
    auto inputs = SequenceFilter::Input::sequence(layout, index, selections);
    for (auto &input : inputs) {
        startStream(std::unique_ptr<Stream>(new AvailableStream(*this, std::move(input))));
    }
}

Components::~Components()
{ join(); }


Components::AvailableBlock::AvailableBlock(AvailableStream &stream,
                                           Structure::Block block,
                                           double modified,
                                           const Structure::Identity &identity) : Block(stream,
                                                                                        block,
                                                                                        modified,
                                                                                        identity),
                                                                                  stream(stream)
{ }

Components::AvailableBlock::~AvailableBlock() = default;

Database::Context Components::AvailableBlock::createRead(const Structure::Identity &identity,
                                                         Structure::Block block)
{
    SequenceFilter::Input &input = stream.input();

    auto binds = stream.readBinds;
    if (input.identity.station == 0)
        binds.emplace(Structure::Column_Station, static_cast<std::int_fast64_t>(identity.station));
    if (input.identity.archive == 0)
        binds.emplace(Structure::Column_Archive, static_cast<std::int_fast64_t>(identity.archive));
    if (input.identity.variable == 0)
        binds.emplace(Structure::Column_Variable,
                      static_cast<std::int_fast64_t>(identity.variable));

    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    return stream.readStatement.execute(binds, true);
}

bool Components::AvailableBlock::unpack(const Util::ByteArray &data,
                                        const SequenceName &baseName,
                                        StringIndex &flavors,
                                        SequenceFilter::Block &filter,
                                        std::unique_lock<std::mutex> &lock)
{
    auto output = ReadBlock::names(data, baseName, flavors, filter);
    if (output.empty())
        return false;
    Q_ASSERT(!lock.owns_lock());
    lock.lock();
    stream.operation.outputStations.emplace(baseName.getStation());
    stream.operation.outputArchives.emplace(baseName.getArchive());
    stream.operation.outputVariables.emplace(baseName.getVariable());
    for (const auto &name : output) {
        auto add = name.getFlavors();
        stream.operation.outputFlavors.insert(add.begin(), add.end());
    }
    return false;
}

bool Components::AvailableBlock::nameProcess(Structure::Block block,
                                             const SequenceName &name,
                                             std::unique_lock<std::mutex> &lock)
{
    switch (stream.operation.granularity) {
    case ReadAll:
        return false;
    case SpanningOnly:
        if (block >= 0)
            break;
        return false;
    case IndexOnly:
        break;
    }
    lock.lock();
    stream.operation.outputStations.emplace(name.getStation());
    stream.operation.outputArchives.emplace(name.getArchive());
    stream.operation.outputVariables.emplace(name.getVariable());
    return true;
}


Components::AvailableStream::AvailableStream(Components &operation,
                                             std::unique_ptr<SequenceFilter::Input> &&input)
        : Stream(operation, std::move(input)), operation(operation)
{
    if (operation.granularity != IndexOnly) {
        std::string whereBlock = Structure::Column_Block;
        whereBlock += " = :";
        whereBlock += Structure::Column_Block;

        auto read = operation.layout().sequenceRead(this->input().identity, whereBlock);
        readStatement = read.statement();
        readBinds = read.binds();
    }
}

Components::AvailableStream::~AvailableStream() = default;

Database::Context Components::AvailableStream::createIndex()
{
    auto index =
            operation.layout().sequenceIndex(input().identity, input().stream.buildIndexWhere());
    auto binds = index.binds();
    input().stream.mergeIndexBinds(binds);
    return index.statement().execute(binds);
}

std::unique_ptr<
        ReadOperation::Block> Components::AvailableStream::createBlock(Structure::Block block,
                                                                       double modified,
                                                                       const Structure::Identity &identity)
{ return std::unique_ptr<Block>(new AvailableBlock(*this, block, modified, identity)); }

void Components::AvailableStream::advanceStream(double)
{ }

void Components::AvailableStream::completeStream()
{ }


bool Exists::processOutput(std::unique_lock<std::mutex> &)
{ return false; }

bool Exists::finishOutput(std::unique_lock<std::mutex> &)
{ return false; }

bool Exists::result() const
{
    Q_ASSERT(const_cast<Exists *>(this)->wait(0.0));
    return output;
}

Exists::Exists(Structure::Layout &layout,
               IdentityIndex &index,
               const Selection::List &selections,
               Granularity granularity) : ReadOperation(layout, index),
                                          granularity(granularity),
                                          output(false)
{
    auto inputs = SequenceFilter::Input::sequence(layout, index, selections);
    for (auto &input : inputs) {
        startStream(std::unique_ptr<Stream>(new AvailableStream(*this, std::move(input))));
    }
}

Exists::~Exists()
{ join(); }


Exists::AvailableBlock::AvailableBlock(AvailableStream &stream,
                                       Structure::Block block,
                                       double modified,
                                       const Structure::Identity &identity) : Block(stream, block,
                                                                                    modified,
                                                                                    identity),
                                                                              stream(stream)
{ }

Exists::AvailableBlock::~AvailableBlock() = default;

Database::Context Exists::AvailableBlock::createRead(const Structure::Identity &identity,
                                                     Structure::Block block)
{
    SequenceFilter::Input &input = stream.input();

    auto binds = stream.readBinds;
    if (input.identity.station == 0)
        binds.emplace(Structure::Column_Station, static_cast<std::int_fast64_t>(identity.station));
    if (input.identity.archive == 0)
        binds.emplace(Structure::Column_Archive, static_cast<std::int_fast64_t>(identity.archive));
    if (input.identity.variable == 0)
        binds.emplace(Structure::Column_Variable,
                      static_cast<std::int_fast64_t>(identity.variable));

    binds.emplace(Structure::Column_Block, static_cast<std::int_fast64_t>(block));
    return stream.readStatement.execute(binds, true);
}

bool Exists::AvailableBlock::unpack(const Util::ByteArray &data,
                                    const SequenceName &baseName,
                                    StringIndex &flavors,
                                    SequenceFilter::Block &filter,
                                    std::unique_lock<std::mutex> &lock)
{
    auto output = ReadBlock::names(data, baseName, flavors, filter);
    if (output.empty())
        return false;
    Q_ASSERT(!lock.owns_lock());
    lock.lock();
    stream.operation.output = true;
    operationAbort(lock);
    return false;
}

bool Exists::AvailableBlock::nameProcess(Structure::Block block,
                                         const SequenceName &,
                                         std::unique_lock<std::mutex> &lock)
{
    switch (stream.operation.granularity) {
    case ReadAll:
        return false;
    case SpanningOnly:
        if (block >= 0)
            break;
        return false;
    case IndexOnly:
        break;
    }
    lock.lock();
    stream.operation.output = true;
    operationAbort(lock);
    return true;
}


Exists::AvailableStream::AvailableStream(Exists &operation,
                                         std::unique_ptr<SequenceFilter::Input> &&input) : Stream(
        operation, std::move(input)), operation(operation)
{
    if (operation.granularity != IndexOnly) {
        std::string whereBlock = Structure::Column_Block;
        whereBlock += " = :";
        whereBlock += Structure::Column_Block;

        auto read = operation.layout().sequenceRead(this->input().identity, whereBlock);
        readStatement = read.statement();
        readBinds = read.binds();
    }
}

Exists::AvailableStream::~AvailableStream() = default;

Database::Context Exists::AvailableStream::createIndex()
{
    auto index =
            operation.layout().sequenceIndex(input().identity, input().stream.buildIndexWhere());
    auto binds = index.binds();
    input().stream.mergeIndexBinds(binds);
    return index.statement().execute(binds);
}

std::unique_ptr<ReadOperation::Block> Exists::AvailableStream::createBlock(Structure::Block block,
                                                                           double modified,
                                                                           const Structure::Identity &identity)
{ return std::unique_ptr<Block>(new AvailableBlock(*this, block, modified, identity)); }

void Exists::AvailableStream::advanceStream(double)
{ }

void Exists::AvailableStream::completeStream()
{ }

}

}
}
}