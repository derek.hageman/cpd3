/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <cmath>
#include <cstring>
#include <string>
#include <algorithm>

#include "structure.hxx"


namespace CPD3 {
namespace Data {
namespace Archive {
namespace Structure {

Identity::Identity(Identity::index s, Identity::index a, Identity::index v) : station(s),
                                                                              archive(a),
                                                                              variable(v)
{ }

Identity::Identity() : station(0), archive(0), variable(0)
{ }

Layout::Backend::Backend(Database::Connection *connection) : connection(connection)
{ }

Layout::Backend::~Backend() = default;

static std::unordered_set<Identity> processUnique(const Identity &base,
                                                  bool station,
                                                  bool archive,
                                                  bool variable,
                                                  Database::Context::Iterator &&it)
{
    std::unordered_set<Identity> unique;
    for (;;) {
        auto results = it.next();
        if (results.empty())
            break;

        Identity identity = base;

        auto rptr = results.cbegin();
        if (station) {
            Q_ASSERT(rptr != results.cend());
            Q_ASSERT(identity.station == 0 ||
                             identity.station ==
                                     static_cast<Structure::Identity::index>(rptr->toInteger()));
            identity.station = static_cast<Structure::Identity::index>(rptr->toInteger());
            ++rptr;
        }
        if (archive) {
            Q_ASSERT(rptr != results.cend());
            Q_ASSERT(identity.archive == 0 ||
                             identity.archive ==
                                     static_cast<Structure::Identity::index>(rptr->toInteger()));
            identity.archive = static_cast<Structure::Identity::index>(rptr->toInteger());
            ++rptr;
        }
        if (variable) {
            Q_ASSERT(rptr != results.cend());
            Q_ASSERT(identity.variable == 0 ||
                             identity.variable ==
                                     static_cast<Structure::Identity::index>(rptr->toInteger()));
            identity.variable = static_cast<Structure::Identity::index>(rptr->toInteger());
            ++rptr;
        }

        unique.insert(std::move(identity));
    }
    return unique;
}

class Layout::Backend_V1 : public Layout::Backend {

    static std::string incorporateWhere(const Identity &identity,
                                        const std::string &input,
                                        Database::Types::Binds &binds,
                                        bool requireIdentity = true)
    {
        std::string where;

        if (requireIdentity || identity.variable != 0) {
            if (!where.empty())
                where += " AND ";
            where += " ( ";
            where += Column_Variable;
            where += " = :";
            where += Column_Variable;
            where += " ) ";
            if (identity.variable != 0)
                binds.emplace(Column_Variable, static_cast<std::int_fast64_t>(identity.variable));
        }

        if (requireIdentity || identity.station != 0) {
            if (!where.empty())
                where += " AND ";
            where += " ( ";
            where += Column_Station;
            where += " = :";
            where += Column_Station;
            where += " ) ";
            if (identity.station != 0)
                binds.emplace(Column_Station, static_cast<std::int_fast64_t>(identity.station));
        }

        if (requireIdentity || identity.archive != 0) {
            if (!where.empty())
                where += " AND ";
            where += " ( ";
            where += Column_Archive;
            where += " = :";
            where += Column_Archive;
            where += " ) ";
            if (identity.archive != 0)
                binds.emplace(Column_Archive, static_cast<std::int_fast64_t>(identity.archive));
        }

        if (!input.empty()) {
            if (!where.empty())
                where += " AND ";
            where += " ( ";
            where += input;
            where += " ) ";
        }

        return where;
    }

    static void incorporateIdentity(const Identity &identity, Database::Types::Binds &binds)
    {
        Q_ASSERT(identity.station != 0);
        binds.emplace(Column_Station, static_cast<std::int_fast64_t>(identity.station));
        Q_ASSERT(identity.archive != 0);
        binds.emplace(Column_Archive, static_cast<std::int_fast64_t>(identity.archive));
        Q_ASSERT(identity.variable != 0);
        binds.emplace(Column_Variable, static_cast<std::int_fast64_t>(identity.variable));
    }

    static std::string buildUnique(const Identity &identity,
                                   bool station,
                                   bool archive,
                                   bool variable,
                                   std::size_t &outputs)
    {
        std::string query;
        if (station) {
            if (!query.empty())
                query += " , ";
            query += Column_Station;
            ++outputs;
        }
        if (archive) {
            if (!query.empty())
                query += " , ";
            query += Column_Archive;
            ++outputs;
        }
        if (variable) {
            if (!query.empty())
                query += " , ";
            query += Column_Variable;
            ++outputs;
        }
        return query;
    }

public:
    Backend_V1(Database::Connection *connection) : Backend(connection)
    { }

    virtual ~Backend_V1() = default;

    bool splitStation() const override
    { return false; }

    bool splitArchive() const override
    { return false; }

    bool splitVariable() const override
    { return false; }


    Database::Types::Variant targetModified(double modified) const override
    {
        Q_ASSERT(FP::defined(modified));
        return std::ceil(modified * 1000.0) / 1000.0;
    }

    Database::Types::Variant modifiedQuery(double modified) const override
    {
        Q_ASSERT(FP::defined(modified));
        return std::floor(modified * 1000.0) / 1000.0;
    }

    double modifiedValue(const Database::Types::Variant &modified) const override
    { return modified.toReal(); }

    double modifiedThreshold(double input) const override
    {
        if (!FP::defined(input))
            return input;
        return std::floor(input * 1000.0) / 1000.0;
    }


    static constexpr const char *Sequence_Table = "CPD3Data";

    std::string sequenceTable(const Identity &) const override
    { return Sequence_Table; }

    std::unordered_set<Identity> sequencePossible() const override
    {
        if (!connection->hasTable(Sequence_Table))
            return std::unordered_set<Identity>();
        return std::unordered_set<Identity>{Identity(0, 0, 0)};
    }

    std::unordered_set<Identity> sequenceUnique(const Identity &identity,
                                                bool station,
                                                bool archive,
                                                bool variable) const override
    {
        std::size_t outputs = 0;
        auto query = buildUnique(identity, station, archive, variable, outputs);
        if (!outputs)
            return {identity};
        query = "SELECT DISTINCT " + query + " FROM " + Sequence_Table;
        Database::Types::Binds binds;
        auto where = incorporateWhere(identity, {}, binds, false);
        if (!where.empty())
            query += " WHERE " + where;

        return processUnique(identity, station, archive, variable, Database::Context::Iterator(
                connection->general(query, outputs).execute(binds)));
    }

    DatabaseAccess sequenceRead(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds);
        return DatabaseAccess(
                connection->select(Sequence_Table, {Column_Data, Column_Block}, selectWhere),
                std::move(binds));
    }

    DatabaseAccess sequenceWrite(const Identity &identity, WriteMode mode) const override
    {
        switch (mode) {
        case WriteMode::Any: {
            Database::Types::Binds binds;
            incorporateIdentity(identity, binds);
            return DatabaseAccess(connection->merge(Sequence_Table, {Column_Station, Column_Archive,
                                                                     Column_Variable, Column_Block},
                                                    {Column_Modified, Column_Data}),
                                  std::move(binds));
        }
        case WriteMode::KnownNew: {
            Database::Types::Binds binds;
            incorporateIdentity(identity, binds);
            return DatabaseAccess(connection->insert(Sequence_Table,
                                                     {Column_Station, Column_Archive,
                                                      Column_Variable, Column_Block,
                                                      Column_Modified, Column_Data}),
                                  std::move(binds));
        }
        case WriteMode::KnownExisting: {
            Database::Types::Binds binds;
            std::string whereBlock = Column_Block;
            whereBlock += " = :";
            whereBlock += Column_Block;
            std::string selectWhere = incorporateWhere(identity, whereBlock, binds);
            return DatabaseAccess(
                    connection->update(Sequence_Table, {Column_Modified, Column_Data}, selectWhere),
                    std::move(binds));
        }
        }
        Q_ASSERT(false);
        return DatabaseAccess();
    }

    DatabaseAccess sequenceDelete(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds);
        return DatabaseAccess(connection->del(Sequence_Table, selectWhere), std::move(binds));
    }

    DatabaseAccess sequenceSize(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds);

        std::string size = "LENGTH(";
        size += Column_Data;
        size += ")";

        return DatabaseAccess(connection->select(Sequence_Table, {size, Column_Block}, selectWhere,
                                                 size + " ASC"), std::move(binds));
    }

    DatabaseAccess sequenceMinimum(const Identity &identity) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, std::string(), binds);

        std::string min = "MIN(";
        min += Column_Block;
        min += ")";

        return DatabaseAccess(connection->select(Sequence_Table, {min}, selectWhere),
                              std::move(binds));
    }

    DatabaseAccess sequenceIndex(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds, false);
        std::string order = Column_Block;
        order += " ASC";
        return DatabaseAccess(connection->select(Sequence_Table,
                                                 {Column_Block, Column_Modified, Column_Station,
                                                  Column_Archive, Column_Variable}, selectWhere,
                                                 order), std::move(binds));
    }


    static constexpr const char *Erasure_Table = "CPD3Deleted";

    std::string erasureTable(const Identity &identity) const override
    { return Erasure_Table; }

    std::unordered_set<Identity> erasurePossible() const override
    {
        if (!connection->hasTable(Erasure_Table))
            return std::unordered_set<Identity>();
        return std::unordered_set<Identity>{Identity(0, 0, 0)};
    }

    std::unordered_set<Identity> erasureUnique(const Identity &identity,
                                               bool station,
                                               bool archive,
                                               bool variable) const override
    {
        std::size_t outputs = 0;
        auto query = buildUnique(identity, station, archive, variable, outputs);
        if (!outputs)
            return {identity};
        query = "SELECT DISTINCT " + query + " FROM " + Erasure_Table;
        Database::Types::Binds binds;
        auto where = incorporateWhere(identity, {}, binds, false);
        if (!where.empty())
            query += " WHERE " + where;

        return processUnique(identity, station, archive, variable, Database::Context::Iterator(
                connection->general(query, outputs).execute(binds)));
    }

    DatabaseAccess erasureRead(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds);
        return DatabaseAccess(
                connection->select(Erasure_Table, {Column_Data, Column_Block}, selectWhere),
                std::move(binds));
    }

    DatabaseAccess erasureWrite(const Identity &identity, WriteMode mode) const override
    {
        switch (mode) {
        case WriteMode::Any: {
            Database::Types::Binds binds;
            incorporateIdentity(identity, binds);
            return DatabaseAccess(connection->merge(Erasure_Table, {Column_Station, Column_Archive,
                                                                    Column_Variable, Column_Block},
                                                    {Column_Modified, Column_Data}),
                                  std::move(binds));
        }
        case WriteMode::KnownNew: {
            Database::Types::Binds binds;
            incorporateIdentity(identity, binds);
            return DatabaseAccess(connection->insert(Erasure_Table, {Column_Station, Column_Archive,
                                                                     Column_Variable, Column_Block,
                                                                     Column_Modified, Column_Data}),
                                  std::move(binds));
        }
        case WriteMode::KnownExisting: {
            Database::Types::Binds binds;
            std::string whereBlock = Column_Block;
            whereBlock += " = :";
            whereBlock += Column_Block;
            std::string selectWhere = incorporateWhere(identity, whereBlock, binds);
            return DatabaseAccess(
                    connection->update(Erasure_Table, {Column_Modified, Column_Data}, selectWhere),
                    std::move(binds));
        }
        }
        Q_ASSERT(false);
        return DatabaseAccess();
    }

    DatabaseAccess erasureDelete(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds);
        return DatabaseAccess(connection->del(Erasure_Table, selectWhere), std::move(binds));
    }

    DatabaseAccess erasureSize(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds);

        std::string size = "LENGTH(";
        size += Column_Data;
        size += ")";

        return DatabaseAccess(
                connection->select(Erasure_Table, {size, Column_Block}, selectWhere, size + " ASC"),
                std::move(binds));
    }

    DatabaseAccess erasureMinimum(const Identity &identity) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, std::string(), binds);

        std::string min = "MIN(";
        min += Column_Block;
        min += ")";

        return DatabaseAccess(connection->select(Erasure_Table, {min}, selectWhere),
                              std::move(binds));
    }

    DatabaseAccess erasureIndex(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds, false);
        std::string order = Column_Block;
        order += " ASC";
        return DatabaseAccess(connection->select(Erasure_Table,
                                                 {Column_Block, Column_Modified, Column_Station,
                                                  Column_Archive, Column_Variable}, selectWhere,
                                                 order), std::move(binds));
    }
};

class Layout::Backend_V2 : public Layout::Backend {
    bool isStationSplit;
    bool isArchiveSplit;
    bool isVariableSplit;

    std::string tableName(const Identity &identity, const char *base) const
    {
        std::string table = base;
        if (splitStation()) {
            Q_ASSERT(identity.station != 0);
            table += "_s";
            table += std::to_string(identity.station);
        }
        if (splitArchive()) {
            Q_ASSERT(identity.archive != 0);
            table += "_a";
            table += std::to_string(identity.archive);
        }
        if (splitVariable()) {
            Q_ASSERT(identity.variable != 0);
            table += "_v";
            table += std::to_string(identity.variable);
        }
        return table;
    }

    static bool extractTableComponent(const char *&data,
                                      const char *end,
                                      char code,
                                      Identity::index &output)
    {
        if (data + 3 > end)
            return false;
        if (*data != '_')
            return false;
        ++data;
        if (std::tolower(*data) != code)
            return false;
        ++data;

        Q_ASSERT(output == 0);
        for (; data != end; ++data) {
            char add = *data;
            if (add < '0' || add > '9')
                break;

            output *= 10;
            output += static_cast<Identity::index>(add - '0');
        }

        return output != 0;
    }

    std::unordered_set<Identity> tableFilter(const char *base) const
    {
        std::size_t lenBase = std::strlen(base);
        const char *endBase = base + lenBase;

        std::unordered_set<Identity> result;
        for (const auto &table : connection->tables()) {
            if (table.length() < lenBase)
                continue;
            if (!std::equal(base, endBase, table.begin(), [](char cA, char cB) -> bool {
                return std::tolower(cA) == std::tolower(cB);
            }))
                continue;

            const char *tableData = table.data() + lenBase;
            const char *tableEnd = tableData + table.length();

            Identity::index station = 0;
            if (splitStation()) {
                if (!extractTableComponent(tableData, tableEnd, 's', station))
                    continue;
            }

            Identity::index archive = 0;
            if (splitArchive()) {
                if (!extractTableComponent(tableData, tableEnd, 'a', archive))
                    continue;
            }

            Identity::index variable = 0;
            if (splitVariable()) {
                if (!extractTableComponent(tableData, tableEnd, 'v', variable))
                    continue;
            }

            result.emplace(station, archive, variable);
        }
        return result;
    }

    std::string incorporateWhere(const Identity &identity,
                                 const std::string &input,
                                 Database::Types::Binds &binds,
                                 bool requireIdentity = true) const
    {
        std::string where;

        if (splitVariable()) {
            Q_ASSERT(identity.variable != 0);
        } else if (requireIdentity || identity.variable != 0) {
            if (!where.empty())
                where += " AND ";
            where += " ( ";
            where += Column_Variable;
            where += " = :";
            where += Column_Variable;
            where += " ) ";
            if (identity.variable != 0)
                binds.emplace(Column_Variable, static_cast<std::int_fast64_t>(identity.variable));
        }

        if (splitStation()) {
            Q_ASSERT(identity.station != 0);
        } else if (requireIdentity || identity.station != 0) {
            if (!where.empty())
                where += " AND ";
            where += " ( ";
            where += Column_Station;
            where += " = :";
            where += Column_Station;
            where += " ) ";
            if (identity.station != 0)
                binds.emplace(Column_Station, static_cast<std::int_fast64_t>(identity.station));
        }

        if (splitArchive()) {
            Q_ASSERT(identity.archive != 0);
        } else if (requireIdentity || identity.archive != 0) {
            if (!where.empty())
                where += " AND ";
            where += " ( ";
            where += Column_Archive;
            where += " = :";
            where += Column_Archive;
            where += " ) ";
            if (identity.archive != 0)
                binds.emplace(Column_Archive, static_cast<std::int_fast64_t>(identity.archive));
        }

        if (!input.empty()) {
            if (!where.empty())
                where += " AND ";
            where += " ( ";
            where += input;
            where += " ) ";
        }

        return where;
    }

    void incorporateIdentity(const Identity &identity, Database::Types::Binds &binds) const
    {
        if (!splitStation()) {
            Q_ASSERT(identity.station != 0);
            binds.emplace(Column_Station, static_cast<std::int_fast64_t>(identity.station));
        }
        if (!splitArchive()) {
            Q_ASSERT(identity.archive != 0);
            binds.emplace(Column_Archive, static_cast<std::int_fast64_t>(identity.archive));
        }
        if (!splitVariable()) {
            Q_ASSERT(identity.variable != 0);
            binds.emplace(Column_Variable, static_cast<std::int_fast64_t>(identity.variable));
        }
    }

    template<typename T>
    void incorporateOutputs(T target) const
    {
        if (!splitStation()) {
            *target = Column_Station;
            ++target;
        }
        if (!splitArchive()) {
            *target = Column_Archive;
            ++target;
        }
        if (!splitVariable()) {
            *target = Column_Variable;
        }
    }

    std::string buildUnique(const Identity &identity,
                            bool station,
                            bool archive,
                            bool variable, Database::Types::Binds &binds,
                            std::size_t &outputs) const
    {
        std::string query;
        if (station) {
            if (!query.empty())
                query += " , ";
            if (splitStation()) {
                Q_ASSERT(identity.station != 0);
                query += ":";
                query += Column_Station;
                binds.emplace(Column_Station, static_cast<std::int_fast64_t>(identity.station));
            } else {
                query += Column_Station;
            }
            ++outputs;
        }
        if (archive) {
            if (!query.empty())
                query += " , ";
            if (splitArchive()) {
                Q_ASSERT(identity.archive != 0);
                query += ":";
                query += Column_Archive;
                binds.emplace(Column_Archive, static_cast<std::int_fast64_t>(identity.archive));
            } else {
                query += Column_Archive;
            }
            ++outputs;
        }
        if (variable) {
            if (!query.empty())
                query += " , ";
            if (splitVariable()) {
                Q_ASSERT(identity.variable != 0);
                query += ":";
                query += Column_Variable;
                binds.emplace(Column_Variable, static_cast<std::int_fast64_t>(identity.variable));
            } else {
                query += Column_Variable;
            }
            ++outputs;
        }
        return query;
    }

public:
    Backend_V2(Database::Connection *connection,
               bool isStationSplit,
               bool isArchiveSplit,
               bool isVariableSplit) : Backend(connection),
                                       isStationSplit(isStationSplit),
                                       isArchiveSplit(isArchiveSplit),
                                       isVariableSplit(isVariableSplit)
    { }

    virtual ~Backend_V2() = default;

    bool splitStation() const override
    { return isStationSplit; }

    bool splitArchive() const override
    { return isArchiveSplit; }

    bool splitVariable() const override
    { return isVariableSplit; }


    Database::Types::Variant targetModified(double modified) const override
    {
        Q_ASSERT(FP::defined(modified));
        return static_cast<std::int_fast64_t>(std::ceil(modified * 1000.0));
    }

    Database::Types::Variant modifiedQuery(double modified) const override
    {
        Q_ASSERT(FP::defined(modified));
        return static_cast<std::int_fast64_t>(std::floor(modified * 1000.0));
    }

    double modifiedValue(const Database::Types::Variant &modified) const override
    { return static_cast<double>(modified.toInteger() / 1000.0); }

    double modifiedThreshold(double input) const override
    {
        if (!FP::defined(input))
            return input;
        return std::floor(input * 1000.0) / 1000.0;
    }


    std::string sequenceTable(const Identity &identity) const override
    { return tableName(identity, Existing::Table_Prefix); }

    std::unordered_set<Identity> sequencePossible() const override
    { return tableFilter(Existing::Table_Prefix); }

    std::unordered_set<Identity> sequenceUnique(const Identity &identity,
                                                bool station,
                                                bool archive,
                                                bool variable) const override
    {
        std::size_t outputs = 0;
        Database::Types::Binds binds;
        auto query = buildUnique(identity, station, archive, variable, binds, outputs);
        if (!outputs)
            return {identity};
        query = "SELECT DISTINCT " + query + " FROM " + sequenceTable(identity);
        auto where = incorporateWhere(identity, {}, binds, false);
        if (!where.empty())
            query += " WHERE " + where;

        return processUnique(identity, station, archive, variable, Database::Context::Iterator(
                connection->general(query, outputs).execute(binds)));
    }

    DatabaseAccess sequenceRead(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds);
        return DatabaseAccess(
                connection->select(sequenceTable(identity), {Column_Data, Column_Block},
                                   selectWhere), std::move(binds));
    }

    DatabaseAccess sequenceWrite(const Identity &identity, WriteMode mode) const override
    {
        switch (mode) {
        case WriteMode::Any: {
            Database::Types::Binds binds;
            incorporateIdentity(identity, binds);
            Database::Types::ColumnSet keys{Column_Block};
            incorporateOutputs(std::inserter(keys, keys.end()));
            return DatabaseAccess(connection->merge(sequenceTable(identity), keys,
                                                    {Column_Modified, Column_Data}),
                                  std::move(binds));
        }
        case WriteMode::KnownNew: {
            Database::Types::Binds binds;
            incorporateIdentity(identity, binds);
            Database::Types::ColumnSet keys{Column_Block, Column_Modified, Column_Data};
            incorporateOutputs(std::inserter(keys, keys.end()));
            return DatabaseAccess(connection->insert(sequenceTable(identity), keys),
                                  std::move(binds));
        }
        case WriteMode::KnownExisting: {
            Database::Types::Binds binds;
            std::string whereBlock = Column_Block;
            whereBlock += " = :";
            whereBlock += Column_Block;
            std::string selectWhere = incorporateWhere(identity, whereBlock, binds);
            return DatabaseAccess(
                    connection->update(sequenceTable(identity), {Column_Modified, Column_Data},
                                       selectWhere), std::move(binds));
        }
        }
        Q_ASSERT(false);
        return DatabaseAccess();
    }

    DatabaseAccess sequenceDelete(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds);
        return DatabaseAccess(connection->del(sequenceTable(identity), selectWhere),
                              std::move(binds));
    }

    DatabaseAccess sequenceSize(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds);

        std::string size = "LENGTH(";
        size += Column_Data;
        size += ")";

        return DatabaseAccess(
                connection->select(sequenceTable(identity), {size, Column_Block}, selectWhere,
                                   size + " ASC"), std::move(binds));
    }

    DatabaseAccess sequenceMinimum(const Identity &identity) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, std::string(), binds);

        std::string min = "MIN(";
        min += Column_Block;
        min += ")";

        return DatabaseAccess(connection->select(sequenceTable(identity), {min}, selectWhere),
                              std::move(binds));
    }

    DatabaseAccess sequenceIndex(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds, false);
        Database::Types::Outputs outputs{Column_Block, Column_Modified};
        incorporateOutputs(Util::back_emplacer(outputs));
        std::string order = Column_Block;
        order += " ASC";
        return DatabaseAccess(
                connection->select(sequenceTable(identity), outputs, selectWhere, order),
                std::move(binds));
    }


    std::string erasureTable(const Identity &identity) const override
    { return tableName(identity, Erasure::Table_Prefix); }

    std::unordered_set<Identity> erasurePossible() const override
    { return tableFilter(Erasure::Table_Prefix); }

    std::unordered_set<Identity> erasureUnique(const Identity &identity,
                                               bool station,
                                               bool archive,
                                               bool variable) const override
    {
        std::size_t outputs = 0;
        Database::Types::Binds binds;
        auto query = buildUnique(identity, station, archive, variable, binds, outputs);
        if (!outputs)
            return {identity};
        query = "SELECT DISTINCT " + query + " FROM " + erasureTable(identity);
        auto where = incorporateWhere(identity, {}, binds, false);
        if (!where.empty())
            query += " WHERE " + where;

        return processUnique(identity, station, archive, variable, Database::Context::Iterator(
                connection->general(query, outputs).execute(binds)));
    }

    DatabaseAccess erasureRead(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds);
        return DatabaseAccess(
                connection->select(erasureTable(identity), {Column_Data, Column_Block},
                                   selectWhere), std::move(binds));
    }

    DatabaseAccess erasureWrite(const Identity &identity, WriteMode mode) const override
    {
        switch (mode) {
        case WriteMode::Any: {
            Database::Types::Binds binds;
            incorporateIdentity(identity, binds);
            Database::Types::ColumnSet keys{Column_Block};
            incorporateOutputs(std::inserter(keys, keys.end()));
            return DatabaseAccess(
                    connection->merge(erasureTable(identity), keys, {Column_Modified, Column_Data}),
                    std::move(binds));
        }
        case WriteMode::KnownNew: {
            Database::Types::Binds binds;
            incorporateIdentity(identity, binds);
            Database::Types::ColumnSet keys{Column_Block, Column_Modified, Column_Data};
            incorporateOutputs(std::inserter(keys, keys.end()));
            return DatabaseAccess(connection->insert(erasureTable(identity), keys),
                                  std::move(binds));
        }
        case WriteMode::KnownExisting: {
            Database::Types::Binds binds;
            std::string whereBlock = Column_Block;
            whereBlock += " = :";
            whereBlock += Column_Block;
            std::string selectWhere = incorporateWhere(identity, whereBlock, binds);
            return DatabaseAccess(
                    connection->update(erasureTable(identity), {Column_Modified, Column_Data},
                                       selectWhere), std::move(binds));
        }
        }
        Q_ASSERT(false);
        return DatabaseAccess();
    }

    DatabaseAccess erasureDelete(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds);
        return DatabaseAccess(connection->del(erasureTable(identity), selectWhere),
                              std::move(binds));
    }

    DatabaseAccess erasureSize(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds);

        std::string size = "LENGTH(";
        size += Column_Data;
        size += ")";

        return DatabaseAccess(
                connection->select(erasureTable(identity), {size, Column_Block}, selectWhere,
                                   size + " ASC"), std::move(binds));
    }

    DatabaseAccess erasureMinimum(const Identity &identity) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, std::string(), binds);

        std::string min = "MIN(";
        min += Column_Block;
        min += ")";

        return DatabaseAccess(connection->select(erasureTable(identity), {min}, selectWhere),
                              std::move(binds));
    }

    DatabaseAccess erasureIndex(const Identity &identity, const std::string &where) const override
    {
        Database::Types::Binds binds;
        std::string selectWhere = incorporateWhere(identity, where, binds, false);
        Database::Types::Outputs outputs{Column_Block, Column_Modified};
        incorporateOutputs(Util::back_emplacer(outputs));
        std::string order = Column_Block;
        order += " ASC";
        return DatabaseAccess(
                connection->select(erasureTable(identity), outputs, selectWhere, order),
                std::move(binds));
    }
};

Layout::DatabaseAccess::DatabaseAccess(Database::Statement &&statement,
                                       Database::Types::Binds &&binds) : baseStatement(
        std::move(statement)), baseBinds(std::move(binds))
{ }

const Database::Statement &Layout::DatabaseAccess::statement() const
{ return baseStatement; }

Database::Types::Binds Layout::DatabaseAccess::binds() const
{ return baseBinds; }

Layout::DatabaseAccess::DatabaseAccess() = default;

Layout::DatabaseAccess::DatabaseAccess(const Layout::DatabaseAccess &) = default;

Layout::DatabaseAccess &Layout::DatabaseAccess::operator=(const Layout::DatabaseAccess &) = default;

Layout::DatabaseAccess::DatabaseAccess(Layout::DatabaseAccess &&) = default;

Layout::DatabaseAccess &Layout::DatabaseAccess::operator=(Layout::DatabaseAccess &&) = default;


Layout::Layout(Database::Connection *connection) : backend(createBackend(connection))
{ }

static constexpr int systemIdentifier = 1;

static constexpr int systemVersion_V1 = 1;

static constexpr int systemVersion_V2 = 0x08;
static constexpr int systemVersion_V2_Mask = 0x0F;
static constexpr int systemVersion_V2_SplitStation = 0x01;
static constexpr int systemVersion_V2_SplitArchive = 0x02;
static constexpr int systemVersion_V2_SplitVariable = 0x04;

Layout::Backend *Layout::createBackend(Database::Connection *connection)
{
    auto base = connection->select("CPD3DBVersion", {"version"}, "system = :system")
                            .single({{"system", systemIdentifier}});
    if (base.type() != Database::Types::Variant::Type::Integer) {
        qFatal("Archive version not initialized");
        return nullptr;
    }
    int version = static_cast<int>(base.toInteger());

    if (version == systemVersion_V1) {
        return new Backend_V1(connection);
    } else if ((version & systemVersion_V2) && (version & systemVersion_V2_Mask) == version) {
        return new Backend_V2(connection, (version & systemVersion_V2_SplitStation) != 0,
                              (version & systemVersion_V2_SplitArchive) != 0,
                              (version & systemVersion_V2_SplitVariable) != 0);
    }

    qFatal("Unsupported archive version (%d)", version);
    return nullptr;
}

Layout::Layout(Database::Connection *connection,
               bool splitStation,
               bool splitArchive,
               bool splitVariable,
               bool updateIdentifier) : backend(
        new Backend_V2(connection, splitStation, splitArchive, splitVariable))
{
    if (!updateIdentifier)
        return;

    int version = systemVersion_V2;
    if (splitStation)
        version |= systemVersion_V2_SplitStation;
    if (splitArchive)
        version |= systemVersion_V2_SplitArchive;
    if (splitVariable)
        version |= systemVersion_V2_SplitVariable;

    connection->update("CPD3DBVersion", {"version"}, "system = :system")
              .synchronous({{"system",  systemIdentifier},
                            {"version", version}});
}

bool Layout::splitStation() const
{ return backend->splitStation(); }

bool Layout::splitArchive() const
{ return backend->splitArchive(); }

bool Layout::splitVariable() const
{ return backend->splitVariable(); }

Structure::Block Layout::targetBlock(double start, double end) const
{
    Structure::Block sb = startBlock(start);
    if (sb < 0)
        return -1;
    Structure::Block eb = endBlock(end);
    if (eb < 0)
        return -1;
    if (sb != eb)
        return -1;
    return sb;
}

Structure::Block Layout::targetBlock(double advance) const
{ return startBlock(advance); }

Structure::Block Layout::startBlock(double start) const
{
    if (!FP::defined(start) || start < 0.0)
        return -1;
    return static_cast<Structure::Block>(std::floor(start / Block_Interval));
}

Structure::Block Layout::endBlock(double end) const
{
    if (!FP::defined(end) || end <= 0.0)
        return -1;
    return static_cast<Structure::Block>(std::ceil(end / Block_Interval)) - 1;
}

double Layout::blockStart(std::int_fast16_t block) const
{
    if (block < 0)
        return FP::undefined();
    return (double) block * Block_Interval;
}

double Layout::blockEnd(std::int_fast16_t block) const
{
    if (block < 0)
        return FP::undefined();
    return (double) (block + 1) * Block_Interval;
}

Database::Types::Variant Layout::targetModified(double modified) const
{ return backend->targetModified(modified); }

Database::Types::Variant Layout::modifiedQuery(double modified) const
{ return backend->modifiedQuery(modified); }

double Layout::modifiedValue(const Database::Types::Variant &modified) const
{ return backend->modifiedValue(modified); }

double Layout::modifiedThreshold(double input) const
{ return backend->modifiedThreshold(input); }

std::string Layout::sequenceTable(const Identity &identity) const
{ return backend->sequenceTable(identity); }

std::unordered_set<Identity> Layout::sequencePossible() const
{ return backend->sequencePossible(); }

std::unordered_set<Identity> Layout::sequenceUnique(const Identity &identity,
                                                    bool station,
                                                    bool archive,
                                                    bool variable) const
{ return backend->sequenceUnique(identity, station, archive, variable); }

Layout::DatabaseAccess Layout::sequenceRead(const Identity &identity,
                                            const std::string &where) const
{ return backend->sequenceRead(identity, where); }

Layout::DatabaseAccess Layout::sequenceWrite(const Identity &identity, WriteMode mode) const
{ return backend->sequenceWrite(identity, mode); }

Layout::DatabaseAccess Layout::sequenceDelete(const Identity &identity,
                                              const std::string &where) const
{ return backend->sequenceDelete(identity, where); }

Layout::DatabaseAccess Layout::sequenceSize(const Identity &identity,
                                            const std::string &where) const
{ return backend->sequenceSize(identity, where); }

Layout::DatabaseAccess Layout::sequenceMinimum(const Identity &identity) const
{ return backend->sequenceMinimum(identity); }

Layout::DatabaseAccess Layout::sequenceIndex(const Identity &identity,
                                             const std::string &where) const
{ return backend->sequenceIndex(identity, where); }

std::string Layout::erasureTable(const Identity &identity) const
{ return backend->erasureTable(identity); }

std::unordered_set<Identity> Layout::erasurePossible() const
{ return backend->erasurePossible(); }

std::unordered_set<Identity> Layout::erasureUnique(const Identity &identity,
                                                   bool station,
                                                   bool archive,
                                                   bool variable) const
{ return backend->erasureUnique(identity, station, archive, variable); }

Layout::DatabaseAccess Layout::erasureRead(const Identity &identity, const std::string &where) const
{ return backend->erasureRead(identity, where); }

Layout::DatabaseAccess Layout::erasureWrite(const Identity &identity, WriteMode mode) const
{ return backend->erasureWrite(identity, mode); }

Layout::DatabaseAccess Layout::erasureDelete(const Identity &identity,
                                             const std::string &where) const
{ return backend->erasureDelete(identity, where); }

Layout::DatabaseAccess Layout::erasureSize(const Identity &identity, const std::string &where) const
{ return backend->erasureSize(identity, where); }

Layout::DatabaseAccess Layout::erasureMinimum(const Identity &identity) const
{ return backend->erasureMinimum(identity); }

Layout::DatabaseAccess Layout::erasureIndex(const Identity &identity,
                                            const std::string &where) const
{ return backend->erasureIndex(identity, where); }

std::string Layout::modifiedInsertSelect(Layout &origin) const
{
    if (dynamic_cast<const Backend_V1 *>(backend.get())) {
        if (dynamic_cast<const Backend_V1 *>(origin.backend.get()))
            return Column_Modified;
        if (dynamic_cast<const Backend_V2 *>(origin.backend.get()))
            return std::string("( ( ") + Column_Modified + " ) / 1000.0)";
    } else if (dynamic_cast<const Backend_V2 *>(backend.get())) {
        if (dynamic_cast<const Backend_V2 *>(origin.backend.get()))
            return Column_Modified;
        /* Lack of a portable ceil, so just add one msec, since it's approximate
         * anyway */
        if (dynamic_cast<const Backend_V1 *>(origin.backend.get()))
            return std::string("( ( ") + Column_Modified + " ) * 1000 + 1)";
    }
    Q_ASSERT(false);
    return Column_Modified;
}

}
}
}
}