/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREARCHIVE_GLOBALOPERATION_HXX
#define CPD3DATACOREARCHIVE_GLOBALOPERATION_HXX

#include "core/first.hxx"

#include <memory>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <mutex>
#include <condition_variable>

#include "datacore/datacore.hxx"
#include "core/threading.hxx"
#include "datacore/archive/structure.hxx"
#include "datacore/archive/stringindex.hxx"

namespace CPD3 {
namespace Data {
namespace Archive {

/**
 * The base class for global operations on the archive.  These are operations that
 * access and optionally modify the entire archive.
 */
class CPD3DATACORE_EXPORT GlobalOperation {
    Structure::Layout &layout;

    enum {
        State_Initialize,
        State_Running,
        State_TerminateRequested,
        State_Terminating,
        State_Finishing,
        State_Complete
    } state;

    std::mutex mutex;
    std::condition_variable notify;
    std::thread thread;

    Threading::PoolRun createRun;
    Threading::PoolRun processRun;

protected:
    class BlockOperation;

    /**
     * The base class for an active read on the archive index.
     */
    class IndexRead {
        friend class GlobalOperation;

        friend class BlockOperation;

        GlobalOperation &operation;
        Structure::Identity identity;

        enum {
            State_Initialize, State_ReadingIndex, State_IndexPaused, State_Complete,
        } state;

        typedef std::unique_ptr<BlockOperation> BlockPointer;

        Database::Context indexContext;
        Structure::Layout::DatabaseAccess blockRead;

        std::unordered_map<BlockOperation *, BlockPointer> blocks;

        void start();

        bool process(std::unique_lock<std::mutex> &lock, bool &complete);

        void signalTerminate();

        bool shouldAdvanceIndex();


        void indexResult(const Database::Types::Results &results);

        void indexComplete();

        void completeBlock(BlockOperation *block);

    protected:
        /**
         * Create the index read.
         *
         * @param operation the parent operation
         * @param identity  the base identity
         * @param context   the index reading context
         * @param blockRead the accessor to read a single block data
         */
        IndexRead(GlobalOperation &operation,
                  const Structure::Identity &identity,
                  Database::Context &&context,
                  Structure::Layout::DatabaseAccess &&blockRead);

        /**
         * Create a block operation from an index result.  This is called once for
         * every index result.  A block will not be deleted until is calls its
         * complete() method and an index will not be deleted until all blocks
         * have been deleted.
         * <br>
         * This is executed from the database thread so it may not access the
         * database directly itself.
         *
         * @param block     the block index
         * @param modified  the modified time
         * @param identity  the identity
         * @return          a new block operation or null if none
         */
        virtual BlockOperation *createBlock(Structure::Block block,
                                            double modified,
                                            const Structure::Identity &identity) = 0;

        /**
         * Get the standard where clause for a block read.
         *
         * @return  the where clause
         */
        static std::string whereBlockRead();

        /**
         * Get the layout of the database.
         *
         * @return  the database layout
         */
        inline const Structure::Layout &getLayout() const
        { return operation.layout; }

    public:
        virtual ~IndexRead();
    };

    friend class IndexRead;


    /**
     * Create the index read for a given identity.
     *
     * @param identity  the base index identity
     * @return          the sequence reader or null
     */
    virtual IndexRead *createSequence(const Structure::Identity &identity) = 0;

    /**
     * Create the index read for a given identity.
     *
     * @param identity  the base index identity
     * @return          the sequence reader or null
     */
    virtual IndexRead *createErasure(const Structure::Identity &identity) = 0;

    /**
     * The base class for operations created from the index itself.
     */
    class BlockOperation {
        friend class IndexRead;

        IndexRead &index;
        GlobalOperation &operation;
        Structure::Block block;
        Structure::Identity identity;

        enum {
            State_Initialize, State_CreateContext, State_ReadData, State_Process, State_Complete,
        } state;

        Database::Context readDataContext;

        void start();

        void createContext();


        void readDataResult(const Database::Types::Results &results);

        void readDataComplete();

    protected:
        /**
         * Create the block operation.
         *
         * @param index     the parent index read
         * @param block     the block index
         * @param identity  the identity
         */
        BlockOperation(IndexRead &index,
                       Structure::Block block,
                       const Structure::Identity &identity);

        /**
         * Process the data from the archive.
         *
         * @param data  the data
         */
        virtual void processData(const Util::ByteArray &data) = 0;

        /**
         * Get the layout of the database.
         *
         * @return  the database layout
         */
        inline const Structure::Layout &getLayout() const
        { return operation.layout; }

    public:
        virtual ~BlockOperation();

        /**
         * Notify the global operation that the block operation has completed.
         * <br>
         * The block will be deleted by this call, so it should only be used as the
         * final operation of a threaded entry point.
         */
        void completed();
    };

    friend class BlockOperation;

    /**
     * The base class for block operations that involve a write back to the database
     * of modified data.
     */
    class BlockRewrite : public BlockOperation {
        Structure::Block block;
        double modified;
        Structure::Identity identity;

        Database::Context writeDataContext;

    protected:
        /**
         * Create the block rewrite operation.
         *
         * @param index     the parent index read
         * @param block     the block index
         * @param modified  the original modified time
         * @param identity  the identity
         */
        BlockRewrite(IndexRead &index,
                     Structure::Block block,
                     double modified,
                     const Structure::Identity &identity);

        void processData(const Util::ByteArray &data) override;

        /**
         * Create the write context for data.
         * <br>
         * This is called from a pooled thread, so database access and blocking
         * are allowed.
         *
         * @param data      the input data
         * @param block     the block index
         * @param modified  the original modification time
         * @param identity  the block identity
         * @return          a database context
         */
        virtual Database::Context createWrite(const Util::ByteArray &data,
                                              Structure::Block block,
                                              double modified,
                                              const Structure::Identity &identity) = 0;

    public:
        virtual ~BlockRewrite();
    };


    /**
     * Create the global operation.
     *
     * @param layout    the archive layout definition
     */
    GlobalOperation(Structure::Layout &layout);

    /**
     * Shutdown the operation and destroy the active thread.  Provided so
     * child class destructors can make sure that they finish cleanup.
     */
    void shutdownDestroy();

    /**
     * Get the layout of the database.
     *
     * @return  the database layout
     */
    inline const Structure::Layout &getLayout() const
    { return layout; }

private:
    typedef std::unique_ptr<IndexRead> IndexPointer;

    std::list<IndexPointer> indices;

    void run();

    void terminateIndices();

    bool processIndices(std::unique_lock<std::mutex> &lock);

public:

    virtual ~GlobalOperation();

    /**
     * Abort the global operation.
     */
    void signalTerminate();

    /**
     * Start the global operation.
     */
    void start();

    /**
     * Wait for the global operation to complete.
     *
     * @param timeout   the maximum time to wait
     * @return          true if the operation has completed
     */
    bool wait(double timeout = FP::undefined());

    /**
     * Test if the global operation has completed.
     *
     * @return          true if the operation has completed
     */
    bool isComplete();

    /**
     * A signal generated from the thread once it has completed work.
     */
    Threading::Signal<> complete;
};


/**
 * A global archive operation that determines which values in the index lookups are
 * no longer referenced by anything.  It effectively reads the entire archive and
 * removes any integer indices it sees from the available set.
 * <br>
 * This is generally combined with a subsequent modify of the indices to remove
 * the unused components.
 */
class CPD3DATACORE_EXPORT FindUnusedIndexOperation final : public GlobalOperation {
public:
    struct IndexContents {
        typedef std::unordered_set<StringIndex::index> Component;

        Component station;
        Component archive;
        Component variable;
        Component flavor;
    };

    typedef std::unordered_set<Structure::Identity> EmptyIndices;

private:
    IndexContents remaining;

    std::mutex resultMutex;

    EmptyIndices absentSequences;
    EmptyIndices absentErasures;

    class IndexSequence final : public IndexRead {
        FindUnusedIndexOperation &operation;
        Structure::Identity identity;
        bool haveSeenBlocks;

        static Database::Context createIndexContext(FindUnusedIndexOperation &operation,
                                                    const Structure::Identity &identity);

        static Structure::Layout::DatabaseAccess createBlockRead(FindUnusedIndexOperation &operation,
                                                                 const Structure::Identity &identity);

    public:
        IndexSequence(FindUnusedIndexOperation &operation, const Structure::Identity &identity);

        virtual ~IndexSequence();

    protected:
        BlockOperation *createBlock(Structure::Block block,
                                            double modified,
                                            const Structure::Identity &identity) override;
    };

    virtual IndexRead *createSequence(const Structure::Identity &identity);

    friend class IndexSequence;

    class SequenceBlock final : public BlockOperation {
        FindUnusedIndexOperation &operation;
        Structure::Identity identity;
    public:
        SequenceBlock(IndexSequence &index,
                      FindUnusedIndexOperation &operation,
                      Structure::Block block,
                      const Structure::Identity &identity);

        virtual ~SequenceBlock();

    protected:
        void processData(const Util::ByteArray &data) override;
    };


    class IndexErasure final : public IndexRead {
        FindUnusedIndexOperation &operation;
        Structure::Identity identity;
        bool haveSeenBlocks;

        static Database::Context createIndexContext(FindUnusedIndexOperation &operation,
                                                    const Structure::Identity &identity);

        static Structure::Layout::DatabaseAccess createBlockRead(FindUnusedIndexOperation &operation,
                                                                 const Structure::Identity &identity);

    public:
        IndexErasure(FindUnusedIndexOperation &operation, const Structure::Identity &identity);

        virtual ~IndexErasure();

    protected:
        BlockOperation *createBlock(Structure::Block block,
                                            double modified,
                                            const Structure::Identity &identity) override;
    };

    virtual IndexRead *createErasure(const Structure::Identity &identity);

    friend class IndexErasure;

    class ErasureBlock final : public BlockOperation {
        FindUnusedIndexOperation &operation;
        Structure::Identity identity;
    public:
        ErasureBlock(IndexErasure &index,
                     FindUnusedIndexOperation &operation,
                     Structure::Block block,
                     const Structure::Identity &identity);

        virtual ~ErasureBlock();

    protected:
        void processData(const Util::ByteArray &data) override;
    };

    friend class SequenceBlock;

public:
    /**
     * Create the unused location operation.
     *
     * @param layout        the database layout
     * @param contents      the existing contents to remove from
     */
    FindUnusedIndexOperation(Structure::Layout &layout, const IndexContents &contents);

    virtual ~FindUnusedIndexOperation();

    /**
     * Get the remaining contents in the index after the scan.
     *
     * @return  the remaining unused contents
     */
    IndexContents remainingContents() const;

    /**
     * Get the list of empty index entries for sequences.
     *
     * @return  the empty sequence entries
     */
    EmptyIndices emptySequences() const;

    /**
     * Get the list of empty index entries for erasures.
     *
     * @return  the empty erasure entries
     */
    EmptyIndices emptyErasures() const;
};


/**
 * A global archive operation that decompresses then recompresses at a higher
 * compression any blocks with a modified time in the specified range.
 * <br>
 * This is generally run periodically to recompress any blocks that have not
 * been modified in a long time.
 */
class CPD3DATACORE_EXPORT RecompressOperation final : public GlobalOperation {
    double modifiedLower;
    double modifiedUpper;

    class IndexSequence final : public IndexRead {
        static Database::Context createIndexContext(RecompressOperation &operation,
                                                    const Structure::Identity &identity,
                                                    double modifiedLower,
                                                    double modifiedUpper);

        static Structure::Layout::DatabaseAccess createBlockRead(RecompressOperation &operation,
                                                                 const Structure::Identity &identity);

    public:
        IndexSequence(RecompressOperation &operation, const Structure::Identity &identity);

        virtual ~IndexSequence();

    protected:
        BlockOperation *createBlock(Structure::Block block,
                                            double modified,
                                            const Structure::Identity &identity) override;
    };

    virtual IndexRead *createSequence(const Structure::Identity &identity);

    friend class IndexSequence;

    class SequenceBlock final : public BlockRewrite {
    public:
        SequenceBlock(IndexRead &index,
                      Structure::Block block,
                      double modified,
                      const Structure::Identity &identity);

        virtual ~SequenceBlock();

    protected:
        Database::Context createWrite(const Util::ByteArray &data,
                                              Structure::Block block,
                                              double modified,
                                              const Structure::Identity &identity) override;
    };

    class IndexErasure final : public IndexRead {
        static Database::Context createIndexContext(RecompressOperation &operation,
                                                    const Structure::Identity &identity,
                                                    double modifiedLower,
                                                    double modifiedUpper);

        static Structure::Layout::DatabaseAccess createBlockRead(RecompressOperation &operation,
                                                                 const Structure::Identity &identity);

    public:
        IndexErasure(RecompressOperation &operation, const Structure::Identity &identity);

        virtual ~IndexErasure();

    protected:
        BlockOperation *createBlock(Structure::Block block,
                                            double modified,
                                            const Structure::Identity &identity) override;
    };

    virtual IndexRead *createErasure(const Structure::Identity &identity);

    friend class IndexErasure;

    class ErasureBlock final : public BlockRewrite {
    public:
        ErasureBlock(IndexRead &index,
                     Structure::Block block,
                     double modified,
                     const Structure::Identity &identity);

        virtual ~ErasureBlock();

    protected:
        Database::Context createWrite(const Util::ByteArray &data,
                                              Structure::Block block,
                                              double modified,
                                              const Structure::Identity &identity) override;
    };

public:
    /**
     * Create the recompression operation.
     *
     * @param layout        the database layout
     * @param modifiedLower the lower (start) modifed time
     * @param modifiedUpper the upper (end) modified time
     */
    RecompressOperation(Structure::Layout &layout,
                        double modifiedLower = FP::undefined(),
                        double modifiedUpper = FP::undefined());

    virtual ~RecompressOperation();
};

}
}
}

#endif //CPD3DATACOREARCHIVE_GLOBALOPERATION_HXX
