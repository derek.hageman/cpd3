/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREARCHIVE_STRINGINDEX_HXX
#define CPD3DATACOREARCHIVE_STRINGINDEX_HXX

#include "core/first.hxx"

#include <cstdint>
#include <mutex>
#include <unordered_map>
#include <string>
#include <list>
#include <functional>
#include <utility>

#include "datacore/datacore.hxx"

#include "database/connection.hxx"

namespace CPD3 {
namespace Data {
namespace Archive {

/**
 * A single string index handler.  String indices convert the numeric
 * identifiers found in the archive into the strings used in the
 * actual sequence names.
 */
class CPD3DATACORE_EXPORT StringIndex {
public:
    typedef std::uint_fast16_t index;
private:
    CPD3::Database::Connection *connection;
    std::string table;
    CPD3::Database::Statement lookupValue;
    CPD3::Database::Statement lookupIndex;

    std::mutex mutex;
    std::unordered_map<index, std::string> fromIndex;
    std::unordered_map<std::string, index> toIndex;
    bool haveReadAll;

    void readAll();

public:
    StringIndex() = delete;

    StringIndex(const StringIndex &) = delete;

    StringIndex &operator=(const StringIndex &) = delete;

    /**
     * Create the string index.
     *
     * @param connection    the database connection
     * @param table         the table name of the index
     */
    StringIndex(Database::Connection *connection, const std::string &table);

    /**
     * Get the numeric index value for the given string value
     *
     * @param value         the string value
     * @return              the numeric index or zero on failure
     */
    index lookup(const std::string &value, bool failNotFound = true);

    /**
     * Get the string value for a numeric index.
     *
     * @param id            the numeric index
     * @return              the string value or empty on failure
     */
    std::string lookup(index id, bool failNotFound = true);

    /**
     * Insert the given string value, if required, and return its numeric index.
     *
     * @param value         the string value
     * @return              the numeric index or zero on failure
     */
    index insert(const std::string &value);

    /**
     * Lookup possible index to string mappings.
     *
     * @param filter        the filter operation, if any
     * @return              the matched index to string mappings
     */
    std::vector<std::pair<index, std::string>> lookup(const std::function<
            bool(const std::string &)> &filter = std::function<bool(const std::string &)>());

    /**
     * Remove a specific index identifier.
     *
     * @param id            the identifier to remove
     */
    void remove(index id);
};


/**
 * The definition of all string indices in the archive.
 */
struct CPD3DATACORE_EXPORT IdentityIndex {
    /**
     * The station index.
     */
    StringIndex station;

    /**
     * The archive index.
     */
    StringIndex archive;

    /**
     * The variable index.
     */
    StringIndex variable;

    /**
     * The flavors index.
     */
    StringIndex flavor;

    /**
     * Create the index.
     *
     * @param connection    the archive connection
     */
    IdentityIndex(Database::Connection *connection);

    IdentityIndex() = delete;

    IdentityIndex(const IdentityIndex &) = delete;

    IdentityIndex &operator=(const IdentityIndex &) = delete;

    /**
     * Create all indices in the archive.
     *
     * @param connection    the archive connection
     */
    static void initialize(Database::Connection *connection);
};

}
}
}

#endif //CPD3DATACOREARCHIVE_STRINGINDEX_HXX
