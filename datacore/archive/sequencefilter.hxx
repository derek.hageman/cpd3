/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREARCHIVE_SEQUENCEFILTER_HXX
#define CPD3DATACOREARCHIVE_SEQUENCEFILTER_HXX

#include "core/first.hxx"

#include <unordered_map>
#include <unordered_set>
#include <cstdint>
#include <mutex>
#include <memory>

#include "datacore/datacore.hxx"
#include "structure.hxx"
#include "stringindex.hxx"
#include "selection.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"

namespace CPD3 {
namespace Data {
namespace Archive {

namespace SequenceFilter {

struct Input;

/**
 * The block level filter for the archive.
 */
class CPD3DATACORE_EXPORT Block {
    double firstPossibleStart;
    double lastPossibleEnd;
    double firstPossibleModified;

    std::mutex mutex;
    std::unique_ptr<SequenceMatch::Basic> flavorsMatch;
    std::unordered_map<SequenceName::Component, bool> flavorsHit;

    struct Selection {
        double start;
        double end;
        double modifiedAfter;
        std::unique_ptr<SequenceMatch::Basic> match;

        Selection();

        Selection(const Selection &other);

        Selection &operator=(const Selection &other);

        Selection(Selection &&);

        Selection &operator=(Selection &&);

        Selection(Structure::Layout &layout, const Archive::Selection::List &selections);

        bool acceptModified(double modified) const;

        bool acceptTimes(const SequenceName &name, double start, double end) const;
    };

    std::vector<std::unique_ptr<Selection>> selections;

    typedef std::unordered_map<SequenceName, std::vector<Selection *> > Dispatch;
    Dispatch dispatch;

    Dispatch::const_iterator lookup(const SequenceName &name);

    friend struct Input;

    Block();

public:
    Block(const Block &) = delete;

    Block &operator=(const Block &) = delete;


    /**
     * The first possible modified time that any value must have at least.
     *
     * @return  the first possible modified time
     */
    double modifiedAfter() const;

    /**
     * Test if a full flavors definition string can possibly pass the filter.
     *
     * @param flavors   the unsplit flavors string
     * @return          true if the flavors may be accepted
     */
    bool flavors(const SequenceName::Component &flavors);

    /**
     * Test if the given bounds can possibly pass the filter.
     *
     * @param start     the value start
     * @param end       the value end
     * @return          true if the bounds can possibly pass the filter
     */
    bool bounds(double start, double end) const;

    /**
     * Perform the final full evaluation of the filter.
     *
     * @param start     the value start
     * @param end       the value end
     * @param modified  the value modified time
     * @param name      the value sequence name
     * @return          true if the value passes the filter
     */
    bool final(double start, double end, double modified, const SequenceName &name);

    /**
     * Perform the final full evaluation of the filter.
     *
     * @param start     the value start
     * @param end       the value end
     * @param modifiedPtr  the pointer to the data to read the modified time from, if required
     * @param name      the value sequence name
     * @return          true if the value passes the filter
     */
    bool final(double start, double end, const uchar *modifiedPtr, const SequenceName &name);

    /**
     * Advance the filter, meaning that no data with start times before the new
     * advance time are possible anymore.
     *
     * @param time      the time to advance to
     */
    void advance(double time);
};


/**
 * The stream (index) level filtering for the archive.
 */
class CPD3DATACORE_EXPORT Stream {
    Structure::Layout &layout;

    Structure::Block firstPossibleBlock;
    Structure::Block lastPossibleBlock;
    double firstPossibleModified;

    std::unordered_set<Structure::Identity::index> stations;
    std::unordered_set<Structure::Identity::index> archives;
    std::unordered_set<Structure::Identity::index> variables;

    std::mutex mutex;

    struct Selection {
        Structure::Block firstBlock;
        Structure::Block lastBlock;
        double modifiedAfter;
        std::unique_ptr<SequenceMatch::Basic> match;

        Selection();

        Selection(const Selection &other);

        Selection &operator=(const Selection &other);

        Selection(Selection &&);

        Selection &operator=(Selection &&);

        Selection(Structure::Layout &layout, const Archive::Selection::List &selections);


        bool acceptBlock(Structure::Block block) const;

        bool acceptModified(double modified) const;

        bool acceptTimes(const SequenceName &name, double start, double end) const;
    };

    std::vector<std::unique_ptr<Selection>> selections;

    typedef std::unordered_map<Structure::Identity, std::vector<Selection *> > Dispatch;
    Dispatch dispatch;

    Dispatch::const_iterator lookup(const Structure::Identity &identity, const SequenceName &name);

    friend struct Input;

    Stream(Structure::Layout &layout);

public:
    Stream() = delete;

    Stream(const Stream &) = delete;

    Stream &operator=(const Stream &) = delete;


    /**
     * Get the where clause for the index read.
     *
     * @return  the where clause of the database read
     */
    std::string buildIndexWhere() const;

    /**
     * Merge the binds required for the index read.
     *
     * @param binds the output binds
     */
    void mergeIndexBinds(Database::Types::Binds &binds) const;


    /**
     * Get the first possible block to inspect.
     *
     * @return  the first possible block
     */
    Structure::Block firstBlock() const;

    /**
     * Get the last possible block to inspect.
     *
     * @return  the last possible block
     */
    Structure::Block lastBlock() const;

    /**
     * Get the first possible modified time.  All blocks must be modified after
     * this threshold.
     *
     * @return  the first possible modified time
     */
    double modifiedAfter() const;

    /**
     * Test if a block index passes the filter.
     * <br>
     * Currently disabled and always returns true (filtered by the index query).
     *
     * @param block the block index
     * @return      true if the block index passes
     */
    bool block(Structure::Block block) const;

    /**
     * Test if a stream identity passes the filter.
     * <br>
     * Currently disabled and always returns true (filtered by the index query).
     *
     * @param identity  the stream identity
     * @return          true if the identity passes the filter
     */
    bool identity(const Structure::Identity &identity) const;

    /**
     * Perform final filtering on the stream.
     *
     * @param block     the block index
     * @param modified  the index level modified time
     * @param identity  the block identity
     * @param name      the sequence name of the block (without flavors only)
     * @return          true if the block passes the filter
     */
    bool final(Structure::Block block,
               double modified,
               const Structure::Identity &identity,
               const SequenceName &name);

    /**
     * Advance the filter, meaning that no data with start times before the new
     * advance time are possible anymore.
     *
     * @param time      the time to advance to
     */
    void advance(double time);

};


/**
 * The input dispatched to the Forge loopback interface.
 */
struct CPD3DATACORE_EXPORT ForgeLoopbackInput {
    /**
     * The program to invoke.
     */
    std::string program;

    /**
     * The selections to dispatch to the program.
     */
    Selection::List selections;
};


/**
 * A single input definition.  Read operations expect one or more
 * inputs to be combined together into the final output data.
 */
struct CPD3DATACORE_EXPORT Input {
    /**
     * The identity as far as it can be specified at the input level.
     */
    Structure::Identity identity;

    /**
     * The name as far as it can be specified at the input level.
     */
    SequenceName name;

    /**
     * The stream (index) level filter.
     */
    Stream stream;

    /**
     * The block level filter.
     */
    Block block;

    Input() = delete;

    Input(const Input &) = delete;

    Input &operator=(const Input &) = delete;

    /**
     * Advance the input, meaning that no data with start times before the new
     * advance time are possible anymore.
     *
     * @param time      the time to advance to
     */
    void advance(double time);


    /**
     * Create the inputs to read sequence values.
     *
     * @param layout        the archive layout definition
     * @param index         the archive index
     * @param selections    the selections to read
     * @return              the inputs to read from
     */
    static std::vector<std::unique_ptr<Input>> sequence(Structure::Layout &layout,
                                                        IdentityIndex &index,
                                                        const Selection::List &selections,
                                                        std::unique_ptr<
                                                                ForgeLoopbackInput> *forgeLoopback = nullptr);

    /**
     * Create the inputs to read erasure values.
     *
     * @param layout        the archive layout definition
     * @param index         the archive index
     * @param selections    the selections to read
     * @return              the inputs to read from
     */
    static std::vector<std::unique_ptr<Input>> erasure(Structure::Layout &layout,
                                                       IdentityIndex &index,
                                                       const Selection::List &selections);

private:
    static std::vector<std::unique_ptr<Input>> generate(Structure::Layout &layout,
                                                        IdentityIndex &index,
                                                        const Selection::List &selections,
                                                        const std::unordered_set<
                                                                Structure::Identity> &possible,
                                                        std::unique_ptr<
                                                                ForgeLoopbackInput> *forgeLoopback = nullptr);

    Input(Structure::Layout &layout, const Structure::Identity &identity, const SequenceName &name);
};


}
}
}
}

#endif //CPD3DATACOREARCHIVE_SEQUENCEFILTER_HXX
