/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <limits>
#include <QLoggingCategory>

#include "stringindex.hxx"
#include "initialization.hxx"
#include "structure.hxx"
#include "database/connection.hxx"
#include "core/qtcompat.hxx"


Q_LOGGING_CATEGORY(log_datacore_archive_stringindex, "cpd3.datacore.archive.stringindex",
                   QtWarningMsg)

using namespace CPD3::Database;

namespace CPD3 {
namespace Data {
namespace Archive {

/* Some databases don't support unsigned integers (looking at you PostgreSQL),
 * so limit the values we'll generate to signed ones */
static constexpr StringIndex::index maximumIndex = (!std::numeric_limits<Structure::Identity::strict>::is_signed) ?
                                                   (std::numeric_limits<Structure::Identity::strict>::max()>>1) :
                                                   std::numeric_limits<Structure::Identity::strict>::max();

StringIndex::StringIndex(Database::Connection *c, const std::string &t) : connection(c),
                                                                          table(t),
                                                                          lookupValue(
                                                                                  connection->select(
                                                                                          table,
                                                                                          {"value"},
                                                                                          "id = :id")),
                                                                          lookupIndex(
                                                                                  connection->select(
                                                                                          table,
                                                                                          {"id"},
                                                                                          "value = :value")),
                                                                          mutex(),
                                                                          fromIndex(),
                                                                          toIndex(),
                                                                          haveReadAll(false)
{ }

static StringIndex::index indexFromVariant(const Database::Types::Variant &var)
{
    if (var.isEmpty())
        return 0;
    Q_ASSERT(!var.isNull());
    return static_cast<StringIndex::index>(var.toInteger());
}

static Database::Types::Variant bindingFromString(const std::string &value)
{
    return Database::Types::Variant(value);
}

StringIndex::index StringIndex::lookup(const std::string &value, bool failNotFound)
{
    std::lock_guard<std::mutex> lock(mutex);
    {
        auto check = toIndex.find(value);
        if (check != toIndex.end())
            return check->second;
    }
    index i = indexFromVariant(
            lookupIndex.single({{"value", bindingFromString(value)}}, failNotFound));
    if (i == 0) {
        qCDebug(log_datacore_archive_stringindex) << "Lookup failed for value" << value;
        return 0;
    }
    fromIndex.emplace(i, value);
    toIndex.emplace(value, i);
    return i;
}

std::string StringIndex::lookup(StringIndex::index id, bool failNotFound)
{
    std::lock_guard<std::mutex> lock(mutex);
    {
        auto check = fromIndex.find(id);
        if (check != fromIndex.end())
            return check->second;
    }
    auto v = lookupValue.single({{"id", static_cast<std::int_fast64_t>(id)}}, failNotFound);
    if (!v.isSet()) {
        qCDebug(log_datacore_archive_stringindex) << "Lookup failed for id" << id;
        return {};
    }
    Q_ASSERT(!v.isNull());
    std::string str = v.takeString();
    fromIndex.emplace(id, str);
    toIndex.emplace(str, id);
    return str;
}

static StringIndex::index findAvailableOrdered(Connection *connection, const std::string &table)
{
    auto result = connection->select(table, {"MIN(id)", "MAX(id)"}).extract();
    if (result.empty())
        return 0;
    if (!result[0].isSet())
        return 1;
    StringIndex::index min = indexFromVariant(result[0]);
    StringIndex::index max = indexFromVariant(result[1]);
    if (min == 0 || max == 0 || max < min)
        return 0;
    if (min > 1)
        return min - 1;
    if (max < maximumIndex - 1)
        return max + 1;
    return 0;
}

static StringIndex::index findAvailableSequential(Statement &read)
{
    Types::Binds binds;
    auto bid = binds.emplace("id", 2).first;
    for (StringIndex::index i = 2; i < maximumIndex; ++i) {
        bid->second = static_cast<std::int_fast64_t>(i);
        auto v = read.single(binds);
        if (v.isSet())
            continue;
        return i;
    }
    return 0;
}

StringIndex::index StringIndex::insert(const std::string &value)
{
    std::lock_guard<std::mutex> lock(mutex);
    {
        auto check = toIndex.find(value);
        if (check != toIndex.end())
            return check->second;
    }
    {
        auto v = lookupIndex.single({{"value", bindingFromString(value)}});
        if (!v.isEmpty()) {
            Q_ASSERT(!v.isNull());
            auto i = static_cast<index>(v.toInteger());
            if (i != 0) {
                fromIndex.emplace(i, value);
                toIndex.emplace(value, i);
                return i;
            }
        }
    }

    index id = findAvailableOrdered(connection, table);
    if (id == 0)
        id = findAvailableSequential(lookupValue);
    if (id == 0) {
        qCWarning(log_datacore_archive_stringindex) << "Index insertion failure for " << value;
        return 0;
    }

    connection->insert(table, {"id", "value"})
              .synchronous({{"id",    static_cast<std::int_fast64_t>(id)},
                            {"value", bindingFromString(value)}});

    fromIndex.emplace(id, value);
    toIndex.emplace(value, id);
    return id;
}

std::vector<std::pair<StringIndex::index, std::string>> StringIndex::lookup(const std::function<
        bool(const std::string &)> &filter)
{
    std::lock_guard<std::mutex> lock(mutex);
    readAll();

    std::vector<std::pair<StringIndex::index, std::string>> result;
    for (const auto &add : fromIndex) {
        if (filter) {
            if (!filter(add.second))
                continue;
        }
        result.emplace_back(add.first, add.second);
    }
    return result;
}

void StringIndex::readAll()
{
    if (haveReadAll)
        return;
    haveReadAll = true;
    auto ctx = connection->select(table, {"id", "value"}).execute({}, true);
    ctx.result.connect([&](const Types::Results &values) {
        index index = indexFromVariant(values[0]);
        const auto &rValue = values[1].toString();
        fromIndex.emplace(index, rValue);
        toIndex.emplace(rValue, index);
        ctx.next();
    });
    ctx.begin();
    ctx.wait();
}

void StringIndex::remove(StringIndex::index id)
{
    std::lock_guard<std::mutex> lock(mutex);
    {
        auto check = fromIndex.find(id);
        if (check != fromIndex.end()) {
            auto other = toIndex.find(check->second);
            Q_ASSERT(other != toIndex.end());
            toIndex.erase(other);
            fromIndex.erase(check);
        }
    }

    connection->del(table, "id = :id").synchronous({{"id", static_cast<std::int_fast64_t>(id)}});
}

IdentityIndex::IdentityIndex(Database::Connection *connection) : station(connection,
                                                                         "CPD3Stations"),
                                                                 archive(connection,
                                                                         "CPD3Archives"),
                                                                 variable(connection,
                                                                          "CPD3Variables"),
                                                                 flavor(connection, "CPD3Flavors")
{ }

void IdentityIndex::initialize(Database::Connection *connection)
{
    Initialization::createStringIndexTable(connection, "CPD3Stations");
    Initialization::createStringIndexTable(connection, "CPD3Archives");
    Initialization::createStringIndexTable(connection, "CPD3Variables");
    Initialization::createStringIndexTable(connection, "CPD3Flavors");
}

}
}
}
