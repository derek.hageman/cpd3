/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREARCHIVE_INITIALIZATION_HXX
#define CPD3DATACOREARCHIVE_INITIALIZATION_HXX

#include "core/first.hxx"

#include <cstdint>

#include "datacore/datacore.hxx"
#include "structure.hxx"

namespace CPD3 {

namespace Database {
class Connection;
}

namespace Data {
namespace Archive {


namespace Initialization {


/**
 * Create the table containing the sequence data for the specified identity.
 *
 * @param connection    the database connection
 * @param layout        the archive layout
 * @param identity      the identity to create
 */
CPD3DATACORE_EXPORT void createSequenceTable(Database::Connection *connection,
                                             const Structure::Layout &layout,
                                             const Structure::Identity &identity);

/**
 * Create the table containing the erasure data for the specified identity.
 *
 * @param connection    the database connection
 * @param layout        the archive layout
 * @param identity      the identity to create
 */
CPD3DATACORE_EXPORT void createErasureTable(Database::Connection *connection,
                                            const Structure::Layout &layout,
                                            const Structure::Identity &identity);

/**
 * Create a string index table.
 *
 * @param connection    the database connection
 * @param name          the name of the index
 */
CPD3DATACORE_EXPORT void createStringIndexTable(Database::Connection *connection,
                                                const std::string &name);

}

}
}
}

#endif //CPD3DATACOREARCHIVE_INITIALIZATION_HXX
