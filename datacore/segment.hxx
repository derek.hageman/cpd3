/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACORESEGMENTREAD_H
#define CPD3DATACORESEGMENTREAD_H

#include "core/first.hxx"

#include <vector>

#include <limits.h>
#include <QtGlobal>
#include <QString>
#include <QHash>
#include <QList>
#include <QMetaType>
#include <QSet>
#include <QRegExp>
#include <QDebug>

#include "core/number.hxx"
#include "core/range.hxx"
#include "core/stream.hxx"
#include "core/util.hxx"
#include "variant/root.hxx"
#include "datacore/stream.hxx"
#include "archive/selection.hxx"

namespace CPD3 {
namespace Data {

namespace Archive {
class Access;
}

class ValueSegment;

class SequenceSegment;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const ValueSegment &value);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, ValueSegment &value);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const ValueSegment &v);

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const SequenceSegment &value);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, SequenceSegment &value);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const SequenceSegment &v);

CPD3DATACORE_EXPORT uint qHash(const ValueSegment &key);

/**
 * A single segment of merged data.  This is a single value of a station,
 * archive, and variable set that was merged from any overlapping components
 * of it.
 */
class CPD3DATACORE_EXPORT ValueSegment {
    double start;
    double end;
    Variant::Root contents;
public:
    typedef std::vector<ValueSegment> Transfer;

    ValueSegment();

    ValueSegment(const ValueSegment &);

    ValueSegment &operator=(const ValueSegment &);

    ValueSegment(ValueSegment &&);

    ValueSegment &operator=(ValueSegment &&);

    ValueSegment(double start, double end, const Variant::Root &value);

    ValueSegment(double start, double end, Variant::Root &&value);

    explicit ValueSegment(const Variant::Root &value);

    explicit ValueSegment(Variant::Root &&value);

    static ValueSegment::Transfer withPath(const ValueSegment::Transfer &input,
                                           const QString &path);

    /**
     * Get the start time.
     * @return the start time
     */
    inline double getStart() const
    { return start; }

    /**
     * Set the start time.
     */
    inline void setStart(double set)
    { start = set; }

    /**
     * Get the end time.
     * @return the end time
     */
    inline double getEnd() const
    { return end; }

    /**
     * Set the end time.
     */
    inline void setEnd(double set)
    { end = set; }

    /**
     * Get the value.
     * @return the value
     */
    inline Variant::Root &root()
    { return contents; }

    /**
     * Get the value.
     * @return the value
     */
    inline const Variant::Root &root() const
    { return contents; }

    /**
     * Get the value.
     * @return the value
     */
    inline Variant::Read getValue() const
    { return contents.read(); }

    /** @see getValue() const */
    inline Variant::Read value() const
    { return contents.read(); }

    /** @see getValue() const */
    inline Variant::Read read() const
    { return contents.read(); }

    inline Variant::Read operator[](const std::string &str) const
    { return contents[str]; }

    inline Variant::Read operator[](std::string &&str) const
    { return contents[std::move(str)]; }

    inline Variant::Read operator[](const QString &str) const
    { return contents[str]; }

    inline Variant::Read operator[](const char *str) const
    { return contents[str]; }

    inline Variant::Write operator[](const std::string &str)
    { return contents[str]; }

    inline Variant::Write operator[](std::string &&str)
    { return contents[std::move(str)]; }

    inline Variant::Write operator[](const QString &str)
    { return contents[str]; }

    inline Variant::Write operator[](const char *str)
    { return contents[str]; }

    /**
     * Get the value.
     * @return the value
     */
    inline Variant::Write write()
    { return contents.write(); }


    inline void setRoot(const Variant::Root &value)
    { contents = value; }

    inline void setRoot(Variant::Root &&value)
    { contents = std::move(value); }

    static ValueSegment::Transfer merge(const ValueSegment::Transfer &segments,
                                        const QList<QString> &targetPaths = QList<QString>());

    friend CPD3DATACORE_EXPORT uint qHash(const ValueSegment &key);

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const ValueSegment &value);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, ValueSegment &value);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const ValueSegment &v);

    class Stream;
};

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                            const ValueSegment::Stream &reader);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, ValueSegment::Stream &reader);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const ValueSegment::Stream &reader);

/**
 * A data segment that can resolve multiple units.
 */
class CPD3DATACORE_EXPORT SequenceSegment {
    class CPD3DATACORE_EXPORT OverlayData {
        std::int_fast32_t priority;
        double start;
        double end;
        bool defaultStation;
        Variant::Root value;
    public:
        explicit OverlayData(const SequenceValue &value);

        explicit OverlayData(SequenceValue &&value);

        OverlayData(const SequenceIdentity &identity, const Variant::Root &value);

        OverlayData(const SequenceIdentity &identity, Variant::Root &&value);

        OverlayData(const SequenceName &identity, Variant::Root &&value);

        OverlayData() = default;

        OverlayData(const OverlayData &) = default;

        OverlayData &operator=(const OverlayData &) = default;

        OverlayData(OverlayData &&) = default;

        OverlayData &operator=(OverlayData &&) = default;

        inline Variant::Read read() const
        { return value.read(); }

        inline const Variant::Root &root() const
        { return value; }

        inline Variant::Root &root()
        { return value; }

        inline operator const Variant::Root &() const
        { return value; }

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline bool isDefaultStation() const
        { return defaultStation; }

        std::int_fast32_t getPriority() const
        { return priority; }

        struct CPD3DATACORE_EXPORT OrderOverlay {
            bool operator()(const OverlayData &a, const OverlayData &b) const;

            bool operator()(const OverlayData &a, const SequenceValue &b) const;

            bool operator()(const SequenceValue &a, const OverlayData &b) const;

            bool operator()(const SequenceValue &a, const SequenceValue &b) const;
        };

        friend QDataStream &operator<<(QDataStream &stream, const OverlayData &value);

        friend QDataStream &operator>>(QDataStream &stream, OverlayData &value);
    };

    friend QDataStream &operator<<(QDataStream &stream, const OverlayData &value);

    friend QDataStream &operator>>(QDataStream &stream, OverlayData &value);

    double start;
    double end;
    SequenceName::Map<Variant::Root> values;
    SequenceName::Map<std::vector<Variant::Root>> inputs;

    SequenceSegment(double setStart,
                    double setEnd,
                    const SequenceName::Map<std::vector<OverlayData>> &setInputs);

    SequenceSegment(double setStart,
                    double setEnd,
                    SequenceName::Map<std::vector<OverlayData>> &&setInputs);

    friend class ValueSegment::Stream;

    Variant::Root &lookupValue(const SequenceName &key);

public:
    typedef std::vector<SequenceSegment> Transfer;

    SequenceSegment();

    SequenceSegment(const SequenceSegment &);

    SequenceSegment &operator=(const SequenceSegment &);

    SequenceSegment(SequenceSegment &&);

    SequenceSegment &operator=(SequenceSegment &&);

    SequenceSegment(double start, double end,
                     const SequenceName::Map<std::vector<SequenceValue>> &setInputs);

    SequenceSegment(double start,
                    double end,
                    SequenceName::Map<std::vector<SequenceValue>> &&setInputs);

    /**
     * Get the start time.
     * @return the start time
     */
    inline double getStart() const
    { return start; }

    /**
     * Set the start time.
     */
    inline void setStart(double set)
    { start = set; }

    /**
     * Get the end time.
     * @return the end time
     */
    inline double getEnd() const
    { return end; }

    /**
     * Set the end time.
     */
    inline void setEnd(double set)
    { end = set; }

    /**
     * Test if a value possibly exists.
     *
     * @param key   the key
     * @return      true if the value has any possible value
     */
    bool exists(const SequenceName &key) const;

    /**
     * Get the value of a given lookup.  This is explicitly non-const since
     * otherwise changing the Value (which is allowed, even if it returned
     * a const one, due to explict sharing) would not be reflected in the
     * segment.
     * 
     * @param key   the key to lookup
     * @return      the Value of the key
     */
    Variant::Write getValue(const SequenceName &key);

    /**
     * Get the value of a given lookup.  This does not return a modifable
     * value.
     *
     * @param key   the key to lookup
     * @return      the Value of the key
     */
    Variant::Read getValue(const SequenceName &key) const;

    /**
     * Create a data value associated with the given key's value.
     * 
     * @param key   the key to lookup
     * @return      a SequenceValue for that key
     */
    inline SequenceValue getSequenceValue(const SequenceName &key)
    { return SequenceValue(key, Variant::Root(getValue(key)), start, end); }

    /**
     * Create a data value associated with the given key's value.
     *
     * @param key   the key to lookup
     * @return      a SequenceValue for that key
     */
    inline SequenceValue getSequenceValue(const SequenceName &key) const
    { return SequenceValue(key, Variant::Root(getValue(key)), start, end); }

    /**
     * Get the value of a given lookup.  
     * 
     * @param station   the station to lookup
     * @param archive   the archive in the station
     * @param variable  the variable in the archive
     * @param flavors   the flavors of the variable
     * @return          the Value of the key
     * @see getValue( const SequenceName & )
     */
    inline Variant::Write getValue(const SequenceName::Component &station,
                                   const SequenceName::Component &archive,
                                   const SequenceName::Component &variable,
                                   const SequenceName::Flavors &flavors = {})
    { return getValue(SequenceName(station, archive, variable, flavors)); }

    /**
     * Get the value of a given lookup.
     *
     * @param station   the station to lookup
     * @param archive   the archive in the station
     * @param variable  the variable in the archive
     * @param flavors   the flavors of the variable
     * @return          the Value of the key
     * @see getValue( const SequenceName & ) const
     */
    inline Variant::Read getValue(const SequenceName::Component &station,
                                  const SequenceName::Component &archive,
                                  const SequenceName::Component &variable,
                                  const SequenceName::Flavors &flavors = {}) const
    { return getValue(SequenceName(station, archive, variable, flavors)); }

    /**
     * Get a value matched by the given regular expressions.
     * 
     * @param station       the station to match
     * @param archive       the archive to match
     * @param variable      the variable to match
     * @param hasFlavors    the flavors to require
     * @param lacksFlavors  the flavors to reject
     * @param matchedUnit   the output unit matched if non-null
     * @return              the Value of the key
     */
    Variant::Write getValue(const QRegExp &station,
                            const QRegExp &archive,
                            const QRegExp &variable,
                            const QList<QRegExp> &hasFlavors = QList<QRegExp>(),
                            const QList<QRegExp> &lacksFlavors = QList<QRegExp>(),
                            SequenceName *matchedUnit = NULL);

    /**
     * Get the value of a given lookup.  
     * 
     * @param key   the key to lookup
     * @return          the Value of the key
     * @see getValue( const SequenceName & )
     */
    inline Variant::Write value(const SequenceName &key)
    { return getValue(key); }

    /**
     * Get the value of a given lookup.
     *
     * @param key   the key to lookup
     * @return          the Value of the key
     * @see getValue( const SequenceName & ) const
     */
    inline Variant::Read value(const SequenceName &key) const
    { return getValue(key); }

    /** @see getValue( const SequenceName & ) */
    inline Variant::Write operator[](const SequenceName &key)
    { return getValue(key); }

    /** @see getValue( const SequenceName & ) const */
    inline Variant::Read operator[](const SequenceName &key) const
    { return getValue(key); }

    /**
     * Assign the given value to the given key.
     * 
     * @param key   the key to set
     * @param v     the value to assign.
     */
    inline void setValue(const SequenceName &key, const Variant::Root &v)
    { values[key] = v; }

    inline void setValue(const SequenceName &key, Variant::Root &&v)
    { values[key] = std::move(v); }

    inline void setValue(SequenceName &&key, Variant::Root &&v)
    { values[std::move(key)] = std::move(v); }

    /**
     * Move a value out of the segment.  This renders any access to the
     * key through that value invalid.
     *
     * @param key   the key to read
     * @return      the moved value root
     */
    Variant::Root takeValue(const SequenceName &key);

    /**
     * Clear the value set for a key, if any.
     *
     * @param key   the key to clear
     */
    inline void clearValue(const SequenceName &key)
    {
        values.erase(key);
        inputs.erase(key);
    }

    QSet<SequenceName> getUnits(bool includeDefault = false) const;

    /**
     * Overlay one segment on another.  This discards any existing ordering information,
     * so the overlay strictly replaces anything not present in the existing one.
     *
     * @param over  the segment to overlay
     */
    void overlay(const SequenceSegment &over);

    void overlay(SequenceSegment &&over);

    static ValueSegment::Transfer reduce(const SequenceSegment::Transfer &input);

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const SequenceSegment &value);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, SequenceSegment &value);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const SequenceSegment &v);

    class Stream;

    friend class Stream;
};


CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                            const SequenceSegment::Stream &reader);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, SequenceSegment::Stream &reader);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const SequenceSegment::Stream &reader);

/**
 * A segmenting data value reader.  It breaks input data into a set of
 * non-overlapping segments of values for all variables that exist within the
 * span of that segment.  When using directly the input values must
 * be sorted in start time ascending order.  Further ordering is imposed
 * by comparing them (so they do not need to be sorted by priority).  To use
 * sequentially add each value with the add(const SequenceValue &) function, using
 * any output it returns as new segments, at the end of the data stream use
 * finish() to get any remaining segments.  The stream can be reset midway
 * with clear().
 * <br>
 * Additional static functions are provided that interface with the standard
 * data archive to simplify reading.  They read in all values matching the
 * criteria and return the result.
 */
class CPD3DATACORE_EXPORT SequenceSegment::Stream {

    typedef SequenceSegment::OverlayData BackendData;

    class CPD3DATACORE_EXPORT Backend : public SegmentStream<SequenceSegment, Backend,
                                                             SequenceSegment::Transfer> {
    public:
        struct CPD3DATACORE_EXPORT ValueMoveData {
            const SequenceName &name;
            SequenceSegment::OverlayData add;

            inline ValueMoveData(const SequenceName &name, BackendData &&add) : name(name),
                                                                                add(std::move(add))
            { }

            inline double getStart() const
            { return add.getStart(); }

            inline double getEnd() const
            { return add.getEnd(); }
        };

        struct CPD3DATACORE_EXPORT FullMoveData {
            SequenceName &name;
            SequenceSegment::OverlayData add;

            inline FullMoveData(SequenceName &name, BackendData &&add) : name(name),
                                                                         add(std::move(add))
            { }

            inline double getStart() const
            { return add.getStart(); }

            inline double getEnd() const
            { return add.getEnd(); }
        };

    private:
        SequenceName::Map<std::vector<BackendData>> values;
        SequenceName::Map<std::vector<BackendData>> injected;
    public:
        Backend();

        Backend(const Backend &);

        Backend &operator=(const Backend &);

        Backend(Backend &&);

        Backend &operator=(Backend &&);

        void serialize(QDataStream &stream) const;

        void deserialize(QDataStream &stream);

        SequenceSegment convertActive(double start, double end) const;

        bool purgeBefore(double before);

        void addActive(const SequenceValue &value);

        void addInjected(const SequenceValue &value);

        void addActive(SequenceValue &&value);

        void addInjected(SequenceValue &&value);

        void addActive(ValueMoveData &&value);

        void addInjected(ValueMoveData &&value);

        void addActive(FullMoveData &&value);

        void addInjected(FullMoveData &&value);

        void clearActive();

        void mergeAll(const Backend &other);
    };

    Backend backend;
public:
    Stream();

    Stream(const Stream &);

    Stream &operator=(const Stream &);

    Stream(Stream &&);

    Stream &operator=(Stream &&);

    /**
     * Add a new value to the segmenting reader.  This function must be called
     * with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     * 
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer add(const SequenceValue &value)
    { return backend.add(value); }

    /**
     * Add a new value to the segmenting reader that doesn't cause fragmenting.  This function
     * must be called with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer inject(const SequenceValue &value)
    { return backend.inject(value); }

    /**
     * Add a new value to the segmenting reader.  This function must be called
     * with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer add(SequenceValue &&value)
    { return backend.add(std::move(value)); }

    /**
     * Add a new value to the segmenting reader that doesn't cause fragmenting.  This function
     * must be called with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer inject(SequenceValue &&value)
    { return backend.inject(std::move(value)); }

    /**
     * Add new values to the segmenting reader.  This function must be called
     * with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     * 
     * @param values    the values to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer add(const SequenceValue::Transfer &values)
    {
        SequenceSegment::Transfer result;
        for (const auto &v : values) {
            Util::append(add(v), result);
        }
        return result;
    }

    /**
     * Add new values to the segmenting reader that doesn't cause fragmenting.  This function
     * must be called with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the values to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer inject(const SequenceValue::Transfer &values)
    {
        SequenceSegment::Transfer result;
        for (const auto &v : values) {
            Util::append(inject(v), result);
        }
        return result;
    }

    /**
     * Add new values to the segmenting reader.  This function must be called
     * with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer add(SequenceValue::Transfer &&values)
    {
        SequenceSegment::Transfer result;
        for (auto &v : values) {
            Util::append(add(std::move(v)), result);
        }
        return result;
    }

    /**
     * Add new values to the segmenting reader that doesn't cause fragmenting.  This function
     * must be called with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer inject(SequenceValue::Transfer &&values)
    {
        SequenceSegment::Transfer result;
        for (const auto &v : values) {
            Util::append(inject(std::move(v)), result);
        }
        return result;
    }

    /**
     * Add a new value to the segmenting reader.  This function must be called
     * with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer add(const SequenceIdentity &identity,
                                         const Variant::Root &value)
    {
        return backend.add(Backend::ValueMoveData(identity.getName(),
                                                  SequenceSegment::OverlayData(identity, value)));
    }

    /**
     * Add a new value to the segmenting reader that doesn't cause fragmenting.  This function
     * must be called with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer inject(const SequenceIdentity &identity,
                                            const Variant::Root &value)
    {
        return backend.inject(Backend::ValueMoveData(identity.getName(),
                                                     SequenceSegment::OverlayData(identity,
                                                                                  value)));
    }

    /**
     * Add a new value to the segmenting reader.  This function must be called
     * with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer add(const SequenceIdentity &identity, Variant::Root &&value)
    {
        return backend.add(Backend::ValueMoveData(identity.getName(),
                                                  SequenceSegment::OverlayData(identity,
                                                                               std::move(value))));
    }

    /**
     * Add a new value to the segmenting reader that doesn't cause fragmenting.  This function
     * must be called with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer inject(const SequenceIdentity &identity, Variant::Root &&value)
    {
        return backend.inject(Backend::ValueMoveData(identity.getName(),
                                                     SequenceSegment::OverlayData(identity,
                                                                                  std::move(
                                                                                           value))));
    }

    /**
     * Add a new value to the segmenting reader.  This function must be called
     * with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer add(SequenceIdentity &&identity, Variant::Root &&value)
    {
        SequenceSegment::OverlayData od(identity, std::move(value));
        return backend.add(Backend::FullMoveData(identity.getName(), std::move(od)));
    }

    /**
     * Add a new value to the segmenting reader that doesn't cause fragmenting.  This function
     * must be called with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer inject(SequenceIdentity &&identity, Variant::Root &&value)
    {
        SequenceSegment::OverlayData od(identity, std::move(value));
        return backend.inject(Backend::FullMoveData(identity.getName(), std::move(od)));
    }

    /**
     * Finish the stream of data values.  This function returns all remaining
     * segments and sets the reader to the default state, ready to accept a new
     * stream of values.
     * 
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer finish()
    { return backend.finish(); }

    /**
     * Resets the reader to the default state, ready to start a new stream of 
     * values.  Any pending completed values are discarded.
     */
    inline void clear()
    { backend.reset(); }

    /**
     * Advance the stream until the given time.  This will emit any segments 
     * that would be completed by that time.  Note that this may produce 
     * overlapping segments with later data if a subsequent one would 
     * fragment the current state.
     * 
     * @param start     the start time to advance until
     * @return          a list of partial segments, which may be empty
     */
    inline SequenceSegment::Transfer advance(double start)
    { return backend.advancePartial(start); }

    /**
     * Advance the stream until the given time.  This will emit any segments 
     * that would be completed by that time.  This will not produce overlapping 
     * segments so no further fragmentation can occur.
     * <br>
     * This also provides an indicator for if the this advance clears all data 
     * out so no further fragmentation can occur.
     * 
     * @param start     the start time to advance until
     * @param futurePending if not NULL then set to true if there are segments in the future that are delaying emission
     * @return          a list of completed segments, which may be empty
     */
    inline SequenceSegment::Transfer completedAdvance(double start, bool *futurePending = NULL)
    {
        auto result = backend.advanceCompleted(start);
        if (futurePending != NULL) *futurePending = backend.hasFutureSegments();
        return result;
    }


    /**
     * Get the intermediate result that might be the next value, if 
     * subsequent input is strictly advancing after the end of it.  This is 
     * useful for realtime displays (for example) that may need to update the 
     * latest even if it might change later.
     * 
     * @param valid if not NULL then set to true if the result is valid
     * @return      the intermediate next value
     */
    inline SequenceSegment intermediateNext(bool *valid = NULL) const
    { return backend.intermediate(valid); }

    /**
     * Get the time of next transition.
     * 
     * @return the next value's start time or undefined if unavailable
     */
    inline double nextTransitionTime() const
    { return backend.getNextTransition(); }

    /**
     * Overlay the given reader on top of this one.  The current reader is 
     * changed so that it incorporates the state of the other one as it had 
     * been reading all the inputs in it.
     * 
     * @param other     the reader to overlay on this one
     */
    inline void overlay(const Stream &other)
    { backend.overlayStream(other.backend); }

    /**
     * Overlay the given value on top of this one.  The current reader is 
     * changed so that it incorporates the given value without producing any 
     * new output.  If the value represents behind of the current time of the 
     * reader then any output it would have generated is discarded.
     * 
     * @param value     the value to overlay on this one
     */
    inline void overlay(const SequenceValue &value)
    { backend.overlaySingle(value); }

    /**
     * Set the reader break points.  Break points are times when the segment is
     * broken into two or more components even if there was no data at that time
     * that would normally cause fragmentation.
     * 
     * @param breaks    the list of breaks to use
     */
    template<typename Container>
    inline void setBreaks(const Container &breaks)
    { backend.insertBreaks(breaks); }

    /**
     * Add a break time to the segment reader.  Break points are times when 
     * the segment is broken into two or more components even if there was no 
     * data at that time that would normally cause fragmentation.
     * 
     * @param time  the time of the break
     */
    inline void addBreak(double time)
    { backend.insertBreak(time); }

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const SequenceSegment::Stream &reader);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       SequenceSegment::Stream &reader);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream,
                                                 const SequenceSegment::Stream &reader);

    static SequenceSegment::Transfer read(const Archive::Selection &selection,
                                          Archive::Access *reader = NULL);

    static SequenceSegment::Transfer read(const Archive::Selection::List &select,
                                          Archive::Access *reader = NULL);

    static void maintainValues(std::vector<SequenceValue> &values, const SequenceValue &incoming);

    static void maintainValues(std::vector<SequenceValue> &values, SequenceValue &&incoming);

    static Variant::Root overlayValues(const std::vector<SequenceValue> &values);
};

/**
 * This is similar to SequenceSegment::Stream except that is assumes all values are flat.
 * That is, it does not consider multiple stacks of values and everything is
 * merged into a single data value.
 * <br>
 * This is useful when an external dispatch has already isolated values
 * into a single stream.
 */
class CPD3DATACORE_EXPORT ValueSegment::Stream {

    typedef SequenceSegment::OverlayData BackendData;

    class CPD3DATACORE_EXPORT Backend : public SegmentStream<ValueSegment, Backend,
                                                             ValueSegment::Transfer> {
        std::vector<BackendData> values;
        std::vector<BackendData> injected;
    public:
        Backend();

        Backend(const Backend &);

        Backend &operator=(const Backend &);

        Backend(Backend &&);

        Backend &operator=(Backend &&);

        void serialize(QDataStream &stream) const;

        void deserialize(QDataStream &stream);

        ValueSegment convertActive(double start, double end) const;

        bool purgeBefore(double before);

        void addActive(const SequenceValue &value);

        void addInjected(const SequenceValue &value);

        void addActive(SequenceValue &&value);

        void addInjected(SequenceValue &&value);

        void clearActive();

        void mergeAll(const Backend &other);
    };

    Backend backend;
public:
    Stream();

    Stream(const Stream &);

    Stream &operator=(const Stream &);

    Stream(Stream &&);

    Stream &operator=(Stream &&);

    /**
     * Add a new value to the segmenting reader.  This function must be called
     * with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     * 
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline ValueSegment::Transfer add(const SequenceValue &value)
    { return backend.add(value); }

    /**
     * Add a new value to the segmenting reader that doesn't cause fragmenting.  This function
     * must be called with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline ValueSegment::Transfer inject(const SequenceValue &value)
    { return backend.inject(value); }

    /**
     * Add a new value to the segmenting reader.  This function must be called
     * with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline ValueSegment::Transfer add(SequenceValue &&value)
    { return backend.add(std::move(value)); }

    /**
     * Add a new value to the segmenting reader that doesn't cause fragmenting.  This function
     * must be called with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline ValueSegment::Transfer inject(SequenceValue &&value)
    { return backend.inject(std::move(value)); }

    /**
     * Add new values to the segmenting reader.  This function must be called
     * with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     * 
     * @param values    the values to add
     * @return          a list of completed segments, which may be empty
     */
    inline ValueSegment::Transfer add(const SequenceValue::Transfer &values)
    {
        ValueSegment::Transfer result;
        for (const auto &v : values) {
            Util::append(add(v), result);
        }
        return result;
    }

    /**
     * Add new values to the segmenting reader that doesn't cause fragmenting.  This function
     * must be called with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the values to add
     * @return          a list of completed segments, which may be empty
     */
    inline ValueSegment::Transfer inject(const SequenceValue::Transfer &values)
    {
        ValueSegment::Transfer result;
        for (const auto &v : values) {
            Util::append(inject(v), result);
        }
        return result;
    }

    /**
     * Add new values to the segmenting reader.  This function must be called
     * with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline ValueSegment::Transfer add(SequenceValue::Transfer &&values)
    {
        ValueSegment::Transfer result;
        for (auto &v : values) {
            Util::append(add(std::move(v)), result);
        }
        return result;
    }

    /**
     * Add new values to the segmenting reader that doesn't cause fragmenting.  This function
     * must be called with all values in start time ascending order.  It returns any completed
     * segments that can no longer be affected by further values.
     *
     * @param value     the value to add
     * @return          a list of completed segments, which may be empty
     */
    inline ValueSegment::Transfer inject(SequenceValue::Transfer &&values)
    {
        ValueSegment::Transfer result;
        for (const auto &v : values) {
            Util::append(inject(std::move(v)), result);
        }
        return result;
    }

    /**
     * Finish the stream of data values.  This function returns all remaining
     * segments and sets the reader to the default state, ready to accept a new
     * stream of values.
     * 
     * @return          a list of completed segments, which may be empty
     */
    inline ValueSegment::Transfer finish()
    { return backend.finish(); }

    /**
     * Resets the reader to the default state, ready to start a new stream of 
     * values.  Any pending completed values are discarded.
     */
    inline void clear()
    { backend.reset(); }

    /**
     * Advance the stream until the given time.  This will emit any segments 
     * that would be completed by that time.  Note that this may produce 
     * overlapping segments with later data if a subsequent one would 
     * fragment the current state.
     * 
     * @param start     the start time to advance until
     * @return          a list of partial segments, which may be empty
     */
    inline ValueSegment::Transfer advance(double start)
    { return backend.advancePartial(start); }

    /**
     * Advance the stream until the given time.  This will emit any segments 
     * that would be completed by that time.  This will not produce overlapping 
     * segments so no further fragmentation can occur.
     * <br>
     * This also provides an indicator for if the this advance clears all data 
     * out so no further fragmentation can occur.
     * 
     * @param start     the start time to advance until
     * @param futurePending if not NULL then set to true if there are segments in the future that are delaying emission
     * @return          a list of completed segments, which may be empty
     */
    inline ValueSegment::Transfer completedAdvance(double start, bool *futurePending = NULL)
    {
        auto result = backend.advanceCompleted(start);
        if (futurePending != NULL) *futurePending = backend.hasFutureSegments();
        return result;
    }


    /**
     * Get the intermediate result that might be the next value, if 
     * subsequent input is strictly advancing after the end of it.  This is 
     * useful for realtime displays (for example) that may need to update the 
     * latest even if it might change later.
     * 
     * @param valid if not NULL then set to true if the result is valid
     * @return      the intermediate next value
     */
    inline ValueSegment intermediateNext(bool *valid = NULL) const
    { return backend.intermediate(valid); }

    /**
     * Get the time of next transition.
     * 
     * @return the next value's start time or undefined if unavailable
     */
    inline double nextTransitionTime() const
    { return backend.getNextTransition(); }

    /**
     * Overlay the given reader on top of this one.  The current reader is 
     * changed so that it incorporates the state of the other one as it had 
     * been reading all the inputs in it.
     * 
     * @param other     the reader to overlay on this one
     */
    inline void overlay(const Stream &other)
    { backend.overlayStream(other.backend); }

    /**
     * Overlay the given value on top of this one.  The current reader is 
     * changed so that it incorporates the given value without producing any 
     * new output.  If the value represents behind of the current time of the 
     * reader then any output it would have generated is discarded.
     * 
     * @param value     the value to overlay on this one
     */
    inline void overlay(const SequenceValue &value)
    { backend.overlaySingle(value); }

    /**
     * Set the reader break points.  Break points are times when the segment is
     * broken into two or more components even if there was no data at that time
     * that would normally cause fragmentation.
     * 
     * @param breaks    the list of breaks to use
     */
    template<typename Container>
    inline void setBreaks(const Container &breaks)
    { backend.insertBreaks(breaks); }

    /**
     * Add a break time to the segment reader.  Break points are times when 
     * the segment is broken into two or more components even if there was no 
     * data at that time that would normally cause fragmentation.
     * 
     * @param time  the time of the break
     */
    inline void addBreak(double time)
    { backend.insertBreak(time); }

    /**
     * Initialize the reader from the given set of values.
     * 
     * @param other the values to initialize from
     */
    inline void assign(const std::vector<SequenceValue> &other)
    { backend.initializeFrom(other); }

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const ValueSegment::Stream &reader);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       ValueSegment::Stream &reader);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const ValueSegment::Stream &reader);

    static ValueSegment::Transfer read(const Archive::Selection &selection,
                                       Archive::Access *reader = NULL);
};


}
}

Q_DECLARE_METATYPE(CPD3::Data::SequenceSegment::Stream);

Q_DECLARE_METATYPE(CPD3::Data::ValueSegment::Stream);

Q_DECLARE_METATYPE(CPD3::Data::ValueSegment);

Q_DECLARE_METATYPE(CPD3::Data::SequenceSegment);

Q_DECLARE_METATYPE(CPD3::Data::ValueSegment::Transfer);

Q_DECLARE_METATYPE(CPD3::Data::SequenceSegment::Transfer);

#endif
