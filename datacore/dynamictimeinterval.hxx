/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACORE_DYNAMICTIMEINTERVAL_HXX
#define CPD3DATACORE_DYNAMICTIMEINTERVAL_HXX

#include "core/first.hxx"

#include <memory>
#include <deque>
#include <QtGlobal>
#include <QList>
#include <QString>
#include <QDataStream>
#include <QDebug>

#include "datacore.hxx"
#include "segment.hxx"
#include "valueoptionparse.hxx"
#include "core/timeutils.hxx"
#include "core/component.hxx"

namespace CPD3 {
namespace Data {

class DynamicTimeInterval;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const DynamicTimeInterval *tis);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, DynamicTimeInterval *&tis);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const DynamicTimeInterval *tis);

/**
 * Defines a means of selecting an interval in time to operate on.
 */
class CPD3DATACORE_EXPORT DynamicTimeInterval {
public:
    class Undefined;

    class None;

    class Constant;

    class Variable;

    virtual ~DynamicTimeInterval();

    /**
     * Apply the interval offset to a given time.  This will advance the time, 
     * so no requests before this time may be made after the call.
     * 
     * @param time      the time to inspect for
     * @param origin    the time to offset around
     * @param roundUp   true to round up instead of down
     * @param multiplier the multiplier to the offset
     * @return          the value at the given time
     * @see Time::logical( double, Time::LogicalTimeUnit, int, bool, bool, int )
     */
    virtual double apply(double time, double origin, bool roundUp = false, int multiplier = 1) = 0;

    /**
     * Apply the interval offset to a given time.
     * 
     * @param time      the time to inspect for
     * @param origin    the time to offset around
     * @param roundUp   true to round up instead of down
     * @param multiplier the multiplier to the offset
     * @return          the value at the given time
     * @see Time::logical( double, Time::LogicalTimeUnit, int, bool, bool, int )
     */
    virtual double
            applyConst(double time, double origin, bool roundUp = false, int multiplier = 1) const =
            0;

    /**
     * Round the given origin at the given time.
     * 
     * @param time      the time to inspect for
     * @param origin    the time to offset around
     * @param roundUp   true to round up instead of down
     * @return          the value at the given time
     * @see Time::floor( double, Time::LogicalTimeUnit, int )
     * @see Time::ceil( double, Time::LogicalTimeUnit, int )
     */
    virtual double round(double time, double origin, bool roundUp = false);

    /**
     * Round the given origin at the given time.
     * 
     * @param time      the time to inspect for
     * @param origin    the time to offset around
     * @param roundUp   true to round up instead of down
     * @return          the value at the given time
     * @see Time::floor( double, Time::LogicalTimeUnit, int )
     * @see Time::ceil( double, Time::LogicalTimeUnit, int )
     */
    virtual double roundConst(double time, double origin, bool roundUp = false) const;

    /**
     * Create a copy of this interval selection.
     */
    virtual DynamicTimeInterval *clone() const = 0;

    /**
     * Get all known times when the "structure" of this selection might change.
     * Generally this should return times when the outside description of it
     * would need to be updated.
     * 
     * @return  a list of known changed times
     */
    virtual QSet<double> getChangedPoints() const;

    /**
     * Returns a string representation of this offset at the given time.
     * 
     * @param time  the time to describe
     * @return      a string
     */
    virtual QString describe(double time) const;

    /**
     * Get the constant offset of the option, if available.  This is used
     * by option description tools to output the values for examples.
     * <br>
     * By default this does nothing and returns false.
     * 
     * @param unit      the output unit or NULL
     * @param count     the output count or NULL
     * @param aligned   the output aligned state or NULL
     * @return          true if the description is valid
     */
    virtual bool constantOffsetDescription
            (Time::LogicalTimeUnit *unit = NULL, int *count = NULL, bool *aligned = NULL) const;

    /**
     * Create a interval selection from a configuration.
     * 
     * @param config    the configuration
     * @param unit      the unit within the configuration
     * @param path      the path within the configuration
     * @param start     the start time to trim the output to
     * @param end       the end time to trim the output to
     * @param skipUndefined if set then undefined configuration segments are left as-is
     * @param baseline  the baseline configuration, if any, to use
     */
    static DynamicTimeInterval *fromConfiguration(SequenceSegment::Transfer &config,
                                                  const SequenceName &unit,
                                                  const QString &path = QString(),
                                                  double start = FP::undefined(),
                                                  double end = FP::undefined(),
                                                  bool skipUndefined = false,
                                                  Variable *baseline = nullptr);

    /**
     * Create a interval selection from a configuration.
     * 
     * @param config    the configuration
     * @param path      the path within the configuration
     * @param start     the start time to trim the output to
     * @param end       the end time to trim the output to
     * @param skipUndefined if set then undefined configuration segments are left as-is
     * @param baseline  the baseline configuration, if any, to use
     */
    static DynamicTimeInterval *fromConfiguration(const ValueSegment::Transfer &config,
                                                  const QString &path = QString(),
                                                  double start = FP::undefined(),
                                                  double end = FP::undefined(),
                                                  bool skipUndefined = false,
                                                  Variable *baseline = nullptr);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const DynamicTimeInterval *tis);

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicTimeInterval *tis);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       DynamicTimeInterval *&tis);

protected:
    virtual void serialize(QDataStream &stream) const = 0;

    virtual void printLog(QDebug &stream) const;
};

class CPD3DATACORE_EXPORT DynamicTimeInterval::Undefined : public DynamicTimeInterval {
public:
    Undefined();

    virtual ~Undefined();

    virtual double apply(double time, double origin, bool roundUp = false, int multiplier = 1);

    virtual double
            applyConst(double time, double origin, bool roundUp = false, int multiplier = 1) const;

    virtual double round(double time, double origin, bool roundUp = false);

    virtual double roundConst(double time, double origin, bool roundUp = false) const;

    virtual DynamicTimeInterval *clone() const;

    virtual QString describe(double time) const;

    virtual bool constantOffsetDescription
            (Time::LogicalTimeUnit *unit = NULL, int *count = NULL, bool *aligned = NULL) const;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicTimeInterval *tis);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       DynamicTimeInterval *&tis);

protected:
    explicit Undefined(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

class CPD3DATACORE_EXPORT DynamicTimeInterval::None : public DynamicTimeInterval {
public:
    None();

    virtual ~None();

    virtual double apply(double time, double origin, bool roundUp = false, int multiplier = 1);

    virtual double
            applyConst(double time, double origin, bool roundUp = false, int multiplier = 1) const;

    virtual double round(double time, double origin, bool roundUp = false);

    virtual double roundConst(double time, double origin, bool roundUp = false) const;

    virtual DynamicTimeInterval *clone() const;

    virtual QString describe(double time) const;

    virtual bool constantOffsetDescription
            (Time::LogicalTimeUnit *unit = NULL, int *count = NULL, bool *aligned = NULL) const;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicTimeInterval *tis);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       DynamicTimeInterval *&tis);

protected:
    explicit None(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

class CPD3DATACORE_EXPORT DynamicTimeInterval::Constant : public DynamicTimeInterval {
    Time::LogicalTimeUnit unit;
    int count;
    bool aligned;

public:
    Constant() = delete;

    virtual ~Constant();

    /**
     * Create a new constant time interval.
     *
     * @param setUnit       the unit to use
     * @param setCount      the count to use
     * @param setAligned    true if aligning
     * @see Time::logical( double, LogicalTimeUnit, int, bool, bool, int )
     */
    Constant
            (Time::LogicalTimeUnit setUnit, int setCount, bool setAligned = false);

    virtual double apply(double time, double origin, bool roundUp = false, int multiplier = 1);

    virtual double
            applyConst(double time, double origin, bool roundUp = false, int multiplier = 1) const;

    virtual double round(double time, double origin, bool roundUp = false);

    virtual double roundConst(double time, double origin, bool roundUp = false) const;

    virtual DynamicTimeInterval *clone() const;

    virtual QString describe(double time) const;

    virtual bool constantOffsetDescription
            (Time::LogicalTimeUnit *unit = NULL, int *count = NULL, bool *aligned = NULL) const;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicTimeInterval *tis);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       DynamicTimeInterval *&tis);

protected:
    explicit Constant(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

class TimeIntervalSelectionOption;

class CPD3DATACORE_EXPORT DynamicTimeInterval::Variable : public DynamicTimeInterval {
    class UnitSetAligned {
    public:
        double start;
        double end;
        bool set;

        UnitSetAligned(double setStart, double setEnd, bool setSet);

        UnitSetAligned(const UnitSetAligned &other);

        UnitSetAligned &operator=(const UnitSetAligned &u);

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }
    };

    class UnitSetUndefined {
    public:
        double start;
        double end;
        bool set;

        UnitSetUndefined(double setStart, double setEnd, bool setSet);

        UnitSetUndefined(const UnitSetUndefined &other);

        UnitSetUndefined &operator=(const UnitSetUndefined &u);

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }
    };

    class Unit {
    public:
        double start;
        double end;
        Time::LogicalTimeUnit unit;
        int count;
        bool aligned;
        bool undefined;

        Unit();

        Unit(double setStart,
             double setEnd,
             Time::LogicalTimeUnit setUnit,
             int setCount,
             bool setAligned = false,
             bool setUndefined = false);

        Unit(const Unit &);

        Unit &operator=(const Unit &);

        Unit(Unit &&);

        Unit &operator=(Unit &&);

        Unit(const Unit &other, double setStart, double setEnd);

        Unit(const UnitSetAligned &other, double setStart, double setEnd);

        Unit(const UnitSetUndefined &other, double setStart, double setEnd);

        Unit(const Unit &under, const Unit &over, double setStart, double setEnd);

        Unit(const Unit &under, const UnitSetAligned &over, double setStart, double setEnd);

        Unit(const Unit &under, const UnitSetUndefined &over, double setStart, double setEnd);

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double s)
        { start = s; }

        inline void setEnd(double s)
        { end = s; }
    };

    std::deque<Unit> units;

public:

    Variable();

    virtual ~Variable();

    Variable(const DynamicTimeInterval::Variable &);

    DynamicTimeInterval::Variable &operator=(const DynamicTimeInterval::Variable &);

    Variable(DynamicTimeInterval::Variable &&);

    DynamicTimeInterval::Variable &operator=(DynamicTimeInterval::Variable &&);

    /**
     * Clear all offset segments.
     */
    void clear();

    /**
     * Set the time interval to be a constant offset.
     *
     * @param setUnit       the unit to use
     * @param setCount      the count to use
     * @param setAligned    true if aligning
     * @see Time::logical( double, LogicalTimeUnit, int, bool, bool, int )
     */
    void set(Time::LogicalTimeUnit setUnit, int setCount, bool setAligned = false);

    /**
     * Set the interval to the given parameters within the given time range
     *
     * @param start         the effective start time
     * @param end           the effective end time
     * @param setUnit       the unit to use
     * @param setCount      the count to use
     * @param setAligned    true if aligning
     */
    void set(double start, double end, Time::LogicalTimeUnit setUnit, int setCount, bool setAligned = false);

    /**
     * Set the alignment state within the given time range.
     *
     * @param start         the effective start time
     * @param end           the effective end time
     * @param aligned       the alignment state to set
     */
    void setAligned(double start, double end, bool aligned);

    /**
     * Set the interval to that specified by the given Value within the given
     * range.
     *
     * @param start         the effective start time
     * @param end           the effective end time
     * @param v             the value to configure from
     */
    void set(double start, double end, const Variant::Read &v);

    /**
     * Set the interval to that specified by the given Value.
     *
     * @param v             the value to configure from
     */
    void set(const Variant::Read &v);

    /**
     * Set the the interval to be undefined within the given time range.
     *
     * @param start         the effective start time
     * @param end           the effective end time
     * @param undefined     the undefined state
     */
    void setUndefined(double start = FP::undefined(), double end = FP::undefined(), bool undefined = true);

    /**
     * Reduce the given selection range to a copy within the given range.  This
     * may simply it to a different type of selection if the configuration is
     * always active within the range.
     *
     * @param icr       the input configuration range
     * @param start     the minimum possible start time
     * @param end       the maximum possible end time
     * @return          a new time interval selection
     */
    static DynamicTimeInterval *reduceConfigurationSegments(const DynamicTimeInterval::Variable &icr,
                                                            double start,
                                                            double end);

    virtual double apply(double time, double origin, bool roundUp = false, int multiplier = 1);

    virtual double
            applyConst(double time, double origin, bool roundUp = false, int multiplier = 1) const;

    virtual double round(double time, double origin, bool roundUp = false);

    virtual double roundConst(double time, double origin, bool roundUp = false) const;

    virtual DynamicTimeInterval *clone() const;

    virtual QString describe(double time) const;

    virtual bool constantOffsetDescription
            (Time::LogicalTimeUnit *unit = NULL, int *count = NULL, bool *aligned = NULL) const;

    virtual QSet<double> getChangedPoints() const;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicTimeInterval *tis);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       DynamicTimeInterval *&tis);

    friend class TimeIntervalSelectionOption;

    friend class DynamicTimeInterval;

protected:
    explicit Variable(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};


/** 
 * A time interval selection option.  This is often just a constant interval
 * but it allows for the interval to be dynamic in time.
 */
class CPD3DATACORE_EXPORT TimeIntervalSelectionOption
        : public ComponentOptionBase, virtual public ComponentOptionValueParsable {
Q_OBJECT

    Q_INTERFACES(CPD3::Data::ComponentOptionValueParsable)

    DynamicTimeInterval::Variable range;
    bool allowZero;
    bool allowNegative;
    bool allowUndefined;
    bool defaultAligned;

public:
    TimeIntervalSelectionOption(const QString &argumentName,
                                const QString &displayName,
                                const QString &description,
                                const QString &defaultBehavior,
                                int sortPriority = 0);

    TimeIntervalSelectionOption(const TimeIntervalSelectionOption &other);

    virtual ~TimeIntervalSelectionOption();

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    DynamicTimeInterval *getInterval() const;

    /**
     * Clear all offset segments.
     */
    void clear();

    /**
     * Set the time interval to be a constant offset.
     *
     * @param setUnit       the unit to use
     * @param setCount      the count to use
     * @param setAligned    true if aligning
     * @see Time::logical( double, LogicalTimeUnit, int, bool, bool, int )
     */
    void set(Time::LogicalTimeUnit setUnit, int setCount, bool setAligned = false);

    /**
     * Set the interval to the given parameters within the given time range
     *
     * @param start         the effective start time
     * @param end           the effective end time
     * @param setUnit       the unit to use
     * @param setCount      the count to use
     * @param setAligned    true if aligning
     */
    void set(double start, double end, Time::LogicalTimeUnit setUnit, int setCount, bool setAligned = false);

    /**
     * Set the interval to the parameters specified in the given Value within
     * the given time range.
     *
     * @param start         the effective start time
     * @param end           the effective end time
     * @param value         the value to use
     */
    void set(double start, double end, const Variant::Read &value);

    /**
     * Set the interval to the parameters specified in the given Value for all time.
     *
     * @param value         the value to use
     */
    void set(const Variant::Read &value);

    /**
     * Set the alignment state within the given time range.
     *
     * @param start         the effective start time
     * @param end           the effective end time
     * @param aligned       the alignment state to set
     */
    void setAligned(double start, double end, bool aligned);

    /**
     * Set the undefined state within the given time range.
     *
     * @param start         the effective start time
     * @param end           the effective end time
     * @param undefined     the undefined state to set
     */
    void setUndefined(double start = FP::undefined(), double end = FP::undefined(), bool undefined = true);

    /**
     * Set if this option allows zero offsets (default false).
     *
     * @param enable    true to allow zero
     */
    void setAllowZero(bool enable);

    /**
     * Set if this option allows negative offsets (default false).
     *
     * @param enable    true to allow negatives
     */
    void setAllowNegative(bool enable);

    /**
     * Set if this option allows undefined offsets (default false)
     *
     * @param enable    true to allow undefined offsets.
     */
    void setAllowUndefined(bool enable);

    /**
     * Set if this option defaults to aligned (default false).
     *
     * @param enable    true to default to aligned times.
     */
    void setDefaultAligned(bool enable);

    /**
     * Get if this option allows zero offsets.
     *
     * @return  true if zero non-aligning offsets are allowed
     */
    bool getAllowZero() const;

    /**
     * Get if this option allows negative offsets.
     *
     * @return  true if negative offsets are allowed
     */
    bool getAllowNegative() const;

    /**
     * Get if this option allows undefined offsets.
     *
     * @return  true if undefined offsets are allowed
     */
    bool getAllowUndefined() const;

    /**
     * Get if this option defaults to aligned offsets.
     *
     * @return  true if the default is aligned offsets
     */
    bool getDefaultAligned() const;

    virtual bool parseFromValue(const Variant::Read &value);
};

}
}

#endif
