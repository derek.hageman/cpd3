/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <memory>
#include <mutex>
#include <condition_variable>
#include <QTime>
#include <QLoggingCategory>

#include "datacore/processingtapchain.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/externalsource.hxx"
#include "archive/selection.hxx"
#include "archive/access.hxx"
#include "valueoptionparse.hxx"


Q_LOGGING_CATEGORY(log_datacore_processingtapchain, "cpd3.datacore.processingtapchain",
                   QtWarningMsg)

using namespace CPD3::Data::Internal;


namespace CPD3 {
namespace Data {

/** @file datacore/filterchain.hxx
 * Provides an interface for constructing and merging chains of filters.
 */

namespace Internal {

typedef QPair<SequenceMatch::Composite, StreamSink *> ChainTarget;

struct ChainStage {
    /* The processing at this stage */
    Variant::Root filter;

    /* The outputs after the processing */
    QList<ChainTarget> outputs;

    /* The next stages (takes the processed output) */
    std::vector<std::unique_ptr<ChainStage>> next;

    explicit ChainStage(const Variant::Read &add) : filter(add)
    { }

    ~ChainStage() = default;
};

struct ProcessingTapChainPrivate {
    double start;
    double end;
    Archive::Selection::Match stations;
    Archive::Selection::Match archives;
    Archive::Selection::Match variables;

    std::unique_ptr<ProcessingTapChain> next;
    ProcessingTapChain::ArchiveBackend *backend;
    bool parentIssueFinished;

    bool chainMergeDisable;
    std::string chainMergeKey;
    std::unique_ptr<ChainStage> chainHead;
    SequenceMatch::Composite chainArchiveSelections;

    std::mutex mutex;
    std::condition_variable request;
    bool terminated;
    int finished;
    double currentProgressValue;
    double nextProgressValue;
    bool pendingRead;

    ProcessingTapChainPrivate()
    {
        start = FP::undefined();
        end = FP::undefined();

        backend = nullptr;
        parentIssueFinished = true;

        chainMergeDisable = false;

        nextProgressValue = FP::undefined();
        currentProgressValue = FP::undefined();
        pendingRead = false;
        terminated = false;
        finished = 0;
    }

    ~ProcessingTapChainPrivate() = default;

    void copyFrom(const ProcessingTapChainPrivate &other)
    {
        if (other.terminated)
            terminated = true;
        start = other.start;
        end = other.end;
        stations = other.stations;
        archives = other.archives;
        variables = other.variables;
        backend = other.backend;
        parentIssueFinished = false;
    }
};
}

ProcessingTapChain::ProcessingTapChain() : p(new ProcessingTapChainPrivate())
{ }

ProcessingTapChain::~ProcessingTapChain()
{
    p->terminated = true;
    finishNext();
    p.reset();
}

static bool isArchiveHead(const Variant::Read &head)
{
    if (!head.exists())
        return true;
    {
        const auto &name = head["Name"].toString();
        if (!name.empty() && name != "archive")
            return false;
    }
    return true;
}

static bool isStageCompatible(const Variant::Read &a, const Variant::Read &b)
{
    if (a["Name"].toString() != b["Name"].toString())
        return false;
    if (a["Options"] != b["Options"])
        return false;
    return true;
}

bool ProcessingTapChain::integerateChain(const Variant::Read &config,
                                         const QList<ChainTarget> &outputs)
{

    if (!p->chainHead) {
        if (config["Merge"].exists())
            p->chainMergeDisable = !config["Merge"].toBoolean();
        else
            p->chainMergeDisable = false;
        p->chainMergeKey = config["MergeKey"].toString();

        p->chainHead.reset(new ChainStage(config["Input"]));
        if (config["Additional"].exists()) {
            p->chainArchiveSelections.append(SequenceMatch::Composite(config["Additional"]));
        }

        if (isArchiveHead(p->chainHead->filter)) {
            for (const auto &t : outputs) {
                p->chainArchiveSelections.append(t.first);
            }
        }

        ChainStage *prior = p->chainHead.get();
        for (auto stage : config["Components"].toChildren()) {
            prior->next.emplace_back(new ChainStage(stage));
            prior = prior->next.back().get();
        }

        prior->outputs = outputs;
        return true;
    }

    if (p->chainMergeDisable || (config["Merge"].exists() && !config["Merge"].toBool()))
        return false;
    if (p->chainMergeKey != config["MergeKey"].toString())
        return false;

    if (!isArchiveHead(p->chainHead->filter)) {
        if (isArchiveHead(config["Input"]))
            return false;
        if (p->chainHead->filter["Merge"].exists() && !p->chainHead->filter["Merge"].toBool())
            return false;
        if (config["Input/Merge"].exists() && !config["Input/Merge"].toBool())
            return false;
        if (p->chainHead->filter.read() != config["Input"])
            return false;
    } else {
        if (!isArchiveHead(config["Input"]))
            return false;
        for (const auto &t : outputs) {
            p->chainArchiveSelections.append(t.first);
        }
        if (config["Additional"].exists()) {
            p->chainArchiveSelections.append(SequenceMatch::Composite(config["Additional"]));
        }
    }

    ChainStage *prior = p->chainHead.get();
    for (auto stage : config["Components"].toChildren()) {
        ChainStage *next = nullptr;
        for (const auto &check : prior->next) {
            if (isStageCompatible(check->filter, stage)) {
                next = check.get();
                break;
            }
        }
        if (!next) {
            next = new ChainStage(stage);
            prior->next.emplace_back(next);
        }
        prior = next;
    }
    std::copy(outputs.begin(), outputs.end(), Util::back_emplacer(prior->outputs));
    return true;
}


/**
 * Add a target to the chain.  The selection should already have any expected/
 * default values registered.  Ownership of the ingress point
 * is retained by the caller.
 * 
 * @param target        the data target
 * @param config        the chain configuration
 * @param selection     the selection of data to be used
 */
void ProcessingTapChain::add(StreamSink *target,
                             const Variant::Read &config,
                             const SequenceMatch::Composite &selection)
{
    Q_ASSERT(!isRunning());
    if (!integerateChain(config, {ChainTarget(selection, target)})) {
        if (!p->next) {
            p->next.reset(new ProcessingTapChain);
            p->next->p->copyFrom(*p);
        }
        p->next->add(target, config, selection);
    }
}

/**
 * Add a set of targets to the chain.  Each selection should already have 
 * any expected/default values registered.  Ownership of the ingress points
 * is retained by the caller.
 * 
 * @param target        the data targets
 * @param config        the chain configuration
 */
void ProcessingTapChain::add(QList<QPair<SequenceMatch::Composite, StreamSink *> > &targets,
                             const Variant::Read &config)
{
    Q_ASSERT(!isRunning());
    if (!integerateChain(config, targets)) {
        if (!p->next) {
            p->next.reset(new ProcessingTapChain);
            p->next->p->copyFrom(*p);
        }
        p->next->add(targets, config);
    }
}

/**
 * Override the archive reading backend used by the chain.
 * 
 * @param backend   the backend to use
 */
void ProcessingTapChain::setBackend(ProcessingTapChain::ArchiveBackend *backend)
{
    Q_ASSERT(!isRunning());
    for (ProcessingTapChain *s = this; s != nullptr; s = s->p->next.get()) {
        s->p->backend = backend;
    }
}

/**
 * Set the default parameters of data selected.
 * 
 * @param start     the start of data
 * @param end       the end of data
 * @param stations  the stations
 * @param archives  the archives
 * @param variablea the variables
 */
void ProcessingTapChain::setDefaultSelection(double start,
                                             double end,
                                             const Archive::Selection::Match &stations,
                                             const Archive::Selection::Match &archives,
                                             const Archive::Selection::Match &variables)
{
    Q_ASSERT(!isRunning());
    for (ProcessingTapChain *s = this; s != nullptr; s = s->p->next.get()) {
        s->p->start = start;
        s->p->end = end;
        s->p->stations = stations;
        s->p->archives = archives;
        s->p->variables = variables;
    }
}

/**
 * Requests a clean terminate of the chain.
 */
void ProcessingTapChain::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(p->mutex);
        p->terminated = true;
    }
    p->request.notify_all();
}

void ProcessingTapChain::nextFinished()
{ p->request.notify_all(); }

void ProcessingTapChain::nextProgress(double value, bool isTime)
{
    Q_UNUSED(isTime);

    {
        std::lock_guard<std::mutex> lock(p->mutex);
        p->nextProgressValue = value;
    }
    p->request.notify_all();
}

namespace {

/* Safe to hold the mutex when calling children, since we know they have
 * no possible way of referencing us. */
class FilterChainHandler : public StreamSink {
public:
    std::mutex mutex;
    QList<ChainTarget> exits;
    SequenceName::Map<std::vector<StreamSink *> > dispatch;
    std::vector<StreamSink *> passThrough;

    FilterChainHandler() = default;

    virtual ~FilterChainHandler() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    {
        if (values.empty())
            return;

        for (auto p : passThrough) {
            p->incomingData(values);
        }

        std::lock_guard<std::mutex> lock(mutex);
        std::unordered_map<StreamSink *, SequenceValue::Transfer> result;
        for (const auto &add : values) {
            auto it = dispatch.find(add.getName());
            if (it == dispatch.end()) {
                it = dispatch.emplace(add.getName(), std::vector<StreamSink *>()).first;
                for (const auto &exit : exits) {
                    if (exit.first.matches(add.getName())) {
                        it->second.push_back(exit.second);
                        result[exit.second].push_back(add);
                    }
                }
            } else {
                for (auto target : it->second) {
                    result[target].push_back(add);
                }
            }
        }

        for (auto &output : result) {
            output.first->incomingData(std::move(output.second));
        }
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        if (values.empty())
            return;

        for (auto p : passThrough) {
            p->incomingData(values);
        }

        std::lock_guard<std::mutex> lock(mutex);
        std::unordered_map<StreamSink *, SequenceValue::Transfer> result;
        for (auto &add : values) {
            auto it = dispatch.find(add.getName());
            if (it == dispatch.end()) {
                it = dispatch.emplace(add.getName(), std::vector<StreamSink *>()).first;
                for (const auto &exit : exits) {
                    if (exit.first.matches(add.getName())) {
                        it->second.push_back(exit.second);
                        result[exit.second].push_back(add);
                    }
                }
            } else if (!it->second.empty()) {
                auto target = it->second.cbegin();
                for (auto end = it->second.cend() - 1; target != end; ++target) {
                    result[*target].push_back(add);
                }
                result[*target].emplace_back(std::move(add));
            }
        }

        for (auto &output : result) {
            output.first->incomingData(std::move(output.second));
        }
    }

    void incomingData(const SequenceValue &value) override
    {
        for (auto p : passThrough) {
            p->incomingData(value);
        }

        std::lock_guard<std::mutex> lock(mutex);
        auto it = dispatch.find(value.getName());
        if (it == dispatch.end()) {
            it = dispatch.emplace(value.getName(), std::vector<StreamSink *>()).first;
            for (const auto &exit : exits) {
                if (exit.first.matches(value.getName())) {
                    it->second.push_back(exit.second);
                    exit.second->incomingData(value);
                }
            }
        } else {
            for (auto target : it->second) {
                target->incomingData(value);
            }
        }
    }

    void incomingData(SequenceValue &&value) override
    {
        for (auto p : passThrough) {
            p->incomingData(value);
        }

        std::lock_guard<std::mutex> lock(mutex);
        auto it = dispatch.find(value.getName());
        if (it == dispatch.end()) {
            it = dispatch.emplace(value.getName(), std::vector<StreamSink *>()).first;
            for (const auto &exit : exits) {
                if (exit.first.matches(value.getName())) {
                    it->second.push_back(exit.second);
                    exit.second->incomingData(value);
                }
            }
        } else if (!it->second.empty()) {
            auto target = it->second.cbegin();
            for (auto end = it->second.cend() - 1; target != end; ++target) {
                (*target)->incomingData(value);
            }
            (*target)->incomingData(std::move(value));
        }
    }

    void endData() override
    {
        for (auto p : passThrough) {
            p->endData();
        }

        std::lock_guard<std::mutex> lock(mutex);
        for (const auto &exit : exits) {
            exit.second->endData();
        }
    }
};

class FilterChainTerminator : public FilterChainHandler {
public:
    ProcessingTapChainPrivate *p;
    bool doProgress;
    double lastEmitTime;

    FilterChainTerminator(ProcessingTapChainPrivate *sp, bool sdp = false)
            : FilterChainHandler(), p(sp), doProgress(sdp)
    {
        lastEmitTime = Time::time();
    }

    virtual ~FilterChainTerminator() = default;

    void progress(double time)
    {
        if (!doProgress)
            return;
        if (!FP::defined(time))
            return;
        double lastTime;
        std::lock_guard<std::mutex> lock(p->mutex);
        lastTime = p->currentProgressValue;
        p->currentProgressValue = time;
        if (!FP::defined(lastTime)) {
            p->request.notify_all();
        } else {
            double tnow = Time::time();
            if ((tnow - lastEmitTime) >= 0.5) {
                lastEmitTime = tnow;
                p->request.notify_all();
            }
        }
    }

    void incomingData(const SequenceValue::Transfer &values) override
    {
        FilterChainHandler::incomingData(values);
        if (values.empty())
            return;
        progress(values.back().getStart());
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        if (values.empty())
            return;
        double time = values.back().getStart();
        FilterChainHandler::incomingData(values);
        progress(time);
    }

    void incomingData(const SequenceValue &value) override
    {
        FilterChainHandler::incomingData(value);
        progress(value.getStart());
    }

    void incomingData(SequenceValue &&value) override
    {
        double time = value.getStart();
        FilterChainHandler::incomingData(std::move(value));
        progress(time);
    }

    void endData() override
    {
        FilterChainHandler::endData();
        {
            std::lock_guard<std::mutex> lock(p->mutex);
            p->finished++;
        }
        p->request.notify_all();
    }
};

class FilterChainFanout : public FilterChainHandler {
public:
    std::vector<StreamSink *> targets;

    FilterChainFanout() = default;

    virtual ~FilterChainFanout() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    {
        if (values.empty())
            return;

        for (auto p : passThrough) {
            p->incomingData(values);
        }
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        if (values.empty())
            return;
        if (passThrough.empty())
            return;

        auto p = passThrough.cbegin();
        for (auto end = passThrough.cend() - 1; p != end; ++p) {
            (*p)->incomingData(values);
        }
        (*p)->incomingData(std::move(values));
    }

    void incomingData(const SequenceValue &value) override
    {
        for (auto p : passThrough) {
            p->incomingData(value);
        }
    }

    void incomingData(SequenceValue &&value) override
    {
        if (passThrough.empty())
            return;

        auto p = passThrough.cbegin();
        for (auto end = passThrough.cend() - 1; p != end; ++p) {
            (*p)->incomingData(value);
        }
        (*p)->incomingData(std::move(value));
    }

    void endData() override
    {
        for (std::vector<StreamSink *>::const_iterator p = passThrough.begin(),
                endP = passThrough.end(); p != endP; ++p) {
            (*p)->endData();
        }
    }
};

class FilterExternalIngress : public StreamSink {
public:
    ProcessingTapChainPrivate *p;
    StreamSink *next;
    bool ended;

    FilterExternalIngress(ProcessingTapChainPrivate *sp, StreamSink *sn) : p(sp),
                                                                           next(sn),
                                                                           ended(false)
    { }

    virtual ~FilterExternalIngress() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    { next->incomingData(values); }

    void incomingData(SequenceValue::Transfer &&values) override
    { next->incomingData(std::move(values)); }

    void incomingData(const SequenceValue &value) override
    { next->incomingData(value); }

    void incomingData(SequenceValue &&value) override
    { next->incomingData(std::move(value)); }

    void endData() override
    {
        if (ended)
            return;
        ended = true;
        next->endData();
        {
            std::lock_guard<std::mutex> lock(p->mutex);
            p->finished++;
        }
        p->request.notify_all();
    }
};

}

void ProcessingTapChain::finishNext()
{
    if (!p->next)
        return;
    p->next->feedback.forward(feedback);

    for (;;) {
        {
            std::unique_lock<std::mutex> lock(p->mutex);
            if (p->terminated) {
                lock.unlock();
                p->next->signalTerminate();
            }
        }

        /* Polling, but not worth another wait condition here */
        if (!p->next->wait(500))
            continue;

        p->next.reset();
        break;
    }
}

void ProcessingTapChain::finishAllExits()
{
    for (const auto &target : p->chainHead->outputs) {
        target.second->endData();
    }

    if (p->next) {
        p->next->signalTerminate();
    }
}

void ProcessingTapChain::finishArchiveIssue()
{
    if (!p->backend) {
        Q_ASSERT(!p->next || !p->next->p->backend);
        return;
    }

    {
        std::unique_lock<std::mutex> lock(p->mutex);
        p->request.wait(lock, [this] { return p->parentIssueFinished; });
    }

    if (!p->next) {
        p->backend->archiveReadIssueComplete();
    } else {
        {
            std::lock_guard<std::mutex> lock(p->next->p->mutex);
            p->next->p->parentIssueFinished = true;
        }
        p->next->p->request.notify_all();
    }
}

void ProcessingTapChain::havePendingRead()
{
    {
        std::lock_guard<std::mutex> lock(p->mutex);
        p->pendingRead = true;
    }
    p->request.notify_all();
}

namespace {

struct ChainBuildState {
    SequenceName::Set &requestedInputs;
    std::vector<std::unique_ptr<FilterChainHandler>> &handlers;
    std::vector<std::unique_ptr<ProcessingStage>> &filters;
    int &terminators;

    ChainStage *current;
    StreamSink *target;
    SequenceName::Set inputs;

    ProcessingTapChainPrivate *p;

    ChainBuildState(SequenceName::Set &req,
                    std::vector<std::unique_ptr<FilterChainHandler>> &h,
                    std::vector<std::unique_ptr<ProcessingStage>> &f,
                    int &t) : requestedInputs(req),
                              handlers(h),
                              filters(f),
                              terminators(t),
                              current(nullptr),
                              target(nullptr),
                              inputs(),
                              p(nullptr)
    { }
};

/* This creates the handler for the current stage and the processing
 * for the NEXT stages.  That way, the caller is responsible for connecting
 * the already created processing to stage.target.  This allows the same
 * function to be used for the chain initial stage (connected to the archive
 * or an external input) and subsequent processing stages (connected to
 * general filters). */
static bool buildChains(ChainBuildState &state)
{
    state.target = nullptr;

    /* Create the filters this stage feeds into, and connect them to
     * their outputs */
    std::vector<StreamSink *> nextTargets;
    for (const auto &next : state.current->next) {
        auto componentConfig = next->filter.read();

        QString componentName(componentConfig["Name"].toQString());
        if (componentName.isEmpty()) {
            qCWarning(log_datacore_processingtapchain) << "Empty component name";
            return false;
        }
        QObject *component = ComponentLoader::create(componentName);
        if (!component) {
            qCWarning(log_datacore_processingtapchain) << "Can't load component" << componentName;
            return false;
        }
        ProcessingStageComponent *filterComponent;
        if (!(filterComponent = qobject_cast<ProcessingStageComponent *>(component))) {
            qCWarning(log_datacore_processingtapchain) << "Component" << componentName
                                                       << "is not a general filter";
            return false;
        }

        ComponentOptions options(filterComponent->getOptions());
        ValueOptionParse::parse(options, componentConfig["Options"]);
        QList<SequenceName> createInputs;
        Util::append(state.inputs, createInputs);
        ProcessingStage *filter =
                filterComponent->createGeneralFilterPredefined(options, state.p->start,
                                                               state.p->end, createInputs);
        if (!filter) {
            qCWarning(log_datacore_processingtapchain) << "Can't create general filter for"
                                                       << componentName;
            return false;
        }
        state.filters.emplace_back(filter);

        auto add = filter->requestedInputs();
        for (const auto &e : state.inputs) {
            add.erase(e);
        }
        Util::merge(add, state.requestedInputs);

        ChainBuildState nextState = state;
        nextState.current = next.get();
        Util::merge(filter->predictedOutputs(), nextState.inputs);
        if (!buildChains(nextState))
            return false;

        nextTargets.push_back(filter);

        Q_ASSERT(nextState.target);
        filter->setEgress(nextState.target);
    }

    /* Now create the outputs for this stage, returning the target
     * in stage.target */

    /* No next targets, so this is a terminator */
    if (nextTargets.empty()) {
        Q_ASSERT(state.current->next.empty());

        FilterChainTerminator *handler = new FilterChainTerminator(state.p, state.terminators == 0);
        state.handlers.emplace_back(handler);
        state.terminators++;
        handler->exits = state.current->outputs;

        state.target = handler;
    } else if (state.current->outputs.empty()) {
        /* No exits and a single next, so we can just directly connect
         * it */
        if (nextTargets.size() == 1) {
            state.target = nextTargets.front();
        } else {
            Q_ASSERT(!nextTargets.empty());
            FilterChainFanout *handler = new FilterChainFanout;
            state.handlers.emplace_back(handler);
            handler->passThrough = nextTargets;
            state.target = handler;
        }
    } else {
        FilterChainHandler *handler = new FilterChainHandler;
        state.handlers.emplace_back(handler);
        handler->exits = state.current->outputs;
        handler->passThrough = nextTargets;
        state.target = handler;
    }

    Q_ASSERT(state.target);
    return true;
}

static void accumulateChainInputs(ChainStage *stage, SequenceName::Set &inputs)
{
    for (const auto &target : stage->outputs) {
        Util::merge(target.first.reduceMultiple(), inputs);
    }
    for (const auto &next : stage->next) {
        accumulateChainInputs(next.get(), inputs);
    }
}

}

void ProcessingTapChain::run()
{
    if (!p->chainHead)
        return;

    feedback.emitStage(tr("Input initialization", "chain start stage"),
                       tr("The processing chain is starting up."), false);

    if (p->next) {
        p->next->feedback.forward(feedback);
        p->next->start();
    }

    p->finished = 0;

    std::unique_ptr<Archive::Access> read;
    std::unique_ptr<ExternalConverter> chainInput;
    std::unique_ptr<QIODevice> inputDevice;
    std::vector<std::unique_ptr<FilterChainHandler>> handlers;
    std::vector<std::unique_ptr<ProcessingStage>> filters;
    std::vector<std::unique_ptr<FilterExternalIngress>> externalReferences;
    StreamSink *chainHead;
    Archive::Selection::List selections;

    if (!isArchiveHead(p->chainHead->filter)) {
        auto config = p->chainHead->filter.read();

        QString componentName(config["Name"].toQString());
        if (componentName.isEmpty()) {
            qCWarning(log_datacore_processingtapchain) << "Empty head component name";
            finishAllExits();
            finishArchiveIssue();
            finishNext();
            return;
        }
        QObject *component = ComponentLoader::create(componentName);
        if (!component) {
            qCWarning(log_datacore_processingtapchain) << "Can't load component" << componentName;
            finishAllExits();
            finishArchiveIssue();
            finishNext();
            return;
        }
        ExternalConverterComponent *ingressComponent;
        ExternalSourceComponent *ingressComponentExternal;
        if ((ingressComponent = qobject_cast<ExternalConverterComponent *>(component))) {
            if (ingressComponent->requiresInputDevice()) {
                QString inputFileName(config["File"].toQString());
                if (inputFileName.isEmpty()) {
                    qCWarning(log_datacore_processingtapchain) << "Component" << componentName
                                                               << "requires an input file but none was specified";
                    finishAllExits();
                    finishArchiveIssue();
                    finishNext();
                    return;
                }
                if (!QFile::exists(inputFileName)) {
                    qCWarning(log_datacore_processingtapchain) << "Input file" << inputFileName
                                                               << "does not exist for component"
                                                               << componentName;
                    finishAllExits();
                    finishArchiveIssue();
                    finishNext();
                    return;
                }
                p->pendingRead = true;

                inputDevice.reset(new QFile(inputFileName));
                connect(inputDevice.get(), SIGNAL(readyRead()), this, SLOT(havePendingRead()),
                        Qt::DirectConnection);
                connect(inputDevice.get(), SIGNAL(readChannelFinished()), this,
                        SLOT(havePendingRead()), Qt::DirectConnection);

                if (!inputDevice->open(QIODevice::ReadOnly)) {
                    qCWarning(log_datacore_processingtapchain) << "Can't open input file"
                                                               << inputFileName << ":"
                                                               << inputDevice->errorString()
                                                               << "on component" << componentName;
                    inputDevice.reset();
                    finishAllExits();
                    finishArchiveIssue();
                    finishNext();
                    return;
                }
            }

            ComponentOptions options(ingressComponent->getOptions());
            ValueOptionParse::parse(options, config["Options"]);
            chainInput.reset(ingressComponent->createDataIngress(options));
        } else if ((ingressComponentExternal =
                            qobject_cast<ExternalSourceComponent *>(component))) {
            auto effectiveStations = p->stations;
            if (static_cast<int>(effectiveStations.size()) >
                    ingressComponentExternal->ingressAllowStations()) {
                effectiveStations.clear();
            }
            auto vCheck = config["Station"];
            if (vCheck.getType() == Variant::Type::String) {
                effectiveStations.clear();
                for (const auto &add : vCheck.toQString()
                                             .toLower()
                                             .split(QRegExp("\\s+"), QString::SkipEmptyParts)) {
                    effectiveStations.emplace_back(add.toStdString());
                }
            } else if (vCheck.exists()) {
                for (const auto &add : vCheck.toChildren().keys()) {
                    effectiveStations.push_back(
                            QString::fromStdString(add).toLower().toStdString());
                }
            }
            if (static_cast<int>(effectiveStations.size()) <
                    ingressComponentExternal->ingressRequireStations()) {
                qCWarning(log_datacore_processingtapchain) << "Component" << componentName
                                                           << "requires"
                                                           << ingressComponentExternal->ingressRequireStations()
                                                           << "station(s), but only"
                                                           << effectiveStations.size()
                                                           << "are available";
                finishAllExits();
                finishArchiveIssue();
                finishNext();
                return;
            }
            if (static_cast<int>(effectiveStations.size()) >
                    ingressComponentExternal->ingressAllowStations()) {
                qCWarning(log_datacore_processingtapchain) << "Component" << componentName
                                                           << "permits"
                                                           << ingressComponentExternal->ingressRequireStations()
                                                           << "station(s), but"
                                                           << effectiveStations.size()
                                                           << "are specified";
                finishAllExits();
                finishArchiveIssue();
                finishNext();
                return;
            }

            bool provideTimes = !config["IgnoreTimes"].toBool();
            double effectiveStart = p->start;
            double effectiveEnd = p->end;
            vCheck = config["Start"];
            if (vCheck.getType() == Variant::Type::Real) {
                effectiveStart = vCheck.toDouble();
            }
            vCheck = config["End"];
            if (vCheck.getType() == Variant::Type::Real) {
                effectiveEnd = vCheck.toDouble();
            }
            if (!ingressComponentExternal->ingressAcceptsUndefinedBounds() &&
                    (!FP::defined(effectiveStart) || !FP::defined(effectiveEnd))) {
                provideTimes = false;
            }
            if (!provideTimes && ingressComponentExternal->ingressRequiresTime()) {
                qCWarning(log_datacore_processingtapchain) << "Component" << componentName
                                                           << "requires time bounds but none are available";
                finishAllExits();
                finishArchiveIssue();
                finishNext();
                return;
            }

            ComponentOptions options(ingressComponentExternal->getOptions());
            ValueOptionParse::parse(options, config["Options"]);
            if (provideTimes) {
                chainInput.reset(
                        ingressComponentExternal->createExternalIngress(options, effectiveStart,
                                                                        effectiveEnd,
                                                                        effectiveStations));
            } else {
                chainInput.reset(ingressComponentExternal->createExternalIngress(options,
                                                                                 effectiveStations));
            }
        } else {
            qCWarning(log_datacore_processingtapchain) << "Component" << componentName
                                                       << "is not an ingress";
            finishAllExits();
            finishArchiveIssue();
            finishNext();
            return;
        }

        if (!chainInput) {
            qCWarning(log_datacore_processingtapchain) << "Can't create ingress for"
                                                       << componentName;
            finishAllExits();
            finishArchiveIssue();
            finishNext();
            if (inputDevice) {
                inputDevice->disconnect();
                inputDevice->close();
                inputDevice.reset();
            }
            return;
        }
    } else {
        auto add = p->chainArchiveSelections
                    .toArchiveSelections(p->stations, p->archives, p->variables);
        for (auto &m : add) {
            m.start = p->start;
            m.end = p->end;
        }
        Util::append(std::move(add), selections);
    }

    SequenceName::Set inputs;
    accumulateChainInputs(p->chainHead.get(), inputs);

    SequenceName::Set requestedInputs;
    int terminators = 0;
    ChainBuildState chainBuild(requestedInputs, handlers, filters, terminators);
    chainBuild.p = p.get();
    chainBuild.current = p->chainHead.get();
    chainBuild.target = nullptr;
    chainBuild.inputs = inputs;

    if (!buildChains(chainBuild)) {
        finishAllExits();
        finishArchiveIssue();
        finishNext();
        filters.clear();
        handlers.clear();
        if (inputDevice) {
            inputDevice->disconnect();
            inputDevice->close();
            inputDevice.reset();
        }
        if (chainInput) {
            chainInput.reset();
        }
        return;
    }
    Q_ASSERT(chainBuild.target);
    chainHead = chainBuild.target;

    if (chainInput)
        chainInput->start();
    for (const auto &filter : filters) {
        filter->start();
    }

    Q_ASSERT(chainHead);
    if (!selections.empty()) {
        for (const auto &name : requestedInputs) {
            selections.push_back(Archive::Selection(name, p->start, p->end));
        }
        if (!p->backend) {
            read = std::unique_ptr<Archive::Access>(new Archive::Access);
            auto reader = read->readStream(selections);
            reader->setEgress(chainHead);
            reader->detach();
            chainHead = nullptr;
            qCDebug(log_datacore_processingtapchain) << "Issued generic archive read for"
                                                     << selections;
        } else {
            FilterExternalIngress *ext = new FilterExternalIngress(p.get(), chainHead);
            ++terminators;
            externalReferences.emplace_back(ext);
            p->backend->issueArchiveRead(selections, ext);
            chainHead = nullptr;
            qCDebug(log_datacore_processingtapchain) << "Issued backend archive read for"
                                                     << selections;
        }
    } else {
        if (!chainInput) {
            chainHead->endData();
        } else {
            chainInput->setEgress(chainHead);
        }
        chainHead = nullptr;
    }

    finishArchiveIssue();

    feedback.emitStage(tr("Processing data", "chain process stage"),
                       tr("The processing chain is running."), true);

    double lastEmitTime = FP::undefined();
    bool morePending = false;
    std::unique_lock<std::mutex> lock(p->mutex);
    for (;;) {
        double eTime = p->currentProgressValue;
        if (p->next &&
                (!FP::defined(eTime) ||
                        !FP::defined(p->nextProgressValue) ||
                        p->nextProgressValue < eTime))
            eTime = p->nextProgressValue;
        if (FP::defined(eTime) && (!FP::defined(lastEmitTime) || lastEmitTime != eTime)) {
            lock.unlock();
            feedback.emitProgressTime(eTime);
            lock.lock();
        }

        if (p->terminated) {
            lock.unlock();
            if (chainHead) {
                chainHead->endData();
                chainHead = nullptr;
            }
            if (chainInput) {
                chainInput->signalTerminate();
            }
            if (inputDevice) {
                inputDevice->disconnect();
                inputDevice->close();
                inputDevice.reset();
            }

            for (const auto &filter : filters) {
                filter->signalTerminate();
            }

            if (p->next)
                p->next->signalTerminate();
            break;
        }

        if (p->next && p->next->isFinished()) {
            p->next->wait();
            p->next.reset();
        }

        if (p->finished >= terminators) {
            lock.unlock();
            break;
        }

        if (!p->pendingRead || !inputDevice) {
            p->request.wait(lock);
            continue;
        }

        p->pendingRead = false;
        lock.unlock();

        morePending = false;
        if (inputDevice) {
            qint64 n = inputDevice->bytesAvailable();
            if (n > 0) {
                chainInput->incomingData(inputDevice->read((int) qMin(Q_INT64_C(65536), n)));
                if (inputDevice->bytesAvailable() > 0)
                    morePending = true;
            }

            if (inputDevice->atEnd() && inputDevice->bytesAvailable() == 0) {
                chainInput->endData();
                inputDevice->disconnect();
                inputDevice->close();
                inputDevice.reset();
            }
        }

        lock.lock();
        if (morePending)
            p->pendingRead = true;
    }

    if (inputDevice) {
        if (chainInput)
            chainInput->endData();
        inputDevice->disconnect();
        inputDevice->close();
        inputDevice.reset();
    }

    if (chainInput) {
        chainInput->wait();
        chainInput.reset();
    }

    /* Make sure the externals are not referenced before we delete them or anything
     * they reference */
    lock.lock();
    for (const auto &check : externalReferences) {
        while (!check->ended) {
            p->request.wait(lock);
        }
    }
    lock.unlock();

    for (const auto &filter : filters) {
        filter->wait();
    }
    filters.clear();
    handlers.clear();
    externalReferences.clear();

    if (read) {
        read->waitForLocks();
        read.reset();
    }

    finishNext();
}

ProcessingTapChain::ArchiveBackend::~ArchiveBackend() = default;

void ProcessingTapChain::ArchiveBackend::archiveReadIssueComplete()
{ }


}
}
