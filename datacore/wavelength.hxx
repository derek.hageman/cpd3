/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACOREWAVELENGTH_H
#define CPD3DATACOREWAVELENGTH_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QMultiMap>

#include "datacore/datacore.hxx"
#include "core/number.hxx"
#include "datacore/segment.hxx"
#include "datacore/dynamicsequenceselection.hxx"

namespace CPD3 {
namespace Data {

/**
 * Provides general routines for dealing with wavelength data.
 */
class CPD3DATACORE_EXPORT Wavelength {
public:
    static const std::string &code(double wavelength);

    /** The behavior when the bracketing hits an exact match.  The exact
     * match may still be passed if excluding it isn't possible (not enough
     * other valid values) */
    enum class BracketingExactBehavior {
        /** Exclude the exact match if possible. */
                ExcludeExact,

        /** Use the exact and the wavelength above it if possible. */
                Above,

        /** Use the exact and the wavelength below it if possible. */
                Below,

        /** Use the exact match as the upper and lower bounds. */
                ExactTwice,
    };

    /**
     * Apply bracketing of a wavelength to some data.  This finds the 
     * wavelengths that bracket some other value, applies a conversion
     * function and returns the result.
     * <br>
     * Lookup is a container (usually a QMultiMap) from a double value to
     * some other type.  The Convert type most provide the following methods:
     * <ul>
     *  <li>Function: convert(const LookupData &, const LookupData &, double) - 
     *      Apply the conversion  to the result type.  The first parameter is
     *      the lower bound, the second is the upper, and the third is the
     *      target wavelength.
     *  <li>Function: isValid(const LookupData &) - Test if the lookup data is 
     *      valid and should be used.  Invalid data are excluded from 
     *      bracketing.
     *  <li>Function: invalid() - Return an invalid result
     * </ul>
     * 
     * @param target        the target wavelength
     * @param data          the input data to convert
     * @param convert       the conversion container
     * @param exactBehavior the behavior for exact matches
     * @param allowExtrapolation enable extrapolation beyond the bounds of the data
     * @return              the result of convert(const LookupData &) on the lower bracketing value
     */
    template<typename Lookup, typename Convert>
    static auto bracketing(double target,
                           const Lookup &data,
                           const Convert &convert,
                           BracketingExactBehavior exactBehavior = BracketingExactBehavior::Above,
                           bool allowExtrapolation = true) -> decltype(convert.invalid())
    {
        if (!FP::defined(target))
            return convert.invalid();
        switch (data.size()) {
        case 0:
            return convert.invalid();
        case 1: {
            if (exactBehavior != BracketingExactBehavior::ExactTwice)
                return convert.invalid();
            auto check = data.begin();
            if (check->first != target)
                return convert.invalid();
            if (!convert.isValid(check->second))
                return convert.invalid();
            return convert.convert(check->second, check->second, target);
        }
        default:
            break;
        }

        auto begin = data.begin();
        auto end = data.end();
        auto ub = data.lower_bound(target);
        auto lb = ub;

        for (; ub != end && !convert.isValid(ub->second); ++ub) { }
        if (ub == end) {
            /* Nothing valid after, so look before */
            if (!allowExtrapolation)
                return convert.invalid();
            /* We know there's nothing after, so move the end */
            end = lb;
            if (end == begin)
                return convert.invalid();
            ub = end;
            --ub;
            for (; ub != begin && !convert.isValid(ub->second); --ub) { }
            if (ub == begin) {
                /* Hit the beginning, so interpolation/extrapolation isn't
                 * possible, but it might be an exact match we can catch */
                if (exactBehavior == BracketingExactBehavior::ExactTwice &&
                        ub->first == target &&
                        convert.isValid(ub->second)) {
                    return convert.convert(ub->second, ub->second, target);
                }
                return convert.invalid();
            }
            lb = ub;
        } else {
            lb = ub;
        }
        Q_ASSERT(convert.isValid(ub->second));

        /* Exact match */
        if (ub->first == target) {
            switch (exactBehavior) {
            case BracketingExactBehavior::ExactTwice:
                return convert.convert(ub->second, ub->second, target);
            case BracketingExactBehavior::Below:
                /* Default behavior is to search backwards now */
                break;

            case BracketingExactBehavior::Above: {
                auto next = ub;
                ++next;
                for (; next != end && next->first == target; ++next) { }
                for (; next != end && !convert.isValid(next->second); ++next) { }
                if (next == end) {
                    /* We know there's nothing after, so move the end, and
                     * continue the backwards search normally */
                    end = ub;
                    ++end;
                    break;
                }
                Q_ASSERT(ub->first < next->first);
                Q_ASSERT(convert.isValid(next->second));
                return convert.convert(ub->second, next->second, target);
            }

            case BracketingExactBehavior::ExcludeExact: {
                /* No exclusion possible */
                if (ub == begin)
                    break;

                auto prev = ub;
                --prev;
                /* Get the one before it */
                for (; prev != begin && prev->first == target; --prev) { }
                for (; prev != begin && !convert.isValid(prev->second); --prev) { }
                if (prev == begin) {
                    if (prev->first == target || !convert.isValid(prev->second)) {
                        /* If we can't get a unique before it, fall back to the
                         * default behavior, noting the new start */
                        begin = ub;
                        break;
                    }
                }
                Q_ASSERT(prev->first < target);
                Q_ASSERT(convert.isValid(prev->second));

                auto next = ub;
                ++next;
                for (; next != end && next->first == target; ++next) { }
                for (; next != end && !convert.isValid(next->second); ++next) { }
                if (next == end) {
                    /* We know there's nothing after, so move the end, and
                     * continue the backwards search normally */
                    end = ub;
                    ++end;
                    break;
                }
                Q_ASSERT(next->first > target);
                Q_ASSERT(convert.isValid(next->second));

                Q_ASSERT(prev->first < next->first);
                return convert.convert(prev->second, next->second, target);

            }
            }
        }

        /* Now have a valid upper bound, so we just need to move the lower
         * until it's also valid */
        for (; lb != begin && lb->first == ub->first; --lb) { }
        for (; lb != begin && !convert.isValid(lb->second); --lb) { }

        if (lb == begin && (lb->first == ub->first || !convert.isValid(lb->second))) {
            /* Failed to find a lower bound, so swap them and try moving
             * the upper */
            lb = ub;
            ++ub;
            for (; ub != end && ub->first == lb->first; ++ub) { }
            for (; ub != end && !convert.isValid(ub->second); ++ub) { }
            if (ub == end) {
                /* At this point, we know there's nothing valid before or
                 * after, so we have to give up */
                return convert.invalid();
            }
            if (!allowExtrapolation && lb->first > target) {
                /* This can happen if we've already had to move it after
                 * the initial, so make sure we don't extrapolate if
                 * we're not allowed to */
                return convert.invalid();
            }
        }
        Q_ASSERT(convert.isValid(lb->second));

        Q_ASSERT(lb->first < ub->first);
        return convert.convert(lb->second, ub->second, target);
    }

    /**
     * Get the nearest wavelength value to a given target.
     * <br>
     * Lookup is a container (usually a QMultiMap) from a double value to
     * some other type.  The Convert type most provide the following methods:
     * <ul>
     *  <li>Function: convert(const LookupData &, double) - 
     *      Apply the conversion  to the result type.  The first parameter is
     *      the nearest matching and the second is the target wavelength.
     *  <li>Function: isValid(const LookupData &) - Test if the lookup data is 
     *      valid and should be used.  Invalid data are excluded from 
     *      searching.
     *  <li>Function: outsidePossible(const LookupData &, double) - Test if
     *      the lookup data is outside the possible range.  Once any data
     *      returns true for this, searching is stopped.  The first parameter is
     *      the nearest matching and the second is the target wavelength.
     *  <li>Function: invalid() - Return an invalid result
     * </ul>
     * 
     * @param target        the target wavelength
     * @param data          the input data to convert
     * @param convert       the conversion container
     */
    template<typename Lookup, typename Convert>
    static auto nearest(double target,
                        const Lookup &data,
                        const Convert &convert) -> decltype(convert.invalid())
    {
        if (data.empty())
            return convert.invalid();

        auto upper = data.lower_bound(target);
        auto begin = data.begin();
        auto end = data.end();

        if (upper == begin) {
            for (; upper != end; ++upper) {
                if (convert.outsidePossible(upper->second, target))
                    return convert.invalid();
                if (!convert.isValid(upper->second))
                    continue;
                return convert.convert(upper->second, target);
            }
            return convert.invalid();
        }

        auto lower = upper;
        --lower;

        for (; lower != begin && upper != end;) {
            double distanceLower = target - lower->first;
            double distanceUpper = upper->first - target;
            Q_ASSERT(distanceLower >= 0.0);
            Q_ASSERT(distanceUpper >= 0.0);

            if (distanceLower < distanceUpper) {
                if (convert.outsidePossible(lower->second, target))
                    return convert.invalid();
                if (convert.isValid(lower->second))
                    return convert.convert(lower->second, target);
                --lower;
            } else {
                if (convert.outsidePossible(upper->second, target))
                    return convert.invalid();
                if (convert.isValid(upper->second))
                    return convert.convert(upper->second, target);
                ++upper;
            }
        }
        /* lower >= begin */

        if (upper == end) {
            for (; lower != begin; --lower) {
                if (convert.outsidePossible(lower->second, target))
                    return convert.invalid();
                if (!convert.isValid(lower->second))
                    continue;
                return convert.convert(lower->second, target);
            }
            Q_ASSERT(lower == begin);
            if (!convert.outsidePossible(lower->second, target) && convert.isValid(lower->second))
                return convert.convert(lower->second, target);
            return convert.invalid();
        }

        Q_ASSERT(lower == begin);
        double distanceLower = target - lower->first;
        Q_ASSERT(distanceLower >= 0.0);
        for (; upper != end; ++upper) {
            double distanceUpper = upper->first - target;
            Q_ASSERT(distanceUpper >= 0.0);

            if (distanceLower < distanceUpper) {
                if (convert.outsidePossible(lower->second, target))
                    return convert.invalid();
                if (convert.isValid(lower->second))
                    return convert.convert(lower->second, target);

                for (; upper != end; ++upper) {
                    if (convert.outsidePossible(upper->second, target))
                        return convert.invalid();
                    if (!convert.isValid(upper->second))
                        continue;
                    return convert.convert(upper->second, target);
                }
                return convert.invalid();
            } else {
                if (convert.outsidePossible(upper->second, target))
                    return convert.invalid();
                if (convert.isValid(upper->second))
                    return convert.convert(upper->second, target);
            }
        }

        Q_ASSERT(upper == end);
        Q_ASSERT(lower == begin);
        if (!convert.outsidePossible(lower->second, target) && convert.isValid(lower->second))
            return convert.convert(lower->second, target);
        return convert.invalid();
    }
};

}
}

#endif
