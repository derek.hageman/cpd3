/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACOREFILTERPRIMITIVE_H
#define CPD3DATACOREFILTERPRIMITIVE_H

#include "core/first.hxx"

#include <limits.h>
#include <vector>
#include <deque>
#include <memory>
#include <QtGlobal>
#include <QDataStream>
#include <QList>
#include <QSet>
#include <QSharedDataPointer>
#include <QDebug>
#include <QDebugStateSaver>

#include "datacore.hxx"
#include "segment.hxx"
#include "valueoptionparse.hxx"
#include "variant/composite.hxx"
#include "core/component.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"

namespace CPD3 {
namespace Data {

template<typename Result>
class DynamicPrimitive;

template<typename Result>
QDataStream &operator<<(QDataStream &stream, const DynamicPrimitive<Result> *to);

template<typename Result>
QDataStream &operator>>(QDataStream &stream, DynamicPrimitive<Result> *&to);

template<typename Result>
QDebug operator<<(QDebug stream, const DynamicPrimitive<Result> *to);

template<typename Result>
QDataStream &operator<<(QDataStream &stream, const std::unique_ptr<DynamicPrimitive<Result>> &to);

template<typename Result>
QDataStream &operator>>(QDataStream &stream, std::unique_ptr<DynamicPrimitive<Result>> &to);

template<typename Result>
QDebug operator<<(QDebug stream, const std::unique_ptr<DynamicPrimitive<Result>> &to);

/**
 * A possibly time dependent list of primitives for input.
 * 
 * @param Result    the type of the result
 */
template<typename Result>
class DynamicPrimitive {
public:
    virtual ~DynamicPrimitive() = default;

    /**
     * Get the result for the given range.  This will advance the time, so no 
     * requests before the start time may be made after the call.
     * 
     * @param start     the start of the desired interval
     * @param end       the end of the desired interval
     * @return          the result
     */
    virtual const Result &get(double start, double end) = 0;

    /** Same as get(range.getStart(), range.getEnd())
     * @see get(double, double) */
    template<typename T>
    inline const Result &get(const T &range)
    { return get(range.getStart(), range.getEnd()); }

    /**
     * Get the result for the given time range.
     * 
     * @param start     the start of the desired interval
     * @param end       the end of the desired interval
     * @return          the result
     */
    virtual const Result &getConst(double start, double end) const = 0;

    /** @see getConst(double, double) */
    inline const Result &get(double start, double end) const
    { return getConst(start, end); }

    /** Same as getConst(range.getStart(), range.getEnd())
     * @see get(double, double) */
    template<typename T>
    inline const Result &getConst(const T &range) const
    { return getConst(range.getStart(), range.getEnd()); }

    /** Same as getConst(T)
     * @see get(const T &) */
    template<typename T>
    inline const Result &get(const T &range) const
    { return getConst(range.getStart(), range.getEnd()); }

    /**
     * Get the result at the given time.  This will advance the time, so no 
     * requests before the time may be made after the call.
     * 
     * @param time      the instantaneous time
     * @return          the result
     */
    virtual const Result &get(double time) = 0;

    /**
     * Get the result at the given time.
     * 
     * @param time      the instantaneous time
     * @return          the result
     */
    virtual const Result &getConst(double time) const = 0;

    /** @see getConst(double) */
    inline const Result &get(double time) const
    { return getConst(time); }

    /**
     * Get all known times when the "structure" of this operation might change.
     * Generally this should include times when the values would change.
     * 
     * @return  a list of known changed times
     */
    virtual QSet<double> getChangedPoints() const
    { return QSet<double>(); }

    /**
     * Duplicate this object, including polymorphism.  Allocates a new object
     * that must be deleted by the caller.
     * 
     * @return      the copy
     */
    virtual DynamicPrimitive<Result> *clone() const = 0;

    friend QDataStream &operator<<<>(QDataStream &stream, const DynamicPrimitive<Result> *to);

    friend QDataStream &operator>><>(QDataStream &stream, DynamicPrimitive<Result> *&to);

    friend QDebug operator<<<>(QDebug stream, const DynamicPrimitive<Result> *to);

    friend QDataStream &operator<<<>(QDataStream &stream,
                                     const std::unique_ptr<DynamicPrimitive<Result>> &to);

    friend QDataStream &operator>><>(QDataStream &stream,
                                     std::unique_ptr<DynamicPrimitive<Result>> &to);

    friend QDebug operator<<<>(QDebug stream, const std::unique_ptr<DynamicPrimitive<Result>> &to);


    /**
     * Create a primitive input from a configuration.
     * 
     * @param converter     the functional object used to convert a Value to a primitive
     * @param config        the configuration
     * @param unit          the unit within the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @return              a new input
     */
    template<typename Converter>
    static DynamicPrimitive<Result> *fromConfiguration(Converter converter,
                                                       SequenceSegment::Transfer &config,
                                                       const SequenceName &unit,
                                                       const QString &path = QString(),
                                                       double start = FP::undefined(),
                                                       double end = FP::undefined());

    /**
     * Create a primitive input from a configuration.
     *
     * @param converter     the functional object used to convert a Value to a primitive
     * @param invalid       the invalid default value
     * @param config        the configuration
     * @param unit          the unit within the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @return              a new input
     */
    template<typename Converter>
    static DynamicPrimitive<Result> *fromConfiguration(Converter converter,
                                                       const Result &invalid,
                                                       SequenceSegment::Transfer &config,
                                                       const SequenceName &unit,
                                                       const QString &path = QString(),
                                                       double start = FP::undefined(),
                                                       double end = FP::undefined());

    /**
     * Create a primitive input from a configuration.
     * 
     * @param converter     the functional object used to convert a Value to a primitive
     * @param config        the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @return              a new input
     */
    template<typename Converter>
    static DynamicPrimitive<Result> *fromConfiguration(Converter converter,
                                                       const ValueSegment::Transfer &config,
                                                       const QString &path = QString(),
                                                       double start = FP::undefined(),
                                                       double end = FP::undefined());

    /**
     * Create a primitive input from a configuration.
     *
     * @param converter     the functional object used to convert a Value to a primitive
     * @param invalid       the invalid default value
     * @param config        the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @return              a new input
     */
    template<typename Converter>
    static DynamicPrimitive<Result> *fromConfiguration(Converter converter,
                                                       const Result &invalid,
                                                       const ValueSegment::Transfer &config,
                                                       const QString &path = QString(),
                                                       double start = FP::undefined(),
                                                       double end = FP::undefined());

    class Constant;

    class Variable;

protected:
    enum {
        SerialType_NULL = 0, SerialType_Constant, SerialType_Variable,
    };

    virtual void serialize(QDataStream &stream) const = 0;

    virtual void printLog(QDebug &) const
    { }
};

/**
 * An input that always returns a constant value.
 */
template<typename Result>
class DynamicPrimitive<Result>::Constant : public DynamicPrimitive<Result> {
    Result r;
public:
    Constant() = default;

    explicit Constant(const Result &ret) : r(ret)
    { }

    explicit Constant(Result &&ret) : r(std::move(ret))
    { }

    virtual ~Constant() = default;

    virtual const Result &get(double, double)
    { return r; }

    virtual const Result &getConst(double, double) const
    { return r; }

    virtual const Result &get(double)
    { return r; }

    virtual const Result &getConst(double) const
    { return r; }

    virtual DynamicPrimitive<Result> *clone() const
    { return new Constant(r); }

    friend QDataStream &operator<<<>(QDataStream &stream, const DynamicPrimitive<Result> *to);

    friend QDataStream &operator>><>(QDataStream &stream, DynamicPrimitive<Result> *&to);

    friend QDebug operator<<<>(QDebug stream, const DynamicPrimitive<Result> *to);

    friend QDataStream &operator<<<>(QDataStream &stream,
                                     const std::unique_ptr<DynamicPrimitive<Result>> &to);

    friend QDataStream &operator>><>(QDataStream &stream,
                                     std::unique_ptr<DynamicPrimitive<Result>> &to);

    friend QDebug operator<<<>(QDebug stream, const std::unique_ptr<DynamicPrimitive<Result>> &to);

protected:
    explicit Constant(QDataStream &stream)
    { stream >> r; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << static_cast<quint8>(DynamicPrimitive<Result>::SerialType_Constant) << r;
    }

    virtual void printLog(QDebug &stream) const
    { stream << "DynamicPrimitive(" << r << ")"; }
};

/**
 * An input that returns values dependent on the time range or a default
 * constructed one if none is available.
 */
template<typename Result>
class DynamicPrimitive<Result>::Variable : public DynamicPrimitive<Result> {
    struct Value {
        double start;
        double end;
        Result value;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double s)
        { start = s; }

        inline void setEnd(double e)
        { end = e; }

        Value() : start(FP::undefined()), end(FP::undefined()), value()
        { }

        Value(const Value &other) = default;

        Value &operator=(const Value &other) = default;

        Value(Value &&other) = default;

        Value &operator=(Value &&other) = default;

        Value(const Value &other, double s, double e) : start(s), end(e), value(other.value)
        { }

        Value(const Result &v, double s = FP::undefined(), double e = FP::undefined()) : start(s),
                                                                                         end(e),
                                                                                         value(v)
        { }

        Value(const Value &under, const Value &over, double s, double e) : start(s),
                                                                           end(e),
                                                                           value(over.value)
        { Q_UNUSED(under); }
    };

    std::deque<Value> values;
    Result invalid;

public:
    Variable() = default;

    virtual ~Variable() = default;

    Variable(const Variable &) = default;

    Variable &operator=(const Variable &) = default;

    Variable(Variable &&) = default;

    Variable &operator=(Variable &&) = default;

    virtual const Result &get(double start, double end)
    {
        if (!Range::intersectShift(values, start, end))
            return invalid;
        return values.front().value;
    }

    virtual const Result &getConst(double start, double end) const
    {
        auto i = Range::findIntersecting(values, start, end);
        if (i == values.cend())
            return invalid;
        return i->value;
    }

    virtual const Result &get(double time)
    {
        if (!Range::intersectShift(values, time))
            return invalid;
        return values.front().value;
    }

    virtual const Result &getConst(double time) const
    {
        auto i = Range::findIntersecting(values, time);
        if (i == values.cend())
            return invalid;
        return i->value;
    }

    virtual DynamicPrimitive<Result> *clone() const
    { return new Variable(*this); }

    virtual QSet<double> getChangedPoints() const
    {
        QSet<double> result;
        for (const auto &v : values) {
            if (FP::defined(v.start))
                result |= v.start;
            if (FP::defined(v.end))
                result |= v.end;
        }
        return result;
    }

    /**
     * Clear the output and reset it to nothing set at any times.
     */
    void clear()
    { values.clear(); }

    /**
     * Set the output to the given value for the time range specified.
     * 
     * @param v         the value to set
     * @param start     the start time
     * @param end       the end time
     */
    void set(const Result &v, double start = FP::undefined(), double end = FP::undefined())
    {
        Range::overlayFragmenting(values, Value(v, start, end));
    }

    /**
     * Apply simplification to the value.  This removes unneeded segments
     * that are either outside the time range specified or that are contiguous
     * with the same value.
     * 
     * @param start     the start of the time range
     * @param end       the end of the time range
     */
    void simplify(double start = FP::undefined(), double end = FP::undefined())
    {
        for (auto v = values.begin(); v != values.end();) {
            if (!Range::intersects(start, end, v->start, v->end)) {
                v = values.erase(v);
                continue;
            }
            if (v != values.begin()) {
                auto prior = v - 1;
                Q_ASSERT(FP::defined(v->start));
                Q_ASSERT(FP::defined(prior->end));
                if (prior->end == v->start && prior->value == v->value) {
                    prior->end = v->end;
                    v = values.erase(v);
                    continue;
                }
            }
            ++v;
        }
    }

    /**
     * Return a compressed version of the value, if possible.  If this returns
     * non-null then the caller assumes ownership of the simplified type.
     * 
     * @param start     the start of the range to simplify for
     * @param end       the end of the range to simplify for
     * @return          a new compressed value or null if compression is not possible
     */
    DynamicPrimitive<Result> *compressedType(double start = FP::undefined(),
                                             double end = FP::undefined()) const
    {
        if (values.empty())
            return new Constant;
        if (Range::compareStartEnd(values.front().start, end) >= 0)
            return new Constant;
        if (Range::compareStartEnd(start, values.back().end) >= 0)
            return new Constant;
        for (const auto &check : values) {
            if (Range::compareStart(start, check.start) >= 0 &&
                    Range::compareEnd(end, check.end) <= 0) {
                return new Constant(check.value);
            }
        }
        for (auto prior = values.begin(), v = prior + 1; v != values.end(); prior = v, ++v) {
            if (Range::compareStart(prior->end, start) <= 0 &&
                    Range::compareEnd(v->start, end) >= 0) {
                return new Constant;
            }
        }
        return nullptr;
    }

    friend QDataStream &operator<<<>(QDataStream &stream, const DynamicPrimitive<Result> *to);

    friend QDataStream &operator>><>(QDataStream &stream, DynamicPrimitive<Result> *&to);

    friend QDebug operator<<<>(QDebug stream, const DynamicPrimitive<Result> *to);

    friend QDataStream &operator<<<>(QDataStream &stream,
                                     const std::unique_ptr<DynamicPrimitive<Result>> &to);

    friend QDataStream &operator>><>(QDataStream &stream,
                                     std::unique_ptr<DynamicPrimitive<Result>> &to);

    friend QDebug operator<<<>(QDebug stream, const std::unique_ptr<DynamicPrimitive<Result>> &to);

protected:
    explicit Variable(QDataStream &stream)
    {
        Deserialize::container(stream, values, [&stream]() {
            Value result;
            stream >> result.start >> result.end >> result.value;
            return result;
        });
    }

    virtual void serialize(QDataStream &stream) const
    {
        stream << static_cast<quint8>(DynamicPrimitive<Result>::SerialType_Variable);
        Serialize::container(stream, values, [&stream](const Value &add) {
            stream << add.start << add.end << add.value;
        });
    }

    virtual void printLog(QDebug &stream) const
    {
        stream << "DynamicPrimitive(";
        bool first = true;
        for (const auto &add : values) {
            if (!first)
                stream << ",";
            stream << "[" << Logging::range(add.start, add.end) << "," << add.value << "]";
            first = false;
        }
        stream << ")";
    }
};


template<typename Result>
QDataStream &operator<<(QDataStream &stream, const DynamicPrimitive<Result> *to)
{
    if (!to) {
        stream << static_cast<quint8>(DynamicPrimitive<Result>::SerialType_NULL);
        return stream;
    }
    to->serialize(stream);
    return stream;
}

template<typename Result>
QDataStream &operator>>(QDataStream &stream, DynamicPrimitive<Result> *&to)
{
    quint8 type;
    stream >> type;
    switch (type) {
    case DynamicPrimitive<Result>::SerialType_NULL:
        to = nullptr;
        break;
    case DynamicPrimitive<Result>::SerialType_Constant:
        to = new typename DynamicPrimitive<Result>::Constant(stream);
        break;
    case DynamicPrimitive<Result>::SerialType_Variable:
        to = new typename DynamicPrimitive<Result>::Variable(stream);
        break;
    default:
        to = nullptr;
        Q_ASSERT(false);
        break;
    }
    return stream;
}

template<typename Result>
QDebug operator<<(QDebug stream, const DynamicPrimitive<Result> *to)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    if (!to) {
        stream << "DynamicPrimitive";
    } else {
        to->printLog(stream);
    }
    return stream;
}

template<typename Result>
QDataStream &operator<<(QDataStream &stream, const std::unique_ptr<DynamicPrimitive<Result>> &to)
{
    if (!to) {
        stream << static_cast<quint8>(DynamicPrimitive<Result>::SerialType_NULL);
        return stream;
    }
    to->serialize(stream);
    return stream;
}

template<typename Result>
QDataStream &operator>>(QDataStream &stream, std::unique_ptr<DynamicPrimitive<Result>> &to)
{
    quint8 type;
    stream >> type;
    switch (type) {
    case DynamicPrimitive<Result>::SerialType_NULL:
        to.reset();
        break;
    case DynamicPrimitive<Result>::SerialType_Constant:
        to.reset(new typename DynamicPrimitive<Result>::Constant(stream));
        break;
    case DynamicPrimitive<Result>::SerialType_Variable:
        to.reset(new typename DynamicPrimitive<Result>::Variable(stream));
        break;
    default:
        to.reset();
        Q_ASSERT(false);
        break;
    }
    return stream;
}

template<typename Result>
QDebug operator<<(QDebug stream, const std::unique_ptr<DynamicPrimitive<Result>> &to)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    if (!to) {
        stream << "DynamicPrimitive";
    } else {
        to->printLog(stream);
    }
    return stream;
}

template<typename Result>
template<typename Converter>
DynamicPrimitive<Result> *DynamicPrimitive<Result>::fromConfiguration(Converter converter,
                                                                      const ValueSegment::Transfer &config,
                                                                      const QString &path,
                                                                      double start,
                                                                      double end)
{
    if (config.empty())
        return new DynamicPrimitive<Result>::Constant;

    DynamicPrimitive<Result>::Variable r;
    for (const auto &i : config) {
        r.set(converter(i.getValue().getPath(path)), i.getStart(), i.getEnd());
    }
    r.simplify(start, end);

    auto check = r.compressedType(start, end);
    if (check)
        return check;
    return r.clone();
}

template<typename Result>
template<typename Converter>
DynamicPrimitive<Result> *DynamicPrimitive<Result>::fromConfiguration(Converter converter,
                                                                      SequenceSegment::Transfer &config,
                                                                      const SequenceName &unit,
                                                                      const QString &path,
                                                                      double start,
                                                                      double end)
{
    if (config.empty())
        return new DynamicPrimitive<Result>::Constant;

    DynamicPrimitive<Result>::Variable r;
    for (auto &i : config) {
        r.set(converter(i.getValue(unit).getPath(path)), i.getStart(), i.getEnd());
    }
    r.simplify(start, end);

    auto check = r.compressedType(start, end);
    if (check)
        return check;
    return r.clone();
}

template<typename Result>
template<typename Converter>
DynamicPrimitive<Result> *DynamicPrimitive<Result>::fromConfiguration(Converter converter,
                                                                      const Result &invalid,
                                                                      const ValueSegment::Transfer &config,
                                                                      const QString &path,
                                                                      double start,
                                                                      double end)
{
    if (config.empty())
        return new DynamicPrimitive<Result>::Constant(invalid);

    DynamicPrimitive<Result>::Variable r;
    r.set(invalid);
    for (const auto &i : config) {
        r.set(converter(i.getValue().getPath(path)), i.getStart(), i.getEnd());
    }
    r.simplify(start, end);

    auto check = r.compressedType(start, end);
    if (check)
        return check;
    return r.clone();
}

template<typename Result>
template<typename Converter>
DynamicPrimitive<Result> *DynamicPrimitive<Result>::fromConfiguration(Converter converter,
                                                                      const Result &invalid,
                                                                      SequenceSegment::Transfer &config,
                                                                      const SequenceName &unit,
                                                                      const QString &path,
                                                                      double start,
                                                                      double end)
{
    if (config.empty())
        return new DynamicPrimitive<Result>::Constant(invalid);

    DynamicPrimitive<Result>::Variable r;
    r.set(invalid);
    for (auto &i : config) {
        r.set(converter(i.getValue(unit).getPath(path)), i.getStart(), i.getEnd());
    }
    r.simplify(start, end);

    auto check = r.compressedType(start, end);
    if (check)
        return check;
    return r.clone();
}


typedef DynamicPrimitive<QString> DynamicString;

/** 
 *A list of strings to operate with.
 */
class CPD3DATACORE_EXPORT DynamicStringOption
        : public ComponentOptionBase, virtual public ComponentOptionValueParsable {
Q_OBJECT

    Q_INTERFACES(CPD3::Data::ComponentOptionValueParsable)

    DynamicPrimitive<QString>::Variable operate;

    static inline QString convert(const Variant::Read &v)
    { return v.toQString(); }

public:
    DynamicStringOption(const QString &argumentName,
                        const QString &displayName,
                        const QString &description,
                        const QString &defaultBehavior,
                        int sortPriority = 0);

    DynamicStringOption(const DynamicStringOption &other);

    virtual ~DynamicStringOption();

    void set(const QString &value, double start = FP::undefined(), double end = FP::undefined());

    void clear();

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    DynamicString *getInput() const;

    virtual bool parseFromValue(const Variant::Read &value);

    /**
     * Create a primitive input from a configuration.
     * 
     * @param config        the configuration
     * @param unit          the unit within the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @return              a new input
     */
    static inline DynamicString *fromConfiguration(SequenceSegment::Transfer &config,
                                                   const SequenceName &unit,
                                                   const QString &path = QString(),
                                                   double start = FP::undefined(),
                                                   double end = FP::undefined())
    {
        return DynamicString::fromConfiguration(convert, config, unit, path, start, end);
    }

    /**
     * Create a primitive input from a configuration.
     * 
     * @param config        the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @return              a new input
     */
    static inline DynamicString *fromConfiguration(const ValueSegment::Transfer &config,
                                                   const QString &path = QString(),
                                                   double start = FP::undefined(),
                                                   double end = FP::undefined())
    {
        return DynamicString::fromConfiguration(convert, config, path, start, end);
    }
};


typedef DynamicPrimitive<bool> DynamicBool;

/** 
 * A list of booleans to operate with.
 */
class CPD3DATACORE_EXPORT DynamicBoolOption
        : public ComponentOptionBase, virtual public ComponentOptionValueParsable {
Q_OBJECT

    Q_INTERFACES(CPD3::Data::ComponentOptionValueParsable)

    DynamicPrimitive<bool>::Variable operate;

    static inline bool convert(const Variant::Read &v)
    { return v.toBool(); }

public:
    DynamicBoolOption(const QString &argumentName,
                      const QString &displayName,
                      const QString &description,
                      const QString &defaultBehavior,
                      int sortPriority = 0);

    DynamicBoolOption(const DynamicBoolOption &other);

    virtual ~DynamicBoolOption();

    void set(bool value, double start = FP::undefined(), double end = FP::undefined());

    void clear();

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    DynamicBool *getInput() const;

    virtual bool parseFromValue(const Variant::Read &value);

    /**
     * Create a primitive input from a configuration.
     * 
     * @param config        the configuration
     * @param unit          the unit within the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @param invalid       the value assigned when there is no configuration
     * @return              a new input
     */
    static inline DynamicBool *fromConfiguration(SequenceSegment::Transfer &config,
                                                 const SequenceName &unit,
                                                 const QString &path = QString(),
                                                 double start = FP::undefined(),
                                                 double end = FP::undefined(),
                                                 bool invalid = false)
    {
        return DynamicBool::fromConfiguration(convert, invalid, config, unit, path, start, end);
    }

    /**
     * Create a primitive input from a configuration.
     * 
     * @param config        the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @param invalid       the value assigned when there is no configuration
     * @return              a new input
     */
    static inline DynamicBool *fromConfiguration(const ValueSegment::Transfer &config,
                                                 const QString &path = QString(),
                                                 double start = FP::undefined(),
                                                 double end = FP::undefined(),
                                                 bool invalid = false)
    {
        return DynamicBool::fromConfiguration(convert, invalid, config, path, start, end);
    }
};


typedef DynamicPrimitive<double> DynamicDouble;

/** 
 * A list of doubles to operate with.
 */
class CPD3DATACORE_EXPORT DynamicDoubleOption
        : public ComponentOptionBase, virtual public ComponentOptionValueParsable {
Q_OBJECT

    Q_INTERFACES(CPD3::Data::ComponentOptionValueParsable)

    DynamicPrimitive<double>::Variable operate;

    static inline double convert(const Variant::Read &v)
    { return v.toDouble(); }

    double max;
    double min;
    bool maxInclusive;
    bool minInclusive;
    bool allowUndefined;
public:
    DynamicDoubleOption(const QString &argumentName,
                        const QString &displayName,
                        const QString &description,
                        const QString &defaultBehavior,
                        int sortPriority = 0);

    DynamicDoubleOption(const DynamicDoubleOption &other);

    virtual ~DynamicDoubleOption();

    void set(double value, double start = FP::undefined(), double end = FP::undefined());

    void clear();

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    DynamicDouble *getInput() const;

    virtual bool parseFromValue(const Variant::Read &value);

    /**
     * Create a primitive input from a configuration.
     * 
     * @param config        the configuration
     * @param unit          the unit within the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @return              a new input
     */
    static inline DynamicDouble *fromConfiguration(SequenceSegment::Transfer &config,
                                                   const SequenceName &unit,
                                                   const QString &path = QString(),
                                                   double start = FP::undefined(),
                                                   double end = FP::undefined())
    {
        return DynamicDouble::fromConfiguration(convert, FP::undefined(), config, unit, path, start,
                                                end);
    }

    /**
     * Create a primitive input from a configuration.
     * 
     * @param config        the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @return              a new input
     */
    static inline DynamicDouble *fromConfiguration(const ValueSegment::Transfer &config,
                                                   const QString &path = QString(),
                                                   double start = FP::undefined(),
                                                   double end = FP::undefined())
    {
        return DynamicDouble::fromConfiguration(convert, FP::undefined(), config, path, start, end);
    }

    void setMaximum(double m, bool inclusive = true);

    void setMinimum(double m, bool inclusive = true);

    double getMaximum() const;

    double getMinimum() const;

    bool getMaximumInclusive() const;

    bool getMinimumInclusive() const;

    void setAllowUndefined(bool a);

    bool getAllowUndefined() const;

    bool isValid(double check) const;
};


typedef DynamicPrimitive<qint64> DynamicInteger;

/** 
 *A list of integer to operate with.
 */
class CPD3DATACORE_EXPORT DynamicIntegerOption
        : public ComponentOptionBase, virtual public ComponentOptionValueParsable {
Q_OBJECT

    Q_INTERFACES(CPD3::Data::ComponentOptionValueParsable)

    DynamicPrimitive<qint64>::Variable operate;

    static inline qint64 convert(const Variant::Read &v)
    { return v.toInt64(); }

    qint64 max;
    qint64 min;
    bool allowUndefined;
public:
    DynamicIntegerOption(const QString &argumentName,
                         const QString &displayName,
                         const QString &description,
                         const QString &defaultBehavior,
                         int sortPriority = 0);

    DynamicIntegerOption(const DynamicIntegerOption &other);

    virtual ~DynamicIntegerOption();

    void set(qint64 value, double start = FP::undefined(), double end = FP::undefined());

    void clear();

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    DynamicInteger *getInput() const;

    virtual bool parseFromValue(const Variant::Read &value);

    /**
     * Create a primitive input from a configuration.
     * 
     * @param config        the configuration
     * @param unit          the unit within the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @return              a new input
     */
    static inline DynamicInteger *fromConfiguration(SequenceSegment::Transfer &config,
                                                    const SequenceName &unit,
                                                    const QString &path = QString(),
                                                    double start = FP::undefined(),
                                                    double end = FP::undefined())
    {
        return DynamicInteger::fromConfiguration(convert, INTEGER::undefined(), config, unit, path,
                                                 start, end);
    }

    /**
     * Create a primitive input from a configuration.
     * 
     * @param config        the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @return              a new input
     */
    static inline DynamicInteger *fromConfiguration(const ValueSegment::Transfer &config,
                                                    const QString &path = QString(),
                                                    double start = FP::undefined(),
                                                    double end = FP::undefined())
    {
        return DynamicInteger::fromConfiguration(convert, INTEGER::undefined(), config, path, start,
                                                 end);
    }

    void setMaximum(qint64 m);

    void setMinimum(qint64 m);

    qint64 getMaximum() const;

    qint64 getMinimum() const;

    void setAllowUndefined(bool a);

    bool getAllowUndefined() const;

    bool isValid(qint64 check) const;
};


typedef DynamicPrimitive<Calibration> DynamicCalibration;

/** 
 *A list of strings to operate with.
 */
class CPD3DATACORE_EXPORT DynamicCalibrationOption
        : public ComponentOptionBase, virtual public ComponentOptionValueParsable {
Q_OBJECT

    Q_INTERFACES(CPD3::Data::ComponentOptionValueParsable)

    DynamicPrimitive<Calibration>::Variable operate;

    static inline Calibration convert(const Variant::Read &v)
    { return Variant::Composite::toCalibration(v); }

public:
    DynamicCalibrationOption(const QString &argumentName,
                             const QString &displayName,
                             const QString &description,
                             const QString &defaultBehavior,
                             int sortPriority = 0);

    DynamicCalibrationOption(const DynamicCalibrationOption &other);

    virtual ~DynamicCalibrationOption();

    void set(const Calibration &value,
             double start = FP::undefined(),
             double end = FP::undefined());

    void clear();

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    DynamicCalibration *getInput() const;

    virtual bool parseFromValue(const Variant::Read &value);

    /**
     * Create a primitive input from a configuration.
     * 
     * @param config        the configuration
     * @param unit          the unit within the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @return              a new input
     */
    static inline DynamicCalibration *fromConfiguration(SequenceSegment::Transfer &config,
                                                        const SequenceName &unit,
                                                        const QString &path = QString(),
                                                        double start = FP::undefined(),
                                                        double end = FP::undefined())
    {
        return DynamicCalibration::fromConfiguration(convert, config, unit, path, start, end);
    }

    /**
     * Create a primitive input from a configuration.
     * 
     * @param config        the configuration
     * @param path          the path within the configuration
     * @param start         the start of the effective time
     * @param end           the end of the effective time
     * @return              a new input
     */
    static inline DynamicCalibration *fromConfiguration(const ValueSegment::Transfer &config,
                                                        const QString &path = QString(),
                                                        double start = FP::undefined(),
                                                        double end = FP::undefined())
    {
        return DynamicCalibration::fromConfiguration(convert, config, path, start, end);
    }

    bool isValid(const Calibration &cal) const;
};


}
}

#endif
