/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <utility>
#include <cctype>

#include <QLoggingCategory>
#include <QRegularExpression>
#include <QSettings>
#include <QDir>

#include "stream.hxx"
#include "archive/access.hxx"
#include "variant/serialization.hxx"
#include "core/util.hxx"


Q_LOGGING_CATEGORY(log_datacore_stream, "cpd3.datacore.stream", QtWarningMsg)

namespace CPD3 {
namespace Data {

std::size_t SequenceName::FlavorsHash::operator()(const SequenceName::Flavors &flavors) const
{
    /* Just XOR so order doesn't matter */
    std::size_t result = 0;
    for (const auto &add : flavors) {
        result ^= std::hash<std::string>()(add);
    }
    return result;
}

SequenceName::SequenceName() : hashValue(0)
{ }

void SequenceName::rehash()
{
    {
        auto len = station.length();
        if (len > 127) {
            qCWarning(log_datacore_stream) << "Truncating overly long station" << station;
            station.resize(127);
            hashValue = std::hash<Component>()(station);
        } else if (len != 0) {
            hashValue = std::hash<Component>()(station);
        } else {
            hashValue = 0;
        }
    }
    {
        auto len = archive.length();
        if (len > 127) {
            qCWarning(log_datacore_stream) << "Truncating overly long archive" << archive;
            archive.resize(127);
            hashValue = INTEGER::mix(hashValue, std::hash<Component>()(archive));
        } else if (len != 0) {
            hashValue = INTEGER::mix(hashValue, std::hash<Component>()(archive));
        }
    }
    {
        auto len = variable.length();
        if (len > 127) {
            qCWarning(log_datacore_stream) << "Truncating overly long variable" << variable;
            variable.resize(127);
            hashValue = INTEGER::mix(hashValue, std::hash<Component>()(variable));
        } else if (len != 0) {
            hashValue = INTEGER::mix(hashValue, std::hash<Component>()(variable));
        }
    }
    {
        auto len = flavors.length();
        if (len > 127) {
            qCWarning(log_datacore_stream) << "Truncating overly long flavors" << flavors;
            flavors.resize(127);
            hashValue = INTEGER::mix(hashValue, std::hash<Component>()(flavors));
        } else if (len != 0) {
            hashValue = INTEGER::mix(hashValue, std::hash<Component>()(flavors));
        }
    }
}

SequenceName::Flavors SequenceName::toFlavors(const Variant::Read &input)
{
    Flavors result;
    for (const auto &add : input.toChildren().keys()) {
        if (add.empty())
            continue;
        result.insert(add);
    }
    return result;
}

void SequenceName::swap(SequenceName &a, SequenceName &b)
{
    using std::swap;
    swap(a.hashValue, b.hashValue);
    swap(a.station, b.station);
    swap(a.archive, b.archive);
    swap(a.variable, b.variable);
    swap(a.flavors, b.flavors);
}

SequenceName::SequenceName(const SequenceName &other, bool) : station(other.station),
                                                              archive(other.archive),
                                                              variable(other.variable)
{ }

SequenceName::SequenceName(const SequenceIdentity &identity) : SequenceName(identity.name)
{ }

SequenceName::SequenceName(SequenceIdentity &&identity) : SequenceName(std::move(identity.name))
{ }

static inline SequenceName::Component makeLowerCase(const SequenceName::Component &flavor)
{ return Util::to_lower(flavor); }

static inline SequenceName::Component makeLowerCase(SequenceName::Component &&flavor)
{ return Util::to_lower(std::move(flavor)); }

SequenceName::SequenceName(const Component &station,
                           const Component &archive,
                           const Component &variable) : station(makeLowerCase(station)),
                                                        archive(makeLowerCase(archive)),
                                                        variable(variable)
{ rehash(); }

SequenceName::SequenceName(Component &&station, Component &&archive, Component &&variable)
        : station(makeLowerCase(std::move(station))),
          archive(makeLowerCase(std::move(archive))),
          variable(std::move(variable))
{ rehash(); }

static bool isValidFlavor(const SequenceName::Component &flavor)
{ return !Util::contains(flavor, ' '); }

void SequenceName::assembleFlavors(const Flavors &flavors)
{
    this->flavors.clear();

    if (flavors.empty())
        return;

    std::vector<Component> sortFlavors;
    for (const auto &add : flavors) {
        if (!isValidFlavor(add)) {
            qCWarning(log_datacore_stream) << "Omitting invalid flavor" << add;
            continue;
        }
        sortFlavors.emplace_back(makeLowerCase(add));
    }
    std::sort(sortFlavors.begin(), sortFlavors.end());

    for (auto &add : sortFlavors) {
        if (!this->flavors.empty()) {
            this->flavors += ' ';
        }
        this->flavors += std::move(add);
    }
}

void SequenceName::assembleFlavors(Flavors &&flavors)
{
    this->flavors.clear();

    if (flavors.empty())
        return;

    std::vector<Component> sortFlavors;
    for (auto &add : flavors) {
        if (!isValidFlavor(add)) {
            qCWarning(log_datacore_stream) << "Omitting invalid flavor" << add;
            continue;
        }
        sortFlavors.emplace_back(makeLowerCase(std::move(add)));
    }
    std::sort(sortFlavors.begin(), sortFlavors.end());

    for (auto &add : sortFlavors) {
        if (!this->flavors.empty()) {
            this->flavors += ' ';
        }
        this->flavors += std::move(add);
    }
}

SequenceName::SequenceName(const Component &station,
                           const Component &archive,
                           const Component &variable,
                           const Flavors &flavors) : station(makeLowerCase(station)),
                                                     archive(makeLowerCase(archive)),
                                                     variable(variable)
{
    assembleFlavors(flavors);
    rehash();
}

SequenceName::SequenceName(Component &&station,
                           Component &&archive,
                           Component &&variable,
                           Flavors &&flavors) : station(makeLowerCase(std::move(station))),
                                                archive(makeLowerCase(std::move(archive))),
                                                variable(std::move(variable))
{
    assembleFlavors(std::move(flavors));
    rehash();
}

static const SequenceName::Component defaultStation = "_";

bool SequenceName::isDefaultStation() const
{ return station == defaultStation; }

SequenceName SequenceName::toDefaultStation() const
{
    if (isDefaultStation())
        return *this;
    SequenceName result = *this;
    result.setStation(defaultStation);
    return result;
}

static const SequenceName::Component metaSuffix = "_meta";

bool SequenceName::isMeta(const Component &archive)
{ return Util::ends_with(archive, metaSuffix); }

bool SequenceName::isMeta() const
{ return isMeta(archive); }

SequenceName SequenceName::toMeta() const
{
    if (isMeta())
        return *this;
    SequenceName result = *this;
    result.archive += metaSuffix;
    result.rehash();
    return result;
}

SequenceName SequenceName::fromMeta() const
{
    if (!isMeta())
        return *this;
    SequenceName result = *this;
    result.archive.resize(result.archive.length() - metaSuffix.length());
    result.rehash();
    return result;
}

void SequenceName::setStation(const Component &set)
{
    station = makeLowerCase(set);
    rehash();
}

void SequenceName::setArchive(const Component &set)
{
    archive = makeLowerCase(set);
    rehash();
}

void SequenceName::setVariable(const Component &set)
{
    variable = set;
    rehash();
}

void SequenceName::setFlavors(const Flavors &set)
{
    assembleFlavors(set);
    rehash();
}

void SequenceName::setFlavorsString(const Component &set)
{
    flavors = makeLowerCase(set);
    rehash();
}

void SequenceName::setStation(Component &&set)
{
    station = makeLowerCase(std::move(set));
    rehash();
}

void SequenceName::setArchive(Component &&set)
{
    archive = makeLowerCase(std::move(set));
    rehash();
}

void SequenceName::setVariable(Component &&set)
{
    variable = std::move(set);
    rehash();
}

void SequenceName::setFlavors(Flavors &&set)
{
    assembleFlavors(std::move(set));
    rehash();
}

void SequenceName::setFlavorsString(Component &&set)
{
    flavors = makeLowerCase(std::move(set));
    rehash();
}

SequenceName::Flavors SequenceName::getFlavors() const
{
    Flavors result;
    if (flavors.empty())
        return result;

    for (auto begin = flavors.cbegin(), end = flavors.cend();;) {
        auto next = std::find(begin, end, ' ');
        Q_ASSERT(next != begin);

        Component add;
        std::copy(begin, next, std::back_inserter(add));
        result.insert(std::move(add));

        if (next == end)
            break;
        begin = next;
        ++begin;
    }
    return result;
}

QSet<QString> SequenceName::getFlavorsQString() const
{
    QSet<QString> result;
    if (flavors.empty())
        return result;

    for (auto begin = flavors.cbegin(), end = flavors.cend();;) {
        auto next = std::find(begin, end, ' ');
        Q_ASSERT(next != begin);

        result.insert(QString::fromUtf8(&(*begin), static_cast<int>(next - begin)));

        if (next == end)
            break;
        begin = next;
        ++begin;
    }
    return result;
}

bool SequenceName::hasFlavor(const Component &flavor) const
{
    /* Since flavors are sorted, this is a match if the first one is the entire flavor */
    auto begin = flavors.begin();
    auto end = flavors.end();
    auto check =
            std::search(begin, end, flavor.begin(), flavor.end(), [](char cA, char cB) -> bool {
                return cA == std::tolower(cB);
            });
    if (check == end)
        return false;

    /* Require this to be the first flavor, or preceded by a space */
    if (check != begin) {
        if (*(check - 1) != ' ') {
            return false;
        }
    }

    /* Require this to end at the end of all flavors, or on a space */
    check += flavor.size();
    if (check == end)
        return true;
    return *check == ' ';
}

bool SequenceName::lacksFlavor(const Component &flavor) const
{ return !hasFlavor(flavor); }

static std::vector<SequenceName::Component> breakdownFlavors(const SequenceName::Component &flavors)
{
    std::vector<SequenceName::Component> result;
    for (auto begin = flavors.cbegin(), end = flavors.cend();;) {
        auto next = std::find(begin, end, ' ');
        Q_ASSERT(next != begin);

        SequenceName::Component add;
        std::copy(begin, next, std::back_inserter(add));
        result.emplace_back(std::move(add));

        if (next == end)
            break;
        begin = next;
        ++begin;
    }
    return result;
}

SequenceName SequenceName::withFlavor(const Component &flavor) const
{
    if (flavor.empty())
        return *this;
    if (!isValidFlavor(flavor)) {
        qCWarning(log_datacore_stream) << "Ignoring invalid flavor" << flavor;
        return *this;
    }

    SequenceName result(*this, true);
    if (flavors.empty()) {
        result.flavors = makeLowerCase(flavor);
        result.rehash();
        return result;
    }

    auto sortFlavors = breakdownFlavors(flavors);
    auto nf = makeLowerCase(flavor);
    auto pos = std::lower_bound(sortFlavors.begin(), sortFlavors.end(), nf);
    if (pos == sortFlavors.end()) {
        sortFlavors.emplace_back(std::move(nf));
    } else if (*pos == nf) {
        result.flavors = flavors;
        result.hashValue = hashValue;
        return result;
    } else {
        sortFlavors.insert(pos, nf);
    }

    Q_ASSERT(result.flavors.empty());
    for (auto &add : sortFlavors) {
        if (!result.flavors.empty()) {
            result.flavors += ' ';
        }
        result.flavors += std::move(add);
    }

    result.rehash();
    return result;
}

SequenceName SequenceName::withoutFlavor(const Component &flavor) const
{
    if (flavor.empty())
        return *this;
    if (flavors.empty())
        return *this;
    if (!isValidFlavor(flavor)) {
        qCWarning(log_datacore_stream) << "Ignoring invalid flavor" << flavor;
        return *this;
    }

    auto sortFlavors = breakdownFlavors(flavors);
    auto nf = makeLowerCase(flavor);
    auto pos = std::lower_bound(sortFlavors.begin(), sortFlavors.end(), nf);
    if (pos == sortFlavors.end() || *pos != nf) {
        return *this;
    }
    sortFlavors.erase(pos);

    SequenceName result(*this, true);
    Q_ASSERT(result.flavors.empty());
    for (auto &add : sortFlavors) {
        if (!result.flavors.empty()) {
            result.flavors += ' ';
        }
        result.flavors += std::move(add);
    }

    result.rehash();
    return result;
}

SequenceName SequenceName::withoutAllFlavors() const
{
    if (flavors.empty())
        return *this;
    SequenceName result(*this, true);
    Q_ASSERT(result.flavors.empty());
    result.rehash();
    return result;
}

void SequenceName::removeFlavor(const Component &flavor)
{
    if (flavor.empty())
        return;
    if (flavors.empty())
        return;
    if (!isValidFlavor(flavor)) {
        qCWarning(log_datacore_stream) << "Ignoring invalid flavor" << flavor;
        return;
    }

    auto sortFlavors = breakdownFlavors(flavors);
    auto nf = makeLowerCase(flavor);
    auto pos = std::lower_bound(sortFlavors.begin(), sortFlavors.end(), nf);
    if (pos == sortFlavors.end() || *pos != nf)
        return;
    sortFlavors.erase(pos);

    flavors.clear();
    for (auto &add : sortFlavors) {
        if (!flavors.empty()) {
            flavors += ' ';
        }
        flavors += std::move(add);
    }
    rehash();
}

void SequenceName::removeFlavors(const Flavors &flavors)
{
    if (flavors.empty())
        return;
    if (this->flavors.empty())
        return;

    auto sortFlavors = breakdownFlavors(this->flavors);
    for (const auto &rem : flavors) {
        auto nf = makeLowerCase(rem);
        auto pos = std::lower_bound(sortFlavors.begin(), sortFlavors.end(), nf);
        if (pos == sortFlavors.end() || *pos != nf)
            continue;
        sortFlavors.erase(pos);
    }

    this->flavors.clear();
    for (auto &add : sortFlavors) {
        if (!this->flavors.empty()) {
            this->flavors += ' ';
        }
        this->flavors += std::move(add);
    }
    rehash();
}

void SequenceName::addFlavor(const Component &flavor)
{
    if (flavor.empty())
        return;
    if (!isValidFlavor(flavor)) {
        qCWarning(log_datacore_stream) << "Ignoring invalid flavor" << flavor;
        return;
    }

    if (flavors.empty()) {
        flavors = makeLowerCase(flavor);
        rehash();
        return;
    }

    auto sortFlavors = breakdownFlavors(flavors);
    auto nf = makeLowerCase(flavor);
    auto pos = std::lower_bound(sortFlavors.begin(), sortFlavors.end(), nf);
    if (pos == sortFlavors.end()) {
        sortFlavors.emplace_back(std::move(nf));
    } else if (*pos == nf) {
        return;
    } else {
        sortFlavors.insert(pos, nf);
    }

    this->flavors.clear();
    for (auto &add : sortFlavors) {
        if (!this->flavors.empty()) {
            this->flavors += ' ';
        }
        this->flavors += std::move(add);
    }
    rehash();
}

void SequenceName::addFlavors(const Flavors &flavors)
{
    if (flavors.empty())
        return;

    if (this->flavors.empty()) {
        assembleFlavors(flavors);
        rehash();
        return;
    }

    auto sortFlavors = breakdownFlavors(this->flavors);
    for (const auto &add : flavors) {
        auto nf = makeLowerCase(add);
        auto pos = std::lower_bound(sortFlavors.begin(), sortFlavors.end(), nf);
        if (pos == sortFlavors.end()) {
            sortFlavors.emplace_back(std::move(nf));
        } else if (*pos == nf) {
            continue;
        } else {
            sortFlavors.insert(pos, nf);
        }
    }

    this->flavors.clear();
    for (auto &add : sortFlavors) {
        if (!this->flavors.empty()) {
            this->flavors += ' ';
        }
        this->flavors += std::move(add);
    }
    rehash();
}

void SequenceName::clearFlavors()
{
    if (flavors.empty())
        return;
    flavors.clear();
    rehash();
}

void SequenceName::clearMeta()
{
    if (!isMeta())
        return;
    archive.resize(archive.length() - metaSuffix.length());
    rehash();
}

void SequenceName::setMeta()
{
    if (isMeta())
        return;
    archive += metaSuffix;
    rehash();
}

QString SequenceName::describe() const
{
    QString result;
    result += QString::fromStdString(station);
    result += ':';
    result += QString::fromStdString(archive);
    result += ':';
    result += QString::fromStdString(variable);
    result += ':';
    for (auto add : flavors) {
        if (add == ' ')
            add = ':';
        result += add;
    }
    return result;
}

bool SequenceName::operator==(const SequenceName &other) const
{
    return variable == other.variable &&
            flavors == other.flavors &&
            archive == other.archive &&
            station == other.station;
}

bool SequenceName::operator!=(const SequenceName &other) const
{
    return variable != other.variable ||
            flavors != other.flavors ||
            archive != other.archive ||
            station != other.station;
}

bool SequenceName::OrderLogical::operator()(const SequenceName &a, const SequenceName &b) const
{
    bool i1DefaultStation = a.isDefaultStation();
    bool i2DefaultStation = b.isDefaultStation();
    if (i1DefaultStation && !i2DefaultStation)
        return true;
    else if (i2DefaultStation && !i1DefaultStation)
        return false;

#if 0
    bool i1Meta = a.isMeta();
    bool i2Meta = b.isMeta();
    if (i1Meta && !i2Meta)
        return true;
    else if (i2Meta && !i1Meta)
        return false;
#endif

    int c = a.getStation().compare(b.getStation());
    if (c != 0)
        return c < 0;
    c = a.getArchive().compare(b.getArchive());
    if (c != 0)
        return c < 0;
    c = a.getVariable().compare(b.getVariable());
    if (c != 0)
        return c < 0;
    c = a.getFlavorsString().compare(b.getFlavorsString());
    if (c != 0)
        return c < 0;
    return false;
}

static int getVariableColorOffset(const QStringList &cap, int base, int start)
{
    if (cap.at(start) == "B" || cap.at(start) == "BG")
        return base;
    if (cap.at(start) == "G" || cap.at(start) == "BR")
        return base + 1;
    if (cap.at(start) == "R" || cap.at(start) == "GR")
        return base + 2;
    if (cap.at(start) == "Q")
        return base + 3;
    return base + 4;
}

static int getBaseVariablePriority(const QString &variable)
{
    /* Flags */
    {
        static thread_local QRegularExpression check("^F\\d*_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return 0;
        }
    }

    /* Extensives */
    {
        static thread_local QRegularExpression check("^N\\d*_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return 1;
        }
    }
    {
        static thread_local QRegularExpression check("^Bs([0-9BGRQ])_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return getVariableColorOffset(r.capturedTexts(), 10, 1);
        }
    }
    {
        static thread_local QRegularExpression check("^Bbs([0-9BGRQ])_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return getVariableColorOffset(r.capturedTexts(), 20, 1);
        }
    }
    {
        static thread_local QRegularExpression check("^Ba([0-9BGRQ])_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return getVariableColorOffset(r.capturedTexts(), 30, 1);
        }
    }

    /* General instrument temperature pressure (usually neph) */
    {
        static thread_local QRegularExpression check("^T(u?)_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            QStringList cap(r.capturedTexts());
            if (cap.at(1) == "u")
                return 3000;
            return 3001;
        }
    }
    {
        static thread_local QRegularExpression check("^U(u?)_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            QStringList cap(r.capturedTexts());
            if (cap.at(1) == "u")
                return 3002;
            return 3003;
        }
    }
    {
        static thread_local QRegularExpression check("^P_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return 3004;
        }
    }

    /* Intensives */
    {
        static thread_local QRegularExpression check("^Be([0-9BGRQ])_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return getVariableColorOffset(r.capturedTexts(), 900, 1);
        }
    }
    {
        static thread_local QRegularExpression check("^ZSSA([0-9BGRQ])_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return getVariableColorOffset(r.capturedTexts(), 910, 1);
        }
    }
    {
        static thread_local QRegularExpression check("^ZBfr([0-9BGRQ])_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return getVariableColorOffset(r.capturedTexts(), 920, 1);
        }
    }
    {
        static thread_local QRegularExpression check("^ZAngB(.?)([0-9BGRQ][BGR]?)_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            QStringList cap(r.capturedTexts());
            if (cap.at(1) == "s")
                return getVariableColorOffset(cap, 930, 2);
            return getVariableColorOffset(cap, 934, 2);
        }
    }
    {
        static thread_local QRegularExpression check("^ZAsyPoly([0-9BGRQ])_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return getVariableColorOffset(r.capturedTexts(), 940, 1);
        }
    }
    {
        static thread_local QRegularExpression check("^ZRFE([0-9BGRQ])_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return getVariableColorOffset(r.capturedTexts(), 950, 1);
        }
    }

    /* Other diagnostics */
    {
        static thread_local QRegularExpression check("^Bsw([0-9BGRQ])_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return getVariableColorOffset(r.capturedTexts(), 1000, 1);
        }
    }
    {
        static thread_local QRegularExpression check("^Bbsw([0-9BGRQ])_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return getVariableColorOffset(r.capturedTexts(), 1010, 1);
        }
    }
    {
        static thread_local QRegularExpression check("^Cc([0-9BGRQ])_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return getVariableColorOffset(r.capturedTexts(), 1020, 1);
        }
    }
    {
        static thread_local QRegularExpression check("^Cbc([0-9BGRQ])_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return getVariableColorOffset(r.capturedTexts(), 1030, 1);
        }
    }

    /* Aeth instance numbering */
    {
        static thread_local QRegularExpression check("^Z?Ir[0-9BGRQ]_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return 99999;
        }
    }
    {
        static thread_local QRegularExpression check("^I[fp]z?[0-9BGRQ]_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return 99999;
        }
    }
    {
        static thread_local QRegularExpression check("^X[0-9BGRQ]c_");
        auto r = check.match(variable);
        if (r.hasMatch()) {
            return 99999;
        }
    }

    return 99999;
}

int SequenceName::OrderDisplay::priority(const Component &variable)
{
    return getBaseVariablePriority(QString::fromStdString(variable));
}

const SequenceName::Component SequenceName::flavor_cover = "cover";
const SequenceName::Component SequenceName::flavor_end = "end";
const SequenceName::Component SequenceName::flavor_stats = "stats";
const SequenceName::Component SequenceName::flavor_pm1 = "pm1";
const SequenceName::Component SequenceName::flavor_pm10 = "pm10";
const SequenceName::Component SequenceName::flavor_pm25 = "pm25";

int SequenceName::OrderDisplay::priority(const SequenceName &name)
{
    int p = priority(name.getVariable());

    if (name.hasFlavor(flavor_pm10) || name.hasFlavor(flavor_pm25))
        p += 1000;
    else if (name.hasFlavor(flavor_pm1))
        p += 2000;
    if (name.hasFlavor(flavor_stats))
        p += 10000;
    else if (name.hasFlavor(flavor_cover))
        p += 20000;

    return p;
}

bool SequenceName::OrderDisplay::operator()(const SequenceName &a, const SequenceName &b) const
{
    int p1 = priority(a);
    int p2 = priority(b);
    if (p1 != p2)
        return p1 < p2;

    int c = a.getVariable().compare(b.getVariable());
    if (c != 0)
        return c < 0;
    c = a.getStation().compare(b.getStation());
    if (c != 0)
        return c < 0;
    c = a.getArchive().compare(b.getArchive());
    if (c != 0)
        return c < 0;
    c = a.getFlavorsString().compare(b.getFlavorsString());
    return c < 0;
}

int SequenceName::OrderDisplayCaching::priority(const SequenceName &v)
{
    auto check = cache.find(v);
    if (check != cache.end())
        return check->second;

    int p = OrderDisplay::priority(v);
    cache.emplace(v, p);
    return p;
}

bool SequenceName::OrderDisplayCaching::operator()(const SequenceName &a, const SequenceName &b)
{
    int p1 = priority(a);
    int p2 = priority(b);
    if (p1 != p2)
        return p1 < p2;

    int c = a.getVariable().compare(b.getVariable());
    if (c != 0)
        return c < 0;
    c = a.getStation().compare(b.getStation());
    if (c != 0)
        return c < 0;
    c = a.getArchive().compare(b.getArchive());
    if (c != 0)
        return c < 0;
    c = a.getFlavorsString().compare(b.getFlavorsString());
    return c < 0;
}

namespace {
class DefaultFlavorStorage {
public:
    SequenceName::Flavors cutSizeFlavors;
    std::unordered_set<SequenceName::Flavors> dataFlavors;
    std::vector<SequenceName::Component> lacksFlavors;

    DefaultFlavorStorage()
    {
        cutSizeFlavors.insert(SequenceName::flavor_pm1);
        cutSizeFlavors.insert(SequenceName::flavor_pm10);
        cutSizeFlavors.insert(SequenceName::flavor_pm25);

        dataFlavors.insert(SequenceName::Flavors());
        dataFlavors.insert(SequenceName::Flavors{SequenceName::flavor_pm1});
        dataFlavors.insert(SequenceName::Flavors{SequenceName::flavor_pm10});
        dataFlavors.insert(SequenceName::Flavors{SequenceName::flavor_pm25});

        lacksFlavors.emplace_back(SequenceName::flavor_cover);
        lacksFlavors.emplace_back(SequenceName::flavor_end);
        lacksFlavors.emplace_back(SequenceName::flavor_stats);
    }
};

Q_GLOBAL_STATIC(DefaultFlavorStorage, defaultFlavorStorage)
}

const SequenceName::Flavors &SequenceName::cutSizeFlavors()
{
    auto d = defaultFlavorStorage();
    if (!d) {
        static Flavors def;
        return def;
    }
    return d->cutSizeFlavors;
}

const std::unordered_set<SequenceName::Flavors> &SequenceName::defaultDataFlavors()
{
    auto d = defaultFlavorStorage();
    if (!d) {
        static std::unordered_set<SequenceName::Flavors> def;
        return def;
    }
    return d->dataFlavors;
}

const std::vector<SequenceName::Component> &SequenceName::defaultLacksFlavors()
{
    auto d = defaultFlavorStorage();
    if (!d) {
        static std::vector<SequenceName::Component> def;
        return def;
    }
    return d->lacksFlavors;
}

SequenceName::Component SequenceName::impliedStation(Archive::Access *archive)
{
    std::unique_ptr<Archive::Access> localAccess;
    if (!archive) {
        localAccess.reset(new Archive::Access);
        archive = localAccess.get();
    }

    Archive::Access::ReadLock lock(*archive);

    {
        auto check = qgetenv("CPD3STATION");
        if (check.isEmpty())
            check = qgetenv("MYSTN");
        while (!check.isEmpty() && check.at(0) == ' ') {
            check.remove(0, 1);
        }
        if (!check.isEmpty()) {
            int end = check.indexOf(' ');
            if (end != -1) {
                check = check.mid(0, end);
            }
            if (!check.isEmpty()) {
                auto result = archive->availableStations({check.toStdString()});
                if (result.size() == 1)
                    return *(result.begin());
            }
        }
    }

    {
        QSettings settings("NOAA ESRL GMD", "CPD3Default");
        auto check = settings.value("station").toString();
        while (!check.isEmpty() && check.at(0) == ' ') {
            check.remove(0, 1);
        }
        if (!check.isEmpty()) {
            int end = check.indexOf(' ');
            if (end != -1) {
                check = check.mid(0, end);
            }
            if (!check.isEmpty()) {
                auto result = archive->availableStations({check.toStdString()});
                if (result.size() == 1)
                    return *(result.begin());
            }
        }
    }

    {
        auto cwdPath = QDir().absolutePath().split('/');
        for (int i = cwdPath.size() - 1; i >= 0; i--) {
            const auto &check = cwdPath.at(i);
            if (check.isEmpty())
                continue;
            auto result = archive->availableStations({check.toStdString()});
            if (result.size() == 1)
                return *(result.begin());
        }
    }

    return {};
}

SequenceName::Component SequenceName::impliedUncheckedStation()
{
    {
        auto check = qgetenv("CPD3STATION");
        if (check.isEmpty())
            check = qgetenv("MYSTN");
        while (!check.isEmpty() && check.at(0) == ' ') {
            check.remove(0, 1);
        }
        if (!check.isEmpty()) {
            int end = check.indexOf(' ');
            if (end != -1) {
                check = check.mid(0, end);
            }
            if (!check.isEmpty()) {
                return check.toStdString();
            }
        }
    }

    {
        QSettings settings("NOAA ESRL GMD", "CPD3Default");
        auto check = settings.value("station").toString();
        while (!check.isEmpty() && check.at(0) == ' ') {
            check.remove(0, 1);
        }
        if (!check.isEmpty()) {
            int end = check.indexOf(' ');
            if (end != -1) {
                check = check.mid(0, end);
            }
            if (!check.isEmpty()) {
                return check.toStdString();
            }
        }
    }

    return {};
}

SequenceName::ComponentSet SequenceName::impliedStations(Archive::Access *archive)
{
    std::unique_ptr<Archive::Access> localAccess;
    if (!archive) {
        localAccess.reset(new Archive::Access);
        archive = localAccess.get();
    }

    Archive::Access::ReadLock lock(*archive);

    {
        auto check = qgetenv("CPD3STATION");
        if (check.isEmpty())
            check = qgetenv("MYSTN");
        while (!check.isEmpty() && check.at(0) == ' ') {
            check.remove(0, 1);
        }
        if (!check.isEmpty()) {
            auto stations = Util::split_string(check.toStdString(), ' ');
            if (!stations.empty()) {
                auto result = archive->availableStations(stations);
                if (!result.empty())
                    return result;
            }
        }
    }

    {
        QSettings settings("NOAA ESRL GMD", "CPD3Default");
        auto check = settings.value("station").toString();
        while (!check.isEmpty() && check.at(0) == ' ') {
            check.remove(0, 1);
        }
        if (!check.isEmpty()) {
            auto stations = Util::split_string(check.toStdString(), ' ');
            if (!stations.empty()) {
                auto result = archive->availableStations(stations);
                if (!result.empty())
                    return result;
            }
        }
    }

    {
        auto cwdPath = QDir().absolutePath().split('/');
        for (int i = cwdPath.size() - 1; i >= 0; i--) {
            const auto &check = cwdPath.at(i);
            if (check.isEmpty())
                continue;
            auto result = archive->availableStations({check.toStdString()});
            if (result.size() == 1)
                return result;
        }
    }

    return {};
}

SequenceName::ComponentSet SequenceName::impliedUncheckedStations()
{
    {
        auto check = qgetenv("CPD3STATION");
        if (check.isEmpty())
            check = qgetenv("MYSTN");
        while (!check.isEmpty() && check.at(0) == ' ') {
            check.remove(0, 1);
        }
        if (!check.isEmpty()) {
            auto stations = Util::split_string(check.toStdString(), ' ');
            if (!stations.empty()) {
                return SequenceName::ComponentSet(stations.begin(), stations.end());
            }
        }
    }

    {
        QSettings settings("NOAA ESRL GMD", "CPD3Default");
        auto check = settings.value("station").toString();
        while (!check.isEmpty() && check.at(0) == ' ') {
            check.remove(0, 1);
        }
        if (!check.isEmpty()) {
            auto stations = Util::split_string(check.toStdString(), ' ');
            if (!stations.empty()) {
                return SequenceName::ComponentSet(stations.begin(), stations.end());
            }
        }
    }

    return {};
}

std::string SequenceName::impliedProfile(Archive::Access *)
{
    QSettings settings("NOAA ESRL GMD", "CPD3Default");
    auto check = settings.value("profile").toString().toLower();
    if (!check.isEmpty())
        return check.toStdString();
    return "aerosol";
}

QDataStream &operator<<(QDataStream &stream, const SequenceName &value)
{
    Variant::Serialization::serializeShortString(stream, value.station);
    Variant::Serialization::serializeShortString(stream, value.archive);
    Variant::Serialization::serializeShortString(stream, value.variable);
    Variant::Serialization::serializeShortString(stream, value.flavors);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, SequenceName &value)
{
    value.station = Variant::Serialization::deserializeShortString(stream);
    value.archive = Variant::Serialization::deserializeShortString(stream);
    value.variable = Variant::Serialization::deserializeShortString(stream);
    value.flavors = Variant::Serialization::deserializeShortString(stream);
    value.rehash();
    return stream;
}

QDebug operator<<(QDebug stream, const SequenceName &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << '{' << v.describe().toUtf8().constData() << '}';
    return stream;
}


SequenceIdentity::SequenceIdentity()
        : name(), start(FP::undefined()), end(FP::undefined()), priority(0)
{ }

void SequenceIdentity::swap(SequenceIdentity &a, SequenceIdentity &b)
{
    SequenceName::swap(a.name, b.name);
    using std::swap;
    swap(a.start, b.start);
    swap(a.end, b.end);
    swap(a.priority, b.priority);
}

SequenceIdentity::SequenceIdentity(const SequenceName &name) : name(name),
                                                               start(FP::undefined()),
                                                               end(FP::undefined()),
                                                               priority(0)
{ }

SequenceIdentity::SequenceIdentity(SequenceName &&name) : name(std::move(name)),
                                                          start(FP::undefined()),
                                                          end(FP::undefined()),
                                                          priority(0)
{ }

SequenceIdentity::SequenceIdentity(const SequenceValue &value) : SequenceIdentity(value.identity)
{ }

SequenceIdentity::SequenceIdentity(SequenceValue &&value) : SequenceIdentity(
        std::move(value.identity))
{ }

SequenceIdentity::SequenceIdentity(const ArchiveValue &value) : SequenceIdentity(value.identity)
{ }

SequenceIdentity::SequenceIdentity(ArchiveValue &&value) : SequenceIdentity(
        std::move(value.identity))
{ }

SequenceIdentity::SequenceIdentity(const ArchiveErasure &value) : SequenceIdentity(value.identity)
{ }

SequenceIdentity::SequenceIdentity(ArchiveErasure &&value) : SequenceIdentity(
        std::move(value.identity))
{ }

SequenceIdentity::SequenceIdentity(const SequenceName &name, double start, double end, int priority)
        : name(name), start(start), end(end), priority(priority)
{ }

SequenceIdentity::SequenceIdentity(SequenceName &&name, double start, double end, int priority)
        : name(std::move(name)), start(start), end(end), priority(priority)
{ }

SequenceIdentity::SequenceIdentity(const SequenceName::Component &station,
                                   const SequenceName::Component &archive,
                                   const SequenceName::Component &variable) : name(station, archive,
                                                                                   variable),
                                                                              start(FP::undefined()),
                                                                              end(FP::undefined()),
                                                                              priority(0)
{ }

SequenceIdentity::SequenceIdentity(SequenceName::Component &&station,
                                   SequenceName::Component &&archive,
                                   SequenceName::Component &&variable) : name(std::move(station),
                                                                              std::move(archive),
                                                                              std::move(variable)),
                                                                         start(FP::undefined()),
                                                                         end(FP::undefined()),
                                                                         priority(0)
{ }

SequenceIdentity::SequenceIdentity(const SequenceName::Component &station,
                                   const SequenceName::Component &archive,
                                   const SequenceName::Component &variable,
                                   const SequenceName::Flavors &flavors) : name(station, archive,
                                                                                variable, flavors),
                                                                           start(FP::undefined()),
                                                                           end(FP::undefined()),
                                                                           priority(0)
{ }

SequenceIdentity::SequenceIdentity(SequenceName::Component &&station,
                                   SequenceName::Component &&archive,
                                   SequenceName::Component &&variable,
                                   SequenceName::Flavors &&flavors) : name(std::move(station),
                                                                           std::move(archive),
                                                                           std::move(variable),
                                                                           std::move(flavors)),
                                                                      start(FP::undefined()),
                                                                      end(FP::undefined()),
                                                                      priority(0)
{ }

SequenceIdentity::SequenceIdentity(const SequenceName::Component &station,
                                   const SequenceName::Component &archive,
                                   const SequenceName::Component &variable,
                                   double start,
                                   double end,
                                   int priority) : name(station, archive, variable),
                                                   start(start),
                                                   end(end),
                                                   priority(priority)
{ }

SequenceIdentity::SequenceIdentity(SequenceName::Component &&station,
                                   SequenceName::Component &&archive,
                                   SequenceName::Component &&variable,
                                   double start,
                                   double end,
                                   int priority) : name(std::move(station), std::move(archive),
                                                        std::move(variable)),
                                                   start(start),
                                                   end(end),
                                                   priority(priority)
{ }

SequenceIdentity::SequenceIdentity(const SequenceName::Component &station,
                                   const SequenceName::Component &archive,
                                   const SequenceName::Component &variable,
                                   const SequenceName::Flavors &flavors,
                                   double start,
                                   double end,
                                   int priority) : name(station, archive, variable, flavors),
                                                   start(start),
                                                   end(end),
                                                   priority(priority)
{ }

SequenceIdentity::SequenceIdentity(SequenceName::Component &&station,
                                   SequenceName::Component &&archive,
                                   SequenceName::Component &&variable,
                                   SequenceName::Flavors &&flavors,
                                   double start,
                                   double end,
                                   int priority) : name(std::move(station), std::move(archive),
                                                        std::move(variable), std::move(flavors)),
                                                   start(start),
                                                   end(end),
                                                   priority(priority)
{ }

bool SequenceIdentity::operator==(const SequenceIdentity &other) const
{
    return FP::equal(start, other.start) &&
            FP::equal(end, other.end) &&
            priority == other.priority &&
            name == other.name;
}

bool SequenceIdentity::operator!=(const SequenceIdentity &other) const
{
    return !FP::equal(start, other.start) ||
            !FP::equal(end, other.end) ||
            priority != other.priority ||
            name != other.name;
}

bool SequenceIdentity::OrderTime::operator()(const SequenceIdentity &a,
                                             const SequenceIdentity &b) const
{
    if (!FP::defined(a.getStart())) {
        if (FP::defined(b.getStart()))
            return true;
    } else if (!FP::defined(b.getStart())) {
        return false;
    } else if (a.getStart() != b.getStart()) {
        return a.getStart() < b.getStart();
    }

    if (a.getPriority() != b.getPriority())
        return a.getPriority() < b.getPriority();

    if (!FP::defined(a.getEnd())) {
        if (FP::defined(b.getEnd()))
            return true;
    } else if (!FP::defined(b.getEnd())) {
        return false;
    } else if (a.getEnd() != b.getEnd()) {
        return a.getEnd() < b.getEnd();
    }

    return SequenceName::OrderLogical()(a.getName(), b.getName());
}

bool SequenceIdentity::OrderOverlay::operator()(const SequenceIdentity &a,
                                                const SequenceIdentity &b) const
{
    bool i1DefaultStation = a.getName().isDefaultStation();
    bool i2DefaultStation = b.getName().isDefaultStation();
    if (i1DefaultStation && !i2DefaultStation)
        return true;
    else if (i2DefaultStation && !i1DefaultStation)
        return false;

    if (a.getPriority() != b.getPriority())
        return a.getPriority() < b.getPriority();

    if (!FP::defined(a.getStart())) {
        if (FP::defined(b.getStart()))
            return true;
    } else if (!FP::defined(b.getStart())) {
        return false;
    } else if (a.getStart() != b.getStart()) {
        return a.getStart() < b.getStart();
    }

#if 0
    bool i1IsMeta = a.getName().isMeta();
    bool i2IsMeta = b.getName().isMeta();
    if (i1IsMeta && !i2IsMeta)
        return true;
    else if (i2IsMeta && !i1IsMeta)
        return false;
#endif

    return false;
}

QDataStream &operator<<(QDataStream &stream, const SequenceIdentity &value)
{
    stream << value.name << value.start << value.end << static_cast<qint32>(value.priority);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, SequenceIdentity &value)
{
    stream >> value.name >> value.start >> value.end;
    quint32 p = 0;
    stream >> p;
    value.priority = p;
    return stream;
}

QDebug operator<<(QDebug stream, const SequenceIdentity &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << '{' << v.name << ',' << Logging::range(v.start, v.end) << ',' << v.getPriority()
           << '}';
    return stream;
}

uint qHash(const SequenceIdentity &key)
{ return static_cast<uint>(std::hash<SequenceIdentity>()(key)); }


void SequenceValue::swap(SequenceValue &a, SequenceValue &b)
{
    using std::swap;
    swap(a.identity, b.identity);
    swap(a.contents, b.contents);
}

SequenceValue::SequenceValue(const SequenceName &name) : identity(name)
{ }

SequenceValue::SequenceValue(SequenceName &&name) : identity(std::move(name))
{ }

SequenceValue::SequenceValue(const SequenceIdentity &identity) : identity(identity)
{ }

SequenceValue::SequenceValue(SequenceIdentity &&identity) : identity(std::move(identity))
{ }

SequenceValue::SequenceValue(const ArchiveValue &value) : identity(value.identity),
                                                          contents(value.contents)
{ }

SequenceValue::SequenceValue(ArchiveValue &&value) : identity(std::move(value.identity)),
                                                     contents(std::move(value.contents))
{ }

SequenceValue::SequenceValue(const ArchiveErasure &value) : identity(value.identity)
{ }

SequenceValue::SequenceValue(ArchiveErasure &&value) : identity(std::move(value.identity))
{ }

SequenceValue::SequenceValue(const SequenceIdentity &identity, const Variant::Root &value)
        : identity(identity), contents(value)
{ }

SequenceValue::SequenceValue(SequenceIdentity &&identity, const Variant::Root &value) : identity(
        std::move(identity)), contents(value)
{ }

SequenceValue::SequenceValue(const SequenceIdentity &identity, Variant::Root &&value) : identity(
        identity), contents(std::move(value))
{ }

SequenceValue::SequenceValue(SequenceIdentity &&identity, Variant::Root &&value) : identity(
        std::move(identity)), contents(std::move(value))
{ }

SequenceValue::SequenceValue(const SequenceName &name,
                             const Variant::Root &value,
                             double start,
                             double end,
                             int priority) : identity(name, start, end, priority), contents(value)
{ }

SequenceValue::SequenceValue(SequenceName &&name,
                             const Variant::Root &value,
                             double start,
                             double end,
                             int priority) : identity(std::move(name), start, end, priority),
                                             contents(value)
{ }

SequenceValue::SequenceValue(const SequenceName &name, Variant::Root &&value,
                             double start,
                             double end,
                             int priority) : identity(name, start, end, priority),
                                             contents(std::move(value))
{ }

SequenceValue::SequenceValue(SequenceName &&name, Variant::Root &&value,
                             double start,
                             double end,
                             int priority) : identity(std::move(name), start, end, priority),
                                             contents(std::move(value))
{ }

bool SequenceValue::ValueEqual::operator()(const SequenceValue &a, const SequenceValue &b) const
{
    if (a.getIdentity() != b.getIdentity())
        return false;
    return a.read() == b.read();
}

/* Odd ordering due to legacy compat */
QDataStream &operator<<(QDataStream &stream, const SequenceValue &value)
{
    stream << value.identity.getStart();
    stream << value.identity.getEnd();
    stream << value.identity.getName();
    stream << static_cast<qint32>(value.identity.getPriority());
    stream << value.root();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, SequenceValue &value)
{
    double start;
    double end;
    qint32 priority;
    stream >> start >> end >> value.getName() >> priority >> value.root();
    value.identity.setStart(start);
    value.identity.setEnd(end);
    value.identity.setPriority(priority);
    return stream;
}

QDebug operator<<(QDebug stream, const SequenceValue &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "SequenceValue(" << v.identity << ',' << v.root() << ')';
    return stream;
}


ArchiveValue::ArchiveValue() : SequenceValue(), modified(FP::undefined()), remoteReferenced(false)
{ }

void ArchiveValue::swap(ArchiveValue &a, ArchiveValue &b)
{
    SequenceValue::swap(a, b);
    using std::swap;
    swap(a.modified, b.modified);
    swap(a.remoteReferenced, b.remoteReferenced);
}

ArchiveValue::ArchiveValue(const SequenceName &name) : SequenceValue(name),
                                                       modified(FP::undefined()),
                                                       remoteReferenced(false)
{ }

ArchiveValue::ArchiveValue(SequenceName &&name) : SequenceValue(std::move(name)),
                                                  modified(FP::undefined()),
                                                  remoteReferenced(false)
{ }

ArchiveValue::ArchiveValue(const SequenceIdentity &identity) : SequenceValue(identity),
                                                               modified(FP::undefined()),
                                                               remoteReferenced(false)
{ }

ArchiveValue::ArchiveValue(SequenceIdentity &&identity) : SequenceValue(std::move(identity)),
                                                          modified(FP::undefined()),
                                                          remoteReferenced(false)
{ }

ArchiveValue::ArchiveValue(const SequenceValue &value) : SequenceValue(value),
                                                         modified(FP::undefined()),
                                                         remoteReferenced(false)
{ }

ArchiveValue::ArchiveValue(SequenceValue &&value) : SequenceValue(std::move(value)),
                                                    modified(FP::undefined()),
                                                    remoteReferenced(false)
{ }

ArchiveValue::ArchiveValue(const ArchiveErasure &value) : SequenceValue(value),
                                                          modified(value.getModified()),
                                                          remoteReferenced(true)
{ }

ArchiveValue::ArchiveValue(ArchiveErasure &&value) : SequenceValue(std::move(value)),
                                                     modified(value.getModified()),
                                                     remoteReferenced(false)
{ }

ArchiveValue::ArchiveValue(const SequenceIdentity &identity, const Variant::Root &value,
                           double modified,
                           bool remoteReferenced) : SequenceValue(identity, value),
                                                    modified(modified),
                                                    remoteReferenced(remoteReferenced)
{ }

ArchiveValue::ArchiveValue(SequenceIdentity &&identity,
                           const Variant::Root &value,
                           double modified,
                           bool remoteReferenced) : SequenceValue(std::move(identity), value),
                                                    modified(modified),
                                                    remoteReferenced(remoteReferenced)
{ }

ArchiveValue::ArchiveValue(const SequenceIdentity &identity, Variant::Root &&value,
                           double modified,
                           bool remoteReferenced) : SequenceValue(identity, std::move(value)),
                                                    modified(modified),
                                                    remoteReferenced(remoteReferenced)
{ }

ArchiveValue::ArchiveValue(SequenceIdentity &&identity, Variant::Root &&value,
                           double modified,
                           bool remoteReferenced) : SequenceValue(std::move(identity),
                                                                  std::move(value)),
                                                    modified(modified),
                                                    remoteReferenced(remoteReferenced)
{ }

ArchiveValue::ArchiveValue(const SequenceName &name, const Variant::Root &other,
                           double start,
                           double end,
                           int priority,
                           double modified,
                           bool remoteReferenced) : SequenceValue(name, other, start, end,
                                                                  priority),
                                                    modified(modified),
                                                    remoteReferenced(remoteReferenced)
{ }

ArchiveValue::ArchiveValue(SequenceName &&name,
                           const Variant::Root &other,
                           double start,
                           double end,
                           int priority,
                           double modified,
                           bool remoteReferenced) : SequenceValue(std::move(name), other, start,
                                                                  end, priority),
                                                    modified(modified),
                                                    remoteReferenced(remoteReferenced)
{ }

ArchiveValue::ArchiveValue(const SequenceName &name, Variant::Root &&other,
                           double start,
                           double end,
                           int priority,
                           double modified,
                           bool remoteReferenced) : SequenceValue(name, std::move(other), start,
                                                                  end, priority),
                                                    modified(modified),
                                                    remoteReferenced(remoteReferenced)
{ }

ArchiveValue::ArchiveValue(SequenceName &&name, Variant::Root &&other,
                           double start,
                           double end,
                           int priority,
                           double modified,
                           bool remoteReferenced) : SequenceValue(std::move(name), std::move(other),
                                                                  start, end, priority),
                                                    modified(modified),
                                                    remoteReferenced(remoteReferenced)
{ }

bool ArchiveValue::ExactlyEqual::operator()(const ArchiveValue &a, const ArchiveValue &b)
{
    if (!SequenceValue::ValueEqual()(a, b))
        return false;
    return a.isRemoteReferenced() == b.isRemoteReferenced() &&
            FP::equal(a.getModified(), b.getModified());
}

QDataStream &operator<<(QDataStream &stream, const ArchiveValue &value)
{
    stream << static_cast<const SequenceValue &>(value) << value.modified << value.remoteReferenced;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, ArchiveValue &value)
{
    stream >> static_cast<SequenceValue &>(value) >> value.modified >> value.remoteReferenced;
    return stream;
}

QDebug operator<<(QDebug stream, const ArchiveValue &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "ArchiveValue(" << v.getIdentity() << ',' << v.read() << ",modified="
           << Logging::time(v.modified);
    if (v.remoteReferenced)
        stream << ",REMOTE)";
    else
        stream << ",LOCAL)";
    return stream;
}


ArchiveErasure::ArchiveErasure() : identity(), modified(FP::undefined())
{ }

void ArchiveErasure::swap(ArchiveErasure &a, ArchiveErasure &b)
{
    SequenceIdentity::swap(a.identity, b.identity);
    using std::swap;
    swap(a.modified, b.modified);
}

ArchiveErasure::ArchiveErasure(const SequenceName &name) : identity(name), modified(FP::undefined())
{ }

ArchiveErasure::ArchiveErasure(SequenceName &&name) : identity(std::move(name)),
                                                      modified(FP::undefined())
{ }

ArchiveErasure::ArchiveErasure(const SequenceIdentity &identity) : identity(identity),
                                                                   modified(FP::undefined())
{ }

ArchiveErasure::ArchiveErasure(SequenceIdentity &&identity) : identity(std::move(identity)),
                                                              modified(FP::undefined())
{ }

ArchiveErasure::ArchiveErasure(const SequenceValue &value) : identity(value.identity),
                                                             modified(FP::undefined())
{ }

ArchiveErasure::ArchiveErasure(SequenceValue &&value) : identity(std::move(value.identity)),
                                                        modified(FP::undefined())
{ }

ArchiveErasure::ArchiveErasure(const ArchiveValue &value) : identity(value.identity),
                                                            modified(value.modified)
{ }

ArchiveErasure::ArchiveErasure(ArchiveValue &&value) : identity(std::move(value.identity)),
                                                       modified(value.modified)
{ }

ArchiveErasure::ArchiveErasure(const SequenceIdentity &identity, double modified) : identity(
        identity), modified(modified)
{ }

ArchiveErasure::ArchiveErasure(SequenceIdentity &&identity, double modified) : identity(
        std::move(identity)), modified(modified)
{ }

ArchiveErasure::ArchiveErasure(const SequenceName &name,
                               double start,
                               double end,
                               int priority,
                               double modified) : identity(name, start, end, priority),
                                                  modified(modified)
{ }

ArchiveErasure::ArchiveErasure(SequenceName &&name,
                               double start,
                               double end,
                               int priority,
                               double modified) : identity(std::move(name), start, end, priority),
                                                  modified(modified)
{ }

bool ArchiveErasure::ExactlyEqual::operator()(const ArchiveErasure &a, const ArchiveErasure &b)
{ return a.getIdentity() == b.getIdentity() && FP::equal(a.getModified(), b.getModified()); }

/* Odd ordering due to legacy compat */
QDataStream &operator<<(QDataStream &stream, const ArchiveErasure &value)
{
    stream << value.identity.getStart();
    stream << value.identity.getEnd();
    stream << value.identity.getName();
    stream << static_cast<qint32>(value.identity.getPriority());
    return stream;
}

QDataStream &operator>>(QDataStream &stream, ArchiveErasure &value)
{
    double start;
    double end;
    qint32 priority;
    stream >> start >> end >> value.getName() >> priority;
    value.identity.setStart(start);
    value.identity.setEnd(end);
    value.identity.setPriority(priority);
    return stream;
}

QDebug operator<<(QDebug stream, const ArchiveErasure &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "ArchiveErasure(" << v.getIdentity() << ",modified=" << Logging::time(v.modified)
           << ')';
    return stream;
}


ErasureSink::~ErasureSink() = default;

void ErasureSink::incomingValue(ArchiveValue &&value)
{ return this->incomingValue(static_cast<const ArchiveValue &>(value)); }

void ErasureSink::incomingErasure(ArchiveErasure &&erasure)
{ return this->incomingErasure(static_cast<const ArchiveErasure &>(erasure)); }


ErasureSink::Buffer::Buffer() : archiveBuffer(), erasureBuffer(), isEnded(false)
{ }

ErasureSink::Buffer::~Buffer() = default;

void ErasureSink::Buffer::incomingValue(const ArchiveValue &value)
{ archiveBuffer.emplace_back(value); }

void ErasureSink::Buffer::incomingValue(ArchiveValue &&value)
{ archiveBuffer.emplace_back(std::move(value)); }

void ErasureSink::Buffer::incomingErasure(const ArchiveErasure &erasure)
{ erasureBuffer.emplace_back(erasure); }

void ErasureSink::Buffer::incomingErasure(ArchiveErasure &&erasure)
{ erasureBuffer.emplace_back(std::move(erasure)); }

void ErasureSink::Buffer::endStream()
{ isEnded = true; }

ArchiveValue::Transfer ErasureSink::Buffer::archiveValues() const
{ return archiveBuffer; }

ArchiveValue::Transfer ErasureSink::Buffer::archiveTake()
{ return std::move(archiveBuffer); }

ArchiveErasure::Transfer ErasureSink::Buffer::erasureValues() const
{ return erasureBuffer; }

ArchiveErasure::Transfer ErasureSink::Buffer::erasureTake()
{ return std::move(erasureBuffer); }

bool ErasureSink::Buffer::ended() const
{ return isEnded; }


ErasureSink::Iterator::Iterator() : mutex(), cv(), archiveBuffer(), erasureBuffer(), state(Active)
{ }

ErasureSink::Iterator::~Iterator() = default;

void ErasureSink::Iterator::incomingValue(const ArchiveValue &value)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state != Active)
            return;
        archiveBuffer.emplace_back(value);
    }
    cv.notify_all();
}

void ErasureSink::Iterator::incomingValue(ArchiveValue &&value)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state != Active)
            return;
        archiveBuffer.emplace_back(std::move(value));
    }
    cv.notify_all();
}

void ErasureSink::Iterator::incomingErasure(const ArchiveErasure &erasure)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state != Active)
            return;
        erasureBuffer.emplace_back(erasure);
    }
    cv.notify_all();
}

void ErasureSink::Iterator::incomingErasure(ArchiveErasure &&erasure)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state != Active)
            return;
        erasureBuffer.emplace_back(std::move(erasure));
    }
    cv.notify_all();
}

void ErasureSink::Iterator::endStream()
{
    std::lock_guard<std::mutex> lock(mutex);
    state = Ended;
    /* Have to hold the mutex during the notify, so we don't get destroyed */
    cv.notify_all();
}

void ErasureSink::Iterator::abort()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state != Active)
            return;
    }
    cv.notify_all();
}

bool ErasureSink::Iterator::ended()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == Ended;
}

bool ErasureSink::Iterator::hasNext()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!archiveBuffer.empty() || !erasureBuffer.empty())
            return true;
        if (state == Ended)
            return false;
        cv.wait(lock);
    }
}

bool ErasureSink::Iterator::isNextArchive()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!archiveBuffer.empty()) {
            if (!erasureBuffer.empty()) {
                return Range::compareStart(archiveBuffer.front().getStart(),
                                           erasureBuffer.front().getStart()) <= 0;
            }
            return true;
        }
        if (!erasureBuffer.empty())
            return false;
        if (state == Ended)
            return false;
        cv.wait(lock);
    }
}

bool ErasureSink::Iterator::isNextErasure()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!erasureBuffer.empty()) {
            if (!archiveBuffer.empty()) {
                return Range::compareStart(erasureBuffer.front().getStart(),
                                           archiveBuffer.front().getStart()) <= 0;
            }
            return true;
        }
        if (!archiveBuffer.empty())
            return false;
        if (state == Ended)
            return false;
        cv.wait(lock);
    }
}

ArchiveValue ErasureSink::Iterator::archiveNext()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!archiveBuffer.empty()) {
            ArchiveValue result = std::move(archiveBuffer.front());
            archiveBuffer.pop_front();
            return result;
        }
        if (state == Ended)
            return ArchiveValue();
        cv.wait(lock);
    }
}

ArchiveErasure ErasureSink::Iterator::erasureNext()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!erasureBuffer.empty()) {
            ArchiveErasure result = std::move(erasureBuffer.front());
            erasureBuffer.pop_front();
            return result;
        }
        if (state == Ended)
            return ArchiveErasure();
        cv.wait(lock);
    }
}

std::pair<ArchiveValue::Transfer, ArchiveErasure::Transfer> ErasureSink::Iterator::all()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!archiveBuffer.empty() || !erasureBuffer.empty()) {
            std::pair<ArchiveValue::Transfer, ArchiveErasure::Transfer> result;
            std::move(archiveBuffer.cbegin(), archiveBuffer.cend(),
                      Util::back_emplacer(result.first));
            archiveBuffer.clear();
            std::move(erasureBuffer.cbegin(), erasureBuffer.cend(),
                      Util::back_emplacer(result.second));
            erasureBuffer.clear();
            return result;
        }
        if (state == Ended)
            return {};
        cv.wait(lock);
    }
}

ArchiveValue ErasureSink::Iterator::archivePeek()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (archiveBuffer.empty())
        return ArchiveValue();
    return archiveBuffer.front();
}

ArchiveErasure ErasureSink::Iterator::erasurePeek()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (erasureBuffer.empty())
        return ArchiveErasure();
    return erasureBuffer.front();
}

std::pair<ArchiveValue::Transfer, ArchiveErasure::Transfer> ErasureSink::Iterator::peekAll()
{
    std::lock_guard<std::mutex> lock(mutex);
    std::pair<ArchiveValue::Transfer, ArchiveErasure::Transfer> result;
    std::copy(archiveBuffer.cbegin(), archiveBuffer.cend(), Util::back_emplacer(result.first));
    std::copy(erasureBuffer.cbegin(), erasureBuffer.cend(), Util::back_emplacer(result.second));
    return result;
}

bool ErasureSink::Iterator::wait(double timeout)
{ return Threading::waitForTimeout(timeout, mutex, cv, [this] { return state == Ended; }); }

}
}