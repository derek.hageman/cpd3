/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACORE_H
#define CPD3DATACORE_H

#include <QtCore/QtGlobal>

#if defined(cpd3datacore_EXPORTS)
#   define CPD3DATACORE_EXPORT Q_DECL_EXPORT
#else
#   define CPD3DATACORE_EXPORT Q_DECL_IMPORT
#endif

#endif
