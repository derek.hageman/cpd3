/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "valueoptionparse.hxx"
#include "core/util.hxx"
#include "variant/composite.hxx"

namespace CPD3 {
namespace Data {

/** @file datacore/valueoptionparse.hxx
 * Provides routines to parse component options from values.
 */

ComponentOptionValueParsable::~ComponentOptionValueParsable()
{ }

static qint64 variantToInteger(const Variant::Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Integer:
        return value.toInteger();
    case Variant::Type::Real: {
        double f = value.toReal();
        if (!FP::defined(f))
            return INTEGER::undefined();
        return static_cast<qint64>(std::round(f));
    }
    default:
        break;
    }
    return INTEGER::undefined();
}

static void parseSingleOption(ComponentOptionBase *option, const Variant::Read &value)
{
    ComponentOptionValueParsable *checkGeneric;
    if ((checkGeneric = qobject_cast<ComponentOptionValueParsable *>(option)) != NULL) {
        checkGeneric->parseFromValue(value);
        return;
    }

    ComponentOptionBoolean *checkBoolean;
    if ((checkBoolean = qobject_cast<ComponentOptionBoolean *>(option)) != NULL) {
        if (value.exists())
            checkBoolean->set(value.toBool());
        return;
    }

    ComponentOptionSingleString *checkSingleString;
    if ((checkSingleString = qobject_cast<ComponentOptionSingleString *>(option)) != NULL) {
        if (value.exists())
            checkSingleString->set(value.toQString());
        return;
    }

    ComponentOptionSingleDouble *checkSingleDouble;
    if ((checkSingleDouble = qobject_cast<ComponentOptionSingleDouble *>(option)) != NULL) {
        double v = Variant::Composite::toNumber(value);
        if (FP::defined(v)) {
            double min = checkSingleDouble->getMinimum();
            if (FP::defined(min)) {
                if (checkSingleDouble->getMinimumInclusive()) {
                    if (v < min)
                        return;
                } else {
                    if (v <= min)
                        return;
                }
            }

            double max = checkSingleDouble->getMaximum();
            if (FP::defined(max)) {
                if (checkSingleDouble->getMaximumInclusive()) {
                    if (v > max)
                        return;
                } else {
                    if (v >= max)
                        return;
                }
            }

            checkSingleDouble->set(v);
        } else if (checkSingleDouble->getAllowUndefined()) {
            checkSingleDouble->set(v);
        }
        return;
    }

    ComponentOptionSingleInteger *checkSingleInteger;
    if ((checkSingleInteger = qobject_cast<ComponentOptionSingleInteger *>(option)) != NULL) {
        qint64 v = variantToInteger(value);
        if (INTEGER::defined(v)) {
            qint64 min = checkSingleInteger->getMinimum();
            if (INTEGER::defined(min) && v < min)
                return;

            qint64 max = checkSingleInteger->getMaximum();
            if (INTEGER::defined(max) && v > max)
                return;

            checkSingleInteger->set(v);
        } else if (checkSingleInteger->getAllowUndefined()) {
            checkSingleInteger->set(v);
        }
        return;
    }

    ComponentOptionStringSet *checkStringSet;
    if ((checkStringSet = qobject_cast<ComponentOptionStringSet *>(option)) != NULL) {
        if (!value.exists())
            return;
        QSet<QString> result;
        for (auto add : value.toChildren().keys()) {
            result.insert(QString::fromStdString(add));
        }
        checkStringSet->set(std::move(result));
        return;
    }

    ComponentOptionDoubleSet *checkDoubleSet;
    if ((checkDoubleSet = qobject_cast<ComponentOptionDoubleSet *>(option)) != NULL) {
        if (!value.exists())
            return;
        switch (value.getType()) {
        case Variant::Type::Integer:
        case Variant::Type::Real: {
            double f = Variant::Composite::toNumber(value);
            if (FP::defined(f))
                checkDoubleSet->add(f);
            break;
        }
        default: {
            QSet<double> result;
            for (auto add : value.toChildren()) {
                double f = Variant::Composite::toNumber(add);
                if (!FP::defined(f))
                    continue;
                result.insert(f);
            }
            checkDoubleSet->set(std::move(result));
            break;
        }
        }
        return;
    }

    ComponentOptionDoubleList *checkDoubleList;
    if ((checkDoubleList = qobject_cast<ComponentOptionDoubleList *>(option)) != NULL) {
        if (!value.exists())
            return;
        switch (value.getType()) {
        case Variant::Type::Integer:
        case Variant::Type::Real:
            checkDoubleList->append(Variant::Composite::toNumber(value));
            break;
        default: {
            QList<double> result;
            for (auto add : value.toChildren()) {
                double f = Variant::Composite::toNumber(add);
                if (!FP::defined(f))
                    continue;
                result.append(f);
            }
            checkDoubleList->set(result);
            break;
        }
        }
        return;
    }

    ComponentOptionSingleCalibration *checkSingleCalibration;
    if ((checkSingleCalibration = qobject_cast<ComponentOptionSingleCalibration *>(option)) !=
            NULL) {
        if (!value.exists())
            return;
        checkSingleCalibration->set(Variant::Composite::toCalibration(value));
        return;
    }

    ComponentOptionEnum *checkEnum;
    if ((checkEnum = qobject_cast<ComponentOptionEnum *>(option)) != NULL) {
        if (!value.exists())
            return;
        const auto &soption = value.toString();
        QList<ComponentOptionEnum::EnumValue> all(checkEnum->getAll());
        for (QList<ComponentOptionEnum::EnumValue>::const_iterator check = all.constBegin(),
                end = all.constEnd(); check != end; ++check) {
            if (Util::equal_insensitive(soption, check->getInternalName().toStdString())) {
                checkEnum->set(*check);
                return;
            }
            if (Util::equal_insensitive(soption, check->getName().toStdString())) {
                checkEnum->set(*check);
                return;
            }
        }
        QHash<QString, ComponentOptionEnum::EnumValue> check(checkEnum->getAliases());
        for (QHash<QString, ComponentOptionEnum::EnumValue>::const_iterator it = check.constBegin();
                it != check.constEnd();
                ++it) {
            if (Util::equal_insensitive(soption, it.key().toStdString())) {
                checkEnum->set(it.value());
                return;
            }
            if (Util::equal_insensitive(soption, it.value().getName().toStdString())) {
                checkEnum->set(it.value());
                return;
            }
        }
        return;
    }

    ComponentOptionTimeOffset *checkTimeOffset;
    if ((checkTimeOffset = qobject_cast<ComponentOptionTimeOffset *>(option)) != NULL) {
        if (!value.exists())
            return;
        Time::LogicalTimeUnit unit = checkTimeOffset->getDefaultUnit();
        int count = checkTimeOffset->getDefaultCount();
        bool align = checkTimeOffset->getDefaultAlign();
        unit = Variant::Composite::toTimeInterval(value, &count, &align);
        checkTimeOffset->set(unit, count, align);
        return;
    }
}

/**
 * Parse the given options from a value.  This reads the value as a hash
 * of internal option names and parses each child of that hash as the
 * type of the option it refers to.
 * 
 * @param options   the options to parse into
 * @param value     the value to parse from
 */
void ValueOptionParse::parse(ComponentOptions &options, const Variant::Read &value)
{
    for (auto c : value.toHash()) {
        if (c.first.empty())
            continue;
        ComponentOptionBase *option = options.get(QString::fromStdString(c.first).toLower());
        if (!option)
            continue;
        parseSingleOption(option, c.second);
    }
}

}
}
