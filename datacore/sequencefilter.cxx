/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <limits>
#include <algorithm>

#include "sequencefilter.hxx"

namespace CPD3 {
namespace Data {

SequenceFilter::SequenceFilter(const SequenceFilter &) = default;

SequenceFilter &SequenceFilter::operator=(const SequenceFilter &) = default;

SequenceFilter::SequenceFilter(SequenceFilter &&) = default;

SequenceFilter &SequenceFilter::operator=(SequenceFilter &&) = default;

SequenceFilter::SequenceFilter() : defaultAccept(false)
{ }

SequenceFilter::~SequenceFilter() = default;

SequenceFilter::Segment SequenceFilter::configureSegment(double start,
                                                         double end,
                                                         const Variant::Read &config,
                                                         const SequenceMatch::Element::PatternList &defaultStation,
                                                         const SequenceMatch::Element::PatternList &defaultArchive,
                                                         const SequenceMatch::Element::PatternList &defaultVariable)
{
    Segment result;
    result.setStart(start);
    result.setEnd(end);

    switch (config.getType()) {
    case Variant::Type::Array:
    case Variant::Type::Matrix:
        for (auto add : config.toChildren()) {
            result.stages
                  .emplace_back(add, createMatchData(add), defaultStation, defaultArchive,
                                defaultVariable);
        }
        break;
    case Variant::Type::Hash: {
        if (config["Actions"].exists()) {
            for (auto add : config["Actions"].toArray()) {
                result.stages
                      .emplace_back(add, createMatchData(add), defaultStation, defaultArchive,
                                    defaultVariable);
            }
        } else if (config["Match"].exists()) {
            for (auto add : config["Match"].toArray()) {
                result.stages
                      .emplace_back(add, createMatchData(add), defaultStation, defaultArchive,
                                    defaultVariable);
            }
        } else {
            result.stages
                  .emplace_back(config, createMatchData(config), defaultStation, defaultArchive,
                                defaultVariable);
        }
        if (config["Default"].getType() == Variant::Type::String) {
            const auto &type = config["Default"].toString();
            if (Util::equal_insensitive(type, "true", "t", "accept", "pass")) {
                result.defaultOutcome = Segment::Accept;
            } else if (Util::equal_insensitive(type, "false", "f", "reject", "fail")) {
                result.defaultOutcome = Segment::Reject;
            } else if (Util::equal_insensitive(type, "universalaccept")) {
                result.defaultOutcome = Segment::UniversalAccept;
            } else if (Util::equal_insensitive(type, "universalreject")) {
                result.defaultOutcome = Segment::UniversalReject;
            } else if (Util::equal_insensitive(type, "system")) {
                result.defaultOutcome = Segment::System;
            }
        } else if (config["Default"].getType() == Variant::Type::Boolean) {
            result.defaultOutcome = config["Default"].toBool() ? Segment::Accept : Segment::Reject;
        }
        break;
    }
    case Variant::Type::Boolean: {
        Segment::Stage s;
        s.accept = config.toBool();
        s.matcher = SequenceMatch::Element(
                config.toBool() ? SequenceMatch::Element::SpecialMatch::All
                                : SequenceMatch::Element::SpecialMatch::None);
        result.stages.emplace_back(std::move(s));
        break;
    }
    default: {
        result.stages
              .emplace_back(config, createMatchData(config), defaultStation, defaultArchive,
                            defaultVariable);
        break;
    }
    }

    std::stable_sort(result.stages.begin(), result.stages.end(),
                     [](const Segment::Stage &a, const Segment::Stage &b) {
                         return a.priority < b.priority;
                     });
    return result;
}

void SequenceFilter::configure(const ValueSegment::Transfer &config,
                               const SequenceMatch::Element::PatternList &defaultStation,
                               const SequenceMatch::Element::PatternList &defaultArchive,
                               const SequenceMatch::Element::PatternList &defaultVariable)
{
    for (const auto &add : config) {
        segments.append(
                configureSegment(add.getStart(), add.getEnd(), add.getValue(), defaultStation,
                                 defaultArchive, defaultVariable));
    }
}

void SequenceFilter::configure(const Variant::Read &config,
                               const SequenceMatch::Element::PatternList &defaultStation,
                               const SequenceMatch::Element::PatternList &defaultArchive,
                               const SequenceMatch::Element::PatternList &defaultVariable)
{
    segments.append(configureSegment(FP::undefined(), FP::undefined(), config, defaultStation,
                                     defaultArchive, defaultVariable));
}

void SequenceFilter::setDefaultAccept(bool accept)
{
    defaultAccept = accept;
}

Archive::Selection::List SequenceFilter::toArchiveSelection(const Archive::Selection::Match &defaultStations,
                                                            const Archive::Selection::Match &defaultArchives,
                                                            const Archive::Selection::Match &defaultVariables,
                                                            double modifiedAfter) const
{
    Archive::Selection::List result;

    segments.applyAll([&](const Segment &seg) {
        for (const auto &s : seg.stages) {
            if (!s.accept)
                continue;

            auto sel = s.matcher
                        .toArchiveSelection(defaultStations, defaultArchives, defaultVariables);
            sel.setStart(seg.getStart());
            sel.setEnd(seg.getEnd());
            sel.modifiedAfter = modifiedAfter;

            if (s.data) {
                sel.modifiedAfter = s.data->overrideModified(sel.modifiedAfter, s.matcher);
            }

            result.emplace_back(std::move(sel));
        }
    });

    return result;
}

void SequenceFilter::advance(double time)
{ segments.advance(time); }

bool SequenceFilter::accept(const SequenceName &name, double start, double end)
{
    AcceptResult result;
    segments.applyActive(start, end, [&result, &name](Segment &seg) {
        result.integrate(name, seg);
    });
    return result.accepted(start, end, defaultAccept);
}

bool SequenceFilter::accept(const SequenceValue &value)
{
    AcceptResult result;
    segments.applyActive(value.getStart(), value.getEnd(), [&result, &value](Segment &seg) {
        result.integrate(value.getUnit(), seg);
    });
    if (!result.accepted(value.getStart(), value.getEnd(), defaultAccept))
        return false;

    for (auto d : result.data) {
        d->passed(value);
    }
    return true;
}

bool SequenceFilter::accept(const ArchiveValue &value)
{
    AcceptResult result;
    segments.applyActive(value.getStart(), value.getEnd(), [&result, &value](Segment &seg) {
        result.integrate(value.getUnit(), seg);
    });
    if (!result.accepted(value.getStart(), value.getEnd(), defaultAccept))
        return false;

    for (auto d : result.data) {
        d->passed(value);
    }
    return true;
}

bool SequenceFilter::accept(const ArchiveErasure &value)
{
    AcceptResult result;
    segments.applyActive(value.getStart(), value.getEnd(), [&result, &value](Segment &seg) {
        result.integrate(value.getUnit(), seg);
    });
    if (!result.accepted(value.getStart(), value.getEnd(), defaultAccept))
        return false;

    for (auto d : result.data) {
        d->passed(value);
    }
    return true;
}

std::unique_ptr<
        SequenceFilter::MatcherData> SequenceFilter::createMatchData(const Variant::Read &config)
{
    return std::unique_ptr<SequenceFilter::MatcherData>();
}


SequenceFilter::Segment::Segment() : defaultOutcome(System)
{ }

SequenceFilter::Segment::Segment(const Segment &other) : Time::Bounds(other),
                                                         defaultOutcome(other.defaultOutcome),
                                                         stages(other.stages),
                                                         result()
{ }

const SequenceFilter::Segment::EvaluationResult &SequenceFilter::Segment::evaluate(const SequenceName &name)
{
    {
        auto check = result.find(name);
        if (check != result.end())
            return check->second;
    }

    EvaluationResult er;
    er.outcome = defaultOutcome;

    auto s = stages.cbegin();
    auto endS = stages.cend();
    for (; s != endS; ++s) {
        if (!s->matcher.matches(name))
            continue;

        if (s->accept && s->data) {
            er.data.emplace_back(s->data.get());
        }

        if (s->universal) {
            er.outcome = s->accept ? UniversalAccept : UniversalReject;
            break;
        }

        er.outcome = s->accept ? Accept : Reject;
        break;
    }

    /* Assign any remaining data to it as well */
    for (; s != endS; ++s) {
        if (!s->matcher.matches(name))
            continue;

        if (s->accept && s->data) {
            er.data.emplace_back(s->data.get());
        }
    }

    return result.emplace(name, std::move(er)).first->second;
}

SequenceFilter::Segment &SequenceFilter::Segment::operator=(const Segment &other)
{
    Time::Bounds::operator=(other);
    defaultOutcome = other.defaultOutcome;
    stages = other.stages;
    result.clear();
    return *this;
}

SequenceFilter::Segment &SequenceFilter::Segment::operator=(Segment &&) = default;

SequenceFilter::Segment::Segment(SequenceFilter::Segment &&) = default;

SequenceFilter::Segment::Stage::Stage() : priority(std::numeric_limits<std::int_fast64_t>::max()),
                                          matcher(),
                                          accept(true),
                                          universal(false)
{ }

SequenceFilter::Segment::Stage::Stage(const Stage &other) : priority(other.priority),
                                                            matcher(other.matcher),
                                                            accept(other.accept),
                                                            universal(other.universal)
{
    if (other.data) {
        data.reset(other.data->clone());
    }
}

SequenceFilter::Segment::Stage &SequenceFilter::Segment::Stage::operator=(const Stage &other)
{
    priority = other.priority;
    matcher = other.matcher;
    accept = other.accept;
    universal = other.universal;
    data.reset(other.data ? other.data->clone() : nullptr);
    return *this;
}

SequenceFilter::Segment::Stage::Stage(SequenceFilter::Segment::Stage &&) = default;

SequenceFilter::Segment::Stage &SequenceFilter::Segment::Stage::operator=(Stage &&) = default;

SequenceFilter::Segment::Stage::Stage(const Variant::Read &config,
                                      std::unique_ptr<SequenceFilter::MatcherData> &&data,
                                      const SequenceMatch::Element::PatternList &defaultStation,
                                      const SequenceMatch::Element::PatternList &defaultArchive,
                                      const SequenceMatch::Element::PatternList &defaultVariable)
        : priority(
        std::numeric_limits<std::int_fast64_t>::max()),
                                                                            matcher(config,
                                                                                    defaultStation,
                                                                                    defaultArchive,
                                                                                    defaultVariable,
                                                                                    {}, {}, {}),
                                                                            accept(true),
                                                                            universal(
                                                                                    config["Universal"]
                                                                                            .toBool()),
                                                                            data(std::move(data))
{
    if (config["Accept"].exists()) {
        accept = config["Accept"].toBoolean();
    }
    {
        auto p = config["Priority"].toInt64();
        if (INTEGER::defined(p)) {
            priority = p;
        }
    }
}

SequenceFilter::Segment::EvaluationResult::EvaluationResult() : outcome(Accept)
{ }

SequenceFilter::Segment::EvaluationResult::EvaluationResult(const EvaluationResult &) = default;

SequenceFilter::Segment::EvaluationResult &SequenceFilter::Segment::EvaluationResult::operator=(
        const EvaluationResult &) = default;

SequenceFilter::Segment::EvaluationResult::EvaluationResult(EvaluationResult &&) = default;

SequenceFilter::Segment::EvaluationResult &SequenceFilter::Segment::EvaluationResult::operator=(
        EvaluationResult &&) = default;

SequenceFilter::AcceptResult::AcceptResult() : anyHit(false),
                                               anyAccepted(false),
                                               anyRejected(false),
                                               anySystem(false),
                                               universal(None),
                                               lastEnd(FP::undefined())
{ }

void SequenceFilter::AcceptResult::integrate(const SequenceName &name,
                                             SequenceFilter::Segment &segment)
{
    if (!anyHit)
        firstStart = segment.getStart();
    anyHit = true;

    const auto &result = segment.evaluate(name);
    for (auto add : result.data) {
        data.emplace_back(add);
    }

    if (FP::defined(lastEnd)) {
        /* Gaps always trigger a system default */
        if (Range::compareStartEnd(segment.getStart(), lastEnd) < 0)
            anySystem = true;
    }
    lastEnd = segment.getEnd();

    switch (universal) {
    case None:
        break;
    case Accept:
    case Reject:
        return;
    }

    switch (result.outcome) {
    case Segment::Accept:
        anyAccepted = true;
        break;
    case Segment::Reject:
        anyRejected = true;
        break;
    case Segment::UniversalAccept:
        universal = Accept;
        return;
    case Segment::UniversalReject:
        universal = Reject;
        return;
    case Segment::System:
        anySystem = true;
        return;
    }
}

bool SequenceFilter::AcceptResult::accepted(double start, double end, bool defaultAccept) const
{
    switch (universal) {
    case None:
        break;
    case Accept:
        return true;
    case Reject:
        return false;
    }

    if (!anyHit)
        return defaultAccept;
    if (anySystem)
        return defaultAccept;
    if (Range::compareStart(start, firstStart) < 0)
        return defaultAccept;
    if (Range::compareEnd(end, lastEnd) > 0)
        return defaultAccept;

    if (anyAccepted && !anyRejected)
        return true;
    if (anyRejected && !anyAccepted)
        return false;

    return defaultAccept;
}

double SequenceFilter::MatcherData::overrideModified(double modifiedAfter,
                                                     const SequenceMatch::Element &)
{ return modifiedAfter; }

}
}