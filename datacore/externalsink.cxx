/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>
#include <QBuffer>
#include <QByteArray>
#include <QXmlStreamWriter>
#include <QtEndian>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QThread>

#include "externalsink.hxx"
#include "stream.hxx"
#include "dataioxml.hxx"
#include "datacore/variant/handle.hxx"
#include "datacore/variant/serialization.hxx"
#include "core/threadpool.hxx"
#include "io/drivers/qio.hxx"


Q_LOGGING_CATEGORY(log_datacore_externalsink, "cpd3.datacore.externalsink", QtWarningMsg)

namespace CPD3 {
namespace Data {

ExternalSink::ExternalSink() = default;

ExternalSink::~ExternalSink() = default;

void ExternalSink::setAccelerationKey(const std::string &)
{ }

void ExternalSink::flushData()
{ }

void ExternalSink::signalTerminate()
{ }

void ExternalSink::normalizeUnicode(QString &str)
{
    str.replace(QChar(0x2018), '\'');
    str.replace(QChar(0x2019), '\'');
    str.replace(QChar(0x201A), ',');
    str.replace(QChar(0x201C), '"');
    str.replace(QChar(0x201D), '"');
    str.replace(QChar(0x201F), '"');
    str.replace(QChar(0x201B), '\'');
    str.replace(QChar(0x2028), '\n');
    str.replace(QChar(0x2029), '\n');
    str.replace(QChar(0x202F), ' ');

    for (ushort space=0x2000; space <= 0x200A; ++space) {
        str.replace(QChar(space), ' ');
    }
    for (ushort format=0x200B; format <= 0x200F; ++format) {
        str.remove(QChar(format));
    }
    for (ushort dash=0x2010; dash <= 0x2015; ++dash) {
        str.replace(QChar(dash), '-');
    }
}

void ExternalSink::simplifyUnicode(QString &str)
{
    normalizeUnicode(str);

    str.replace(QString::fromUtf8("\xC2\xB0\x43"), QObject::tr("C", "temperature unit"));
    str.replace(QString::fromUtf8("Mm\xE2\x81\xBB¹"),
                QObject::tr("Mm-1", "extinction component unit"));
    str.replace(QString::fromUtf8("cm\xE2\x81\xBB³"), QObject::tr("cm-3", "concentration unit"));
    str.replace(QString::fromUtf8("\xCE\xBCg/m\xC2\xB3"),
                QObject::tr("ug/m3", "micro grams per m3 unit"));
    str.replace(QString::fromUtf8("\xCE\xBCm"), QObject::tr("um", "micrometer unit"));
    str.replace(QString::fromUtf8("m\xE2\x81\xBB¹"), QObject::tr("m-1", "inverse meters unit"));
    str.replace(QString::fromUtf8("\xE2\x84\xAB"), QObject::tr("A", "angstrom unit"));
    str.replace(QString::fromUtf8("\xC2\xB0"), QObject::tr("deg", "degree unit"));

    str.replace(QString::fromUtf8("\xE2\x81\xBB"), QObject::tr("-", "inverse replace"));
    str.replace(QString::fromUtf8("\xC3\x97"), QObject::tr("*", "multiplication replace"));
    str.replace(QString::fromUtf8("¹"), QObject::tr("1", "superscript one replace"));
    str.replace(QString::fromUtf8("²"), QObject::tr("2", "superscript two replace"));
    str.replace(QString::fromUtf8("³"), QObject::tr("3", "superscript three replace"));
    str.replace(QString::fromUtf8("\xE2\x81\xB4"), QObject::tr("4", "superscript four replace"));
    str.replace(QString::fromUtf8("\xE2\x82\x81"), QObject::tr("1", "subscript one replace"));
    str.replace(QString::fromUtf8("\xE2\x82\x82"), QObject::tr("2", "subscript two replace"));
    str.replace(QString::fromUtf8("\xE2\x82\x83"), QObject::tr("3", "subscript three replace"));
    str.replace(QString::fromUtf8("\xE2\x82\x84"), QObject::tr("4", "subscript four replace"));
    str.replace(QString::fromUtf8("\xE2\x82\x93"), QObject::tr("4", "subscript x replace"));
    str.replace(QString::fromUtf8("\xCE\xBC"), QObject::tr("u", "micro replace"));
    str.replace(QString::fromUtf8("\xCF\x89"), QObject::tr("w", "omega replace"));
    str.replace(QString::fromUtf8("\xCF\x83"), QObject::tr("B", "sigma replace"));
    str.replace(QString::fromUtf8("\xCE\xB4"), QObject::tr("d", "small delta replace"));
    str.replace(QString::fromUtf8("\xCE\xB7"), QObject::tr("h", "small eta replace"));

    /* JAO says: just drop the unicode rather than trying to use "aa", "oe", etc */
    str.replace(QString::fromUtf8("\xC3\xA5"), QObject::tr("a", "a-ring replace"));
    str.replace(QString::fromUtf8("\xC3\x85"), QObject::tr("A", "A-ring replace"));
    str.replace(QString::fromUtf8("\xC3\xB6"), QObject::tr("o", "o-umlaut replace"));
    str.replace(QString::fromUtf8("\xC3\xBC"), QObject::tr("u", "u-umlaut replace"));
}

bool ExternalSink::wait(double timeout)
{ return finished.wait(std::bind(&ExternalSink::isFinished, this), timeout); }


StandardDataOutput::StandardDataOutput(std::unique_ptr<IO::Generic::Stream> &&device,
                                       OutputType outputType) : state(State::Initialize),
                                                                outputStallRequested(false),
                                                                mode(outputType),
                                                                device(std::move(device)),
                                                                nextIndex(0)
{
    Q_ASSERT(this->device.get() != nullptr);
    this->device
        ->writeStall
        .connect(std::bind(&StandardDataOutput::updateOutputStall, this, std::placeholders::_1));
}

StandardDataOutput::StandardDataOutput(QIODevice *device, OutputType outputType)
        : StandardDataOutput(IO::Access::qio(device)->stream(), outputType)
{
    Q_ASSERT(device != nullptr);
}

StandardDataOutput::~StandardDataOutput()
{
    signalTerminate();
    if (thread.joinable()) {
        thread.join();
    }
    if (qthread) {
        qthread->wait();
        qthread.reset();
    }

    device.reset();
}

void StandardDataOutput::setAccelerationKey(const std::string &key)
{ accelerationKey = key; }

void StandardDataOutput::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Util::append(values, pending);
    }
    notify.notify_all();
}

void StandardDataOutput::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Util::append(std::move(values), pending);
    }
    notify.notify_all();
}

void StandardDataOutput::incomingData(const SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        pending.emplace_back(value);
    }
    notify.notify_all();
}

void StandardDataOutput::incomingData(SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        pending.emplace_back(std::move(value));
    }
    notify.notify_all();
}

void StandardDataOutput::endData()
{
    std::lock_guard<std::mutex> lock(mutex);
    switch (state) {
    case State::Initialize:
    case State::Running:
        state = State::EndPending;
        notify.notify_all();
        return;
    default:
        break;
    }
}

void StandardDataOutput::stall(std::unique_lock<std::mutex> &lock)
{
    while (pending.size() > stallThreshold) {
        switch (state) {
        case State::TerminateRequested:
        case State::Complete:
            return;
        default:
            break;
        }
        notify.wait(lock);
    }
}

void StandardDataOutput::updateOutputStall(bool stalled)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        outputStallRequested = stalled;
        if (outputStallRequested)
            return;
    }
    notify.notify_all();
}

void StandardDataOutput::signalTerminate()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (state == State::Complete)
        return;
    state = State::TerminateRequested;
    notify.notify_all();
}

void StandardDataOutput::start()
{
    Q_ASSERT(state != State::Complete && state != State::Running);

    switch (mode) {
    case OutputType::Direct:
    case OutputType::Direct_Legacy:
    case OutputType::Raw:
    case OutputType::Raw_Legacy:
        thread = std::thread(std::bind(&StandardDataOutput::runBasicSerialization, this));
        break;
    case OutputType::XML: {
        class Worker : public QThread {
            StandardDataOutput &parent;
        public:
            explicit Worker(StandardDataOutput &parent) : parent(parent)
            {
                setObjectName("StandardDataOutputXML");
            }

            virtual ~Worker() = default;

        protected:
            void run() override
            { return parent.runXML(); }
        };
        qthread.reset(new Worker(*this));
        qthread->start();
        break;
    }
    case OutputType::JSON:
        thread = std::thread(std::bind(&StandardDataOutput::runJSON, this));
        break;
    }
}

bool StandardDataOutput::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == State::Complete;
}

bool StandardDataOutput::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, mutex, notify,
                                     [this] { return state == State::Complete; });
}

static constexpr std::size_t outputProgressUpdateInterval = 1024;

void StandardDataOutput::outputBasicSerialization(const QByteArray &input)
{
    switch (mode) {
    case OutputType::Direct:
    case OutputType::Direct_Legacy: {
        auto encoded = input.toBase64();
        encoded.append('\n');
        device->write(std::move(encoded));
        break;
    }
    case OutputType::Raw:
    case OutputType::Raw_Legacy: {
        uchar tmp[6];
        int sz = input.size();
        if (sz < 0xFFFF) {
            qToLittleEndian<quint16>(static_cast<quint16>(sz), tmp);
            device->write(Util::ByteView(tmp, 2));
        } else {
            tmp[0] = 0xFFU;
            tmp[1] = 0xFFU;
            qToLittleEndian<quint32>(static_cast<quint32>(sz), &tmp[2]);
            device->write(Util::ByteView(tmp, 6));
        }
        device->write(input);
        break;
    }
    default:
        Q_ASSERT(false);
        break;
    }
}

void StandardDataOutput::runBasicSerialization()
{
    feedback.emitStage(QObject::tr("Writing data"),
                       QObject::tr("Data are being written out in CPD3 native format."), true);

    bool legacyMode = false;
    switch (mode) {
    case OutputType::Direct: {
        device->write("CPD3DATA-2.0");
        if (!accelerationKey.empty()) {
            device->write(" ");
            device->write(accelerationKey);
        }
        device->write("\n");
        break;
    }
    case OutputType::Direct_Legacy: {
        device->write("CPD3DATA-1.0");
        if (!accelerationKey.empty()) {
            device->write(" ");
            device->write(accelerationKey);
        }
        device->write("\n");
        legacyMode = true;
        break;
    }
    case OutputType::Raw: {
        Util::ByteArray header;
        header.push_back(0xC4);
        header.push_back(0xD3);
        header.push_back(2);
        device->write(std::move(header));
        break;
    }
    case OutputType::Raw_Legacy: {
        Util::ByteArray header;
        header.push_back(0xC4);
        header.push_back(0xD3);
        header.push_back(1);
        device->write(std::move(header));
        legacyMode = true;
        break;
    }
    default:
        Q_ASSERT(false);
        return;
    }


    std::size_t progressCounter = outputProgressUpdateInterval;
    double finalTime = FP::undefined();

    for (;;) {
        SequenceValue::Transfer processing;
        bool finishData = false;

        {
            std::unique_lock<std::mutex> lock(mutex);
            switch (state) {
            case State::Initialize:
                state = State::Running;
                break;
            case State::Running:
                if (pending.empty()) {
                    notify.wait(lock);
                    continue;
                }
                if (outputStallRequested) {
                    notify.wait(lock);
                    continue;
                }
                break;
            case State::EndPending:
                finishData = true;
                break;
            case State::TerminateRequested:
                state = State::Complete;
                pending.clear();
                lock.unlock();
                notify.notify_all();
                finished();
                return;
            case State::Complete:
                Q_ASSERT(false);
                return;
            }

            processing = std::move(pending);
            pending.clear();
        }

        if (processing.size() > stallThreshold)
            notify.notify_all();

        QByteArray data;
        QBuffer buffer(&data);
        buffer.open(QIODevice::WriteOnly);
        QDataStream stream(&buffer);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);

        auto outputBegin = processing.cbegin();
        for (auto processingEnd = processing.cend(); outputBegin != processingEnd;) {
            if (!progressCounter) {
                feedback.emitProgressTime(outputBegin->getStart());
                progressCounter = outputProgressUpdateInterval;
            }

            std::uint_fast8_t total = 0;
            auto outputEnd = outputBegin;
            for (; total < 0x7FU && outputEnd != processingEnd; ++outputEnd, ++total) {
                if (!FP::equal(outputBegin->getStart(), outputEnd->getStart()) ||
                        !FP::equal(outputBegin->getEnd(), outputEnd->getEnd()) ||
                        outputBegin->getPriority() != outputEnd->getPriority())
                    break;

                if (progressCounter) {
                    --progressCounter;
                }

                std::uint_fast16_t nameIndex = 0;
                auto check = forwardIndexer.find(outputEnd->getName());
                if (check == forwardIndexer.end()) {
                    if (total) {
                        Q_ASSERT(total < 0x80U);
                        data[0] = static_cast<char>(total);
                        outputBasicSerialization(data);

                        buffer.seek(0);
                        data.clear();
                        total = 0;
                    }

                    nameIndex = nextIndex;
                    nextIndex = (nextIndex + 1U) & 0xFFFFU;
                    if (nameIndex >= reverseIndexer.size()) {
                        forwardIndexer.emplace(outputEnd->getUnit(), nameIndex);
                        reverseIndexer.emplace_back(outputEnd->getUnit());
                    } else {
                        forwardIndexer.erase(reverseIndexer[nameIndex]);
                        forwardIndexer.emplace(outputEnd->getUnit(), nameIndex);
                        reverseIndexer[nameIndex] = outputEnd->getUnit();
                    }

                    stream << static_cast<quint8>(0x80U) << outputEnd->getUnit();
                    outputBasicSerialization(data);

                    buffer.seek(0);
                    data.clear();
                } else {
                    nameIndex = check->second;
                }

                if (!total) {
                    /* Placeholder count */
                    stream << static_cast<quint8>(0) << outputBegin->getStart()
                           << outputBegin->getEnd()
                           << static_cast<qint32>(outputBegin->getPriority());
                }

                stream << static_cast<quint16>(nameIndex);
                if (legacyMode) {
                    Variant::Serialization::serializeLegacy(stream, outputEnd->root());
                } else {
                    stream << outputEnd->root();
                }
            }
            outputBegin = outputEnd;

            if (total) {
                Q_ASSERT(total < 0x80U);
                data[0] = static_cast<char>(total);
                outputBasicSerialization(data);

                buffer.seek(0);
                data.clear();
            }
        }

        if (!processing.empty()) {
            finalTime = processing.back().getStart();
        }

        if (finishData) {
            if (!accelerationKey.empty()) {
                outputBasicSerialization(QByteArray(1, static_cast<char>(0x81U)));
            }

            if (FP::defined(finalTime)) {
                feedback.emitProgressTime(finalTime);
            }

            device.reset();

            {
                std::lock_guard<std::mutex> lock(mutex);
                state = State::Complete;
            }
            notify.notify_all();
            finished();
            return;
        }
    }
}

template<typename MapType>
void xmlMapWrite(QXmlStreamWriter *xml,
                 const MapType &map,
                 int indent,
                 int indentSpaces,
                 const QString &elementType = QLatin1String("value"))
{
    QStringList keyOrder;
    for (const auto &add : map.keys()) {
        keyOrder.push_back(QString::fromStdString(add));
    }
    std::sort(keyOrder.begin(), keyOrder.end());
    for (const auto &key : keyOrder) {
        if (indentSpaces != 0) {
            xml->writeCharacters(QString('\n'));
            xml->writeCharacters(QString(' ').repeated(indent + indentSpaces));
        }
        xml->writeStartElement(elementType);
        xml->writeAttribute("key", key);
        XMLDataOutput::write(xml, map[key], indent + indentSpaces, indentSpaces);
        xml->writeEndElement();
    }
    if (indentSpaces != 0 && !keyOrder.empty()) {
        xml->writeCharacters(QString('\n'));
        xml->writeCharacters(QString(' ').repeated(indent));
    }
}

/**
 * Write the given Value to an XML stream.  The writing will set attributes
 * in the current element and create children for it.
 * 
 * @param xml           the stream to write to
 * @param value         the value to write
 * @param ident         the number of spaces already indented
 * @param indentSpaces  the number of spaces to indent, zero disables
 */
void XMLDataOutput::write(QXmlStreamWriter *xml,
                          const Variant::Read &value,
                          int indent,
                          int indentSpaces)
{
    switch (value.getType()) {
    case Variant::Type::Empty:
        return;
    case Variant::Type::Real:
        xml->writeAttribute("type", "real");
        if (FP::defined(value.toDouble()))
            xml->writeCharacters(QString::number(value.toDouble(), 'g', 15));
        return;
    case Variant::Type::Integer:
        xml->writeAttribute("type", "integer");
        if (INTEGER::defined(value.toInt64()))
            xml->writeCharacters(QString::number(value.toInt64()));
        return;
    case Variant::Type::Boolean:
        xml->writeAttribute("type", "boolean");
        if (value.toBool())
            xml->writeCharacters("true");
        else
            xml->writeCharacters("false");
        return;
    case Variant::Type::String: {
        auto localized = value.allLocalizedStrings();
        if (localized.size() <= 1) {
            xml->writeAttribute("type", "string");
            xml->writeCharacters(value.toQString());
        } else {
            xml->writeAttribute("type", "localizedstring");
            QStringList keyOrder;
            for (const auto &add : localized) {
                keyOrder.push_back(add.first);
            }
            std::sort(keyOrder.begin(), keyOrder.end());
            for (const auto &key : keyOrder) {
                if (indentSpaces != 0) {
                    xml->writeCharacters(QString('\n'));
                    xml->writeCharacters(QString(' ').repeated(indent + indentSpaces));
                }
                xml->writeStartElement("locale");
                xml->writeAttribute("name", key);
                xml->writeCharacters(localized[key]);
                xml->writeEndElement();
            }
            if (indentSpaces != 0 && !localized.empty()) {
                xml->writeCharacters(QString('\n'));
                xml->writeCharacters(QString(' ').repeated(indent));
            }
        }
        return;
    }
    case Variant::Type::Bytes:
        xml->writeAttribute("type", "binary");
        xml->writeCharacters(value.toBinary().toBase64());
        return;
    case Variant::Type::Flags: {
        xml->writeAttribute("type", "flags");
        QStringList flags;
        for (const auto &flag : value.toFlags()) {
            flags.push_back(QString::fromStdString(flag));
        }
        std::sort(flags.begin(), flags.end());
        for (const auto &flag : flags) {
            if (indentSpaces != 0) {
                xml->writeCharacters(QString('\n'));
                xml->writeCharacters(QString(' ').repeated(indent + indentSpaces));
            }
            xml->writeStartElement("flag");
            xml->writeCharacters(flag);
            xml->writeEndElement();
        }
        if (indentSpaces != 0 && !flags.isEmpty()) {
            xml->writeCharacters(QString('\n'));
            xml->writeCharacters(QString(' ').repeated(indent));
        }
        return;
    }
    case Variant::Type::Array: {
        xml->writeAttribute("type", "array");
        auto array = value.toArray();
        for (auto child : array) {
            if (indentSpaces != 0) {
                xml->writeCharacters("\n");
                xml->writeCharacters(QString(' ').repeated(indent + indentSpaces));
            }
            xml->writeStartElement("value");
            write(xml, child, indent + indentSpaces, indentSpaces);
            xml->writeEndElement();
        }
        if (indentSpaces != 0 && !array.empty()) {
            xml->writeCharacters(QString('\n'));
            xml->writeCharacters(QString(' ').repeated(indent));
        }
        return;
    }
    case Variant::Type::Matrix: {
        xml->writeAttribute("type", "matrix");
        auto matrix = value.toMatrix();
        for (auto child :matrix) {
            if (indentSpaces != 0) {
                xml->writeCharacters("\n");
                xml->writeCharacters(QString(' ').repeated(indent + indentSpaces));
            }
            xml->writeStartElement("value");
            {
                QString index;
                for (auto add : child.first) {
                    if (!index.isEmpty())
                        index.append(',');
                    index.append(QString::number(add));
                }
                xml->writeAttribute("index", index);
            }
            write(xml, child.second, indent + indentSpaces, indentSpaces);
            xml->writeEndElement();
        }
        if (indentSpaces != 0 && !matrix.empty()) {
            xml->writeCharacters(QString('\n'));
            xml->writeCharacters(QString(' ').repeated(indent));
        }
        return;
    }
    case Variant::Type::Hash:
        xml->writeAttribute("type", "hash");
        xmlMapWrite(xml, value.toHash(), indent, indentSpaces);
        return;
    case Variant::Type::Keyframe: {
        xml->writeAttribute("type", "keyframe");
        auto keyframe = value.toKeyframe();
        for (auto add : keyframe) {
            if (indentSpaces != 0) {
                xml->writeCharacters(QString('\n'));
                xml->writeCharacters(QString(' ').repeated(indent + indentSpaces));
            }
            xml->writeStartElement("value");
            xml->writeAttribute("key", QString::number(add.first, 'g', 15));
            write(xml, add.second, indent + indentSpaces, indentSpaces);
            xml->writeEndElement();
        }
        if (indentSpaces != 0 && !keyframe.empty()) {
            xml->writeCharacters(QString('\n'));
            xml->writeCharacters(QString(' ').repeated(indent));
        }
        return;
    }

    case Variant::Type::MetadataReal:
        xml->writeAttribute("type", "metareal");
        xmlMapWrite(xml, value.toMetadata(), indent, indentSpaces, "meta");
        return;
    case Variant::Type::MetadataInteger:
        xml->writeAttribute("type", "metainteger");
        xmlMapWrite(xml, value.toMetadata(), indent, indentSpaces, "meta");
        return;
    case Variant::Type::MetadataBoolean:
        xml->writeAttribute("type", "metaboolean");
        xmlMapWrite(xml, value.toMetadata(), indent, indentSpaces, "meta");
        return;
    case Variant::Type::MetadataString:
        xml->writeAttribute("type", "metastring");
        xmlMapWrite(xml, value.toMetadata(), indent, indentSpaces, "meta");
        return;
    case Variant::Type::MetadataBytes:
        xml->writeAttribute("type", "metabinary");
        xmlMapWrite(xml, value.toMetadata(), indent, indentSpaces, "meta");
        return;
    case Variant::Type::MetadataFlags:
        xml->writeAttribute("type", "metaflags");
        xmlMapWrite(xml, value.toMetadata(), indent, indentSpaces, "meta");
        xmlMapWrite(xml, value.toMetadataSingleFlag(), indent, indentSpaces);
        return;
    case Variant::Type::MetadataArray:
        xml->writeAttribute("type", "metaarray");
        xmlMapWrite(xml, value.toMetadata(), indent, indentSpaces, "meta");
        return;
    case Variant::Type::MetadataMatrix:
        xml->writeAttribute("type", "metamatrix");
        xmlMapWrite(xml, value.toMetadata(), indent, indentSpaces, "meta");
        return;
    case Variant::Type::MetadataHash:
        xml->writeAttribute("type", "metahash");
        xmlMapWrite(xml, value.toMetadata(), indent, indentSpaces, "meta");
        xmlMapWrite(xml, value.toMetadataHashChild(), indent, indentSpaces);
        return;
    case Variant::Type::MetadataKeyframe:
        xml->writeAttribute("type", "metakeyframe");
        xmlMapWrite(xml, value.toMetadata(), indent, indentSpaces, "meta");
        return;

    case Variant::Type::Overlay:
        xml->writeAttribute("type", "overlay");
        xml->writeCharacters(value.toQString());
        return;
    }
    Q_ASSERT(false);
}

/**
 * Write the given Value path to an XML stream.  This will create child
 * elements of the current one.
 * 
 * @param xml           the stream to write to
 * @param path          the path to write
 * @param indent        the current number of spaces that's indent
 * @param indentSpaces  the number of spaces to indent, zero disables
 */
void XMLDataOutput::writePath(QXmlStreamWriter *xml, const Variant::Path &path,
                              int indent,
                              int indentSpaces)
{
    if (path.empty())
        return;

    for (const auto &pe : path) {
        auto code = pe.encode();
        if (indentSpaces > 0) {
            xml->writeCharacters(QString('\n'));
            xml->writeCharacters(QString(' ').repeated(indent + indentSpaces));
        }
        xml->writeStartElement(QString::fromStdString(code.first));
        xml->writeCharacters(QString::fromStdString(code.second));
        xml->writeEndElement();
    }

    if (indentSpaces > 0) {
        xml->writeCharacters(QString('\n'));
        xml->writeCharacters(QString(' ').repeated(indent));
    }
}

void StandardDataOutput::runXML()
{
    feedback.emitStage(QObject::tr("Writing data"),
                       QObject::tr("Data are being written out in XML."), true);

    auto qdevice = IO::Generic::Backing::qioStream(std::move(device));
    qdevice->open(QIODevice::WriteOnly | QIODevice::Unbuffered);
    std::unique_ptr<QXmlStreamWriter> xml(new QXmlStreamWriter(qdevice.get()));

    xml->writeStartDocument();
    xml->writeCharacters(QString('\n'));
    xml->writeDTD("<!DOCTYPE cpd3data>");
    xml->writeCharacters(QString('\n'));
    xml->writeStartElement("cpd3data");
    xml->writeAttribute("version", "1.0");
    xml->writeCharacters(QString('\n'));

    std::size_t progressCounter = outputProgressUpdateInterval;
    double finalTime = FP::undefined();

    for (;;) {
        SequenceValue::Transfer processing;
        bool finishData = false;

        {
            std::unique_lock<std::mutex> lock(mutex);
            switch (state) {
            case State::Initialize:
                state = State::Running;
                break;
            case State::Running:
                if (pending.empty()) {
                    notify.wait(lock);
                    continue;
                }
                if (outputStallRequested) {
                    notify.wait(lock);
                    continue;
                }
                break;
            case State::EndPending:
                finishData = true;
                break;
            case State::TerminateRequested:
                state = State::Complete;
                pending.clear();
                lock.unlock();
                notify.notify_all();
                finished();
                return;
            case State::Complete:
                Q_ASSERT(false);
                return;
            }

            processing = std::move(pending);
            pending.clear();
        }

        if (processing.size() > stallThreshold)
            notify.notify_all();

        for (const auto &d : processing) {
            if (!progressCounter) {
                feedback.emitProgressTime(d.getStart());
                progressCounter = outputProgressUpdateInterval;
            }
            --progressCounter;
            finalTime = d.getStart();

            xml->writeCharacters("    ");
            xml->writeStartElement("value");
            xml->writeAttribute("station", d.getStationQString());
            xml->writeAttribute("archive", d.getArchiveQString());
            xml->writeAttribute("variable", d.getVariableQString());
            xml->writeAttribute("flavors", QString::fromStdString(d.getName().getFlavorsString()));
            if (FP::defined(d.getStart()))
                xml->writeAttribute("start", Time::toISO8601(d.getStart(), true));
            if (FP::defined(d.getEnd()))
                xml->writeAttribute("end", Time::toISO8601(d.getEnd(), true));
            xml->writeAttribute("priority", QString::number(d.getPriority()));
            XMLDataOutput::write(xml.get(), d.read(), 4, 4);
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
        }

        if (finishData) {
            if (FP::defined(finalTime)) {
                feedback.emitProgressTime(finalTime);
            }

            xml->writeEndDocument();
            xml.reset();
            qdevice.reset();

            {
                std::lock_guard<std::mutex> lock(mutex);
                state = State::Complete;
            }
            notify.notify_all();
            finished();
            return;
        }
    }
}

static void writeJSONValue(const Variant::Read &value, QJsonValue &data, QJsonObject &metadata);

template<typename MapType>
void jsonMapWrite(const MapType &map, QJsonObject &data, QJsonObject &metadata)
{
    for (auto child : map) {
        QJsonValue cdata;
        QJsonObject cmeta;
        writeJSONValue(child.second, cdata, cmeta);
        QString key = QString::fromStdString(child.first);
        data.insert(key, cdata);
        metadata.insert(key, cmeta);
    }
}

static void writeJSONValue(const Variant::Read &value, QJsonValue &data, QJsonObject &metadata)
{
    switch (value.getType()) {
    case Variant::Type::Empty:
        metadata.insert("type", QJsonValue(QLatin1String("empty")));
        return;
    case Variant::Type::Real:
        metadata.insert("type", QJsonValue(QLatin1String("real")));
        if (FP::defined(value.toDouble()))
            data = QJsonValue(value.toDouble());
        return;
    case Variant::Type::Integer:
        metadata.insert("type", QJsonValue(QLatin1String("integer")));
        if (INTEGER::defined(value.toInt64()))
            data = QJsonValue(value.toInt64());
        return;
    case Variant::Type::Boolean:
        metadata.insert("type", QJsonValue(QLatin1String("boolean")));
        data = QJsonValue(value.toBool());
        return;
    case Variant::Type::String: {
        auto localized = value.allLocalizedStrings();
        if (localized.size() <= 1) {
            metadata.insert("type", QJsonValue(QLatin1String("string")));
            data = QJsonValue(value.toQString());
        } else {
            metadata.insert("type", QJsonValue(QLatin1String("localizedstring")));
            QJsonObject output;
            for (const auto &v : localized) {
                output.insert(v.first, v.second);
            }
            data = QJsonValue(output);
        }
        return;
    }
    case Variant::Type::Bytes:
        metadata.insert("type", QJsonValue(QLatin1String("binary")));
        data = QJsonValue(QString::fromLatin1(value.toBinary().toBase64()));
        return;
    case Variant::Type::Flags: {
        metadata.insert("type", QJsonValue(QLatin1String("flags")));
        QStringList flags;
        for (const auto &flag : value.toFlags()) {
            flags.push_back(QString::fromStdString(flag));
        }
        std::sort(flags.begin(), flags.end());
        QJsonArray output;
        for (const auto &flag : flags) {
            output.push_back(QJsonValue(flag));
        }
        data = QJsonValue(output);
        return;
    }
    case Variant::Type::Array: {
        metadata.insert("type", QJsonValue(QLatin1String("array")));
        QJsonArray output;
        QJsonArray children;
        for (auto child : value.toArray()) {
            QJsonValue cdata;
            QJsonObject cmeta;
            writeJSONValue(child, cdata, cmeta);
            output.push_back(cdata);
            children.push_back(cmeta);
        }
        data = QJsonValue(output);
        metadata.insert("values", children);
        return;
    }
    case Variant::Type::Matrix: {
        metadata.insert("type", QJsonValue(QLatin1String("matrix")));
        auto matrix = value.toMatrix();
        {
            QJsonArray output;
            for (auto add : matrix.shape()) {
                output.push_back(static_cast<int>(add));
            }
            metadata.insert("dimensions", output);
        }
        QJsonArray output;
        QJsonArray children;
        for (auto child : matrix) {
            QJsonValue cdata;
            QJsonObject cmeta;
            writeJSONValue(child.second, cdata, cmeta);
            output.push_back(cdata);
            children.push_back(cmeta);
        }
        data = QJsonValue(output);
        metadata.insert("values", children);
        return;
    }

    case Variant::Type::Hash: {
        metadata.insert("type", QJsonValue(QLatin1String("hash")));
        QJsonObject output;
        QJsonObject children;
        jsonMapWrite(value.toHash(), output, children);
        data = QJsonValue(output);
        metadata.insert("values", children);
        return;
    }
    case Variant::Type::Keyframe: {
        metadata.insert("type", QJsonValue(QLatin1String("keyframe")));
        QJsonArray output;
        QJsonArray children;
        for (auto child : value.toKeyframe()) {
            QJsonValue cdata;
            QJsonObject cmeta;
            writeJSONValue(child.second, cdata, cmeta);
            output.push_back(cdata);
            cmeta.insert("index", child.first);
            children.push_back(cmeta);
        }
        data = QJsonValue(output);
        metadata.insert("values", children);
        return;
    }

    case Variant::Type::MetadataReal: {
        metadata.insert("type", QJsonValue(QLatin1String("metareal")));
        QJsonObject output;
        QJsonObject children;
        jsonMapWrite(value.toMetadata(), output, children);
        data = QJsonValue(output);
        metadata.insert("metadata", children);
        return;
    }
    case Variant::Type::MetadataInteger: {
        metadata.insert("type", QJsonValue(QLatin1String("metainteger")));
        QJsonObject output;
        QJsonObject children;
        jsonMapWrite(value.toMetadata(), output, children);
        data = QJsonValue(output);
        metadata.insert("metadata", children);
        return;
    }
    case Variant::Type::MetadataBoolean: {
        metadata.insert("type", QJsonValue(QLatin1String("metaboolean")));
        QJsonObject output;
        QJsonObject children;
        jsonMapWrite(value.toMetadata(), output, children);
        data = QJsonValue(output);
        metadata.insert("metadata", children);
        return;
    }
    case Variant::Type::MetadataString: {
        metadata.insert("type", QJsonValue(QLatin1String("metastring")));
        QJsonObject output;
        QJsonObject children;
        jsonMapWrite(value.toMetadata(), output, children);
        data = QJsonValue(output);
        metadata.insert("metadata", children);
        return;
    }
    case Variant::Type::MetadataBytes: {
        metadata.insert("type", QJsonValue(QLatin1String("metabinary")));
        QJsonObject output;
        QJsonObject children;
        jsonMapWrite(value.toMetadata(), output, children);
        data = QJsonValue(output);
        metadata.insert("metadata", children);
        return;
    }
    case Variant::Type::MetadataFlags: {
        metadata.insert("type", QJsonValue(QLatin1String("metaflags")));
        QJsonObject v;
        {
            QJsonObject output;
            QJsonObject children;
            jsonMapWrite(value.toMetadata(), output, children);
            v.insert("metadata", output);
            metadata.insert("metadata", children);
        }
        {
            QJsonObject output;
            QJsonObject children;
            jsonMapWrite(value.toMetadataSingleFlag(), output, children);
            v.insert("values", output);
            metadata.insert("values", children);
        }
        data = QJsonValue(v);
        return;
    }
    case Variant::Type::MetadataArray: {
        metadata.insert("type", QJsonValue(QLatin1String("metaarray")));
        QJsonObject output;
        QJsonObject children;
        jsonMapWrite(value.toMetadata(), output, children);
        data = QJsonValue(output);
        metadata.insert("metadata", children);
        return;
    }
    case Variant::Type::MetadataMatrix: {
        metadata.insert("type", QJsonValue(QLatin1String("metamatrix")));
        QJsonObject output;
        QJsonObject children;
        jsonMapWrite(value.toMetadata(), output, children);
        data = QJsonValue(output);
        metadata.insert("metadata", children);
        return;
    }
    case Variant::Type::MetadataHash: {
        metadata.insert("type", QJsonValue(QLatin1String("metahash")));
        QJsonObject v;
        {
            QJsonObject output;
            QJsonObject children;
            jsonMapWrite(value.toMetadata(), output, children);
            v.insert("metadata", output);
            metadata.insert("metadata", children);
        }
        {
            QJsonObject output;
            QJsonObject children;
            jsonMapWrite(value.toMetadataHashChild(), output, children);
            v.insert("values", output);
            metadata.insert("values", children);
        }
        data = QJsonValue(v);
        return;
    }
    case Variant::Type::MetadataKeyframe: {
        metadata.insert("type", QJsonValue(QLatin1String("metakeyframe")));
        QJsonObject output;
        QJsonObject children;
        jsonMapWrite(value.toMetadata(), output, children);
        data = QJsonValue(output);
        metadata.insert("metadata", children);
        return;
    }

    case Variant::Type::Overlay:
        metadata.insert("type", QJsonValue(QLatin1String("overlay")));
        data = QJsonValue(value.toQString());
        return;
    }

    Q_ASSERT(false);
}

void StandardDataOutput::runJSON()
{

    feedback.emitStage(QObject::tr("Writing data"),
                       QObject::tr("Data are being written out in JSON."), true);

    device->write("[\n");

    std::size_t progressCounter = outputProgressUpdateInterval;
    double finalTime = FP::undefined();
    bool isFirst = true;

    for (;;) {
        SequenceValue::Transfer processing;
        bool finishData = false;

        {
            std::unique_lock<std::mutex> lock(mutex);
            switch (state) {
            case State::Initialize:
                state = State::Running;
                break;
            case State::Running:
                if (pending.empty()) {
                    notify.wait(lock);
                    continue;
                }
                if (outputStallRequested) {
                    notify.wait(lock);
                    continue;
                }
                break;
            case State::EndPending:
                finishData = true;
                break;
            case State::TerminateRequested:
                state = State::Complete;
                pending.clear();
                lock.unlock();
                notify.notify_all();
                finished();
                return;
            case State::Complete:
                Q_ASSERT(false);
                return;
            }

            processing = std::move(pending);
            pending.clear();
        }

        if (processing.size() > stallThreshold)
            notify.notify_all();

        for (const auto &d : processing) {
            if (!progressCounter) {
                feedback.emitProgressTime(d.getStart());
                progressCounter = outputProgressUpdateInterval;
            }
            --progressCounter;
            finalTime = d.getStart();

            QJsonObject v;
            v.insert("station", d.getStationQString());
            v.insert("archive", d.getArchiveQString());
            v.insert("variable", d.getVariableQString());
            if (!d.getName().getFlavorsString().empty())
                v.insert("flavors", QString::fromStdString(d.getName().getFlavorsString()));
            if (FP::defined(d.getStart()))
                v.insert("start", Time::toISO8601(d.getStart(), true));
            if (FP::defined(d.getEnd()))
                v.insert("end", Time::toISO8601(d.getEnd(), true));
            if (d.getPriority() != 0)
                v.insert("priority", d.getPriority());

            {
                QJsonValue value;
                QJsonObject meta;
                writeJSONValue(d.getValue(), value, meta);
                v.insert("structure", meta);
                v.insert("value", value);
            }

            if (isFirst) {
                isFirst = false;
            } else {
                device->write(",\n");
            }
            device->write(QJsonDocument(v).toJson().trimmed());
        }

        if (finishData) {
            if (FP::defined(finalTime)) {
                feedback.emitProgressTime(finalTime);
            }

            device->write("\n]\n");
            device.reset();

            {
                std::lock_guard<std::mutex> lock(mutex);
                state = State::Complete;
            }
            notify.notify_all();
            finished();
            return;
        }
    }
}


ExternalSinkComponent::~ExternalSinkComponent() = default;

bool ExternalSinkComponent::requiresOutputDevice()
{ return true; }

ComponentOptions ExternalSinkComponent::getOptions()
{ return {}; }

QList<ComponentExample> ExternalSinkComponent::getExamples()
{ return {}; }

ExternalSink *ExternalSinkComponent::createDataEgress(QIODevice *device,
                                                      const ComponentOptions &options)
{
    if (!device)
        return createDataSink({}, options);
    return createDataSink(IO::Access::qio(device)->stream(), options);
}

}
}