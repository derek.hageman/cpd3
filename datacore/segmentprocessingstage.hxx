/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACORE_SEGMENTPROCESSINGSTAGE_HXX
#define CPD3DATACORE_SEGMENTPROCESSINGSTAGE_HXX

#include "core/first.hxx"

#include <vector>
#include <limits.h>
#include <QtGlobal>
#include <QThread>
#include <QDataStream>

#include "datacore/datacore.hxx"
#include "datacore/segment.hxx"
#include "datacore/stream.hxx"
#include "datacore/processingstage.hxx"
#include "core/component.hxx"
#include "core/timeutils.hxx"
#include "core/waitutils.hxx"

namespace CPD3 {
namespace Data {

#ifndef NDEBUG
#define BASICFILTER_STREAM_TIME_DEBUGGING
#endif

/**
 * An interface to a "basic" type of data filter.  This type of data filter
 * operates on one or more sets if inputs that are co-temporal and produce
 * output at exactly the same times as their input.  It may also require
 * additional inputs that are not output (which may be interpolated to the same
 * time base as the input/output sets).
 */
class CPD3DATACORE_EXPORT SegmentProcessingStage {
public:
    /**
     * The interface used to control how an unhandled input should be treated. 
     * <br>
     * In general input status (input/filter/bypassed) can only be controlled
     * when the input is first referenced.  So it must be assigned to all targets
     * when it is initially unhandled or when the first reference to it is
     * set.  However, the handling can be deferred if it is flagged as such,
     * but this results in any existing values being passed through unchanged
     * until is is assigned to sets.
     */
    class CPD3DATACORE_EXPORT SequenceHandlerControl {
    public:
        virtual ~SequenceHandlerControl();

        /**
         * Mark the given unit as a filter (input/output) for the given ID.
         * This overrides the settings if the input was marked as a pure input
         * before.  A unit can only be filtered by a single ID. 
         * 
         * @param unit          the unit to mark
         * @param targetID      the ID to use for it, must be >= 0
         */
        virtual void filterUnit(const SequenceName &unit, int targetID) = 0;

        /**
         * Mark the given unit as a pure input for given ID.  If the input is
         * already marked as input/output then this has no effect.  N
         * 
         * @param unit          the unit to mark
         * @param targetID      the ID to use for it, must be >= 0
         */
        virtual void inputUnit(const SequenceName &unit, int targetID) = 0;

        /**
         * Mark the given unit as an injected input for given ID.  If the input is
         * already marked as input/output then this has no effect.  Injected units
         * avoid input fragmentation when possible. 
         *
         * @param unit          the unit to mark
         * @param targetID      the ID to use for it, must be >= 0
         */
        virtual void injectUnit(const SequenceName &unit, int targetID);

        /**
         * Test if the given unit is handled by anything.  This does not
         * discriminate between input and output handling.
         * 
         * @param unit          the unit to test
         * @return              true if the unit is already handled
         */
        virtual bool isHandled(const SequenceName &unit) = 0;

        /**
         * Indicate that the input handling should be deferred.  This allows
         * the input status to be changed later, but may result in any
         * previous data having been bypassed (passed through unchanged).
         *
         * @param unit          the unit to mark
         */
        virtual void deferHandling(const SequenceName &unit);
    };

    virtual ~SegmentProcessingStage();

    /**
     * Called for the first appearance of an unhandled data unit.  Can use
     * the given control object to set handlers for it, if none are set then
     * the input is not used.
     * 
     * @param unit      the unhandled unit
     * @param control   the control object to use    
     */
    virtual void unhandled(const SequenceName &unit, SequenceHandlerControl *control) = 0;

    /**
     * Called whenever a set of data input/outputs is available.  All those that
     * are outputs are read out of the segment, so it should just be modified
     * with the new values.
     * 
     * @param id    the id being processed
     * @param data  the data values
     */
    virtual void process(int id, SequenceSegment &data) = 0;

    /**
     * Similar to process(int, DataMultiSegment) except that it operates on
     * the metadata for the given id set.  Note that the units in the segment
     * are the metadata ones (unit.toMeta()).  This will be called for any time
     * covered by the normal input/outputs, however the interval may not be
     * the same.
     * 
     * @param id    the id being processed
     * @param data  the metadata values
     */
    virtual void processMeta(int id, SequenceSegment &data);

    /**
     * Returns a list of additional times that the metadata should be broken
     * at.
     * 
     * @param id    the id being processed
     * @return      a list of additional times to break up
     */
    virtual QSet<double> metadataBreaks(int id);

    /**
     * Returns any additional inputs requested by this filter.  They may not
     * be available in the final data stream, but this requests them from the
     * originating source if possible.  It is acceptable to request inputs
     * that are known to be provided (which has no effect).
     * 
     * @return a list of requested inputs
     */
    virtual SequenceName::Set requestedInputs();

    /**
     * Returns any additional outputs not in the input data stream that this
     * filter expects to generate.  It is acceptable to report producing 
     * outputs that do not actually exist in the final stream.
     * 
     * @return a list of predicted outputs
     */
    virtual SequenceName::Set predictedOutputs();

    /**
     * Perform the serialization of this filter into the given stream.
     * 
     * @param stream    the stream to serialize into
     */
    virtual void serialize(QDataStream &stream);

    /**
     * Clear smoothing metadata if it can't be propagated.  Generally
     * this means clear difference measurement calculated metadata.
     *
     * @param smoothing the smoothing metadata
     */
    static void clearPropagatedSmoothing(Variant::Write &smoothing);

    /** @see clearPropagatedSmoothing(Variant::Write &) */
    static void clearPropagatedSmoothing(Variant::Write &&smoothing);
};


/**
 * The component interface for a basic filter object.  This allows it to be
 * loaded as a Qt plugin and instances of it created as needed.  Normally
 * this is not used directly, instead the component is used as a general
 * filter, but this interface allows direct access to the internal
 * basic filter.  It provides wrapping with the appropriate handler
 * to implement the general filter calls.  In general if this is used
 * the GeneralFilter calls should not be overridden and the implementations
 * provided in the basic filter analogs.
 */
class CPD3DATACORE_EXPORT SegmentProcessingStageComponent : public ProcessingStageComponent {
public:
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent);

    virtual ~SegmentProcessingStageComponent();

    /**
     * Create a filter object from the given options to work on a fully
     * dynamic stream.  This is used for command line chain construction.
     * <br>
     * This allocates the object with new, so the caller must delete it when 
     * finished.
     * 
     * @param options   the options to use
     * @return          a newly allocated filter object
     */
    virtual SegmentProcessingStage *createBasicFilterDynamic(const ComponentOptions &options = {}) = 0;

    /**
     * Create a filter object from the given options and expected predefined
     * inputs.  This is used for GUI filter chain construction.
     * <br>
     * This allocates the object with new, so the caller must delete it when 
     * finished.
     * 
     * @param options   the options to use
     * @param start     the start of the data
     * @param end       the end of the data
     * @param inputs    the expected inputs
     * @return          a newly allocated filter object
     */
    virtual SegmentProcessingStage *createBasicFilterPredefined(const ComponentOptions &options,
                                                                double start,
                                                                double end,
                                                                const QList<SequenceName> &inputs);

    /**
     * Create a filter object for use as a component of an editing stream.
     * <br>
     * This allocates the object with new, so the caller must delete it when 
     * finished.
     * 
     * @param start     the start of the data
     * @param end       the end of the data
     * @param station   the station
     * @param archive   the input archive (usually raw)
     * @param config    the configuration data
     * @return          a newly allocated filter object
     */
    virtual SegmentProcessingStage *createBasicFilterEditing(double start,
                                                             double end,
                                                             const SequenceName::Component &station,
                                                             const SequenceName::Component &archive,
                                                             const ValueSegment::Transfer &config);

    /**
     * Create a filter object from a serialized copy of it.  This is used
     * for the acceleration mechanisms for command line invocation.  The
     * containing filter handler MUST be de-serialized to a compatible state 
     * from when the original filter was serialized.
     * <br>
     * This allocates the object with new, so the caller must delete it when 
     * finished.
     * @param stream    the stream containing the serialized copy
     * @return          a newly allocated filter object
     */
    virtual SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);

    /**
     * Called before creation of a filter object to allow an extension request
     * to editing chain.  This allows for the request of more edited data than
     * was requested at the editing level.  For example, this can be used to
     * request the start of the current PSAP filter for integrating 
     * corrections.
     * 
     * @param start     the input and output start of the request
     * @param end       the input and output start of the request
     * @param station   the station
     * @param archive   the input archive (usually raw)
     * @param config    the configuration data
     * @param access    the archive access, if any
     * @see ProcessingStageComponent::extendBasicFilterEditing(start, end, station, archive, config, access)
     */
    virtual void extendBasicFilterEditing(double &start,
                                          double &end,
                                          const SequenceName::Component &station,
                                          const SequenceName::Component &archive,
                                          const ValueSegment::Transfer &config,
                                          CPD3::Data::Archive::Access *access = nullptr);

    /** @see getGeneralSerializationName() */
    virtual QString getBasicSerializationName() const;

    virtual ProcessingStage *createGeneralFilterDynamic(const ComponentOptions &options = {});

    virtual ProcessingStage *createGeneralFilterPredefined(const ComponentOptions &options,
                                                           double start,
                                                           double end,
                                                           const QList<SequenceName> &inputs);

    virtual ProcessingStage *createGeneralFilterEditing(double start,
                                                        double end,
                                                        const SequenceName::Component &station,
                                                        const SequenceName::Component &archive,
                                                        const ValueSegment::Transfer &config);

    virtual ProcessingStage *deserializeGeneralFilter(QDataStream &stream);

    virtual void extendGeneralFilterEditing(double &start,
                                            double &end,
                                            const SequenceName::Component &station,
                                            const SequenceName::Component &archive,
                                            const ValueSegment::Transfer &config,
                                            CPD3::Data::Archive::Access *access);

    virtual QString getGeneralSerializationName() const;
};


/**
 * A wrapper to service a segment processor.  This is generally what is returned by the
 * component creation utilities.
 */
class CPD3DATACORE_EXPORT SegmentProcessingHandler : public AsyncProcessingStage {
    class Set;

    class Dispatch;

    class Handler : public SegmentProcessingStage::SequenceHandlerControl {
    private:
        SegmentProcessingHandler *parent;

        void setUnitInputs(const SequenceName &unit,
                           Set *target,
                           Dispatch *dispatchData,
                           Dispatch *dispatchMeta);

    public:
        QSet<int> updated;
        double effectiveStart;

        Handler(SegmentProcessingHandler *setParent);

        virtual void filterUnit(const SequenceName &unit, int targetID);

        virtual void inputUnit(const SequenceName &unit, int targetID);

        virtual void injectUnit(const SequenceName &unit, int targetID);

        virtual bool isHandled(const SequenceName &unit);

        virtual void deferHandling(const SequenceName &unit);
    };

    Handler handler;

    friend class Handler;

    SegmentProcessingStage *filter;
    std::unique_ptr<SegmentProcessingStage> ownFilter;

    class Set {
    public:
        Set(SegmentProcessingHandler *setParent, int setID);

        Set(SegmentProcessingHandler *setParent, int setID, QDataStream &stream);

        SegmentProcessingHandler *parent;
        int id;

        SequenceSegment::Stream reader;
        std::deque<SequenceSegment> pendingProcessing;
        std::vector<SequenceName> outputs;
        QSet<SequenceName> inputs;
        QSet<SequenceName> injected;

        SequenceSegment::Stream metaReader;
        std::deque<SequenceSegment> metaPendingProcessing;
        std::vector<SequenceName> metaOutputs;

#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        double lastIncomingTime;
#endif

        void incomingData(const SequenceValue &value);

        void incomingInjected(const SequenceValue &value);

        void incomingMeta(const SequenceValue &value);

        void advanceTime(double time);

        void finish();

        void serialize(QDataStream &stream) const;
    };

    std::vector<std::unique_ptr<Set>> sets;

    friend class Set;

    Set *getSet(int targetID);

    bool isAnOutput(const SequenceName &unit) const;

    class Dispatch {
    public:
        virtual ~Dispatch()
        { }

        bool allowBypass;

        virtual void incoming(const SequenceValue &value) = 0;

        virtual bool referencesSet(const Set *set) const;

        virtual void serialize(QDataStream &stream) const = 0;
    };

    class DefaultDispatch : public Dispatch {
    public:
        DefaultDispatch();

        std::unique_ptr<SequenceValue> latest;
        std::unique_ptr<SequenceSegment::Stream> reader;

        virtual ~DefaultDispatch();

        virtual void incoming(const SequenceValue &value);

        virtual void serialize(QDataStream &stream) const;

        void advanceTime(double time);

        void overlay(SequenceSegment::Stream &target, double effectiveStart) const;
    };

    class SetDataDispatch : public Dispatch {
    public:
        SetDataDispatch(Set *setTarget);

        virtual ~SetDataDispatch()
        { }

        Set *target;

        virtual void incoming(const SequenceValue &value);

        virtual bool referencesSet(const Set *set) const;

        virtual void serialize(QDataStream &stream) const;
    };

    class SetMetaDispatch : public Dispatch {
    public:
        SetMetaDispatch(Set *setTarget);

        virtual ~SetMetaDispatch()
        { }

        Set *target;

        virtual void incoming(const SequenceValue &value);

        virtual bool referencesSet(const Set *set) const;

        virtual void serialize(QDataStream &stream) const;
    };

    class SetPureInputDispatch : public Dispatch {
    public:
        SetPureInputDispatch(Set *setTarget);

        virtual ~SetPureInputDispatch()
        { }

        Set *target;

        virtual void incoming(const SequenceValue &value);

        virtual bool referencesSet(const Set *set) const;

        virtual void serialize(QDataStream &stream) const;
    };

    class SetInjectInputDispatch : public Dispatch {
    public:
        SetInjectInputDispatch(Set *setTarget);

        virtual ~SetInjectInputDispatch()
        { }

        Set *target;

        virtual void incoming(const SequenceValue &value);

        virtual bool referencesSet(const Set *set) const;

        virtual void serialize(QDataStream &stream) const;
    };

    class SetPureInputMetaDispatch : public Dispatch {
    public:
        SetPureInputMetaDispatch(Set *setTarget);

        virtual ~SetPureInputMetaDispatch()
        { }

        Set *target;

        virtual void incoming(const SequenceValue &value);

        virtual bool referencesSet(const Set *set) const;

        virtual void serialize(QDataStream &stream) const;
    };

    SequenceName::Map<std::vector<Dispatch *>> dispatch;
    std::vector<std::unique_ptr<Dispatch>> uniqueDispatch;

    std::deque<SequenceValue> bypassed;

    std::vector<Dispatch *> &dispatchDefaultFind(const SequenceName &unit);

    std::vector<Dispatch *> &dispatchIncoming(const SequenceValue &inc);

    static void overlayDefault(std::vector<Dispatch *> &def, SequenceSegment::Stream &target,
                               double effectiveStart);

    static void removeSetReference(std::vector<Dispatch *> &d, const Set *set);

    void pruneUnusedDispatch();

#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
    double lastOutputTime;
#endif

    void flushUndefined();

    void dumpBypassedBefore(double before, SequenceValue::Transfer &output);

    struct FlushState {
        struct TimingElement {
            enum Type {
                Metadata, Data, Limiter
            };
            Type type;
            Set *set;

            inline TimingElement(Type type, Set *set) : type(type), set(set)
            { }
        };

        std::multimap<double, TimingElement> startTiming;
        bool finish;
        double before;

        void injectMeta(Set *set);

        void injectData(Set *set);
    };

    bool processState(FlushState &state, SequenceValue::Transfer &output);

    void flushData(double before, bool finish = false);

public:
    SegmentProcessingHandler(SegmentProcessingStage *setFilter, bool takeOwnership = true);

    SegmentProcessingHandler(QDataStream &stream,
                             SegmentProcessingStage *setFilter,
                             bool takeOwnership = true);

    virtual ~SegmentProcessingHandler();

    using StreamSink::incomingData;

    virtual void serialize(QDataStream &stream);

    virtual SequenceName::Set requestedInputs();

    virtual SequenceName::Set predictedOutputs();

protected:
    virtual void process(SequenceValue::Transfer &&incoming);

    virtual void finish();
};

}
}

Q_DECLARE_INTERFACE(CPD3::Data::SegmentProcessingStageComponent,
                    "CPD3.Data.SegmentProcessingStageComponent/1.0");

#endif
