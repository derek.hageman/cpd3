/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACORE_EXTERNALSOURCE_HXX
#define CPD3DATACORE_EXTERNALSOURCE_HXX

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <QRunnable>

#include "datacore/datacore.hxx"
#include "datacore/stream.hxx"
#include "core/actioncomponent.hxx"
#include "core/threadpool.hxx"
#include "core/waitutils.hxx"
#include "core/util.hxx"

class QXmlStreamWriter;

namespace CPD3 {
namespace Data {

class XMLDataInputParser;


/**
 * The base class for external sources of data.  These are source that
 * produce a stream of data from some external origin.
 */
class CPD3DATACORE_EXPORT ExternalSource : public StreamSource {
public:
    ExternalSource() = default;

    virtual ~ExternalSource();

    /**
     * Signals for the thread to terminate even if it's not yet done.  Will
     * send endData() to the egress if any.
     */
    virtual void signalTerminate() = 0;

    /**
     * Test if the input has finished execution.
     *
     * @return  true if the input has finished
     */
    virtual bool isFinished() = 0;

    /**
     * Launch the data input.
     */
    virtual void start() = 0;

    /**
     * Wait for the input to finish execution.
     * <br>
     * The default implementation does a polling loop on isFinished()
     *
     * @param timeout   the maximum time to wait
     * @return          true if the input has finished
     */
    virtual bool wait(double timeout = FP::undefined());

    /**
     * Returns any predicted outputs int the output stream.  This does not
     * have to be accurate (e.x. it may report nothing).  It is acceptable to
     * report producing outputs that do not actually exist in the final stream.
     *
     * @return a list of predicted outputs
     */
    virtual SequenceName::Set predictedOutputs();

    /**
     * Emitted when the input has finished executing.
     */
    Threading::Signal<> finished;
};

/**
 * The abstract thread that can read data.  Note that this cannot read directly
 * from a QIODevice, since that access must be done in the thread that created
 * the device.
 */
class CPD3DATACORE_EXPORT ExternalConverter : public ExternalSource {
public:
    ExternalConverter() = default;

    virtual ~ExternalConverter();

    /**
     * Add some incoming data.
     * 
     * @param data  the data to add
     */
    virtual void incomingData(const Util::ByteView &data) = 0;

    virtual void incomingData(const Util::ByteArray &data);

    virtual void incomingData(Util::ByteArray &&data);

    virtual void incomingData(const QByteArray &data);

    virtual void incomingData(QByteArray &&data);

    void incomingData(const std::string &data);

    void incomingData(const QString &data);

    void incomingData(const char *data);

    /**
     * Signal that the data stream has finished.
     */
    virtual void endData() = 0;

    /**
     * Emitted when an acceleration key is available on the stream.
     */
    Threading::Signal<const std::string &> accelerationKey;

    /**
     * Emitted when the stall request state changes.  When a stall is requested, the
     * caller should ideally stop sending data until the stall is cleared.
     */
    Threading::Signal<bool> stallRequested;
};

class StandardDataInput;

/**
 * A base class for input processors that run on a simple thread.
 */
class CPD3DATACORE_EXPORT ExternalSourceProcessor : public ExternalConverter {
    friend class StandardDataInput;

    std::thread thread;
    std::mutex mutex;
    std::mutex egressMutex;
    std::condition_variable externalChanged;
    enum class State {
        Initialize, Running, Terminating, InitializeTerminate, Completed,
    } state;

    std::unique_lock<std::mutex> egressLock;

    void run();

protected:
    /**
     * The output egress.  Will never be NULL when prepare() or process()
     * is called, but may be NULL when begin() and finish() are.
     */
    StreamSink *egress;

    /**
     * A wait condition for internal state changes.
     */
    std::condition_variable internalChanged;

    /**
     * The full processing buffer.
     */
    Util::ByteArray buffer;

    /**
     * Set to true when the stream has ended externally.  That is, the
     * endData() call sets this to true.
     */
    bool dataEnded;

    /**
     * Test if the input processor is currently stalled, based on the buffer size.
     * The mutex must be held.
     *
     * @return  true if the processor is currently stalled
     */
    bool isStalled() const;

    /**
     * Prepare for processing.  This is called with the mutex held.
     * 
     * @return true if there is processing to do or if the processor is complete
     */
    virtual bool prepare() = 0;

    /**
     * Execute processing.  This is called WITHOUT the mutex held, so it
     * must only operate on data that has been copied out.  This is only
     * called when the egress is not NULL, so it can freely convert data
     * and output it.
     *
     * @return true if more processing can be done, false causes the processor to end
     */
    virtual bool process() = 0;

    /**
     * Called before any processing is done after start up.  This is called
     * WITHOUT the mutex held.
     * <br>
     * The default implementation does nothing and returns true.
     *
     * @return false to immediately end the processor
     */
    virtual bool begin();

    /**
     * Called after the last processing step, before locking the egress.
     * his is called WITHOUT the mutex held.
     * <br>
     * The default implementation does nothing.
     */
    virtual void finalize();

    /**
     * Called after the last processing step.  This is called WITHOUT the
     * mutex held.
     * <br>
     * The default implementation does nothing.
     */
    virtual void finish();

    /**
     * Can be called from the process() function to check for termination.
     * This can be used to allow the process() function to abort early.
     *
     * @param lockMutex if set then the mutex is locked, otherwise it is assumed that it was already locked
     * @return          true if the processor has been terminated
     */
    bool isTerminated(bool lockMutex = true);

    /**
     * Send data to the egress.  This may only be called when the egress
     * is not NULL.
     *
     * @param values    the values to output
     */
    inline void outputData(const SequenceValue::Transfer &values)
    {
        Q_ASSERT(egressLock);
        Q_ASSERT(egress != nullptr);
        egress->incomingData(values);
    }

    /**
     * Send data to the egress.  This may only be called when the egress
     * is not NULL.
     *
     * @param values    the value to output
     */
    inline void outputData(const SequenceValue &value)
    {
        Q_ASSERT(egressLock);
        Q_ASSERT(egress != nullptr);
        egress->incomingData(value);
    }

    /**
     * Send data to the egress.  This may only be called when the egress
     * is not NULL.
     *
     * @param values    the values to output
     */
    inline void outputData(SequenceValue::Transfer &&values)
    {
        Q_ASSERT(egressLock);
        Q_ASSERT(egress != nullptr);
        egress->incomingData(std::move(values));
    }

    /**
     * Send data to the egress.  This may only be called when the egress
     * is not NULL.
     *
     * @param values    the value to output
     */
    inline void outputData(SequenceValue &&value)
    {
        Q_ASSERT(egressLock);
        Q_ASSERT(egress != nullptr);
        egress->incomingData(std::move(value));
    }

    /**
     * Acquire the main lock.
     *
     * @return the main lock
     */
    inline std::unique_lock<std::mutex> acquireLock()
    { return std::unique_lock<std::mutex>(mutex); }

    /**
     * Release the egress lock if it is held.
     */
    void releaseEgressLock();

    /**
     * Relock the mutex in processing.  This unlocks the egress lock, so processing
     * should return before touching the egress afterwards.
     *
     * @return the lock on the mutex
     */
    std::unique_lock<std::mutex> processingRelock();

    /**
     * Notify the worker that something has changed and it needs to check
     * for processing again.
     */
    void externalNotify();

public:
    ExternalSourceProcessor();

    virtual ~ExternalSourceProcessor();

    using ExternalConverter::incomingData;

    void incomingData(const Util::ByteView &data) override;

    void incomingData(Util::ByteArray &&data) override;

    void endData() override;

    bool isFinished() override;

    void start() override;

    bool wait(double timeout = FP::undefined()) override;

    void setEgress(StreamSink *setEgress) override;

    void signalTerminate() override;
};

namespace Internal {
class StandardDataInputXMLHandler;

struct XMLContainer;
};

/**
 * Provides an interface to read data from an input source.
 */
class CPD3DATACORE_EXPORT StandardDataInput : public ExternalSourceProcessor {
    bool isHeader;
    Util::ByteArray processingBuffer;
    bool processingEnded;
    bool bufferConsumed;

    enum class InputType {
        Unidentified, Direct, XML, Raw, JSON, CPD2, EBAS
    } type;
    std::string processAccelerationKey;

    std::unique_ptr<ExternalConverter> child;

    class DiscardIngress : public StreamSink {
    public:
        DiscardIngress();

        virtual ~DiscardIngress();

        void incomingData(const SequenceValue::Transfer &values) override;

        void incomingData(SequenceValue::Transfer &&values) override;

        void incomingData(const SequenceValue &value) override;

        void incomingData(SequenceValue &&value) override;

        void endData() override;
    };

    DiscardIngress childDiscard;

    std::vector<SequenceName> indexLookup;
    std::uint16_t nextIndex;

    friend class Internal::StandardDataInputXMLHandler;

    std::unique_ptr<Internal::XMLContainer> xmlContainer;

    std::size_t processBasicSerialization(QDataStream &stream, SequenceValue::Transfer &result);

    bool processDirect();

    bool processXML();

    bool processRaw();

    bool processJSON();

    bool createExternal(const QString &name);

    bool processExternal();

    bool identifyStream();

public:
    StandardDataInput();

    virtual ~StandardDataInput();

    void incomingData(const Util::ByteView &data) override;

    void incomingData(const Util::ByteArray &data) override;

    void incomingData(Util::ByteArray &&data) override;

    void incomingData(const QByteArray &data) override;

    void incomingData(QByteArray &&data) override;

    void endData() override;

    void setEgress(StreamSink *setEgress) override;

    void signalTerminate() override;

protected:
    bool prepare() override;

    bool process() override;
};


/**
 * An interface for a component that represents an ingress point for data.
 * Generally this accepts a stream of raw data and converts it into CPD3
 * values.
 */
class CPD3DATACORE_EXPORT ExternalConverterComponent {
public:
    virtual ~ExternalConverterComponent();

    /**
     * Returns true if this egress point requires a valid QIODevice to
     * operate on (the default).  This should return false for things that
     * can only ever read from a fixed location (for example, automatic
     * legacy conversion).  If this returns false then the incomingData() and
     * endData() methods may never be called.
     *
     * @return true if this component uses a device
     */
    virtual bool requiresInputDevice();

    /**
     * Get an options object (set to all defaults) for this ingress point.
     *
     * @return an options object for this filter type
     */
    virtual ComponentOptions getOptions();

    /**
     * Gets a list of examples that show usage of the component.  The caller
     * will use the specified options to construct usage in the context it's
     * called (e.x. command lines).
     *
     * @return a list in display order of examples
     */
    virtual QList<ComponentExample> getExamples();

    /**
     * Creates the input object for the given options and device.  Note that
     * data is read only by he calling thread, and passed via the
     * incomingData() and endData() interfaces.
     *
     * @param options   the options for the input
     * @return          a DataInput object to read with
     */
    virtual ExternalConverter *createDataIngress(const ComponentOptions &options = ComponentOptions()) = 0;
};

/**
 * An interface for a component that represents an ingress point for data.
 * Generally this produces CPD3 data from some external source.
 */
class CPD3DATACORE_EXPORT ExternalSourceComponent {
public:
    virtual ~ExternalSourceComponent();

    /**
     * Get an options object (set to all defaults) for this ingress point.
     *
     * @return an options object for this filter type
     */
    virtual ComponentOptions getOptions();

    /**
     * Gets a list of examples that show usage of the component.  The caller
     * will use the specified options to construct usage in the context it's
     * called (e.x. command lines).
     *
     * @return a list in display order of examples
     */
    virtual QList<ComponentExample> getExamples();

    /**
     * Get the number of stations allowed to be specified.  The default
     * implementation returns zero.  Return INT_MAX to disable the maximum.
     *
     * @return the number of allowed stations to be specified
     */
    virtual int ingressAllowStations();

    /**
     * Get the number of stations required to be specified.  The default
     * implementation returns zero.
     *
     * @return the number of required stations to be specified
     */
    virtual int ingressRequireStations();

    /**
     * This should return true if the ingress always requires time bounds.
     * The default implementation returns false.
     *
     * @return true if the action requires a time range
     */
    virtual bool ingressRequiresTime();

    /**
     * This should return true if the ingress can accept undefined/infinite
     * time bounds.  The default implementation returns true.
     *
     * @return true if the action accepts infinite bounds
     */
    virtual bool ingressAcceptsUndefinedBounds();

    /**
     * This should return the default time unit for parsing times.  For example
     * setting this to a week allows a string like "2010 2" to specify the
     * second week of 2010.  The default implementation returns Time::Day.
     *
     * @return the default unit for time parsing
     * @see parseListBounds( const QStringList &, const bool, const bool,
     *          Time::LogicalTimeUnit )
     */
    virtual Time::LogicalTimeUnit ingressDefaultTimeUnit();

    /**
     * Create a ingress object from the given options and start time range.
     * <br>
     * This allocates the object with new, so the caller must delete it when
     * finished.
     *
     * @param options   the options to use
     * @param start     the start time
     * @param end       the end time
     * @param stations  the stations specified, only valid if the mode allows it
     */
    virtual ExternalConverter *createExternalIngress(const ComponentOptions &options,
                                                     double start,
                                                     double end,
                                                     const std::vector<
                                                             SequenceName::Component> &stations = {}) = 0;

    /**
     * Create a ingress object from the given options.  This is called when no
     * time range was specified and only if ingressRequiresTime() returns false.
     * The default implmentation calls createExternalIngress(
     * const ComponentOptions &, double, double ) with both bounds set to
     * FP::undefined().
     * <br>
     * This allocates the object with new, so the caller must delete it when
     * finished.
     *
     * @param options   the options to use
     * @param stations  the stations specified, only valid if the mode allows it
     */
    virtual ExternalConverter *createExternalIngress(const ComponentOptions &options = {},
                                                     const std::vector<
                                                             SequenceName::Component> &stations = {});
};

}
}

Q_DECLARE_INTERFACE(CPD3::Data::ExternalConverterComponent,
                    "CPD3.Data.ExternalConverterComponent/1.0");

Q_DECLARE_INTERFACE(CPD3::Data::ExternalSourceComponent, "CPD3.Data.ExternalSourceComponent/1.0");

#endif
