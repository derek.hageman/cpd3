/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACORE_DYNAMICSEQUENCESELECTION_HXX
#define CPD3DATACORE_DYNAMICSEQUENCESELECTION_HXX

#include "core/first.hxx"

#include <vector>
#include <deque>
#include <memory>
#include <QtGlobal>
#include <QDataStream>
#include <QList>
#include <QSet>
#include <QDebug>

#include "datacore/datacore.hxx"
#include "datacore/segment.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/valueoptionparse.hxx"
#include "datacore/sequencematch.hxx"
#include "core/component.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Data {

class DynamicSequenceSelection;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                            const DynamicSequenceSelection *to);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, DynamicSequenceSelection *&to);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const DynamicSequenceSelection *to);

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                            const std::unique_ptr<DynamicSequenceSelection> &to);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                            std::unique_ptr<DynamicSequenceSelection> &to);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream,
                                      const std::unique_ptr<DynamicSequenceSelection> &to);

/**
 * A possibly time dependent list of variables to operate on.
 */
class CPD3DATACORE_EXPORT DynamicSequenceSelection {
public:
    virtual ~DynamicSequenceSelection();

    /**
     * Get all units that this operation is effective on in the given time
     * range.  This will advance the time, so no requests before the start time 
     * may be made after the call.
     * 
     * @param start     the start of the desired interval
     * @param end       the end of the desired interval
     * @return          the set of effective units
     */
    virtual const SequenceName::Set &get(double start, double end);

    /**
     * Test if the selection matches a specified name.
     *
     * @param start     the start of the desired interval
     * @param end       the end of the desired interval
     * @param name      the name to test
     * @return          true if the sequence matches the name
     */
    virtual bool matches(double start, double end, const SequenceName &name);

    /** Same as get(range.getStart(), range.getEnd())
     * @see get(double, double) */
    template<typename T>
    inline const SequenceName::Set &get(const T &range)
    { return get(range.getStart(), range.getEnd()); }

    /** Same as matches(range.getStart(), range.getEnd(), name)
     * @see matches(double, double, const SequenceName &) */
    template<typename T>
    inline bool matches(const T &range, const SequenceName &name)
    { return matches(range.getStart(), range.getEnd(), name); }

    /**
     * Get all units that this operation is effective on in the given time
     * range.
     * 
     * @param start     the start of the desired interval
     * @param end       the end of the desired interval
     * @return          the set of effective units
     */
    virtual const SequenceName::Set &getConst(double start, double end) const = 0;

    /**
     * Test if the selection matches a specified name.
     *
     * @param start     the start of the desired interval
     * @param end       the end of the desired interval
     * @param name      the name to testx
     * @return          true if the sequence matches the name
     */
    virtual bool matchesConst(double start, double end, const SequenceName &name) const;

    /** @see getConst(double, double) */
    inline const SequenceName::Set &get(double start, double end) const
    { return getConst(start, end); }

    /** @see matchesConst(double, double) */
    inline bool matches(double start, double end, const SequenceName &name) const
    { return matchesConst(start, end, name); }

    /** Same as getConst(range.getStart(), range.getEnd())
     * @see getConst(double, double) */
    template<typename T>
    inline const SequenceName::Set &getConst(const T &range) const
    { return getConst(range.getStart(), range.getEnd()); }

    /** Same as getConst(T)
     * @see getConst(const T &) */
    template<typename T>
    inline const SequenceName::Set &get(const T &range) const
    { return getConst(range.getStart(), range.getEnd()); }

    /** Same as matchesConst(range.getStart(), range.getEnd(), name)
     * @see matchesConst(double, double, const SequenceName &) */
    template<typename T>
    inline bool matchesConst(const T &range, const SequenceName &name) const
    { return matchesConst(range.getStart(), range.getEnd(), name); }

    /** Same as matchesConst(T, name)
     * @see matchesConst(const T &, const SequenceName &) */
    template<typename T>
    inline bool matches(const T &range, const SequenceName &name) const
    { return matchesConst(range.getStart(), range.getEnd(), name); }

    /**
     * Get all units that this operation is effective on in the given time
     * This will advance the time, so no requests before the time may be made 
     * after the call.
     * 
     * @param time      the instantaneous time
     * @return          the set of effective units
     */
    virtual const SequenceName::Set &get(double time);

    /**
     * Test if the selection matches the specified name at a given time.
     *
     * @param time      the instantaneous time
     * @param name      the name to check
     * @return          true if there is a match
     */
    virtual bool matches(double time, const SequenceName &name);

    /**
     * Get all units that this operation is effective on in the given time.
     * 
     * @param time      the instantaneous time
     * @return          the set of effective units
     */
    virtual const SequenceName::Set &getConst(double time) const = 0;

    /**
     * Test if the selection matches the specified name at a given time.
     *
     * @param time      the instantaneous time
     * @param name      the name to check
     * @return          true if there is a match
     */
    virtual bool matchesConst(double time, const SequenceName &name) const;

    /** @see getConst(double) */
    inline const SequenceName::Set &get(double time) const
    { return getConst(time); }

    /** @see matchesConst(double) */
    inline bool matches(double time, const SequenceName &name) const
    { return matchesConst(time, name); }

    /**
     * Register an input for this list of variables.  Returns true if it could
     * be used.
     * 
     * @param input     the input unit to test
     * @return          true if the input is used
     */
    virtual bool registerInput(const SequenceName &input);

    /**
     * Register a set of expected matched parameters.  This allows the selection
     * to determine the used inputs before they actually appear in the data
     * stream.  If any component is empty is is treated as unknown and required
     * to be exact before a component can be used.  That is specifying a
     * station and archive will allow all inputs that specify exactly a single
     * variable to be known.  The flavors are considered unknown.
     * 
     * @param station   the station to expect or empty for unknown
     * @param archive   the archive to expect or empty for unknown
     * @param variable  the variable to expect or empty for unknown
     */
    virtual void registerExpectedFlavorless(const SequenceName::Component &station = {},
                                            const SequenceName::Component &archive = {},
                                            const SequenceName::Component &variable = {});

    /**
     * The same as registerExpectedFlavorless( const SequenceName::Component &,
     * const SequenceName::Component &,  const SequenceName::Component & )
     * except that the flavors are known.
     * 
     * @param station       the station to expect or empty for unknown
     * @param archive       the archive to expect or empty for unknown
     * @param variable      the variable to expect or empty for unknown
     * @param flavors       the flavors to expect
     * @see registerExpectedFlavorless( const SequenceName::Component &, const SequenceName::Component &, const SequenceName::Component & )
     */
    virtual void registerExpected(const SequenceName::Component &station,
                                  const SequenceName::Component &archive,
                                  const SequenceName::Component &variable,
                                  const SequenceName::Flavors &flavors);

    /**
     * The same as registerExpected( const SequenceName::Component &, const SequenceName::Component &,
     * const SequenceName::Component &, const SequenceName::Flavors & ) except that the flavors are
     * known only as a set of possibilities
     * 
     * @param station       the station to expect or empty for unknown
     * @param archive       the archive to expect or empty for unknown
     * @param variable      the variable to expect or empty for unknown
     * @param flavors       the sets of flavors to expect
     * @see registerExpectedFlavorless( const SequenceName::Component &, const SequenceName::Component &, const SequenceName::Component & )
     */
    virtual void registerExpected(const SequenceName::Component &station = {},
                                  const SequenceName::Component &archive = {},
                                  const SequenceName::Component &variable = {},
                                  const std::unordered_set<
                                          SequenceName::Flavors> &flavors = SequenceName::defaultDataFlavors());

    /**
     * Convert the sequence selection to a list of archive selections to read.
     *
     * @param defaultStations   the stations to set by default
     * @param defaultArchives   the archives to set by default
     * @param defaultVariables  the variable to set by default
     * @return                  approximate archive selections
     */
    virtual Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                         const Archive::Selection::Match &defaultArchives = {},
                                                         const Archive::Selection::Match &defaultVariables = {}) const = 0;

    /**
     * Get all known input values.
     * 
     * @return  a list of known inputs that this input uses
     */
    virtual SequenceName::Set getAllUnits() const;

    /**
     * Get all known times when the "structure" of this operation might change.
     * Generally this should include times when the actual variables being
     * operated on would be changed.
     * 
     * @return  a list of known changed times
     */
    virtual QSet<double> getChangedPoints() const;

    /**
     * Duplicate this object, including polymorphism.  Allocates a new object
     * that must be deleted by the caller.
     * 
     * @return      the copy
     */
    virtual DynamicSequenceSelection *clone() const = 0;

    /**
     * Force all components of the operate selection to match the given
     * unit components.  Empty components are not changed.
     * 
     * @param station   the station to match
     * @param archive   the archive to match
     * @param variable  the variable to match
     */
    virtual void forceUnit(const SequenceName::Component &station,
                           const SequenceName::Component &archive = {},
                           const SequenceName::Component &variable = {});

    /**
     * Force all components of the operate selection to match the given
     * unit components.  Empty components are not changed.
     * 
     * @param station   the station to match
     * @param archive   the archive to match
     * @param variable  the variable to match
     * @param flavors   the flavors to match (always set)
     */
    virtual void forceUnit(const SequenceName::Component &station,
                           const SequenceName::Component &archive,
                           const SequenceName::Component &variable,
                           const SequenceName::Flavors &flavors);


    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicSequenceSelection *to);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       DynamicSequenceSelection *&to);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const DynamicSequenceSelection *to);


    static DynamicSequenceSelection *fromConfiguration(SequenceSegment::Transfer &config,
                                                       const SequenceName &unit,
                                                       const QString &path = QString(),
                                                       double start = FP::undefined(),
                                                       double end = FP::undefined());

    static DynamicSequenceSelection *fromConfiguration(const ValueSegment::Transfer &config,
                                                       const QString &path = QString(),
                                                       double start = FP::undefined(),
                                                       double end = FP::undefined());

    class None;

    class Composite;

    class Single;

    class Basic;

    class Match;

    class Variable;

protected:
    virtual void serialize(QDataStream &stream) const = 0;

    virtual void printLog(QDebug &stream) const;
};

/**
 * An operate that never operates on anything.  This exists to allow for
 * a non-null type to be returned for no-ops.
 */
class CPD3DATACORE_EXPORT DynamicSequenceSelection::None : public DynamicSequenceSelection {
public:
    None();

    virtual ~None();

    const SequenceName::Set &get(double start, double end) override;

    const SequenceName::Set &get(double time) override;

    const SequenceName::Set &getConst(double start, double end) const override;

    const SequenceName::Set &getConst(double time) const override;

    bool matches(double start, double end, const SequenceName &name) override;

    bool matchesConst(double start, double end, const SequenceName &name) const override;

    bool matches(double time, const SequenceName &name) override;

    bool matchesConst(double time, const SequenceName &name) const override;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override;

    DynamicSequenceSelection *clone() const override;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicSequenceSelection *to);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       DynamicSequenceSelection *&to);

protected:
    None(QDataStream &);

    void serialize(QDataStream &stream) const override;

    void printLog(QDebug &stream) const override;
};

/**
 * A wrapper that allows for multiple time operates to be combined.
 */
class CPD3DATACORE_EXPORT DynamicSequenceSelection::Composite : public DynamicSequenceSelection {
    std::vector<std::unique_ptr<DynamicSequenceSelection>> components;
public:
    Composite();

    Composite(const Composite &) = delete;

    Composite &operator=(const Composite &) = delete;

    explicit Composite(std::unique_ptr<DynamicSequenceSelection> &&base);

    explicit Composite(std::vector<std::unique_ptr<DynamicSequenceSelection>> &&bases);

    virtual ~Composite();

    const SequenceName::Set &get(double start, double end) override;

    const SequenceName::Set &get(double time) override;

    const SequenceName::Set &getConst(double start, double end) const override;

    const SequenceName::Set &getConst(double time) const override;

    SequenceName::Set getAllUnits() const override;

    bool matches(double start, double end, const SequenceName &name) override;

    bool matchesConst(double start, double end, const SequenceName &name) const override;

    bool matches(double time, const SequenceName &name) override;

    bool matchesConst(double time, const SequenceName &name) const override;

    bool registerInput(const SequenceName &input) override;

    void registerExpectedFlavorless(const SequenceName::Component &station = {},
                                    const SequenceName::Component &archive = {},
                                    const SequenceName::Component &variable = {}) override;

    void registerExpected(const SequenceName::Component &station,
                          const SequenceName::Component &archive,
                          const SequenceName::Component &variable,
                          const SequenceName::Flavors &flavors) override;

    void registerExpected(const SequenceName::Component &station = {},
                          const SequenceName::Component &archive = {},
                          const SequenceName::Component &variable = {},
                          const std::unordered_set<
                                  SequenceName::Flavors> &flavors = SequenceName::defaultDataFlavors()) override;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override;

    DynamicSequenceSelection *clone() const override;

    void forceUnit(const SequenceName::Component &station,
                   const SequenceName::Component &archive = {},
                   const SequenceName::Component &variable = {}) override;

    void forceUnit(const SequenceName::Component &station,
                   const SequenceName::Component &archive,
                   const SequenceName::Component &variable,
                   const SequenceName::Flavors &flavors) override;

    void add(DynamicSequenceSelection *a);

    void add(std::unique_ptr<DynamicSequenceSelection> &&a);

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicSequenceSelection *to);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       DynamicSequenceSelection *&to);

protected:
    explicit Composite(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    void printLog(QDebug &stream) const override;
};

/**
 * A thin wrapper around a single unit that's always selected
 */
class CPD3DATACORE_EXPORT DynamicSequenceSelection::Single : public DynamicSequenceSelection {
    SequenceName selected;
    SequenceName::Set set;
public:
    Single() = delete;

    explicit Single(const SequenceName &unit);

    explicit Single(SequenceName &&unit);

    virtual ~Single();

    const SequenceName::Set &get(double start, double end) override;

    const SequenceName::Set &get(double time) override;

    const SequenceName::Set &getConst(double start, double end) const override;

    const SequenceName::Set &getConst(double time) const override;

    SequenceName::Set getAllUnits() const override;

    bool matches(double start, double end, const SequenceName &name) override;

    bool matchesConst(double start, double end, const SequenceName &name) const override;

    bool matches(double time, const SequenceName &name) override;

    bool matchesConst(double time, const SequenceName &name) const override;

    bool registerInput(const SequenceName &input) override;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override;

    DynamicSequenceSelection *clone() const override;

    void forceUnit(const SequenceName::Component &station,
                   const SequenceName::Component &archive = {},
                   const SequenceName::Component &variable = {}) override;

    void forceUnit(const SequenceName::Component &station,
                   const SequenceName::Component &archive,
                   const SequenceName::Component &variable,
                   const SequenceName::Flavors &flavors) override;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicSequenceSelection *to);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       DynamicSequenceSelection *&to);

protected:
    explicit Single(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    void printLog(QDebug &stream) const override;
};

/**
 * A thin wrapper around a list of units that are always selected
 */
class CPD3DATACORE_EXPORT DynamicSequenceSelection::Basic : public DynamicSequenceSelection {
    SequenceName::Set units;
public:
    Basic();

    explicit Basic(const SequenceName &unit);

    explicit Basic(const QList<SequenceName> &setUnits);

    explicit Basic(const SequenceName::Set &setUnits);

    explicit Basic(SequenceName::Set &&setUnits);

    virtual ~Basic();

    const SequenceName::Set &get(double start, double end) override;

    const SequenceName::Set &get(double time) override;

    const SequenceName::Set &getConst(double start, double end) const override;

    const SequenceName::Set &getConst(double time) const override;

    SequenceName::Set getAllUnits() const override;

    bool matches(double start, double end, const SequenceName &name) override;

    bool matchesConst(double start, double end, const SequenceName &name) const override;

    bool matches(double time, const SequenceName &name) override;

    bool matchesConst(double time, const SequenceName &name) const override;

    bool registerInput(const SequenceName &input) override;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override;

    DynamicSequenceSelection *clone() const override;

    void forceUnit(const SequenceName::Component &station,
                   const SequenceName::Component &archive = {},
                   const SequenceName::Component &variable = {}) override;

    void forceUnit(const SequenceName::Component &station,
                   const SequenceName::Component &archive,
                   const SequenceName::Component &variable,
                   const SequenceName::Flavors &flavors) override;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicSequenceSelection *to);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       DynamicSequenceSelection *&to);

protected:
    explicit Basic(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    void printLog(QDebug &stream) const override;
};

class DynamicSequenceSelectionOption;

/**
 * A list of regular expressions that match registered inputs.
 */
class CPD3DATACORE_EXPORT DynamicSequenceSelection::Match : public DynamicSequenceSelection {
    std::vector<SequenceMatch::Element> matchers;
    SequenceName::Set registered;
public:
    Match();

    Match(const Match &other);

    Match(const SequenceMatch::Element::Pattern &station,
          const SequenceMatch::Element::Pattern &archive,
          const SequenceMatch::Element::Pattern &variable,
          const SequenceName::Flavors &exactFlavors);

    inline Match(const char *station,
                 const char *archive,
                 const char *variable,
                 const SequenceName::Flavors &exactFlavors) : Match(
            SequenceMatch::Element::Pattern(station), SequenceMatch::Element::Pattern(archive),
            SequenceMatch::Element::Pattern(variable), exactFlavors)
    { }

    Match(const SequenceMatch::Element::Pattern &station,
          const SequenceMatch::Element::Pattern &archive,
          const SequenceMatch::Element::Pattern &variable,
          const SequenceMatch::Element::PatternList &hasFlavors = {},
          const SequenceMatch::Element::PatternList &lacksFlavors = Util::to_qstringlist(
                  SequenceName::defaultLacksFlavors()));

    explicit Match(const QList<SequenceMatch::Element> &setOperateMatchers);

    explicit Match(const std::vector<SequenceMatch::Element> &setOperateMatchers);

    virtual ~Match();

    const SequenceName::Set &get(double start, double end) override;

    const SequenceName::Set &get(double time) override;

    const SequenceName::Set &getConst(double start, double end) const override;

    const SequenceName::Set &getConst(double time) const override;

    bool matches(double start, double end, const SequenceName &name) override;

    bool matchesConst(double start, double end, const SequenceName &name) const override;

    bool matches(double time, const SequenceName &name) override;

    bool matchesConst(double time, const SequenceName &name) const override;

    bool registerInput(const SequenceName &input) override;

    void registerExpectedFlavorless(const SequenceName::Component &station,
                                    const SequenceName::Component &archive,
                                    const SequenceName::Component &variable) override;

    void registerExpected(const SequenceName::Component &station,
                          const SequenceName::Component &archive,
                          const SequenceName::Component &variable,
                          const SequenceName::Flavors &flavors) override;

    void registerExpected(const SequenceName::Component &station,
                          const SequenceName::Component &archive,
                          const SequenceName::Component &variable,
                          const std::unordered_set<SequenceName::Flavors> &flavors) override;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override;

    SequenceName::Set getAllUnits() const override;

    DynamicSequenceSelection *clone() const override;

    void forceUnit(const SequenceName::Component &station,
                   const SequenceName::Component &archive = {},
                   const SequenceName::Component &variable = {}) override;

    void forceUnit(const SequenceName::Component &station,
                   const SequenceName::Component &archive,
                   const SequenceName::Component &variable,
                   const SequenceName::Flavors &flavors) override;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicSequenceSelection *to);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       DynamicSequenceSelection *&to);

protected:
    explicit Match(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    void printLog(QDebug &stream) const override;
};


class CPD3DATACORE_EXPORT DynamicSequenceSelection::Variable : public DynamicSequenceSelection {
    struct OperateSegment {
        std::vector<SequenceMatch::Element> matchers;
        SequenceName::Set registered;

        double start;
        double end;

        OperateSegment();

        OperateSegment(const OperateSegment &other);

        OperateSegment &operator=(const OperateSegment &other);

        OperateSegment(OperateSegment &&other);

        OperateSegment &operator=(OperateSegment &&other);

        OperateSegment(const OperateSegment &other, double setStart, double setEnd);

        OperateSegment(const OperateSegment &under,
                       const OperateSegment &over,
                       double setStart,
                       double setEnd);

        OperateSegment(const SequenceMatch::Element::Pattern &station,
                       const SequenceMatch::Element::Pattern &archive,
                       const SequenceMatch::Element::Pattern &variable,
                       const SequenceMatch::Element::PatternList &hasFlavors,
                       const SequenceMatch::Element::PatternList &lacksFlavors,
                       double setStart = FP::undefined(),
                       double setEnd = FP::undefined());

        OperateSegment(const SequenceMatch::Element::Pattern &station,
                       const SequenceMatch::Element::Pattern &archive,
                       const SequenceMatch::Element::Pattern &variable,
                       const SequenceName::Flavors &exactFlavors,
                       double setStart = FP::undefined(),
                       double setEnd = FP::undefined());

        explicit OperateSegment(const QList<SequenceMatch::Element> &setMatchers,
                                double setStart = FP::undefined(),
                                double setEnd = FP::undefined());

        OperateSegment(QDataStream &stream);

        void serialize(QDataStream &stream) const;

        bool registerInput(const SequenceName &input);

        bool matches(const SequenceName &input) const;

        void registerExpectedFlavorless(const SequenceName::Component &station,
                                        const SequenceName::Component &archive,
                                        const SequenceName::Component &variable);

        void registerExpected(const SequenceName::Component &station,
                              const SequenceName::Component &archive,
                              const SequenceName::Component &variable,
                              const SequenceName::Flavors &flavors);

        void registerExpected(const SequenceName::Component &station,
                              const SequenceName::Component &archive,
                              const SequenceName::Component &variable,
                              const std::unordered_set<SequenceName::Flavors> &flavors);

        Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                     const Archive::Selection::Match &defaultArchives = {},
                                                     const Archive::Selection::Match &defaultVariables = {}) const;

        void forceUnit(const SequenceName::Component &station,
                       const SequenceName::Component &archive,
                       const SequenceName::Component &variable);

        void forceUnit(const SequenceName::Component &station,
                       const SequenceName::Component &archive,
                       const SequenceName::Component &variable,
                       const SequenceName::Flavors &flavors);

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }
    };

    std::deque<OperateSegment> segments;

public:
    Variable();

    virtual ~Variable();

    Variable(const Variable &other);

    Variable &operator=(const Variable &other);

    Variable(Variable &&other);

    Variable &operator=(Variable &&other);

    const SequenceName::Set &get(double start, double end) override;

    const SequenceName::Set &get(double time) override;

    const SequenceName::Set &getConst(double start, double end) const override;

    const SequenceName::Set &getConst(double time) const override;

    bool matches(double start, double end, const SequenceName &name) override;

    bool matchesConst(double start, double end, const SequenceName &name) const override;

    bool matches(double time, const SequenceName &name) override;

    bool matchesConst(double time, const SequenceName &name) const override;

    bool registerInput(const SequenceName &input) override;

    void registerExpectedFlavorless(const SequenceName::Component &station,
                                    const SequenceName::Component &archive,
                                    const SequenceName::Component &variable) override;

    void registerExpected(const SequenceName::Component &station,
                          const SequenceName::Component &archive,
                          const SequenceName::Component &variable,
                          const SequenceName::Flavors &flavors) override;

    void registerExpected(const SequenceName::Component &station,
                          const SequenceName::Component &archive,
                          const SequenceName::Component &variable,
                          const std::unordered_set<SequenceName::Flavors> &flavors) override;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override;

    SequenceName::Set getAllUnits() const override;

    QSet<double> getChangedPoints() const override;

    DynamicSequenceSelection *clone() const override;

    void forceUnit(const SequenceName::Component &station,
                   const SequenceName::Component &archive = {},
                   const SequenceName::Component &variable = {}) override;

    void forceUnit(const SequenceName::Component &station,
                   const SequenceName::Component &archive,
                   const SequenceName::Component &variable,
                   const SequenceName::Flavors &flavors) override;

    /**
     * Clear all input segments from this options wrapper.
     */
    void clear();

    /**
     * Set the operation to always match the given station, archive, variable,
     * and flavor regular expressions.
     *
     * @param station       the station regular expression
     * @param archive       the archive regular expression
     * @param variable      the variable regular expression
     * @param hasFlavors    the list of flavor regular expressions to require
     * @param lacksFlavors  the list of flavor regular expressions to reject
     */
    void set(const SequenceMatch::Element::Pattern &station,
             const SequenceMatch::Element::Pattern &archive,
             const SequenceMatch::Element::Pattern &variable,
             const SequenceMatch::Element::PatternList &hasFlavors = {},
             const SequenceMatch::Element::PatternList &lacksFlavors = Util::to_qstringlist(
                     SequenceName::defaultLacksFlavors()));

    /**
     * Set the operation to always match the given station, archive, variable,
     * regular expressions and have exactly the given flavors.
     *
     * @param station       the station regular expression
     * @param archive       the archive regular expression
     * @param variable      the variable regular expression
     * @param exactFlavors  the exact flavors to require
     */
    void set(const SequenceMatch::Element::Pattern &station,
             const SequenceMatch::Element::Pattern &archive,
             const SequenceMatch::Element::Pattern &variable,
             const SequenceName::Flavors &exactFlavors);

    /**
     * Set the operation to always match the given set of matchers.
     *
     * @param matchers  the matchers to use
     */
    void set(const QList<SequenceMatch::Element> &matchers);

    /**
     * Set the operation to match the given station, archive, variable and
     * flavors between the given times.  This replaces whatever was matched
     * before in the time range.
     *
     * @param start         the start time of the new match
     * @param end           the end time of the new match
     * @param station       the station regular expression
     * @param archive       the archive regular expression
     * @param variable      the variable regular expression
     * @param hasFlavors    the list of flavor regular expressions to require
     * @param lacksFlavors  the list of flavor regular expressions to reject
     */
    void set(double start,
             double end,
             const SequenceMatch::Element::Pattern &station,
             const SequenceMatch::Element::Pattern &archive,
             const SequenceMatch::Element::Pattern &variable,
             const SequenceMatch::Element::PatternList &hasFlavors = {},
             const SequenceMatch::Element::PatternList &lacksFlavors = Util::to_qstringlist(
                     SequenceName::defaultLacksFlavors()));

    /**
     * Set the operation to match the given station, archive, variable and
     * flavors between the given times.  This replaces whatever was matched
     * before in the time range.
     *
     * @param start         the start time of the new match
     * @param end           the end time of the new match
     * @param station       the station regular expression
     * @param archive       the archive regular expression
     * @param variable      the variable regular expression
     * @param flavors       the exact flavors to match
     */
    void set(double start,
             double end,
             const SequenceMatch::Element::Pattern &station,
             const SequenceMatch::Element::Pattern &archive,
             const SequenceMatch::Element::Pattern &variable,
             const SequenceName::Flavors &exactFlavors);

    /**
     * Set the operation to match the given matcher set between the given times.
     * This replaces whatever was matched before in the time range.
     *
     * @param start     the start time of the new match
     * @param end       the end time of the new match
     * @param matchers  the matchers to use
     */
    void set(double start, double end, const QList<SequenceMatch::Element> &matchers);

    /**
     * Set the operation between the given times to match the configuration
     * defined in the given value.
     *
     * @param start     the start time of the new match
     * @param end       the end time of the new match
     * @param v         the configuration value
     */
    void set(double start, double end, const Variant::Read &v);

    /**
     * Set the operation between to match the configuration defined in the given
     * value at all times.
     *
     * @param v         the configuration value
     */
    void set(const Variant::Read &v);

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicSequenceSelection *to);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       DynamicSequenceSelection *&to);

    friend class DynamicSequenceSelectionOption;

    friend class DynamicSequenceSelection;

protected:
    explicit Variable(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    void printLog(QDebug &stream) const override;

private:
    static DynamicSequenceSelection *reduceConfigurationSegments(const DynamicSequenceSelection::Variable &tor,
                                                                 double start,
                                                                 double end);
};

/** 
 * A list of variable specifications to operate on.  This list can consist
 * of regular expressions and may have be time dependent.
 */
class CPD3DATACORE_EXPORT DynamicSequenceSelectionOption
        : public ComponentOptionBase, virtual public ComponentOptionValueParsable {
Q_OBJECT

    Q_INTERFACES(CPD3::Data::ComponentOptionValueParsable)

    DynamicSequenceSelection::Variable operate;
public:
    DynamicSequenceSelectionOption(const QString &argumentName,
                                   const QString &displayName,
                                   const QString &description,
                                   const QString &defaultBehavior,
                                   int sortPriority = 0);

    DynamicSequenceSelectionOption(const DynamicSequenceSelectionOption &other);

    virtual ~DynamicSequenceSelectionOption();

    /**
     * Clear all input segments from this options wrapper.
     */
    void clear();

    /**
     * Set the operation to always match the given station, archive, variable and
     * flavor regular expressions.
     *
     * @param station       the station regular expression
     * @param archive       the archive regular expression
     * @param variable      the variable regular expression
     * @param hasFlavors    the list of flavor regular expressions to require
     * @param lacksFlavors  the list of flavor regular expressions to reject
     */
    void set(const SequenceMatch::Element::Pattern &station,
             const SequenceMatch::Element::Pattern &archive,
             const SequenceMatch::Element::Pattern &variable,
             const SequenceMatch::Element::PatternList &hasFlavors = {},
             const SequenceMatch::Element::PatternList &lacksFlavors = Util::to_qstringlist(
                     SequenceName::defaultLacksFlavors()));

    /**
     * Set the operation to always match the given station, archive, and variable
     * regular expressions and to have exactly the given flavors.
     *
     * @param station       the station regular expression
     * @param archive       the archive regular expression
     * @param variable      the variable regular expression
     * @param exactFlavors  the exact flavors
     */
    void set(const SequenceMatch::Element::Pattern &station,
             const SequenceMatch::Element::Pattern &archive,
             const SequenceMatch::Element::Pattern &variable,
             const SequenceName::Flavors &exactFlavors);

    void set(const QList<SequenceMatch::Element> &matchers);

    /**
     * Set the operation to match the given station, archive, variable and
     * flavors between the given times.  This replaces whatever was matched
     * before in the time range.
     *
     * @param start         the start time of the new match
     * @param end           the end time of the new match
     * @param station       the station regular expression
     * @param archive       the archive regular expression
     * @param variable      the variable regular expression
     * @param flavors       the exact flavors to match
     */
    void set(double start,
             double end,
             const SequenceMatch::Element::Pattern &station,
             const SequenceMatch::Element::Pattern &archive,
             const SequenceMatch::Element::Pattern &variable,
             const SequenceMatch::Element::PatternList &hasFlavors = {},
             const SequenceMatch::Element::PatternList &lacksFlavors = Util::to_qstringlist(
                     SequenceName::defaultLacksFlavors()));

    void set(double start,
             double end,
             const SequenceMatch::Element::Pattern &station,
             const SequenceMatch::Element::Pattern &archive,
             const SequenceMatch::Element::Pattern &variable,
             const SequenceName::Flavors &exactFlavors);

    /**
     * Set the operation to match the given matcher set between the given times.
     * This replaces whatever was matched before in the time range.
     *
     * @param start     the start time of the new match
     * @param end       the end time of the new match
     * @param matchers  the matchers to use
     */
    void set(double start, double end, const QList<SequenceMatch::Element> &matchers);

    /**
     * Set the operation to the matcher specified in the given Value between the
     * given times.  This replaces whatever was matched before in the time range.
     *
     * @param start     the start time of the new match
     * @param end       the end time of the new match
     * @param value     the Value to use
     */
    void set(double start, double end, const Variant::Read &value);

    /**
     * Set the operation to the matcher specified in the given Value for all time.
     * This replaces all matchers.
     *
     * @param value     the Value to use
     */
    void set(const Variant::Read &value);

    void reset() override;

    ComponentOptionBase *clone() const override;

    /**
     * Allocate a new operator to use.  The operator allocated this way must
     * be deleted by the caller.  Note that operators created this way do not
     * reflect changes to this operator options wrapper after they are created.
     *
     * @return a new operator
     */
    DynamicSequenceSelection *getOperator() const;

    /**
     * A simple segment representing the selection at a range in time.
     */
    struct CPD3DATACORE_EXPORT Segment : public Time::Bounds {
        SequenceMatch::OrderedLookup lookup;

        inline SequenceMatch::OrderedLookup &getLookup()
        { return lookup; }

        inline const SequenceMatch::OrderedLookup &getLookup() const
        { return lookup; }

        inline Segment(double start, double end, const SequenceMatch::OrderedLookup &lookup)
                : Time::Bounds(start, end), lookup(lookup)
        { }

        inline Segment(double start, double end, SequenceMatch::OrderedLookup &&lookup)
                : Time::Bounds(start, end), lookup(std::move(lookup))
        { }
    };

    /**
     * Get the matching segments that represent the selection.  This
     * creates a set of non-overlapping segments that match the current
     * contents of the options wrapper.
     *
     * @return  non-overlapping matching segments
     */
    std::vector<Segment> getSegments() const;

    bool parseFromValue(const Variant::Read &value) override;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const DynamicSequenceSelection *to);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       DynamicSequenceSelection *&to);
};

}
}

#endif
