/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACORE_PROCESSINGTAPCHAIN_HXX
#define CPD3DATACORE_PROCESSINGTAPCHAIN_HXX

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QList>
#include <QThread>
#include <QPair>
#include <QStringList>

#include "datacore.hxx"
#include "stream.hxx"
#include "variant/handle.hxx"
#include "sequencematch.hxx"
#include "archive/selection.hxx"
#include "core/number.hxx"
#include "core/actioncomponent.hxx"


namespace CPD3 {
namespace Data {

namespace Internal {
struct ProcessingTapChainPrivate;
}

/**
 * An implementation of a chain of filters with various taps at points
 * along that chain.
 */
class CPD3DATACORE_EXPORT ProcessingTapChain : public QThread {
Q_OBJECT

    friend struct Internal::ProcessingTapChainPrivate;

    std::unique_ptr<Internal::ProcessingTapChainPrivate> p;

    bool integerateChain(const Variant::Read &config,
                         const QList<QPair<SequenceMatch::Composite, StreamSink *> > &outputs);

    void finishNext();

    void finishAllExits();

    void finishArchiveIssue();

public:
    ProcessingTapChain();

    ProcessingTapChain(const ProcessingTapChain &) = delete;

    ProcessingTapChain &operator=(const ProcessingTapChain &) = delete;

    virtual ~ProcessingTapChain();

    virtual void add(StreamSink *target, const Variant::Read &config,
                     const SequenceMatch::Composite &selection = SequenceMatch::Composite(
                             SequenceMatch::Element::SpecialMatch::Data));

    virtual void add(QList<QPair<SequenceMatch::Composite, StreamSink *> > &targets,
                     const Variant::Read &config);

    /**
     * An interface used to override the source of data for the chain.
     */
    class CPD3DATACORE_EXPORT ArchiveBackend {
    public:
        virtual ~ArchiveBackend();

        /**
         * Issues a request to the archive reader backend for the given
         * selections of data.  This backend should send data from those
         * selections to the given ingress point.  This must be callable from
         * any thread, not block, and be able to handle multiple concurrent
         * requests to read.
         * <br>
         * Note that whatever results in feeding data to the target will not
         * be controlled by the chain's signalTerminate, so any calls to that
         * must be mirrored to the backend resources by the parent.
         * 
         * @param selections    the archive selections to read
         * @param target        the ingress target of data
         */
        virtual void issueArchiveRead(const Archive::Selection::List &selections,
                                      StreamSink *target) = 0;

        /**
         * Called when all archive reads have been issued and the chain is simply
         * processing data.
         */
        virtual void archiveReadIssueComplete();
    };

    void setBackend(ArchiveBackend *backend);

    void setDefaultSelection(double start = FP::undefined(),
                             double end = FP::undefined(),
                             const Archive::Selection::Match &stations = {},
                             const Archive::Selection::Match &archives = {},
                             const Archive::Selection::Match &variables = {});

    ActionFeedback::Source feedback;

public slots:

    void signalTerminate();

private slots:

    void nextFinished();

    void nextProgress(double value, bool isTime);

    void havePendingRead();

protected:
    virtual void run();
};

}
}

#endif
