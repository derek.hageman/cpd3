/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <chrono>
#include <QLoggingCategory>
#include <QFileInfo>
#include <QCryptographicHash>

#include "datacore/streampipeline.hxx"
#include "core/qtcompat.hxx"
#include "io/drivers/stdio.hxx"
#include "io/drivers/qio.hxx"
#include "io/drivers/file.hxx"

#ifdef Q_OS_UNIX

#include "io/drivers/unixsocket.hxx"

#else
#include "io/drivers/localsocket.hxx"
#endif


Q_LOGGING_CATEGORY(log_datacore_streampipeline, "cpd3.datacore.streampipeline", QtWarningMsg)


using namespace CPD3::Data::Internal;

namespace CPD3 {
namespace Data {

StreamPipeline::StreamPipeline(bool acceleration, bool serialization, bool forwarding)
        : enableAcceleration(acceleration),
          enableSerialization(serialization),
          enableForwarding(forwarding),
          threadState(ThreadState::Initialize),
          reapPending(true),
          inputBytesRead(0),
          inputTotalSize(0),
          chainInputArchive(nullptr),
          sourceCanAccelerate(false),
          sinkCanAccelerate(false),
          chainTapUseRequestedInput(false),
          retainOutput(false)
{ }

StreamPipeline::~StreamPipeline()
{
    signalTerminate();
    if (thread.joinable()) {
        thread.join();
    }
}

void StreamPipeline::signalTerminate()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        switch (threadState) {
        case ThreadState::Completed:
        case ThreadState::TerminateRequested:
            return;
        case ThreadState::LockRequested:
        case ThreadState::Locked:
            notify.wait(lock);
            continue;
        default:
            threadState = ThreadState::TerminateRequested;
            lock.unlock();
            notify.notify_all();
            return;
        }
    }
}

bool StreamPipeline::attachInputStream()
{
    Q_ASSERT(chainSource.get() != nullptr);
    auto converter = dynamic_cast<ExternalConverter *>(chainSource.get());
    if (!converter)
        return false;

    if (enableAcceleration && sourceCanAccelerate) {
        converter->accelerationKey.connect([this](const std::string &key) {
            if (key.empty())
                return;
            {
                std::lock_guard<std::mutex> lock(mutex);
                inputAccelerationKey = key;
            }
            notify.notify_all();
        });
    }

    if (!chainInputStream) {
        converter->endData();
        return false;
    }

    converter->stallRequested
             .connect(std::bind(&IO::Generic::Stream::readStall, chainInputStream.get(),
                                std::placeholders::_1));

    inputBytesRead.store(0);
    chainInputStream->read.connect([converter, this](const Util::ByteArray &data) {
        inputBytesRead.fetch_add(data.size(), std::memory_order_relaxed);
        converter->incomingData(data);
    });
    chainInputStream->ended.connect([converter] {
        converter->endData();
    });

    return true;
}

#ifdef Q_OS_UNIX

static std::string getServerName(const std::string &name)
{
    {
        auto runtime = qgetenv("XDG_RUNTIME_DIR");
        if (!runtime.isEmpty()) {
            QFileInfo info(QString::fromUtf8(runtime));
            if (info.isDir() && info.isWritable()) {
                return runtime.toStdString() + "/CPD3PIPE-" + name;
            }
        }
    }
    return "/tmp/CPD3PIPE-" + name;
}

#else

static std::string getServerName(const std::string &name)
{ return "CPD3PIPE-" + name; }

#endif

void StreamPipeline::attachOutputSink()
{
    Q_ASSERT(chainSink.get() != nullptr);

    if (enableAcceleration && sinkCanAccelerate) {
        std::string identity;
        if (auto ids = dynamic_cast<IO::STDIO::Backing *>(pipelineOutputHandle.get())) {
            identity = ids->pipelineOutputIdentity();
        }

        std::string key = Random::string().toStdString();
        accelerationSecret = Random::string().toStdString();

#ifdef Q_OS_UNIX
        std::unique_ptr<IO::Socket::Unix::Server> server(new IO::Socket::Unix::Server(
                [this](std::unique_ptr<IO::Socket::Connection> &&connection) {
                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        accelerationIncoming.emplace_back(std::move(connection));
                    }
                    notify.notify_all();
                }, getServerName(key)));
#else
        std::unique_ptr<IO::Socket::Local::Server> server(new IO::Socket::Local::Server(
                [this](std::unique_ptr<IO::Socket::Connection> &&connection) {
                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        accelerationIncoming.emplace_back(std::move(connection));
                    }
                    notify.notify_all();
                }, getServerName(key)));
#endif

        if (server->startListening()) {
            qCDebug(log_datacore_streampipeline) << "Listening for acceleration on"
                                                 << server->describeServer();

            accelerationServer = std::move(server);

            key += ";";
            key += accelerationSecret;
            if (!identity.empty()) {
                key += ";";
                key += identity;
            }
            chainSink->setAccelerationKey(key);
        }
    }

    chainSink->finished.connect([this] {
        {
            std::lock_guard<std::mutex> lock(mutex);
            reapPending = true;
        }
        notify.notify_all();
    });
}

bool StreamPipeline::canRestartWithRetained()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (threadState != ThreadState::Completed)
            return false;
    }
    if (!chain.empty())
        return false;
    if (retainedShortCircuit)
        return true;
    if (!retainedInputFile.empty())
        return true;
    if (!retainOutput)
        return false;
    if (!retainedSink)
        return false;
    return true;
}

bool StreamPipeline::restartWithRetained()
{
    if (!canRestartWithRetained())
        return false;

    releaseInput(true);

    if (retainedShortCircuit && chainTapOutput) {
        qCDebug(log_datacore_streampipeline)
            << "Pipeline restarted with direct active connection for tap chain";
        if (thread.joinable()) {
            thread.join();
        }

        startTapChain(false);

        {
            std::lock_guard<std::mutex> lock(mutex);
            if (threadState == ThreadState::Initialize || threadState == ThreadState::Completed) {
                threadState = ThreadState::Running;
            }
        }
        thread = std::thread(std::bind(&StreamPipeline::run, this));
        return true;
    }

    if (chainTapOutput) {
        startTapChain(true);
        tapChainBackend->waitForSelections();
    }

    auto target = startInitialOutputSink();
    if (!target) {
        outputError = QObject::tr("No output set");
        qCDebug(log_datacore_streampipeline) << "Pipeline restarted with no output set";
        return false;
    }

    if (!retainedInputFile.empty()) {
        auto handle = IO::Access::file(retainedInputFile, IO::File::Mode::readOnly());
        if (!handle) {
            qCDebug(log_datacore_streampipeline) << "Unable to reopen input handle for restart";
            return false;
        }

        chainInputStream = handle->stream();
        if (!chainInputStream) {
            qCDebug(log_datacore_streampipeline) << "Unable to reopen stream handle for restart";
            return false;
        }

        qCDebug(log_datacore_streampipeline) << "Pipeline restarted with retained file"
                                             << handle->filename();

        inputTotalSize = handle->size();
        chainSource.reset(new StandardDataInput);

        attachInputStream();
        chainSource->finished.connect([this] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                reapPending = true;
            }
            notify.notify_all();
        });
        chainSource->start();
        chainSource->setEgress(target);
        chainInputStream->start();
    } else {
        if (!retainedSink)
            return false;

        target->incomingData(retainedSink->retainedData());
        target->endData();

        qCDebug(log_datacore_streampipeline) << "Pipeline restarted with retained data";
    }

    if (thread.joinable()) {
        thread.join();
    }
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (threadState == ThreadState::Initialize || threadState == ThreadState::Completed) {
            threadState = ThreadState::Running;
        }
    }
    thread = std::thread(std::bind(&StreamPipeline::run, this));
    return true;
}

void StreamPipeline::startTapChain(bool usingBackend)
{
    if (usingBackend) {
        tapChainBackend.reset(new ProcessingTapBackend(*this));
        chainTapOutput->setBackend(tapChainBackend.get());
    } else {
        chainTapOutput->setBackend(nullptr);
    }
    QObject::connect(chainTapOutput, &ProcessingTapChain::finished, [this] {
        {
            std::lock_guard<std::mutex> lock(mutex);
            reapPending = true;
        }
        notify.notify_all();
    });
    chainTapOutput->start();
}

bool StreamPipeline::shortCircuitTapChain()
{
    /* See if we can connect the tap chain directly to the archive. */
    if (!chainTapOutput)
        return false;
    if (!chainInputArchive && !chainTapUseRequestedInput)
        return false;
    if (!chain.empty())
        return false;

    chainInputArchive = nullptr;
    ownedArchive.reset();
    retainedShortCircuit = retainOutput;
    retainOutput = false;

    startTapChain(false);

    qCDebug(log_datacore_streampipeline)
        << "Pipeline started with direct active connection for tap chain";

    {
        std::lock_guard<std::mutex> lock(mutex);
        if (threadState == ThreadState::Initialize) {
            threadState = ThreadState::Running;
        }
    }
    thread = std::thread(std::bind(&StreamPipeline::run, this));
    return true;
}

StreamSink *StreamPipeline::startInitialOutputSink()
{
    if (chainSink) {
        attachOutputSink();
        if (enableAcceleration && enableForwarding && sourceCanAccelerate && sinkCanAccelerate) {
            sinkStartDifferTimeout.reset(
                    new Clock::time_point(Clock::now() + std::chrono::seconds(5)));
            qCDebug(log_datacore_streampipeline)
                << "Delaying pipeline standard sink start to wait for incoming acceleration";
            return chainSink.get();
        }
        chainSink->start();
        qCDebug(log_datacore_streampipeline) << "Pipeline output set to standard sink";
        return chainSink.get();
    } else if (chainExternalOutput) {
        qCDebug(log_datacore_streampipeline) << "Pipeline output set to external sink";
        return chainExternalOutput.get();
    } else if (chainTapOutput) {
        Q_ASSERT(tapChainBackend.get() != nullptr);
        tapChainBackend->waitForSelections();
        qCDebug(log_datacore_streampipeline) << "Pipeline output set to tap chain";
        return &tapChainBackend->dispatch;
    }

    return nullptr;
}

bool StreamPipeline::start()
{
    reapPending = true;

    if (restartWithRetained())
        return true;

    {
        std::unique_lock<std::mutex> lock(mutex);
        if (threadState == ThreadState::Completed) {
            lock.unlock();
            qCDebug(log_datacore_streampipeline) << "Cannot restart a completed chain";
            finished();
            return false;
        }

        Q_ASSERT(threadState == ThreadState::Initialize ||
                         threadState == ThreadState::TerminateRequested);
    }

    if (shortCircuitTapChain())
        return true;
    retainedShortCircuit = false;

    if (chainTapOutput) {
        startTapChain(true);
    }

    if (chainTapOutput && chainTapUseRequestedInput && chainInputSelections.empty()) {
        /* Attempt to construct the input request if the short circuit failed */
        if (!chainInputArchive) {
            ownedArchive.reset(new Archive::Access);
            chainInputArchive = ownedArchive.get();
        }
        chainInputSelections = tapChainBackend->waitForSelections();
        for (const auto &name : requestedInputs()) {
            chainInputSelections.emplace_back(name, chainTapRequestedBounds.getStart(),
                                              chainTapRequestedBounds.getEnd());
        }
    }

    if (chainSource) {
        if (!attachInputStream()) {
            chainInputStream.reset();
            pipelineInputHandle.reset();
        }
        chainSource->finished.connect([this] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                reapPending = true;
            }
            notify.notify_all();
        });
        chainSource->start();
        if (chainInputStream) {
            chainInputStream->start();
        }
    } else if (chainInputArchive && !chainInputSelections.empty()) {
        chainInputReader = chainInputArchive->readStream(chainInputSelections);
        chainInputReader->complete.connect([this] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                reapPending = true;
            }
            notify.notify_all();
        });
    }

    if (!chainSource) {
        chainInputStream.reset();
        pipelineInputHandle.reset();
    }

    StreamSink *nextSink = startInitialOutputSink();
    if (!nextSink) {
        outputError = QObject::tr("No output set");
        qCDebug(log_datacore_streampipeline) << "Pipeline started with no output set";

        {
            std::lock_guard<std::mutex> lock(mutex);
            threadState = ThreadState::Completed;
            notify.notify_all();
        }
        finished();
        return false;
    }

    chainOutputSink.reset(new EndTrackingSink(nextSink, *this));
    nextSink = chainOutputSink.get();

    if (retainOutput) {
        if (!retainedInputFile.empty() && chain.empty()) {
            qCDebug(log_datacore_streampipeline) << "Pipeline simple input file retained";
        } else {
            Q_ASSERT(nextSink);
            retainedSink.reset(new RetainSink(nextSink));
            nextSink = retainedSink.get();
            qCDebug(log_datacore_streampipeline) << "Pipeline retaining output";

            retainedInputFile.clear();
        }
    } else {
        retainedInputFile.clear();
    }

    for (auto chainStage = chain.rbegin(), firstStage = chain.rend();
            chainStage != firstStage;
            ++chainStage) {
        Q_ASSERT(nextSink);

        chainStage->outputSink.reset(new EndTrackingSink(nextSink, *this));
        chainStage->stage->setEgress(chainStage->outputSink.get());

        chainStage->stage->finished.connect([this] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                reapPending = true;
            }
            notify.notify_all();
        });

        chainStage->stage->start();

        if (chainStage->component) {
            qCDebug(log_datacore_streampipeline) << "Added pipeline processing stage" << chainStage
                        ->component
                        ->getGeneralSerializationName();
        } else {
            qCDebug(log_datacore_streampipeline) << "Added pipeline processing stage";
        }

        nextSink = chainStage->stage.get();
    }
    Q_ASSERT(nextSink);

    if (FP::defined(chainInputClipBounds.getStart()) ||
            FP::defined(chainInputClipBounds.getEnd())) {
        chainInputClippingSink.reset(new ClippingSink(nextSink, chainInputClipBounds));
        nextSink = chainInputClippingSink.get();
    }

    bool isOk = true;

    chainInputSink.reset(new EndTrackingSink(nextSink, *this));
    nextSink = chainInputSink.get();
    if (sinkStartDifferTimeout) {
        differedStartSink.reset(new StartTrackingSink(nextSink, *this));
        nextSink = differedStartSink.get();
    }
    if (chainSource) {
        chainSource->setEgress(nextSink);
        qCDebug(log_datacore_streampipeline) << "Pipeline input set to generic source";
    } else if (chainInputReader) {
        chainInputReader->setEgress(nextSink);
        qCDebug(log_datacore_streampipeline) << "Pipeline input set to archive data";
    } else if (chainExternalInput) {
        chainExternalInput->setEgress(nextSink);
        qCDebug(log_datacore_streampipeline) << "Pipeline input set to external";
    } else {
        inputError = QObject::tr("No input set");
        qCDebug(log_datacore_streampipeline) << "Pipeline starting with no input configured";
        nextSink->endData();
        isOk = false;
    }

    /* Release handles, so the devices are owned entirely by the things using them (so
     * they close when reset) */
    pipelineInputHandle.reset();
    pipelineOutputHandle.reset();

    qCDebug(log_datacore_streampipeline) << "Pipeline started with" << chain.size() << "stages";

    {
        std::lock_guard<std::mutex> lock(mutex);
        if (threadState == ThreadState::Initialize) {
            threadState = ThreadState::Running;
        }
    }
    thread = std::thread(std::bind(&StreamPipeline::run, this));

    return isOk;
}

void StreamPipeline::executeTerminate()
{
    accelerationServer.reset();
    accelerationIncoming.clear();

    chainInputStream.reset();
    if (chainSource) {
        if (auto converter = dynamic_cast<ExternalConverter *>(chainSource.get())) {
            converter->endData();
        }
        chainSource->signalTerminate();
    }
    if (chainInputReader) {
        chainInputReader->signalTerminate();
    }
    for (const auto &chainStage : chain) {
        chainStage.stage->signalTerminate();
    }
    if (chainSink) {
        chainSink->signalTerminate();
    }
    if (chainTapOutput) {
        chainTapOutput->signalTerminate();
    }
    if (chainExternalInput) {
        chainExternalInput->signalTerminate();
    }

    if (chainSource) {
        if (auto converter = dynamic_cast<ExternalConverter *>(chainSource.get())) {
            converter->stallRequested.disconnect();
        }
        chainSource->wait();
        chainSource.reset();
    }
    if (chainInputReader) {
        chainInputReader->wait();
        chainInputReader.reset();
    }
    if (chainInputSink && !chainInputSink->isEnded()) {
        chainInputSink->endData();
    }
    for (auto &chainStage : chain) {
        chainStage.stage->wait();
        chainStage.stage.reset();
        if (chainStage.outputSink && !chainStage.outputSink->isEnded()) {
            chainStage.outputSink->endData();
        }
    }
    if (chainSink) {
        chainSink->wait();
        chainSink.reset();
    }
    if (chainTapOutput) {
        chainTapOutput->wait();
        chainTapOutput = nullptr;
    }
    if (chainExternalOutput) {
        if (!chainExternalOutput->isEnded()) {
            chainExternalOutput->endData();
        }
        chainExternalOutput.reset();
    }
}

StreamPipeline::ThreadLock::ThreadLock(StreamPipeline &parent) : parent(parent), didLock(false)
{
    std::unique_lock<std::mutex> lock(parent.mutex);
    for (;;) {
        switch (parent.threadState) {
        case ThreadState::Initialize:
        case ThreadState::Completed:
            return;
        case ThreadState::Running:
            parent.threadState = ThreadState::LockRequested;
            lock.unlock();
            parent.notify.notify_all();
            lock.lock();
            for (;;) {
                switch (parent.threadState) {
                case ThreadState::Completed:
                    /* Might have been in shutdown */
                    return;
                case ThreadState::Initialize:
                case ThreadState::Running:
                case ThreadState::TerminateRequested:
                    Q_ASSERT(false);
                    return;
                case ThreadState::LockRequested:
                    parent.notify.wait(lock);
                    break;
                case ThreadState::Locked:
                    didLock = true;
                    return;
                }
            }
            break;
        case ThreadState::TerminateRequested:
        case ThreadState::LockRequested:
        case ThreadState::Locked:
            parent.notify.wait(lock);
            break;
        }
    }
}

StreamPipeline::ThreadLock::~ThreadLock()
{
    if (!didLock)
        return;
    {
        std::lock_guard<std::mutex> lock(parent.mutex);
        Q_ASSERT(parent.threadState == ThreadState::Locked);
        parent.threadState = ThreadState::Running;
    }
    parent.notify.notify_all();
}

void StreamPipeline::run()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!lock)
            lock.lock();

        bool doReap = false;
        bool doStartDiffered = false;
        std::string connectAcceleration;
        std::unique_ptr<IO::Socket::Connection> accelerationConnection;
        {
            switch (threadState) {
            case ThreadState::Initialize:
            case ThreadState::Completed:
                Q_ASSERT(false);
                return;
            case ThreadState::Running:
                break;
            case ThreadState::TerminateRequested:
                lock.unlock();
                executeTerminate();
                lock.lock();
                threadState = ThreadState::Completed;
                lock.unlock();
                notify.notify_all();
                finished();
                return;
            case ThreadState::LockRequested:
                threadState = ThreadState::Locked;
                lock.unlock();
                notify.notify_all();
                continue;
            case ThreadState::Locked:
                notify.wait(lock);
                continue;
            }

            doReap = reapPending;
            reapPending = false;

            connectAcceleration = std::move(inputAccelerationKey);
            inputAccelerationKey.clear();

            if (!accelerationIncoming.empty()) {
                accelerationConnection = std::move(accelerationIncoming.front());
                accelerationIncoming.pop_front();
            }

            if (!doReap && connectAcceleration.empty() && !accelerationConnection) {
                if (sinkStartDifferTimeout) {
                    if (differedStartSink && differedStartSink->isStartedUnlocked()) {
                        doStartDiffered = true;
                    } else {
                        if (notify.wait_until(lock, *sinkStartDifferTimeout) ==
                                std::cv_status::no_timeout)
                            continue;
                        doStartDiffered = true;
                    }
                } else {
                    notify.wait(lock);
                    continue;
                }
            }
        }

        lock.unlock();

        if (doReap) {
            bool reapingDone = true;
            if (chainSource) {
                if (chainSource->isFinished()) {
                    if (auto converter = dynamic_cast<ExternalConverter *>(chainSource.get())) {
                        converter->stallRequested.disconnect();
                    }
                    chainInputStream.reset();
                    chainSource.reset();
                    Q_ASSERT(chainInputSink->isEnded());
                    qCDebug(log_datacore_streampipeline) << "Pipeline reaped generic source";
                } else {
                    reapingDone = false;
                }
            }
            if (chainInputReader) {
                if (chainInputReader->isComplete()) {
                    chainInputReader.reset();
                    Q_ASSERT(chainInputSink->isEnded());
                    ownedArchive.reset();
                    qCDebug(log_datacore_streampipeline) << "Pipeline reaped archive source";
                } else {
                    reapingDone = false;
                }
            }
            if (chainExternalInput) {
                if (chainExternalInput->isEnded()) {
                    chainExternalInput.reset();
                    Q_ASSERT(chainInputSink->isEnded());
                    qCDebug(log_datacore_streampipeline) << "Pipeline reaped external source";
                } else {
                    reapingDone = false;
                }
            }

            if (reapingDone && sinkStartDifferTimeout && chainSink) {
                qCDebug(log_datacore_streampipeline)
                    << "Starting differed sink with finished input";
                sinkStartDifferTimeout.reset();
                chainSink->start();
            }

            if (reapingDone) {
                while (!chain.empty() && chain.front().stage->isFinished()) {
                    chain.front().stage.reset();
                    Q_ASSERT(chain.front().outputSink->isEnded());

                    if (chain.front().component) {
                        qCDebug(log_datacore_streampipeline) << "Pipeline reaped processing stage"
                                                             << chain.front()
                                                                     .component
                                                                     ->getGeneralSerializationName();
                    } else {
                        qCDebug(log_datacore_streampipeline) << "Pipeline reaped processing stage";
                    }

                    chain.erase(chain.begin());
                }
                if (!chain.empty()) {
                    reapingDone = false;
                }
            }

            if (reapingDone) {
                if (chainSink) {
                    if (chainSink->isFinished()) {
                        chainSink.reset();
                        qCDebug(log_datacore_streampipeline) << "Pipeline reaped generic sink";
                    } else {
                        reapingDone = false;
                    }
                }
                if (chainTapOutput) {
                    if (chainTapOutput->isFinished()) {
                        chainTapOutput = nullptr;
                        tapChainBackend.reset();
                        qCDebug(log_datacore_streampipeline) << "Pipeline reaped tap chain sink";
                    } else {
                        reapingDone = false;
                    }
                }
                if (chainExternalOutput) {
                    if (chainExternalOutput->isEnded()) {
                        chainExternalOutput.reset();
                        qCDebug(log_datacore_streampipeline) << "Pipeline reaped external sink";
                    } else {
                        reapingDone = false;
                    }
                }
            }

            if (reapingDone) {
                accelerationServer.reset();
                accelerationIncoming.clear();
                qCDebug(log_datacore_streampipeline) << "Pipeline execution complete";
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    threadState = ThreadState::Completed;
                }
                notify.notify_all();
                finished();
                return;
            }
        }

        if (!connectAcceleration.empty()) {
            attemptAcceleration(connectAcceleration);
            if (sinkStartDifferTimeout && chainSink && !forwardServerKey.empty()) {
                qCDebug(log_datacore_streampipeline)
                    << "Starting differed sink with accelerated input";
                sinkStartDifferTimeout.reset();
                chainSink->start();
            }
        }

        if (accelerationConnection) {
            processIncomingAcceleration(std::move(accelerationConnection));
        }

        if (sinkStartDifferTimeout && doStartDiffered) {
            qCDebug(log_datacore_streampipeline) << "Starting differed sink";
            sinkStartDifferTimeout.reset();
            chainSink->start();
        }
    }
}


namespace {

enum class AccelerationState : std::uint8_t {
    /*
     * Server sends a challenge to the client (32 bytes).
     */
            Handshake_Server = 0,

    /*
     * Client responds to the server challenge (64 bytes) and sends its
     * own challenge (32 bytes).
     */
            Handshake_Client,

    /*
     * Server responds to the client challenge (64 bytes).
     */
            Handshake_Complete,

    /*
     * Server is in forward mode and sends the acceleration target length (2 bytes) and
     * data (N bytes) then closes the connection.
     */
            Server_Forward,

    /*
     * Client acknowledges the handshake and starts the detach process (no state change
     * on the client yet).
     */
            Detach_Begin,

    /*
     * Server has disconnected output (more data may still arrive, but the end of
     * data will not).  Server is now awaiting the client.
     */
            Detach_Interlock_Server,

    /*
     * Client has disconnected the input end processing and is awaiting the end of
     * the original data stream.
     */
            Detach_Interlock_Client,

    /*
     * Client has received the end of data.  Server will listen for subsequent request.
     * The interlock part of this is the client receiving the end of data.
     */
            Detach_Complete,

    /*
     * Request adding serialized element to the processing chain.
     */
            Request_Add_To_Chain,

    /*
     * Accept adding to the chain, and being waiting for the name (2 byte length,
     * N byte data) then the serialized state (4 length, N byte data).
     */
            Accept_Add_To_Chain,

    /*
     * Deny adding to the chain (serialization disabled, etc).
     */
            Deny_Add_To_Chain,

    /*
     * Acknowledge completion of a component being added to the chain.
     */
            Acknowledge_Add_To_Chain,


    /*
     * Handoff the socket into raw data transfer mode.  After this the client only
     * expectes to receive raw data over the socket.
     */
            Handoff_Socket,

};

class AccelerationInterface final : public Threading::Receiver {
    using Clock = std::chrono::steady_clock;

    IO::Generic::Stream &stream;
    Clock::time_point timeout;

    std::mutex mutex;
    std::condition_variable cv;
    bool ended = false;
    Util::ByteArray buffer;
public:

    explicit AccelerationInterface(IO::Generic::Stream &stream) : stream(stream),
                                                                  timeout(Clock::now() +
                                                                                  std::chrono::seconds(
                                                                                          15))
    {
        stream.read.connect(*this, [this](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                buffer += data;
            }
            cv.notify_all();
        });
        stream.ended.connect(*this, [this] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ended = true;
            }
            cv.notify_all();
        });
        stream.start();
    }

    virtual ~AccelerationInterface()
    { disconnect(); }

    void releaseStream()
    { disconnectImmediate(); }

    Util::ByteArray read(std::size_t count)
    {
        Q_ASSERT(count > 0);

        std::unique_lock<std::mutex> lock(mutex);
        cv.wait_until(lock, timeout, [this, count] { return buffer.size() >= count || ended; });
        if (buffer.size() < count)
            return {};
        Util::ByteArray result;
        if (buffer.size() == count) {
            result = std::move(buffer);
            buffer.clear();
        } else {
            result = buffer.mid(0, count);
            buffer.pop_front(count);
        }
        return result;
    }

    static bool consumeState(Util::ByteArray &data, AccelerationState state)
    {
        if (!data) {
            qCDebug(log_datacore_streampipeline) << "No response waiting for state"
                                                 << static_cast<int>(state);
            return false;
        }
        if (data.front() != static_cast<std::uint8_t>(state)) {
            qCDebug(log_datacore_streampipeline) << "Waiting for state" << static_cast<int>(state)
                                                 << "but received" << data.front();
            return false;
        }
        data.pop_front(1);
        return true;
    }

    void write(const Util::ByteView &data)
    { stream.write(data); }

    void write(const Util::ByteArray &data)
    { stream.write(data); }

    void write(Util::ByteArray &&data)
    { stream.write(std::move(data)); }

    void write(const QByteArray &data)
    { stream.write(data); }

    void write(QByteArray &&data)
    { stream.write(std::move(data)); }

    void write(AccelerationState state)
    {
        auto i = static_cast<std::uint8_t>(state);
        write(Util::ByteView(&i, 1));
    }
};

}

void StreamPipeline::attemptAcceleration(const std::string &key)
{
    if (!enableAcceleration) {
        qCDebug(log_datacore_streampipeline)
            << "Acceleration disabled so the incoming key was discarded";
        return;
    }
    if (!chainSource) {
        qCDebug(log_datacore_streampipeline)
            << "Acceleration not attempted because the source is not available";
        return;
    }

    auto parts = Util::split_string(key, ';', true);
    if (parts.size() < 2) {
        qCDebug(log_datacore_streampipeline) << "Invalid acceleration key" << key;
        return;
    }
    auto name = std::move(parts[0]);
    if (name.empty()) {
        qCDebug(log_datacore_streampipeline) << "No acceleration name provided";
        return;
    }
    auto secret = std::move(parts[1]);
    if (secret.empty()) {
        qCDebug(log_datacore_streampipeline) << "No acceleration secret provided for" << name;
        return;
    }

    std::string identity;
    if (parts.size() > 2)
        identity = std::move(parts[2]);
    if (!identity.empty()) {
        if (auto ids = dynamic_cast<IO::STDIO::Stream *>(chainInputStream.get())) {
            auto check = ids->pipelineInputIdentity();
            if (identity != check) {
                qCDebug(log_datacore_streampipeline) << "Local acceleration identity" << check
                                                     << "does not match the incoming" << identity
                                                     << "for" << name;
                return;
            }
        }
    }

    qCDebug(log_datacore_streampipeline) << "Connecting to acceleration on" << name;
    acceleration_client_connection();

#ifdef Q_OS_UNIX
    auto connection = IO::Socket::Unix::connect(getServerName(name));
#else
    auto connection = IO::Socket::Local::connect(getServerName(name));
#endif
    if (!connection) {
        qCDebug(log_datacore_streampipeline) << "Acceleration connection failed for" << name;
        return;
    }

    AccelerationInterface interface(*connection);
    {
        auto challenge = interface.read(33);
        if (!interface.consumeState(challenge, AccelerationState::Handshake_Server))
            return;
        Q_ASSERT(challenge.size() == 32);
        {
            QCryptographicHash hash(QCryptographicHash::Sha512);
            hash.addData(QByteArray::fromStdString(secret));
            hash.addData(challenge.toQByteArrayRef());
            challenge = hash.result();
        }
        Q_ASSERT(challenge.size() == 64);

        interface.write(AccelerationState::Handshake_Client);
        interface.write(std::move(challenge));
    }
    {
        Util::ByteArray challenge(Random::data(32));
        interface.write(challenge);

        {
            QCryptographicHash hash(QCryptographicHash::Sha512);
            hash.addData(QByteArray::fromStdString(secret));
            hash.addData(challenge.toQByteArrayRef());
            challenge = hash.result();
        }
        Q_ASSERT(challenge.size() == 64);

        auto response = interface.read(65);
        if (!interface.consumeState(response, AccelerationState::Handshake_Complete))
            return;
        if (response != challenge) {
            qCDebug(log_datacore_streampipeline) << "Invalid response to server handshake challenge"
                                                 << response << "but expected" << challenge;
            return;
        }
    }

    class InputLock {
        StreamPipeline &pipeline;
        bool lockHeld;

        class EndDiscardSink final : public ForwardSink {
        public:
            explicit EndDiscardSink(StreamSink *target) : ForwardSink(target)
            { }

            virtual ~EndDiscardSink() = default;

            void endData() override
            { }
        };

        std::unique_ptr<EndDiscardSink> endDiscard;

    public:
        explicit InputLock(StreamPipeline &pipeline) : pipeline(pipeline), lockHeld(false)
        {
            Q_ASSERT(pipeline.chainSource);
            Q_ASSERT(pipeline.chainInputSink);

            pipeline.chainSource->setEgress(nullptr);
            if (pipeline.chainInputSink->isEnded()) {
                qCDebug(log_datacore_streampipeline) << "Pipeline input already ended";
                return;
            }

            endDiscard.reset(new EndDiscardSink(pipeline.chainInputSink.get()));
            pipeline.chainSource->setEgress(endDiscard.get());

            lockHeld = true;
        }

        ~InputLock()
        {
            if (!lockHeld)
                return;

            qCDebug(log_datacore_streampipeline) << "Pipeline cannot reconnect input, aborting";

            /*
             * Non-recoverable abort here, so just get into a non-failing state and
             * terminate
             */
            if (pipeline.chainSource) {
                pipeline.chainSource->setEgress(&pipeline.discardSink);
            }
            pipeline.chainInputSink->endData();
            pipeline.signalTerminate();
        }

        bool isLocked() const
        { return lockHeld; }

        void completeDetach()
        {
            pipeline.chainSource->wait();
            if (auto converter = dynamic_cast<ExternalConverter *>(pipeline.chainSource.get())) {
                converter->stallRequested.disconnect();
            }
            pipeline.chainInputStream.reset();
            pipeline.chainSource.reset();

            qCDebug(log_datacore_streampipeline) << "Pipeline input detatch complete";
        }

        void handoffAttach(std::unique_ptr<IO::Generic::Stream> &&stream)
        {
            Q_ASSERT(pipeline.chainSource.get() == nullptr);

            pipeline.chainInputStream = std::move(stream);
            auto converter = new StandardDataInput;
            pipeline.chainSource.reset(converter);
            pipeline.sourceCanAccelerate = false;
            {
                std::lock_guard<std::mutex> lock(pipeline.mutex);
                pipeline.reapPending = true;
            }

            converter->stallRequested
                     .connect(std::bind(&IO::Generic::Stream::readStall,
                                        pipeline.chainInputStream.get(), std::placeholders::_1));
            pipeline.chainInputStream
                    ->read
                    .connect(std::bind(
                            static_cast<void (StandardDataInput::*)(const Util::ByteArray &)>(&StandardDataInput::incomingData),
                            converter, std::placeholders::_1));
            pipeline.chainInputStream
                    ->ended
                    .connect(std::bind(&StandardDataInput::endData, converter));

            auto parent = &pipeline;
            pipeline.chainSource->finished.connect([parent] {
                {
                    std::lock_guard<std::mutex> lock(parent->mutex);
                    parent->reapPending = true;
                }
                parent->notify.notify_all();
            });
            pipeline.chainSource->start();
            pipeline.chainSource->setEgress(pipeline.chainInputSink.get());

            /*
             * If it's already ended before we can send the switch ack, we have to abort since
             * we don't know if the input actually received the end, and it shouldn't have
             * ended regardless.
             */
            if (pipeline.chainInputStream->isEnded()) {
                qCWarning(log_datacore_streampipeline) << "Premature end of accelerated stream";
                return;
            }
            {
                auto i = static_cast<std::uint8_t>(AccelerationState::Handoff_Socket);
                pipeline.chainInputStream->write(Util::ByteView(&i, 1));
            }

            qCDebug(log_datacore_streampipeline) << "Pipeline input attached to socket";
            lockHeld = false;
            pipeline.acceleration_client_handoffComplete();
        }
    };

    acceleration_client_detaching();

    interface.write(AccelerationState::Detach_Begin);
    {
        auto data = interface.read(1);
        if (!data) {
            qCDebug(log_datacore_streampipeline) << "No ready response from server received";
            return;
        }
        switch (static_cast<AccelerationState>(data.front())) {
        default:
            qCDebug(log_datacore_streampipeline) << "Invalid response from server:"
                                                 << static_cast<int>(data.front());
            return;

        case AccelerationState::Detach_Interlock_Server:
            break;

        case AccelerationState::Server_Forward: {
            if (!enableForwarding) {
                qCDebug(log_datacore_streampipeline) << "Discarding forward response";
                return;
            }

            data = interface.read(2);
            if (!data) {
                qCDebug(log_datacore_streampipeline) << "No forwarding key length received";
                return;
            }
            data = interface.read(data.readNumber<quint16>());
            if (!data) {
                qCDebug(log_datacore_streampipeline) << "No forwarding key received";
                return;
            }
            qCDebug(log_datacore_streampipeline) << "Retrying acceleration with forwarded target";
            acceleration_client_forwarded();
            {
                std::lock_guard<std::mutex> lock(mutex);
                inputAccelerationKey = data.toString();
            }
            return;
        }
        }
    }
    InputLock input(*this);
    if (!input.isLocked()) {
        acceleration_client_detachAborted();
        return;
    }
    interface.write(AccelerationState::Detach_Interlock_Client);
    input.completeDetach();
    interface.write(AccelerationState::Detach_Complete);

    acceleration_client_detachCompleted();

    class ChainSender {
        StreamPipeline &pipeline;
        ChainStage detached;
    public:
        explicit ChainSender(StreamPipeline &pipeline) : pipeline(pipeline)
        { }

        ~ChainSender()
        {
            if (detached.stage) {
                detached.stage->endData();
                detached.stage->setEgress(&pipeline.discardSink);
                detached.stage->signalTerminate();
                detached.stage->wait();
                detached.stage.reset();
            }
            if (detached.outputSink) {
                detached.outputSink->endData();
            }
        }

        bool canSendHead() const
        {
            if (!pipeline.enableSerialization)
                return false;
            if (pipeline.chain.empty())
                return false;
            if (!pipeline.chain.front().component)
                return false;
            return !pipeline.chain.front().component->getGeneralSerializationName().isEmpty();
        }

        bool detachHead()
        {
            Q_ASSERT(!pipeline.chain.empty());

            detached = std::move(pipeline.chain.front());
            pipeline.chain.erase(pipeline.chain.begin());
            detached.stage->setEgress(nullptr);

            /*
             * The input lock has disconnected the input sink here, so we can manipulate it
             * without locking.
             */
            Q_ASSERT(pipeline.chainInputSink);
            Q_ASSERT(!pipeline.chainInputSink->isEnded());

            if (detached.outputSink->isEnded()) {
                qCDebug(log_datacore_streampipeline) << "Processing stage" << detached.component
                                                                                      ->getGeneralSerializationName()
                                                     << "has already ended, serialization not possible";
                detached.outputSink.reset();

                /*
                 * The end has already been sent down the pipeline, so just discard whatever
                 * else comes through.
                 */
                pipeline.chainInputSink->retarget(&pipeline.discardSink);
                return false;
            }

            /* Now set the pipeline input to the new head or the end, if the chain is empty */
            if (!pipeline.chain.empty()) {
                pipeline.chainInputSink->retarget(pipeline.chain.front().stage.get());
            } else {
                pipeline.chainInputSink->retarget(pipeline.chainOutputSink.get());
            }

            return true;
        }

        void serializeHead(IO::Generic::Stream &stream)
        {
            Q_ASSERT(detached.component);

            {
                QByteArray name(detached.component->getGeneralSerializationName().toLatin1());
                {
                    Util::ByteArray data;
                    data.appendNumber<quint16>(name.length());
                    stream.write(std::move(data));
                }
                stream.write(std::move(name));
            }

            Util::ByteArray data;
            data.resize(4);
            {
                Util::ByteArray::Device device(data);
                device.open(QIODevice::WriteOnly);
                device.seek(4);
                QDataStream target(&device);
                detached.stage->serialize(target);
            }
            qToLittleEndian<quint32>(data.size() - 4, data.data<uchar *>());

            qCDebug(log_datacore_streampipeline) << "Sending processing stage" << detached.component
                                                                                          ->getGeneralSerializationName()
                                                 << "with" << data.size()
                                                 << "bytes of serialized state";
            stream.write(std::move(data));

            detached.stage->endData();
            detached.stage->setEgress(&pipeline.discardSink);
            detached.stage->signalTerminate();
            detached.stage->wait();
            detached.stage.reset();
            detached.outputSink.reset();
            detached = ChainStage();
        }
    };

    {
        ChainSender sender(*this);
        while (sender.canSendHead()) {
            acceleration_client_sendStart();

            interface.write(AccelerationState::Request_Add_To_Chain);
            {
                auto data = interface.read(1);
                if (!data) {
                    qCDebug(log_datacore_streampipeline)
                        << "No chain add response from server received";
                    return;
                }
                auto response = static_cast<AccelerationState>(data.front());
                if (response == AccelerationState::Deny_Add_To_Chain) {
                    qCDebug(log_datacore_streampipeline) << "Chain add request denied";
                    acceleration_client_sendDenied();
                    break;
                } else if (response != AccelerationState::Accept_Add_To_Chain) {
                    qCDebug(log_datacore_streampipeline) << "Invalid response from server:"
                                                         << static_cast<int>(data.front());
                    return;
                }
            }

            if (!sender.detachHead())
                break;

            sender.serializeHead(*connection);
            {
                auto data = interface.read(1);
                if (!interface.consumeState(data, AccelerationState::Acknowledge_Add_To_Chain))
                    return;
            }

            acceleration_client_sendCompleted();
        }
    }

    acceleration_client_handoffStart();
    interface.releaseStream();
    input.handoffAttach(std::move(connection));
    forwardServerKey = name + ";" + secret;
}

bool StreamPipeline::canForwardConnect() const
{
    if (!enableForwarding)
        return false;
    if (forwardServerKey.empty())
        return false;
    if (!chain.empty())
        return false;
    if (!chainSource)
        return false;
    if (!chainSink)
        return false;
    if (chainInputSink->isEnded())
        return false;
    if (chainOutputSink->isEnded())
        return false;
    return true;
}

void StreamPipeline::processIncomingAcceleration(std::unique_ptr<
        IO::Socket::Connection> &&connection)
{
    Q_ASSERT(connection);

    qCDebug(log_datacore_streampipeline) << "Received acceleration connection on"
                                         << connection->describePeer();
    acceleration_server_connection();

    AccelerationInterface interface(*connection);
    {
        Util::ByteArray challenge(Random::data(32));
        interface.write(AccelerationState::Handshake_Server);
        interface.write(challenge);

        {
            QCryptographicHash hash(QCryptographicHash::Sha512);
            hash.addData(QByteArray::fromStdString(accelerationSecret));
            hash.addData(challenge.toQByteArrayRef());
            challenge = hash.result();
        }
        Q_ASSERT(challenge.size() == 64);

        auto response = interface.read(65);
        if (!interface.consumeState(response, AccelerationState::Handshake_Client))
            return;
        if (response != challenge) {
            qCDebug(log_datacore_streampipeline) << "Invalid response to server handshake challenge"
                                                 << response << "but expected" << challenge;
            return;
        }
    }
    {
        auto challenge = interface.read(32);
        if (!challenge) {
            qCDebug(log_datacore_streampipeline) << "No client challenge presented";
            return;
        }

        {
            QCryptographicHash hash(QCryptographicHash::Sha512);
            hash.addData(QByteArray::fromStdString(accelerationSecret));
            hash.addData(challenge.toQByteArrayRef());
            challenge = hash.result();
        }
        Q_ASSERT(challenge.size() == 64);

        interface.write(AccelerationState::Handshake_Complete);
        interface.write(std::move(challenge));
    }

    if (canForwardConnect()) {
        qCDebug(log_datacore_streampipeline) << "Sending forwarding response";
        acceleration_server_forward();

        interface.write(AccelerationState::Server_Forward);
        Util::ByteArray key;
        key.appendNumber<quint16>(forwardServerKey.length());
        key += Util::ByteView(forwardServerKey.data(), forwardServerKey.size());
        interface.write(std::move(key));
        return;
    }

    class OutputLock {
        StreamPipeline &pipeline;
        StreamSource *disconnectedSource;
        EndTrackingSink *disconnectedSink;
        bool lockHeld;
        bool chainOutputSinkReleased;
    public:
        explicit OutputLock(StreamPipeline &pipeline) : pipeline(pipeline),
                                                        disconnectedSource(nullptr),
                                                        disconnectedSink(nullptr),
                                                        lockHeld(false),
                                                        chainOutputSinkReleased(false)
        {
            if (!pipeline.chainSink) {
                qCDebug(log_datacore_streampipeline) << "No output sink available";
                return;
            }
            if (!pipeline.chainOutputSink || pipeline.chainOutputSink->isEnded()) {
                qCDebug(log_datacore_streampipeline) << "Chain output already ended";
                return;
            }

            if (!pipeline.chain.empty()) {
                auto &ce = pipeline.chain.back();
                Q_ASSERT(ce.stage);
                Q_ASSERT(ce.outputSink);
                ce.stage->setEgress(nullptr);
                if (ce.outputSink->isEnded()) {
                    qCDebug(log_datacore_streampipeline) << "Chain output already ended";
                    return;
                }

                qCDebug(log_datacore_streampipeline) << "Chain output disconnected";
                disconnectedSource = ce.stage.get();
                disconnectedSink = ce.outputSink.get();
            } else if (pipeline.chainSource) {
                Q_ASSERT(pipeline.chainInputSink);
                pipeline.chainSource->setEgress(nullptr);
                if (pipeline.chainInputSink->isEnded()) {
                    qCDebug(log_datacore_streampipeline) << "Pipeline input already ended";
                    return;
                }

                qCDebug(log_datacore_streampipeline) << "Pipeline source disconnected";
                disconnectedSource = pipeline.chainSource.get();
                disconnectedSink = pipeline.chainInputSink.get();
            } else if (pipeline.chainInputReader) {
                Q_ASSERT(pipeline.chainInputSink);
                pipeline.chainInputReader->setEgress(nullptr);
                if (pipeline.chainInputSink->isEnded()) {
                    qCDebug(log_datacore_streampipeline) << "Pipeline input already ended";
                    return;
                }

                qCDebug(log_datacore_streampipeline) << "Pipeline reader disconnected";
                disconnectedSource = pipeline.chainInputReader.get();
                disconnectedSink = pipeline.chainInputSink.get();
            } else if (pipeline.chainExternalInput) {
                Q_ASSERT(pipeline.chainInputSink);
                pipeline.chainExternalInput->setEgress(nullptr);
                if (pipeline.chainInputSink->isEnded()) {
                    qCDebug(log_datacore_streampipeline) << "Pipeline input already ended";
                    return;
                }

                qCDebug(log_datacore_streampipeline) << "Pipeline external input disconnected";
                disconnectedSource = pipeline.chainExternalInput.get();
                disconnectedSink = pipeline.chainInputSink.get();
            } else {
                qCDebug(log_datacore_streampipeline) << "No pipeline input available";
                return;
            }

            lockHeld = true;
        }

        ~OutputLock()
        {
            if (chainOutputSinkReleased) {
                qCDebug(log_datacore_streampipeline)
                    << "Pipeline cannot reconnect output, aborting";

                /*
                 * Non-recoverable abort here, so just get into a non-failing state and
                 * terminate
                 */
                pipeline.chainOutputSink->retarget(&pipeline.discardSink);
                pipeline.signalTerminate();
            }
            if (!lockHeld)
                return;

            Q_ASSERT(disconnectedSource);
            Q_ASSERT(disconnectedSink);

            qCDebug(log_datacore_streampipeline) << "Reattaching disconnected pipeline";
            disconnectedSource->setEgress(disconnectedSink);
        }

        bool isLocked() const
        { return lockHeld; }

        bool canAddToChain()
        { return pipeline.enableSerialization; }

        void detatchEndOriginal()
        {
            pipeline.chainSink->endData();
            pipeline.chainOutputSink->retarget(nullptr);
            pipeline.chainSink->wait();
            pipeline.chainSink.reset();
            chainOutputSinkReleased = true;

            qCDebug(log_datacore_streampipeline) << "Pipeline output detatch ended";
        }

        void handoffStartRaw(std::unique_ptr<IO::Generic::Stream> &&stream)
        {
            pipeline.sinkCanAccelerate = true;
            pipeline.chainSink
                    .reset(new StandardDataOutput(std::move(stream),
                                                  StandardDataOutput::OutputType::Raw));
            {
                auto parent = &pipeline;
                pipeline.chainSink->finished.connect([parent] {
                    {
                        std::lock_guard<std::mutex> lock(parent->mutex);
                        parent->reapPending = true;
                    }
                    parent->notify.notify_all();
                });
            }
            pipeline.chainSink->start();
            {
                std::lock_guard<std::mutex> lock(pipeline.mutex);
                pipeline.reapPending = true;
            }

            pipeline.chainOutputSink->retarget(pipeline.chainSink.get());
            chainOutputSinkReleased = false;

            qCDebug(log_datacore_streampipeline)
                << "Pipeline output attached to socket in raw mode";
        }

        void addToChain(std::unique_ptr<ProcessingStage> &&stage,
                        ProcessingStageComponent *component)
        {
            pipeline.chain.emplace_back(std::move(stage), component);
            auto &chainStage = pipeline.chain.back();

            chainStage.outputSink
                      .reset(new EndTrackingSink(pipeline.chainOutputSink.get(), pipeline));

            auto parent = &pipeline;
            chainStage.stage->finished.connect([parent] {
                parent->notify.notify_all();
            });
            {
                std::lock_guard<std::mutex> lock(pipeline.mutex);
                pipeline.reapPending = true;
            }

            chainStage.stage->start();

            /*
             * Safe because we've disconnected the original input by now, so we can alter
             * this without locking (since nothing should be referencing it by now).  We also
             * know it hasn't ended yet, so we don't have to worry about re-sending that.
             */
            disconnectedSink->retarget(chainStage.stage.get());

            /*
             * Restore the original connection, since we've now got a new and still disconnected
             * end of the pipeline.
             */
            disconnectedSource->setEgress(disconnectedSink);

            /*
             * And now record the new end of pipeline.
             */
            disconnectedSource = chainStage.stage.get();
            disconnectedSink = chainStage.outputSink.get();

            qCDebug(log_datacore_streampipeline) << "Deserialized pipeline processing stage"
                                                 << chainStage.component
                                                              ->getGeneralSerializationName();
        }
    };

    {
        auto data = interface.read(1);
        if (!interface.consumeState(data, AccelerationState::Detach_Begin))
            return;
    }
    acceleration_server_detaching();
    OutputLock output(*this);
    if (!output.isLocked()) {
        acceleration_server_detachAborted();
        return;
    }
    interface.write(AccelerationState::Detach_Interlock_Server);
    {
        auto data = interface.read(1);
        if (!interface.consumeState(data, AccelerationState::Detach_Interlock_Client)) {
            acceleration_server_detachFailed();
            return;
        }
    }
    output.detatchEndOriginal();
    {
        auto data = interface.read(1);
        if (!interface.consumeState(data, AccelerationState::Detach_Complete)) {
            acceleration_server_detachFailed();
            return;
        }
    }

    for (;;) {
        AccelerationState request;
        {
            auto data = interface.read(1);
            if (!data) {
                qCDebug(log_datacore_streampipeline) << "Client closed acceleration interface";
                return;
            }
            request = static_cast<AccelerationState>(data.front());
        }

        switch (request) {
        default:
            qCDebug(log_datacore_streampipeline) << "Invalid request from client:"
                                                 << static_cast<int>(request);
            return;

        case AccelerationState::Request_Add_To_Chain: {
            acceleration_server_incomingSend();

            if (!output.canAddToChain()) {
                qCDebug(log_datacore_streampipeline) << "Denying serialization request";
                interface.write(AccelerationState::Deny_Add_To_Chain);
                break;
            }
            interface.write(AccelerationState::Accept_Add_To_Chain);

            auto data = interface.read(2);
            if (!data) {
                qCDebug(log_datacore_streampipeline) << "No chain serialization name received";
                return;
            }
            auto nameLength = data.readNumber<quint16>();
            data = interface.read(nameLength);
            if (!data) {
                qCDebug(log_datacore_streampipeline) << "No component name received";
                return;
            }
            QString name(data.toQString(false));
            auto component = getDeserializationComponent(name);
            if (!component) {
                qCDebug(log_datacore_streampipeline) << "Unable to load component" << name;
                return;
            }

            data = interface.read(4);
            if (!data) {
                qCDebug(log_datacore_streampipeline)
                    << "No chain serialization data length received";
                return;
            }
            auto dataLength = data.readNumber<quint32>();
            data = interface.read(dataLength);
            if (data.size() != dataLength) {
                qCDebug(log_datacore_streampipeline) << "No chain serialization data received";
                return;
            }

            QDataStream stream(data.toQByteArrayRef());
            std::unique_ptr<ProcessingStage> stage(component->deserializeGeneralFilter(stream));
            if (!stage || stream.status() != QDataStream::Ok) {
                qCDebug(log_datacore_streampipeline) << "Deserialization of" << name << "failed";
                return;
            }
            output.addToChain(std::move(stage), component);

            interface.write(AccelerationState::Acknowledge_Add_To_Chain);
            break;
        }

        case AccelerationState::Handoff_Socket: {
            acceleration_server_handoff();
            output.handoffStartRaw(std::move(connection));
            return;
        }
        }
    }
}

void StreamPipeline::acceleration_client_connection()
{ }

void StreamPipeline::acceleration_client_detaching()
{ }

void StreamPipeline::acceleration_client_detachAborted()
{ }

void StreamPipeline::acceleration_client_forwarded()
{ }

void StreamPipeline::acceleration_client_detachCompleted()
{ }

void StreamPipeline::acceleration_client_sendStart()
{ }

void StreamPipeline::acceleration_client_sendDenied()
{ }

void StreamPipeline::acceleration_client_sendCompleted()
{ }

void StreamPipeline::acceleration_client_handoffStart()
{ }

void StreamPipeline::acceleration_client_handoffComplete()
{ }

void StreamPipeline::acceleration_server_connection()
{ }

void StreamPipeline::acceleration_server_forward()
{ }

void StreamPipeline::acceleration_server_detaching()
{ }

void StreamPipeline::acceleration_server_detachAborted()
{ }

void StreamPipeline::acceleration_server_detachFailed()
{ }

void StreamPipeline::acceleration_server_incomingSend()
{ }

void StreamPipeline::acceleration_server_handoff()
{ }


bool StreamPipeline::releaseInput(bool usingRetained)
{
    if (!usingRetained) {
        std::lock_guard<std::mutex> lock(mutex);
        if (threadState != ThreadState::Initialize) {
            chainError = QObject::tr("Cannot change the input after pipeline has been started");
            return false;
        }
        retainedShortCircuit = false;
        retainedInputFile.clear();
    }

    if (auto converter = dynamic_cast<ExternalConverter *>(chainSource.get())) {
        converter->stallRequested.disconnect();
    }
    chainInputStream.reset();
    chainSource.reset();
    ownedArchive.reset();
    chainInputArchive = nullptr;
    chainInputSelections.clear();
    chainExternalInput.reset();
    sourceCanAccelerate = false;
    chainTapUseRequestedInput = false;
    chainTapRequestedBounds = Time::Bounds();
    inputTotalSize = 0;
    chainInputClipBounds = Time::Bounds();

    return true;
}

bool StreamPipeline::setInputPipeline()
{
    if (!releaseInput())
        return false;

    if (!pipelineInputHandle) {
        pipelineInputHandle = createPipelineInput();
        if (!pipelineInputHandle) {
            inputError = QObject::tr("Unable to open standard input handle");
            return false;
        }
    }

    chainInputStream = pipelineInputHandle->stream();
    if (!chainInputStream) {
        inputError = QObject::tr("Unable to open standard input");
        return false;
    }

    qCDebug(log_datacore_streampipeline) << "Pipeline input attached to standard input";

    chainSource.reset(new StandardDataInput);
    sourceCanAccelerate = true;
    return true;
}

bool StreamPipeline::setInputFile(std::string name)
{
    if (!releaseInput())
        return false;

    auto handle = IO::Access::file(name, IO::File::Mode::readOnly());
    if (!handle) {
        inputError = QObject::tr("Unable to get input handle");
        return false;
    }

    chainInputStream = handle->stream();
    if (!chainInputStream) {
        inputError = QObject::tr("Unable to open input file");
        return false;
    }

    qCDebug(log_datacore_streampipeline) << "Pipeline input attached to file" << handle->filename();

    inputTotalSize = handle->size();
    chainSource.reset(new StandardDataInput);
    if (QFileInfo(QString::fromStdString(name)).isFile()) {
        retainedInputFile = std::move(name);
    }
    return true;
}

bool StreamPipeline::setInputArchive(const Archive::Selection::List &selections,
                                     const Time::Bounds &clip,
                                     Archive::Access *archive)
{
    if (!releaseInput())
        return false;
    if (selections.empty()) {
        inputError = QObject::tr("No archive selections specified");
        return false;
    }

    if (!archive) {
        ownedArchive.reset(new Archive::Access);
        archive = ownedArchive.get();
    }

    Q_ASSERT(archive);

    qCDebug(log_datacore_streampipeline) << "Pipeline input attached to archive with" << selections
                                         << "clipped to" << clip;

    chainInputSelections = selections;
    chainInputClipBounds = clip;
    chainInputArchive = archive;
    return true;
}

bool StreamPipeline::setInputArchive(const Archive::Selection &selection,
                                     const Time::Bounds &clip,
                                     Archive::Access *archive)
{ return setInputArchive(Archive::Selection::List{selection}, clip, archive); }

bool StreamPipeline::setInputArchiveRequested(const Time::Bounds &bounds,
                                              bool clip,
                                              Archive::Access *archive)
{
    if (!releaseInput())
        return false;

    if (chainTapOutput && chain.empty()) {
        if (clip) {
            chainInputClipBounds = bounds;
        }
        chainTapUseRequestedInput = true;
        chainTapRequestedBounds = bounds;
        chainInputArchive = archive;
        return true;
    }

    Archive::Selection::List req;
    for (const auto &name : requestedInputs()) {
        req.emplace_back(name, bounds.getStart(), bounds.getEnd());
    }
    if (req.empty()) {
        inputError = QObject::tr("No input requested");
        return false;
    }

    return setInputArchive(std::move(req), clip ? bounds : Time::Bounds(), archive);
}

bool StreamPipeline::setInputSource(std::unique_ptr<ExternalSource> &&input)
{
    Q_ASSERT(input);
    if (!releaseInput())
        return false;

    qCDebug(log_datacore_streampipeline) << "Pipeline input attached to external source";

    chainSource = std::move(input);
    return true;
}

bool StreamPipeline::setInputGeneral(std::unique_ptr<ExternalConverter> &&input,
                                     std::unique_ptr<IO::Generic::Stream> &&stream,
                                     bool canAccelerate)
{
    Q_ASSERT(input);
    if (!releaseInput())
        return false;

    qCDebug(log_datacore_streampipeline) << "Pipeline input attached to external converter";

    chainSource = std::move(input);
    chainInputStream = std::move(stream);
    sourceCanAccelerate = canAccelerate;
    return true;
}

bool StreamPipeline::setInputGeneral(std::unique_ptr<ExternalConverter> &&input,
                                     QIODevice *inputDevice,
                                     bool canAccelerate,
                                     bool ownDevice)
{
    Q_ASSERT(input);
    if (!releaseInput())
        return false;

    {
        auto sz = inputDevice->size();
        if (sz > 0) {
            inputTotalSize = static_cast<std::size_t>(sz);
        }
    }

    IO::Access::Handle handle;
    if (!ownDevice) {
        handle = IO::Access::qio(inputDevice);
    } else {
        QThread *target = nullptr;
        handle = IO::Access::qio([inputDevice, &target]() -> std::unique_ptr<QIODevice> {
            target = QThread::currentThread();
            return std::unique_ptr<QIODevice>(inputDevice);
        });
        if (!target) {
            inputError = QObject::tr("Error attaching QIODevice thread");
            return false;
        }
        Q_ASSERT(handle);
        inputDevice->setParent(nullptr);
        inputDevice->moveToThread(target);
    }

    if (!handle) {
        inputError = QObject::tr("Unable to create QIODevice handle");
        return false;
    }

    qCDebug(log_datacore_streampipeline) << "Pipeline input attached to external converter";

    chainSource = std::move(input);
    chainInputStream = handle->stream();
    sourceCanAccelerate = canAccelerate;
    return true;
}

bool StreamPipeline::setInputGeneral(std::unique_ptr<ExternalConverter> &&input,
                                     std::string name,
                                     bool textMode)
{
    Q_ASSERT(input);
    if (!releaseInput())
        return false;

    auto handle = IO::Access::file(std::move(name), IO::File::Mode::readOnly().textMode(textMode));
    if (!handle) {
        inputError = QObject::tr("Unable to get input handle");
        return false;
    }

    chainInputStream = handle->stream();
    if (!chainInputStream) {
        inputError = QObject::tr("Unable to open input file");
        return false;
    }

    chainSource = std::move(input);
    return true;
}

bool StreamPipeline::setInputGeneralPipeline(std::unique_ptr<ExternalConverter> &&input,
                                             bool canAccelerate)
{
    Q_ASSERT(input);
    if (!releaseInput())
        return false;

    if (!pipelineInputHandle) {
        pipelineInputHandle = createPipelineInput();
        if (!pipelineInputHandle) {
            inputError = QObject::tr("Unable to open standard input handle");
            return false;
        }
    }

    chainInputStream = pipelineInputHandle->stream();
    if (!chainInputStream) {
        inputError = QObject::tr("Unable to open standard input");
        return false;
    }

    qCDebug(log_datacore_streampipeline)
        << "Pipeline input attached to external converter from standard input";

    chainSource = std::move(input);
    sourceCanAccelerate = canAccelerate;
    return true;
}

StreamSink *StreamPipeline::setInputExternal()
{
    if (!releaseInput())
        return nullptr;

    qCDebug(log_datacore_streampipeline) << "Pipeline input attached to external input";

    chainExternalInput.reset(new ExternalInputSink(*this));
    return chainExternalInput.get();
}


bool StreamPipeline::releaseOutput()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (threadState != ThreadState::Initialize) {
            chainError = QObject::tr("Cannot change the output after pipeline has been started");
            return false;
        }
    }

    chainSink.reset();
    sinkCanAccelerate = false;
    chainTapOutput = nullptr;
    chainExternalOutput.reset();
    retainOutput = false;
    retainedShortCircuit = false;

    return true;
}

bool StreamPipeline::setOutputPipeline()
{
    if (!releaseOutput())
        return false;

    if (!pipelineOutputHandle) {
        pipelineOutputHandle = createPipelineOutput();
        if (!pipelineOutputHandle) {
            outputError = QObject::tr("Unable to open standard output handle");
            return false;
        }
    }

    auto stream = pipelineOutputHandle->stream();
    if (!stream) {
        outputError = QObject::tr("Unable to open standard output");
        return false;
    }

    qCDebug(log_datacore_streampipeline) << "Pipeline output attached to standard output";

    chainSink.reset(new StandardDataOutput(std::move(stream)));
    sinkCanAccelerate = true;
    return true;
}

bool StreamPipeline::setOutputFile(std::string name, StandardDataOutput::OutputType mode)
{
    if (!releaseOutput())
        return false;

    auto fileMode = IO::File::Mode::writeOnly().bufferedMode();
    switch (mode) {
    case StandardDataOutput::OutputType::Raw:
    case StandardDataOutput::OutputType::Raw_Legacy:
        fileMode.textMode(false);
        break;
    default:
        fileMode.textMode(true);
        break;
    }

    auto handle = IO::Access::file(std::move(name), fileMode);
    if (!handle) {
        outputError = QObject::tr("Unable to get output handle");
        return false;
    }

    auto stream = handle->stream();
    if (!stream) {
        outputError = QObject::tr("Unable to open output file");
        return false;
    }

    qCDebug(log_datacore_streampipeline) << "Pipeline output attached to file"
                                         << handle->filename();

    chainSink.reset(new StandardDataOutput(std::move(stream), mode));
    chainSink->feedback.forward(feedback);
    return true;
}

bool StreamPipeline::setOutputGeneral(std::unique_ptr<ExternalSink> &&output)
{
    Q_ASSERT(output);
    if (!releaseOutput())
        return false;

    qCDebug(log_datacore_streampipeline) << "Pipeline output attached to external sink";

    chainSink = std::move(output);
    chainSink->feedback.forward(feedback);
    return true;
}

bool StreamPipeline::setOutputFilterChain(ProcessingTapChain *tapChain, bool retain)
{
    Q_ASSERT(tapChain != nullptr);

    if (retain && canRestartWithRetained()) {
        chainTapOutput = tapChain;
        chainTapOutput->feedback.forward(feedback);
        return true;
    }

    if (!releaseOutput())
        return false;

    qCDebug(log_datacore_streampipeline) << "Pipeline output attached to tap chain";

    chainTapOutput = tapChain;
    retainOutput = retain;
    chainTapOutput->feedback.forward(feedback);
    return true;
}

bool StreamPipeline::setOutputIngress(StreamSink *target)
{
    Q_ASSERT(target != nullptr);

    if (!releaseOutput())
        return false;

    qCDebug(log_datacore_streampipeline) << "Pipeline output attached to sink";

    chainExternalOutput.reset(new ExternalOutputSink(target, *this));
    return true;
}

IO::Access::Handle StreamPipeline::pipelineOutputDevice()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (threadState != ThreadState::Initialize) {
            outputError = QObject::tr(
                    "Cannot create a standard output handle after the pipeline has started");
            return {};
        }
    }

    if (!pipelineOutputHandle) {
        pipelineOutputHandle = createPipelineOutput();
        if (!pipelineOutputHandle) {
            outputError = QObject::tr("Unable to open standard output handle");
            return {};
        }
    }

    sinkCanAccelerate = true;
    return pipelineOutputHandle;
}


StreamPipeline::ChainStage::ChainStage() : component(nullptr)
{ }

StreamPipeline::ChainStage::ChainStage(std::unique_ptr<ProcessingStage> &&stage,
                                       ProcessingStageComponent *component) : stage(
        std::move(stage)), component(component)
{ }

bool StreamPipeline::canModifyChain()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (threadState == ThreadState::Initialize)
            return true;
    }
    chainError = QObject::tr("Cannot add processing stages after the pipeline has been started");
    return false;
}

bool StreamPipeline::addProcessingStage(std::unique_ptr<ProcessingStage> &&stage,
                                        ProcessingStageComponent *component)
{
    Q_ASSERT(stage);
    if (!canModifyChain())
        return false;

    if (component) {
        qCDebug(log_datacore_streampipeline) << "Added processing stage"
                                             << component->getGeneralSerializationName();
    } else {
        qCDebug(log_datacore_streampipeline) << "Added processing stage";
    }

    chain.emplace_back(std::move(stage), component);
    return true;
}


SequenceName::Set StreamPipeline::requestedInputs()
{
    SequenceName::Set result;
    ThreadLock lock(*this);
    for (const auto &proc : chain) {
        Util::merge(proc.stage->requestedInputs(), result);
    }
    return result;
}

SequenceName::Set StreamPipeline::predictedOutputs()
{
    SequenceName::Set result;
    ThreadLock lock(*this);
    if (chainSource) {
        result = chainSource->predictedOutputs();
    }
    for (const auto &proc : chain) {
        Util::merge(proc.stage->predictedOutputs(), result);
    }
    return result;
}

bool StreamPipeline::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return threadState == ThreadState::Completed;
}

bool StreamPipeline::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, mutex, notify,
                                     [this] { return threadState == ThreadState::Completed; });
}

bool StreamPipeline::waitInEventLoop(double timeout)
{ return finished.waitInEventLoop(std::bind(&StreamPipeline::isFinished, this), timeout); }

std::uint_fast64_t StreamPipeline::getExpectedInputSize()
{ return inputTotalSize; }

std::uint_fast64_t StreamPipeline::getTotalInputRead()
{ return inputBytesRead.load(std::memory_order_relaxed); }

ProcessingStageComponent *StreamPipeline::getDeserializationComponent(const QString &name)
{
    auto component = ComponentLoader::create(name);
    auto stage = qobject_cast<ProcessingStageComponent *>(component);
    if (!stage) {
        qFatal("Unable to load deserialization component %s", name.toUtf8().constData());
        return nullptr;
    }
    if (stage->getGeneralSerializationName() != name) {
        qFatal("Attempt to load component %s instead resulted in %s", name.toUtf8().constData(),
               stage->getGeneralSerializationName().toUtf8().constData());
        return nullptr;
    }
    return stage;
}

IO::Access::Handle StreamPipeline::createPipelineInput()
{ return IO::Access::stdio_in(); }

IO::Access::Handle StreamPipeline::createPipelineOutput()
{ return IO::Access::stdio_out(); }


StreamPipeline::ForwardSink::ForwardSink(StreamSink *target) : target(target)
{
    Q_ASSERT(this->target != nullptr);
}

StreamPipeline::ForwardSink::~ForwardSink() = default;

void StreamPipeline::ForwardSink::incomingData(const SequenceValue::Transfer &values)
{ return target->incomingData(values); }

void StreamPipeline::ForwardSink::incomingData(SequenceValue::Transfer &&values)
{ return target->incomingData(std::move(values)); }

void StreamPipeline::ForwardSink::incomingData(const SequenceValue &value)
{ return target->incomingData(value); }

void StreamPipeline::ForwardSink::incomingData(SequenceValue &&value)
{ return target->incomingData(std::move(value)); }

void StreamPipeline::ForwardSink::endData()
{ return target->endData(); }

void StreamPipeline::ForwardSink::retarget(StreamSink *change)
{ target = change; }

StreamPipeline::ClippingSink::ClippingSink(StreamSink *target, const Time::Bounds &clip)
        : ForwardSink(target), clip(clip)
{ }

StreamPipeline::ClippingSink::~ClippingSink() = default;

void StreamPipeline::ClippingSink::incomingData(const SequenceValue::Transfer &values)
{
    for (auto check = values.cbegin(), endCheck = values.cend(); check != endCheck; ++check) {
        if (Range::compareEnd(check->getEnd(), clip.end) <= 0 &&
                Range::compareStart(check->getStart(), clip.start) >= 0)
            continue;

        SequenceValue::Transfer result;
        std::copy(values.cbegin(), check, Util::back_emplacer(result));

        for (; check != endCheck; ++check) {
            SequenceValue add = *check;
            if (Range::compareEnd(add.getEnd(), clip.end) > 0)
                add.setEnd(clip.end);
            if (Range::compareStart(add.getStart(), clip.start) < 0)
                add.setStart(clip.start);
            if (Range::compareStartEnd(add.getStart(), add.getEnd()) > 0)
                continue;
            result.emplace_back(std::move(add));
        }

        return ForwardSink::incomingData(std::move(result));
    }

    return ForwardSink::incomingData(values);
}

void StreamPipeline::ClippingSink::incomingData(SequenceValue::Transfer &&values)
{
    for (auto &mod : values) {
        if (Range::compareEnd(mod.getEnd(), clip.end) > 0)
            mod.setEnd(clip.end);
        if (Range::compareStart(mod.getStart(), clip.start) < 0)
            mod.setStart(clip.start);
    }

    return ForwardSink::incomingData(std::move(values));
}

void StreamPipeline::ClippingSink::incomingData(const SequenceValue &value)
{
    if (Range::compareEnd(value.getEnd(), clip.end) > 0) {
        SequenceValue add = value;
        add.setEnd(clip.end);
        if (Range::compareStart(add.getStart(), clip.start) < 0)
            add.setStart(clip.start);
        if (Range::compareStartEnd(add.getStart(), add.getEnd()) > 0)
            return;
        return ForwardSink::incomingData(std::move(add));
    } else if (Range::compareStart(value.getStart(), clip.start) < 0) {
        SequenceValue add = value;
        add.setStart(clip.start);
        if (Range::compareStartEnd(add.getStart(), add.getEnd()) > 0)
            return;
        return ForwardSink::incomingData(std::move(add));
    }

    return ForwardSink::incomingData(value);
}

void StreamPipeline::ClippingSink::incomingData(SequenceValue &&value)
{
    if (Range::compareEnd(value.getEnd(), clip.end) > 0)
        value.setEnd(clip.end);
    if (Range::compareStart(value.getStart(), clip.start) < 0)
        value.setStart(clip.start);
    if (Range::compareStartEnd(value.getStart(), value.getEnd()) > 0)
        return;

    return ForwardSink::incomingData(std::move(value));
}

StreamPipeline::EndTrackingSink::EndTrackingSink(StreamSink *target, StreamPipeline &parent)
        : ForwardSink(target), mutex(parent.mutex), receivedEnd(false)
{ }

StreamPipeline::EndTrackingSink::~EndTrackingSink() = default;

void StreamPipeline::EndTrackingSink::endData()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        receivedEnd = true;
    }
    ForwardSink::endData();
}

bool StreamPipeline::EndTrackingSink::isEnded()
{
    std::lock_guard<std::mutex> lock(mutex);
    return receivedEnd;
}

StreamPipeline::StartTrackingSink::StartTrackingSink(StreamSink *target, StreamPipeline &parent)
        : ForwardSink(target), mutex(parent.mutex), notify(parent.notify), receivedData(false)
{ }

StreamPipeline::StartTrackingSink::~StartTrackingSink() = default;

void StreamPipeline::StartTrackingSink::incomingData(const SequenceValue::Transfer &values)
{
    if (!receivedData) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            receivedData = true;
        }
        notify.notify_all();
    }
    ForwardSink::incomingData(values);
}

void StreamPipeline::StartTrackingSink::incomingData(SequenceValue::Transfer &&values)
{
    if (!receivedData) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            receivedData = true;
        }
        notify.notify_all();
    }
    ForwardSink::incomingData(values);
}

void StreamPipeline::StartTrackingSink::incomingData(const SequenceValue &value)
{
    if (!receivedData) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            receivedData = true;
        }
        notify.notify_all();
    }
    ForwardSink::incomingData(value);
}

void StreamPipeline::StartTrackingSink::incomingData(SequenceValue &&value)
{
    if (!receivedData) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            receivedData = true;
        }
        notify.notify_all();
    }
    ForwardSink::incomingData(value);
}

bool StreamPipeline::StartTrackingSink::isStartedUnlocked() const
{ return receivedData; }

StreamPipeline::RetainSink::RetainSink(StreamSink *target) : ForwardSink(target)
{ }

StreamPipeline::RetainSink::~RetainSink() = default;

void StreamPipeline::RetainSink::incomingData(const SequenceValue::Transfer &values)
{
    Util::append(values, contents);
    ForwardSink::incomingData(values);
}

void StreamPipeline::RetainSink::incomingData(SequenceValue::Transfer &&values)
{
    Util::append(values, contents);
    ForwardSink::incomingData(std::move(values));
}

void StreamPipeline::RetainSink::incomingData(const SequenceValue &value)
{
    contents.emplace_back(value);
    ForwardSink::incomingData(value);
}

void StreamPipeline::RetainSink::incomingData(SequenceValue &&value)
{
    contents.emplace_back(value);
    ForwardSink::incomingData(std::move(value));
}

StreamPipeline::ExternalInputSink::ExternalInputSink(StreamPipeline &parent) : next(nullptr),
                                                                               endMutex(
                                                                                       parent.mutex),
                                                                               endNotify(
                                                                                       parent.notify),
                                                                               reapPending(
                                                                                       parent.reapPending),
                                                                               receivedEnd(false),
                                                                               discardData(false)
{ }

StreamPipeline::ExternalInputSink::~ExternalInputSink() = default;

void StreamPipeline::ExternalInputSink::incomingData(const SequenceValue::Transfer &values)
{
    std::unique_lock<std::mutex> lock(mutex);
    notify.wait(lock, [this] { return discardData || next != nullptr; });
    if (discardData)
        return;
    next->incomingData(values);
}

void StreamPipeline::ExternalInputSink::incomingData(SequenceValue::Transfer &&values)
{
    std::unique_lock<std::mutex> lock(mutex);
    notify.wait(lock, [this] { return discardData || next != nullptr; });
    if (discardData)
        return;
    next->incomingData(std::move(values));
}

void StreamPipeline::ExternalInputSink::incomingData(const SequenceValue &value)
{
    std::unique_lock<std::mutex> lock(mutex);
    notify.wait(lock, [this] { return discardData || next != nullptr; });
    if (discardData)
        return;
    next->incomingData(value);
}

void StreamPipeline::ExternalInputSink::incomingData(SequenceValue &&value)
{
    std::unique_lock<std::mutex> lock(mutex);
    notify.wait(lock, [this] { return discardData || next != nullptr; });
    if (discardData)
        return;
    next->incomingData(std::move(value));
}

void StreamPipeline::ExternalInputSink::endData()
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        if (!discardData) {
            if (next) {
                next->endData();
            }
            /* Don't need to notify, since the calls that wait on it (incomingData(...)) should
             * be serialized with endData() anyway */
            discardData = true;
        }
    }
    {
        std::lock_guard<std::mutex> lock(endMutex);
        receivedEnd = true;
        reapPending = true;
    }
    endNotify.notify_all();
}

void StreamPipeline::ExternalInputSink::setEgress(StreamSink *sink)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        if (discardData) {
            next = nullptr;
            lock.unlock();
            if (sink) {
                sink->endData();
            }
            return;
        }
        next = sink;
        if (!next)
            return;
    }
    notify.notify_all();
}

bool StreamPipeline::ExternalInputSink::isEnded()
{
    std::lock_guard<std::mutex> lock(endMutex);
    return receivedEnd;
}

void StreamPipeline::ExternalInputSink::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (discardData)
            return;
        discardData = true;
        if (next) {
            next->endData();
        }
    }
    notify.notify_all();
    {
        std::lock_guard<std::mutex> lock(endMutex);
        receivedEnd = true;
        reapPending = true;
    }
    endNotify.notify_all();
}

StreamPipeline::ExternalOutputSink::ExternalOutputSink(StreamSink *target, StreamPipeline &parent)
        : ForwardSink(target),
          mutex(parent.mutex),
          notify(parent.notify),
          reapPending(parent.reapPending),
          receivedEnd(false)
{ }

StreamPipeline::ExternalOutputSink::~ExternalOutputSink() = default;

void StreamPipeline::ExternalOutputSink::endData()
{
    ForwardSink::endData();
    std::lock_guard<std::mutex> lock(mutex);
    receivedEnd = true;
    reapPending = true;
    notify.notify_all();
}

bool StreamPipeline::ExternalOutputSink::isEnded()
{
    std::lock_guard<std::mutex> lock(mutex);
    return receivedEnd;
}

StreamPipeline::FullDiscardSink::FullDiscardSink() = default;

StreamPipeline::FullDiscardSink::~FullDiscardSink() = default;

void StreamPipeline::FullDiscardSink::incomingData(const SequenceValue::Transfer &)
{ }

void StreamPipeline::FullDiscardSink::incomingData(SequenceValue::Transfer &&)
{ }

void StreamPipeline::FullDiscardSink::incomingData(const SequenceValue &)
{ }

void StreamPipeline::FullDiscardSink::incomingData(SequenceValue &&)
{ }

void StreamPipeline::FullDiscardSink::endData()
{ }


StreamPipeline::ProcessingTapBackend::ProcessingTapBackend(StreamPipeline &parent) : parent(parent),
                                                                                     issueComplete(
                                                                                             false),
                                                                                     dispatch(*this)
{ }

StreamPipeline::ProcessingTapBackend::~ProcessingTapBackend() = default;

void StreamPipeline::ProcessingTapBackend::issueArchiveRead(const Archive::Selection::List &selections,
                                                            StreamSink *target)
{
    Q_ASSERT(!issueComplete);
    dispatch.addTarget(target);
    Util::append(selections, allSelections);
}

void StreamPipeline::ProcessingTapBackend::archiveReadIssueComplete()
{
    {
        std::lock_guard<std::mutex> lock(parent.mutex);
        issueComplete = true;
    }
    parent.notify.notify_all();
}

const Archive::Selection::List &StreamPipeline::ProcessingTapBackend::waitForSelections()
{
    {
        std::unique_lock<std::mutex> lock(parent.mutex);
        parent.notify.wait(lock, [this] { return issueComplete; });
    }
    return allSelections;
}

StreamPipeline::ProcessingTapBackend::DispatchSink::DispatchSink(ProcessingTapBackend &parent)
{ }

StreamPipeline::ProcessingTapBackend::DispatchSink::~DispatchSink() = default;

void StreamPipeline::ProcessingTapBackend::DispatchSink::incomingData(const SequenceValue::Transfer &values)
{
    for (auto t : targets) {
        t->incomingData(values);
    }
}

void StreamPipeline::ProcessingTapBackend::DispatchSink::incomingData(SequenceValue::Transfer &&values)
{
    if (targets.empty())
        return;

    auto t = targets.cbegin();
    for (auto endT = targets.cend() - 1; t != endT; ++t) {
        (*t)->incomingData(values);
    }
    (*t)->incomingData(std::move(values));
}

void StreamPipeline::ProcessingTapBackend::DispatchSink::incomingData(const SequenceValue &value)
{
    for (auto t : targets) {
        t->incomingData(value);
    }
}

void StreamPipeline::ProcessingTapBackend::DispatchSink::incomingData(SequenceValue &&value)
{
    if (targets.empty())
        return;

    auto t = targets.cbegin();
    for (auto endT = targets.cend() - 1; t != endT; ++t) {
        (*t)->incomingData(value);
    }
    (*t)->incomingData(std::move(value));
}

void StreamPipeline::ProcessingTapBackend::DispatchSink::endData()
{
    for (auto t : targets) {
        t->endData();
    }
}

void StreamPipeline::ProcessingTapBackend::DispatchSink::addTarget(StreamSink *target)
{ targets.emplace_back(target); }

}
}
