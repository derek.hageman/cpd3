/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "datacore/wavelength.hxx"

namespace CPD3 {
namespace Data {

/** @file datacore/wavelength.hxx
 * Provides routines to deal with wavelength dependent data.
 */

static const std::string code_none;
static const std::string code_B = "B";
static const std::string code_G = "G";
static const std::string code_R = "R";
static const std::string code_Q = "Q";

/**
 * Get the wavelength code for a given wavelength value.
 * 
 * @param wavelength    the wavelength in nm
 * @return              the wavelength code (e.x. "R") or empty for none
 */
const std::string &Wavelength::code(double wavelength)
{
    Q_ASSERT(FP::defined(wavelength));
    if (wavelength < 400.0)
        return code_none;
    if (wavelength < 500.0)
        return code_B;
    if (wavelength < 600.0)
        return code_G;
    if (wavelength < 750.0)
        return code_R;
    return code_Q;
}

}
}
