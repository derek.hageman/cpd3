/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <memory>
#include <QDebugStateSaver>

#include "dynamicsequenceselection.hxx"
#include "core/qtcompat.hxx"
#include "core/util.hxx"


namespace CPD3 {
namespace Data {

enum TimeOperateType {
    TO_NULL = 0, TO_NEVER, TO_COMPOSITE, TO_ALWAYSSINGLE, TO_ALWAYS, TO_ALWAYSREGEX, TO_TIMEREGEX
};

static const SequenceName::Set invalidSet;

DynamicSequenceSelection::None::None() = default;

DynamicSequenceSelection::None::~None() = default;

DynamicSequenceSelection::None::None(QDataStream &)
{ }

const SequenceName::Set &DynamicSequenceSelection::None::get(double, double)
{ return invalidSet; }

const SequenceName::Set &DynamicSequenceSelection::None::get(double)
{ return invalidSet; }

const SequenceName::Set &DynamicSequenceSelection::None::getConst(double, double) const
{ return invalidSet; }

const SequenceName::Set &DynamicSequenceSelection::None::getConst(double) const
{ return invalidSet; }

bool DynamicSequenceSelection::None::matches(double, double, const SequenceName &)
{ return false; }

bool DynamicSequenceSelection::None::matchesConst(double, double, const SequenceName &) const
{ return false; }

bool DynamicSequenceSelection::None::matches(double, const SequenceName &)
{ return false; }

bool DynamicSequenceSelection::None::matchesConst(double, const SequenceName &) const
{ return false; }

Archive::Selection::List DynamicSequenceSelection::None::toArchiveSelections(const Archive::Selection::Match &,
                                                                             const Archive::Selection::Match &,
                                                                             const Archive::Selection::Match &) const
{ return Archive::Selection::List(); }

DynamicSequenceSelection *DynamicSequenceSelection::None::clone() const
{ return new DynamicSequenceSelection::None(); }

void DynamicSequenceSelection::None::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(TO_NEVER);
}

void DynamicSequenceSelection::None::printLog(QDebug &stream) const
{ stream << "DynamicSequenceSelection::None()"; }

DynamicSequenceSelection::Composite::Composite() = default;

DynamicSequenceSelection::Composite::Composite(std::unique_ptr<DynamicSequenceSelection> &&base)
{
    components.emplace_back(std::move(base));
}

DynamicSequenceSelection::Composite::Composite(std::vector<
        std::unique_ptr<DynamicSequenceSelection>> &&bases) : components(std::move(bases))
{ }

DynamicSequenceSelection::Composite::~Composite() = default;

const SequenceName::Set &DynamicSequenceSelection::Composite::get(double start, double end)
{
    if (components.empty())
        return invalidSet;
    if (components.size() == 1)
        return components.front()->get(start, end);
    static thread_local SequenceName::Set result;
    result.clear();
    for (auto &c : components) {
        Util::merge(c->get(start, end), result);
    }
    return result;
}

const SequenceName::Set &DynamicSequenceSelection::Composite::get(double time)
{
    if (components.empty())
        return invalidSet;
    if (components.size() == 1)
        return components.front()->get(time);
    static thread_local SequenceName::Set result;
    result.clear();
    for (auto &c : components) {
        Util::merge(c->get(time), result);
    }
    return result;
}

const SequenceName::Set &DynamicSequenceSelection::Composite::getConst(double start,
                                                                       double end) const
{
    if (components.empty())
        return invalidSet;
    if (components.size() == 1)
        return components.front()->getConst(start, end);
    static thread_local SequenceName::Set result;
    result.clear();
    for (auto &c : components) {
        Util::merge(c->getConst(start, end), result);
    }
    return result;
}

const SequenceName::Set &DynamicSequenceSelection::Composite::getConst(double time) const
{
    if (components.empty())
        return invalidSet;
    if (components.size() == 1)
        return components.front()->getConst(time);
    static thread_local SequenceName::Set result;
    result.clear();
    for (auto &c : components) {
        Util::merge(c->getConst(time), result);
    }
    return result;
}

bool DynamicSequenceSelection::Composite::matches(double start,
                                                  double end,
                                                  const SequenceName &name)
{
    for (auto &c : components) {
        if (c->matches(start, end, name))
            return true;
    }
    return false;
}

bool DynamicSequenceSelection::Composite::matchesConst(double start,
                                                       double end,
                                                       const SequenceName &name) const
{
    for (const auto &c : components) {
        if (c->matchesConst(start, end, name))
            return true;
    }
    return false;
}

bool DynamicSequenceSelection::Composite::matches(double time, const SequenceName &name)
{
    for (auto &c : components) {
        if (c->matches(time, name))
            return true;
    }
    return false;
}

bool DynamicSequenceSelection::Composite::matchesConst(double time, const SequenceName &name) const
{
    for (const auto &c : components) {
        if (c->matchesConst(time, name))
            return true;
    }
    return false;
}

Archive::Selection::List DynamicSequenceSelection::Composite::toArchiveSelections(const Archive::Selection::Match &defaultStations,
                                                                                  const Archive::Selection::Match &defaultArchives,
                                                                                  const Archive::Selection::Match &defaultVariables) const
{
    Archive::Selection::List result;
    for (const auto &c : components) {
        Util::append(c->toArchiveSelections(defaultStations, defaultArchives, defaultVariables), result);
    }
    return result;
}

SequenceName::Set DynamicSequenceSelection::Composite::getAllUnits() const
{
    SequenceName::Set result;
    for (const auto &c : components) {
        Util::merge(c->getAllUnits(), result);
    }
    return result;
}

bool DynamicSequenceSelection::Composite::registerInput(const SequenceName &input)
{
    if (components.empty())
        return false;
    bool result = false;
    for (auto &c : components) {
        if (c->registerInput(input))
            result = true;
    }
    return result;
}

void DynamicSequenceSelection::Composite::registerExpectedFlavorless(const SequenceName::Component &station,
                                                                     const SequenceName::Component &archive,
                                                                     const SequenceName::Component &variable)
{
    for (auto &c : components) {
        c->registerExpectedFlavorless(station, archive, variable);
    }
}

void DynamicSequenceSelection::Composite::registerExpected(const SequenceName::Component &station,
                                                           const SequenceName::Component &archive,
                                                           const SequenceName::Component &variable,
                                                           const SequenceName::Flavors &flavors)
{
    for (auto &c : components) {
        c->registerExpected(station, archive, variable, flavors);
    }
}

void DynamicSequenceSelection::Composite::registerExpected(const SequenceName::Component &station,
                                                           const SequenceName::Component &archive,
                                                           const SequenceName::Component &variable,
                                                           const std::unordered_set<
                                                                   SequenceName::Flavors> &flavors)
{
    for (auto &c : components) {
        c->registerExpected(station, archive, variable, flavors);
    }
}

DynamicSequenceSelection *DynamicSequenceSelection::Composite::clone() const
{
    auto result = new DynamicSequenceSelection::Composite;
    for (const auto &c : components) {
        result->components.emplace_back(c->clone());
    }
    return result;
}

void DynamicSequenceSelection::Composite::forceUnit(const SequenceName::Component &station,
                                                    const SequenceName::Component &archive,
                                                    const SequenceName::Component &variable)
{
    for (auto &c : components) {
        c->forceUnit(station, archive, variable);
    }
}

void DynamicSequenceSelection::Composite::forceUnit(const SequenceName::Component &station,
                                                    const SequenceName::Component &archive,
                                                    const SequenceName::Component &variable,
                                                    const SequenceName::Flavors &flavors)
{
    for (auto &c : components) {
        c->forceUnit(station, archive, variable, flavors);
    }
}

void DynamicSequenceSelection::Composite::add(DynamicSequenceSelection *a)
{ components.emplace_back(a); }

void DynamicSequenceSelection::Composite::add(std::unique_ptr<DynamicSequenceSelection> &&a)
{ components.emplace_back(std::move(a)); }

DynamicSequenceSelection::Composite::Composite(QDataStream &stream) : components()
{
    Deserialize::container(stream, components, [&stream] {
        DynamicSequenceSelection *a;
        stream >> a;
        return std::unique_ptr<DynamicSequenceSelection>(a);
    });
}

void DynamicSequenceSelection::Composite::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(TO_COMPOSITE);
    Serialize::container(stream, components,
                         [&stream](const std::unique_ptr<DynamicSequenceSelection> &a) {
                             stream << a.get();
                         });
}

void DynamicSequenceSelection::Composite::printLog(QDebug &stream) const
{
    stream << "DynamicSequenceSelection::Composite(";
    bool first = true;
    for (const auto &add : components) {
        if (!first)
            stream << ",";
        first = false;
        stream << add.get();
    }
    stream << ")";
}

DynamicSequenceSelection::Single::Single(const SequenceName &unit) : selected(unit), set()
{ set.insert(selected); }

DynamicSequenceSelection::Single::Single(SequenceName &&unit) : selected(std::move(unit)), set()
{ set.insert(selected); }

DynamicSequenceSelection::Single::~Single() = default;

const SequenceName::Set &DynamicSequenceSelection::Single::get(double, double)
{ return set; }

const SequenceName::Set &DynamicSequenceSelection::Single::get(double)
{ return set; }

const SequenceName::Set &DynamicSequenceSelection::Single::getConst(double, double) const
{ return set; }

const SequenceName::Set &DynamicSequenceSelection::Single::getConst(double) const
{ return set; }

SequenceName::Set DynamicSequenceSelection::Single::getAllUnits() const
{ return set; }

bool DynamicSequenceSelection::Single::matches(double, double, const SequenceName &name)
{ return selected == name; }

bool DynamicSequenceSelection::Single::matchesConst(double, double, const SequenceName &name) const
{ return selected == name; }

bool DynamicSequenceSelection::Single::matches(double, const SequenceName &name)
{ return selected == name; }

bool DynamicSequenceSelection::Single::matchesConst(double, const SequenceName &name) const
{ return selected == name; }


Archive::Selection::List DynamicSequenceSelection::Single::toArchiveSelections(const Archive::Selection::Match &,
                                                                               const Archive::Selection::Match &,
                                                                               const Archive::Selection::Match &) const
{ return Archive::Selection::List{Archive::Selection(selected)}; }

bool DynamicSequenceSelection::Single::registerInput(const SequenceName &input)
{ return input == selected; }

DynamicSequenceSelection *DynamicSequenceSelection::Single::clone() const
{ return new DynamicSequenceSelection::Single(selected); }

void DynamicSequenceSelection::Single::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(TO_ALWAYSSINGLE) << selected;
}

DynamicSequenceSelection::Single::Single(QDataStream &stream)
{
    stream >> selected;
    set.emplace(selected);
}

void DynamicSequenceSelection::Single::printLog(QDebug &stream) const
{ stream << "DynamicSequenceSelection::Single(" << selected << ")"; }

void DynamicSequenceSelection::Single::forceUnit(const SequenceName::Component &station,
                                                 const SequenceName::Component &archive,
                                                 const SequenceName::Component &variable)
{
    if (!station.empty())
        selected.setStation(station);
    if (!archive.empty())
        selected.setArchive(archive);
    if (!variable.empty())
        selected.setVariable(variable);
    set.clear();
    set.emplace(selected);
}

void DynamicSequenceSelection::Single::forceUnit(const SequenceName::Component &station,
                                                 const SequenceName::Component &archive,
                                                 const SequenceName::Component &variable,
                                                 const SequenceName::Flavors &flavors)
{
    if (!station.empty())
        selected.setStation(station);
    if (!archive.empty())
        selected.setArchive(archive);
    if (!variable.empty())
        selected.setVariable(variable);
    selected.setFlavors(flavors);
    set.clear();
    set.emplace(selected);
}

DynamicSequenceSelection::Basic::Basic() = default;

DynamicSequenceSelection::Basic::~Basic() = default;

DynamicSequenceSelection::Basic::Basic(const SequenceName &unit)
{ units.emplace(unit); }

DynamicSequenceSelection::Basic::Basic(const QList<SequenceName> &setUnits)
{
    Util::merge(setUnits, units);
}

DynamicSequenceSelection::Basic::Basic(const SequenceName::Set &setUnits) : units(setUnits)
{ }

DynamicSequenceSelection::Basic::Basic(SequenceName::Set &&setUnits) : units(std::move(setUnits))
{ }

const SequenceName::Set &DynamicSequenceSelection::Basic::get(double, double)
{ return units; }

const SequenceName::Set &DynamicSequenceSelection::Basic::get(double)
{ return units; }

const SequenceName::Set &DynamicSequenceSelection::Basic::getConst(double, double) const
{ return units; }

const SequenceName::Set &DynamicSequenceSelection::Basic::getConst(double) const
{ return units; }

bool DynamicSequenceSelection::Basic::matches(double, double, const SequenceName &name)
{ return units.count(name) != 0; }

bool DynamicSequenceSelection::Basic::matchesConst(double, double, const SequenceName &name) const
{ return units.count(name) != 0; }

bool DynamicSequenceSelection::Basic::matches(double, const SequenceName &name)
{ return units.count(name) != 0; }

bool DynamicSequenceSelection::Basic::matchesConst(double, const SequenceName &name) const
{ return units.count(name) != 0; }

Archive::Selection::List DynamicSequenceSelection::Basic::toArchiveSelections(const Archive::Selection::Match &,
                                                                              const Archive::Selection::Match &,
                                                                              const Archive::Selection::Match &) const
{
    Archive::Selection::List result;
    for (const auto &add : units) {
        result.emplace_back(add);
    }
    return result;
}

bool DynamicSequenceSelection::Basic::registerInput(const SequenceName &input)
{ return units.count(input) != 0; }

SequenceName::Set DynamicSequenceSelection::Basic::getAllUnits() const
{ return units; }

DynamicSequenceSelection *DynamicSequenceSelection::Basic::clone() const
{ return new DynamicSequenceSelection::Basic(units); }

DynamicSequenceSelection::Basic::Basic(QDataStream &stream)
{ stream >> units; }

void DynamicSequenceSelection::Basic::serialize(QDataStream &stream) const
{
    auto t = static_cast<quint8>(TO_ALWAYS);
    stream << t << units;
}

void DynamicSequenceSelection::Basic::printLog(QDebug &stream) const
{ stream << "DynamicSequenceSelection::Basic(" << units << ")"; }

void DynamicSequenceSelection::Basic::forceUnit(const SequenceName::Component &station,
                                                const SequenceName::Component &archive,
                                                const SequenceName::Component &variable)
{
    SequenceName::Set old = std::move(units);
    units.clear();
    for (auto modified : old) {
        if (!station.empty())
            modified.setStation(station);
        if (!archive.empty())
            modified.setArchive(archive);
        if (!variable.empty())
            modified.setVariable(variable);
        units.emplace(std::move(modified));
    }
}

void DynamicSequenceSelection::Basic::forceUnit(const SequenceName::Component &station,
                                                const SequenceName::Component &archive,
                                                const SequenceName::Component &variable,
                                                const SequenceName::Flavors &flavors)
{
    SequenceName::Set old = std::move(units);
    units.clear();
    for (auto modified : old) {
        if (!station.empty())
            modified.setStation(station);
        if (!archive.empty())
            modified.setArchive(archive);
        if (!variable.empty())
            modified.setVariable(variable);
        modified.setFlavors(flavors);
        units.emplace(std::move(modified));
    }
}


DynamicSequenceSelection::Match::Match() = default;

DynamicSequenceSelection::Match::~Match() = default;

DynamicSequenceSelection::Match::Match(const SequenceMatch::Element::Pattern &station,
                                       const SequenceMatch::Element::Pattern &archive,
                                       const SequenceMatch::Element::Pattern &variable,
                                       const SequenceName::Flavors &exactFlavors) : matchers(), registered()
{
    matchers.emplace_back(station, archive, variable, exactFlavors);
    auto check = matchers.back().reduce();
    if (check.isValid())
        registered.insert(std::move(check));
}

DynamicSequenceSelection::Match::Match(const SequenceMatch::Element::Pattern &station,
                                       const SequenceMatch::Element::Pattern &archive,
                                       const SequenceMatch::Element::Pattern &variable,
                                       const SequenceMatch::Element::PatternList &hasFlavors,
                                       const SequenceMatch::Element::PatternList &lacksFlavors)
        : matchers(), registered()
{
    matchers.emplace_back(station, archive, variable, hasFlavors, lacksFlavors);
}

DynamicSequenceSelection::Match::Match(const QList<SequenceMatch::Element> &setOperateMatchers) : matchers(
        setOperateMatchers.begin(), setOperateMatchers.end()), registered()
{
    for (const auto &m : matchers) {
        auto check = m.reduce();
        if (check.isValid())
            registered.insert(std::move(check));
    }
}

DynamicSequenceSelection::Match::Match(const std::vector<SequenceMatch::Element> &setOperateMatchers)
        : matchers(setOperateMatchers), registered()
{
    for (const auto &m : matchers) {
        auto check = m.reduce();
        if (check.isValid())
            registered.insert(std::move(check));
    }
}

DynamicSequenceSelection::Match::Match(const DynamicSequenceSelection::Match &other) = default;

const SequenceName::Set &DynamicSequenceSelection::Match::get(double, double)
{ return registered; }

const SequenceName::Set &DynamicSequenceSelection::Match::get(double)
{ return registered; }

const SequenceName::Set &DynamicSequenceSelection::Match::getConst(double, double) const
{ return registered; }

const SequenceName::Set &DynamicSequenceSelection::Match::getConst(double) const
{ return registered; }

bool DynamicSequenceSelection::Match::matches(double, double, const SequenceName &name)
{
    for (const auto &m : matchers) {
        if (!m.matches(name))
            continue;
        return true;
    }
    return false;
}

bool DynamicSequenceSelection::Match::matchesConst(double, double, const SequenceName &name) const
{
    for (const auto &m : matchers) {
        if (!m.matches(name))
            continue;
        return true;
    }
    return false;
}

bool DynamicSequenceSelection::Match::matches(double, const SequenceName &name)
{
    for (const auto &m : matchers) {
        if (!m.matches(name))
            continue;
        return true;
    }
    return false;
}

bool DynamicSequenceSelection::Match::matchesConst(double, const SequenceName &name) const
{
    for (const auto &m : matchers) {
        if (!m.matches(name))
            continue;
        return true;
    }
    return false;
}

Archive::Selection::List DynamicSequenceSelection::Match::toArchiveSelections(const Archive::Selection::Match &defaultStations,
                                                                              const Archive::Selection::Match &defaultArchives,
                                                                              const Archive::Selection::Match &defaultVariables) const
{
    Archive::Selection::List result;
    for (const auto &m : matchers) {
        result.emplace_back(m.toArchiveSelection(defaultStations, defaultArchives, defaultVariables));
    }
    return result;
}

bool DynamicSequenceSelection::Match::registerInput(const SequenceName &input)
{
    for (const auto &m : matchers) {
        if (!m.matches(input))
            continue;
        registered.insert(input);
        return true;
    }
    return false;
}

void DynamicSequenceSelection::Match::registerExpectedFlavorless(const SequenceName::Component &station,
                                                                 const SequenceName::Component &archive,
                                                                 const SequenceName::Component &variable)
{
    for (const auto &m : matchers) {
        SequenceName unit(station, archive, variable);
        if (!m.fill(unit, false))
            continue;
        registered.insert(std::move(unit));
    }
}

void DynamicSequenceSelection::Match::registerExpected(const SequenceName::Component &station,
                                                       const SequenceName::Component &archive,
                                                       const SequenceName::Component &variable,
                                                       const SequenceName::Flavors &flavors)
{
    for (const auto &m : matchers) {
        SequenceName unit(station, archive, variable, flavors);
        if (!m.fill(unit))
            continue;
        registered.insert(std::move(unit));
    }
}

void DynamicSequenceSelection::Match::registerExpected(const SequenceName::Component &station,
                                                       const SequenceName::Component &archive,
                                                       const SequenceName::Component &variable,
                                                       const std::unordered_set<
                                                               SequenceName::Flavors> &flavors)
{
    for (const auto &m : matchers) {
        for (const auto &f : flavors) {
            SequenceName unit(station, archive, variable, f);
            if (!m.fill(unit))
                continue;
            registered.insert(std::move(unit));
        }
    }
}

SequenceName::Set DynamicSequenceSelection::Match::getAllUnits() const
{ return registered; }

DynamicSequenceSelection *DynamicSequenceSelection::Match::clone() const
{ return new DynamicSequenceSelection::Match(*this); }

DynamicSequenceSelection::Match::Match(QDataStream &stream)
{ stream >> matchers >> registered; }

void DynamicSequenceSelection::Match::serialize(QDataStream &stream) const
{
    auto t = static_cast<quint8>(TO_ALWAYSREGEX);
    stream << t << matchers << registered;
}

void DynamicSequenceSelection::Match::printLog(QDebug &stream) const
{
    stream << "DynamicSequenceSelection::Match(matchers=[";
    bool first = true;
    for (const auto &m : matchers) {
        if (!first)
            stream << ",";
        first = false;
        stream << m;
    }
    stream << "], registered=" << registered << ")";
}

void DynamicSequenceSelection::Match::forceUnit(const SequenceName::Component &station,
                                                const SequenceName::Component &archive,
                                                const SequenceName::Component &variable)
{
    for (auto &m : matchers) {
        m.setMatch(SequenceName(station, archive, variable), false);
    }
    SequenceName::Set old = std::move(registered);
    registered.clear();
    for (auto modified : old) {
        if (!station.empty()) {
            modified.setStation(station);
        }
        if (!archive.empty()) {
            modified.setArchive(archive);
        }
        if (!variable.empty()) {
            modified.setVariable(variable);
        }
        registered.insert(std::move(modified));
    }
}

void DynamicSequenceSelection::Match::forceUnit(const SequenceName::Component &station,
                                                const SequenceName::Component &archive,
                                                const SequenceName::Component &variable,
                                                const SequenceName::Flavors &flavors)
{
    for (auto &m : matchers) {
        m.setMatch(SequenceName(station, archive, variable, flavors));
    }
    SequenceName::Set old = std::move(registered);
    registered.clear();
    for (auto modified : old) {
        if (!station.empty()) {
            modified.setStation(station);
        }
        if (!archive.empty()) {
            modified.setArchive(archive);
        }
        if (!variable.empty()) {
            modified.setVariable(variable);
        }
        modified.setFlavors(flavors);
        registered.insert(std::move(modified));
    }
}


DynamicSequenceSelection::Variable::OperateSegment::OperateSegment()
        : matchers(), registered(), start(FP::undefined()), end(FP::undefined())
{ }

DynamicSequenceSelection::Variable::OperateSegment::OperateSegment(const OperateSegment &other) = default;

DynamicSequenceSelection::Variable::OperateSegment &DynamicSequenceSelection::Variable::OperateSegment::operator=(
        const OperateSegment &other) = default;

DynamicSequenceSelection::Variable::OperateSegment::OperateSegment(OperateSegment &&other) = default;

DynamicSequenceSelection::Variable::OperateSegment &DynamicSequenceSelection::Variable::OperateSegment::operator=(
        OperateSegment &&other) = default;

DynamicSequenceSelection::Variable::OperateSegment::OperateSegment(const OperateSegment &other,
                                                                   double setStart,
                                                                   double setEnd) : matchers(
        other.matchers), registered(other.registered), start(setStart), end(setEnd)
{ }

DynamicSequenceSelection::Variable::OperateSegment::OperateSegment(const OperateSegment &under,
                                                                   const OperateSegment &over,
                                                                   double setStart,
                                                                   double setEnd) : matchers(
        over.matchers), registered(over.registered), start(setStart), end(setEnd)
{ Q_UNUSED(under); }

DynamicSequenceSelection::Variable::OperateSegment::OperateSegment(const QString &station,
                                                                   const QString &archive,
                                                                   const QString &variable,
                                                                   const SequenceName::Flavors &flavors,
                                                                   double setStart,
                                                                   double setEnd)
        : matchers(), registered(), start(setStart), end(setEnd)
{
    matchers.emplace_back(station, archive, variable, flavors);
    auto check = matchers.back().reduce();
    if (check.isValid())
        registered.insert(std::move(check));
}

DynamicSequenceSelection::Variable::OperateSegment::OperateSegment(const SequenceMatch::Element::Pattern &station,
                                                                   const SequenceMatch::Element::Pattern &archive,
                                                                   const SequenceMatch::Element::Pattern &variable,
                                                                   const SequenceMatch::Element::PatternList &hasFlavors,
                                                                   const SequenceMatch::Element::PatternList &lacksFlavors,
                                                                   double setStart,
                                                                   double setEnd)
        : matchers(), registered(), start(setStart), end(setEnd)
{
    matchers.emplace_back(station, archive, variable, hasFlavors, lacksFlavors);
}

DynamicSequenceSelection::Variable::OperateSegment::OperateSegment(const QList<
        SequenceMatch::Element> &setMatchers, double setStart, double setEnd) : matchers(
        setMatchers.begin(), setMatchers.end()), registered(), start(setStart), end(setEnd)
{
    for (const auto &m : matchers) {
        auto check = m.reduce();
        if (check.isValid())
            registered.insert(std::move(check));
    }
}

DynamicSequenceSelection::Variable::OperateSegment::OperateSegment(QDataStream &stream)
{ stream >> start >> end >> matchers >> registered; }

void DynamicSequenceSelection::Variable::OperateSegment::serialize(QDataStream &stream) const
{ stream << start << end << matchers << registered; }

bool DynamicSequenceSelection::Variable::OperateSegment::registerInput(const SequenceName &input)
{
    for (const auto &m : matchers) {
        if (!m.matches(input))
            continue;
        registered.insert(input);
        return true;
    }
    return false;
}

bool DynamicSequenceSelection::Variable::OperateSegment::matches(const SequenceName &input) const
{
    for (const auto &m : matchers) {
        if (!m.matches(input))
            continue;
        return true;
    }
    return false;
}

void DynamicSequenceSelection::Variable::OperateSegment::registerExpectedFlavorless(const SequenceName::Component &station,
                                                                                    const SequenceName::Component &archive,
                                                                                    const SequenceName::Component &variable)
{
    for (const auto &m : matchers) {
        SequenceName unit(station, archive, variable);
        if (!m.fill(unit, false))
            continue;
        registered.insert(std::move(unit));
    }
}

void DynamicSequenceSelection::Variable::OperateSegment::registerExpected(const SequenceName::Component &station,
                                                                          const SequenceName::Component &archive,
                                                                          const SequenceName::Component &variable,
                                                                          const SequenceName::Flavors &flavors)
{
    for (const auto &m : matchers) {
        SequenceName unit(station, archive, variable, flavors);
        if (!m.fill(unit))
            continue;
        registered.insert(std::move(unit));
    }
}

void DynamicSequenceSelection::Variable::OperateSegment::registerExpected(const SequenceName::Component &station,
                                                                          const SequenceName::Component &archive,
                                                                          const SequenceName::Component &variable,
                                                                          const std::unordered_set<
                                                                                  SequenceName::Flavors> &flavors)
{
    for (const auto &m : matchers) {
        for (const auto &f : flavors) {
            SequenceName unit(station, archive, variable, f);
            if (!m.fill(unit))
                continue;
            registered.insert(std::move(unit));
        }
    }
}

void DynamicSequenceSelection::Variable::OperateSegment::forceUnit(const SequenceName::Component &station,
                                                                   const SequenceName::Component &archive,
                                                                   const SequenceName::Component &variable)
{
    for (auto &m : matchers) {
        m.setMatch(SequenceName(station, archive, variable), false);
    }
    SequenceName::Set old = std::move(registered);
    registered.clear();
    for (auto modified : old) {
        if (!station.empty()) {
            modified.setStation(station);
        }
        if (!archive.empty()) {
            modified.setArchive(archive);
        }
        if (!variable.empty()) {
            modified.setVariable(variable);
        }
        registered.insert(std::move(modified));
    }
}

void DynamicSequenceSelection::Variable::OperateSegment::forceUnit(const SequenceName::Component &station,
                                                                   const SequenceName::Component &archive,
                                                                   const SequenceName::Component &variable,
                                                                   const SequenceName::Flavors &flavors)
{
    for (auto &m : matchers) {
        m.setMatch(SequenceName(station, archive, variable, flavors));
    }
    SequenceName::Set old = std::move(registered);
    registered.clear();
    for (auto modified : old) {
        if (!station.empty()) {
            modified.setStation(station);
        }
        if (!archive.empty()) {
            modified.setArchive(archive);
        }
        if (!variable.empty()) {
            modified.setVariable(variable);
        }
        modified.setFlavors(flavors);
        registered.insert(std::move(modified));
    }
}

Archive::Selection::List DynamicSequenceSelection::Variable::OperateSegment::toArchiveSelections(
        const Archive::Selection::Match &defaultStations,
        const Archive::Selection::Match &defaultArchives,
        const Archive::Selection::Match &defaultVariables) const
{
    Archive::Selection::List result;
    for (const auto &m : matchers) {
        auto add = m.toArchiveSelection(defaultStations, defaultArchives, defaultVariables);
        add.start = start;
        add.end = end;
        result.emplace_back(std::move(add));
    }
    return result;
}

DynamicSequenceSelection::Variable::Variable() = default;

DynamicSequenceSelection::Variable::~Variable() = default;

DynamicSequenceSelection::Variable::Variable(const DynamicSequenceSelection::Variable &other) = default;

DynamicSequenceSelection::Variable &DynamicSequenceSelection::Variable::operator=(const DynamicSequenceSelection::Variable &other) = default;

DynamicSequenceSelection::Variable::Variable(DynamicSequenceSelection::Variable &&other) = default;

DynamicSequenceSelection::Variable &DynamicSequenceSelection::Variable::operator=(
        DynamicSequenceSelection::Variable &&other) = default;

const SequenceName::Set &DynamicSequenceSelection::Variable::get(double start, double end)
{
    if (!Range::intersectShift(segments, start, end))
        return invalidSet;
    if (Range::compareStartEnd(segments.front().start, end) >= 0)
        return segments.front().registered;

    static thread_local SequenceName::Set out;
    out.clear();
    for (const auto &s : segments) {
        if (Range::compareStartEnd(s.start, end) >= 0)
            break;
        Util::merge(s.registered, out);
    }
    return out;
}

const SequenceName::Set &DynamicSequenceSelection::Variable::get(double time)
{
    if (!Range::intersectShift(segments, time))
        return invalidSet;
    return segments.front().registered;
}

const SequenceName::Set &DynamicSequenceSelection::Variable::getConst(double start,
                                                                      double end) const
{
    auto i = Range::findIntersecting(segments, start, end);
    auto last = segments.end();
    if (i == last)
        return invalidSet;
    static thread_local SequenceName::Set out;
    out.clear();
    for (; i != last; ++i) {
        if (Range::compareStartEnd(i->start, end) >= 0)
            break;
        Util::merge(i->registered, out);
    }
    return out;
}

const SequenceName::Set &DynamicSequenceSelection::Variable::getConst(double time) const
{
    auto i = Range::findIntersecting(segments, time);
    if (i == segments.end())
        return invalidSet;
    return i->registered;
}

bool DynamicSequenceSelection::Variable::matches(double start, double end, const SequenceName &name)
{
    if (!Range::intersectShift(segments, start, end))
        return false;
    for (const auto &s : segments) {
        if (Range::compareStartEnd(s.start, end) >= 0)
            break;
        if (s.matches(name))
            return true;
    }
    return false;
}

bool DynamicSequenceSelection::Variable::matchesConst(double start,
                                                      double end,
                                                      const SequenceName &name) const
{
    auto i = Range::findIntersecting(segments, start, end);
    auto last = segments.end();
    if (i == last)
        return false;
    for (; i != last; ++i) {
        if (Range::compareStartEnd(i->start, end) >= 0)
            break;
        if (i->matches(name))
            return true;
    }
    return false;
}

bool DynamicSequenceSelection::Variable::matches(double time, const SequenceName &name)
{
    if (!Range::intersectShift(segments, time))
        return false;
    return segments.front().matches(name);
}

bool DynamicSequenceSelection::Variable::matchesConst(double time, const SequenceName &name) const
{
    auto i = Range::findIntersecting(segments, time);
    if (i == segments.end())
        return false;
    return i->matches(name);
}

bool DynamicSequenceSelection::Variable::registerInput(const SequenceName &input)
{
    bool hit = false;
    for (auto &s : segments) {
        if (s.registerInput(input))
            hit = true;
    }
    return hit;
}

void DynamicSequenceSelection::Variable::registerExpectedFlavorless(const SequenceName::Component &station,
                                                                    const SequenceName::Component &archive,
                                                                    const SequenceName::Component &variable)
{
    for (auto &s : segments) {
        s.registerExpectedFlavorless(station, archive, variable);
    }
}

void DynamicSequenceSelection::Variable::registerExpected(const SequenceName::Component &station,
                                                          const SequenceName::Component &archive,
                                                          const SequenceName::Component &variable,
                                                          const SequenceName::Flavors&flavors)
{
    for (auto &s : segments) {
        s.registerExpected(station, archive, variable, flavors);
    }
}

void DynamicSequenceSelection::Variable::registerExpected(const SequenceName::Component &station,
                                                          const SequenceName::Component &archive,
                                                          const SequenceName::Component &variable,
                                                          const std::unordered_set<
                                                                  SequenceName::Flavors> &flavors)
{
    for (auto &s : segments) {
        s.registerExpected(station, archive, variable, flavors);
    }
}

Archive::Selection::List DynamicSequenceSelection::Variable::toArchiveSelections(const Archive::Selection::Match &defaultStations,
                                                                                 const Archive::Selection::Match &defaultArchives,
                                                                                 const Archive::Selection::Match &defaultVariables) const
{
    Archive::Selection::List result;
    for (const auto &s : segments) {
        Util::append(s.toArchiveSelections(defaultStations, defaultArchives, defaultVariables), result);
    }
    return result;
}

SequenceName::Set DynamicSequenceSelection::Variable::getAllUnits() const
{
    if (segments.empty())
        return SequenceName::Set();
    if (segments.size() == 1)
        return segments.front().registered;
    SequenceName::Set out;
    for (const auto &s : segments) {
        Util::merge(s.registered, out);
    }
    return out;
}

QSet<double> DynamicSequenceSelection::Variable::getChangedPoints() const
{
    QSet<double> out;
    for (const auto &s : segments) {
        if (FP::defined(s.start))
            out.insert(s.start);
        if (FP::defined(s.end))
            out.insert(s.end);
    }
    return out;
}

DynamicSequenceSelection *DynamicSequenceSelection::Variable::clone() const
{ return new DynamicSequenceSelection::Variable(*this); }

void DynamicSequenceSelection::Variable::forceUnit(const SequenceName::Component &station,
                                                   const SequenceName::Component &archive,
                                                   const SequenceName::Component &variable)
{
    for (auto &s : segments) {
        s.forceUnit(station, archive, variable);
    }
}

void DynamicSequenceSelection::Variable::forceUnit(const SequenceName::Component &station,
                                                   const SequenceName::Component &archive,
                                                   const SequenceName::Component &variable,
                                                   const SequenceName::Flavors &flavors)
{
    for (auto &s : segments) {
        s.forceUnit(station, archive, variable, flavors);
    }
}

void DynamicSequenceSelection::Variable::clear()
{ segments.clear(); }

void DynamicSequenceSelection::Variable::set(const SequenceMatch::Element::Pattern &station,
                                             const SequenceMatch::Element::Pattern &archive,
                                             const SequenceMatch::Element::Pattern &variable,
                                             const SequenceMatch::Element::PatternList &hasFlavors,
                                             const SequenceMatch::Element::PatternList &lacksFlavors)
{
    segments.clear();
    segments.emplace_back(station, archive, variable, hasFlavors, lacksFlavors);
}

void DynamicSequenceSelection::Variable::set(const SequenceMatch::Element::Pattern &station,
                                             const SequenceMatch::Element::Pattern &archive,
                                             const SequenceMatch::Element::Pattern &variable,
                                             const SequenceName::Flavors &flavors)
{
    segments.clear();
    segments.emplace_back(station, archive, variable, flavors);
}

void DynamicSequenceSelection::Variable::set(const QList<SequenceMatch::Element> &matchers)
{
    segments.clear();
    segments.emplace_back(matchers);
}

void DynamicSequenceSelection::Variable::set(double start,
                                             double end,
                                             const SequenceMatch::Element::Pattern &station,
                                             const SequenceMatch::Element::Pattern &archive,
                                             const SequenceMatch::Element::Pattern &variable,
                                             const SequenceMatch::Element::PatternList &hasFlavors,
                                             const SequenceMatch::Element::PatternList &lacksFlavors)
{
    Range::overlayFragmenting(segments,
                              OperateSegment(station, archive, variable, hasFlavors, lacksFlavors,
                                             start, end));
}

void DynamicSequenceSelection::Variable::set(double start,
                                             double end,
                                             const SequenceMatch::Element::Pattern &station,
                                             const SequenceMatch::Element::Pattern &archive,
                                             const SequenceMatch::Element::Pattern &variable,
                                             const SequenceName::Flavors &flavors)
{
    Range::overlayFragmenting(segments,
                              OperateSegment(station, archive, variable, flavors, start, end));
}

void DynamicSequenceSelection::Variable::set(double start,
                                             double end,
                                             const QList<SequenceMatch::Element> &matchers)
{
    Range::overlayFragmenting(segments, OperateSegment(matchers, start, end));
}

void DynamicSequenceSelection::Variable::set(double start, double end, const Variant::Read &v)
{
    QList<SequenceMatch::Element> sl;
    auto type = v.getType();
    if (type == Variant::Type::Array) {
        for (const auto &add : v.toArray()) {
            sl.push_back(SequenceMatch::Element(add));
        }
    } else {
        sl.push_back(SequenceMatch::Element(v));
    }
    set(start, end, sl);
}

void DynamicSequenceSelection::Variable::set(const Variant::Read &v)
{
    segments.clear();
    set(FP::undefined(), FP::undefined(), v);
}


DynamicSequenceSelection::Variable::Variable(QDataStream &stream) : segments()
{
    Deserialize::container(stream, segments, [&stream] {
        return OperateSegment(stream);
    });
}

void DynamicSequenceSelection::Variable::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(TO_TIMEREGEX);
    Serialize::container(stream, segments, [&stream](const OperateSegment &seg) {
        seg.serialize(stream);
    });
}

void DynamicSequenceSelection::Variable::printLog(QDebug &stream) const
{
    stream << "DynamicSequenceSelection::Variable(";
    bool firstS = true;
    for (const auto &s : segments) {
        if (!firstS)
            stream << ",";
        firstS = false;

        stream << "[" << Logging::range(s.start, s.end) << ",matchers=(" << s.matchers << ")"
               << "),registered=(" << s.registered << ")]";
    }
    stream << ")";
}

DynamicSequenceSelectionOption::DynamicSequenceSelectionOption(const QString &argumentName,
                                                               const QString &displayName,
                                                               const QString &description,
                                                               const QString &defaultBehavior,
                                                               int sortPriority)
        : ComponentOptionBase(argumentName, displayName, description, defaultBehavior, sortPriority)
{ }

DynamicSequenceSelectionOption::DynamicSequenceSelectionOption(const DynamicSequenceSelectionOption &other) = default;

DynamicSequenceSelectionOption::~DynamicSequenceSelectionOption() = default;

void DynamicSequenceSelectionOption::reset()
{
    optionSet = false;
    operate.clear();
}

ComponentOptionBase *DynamicSequenceSelectionOption::clone() const
{ return new DynamicSequenceSelectionOption(*this); }

void DynamicSequenceSelectionOption::clear()
{
    optionSet = true;
    operate.clear();
}

void DynamicSequenceSelectionOption::set(const SequenceMatch::Element::Pattern &station,
                                         const SequenceMatch::Element::Pattern &archive,
                                         const SequenceMatch::Element::Pattern &variable,
                                         const SequenceMatch::Element::PatternList &hasFlavors,
                                         const SequenceMatch::Element::PatternList &lacksFlavors)
{
    optionSet = true;
    operate.set(station, archive, variable, hasFlavors, lacksFlavors);
}

void DynamicSequenceSelectionOption::set(const SequenceMatch::Element::Pattern &station,
                                         const SequenceMatch::Element::Pattern &archive,
                                         const SequenceMatch::Element::Pattern &variable,
                                         const SequenceName::Flavors &flavors)
{
    optionSet = true;
    operate.set(station, archive, variable, flavors);
}

/**
 * Set the operation to always match the given set of matchers.
 * 
 * @param matchers  the matchers to use
 */
void DynamicSequenceSelectionOption::set(const QList<SequenceMatch::Element> &matchers)
{
    optionSet = true;
    operate.set(matchers);
}

/**
 * Set the operation to match the given station, archive, variable and
 * flavors between the given times.  This replaces whatever was matched 
 * before in the time range.
 * 
 * @param start         the start time of the new match
 * @param end           the end time of the new match
 * @param station       the station regular expression
 * @param archive       the archive regular expression
 * @param variable      the variable regular expression
 * @param hasFlavors    the list of flavor regular expressions to require
 * @param lacksFlavors  the list of flavor regular expressions to reject
 */
void DynamicSequenceSelectionOption::set(double start,
                                         double end,
                                         const SequenceMatch::Element::Pattern &station,
                                         const SequenceMatch::Element::Pattern &archive,
                                         const SequenceMatch::Element::Pattern &variable,
                                         const SequenceMatch::Element::PatternList &hasFlavors,
                                         const SequenceMatch::Element::PatternList &lacksFlavors)
{
    optionSet = true;
    operate.set(start, end, station, archive, variable, hasFlavors, lacksFlavors);
}

void DynamicSequenceSelectionOption::set(double start,
                                         double end,
                                         const SequenceMatch::Element::Pattern &station,
                                         const SequenceMatch::Element::Pattern &archive,
                                         const SequenceMatch::Element::Pattern &variable,
                                         const SequenceName::Flavors &flavors)
{
    optionSet = true;
    operate.set(start, end, station, archive, variable, flavors);
}

void DynamicSequenceSelectionOption::set(double start,
                                         double end,
                                         const QList<SequenceMatch::Element> &matchers)
{
    optionSet = true;
    operate.set(start, end, matchers);
}

void DynamicSequenceSelectionOption::set(double start, double end, const Variant::Read &value)
{
    optionSet = true;
    operate.set(start, end, value);
}

void DynamicSequenceSelectionOption::set(const Variant::Read &value)
{
    optionSet = true;
    operate.set(value);
}

DynamicSequenceSelection *DynamicSequenceSelectionOption::getOperator() const
{
    if (operate.segments.empty())
        return new DynamicSequenceSelection::None();
    if (operate.segments.size() == 1) {
        const auto &s = operate.segments.front();
        if (!FP::defined(s.start) && !FP::defined(s.end)) {
            auto i = s.matchers.cbegin();
            auto end = s.matchers.cend();
            if (i == end)
                return new DynamicSequenceSelection::None();
            bool allNever = true;
            for (; i != end; ++i) {
                if (allNever) {
                    if (i->neverMatches())
                        continue;
                    allNever = false;
                }
                if (!i->reduce().isValid())
                    return new DynamicSequenceSelection::Match(s.matchers);
            }
            if (allNever)
                return new DynamicSequenceSelection::None();
            if (s.registered.size() == 1)
                return new DynamicSequenceSelection::Single(*s.registered.cbegin());
            return new DynamicSequenceSelection::Basic(s.registered);
        }
    }
    return operate.clone();
}

std::vector<
        DynamicSequenceSelectionOption::Segment> DynamicSequenceSelectionOption::getSegments() const
{
    std::vector<Segment> result;
    if (operate.segments.empty())
        return result;
    if (operate.segments.size() == 1) {
        const auto &s = operate.segments.front();
        if (!FP::defined(s.start) && !FP::defined(s.end)) {
            auto i = s.matchers.cbegin();
            auto end = s.matchers.cend();
            if (i == end)
                return result;
            bool allNever = true;
            for (; i != end; ++i) {
                if (i->neverMatches())
                    continue;
                allNever = false;
                break;
            }
            if (allNever)
                return result;
        }
    }
    for (const auto &s : operate.segments) {
        result.emplace_back(s.start, s.end, SequenceMatch::OrderedLookup(s.matchers));
    }
    return result;
}

bool DynamicSequenceSelectionOption::parseFromValue(const Variant::Read &value)
{
    if (value.getType() == Variant::Type::Array) {
        for (const auto &v : value.toArray()) {
            set(v.hash("Start").toDouble(), v.hash("End").toDouble(), v);
        }
    } else {
        set(value);
    }
    return true;
}

DynamicSequenceSelection *DynamicSequenceSelection::Variable::reduceConfigurationSegments(const DynamicSequenceSelection::Variable &tor,
                                                                                          double start,
                                                                                          double end)
{
    if (tor.segments.empty())
        return new DynamicSequenceSelection::None();
    if (tor.segments.size() == 1) {
        auto &s = tor.segments.front();
        if (Range::compareStart(s.start, start) <= 0 && Range::compareEnd(s.end, end) >= 0) {
            auto i = s.matchers.cbegin();
            auto endS = s.matchers.cend();
            if (i == endS)
                return new DynamicSequenceSelection::None();
            bool allNever = true;
            for (; i != endS; ++i) {
                if (allNever) {
                    if (i->neverMatches())
                        continue;
                    allNever = false;
                }
                if (!i->reduce().isValid())
                    return new DynamicSequenceSelection::Match(s.matchers);
            }
            if (allNever)
                return new DynamicSequenceSelection::None();
            if (s.registered.size() == 1)
                return new DynamicSequenceSelection::Single(*s.registered.cbegin());
            return new DynamicSequenceSelection::Basic(s.registered);
        }
    }
    return tor.clone();
}

DynamicSequenceSelection *DynamicSequenceSelection::fromConfiguration(SequenceSegment::Transfer &config,
                                                                      const SequenceName &unit,
                                                                      const QString &path,
                                                                      double start,
                                                                      double end)
{
    if (config.empty()) return new DynamicSequenceSelection::None();

    DynamicSequenceSelection::Variable tor;

    auto i = Range::findLowerBound(config.begin(), config.end(), start);
    if (i == config.end()) {
        --i;
        if (Range::compareStartEnd(start, i->getEnd()) < 0) {
            tor.set(i->getStart(), i->getEnd(), i->getValue(unit).getPath(path));
        }
    } else if (i != config.begin() || Range::compareStartEnd(i->getStart(), end) < 0) {
        if (Range::compareStartEnd(start, i->getEnd()) >= 0)
            ++i;
        for (; i != config.end() && Range::compareStartEnd(i->getStart(), end) < 0; ++i) {

            auto configValue = i->getValue(unit).getPath(path);
            double st = i->getStart();
            if (configValue.exists()) {
                for (; i + 1 != config.end() &&
                        Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                        Range::compareStartEnd((i + 1)->getStart(), i->getEnd()) == 0 &&
                        (i + 1)->getValue(unit).getPath(path) == configValue; ++i) { }
            } else {
                for (; i + 1 != config.end() &&
                        Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                        !(i + 1)->getValue(unit).getPath(path).exists(); ++i) { }
            }
            tor.set(st, i->getEnd(), configValue);
        }
    }
    return DynamicSequenceSelection::Variable::reduceConfigurationSegments(tor, start, end);
}

DynamicSequenceSelection *DynamicSequenceSelection::fromConfiguration(const ValueSegment::Transfer &config,
                                                                      const QString &path,
                                                                      double start,
                                                                      double end)
{
    if (config.empty())
        return new DynamicSequenceSelection::None();

    DynamicSequenceSelection::Variable tor;

    auto i = Range::findLowerBound(config.begin(), config.end(), start);
    if (i == config.end()) {
        --i;
        if (Range::compareStartEnd(start, i->getEnd()) < 0) {
            tor.set(i->getStart(), i->getEnd(), i->value().getPath(path));
        }
    } else if (i != config.begin() || Range::compareStartEnd(i->getStart(), end) < 0) {
        if (Range::compareStartEnd(start, i->getEnd()) >= 0)
            ++i;
        for (; i != config.end() && Range::compareStartEnd(i->getStart(), end) < 0; ++i) {

            auto configValue = i->value().getPath(path);
            double st = i->getStart();
            if (configValue.exists()) {
                for (; i + 1 != config.end() &&
                        Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                        Range::compareStartEnd((i + 1)->getStart(), i->getEnd()) == 0 &&
                        (i + 1)->value().getPath(path) == configValue; ++i) { }
            } else {
                for (; i + 1 != config.end() &&
                        Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                        !(i + 1)->value().getPath(path).exists(); ++i) { }
            }
            tor.set(st, i->getEnd(), configValue);
        }
    }
    return DynamicSequenceSelection::Variable::reduceConfigurationSegments(tor, start, end);
}


DynamicSequenceSelection::~DynamicSequenceSelection() = default;

const SequenceName::Set &DynamicSequenceSelection::get(double start, double end)
{ return getConst(start, end); }

bool DynamicSequenceSelection::matches(double start, double end, const SequenceName &name)
{ return get(start, end).count(name) != 0; }

bool DynamicSequenceSelection::matchesConst(double start,
                                            double end,
                                            const SequenceName &name) const
{ return getConst(start, end).count(name) != 0; }

const SequenceName::Set &DynamicSequenceSelection::get(double time)
{ return getConst(time); }

bool DynamicSequenceSelection::matches(double time, const SequenceName &name)
{ return get(time).count(name) != 0; }

bool DynamicSequenceSelection::matchesConst(double time, const SequenceName &name) const
{ return getConst(time).count(name) != 0; }

bool DynamicSequenceSelection::registerInput(const SequenceName &)
{ return false; }

void DynamicSequenceSelection::registerExpectedFlavorless(const SequenceName::Component &,
                                                          const SequenceName::Component &,
                                                          const SequenceName::Component &)
{ }

void DynamicSequenceSelection::registerExpected(const SequenceName::Component &,
                                                const SequenceName::Component &,
                                                const SequenceName::Component &,
                                                const SequenceName::Flavors &)
{ }

void DynamicSequenceSelection::registerExpected(const SequenceName::Component &,
                                                const SequenceName::Component &,
                                                const SequenceName::Component &,
                                                const std::unordered_set<SequenceName::Flavors> &)
{ }

SequenceName::Set DynamicSequenceSelection::getAllUnits() const
{ return SequenceName::Set(); }

QSet<double> DynamicSequenceSelection::getChangedPoints() const
{ return QSet<double>(); }

void DynamicSequenceSelection::printLog(QDebug &stream) const
{ stream << "DynamicSequenceSelection(Unknown)"; }

void DynamicSequenceSelection::forceUnit(const SequenceName::Component &,
                                         const SequenceName::Component &,
                                         const SequenceName::Component &)
{ }

void DynamicSequenceSelection::forceUnit(const SequenceName::Component &,
                                         const SequenceName::Component &,
                                         const SequenceName::Component &,
                                         const SequenceName::Flavors &)
{ }

QDataStream &operator<<(QDataStream &stream, const DynamicSequenceSelection *to)
{
    if (to == NULL) {
        auto t = static_cast<quint8>(TO_NULL);
        stream << t;
        return stream;
    }
    to->serialize(stream);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, DynamicSequenceSelection *&to)
{
    quint8 tRaw;
    stream >> tRaw;
    switch (static_cast<TimeOperateType>(tRaw)) {
    case TO_NULL:
        to = NULL;
        return stream;
    case TO_NEVER:
        to = new DynamicSequenceSelection::None(stream);
        return stream;
    case TO_COMPOSITE:
        to = new DynamicSequenceSelection::Composite(stream);
        return stream;
    case TO_ALWAYSSINGLE:
        to = new DynamicSequenceSelection::Single(stream);
        return stream;
    case TO_ALWAYS:
        to = new DynamicSequenceSelection::Basic(stream);
        return stream;
    case TO_ALWAYSREGEX:
        to = new DynamicSequenceSelection::Match(stream);
        return stream;
    case TO_TIMEREGEX:
        to = new DynamicSequenceSelection::Variable(stream);
        return stream;
    }
    Q_ASSERT(false);
    to = NULL;
    return stream;
}

QDebug operator<<(QDebug stream, const DynamicSequenceSelection *to)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << reinterpret_cast<const void *>(to) << ":";
    if (!to) {
        stream << "DynamicSequenceSelection(NULL)";
    } else {
        to->printLog(stream);
    }
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const std::unique_ptr<DynamicSequenceSelection> &to)
{
    stream << to.get();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, std::unique_ptr<DynamicSequenceSelection> &to)
{
    DynamicSequenceSelection *temp = nullptr;
    stream >> temp;
    to.reset(temp);
    return stream;
}

QDebug operator<<(QDebug stream, const std::unique_ptr<DynamicSequenceSelection> &to)
{
    stream << to.get();
    return stream;
}

}
}
