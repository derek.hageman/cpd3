/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACORE_SINKMULTIPLEXER_HXX
#define CPD3DATACORE_SINKMULTIPLEXER_HXX

#include "core/first.hxx"

#include <vector>
#include <algorithm>
#include <mutex>
#include <condition_variable>
#include <QtGlobal>
#include <QDataStream>
#include <QList>
#include <QDebug>

#include "datacore.hxx"
#include "stream.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/threadpool.hxx"
#include "core/merge.hxx"

namespace CPD3 {
namespace Data {

#ifndef NDEBUG
//#define MULTIPLEXER_UNIT_TRACKING
//#define MULTIPLEXER_TAG_TRACKING
#endif

class SinkMultiplexer;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const SinkMultiplexer &mux);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, SinkMultiplexer &mux);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const SinkMultiplexer &mux);

/**
 * A multiplexer for multiple data streams that maintains distinct endpoints
 * for all inputs and allows for dynamic addition to those endpoints.
 * <br>
 * This is similar to a DataMultiplexer except for the better support for
 * dynamic endpoints and more optimization to work with threaded inputs.
 * <br>
 * Note that serialization does NOT preserve the sink structure.  That is
 * a serialize-deserialize sequence will preserve the pending values but the
 * deserialized one will have no sinks defined.  This is generally fine,
 * as the deserialize for anything using the multiplexer can just create
 * a new set of sinks.
 * <br>
 * This conforms to the specifications of a StreamSource, in that
 * it will buffer data as long as the egress is NULL (the default).
 * <br>
 * Multiplexers do NOT block their inputs based on their buffer size.  This
 * is because the usage is such that the generators are usually a single
 * fanout process connected to a multiplexer, so blocking would stop
 * the fanout and deadlock the system.  However, they can block if their
 * own output does, to allow for stall propagation to the original inputs.
 */
class CPD3DATACORE_EXPORT SinkMultiplexer : public StreamSource {
    typedef StreamMultiplexer<SequenceValue> Backend;

    std::thread thread;
    bool canStall;
    std::mutex mutex;
    std::mutex egressLock;
    std::condition_variable request;
    std::condition_variable response;
    enum class ExecutionState {
        NotStarted, Starting, Running, OutputStalled, Completed,
    } executionState;
    enum class TerminationRequest {
        None, Graceful, Immediate,
    } terminationRequest;

    StreamSink *egress;

    Backend backend;

    void run();

    inline void notifyUpdate()
    { request.notify_all(); }

    void stall(std::unique_lock<std::mutex> &lock);

    void stall(std::unique_lock<std::mutex> &lock, std::size_t bufferSize);

public:
    /**
     * An sink for a multiplexer.
     */
    class CPD3DATACORE_EXPORT Sink : public StreamSink {
        friend class SinkMultiplexer;

#ifdef MULTIPLEXER_TAG_TRACKING
        QString sinkTag;
#endif

    protected:
        SinkMultiplexer *mux;

#ifdef MULTIPLEXER_UNIT_TRACKING
        QSet<SequenceName> seenUnits;
#endif

        Sink(SinkMultiplexer *multiplexer);

        virtual void serialize(QDataStream &stream) const = 0;

        virtual void deserializeRecreate() = 0;

        virtual void describe(QDebug &stream, bool detailed = false) const = 0;

        friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const SinkMultiplexer &mux);

    public:
        virtual ~Sink();


        /**
         * Advance the sink to the given time without adding any data.  This
         * allows the multiplexer to emit data sooner.  Calling this function
         * is optional.
         *
         * @param time  the time to advance to
         */
        virtual void incomingAdvance(double time) = 0;


#ifndef NDEBUG

        virtual int uid() const = 0;

        inline int index()
        {
            std::lock_guard<std::mutex> lock(mux->mutex);
            for (int i = 0, max = (int) mux->activeSinks.size(); i < max; ++i) {
                if (mux->activeSinks[i].get() == this)
                    return i;
            }
            return -1;
        }

#endif

#ifdef MULTIPLEXER_UNIT_TRACKING

        inline QSet<SequenceName> units() const
        { return seenUnits; }

#endif

#ifdef MULTIPLEXER_TAG_TRACKING

        inline QString tag() const
        { return sinkTag; }

#endif

    };

    friend class Sink;

private:

    void completeSink(Sink *in);

    static inline void serializeSink(const Sink &sink, QDataStream &stream)
    { sink.serialize(stream); }

    class SinkSimple final : public Sink {
        Backend::Simple *backend;

        friend class SinkMultiplexer;

        SinkSimple(SinkMultiplexer *mux);

    public:

        virtual ~SinkSimple();

        void incomingData(const SequenceValue::Transfer &values) override;

        void incomingData(SequenceValue::Transfer &&values) override;

        void incomingData(const SequenceValue &value) override;

        void incomingData(SequenceValue &&value) override;

        void endData() override;

        void incomingAdvance(double time) override;

#ifndef NDEBUG

        virtual int uid() const;

#endif

    protected:

        virtual void describe(QDebug &stream, bool detailed) const;

        virtual void serialize(QDataStream &stream) const;

        virtual void deserializeRecreate();
    };

    friend class SinkSimple;

    class SinkSegmented final : public Sink {
        Backend::Segmented *backend;

        friend class SinkMultiplexer;

        SinkSegmented(SinkMultiplexer *mux);

    public:
        virtual ~SinkSegmented();

        void incomingData(const SequenceValue::Transfer &values) override;

        void incomingData(SequenceValue::Transfer &&values) override;

        void incomingData(const SequenceValue &value) override;

        void incomingData(SequenceValue &&value) override;

        void endData() override;

        void incomingAdvance(double time) override;

#ifndef NDEBUG

        virtual int uid() const;

#endif

    protected:

        virtual void describe(QDebug &stream, bool detailed) const;

        virtual void serialize(QDataStream &stream) const;

        virtual void deserializeRecreate();
    };

    friend class SinkSegmented;

    class SinkUnsorted final : public Sink {
        Backend::Unsorted *backend;

        friend class SinkMultiplexer;

        SinkUnsorted(SinkMultiplexer *mux);

    public:
        virtual ~SinkUnsorted();

        void incomingData(const SequenceValue::Transfer &values) override;

        void incomingData(SequenceValue::Transfer &&values) override;

        void incomingData(const SequenceValue &value) override;

        void incomingData(SequenceValue &&value) override;

        void endData() override;

        void incomingAdvance(double time) override;

#ifndef NDEBUG

        virtual int uid() const;

#endif

    protected:

        virtual void describe(QDebug &stream, bool detailed) const;

        virtual void serialize(QDataStream &stream) const;

        virtual void deserializeRecreate();
    };

    friend class SinkUnsorted;

    class SinkPlaceholder final : public Sink {
        Backend::Placeholder *backend;

        friend class SinkMultiplexer;

        SinkPlaceholder(SinkMultiplexer *mux);

    public:

        virtual ~SinkPlaceholder();

        void incomingData(const SequenceValue::Transfer &values) override;

        void incomingData(SequenceValue::Transfer &&values) override;

        void incomingData(const SequenceValue &value) override;

        void incomingData(SequenceValue &&value) override;

        void endData() override;

        void incomingAdvance(double time) override;

#ifndef NDEBUG

        virtual int uid() const;

#endif

    protected:

        virtual void describe(QDebug &stream, bool detailed) const;

        virtual void serialize(QDataStream &stream) const;

        virtual void deserializeRecreate();
    };

    friend class SinkPlaceholder;

    std::vector<std::unique_ptr<Sink>> activeSinks;
    std::vector<std::unique_ptr<Sink>> completedSinks;

    void deserializeRecreate();

#ifdef MULTIPLEXER_TAG_TRACKING
    QString localTag;
#endif

public:
    SinkMultiplexer(bool canStall = true);

    virtual ~SinkMultiplexer();

    void setEgress(StreamSink *egress) override;

    /**
     * Get the total number of values currently contained in the multiplexer.
     *
     * @return the total number of all values in the multiplexer
     */
    int totalBufferedValues();

    /**
     * Get the latest time tracked by the multiplexer.  This is equal the the earliest time
     * that the a new sink could accept data.
     *
     * @return the total number of all values in the multiplexer
     */
    double trackedTime();

    /**
     * Create a new sink for a stream in the multiplexer.  This new stream is
     * advanced to the earliest existing stream, if any.  That is, values may
     * not be added before the "slowest" stream.
     *
     * @return                      a new stream
     */
    Sink *createSink(
#ifdef MULTIPLEXER_TAG_TRACKING
            , const QString &tag = QString()
#endif
    );

    /**
     * Create a new sink for a stream in the multiplexer.  This new stream is
     * advanced to the earliest existing stream, if any.  That is, values may
     * not be added before the "slowest" stream.  Segmented streams require
     * that the start of each value be greater than or equal to the end of
     * the previous one or the latest advance.
     *
     * @return                      a new stream
     */
    Sink *createSegmented(
#ifdef MULTIPLEXER_TAG_TRACKING
            , const QString &tag = QString()
#endif
    );

    /**
     * Create a new sink for a stream in the multiplexer.  This new stream is
     * advanced to the earliest existing stream, if any.  That is, values may
     * not be added before the "slowest" stream.  Unsorted streams allow
     * values to be added in any order, as long as they start after or equal
     * to the latest advance.
     *
     * @return                      a new stream
     */
    Sink *createUnsorted(
#ifdef MULTIPLEXER_TAG_TRACKING
            , const QString &tag = QString()
#endif
    );

    /**
     * Create a new sink to a stream in the multiplexer.  This new stream is
     * advanced to the earliest existing stream, if any.  That is, values may
     * not be added before the "slowest" stream.  Placeholder streams only
     * accept advances.
     *
     * @return                      a new stream
     */
    Sink *createPlaceholder(
#ifdef MULTIPLEXER_TAG_TRACKING
            , const QString &tag = QString()
#endif
    );

#ifdef MULTIPLEXER_TAG_TRACKING
    static void globalTag(const QString &tag = QString());
    void setTag(const QString &tag = QString());
#endif

    /**
     * Serialize the multiplexer to the given stream.  To insure coherency, all
     * sinks must be serialized at the same time.  In general, that means they
     * all must be paused simultaneously with the serialization of the multiplexer.
     *
     * @param stream the target stream
     */
    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const SinkMultiplexer &mux);

    /**
     * Deserialize the multiplexer from saved state.  This must be run before
     * any other sinks are used requested (though they can be created).  That
     * is, in order to preserve the output integrity, the multiplexer must not
     * have emitted any values (since they resulting values from the deserialize
     * may be before them).
     */
    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, SinkMultiplexer &mux);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const SinkMultiplexer &mux);

    /**
     * Test if the multiplexer has completed its execution.  Once this returns
     * true the interface may be destroyed at will.
     *
     * @return true if the interface is finished
     */
    bool isFinished();

    /**
     * Wait for completion of the multiplexer.
     *
     * @param timeout   the maximum time to wait
     * @return          true if the interface has completed
     */
    bool wait(double timeout = FP::undefined());

    /**
     * Emitted when the multiplexer has completed.
     */
    Threading::Signal<> finished;

    /**
     * Start the execution of the multiplexer.
     */
    void start();

    /**
     * Signal the multiplexer to terminate as soon as possible.  If the wait
     * flag is set then the multiplexer will wait for all streams to complete
     * before exiting.  If the wait flag is set then this is only different
     * from sinkCreationComplete() in that it will not flush values to
     * the egress (or wait for it to be non-NULL).
     *
     * @param waitForEnd    do not terminate the thread until all streams have ended
     */
    void signalTerminate(bool waitForEnd = false);

    /**
     * Inform the multiplexer that all sinks have been created.  The effect
     * of this is that the multiplexer will exit once all those sinks have
     * ended and all data is flushed.
     */
    void sinkCreationComplete();
};


}
}

#endif
