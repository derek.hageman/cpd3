/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACOREVALUEOPTIONPARSE_H
#define CPD3DATACOREVALUEOPTIONPARSE_H

#include "core/first.hxx"

#include <limits.h>
#include <QtGlobal>

#include "datacore/datacore.hxx"
#include "datacore/variant/handle.hxx"
#include "core/component.hxx"


namespace CPD3 {
namespace Data {

class CPD3DATACORE_EXPORT ComponentOptionValueParsable {
public:
    virtual ~ComponentOptionValueParsable();

    /**
     * Set the option based on the generic configuration value.
     * 
     * @param value the value to parse from
     * @return      true on success
     */
    virtual bool parseFromValue(const Variant::Read &value) = 0;
};


/**
 * Basic routines for parsing component options from a value
 * configuration.
 */
class CPD3DATACORE_EXPORT ValueOptionParse {
public:
    static void parse(ComponentOptions &options, const Variant::Read &value);
};

}
}

Q_DECLARE_INTERFACE(CPD3::Data::ComponentOptionValueParsable,
                    "CPD3.Data.ComponentOptionValueParsable/1.0")

#endif
