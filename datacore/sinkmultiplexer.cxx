/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>
#include <algorithm>
#include <QDebugStateSaver>


#include "core/threadpool.hxx"
#include "datacore/sinkmultiplexer.hxx"


namespace CPD3 {
namespace Data {

SinkMultiplexer::SinkMultiplexer(bool canStall) : canStall(canStall),
                                                  executionState(ExecutionState::NotStarted),
                                                  terminationRequest(TerminationRequest::None),
                                                  egress(nullptr)
{ }

SinkMultiplexer::~SinkMultiplexer()
{
    {
        std::unique_lock<std::mutex> lock(mutex);

        backend.creationComplete();

        activeSinks.clear();
        completedSinks.clear();

        terminationRequest = TerminationRequest::Immediate;
    }
    request.notify_all();
    if (thread.joinable())
        thread.join();
}

void SinkMultiplexer::setEgress(StreamSink *set)
{
    std::unique_lock<std::mutex> lock(mutex);
    std::unique_lock<std::mutex> egressLocker(egressLock);
    if (executionState == ExecutionState::Completed) {
        lock.unlock();
        if (set && set != this->egress) {
            set->endData();
        }
        this->egress = set;
        return;
    }
    this->egress = set;
    notifyUpdate();
}

void SinkMultiplexer::sinkCreationComplete()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        backend.creationComplete();
    }
    notifyUpdate();
}

void SinkMultiplexer::signalTerminate(bool waitForEnd)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        backend.creationComplete();
        if (waitForEnd)
            terminationRequest = TerminationRequest::Graceful;
        else
            terminationRequest = TerminationRequest::Immediate;
    }
    notifyUpdate();
}

int SinkMultiplexer::totalBufferedValues()
{
    std::lock_guard<std::mutex> lock(mutex);
    return static_cast<int>(backend.totalBufferSize());
}

double SinkMultiplexer::trackedTime()
{
    std::lock_guard<std::mutex> lock(mutex);
    return backend.getCurrentAdvance();
}

#ifdef MULTIPLEXER_TAG_TRACKING
static std::mutex globalTagMutex;
static QString globalTag;
void SinkMultiplexer::globalTag(const QString &tag)
{
    std::lock_guard<std::mutex> locker(globalTagMutex);
    CPD3::Data::globalTag = tag;
}

void SinkMultiplexer::setTag(const QString &tag)
{
    std::lock_guard<std::mutex> locker(mutex);
    this->localTag = tag;
}
#endif

SinkMultiplexer::Sink *SinkMultiplexer::createSink(

#ifdef MULTIPLEXER_TAG_TRACKING
        , const QString &tag
#endif
)
{
    std::lock_guard<std::mutex> lock(mutex);
    SinkSimple *in = new SinkSimple(this);
#ifdef MULTIPLEXER_TAG_TRACKING
    {
        std::lock_guard<std::mutex> locker(globalTagMutex);
        in->sinkTag = CPD3::Data::globalTag;
    }
    in->sinkTag.append(this->localTag);
    in->sinkTag.append(tag);
#endif
    return in;
}

SinkMultiplexer::Sink *SinkMultiplexer::createSegmented(

#ifdef MULTIPLEXER_TAG_TRACKING
        , const QString &tag
#endif
)
{
    std::lock_guard<std::mutex> lock(mutex);
    SinkSegmented *in = new SinkSegmented(this);
#ifdef MULTIPLEXER_TAG_TRACKING
    {
        std::lock_guard<std::mutex> locker(globalTagMutex);
        in->sinkTag = CPD3::Data::globalTag;
    }
    in->sinkTag.append(this->localTag);
    in->sinkTag.append(tag);
#endif
    return in;
}

SinkMultiplexer::Sink *SinkMultiplexer::createUnsorted(

#ifdef MULTIPLEXER_TAG_TRACKING
        , const QString &tag
#endif
)
{
    std::lock_guard<std::mutex> lock(mutex);
    SinkUnsorted *in = new SinkUnsorted(this);
#ifdef MULTIPLEXER_TAG_TRACKING
    {
        std::lock_guard<std::mutex> locker(globalTagMutex);
        in->sinkTag = CPD3::Data::globalTag;
    }
    in->sinkTag.append(this->localTag);
    in->sinkTag.append(tag);
#endif
    return in;
}

SinkMultiplexer::Sink *SinkMultiplexer::createPlaceholder(

#ifdef MULTIPLEXER_TAG_TRACKING
        , const QString &tag
#endif
)
{
    std::lock_guard<std::mutex> lock(mutex);
    SinkPlaceholder *in = new SinkPlaceholder(this);
#ifdef MULTIPLEXER_TAG_TRACKING
    {
        std::lock_guard<std::mutex> locker(globalTagMutex);
        in->sinkTag = CPD3::Data::globalTag;
    }
    in->sinkTag.append(this->localTag);
    in->sinkTag.append(tag);
#endif
    return in;
}

enum {
    TYPE_SIMPLE, TYPE_SEGMENTED, TYPE_UNSORTED, TYPE_PLACEHOLDER,
};


QDataStream &operator<<(QDataStream &stream, const SinkMultiplexer &mux)
{
    std::lock_guard<std::mutex> locker(const_cast<std::mutex &>(mux.mutex));
    std::lock_guard<std::mutex> egressLocker(const_cast<std::mutex &>(mux.egressLock));

    Q_ASSERT(mux.executionState == SinkMultiplexer::ExecutionState::Completed ||
                     mux.executionState == SinkMultiplexer::ExecutionState::NotStarted ||
                     !mux.egress);

    mux.backend.serialize(stream);
    stream << (quint32) mux.activeSinks.size();
    for (const auto &in : mux.activeSinks) {
        mux.serializeSink(*in, stream);
    }

    return stream;
}

QDataStream &operator>>(QDataStream &stream, SinkMultiplexer &mux)
{
    std::lock_guard<std::mutex> locker(const_cast<std::mutex &>(mux.mutex));
    std::lock_guard<std::mutex> egressLocker(const_cast<std::mutex &>(mux.egressLock));

    mux.completedSinks.clear();

    bool completed = false;
    mux.backend.deserialize(stream, &completed);
    mux.deserializeRecreate();

    quint32 ns;
    stream >> ns;
    for (quint32 i = 0; i < ns; i++) {
        quint8 type = 0;
        stream >> type;
        switch (type) {
        case TYPE_SIMPLE:
            mux.backend.deserializeSimple(stream)->end();
            break;
        case TYPE_SEGMENTED:
            mux.backend.deserializeSegmented(stream)->end();
            break;
        case TYPE_UNSORTED:
            mux.backend.deserializeUnsorted(stream)->end();
            break;
        case TYPE_PLACEHOLDER:
            mux.backend.deserializePlaceholder(stream)->end();
            break;
        default:
            Q_ASSERT(false);
            break;
        }
    }

    if (completed)
        mux.backend.creationComplete();

    mux.notifyUpdate();

    return stream;
}

void SinkMultiplexer::deserializeRecreate()
{
    for (const auto &in : activeSinks) {
        in->deserializeRecreate();
    }
}


void SinkMultiplexer::run()
{
    bool localTerminated;
    SequenceValue::Transfer emitValues;
    bool dataEnded = false;

    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!lock)
            lock.lock();
        localTerminated = false;

        completedSinks.clear();

        std::unique_lock<std::mutex> egressLocker(egressLock);

        switch (terminationRequest) {
        case TerminationRequest::None:
            break;
        case TerminationRequest::Graceful:
            localTerminated = true;
            break;
        case TerminationRequest::Immediate:
            if (egress) {
                lock.unlock();
                egress->endData();
                egressLocker.unlock();
                lock.lock();
            }
            executionState = ExecutionState::Completed;
            lock.unlock();
            response.notify_all();
            finished();
            return;
        }

        switch (executionState) {
        case ExecutionState::NotStarted:
        case ExecutionState::Completed:
            Q_ASSERT(false);
            return;

        case ExecutionState::Starting:
            executionState = ExecutionState::Running;
            lock.unlock();
            response.notify_all();
            continue;

        case ExecutionState::Running:
        case ExecutionState::OutputStalled:
            if (!localTerminated && !egress) {
                egressLocker.unlock();
                if (executionState == ExecutionState::OutputStalled) {
                    executionState = ExecutionState::Running;
                    response.notify_all();
                }
                request.wait(lock);
                continue;
            }

            backend.output(emitValues);
            if (backend.isComplete())
                dataEnded = true;

            if (localTerminated || !emitValues.empty() || dataEnded) {
                if (canStall && egress && !emitValues.empty() && !localTerminated) {
                    executionState = ExecutionState::OutputStalled;
                }
                break;
            }

            if (executionState == ExecutionState::OutputStalled) {
                executionState = ExecutionState::Running;
                response.notify_all();
            }
            egressLocker.unlock();
            request.wait(lock);
            continue;
        }
        lock.unlock();

        if (egress && !emitValues.empty() && !localTerminated) {
            egress->incomingData(std::move(emitValues));
            emitValues.clear();
        } else if (localTerminated) {
            emitValues.clear();
        }

        if (dataEnded && (localTerminated || egress)) {
            if (egress)
                egress->endData();

            egressLocker.unlock();
            lock.lock();
            executionState = ExecutionState::Completed;
            lock.unlock();
            response.notify_all();
            finished();
            return;
        }
    }
}

void SinkMultiplexer::start()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(executionState == ExecutionState::NotStarted);
        executionState = ExecutionState::Starting;
    }
    response.notify_all();
    thread = std::thread(&SinkMultiplexer::run, this);
}

bool SinkMultiplexer::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return executionState == ExecutionState::Completed;
}

bool SinkMultiplexer::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, mutex, response, [this] {
        switch (executionState) {
        case ExecutionState::NotStarted:
        case ExecutionState::Completed:
            return true;
        case ExecutionState::Starting:
        case ExecutionState::Running:
        case ExecutionState::OutputStalled:
            break;
        }
        return false;
    });
}

void SinkMultiplexer::stall(std::unique_lock<std::mutex> &lock)
{
    for (;;) {
        if (executionState != ExecutionState::OutputStalled)
            return;
        response.wait(lock);
    }
}

void SinkMultiplexer::stall(std::unique_lock<std::mutex> &lock, std::size_t bufferSize)
{
    std::size_t limit = StreamSink::stallThreshold;
    std::size_t div = activeSinks.size();
    if (div > 2) {
        div /= 2;
        limit /= div;
        if (limit < 256)
            limit = 256;
    }
    if (limit > 0) {
        if (bufferSize < limit)
            return;
    }
    return stall(lock);
}


void SinkMultiplexer::completeSink(Sink *check)
{
    for (auto in = activeSinks.begin(), endI = activeSinks.end(); in != endI; ++in) {
        if (check != in->get())
            continue;
        completedSinks.emplace_back(std::move(*in));
        activeSinks.erase(in);
        notifyUpdate();
        return;
    }
    Q_ASSERT(false);
}

SinkMultiplexer::Sink::Sink(SinkMultiplexer *multiplexer) : mux(multiplexer)
{
    mux->activeSinks.emplace_back(this);
}

SinkMultiplexer::Sink::~Sink() = default;

SinkMultiplexer::SinkSimple::SinkSimple(SinkMultiplexer *mux) : Sink(mux),
                                                                backend(mux->backend.createSimple())
{ }

SinkMultiplexer::SinkSimple::~SinkSimple()
{
    if (backend)
        backend->end();
}

void SinkMultiplexer::SinkSimple::incomingData(const SequenceValue::Transfer &values)
{
    std::unique_lock<std::mutex> lock(mux->mutex);
    mux->stall(lock, backend->bufferSize());

#ifdef MULTIPLEXER_UNIT_TRACKING
    for (const auto &v : values) {
        seenUnits |= v.getUnit();
    }
#endif
    if (!backend->incoming(values))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkSimple::incomingData(SequenceValue::Transfer &&values)
{
    std::unique_lock<std::mutex> lock(mux->mutex);
    mux->stall(lock, backend->bufferSize());

#ifdef MULTIPLEXER_UNIT_TRACKING
    for (const auto &v : values) {
        seenUnits |= v.getUnit();
    }
#endif
    if (!backend->incoming(std::move(values)))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkSimple::incomingData(const SequenceValue &value)
{
    std::unique_lock<std::mutex> lock(mux->mutex);
    mux->stall(lock, backend->bufferSize());

#ifdef MULTIPLEXER_UNIT_TRACKING
    seenUnits |= value.getUnit();
#endif
    if (!backend->incomingValue(value))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkSimple::incomingData(SequenceValue &&value)
{
    std::unique_lock<std::mutex> lock(mux->mutex);
    mux->stall(lock, backend->bufferSize());

#ifdef MULTIPLEXER_UNIT_TRACKING
    seenUnits |= value.getUnit();
#endif
    if (!backend->incomingValue(std::move(value)))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkSimple::endData()
{
    std::lock_guard<std::mutex> lock(mux->mutex);
    if (!backend)
        return;
    backend->end();
    backend = nullptr;
    mux->completeSink(this);
}

void SinkMultiplexer::SinkSimple::incomingAdvance(double time)
{
    std::lock_guard<std::mutex> lock(mux->mutex);
    if (!backend->advance(time))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkSimple::describe(QDebug &stream, bool detailed) const
{ backend->describe(stream, detailed); }

void SinkMultiplexer::SinkSimple::serialize(QDataStream &stream) const
{
    stream << (quint8) TYPE_SIMPLE;
    backend->serialize(stream);
}

void SinkMultiplexer::SinkSimple::deserializeRecreate()
{
    if (!backend)
        return;
    backend = mux->backend.createSimple();
}

#ifndef NDEBUG

int SinkMultiplexer::SinkSimple::uid() const
{ return backend->uid(); }

#endif


SinkMultiplexer::SinkSegmented::SinkSegmented(SinkMultiplexer *mux) : Sink(mux),
                                                                      backend(mux->backend
                                                                                 .createSegmented())
{ }

SinkMultiplexer::SinkSegmented::~SinkSegmented()
{
    if (backend)
        backend->end();
}

void SinkMultiplexer::SinkSegmented::incomingData(const SequenceValue::Transfer &values)
{
    std::unique_lock<std::mutex> lock(mux->mutex);
    mux->stall(lock, backend->bufferSize());

#ifdef MULTIPLEXER_UNIT_TRACKING
    for (const auto &v : values) {
        seenUnits |= v.getUnit();
    }
#endif
    if (!backend->incoming(values))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkSegmented::incomingData(SequenceValue::Transfer &&values)
{
    std::unique_lock<std::mutex> lock(mux->mutex);
    mux->stall(lock, backend->bufferSize());

#ifdef MULTIPLEXER_UNIT_TRACKING
    for (const auto &v : values) {
        seenUnits |= v.getUnit();
    }
#endif
    if (!backend->incoming(std::move(values)))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkSegmented::incomingData(const SequenceValue &value)
{
    std::unique_lock<std::mutex> lock(mux->mutex);
    mux->stall(lock, backend->bufferSize());

#ifdef MULTIPLEXER_UNIT_TRACKING
    seenUnits |= value.getUnit();
#endif
    if (!backend->incomingValue(value))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkSegmented::incomingData(SequenceValue &&value)
{
    std::unique_lock<std::mutex> lock(mux->mutex);
    mux->stall(lock, backend->bufferSize());

#ifdef MULTIPLEXER_UNIT_TRACKING
    seenUnits |= value.getUnit();
#endif
    if (!backend->incomingValue(std::move(value)))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkSegmented::endData()
{
    std::lock_guard<std::mutex> lock(mux->mutex);
    if (!backend)
        return;
    backend->end();
    backend = nullptr;
    mux->completeSink(this);
}

void SinkMultiplexer::SinkSegmented::incomingAdvance(double time)
{
    std::lock_guard<std::mutex> lock(mux->mutex);
    if (!backend->advance(time))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkSegmented::describe(QDebug &stream, bool detailed) const
{ backend->describe(stream, detailed); }

void SinkMultiplexer::SinkSegmented::serialize(QDataStream &stream) const
{
    stream << (quint8) TYPE_SEGMENTED;
    backend->serialize(stream);
}

void SinkMultiplexer::SinkSegmented::deserializeRecreate()
{
    if (!backend)
        return;
    backend = mux->backend.createSegmented();
}

#ifndef NDEBUG

int SinkMultiplexer::SinkSegmented::uid() const
{ return backend->uid(); }

#endif


SinkMultiplexer::SinkUnsorted::SinkUnsorted(SinkMultiplexer *mux) : Sink(mux),
                                                                    backend(mux->backend
                                                                               .createUnsorted())
{ }

SinkMultiplexer::SinkUnsorted::~SinkUnsorted()
{
    if (backend)
        backend->end();
}

void SinkMultiplexer::SinkUnsorted::incomingData(const SequenceValue::Transfer &values)
{
    std::unique_lock<std::mutex> lock(mux->mutex);
    mux->stall(lock);

#ifdef MULTIPLEXER_UNIT_TRACKING
    for (const auto &v : values) {
        seenUnits |= v.getUnit();
    }
#endif
    if (!backend->incoming(values))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkUnsorted::incomingData(SequenceValue::Transfer &&values)
{
    std::unique_lock<std::mutex> lock(mux->mutex);
    mux->stall(lock);

#ifdef MULTIPLEXER_UNIT_TRACKING
    for (const auto &v : values) {
        seenUnits |= v.getUnit();
    }
#endif
    if (!backend->incoming(std::move(values)))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkUnsorted::incomingData(const SequenceValue &value)
{
    std::unique_lock<std::mutex> lock(mux->mutex);
    mux->stall(lock);

#ifdef MULTIPLEXER_UNIT_TRACKING
    seenUnits |= value.getUnit();
#endif
    if (!backend->incomingValue(value))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkUnsorted::incomingData(SequenceValue &&value)
{
    std::unique_lock<std::mutex> lock(mux->mutex);
    mux->stall(lock);

#ifdef MULTIPLEXER_UNIT_TRACKING
    seenUnits |= value.getUnit();
#endif
    if (!backend->incomingValue(std::move(value)))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkUnsorted::endData()
{
    std::lock_guard<std::mutex> lock(mux->mutex);
    if (!backend)
        return;
    backend->end();
    backend = nullptr;
    mux->completeSink(this);
}

void SinkMultiplexer::SinkUnsorted::incomingAdvance(double time)
{
    std::lock_guard<std::mutex> lock(mux->mutex);
    if (!backend->advance(time))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkUnsorted::describe(QDebug &stream, bool detailed) const
{ backend->describe(stream, detailed); }

void SinkMultiplexer::SinkUnsorted::serialize(QDataStream &stream) const
{
    stream << (quint8) TYPE_UNSORTED;
    backend->serialize(stream);
}

void SinkMultiplexer::SinkUnsorted::deserializeRecreate()
{
    if (!backend)
        return;
    backend = mux->backend.createUnsorted();
}

#ifndef NDEBUG

int SinkMultiplexer::SinkUnsorted::uid() const
{ return backend->uid(); }

#endif


SinkMultiplexer::SinkPlaceholder::SinkPlaceholder(SinkMultiplexer *mux) : Sink(mux),
                                                                          backend(mux->backend
                                                                                     .createPlaceholder())
{ }

SinkMultiplexer::SinkPlaceholder::~SinkPlaceholder()
{
    if (backend)
        backend->end();
}

void SinkMultiplexer::SinkPlaceholder::incomingData(const SequenceValue::Transfer &)
{ Q_ASSERT(false); }

void SinkMultiplexer::SinkPlaceholder::incomingData(SequenceValue::Transfer &&)
{ Q_ASSERT(false); }

void SinkMultiplexer::SinkPlaceholder::incomingData(const SequenceValue &)
{ Q_ASSERT(false); }

void SinkMultiplexer::SinkPlaceholder::incomingData(SequenceValue &&)
{ Q_ASSERT(false); }

void SinkMultiplexer::SinkPlaceholder::endData()
{
    std::lock_guard<std::mutex> lock(mux->mutex);
    if (!backend)
        return;
    backend->end();
    backend = nullptr;
    mux->completeSink(this);
}

void SinkMultiplexer::SinkPlaceholder::incomingAdvance(double time)
{
    std::lock_guard<std::mutex> lock(mux->mutex);
    if (!backend->advance(time))
        return;
    mux->notifyUpdate();
}

void SinkMultiplexer::SinkPlaceholder::describe(QDebug &stream, bool) const
{ backend->describe(stream, false); }

void SinkMultiplexer::SinkPlaceholder::serialize(QDataStream &stream) const
{
    stream << (quint8) TYPE_PLACEHOLDER;
    backend->serialize(stream);
}

void SinkMultiplexer::SinkPlaceholder::deserializeRecreate()
{
    if (!backend)
        return;
    backend = mux->backend.createPlaceholder();
}

#ifndef NDEBUG

int SinkMultiplexer::SinkPlaceholder::uid() const
{ return backend->uid(); }

#endif


QDebug operator<<(QDebug stream, const SinkMultiplexer &mux)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "SinkMultiplexer({";
    std::lock_guard<std::mutex> lock(const_cast<std::mutex &>(mux.mutex));
#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
    mux.backend.describe(stream, stream.verbosity() >= 4);
#else
    mux.backend.describe(stream);
#endif
    stream << '}';
    for (const auto &in : mux.activeSinks) {
        stream << ',';
#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
        if (stream.verbosity() >= 3) {
            QDebugStateSaver baseOutput(stream);
            stream << hex << showbase << reinterpret_cast<quintptr>(in.get()) << '=';
        }
#endif
#ifdef MULTIPLEXER_UNIT_TRACKING
        {
            auto units = in->seenUnits.toList();
            std::sort(units.begin(), units.end());
            stream << '<' << units << '>';
        }
#endif
#ifdef MULTIPLEXER_TAG_TRACKING
        stream << "<" << in->tag() << ">";
#endif
        stream << '{';
#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
        in->describe(stream, stream.verbosity() >= 4);
#else
        in->describe(stream);
#endif
        stream << '}';
    }
    stream << ')';
    return stream;
}

}
}
