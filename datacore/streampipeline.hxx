/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACORE_STREAMPIPELINE_HXX
#define CPD3DATACORE_STREAMPIPELINE_HXX

#include "core/first.hxx"

#include <memory>
#include <cstdint>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <chrono>
#include <QIODevice>

#include "datacore/datacore.hxx"
#include "datacore/stream.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/processingtapchain.hxx"
#include "core/timeutils.hxx"
#include "core/range.hxx"
#include "archive/selection.hxx"
#include "archive/access.hxx"
#include "io/access.hxx"
#include "io/socket.hxx"

namespace CPD3 {
namespace Data {

namespace Archive {
class Access;
}

/**
 * The base manager that deals with pipeline chains of filters.  This provides
 * a common interface for a simple (no-fanout) chain of filters that allows
 * for multiprocess combination when possible.
 * <br>
 * This class is not externally thread safe and requires periodic processing
 * by the thread that creates it.  Normally this is implemented by calling
 * the process() function in response to the updated() signal.
 */
class CPD3DATACORE_EXPORT StreamPipeline {
    QString inputError;
    QString outputError;
    QString chainError;

    bool enableAcceleration;
    bool enableSerialization;
    bool enableForwarding;

    using Clock = std::chrono::steady_clock;

    std::mutex mutex;
    std::condition_variable notify;
    enum class ThreadState {
        Initialize, Running, Completed, TerminateRequested, LockRequested, Locked,
    } threadState;
    bool reapPending;
    std::string inputAccelerationKey;

    std::atomic_size_t inputBytesRead;
    std::size_t inputTotalSize;

    class ThreadLock final {
        StreamPipeline &parent;
        bool didLock;
    public:
        explicit ThreadLock(StreamPipeline &parent);

        ~ThreadLock();
    };

    class ForwardSink : public StreamSink {
        StreamSink *target;
    public:
        explicit ForwardSink(StreamSink *target);

        virtual ~ForwardSink();

        void incomingData(const SequenceValue::Transfer &values) override;

        void incomingData(SequenceValue::Transfer &&values) override;

        void incomingData(const SequenceValue &value) override;

        void incomingData(SequenceValue &&value) override;

        void endData() override;

        void retarget(StreamSink *change);
    };

    class ClippingSink : public ForwardSink {
        Time::Bounds clip;
    public:
        ClippingSink(StreamSink *target, const Time::Bounds &clip);

        virtual ~ClippingSink();

        void incomingData(const SequenceValue::Transfer &values) override;

        void incomingData(SequenceValue::Transfer &&values) override;

        void incomingData(const SequenceValue &value) override;

        void incomingData(SequenceValue &&value) override;
    };

    class EndTrackingSink final : public ForwardSink {
        std::mutex &mutex;
        bool receivedEnd;
    public:
        EndTrackingSink(StreamSink *target, StreamPipeline &parent);

        virtual ~EndTrackingSink();

        void endData() override;

        bool isEnded();
    };

    class StartTrackingSink final : public ForwardSink {
        std::mutex &mutex;
        std::condition_variable &notify;
        bool receivedData;
    public:
        StartTrackingSink(StreamSink *target, StreamPipeline &parent);

        virtual ~StartTrackingSink();

        void incomingData(const SequenceValue::Transfer &values) override;

        void incomingData(SequenceValue::Transfer &&values) override;

        void incomingData(const SequenceValue &value) override;

        void incomingData(SequenceValue &&value) override;

        bool isStartedUnlocked() const;
    };

    class RetainSink final : public ForwardSink {
        SequenceValue::Transfer contents;
    public:
        explicit RetainSink(StreamSink *target);

        virtual ~RetainSink();

        void incomingData(const SequenceValue::Transfer &values) override;

        void incomingData(SequenceValue::Transfer &&values) override;

        void incomingData(const SequenceValue &value) override;

        void incomingData(SequenceValue &&value) override;

        const SequenceValue::Transfer &retainedData() const
        { return contents; }
    };

    class ExternalInputSink final : public StreamSink, public StreamSource {
        StreamSink *next;
        std::mutex mutex;
        std::condition_variable notify;
        std::mutex &endMutex;
        std::condition_variable &endNotify;
        bool &reapPending;
        bool receivedEnd;
        bool discardData;
    public:
        explicit ExternalInputSink(StreamPipeline &parent);

        virtual ~ExternalInputSink();

        void incomingData(const SequenceValue::Transfer &values) override;

        void incomingData(SequenceValue::Transfer &&values) override;

        void incomingData(const SequenceValue &value) override;

        void incomingData(SequenceValue &&value) override;

        void endData() override;

        void setEgress(StreamSink *sink) override;

        bool isEnded();

        void signalTerminate();
    };

    class ExternalOutputSink final : public ForwardSink {
        std::mutex &mutex;
        std::condition_variable &notify;
        bool &reapPending;
        bool receivedEnd;
    public:
        ExternalOutputSink(StreamSink *target, StreamPipeline &parent);

        virtual ~ExternalOutputSink();

        void endData() override;

        bool isEnded();
    };

    class FullDiscardSink final : public StreamSink {
    public:
        explicit FullDiscardSink();

        virtual ~FullDiscardSink();

        void incomingData(const SequenceValue::Transfer &values) override;

        void incomingData(SequenceValue::Transfer &&values) override;

        void incomingData(const SequenceValue &value) override;

        void incomingData(SequenceValue &&value) override;

        void endData() override;
    };

    class ProcessingTapBackend final : public ProcessingTapChain::ArchiveBackend {
        StreamPipeline &parent;
        Archive::Selection::List allSelections;
        bool issueComplete;
    public:
        class DispatchSink final : public StreamSink {
            std::vector<StreamSink *> targets;
        public:
            explicit DispatchSink(ProcessingTapBackend &parent);

            virtual ~DispatchSink();

            void incomingData(const SequenceValue::Transfer &values) override;

            void incomingData(SequenceValue::Transfer &&values) override;

            void incomingData(const SequenceValue &value) override;

            void incomingData(SequenceValue &&value) override;

            void endData() override;

            void addTarget(StreamSink *target);
        };

        DispatchSink dispatch;

        explicit ProcessingTapBackend(StreamPipeline &parent);

        virtual ~ProcessingTapBackend();

        void issueArchiveRead(const Archive::Selection::List &selections,
                              StreamSink *target) override;

        void archiveReadIssueComplete() override;

        const Archive::Selection::List &waitForSelections();
    };

    IO::Access::Handle pipelineInputHandle;
    std::unique_ptr<IO::Generic::Stream> chainInputStream;
    std::unique_ptr<ExternalSource> chainSource;
    std::unique_ptr<EndTrackingSink> chainInputSink;

    std::unique_ptr<Archive::Access> ownedArchive;
    Archive::Access *chainInputArchive;
    Archive::Selection::List chainInputSelections;
    Archive::Access::StreamHandle chainInputReader;

    std::unique_ptr<ExternalInputSink> chainExternalInput;

    bool sourceCanAccelerate;
    std::string forwardServerKey;

    Time::Bounds chainInputClipBounds;
    std::unique_ptr<ClippingSink> chainInputClippingSink;

    bool attachInputStream();

    IO::Access::Handle pipelineOutputHandle;
    std::unique_ptr<ExternalSink> chainSink;
    bool sinkCanAccelerate;
    std::unique_ptr<Clock::time_point> sinkStartDifferTimeout;
    std::unique_ptr<StartTrackingSink> differedStartSink;
    std::string accelerationSecret;

    bool chainTapUseRequestedInput;
    Time::Bounds chainTapRequestedBounds;
    ProcessingTapChain *chainTapOutput;
    std::unique_ptr<ProcessingTapBackend> tapChainBackend;

    std::unique_ptr<ExternalOutputSink> chainExternalOutput;

    std::unique_ptr<EndTrackingSink> chainOutputSink;

    bool retainOutput;
    bool retainedShortCircuit;
    std::string retainedInputFile;
    std::unique_ptr<RetainSink> retainedSink;

    std::unique_ptr<IO::Socket::Server> accelerationServer;
    std::deque<std::unique_ptr<IO::Socket::Connection>> accelerationIncoming;

    void attachOutputSink();

    bool canRestartWithRetained();

    bool restartWithRetained();

    void startTapChain(bool usingBackend);

    bool shortCircuitTapChain();

    StreamSink *startInitialOutputSink();

    struct ChainStage {
        std::unique_ptr<ProcessingStage> stage;
        ProcessingStageComponent *component;
        std::unique_ptr<EndTrackingSink> outputSink;

        ChainStage();

        ChainStage(std::unique_ptr<ProcessingStage> &&stage, ProcessingStageComponent *component);
    };

    std::vector<ChainStage> chain;

    FullDiscardSink discardSink;

    std::thread thread;

    void run();

    void executeTerminate();

    void attemptAcceleration(const std::string &key);

    bool canForwardConnect() const;

    void processIncomingAcceleration(std::unique_ptr<IO::Socket::Connection> &&connection);

    bool releaseInput(bool usingRetained = false);

    bool canModifyChain();

    bool releaseOutput();

public:
    /**
     * Create the pipe chain.
     *
     * @param acceleration  enable acceleration
     * @param serialization enable serialization
     * @param forwarding    enable forwarding
     */
    explicit StreamPipeline(bool acceleration = true,
                            bool serialization = true,
                            bool forwarding = true);

    virtual ~StreamPipeline();

    /**
     * Set the chain to read from the standard input pipeline.
     *
     * @return  true on success
     */
    bool setInputPipeline();

    /**
     * Set the chain to read from the given file.
     *
     * @param name      the file name to read from
     * @return          true on success
     */
    bool setInputFile(std::string name);

    inline bool setInputFile(const QString &name)
    { return setInputFile(name.toStdString()); }

    inline bool setInputFile(const char *name)
    { return setInputFile(std::string(name)); }

    /**
     * Set the chain to read from the data archive.
     *
     * @param selections    the selections to read
     * @param clip          the bounds to clip to
     * @param archive       the archive to read from or null to allocate, this does NOT take ownership
     * @return              true on success
     */
    bool setInputArchive(const Archive::Selection::List &selections,
                         const Time::Bounds &clip = {},
                         Archive::Access *archive = nullptr);

    bool setInputArchive(const Archive::Selection &selection, const Time::Bounds &clip = {},
                         Archive::Access *archive = nullptr);

    /**
     * Set the chain to read the inputs requested by its current components from
     * the data archive.
     *
     * @param bounds        the bounds to read
     * @param clip          clip to exactly those bounds
     * @param archive       the archive to read from or NULL to allocate, this does NOT take ownership
     * @return              true on success
     */
    bool setInputArchiveRequested(const Time::Bounds &bounds = {},
                                  bool clip = false,
                                  Archive::Access *archive = nullptr);

    /**
     * Set the chain to read from the given input handler.
     *
     * @param input         the input to read from
     * @return              true on success
     */
    bool setInputSource(std::unique_ptr<ExternalSource> &&source);

    /**
     * Set the chain to read from the given input handler.
     *
     * @param input         the input to read from
     * @param stream        the stream to send to the input (if any)
     * @param canAccelerate true if the input (and the device) support acceleration
     * @return              true on success
     */
    bool setInputGeneral(std::unique_ptr<ExternalConverter> &&input,
                         std::unique_ptr<IO::Generic::Stream> &&stream = {},
                         bool canAccelerate = false);

    bool setInputGeneral(std::unique_ptr<ExternalConverter> &&input, QIODevice *inputDevice,
                         bool canAccelerate = false,
                         bool ownDevice = false);

    /**
     * Set the chain to read from the given input handler.
     *
     * @param input         the input to read from
     * @param name          the file to send to the input
     * @param textMode      open the file in text mode
     * @return              true on success
     */
    bool setInputGeneral(std::unique_ptr<ExternalConverter> &&input,
                         std::string name,
                         bool textMode = true);

    inline bool setInputGeneral(std::unique_ptr<ExternalConverter> &&input,
                                const QString &name,
                                bool textMode = true)
    { return setInputGeneral(std::move(input), name.toStdString(), textMode); }

    inline bool setInputGeneral(std::unique_ptr<ExternalConverter> &&input,
                                const char *name,
                                bool textMode = true)
    { return setInputGeneral(std::move(input), std::string(name), textMode); }

    /**
     * Set the chain to read from the given input handler taking data from
     * the standard input pipeline.
     *
     * @param input         the input to read from
     * @param canAccelerate true if the input reader supports acceleration
     * @return              true on success
     */
    bool setInputGeneralPipeline(std::unique_ptr<ExternalConverter> &&input,
                                 bool canAccelerate = false);

    /**
     * Set the chain to get input from an external ingress.  This is used to
     * connect it to an arbitrary input.  The returned ingress is valid
     * immediately but if anything is written to then the input changed again
     * the result is undefined.
     * <br>
     * Ownership of the sink is retained by the chain.
     *
     * @return              a sink for data input or null on error
     */
    StreamSink *setInputExternal();

    /**
     * Get an error string describing the last error on the input subsystem.
     *
     * @return  the description of the last input error
     */
    inline const QString &getInputError() const
    { return inputError; }

    /**
     * Set the chain to output to the standard output pipeline.
     *
     * @return              true on success
     */
    bool setOutputPipeline();

    /**
     * Set the chain to output to a file.
     *
     * @param name          the file to write to
     * @param mode          the output mode
     * @return              true on success
     */
    bool setOutputFile(std::string name,
                       StandardDataOutput::OutputType mode = StandardDataOutput::OutputType::Direct);

    inline bool setOutputFile(const QString &name,
                              StandardDataOutput::OutputType mode = StandardDataOutput::OutputType::Direct)
    { return setOutputFile(name.toStdString(), mode); }

    inline bool setOutputFile(const char *name,
                              StandardDataOutput::OutputType mode = StandardDataOutput::OutputType::Direct)
    { return setOutputFile(std::string(name), mode); }

    /**
     * Set the chain to output to the given general handler.
     *
     * @param output        the output to send to
     * @return              true on success
     */
    bool setOutputGeneral(std::unique_ptr<ExternalSink> &&output);

    /**
     * Set the chain to output to the given filter chain.  This can be used
     * to allow for standard input connection to a filter chain used with
     * graphing.
     *
     * @param chain     the chain to output to, ownership is retained by the caller
     * @param retain    if set then retain values (only meaningful if the input is standard input)
     * @return          true on success
     */
    bool setOutputFilterChain(ProcessingTapChain *chain, bool retain = false);

    /**
     * Set the chain output to the given ingress.
     *
     * @param ingress   the target ingress
     * @
     */
    bool setOutputIngress(StreamSink *ingress);

    /**
     * Get an error string describing the last error on the output subsystem.
     *
     * @return  the description of the last output error
     */
    inline const QString &getOutputError() const
    { return outputError; }

    /**
     * Get a device for pipeline output.  This retrieves (creating if required)
     * the device used for pipeline output.  Generally this is used during
     * general data output creation as the argument to its constructor if
     * required.
     *
     * @return  the pipeline output device or null on error
     */
    IO::Access::Handle pipelineOutputDevice();

    /**
     * Add a general filter to the end of the chain.
     * <br>
     * This takes ownership of the filter and the component remains owned by Qt's
     * management system.
     *
     * @param filter        the filter to add
     * @param component     the component that created the filter, if any
     */
    bool addProcessingStage(std::unique_ptr<ProcessingStage> &&stage,
                            ProcessingStageComponent *component = nullptr);

    /**
     * Get an error string describing the last error related to processing stage chaining.
     *
     * @return  the description of the last filter error
     */
    inline const QString &getChainError() const
    { return chainError; }

    /**
     * Get the requested inputs of the whole chain.
     *
     * @return  the units requested as inputs
     */
    SequenceName::Set requestedInputs();

    /**
     * Get the predicted outputs of the whole chain (this does not include
     * archive access).
     *
     * @return  the units expected as outputs
     */
    SequenceName::Set predictedOutputs();

    /**
     * Wait for completion of the pipeline.
     *
     * @param time  the maximum time to wait
     * @return      true if the chain has completed
     */
    bool wait(double timeout = FP::undefined());

    /**
     * Wait for completion of the pipeline.
     * <br>
     * This runs the Qt event loop while waiting.
     *
     * @param time  the maximum time to wait
     * @return      true if the chain has completed
     */
    bool waitInEventLoop(double timeout = FP::undefined());

    /**
     * Test if the chain has completed.
     *
     * @return      true if the pipeline has completed
     */
    bool isFinished();

    /**
     * Get the expected size of the input in bytes or zero if unknown.
     * 
     * @return  the expected input size
     */
    std::uint_fast64_t getExpectedInputSize();

    /**
     * Get the total number of bytes read from the input so far.  This
     * may be non-zero even if the expected size is unknown (e.x. on a
     * pipeline).
     * 
     * @return  the total number of bytes read.
     */
    std::uint_fast64_t getTotalInputRead();

    /**
     * Start the chain.  After starting parameters cannot be changed and links
     * cannot be added.
     * <br>
     * In general a chain cannot be restarted, but if the outputs and inputs
     * are reset then it can be restarted in some cases.  The most common
     * case is a FilterChain output reading from the archive or standard input.
     */
    bool start();

    /**
     * Abort execution of the chain.
     */
    void signalTerminate();

    ActionFeedback::Source feedback;

    /**
     * Emitted when the pipeline has finished executing.
     */
    Threading::Signal<> finished;

protected:
    /**
     * Get the component object used to de-serialize the named component.
     * <br>
     * This normally just used the Component subsystem to access it.  Normally
     * this is only set for testing.
     *
     * @param name  the name to load
     * @return      the component used for deserialization
     */
    virtual ProcessingStageComponent *getDeserializationComponent(const QString &name);

    /**
     * Create the IO handle for pipeline input.  This exists
     * primarily so test cases can override the input.
     * <br>
     * If this returns null it should set the input error appropriately.
     *
     * @return  a device for reading from the pipeline
     */
    virtual IO::Access::Handle createPipelineInput();

    /**
     * Create the IO handle for pipeline output.  This exists
     * primarily so test cases can override the output.
     * <br>
     * If this returns null it should set the output error appropriately.
     *
     * @return  a device for writing to the pipeline
     */
    virtual IO::Access::Handle createPipelineOutput();

    /* Various calls that are stubs in the acceleration sequence, used for testing */

    virtual void acceleration_client_connection();

    virtual void acceleration_client_detaching();

    virtual void acceleration_client_detachAborted();

    virtual void acceleration_client_forwarded();

    virtual void acceleration_client_detachCompleted();

    virtual void acceleration_client_sendStart();

    virtual void acceleration_client_sendDenied();

    virtual void acceleration_client_sendCompleted();

    virtual void acceleration_client_handoffStart();

    virtual void acceleration_client_handoffComplete();

    virtual void acceleration_server_connection();

    virtual void acceleration_server_forward();

    virtual void acceleration_server_detaching();

    virtual void acceleration_server_detachAborted();

    virtual void acceleration_server_detachFailed();

    virtual void acceleration_server_incomingSend();

    virtual void acceleration_server_handoff();
};

}
}

#endif
