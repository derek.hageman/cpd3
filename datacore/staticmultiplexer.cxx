/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>
#include <algorithm>
#include <QDebugStateSaver>

#include "datacore/staticmultiplexer.hxx"
#include "core/qtcompat.hxx"


namespace CPD3 {
namespace Data {

StaticMultiplexer::StaticMultiplexer(int nStreams) : backend(), streams(), outputQueued(false)
{
    Q_ASSERT(nStreams > 0);
    for (int i = 0; i < nStreams; ++i) {
        streams.push_back(backend.createSimple());
    }
}

StaticMultiplexer::~StaticMultiplexer() = default;

void StaticMultiplexer::setStreams(std::size_t nStreams)
{
    Q_ASSERT(nStreams > 0);
    auto existing = streams.size();
    if (nStreams <= existing) {
        for (auto s = streams.begin() + nStreams, endS = streams.end(); s != endS; ++s) {
            if (!*s)
                continue;
            (*s)->end();
        }
        streams.resize((size_t) nStreams);
    } else {
        for (auto add = existing; add < nStreams; ++add) {
            streams.push_back(backend.createSimple());
        }
    }
}

int StaticMultiplexer::addStream()
{
    int id = (int) streams.size();
    streams.push_back(backend.createSimple());
    return id;
}

void StaticMultiplexer::clear(bool clearPrior)
{
    outputQueued = false;
    backend.reset(clearPrior);
    for (auto &s : streams) {
        s = backend.createSimple();
    }
}

double StaticMultiplexer::firstStreamTime() const
{ return backend.getCurrentAdvance(); }

int StaticMultiplexer::totalBufferedValues() const
{ return static_cast<int>(backend.totalBufferSize()); }

SequenceValue::Transfer StaticMultiplexer::incoming(int stream, const SequenceValue &add, bool dump)
{
    Q_ASSERT(stream >= 0 && stream < (int) streams.size());
    Q_ASSERT(streams[stream]);
    if (!streams[stream]->incomingValue(add) && !outputQueued)
        return SequenceValue::Transfer();
    if (dump) {
        outputQueued = false;
        return backend.output<SequenceValue::Transfer>();
    }
    outputQueued = true;
    return SequenceValue::Transfer();
}

SequenceValue::Transfer StaticMultiplexer::incoming(int stream,
                                                    const SequenceValue::Transfer &add,
                                                    bool dump)
{
    Q_ASSERT(stream >= 0 && stream < (int) streams.size());
    Q_ASSERT(streams[stream]);
    if (!streams[stream]->incoming(add) && !outputQueued)
        return SequenceValue::Transfer();
    if (dump) {
        outputQueued = false;
        return backend.output<SequenceValue::Transfer>();
    }
    outputQueued = true;
    return SequenceValue::Transfer();
}

SequenceValue::Transfer StaticMultiplexer::incoming(int stream, SequenceValue &&add, bool dump)
{
    Q_ASSERT(stream >= 0 && stream < (int) streams.size());
    Q_ASSERT(streams[stream]);
    if (!streams[stream]->incomingValue(std::move(add)) && !outputQueued)
        return SequenceValue::Transfer();
    if (dump) {
        outputQueued = false;
        return backend.output<SequenceValue::Transfer>();
    }
    outputQueued = true;
    return SequenceValue::Transfer();
}

SequenceValue::Transfer StaticMultiplexer::incoming(int stream,
                                                    SequenceValue::Transfer &&add,
                                                    bool dump)
{
    Q_ASSERT(stream >= 0 && stream < (int) streams.size());
    Q_ASSERT(streams[stream]);
    if (!streams[stream]->incoming(std::move(add)) && !outputQueued)
        return SequenceValue::Transfer();
    if (dump) {
        outputQueued = false;
        return backend.output<SequenceValue::Transfer>();
    }
    outputQueued = true;
    return SequenceValue::Transfer();
}

SequenceValue::Transfer StaticMultiplexer::advance(int stream, double time, bool dump)
{
    Q_ASSERT(stream >= 0 && stream < (int) streams.size());
    Q_ASSERT(streams[stream]);
    if (!streams[stream]->advance(time) && !outputQueued)
        return SequenceValue::Transfer();
    if (dump) {
        outputQueued = false;
        return backend.output<SequenceValue::Transfer>();
    }
    outputQueued = true;
    return SequenceValue::Transfer();
}

SequenceValue::Transfer StaticMultiplexer::finish(int stream, bool dump)
{
    Q_ASSERT(stream >= 0 && stream < (int) streams.size());
    Q_ASSERT(streams[stream]);
    streams[stream]->end();
    streams[stream] = nullptr;
    if (dump) {
        outputQueued = false;
        return backend.output<SequenceValue::Transfer>();
    }
    return SequenceValue::Transfer();
}

SequenceValue::Transfer StaticMultiplexer::finish()
{
    backend.creationComplete();
    for (auto &s : streams) {
        if (!s)
            continue;
        s->end();
        s = nullptr;
    }
    SequenceValue::Transfer result;
    backend.output(result);
    outputQueued = false;
    clear(true);
    return result;
}

void StaticMultiplexer::incoming(int stream, const SequenceValue &add, StreamSink *egress,
                                 bool discardIfNull)
{
    if (!egress) {
        incoming(stream, add, discardIfNull);
    } else {
        auto result = incoming(stream, add, true);
        if (!result.empty())
            egress->incomingData(std::move(result));
    }
}

void StaticMultiplexer::incoming(int stream, const SequenceValue::Transfer &add, StreamSink *egress,
                                 bool discardIfNull)
{
    if (!egress) {
        incoming(stream, add, discardIfNull);
    } else {
        auto result = incoming(stream, add, true);
        if (!result.empty())
            egress->incomingData(std::move(result));
    }
}


void StaticMultiplexer::incoming(int stream,
                               SequenceValue &&add, StreamSink *egress,
                               bool discardIfNull)
{
    if (!egress) {
        incoming(stream, std::move(add), discardIfNull);
    } else {
        auto result = incoming(stream, std::move(add), true);
        if (!result.empty())
            egress->incomingData(std::move(result));
    }
}

void StaticMultiplexer::incoming(int stream,
                                 SequenceValue::Transfer &&add, StreamSink *egress,
                                 bool discardIfNull)
{
    if (!egress) {
        incoming(stream, std::move(add), discardIfNull);
    } else {
        auto result = incoming(stream, std::move(add), true);
        if (!result.empty())
            egress->incomingData(std::move(result));
    }
}

void StaticMultiplexer::advance(int stream, double time, StreamSink *egress, bool discardIfNull)
{
    if (!egress) {
        advance(stream, time, discardIfNull);
    } else {
        auto result = advance(stream, time, true);
        if (!result.empty())
            egress->incomingData(std::move(result));
    }
}

void StaticMultiplexer::finish(StreamSink *egress)
{
    auto result = finish();
    if (egress && !result.empty())
        egress->incomingData(std::move(result));
}

void StaticMultiplexer::finish(int stream, StreamSink *egress, bool discardIfNull)
{
    if (!egress) {
        finish(stream, discardIfNull);
    } else {
        auto result = finish(stream, true);
        if (!result.empty())
            egress->incomingData(std::move(result));
    }
}

QDataStream &operator<<(QDataStream &stream, const StaticMultiplexer &mux)
{
    mux.backend.serialize(stream);
    Serialize::container(stream, mux.streams,
                         [&mux, &stream](StaticMultiplexer::Backend::Simple *s) {
        if (!s) {
            stream << static_cast<quint8>(0);
            return;
        }
        stream << static_cast<quint8>(1);
        s->serialize(stream);
    });
    stream << mux.outputQueued;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, StaticMultiplexer &mux)
{
    mux.backend.deserialize(stream);
    Deserialize::container(stream, mux.streams,
                           [&stream, &mux]() -> StaticMultiplexer::Backend::Simple * {
                               quint8 flag = 0;
                               stream >> flag;
                               if (flag == 0)
                                   return nullptr;
                               return mux.backend.deserializeSimple(stream);
                           });
    stream >> mux.outputQueued;
    return stream;
}

QDebug operator<<(QDebug stream, const StaticMultiplexer &mux)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "StaticMultiplexer({";
#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
    mux.backend.describe(stream, stream.verbosity() >= 4);
#else
    mux.backend.describe(stream);
#endif
    stream << '}';
    for (size_t i = 0; i < mux.streams.size(); i++) {
        if (i != 0) stream << ",";
        stream << i << '=';
        if (!mux.streams[i]) {
            stream << "[FINISHED]";
            continue;
        }
        stream << '}';
#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
        mux.streams[i]->describe(stream, stream.verbosity() >= 4);
#else
        mux.streams[i]->describe(stream);
#endif
        stream << '{';
    }
    stream << ')';
    return stream;
}

}
}
