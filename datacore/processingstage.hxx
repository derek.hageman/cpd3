/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACORE_PROCESSINGSTAGE_HXX
#define CPD3DATACORE_PROCESSINGSTAGE_HXX

#include "core/first.hxx"

#include <limits.h>
#include <mutex>
#include <condition_variable>
#include <QtGlobal>
#include <QDataStream>
#include <QList>

#include "datacore/datacore.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "stream.hxx"
#include "core/component.hxx"
#include "core/timeutils.hxx"

namespace CPD3 {
namespace Data {

namespace Archive {
class Access;
}

#ifndef NDEBUG
#define GENERALFILTER_STREAM_TIME_DEBUGGING
#endif

/**
 * An interface to a general type of data filter.  This type of filter supports
 * the general paradigm of a stream of input values producing a stream of
 * output values.  It must support the capacity to both accept input 
 * values like StreamSink and change the egress point like
 * StreamSource.  It must also support the capacity to terminate
 * safely via signalTerminate().
 * <br>
 * If the parent component indicates that it can be serialized it must also
 * provide an implementation of serialize( QDataStream & ) that calls the
 * base implementation first and a constructor that accepts a QDataStream to
 * create it from the serialized data (calling the parent constructor first).
 * Serialization will only occur after the filter has been stopped (via
 * setEgress(NULL) ).
 */
class CPD3DATACORE_EXPORT ProcessingStage : public StreamSink, public StreamSource {
public:
    ProcessingStage();

    /**
     * Create the filter from serialized data.
     */
    explicit ProcessingStage(QDataStream &stream);

    virtual ~ProcessingStage();

    /**
     * Returns any additional inputs requested by this filter.  They may not
     * be available in the final data stream, but this requests them from the
     * originating source if possible.  It is acceptable to request inputs
     * that are known to be provided (which has no effect).  This will
     * only be called before the thread is started.
     * 
     * @return a list of requested inputs
     */
    virtual SequenceName::Set requestedInputs();

    /**
     * Returns any additional outputs not in the input data stream that this
     * filter expects to generate.  It is acceptable to report producing 
     * outputs that do not actually exist in the final stream.  This will
     * only be called before the thread is started.
     * 
     * @return a list of predicted outputs
     */
    virtual SequenceName::Set predictedOutputs();

    /**
     * The base implementation of the serialization for the filter.  This must
     * be called first if it is overriden.
     * <br>
     * This requires that the output be paused (NULL egress).  It's also implied
     * that the thread will be terminated immediately after the call without
     * generating any more data (since that wouldn't make sense).
     * 
     * @param stream    the target data stream
     */
    virtual void serialize(QDataStream &stream);

    /**
     * Test if the filter has completed its execution.  Once this returns
     * true the filter may be destroyed at will.
     * 
     * @return true if the interface is finished
     */
    virtual bool isFinished() = 0;

    /**
     * Wait for completion of the filter.
     * 
     * @param timeout   the maximum time to wait
     * @return          true if the interface has completed
     */
    virtual bool wait(double timeout = FP::undefined()) = 0;

    /**
     * Create a tap that writes all data to a given file name before 
     * passing it on.  This is normally only used for debugging.
     * 
     * @param fileName  the file name to write to
     * @return          a new filter owned by the caller
     */
    static ProcessingStage *createTap(const QString &fileName);

    /**
     * Create a processing stage that does nothing but buffer and pass data through.
     *
     * @return          a new filter owned by the caller
     */
    static ProcessingStage *createNOOP();

    /**
     * Signals for the filter to terminate at the next possible safe point.
     */
    virtual void signalTerminate() = 0;

    /**
     * Start the filter execution.
     * <br>
     * The default implementation does nothing.
     */
    virtual void start();

    /**
     * Emitted when the thread has finished execution and a wait will not block
     * for longer than acquiring the mutex takes.
     */
    Threading::Signal<> finished;
};

/**
 * A standard implementation of a general filter that provides a dedicated 
 * thread simple, support for input buffering, egress switching, and 
 * termination signaling.
 * <br>
 * The user override functions process(const SequenceValue::Transfer &) and finish()
 * are called such that all access to interval values is thread safe with no
 * locks held.  That is, they can be treated as conventional serial code, as
 * long as they do not access anything that is otherwise shared.
 */
class CPD3DATACORE_EXPORT ProcessingStageThread : public ProcessingStage {
    SequenceValue::Transfer pending;
    bool dataEnded;

    enum class State {
        NotStarted, Running, Terminated, Completed,
    } state;

    std::thread thread;
    std::mutex mutex;
    std::mutex egressLock;
    std::condition_variable request;
    std::condition_variable response;

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
    double lastIncomingTime;
#endif

    void run();

    void stall(std::unique_lock<std::mutex> &lock);

public:
    virtual ~ProcessingStageThread();

    void setEgress(StreamSink *egress) override;

    void signalTerminate() override;

    void incomingData(const SequenceValue::Transfer &values) override;

    void incomingData(SequenceValue::Transfer &&values) override;

    void incomingData(const SequenceValue &value) override;

    void incomingData(SequenceValue &&value) override;

    void endData() override;

    void serialize(QDataStream &stream) override;

    bool isFinished() override;

    bool wait(double timeout = FP::undefined()) override;

    void start() override;

protected:
    /**
     * Create the filter base.
     */
    ProcessingStageThread();

    /**
     * Deserialize the filter base.
     * 
     * @param stream    the stream to deserialize from
     */
    explicit ProcessingStageThread(QDataStream &stream);

    /**
     * The current egress point.  Will always be valid when 
     * process( const SequenceValue::Transfer & ) and finish() are called.
     */
    StreamSink *egress;

    /**
     * Process some incoming data values.  This will only be called when
     * there is an available egress.
     * 
     * @param incoming  the incoming data
     */
    virtual void process(SequenceValue::Transfer &&incoming) = 0;

    /**
     * Called when the thread first begins regardless of egress presence.  This
     * is called without any locks held.
     * <br>
     * The default implementation does nothing and returns true.
     * 
     * @return false    to abort processing
     */
    virtual bool begin();

    /**
     * Called when data has ended and there is an available egress.  If there
     * was any pending data, process( const SequenceValue::Transfer & ) is called
     * first.  The default implementation calls egress->endData() and if it
     * is overridden that must be called.
     */
    virtual void finish();

    /**
     * Called when the thread is ending regardless of egress presence, etc.
     * This will NOT be called if begin returns false.  The lock is not held
     * during the call.
     * <br>
     * The default implementation does nothing and returns true.
     * 
     * @return true if the egress should be ended (if it wasn't already implicitly in a finish() call)
     */
    virtual bool finalize();
};

/**
 * A standard implementation of a general filter that provides utilities to
 * handle work.  This includes buffering of the data for processing, egress
 * switching, and termination handling.
 * <br>
 * The user override functions process(const SequenceValue::Transfer &) and finish()
 * are called such that all access to interval values is thread safe with no
 * locks held.  That is, they can be treated as conventional serial code, as
 * long as they do not access anything that is otherwise shared.
 */
class CPD3DATACORE_EXPORT AsyncProcessingStage : public ProcessingStage {
    SequenceValue::Transfer pending;
    bool dataEnded;

    enum class State {
        NotStarted, Running, Terminated, Completed,
    } state;

    std::thread thread;
    std::mutex mutex;
    std::mutex egressLock;
    std::condition_variable request;
    std::condition_variable response;

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
    double lastIncomingTime;
#endif

    void run();

    void stall(std::unique_lock<std::mutex> &lock);

public:
    virtual ~AsyncProcessingStage();

    void setEgress(StreamSink *egress) override;

    void signalTerminate() override;

    void incomingData(const SequenceValue::Transfer &values) override;

    void incomingData(SequenceValue::Transfer &&values) override;

    void incomingData(const SequenceValue &value) override;

    void incomingData(SequenceValue &&value) override;

    void endData() override;

    void serialize(QDataStream &stream) override;

    bool isFinished() override;

    bool wait(double timeout = FP::undefined()) override;

    void start() override;

protected:
    /**
     * Create the filter.
     */
    AsyncProcessingStage();

    /**
     * Deserialize the filter.
     * 
     * @param stream    the stream to deserialize from
     */
    explicit AsyncProcessingStage(QDataStream &stream);

    /**
     * The current egress point.  Will always be valid when 
     * process( const SequenceValue::Transfer & ) and finish() are called.
     */
    StreamSink *egress;

    /**
     * Prepare for processing.  This is called with the mutex held on the
     * values that are newly incoming.  If this returns false the values
     * remain in the shared buffer and no processing is done unless we're
     * at the end of the stream.
     * <br>
     * The default implementation returns true if the buffer is not empty.
     * 
     * @param incoming      the entire incoming buffer
     * @return              true to process the data
     */
    virtual bool prepare(const SequenceValue::Transfer &incoming);

    /**
     * Process some incoming data values.  This will only be called when
     * there is an available egress (non-NULL).
     * 
     * @param incoming  the incoming data
     */
    virtual void process(SequenceValue::Transfer &&incoming) = 0;

    /**
     * Test if the final output has been delivered to the egress.  This is
     * called when the input has ended and there is an egress.  If it returns
     * true then the normal egress completion begins.
     * <br>
     * The default implementation always returns true.
     * 
     * @return  true to end the output
     */
    virtual bool outputFinal();

    /**
     * Continue the processing loop.  This is called without the mutex held
     * after processing if this is not the end of data.  If this returns false
     * then processing is stopped and the end function called.
     * <br>
     * The default implementation always returns true.
     * 
     * @return  true to continue processing
     */
    virtual bool continueProcessing();

    /**
     * Called when data has ended and there is an available egress.  If there
     * was any pending data, process( const SequenceValue::Transfer & ) is called
     * first.  The default implementation calls egress->endData() and if it
     * is overridden that must be called.
     */
    virtual void finish();

    /**
     * Called when the thread is ending regardless of egress presence, etc.
     * The lock is not held during the call.
     * <br>
     * The default implementation does nothing and returns true.
     * 
     * @return true if the egress should be ended (if it wasn't already implicitly in a finish() call)
     */
    virtual bool finalize();

    /**
     * Notify the worker of an update.  This must be called whenever 
     * prepare(const SequenceValue::Transfer &) might return true.
     */
    void notifyUpdate();

    /**
     * Test if the filter has a terminate request pending.  This can only
     * be used from within the process function and should abort it when
     * it returns true.  Calling this is optional as the terminate will be
     * handled the next time the process function returns.
     * 
     * @return  true if the filter should terminate processing
     */
    bool terminateRequested();
};

/**
 * The component interface for a general filter object.  This allows it to be
 * loaded as a Qt plugin and instances of it created as needed.
 */
class CPD3DATACORE_EXPORT ProcessingStageComponent {
public:
    virtual ~ProcessingStageComponent();

    /**
     * Get an options object (set to all defaults) for this filter type.
     * 
     * @return an options object for this filter type
     */
    virtual ComponentOptions getOptions();

    /**
     * Gets a list of examples that show usage of the component.  The caller
     * will use the specified options to construct usage in the context it's
     * called (e.x. command lines).
     * 
     * @return a list in display order of examples
     */
    virtual QList<ComponentExample> getExamples();

    /**
     * Create a filter object from the given options to work on a fully
     * dynamic stream.  This is used for command line chain construction.
     * <br>
     * This allocates the object with new, so the caller must delete it when 
     * finished.
     * 
     * @param options   the options to use
     * @return          a newly allocated filter object
     */
    virtual ProcessingStage *createGeneralFilterDynamic(const ComponentOptions &options = {}) = 0;

    /**
     * Create a filter object from the given options and expected predefined
     * inputs.  This is used for GUI filter chain construction.
     * <br>
     * This allocates the object with new, so the caller must delete it when 
     * finished.
     * 
     * @param options   the options to use
     * @param start     the start of the data
     * @param end       the end of the data
     * @param inputs    the expected inputs
     * @return          a newly allocated filter object
     */
    virtual ProcessingStage *createGeneralFilterPredefined(const ComponentOptions &options,
                                                           double start,
                                                           double end,
                                                           const QList<SequenceName> &inputs);

    /**
     * Create a filter object for use as a component of an editing stream.
     * <br>
     * This allocates the object with new, so the caller must delete it when 
     * finished.
     * 
     * @param start     the start of the data
     * @param end       the end of the data
     * @param station   the station
     * @param archive   the input archive (usually raw)
     * @param config    the configuration data
     * @return          a newly allocated filter object
     */
    virtual ProcessingStage *createGeneralFilterEditing(double start,
                                                        double end,
                                                        const SequenceName::Component &station,
                                                        const SequenceName::Component &archive,
                                                        const ValueSegment::Transfer &config);

    /**
     * Called before creation of a filter object to allow an extension request
     * to editing chain.  This allows for the request of more edited data than
     * was requested at the editing level.  For example, this can be used to
     * request the start of the current PSAP filter for integrating 
     * corrections.
     * 
     * @param start     the input and output start of the request
     * @param end       the input and output start of the request
     * @param station   the station
     * @param archive   the input archive (usually raw)
     * @param config    the configuration data
     * @param access    the existing archive access, if any
     */
    virtual void extendGeneralFilterEditing(double &start,
                                            double &end,
                                            const SequenceName::Component &station,
                                            const SequenceName::Component &archive,
                                            const ValueSegment::Transfer &config,
                                            Archive::Access *access = nullptr);

    /**
     * Create a filter object from a serialized copy of it.  This is used
     * for the acceleration mechanisms for command line invocation.  The
     * containing filter handler MUST be de-serialized to a compatible state 
     * from when the original filter was serialized.
     * <br>
     * This allocates the object with new, so the caller must delete it when 
     * finished.
     * @param stream    the stream containing the serialized copy
     * @return          a newly allocated filter object
     */
    virtual ProcessingStage *deserializeGeneralFilter(QDataStream &stream);

    /**
     * Returns the global name to use for serialization or empty if the
     * filter cannot be serialized.
     *
     * @return the global ID string
     */
    virtual QString getGeneralSerializationName() const;
};

}
}

Q_DECLARE_INTERFACE(CPD3::Data::ProcessingStageComponent, "CPD3.Data.ProcessingStageComponent/1.0");

#endif
