/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>
#include <QLoggingCategory>

#include "datacore/processingstage.hxx"
#include "core/threadpool.hxx"
#include "core/util.hxx"


Q_LOGGING_CATEGORY(log_datacore_processingstage, "cpd3.datacore.processingstage", QtWarningMsg)


namespace CPD3 {
namespace Data {

ProcessingStage::ProcessingStage()
{ }

ProcessingStage::~ProcessingStage() = default;

ProcessingStage::ProcessingStage(QDataStream &)
{ }

SequenceName::Set ProcessingStage::requestedInputs()
{ return {}; }

SequenceName::Set ProcessingStage::predictedOutputs()
{ return {}; }

void ProcessingStage::serialize(QDataStream &)
{ }

void ProcessingStage::start()
{ }

ProcessingStage *ProcessingStage::createTap(const QString &fileName)
{
    QObject *component = ComponentLoader::create("tap");
    if (component == NULL) {
        qCWarning(log_datacore_processingstage) << "Can't load tap component";
        return nullptr;
    }
    ProcessingStageComponent *filterComponent;
    if (!(filterComponent = qobject_cast<ProcessingStageComponent *>(component))) {
        qCWarning(log_datacore_processingstage) << "Tap component is not a general filter";
        return nullptr;
    }

    ComponentOptions options(filterComponent->getOptions());
    ComponentOptionFile *file = qobject_cast<ComponentOptionFile *>(options.get("file"));
    if (!file) {
        qCWarning(log_datacore_processingstage) << "Can't set tap output file";
    } else {
        file->set(fileName);
    }

    ProcessingStage *filter = filterComponent->createGeneralFilterDynamic(options);
    if (!filter) {
        qCWarning(log_datacore_processingstage) << "Can't create tap filter";
        return nullptr;
    }
    return filter;
}

ProcessingStage *ProcessingStage::createNOOP()
{
    class NOOP : public ProcessingStageThread {
    public:
        NOOP() = default;

        virtual ~NOOP() = default;

    protected:
        virtual void process(SequenceValue::Transfer &&incoming)
        {
            egress->incomingData(std::move(incoming));
        }
    };
    return new NOOP();
}


ProcessingStageThread::ProcessingStageThread() : dataEnded(false),
                                                 state(State::NotStarted),
                                                 mutex(),
                                                 egressLock(),
#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
                                                 lastIncomingTime(FP::undefined()),
#endif
                                                 egress(nullptr)
{ }

ProcessingStageThread::ProcessingStageThread(QDataStream &stream) : ProcessingStage(stream),
                                                                    dataEnded(false),
                                                                    state(State::NotStarted),
#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
                                                                    lastIncomingTime(
                                                                            FP::undefined()),
#endif
                                                                    egress(nullptr)
{
    stream >> pending;
    stream >> dataEnded;
}

void ProcessingStageThread::serialize(QDataStream &stream)
{
    ProcessingStage::serialize(stream);

    std::lock_guard<std::mutex> lock(mutex);
    std::lock_guard<std::mutex> egressLocker(egressLock);
    Q_ASSERT(!egress);
    stream << pending;
    stream << dataEnded;
}

ProcessingStageThread::~ProcessingStageThread()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        state = State::Terminated;
    }
    request.notify_all();
    if (thread.joinable())
        thread.join();
}

void ProcessingStageThread::setEgress(StreamSink *set)
{
    std::lock_guard<std::mutex> lock(mutex);
    std::lock_guard<std::mutex> egressLocker(egressLock);
    this->egress = set;
    request.notify_all();
}

void ProcessingStageThread::signalTerminate()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (state != State::Completed)
        state = State::Terminated;
    request.notify_all();
}

void ProcessingStageThread::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
    for (auto v = values.cbegin() + 1, endV = values.cend(); v != endV; ++v) {
        Q_ASSERT(Range::compareStart((v - 1)->getStart(), v->getStart()) <= 0);
    }
#endif

    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Q_ASSERT(!dataEnded);

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(lastIncomingTime, values.front().getStart()) <= 0);
        lastIncomingTime = values.back().getStart();
        if (!pending.empty()) {
            Q_ASSERT(
                    Range::compareStart(pending.back().getStart(), values.front().getStart()) <= 0);
        }
#endif
        Util::append(values, pending);
    }

    request.notify_all();
}

void ProcessingStageThread::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
    for (auto v = values.cbegin() + 1, endV = values.cend(); v != endV; ++v) {
        Q_ASSERT(Range::compareStart((v - 1)->getStart(), v->getStart()) <= 0);
    }
#endif

    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Q_ASSERT(!dataEnded);

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(lastIncomingTime, values.front().getStart()) <= 0);
        lastIncomingTime = values.back().getStart();
        if (!pending.empty()) {
            Q_ASSERT(
                    Range::compareStart(pending.back().getStart(), values.front().getStart()) <= 0);
        }
#endif
        Util::append(std::move(values), pending);
    }

    request.notify_all();
}

void ProcessingStageThread::incomingData(const SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Q_ASSERT(!dataEnded);

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(lastIncomingTime, value.getStart()) <= 0);
        lastIncomingTime = value.getStart();
        Q_ASSERT(pending.empty() ||
                         Range::compareStart(pending.back().getStart(), value.getStart()) <= 0);
#endif
        pending.emplace_back(value);
    }

    request.notify_all();
}

void ProcessingStageThread::incomingData(SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Q_ASSERT(!dataEnded);

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(lastIncomingTime, value.getStart()) <= 0);
        lastIncomingTime = value.getStart();
        Q_ASSERT(pending.empty() ||
                         Range::compareStart(pending.back().getStart(), value.getStart()) <= 0);
#endif
        pending.emplace_back(std::move(value));
    }

    request.notify_all();
}

void ProcessingStageThread::endData()
{
    std::lock_guard<std::mutex> lock(mutex);
    dataEnded = true;
    request.notify_all();
}

void ProcessingStageThread::stall(std::unique_lock<std::mutex> &lock)
{
    while (pending.size() > stallThreshold) {
        switch (state) {
        case State::NotStarted:
        case State::Running:
            break;
        case State::Terminated:
        case State::Completed:
            return;
        }
        response.wait(lock);
    }
}

void ProcessingStageThread::run()
{
    if (!begin()) {
        {
            std::unique_lock<std::mutex> lock(mutex);
            std::unique_lock<std::mutex> egressLocker(egressLock);
            state = State::Completed;
            if (egress) {
                lock.unlock();
                egress->endData();
            }
        }
        response.notify_all();
        finished();
        return;
    }

    for (;;) {
        bool finishData;
        SequenceValue::Transfer incoming;

        std::unique_lock<std::mutex> lock(mutex);
        std::unique_lock<std::mutex> egressLocker(egressLock);
        for (;;) {
            if (state == State::Terminated) {
                lock.unlock();

                if (finalize()) {
                    if (egress) {
                        egress->endData();
                    }
                }

                lock.lock();
                state = State::Completed;
                lock.unlock();
                response.notify_all();
                finished();
                return;
            }

            if (!egress) {
                egressLocker.unlock();
                request.wait(lock);
                egressLocker.lock();
                continue;
            }

            incoming = std::move(pending);
            pending.clear();
            finishData = dataEnded;

            if (!finishData && incoming.empty()) {
                egressLocker.unlock();
                request.wait(lock);
                egressLocker.lock();
                continue;
            }
            break;
        }
        lock.unlock();

        if (incoming.size() > stallThreshold)
            response.notify_all();

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
        if (!incoming.empty()) {
            for (auto v = incoming.cbegin() + 1, endV = incoming.cend(); v != endV; ++v) {
                Q_ASSERT(Range::compareStart((v - 1)->getStart(), v->getStart()) <= 0);
            }
        }
#endif

        process(std::move(incoming));
        if (finishData) {
            finish();
            finalize();

            egressLocker.unlock();
            lock.lock();
            state = State::Completed;
            lock.unlock();
            response.notify_all();
            finished();
            return;
        }
    }
}

void ProcessingStageThread::finish()
{ egress->endData(); }

bool ProcessingStageThread::begin()
{ return true; }

bool ProcessingStageThread::finalize()
{ return true; }

bool ProcessingStageThread::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == State::Completed;
}

bool ProcessingStageThread::wait(double timeout)
{
#ifndef NDEBUG
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(state != State::NotStarted);
    }
#endif
    return finished.wait(std::bind(&ProcessingStageThread::isFinished, this), timeout);
}

void ProcessingStageThread::start()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        switch (state) {
        case State::NotStarted:
            state = State::Running;
            break;
        case State::Running:
        case State::Completed:
            Q_ASSERT(false);
            return;
        case State::Terminated:
            break;
        }
    }
    thread = std::thread(std::bind(&ProcessingStageThread::run, this));
}


AsyncProcessingStage::AsyncProcessingStage() : dataEnded(false), state(State::NotStarted),
#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
                                               lastIncomingTime(FP::undefined()),
#endif
                                               egress(nullptr)
{ }

AsyncProcessingStage::~AsyncProcessingStage()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        state = State::Terminated;
    }
    request.notify_all();
    if (thread.joinable())
        thread.join();
}

AsyncProcessingStage::AsyncProcessingStage(QDataStream &stream) : ProcessingStage(stream),
                                                                  dataEnded(false),
                                                                  state(State::NotStarted),
#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
                                                                  lastIncomingTime(FP::undefined()),
#endif
                                                                  egress(nullptr)
{
    stream >> pending;
    stream >> dataEnded;
}

void AsyncProcessingStage::serialize(QDataStream &stream)
{
    ProcessingStage::serialize(stream);

    std::lock_guard<std::mutex> lock(mutex);
    std::lock_guard<std::mutex> egressLocker(egressLock);
    Q_ASSERT(egress == nullptr);
    stream << pending;
    stream << dataEnded;
}

void AsyncProcessingStage::run()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!lock)
            lock.lock();

        std::unique_lock<std::mutex> egressLocker(egressLock);
        bool localEnded = false;
        SequenceValue::Transfer toProcess;
        switch (state) {
        case State::NotStarted:
        case State::Completed:
            Q_ASSERT(false);
            return;

        case State::Terminated: {
            lock.unlock();

            if (finalize()) {
                if (egress) {
                    egress->endData();
                }
            }

            egressLocker.unlock();
            lock.lock();
            state = State::Completed;
            pending.clear();
            lock.unlock();
            response.notify_all();
            finished();
            return;
        }

        case State::Running: {
            if (!egress) {
                egressLocker.unlock();
                request.wait(lock);
                continue;
            }

            if (prepare(pending) || dataEnded) {
                localEnded = dataEnded;
                toProcess = std::move(pending);
                pending.clear();
                break;
            }

            egressLocker.unlock();
            request.wait(lock);
            continue;
        }
        }
        lock.unlock();

        if (toProcess.size() > stallThreshold)
            response.notify_all();

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
        if (!toProcess.empty()) {
            for (auto v = toProcess.cbegin() + 1, endV = toProcess.cend(); v != endV; ++v) {
                Q_ASSERT(Range::compareStart((v - 1)->getStart(), v->getStart()) <= 0);
            }
        }
#endif

        process(std::move(toProcess));

        if ((localEnded && outputFinal()) || !continueProcessing()) {
            finish();
            finalize();

            egressLocker.unlock();
            lock.lock();
            state = State::Completed;
            pending.clear();
            lock.unlock();
            response.notify_all();
            finished();
            return;
        }
    }
}

bool AsyncProcessingStage::wait(double timeout)
{
#ifndef NDEBUG
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(state != State::NotStarted);
    }
#endif
    return finished.wait(std::bind(&AsyncProcessingStage::isFinished, this), timeout);
}

void AsyncProcessingStage::start()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        switch (state) {
        case State::NotStarted:
            state = State::Running;
            break;
        case State::Running:
        case State::Completed:
            Q_ASSERT(false);
            return;
        case State::Terminated:
            break;
        }
    }
    thread = std::thread(std::bind(&AsyncProcessingStage::run, this));
}

bool AsyncProcessingStage::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == State::Completed;
}

bool AsyncProcessingStage::terminateRequested()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == State::Terminated;
}

void AsyncProcessingStage::setEgress(StreamSink *set)
{
    std::lock_guard<std::mutex> lock(mutex);
    std::lock_guard<std::mutex> egressLocker(egressLock);
    this->egress = set;
    request.notify_all();
}

void AsyncProcessingStage::signalTerminate()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (state != State::Completed)
        state = State::Terminated;
    request.notify_all();
}

void AsyncProcessingStage::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;

    std::unique_lock<std::mutex> lock(mutex);
    stall(lock);
    Q_ASSERT(!dataEnded);

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
    Q_ASSERT(Range::compareStart(lastIncomingTime, values.front().getStart()) <= 0);
    lastIncomingTime = values.back().getStart();
    for (auto v = values.cbegin() + 1, endV = values.cend(); v != endV; ++v) {
        Q_ASSERT(Range::compareStart((v - 1)->getStart(), v->getStart()) <= 0);
    }
    if (!pending.empty()) {
        Q_ASSERT(Range::compareStart(pending.back().getStart(), values.front().getStart()) <= 0);
    }
#endif
    Util::append(values, pending);
    notifyUpdate();
}

void AsyncProcessingStage::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;

    std::unique_lock<std::mutex> lock(mutex);
    stall(lock);
    Q_ASSERT(!dataEnded);

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
    Q_ASSERT(Range::compareStart(lastIncomingTime, values.front().getStart()) <= 0);
    lastIncomingTime = values.back().getStart();
    for (auto v = values.cbegin() + 1, endV = values.cend(); v != endV; ++v) {
        Q_ASSERT(Range::compareStart((v - 1)->getStart(), v->getStart()) <= 0);
    }
    if (!pending.empty()) {
        Q_ASSERT(Range::compareStart(pending.back().getStart(), values.front().getStart()) <= 0);
    }
#endif
    Util::append(std::move(values), pending);
    notifyUpdate();
}

void AsyncProcessingStage::incomingData(const SequenceValue &value)
{
    std::unique_lock<std::mutex> lock(mutex);
    stall(lock);
    Q_ASSERT(!dataEnded);

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
    Q_ASSERT(Range::compareStart(lastIncomingTime, value.getStart()) <= 0);
    lastIncomingTime = value.getStart();
    if (!pending.empty()) {
        Q_ASSERT(Range::compareStart(pending.back().getStart(), value.getStart()) <= 0);
    }
#endif

    pending.emplace_back(value);

    notifyUpdate();
}

void AsyncProcessingStage::incomingData(SequenceValue &&value)
{
    std::unique_lock<std::mutex> lock(mutex);
    stall(lock);
    Q_ASSERT(!dataEnded);

#ifdef GENERALFILTER_STREAM_TIME_DEBUGGING
    Q_ASSERT(Range::compareStart(lastIncomingTime, value.getStart()) <= 0);
    lastIncomingTime = value.getStart();
    if (!pending.empty()) {
        Q_ASSERT(Range::compareStart(pending.back().getStart(), value.getStart()) <= 0);
    }
#endif

    pending.emplace_back(std::move(value));

    notifyUpdate();
}

void AsyncProcessingStage::endData()
{
    std::lock_guard<std::mutex> lock(mutex);
    dataEnded = true;
    notifyUpdate();
}

void AsyncProcessingStage::stall(std::unique_lock<std::mutex> &lock)
{
    while (pending.size() > stallThreshold) {
        switch (state) {
        case State::NotStarted:
        case State::Running:
            break;
        case State::Terminated:
        case State::Completed:
            return;
        }
        response.wait(lock);
    }
}

void AsyncProcessingStage::notifyUpdate()
{ request.notify_all(); }

bool AsyncProcessingStage::prepare(const SequenceValue::Transfer &incoming)
{ return !incoming.empty(); }

bool AsyncProcessingStage::continueProcessing()
{ return true; }

void AsyncProcessingStage::finish()
{ egress->endData(); }

bool AsyncProcessingStage::finalize()
{ return true; }

bool AsyncProcessingStage::outputFinal()
{ return true; }


ProcessingStageComponent::~ProcessingStageComponent() = default;

ComponentOptions ProcessingStageComponent::getOptions()
{ return ComponentOptions(); }

QList<ComponentExample> ProcessingStageComponent::getExamples()
{ return QList<ComponentExample>(); }

ProcessingStage *ProcessingStageComponent::createGeneralFilterPredefined(const ComponentOptions &options,
                                                                         double,
                                                                         double,
                                                                         const QList<SequenceName> &)
{ return createGeneralFilterDynamic(options); }

ProcessingStage *ProcessingStageComponent::createGeneralFilterEditing(double,
                                                                      double,
                                                                      const SequenceName::Component &,
                                                                      const SequenceName::Component &,
                                                                      const ValueSegment::Transfer &)
{ return createGeneralFilterDynamic(); }

ProcessingStage *ProcessingStageComponent::deserializeGeneralFilter(QDataStream &)
{
    Q_ASSERT(false);
    return nullptr;
}

void ProcessingStageComponent::extendGeneralFilterEditing(double &,
                                                          double &,
                                                          const SequenceName::Component &,
                                                          const SequenceName::Component &,
                                                          const ValueSegment::Transfer &,
                                                          Archive::Access *)
{ }

QString ProcessingStageComponent::getGeneralSerializationName() const
{ return {}; }


}
}
