/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3DATACORE_EXTERNALSINK_HXX
#define CPD3DATACORE_EXTERNALSINK_HXX

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <QObject>

#include "datacore/datacore.hxx"
#include "datacore/stream.hxx"
#include "core/actioncomponent.hxx"
#include "io/access.hxx"

namespace CPD3 {
namespace Data {

/**
 * The abstract interface to output data to a given source.  This does not
 * operate as a thread since access to a QIODevice must be done from within
 * the thread that created the device.
 */
class CPD3DATACORE_EXPORT ExternalSink : public StreamSink {
public:
    ExternalSink();

    virtual ~ExternalSink();

    /**
     * Set the acceleration key for this data source.  Must be done before
     * the first step call.
     * 
     * @param key   the key to output
     */
    virtual void setAccelerationKey(const std::string &key);

    /**
     * Normalize unicode, replacing "equivlant" characters with their ASCII variants
     * (e.g. replace U2028 with \n).
     *
     * @param str   the input and output string
     */
    static void normalizeUnicode(QString &str);

    /**
     * Simplify some common unicode contents in the given string.  This
     * will not remove all unicode from the string, it just replaces some
     * common (mostly units) contents with ASCII "equivalents".
     * 
     * @param str   the input and output string
     */
    static void simplifyUnicode(QString &str);

    /**
     * Test if the output has finished execution.
     *
     * @return  true if the input has finished
     */
    virtual bool isFinished() = 0;

    /**
     * Launch the data output.
     */
    virtual void start() = 0;

    /**
     * Wait for the input to finish execution.
     * <br>
     * The default implementation does a polling loop on isFinished()
     *
     * @param timeout   the maximum time to wait
     * @return          true if the input has finished
     */
    virtual bool wait(double timeout = FP::undefined());

    /**
     * Flush any long running buffers.  This is optional (both in calling
     * and implementation).
     * <br>
     * The default implementation does nothing.
     */
    virtual void flushData();

    /**
     * Signals termination of the output.  This is usually used to unblock
     * any incoming data if required.
     * <br>
     * The default implementation does nothing.
     */
    virtual void signalTerminate();

    ActionFeedback::Source feedback;

    /**
     * Emitted when the output has finished executing.
     */
    Threading::Signal<> finished;
};

/**
 * Provides an interface to write data to an output source.
 */
class CPD3DATACORE_EXPORT StandardDataOutput : public ExternalSink {
public:
    /**
     * Specifies the type of output encoding to be written.
     */
    enum class OutputType {
        /**
         * Directly encoded data in base 64.  This is the "standard" output
         * format.
         */
                Direct,

        /**
         * Same as Direct, but using legacy serialization.
         */
                Direct_Legacy,

        /**
         * Data encoded in XML.
         */
                XML,

        /**
         * Data encoded in JSON.
         */
                JSON,

        /**
         * A raw binary data stream.  Not suitable for terminal output.
         */
                Raw,

        /**
         * Same as RAW but using legacy serialization.
         */
                Raw_Legacy
    };
private:

    enum class State {
        Initialize, Running, EndPending, TerminateRequested, Complete
    } state;

    SequenceValue::Transfer pending;
    bool outputStallRequested;
    std::mutex mutex;
    std::condition_variable notify;

    OutputType mode;

    std::string accelerationKey;

    std::unique_ptr<IO::Generic::Stream> device;

    SequenceName::Map<std::uint_fast16_t> forwardIndexer;
    std::vector<SequenceName> reverseIndexer;
    std::uint_fast16_t nextIndex;

    std::thread thread;
    std::unique_ptr<QThread> qthread;

    void outputBasicSerialization(const QByteArray &input);

    void runBasicSerialization();

    void runXML();

    void runJSON();

    void stall(std::unique_lock<std::mutex> &lock);

    void updateOutputStall(bool stalled);

public:

    /**
     * Create a new data output handler.  The ingress point functions are thread
     * safe, but the others are not.
     *
     * @param setDevice     the output device
     * @param outputType    the type of output to produce
     * @param parent        the parent object
     */
    StandardDataOutput(std::unique_ptr<IO::Generic::Stream> &&device,
                       OutputType outputType = OutputType::Direct);

    StandardDataOutput(QIODevice *device, OutputType outputType = OutputType::Direct);

    virtual ~StandardDataOutput();

    void setAccelerationKey(const std::string &key) override;

    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

    void incomingData(const CPD3::Data::SequenceValue &value) override;

    void incomingData(CPD3::Data::SequenceValue &&value) override;

    void endData() override;

    bool isFinished() override;

    void start() override;

    bool wait(double timeout = FP::undefined()) override;

    void signalTerminate() override;
};


/**
 * An interface for a component that represents an egress point for data.
 * Generally this accepts a stream of incoming values and outputs them to
 * some device.
 */
class CPD3DATACORE_EXPORT ExternalSinkComponent {
public:
    virtual ~ExternalSinkComponent();

    /**
     * Returns true if this egress point requires a valid device to
     * operate on (the default).  This should return false for things that
     * can only ever output to a fixed location (for example, archive writing).
     *
     * @return true if this component does not use a device
     */
    virtual bool requiresOutputDevice();

    /**
     * Get an options object (set to all defaults) for this egress point.
     *
     * @return an options object for this filter type
     */
    virtual ComponentOptions getOptions();

    /**
     * Gets a list of examples that show usage of the component.  The caller
     * will use the specified options to construct usage in the context it's
     * called (e.x. command lines).
     *
     * @return a list in display order of examples
     */
    virtual QList<ComponentExample> getExamples();

    /**
     * Creates an output sink.
     *
     * @param stream    the stream to write to or null if requiresOutputDevice() is false
     * @param options   the options for the output
     * @return          a sink for writing to the stream
     */
    virtual ExternalSink *createDataSink(std::unique_ptr<IO::Generic::Stream> &&stream = {},
                                         const ComponentOptions &options = {}) = 0;

    ExternalSink *createDataEgress(QIODevice *device, const ComponentOptions &options = {});
};

}
}

Q_DECLARE_INTERFACE(CPD3::Data::ExternalSinkComponent, "CPD3.Data.ExternalSinkComponent/1.0");

#endif
