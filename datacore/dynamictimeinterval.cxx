/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QDebugStateSaver>

#include "core/timeutils.hxx"
#include "core/timeparse.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "dynamictimeinterval.hxx"
#include "variant/composite.hxx"

namespace CPD3 {
namespace Data {

enum TimeIntervalType {
    TI_NULL = 0, TI_UNDEFINED, TI_NONE, TI_CONSTANT, TI_RANGE
};

DynamicTimeInterval::~DynamicTimeInterval() = default;

QString DynamicTimeInterval::describe(double) const
{ return QString(); }

bool DynamicTimeInterval::constantOffsetDescription(Time::LogicalTimeUnit *, int *, bool *) const
{ return false; }

QSet<double> DynamicTimeInterval::getChangedPoints() const
{ return QSet<double>(); }

void DynamicTimeInterval::printLog(QDebug &stream) const
{ stream << "DynamicTimeInterval(Unknown)"; }

double DynamicTimeInterval::round(double time, double origin, bool roundUp)
{
    origin = apply(time, origin, roundUp);
    return apply(time, origin, roundUp, true);
}

double DynamicTimeInterval::roundConst(double time, double origin, bool roundUp) const
{
    origin = applyConst(time, origin, roundUp);
    return applyConst(time, origin, roundUp, true);
}

DynamicTimeInterval::Undefined::Undefined() = default;

DynamicTimeInterval::Undefined::~Undefined() = default;

double DynamicTimeInterval::Undefined::apply(double, double, bool, int)
{ return FP::undefined(); }

double DynamicTimeInterval::Undefined::applyConst(double, double, bool, int) const
{ return FP::undefined(); }

double DynamicTimeInterval::Undefined::round(double, double, bool)
{ return FP::undefined(); }

double DynamicTimeInterval::Undefined::roundConst(double, double, bool) const
{ return FP::undefined(); }

DynamicTimeInterval *DynamicTimeInterval::Undefined::clone() const
{ return new DynamicTimeInterval::Undefined(); }

QString DynamicTimeInterval::Undefined::describe(double) const
{ return QString("Undefined"); }

bool DynamicTimeInterval::Undefined::constantOffsetDescription(Time::LogicalTimeUnit *unit,
                                                               int *count,
                                                               bool *aligned) const
{ return false; }

DynamicTimeInterval::Undefined::Undefined(QDataStream &)
{ }

void DynamicTimeInterval::Undefined::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) TI_UNDEFINED;
    stream << t;
}

void DynamicTimeInterval::Undefined::printLog(QDebug &stream) const
{ stream << "DynamicTimeInterval::Undefined()"; }

DynamicTimeInterval::None::None() = default;

DynamicTimeInterval::None::~None() = default;

double DynamicTimeInterval::None::apply(double, double origin, bool, int)
{ return origin; }

double DynamicTimeInterval::None::applyConst(double, double origin, bool, int) const
{ return origin; }

double DynamicTimeInterval::None::round(double, double origin, bool)
{ return origin; }

double DynamicTimeInterval::None::roundConst(double, double origin, bool) const
{ return origin; }

DynamicTimeInterval *DynamicTimeInterval::None::clone() const
{ return new DynamicTimeInterval::None(); }

QString DynamicTimeInterval::None::describe(double) const
{ return QString("0"); }

bool DynamicTimeInterval::None::constantOffsetDescription(Time::LogicalTimeUnit *unit,
                                                          int *count,
                                                          bool *aligned) const
{
    if (unit) *unit = Time::Second;
    if (count) *count = 0;
    if (aligned) *aligned = false;
    return true;
}

DynamicTimeInterval::None::None(QDataStream &)
{ }

void DynamicTimeInterval::None::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(TI_NONE);
}

void DynamicTimeInterval::None::printLog(QDebug &stream) const
{ stream << "DynamicTimeInterval::None()"; }

DynamicTimeInterval::Constant::Constant(Time::LogicalTimeUnit setUnit,
                                                             int setCount,
                                                             bool setAligned) : unit(setUnit),
                                                                                count(setCount),
                                                                                aligned(setAligned)
{ }

DynamicTimeInterval::Constant::~Constant() = default;

double DynamicTimeInterval::Constant::apply(double, double origin, bool roundUp, int multiplier)
{
    return Time::logical(origin, unit, count, aligned, roundUp, multiplier);
}

double DynamicTimeInterval::Constant::applyConst(double,
                                                 double origin,
                                                 bool roundUp,
                                                 int multiplier) const
{
    return Time::logical(origin, unit, count, aligned, roundUp, multiplier);
}

double DynamicTimeInterval::Constant::round(double, double origin, bool roundUp)
{
    if (!aligned) return origin;
    if (roundUp)
        return Time::ceil(origin, unit, count);
    else
        return Time::floor(origin, unit, count);
}

double DynamicTimeInterval::Constant::roundConst(double, double origin, bool roundUp) const
{
    if (!aligned) return origin;
    if (roundUp)
        return Time::ceil(origin, unit, count);
    else
        return Time::floor(origin, unit, count);
}

DynamicTimeInterval *DynamicTimeInterval::Constant::clone() const
{ return new DynamicTimeInterval::Constant(unit, count, aligned); }

QString DynamicTimeInterval::Constant::describe(double) const
{
    return Time::describeOffset(unit, count, aligned);
}

bool DynamicTimeInterval::Constant::constantOffsetDescription(Time::LogicalTimeUnit *unit,
                                                              int *count,
                                                              bool *aligned) const
{
    if (unit) *unit = this->unit;
    if (count) *count = this->count;
    if (aligned) *aligned = this->aligned;
    return true;
}

DynamicTimeInterval::Constant::Constant(QDataStream &stream)
{
    quint8 u;
    stream >> u;
    unit = (Time::LogicalTimeUnit) u;
    qint32 c;
    stream >> c;
    count = (int) c;
    stream >> aligned;
}

void DynamicTimeInterval::Constant::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(TI_CONSTANT);
    stream << static_cast<quint8>(unit);
    stream << static_cast<qint32>(count);
    stream << aligned;
}

void DynamicTimeInterval::Constant::printLog(QDebug &stream) const
{
    stream << "DynamicTimeInterval::Constant("
           << Time::describeOffset(unit, count, aligned).toUtf8().constData() << ")";
}


DynamicTimeInterval::Variable::UnitSetAligned::UnitSetAligned(double setStart,
                                                              double setEnd,
                                                              bool setSet) : start(setStart),
                                                                          end(setEnd),
                                                                          set(setSet)
{ }

DynamicTimeInterval::Variable::UnitSetAligned::UnitSetAligned(const UnitSetAligned &) = default;

DynamicTimeInterval::Variable::UnitSetAligned &DynamicTimeInterval::Variable::UnitSetAligned::operator=(
        const UnitSetAligned &) = default;

DynamicTimeInterval::Variable::UnitSetUndefined::UnitSetUndefined(double setStart,
                                                               double setEnd,
                                                               bool setSet) : start(setStart),
                                                                              end(setEnd),
                                                                              set(setSet)
{ }

DynamicTimeInterval::Variable::UnitSetUndefined::UnitSetUndefined(const UnitSetUndefined &other) = default;

DynamicTimeInterval::Variable::UnitSetUndefined &DynamicTimeInterval::Variable::UnitSetUndefined::operator=(
        const UnitSetUndefined &other)
{
    if (&other == this) return *this;
    start = other.start;
    end = other.end;
    set = other.set;
    return *this;
}

DynamicTimeInterval::Variable::Unit::Unit() : start(FP::undefined()),
                                              end(FP::undefined()),
                                              unit(Time::Second),
                                              count(0),
                                              aligned(false),
                                              undefined(false)
{ }

DynamicTimeInterval::Variable::Unit::Unit(double setStart,
                                          double setEnd,
                                          Time::LogicalTimeUnit setUnit,
                                          int setCount,
                                          bool setAligned,
                                          bool setUndefined) : start(setStart),
                                                            end(setEnd),
                                                            unit(setUnit),
                                                            count(setCount),
                                                            aligned(setAligned),
                                                            undefined(setUndefined)
{ }

DynamicTimeInterval::Variable::Unit::Unit(const DynamicTimeInterval::Variable::Unit &) = default;

DynamicTimeInterval::Variable::Unit &DynamicTimeInterval::Variable::Unit::operator=(const DynamicTimeInterval::Variable::Unit &) = default;

DynamicTimeInterval::Variable::Unit::Unit(DynamicTimeInterval::Variable::Unit &&) = default;

DynamicTimeInterval::Variable::Unit &DynamicTimeInterval::Variable::Unit::operator=(
        DynamicTimeInterval::Variable::Unit &&) = default;

DynamicTimeInterval::Variable::Unit::Unit(const Unit &other, double setStart, double setEnd)
        : start(
        setStart),
                                                                                            end(setEnd),
                                                                                            unit(other.unit),
                                                                                            count(other.count),
                                                                                            aligned(other.aligned),
                                                                                            undefined(
                                                                                                    other.undefined)
{ }

DynamicTimeInterval::Variable::Unit::Unit(const UnitSetAligned &other,
                                          double setStart,
                                          double setEnd)
        : start(setStart),
          end(setEnd),
          unit(Time::Second),
          count(0),
          aligned(other.set),
          undefined(false)
{ }

DynamicTimeInterval::Variable::Unit::Unit(const UnitSetUndefined &other,
                                          double setStart,
                                          double setEnd) : start(setStart),
                                                        end(setEnd),
                                                        unit(Time::Second),
                                                        count(0),
                                                        aligned(false),
                                                        undefined(other.set)
{ }

DynamicTimeInterval::Variable::Unit::Unit(const Unit &under,
                                          const Unit &over,
                                          double setStart,
                                          double setEnd) : start(setStart),
                                                        end(setEnd),
                                                        unit(over.unit),
                                                        count(over.count),
                                                        aligned(over.aligned),
                                                        undefined(over.undefined)
{ Q_UNUSED(under); }

DynamicTimeInterval::Variable::Unit::Unit(const Unit &under,
                                          const UnitSetAligned &over,
                                          double setStart,
                                          double setEnd) : start(setStart),
                                                        end(setEnd),
                                                        unit(under.unit),
                                                        count(under.count),
                                                        aligned(over.set),
                                                        undefined(under.undefined)
{ }

DynamicTimeInterval::Variable::Unit::Unit(const Unit &under,
                                          const UnitSetUndefined &over,
                                          double setStart,
                                          double setEnd) : start(setStart),
                                                        end(setEnd),
                                                        unit(under.unit),
                                                        count(under.count),
                                                        aligned(under.aligned),
                                                        undefined(over.set)
{ }

DynamicTimeInterval::Variable::Variable() = default;

DynamicTimeInterval::Variable::~Variable() = default;

DynamicTimeInterval::Variable::Variable(const DynamicTimeInterval::Variable &) = default;

DynamicTimeInterval::Variable &DynamicTimeInterval::Variable::operator=(const DynamicTimeInterval::Variable &) = default;

DynamicTimeInterval::Variable::Variable(DynamicTimeInterval::Variable &&) = default;

DynamicTimeInterval::Variable &DynamicTimeInterval::Variable::operator=(DynamicTimeInterval::Variable &&) = default;


void DynamicTimeInterval::Variable::clear()
{ units.clear(); }

void DynamicTimeInterval::Variable::set(Time::LogicalTimeUnit setUnit,
                                        int setCount,
                                        bool setAligned)
{
    units.clear();
    units.emplace_back(FP::undefined(), FP::undefined(), setUnit, setCount, setAligned);
}

void DynamicTimeInterval::Variable::set(double start,
                                     double end,
                                     Time::LogicalTimeUnit setUnit,
                                     int setCount,
                                     bool setAligned)
{
    Range::overlayFragmenting(units, Unit(start, end, setUnit, setCount, setAligned));
}

void DynamicTimeInterval::Variable::setAligned(double start, double end, bool aligned)
{
    Range::overlayFragmenting(units, UnitSetAligned(start, end, aligned));
}

void DynamicTimeInterval::Variable::setUndefined(double start, double end, bool undefined)
{
    Range::overlayFragmenting(units, UnitSetUndefined(start, end, undefined));
}

void DynamicTimeInterval::Variable::set(double start, double end, const Variant::Read &v)
{
    Unit u;
    u.start = start;
    u.end = end;

    if (!v.exists()) {
        Range::overlayFragmenting(units, UnitSetUndefined(start, end, true));
        return;
    }

    switch (v.getType()) {
    case Variant::Type::Real: {
        auto i = v.toReal();
        if (!FP::defined(i)) {
            Range::overlayFragmenting(units, UnitSetUndefined(start, end, true));
            return;
        }
        u.count = static_cast<int>(i);
        Range::overlayFragmenting(units, u);
        return;
    }
    case Variant::Type::Integer: {
        auto i = v.toInteger();
        if (!INTEGER::defined(i)) {
            Range::overlayFragmenting(units, UnitSetUndefined(start, end, true));
            return;
        }
        u.count = static_cast<int>(i);
        Range::overlayFragmenting(units, u);
        return;
    }
    case Variant::Type::String: {
        const auto &s = v.toString();
        if (s.empty() || Util::equal_insensitive(s, "undefined", "undef")) {
            Range::overlayFragmenting(units, UnitSetUndefined(start, end, true));
        } else if (Util::equal_insensitive(s, "none")) {
            Range::overlayFragmenting(units, u);
        } else {
            try {
                TimeParse::parseOffset(QString::fromStdString(s), &u.unit, &u.count, &u.aligned);
                Range::overlayFragmenting(units, u);
            } catch (TimeParsingException tpe) {
            }
        }
        return;
    }
    default:
        break;
    }

    if (v["Undefined"].toBool()) {
        Range::overlayFragmenting(units, UnitSetUndefined(start, end, true));
        return;
    }

    Variant::Composite::toTimeInterval(v, &u.unit, &u.count, &u.aligned);
    Range::overlayFragmenting(units, u);
}

void DynamicTimeInterval::Variable::set(const Variant::Read &v)
{
    units.clear();
    set(FP::undefined(), FP::undefined(), v);
}

double DynamicTimeInterval::Variable::apply(double time,
                                            double origin,
                                            bool roundUp,
                                            int multiplier)
{
    if (!Range::intersectShift(units, time))
        return origin;
    const auto &f = units.front();
    if (f.undefined)
        return FP::undefined();
    return Time::logical(origin, f.unit, f.count, f.aligned, roundUp, multiplier);
}

double DynamicTimeInterval::Variable::applyConst(double time,
                                              double origin,
                                              bool roundUp,
                                              int multiplier) const
{
    auto i = Range::findIntersecting(units, time);
    if (i == units.cend())
        return origin;
    if (i->undefined)
        return FP::undefined();
    return Time::logical(origin, i->unit, i->count, i->aligned, roundUp, multiplier);
}

double DynamicTimeInterval::Variable::round(double time, double origin, bool roundUp)
{
    if (!Range::intersectShift(units, time))
        return origin;
    const auto &f = units.front();
    if (f.undefined)
        return FP::undefined();
    if (roundUp)
        return Time::ceil(origin, f.unit, f.count);
    else
        return Time::floor(origin, f.unit, f.count);
}

double DynamicTimeInterval::Variable::roundConst(double time, double origin, bool roundUp) const
{
    auto i = Range::findIntersecting(units, time);
    if (i == units.cend())
        return origin;
    if (i->undefined)
        return FP::undefined();
    if (roundUp)
        return Time::ceil(origin, i->unit, i->count);
    else
        return Time::floor(origin, i->unit, i->count);
}

DynamicTimeInterval *DynamicTimeInterval::Variable::clone() const
{
    DynamicTimeInterval::Variable *r = new DynamicTimeInterval::Variable();
    r->units = units;
    return r;
}

QString DynamicTimeInterval::Variable::describe(double time) const
{
    auto i = Range::findIntersecting(units, time);
    if (i == units.cend())
        return QString("0");
    if (i->undefined)
        return QString("Undefined");
    return Time::describeOffset(i->unit, i->count, i->aligned);
}

bool DynamicTimeInterval::Variable::constantOffsetDescription(Time::LogicalTimeUnit *unit,
                                                              int *count,
                                                              bool *aligned) const
{
    if (units.size() != 1)
        return false;
    const auto &f = units.front();
    if (unit) *unit = f.unit;
    if (count) *count = f.count;
    if (aligned) *aligned = f.aligned;
    return true;
}

QSet<double> DynamicTimeInterval::Variable::getChangedPoints() const
{
    QSet<double> result;
    for (const auto &u : units) {
        if (FP::defined(u.getStart()))
            result.insert(u.getStart());
        if (FP::defined(u.getEnd()))
            result.insert(u.getEnd());
    }
    return result;
}

DynamicTimeInterval::Variable::Variable(QDataStream &stream)
{
    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        double start;
        stream >> start;
        double end;
        stream >> end;
        quint8 t;
        stream >> t;
        qint32 c;
        stream >> c;
        bool a;
        stream >> a;
        bool u;
        stream >> u;
        units.emplace_back(start, end, (Time::LogicalTimeUnit) t, (int) c, a, u);
    }
}

void DynamicTimeInterval::Variable::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(TI_RANGE);
    Serialize::container(stream, units, [&stream](const Unit &u) {
        stream << u.start << u.end << static_cast<quint8>(u.unit) << static_cast<qint32>(u.count)
               << u.aligned << u.undefined;
    });
}

void DynamicTimeInterval::Variable::printLog(QDebug &stream) const
{
    stream << "DynamicTimeInterval::Variable(";
    bool first = true;
    for (const auto &u : units) {
        if (!first)
            stream << ",";
        first = false;

        stream << "[" << Logging::range(u.start, u.end) << ",";
        if (u.undefined) {
            stream << "Undefined]";
            continue;
        }
        stream << Time::describeOffset(u.unit, u.count, u.aligned).toUtf8().constData() << "]";
    }
    stream << ")";
}

DynamicTimeInterval *DynamicTimeInterval::Variable::reduceConfigurationSegments(const DynamicTimeInterval::Variable &icr,
                                                                                double start,
                                                                                double end)
{
    if (icr.units.empty())
        return new DynamicTimeInterval::None();
    if (icr.units.size() == 1) {
        const auto &f = icr.units.front();
        if (Range::compareStart(f.start, start) <= 0 && Range::compareEnd(f.end, end) >= 0) {
            if (f.undefined)
                return new DynamicTimeInterval::Undefined;
            if (f.count == 0 && !f.aligned)
                return new DynamicTimeInterval::None();
            return new DynamicTimeInterval::Constant(f.unit, f.count, f.aligned);
        }
    }
    return icr.clone();
}

QDataStream &operator<<(QDataStream &stream, const DynamicTimeInterval *tis)
{
    if (!tis) {
        stream << static_cast<quint8>(TI_NULL);
        return stream;
    }
    tis->serialize(stream);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, DynamicTimeInterval *&tis)
{
    quint8 tRaw;
    stream >> tRaw;
    switch ((TimeIntervalType) tRaw) {
    case TI_NULL:
        tis = nullptr;
        return stream;
    case TI_NONE:
        tis = new DynamicTimeInterval::None(stream);
        return stream;
    case TI_UNDEFINED:
        tis = new DynamicTimeInterval::Undefined(stream);
        return stream;
    case TI_CONSTANT:
        tis = new DynamicTimeInterval::Constant(stream);
        return stream;
    case TI_RANGE:
        tis = new DynamicTimeInterval::Variable(stream);
        return stream;
    }
    Q_ASSERT(false);
    tis = nullptr;
    return stream;
}

QDebug operator<<(QDebug stream, const DynamicTimeInterval *tis)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << reinterpret_cast<const void *>(tis) << ':';
    if (!tis) {
        stream << "DynamicTimeInterval(NULL)";
    } else {
        tis->printLog(stream);
    }
    return stream;
}


TimeIntervalSelectionOption::TimeIntervalSelectionOption(const QString &argumentName,
                                                         const QString &displayName,
                                                         const QString &description,
                                                         const QString &defaultBehavior,
                                                         int sortPriority) : ComponentOptionBase(
        argumentName, displayName, description, defaultBehavior, sortPriority),
                                                                             allowZero(false),
                                                                             allowNegative(false),
                                                                             allowUndefined(false),
                                                                             defaultAligned(false)
{ }

TimeIntervalSelectionOption::~TimeIntervalSelectionOption() = default;

TimeIntervalSelectionOption::TimeIntervalSelectionOption(const TimeIntervalSelectionOption &other) = default;

void TimeIntervalSelectionOption::reset()
{
    optionSet = false;
    range.clear();
}

ComponentOptionBase *TimeIntervalSelectionOption::clone() const
{ return new TimeIntervalSelectionOption(*this); }


DynamicTimeInterval *TimeIntervalSelectionOption::getInterval() const
{
    if (range.units.empty())
        return new DynamicTimeInterval::None();
    if (range.units.size() == 1) {
        const auto &f = range.units.front();
        if (!FP::defined(f.start) && !FP::defined(f.end)) {
            if (f.undefined)
                return new DynamicTimeInterval::Undefined;
            return new DynamicTimeInterval::Constant(f.unit, f.count, f.aligned);
        }
    }
    return range.clone();
}


void TimeIntervalSelectionOption::clear()
{
    optionSet = true;
    range.clear();
}

void TimeIntervalSelectionOption::set(Time::LogicalTimeUnit setUnit, int setCount, bool setAligned)
{
    optionSet = true;
    range.set(setUnit, setCount, setAligned);
}

void TimeIntervalSelectionOption::set(double start,
                                      double end,
                                      Time::LogicalTimeUnit setUnit,
                                      int setCount,
                                      bool setAligned)
{
    optionSet = true;
    range.set(start, end, setUnit, setCount, setAligned);
}

void TimeIntervalSelectionOption::set(double start, double end, const Variant::Read &value)
{
    optionSet = true;
    range.set(start, end, value);
}

void TimeIntervalSelectionOption::set(const Variant::Read &value)
{
    optionSet = true;
    range.set(value);
}

void TimeIntervalSelectionOption::setAligned(double start, double end, bool aligned)
{
    optionSet = true;
    range.setAligned(start, end, aligned);
}

void TimeIntervalSelectionOption::setUndefined(double start, double end, bool undefined)
{
    optionSet = true;
    range.setUndefined(start, end, undefined);
}

void TimeIntervalSelectionOption::setAllowZero(bool enable)
{ allowZero = enable; }

void TimeIntervalSelectionOption::setAllowNegative(bool enable)
{ allowNegative = enable; }

void TimeIntervalSelectionOption::setAllowUndefined(bool enable)
{ allowUndefined = enable; }

void TimeIntervalSelectionOption::setDefaultAligned(bool enable)
{ defaultAligned = enable; }

bool TimeIntervalSelectionOption::getAllowZero() const
{ return allowZero; }

bool TimeIntervalSelectionOption::getAllowNegative() const
{ return allowNegative; }

bool TimeIntervalSelectionOption::getAllowUndefined() const
{ return allowUndefined; }

bool TimeIntervalSelectionOption::getDefaultAligned() const
{ return defaultAligned; }

bool TimeIntervalSelectionOption::parseFromValue(const Variant::Read &value)
{
    if (value.getType() == Variant::Type::Array) {
        for (auto v : value.toArray()) {
            set(v.hash("Start").toDouble(), v.hash("End").toDouble(), v);
        }
    } else {
        set(value);
    }
    return true;
}


DynamicTimeInterval *DynamicTimeInterval::fromConfiguration(SequenceSegment::Transfer &config,
                                                            const SequenceName &unit,
                                                            const QString &path,
                                                            double start,
                                                            double end,
                                                            bool skipUndefined,
                                                            DynamicTimeInterval::Variable *baseline)
{
    if (config.empty()) {
        if (baseline)
            return baseline->clone();
        return new DynamicTimeInterval::None();
    }

    DynamicTimeInterval::Variable icr;
    if (baseline)
        icr.units = baseline->units;

    auto i = Range::findLowerBound(config.begin(), config.end(), start);
    if (i == config.end()) {
        --i;
        if (Range::compareStartEnd(start, i->getEnd()) < 0) {
            auto configValue = i->getValue(unit).getPath(path);
            if (!skipUndefined || configValue.exists())
                icr.set(i->getStart(), i->getEnd(), configValue);
        }
    } else if (i != config.begin() || Range::compareStartEnd(i->getStart(), end) < 0) {
        if (Range::compareStartEnd(start, i->getEnd()) >= 0)
            ++i;
        for (; i != config.end() && Range::compareStartEnd(i->getStart(), end) < 0; ++i) {

            auto configValue = i->getValue(unit).getPath(path);
            double st = i->getStart();
            if (configValue.exists()) {
                for (; i + 1 != config.end() &&
                        Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                        Range::compareStartEnd((i + 1)->getStart(), i->getEnd()) == 0 &&
                        (i + 1)->getValue(unit).getPath(path) == configValue; ++i) { }
            } else {
                for (; i + 1 != config.end() &&
                        Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                        Range::compareStartEnd((i + 1)->getStart(), i->getEnd()) == 0 &&
                        !(i + 1)->getValue(unit).getPath(path).exists(); ++i) { }
            }
            if (!skipUndefined || configValue.exists())
                icr.set(st, i->getEnd(), configValue);
        }
    }
    return DynamicTimeInterval::Variable::reduceConfigurationSegments(icr, start, end);
}

DynamicTimeInterval *DynamicTimeInterval::fromConfiguration(const ValueSegment::Transfer &config,
                                                            const QString &path,
                                                            double start,
                                                            double end,
                                                            bool skipUndefined,
                                                            DynamicTimeInterval::Variable *baseline)
{
    if (config.empty()) {
        if (baseline)
            return baseline->clone();
        return new DynamicTimeInterval::None();
    }

    DynamicTimeInterval::Variable icr;
    if (baseline)
        icr.units = baseline->units;

    auto i = Range::findLowerBound(config.begin(), config.end(), start);
    if (i == config.end()) {
        --i;
        if (Range::compareStartEnd(start, i->getEnd()) < 0) {
            auto configValue = i->value().getPath(path);
            if (!skipUndefined || configValue.exists())
                icr.set(i->getStart(), i->getEnd(), configValue);
        }
    } else if (i != config.begin() || Range::compareStartEnd(i->getStart(), end) < 0) {
        if (Range::compareStartEnd(start, i->getEnd()) >= 0)
            ++i;
        for (; i != config.end() && Range::compareStartEnd(i->getStart(), end) < 0; ++i) {

            auto configValue = i->value().getPath(path);
            double st = i->getStart();
            for (; i + 1 != config.end() &&
                    Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                    Range::compareStartEnd((i + 1)->getStart(), i->getEnd()) == 0 &&
                    (i + 1)->value().getPath(path) == configValue; ++i) { }
            if (!skipUndefined || configValue.exists())
                icr.set(st, i->getEnd(), configValue);
        }
    }
    return DynamicTimeInterval::Variable::reduceConfigurationSegments(icr, start, end);
}

}
}
