/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACORE_STREAM_HXX
#define CPD3DATACORE_STREAM_HXX

#include "core/first.hxx"

#include <mutex>
#include <deque>
#include <condition_variable>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <cstdint>

#include <QDebug>
#include <QDataStream>

#include "datacore.hxx"
#include "core/util.hxx"
#include "variant/root.hxx"
#include "core/stream.hxx"
#include "core/timeutils.hxx"
#include "core/qtcompat.hxx"
#include "core/threading.hxx"

namespace CPD3 {
namespace Data {

namespace Archive {
class Access;
}

class SequenceName;

class SequenceIdentity;

class SequenceValue;

class ArchiveValue;

class ArchiveErasure;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const SequenceName &value);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, SequenceName &value);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const SequenceName &v);

/**
 * The identifier of a sequence of values.  This represents the unique "name" of a series
 * of values that all identify to the same general quantity.
 */
class CPD3DATACORE_EXPORT SequenceName final {
public:
    typedef std::string Component;

    typedef std::unordered_set<SequenceName> Set;

    typedef std::unordered_set<Component> ComponentSet;

    template<typename Container>
    static ComponentSet toComponentSet(const Container &container)
    { return ComponentSet(container.begin(), container.end()); }

    static const ComponentSet &toComponentSet(const ComponentSet &container)
    { return container; }


    typedef std::unordered_set<Component> Flavors;

    template<typename T> using Map = std::unordered_map<SequenceName, T>;

    struct CPD3DATACORE_EXPORT FlavorsHash {
        std::size_t operator()(const SequenceName::Flavors &flavors) const;
    };

    typedef std::unordered_set<Flavors, FlavorsHash> FlavorsSet;
private:

    std::size_t hashValue;
    Component station;
    Component archive;
    Component variable;
    Component flavors;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const SequenceName &value);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, SequenceName &value);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const SequenceName &v);

    void rehash();

    void assembleFlavors(const Flavors &flavors);

    void assembleFlavors(Flavors &&flavors);

    SequenceName(const SequenceName &other, bool);

public:

    SequenceName();

    SequenceName(const SequenceName &) = default;

    SequenceName &operator=(const SequenceName &) = default;

    SequenceName(SequenceName &&) = default;

    SequenceName &operator=(SequenceName &&) = default;

    explicit SequenceName(const SequenceIdentity &identity);

    explicit SequenceName(SequenceIdentity &&identity);

    /**
     * Convert a variant to flavors.
     *
     * @param input the input variant
     * @return      the converted flavors
     */
    static Flavors toFlavors(const Variant::Read &input);

    /**
     * Swap two sequence names.
     *
     * @param a     the first sequence name
     * @param b     the second sequence name
     */
    static void swap(SequenceName &a, SequenceName &b);

    /**
     * Get the hash of the name.
     *
     * @return      a hash value
     */
    inline std::size_t hash() const
    { return hashValue; }


    /**
     * Construct a sequence name.
     *
     * @param station       the station
     * @param archive       the archive
     * @param variable      the variable
     */
    SequenceName(const Component &station, const Component &archive, const Component &variable);

    /**
     * Construct a sequence name.
     *
     * @param station       the station
     * @param archive       the archive
     * @param variable      the variable
     */
    SequenceName(Component &&station, Component &&archive, Component &&variable);

    /**
     * Construct a sequence name.
     *
     * @param station       the station
     * @param archive       the archive
     * @param variable      the variable
     * @param flavors       the flavors
     */
    SequenceName(const Component &station,
                 const Component &archive,
                 const Component &variable,
                 const Flavors &flavors);

    /**
     * Construct a sequence name.
     *
     * @param station       the station
     * @param archive       the archive
     * @param variable      the variable
     * @param flavors       the flavors
     */
    SequenceName(Component &&station, Component &&archive, Component &&variable, Flavors &&flavors);


    /**
     * Get a copy of the name with the station set to the default station.
     *
     * @return  a copy of the sequence name with the defualt station
     */
    SequenceName toDefaultStation() const;

    /**
     * Test if the sequence name refers to the default station.
     *
     * @return  true if the sequence name is attached to the default station
     */
    bool isDefaultStation() const;

    /**
     * Get a copy of the name with archive set to the metadata archive.
     *
     * @return  a metadata copy of the sequence name
     */
    SequenceName toMeta() const;

    /**
     * Get a copy of the name with the archive cleared from the metadata archive.
     *
     * @return  a non-metadata copy of the sequence name
     */
    SequenceName fromMeta() const;

    /**
    * Test an archive name identifies the metadata archive.
    *
     * @param archive   the archive to check
    * @return  true if the name belongs to the metadata archive
    */
    static bool isMeta(const Component &archive);

    /**
     * Test if the name identifies the metadata archive.
     *
     * @return  true if the name belongs to the metadata archive
     */
    bool isMeta() const;

    /**
     * Get the station.
     * @return the station
     */
    inline const Component &getStation() const
    { return station; }

    /**
     * Get the station.
     * @return the station
     */
    inline QString getStationQString() const
    { return QString::fromStdString(getStation()); }

    /**
     * Get the archive.
     * @return the archive
     */
    inline const Component &getArchive() const
    { return archive; }

    /**
     * Get the archive.
     * @return the archive
     */
    inline QString getArchiveQString() const
    { return QString::fromStdString(getArchive()); }

    /**
     * Get the variable.
     * @return the variable
     */
    inline const Component &getVariable() const
    { return variable; }

    /**
     * Get the variable.
     * @return the variable
     */
    inline QString getVariableQString() const
    { return QString::fromStdString(getVariable()); }

    /**
     * Get the flavors string.  This is generally only useful for database
     * references.
     * @return the flavors string
     */
    inline const Component &getFlavorsString() const
    { return flavors; }

    /**
     * Test if the name has an flavors set.
     *
     * @return  true if there are flavors set
     */
    inline bool hasFlavors() const
    { return flavors.length() != 0; }

    /**
     * Set the station.
     * @param set   the new station
     */
    void setStation(const Component &set);

    inline void setStation(const QString &set)
    { return setStation(set.toStdString()); }

    inline void setStation(const char *set)
    { return setStation(Component(set)); }

    /**
     * Set the archive.
     * @param set   the new archive
     */
    void setArchive(const Component &set);

    inline void setArchive(const QString &set)
    { return setArchive(set.toStdString()); }

    inline void setArchive(const char *set)
    { return setArchive(Component(set)); }

    /**
     * Set the variable.
     * @param set   the new variable
     */
    void setVariable(const Component &set);

    inline void setVariable(const QString &set)
    { return setVariable(set.toStdString()); }

    inline void setVariable(const char *set)
    { return setVariable(Component(set)); }

    /**
     * Set the flavors.
     * @param set   the new flavors
     */
    void setFlavors(const Flavors &set);

    /**
     * Set the flavors identification string.  In general this
     * should only be called by internal archive usage.
     * @param set   the new flavors string
     */
    void setFlavorsString(const Component &set);

    /**
     * Set the station.
     * @param set   the new station
     */
    void setStation(Component &&set);

    /**
     * Set the archive.
     * @param set   the new archive
     */
    void setArchive(Component &&set);

    /**
     * Set the variable.
     * @param set   the new variable
     */
    void setVariable(Component &&set);

    /**
     * Set the flavors.
     * @param set   the new flavors
     */
    void setFlavors(Flavors &&set);

    /**
     * Set the flavors identification string.  In general this
     * should only be called by internal archive usage.
     * @param set   the new flavors string
     */
    void setFlavorsString(Component &&set);


    /**
     * Test if this unit is valid (non-empty station, archive and variable).
     * @return true if valid
     */
    inline bool isValid() const
    { return !station.empty() && !archive.empty() && !variable.empty(); }


    /**
     * Get the flavors
     * @return  the set of flavors
     */
    Flavors getFlavors() const;

    /**
     * Get the flavors
     * @return  the set of flavors
     */
    QSet<QString> getFlavorsQString() const;

    /**
     * Test if the name has a specific flavor.
     * @param flavor    the flavor to test
     * @return          true if the name has the specified flavor
     */
    bool hasFlavor(const Component &flavor) const;

    /**
     * Test if the name lacks a specific flavor.
     * @param flavor    the flavor to test
     * @return          true if the name lacks the specified flavor
     */
    bool lacksFlavor(const Component &flavor) const;

    /**
     * Get a copy of the name with a flavor added.
     *
     * @param flavor    the flavor to add
     * @return          a copy of the name with the flavor added
     */
    SequenceName withFlavor(const Component &flavor) const;

    /**
     * Get a copy of the name with a flavor removed.
     *
     * @param flavor    the flavor to remove
     * @return          a copy of the name with the flavor removed
     */
    SequenceName withoutFlavor(const Component &flavor) const;

    /**
     * Get a copy of the name with all flavors removed.
     *
     * @return          a copy of the name without any flavors
     */
    SequenceName withoutAllFlavors() const;


    /**
     * Remove a flavor from the name.
     *
     * @param flavor    the flavor to remove
     */
    void removeFlavor(const Component &flavor);

    /**
     * Remove multiple flavors from the name.
     *
     * @param flavors   the flavors to remove
     */
    void removeFlavors(const Flavors &flavors);

    /**
     * Add a flavor to the name.
     *
     * @param flavor    the flavor to add
     */
    void addFlavor(const Component &flavor);

    /**
     * Add multiple flavors to the name.
     *
     * @param flavors   the flavors to add
     */
    void addFlavors(const Flavors &flavors);

    /**
     * Remove all flavors from the name.
     */
    void clearFlavors();

    /**
     * Remove the metadata identifier from the archive in the name.
     */
    void clearMeta();

    /**
     * Set the metadata identifier in the archive in the name.
     */
    void setMeta();

    /**
     * Get a displayable description of the name.
     */
    QString describe() const;


    bool operator==(const SequenceName &other) const;

    bool operator!=(const SequenceName &other) const;


    /**
     * Get the set of flavors that can identify cut sizes.
     *
     * @return  possible cut size flavors
     */
    static const Flavors &cutSizeFlavors();

    /**
     * Get the set of flavors that are normally seen in data.
     *
     * @return  the set of flavors sets that are normally seen in data
     */
    static const std::unordered_set<Flavors> &defaultDataFlavors();

    /**
     * Get the default flavors to exclude from "normal" access.
     *
     * @return  the default flavors to exclude
     */
    static const std::vector<Component> &defaultLacksFlavors();

    /**
     * The "cover" flavor.
     */
    static const Component flavor_cover;
    /**
     * The "end" flavor.
     */
    static const Component flavor_end;
    /**
     * The "stats" flavor.
     */
    static const Component flavor_stats;
    /**
     * The "pm1" flavor.
     */
    static const Component flavor_pm1;
    /**
     * The "pm10" flavor.
     */
    static const Component flavor_pm10;
    /**
     * The "pm25" flavor.
     */
    static const Component flavor_pm25;


    /**
     * A sort object that orders names in "logical" ordering.
     */
    struct CPD3DATACORE_EXPORT OrderLogical {
        bool operator()(const SequenceName &a, const SequenceName &b) const;
    };

    /**
     * A sort object that orders names in "display" ordering.
     */
    struct CPD3DATACORE_EXPORT OrderDisplay {
        /**
         * Get the priority level of a specified variable name.
         *
         * @param variable  the variable name
         * @return          a priority integer
         */
        static int priority(const Component &variable);

        /**
         * Get the priority level of a specified sequence name.
         *
         * @param name  the sequence name
         * @return      a priority integer
         */
        static int priority(const SequenceName &variable);

        bool operator()(const SequenceName &a, const SequenceName &b) const;
    };

    struct OrderDisplayCaching;


    /**
     * Get the station implied if none is present where one is required.
     *
     * @param archive   the archive access to use
     * @return          the implied station
     */
    static Component impliedStation(Archive::Access *archive = nullptr);

    /**
     * Get the station implied if none is present where one is required.
     * This returns the station without checking it against the archive.
     *
     * @return          the implied station
     */
    static Component impliedUncheckedStation();

    /**
     * Get the set of implied stations if none are present where one or more are required.
     *
     * @param archive   the archive access to use
     * @return          the set of implied stations
     */
    static ComponentSet impliedStations(Archive::Access *archive = nullptr);

    /**
     * Get the set of implied stations if none are present where one or more are required.
     * This returns the station without checking it against the archive.
     *
     * @return          the set of implied stations
     */
    static ComponentSet impliedUncheckedStations();

    /**
     * Get the implied profile name if none is present where one is required.
     *
     * @param archive   the archive access to use
     * @return          the profile name
     */
    static std::string impliedProfile(Archive::Access *archive = nullptr);
};

CPD3DATACORE_EXPORT inline void swap(SequenceName &lhs, SequenceName &rhs)
{ SequenceName::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(SequenceName &lhs, SequenceName &rhs)
{ SequenceName::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline uint qHash(const SequenceName &key)
{ return static_cast<uint>(key.hash()); }


CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const SequenceIdentity &value);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, SequenceIdentity &value);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const SequenceIdentity &v);

/**
 * A representation of the unique identify of a single value in the data.  This
 * is the unit by which values are unique and no two hold the same.
 */
class CPD3DATACORE_EXPORT SequenceIdentity final {
public:

    template<typename T> using Map = std::unordered_map<SequenceIdentity, T>;

    typedef std::unordered_set<SequenceIdentity> Set;

    typedef std::vector<SequenceIdentity> Transfer;

private:
    SequenceName name;
    double start;
    double end;
    std::int_fast32_t priority;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const SequenceIdentity &value);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       SequenceIdentity &value);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const SequenceIdentity &v);

    friend class SequenceName;

public:
    SequenceIdentity();

    SequenceIdentity(const SequenceIdentity &) = default;

    SequenceIdentity &operator=(const SequenceIdentity &) = default;

    SequenceIdentity(SequenceIdentity &&) = default;

    SequenceIdentity &operator=(SequenceIdentity &&) = default;

    explicit SequenceIdentity(const SequenceName &name);

    explicit SequenceIdentity(SequenceName &&name);

    explicit SequenceIdentity(const SequenceValue &value);

    explicit SequenceIdentity(SequenceValue &&value);

    explicit SequenceIdentity(const ArchiveValue &value);

    explicit SequenceIdentity(ArchiveValue &&value);

    explicit SequenceIdentity(const ArchiveErasure &value);

    explicit SequenceIdentity(ArchiveErasure &&value);

    /**
     * Swap two sequence identities.
     *
     * @param a     the first identity
     * @param b     the second identity
     */
    static void swap(SequenceIdentity &a, SequenceIdentity &b);

    /**
     * Create the identity.
     *
     * @param name      the sequence name
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     */
    SequenceIdentity(const SequenceName &name, double start, double end, int priority = 0);

    /**
     * Create the identity
     *
     * @param name      the sequence name
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     */
    SequenceIdentity(SequenceName &&name, double start, double end, int priority = 0);

    /**
     * Create the identity.
     *
     * @param station   the station
     * @param archive   the archive
     * @param variable  the variable
     */
    SequenceIdentity(const SequenceName::Component &station,
                     const SequenceName::Component &archive,
                     const SequenceName::Component &variable);

    /**
     * Create the identity.
     *
     * @param station   the station
     * @param archive   the archive
     * @param variable  the variable
     */
    SequenceIdentity(SequenceName::Component &&station,
                     SequenceName::Component &&archive,
                     SequenceName::Component &&variable);

    /**
     * Create the identity.
     *
     * @param station   the station
     * @param archive   the archive
     * @param variable  the variable
     * @param flavors   the flavors
     */
    SequenceIdentity(const SequenceName::Component &station,
                     const SequenceName::Component &archive,
                     const SequenceName::Component &variable,
                     const SequenceName::Flavors &flavors);

    /**
     * Create the identity.
     *
     * @param station   the station
     * @param archive   the archive
     * @param variable  the variable
     * @param flavors   the flavors
     */
    SequenceIdentity(SequenceName::Component &&station,
                     SequenceName::Component &&archive,
                     SequenceName::Component &&variable,
                     SequenceName::Flavors &&flavors);

    /**
     * Create the identity.
     *
     * @param station   the station
     * @param archive   the archive
     * @param variable  the variable
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     */
    SequenceIdentity(const SequenceName::Component &station,
                     const SequenceName::Component &archive,
                     const SequenceName::Component &variable,
                     double start,
                     double end,
                     int priority = 0);

    /**
     * Create the identity.
     *
     * @param station   the station
     * @param archive   the archive
     * @param variable  the variable
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     */
    SequenceIdentity(SequenceName::Component &&station,
                     SequenceName::Component &&archive,
                     SequenceName::Component &&variable,
                     double start,
                     double end,
                     int priority = 0);

    /**
     * Create the identity.
     *
     * @param station   the station
     * @param archive   the archive
     * @param variable  the variable
     * @param flavors   the flavors
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     */
    SequenceIdentity(const SequenceName::Component &station,
                     const SequenceName::Component &archive,
                     const SequenceName::Component &variable,
                     const SequenceName::Flavors &flavors,
                     double start,
                     double end,
                     int priority = 0);

    /**
     * Create the identity.
     *
     * @param station   the station
     * @param archive   the archive
     * @param variable  the variable
     * @param flavors   the flavors
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     */
    SequenceIdentity(SequenceName::Component &&station,
                     SequenceName::Component &&archive,
                     SequenceName::Component &&variable,
                     SequenceName::Flavors &&flavors,
                     double start,
                     double end,
                     int priority = 0);


    /**
     * Test if the identity is considered valid.
     *
     * @return  true if the name is valid
     */
    inline bool isValid() const
    { return name.isValid(); }

    /**
     * Get the sequence name.
     * @return  the sequence name
     */
    inline const SequenceName &getName() const
    { return name; }

    /**
     * Get the sequence name.
     * @return  the sequence name
     */
    inline SequenceName &getName()
    { return name; }

    /**
     * Set the sequence name.
     * @param set   the new sequence name
     */
    inline void setName(const SequenceName &set)
    { name = set; }

    /**
     * Set the sequence name.
     * @param set   the new sequence name
     */
    inline void setName(SequenceName &&set)
    { name = std::move(set); }


    /**
     * Get the station.
     * @return the station
     */
    inline const SequenceName::Component &getStation() const
    { return name.getStation(); }

    /**
     * Get the station.
     * @return the station
     */
    inline QString getStationQString() const
    { return name.getStationQString(); }

    /**
     * Get the archive.
     * @return the archive
     */
    inline const SequenceName::Component &getArchive() const
    { return name.getArchive(); }

    /**
     * Get the archive.
     * @return the archive
     */
    inline QString getArchiveQString() const
    { return name.getArchiveQString(); }

    /**
     * Get the variable.
     * @return the variable
     */
    inline const SequenceName::Component &getVariable() const
    { return name.getVariable(); }

    /**
     * Get the variable.
     * @return the variable
     */
    inline QString getVariableQString() const
    { return name.getVariableQString(); }

    /**
     * Get the flavors
     * @return  the set of flavors
     */
    inline SequenceName::Flavors getFlavors() const
    { return name.getFlavors(); }

    /**
     * Get the flavors
     * @return  the set of flavors
     */
    inline QSet<QString> getFlavorsQString() const
    { return name.getFlavorsQString(); }

    /**
     * Set the station.
     * @param set   the new station
     */
    inline void setStation(const SequenceName::Component &set)
    { name.setStation(set); }

    /**
     * Set the archive.
     * @param set   the new archive
     */
    inline void setArchive(const SequenceName::Component &set)
    { name.setArchive(set); }

    /**
     * Set the variable.
     * @param set   the new variable
     */
    inline void setVariable(const SequenceName::Component &set)
    { name.setVariable(set); }

    /**
     * Set the flavors.
     * @param set   the new flavors
     */
    inline void setFlavors(const SequenceName::Flavors &set)
    { name.setFlavors(set); }

    /**
     * Set the station.
     * @param set   the new station
     */
    inline void setStation(SequenceName::Component &&set)
    { name.setStation(std::move(set)); }

    /**
     * Set the archive.
     * @param set   the new archive
     */
    inline void setArchive(SequenceName::Component &&set)
    { name.setArchive(std::move(set)); }

    /**
     * Set the variable.
     * @param set   the new variable
     */
    inline void setVariable(SequenceName::Component &&set)
    { name.setVariable(std::move(set)); }

    /**
     * Set the flavors.
     * @param set   the new flavors
     */
    inline void setFlavors(SequenceName::Flavors &&set)
    { name.setFlavors(std::move(set)); }


    /**
     * Get the start time.
     * @return  the start time
     */
    inline double getStart() const
    { return start; }

    /**
     * Get the end time.
     * @return  the end time
     */
    inline double getEnd() const
    { return end; }

    /**
     * Set the start time.
     * @param set   the new start time
     */
    inline void setStart(double set)
    { start = set; }

    /**
     * Set the end time.
     * @param set   the new end time
     */
    inline void setEnd(double set)
    { end = set; }


    /**
     * Get the priority.
     * @return  the priority
     */
    inline int getPriority() const
    { return static_cast<int>(priority); }

    /**
     * Set the priority
     * @param set the priority
     */
    inline void setPriority(int set)
    { priority = set; }


    bool operator==(const SequenceIdentity &other) const;

    bool operator!=(const SequenceIdentity &other) const;

    inline operator const SequenceName &() const
    { return name; }


    /**
     * A sort object that orders identities in time ascending order.
     */
    struct CPD3DATACORE_EXPORT OrderTime {
        bool operator()(const SequenceIdentity &a, const SequenceIdentity &b) const;
    };

    /**
     * A sort object that orders identities in overlay priority.
     */
    struct CPD3DATACORE_EXPORT OrderOverlay {
        bool operator()(const SequenceIdentity &a, const SequenceIdentity &b) const;
    };
};

CPD3DATACORE_EXPORT inline void swap(SequenceIdentity &lhs, SequenceIdentity &rhs)
{ SequenceIdentity::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(SequenceIdentity &lhs, SequenceIdentity &rhs)
{ SequenceIdentity::swap(lhs, rhs); }

CPD3DATACORE_EXPORT uint qHash(const SequenceIdentity &key);


CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const SequenceValue &value);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, SequenceValue &value);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const SequenceValue &v);

/**
 * A single value of data.  This is a combination of an identity and
 * the actual value associated with it.
 */
class CPD3DATACORE_EXPORT SequenceValue {
public:
    typedef std::vector<SequenceValue> Transfer;

#if 0
    template<typename T> using Map = std::unordered_map<SequenceValue, T,
                                                        std::hash<SequenceIdentity>,
                                                        std::equal_to<SequenceIdentity>>;
#endif

private:
    SequenceIdentity identity;
    Variant::Root contents;

    friend class SequenceIdentity;

    friend class ArchiveValue;

    friend class ArchiveErasure;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const SequenceValue &value);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, SequenceValue &value);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const SequenceValue &v);

public:

    SequenceValue() = default;

    /**
     * Swap two sequence values.
     *
     * @param a     the first value
     * @param b     the second value
     */
    static void swap(SequenceValue &a, SequenceValue &b);

    SequenceValue(const SequenceValue &) = default;

    SequenceValue &operator=(const SequenceValue &) = default;

    SequenceValue(SequenceValue &&) = default;

    SequenceValue &operator=(SequenceValue &&) = default;

    explicit SequenceValue(const SequenceName &name);

    explicit SequenceValue(SequenceName &&name);

    explicit SequenceValue(const SequenceIdentity &identity);

    explicit SequenceValue(SequenceIdentity &&identity);

    explicit SequenceValue(const ArchiveValue &value);

    explicit SequenceValue(ArchiveValue &&value);

    explicit SequenceValue(const ArchiveErasure &value);

    explicit SequenceValue(ArchiveErasure &&value);

    /**
     * Create the sequence value.
     *
     * @param identity  the identity
     * @param value     the value
     */
    SequenceValue(const SequenceIdentity &identity, const Variant::Root &value);

    /**
     * Create the sequence value.
     *
     * @param identity  the identity
     * @param value     the value
     */
    SequenceValue(SequenceIdentity &&identity, const Variant::Root &value);

    /**
     * Create the sequence value.
     *
     * @param identity  the identity
     * @param value     the value
     */
    SequenceValue(const SequenceIdentity &identity, Variant::Root &&value);

    /**
     * Create the sequence value.
     *
     * @param identity  the identity
     * @param value     the value
     */
    SequenceValue(SequenceIdentity &&identity, Variant::Root &&value);

    /**
     * Create the sequence value
     * @param name      the sequence name
     * @param value     the value
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     */
    SequenceValue(const SequenceName &name, const Variant::Root &value,
                  double start,
                  double end,
                  int priority = 0);

    /**
     * Create the sequence value
     * @param name      the sequence name
     * @param value     the value
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     */
    SequenceValue(SequenceName &&name,
                  const Variant::Root &value,
                  double start,
                  double end,
                  int priority = 0);

    /**
     * Create the sequence value
     * @param name      the sequence name
     * @param value     the value
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     */
    SequenceValue(const SequenceName &name, Variant::Root &&value,
                  double start,
                  double end,
                  int priority = 0);

    /**
     * Create the sequence value
     * @param name      the sequence name
     * @param value     the value
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     */
    SequenceValue(SequenceName &&name,
                  Variant::Root &&value,
                  double start,
                  double end,
                  int priority = 0);

    /**
     * Test if the value is considered valid.
     *
     * @return  true if the identity of the value is valid
     */
    inline bool isValid() const
    { return identity.isValid(); }

    /**
     * Get the sequence identity.
     * @return  the sequence identity
     */
    inline const SequenceIdentity &getIdentity() const
    { return identity; }

    /**
     * Get the sequence identity.
     * @return  the sequence identity
     */
    inline SequenceIdentity &getIdentity()
    { return identity; }

    /**
     * Set the sequence identity.
     * @param set   the new sequence identity
     */
    inline void setIdentity(const SequenceIdentity &set)
    { identity = set; }

    /**
     * Set the sequence identity.
     * @param set   the new sequence identity
     */
    inline void setIdentity(SequenceIdentity &&set)
    { identity = std::move(set); }

    /**
     * Get the sequence name.
     * @return  the sequence name
     */
    inline const SequenceName &getName() const
    { return identity.getName(); }

    /**
     * Get the sequence name.
     * @return  the sequence name
     */
    inline SequenceName &getName()
    { return identity.getName(); }

    /**
     * Set the sequence name.
     * @param set   the new sequence name
     */
    inline void setName(const SequenceName &set)
    { identity.setName(set); }

    /**
     * Set the sequence name.
     * @param set   the new sequence name
     */
    inline void setName(SequenceName &&set)
    { identity.setName(std::move(set)); }

    /** @see getName() const */
    inline const SequenceName &getUnit() const
    { return identity.getName(); }

    /** @see getName() */
    inline SequenceName &getUnit()
    { return identity.getName(); }

    /** @see setName(const SequenceName &) */
    inline void setUnit(const SequenceName &set)
    { identity.setName(set); }

    /** @see setName(const SequenceName &) */
    inline void setUnit(SequenceName &&set)
    { identity.setName(std::move(set)); }


    /**
     * Get the station.
     * @return the station
     */
    inline const SequenceName::Component &getStation() const
    { return identity.getStation(); }

    /**
     * Get the station.
     * @return the station
     */
    inline QString getStationQString() const
    { return identity.getStationQString(); }

    /**
     * Get the archive.
     * @return the archive
     */
    inline const SequenceName::Component &getArchive() const
    { return identity.getArchive(); }

    /**
     * Get the archive.
     * @return the archive
     */
    inline QString getArchiveQString() const
    { return QString::fromStdString(identity.getArchive()); }

    /**
     * Get the variable.
     * @return the variable
     */
    inline const SequenceName::Component &getVariable() const
    { return identity.getVariable(); }

    /**
     * Get the variable.
     * @return the variable
     */
    inline QString getVariableQString() const
    { return QString::fromStdString(identity.getVariable()); }

    /**
     * Get the flavors
     * @return  the set of flavors
     */
    inline SequenceName::Flavors getFlavors() const
    { return identity.getFlavors(); }

    /**
     * Get the flavors
     * @return  the set of flavors
     */
    inline QSet<QString> getFlavorsQString() const
    { return identity.getFlavorsQString(); }

    /**
     * Set the station.
     * @param set   the new station
     */
    inline void setStation(const SequenceName::Component &set)
    { identity.setStation(set); }

    /**
     * Set the archive.
     * @param set   the new archive
     */
    inline void setArchive(const SequenceName::Component &set)
    { identity.setArchive(set); }

    /**
     * Set the variable.
     * @param set   the new variable
     */
    inline void setVariable(const SequenceName::Component &set)
    { identity.setVariable(set); }

    /**
     * Set the flavors.
     * @param set   the new flavors
     */
    inline void setFlavors(const SequenceName::Flavors &set)
    { identity.setFlavors(set); }

    /**
     * Set the station.
     * @param set   the new station
     */
    inline void setStation(SequenceName::Component &&set)
    { identity.setStation(std::move(set)); }

    /**
     * Set the archive.
     * @param set   the new archive
     */
    inline void setArchive(SequenceName::Component &&set)
    { identity.setArchive(std::move(set)); }

    /**
     * Set the variable.
     * @param set   the new variable
     */
    inline void setVariable(SequenceName::Component &&set)
    { identity.setVariable(std::move(set)); }

    /**
     * Set the flavors.
     * @param set   the new flavors
     */
    inline void setFlavors(SequenceName::Flavors &&set)
    { identity.setFlavors(std::move(set)); }

    /**
     * Get the start time.
     * @return  the start time
     */
    inline double getStart() const
    { return identity.getStart(); }

    /**
     * Get the end time.
     * @return  the end time
     */
    inline double getEnd() const
    { return identity.getEnd(); }

    /**
     * Set the start time.
     * @param set   the new start time
     */
    inline void setStart(double set)
    { identity.setStart(set); }

    /**
     * Set the end time.
     * @param set   the new end time
     */
    inline void setEnd(double set)
    { identity.setEnd(set); }


    /**
     * Get the priority.
     * @return  the priority
     */
    inline int getPriority() const
    { return identity.getPriority(); }

    /**
     * Set the priority
     * @param set the priority
     */
    inline void setPriority(int set)
    { identity.setPriority(set); }

    inline operator const SequenceIdentity &() const
    { return identity; }

    inline operator const SequenceName &() const
    { return identity.getName(); }

    /**
     * Get the root handle.
     * @return the root handle
     */
    inline const Variant::Root &root() const
    { return contents; }

    /**
     * Get the root handle.
     * @return the root handle
     */
    inline Variant::Root &root()
    { return contents; }

    /**
     * Get the value.
     * @return the value
     */
    inline Variant::Read read() const
    { return contents.read(); }

    /**
     * Get the value.
     * @return the value
     */
    inline Variant::Read getValue() const
    { return contents.read(); }

    /**
     * Get the value.
     * @return the value
     */
    inline Variant::Write write()
    { return contents.write(); }

    /**
     * Set the value.
     *
     * @param set   the new value
     */
    inline void setRoot(const Variant::Root &set)
    { contents = set; }

    /**
     * Set the value.
     *
     * @param set   the new value
     */
    inline void setRoot(Variant::Root &&set)
    { contents = std::move(set); }


    /**
     * A comparison object that also compares the contained values
     * for equality.
     */
    struct CPD3DATACORE_EXPORT ValueEqual {
        bool operator()(const SequenceValue &a, const SequenceValue &b) const;
    };
};

CPD3DATACORE_EXPORT inline void swap(SequenceValue &lhs, SequenceValue &rhs)
{ SequenceValue::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(SequenceValue &lhs, SequenceValue &rhs)
{ SequenceValue::swap(lhs, rhs); }


CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const ArchiveValue &value);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, ArchiveValue &value);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const ArchiveValue &v);

/**
 * A value directly connected to the archive.  This variant tracks
 * the modified time and remote referenced state of the the associated value.
 */
class CPD3DATACORE_EXPORT ArchiveValue final : public SequenceValue {
public:
    typedef std::vector<ArchiveValue> Transfer;

private:
    double modified;
    bool remoteReferenced;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const ArchiveValue &value);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, ArchiveValue &value);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const ArchiveValue &v);

    friend class SequenceIdentity;

    friend class SequenceValue;

    friend class ArchiveErasure;

public:

    /**
     * Swap two archive values.
     *
     * @param a     the first value
     * @param b     the second value
     */
    static void swap(ArchiveValue &a, ArchiveValue &b);

    ArchiveValue();

    ArchiveValue(const ArchiveValue &) = default;

    ArchiveValue &operator=(const ArchiveValue &) = default;

    ArchiveValue(ArchiveValue &&) = default;

    ArchiveValue &operator=(ArchiveValue &&) = default;

    explicit ArchiveValue(const SequenceName &name);

    explicit ArchiveValue(SequenceName &&name);

    explicit ArchiveValue(const SequenceIdentity &identity);

    explicit ArchiveValue(SequenceIdentity &&identity);

    explicit ArchiveValue(const SequenceValue &value);

    explicit ArchiveValue(SequenceValue &&value);

    explicit ArchiveValue(const ArchiveErasure &value);

    explicit ArchiveValue(ArchiveErasure &&value);

    /**
     * Create the archive value.
     *
     * @param identity  the identity
     * @param value     the value
     * @param modified  the modified time
     * @param remoteReferenced the remote referenced state
     */
    ArchiveValue(const SequenceIdentity &identity, const Variant::Root &value,
                 double modified = FP::undefined(),
                 bool remoteReferenced = false);

    /**
     * Create the archive value.
     *
     * @param identity  the identity
     * @param value     the value
     * @param modified  the modified time
     * @param remoteReferenced the remote referenced state
     */
    ArchiveValue(SequenceIdentity &&identity,
                 const Variant::Root &value,
                 double modified = FP::undefined(),
                 bool remoteReferenced = false);

    /**
     * Create the archive value.
     *
     * @param identity  the identity
     * @param value     the value
     * @param modified  the modified time
     * @param remoteReferenced the remote referenced state
     */
    ArchiveValue(const SequenceIdentity &identity, Variant::Root &&value,
                 double modified = FP::undefined(),
                 bool remoteReferenced = false);

    /**
     * Create the archive value.
     *
     * @param identity  the identity
     * @param value     the value
     * @param modified  the modified time
     * @param remoteReferenced the remote referenced state
     */
    ArchiveValue(SequenceIdentity &&identity, Variant::Root &&value,
                 double modified = FP::undefined(),
                 bool remoteReferenced = false);

    /**
     * Create the archive value.
     *
     * @param name      the sequence name
     * @param other     the value
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     * @param modified  the modified time
     * @param remoteReferenced  the remote referenced state
     */
    ArchiveValue(const SequenceName &name, const Variant::Root &other,
                 double start,
                 double end,
                 int priority,
                 double modified,
                 bool remoteReferenced);

    /**
     * Create the archive value.
     *
     * @param name      the sequence name
     * @param other     the value
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     * @param modified  the modified time
     * @param remoteReferenced  the remote referenced state
     */
    ArchiveValue(SequenceName &&name,
                 const Variant::Root &other,
                 double start,
                 double end,
                 int priority,
                 double modified,
                 bool remoteReferenced);

    /**
     * Create the archive value.
     *
     * @param name      the sequence name
     * @param other     the value
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     * @param modified  the modified time
     * @param remoteReferenced  the remote referenced state
     */
    ArchiveValue(const SequenceName &name, Variant::Root &&other,
                 double start,
                 double end,
                 int priority,
                 double modified,
                 bool remoteReferenced);

    /**
     * Create the archive value.
     *
     * @param name      the sequence name
     * @param other     the value
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     * @param modified  the modified time
     * @param remoteReferenced  the remote referenced state
     */
    ArchiveValue(SequenceName &&name, Variant::Root &&other,
                 double start,
                 double end,
                 int priority,
                 double modified,
                 bool remoteReferenced);

    /**
     * Get the modified time.
     * @return the modified time
     */
    inline double getModified() const
    { return modified; }

    /**
     * Set the modified time.
     */
    inline void setModified(double set)
    { modified = set; }

    /**
     * Test if the value is remotely referenced.
     * @return  true if there is a remote reference
     */
    inline bool isRemoteReferenced() const
    { return remoteReferenced; }

    /**
     * A comparison object that checks the identity, value, modified time
     * and remote referenced state.
     */
    struct CPD3DATACORE_EXPORT ExactlyEqual {
        bool operator()(const ArchiveValue &a, const ArchiveValue &b);
    };
};

CPD3DATACORE_EXPORT inline void swap(ArchiveValue &lhs, ArchiveValue &rhs)
{ ArchiveValue::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(ArchiveValue &lhs, ArchiveValue &rhs)
{ ArchiveValue::swap(lhs, rhs); }


CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const ArchiveErasure &value);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, ArchiveErasure &value);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const ArchiveErasure &v);

/**
 * A representation of a value that has been removed from the archive.  This is use
 * to keep an indicator that a remotely referenced value has been deleted.
 */
class CPD3DATACORE_EXPORT ArchiveErasure final {
public:
    typedef std::vector<ArchiveErasure> Transfer;

private:
    SequenceIdentity identity;
    double modified;

    friend class SequenceIdentity;

    friend class SequenceValue;

    friend class ArchiveValue;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const ArchiveErasure &value);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, ArchiveErasure &value);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const ArchiveErasure &v);

public:

    ArchiveErasure();

    /**
     * Swap two archive erasures.
     *
     * @param a     the first value
     * @param b     the second value
     */
    static void swap(ArchiveErasure &a, ArchiveErasure &b);

    ArchiveErasure(const ArchiveErasure &) = default;

    ArchiveErasure &operator=(const ArchiveErasure &) = default;

    ArchiveErasure(ArchiveErasure &&) = default;

    ArchiveErasure &operator=(ArchiveErasure &&) = default;

    explicit ArchiveErasure(const SequenceName &name);

    explicit ArchiveErasure(SequenceName &&name);

    explicit ArchiveErasure(const SequenceIdentity &identity);

    explicit ArchiveErasure(SequenceIdentity &&identity);

    explicit ArchiveErasure(const SequenceValue &value);

    explicit ArchiveErasure(SequenceValue &&value);

    explicit ArchiveErasure(const ArchiveValue &value);

    explicit ArchiveErasure(ArchiveValue &&value);

    /**
     * Create the erasure value.
     *
     * @param identity  the identity
     * @param modified  the modified time
     */
    ArchiveErasure(const SequenceIdentity &identity, double modified);

    /**
     * Create the erasure value.
     *
     * @param identity  the identity
     * @param modified  the modified time
     */
    ArchiveErasure(SequenceIdentity &&identity, double modified);

    /**
     * Create the erasure value.
     *
     * @param name      the sequence name
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     * @param modified  the modified time
     */
    ArchiveErasure(const SequenceName &name,
                   double start,
                   double end,
                   int priority,
                   double modified);

    /**
     * Create the erasure value.
     *
     * @param name      the sequence name
     * @param start     the start time
     * @param end       the end time
     * @param priority  the priority
     * @param modified  the modified time
     */
    ArchiveErasure(SequenceName &&name, double start, double end, int priority, double modified);

    /**
     * Test if the value is considered valid.
     *
     * @return  true if the identity of the value is valid
     */
    inline bool isValid() const
    { return identity.isValid(); }

    /**
     * Get the sequence identity.
     * @return  the sequence identity
     */
    inline const SequenceIdentity &getIdentity() const
    { return identity; }

    /**
     * Get the sequence identity.
     * @return  the sequence identity
     */
    inline SequenceIdentity &getIdentity()
    { return identity; }

    /**
     * Set the sequence identity.
     * @param set   the new sequence identity
     */
    inline void setIdentity(const SequenceIdentity &set)
    { identity = set; }

    /**
     * Set the sequence identity.
     * @param set   the new sequence identity
     */
    inline void setIdentity(SequenceIdentity &&set)
    { identity = std::move(set); }

    /**
     * Get the sequence name.
     * @return  the sequence name
     */
    inline const SequenceName &getName() const
    { return identity.getName(); }

    /**
     * Get the sequence name.
     * @return  the sequence name
     */
    inline SequenceName &getName()
    { return identity.getName(); }

    /**
     * Set the sequence name.
     * @param set   the new sequence name
     */
    inline void setName(const SequenceName &set)
    { identity.setName(set); }

    /**
     * Set the sequence name.
     * @param set   the new sequence name
     */
    inline void setName(SequenceName &&set)
    { identity.setName(std::move(set)); }

    /** @see getName() const */
    inline const SequenceName &getUnit() const
    { return identity.getName(); }

    /** @see getName() */
    inline SequenceName &getUnit()
    { return identity.getName(); }

    /** @see setName(const SequenceName &) */
    inline void setUnit(const SequenceName &set)
    { identity.setName(set); }

    /** @see setName(const SequenceName &) */
    inline void setUnit(SequenceName &&set)
    { identity.setName(std::move(set)); }


    /**
     * Get the station.
     * @return the station
     */
    inline const SequenceName::Component &getStation() const
    { return identity.getStation(); }

    /**
     * Get the archive.
     * @return the archive
     */
    inline const SequenceName::Component &getArchive() const
    { return identity.getArchive(); }

    /**
     * Get the variable.
     * @return the variable
     */
    inline const SequenceName::Component &getVariable() const
    { return identity.getVariable(); }

    /**
     * Get the flavors
     * @return  the set of flavors
     */
    inline SequenceName::Flavors getFlavors() const
    { return identity.getFlavors(); }

    /**
     * Set the station.
     * @param set   the new station
     */
    inline void setStation(const SequenceName::Component &set)
    { identity.setStation(set); }

    /**
     * Set the archive.
     * @param set   the new archive
     */
    inline void setArchive(const SequenceName::Component &set)
    { identity.setArchive(set); }

    /**
     * Set the variable.
     * @param set   the new variable
     */
    inline void setVariable(const SequenceName::Component &set)
    { identity.setVariable(set); }

    /**
     * Set the flavors.
     * @param set   the new flavors
     */
    inline void setFlavors(const SequenceName::Flavors &set)
    { identity.setFlavors(set); }

    /**
     * Set the station.
     * @param set   the new station
     */
    inline void setStation(SequenceName::Component &&set)
    { identity.setStation(std::move(set)); }

    /**
     * Set the archive.
     * @param set   the new archive
     */
    inline void setArchive(SequenceName::Component &&set)
    { identity.setArchive(std::move(set)); }

    /**
     * Set the variable.
     * @param set   the new variable
     */
    inline void setVariable(SequenceName::Component &&set)
    { identity.setVariable(std::move(set)); }

    /**
     * Set the flavors.
     * @param set   the new flavors
     */
    inline void setFlavors(SequenceName::Flavors &&set)
    { identity.setFlavors(std::move(set)); }

    /**
     * Get the start time.
     * @return  the start time
     */
    inline double getStart() const
    { return identity.getStart(); }

    /**
     * Get the end time.
     * @return  the end time
     */
    inline double getEnd() const
    { return identity.getEnd(); }

    /**
     * Set the start time.
     * @param set   the new start time
     */
    inline void setStart(double set)
    { identity.setStart(set); }

    /**
     * Set the end time.
     * @param set   the new end time
     */
    inline void setEnd(double set)
    { identity.setEnd(set); }


    /**
     * Get the priority.
     * @return  the priority
     */
    inline int getPriority() const
    { return identity.getPriority(); }

    /**
     * Set the priority
     * @param set the priority
     */
    inline void setPriority(int set)
    { identity.setPriority(set); }


    /**
     * Get the modified time.
     * @return the modified time
     */
    inline double getModified() const
    { return modified; }

    /**
     * Set the modified time.
     */
    inline void setModified(double set)
    { modified = set; }


    inline operator const SequenceIdentity &() const
    { return identity; }

    inline operator const SequenceName &() const
    { return identity.getName(); }


    /**
     * A comparison object that checks the identity and modified time.
     */
    struct CPD3DATACORE_EXPORT ExactlyEqual {
        bool operator()(const ArchiveErasure &a, const ArchiveErasure &b);
    };
};

CPD3DATACORE_EXPORT inline void swap(ArchiveErasure &lhs, ArchiveErasure &rhs)
{ ArchiveErasure::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(ArchiveErasure &lhs, ArchiveErasure &rhs)
{ ArchiveErasure::swap(lhs, rhs); }


/**
 * A sink for incoming values.
 *
 * @tparam T        the singe value type
 * @param Container the list of values type
 */
template<typename T, typename Container=std::vector<T>>
class GenericSink {
public:
    /**
     * The default threshold of values to buffer before stalling input.
     */
    static constexpr int stallThreshold = 65536;

    virtual ~GenericSink() = default;

    /**
     * Handle incoming data.
     *
     * @param values    the start time ascending values
     */
    virtual void incomingData(const Container &values) = 0;

    /** @see incomingData(const SequenceValue::Transfer &) */
    virtual void incomingData(Container &&values)
    { this->incomingData(static_cast<const Container &>(values)); }

    /**
     * Handle an incoming value.
     *
     * @param value     the value
     */
    virtual void incomingData(const T &value)
    { return this->incomingData(Container{value}); }

    /** @see incomingData(const T &) */
    virtual void incomingData(T &&value)
    {
        Container tx;
        tx.emplace_back(std::move(value));
        return this->incomingData(std::move(tx));
    }

    /**
     * Construct a value and handle it.
     *
     * @tparam Args     the argument types
     * @param args      the constructor arguments
     */
    template<typename... Args>
    inline void emplaceData(Args &&... args)
    { this->incomingData(std::move(T(std::forward<Args>(args)...))); }

    /**
     * Signal the end of data.
     */
    virtual void endData() = 0;

    class Buffer;

    class Iterator;
};

/**
 * A simple buffer target for data, without any form of locking.
 */
template<typename T, typename Container>
class GenericSink<T, Container>::Buffer : public GenericSink<T, Container> {
    Container buffer;
    bool isEnded;
public:
    Buffer() : buffer(), isEnded(false)
    { }

    virtual ~Buffer() = default;

    Buffer(const Buffer &) = delete;

    Buffer &operator=(const Buffer &) = delete;

    virtual void incomingData(const Container &values)
    { Util::append(values, buffer); }

    virtual void incomingData(Container &&values)
    { Util::append(std::move(values), buffer); }

    virtual void incomingData(const T &value)
    { buffer.emplace_back(value); }

    virtual void incomingData(T &&value)
    { buffer.emplace_back(std::move(value)); }

    virtual void endData()
    { isEnded = true; }

    /**
     * Clear and reset the buffer.
     */
    void reset()
    {
        buffer.clear();
        isEnded = false;
    }

    /**
     * Get the buffer contents.
     *
     * @return  the buffer contents
     */
    const Container &values() const
    { return buffer; }

    /**
     * Take the buffer contents.
     *
     * @return  the buffer contents
     */
    Container take()
    { return std::move(buffer); }

    /**
     * Test if the buffer has received the end of data marker.
     *
     * @return  true if endData() has been called
     */
    bool ended() const
    { return isEnded; }

    /**
     * Test if the buffer contains a specific value.
     *
     * @tparam Other    the type to find
     * @tparam Compare  the comparator type
     * @param check     the value to find
     * @param compare   the comparator
     * @return          true if the buffer contains the value
     */
    template<typename Other, typename Compare>
    bool contains(Other check, Compare compare) const
    {
        for (const auto &v : buffer) {
            if (compare(v, check))
                return true;
        }
        return false;
    }

    template<typename Other>
    bool contains(Other check) const
    {
        typedef typename T::ValueEqual Compare;
        return contains(std::forward<Other>(check), Compare());
    }
};

/**
 * A simple bufferring iterator, that provides locking and synchronization.
 */
template<typename T, typename Container>
class GenericSink<T, Container>::Iterator : public GenericSink<T, Container> {
    std::mutex mutex;
    std::condition_variable cv;
    std::deque<T> buffer;
    enum {
        Active, Aborted, Ended,
    } state;
public:
    Iterator() : mutex(), cv(), buffer(), state(Active)
    { }

    virtual ~Iterator() = default;

    Iterator(const Iterator &) = delete;

    Iterator &operator=(const Iterator &) = delete;

    virtual void incomingData(const Container &values)
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (state == Aborted)
                return;
            Util::append(values, buffer);
        }
        cv.notify_all();
    }

    virtual void incomingData(Container &&values)
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (state == Aborted)
                return;
            Util::append(std::move(values), buffer);
        }
        cv.notify_all();
    }

    virtual void incomingData(const T &value)
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (state == Aborted)
                return;
            buffer.emplace_back(value);
        }
        cv.notify_all();
    }

    virtual void incomingData(T &&value)
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (state == Aborted)
                return;
            buffer.emplace_back(std::move(value));
        }
        cv.notify_all();
    }

    virtual void endData()
    {
        std::lock_guard<std::mutex> lock(mutex);
        state = Ended;
        /* Have to hold the mutex during the notify, so we don't get destroyed */
        cv.notify_all();
    }

    /**
     * Abort the iterator, potentially discarding all further input.
     */
    void abort()
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (state != Active)
                return;
        }
        cv.notify_all();
    }

    /**
     * Test if the iterator has received the end of data marker.
     *
     * @return  true if endData() has been called
     */
    bool ended()
    {
        std::lock_guard<std::mutex> lock(mutex);
        return state == Ended;
    }

    /**
     * Test if the iterator has more data, waiting if required.
     *
     * @return  true if there are more data
     */
    bool hasNext()
    {
        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            if (!buffer.empty())
                return true;
            if (state == Ended)
                return false;
            cv.wait(lock);
        }
    }

    /**
     * Take the next result from the iterator, waiting if required.
     *
     * @return  the next single value
     */
    T next()
    {
        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            if (!buffer.empty()) {
                T result = std::move(buffer.front());
                buffer.pop_front();
                return result;
            }
            if (state == Ended)
                return T();
            cv.wait(lock);
        }
    }

    /**
     * Take all available results from the iterator, waiting if required.
     *
     * @return  the currently available results or empty on the end
     */
    Container all()
    {
        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            if (!buffer.empty()) {
                Container result(std::make_move_iterator(buffer.begin()),
                                 std::make_move_iterator(buffer.end()));
                buffer.clear();
                return result;
            }
            if (state == Ended)
                return Container();
            cv.wait(lock);
        }
    }

    /**
     * Take all available results from the iterator, waiting if required.
     *
     * @tparam OutputContainer  the output container type
     * @param output            the output target, with values pushed on the back
     * @return                  true if the iterator has ended
     */
    template<typename OutputContainer>
    bool all(OutputContainer &output)
    {
        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            if (!buffer.empty()) {
                Util::append(std::move(buffer), output);
                buffer.clear();
                return state == Ended;
            }
            if (state == Ended)
                return true;
            cv.wait(lock);
        }
    }

    /**
     * Return the first available result, with no waiting and without
     * removing it from the buffer.
     *
     * @return  the first available result
     */
    T peek()
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (buffer.empty())
            return T();
        return buffer.front();
    }

    /**
     * Return all available results, with no waiting and without
     * clearing the buffer.
     *
     * @return  the currently available results
     */
    Container peekAll()
    {
        std::lock_guard<std::mutex> lock(mutex);
        Container result;
        std::copy(buffer.cbegin(), buffer.cend(), Util::back_emplacer(result));
        return result;
    }

    /**
     * Return all available results, with no waiting and without
     * clearing the buffer.
     *
     * @tparam OutputContainer  the output container type
     * @param output            the output target, with values pushed on the back
     * @return                  true if the iterator has ended
     */
    template<typename OutputContainer>
    bool peekAll(OutputContainer &output)
    {
        std::lock_guard<std::mutex> lock(mutex);
        std::copy(buffer.cbegin(), buffer.cend(), Util::back_emplacer(output));
        return state == Ended;
    }

    /**
     * Wait for completion.
     *
     * @param timeout   the maximum time to wait
     * @return          true if the iteratoe has received and end of data
     */
    bool wait(double timeout = FP::undefined())
    { return Threading::waitForTimeout(timeout, mutex, cv, [this] { return state == Ended; }); }
};

#ifdef Q_CC_MSVC
template class CPD3DATACORE_EXPORT GenericSink<SequenceValue, SequenceValue::Transfer>;
#endif
typedef GenericSink<SequenceValue, SequenceValue::Transfer> StreamSink;

#ifdef Q_CC_MSVC
template class CPD3DATACORE_EXPORT GenericSink<ArchiveValue, ArchiveValue::Transfer>;
#endif
typedef GenericSink<ArchiveValue, ArchiveValue::Transfer> ArchiveSink;

#ifdef Q_CC_MSVC
template class CPD3DATACORE_EXPORT GenericSink<SequenceIdentity, SequenceIdentity::Transfer>;
#endif
typedef GenericSink<SequenceIdentity, SequenceIdentity::Transfer> SynchronizeLocalSink;


/**
 * A sink for a combined stream of values and erasures.
 */
class CPD3DATACORE_EXPORT ErasureSink {
public:
    /**
     * The default threshold of values to buffer before stalling input.
     */
    static constexpr int stallThreshold = 65536;

    virtual ~ErasureSink();

    /**
     * Handle an incoming value
     *
     * @param value the value
     */
    virtual void incomingValue(const ArchiveValue &value) = 0;

    /** @see incomingValue(const ArchiveValue &) */
    virtual void incomingValue(ArchiveValue &&value);

    /**
     * Construct a value and handle it.
     *
     * @tparam Args     the argument types
     * @param args      the constructor arguments
     * @see incomingValue(ArchiveValue &&)
     */
    template<typename... Args>
    inline void emplaceValue(Args &&... args)
    { this->incomingValue(std::move(ArchiveValue(std::forward<Args>(args)...))); }

    /**
     * Handle an incoming erasure
     *
     * @param erasure the erasure mark
     */
    virtual void incomingErasure(const ArchiveErasure &erasure) = 0;

    /** @see incomingErasure(const ArchiveErasure &) */
    virtual void incomingErasure(ArchiveErasure &&erasure);

    /**
     * Construct en erasure and handle it.
     *
     * @tparam Args     the argument types
     * @param args      the constructor arguments
     * @see incomingErasure(ArchiveErasure &&)
     */
    template<typename... Args>
    inline void emplaceErasure(Args &&... args)
    { this->incomingErasure(std::move(ArchiveErasure(std::forward<Args>(args)...))); }


    /**
     * Signal the end of incoming values.
     */
    virtual void endStream() = 0;

    class Buffer;

    class Iterator;
};

/**
 * A simple buffer target for data, without any form of locking.
 */
class CPD3DATACORE_EXPORT ErasureSink::Buffer : public ErasureSink {
    ArchiveValue::Transfer archiveBuffer;
    ArchiveErasure::Transfer erasureBuffer;
    bool isEnded;
public:
    Buffer();

    virtual ~Buffer();

    Buffer(const Buffer &) = delete;

    Buffer &operator=(const Buffer &) = delete;

    virtual void incomingValue(const ArchiveValue &value);

    virtual void incomingValue(ArchiveValue &&value);

    virtual void incomingErasure(const ArchiveErasure &erasure);

    virtual void incomingErasure(ArchiveErasure &&erasure);

    virtual void endStream();

    /**
     * Get the buffer value contents.
     *
     * @return  the buffer contents
     */
    ArchiveValue::Transfer archiveValues() const;

    /**
     * Take the buffer value contents.
     *
     * @return  the buffer contents
     */
    ArchiveValue::Transfer archiveTake();

    /**
     * Get the buffer erasure contents.
     *
     * @return  the buffer contents
     */
    ArchiveErasure::Transfer erasureValues() const;

    /**
     * Take the buffer erasure contents.
     *
     * @return  the buffer contents
     */
    ArchiveErasure::Transfer erasureTake();

    /**
     * Test if the buffer has received the end of data marker.
     *
     * @return  true if endData() has been called
     */
    bool ended() const;
};

/**
 * A simple bufferring iterator, that provides locking and synchronization.
 */
class CPD3DATACORE_EXPORT ErasureSink::Iterator : public ErasureSink {
    std::mutex mutex;
    std::condition_variable cv;
    std::deque<ArchiveValue> archiveBuffer;
    std::deque<ArchiveErasure> erasureBuffer;
    enum {
        Active, Aborted, Ended
    } state;
public:
    Iterator();

    virtual ~Iterator();

    Iterator(const Iterator &) = delete;

    Iterator &operator=(const Iterator &) = delete;

    virtual void incomingValue(const ArchiveValue &value);

    virtual void incomingValue(ArchiveValue &&value);

    virtual void incomingErasure(const ArchiveErasure &erasure);

    virtual void incomingErasure(ArchiveErasure &&erasure);

    virtual void endStream();

    /**
     * Abort the iterator, potentially discarding all further input.
     */
    void abort();

    /**
     * Test if the iterator has received the end of data marker.
     *
     * @return  true if endData() has been called
     */
    bool ended();

    /**
     * Test if the iterator has more data, waiting if required.
     *
     * @return  true if there are more data
     */
    bool hasNext();

    /**
     * Test if the next value is an archive value, instead of an erasure, waiting if required.
     *
     * @return  true if there are more data and the next value is an archive value
     */
    bool isNextArchive();

    /**
     * Test if the next value is an erasure value, instead of an archive, waiting if required.
     *
     * @return  true if there are more data and the next value is an erasure value
     */
    bool isNextErasure();

    /**
     * Take the next archive value from the iterator, waiting if required.
     *
     * @return  the next single value
     */
    ArchiveValue archiveNext();

    /**
     * Take the next erasure value from the iterator, waiting if required.
     *
     * @return  the next single value
     */
    ArchiveErasure erasureNext();

    /**
     * Take all available results from the iterator, waiting if required.
     *
     * @return  the currently available results or empty on the end
     */
    std::pair<ArchiveValue::Transfer, ArchiveErasure::Transfer> all();

    /**
     * Take all available results from the iterator, waiting if required.
     *
     * @tparam OutputArchive    the output archive type
     * @tparam OutputErasure    the output erasure type
     * @param archive           the target for archive values, with them pushed on to the back
     * @param erasure           the target for erasure values, with them pushed on to the back
     * @return                  true if the iterator has ended
     */
    template<typename OutputArchive, typename OutputErasure>
    bool all(OutputArchive &archive, OutputErasure &erasure)
    {
        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            if (!archiveBuffer.empty() || !erasureBuffer.empty()) {
                std::move(archiveBuffer.begin(), archiveBuffer.end(), Util::back_emplacer(archive));
                archiveBuffer.clear();
                std::move(erasureBuffer.begin(), erasureBuffer.end(), Util::back_emplacer(erasure));
                erasureBuffer.clear();
                return state == Ended;
            }
            if (state == Ended)
                return true;
            cv.wait(lock);
        }
    }

    /**
     * Return the first available archive result, with no waiting and without
     * removing it from the buffer.
     *
     * @return  the first available result
     */
    ArchiveValue archivePeek();

    /**
     * Return the first available erasure result, with no waiting and without
     * removing it from the buffer.
     *
     * @return  the first available result
     */
    ArchiveErasure erasurePeek();

    /**
     * Return all available results, with no waiting and without
     * clearing the buffer.
     *
     * @return  the currently available results
     */
    std::pair<ArchiveValue::Transfer, ArchiveErasure::Transfer> peekAll();

    /**
     * Return all available results, with no waiting and without
     * clearing the buffer.
     *
     * @tparam OutputArchive    the output archive type
     * @tparam OutputErasure    the output erasure type
     * @param archive           the target for archive values, with them pushed on to the back
     * @param erasure           the target for erasure values, with them pushed on to the back
     * @return                  true if the iterator has ended
     */
    template<typename OutputArchive, typename OutputErasure>
    bool peekAll(OutputArchive &archive, OutputErasure &erasure)
    {
        std::lock_guard<std::mutex> lock(mutex);
        std::copy(archiveBuffer.cbegin(), archiveBuffer.cend(), Util::back_emplacer(archive));
        std::copy(erasureBuffer.cbegin(), erasureBuffer.cend(), Util::back_emplacer(erasure));
        return state == Ended;
    }

    /**
     * Wait for completion.
     *
     * @param timeout   the maximum time to wait
     * @return          true if the iterator has received and end of data
     */
    bool wait(double timeout = FP::undefined());
};


/**
 * A source for values.
 *
 * @tparam Sink the sink target type
 */
template<typename Sink>
class GenericSource {
public:
    virtual ~GenericSource() = default;

    /**
     * Set the target sink or set to null to pause the source.
     *
     * @param sink  the target sink
     */
    virtual void setEgress(Sink *sink) = 0;
};

#ifdef Q_CC_MSVC
template class CPD3DATACORE_EXPORT GenericSource<StreamSink>;
#endif
typedef GenericSource<StreamSink> StreamSource;

#ifdef Q_CC_MSVC
template class CPD3DATACORE_EXPORT GenericSource<ArchiveSink>;
#endif
typedef GenericSource<ArchiveSink> ArchiveSource;

#ifdef Q_CC_MSVC
template class CPD3DATACORE_EXPORT GenericSource<ErasureSink>;
#endif
typedef GenericSource<ErasureSink> ErasureSource;


/**
 * A simple guard for a sink that ensures it is ended when it is destroyed.
 *
 * @tparam Sink the sink type
 */
template<typename Sink>
class SinkEndGuard {
    Sink *sink;
public:
    SinkEndGuard(Sink *sink) : sink(sink)
    { Q_ASSERT(sink); }

    ~SinkEndGuard()
    { end(); }

    /**
     * Release the guard without ending the sink.
     *
     */
    void release()
    {
        sink = nullptr;
    }

    /**
     * End the sink.
     */
    void end()
    {
        if (!sink)
            return;
        sink->endData();
        sink = nullptr;
    }
};

template<>
class SinkEndGuard<ErasureSink> {
    ErasureSink *sink;
public:
    SinkEndGuard(ErasureSink *sink) : sink(sink)
    { Q_ASSERT(sink); }

    ~SinkEndGuard()
    { end(); }

    void release()
    {
        sink = nullptr;
    }

    void end()
    {
        if (!sink)
            return;
        sink->endStream();
        sink = nullptr;
    }
};

}
}


namespace std {

template<>
struct hash<CPD3::Data::SequenceName> {
    inline std::size_t operator()(const CPD3::Data::SequenceName &s) const
    { return s.hash(); }
};

template<>
struct hash<CPD3::Data::SequenceIdentity> {
    inline std::size_t operator()(const CPD3::Data::SequenceIdentity &s) const
    {
        std::size_t h = std::hash<CPD3::Data::SequenceName>()(s.getName());
        double v = s.getStart();
        if (CPD3::FP::defined(v)) {
            h = CPD3::INTEGER::mix(h, std::hash<double>()(v));
        }
        v = s.getEnd();
        if (CPD3::FP::defined(v)) {
            h = CPD3::INTEGER::mix(h, std::hash<double>()(v));
        }
        h = CPD3::INTEGER::mix(h, std::hash<int>()(s.getPriority()));
        return h;
    }
};

}

namespace CPD3 {
namespace Data {
/**
 * A sort object that orders names in "display" ordering and
 * caches the results for some speedup.
 */
struct CPD3DATACORE_EXPORT SequenceName::OrderDisplayCaching : public SequenceName::OrderDisplay {
    bool operator()(const SequenceName &a, const SequenceName &b);

    int priority(const SequenceName &name);

private:
    SequenceName::Map<int> cache;
};

}
}

#ifdef CPD3_QTEST
namespace CPD3 {
namespace Data {

inline char *toString(const CPD3::Data::SequenceName &value)
{
    QByteArray ba = "SequenceName(";
    ba += QByteArray::fromStdString(value.getStation());
    ba += ":";
    ba += QByteArray::fromStdString(value.getArchive());
    ba += ":";
    ba += QByteArray::fromStdString(value.getVariable());
    ba += ":";
    ba += QByteArray::fromStdString(value.getFlavorsString());
    ba += ")";
    return ::qstrdup(ba.data());
}

inline char *toString(const CPD3::Data::SequenceName::Set &value)
{
    QByteArray ba = "SequenceName::Set(";
    std::vector<CPD3::Data::SequenceName> sorted(value.begin(), value.end());
    std::sort(sorted.begin(), sorted.end(), CPD3::Data::SequenceName::OrderLogical());
    bool first = true;
    for (const auto &add : sorted) {
        if (!first)
            ba += ",";
        first = false;
        ba += "{";
        ba += QByteArray::fromStdString(add.getStation());
        ba += ":";
        ba += QByteArray::fromStdString(add.getArchive());
        ba += ":";
        ba += QByteArray::fromStdString(add.getVariable());
        ba += ":";
        ba += QByteArray::fromStdString(add.getFlavorsString());
        ba += "}";
    }
    ba += ")";
    return ::qstrdup(ba.data());
}

inline char *toString(const QSet<CPD3::Data::SequenceName> &value)
{
    QByteArray ba = "SequenceName::Set(";
    std::vector<CPD3::Data::SequenceName> sorted(value.begin(), value.end());
    std::sort(sorted.begin(), sorted.end(), CPD3::Data::SequenceName::OrderLogical());
    bool first = true;
    for (const auto &add : sorted) {
        if (!first)
            ba += ",";
        first = false;
        ba += "{";
        ba += QByteArray::fromStdString(add.getStation());
        ba += ":";
        ba += QByteArray::fromStdString(add.getArchive());
        ba += ":";
        ba += QByteArray::fromStdString(add.getVariable());
        ba += ":";
        ba += QByteArray::fromStdString(add.getFlavorsString());
        ba += "}";
    }
    ba += ")";
    return ::qstrdup(ba.data());
}

inline char *toString(const CPD3::Data::SequenceIdentity &value)
{
    QByteArray ba = "SequenceIdentity(name=(";
    ba += QByteArray::fromStdString(value.getStation());
    ba += ":";
    ba += QByteArray::fromStdString(value.getArchive());
    ba += ":";
    ba += QByteArray::fromStdString(value.getVariable());
    ba += ":";
    ba += QByteArray::fromStdString(value.getName().getFlavorsString());
    ba += "),bounds=";
    appendRange(ba, value.getStart(), value.getEnd());
    ba += ",priority=";
    ba += QByteArray::number(value.getPriority());
    ba += ")";
    return ::qstrdup(ba.data());
}

inline char *toString(const CPD3::Data::SequenceValue &value)
{
    QByteArray ba = "SequenceValue(name=(";
    ba += QByteArray::fromStdString(value.getStation());
    ba += ":";
    ba += QByteArray::fromStdString(value.getArchive());
    ba += ":";
    ba += QByteArray::fromStdString(value.getVariable());
    ba += ":";
    ba += QByteArray::fromStdString(value.getName().getFlavorsString());
    ba += "),bounds=";
    appendRange(ba, value.getStart(), value.getEnd());
    ba += ",priority=";
    ba += QByteArray::number(value.getPriority());
    ba += ")";
    return ::qstrdup(ba.data());
}

inline char *toString(const CPD3::Data::ArchiveValue &value)
{
    QByteArray ba = "ArchiveValue(name=(";
    ba += QByteArray::fromStdString(value.getStation());
    ba += ":";
    ba += QByteArray::fromStdString(value.getArchive());
    ba += ":";
    ba += QByteArray::fromStdString(value.getVariable());
    ba += ":";
    ba += QByteArray::fromStdString(value.getName().getFlavorsString());
    ba += "),bounds=";
    appendRange(ba, value.getStart(), value.getEnd());
    ba += ",priority=";
    ba += QByteArray::number(value.getPriority());
    ba += ",modified=";
    appendTime(ba, value.getModified());
    if (value.isRemoteReferenced())
        ba += ",RemoteReferenced";
    ba += ")";
    return ::qstrdup(ba.data());
}

inline char *toString(const CPD3::Data::ArchiveErasure &value)
{
    QByteArray ba = "ArchiveErasure(name=(";
    ba += QByteArray::fromStdString(value.getStation());
    ba += ":";
    ba += QByteArray::fromStdString(value.getArchive());
    ba += ":";
    ba += QByteArray::fromStdString(value.getVariable());
    ba += ":";
    ba += QByteArray::fromStdString(value.getName().getFlavorsString());
    ba += "),bounds=";
    appendRange(ba, value.getStart(), value.getEnd());
    ba += ",priority=";
    ba += QByteArray::number(value.getPriority());
    ba += ",modified=";
    appendTime(ba, value.getModified());
    ba += ")";
    return ::qstrdup(ba.data());
}

inline char *toString(const CPD3::Data::SequenceIdentity::Transfer &value)
{
    QByteArray ba = "SequenceIdentity::Transfer(";
    bool first = true;
    for (const auto &add : value) {
        if (!first)
            ba += ",";
        first = false;
        ba += "{name=(";
        ba += QByteArray::fromStdString(add.getStation());
        ba += ":";
        ba += QByteArray::fromStdString(add.getArchive());
        ba += ":";
        ba += QByteArray::fromStdString(add.getVariable());
        ba += ":";
        ba += QByteArray::fromStdString(add.getName().getFlavorsString());
        ba += "),bounds=";
        appendRange(ba, add.getStart(), add.getEnd());
        ba += ",priority=";
        ba += QByteArray::number(add.getPriority());
        ba += "}";
    }
    ba += ")";
    return ::qstrdup(ba.data());
}

inline char *toString(const CPD3::Data::SequenceValue::Transfer &value)
{
    QByteArray ba = "SequenceValue::Transfer(";
    bool first = true;
    for (const auto &add : value) {
        if (!first)
            ba += ",";
        first = false;
        ba += "{name=(";
        ba += QByteArray::fromStdString(add.getStation());
        ba += ":";
        ba += QByteArray::fromStdString(add.getArchive());
        ba += ":";
        ba += QByteArray::fromStdString(add.getVariable());
        ba += ":";
        ba += QByteArray::fromStdString(add.getName().getFlavorsString());
        ba += "),bounds=";
        appendRange(ba, add.getStart(), add.getEnd());
        ba += ",priority=";
        ba += QByteArray::number(add.getPriority());
        ba += "}";
    }
    ba += ")";
    return ::qstrdup(ba.data());
}

inline char *toString(const CPD3::Data::ArchiveValue::Transfer &value)
{
    QByteArray ba = "SequenceValue::Transfer(";
    bool first = true;
    for (const auto &add : value) {
        if (!first)
            ba += ",";
        first = false;
        ba += "{name=(";
        ba += QByteArray::fromStdString(add.getStation());
        ba += ":";
        ba += QByteArray::fromStdString(add.getArchive());
        ba += ":";
        ba += QByteArray::fromStdString(add.getVariable());
        ba += ":";
        ba += QByteArray::fromStdString(add.getName().getFlavorsString());
        ba += "),bounds=";
        appendRange(ba, add.getStart(), add.getEnd());
        ba += ",priority=";
        ba += QByteArray::number(add.getPriority());
        ba += ",modified=";
        appendTime(ba, add.getModified());
        if (add.isRemoteReferenced())
            ba += ",RemoteReferenced";
        ba += "}";
    }
    ba += ")";
    return ::qstrdup(ba.data());
}

inline char *toString(const CPD3::Data::ArchiveErasure::Transfer &value)
{
    QByteArray ba = "ArchiveErasure::Transfer(";
    bool first = true;
    for (const auto &add : value) {
        if (!first)
            ba += ",";
        first = false;
        ba += "{name=(";
        ba += QByteArray::fromStdString(add.getStation());
        ba += ":";
        ba += QByteArray::fromStdString(add.getArchive());
        ba += ":";
        ba += QByteArray::fromStdString(add.getVariable());
        ba += ":";
        ba += QByteArray::fromStdString(add.getName().getFlavorsString());
        ba += "),bounds=";
        appendRange(ba, add.getStart(), add.getEnd());
        ba += ",priority=";
        ba += QByteArray::number(add.getPriority());
        ba += ",modified=";
        appendTime(ba, add.getModified());
        ba += "}";
    }
    ba += ")";
    return ::qstrdup(ba.data());
}

}
}
#endif


Q_DECLARE_METATYPE(CPD3::Data::SequenceName);

Q_DECLARE_METATYPE(CPD3::Data::SequenceIdentity);

Q_DECLARE_METATYPE(CPD3::Data::SequenceValue);

Q_DECLARE_METATYPE(CPD3::Data::ArchiveValue);

Q_DECLARE_METATYPE(CPD3::Data::ArchiveErasure);

Q_DECLARE_METATYPE(QSet<CPD3::Data::SequenceName>);

Q_DECLARE_METATYPE(CPD3::Data::SequenceValue::Transfer);

Q_DECLARE_METATYPE(CPD3::Data::ArchiveValue::Transfer);

Q_DECLARE_METATYPE(CPD3::Data::ArchiveErasure::Transfer);

#endif //CPD3DATACORE_STREAM_HXX
