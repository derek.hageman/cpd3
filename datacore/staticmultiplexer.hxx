/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACOREDATAMULTIPLEXER_H
#define CPD3DATACOREDATAMULTIPLEXER_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QDataStream>
#include <QList>
#include <QDebug>

#include "datacore/datacore.hxx"
#include "datacore/stream.hxx"
#include "core/merge.hxx"

namespace CPD3 {
namespace Data {

class StaticMultiplexer;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const StaticMultiplexer &mux);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, StaticMultiplexer &mux);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const StaticMultiplexer &mux);

/**
 * A multiplexer for input data streams.  This for of multiplexer is designed for
 * static allocation of streams, so it provides fixed numeric indexing for
 * each stream.
 */
class CPD3DATACORE_EXPORT StaticMultiplexer {
    typedef StreamMultiplexer<SequenceValue> Backend;

    Backend backend;
    std::vector<Backend::Simple *> streams;
    bool outputQueued;

public:
    /**
     * Create a new multiplexer with the given total number of streams.
     *
     * @param nStreams  the initial number of streams
     */
    StaticMultiplexer(int nStreams = 1);

    ~StaticMultiplexer();

    /**
     * Change the multiplexer to use the given number of streams.  Streams added this
     * way may not output values before the latest advanced time.
     *
     * @param nStreams  the number of streams
     */
    void setStreams(std::size_t nStreams);

    /**
     * Add a stream the multiplexer and return the added index.  Streams added this
     * way may not output values before the latest advanced time.
     *
     * @return a new stream index
     */
    int addStream();

    /**
     * Clear the multiplexer.  This discards all pending data.  The finished state
     * is not changed.
     *
     * @param cleanPrior    if set then the prior information (if any) is discarded
     */
    void clear(bool clearPrior = false);

    /**
     * Get the first  time of any stream.
     *
     * @return the first time of any stream
     */
    double firstStreamTime() const;

    /**
     * Get the total number of buffered values currently contained in the
     * multiplexer.
     *
     * @return the number of buffered values
     */
    int totalBufferedValues() const;

    /**
     * Insert a single value into the multiplexer.
     *
     * @param stream    the stream index
     * @param add       the value to add
     * @param dump      if set then any pending values are dumped
     * @return          the output from the insertion (if dump is set)
     */
    SequenceValue::Transfer incoming(int stream, const SequenceValue &add, bool dump = true);

    /**
     * Insert multiple values into the multiplexer.
     *
     * @param stream    the stream index
     * @param add       the value to add
     * @param dump      if set then any pending values are dumped
     * @return          the output from the insertion (if dump is set)
     */
    SequenceValue::Transfer incoming(int stream, const SequenceValue::Transfer &add, bool dump = true);

    /** @see incoming(int, const SequenceValue &, bool) */
    SequenceValue::Transfer incoming(int stream, SequenceValue &&add, bool dump = true);

    /** @see incoming(int, const SequenceValue::Transfer &, bool) */
    SequenceValue::Transfer incoming(int stream, SequenceValue::Transfer &&add, bool dump = true);

    /**
     * Advance a stream to the given time.
     *
     * @param stream    the stream index
     * @param time      the time to advance to
     * @param dump      if set then any pending values are dumped
     * @return          the output from the advance (if dump is set)
     */
    SequenceValue::Transfer advance(int stream, double time, bool dump = true);

    /**
     * Mark a stream as finished a finished stream can receive no more values.
     *
     * @param stream    the stream index
     * @param dump      if set then any pending values are dumped
     * @return          the output from the advance (if dump is set)
     */
    SequenceValue::Transfer finish(int stream, bool dump = true);

    /**
     * Finish the multiplexer.  This will also flag all streams as being
     * ready to start again.
     *
     * @return all pending values
     */
    SequenceValue::Transfer finish();

    /**
     * Add an incoming value to the multiplexer, writing any output to the given
     * egress.
     *
     * @param stream    the stream index
     * @param add       the value to add
     * @param egress    the egress to write to or NULL
     * @param discardIfNull if set then values with a NULL egress are simply discarded instead of buffered
     */
    void incoming(int stream, const SequenceValue &add, StreamSink *egress, bool discardIfNull = true);

    /**
     * Add an incoming values to the multiplexer, writing any output to the given
     * egress.
     *
     * @param stream    the stream index
     * @param add       the values to add
     * @param egress    the egress to write to or NULL
     * @param discardIfNull if set then values with a NULL egress are simply discarded instead of buffered
     */
    void incoming(int stream, const SequenceValue::Transfer &add, StreamSink *egress, bool discardIfNull = true);

    /** @see incoming(int, const SequenceValue &, StreamSink *, bool) */
    void incoming(int stream, SequenceValue &&add, StreamSink *egress, bool discardIfNull = true);

    /** @see incoming(int, const SequenceValue::Transfer &, StreamSink *, bool) */
    void incoming(int stream, SequenceValue::Transfer &&add, StreamSink *egress, bool discardIfNull = true);

    /**
     * Advance a stream to the given time, writing any output to the given
     * egress.
     *
     * @param stream    the stream index
     * @param time      the time to advance to
     * @param egress    the egress to write to or null
     * @param discardIfNull if set then values with a NULL egress are simply discarded instead of buffered
     */
    void advance(int stream, double time, StreamSink *egress, bool discardIfNull = true);

    /**
     * Finish the multiplexer.  This will also flag all streams as being
     * ready to start again.  Output is written to the given egress if any.
     *
     * @param egress    the egress to write to or NULL
     */
    void finish(int stream, StreamSink *egress, bool discardIfNull = true);

    /**
     * Mark a stream as finished a finished stream can receive no more values.
     * Output is written to the given egress
     *
     * @param stream    the stream index
     * @param egress    the egress to write to or NULL
     * @param discardIfNull if set then values with a NULL egress are simply discarded instead of buffered
     */
    void finish(StreamSink *egress);

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const StaticMultiplexer &mux);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, StaticMultiplexer &mux);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const StaticMultiplexer &mux);
};


}
}

#endif
