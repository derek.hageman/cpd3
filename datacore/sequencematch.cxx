/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <cstdint>
#include <algorithm>
#include <deque>
#include <QRegularExpression>
#include <QLoggingCategory>
#include <QDebugStateSaver>

#include "sequencematch.hxx"
#include "segment.hxx"
#include "core/timeutils.hxx"
#include "core/range.hxx"
#include "core/stream.hxx"
#include "core/util.hxx"


Q_LOGGING_CATEGORY(log_datacore_sequencematch, "cpd3.datacore.sequencematch", QtWarningMsg)

namespace CPD3 {
namespace Data {
namespace SequenceMatch {

enum {
    ID_MatchComponent_Never = 0,

    ID_MatchComponent_StationSingle,
    ID_MatchComponent_DefaultStationSingle,
    ID_MatchComponent_ArchiveSingle,
    ID_MatchComponent_MetaArchiveSingle,
    ID_MatchComponent_VariableSingle,
    ID_MatchComponent_FlavorsSingle,

    ID_MatchComponent_StationComposite,
    ID_MatchComponent_DefaultStationComposite,
    ID_MatchComponent_ArchiveComposite,
    ID_MatchComponent_MetaArchiveComposite,
    ID_MatchComponent_VariableComposite,
    ID_MatchComponent_FlavorsComposite,

    ID_MatchComponent_ExcludeDefaultStation,
    ID_MatchComponent_ExcludeMetaArchive,
};


class Element::MatchComponentNever : public MatchComponent {
public:
    virtual ~MatchComponentNever() = default;

    MatchComponentNever() = default;

    explicit MatchComponentNever(QDataStream &)
    { }

    virtual bool matches(const SequenceName &) const
    { return false; }

    virtual bool integrate(Archive::Selection &) const
    { return false; }

    virtual bool neverMatches() const
    { return true; }

    virtual bool fill(SequenceName &, Contents &) const
    { return false; }

    virtual bool appliesTo(const Contents &) const
    { return true; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_Never;
    }

    virtual QString describe() const
    { return QString("Never"); }

    virtual MatchComponent *clone() const
    { return new MatchComponentNever; }
};

class Element::MatchStationSingle : public MatchComponentSingle<MatchStationSingle> {
public:
    virtual ~MatchStationSingle() = default;

    explicit MatchStationSingle(MatchString *matcher) : MatchComponentSingle<MatchStationSingle>(
            matcher)
    { }

    explicit MatchStationSingle(std::unique_ptr<MatchString> &&matcher) : MatchComponentSingle<
            MatchStationSingle>(std::move(matcher))
    { }

    explicit MatchStationSingle(QDataStream &stream) : MatchComponentSingle<MatchStationSingle>(
            stream)
    { }

    static inline const SequenceName::Component &component(const SequenceName &name)
    { return name.getStation(); }

    static inline void applyFill(SequenceName &name,
                                 const SequenceName::Component &value,
                                 Contents &filled)
    {
        filled.station = true;
        name.setStation(value);
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.station; }

    static inline Archive::Selection::Match &integrationTarget(Archive::Selection &selection)
    { return selection.stations; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_StationSingle;
        MatchComponentSingle<MatchStationSingle>::serialize(stream);
    }

    virtual QString describe() const
    { return QString("Station=%1").arg(matcher->describe()); }

    virtual MatchComponent *clone() const
    { return new MatchStationSingle(*this); }

protected:
    MatchStationSingle(const MatchStationSingle &other) = default;
};

class Element::MatchDefaultStationSingle : public MatchComponentSingle<MatchDefaultStationSingle> {
public:
    virtual ~MatchDefaultStationSingle() = default;

    explicit MatchDefaultStationSingle(MatchString *matcher) : MatchComponentSingle<
            MatchDefaultStationSingle>(matcher)
    { }

    explicit MatchDefaultStationSingle(std::unique_ptr<MatchString> &&matcher)
            : MatchComponentSingle<MatchDefaultStationSingle>(std::move(matcher))
    { }

    explicit MatchDefaultStationSingle(QDataStream &stream) : MatchComponentSingle<
            MatchDefaultStationSingle>(stream)
    { }

    virtual bool matches(const SequenceName &name) const
    {
        if (name.isDefaultStation())
            return true;
        return MatchComponentSingle<MatchDefaultStationSingle>::matches(name);
    }

    static inline Archive::Selection::Match &integrationTarget(Archive::Selection &selection)
    { return selection.stations; }

    static inline const SequenceName::Component &component(const SequenceName &name)
    { return name.getStation(); }

    static inline void applyFill(SequenceName &name,
                                 const SequenceName::Component &value,
                                 Contents &filled)
    {
        filled.station = true;
        name.setStation(value);
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.station; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_DefaultStationSingle;
        MatchComponentSingle<MatchDefaultStationSingle>::serialize(stream);
    }

    virtual QString describe() const
    { return QString("DefaultStation=%1").arg(matcher->describe()); }

    virtual MatchComponent *clone() const
    { return new MatchDefaultStationSingle(*this); }

protected:
    MatchDefaultStationSingle(const MatchDefaultStationSingle &other) = default;
};

class Element::MatchArchiveSingle : public MatchComponentSingle<MatchArchiveSingle> {
public:
    virtual ~MatchArchiveSingle() = default;

    explicit MatchArchiveSingle(MatchString *matcher) : MatchComponentSingle<MatchArchiveSingle>(
            matcher)
    { }

    explicit MatchArchiveSingle(std::unique_ptr<MatchString> &&matcher) : MatchComponentSingle<
            MatchArchiveSingle>(std::move(matcher))
    { }

    explicit MatchArchiveSingle(QDataStream &stream) : MatchComponentSingle<MatchArchiveSingle>(
            stream)
    { }

    static inline const SequenceName::Component &component(const SequenceName &name)
    { return name.getArchive(); }

    static inline void applyFill(SequenceName &name,
                                 const SequenceName::Component &value,
                                 Contents &filled)
    {
        filled.archive = true;
        name.setArchive(value);
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.archive; }

    static inline Archive::Selection::Match &integrationTarget(Archive::Selection &selection)
    { return selection.archives; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_ArchiveSingle;
        MatchComponentSingle<MatchArchiveSingle>::serialize(stream);
    }

    virtual QString describe() const
    { return QString("Archive=%1").arg(matcher->describe()); }

    virtual MatchComponent *clone() const
    { return new MatchArchiveSingle(*this); }

protected:
    MatchArchiveSingle(const MatchArchiveSingle &other) = default;
};

class Element::MatchMetaArchiveSingle : public MatchComponentSingle<MatchMetaArchiveSingle> {
public:
    virtual ~MatchMetaArchiveSingle() = default;

    explicit MatchMetaArchiveSingle(MatchString *matcher) : MatchComponentSingle<
            MatchMetaArchiveSingle>(matcher)
    { }

    explicit MatchMetaArchiveSingle(std::unique_ptr<MatchString> &&matcher) : MatchComponentSingle<
            MatchMetaArchiveSingle>(std::move(matcher))
    { }

    explicit MatchMetaArchiveSingle(QDataStream &stream) : MatchComponentSingle<
            MatchMetaArchiveSingle>(stream)
    { }

    virtual bool matches(const SequenceName &name) const
    {
        if (matcher->matches(name.getArchive()))
            return true;
        if (!name.isMeta())
            return false;
        auto check = name.getArchive();
        check.resize(check.size() - 5);
        return matcher->matches(check);
    }

    static inline Archive::Selection::Match &integrationTarget(Archive::Selection &selection)
    { return selection.archives; }

    static inline const SequenceName::Component &component(const SequenceName &name)
    { return name.getArchive(); }

    static inline void applyFill(SequenceName &name,
                                 const SequenceName::Component &value,
                                 Contents &filled)
    {
        filled.archive = true;
        name.setArchive(value);
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.archive; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_MetaArchiveSingle;
        MatchComponentSingle<MatchMetaArchiveSingle>::serialize(stream);
    }

    virtual QString describe() const
    { return QString("MetaArchive=%1").arg(matcher->describe()); }

    virtual MatchComponent *clone() const
    { return new MatchMetaArchiveSingle(*this); }

protected:
    MatchMetaArchiveSingle(const MatchMetaArchiveSingle &other) = default;
};

class Element::MatchVariableSingle : public MatchComponentSingle<MatchVariableSingle> {
public:
    virtual ~MatchVariableSingle() = default;

    explicit MatchVariableSingle(MatchString *matcher) : MatchComponentSingle<MatchVariableSingle>(
            matcher)
    { }

    explicit MatchVariableSingle(std::unique_ptr<MatchString> &&matcher) : MatchComponentSingle<
            MatchVariableSingle>(std::move(matcher))
    { }

    explicit MatchVariableSingle(QDataStream &stream) : MatchComponentSingle<MatchVariableSingle>(
            stream)
    { }

    static inline const SequenceName::Component &component(const SequenceName &name)
    { return name.getVariable(); }

    static inline void applyFill(SequenceName &name,
                                 const SequenceName::Component &value,
                                 Contents &filled)
    {
        filled.variable = true;
        name.setVariable(value);
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.variable; }

    static inline Archive::Selection::Match &integrationTarget(Archive::Selection &selection)
    { return selection.variables; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_VariableSingle;
        MatchComponentSingle<MatchVariableSingle>::serialize(stream);
    }

    virtual QString describe() const
    { return QString("Variable=%1").arg(matcher->describe()); }

    virtual MatchComponent *clone() const
    { return new MatchVariableSingle(*this); }

protected:
    MatchVariableSingle(const MatchVariableSingle &other) = default;
};

class Element::MatchFlavorsSingle : public MatchComponentSingle<MatchFlavorsSingle, MatchSet> {
public:
    virtual ~MatchFlavorsSingle() = default;

    explicit MatchFlavorsSingle(MatchSet *matcher) : MatchComponentSingle<MatchFlavorsSingle,
                                                                          MatchSet>(matcher)
    { }

    explicit MatchFlavorsSingle(std::unique_ptr<MatchSet> &&matcher) : MatchComponentSingle<
            MatchFlavorsSingle, MatchSet>(std::move(matcher))
    { }

    explicit MatchFlavorsSingle(QDataStream &stream) : MatchComponentSingle<MatchFlavorsSingle,
                                                                            MatchSet>(stream)
    { }

    static inline SequenceName::Flavors component(const SequenceName &name)
    { return name.getFlavors(); }

    static inline void applyFill(SequenceName &name,
                                 const SequenceName::Flavors &value,
                                 Contents &filled)
    {
        filled.flavors = true;
        name.setFlavors(value);
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.flavors; }

    static inline MatchSet::Integration integrationTarget(Archive::Selection &selection)
    { return MatchSet::Integration(selection); }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_FlavorsSingle;
        MatchComponentSingle<MatchFlavorsSingle, MatchSet>::serialize(stream);
    }

    virtual QString describe() const
    { return QString("Flavors=%1").arg(matcher->describe()); }

    virtual MatchComponent *clone() const
    { return new MatchFlavorsSingle(*this); }

protected:
    MatchFlavorsSingle(const MatchFlavorsSingle &other) = default;
};


class Element::MatchStationComposite : public MatchComponentComposite<MatchStationComposite> {
public:
    virtual ~MatchStationComposite() = default;

    explicit MatchStationComposite(std::vector<std::unique_ptr<MatchString>> &&matchers)
            : MatchComponentComposite<MatchStationComposite>(std::move(matchers))
    { }

    explicit MatchStationComposite(QDataStream &stream) : MatchComponentComposite<
            MatchStationComposite>(stream)
    { }

    static inline const SequenceName::Component &component(const SequenceName &name)
    { return name.getStation(); }

    static inline void applyFill(SequenceName &name,
                                 const SequenceName::Component &value,
                                 Contents &filled)
    {
        filled.station = true;
        name.setStation(value);
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.station; }

    static inline Archive::Selection::Match &integrationTarget(Archive::Selection &selection)
    { return selection.stations; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_StationComposite;
        MatchComponentComposite<MatchStationComposite>::serialize(stream);
    }

    virtual QString describe() const
    {
        return QString("Station=%1").arg(
                MatchComponentComposite<MatchStationComposite>::describe());
    }

    virtual MatchComponent *clone() const
    { return new MatchStationComposite(*this); }

protected:
    MatchStationComposite(const MatchStationComposite &other) = default;
};

class Element::MatchDefaultStationComposite : public MatchComponentComposite<
        MatchDefaultStationComposite> {
public:
    virtual ~MatchDefaultStationComposite() = default;

    explicit MatchDefaultStationComposite(std::vector<std::unique_ptr<MatchString>> &&matchers)
            : MatchComponentComposite<MatchDefaultStationComposite>(std::move(matchers))
    { }

    explicit MatchDefaultStationComposite(QDataStream &stream) : MatchComponentComposite<
            MatchDefaultStationComposite>(stream)
    { }

    virtual bool matches(const SequenceName &name) const
    {
        if (name.isDefaultStation())
            return true;
        return MatchComponentComposite<MatchDefaultStationComposite>::matches(name);
    }

    static inline void applyFill(SequenceName &name,
                                 const SequenceName::Component &value,
                                 Contents &filled)
    {
        filled.station = true;
        name.setStation(value);
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.station; }

    static inline const SequenceName::Component &component(const SequenceName &name)
    { return name.getStation(); }

    static inline Archive::Selection::Match &integrationTarget(Archive::Selection &selection)
    { return selection.stations; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_DefaultStationComposite;
        MatchComponentComposite<MatchDefaultStationComposite>::serialize(stream);
    }

    virtual QString describe() const
    {
        return QString("Station=%1").arg(
                MatchComponentComposite<MatchDefaultStationComposite>::describe());
    }

    virtual MatchComponent *clone() const
    { return new MatchDefaultStationComposite(*this); }

protected:
    MatchDefaultStationComposite(const MatchDefaultStationComposite &other) = default;
};

class Element::MatchArchiveComposite : public MatchComponentComposite<MatchArchiveComposite> {
public:
    virtual ~MatchArchiveComposite() = default;

    explicit MatchArchiveComposite(std::vector<std::unique_ptr<MatchString>> &&matchers)
            : MatchComponentComposite<MatchArchiveComposite>(std::move(matchers))
    { }

    explicit MatchArchiveComposite(QDataStream &stream) : MatchComponentComposite<
            MatchArchiveComposite>(stream)
    { }

    static inline const SequenceName::Component &component(const SequenceName &name)
    { return name.getArchive(); }

    static inline void applyFill(SequenceName &name,
                                 const SequenceName::Component &value,
                                 Contents &filled)
    {
        filled.archive = true;
        name.setArchive(value);
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.archive; }

    static inline Archive::Selection::Match &integrationTarget(Archive::Selection &selection)
    { return selection.archives; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_ArchiveComposite;
        MatchComponentComposite<MatchArchiveComposite>::serialize(stream);
    }

    virtual QString describe() const
    {
        return QString("Archive=%1").arg(
                MatchComponentComposite<MatchArchiveComposite>::describe());
    }

    virtual MatchComponent *clone() const
    { return new MatchArchiveComposite(*this); }

protected:
    MatchArchiveComposite(const MatchArchiveComposite &other) = default;
};

class Element::MatchMetaArchiveComposite : public MatchComponentComposite<
        MatchMetaArchiveComposite> {
public:
    virtual ~MatchMetaArchiveComposite() = default;

    explicit MatchMetaArchiveComposite(std::vector<std::unique_ptr<MatchString>> &&matchers)
            : MatchComponentComposite<MatchMetaArchiveComposite>(std::move(matchers))
    { }

    explicit MatchMetaArchiveComposite(QDataStream &stream) : MatchComponentComposite<
            MatchMetaArchiveComposite>(stream)
    { }

    virtual bool matches(const SequenceName &name) const
    {
        auto check = name.getArchive();
        for (const auto &m : matchers) {
            if (m->matches(check))
                return true;
        }
        if (!name.isMeta())
            return false;
        check.resize(check.size() - 5);
        for (const auto &m : matchers) {
            if (m->matches(check))
                return true;
        }
        return false;
    }

    static inline const SequenceName::Component &component(const SequenceName &name)
    { return name.getArchive(); }

    static inline void applyFill(SequenceName &name,
                                 const SequenceName::Component &value,
                                 Contents &filled)
    {
        filled.archive = true;
        name.setArchive(value);
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.archive; }

    static inline Archive::Selection::Match &integrationTarget(Archive::Selection &selection)
    { return selection.archives; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_MetaArchiveComposite;
        MatchComponentComposite<MatchMetaArchiveComposite>::serialize(stream);
    }

    virtual QString describe() const
    {
        return QString("MetaArchive=%1").arg(
                MatchComponentComposite<MatchMetaArchiveComposite>::describe());
    }

    virtual MatchComponent *clone() const
    { return new MatchMetaArchiveComposite(*this); }

protected:
    MatchMetaArchiveComposite(const MatchMetaArchiveComposite &other) = default;
};

class Element::MatchVariableComposite : public MatchComponentComposite<MatchVariableComposite> {
public:
    virtual ~MatchVariableComposite() = default;

    explicit MatchVariableComposite(std::vector<std::unique_ptr<MatchString>> &&matchers)
            : MatchComponentComposite<MatchVariableComposite>(std::move(matchers))
    { }

    explicit MatchVariableComposite(QDataStream &stream) : MatchComponentComposite<
            MatchVariableComposite>(stream)
    { }

    static inline const SequenceName::Component &component(const SequenceName &name)
    { return name.getVariable(); }

    static inline void applyFill(SequenceName &name,
                                 const SequenceName::Component &value,
                                 Contents &filled)
    {
        filled.variable = true;
        name.setVariable(value);
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.variable; }

    static inline Archive::Selection::Match &integrationTarget(Archive::Selection &selection)
    { return selection.variables; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_VariableComposite;
        MatchComponentComposite<MatchVariableComposite>::serialize(stream);
    }

    virtual QString describe() const
    {
        return QString("Variable=%1").arg(
                MatchComponentComposite<MatchVariableComposite>::describe());
    }

    virtual MatchComponent *clone() const
    { return new MatchVariableComposite(*this); }

protected:
    MatchVariableComposite(const MatchVariableComposite &other) = default;
};

class Element::MatchFlavorsComposite : public MatchComponentComposite<MatchFlavorsComposite,
                                                                      MatchSet> {
public:
    virtual ~MatchFlavorsComposite() = default;

    explicit MatchFlavorsComposite(std::vector<std::unique_ptr<MatchSet>> &&matchers)
            : MatchComponentComposite<MatchFlavorsComposite, MatchSet>(std::move(matchers))
    { }

    explicit MatchFlavorsComposite(QDataStream &stream) : MatchComponentComposite<
            MatchFlavorsComposite, MatchSet>(stream)
    { }

    static inline SequenceName::Flavors component(const SequenceName &name)
    { return name.getFlavors(); }

    static inline void applyFill(SequenceName &name,
                                 const SequenceName::Flavors &value,
                                 Contents &filled)
    {
        filled.flavors = true;
        name.setFlavors(value);
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.flavors; }

    static inline MatchSet::Integration integrationTarget(Archive::Selection &selection)
    { return MatchSet::Integration(selection); }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_FlavorsComposite;
        MatchComponentComposite<MatchFlavorsComposite, MatchSet>::serialize(stream);
    }

    virtual QString describe() const
    {
        return QString("Flavors=%1").arg(
                MatchComponentComposite<MatchFlavorsComposite, MatchSet>::describe());
    }

    virtual MatchComponent *clone() const
    { return new MatchFlavorsComposite(*this); }

protected:
    MatchFlavorsComposite(const MatchFlavorsComposite &other) = default;
};


class Element::MatchExcludeDefaultStation : public MatchComponent {
public:
    virtual ~MatchExcludeDefaultStation() = default;

    MatchExcludeDefaultStation() = default;

    explicit MatchExcludeDefaultStation(QDataStream &)
    { }

    virtual bool matches(const SequenceName &name) const
    { return !name.isDefaultStation(); }

    virtual bool integrate(Archive::Selection &selection) const
    {
        selection.includeDefaultStation = false;
        return true;
    }

    virtual bool neverMatches() const
    { return false; }

    virtual bool fill(SequenceName &name, Contents &filled) const
    {
        if (filled.station)
            return !name.isDefaultStation();
        return true;
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.station; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_ExcludeDefaultStation;
    }

    virtual QString describe() const
    { return QString("ExcludeDefaultStation"); }

    virtual MatchComponent *clone() const
    { return new MatchExcludeDefaultStation; }
};

class Element::MatchExcludeMetaArchive : public MatchComponent {
public:
    virtual ~MatchExcludeMetaArchive() = default;

    MatchExcludeMetaArchive() = default;

    explicit MatchExcludeMetaArchive(QDataStream &)
    { }

    virtual bool matches(const SequenceName &name) const
    { return !name.isMeta(); }

    virtual bool integrate(Archive::Selection &selection) const
    {
        selection.includeMetaArchive = false;
        return true;
    }

    virtual bool neverMatches() const
    { return false; }

    virtual bool fill(SequenceName &name, Contents &filled) const
    {
        if (filled.archive)
            return !name.isMeta();
        return true;
    }

    virtual bool appliesTo(const Contents &check) const
    { return check.archive; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchComponent_ExcludeMetaArchive;
    }

    virtual QString describe() const
    { return QString("ExcludeMetaArchive"); }

    virtual MatchComponent *clone() const
    { return new MatchExcludeMetaArchive; }
};


Element::MatchComponent::~MatchComponent() = default;

Element::MatchComponent *Element::MatchComponent::deserialize(QDataStream &stream)
{
    quint8 type = 0;
    stream >> type;
    switch (type) {
    case ID_MatchComponent_Never:
        return new MatchComponentNever(stream);
    case ID_MatchComponent_StationSingle:
        return new MatchStationSingle(stream);
    case ID_MatchComponent_DefaultStationSingle:
        return new MatchDefaultStationSingle(stream);
    case ID_MatchComponent_ArchiveSingle:
        return new MatchArchiveSingle(stream);
    case ID_MatchComponent_MetaArchiveSingle:
        return new MatchMetaArchiveSingle(stream);
    case ID_MatchComponent_VariableSingle:
        return new MatchVariableSingle(stream);
    case ID_MatchComponent_FlavorsSingle:
        return new MatchFlavorsSingle(stream);
    case ID_MatchComponent_StationComposite:
        return new MatchStationComposite(stream);
    case ID_MatchComponent_DefaultStationComposite:
        return new MatchDefaultStationComposite(stream);
    case ID_MatchComponent_ArchiveComposite:
        return new MatchArchiveComposite(stream);
    case ID_MatchComponent_MetaArchiveComposite:
        return new MatchMetaArchiveComposite(stream);
    case ID_MatchComponent_VariableComposite:
        return new MatchVariableComposite(stream);
    case ID_MatchComponent_FlavorsComposite:
        return new MatchFlavorsComposite(stream);
    case ID_MatchComponent_ExcludeDefaultStation:
        return new MatchExcludeDefaultStation(stream);
    case ID_MatchComponent_ExcludeMetaArchive:
        return new MatchExcludeMetaArchive(stream);
    default:
        Q_ASSERT(false);
        break;
    }
    return nullptr;
}

Element::MatchComponent::Contents::Contents(const SequenceName &name, bool flavors) : station(
        !name.getStation().empty()),
                                                                                      archive(!name.getArchive()
                                                                                                   .empty()),
                                                                                      variable(!name
                                                                                              .getVariable()
                                                                                              .empty()),
                                                                                      flavors(flavors)
{ }

bool Element::MatchComponent::Contents::complete() const
{ return station && archive && variable && flavors; }


enum {
    ID_MatchString_Always = 0,
    ID_MatchString_Never,
    ID_MatchString_Static,
    ID_MatchString_RegularExpression
};


class Element::MatchStringAlways : public MatchString {
public:
    MatchStringAlways() = default;

    virtual ~MatchStringAlways() = default;

    explicit MatchStringAlways(QDataStream &)
    { }

    virtual bool matches(const SequenceName::Component &) const
    { return true; }

    virtual bool integrate(Archive::Selection::Match &list) const
    {
        list.clear();
        return true;
    }

    virtual bool canFill() const
    { return false; }

    virtual bool neverMatches() const
    { return false; }

    virtual SequenceName::Component fillValue() const
    { return SequenceName::Component(); }

    virtual void serialize(QDataStream &stream) const
    { stream << (quint8) ID_MatchString_Always; }

    virtual QString describe() const
    { return "Any"; }

    virtual MatchString *clone() const
    { return new MatchStringAlways; }
};

class Element::MatchStringNever : public MatchString {
public:
    MatchStringNever() = default;

    virtual ~MatchStringNever() = default;

    explicit MatchStringNever(QDataStream &)
    { }

    virtual bool matches(const SequenceName::Component &) const
    { return false; }

    virtual bool integrate(Archive::Selection::Match &list) const
    {
        list.clear();
        return false;
    }

    virtual bool canFill() const
    { return false; }

    virtual bool neverMatches() const
    { return true; }

    virtual SequenceName::Component fillValue() const
    { return SequenceName::Component(); }

    virtual void serialize(QDataStream &stream) const
    { stream << (quint8) ID_MatchString_Never; }

    virtual QString describe() const
    { return "None"; }

    virtual MatchString *clone() const
    { return new MatchStringNever; }
};

class Element::MatchStringStatic : public MatchString {
    SequenceName::Component value;
public:
    explicit MatchStringStatic(const SequenceName::Component &value, bool caseSensitive = false)
            : value(caseSensitive ? value : Util::to_lower(value))
    { }

    explicit MatchStringStatic(const Element::Pattern &value, bool caseSensitive = false)
            : MatchStringStatic(value.toStdString(), caseSensitive)
    { }

    virtual ~MatchStringStatic() = default;

    explicit MatchStringStatic(QDataStream &stream) : value()
    {
        stream >> value;
    }

    virtual bool matches(const SequenceName::Component &test) const
    { return test == value; }

    virtual bool integrate(Archive::Selection::Match &list) const
    {
        list.emplace_back(value);
        return true;
    }

    virtual bool canFill() const
    { return true; }

    virtual bool neverMatches() const
    { return false; }

    virtual SequenceName::Component fillValue() const
    { return value; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchString_Static;
        stream << value;
    }

    virtual QString describe() const
    { return QString::fromStdString(value); }

    virtual MatchString *clone() const
    { return new MatchStringStatic(*this); }

protected:
    MatchStringStatic(const MatchStringStatic &other) : value(other.value)
    { }
};

class Element::MatchStringRegularExpression : public MatchString {
    QRegularExpression re;
    bool caseSensitive;

public:
    explicit MatchStringRegularExpression(const Element::Pattern &pattern,
                                          bool caseSensitive = false) : re(pattern, caseSensitive
                                                                                    ? QRegularExpression::NoPatternOption
                                                                                    : QRegularExpression::CaseInsensitiveOption),
                                                                        caseSensitive(caseSensitive)
    {
        if (!re.isValid()) {
            qCDebug(log_datacore_sequencematch) << "Invalid regular expression" << pattern << ":"
                                                << re.errorString();
        }
        re.optimize();
    }

    explicit MatchStringRegularExpression(const std::string &value, bool caseSensitive = false)
            : MatchStringRegularExpression(QString::fromStdString(value), caseSensitive)
    { }

    virtual ~MatchStringRegularExpression() = default;

    explicit MatchStringRegularExpression(QDataStream &stream) : re(), caseSensitive(false)
    {
        QString pattern;
        stream >> caseSensitive;
        stream >> pattern;
        re = QRegularExpression(pattern, caseSensitive ? QRegularExpression::NoPatternOption
                                                       : QRegularExpression::CaseInsensitiveOption);
        re.optimize();
    }

    virtual bool matches(const SequenceName::Component &test) const
    { return Util::exact_match(test, re); }

    virtual bool integrate(Archive::Selection::Match &list) const
    {
        list.emplace_back(re.pattern().toStdString());
        return true;
    }

    virtual bool canFill() const
    { return false; }

    virtual bool neverMatches() const
    { return false; }

    virtual SequenceName::Component fillValue() const
    { return SequenceName::Component(); }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchString_RegularExpression;
        stream << caseSensitive;
        stream << re.pattern();
    }

    virtual QString describe() const
    { return re.pattern(); }

    virtual MatchString *clone() const
    { return new MatchStringRegularExpression(*this); }

protected:
    MatchStringRegularExpression(const MatchStringRegularExpression &other) : re(other.re),
                                                                              caseSensitive(
                                                                                      other.caseSensitive)
    {
        re.optimize();
    }
};


Element::MatchString::~MatchString() = default;

Element::MatchString *Element::MatchString::deserialize(QDataStream &stream)
{
    quint8 type = 0;
    stream >> type;
    switch (type) {
    case ID_MatchString_Always:
        return new MatchStringAlways(stream);
    case ID_MatchString_Never:
        return new MatchStringNever(stream);
    case ID_MatchString_Static:
        return new MatchStringStatic(stream);
    case ID_MatchString_RegularExpression:
        return new MatchStringRegularExpression(stream);
    default:
        Q_ASSERT(false);
        break;
    }
    return nullptr;
}


enum {
    ID_MatchSet_Always = 0,
    ID_MatchSet_Never,
    ID_MatchSet_Empty,
    ID_MatchSet_StaticExact,
    ID_MatchSet_RegularExpressionExact,
    ID_MatchSet_StaticHas,
    ID_MatchSet_RegularExpressionHas,
    ID_MatchSet_StaticLacks,
    ID_MatchSet_RegularExpressionLacks,
};


class Element::MatchSetAlways : public MatchSet {
public:
    MatchSetAlways() = default;

    virtual ~MatchSetAlways() = default;

    explicit MatchSetAlways(QDataStream &)
    { }

    virtual bool matches(const SequenceName::Flavors &) const
    { return true; }

    virtual bool integrate(MatchSet::Integration &target) const
    {
        target.matchAny();
        return true;
    }

    virtual bool canFill() const
    { return false; }

    virtual bool neverMatches() const
    { return false; }

    virtual SequenceName::Flavors fillValue() const
    { return SequenceName::Flavors(); }

    virtual void serialize(QDataStream &stream) const
    { stream << (quint8) ID_MatchSet_Always; }

    virtual QString describe() const
    { return "Any"; }

    virtual MatchSet *clone() const
    { return new MatchSetAlways; }
};

class Element::MatchSetNever : public MatchSet {
public:
    MatchSetNever() = default;

    virtual ~MatchSetNever() = default;

    explicit MatchSetNever(QDataStream &)
    { }

    virtual bool matches(const SequenceName::Flavors &) const
    { return false; }

    virtual bool integrate(MatchSet::Integration &target) const
    {
        target.matchAny();
        return false;
    }

    virtual bool canFill() const
    { return false; }

    virtual bool neverMatches() const
    { return true; }

    virtual SequenceName::Flavors fillValue() const
    { return SequenceName::Flavors(); }

    virtual void serialize(QDataStream &stream) const
    { stream << (quint8) ID_MatchSet_Never; }

    virtual QString describe() const
    { return "None"; }

    virtual MatchSet *clone() const
    { return new MatchSetNever; }
};

class Element::MatchSetEmpty : public MatchSet {
public:
    MatchSetEmpty() = default;

    virtual ~MatchSetEmpty() = default;

    explicit MatchSetEmpty(QDataStream &)
    { }

    virtual bool matches(const SequenceName::Flavors &test) const
    { return test.empty(); }

    virtual bool integrate(MatchSet::Integration &target) const
    {
        if (target.has) {
            if (!target.has->empty()) {
                target.matchAny();
                return true;
            }
            target.has = nullptr;
        }
        if (target.lacks) {
            if (!target.lacks->empty()) {
                target.matchAny();
                return true;
            }
            target.lacks = nullptr;
        }
        if (target.exact) {
            if (!target.exact->empty() &&
                    (target.exact->size() != 1 || !target.exact->front().empty())) {
                target.matchAny();
                return false;
            }
            target.exact->clear();
            target.exact->emplace_back();
        }
        return true;
    }

    virtual bool canFill() const
    { return true; }

    virtual bool neverMatches() const
    { return false; }

    virtual SequenceName::Flavors fillValue() const
    { return SequenceName::Flavors(); }

    virtual void serialize(QDataStream &stream) const
    { stream << (quint8) ID_MatchSet_Empty; }

    virtual QString describe() const
    { return "Empty"; }

    virtual MatchSet *clone() const
    { return new MatchSetEmpty; }
};

class Element::MatchSetStaticBase : public MatchSet {
    inline static const std::string &patternToString(const std::string &str)
    { return str; }

    inline static std::string patternToString(const Element::Pattern &str)
    { return str.toStdString(); }

protected:
    SequenceName::Flavors value;
public:
    template<typename PatternList>
    explicit MatchSetStaticBase(const PatternList &value, bool caseSensitive = false)
            : value()
    {
        for (const auto &add : value) {
            if (caseSensitive) {
                this->value.insert(patternToString(add));
            } else {
                this->value.insert(Util::to_lower(patternToString(add)));
            }
        }
    }

    virtual ~MatchSetStaticBase() = default;

    explicit MatchSetStaticBase(QDataStream &stream) : value()
    {
        stream >> value;
    }

    virtual bool neverMatches() const
    { return false; }

    virtual SequenceName::Flavors fillValue() const
    { return value; }

    virtual void serialize(QDataStream &stream) const
    { stream << value; }

    virtual QString describe() const
    {
        auto sorted = Util::to_qstringlist(value);
        std::sort(sorted.begin(), sorted.end());
        return QString("{%1}").arg(sorted.join(','));
    }

protected:
    MatchSetStaticBase(const MatchSetStaticBase &other) : value(other.value)
    { }
};

class Element::MatchSetRegularExpressionBase : public MatchSet {
protected:
    std::vector<QRegularExpression> re;
private:
    bool caseSensitive;

public:
    template<typename PatternList>
    explicit MatchSetRegularExpressionBase(const PatternList &pattern, bool caseSensitive = false)
            : re(), caseSensitive(caseSensitive)
    {
        for (const auto &p : pattern) {
            re.emplace_back(p, caseSensitive ? QRegularExpression::NoPatternOption
                                             : QRegularExpression::CaseInsensitiveOption);
            if (!re.back().isValid()) {
                qCDebug(log_datacore_sequencematch) << "Invalid regular expression" << p << ":"
                                                    << re.back().errorString();
                continue;
            }
            re.back().optimize();
        }
    }

    virtual ~MatchSetRegularExpressionBase() = default;

    explicit MatchSetRegularExpressionBase(QDataStream &stream) : re(), caseSensitive(false)
    {
        stream >> caseSensitive;
        quint32 n = 0;
        stream >> n;
        for (quint32 i = 0; i < n; ++i) {
            QString pattern;
            stream >> pattern;
            re.emplace_back(pattern, caseSensitive ? QRegularExpression::NoPatternOption
                                                   : QRegularExpression::CaseInsensitiveOption);
            re.back().optimize();
        }
    }

    virtual bool canFill() const
    { return false; }

    virtual bool neverMatches() const
    { return false; }

    virtual SequenceName::Flavors fillValue() const
    { return SequenceName::Flavors(); }

    virtual void serialize(QDataStream &stream) const
    {
        stream << caseSensitive;
        stream << (quint32) re.size();
        for (const auto &r : re) {
            stream << r.pattern();
        }
    }

    virtual QString describe() const
    {
        QStringList sorted;
        for (const auto &r : re) {
            sorted.append(r.pattern());
        }
        std::sort(sorted.begin(), sorted.end());
        return QString("{%1}").arg(sorted.join(','));
    }

protected:
    MatchSetRegularExpressionBase(const MatchSetRegularExpressionBase &other) : re(other.re),
                                                                                caseSensitive(
                                                                                        other.caseSensitive)
    {
        for (auto &r : re) {
            r.optimize();
        }
    }
};

class Element::MatchSetStaticExact : public MatchSetStaticBase {
public:
    template<typename PatternList>
    explicit MatchSetStaticExact(const PatternList &value, bool caseSensitive = false)
            : MatchSetStaticBase(value, caseSensitive)
    { }

    virtual ~MatchSetStaticExact() = default;

    explicit MatchSetStaticExact(QDataStream &stream) : MatchSetStaticBase(stream)
    { }

    virtual bool matches(const SequenceName::Flavors &test) const
    { return test == value; }

    virtual bool integrate(MatchSet::Integration &target) const
    {
        if ((target.has && !target.has->empty()) ||
                (target.lacks && !target.lacks->empty()) ||
                (target.exact && !target.exact->empty())) {
            target.matchAny();
            return true;
        }
        if (target.exact) {
            target.exact->clear();
            Util::append(value, *target.exact);
        }
        return true;
    }

    virtual bool canFill() const
    { return true; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchSet_StaticExact;
        MatchSetStaticBase::serialize(stream);
    }

    virtual MatchSet *clone() const
    { return new MatchSetStaticExact(*this); }

protected:
    MatchSetStaticExact(const MatchSetStaticExact &other) = default;
};

class Element::MatchSetRegularExpressionExact : public MatchSetRegularExpressionBase {
public:
    template<typename PatternList>
    explicit MatchSetRegularExpressionExact(const PatternList &value, bool caseSensitive = false)
            : MatchSetRegularExpressionBase(value, caseSensitive)
    { }

    virtual ~MatchSetRegularExpressionExact() = default;

    explicit MatchSetRegularExpressionExact(QDataStream &stream) : MatchSetRegularExpressionBase(
            stream)
    { }

    virtual bool matches(const SequenceName::Flavors &test) const
    {
        std::vector<std::uint_fast8_t> totalHit(re.size(), 0);
        for (const auto &t : test) {
            bool hit = false;
            std::size_t index = 0;
            for (auto r = re.cbegin(), endR = re.cend(); r != endR; ++r, ++index) {
                if (!Util::exact_match(t, *r))
                    continue;
                totalHit[index] = 1;
                hit = true;
            }
            if (!hit)
                return false;
        }
        return std::find(totalHit.cbegin(), totalHit.cend(), 0) == totalHit.cend();
    }

    virtual bool integrate(MatchSet::Integration &target) const
    {
        if ((target.has && !target.has->empty()) ||
                (target.lacks && !target.lacks->empty()) ||
                (target.exact && !target.exact->empty())) {
            target.matchAny();
            return true;
        }
        if (target.exact) {
            target.exact->clear();
            for (const auto &r : re) {
                target.exact->emplace_back(r.pattern().toStdString());
            }
        }
        return true;
    }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchSet_RegularExpressionExact;
        MatchSetRegularExpressionBase::serialize(stream);
    }

    virtual MatchSet *clone() const
    { return new MatchSetRegularExpressionExact(*this); }

protected:
    MatchSetRegularExpressionExact(const MatchSetRegularExpressionExact &other) = default;
};

class Element::MatchSetStaticHas : public MatchSetStaticBase {
public:
    template<typename PatternList>
    explicit MatchSetStaticHas(const PatternList &value, bool caseSensitive = false)
            : MatchSetStaticBase(value, caseSensitive)
    { }

    virtual ~MatchSetStaticHas() = default;

    explicit MatchSetStaticHas(QDataStream &stream) : MatchSetStaticBase(stream)
    { }

    virtual bool matches(const SequenceName::Flavors &test) const
    {
        for (const auto &check : value) {
            if (!test.count(check))
                return false;
        }
        return true;
    }

    virtual bool integrate(MatchSet::Integration &target) const
    {
        if ((target.has && !target.has->empty()) || (target.exact && !target.exact->empty())) {
            target.matchAny();
            return true;
        }
        if (target.has) {
            target.has->clear();
            Util::append(value, *target.has);
        }
        return true;
    }

    virtual bool canFill() const
    { return false; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchSet_StaticHas;
        MatchSetStaticBase::serialize(stream);
    }

    virtual QString describe() const
    {
        return QString("+%1").arg(MatchSetStaticBase::describe());
    }

    virtual MatchSet *clone() const
    { return new MatchSetStaticHas(*this); }

protected:
    MatchSetStaticHas(const MatchSetStaticHas &other) = default;
};

class Element::MatchSetRegularExpressionHas : public MatchSetRegularExpressionBase {
public:
    template<typename PatternList>
    explicit MatchSetRegularExpressionHas(const PatternList &value, bool caseSensitive = false)
            : MatchSetRegularExpressionBase(value, caseSensitive)
    { }

    virtual ~MatchSetRegularExpressionHas() = default;

    explicit MatchSetRegularExpressionHas(QDataStream &stream) : MatchSetRegularExpressionBase(
            stream)
    { }

    virtual bool matches(const SequenceName::Flavors &test) const
    {
        for (const auto &r : re) {
            bool hit = false;
            for (const auto &t : test) {
                if (Util::exact_match(t, r)) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                return false;
        }
        return true;
    }

    virtual bool integrate(MatchSet::Integration &target) const
    {
        if ((target.has && !target.has->empty()) || (target.exact && !target.exact->empty())) {
            target.matchAny();
            return true;
        }
        if (target.has) {
            target.has->clear();
            for (const auto &r : re) {
                target.has->emplace_back(r.pattern().toStdString());
            }
        }
        return true;
    }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchSet_RegularExpressionHas;
        MatchSetRegularExpressionBase::serialize(stream);
    }

    virtual QString describe() const
    {
        return QString("+%1").arg(MatchSetRegularExpressionBase::describe());
    }

    virtual MatchSet *clone() const
    { return new MatchSetRegularExpressionHas(*this); }

protected:
    MatchSetRegularExpressionHas(const MatchSetRegularExpressionHas &other) = default;
};

class Element::MatchSetStaticLacks : public MatchSetStaticBase {
public:
    template<typename PatternList>
    explicit MatchSetStaticLacks(const PatternList &value, bool caseSensitive = false)
            : MatchSetStaticBase(value, caseSensitive)
    { }

    virtual ~MatchSetStaticLacks() = default;

    explicit MatchSetStaticLacks(QDataStream &stream) : MatchSetStaticBase(stream)
    { }

    virtual bool matches(const SequenceName::Flavors &test) const
    {
        for (const auto &check : value) {
            if (test.count(check))
                return false;
        }
        return true;
    }

    virtual bool integrate(MatchSet::Integration &target) const
    {
        if ((target.lacks && !target.lacks->empty()) || (target.exact && !target.exact->empty())) {
            target.matchAny();
            return true;
        }
        if (target.lacks) {
            target.lacks->clear();
            Util::append(value, *target.lacks);
        }
        return true;
    }

    virtual bool canFill() const
    { return false; }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchSet_StaticLacks;
        MatchSetStaticBase::serialize(stream);
    }

    virtual QString describe() const
    {
        return QString("-%1").arg(MatchSetStaticBase::describe());
    }

    virtual MatchSet *clone() const
    { return new MatchSetStaticLacks(*this); }

protected:
    MatchSetStaticLacks(const MatchSetStaticLacks &other) = default;
};

class Element::MatchSetRegularExpressionLacks : public MatchSetRegularExpressionBase {
public:
    template<typename PatternList>
    explicit MatchSetRegularExpressionLacks(const PatternList &value, bool caseSensitive = false)
            : MatchSetRegularExpressionBase(value, caseSensitive)
    { }

    virtual ~MatchSetRegularExpressionLacks() = default;

    explicit MatchSetRegularExpressionLacks(QDataStream &stream) : MatchSetRegularExpressionBase(
            stream)
    { }

    virtual bool matches(const SequenceName::Flavors &test) const
    {
        for (const auto &r : re) {
            for (const auto &t : test) {
                if (Util::exact_match(t, r))
                    return false;
            }
        }
        return true;
    }

    virtual bool integrate(MatchSet::Integration &target) const
    {
        if ((target.lacks && !target.lacks->empty()) || (target.exact && !target.exact->empty())) {
            target.matchAny();
            return true;
        }
        if (target.lacks) {
            target.lacks->clear();
            for (const auto &r : re) {
                target.lacks->emplace_back(r.pattern().toStdString());
            }
        }
        return true;
    }

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_MatchSet_RegularExpressionLacks;
        MatchSetRegularExpressionBase::serialize(stream);
    }

    virtual QString describe() const
    {
        return QString("-%1").arg(MatchSetRegularExpressionBase::describe());
    }

    virtual MatchSet *clone() const
    { return new MatchSetRegularExpressionLacks(*this); }

protected:
    MatchSetRegularExpressionLacks(const MatchSetRegularExpressionLacks &other) = default;
};


Element::MatchSet::~MatchSet() = default;

Element::MatchSet::Integration::Integration() : has(nullptr), lacks(nullptr), exact(nullptr)
{ }

Element::MatchSet::Integration::Integration(Archive::Selection &target) : has(&target.hasFlavors),
                                                                          lacks(&target.lacksFlavors),
                                                                          exact(&target.exactFlavors)
{ }

void Element::MatchSet::Integration::clear()
{ }

void Element::MatchSet::Integration::matchAny()
{
    if (has) {
        has->clear();
        has = nullptr;
    }
    if (lacks) {
        lacks->clear();
        lacks = nullptr;
    }
    if (exact) {
        exact->clear();
        exact = nullptr;
    }
}

Element::MatchSet *Element::MatchSet::deserialize(QDataStream &stream)
{
    quint8 type = 0;
    stream >> type;
    switch (type) {
    case ID_MatchSet_Always:
        return new MatchSetAlways(stream);
    case ID_MatchSet_Never:
        return new MatchSetNever(stream);
    case ID_MatchSet_Empty:
        return new MatchSetEmpty(stream);
    case ID_MatchSet_StaticExact:
        return new MatchSetStaticExact(stream);
    case ID_MatchSet_RegularExpressionExact:
        return new MatchSetRegularExpressionExact(stream);
    case ID_MatchSet_StaticHas:
        return new MatchSetStaticHas(stream);
    case ID_MatchSet_RegularExpressionHas:
        return new MatchSetRegularExpressionHas(stream);
    case ID_MatchSet_StaticLacks:
        return new MatchSetStaticLacks(stream);
    case ID_MatchSet_RegularExpressionLacks:
        return new MatchSetRegularExpressionLacks(stream);
    default:
        Q_ASSERT(false);
        break;
    }
    return nullptr;
}


Element::Element() = default;

Element::Element(Element &&other) = default;

Element &Element::operator=(Element &&other) = default;

Element::Element(const Element &other) : matchers()
{
    for (const auto &m : other.matchers) {
        matchers.emplace_back(m->clone());
    }
}

Element &Element::operator=(const Element &other)
{
    std::vector<std::unique_ptr<MatchComponent>> nm;
    for (const auto &m : other.matchers) {
        nm.emplace_back(m->clone());
    }
    matchers = std::move(nm);
    return *this;
}

QString Element::describe() const
{
    QStringList components;
    for (const auto &m : matchers) {
        components.append(m->describe());
    }
    return components.join(',');
}

bool Element::matches(const SequenceName &name) const
{
    for (const auto &m : matchers) {
        if (!m->matches(name))
            return false;
    }
    return true;
}

bool Element::neverMatches() const
{
    for (const auto &m : matchers) {
        if (m->neverMatches())
            return true;
    }
    return false;
}

Archive::Selection Element::toArchiveSelection(const Archive::Selection::Match &defaultStations,
                                               const Archive::Selection::Match &defaultArchives,
                                               const Archive::Selection::Match &defaultVariables) const
{
    Archive::Selection result;
    result.stations = defaultStations;
    result.archives = defaultArchives;
    result.variables = defaultVariables;
    for (const auto &m : matchers) {
        if (!m->integrate(result)) {
            return Archive::Selection(SequenceName());
        }
    }
    return result;
}

Element::Element(const SequenceName &name) : matchers()
{
    matchers.emplace_back(new MatchVariableSingle(new MatchStringStatic(name.getVariable(), true)));
    {
        auto s = name.getFlavors();
        MatchSet *m;
        if (!s.empty()) {
            m = new MatchSetStaticExact(s);
        } else {
            m = new MatchSetEmpty;
        }
        matchers.emplace_back(new MatchFlavorsSingle(m));
    }
    matchers.emplace_back(new MatchArchiveSingle(new MatchStringStatic(name.getArchive())));
    matchers.emplace_back(new MatchStationSingle(new MatchStringStatic(name.getStation())));
}


template<typename Container>
bool canFlattenRegexp(const Container &patterns);

template<>
bool canFlattenRegexp(const Element::Pattern &pattern)
{
    return pattern == QRegularExpression::escape(pattern);
}

template<>
bool canFlattenRegexp(const std::string &pattern)
{ return canFlattenRegexp(QString::fromStdString(pattern)); }

template<typename Container>
bool canFlattenRegexp(const Container &patterns)
{
    for (const auto &p : patterns) {
        if (!canFlattenRegexp(p))
            return false;
    }
    return true;
}

Element::Element(SpecialMatch specialMatch) : matchers()
{
    switch (specialMatch) {
    case SpecialMatch::None:
        matchers.emplace_back(new MatchComponentNever);
        break;
    case SpecialMatch::All:
        break;
    case SpecialMatch::Data: {
        const auto &exclude = SequenceName::defaultLacksFlavors();
        if (!exclude.empty()) {
            MatchSet *m;
            if (canFlattenRegexp(exclude)) {
                m = new MatchSetStaticLacks(exclude);
            } else {
                m = new MatchSetRegularExpressionLacks(Util::to_qstringlist(exclude));
            }
            matchers.emplace_back(new MatchFlavorsSingle(m));
        }
        break;
    }
    }
}

static void trimEmptyElements(Element::PatternList &list)
{
    for (auto e = list.begin(); e != list.end();) {
        if (e->isEmpty()) {
            e = list.erase(e);
            continue;
        }
        ++e;
    }
}

static void trimEmptyElements(std::vector<SequenceName::Component> &list)
{
    for (auto e = list.begin(); e != list.end();) {
        if (e->empty()) {
            e = list.erase(e);
            continue;
        }
        ++e;
    }
}

Element::Element(const Pattern &station, const Pattern &archive, const Pattern &variable)
{
    if (!variable.isEmpty()) {
        MatchString *m;
        if (canFlattenRegexp(variable)) {
            m = new MatchStringStatic(variable, true);
        } else {
            m = new MatchStringRegularExpression(variable, true);
        }
        matchers.emplace_back(new MatchVariableSingle(m));
    }
    if (!archive.isEmpty()) {
        MatchString *m;
        if (canFlattenRegexp(archive)) {
            m = new MatchStringStatic(archive);
        } else {
            m = new MatchStringRegularExpression(archive);
        }
        matchers.emplace_back(new MatchArchiveSingle(m));
    }
    if (!station.isEmpty()) {
        MatchString *m;
        if (canFlattenRegexp(station)) {
            m = new MatchStringStatic(station);
        } else {
            m = new MatchStringRegularExpression(station);
        }
        matchers.emplace_back(new MatchStationSingle(m));
    }
}

Element Element::variable(const Pattern &variable)
{
    Element result;
    if (!variable.isEmpty()) {
        MatchString *m;
        if (canFlattenRegexp(variable)) {
            m = new MatchStringStatic(variable, true);
        } else {
            m = new MatchStringRegularExpression(variable, true);
        }
        result.matchers.emplace_back(new MatchVariableSingle(m));
    }
    return result;
}

Element::Element(const Pattern &station,
                 const Pattern &archive,
                 const Pattern &variable,
                 PatternList hasFlavors,
                 PatternList lacksFlavors)
{
    trimEmptyElements(hasFlavors);
    trimEmptyElements(lacksFlavors);

    if (!variable.isEmpty()) {
        MatchString *m;
        if (canFlattenRegexp(variable)) {
            m = new MatchStringStatic(variable, true);
        } else {
            m = new MatchStringRegularExpression(variable, true);
        }
        matchers.emplace_back(new MatchVariableSingle(m));
    }
    if (!hasFlavors.isEmpty()) {
        MatchSet *m;
        if (canFlattenRegexp(hasFlavors)) {
            m = new MatchSetStaticHas(hasFlavors);
        } else {
            m = new MatchSetRegularExpressionHas(hasFlavors);
        }
        matchers.emplace_back(new MatchFlavorsSingle(m));
    }
    if (!lacksFlavors.isEmpty()) {
        MatchSet *m;
        if (canFlattenRegexp(lacksFlavors)) {
            m = new MatchSetStaticLacks(lacksFlavors);
        } else {
            m = new MatchSetRegularExpressionLacks(lacksFlavors);
        }
        matchers.emplace_back(new MatchFlavorsSingle(m));
    }
    if (!archive.isEmpty()) {
        MatchString *m;
        if (canFlattenRegexp(archive)) {
            m = new MatchStringStatic(archive);
        } else {
            m = new MatchStringRegularExpression(archive);
        }
        matchers.emplace_back(new MatchArchiveSingle(m));
    }
    if (!station.isEmpty()) {
        MatchString *m;
        if (canFlattenRegexp(station)) {
            m = new MatchStringStatic(station);
        } else {
            m = new MatchStringRegularExpression(station);
        }
        matchers.emplace_back(new MatchStationSingle(m));
    }
}

Element::Element(const SequenceName::Component &station,
                 const SequenceName::Component &archive,
                 const SequenceName::Component &variable,
                 std::vector<SequenceName::Component> hasFlavors,
                 std::vector<SequenceName::Component> lacksFlavors)
{
    trimEmptyElements(hasFlavors);
    trimEmptyElements(lacksFlavors);

    if (!variable.empty()) {
        MatchString *m;
        if (canFlattenRegexp(variable)) {
            m = new MatchStringStatic(variable, true);
        } else {
            m = new MatchStringRegularExpression(variable, true);
        }
        matchers.emplace_back(new MatchVariableSingle(m));
    }
    if (!hasFlavors.empty()) {
        MatchSet *m;
        if (canFlattenRegexp(hasFlavors)) {
            m = new MatchSetStaticHas(hasFlavors);
        } else {
            m = new MatchSetRegularExpressionHas(Util::to_qstringlist(hasFlavors));
        }
        matchers.emplace_back(new MatchFlavorsSingle(m));
    }
    if (!lacksFlavors.empty()) {
        MatchSet *m;
        if (canFlattenRegexp(lacksFlavors)) {
            m = new MatchSetStaticLacks(lacksFlavors);
        } else {
            m = new MatchSetRegularExpressionLacks(Util::to_qstringlist(lacksFlavors));
        }
        matchers.emplace_back(new MatchFlavorsSingle(m));
    }
    if (!archive.empty()) {
        MatchString *m;
        if (canFlattenRegexp(archive)) {
            m = new MatchStringStatic(archive);
        } else {
            m = new MatchStringRegularExpression(archive);
        }
        matchers.emplace_back(new MatchArchiveSingle(m));
    }
    if (!station.empty()) {
        MatchString *m;
        if (canFlattenRegexp(station)) {
            m = new MatchStringStatic(station);
        } else {
            m = new MatchStringRegularExpression(station);
        }
        matchers.emplace_back(new MatchStationSingle(m));
    }
}

Element::Element(const Pattern &station, const Pattern &archive, const Pattern &variable,
                 const SequenceName::Flavors &exactFlavors)
{
    if (!variable.isEmpty()) {
        MatchString *m;
        if (canFlattenRegexp(variable)) {
            m = new MatchStringStatic(variable, true);
        } else {
            m = new MatchStringRegularExpression(variable, true);
        }
        matchers.emplace_back(new MatchVariableSingle(m));
    }
    if (exactFlavors.empty()) {
        matchers.emplace_back(new MatchFlavorsSingle(new MatchSetEmpty));
    } else {
        MatchSet *m;
        if (canFlattenRegexp(exactFlavors)) {
            m = new MatchSetStaticExact(exactFlavors);
        } else {
            m = new MatchSetRegularExpressionExact(Util::to_qstringlist(exactFlavors));
        }
        matchers.emplace_back(new MatchFlavorsSingle(m));
    }
    if (!archive.isEmpty()) {
        MatchString *m;
        if (canFlattenRegexp(archive)) {
            m = new MatchStringStatic(archive);
        } else {
            m = new MatchStringRegularExpression(archive);
        }
        matchers.emplace_back(new MatchArchiveSingle(m));
    }
    if (!station.isEmpty()) {
        MatchString *m;
        if (canFlattenRegexp(station)) {
            m = new MatchStringStatic(station);
        } else {
            m = new MatchStringRegularExpression(station);
        }
        matchers.emplace_back(new MatchStationSingle(m));
    }
}

Element::Element(const SequenceName::Component &station,
                 const SequenceName::Component &archive,
                 const SequenceName::Component &variable,
                 const SequenceName::Flavors &exactFlavors)
{
    if (!variable.empty()) {
        MatchString *m;
        if (canFlattenRegexp(variable)) {
            m = new MatchStringStatic(variable, true);
        } else {
            m = new MatchStringRegularExpression(variable, true);
        }
        matchers.emplace_back(new MatchVariableSingle(m));
    }
    if (exactFlavors.empty()) {
        matchers.emplace_back(new MatchFlavorsSingle(new MatchSetEmpty));
    } else {
        MatchSet *m;
        if (canFlattenRegexp(exactFlavors)) {
            m = new MatchSetStaticExact(exactFlavors);
        } else {
            m = new MatchSetRegularExpressionExact(Util::to_qstringlist(exactFlavors));
        }
        matchers.emplace_back(new MatchFlavorsSingle(m));
    }
    if (!archive.empty()) {
        MatchString *m;
        if (canFlattenRegexp(archive)) {
            m = new MatchStringStatic(archive);
        } else {
            m = new MatchStringRegularExpression(archive);
        }
        matchers.emplace_back(new MatchArchiveSingle(m));
    }
    if (!station.empty()) {
        MatchString *m;
        if (canFlattenRegexp(station)) {
            m = new MatchStringStatic(station);
        } else {
            m = new MatchStringRegularExpression(station);
        }
        matchers.emplace_back(new MatchStationSingle(m));
    }
}

Element::Element(PatternList stations, PatternList archives, PatternList variables)
{
    trimEmptyElements(stations);
    trimEmptyElements(archives);
    trimEmptyElements(variables);

    if (!variables.empty()) {
        std::vector<std::unique_ptr<MatchString>> add;
        for (const auto &variable : variables) {
            MatchString *m;
            if (canFlattenRegexp(variable)) {
                m = new MatchStringStatic(variable, true);
            } else {
                m = new MatchStringRegularExpression(variable, true);
            }
            add.emplace_back(m);
        }
        matchers.emplace_back(new MatchVariableComposite(std::move(add)));
    }
    if (!archives.empty()) {
        std::vector<std::unique_ptr<MatchString>> add;
        for (const auto &archive : archives) {
            MatchString *m;
            if (canFlattenRegexp(archive)) {
                m = new MatchStringStatic(archive);
            } else {
                m = new MatchStringRegularExpression(archive);
            }
            add.emplace_back(m);
        }
        matchers.emplace_back(new MatchArchiveComposite(std::move(add)));
    }
    if (!stations.empty()) {
        std::vector<std::unique_ptr<MatchString>> add;
        for (const auto &station : stations) {
            MatchString *m;
            if (canFlattenRegexp(station)) {
                m = new MatchStringStatic(station);
            } else {
                m = new MatchStringRegularExpression(station);
            }
            add.emplace_back(m);
        }
        matchers.emplace_back(new MatchStationComposite(std::move(add)));
    }
}

Element::Element(PatternList stations,
                 PatternList archives,
                 PatternList variables,
                 PatternList hasFlavors,
                 PatternList lacksFlavors)
{
    trimEmptyElements(stations);
    trimEmptyElements(archives);
    trimEmptyElements(variables);
    trimEmptyElements(hasFlavors);
    trimEmptyElements(lacksFlavors);

    if (!variables.empty()) {
        std::vector<std::unique_ptr<MatchString>> add;
        for (const auto &variable : variables) {
            MatchString *m;
            if (canFlattenRegexp(variable)) {
                m = new MatchStringStatic(variable, true);
            } else {
                m = new MatchStringRegularExpression(variable, true);
            }
            add.emplace_back(m);
        }
        if (add.size() != 1) {
            matchers.emplace_back(new MatchVariableComposite(std::move(add)));
        } else {
            matchers.emplace_back(new MatchVariableSingle(std::move(add.front())));
        }
    }
    if (!hasFlavors.isEmpty()) {
        MatchSet *m;
        if (canFlattenRegexp(hasFlavors)) {
            m = new MatchSetStaticHas(hasFlavors);
        } else {
            m = new MatchSetRegularExpressionHas(hasFlavors);
        }
        matchers.emplace_back(new MatchFlavorsSingle(m));
    }
    if (!lacksFlavors.isEmpty()) {
        MatchSet *m;
        if (canFlattenRegexp(lacksFlavors)) {
            m = new MatchSetStaticLacks(lacksFlavors);
        } else {
            m = new MatchSetRegularExpressionLacks(lacksFlavors);
        }
        matchers.emplace_back(new MatchFlavorsSingle(m));
    }
    if (!archives.empty()) {
        std::vector<std::unique_ptr<MatchString>> add;
        for (const auto &archive : archives) {
            MatchString *m;
            if (canFlattenRegexp(archive)) {
                m = new MatchStringStatic(archive);
            } else {
                m = new MatchStringRegularExpression(archive);
            }
            add.emplace_back(m);
        }
        if (add.size() != 1) {
            matchers.emplace_back(new MatchArchiveComposite(std::move(add)));
        } else {
            matchers.emplace_back(new MatchArchiveSingle(std::move(add.front())));
        }
    }
    if (!stations.empty()) {
        std::vector<std::unique_ptr<MatchString>> add;
        for (const auto &station : stations) {
            MatchString *m;
            if (canFlattenRegexp(station)) {
                m = new MatchStringStatic(station);
            } else {
                m = new MatchStringRegularExpression(station);
            }
            add.emplace_back(m);
        }
        if (add.size() != 1) {
            matchers.emplace_back(new MatchStationComposite(std::move(add)));
        } else {
            matchers.emplace_back(new MatchStationSingle(std::move(add.front())));
        }
    }
}

Element::Element(PatternList stations, PatternList archives, PatternList variables,
                 const SequenceName::Flavors &exactFlavors)
{
    trimEmptyElements(stations);
    trimEmptyElements(archives);
    trimEmptyElements(variables);

    if (!variables.empty()) {
        std::vector<std::unique_ptr<MatchString>> add;
        for (const auto &variable : variables) {
            MatchString *m;
            if (canFlattenRegexp(variable)) {
                m = new MatchStringStatic(variable, true);
            } else {
                m = new MatchStringRegularExpression(variable, true);
            }
            add.emplace_back(m);
        }
        if (add.size() != 1) {
            matchers.emplace_back(new MatchVariableComposite(std::move(add)));
        } else {
            matchers.emplace_back(new MatchVariableSingle(std::move(add.front())));
        }
    }
    if (exactFlavors.empty()) {
        matchers.emplace_back(new MatchFlavorsSingle(new MatchSetEmpty));
    } else {
        MatchSet *m;
        if (canFlattenRegexp(exactFlavors)) {
            m = new MatchSetStaticExact(exactFlavors);
        } else {
            m = new MatchSetRegularExpressionExact(Util::to_qstringlist(exactFlavors));
        }
        matchers.emplace_back(new MatchFlavorsSingle(m));
    }
    if (!archives.empty()) {
        std::vector<std::unique_ptr<MatchString>> add;
        for (const auto &archive : archives) {
            MatchString *m;
            if (canFlattenRegexp(archive)) {
                m = new MatchStringStatic(archive);
            } else {
                m = new MatchStringRegularExpression(archive);
            }
            add.emplace_back(m);
        }
        if (add.size() != 1) {
            matchers.emplace_back(new MatchArchiveComposite(std::move(add)));
        } else {
            matchers.emplace_back(new MatchArchiveSingle(std::move(add.front())));
        }
    }
    if (!stations.empty()) {
        std::vector<std::unique_ptr<MatchString>> add;
        for (const auto &station : stations) {
            MatchString *m;
            if (canFlattenRegexp(station)) {
                m = new MatchStringStatic(station);
            } else {
                m = new MatchStringRegularExpression(station);
            }
            add.emplace_back(m);
        }
        if (add.size() != 1) {
            matchers.emplace_back(new MatchStationComposite(std::move(add)));
        } else {
            matchers.emplace_back(new MatchStationSingle(std::move(add.front())));
        }
    }
}

static void setListFromConfig(const Variant::Read &config, Element::PatternList &list,
                              bool includeEmpty = false)
{
    if (!config.exists())
        return;
    list.clear();
    for (const auto &add : config.toChildren().keys()) {
        list.push_back(QString::fromStdString(add));
    }
    if (includeEmpty && list.empty())
        list.push_back(QString());
}

Element::ConfigurationOptions::ConfigurationOptions() : acceptMetadata(false),
                                                        acceptDefaultStation(false)
{ }

Element::Element(const Variant::Read &config,
                 const ConfigurationOptions &options,
                 PatternList stations,
                 PatternList archives,
                 PatternList variables,
                 PatternList hasFlavors,
                 PatternList lacksFlavors,
                 bool useExactFlavors)
{
    if (!config.exists()) {
        matchers.emplace_back(new MatchComponentNever);
        return;
    }

    QStringList exactFlavors;
    if (useExactFlavors) {
        exactFlavors = std::move(hasFlavors);
        trimEmptyElements(exactFlavors);
        if (exactFlavors.isEmpty()) {
            exactFlavors.push_back(QString());
        }

        hasFlavors = QStringList();
        lacksFlavors = QStringList();
    }

    trimEmptyElements(stations);
    trimEmptyElements(archives);
    trimEmptyElements(variables);
    trimEmptyElements(hasFlavors);
    trimEmptyElements(lacksFlavors);

    switch (config.getType()) {
    case Variant::Type::Empty:
        matchers.emplace_back(new MatchComponentNever);
        return;
    case Variant::Type::Boolean:
        if (!config.toBool()) {
            matchers.emplace_back(new MatchComponentNever);
        }
        return;
    case Variant::Type::String: {
        QStringList components = config.toQString().split(':', QString::KeepEmptyParts);
        switch (components.size()) {
        case 0:
            matchers.emplace_back(new MatchComponentNever);
            return;
        case 1:
            if (components[0].isEmpty()) {
                matchers.emplace_back(new MatchComponentNever);
                return;
            }
            variables.clear();
            variables.push_back(components[0]);
            break;
        case 2:
            archives.clear();
            if (!components[0].isEmpty())
                archives.push_back(components[0]);
            variables.clear();
            if (!components[1].isEmpty())
                variables.push_back(components[1]);
            break;
        case 3:
            stations.clear();
            if (!components[0].isEmpty())
                stations.push_back(components[0]);
            archives.clear();
            if (!components[1].isEmpty())
                archives.push_back(components[1]);
            variables.clear();
            if (!components[2].isEmpty())
                variables.push_back(components[2]);
            break;
        default: {
            stations.clear();
            if (!components.front().isEmpty())
                stations.push_back(std::move(components.front()));
            components.pop_front();

            archives.clear();
            if (!components.front().isEmpty())
                archives.push_back(std::move(components.front()));
            components.pop_front();

            variables.clear();
            if (!components.front().isEmpty())
                variables.push_back(std::move(components.front()));
            components.pop_front();

            hasFlavors.clear();
            lacksFlavors.clear();
            exactFlavors.clear();
            for (const auto &c : components) {
                if (c.startsWith('!') || c.startsWith('-')) {
                    exactFlavors.clear();
                    if (c.length() > 1)
                        lacksFlavors.push_back(c.mid(1));
                } else if (c.startsWith('=')) {
                    hasFlavors.clear();
                    lacksFlavors.clear();
                    if (c.length() == 1) {
                        exactFlavors.clear();
                        exactFlavors.push_back(QString());
                    } else if (c.length() > 1) {
                        exactFlavors.push_back(c.mid(1));
                    }
                } else {
                    exactFlavors.clear();
                    if (c.startsWith('+')) {
                        if (c.length() > 1)
                            hasFlavors.push_back(c.mid(1));
                    } else {
                        hasFlavors.push_back(c);
                    }
                }
            }

            break;
        }
        }
    }
    default: {
        setListFromConfig(config["Station"], stations);
        setListFromConfig(config["Archive"], archives);
        setListFromConfig(config["Variable"], variables);
        if (config["Flavors"].exists()) {
            hasFlavors.clear();
            lacksFlavors.clear();
            setListFromConfig(config["Flavors"], exactFlavors, true);
        } else {
            if (config["HasFlavors"].exists() || config["LacksFlavors"].exists()) {
                exactFlavors.clear();
            }
            setListFromConfig(config["HasFlavors"], hasFlavors);
            setListFromConfig(config["LacksFlavors"], lacksFlavors);
        }
        break;
    }
    }


    if (!variables.empty()) {
        std::vector<std::unique_ptr<MatchString>> add;
        for (const auto &variable : variables) {
            MatchString *m;
            if (canFlattenRegexp(variable)) {
                m = new MatchStringStatic(variable, true);
            } else {
                m = new MatchStringRegularExpression(variable, true);
            }
            add.emplace_back(m);
        }
        if (add.size() != 1) {
            matchers.emplace_back(new MatchVariableComposite(std::move(add)));
        } else {
            matchers.emplace_back(new MatchVariableSingle(std::move(add.front())));
        }
    }
    if (!hasFlavors.empty()) {
        MatchSet *m;
        if (canFlattenRegexp(hasFlavors)) {
            m = new MatchSetStaticHas(hasFlavors);
        } else {
            m = new MatchSetRegularExpressionHas(hasFlavors);
        }
        matchers.emplace_back(new MatchFlavorsSingle(m));
    }
    if (!lacksFlavors.empty()) {
        MatchSet *m;
        if (canFlattenRegexp(lacksFlavors)) {
            m = new MatchSetStaticLacks(lacksFlavors);
        } else {
            m = new MatchSetRegularExpressionLacks(lacksFlavors);
        }
        matchers.emplace_back(new MatchFlavorsSingle(m));
    }
    if (!exactFlavors.isEmpty()) {
        bool isAllEmpty = true;
        for (const auto &check : exactFlavors) {
            if (!check.isEmpty()) {
                isAllEmpty = false;
                break;
            }
        }
        MatchSet *m;
        if (isAllEmpty) {
            m = new MatchSetEmpty;
        } else if (canFlattenRegexp(exactFlavors)) {
            m = new MatchSetStaticExact(exactFlavors);
        } else {
            m = new MatchSetRegularExpressionExact(exactFlavors);
        }
        matchers.emplace_back(new MatchFlavorsSingle(m));
    }
    if (!archives.empty()) {
        std::vector<std::unique_ptr<MatchString>> add;
        for (const auto &archive : archives) {
            MatchString *m;
            if (canFlattenRegexp(archive)) {
                m = new MatchStringStatic(archive);
            } else {
                m = new MatchStringRegularExpression(archive);
            }
            add.emplace_back(m);
        }
        if (options.acceptMetadata) {
            if (add.size() != 1) {
                matchers.emplace_back(new MatchMetaArchiveComposite(std::move(add)));
            } else {
                matchers.emplace_back(new MatchMetaArchiveSingle(std::move(add.front())));
            }
        } else {
            if (add.size() != 1) {
                matchers.emplace_back(new MatchArchiveComposite(std::move(add)));
            } else {
                matchers.emplace_back(new MatchArchiveSingle(std::move(add.front())));
            }
        }
    }
    if (!stations.empty()) {
        std::vector<std::unique_ptr<MatchString>> add;
        for (const auto &station : stations) {
            MatchString *m;
            if (canFlattenRegexp(station)) {
                m = new MatchStringStatic(station);
            } else {
                m = new MatchStringRegularExpression(station);
            }
            add.emplace_back(m);
        }
        if (options.acceptDefaultStation) {
            if (add.size() != 1) {
                matchers.emplace_back(new MatchDefaultStationComposite(std::move(add)));
            } else {
                matchers.emplace_back(new MatchDefaultStationSingle(std::move(add.front())));
            }
        } else {
            if (add.size() != 1) {
                matchers.emplace_back(new MatchStationComposite(std::move(add)));
            } else {
                matchers.emplace_back(new MatchStationSingle(std::move(add.front())));
            }
        }
    }
}

Element::Element(const Variant::Read &config, const ConfigurationOptions &options) : Element(config,
                                                                                             options,
                                                                                             {}, {},
                                                                                             {}, {},
                                                                                             Util::to_qstringlist(
                                                                                                     SequenceName::defaultLacksFlavors()),
                                                                                             false)
{ }

Element::Element(const Variant::Read &config,
                 PatternList defaultStations,
                 PatternList defaultArchives,
                 PatternList defaultVariables,
                 const ConfigurationOptions &options) : Element(config, options,
                                                                std::move(defaultStations),
                                                                std::move(defaultArchives),
                                                                std::move(defaultVariables), {},
                                                                Util::to_qstringlist(
                                                                        SequenceName::defaultLacksFlavors()),
                                                                false)
{ }

Element::Element(const Variant::Read &config,
                 PatternList defaultStations,
                 PatternList defaultArchives,
                 PatternList defaultVariables,
                 PatternList defaultHasFlavors,
                 PatternList defaultLacksFlavors,
                 const ConfigurationOptions &options) : Element(config, options,
                                                                std::move(defaultStations),
                                                                std::move(defaultArchives),
                                                                std::move(defaultVariables),
                                                                std::move(defaultHasFlavors),
                                                                std::move(defaultLacksFlavors),
                                                                false)
{ }

Element::Element(const Variant::Read &config,
                 PatternList defaultStations,
                 PatternList defaultArchives,
                 PatternList defaultVariables,
                 const SequenceName::Flavors &defaultExactFlavors,
                 const ConfigurationOptions &options) : Element(config, options,
                                                                std::move(defaultStations),
                                                                std::move(defaultArchives),
                                                                std::move(defaultVariables),
                                                                Util::to_qstringlist(
                                                                        defaultExactFlavors), {},
                                                                true)
{ }


QDataStream &operator<<(QDataStream &stream, const Element &e)
{
    stream << (quint32) e.matchers.size();
    for (const auto &m : e.matchers) {
        m->serialize(stream);
    }
    return stream;
}

QDataStream &operator>>(QDataStream &stream, Element &e)
{
    e.matchers.clear();
    quint32 n = 0;
    stream >> n;
    for (quint32 i = 0; i < n; ++i) {
        e.matchers.emplace_back(SequenceMatch::Element::MatchComponent::deserialize(stream));
    }
    return stream;
}

QDebug operator<<(QDebug stream, const Element &e)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    QStringList components;
    for (const auto &m : e.matchers) {
        components.append(m->describe());
    }
    stream << "Element(" << components.join(',').toUtf8().constData() << ")";
    return stream;
}

SequenceName Element::reduce() const
{
    SequenceName result;
    if (!fill(result, false))
        return SequenceName();
    return result;
}

bool Element::fill(SequenceName &original, bool definedFlavors) const
{
    MatchComponent::Contents filled(original, definedFlavors);
    for (const auto &m : matchers) {
        if (!m->fill(original, filled))
            return false;
    }
    return filled.complete() && original.isValid();
}

void Element::setMatch(const SequenceName &target, bool includeFlavors)
{
    MatchComponent::Contents check(target, includeFlavors);
    for (auto m = matchers.begin(); m != matchers.end();) {
        if (!(*m)->appliesTo(check)) {
            ++m;
            continue;
        }
        m = matchers.erase(m);
    }

    if (check.flavors) {
        matchers.emplace_back(new Element::MatchFlavorsSingle(
                new Element::MatchSetStaticExact(target.getFlavors())));
    }
    if (check.variable) {
        matchers.emplace_back(new Element::MatchVariableSingle(
                new Element::MatchStringStatic(target.getVariable(), true)));
    }
    if (check.archive) {
        matchers.emplace_back(new Element::MatchArchiveSingle(
                new Element::MatchStringStatic(target.getArchive())));
    }
    if (check.station) {
        matchers.emplace_back(new Element::MatchStationSingle(
                new Element::MatchStringStatic(target.getStation())));
    }
}

void Element::clearMatch(bool station, bool archive, bool variable, bool flavors)
{
    /* If we can never match anything, refuse to clear unless it's an explicit
     * conversion to always match */
    if (neverMatches() && (!station || !archive || !variable || !flavors))
        return;

    MatchComponent::Contents check(station, archive, variable, flavors);
    for (auto m = matchers.begin(); m != matchers.end();) {
        if (!(*m)->appliesTo(check)) {
            ++m;
            continue;
        }
        m = matchers.erase(m);
    }
}


Composite::Composite() = default;

Composite::~Composite() = default;

Composite::Composite(const Composite &other) = default;

Composite &Composite::operator=(const Composite &other) = default;

Composite::Composite(Composite &&other) = default;

Composite &Composite::operator=(Composite &&other) = default;

size_t Composite::findMatching(const SequenceName &name) const
{
    auto check = elements.cbegin();
    for (std::size_t i = 0; i < elements.size(); ++i, ++check) {
        if (check->matches(name))
            return i;
    }
    return static_cast<std::size_t>(-1);
}

bool Composite::matches(const SequenceName &name) const
{ return Element::matchesAny(elements, name); }

QString Composite::describe() const
{
    QStringList parts;
    for (const auto &e : elements) {
        parts.append(e.describe());
    }
    return parts.join(':');
}

void Composite::append(const Element &element)
{ elements.emplace_back(element); }

void Composite::append(Element &&element)
{ elements.emplace_back(std::move(element)); }

void Composite::append(const SequenceName &name)
{ elements.emplace_back(name); }

void Composite::append(const Element::Pattern &station,
                       const Element::Pattern &archive,
                       const Element::Pattern &variable)
{ elements.emplace_back(station, archive, variable); }

void Composite::append(const Element::Pattern &station,
                       const Element::Pattern &archive,
                       const Element::Pattern &variable,
                       const SequenceName::Flavors &exactFlavors)
{ elements.emplace_back(station, archive, variable, exactFlavors); }

void Composite::append(const Element::Pattern &station,
                       const Element::Pattern &archive,
                       const Element::Pattern &variable,
                       Element::PatternList hasFlavors,
                       Element::PatternList lacksFlavors)
{
    elements.emplace_back(station, archive, variable, std::move(hasFlavors),
                          std::move(lacksFlavors));
}

void Composite::append(const Composite &other)
{ Util::append(other.elements, elements); }

void Composite::append(Composite &&other)
{ Util::append(std::move(other.elements), elements); }

void Composite::clearArchiveSpecification()
{
    for (auto &e : elements) {
        e.clearMatch(false, true, false, false);
    }
}

void Composite::requireFlavors(const SequenceName::Flavors &flavors)
{
    if (flavors.empty())
        return;
    for (auto &e : elements) {
        Element::MatchSet *m;
        if (canFlattenRegexp(flavors)) {
            m = new Element::MatchSetStaticHas(flavors);
        } else {
            m = new Element::MatchSetRegularExpressionHas(Util::to_qstringlist(flavors));
        }
        e.matchers.emplace_back(new Element::MatchFlavorsSingle(m));
    }
}

void Composite::excludeFlavors(const SequenceName::Flavors &flavors)
{
    if (flavors.empty())
        return;
    for (auto &e : elements) {
        Element::MatchSet *m;
        if (canFlattenRegexp(flavors)) {
            m = new Element::MatchSetStaticLacks(flavors);
        } else {
            m = new Element::MatchSetRegularExpressionLacks(Util::to_qstringlist(flavors));
        }
        e.matchers.emplace_back(new Element::MatchFlavorsSingle(m));
    }
}

bool Composite::valid() const
{ return !elements.empty(); }

SequenceName Composite::reduce() const
{
    if (elements.size() != 1)
        return SequenceName();
    return elements.front().reduce();
}

SequenceName::Set Composite::reduceMultiple() const
{
    SequenceName::Set result;
    for (const auto &e : elements) {
        auto add = e.reduce();
        if (!add.isValid())
            continue;
        result.insert(std::move(add));
    }
    return result;
}

Archive::Selection::List Composite::toArchiveSelections(const Archive::Selection::Match &defaultStations,
                                                        const Archive::Selection::Match &defaultArchives,
                                                        const Archive::Selection::Match &defaultVariables) const
{
    CPD3::Data::Archive::Selection::List result;
    for (const auto &e : elements) {
        result.emplace_back(
                e.toArchiveSelection(defaultStations, defaultArchives, defaultVariables));
    }
    return result;
}

Composite::Composite(const Element &element)
{ elements.emplace_back(element); }

Composite::Composite(Element &&element)
{ elements.emplace_back(std::move(element)); }

Composite::Composite(const std::vector<Element> &elements) : elements(elements)
{ }

Composite::Composite(std::vector<Element> &&elements) : elements(std::move(elements))
{ }

static Variant::Read normalizeConfiguration(const Variant::Read &config)
{
    switch (config.getType()) {
    case Variant::Type::Array:
        return config;

    case Variant::Type::String: {
        Variant::Write result;
        result.detachFromRoot();
        static thread_local QRegularExpression reSplit("[;,]+");
        auto a = result.toArray();
        for (const auto &p : config.toQString().split(reSplit, QString::SkipEmptyParts)) {
            a.after_back().setString(p);
        }
        return result;
    }

    default: {
        Variant::Write result;
        result.detachFromRoot();
        result.array(0).set(config);
        return result;
    }
    }
}

Composite::Composite(const Variant::Read &config, const ConfigurationOptions &options)
{
    for (const auto &p : normalizeConfiguration(config).toArray()) {
        elements.emplace_back(p, options);
    }
}

Composite::Composite(const Variant::Read &config,
                     const Element::PatternList &defaultStations,
                     const Element::PatternList &defaultArchives,
                     const Element::PatternList &defaultVariables,
                     const ConfigurationOptions &options)
{
    for (const auto &p : normalizeConfiguration(config).toArray()) {
        elements.emplace_back(p, defaultStations, defaultArchives, defaultVariables, options);
    }
}

Composite::Composite(const Variant::Read &config,
                     const Element::PatternList &defaultStations,
                     const Element::PatternList &defaultArchives,
                     const Element::PatternList &defaultVariables,
                     const Element::PatternList &defaultHasFlavors,
                     const Element::PatternList &defaultLacksFlavors,
                     const ConfigurationOptions &options)
{
    for (const auto &p : normalizeConfiguration(config).toArray()) {
        elements.emplace_back(p, defaultStations, defaultArchives, defaultVariables,
                              defaultHasFlavors, defaultLacksFlavors, options);
    }
}

Composite::Composite(const Variant::Read &config,
                     const Element::PatternList &defaultStations,
                     const Element::PatternList &defaultArchives,
                     const Element::PatternList &defaultVariables,
                     const SequenceName::Flavors &defaultExactFlavors,
                     const ConfigurationOptions &options)
{
    for (const auto &p : normalizeConfiguration(config).toArray()) {
        elements.emplace_back(p, defaultStations, defaultArchives, defaultVariables,
                              defaultExactFlavors, options);
    }
}

void Composite::addToConfiguration(Variant::Write &config, const SequenceName &name)
{
    config.set(normalizeConfiguration(config));

    auto add = config.toArray().after_back();
    add["Station"].setString(name.getStation());
    add["Archive"].setString(name.getArchive());
    add["Variable"].setString(name.getVariable());
    add["Flavors"].setType(Variant::Type::Array);
    {
        auto access = add["Flavors"].toArray();
        for (const auto &f : name.getFlavors()) {
            access.after_back().setString(f);
        }
    }
}

void Composite::addToConfiguration(Variant::Write &config,
                                   const Element::Pattern &station,
                                   const Element::Pattern &archive,
                                   const Element::Pattern &variable)
{
    config.set(normalizeConfiguration(config));

    auto add = config.toArray().after_back();
    add.setEmpty();
    if (!station.isEmpty())
        add["Station"].setString(station);
    if (!archive.isEmpty())
        add["Archive"].setString(archive);
    if (!variable.isEmpty())
        add["Variable"].setString(variable);
}

void Composite::addToConfiguration(Variant::Write &&config, const SequenceName &name)
{ return addToConfiguration(config, name); }

void Composite::addToConfiguration(Variant::Write &&config,
                                   const Element::Pattern &station,
                                   const Element::Pattern &archive,
                                   const Element::Pattern &variable)
{ return addToConfiguration(config, station, archive, variable); }

QDataStream &operator<<(QDataStream &stream, const Composite &c)
{
    stream << c.elements;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, Composite &c)
{
    stream >> c.elements;
    return stream;
}

QDebug operator<<(QDebug stream, const Composite &c)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "Composite(" << c.describe().toUtf8().constData() << ")";
    return stream;
}


OrderedLookup::OrderedLookup() = default;

OrderedLookup::~OrderedLookup() = default;

OrderedLookup::OrderedLookup(const OrderedLookup &other) = default;

OrderedLookup &OrderedLookup::operator=(const OrderedLookup &other) = default;

OrderedLookup::OrderedLookup(OrderedLookup &&other) = default;

OrderedLookup &OrderedLookup::operator=(OrderedLookup &&other) = default;

OrderedLookup::OrderedLookup(const Element &element) : Composite(element)
{ }

OrderedLookup::OrderedLookup(Element &&element) : Composite(std::move(element))
{ }

OrderedLookup::OrderedLookup(const std::vector<Element> &elements) : Composite(elements)
{ }

OrderedLookup::OrderedLookup(std::vector<Element> &&elements) : Composite(std::move(elements))
{ }

OrderedLookup::OrderedLookup(const Variant::Read &config,
                             const Composite::ConfigurationOptions &options) : Composite(config,
                                                                                         options)
{ }

OrderedLookup::OrderedLookup(const Variant::Read &config,
                             const Element::PatternList &defaultStations,
                             const Element::PatternList &defaultArchives,
                             const Element::PatternList &defaultVariables,
                             const Composite::ConfigurationOptions &options) : Composite(config,
                                                                                         defaultStations,
                                                                                         defaultArchives,
                                                                                         defaultVariables,
                                                                                         options)
{ }

OrderedLookup::OrderedLookup(const Variant::Read &config,
                             const Element::PatternList &defaultStations,
                             const Element::PatternList &defaultArchives,
                             const Element::PatternList &defaultVariables,
                             const Element::PatternList &defaultHasFlavors,
                             const Element::PatternList &defaultLacksFlavors,
                             const Composite::ConfigurationOptions &options) : Composite(config,
                                                                                         defaultStations,
                                                                                         defaultArchives,
                                                                                         defaultVariables,
                                                                                         defaultHasFlavors,
                                                                                         defaultLacksFlavors,
                                                                                         options)
{ }

OrderedLookup::OrderedLookup(const Variant::Read &config,
                             const Element::PatternList &defaultStations,
                             const Element::PatternList &defaultArchives,
                             const Element::PatternList &defaultVariables,
                             const SequenceName::Flavors &defaultExactFlavors,
                             const Composite::ConfigurationOptions &options) : Composite(config,
                                                                                         defaultStations,
                                                                                         defaultArchives,
                                                                                         defaultVariables,
                                                                                         defaultExactFlavors,
                                                                                         options)
{ }

QString OrderedLookup::describe() const
{
    QString result = Composite::describe();
    result.prepend("match=(");
    result.append("),known=(");
    bool first = true;
    for (const auto &k : known) {
        if (!first)
            result.append(',');
        first = false;
        result.append('{');
        result.append(k.name.describe());
        result.append('}');
    }
    result.append(')');
    return result;
}

bool OrderedLookup::Known::compare(const OrderedLookup::Known &a, const OrderedLookup::Known &b)
{
    if (a.index != b.index)
        return a.index < b.index;
    /* Intentionally reversed: we want to return "higher" priority names before
     * the lower ones (i.e. only use the default station as a fallback) */
    return SequenceName::OrderLogical()(b.name, a.name);
}

bool OrderedLookup::registerInput(const SequenceName &input)
{
    auto index = findMatching(input);
    if (index == static_cast<std::size_t>(-1))
        return false;

    Known add(index, input);
    auto check = std::lower_bound(known.begin(), known.end(), add, Known::compare);
    if (check != known.end()) {
        if (check->name == add.name)
            return true;
    }
    known.emplace(check, std::move(add));
    return true;
}

bool OrderedLookup::registerInput(SequenceName &&input)
{
    auto index = findMatching(input);
    if (index == static_cast<std::size_t>(-1))
        return false;

    Known add(index, std::move(input));
    auto check = std::lower_bound(known.begin(), known.end(), add, Known::compare);
    if (check != known.end()) {
        if (check->name == add.name)
            return true;
    }
    known.emplace(check, std::move(add));
    return true;
}

SequenceName::Set OrderedLookup::knownInputs() const
{
    SequenceName::Set result;
    for (const auto &k : known) {
        result.insert(k.name);
    }
    return result;
}

Variant::Write OrderedLookup::lookup(SequenceSegment &segment) const
{
    for (const auto &k : known) {
        if (k.name.isDefaultStation())
            continue;
        if (!segment.exists(k.name))
            continue;
        Variant::Write v = segment.getValue(k.name);
        if (!v.exists())
            continue;
        return v;
    }
    for (const auto &k : known) {
        if (!k.name.isDefaultStation())
            continue;
        if (!segment.exists(k.name))
            continue;
        Variant::Write v = segment.getValue(k.name);
        if (!v.exists())
            continue;
        return v;
    }
    return Variant::Write::empty();
}

Variant::Read OrderedLookup::lookup(const SequenceSegment &segment) const
{
    for (const auto &k : known) {
        if (k.name.isDefaultStation())
            continue;
        if (!segment.exists(k.name))
            continue;
        Variant::Read v = segment.getValue(k.name);
        if (!v.exists())
            continue;
        return v;
    }
    for (const auto &k : known) {
        if (!k.name.isDefaultStation())
            continue;
        if (!segment.exists(k.name))
            continue;
        Variant::Read v = segment.getValue(k.name);
        if (!v.exists())
            continue;
        return v;
    }
    return Variant::Read::empty();
}

void OrderedLookup::registerExpectedFlavorless(const SequenceName::Component &station,
                                               const SequenceName::Component &archive,
                                               const SequenceName::Component &variable)
{
    forAllElements([this, &station, &archive, &variable](const Element &e, std::size_t index) {
        SequenceName name(station, archive, variable);
        if (!e.fill(name, false))
            return;

        Known add(index, std::move(name));
        auto check = std::lower_bound(known.begin(), known.end(), add, Known::compare);
        if (check != known.end()) {
            if (check->name == add.name)
                return;
        }
        known.emplace(check, std::move(add));
    });
}

void OrderedLookup::registerExpected(const SequenceName::Component &station,
                                     const SequenceName::Component &archive,
                                     const SequenceName::Component &variable,
                                     const SequenceName::Flavors &flavors)
{
    forAllElements(
            [this, &station, &archive, &variable, &flavors](const Element &e, std::size_t index) {
                SequenceName name(station, archive, variable, flavors);
                if (!e.fill(name))
                    return;

                Known add(index, std::move(name));
                auto check = std::lower_bound(known.begin(), known.end(), add, Known::compare);
                if (check != known.end()) {
                    if (check->name == add.name)
                        return;
                }
                known.emplace(check, std::move(add));
            });
}

void OrderedLookup::registerExpected(const SequenceName::Component &station,
                                     const SequenceName::Component &archive,
                                     const SequenceName::Component &variable,
                                     const std::unordered_set<SequenceName::Flavors> &flavors)
{
    for (const auto &fl : flavors) {
        forAllElements(
                [this, &station, &archive, &variable, &fl](const Element &e, std::size_t index) {
                    SequenceName name(station, archive, variable, fl);
                    if (!e.fill(name))
                        return;

                    Known add(index, std::move(name));
                    auto check = std::lower_bound(known.begin(), known.end(), add, Known::compare);
                    if (check != known.end()) {
                        if (check->name == add.name)
                            return;
                    }
                    known.emplace(check, std::move(add));
                });
    }
}

QDataStream &operator<<(QDataStream &stream, const OrderedLookup &o)
{
    stream << static_cast<const Composite &>(o);
    Serialize::container(stream, o.known, [&stream](const OrderedLookup::Known &k) {
        stream << static_cast<quint32>(k.index) << k.name;
    });
    return stream;
}

QDataStream &operator>>(QDataStream &stream, OrderedLookup &o)
{
    stream >> static_cast<Composite &>(o);
    Deserialize::container(stream, o.known, [&stream]() {
        quint32 i = 0;
        stream >> i;
        SequenceName name;
        stream >> name;

        return OrderedLookup::Known(i, std::move(name));
    });
    return stream;
}

QDebug operator<<(QDebug stream, const OrderedLookup &o)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "OrderedLookup(" << o.describe().toUtf8().constData() << ")";
    return stream;
}


Basic::~Basic() = default;

namespace {
struct ArchiveSelectionSegment : public Time::Bounds {
    Archive::Selection::List selections;

    ArchiveSelectionSegment() = default;

    ArchiveSelectionSegment(const ArchiveSelectionSegment &) = default;

    ArchiveSelectionSegment &operator=(const ArchiveSelectionSegment &) = default;

    ArchiveSelectionSegment(ArchiveSelectionSegment &&) = default;

    ArchiveSelectionSegment &operator=(ArchiveSelectionSegment &&) = default;

    explicit ArchiveSelectionSegment(const Archive::Selection &selection) : Time::Bounds(
            selection.getStart(), selection.getEnd()), selections{selection}
    { }

    ArchiveSelectionSegment(const ArchiveSelectionSegment &other, double start, double end)
            : Time::Bounds(start, end), selections(other.selections)
    { }

    ArchiveSelectionSegment(const ArchiveSelectionSegment &under,
                            const ArchiveSelectionSegment &over,
                            double start,
                            double end) : Time::Bounds(start, end), selections(under.selections)
    {
        std::copy(over.selections.begin(), over.selections.end(), Util::back_emplacer(selections));
    }
};
}


enum {
    ID_Basic_Null = 0,
    ID_Basic_None,
    ID_Basic_Single,
    ID_Basic_Constant,
    ID_Basic_Bounded,
    ID_Basic_Dynamic,
};

namespace {
class BasicNone : public Basic {
public:
    BasicNone() = default;

    virtual ~BasicNone() = default;

    virtual void advance(double)
    { }

    virtual bool matches(const SequenceName &) const
    { return false; }

    virtual bool matches(const SequenceName &, double, double)
    { return false; }

    virtual bool matches(const SequenceName &, double, double) const
    { return false; }

    virtual bool matches(const SequenceName &, double)
    { return false; }

    virtual bool matches(const SequenceName &, double) const
    { return false; }

    virtual std::unique_ptr<Basic> clone() const
    { return std::unique_ptr<Basic>(new BasicNone); }

    virtual QString describe() const
    { return QString("None"); }

    explicit BasicNone(QDataStream &)
    { }

protected:
    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_Basic_None;
    }
};

class BasicSingle : public Basic {
    Element e;
public:
    BasicSingle(Element &&e) : e(std::move(e))
    { }

    virtual ~BasicSingle() = default;

    virtual void advance(double)
    { }

    virtual bool matches(const SequenceName &name) const
    { return e.matches(name); }

    virtual bool matches(const SequenceName &name, double, double)
    { return e.matches(name); }

    virtual bool matches(const SequenceName &name, double, double) const
    { return e.matches(name); }

    virtual bool matches(const SequenceName &name, double)
    { return e.matches(name); }

    virtual bool matches(const SequenceName &name, double) const
    { return e.matches(name); }

    virtual std::unique_ptr<Basic> clone() const
    { return std::unique_ptr<Basic>(new BasicSingle(*this)); }

    virtual QString describe() const
    { return e.describe(); }

    explicit BasicSingle(QDataStream &stream) : e()
    {
        stream >> e;
    }

protected:
    BasicSingle(const BasicSingle &) = default;

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_Basic_Single;
        stream << e;
    }
};

class BasicConstant : public Basic {
    Composite m;
public:
    explicit BasicConstant(std::vector<Element> &&e) : m(std::move(e))
    { }

    virtual ~BasicConstant() = default;

    virtual void advance(double)
    { }

    virtual bool matches(const SequenceName &name) const
    { return m.matches(name); }

    virtual bool matches(const SequenceName &name, double, double)
    { return BasicConstant::matches(name); }

    virtual bool matches(const SequenceName &name, double, double) const
    { return BasicConstant::matches(name); }

    virtual bool matches(const SequenceName &name, double)
    { return BasicConstant::matches(name); }

    virtual bool matches(const SequenceName &name, double) const
    { return BasicConstant::matches(name); }

    virtual std::unique_ptr<Basic> clone() const
    { return std::unique_ptr<Basic>(new BasicConstant(*this)); }

    virtual QString describe() const
    { return m.describe(); }

    explicit BasicConstant(QDataStream &stream)
    { stream >> m; }

protected:
    BasicConstant(const BasicConstant &) = default;

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_Basic_Constant;
        stream << m;
    }
};

class BasicBounded : public Basic {
    Composite m;
    double start;
    double end;

public:
    BasicBounded(std::vector<Element> &&e, double start, double end) : m(std::move(e)),
                                                                       start(start),
                                                                       end(end)
    { }

    virtual ~BasicBounded() = default;

    virtual void advance(double)
    { }

    virtual bool matches(const SequenceName &name) const
    { return m.matches(name); }

    virtual bool matches(const SequenceName &name, double start, double end)
    {
        if (!Range::intersects(start, end, this->start, this->end))
            return false;
        return BasicBounded::matches(name);
    }

    virtual bool matches(const SequenceName &name, double start, double end) const
    {
        if (!Range::intersects(start, end, this->start, this->end))
            return false;
        return BasicBounded::matches(name);
    }

    virtual bool matches(const SequenceName &name, double point)
    {
        if (Range::compareStart(point, start) < 0)
            return false;
        if (Range::compareStartEnd(point, end) >= 0)
            return false;
        return BasicBounded::matches(name);
    }

    virtual bool matches(const SequenceName &name, double point) const
    {
        if (Range::compareStart(point, start) < 0)
            return false;
        if (Range::compareStartEnd(point, end) >= 0)
            return false;
        return BasicBounded::matches(name);
    }

    virtual std::unique_ptr<Basic> clone() const
    { return std::unique_ptr<Basic>(new BasicBounded(*this)); }

    virtual QString describe() const
    {
        return QString("%1/%2/%3").arg(Time::toISO8601(start), Time::toISO8601(end), m.describe());
    }

    explicit BasicBounded(QDataStream &stream)
    { stream >> start >> end >> m; }

protected:
    BasicBounded(const BasicBounded &) = default;

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_Basic_Bounded;
        stream << start << end << m;
    }
};

class BasicDynamic : public Basic {
    struct Segment : Time::Bounds {
        Composite m;

        Segment(std::vector<Element> &&e, double start, double end) : Time::Bounds(start, end),
                                                                      m(std::move(e))
        { }

        Segment(double start, double end) : Time::Bounds(start, end)
        { }

        Segment() = default;

        Segment(const Segment &) = default;

        Segment &operator=(const Segment &) = default;

        Segment(Segment &&) = default;

        Segment &operator=(Segment &&) = default;

        explicit Segment(QDataStream &stream)
        {
            stream >> start >> end >> m;
        }

        void serialize(QDataStream &stream) const
        {
            stream << getStart() << getEnd() << m;
        }

        QString describe() const
        {
            return QString("%1/%2/%3").arg(Time::toISO8601(getStart()), Time::toISO8601(getEnd()),
                                           m.describe());
        }

        inline bool matches(const SequenceName &name) const
        { return m.matches(name); }
    };

    ConfigurationStream<Segment> segments;

public:
    BasicDynamic() = default;

    virtual ~BasicDynamic() = default;

    void add(std::vector<Element> &&elements, double start, double end)
    {
        segments.append(Segment(std::move(elements), start, end));
    }

    virtual void advance(double time)
    {
        segments.advance(time);
    }

    virtual bool matches(const SequenceName &name) const
    {
        return segments.applyUntil([&name](const Segment &check) {
            return check.matches(name);
        });
    }

    virtual bool matches(const SequenceName &name, double start, double end)
    {
        BasicDynamic::advance(start);
        return segments.applyUntil(start, end, [&name](const Segment &check) {
            return check.matches(name);
        });
    }

    virtual bool matches(const SequenceName &name, double start, double end) const
    {
        return segments.applyUntil(start, end, [&name](const Segment &check) {
            return check.matches(name);
        });
    }

    virtual bool matches(const SequenceName &name, double point)
    {
        if (!FP::defined(point))
            return false;
        BasicDynamic::advance(point);
        return segments.applySingle(point, [&name](const Segment &check) {
            return check.matches(name);
        });
    }

    virtual bool matches(const SequenceName &name, double point) const
    {
        if (!FP::defined(point))
            return false;
        return segments.applySingle(point, [&name](const Segment &check) {
            return check.matches(name);
        });
    }

    virtual std::unique_ptr<Basic> clone() const
    { return std::unique_ptr<Basic>(new BasicDynamic(*this)); }

    virtual QString describe() const
    {
        QStringList parts;
        segments.applyAll([&parts](const Segment &add) {
            parts.append(add.describe());
        });
        return parts.join(':');
    }

    explicit BasicDynamic(QDataStream &stream) : segments()
    {
        segments.deserialize(stream, [&stream]() {
            return Segment(stream);
        });
    }

protected:
    BasicDynamic(const BasicDynamic &) = default;

    virtual void serialize(QDataStream &stream) const
    {
        stream << (quint8) ID_Basic_Dynamic;
        segments.serialize(stream, [&stream](const Segment &seg) {
            seg.serialize(stream);
        });
    }
};


}


Basic *Basic::deserialize(QDataStream &stream)
{
    quint8 type = 0;
    stream >> type;
    switch (type) {
    case ID_Basic_Null:
        return nullptr;
    case ID_Basic_None:
        return new BasicNone(stream);
    case ID_Basic_Single:
        return new BasicSingle(stream);
    case ID_Basic_Constant:
        return new BasicConstant(stream);
    case ID_Basic_Bounded:
        return new BasicBounded(stream);
    case ID_Basic_Dynamic:
        return new BasicDynamic(stream);
    default:
        Q_ASSERT(false);
        return nullptr;
    }
    Q_ASSERT(false);
    return nullptr;
}

Basic::CompileOptions::CompileOptions() : ignoreStation(false),
                                          ignoreArchive(false),
                                          ignoreVariable(false),
                                          ignoreFlavors(false)
{ }

std::unique_ptr<Basic> Basic::compile(const Archive::Selection::List &selections,
                                      double limitStart,
                                      double limitEnd,
                                      const CompileOptions &options)
{
    std::deque<ArchiveSelectionSegment> segments;
    for (const auto &sel : selections) {
        if (!Range::intersects(limitStart, limitEnd, sel.getStart(), sel.getEnd()))
            continue;
        Range::overlayFragmenting(segments, ArchiveSelectionSegment(sel));
    }

    if (segments.empty())
        return std::unique_ptr<Basic>(new BasicNone);

    if (segments.size() == 1) {
        if (Range::compareStart(segments.front().getStart(), limitStart) <= 0 &&
                Range::compareEnd(segments.back().getEnd(), limitEnd) >= 0) {
            std::vector<Element> e = compileToElements(segments.front().selections, options);
            switch (e.size()) {
            case 0:
                return std::unique_ptr<Basic>(new BasicNone);
            case 1:
                return std::unique_ptr<Basic>(new BasicSingle(std::move(e.front())));
            default:
                return std::unique_ptr<Basic>(new BasicConstant(std::move(e)));
            }
            Q_ASSERT(false);
            return std::unique_ptr<Basic>();
        }

        ArchiveSelectionSegment &seg = segments.front();
        return std::unique_ptr<Basic>(
                new BasicBounded(compileToElements(seg.selections, options), seg.getStart(),
                                 seg.getEnd()));
    }

    auto result = new BasicDynamic;
    for (const auto &seg : segments) {
        result->add(compileToElements(seg.selections, options), seg.getStart(), seg.getEnd());
    }
    return std::unique_ptr<Basic>(result);
}

Element Basic::compileElement(const Archive::Selection &selection, const CompileOptions &options)
{
    Element e;
    if (!options.ignoreFlavors) {
        if (!selection.hasFlavors.empty()) {
            Element::MatchSet *m;
            if (canFlattenRegexp(selection.hasFlavors)) {
                m = new Element::MatchSetStaticHas(selection.hasFlavors);
            } else {
                m = new Element::MatchSetRegularExpressionHas(
                        Util::to_qstringlist(selection.hasFlavors));
            }
            e.matchers.emplace_back(new Element::MatchFlavorsSingle(m));
        }
        if (!selection.lacksFlavors.empty()) {
            Element::MatchSet *m;
            if (canFlattenRegexp(selection.lacksFlavors)) {
                m = new Element::MatchSetStaticLacks(selection.lacksFlavors);
            } else {
                m = new Element::MatchSetRegularExpressionLacks(
                        Util::to_qstringlist(selection.lacksFlavors));
            }
            e.matchers.emplace_back(new Element::MatchFlavorsSingle(m));
        }
        if (selection.exactFlavors.size() == 1 && selection.exactFlavors.begin()->empty()) {
            e.matchers.emplace_back(new Element::MatchFlavorsSingle(new Element::MatchSetEmpty));
        } else if (!selection.exactFlavors.empty()) {
            Element::MatchSet *m;
            if (canFlattenRegexp(selection.exactFlavors)) {
                m = new Element::MatchSetStaticExact(selection.exactFlavors);
            } else {
                m = new Element::MatchSetRegularExpressionExact(
                        Util::to_qstringlist(selection.exactFlavors));
            }
            e.matchers.emplace_back(new Element::MatchFlavorsSingle(m));
        }
    }

    if (!options.ignoreVariable && !selection.variables.empty()) {
        if (selection.variables.size() == 1 && selection.variables.begin()->empty())
            return Element(Element::SpecialMatch::None);
        std::vector<std::unique_ptr<Element::MatchString>> add;
        for (const auto &variable : selection.variables) {
            Element::MatchString *m;
            if (canFlattenRegexp(variable)) {
                m = new Element::MatchStringStatic(variable, true);
            } else {
                m = new Element::MatchStringRegularExpression(variable, true);
            }
            add.emplace_back(m);
        }
        if (add.size() != 1) {
            e.matchers.emplace_back(new Element::MatchVariableComposite(std::move(add)));
        } else {
            e.matchers.emplace_back(new Element::MatchVariableSingle(std::move(add.front())));
        }
    }

    if (!options.ignoreArchive) {
        if (!selection.archives.empty()) {
            if (selection.archives.size() == 1 && selection.archives.begin()->empty())
                return Element(Element::SpecialMatch::None);
            std::vector<std::unique_ptr<Element::MatchString>> add;
            for (const auto &archive : selection.archives) {
                Element::MatchString *m;
                if (canFlattenRegexp(archive)) {
                    m = new Element::MatchStringStatic(archive);
                } else {
                    m = new Element::MatchStringRegularExpression(archive);
                }
                add.emplace_back(m);
            }
            if (selection.includeMetaArchive) {
                if (add.size() != 1) {
                    e.matchers.emplace_back(new Element::MatchMetaArchiveComposite(std::move(add)));
                } else {
                    e.matchers
                     .emplace_back(new Element::MatchMetaArchiveSingle(std::move(add.front())));
                }
            } else {
                if (add.size() != 1) {
                    e.matchers.emplace_back(new Element::MatchArchiveComposite(std::move(add)));
                } else {
                    e.matchers
                     .emplace_back(new Element::MatchArchiveSingle(std::move(add.front())));
                }
            }
        } else if (!selection.includeMetaArchive) {
            e.matchers.emplace_back(new Element::MatchExcludeMetaArchive);
        }
    }

    if (!options.ignoreStation) {
        if (!selection.stations.empty()) {
            if (selection.stations.size() == 1 && selection.stations.begin()->empty() &&
                    !selection.includeDefaultStation) {
                return Element(Element::SpecialMatch::None);
            }
            std::vector<std::unique_ptr<Element::MatchString>> add;
            for (const auto &station : selection.stations) {
                Element::MatchString *m;
                if (canFlattenRegexp(station)) {
                    m = new Element::MatchStringStatic(station);
                } else {
                    m = new Element::MatchStringRegularExpression(station);
                }
                add.emplace_back(m);
            }
            if (selection.includeDefaultStation) {
                if (add.size() != 1) {
                    e.matchers
                     .emplace_back(new Element::MatchDefaultStationComposite(std::move(add)));
                } else {
                    e.matchers
                     .emplace_back(new Element::MatchDefaultStationSingle(std::move(add.front())));
                }
            } else {
                if (add.size() != 1) {
                    e.matchers.emplace_back(new Element::MatchStationComposite(std::move(add)));
                } else {
                    e.matchers
                     .emplace_back(new Element::MatchStationSingle(std::move(add.front())));
                }
            }
        } else if (!selection.includeDefaultStation) {
            e.matchers.emplace_back(new Element::MatchExcludeDefaultStation);
        }
    }

    return e;
}

std::vector<Element> Basic::compileToElements(const Archive::Selection::List &selections,
                                              const CompileOptions &options)
{
    std::vector<Element> result;
    for (const auto &selection : selections) {
        result.emplace_back(compileElement(selection, options));

        /* If we make a global matcher, just return a list of a single one that always matches */
        if (result.back().matchers.empty()) {
            return std::vector<Element>(1, Element());
        }
    }
    return result;
}


QDataStream &operator<<(QDataStream &stream, const std::unique_ptr<Basic> &b)
{
    if (!b) {
        stream << (quint8) SequenceMatch::ID_Basic_Null;
        return stream;
    }
    b->serialize(stream);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, std::unique_ptr<Basic> &b)
{
    b.reset(SequenceMatch::Basic::deserialize(stream));
    return stream;
}

QDebug operator<<(QDebug stream, const std::unique_ptr<Basic> &b)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    if (!b) {
        stream << "Basic(NULL)";
        return stream;
    }
    stream << "Basic(" << b->describe().toUtf8().constData() << ")";
    return stream;
}

}
}
}