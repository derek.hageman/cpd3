/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <sstream>

#include "metadata.hxx"
#include "overlay.hxx"
#include "serialization.hxx"
#include "core/util.hxx"


namespace CPD3 {
namespace Data {
namespace Variant {

NodeMetadataBase::~NodeMetadataBase() = default;

NodeMetadataBase::NodeMetadataBase() = default;

NodeMetadataBase::NodeMetadataBase(const NodeMetadataBase &other) : children(other.children)
{
    for (auto &mod : children) {
        mod.second->detach(mod.second);
    }
}

NodeMetadataBase::NodeMetadataBase(Children &&children) : children(std::move(children))
{ }

NodeMetadataBase::NodeMetadataBase(QDataStream &stream)
{
    std::size_t n = Serialization::deserializeShortLength(stream);
    for (std::size_t i = 0; i < n; ++i) {
        std::string key = Serialization::deserializeShortString(stream);
        children.emplace(std::move(key), Serialization::construct(stream));
    }
}

void NodeMetadataBase::serializeInternal(QDataStream &stream) const
{
    Serialization::serializeShortLength(stream, children.size());
    for (const auto &child : children) {
        Serialization::serializeShortString(stream, child.first);
        child.second->serialize(stream);
    }
}

std::shared_ptr<Node> NodeMetadataBase::read(const PathElement &path) const
{
    auto point = children.end();
    switch (path.type) {
    case PathElement::Type::Empty:
        point = children.find(PathElement::MetadataIndex());
        break;
    case PathElement::Type::AnyMetadata:
        point = children.find(path.ref<PathElement::MetadataIndex>());
        break;
    default:
        if (!pathCompatible(path.type))
            break;
        point = children.find(path.ref<PathElement::MetadataIndex>());
        break;
    }
    if (point == children.end())
        return std::shared_ptr<Node>();
    return point->second;
}

bool NodeMetadataBase::childrenWritable(PathElement::Type type) const
{
    switch (type) {
    case PathElement::Type::Empty:
    case PathElement::Type::AnyMetadata:
        return true;
    default:
        break;
    }
    return pathCompatible(type);
}

std::shared_ptr<Node> NodeMetadataBase::write(const PathElement &path, Node::WriteGoal final)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto &child = children[PathElement::MetadataIndex()];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    default: {
        Q_ASSERT(path.type == PathElement::Type::AnyMetadata || pathCompatible(path.type));
        auto &child = children[path.ref<PathElement::MetadataIndex>()];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    }
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMetadataBase::write(const PathElement &path, PathElement::Type next)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto &child = children[PathElement::MetadataIndex()];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    default: {
        Q_ASSERT(path.type == PathElement::Type::AnyMetadata || pathCompatible(path.type));
        auto &child = children[path.ref<PathElement::MetadataIndex>()];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    }
    return std::shared_ptr<Node>();
}

void NodeMetadataBase::insert(const PathElement &path, const std::shared_ptr<Node> &final)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto &child = children[PathElement::MetadataIndex()];
        child = final;
        child->detach(child);
        break;
    }
    default: {
        Q_ASSERT(path.type == PathElement::Type::AnyMetadata || pathCompatible(path.type));
        auto &child = children[path.ref<PathElement::MetadataIndex>()];
        child = final;
        child->detach(child);
        break;
    }
    }
}

void NodeMetadataBase::remove(const PathElement &path)
{
    switch (path.type) {
    case PathElement::Type::Empty:
        children.erase(PathElement::MetadataIndex());
        break;
    default:
        Q_ASSERT(path.type == PathElement::Type::AnyMetadata || pathCompatible(path.type));
        children.erase(path.ref<PathElement::MetadataIndex>());
        break;
    }
}

bool NodeMetadataBase::bypassFirstOverlay(const Node &over) const
{
    if (!typeCompatible(over.type()))
        return Node::bypassFirstOverlay(over);
    if (!children.empty())
        return false;
    return over.bypassSecondOverlay();
}

bool NodeMetadataBase::bypassSecondOverlay() const
{
    for (const auto &child : children) {
        if (!child.second->bypassSecondOverlay())
            return false;
    }
    return true;
}

bool NodeMetadataBase::isOverlayOpaque() const
{ return false; }

void NodeMetadataBase::overlayFirst(NodeMetadataBase &under, const NodeMetadataBase &over)
{
    Q_ASSERT(under.typeCompatible(under.type(), false));
    Q_ASSERT(under.typeCompatible(over.type()));

    for (const auto &add : over.children) {
        Overlay::first(under.children[add.first], add.second);
    }
}

void NodeMetadataBase::overlaySecond(NodeMetadataBase &under,
                                     const NodeMetadataBase &over,
                                     const TopLevel &origin,
                                     const Path &path,
                                     uint_fast16_t depth)
{
    Q_ASSERT(under.typeCompatible(under.type(), true));
    Q_ASSERT(under.typeCompatible(over.type()));

    /* First, recurse the second stage */
    Path childPath = path;
    for (const auto &add : over.children) {
        childPath.emplace_back(under.pathType(), add.first);
        Overlay::second(under.children[add.first], add.second, origin, childPath, depth);
        childPath.pop_back();
    }

    /* Now detach and make constant all children, so
     * future overlays can re-use them */
    for (auto &child : under.children) {
        child.second->detach(child.second);
    }
}

void NodeMetadataBase::clear()
{ children.clear(); }

bool NodeMetadataBase::identicalTo(const Node &other) const
{
    if (!typeCompatible(other.type()))
        return false;
    const auto &otherm = static_cast<const NodeMetadataBase &>(other);
    if (children.size() != otherm.children.size())
        return false;
    for (const auto &child : children) {
        auto check = otherm.children.find(child.first);
        if (check == otherm.children.end())
            return false;
        if (!child.second->identicalTo(*check->second))
            return false;
    }
    return true;
}

template<typename Map>
static std::string describeMap(const Map &map)
{
    std::stringstream str;

    std::vector<typename Map::const_iterator> sorted;
    for (auto add = map.begin(), end = map.end(); add != end; ++add) {
        sorted.emplace_back(add);
    }
    std::sort(sorted.begin(), sorted.end(),
              [](const typename Map::const_iterator &a, const typename Map::const_iterator &b) {
                  return a->first < b->first;
              });

    bool first = true;
    for (const auto &child : sorted) {
        if (!first)
            str << ",";
        first = false;

        if (child->first.find_first_of("\"\n\r\t ") == child->first.npos) {
            str << child->first << "=" << child->second->describe();
        } else {
            std::string key = child->first;
            Util::replace_all(key, "\"", "\\\"");
            Util::replace_all(key, "\n", "\\n");
            Util::replace_all(key, "\r", "\\r");
            Util::replace_all(key, "\t", "\\t");
            str << "\"" << key << "\"=" << child->second->describe();
        }
    }
    return str.str();
}

std::string NodeMetadataBase::describeInternal() const
{ return describeMap(children); }

std::size_t NodeMetadataBase::hashInternal() const
{
    std::size_t result = children.size();
    /* Just use simple XOR, since the order doesn't matter then */
    for (const auto &add : children) {
        result ^= std::hash<PathElement::MetadataIndex>()(add.first);
        result ^= add.second->hash();
    }
    return result;
}


NodeMetadataReal::~NodeMetadataReal() = default;

NodeMetadataReal::NodeMetadataReal() = default;

NodeMetadataReal::NodeMetadataReal(const NodeMetadataReal &other) = default;

NodeMetadataReal::NodeMetadataReal(NodeMetadataBase::Children &&children) : NodeMetadataBase(
        std::move(children))
{ }

NodeMetadataReal::NodeMetadataReal(QDataStream &stream) : NodeMetadataBase(stream)
{ }

void NodeMetadataReal::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_MetadataReal_v2);
    serializeInternal(stream);
}

Node::Type NodeMetadataReal::type() const
{ return Node::Type::MetadataReal; }

bool NodeMetadataReal::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::MetadataReal; }

void NodeMetadataReal::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeMetadataRealConstant>(*this); }

std::string NodeMetadataReal::describe() const
{ return std::string("MetadataReal(") + describeInternal() + ")"; }

std::size_t NodeMetadataReal::hash() const
{ return INTEGER::mix(Serialization::ID_MetadataReal_v2, hashInternal()); }

bool NodeMetadataReal::pathCompatible(PathElement::Type type) const
{ return type == PathElement::Type::MetadataReal; }

PathElement::Type NodeMetadataReal::pathType() const
{ return PathElement::Type::MetadataReal; }

bool NodeMetadataReal::typeCompatible(Node::Type type, bool allowConstant) const
{
    switch (type) {
    case Node::Type::MetadataRealConstant:
        if (!allowConstant)
            break;
    case Node::Type::MetadataReal:
        return true;
    default:
        break;
    }
    return false;
}

NodeMetadataRealConstant::~NodeMetadataRealConstant() = default;

NodeMetadataRealConstant::NodeMetadataRealConstant() = default;

NodeMetadataRealConstant::NodeMetadataRealConstant(const NodeMetadataReal &other)
        : NodeMetadataReal(other)
{ }

NodeMetadataRealConstant::NodeMetadataRealConstant(NodeMetadataBase::Children &&children)
        : NodeMetadataReal(std::move(children))
{ }

NodeMetadataRealConstant::NodeMetadataRealConstant(QDataStream &stream) : NodeMetadataReal(stream)
{ }

Node::Type NodeMetadataRealConstant::type() const
{ return Node::Type::MetadataRealConstant; }

void NodeMetadataRealConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeMetadataRealConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeMetadataRealConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeMetadataRealConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMetadataRealConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeMetadataRealConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeMetadataRealConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeMetadataRealConstant::clear()
{ Q_ASSERT(false); }


NodeMetadataInteger::~NodeMetadataInteger() = default;

NodeMetadataInteger::NodeMetadataInteger() = default;

NodeMetadataInteger::NodeMetadataInteger(const NodeMetadataInteger &other) = default;

NodeMetadataInteger::NodeMetadataInteger(NodeMetadataBase::Children &&children) : NodeMetadataBase(
        std::move(children))
{ }

NodeMetadataInteger::NodeMetadataInteger(QDataStream &stream) : NodeMetadataBase(stream)
{ }

void NodeMetadataInteger::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_MetadataInteger_v2);
    serializeInternal(stream);
}

Node::Type NodeMetadataInteger::type() const
{ return Node::Type::MetadataInteger; }

bool NodeMetadataInteger::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::MetadataInteger; }

void NodeMetadataInteger::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeMetadataIntegerConstant>(*this); }

std::string NodeMetadataInteger::describe() const
{ return std::string("MetadataInteger(") + describeInternal() + ")"; }

std::size_t NodeMetadataInteger::hash() const
{ return INTEGER::mix(Serialization::ID_MetadataInteger_v2, hashInternal()); }

bool NodeMetadataInteger::pathCompatible(PathElement::Type type) const
{ return type == PathElement::Type::MetadataInteger; }

PathElement::Type NodeMetadataInteger::pathType() const
{ return PathElement::Type::MetadataInteger; }

bool NodeMetadataInteger::typeCompatible(Node::Type type, bool allowConstant) const
{
    switch (type) {
    case Node::Type::MetadataIntegerConstant:
        if (!allowConstant)
            break;
    case Node::Type::MetadataInteger:
        return true;
    default:
        break;
    }
    return false;
}

NodeMetadataIntegerConstant::~NodeMetadataIntegerConstant() = default;

NodeMetadataIntegerConstant::NodeMetadataIntegerConstant() = default;

NodeMetadataIntegerConstant::NodeMetadataIntegerConstant(const NodeMetadataInteger &other)
        : NodeMetadataInteger(other)
{ }

NodeMetadataIntegerConstant::NodeMetadataIntegerConstant(NodeMetadataBase::Children &&children)
        : NodeMetadataInteger(std::move(children))
{ }

NodeMetadataIntegerConstant::NodeMetadataIntegerConstant(QDataStream &stream) : NodeMetadataInteger(
        stream)
{ }

Node::Type NodeMetadataIntegerConstant::type() const
{ return Node::Type::MetadataIntegerConstant; }

void NodeMetadataIntegerConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeMetadataIntegerConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeMetadataIntegerConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeMetadataIntegerConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMetadataIntegerConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeMetadataIntegerConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeMetadataIntegerConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeMetadataIntegerConstant::clear()
{ Q_ASSERT(false); }


NodeMetadataBoolean::~NodeMetadataBoolean() = default;

NodeMetadataBoolean::NodeMetadataBoolean() = default;

NodeMetadataBoolean::NodeMetadataBoolean(const NodeMetadataBoolean &other) = default;

NodeMetadataBoolean::NodeMetadataBoolean(NodeMetadataBase::Children &&children) : NodeMetadataBase(
        std::move(children))
{ }

NodeMetadataBoolean::NodeMetadataBoolean(QDataStream &stream) : NodeMetadataBase(stream)
{ }

void NodeMetadataBoolean::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_MetadataBoolean_v2);
    serializeInternal(stream);
}

Node::Type NodeMetadataBoolean::type() const
{ return Node::Type::MetadataBoolean; }

bool NodeMetadataBoolean::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::MetadataBoolean; }

void NodeMetadataBoolean::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeMetadataBooleanConstant>(*this); }

std::string NodeMetadataBoolean::describe() const
{ return std::string("MetadataBoolean(") + describeInternal() + ")"; }

std::size_t NodeMetadataBoolean::hash() const
{ return INTEGER::mix(Serialization::ID_MetadataBoolean_v2, hashInternal()); }

bool NodeMetadataBoolean::pathCompatible(PathElement::Type type) const
{ return type == PathElement::Type::MetadataBoolean; }

PathElement::Type NodeMetadataBoolean::pathType() const
{ return PathElement::Type::MetadataBoolean; }

bool NodeMetadataBoolean::typeCompatible(Node::Type type, bool allowConstant) const
{
    switch (type) {
    case Node::Type::MetadataBooleanConstant:
        if (!allowConstant)
            break;
    case Node::Type::MetadataBoolean:
        return true;
    default:
        break;
    }
    return false;
}

NodeMetadataBooleanConstant::~NodeMetadataBooleanConstant() = default;

NodeMetadataBooleanConstant::NodeMetadataBooleanConstant() = default;

NodeMetadataBooleanConstant::NodeMetadataBooleanConstant(const NodeMetadataBoolean &other)
        : NodeMetadataBoolean(other)
{ }

NodeMetadataBooleanConstant::NodeMetadataBooleanConstant(NodeMetadataBase::Children &&children)
        : NodeMetadataBoolean(std::move(children))
{ }

NodeMetadataBooleanConstant::NodeMetadataBooleanConstant(QDataStream &stream) : NodeMetadataBoolean(
        stream)
{ }

Node::Type NodeMetadataBooleanConstant::type() const
{ return Node::Type::MetadataBooleanConstant; }

void NodeMetadataBooleanConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeMetadataBooleanConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeMetadataBooleanConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeMetadataBooleanConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMetadataBooleanConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeMetadataBooleanConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeMetadataBooleanConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeMetadataBooleanConstant::clear()
{ Q_ASSERT(false); }


NodeMetadataString::~NodeMetadataString() = default;

NodeMetadataString::NodeMetadataString() = default;

NodeMetadataString::NodeMetadataString(const NodeMetadataString &other) = default;

NodeMetadataString::NodeMetadataString(NodeMetadataBase::Children &&children) : NodeMetadataBase(
        std::move(children))
{ }

NodeMetadataString::NodeMetadataString(QDataStream &stream) : NodeMetadataBase(stream)
{ }

void NodeMetadataString::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_MetadataString_v2);
    serializeInternal(stream);
}

Node::Type NodeMetadataString::type() const
{ return Node::Type::MetadataString; }

bool NodeMetadataString::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::MetadataString; }

void NodeMetadataString::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeMetadataStringConstant>(*this); }

std::string NodeMetadataString::describe() const
{ return std::string("MetadataString(") + describeInternal() + ")"; }

std::size_t NodeMetadataString::hash() const
{ return INTEGER::mix(Serialization::ID_MetadataString_v2, hashInternal()); }

bool NodeMetadataString::pathCompatible(PathElement::Type type) const
{ return type == PathElement::Type::MetadataString; }

PathElement::Type NodeMetadataString::pathType() const
{ return PathElement::Type::MetadataString; }

bool NodeMetadataString::typeCompatible(Node::Type type, bool allowConstant) const
{
    switch (type) {
    case Node::Type::MetadataStringConstant:
        if (!allowConstant)
            break;
    case Node::Type::MetadataString:
        return true;
    default:
        break;
    }
    return false;
}

NodeMetadataStringConstant::~NodeMetadataStringConstant() = default;

NodeMetadataStringConstant::NodeMetadataStringConstant() = default;

NodeMetadataStringConstant::NodeMetadataStringConstant(const NodeMetadataString &other)
        : NodeMetadataString(other)
{ }

NodeMetadataStringConstant::NodeMetadataStringConstant(NodeMetadataBase::Children &&children)
        : NodeMetadataString(std::move(children))
{ }

NodeMetadataStringConstant::NodeMetadataStringConstant(QDataStream &stream) : NodeMetadataString(
        stream)
{ }

Node::Type NodeMetadataStringConstant::type() const
{ return Node::Type::MetadataStringConstant; }

void NodeMetadataStringConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeMetadataStringConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeMetadataStringConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeMetadataStringConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMetadataStringConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeMetadataStringConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeMetadataStringConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeMetadataStringConstant::clear()
{ Q_ASSERT(false); }


NodeMetadataBytes::~NodeMetadataBytes() = default;

NodeMetadataBytes::NodeMetadataBytes() = default;

NodeMetadataBytes::NodeMetadataBytes(const NodeMetadataBytes &other) = default;

NodeMetadataBytes::NodeMetadataBytes(NodeMetadataBase::Children &&children) : NodeMetadataBase(
        std::move(children))
{ }

NodeMetadataBytes::NodeMetadataBytes(QDataStream &stream) : NodeMetadataBase(stream)
{ }

void NodeMetadataBytes::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_MetadataBytes_v2);
    serializeInternal(stream);
}

Node::Type NodeMetadataBytes::type() const
{ return Node::Type::MetadataBytes; }

bool NodeMetadataBytes::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::MetadataBytes; }

void NodeMetadataBytes::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeMetadataBytesConstant>(*this); }

std::string NodeMetadataBytes::describe() const
{ return std::string("MetadataBytes(") + describeInternal() + ")"; }

std::size_t NodeMetadataBytes::hash() const
{ return INTEGER::mix(Serialization::ID_MetadataBytes_v2, hashInternal()); }

bool NodeMetadataBytes::pathCompatible(PathElement::Type type) const
{ return type == PathElement::Type::MetadataBytes; }

PathElement::Type NodeMetadataBytes::pathType() const
{ return PathElement::Type::MetadataBytes; }

bool NodeMetadataBytes::typeCompatible(Node::Type type, bool allowConstant) const
{
    switch (type) {
    case Node::Type::MetadataBytesConstant:
        if (!allowConstant)
            break;
    case Node::Type::MetadataBytes:
        return true;
    default:
        break;
    }
    return false;
}

NodeMetadataBytesConstant::~NodeMetadataBytesConstant() = default;

NodeMetadataBytesConstant::NodeMetadataBytesConstant() = default;

NodeMetadataBytesConstant::NodeMetadataBytesConstant(const NodeMetadataBytes &other)
        : NodeMetadataBytes(other)
{ }

NodeMetadataBytesConstant::NodeMetadataBytesConstant(NodeMetadataBase::Children &&children)
        : NodeMetadataBytes(std::move(children))
{ }

NodeMetadataBytesConstant::NodeMetadataBytesConstant(QDataStream &stream) : NodeMetadataBytes(
        stream)
{ }

Node::Type NodeMetadataBytesConstant::type() const
{ return Node::Type::MetadataBytesConstant; }

void NodeMetadataBytesConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeMetadataBytesConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeMetadataBytesConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeMetadataBytesConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMetadataBytesConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeMetadataBytesConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeMetadataBytesConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeMetadataBytesConstant::clear()
{ Q_ASSERT(false); }


NodeMetadataFlags::~NodeMetadataFlags() = default;

NodeMetadataFlags::NodeMetadataFlags() = default;

NodeMetadataFlags::NodeMetadataFlags(const NodeMetadataFlags &other) = default;

NodeMetadataFlags::NodeMetadataFlags(NodeMetadataBase::Children &&children, Flags &&flags)
        : NodeMetadataBase(std::move(children)), flags(std::move(flags))
{ }

NodeMetadataFlags::NodeMetadataFlags(QDataStream &stream) : NodeMetadataBase(stream)
{
    std::size_t n = Serialization::deserializeShortLength(stream);
    for (std::size_t i = 0; i < n; ++i) {
        std::string key = Serialization::deserializeShortString(stream);
        flags.emplace(std::move(key), Serialization::construct(stream));
    }
}

void NodeMetadataFlags::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_MetadataFlags_v2);
    serializeInternal(stream);
    Serialization::serializeShortLength(stream, flags.size());
    for (const auto &flag : flags) {
        Serialization::serializeShortString(stream, flag.first);
        flag.second->serialize(stream);
    }
}

Node::Type NodeMetadataFlags::type() const
{ return Node::Type::MetadataFlags; }

bool NodeMetadataFlags::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::MetadataFlags; }

void NodeMetadataFlags::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeMetadataFlagsConstant>(*this); }

std::string NodeMetadataFlags::describe() const
{
    std::string result = "MetadataFlags(";
    result += describeInternal();
    result += " {";
    result += describeMap(flags);
    result += "})";
    return result;
}

std::size_t NodeMetadataFlags::hash() const
{
    std::size_t result = INTEGER::mix(Serialization::ID_MetadataFlags_v2, hashInternal());
    result = INTEGER::mix(result, flags.size());
    /* Just use simple XOR, since the order doesn't matter then */
    for (const auto &add : flags) {
        result ^= std::hash<PathElement::MetadataSingleFlagIndex>()(add.first);
        result ^= add.second->hash();
    }
    return result;
}

bool NodeMetadataFlags::pathCompatible(PathElement::Type type) const
{ return type == PathElement::Type::MetadataFlags; }

PathElement::Type NodeMetadataFlags::pathType() const
{ return PathElement::Type::MetadataFlags; }

bool NodeMetadataFlags::typeCompatible(Node::Type type, bool allowConstant) const
{
    switch (type) {
    case Node::Type::MetadataFlagsConstant:
        if (!allowConstant)
            break;
    case Node::Type::MetadataFlags:
        return true;
    default:
        break;
    }
    return false;
}

std::shared_ptr<Node> NodeMetadataFlags::read(const PathElement &path) const
{
    auto point = children.end();
    switch (path.type) {
    case PathElement::Type::Empty:
        point = children.find(PathElement::MetadataIndex());
        break;
    case PathElement::Type::AnyMetadata:
        point = children.find(path.ref<PathElement::MetadataIndex>());
        break;
    case PathElement::Type::AnyString: {
        auto check = flags.find(path.ref<PathElement::StringIndex>());
        if (check == flags.end()) {
            check = flags.find(path.ref<PathElement::MetadataSingleFlagIndex>());
            if (check == flags.end()) {
                return std::shared_ptr<Node>();
            }
        }
        return check->second;
    }
    case PathElement::Type::MetadataSingleFlag: {
        auto check = flags.find(path.ref<PathElement::MetadataSingleFlagIndex>());
        if (check == flags.end()) {
            check = flags.find(path.ref<PathElement::MetadataSingleFlagIndex>());
            if (check == flags.end()) {
                return std::shared_ptr<Node>();
            }
        }
        return check->second;
    }
    default:
        if (!pathCompatible(path.type))
            break;
        point = children.find(path.ref<PathElement::MetadataIndex>());
        break;
    }
    if (point == children.end())
        return std::shared_ptr<Node>();
    return point->second;
}

bool NodeMetadataFlags::childrenWritable(PathElement::Type type) const
{
    switch (type) {
    case PathElement::Type::Empty:
    case PathElement::Type::AnyMetadata:
    case PathElement::Type::AnyString:
    case PathElement::Type::MetadataSingleFlag:
        return true;
    default:
        break;
    }
    return pathCompatible(type);
}

std::shared_ptr<Node> NodeMetadataFlags::write(const PathElement &path, Node::WriteGoal final)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto &child = children[PathElement::MetadataIndex()];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    case PathElement::Type::AnyString: {
        auto &child = flags[path.ref<PathElement::StringIndex>()];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    case PathElement::Type::MetadataSingleFlag: {
        auto &child = flags[path.ref<PathElement::MetadataSingleFlagIndex>()];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    default: {
        Q_ASSERT(path.type == PathElement::Type::AnyMetadata || pathCompatible(path.type));
        auto &child = children[path.ref<PathElement::MetadataIndex>()];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    }
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMetadataFlags::write(const PathElement &path, PathElement::Type next)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto &child = children[PathElement::MetadataIndex()];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    case PathElement::Type::AnyString: {
        auto &child = flags[path.ref<PathElement::StringIndex>()];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    case PathElement::Type::MetadataSingleFlag: {
        auto &child = flags[path.ref<PathElement::MetadataSingleFlagIndex>()];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    default: {
        Q_ASSERT(path.type == PathElement::Type::AnyMetadata || pathCompatible(path.type));
        auto &child = children[path.ref<PathElement::MetadataIndex>()];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    }
    return std::shared_ptr<Node>();
}

void NodeMetadataFlags::insert(const PathElement &path, const std::shared_ptr<Node> &final)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto &child = children[PathElement::MetadataIndex()];
        child = final;
        child->detach(child);
        break;
    }
    case PathElement::Type::AnyString: {
        auto &child = flags[path.ref<PathElement::StringIndex>()];
        child = final;
        child->detach(child);
        break;
    }
    case PathElement::Type::MetadataSingleFlag: {
        auto &child = flags[path.ref<PathElement::MetadataSingleFlagIndex>()];
        child = final;
        child->detach(child);
        break;
    }
    default: {
        Q_ASSERT(path.type == PathElement::Type::AnyMetadata || pathCompatible(path.type));
        auto &child = children[path.ref<PathElement::MetadataIndex>()];
        child = final;
        child->detach(child);
        break;
    }
    }
}

void NodeMetadataFlags::remove(const PathElement &path)
{
    switch (path.type) {
    case PathElement::Type::Empty:
        children.erase(PathElement::MetadataIndex());
        break;
    case PathElement::Type::AnyString:
        flags.erase(path.ref<PathElement::StringIndex>());
        break;
    case PathElement::Type::MetadataSingleFlag:
        flags.erase(path.ref<PathElement::MetadataSingleFlagIndex>());
        break;
    default:
        Q_ASSERT(path.type == PathElement::Type::AnyMetadata || pathCompatible(path.type));
        children.erase(path.ref<PathElement::MetadataIndex>());
        break;
    }
}

bool NodeMetadataFlags::bypassFirstOverlay(const Node &over) const
{
    if (!typeCompatible(over.type()))
        return Node::bypassFirstOverlay(over);
    if (!children.empty())
        return false;
    if (!flags.empty())
        return false;
    return over.bypassSecondOverlay();
}

bool NodeMetadataFlags::bypassSecondOverlay() const
{
    for (const auto &child : children) {
        if (!child.second->bypassSecondOverlay())
            return false;
    }
    for (const auto &flag : flags) {
        if (!flag.second->bypassSecondOverlay())
            return false;
    }
    return true;
}

void NodeMetadataFlags::overlayFirst(NodeMetadataFlags &under, const NodeMetadataFlags &over)
{
    Q_ASSERT(under.typeCompatible(under.type(), false));
    Q_ASSERT(under.typeCompatible(over.type()));

    for (const auto &add : over.children) {
        Overlay::first(under.children[add.first], add.second);
    }
    for (const auto &add : over.flags) {
        Overlay::first(under.flags[add.first], add.second);
    }
}

void NodeMetadataFlags::overlaySecond(NodeMetadataFlags &under,
                                      const NodeMetadataFlags &over,
                                      const TopLevel &origin,
                                      const Path &path,
                                      uint_fast16_t depth)
{
    Q_ASSERT(under.typeCompatible(under.type(), true));
    Q_ASSERT(under.typeCompatible(over.type()));

    /* First, recurse the second stage */
    Path childPath = path;
    for (const auto &add : over.children) {
        childPath.emplace_back(under.pathType(), add.first);
        Overlay::second(under.children[add.first], add.second, origin, childPath, depth);
        childPath.pop_back();
    }
    for (const auto &add : over.flags) {
        childPath.emplace_back(PathElement::Type::MetadataSingleFlag, add.first);
        Overlay::second(under.flags[add.first], add.second, origin, childPath, depth);
        childPath.pop_back();
    }

    /* Now detach and make constant all children, so
     * future overlays can re-use them */
    for (auto &child : under.children) {
        child.second->detach(child.second);
    }
    for (auto &flag : under.flags) {
        flag.second->detach(flag.second);
    }
}

void NodeMetadataFlags::clear()
{
    children.clear();
    flags.clear();
}

bool NodeMetadataFlags::identicalTo(const Node &other) const
{
    if (!typeCompatible(other.type()))
        return false;
    const auto &otherm = static_cast<const NodeMetadataFlags &>(other);
    if (children.size() != otherm.children.size())
        return false;
    if (flags.size() != otherm.flags.size())
        return false;
    for (const auto &child : children) {
        auto check = otherm.children.find(child.first);
        if (check == otherm.children.end())
            return false;
        if (!child.second->identicalTo(*check->second))
            return false;
    }
    for (const auto &flag : flags) {
        auto check = otherm.flags.find(flag.first);
        if (check == otherm.flags.end())
            return false;
        if (!flag.second->identicalTo(*check->second))
            return false;
    }
    return true;
}

NodeMetadataFlagsConstant::~NodeMetadataFlagsConstant() = default;

NodeMetadataFlagsConstant::NodeMetadataFlagsConstant() = default;

NodeMetadataFlagsConstant::NodeMetadataFlagsConstant(const NodeMetadataFlags &other)
        : NodeMetadataFlags(other)
{ }

NodeMetadataFlagsConstant::NodeMetadataFlagsConstant(NodeMetadataBase::Children &&children,
                                                     NodeMetadataFlags::Flags &&flags)
        : NodeMetadataFlags(std::move(children), std::move(flags))
{ }

NodeMetadataFlagsConstant::NodeMetadataFlagsConstant(QDataStream &stream) : NodeMetadataFlags(
        stream)
{ }

Node::Type NodeMetadataFlagsConstant::type() const
{ return Node::Type::MetadataFlagsConstant; }

void NodeMetadataFlagsConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeMetadataFlagsConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeMetadataFlagsConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeMetadataFlagsConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMetadataFlagsConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeMetadataFlagsConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeMetadataFlagsConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeMetadataFlagsConstant::clear()
{ Q_ASSERT(false); }


NodeMetadataArray::~NodeMetadataArray() = default;

NodeMetadataArray::NodeMetadataArray() = default;

NodeMetadataArray::NodeMetadataArray(const NodeMetadataArray &other) = default;

NodeMetadataArray::NodeMetadataArray(NodeMetadataBase::Children &&children) : NodeMetadataBase(
        std::move(children))
{ }

NodeMetadataArray::NodeMetadataArray(QDataStream &stream) : NodeMetadataBase(stream)
{ }

void NodeMetadataArray::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_MetadataArray_v2);
    serializeInternal(stream);
}

Node::Type NodeMetadataArray::type() const
{ return Node::Type::MetadataArray; }

bool NodeMetadataArray::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::MetadataArray; }

void NodeMetadataArray::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeMetadataArrayConstant>(*this); }

std::string NodeMetadataArray::describe() const
{ return std::string("MetadataArray(") + describeInternal() + ")"; }

std::size_t NodeMetadataArray::hash() const
{ return INTEGER::mix(Serialization::ID_MetadataArray_v2, hashInternal()); }

bool NodeMetadataArray::pathCompatible(PathElement::Type type) const
{ return type == PathElement::Type::MetadataArray; }

PathElement::Type NodeMetadataArray::pathType() const
{ return PathElement::Type::MetadataArray; }

bool NodeMetadataArray::typeCompatible(Node::Type type, bool allowConstant) const
{
    switch (type) {
    case Node::Type::MetadataArrayConstant:
        if (!allowConstant)
            break;
    case Node::Type::MetadataArray:
        return true;
    default:
        break;
    }
    return false;
}

NodeMetadataArrayConstant::~NodeMetadataArrayConstant() = default;

NodeMetadataArrayConstant::NodeMetadataArrayConstant() = default;

NodeMetadataArrayConstant::NodeMetadataArrayConstant(const NodeMetadataArray &other)
        : NodeMetadataArray(other)
{ }

NodeMetadataArrayConstant::NodeMetadataArrayConstant(NodeMetadataBase::Children &&children)
        : NodeMetadataArray(std::move(children))
{ }

NodeMetadataArrayConstant::NodeMetadataArrayConstant(QDataStream &stream) : NodeMetadataArray(
        stream)
{ }

Node::Type NodeMetadataArrayConstant::type() const
{ return Node::Type::MetadataArrayConstant; }

void NodeMetadataArrayConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeMetadataArrayConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeMetadataArrayConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeMetadataArrayConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMetadataArrayConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeMetadataArrayConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeMetadataArrayConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeMetadataArrayConstant::clear()
{ Q_ASSERT(false); }


NodeMetadataMatrix::~NodeMetadataMatrix() = default;

NodeMetadataMatrix::NodeMetadataMatrix() = default;

NodeMetadataMatrix::NodeMetadataMatrix(const NodeMetadataMatrix &other) = default;

NodeMetadataMatrix::NodeMetadataMatrix(NodeMetadataBase::Children &&children) : NodeMetadataBase(
        std::move(children))
{ }

NodeMetadataMatrix::NodeMetadataMatrix(QDataStream &stream) : NodeMetadataBase(stream)
{ }

void NodeMetadataMatrix::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_MetadataMatrix_v2);
    serializeInternal(stream);
}

Node::Type NodeMetadataMatrix::type() const
{ return Node::Type::MetadataMatrix; }

bool NodeMetadataMatrix::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::MetadataMatrix; }

void NodeMetadataMatrix::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeMetadataMatrixConstant>(*this); }

std::string NodeMetadataMatrix::describe() const
{ return std::string("MetadataMatrix(") + describeInternal() + ")"; }

std::size_t NodeMetadataMatrix::hash() const
{ return INTEGER::mix(Serialization::ID_MetadataMatrix_v2, hashInternal()); }

bool NodeMetadataMatrix::pathCompatible(PathElement::Type type) const
{ return type == PathElement::Type::MetadataMatrix; }

PathElement::Type NodeMetadataMatrix::pathType() const
{ return PathElement::Type::MetadataMatrix; }

bool NodeMetadataMatrix::typeCompatible(Node::Type type, bool allowConstant) const
{
    switch (type) {
    case Node::Type::MetadataMatrixConstant:
        if (!allowConstant)
            break;
    case Node::Type::MetadataMatrix:
        return true;
    default:
        break;
    }
    return false;
}

NodeMetadataMatrixConstant::~NodeMetadataMatrixConstant() = default;

NodeMetadataMatrixConstant::NodeMetadataMatrixConstant() = default;

NodeMetadataMatrixConstant::NodeMetadataMatrixConstant(const NodeMetadataMatrix &other)
        : NodeMetadataMatrix(other)
{ }

NodeMetadataMatrixConstant::NodeMetadataMatrixConstant(NodeMetadataBase::Children &&children)
        : NodeMetadataMatrix(std::move(children))
{ }

NodeMetadataMatrixConstant::NodeMetadataMatrixConstant(QDataStream &stream) : NodeMetadataMatrix(
        stream)
{ }

Node::Type NodeMetadataMatrixConstant::type() const
{ return Node::Type::MetadataMatrixConstant; }

void NodeMetadataMatrixConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeMetadataMatrixConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeMetadataMatrixConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeMetadataMatrixConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMetadataMatrixConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeMetadataMatrixConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeMetadataMatrixConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeMetadataMatrixConstant::clear()
{ Q_ASSERT(false); }


NodeMetadataHash::~NodeMetadataHash() = default;

NodeMetadataHash::NodeMetadataHash() = default;

NodeMetadataHash::NodeMetadataHash(const NodeMetadataHash &other) = default;

NodeMetadataHash::NodeMetadataHash(NodeMetadataBase::Children &&children, Keys &&keys)
        : NodeMetadataBase(std::move(children)), keys(std::move(keys))
{ }

NodeMetadataHash::NodeMetadataHash(QDataStream &stream) : NodeMetadataBase(stream)
{
    std::size_t n = Serialization::deserializeShortLength(stream);
    for (std::size_t i = 0; i < n; ++i) {
        std::string key = Serialization::deserializeShortString(stream);
        keys.emplace(std::move(key), Serialization::construct(stream));
    }
}

void NodeMetadataHash::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_MetadataHash_v2);
    serializeInternal(stream);
    Serialization::serializeShortLength(stream, keys.size());
    for (const auto &key : keys) {
        Serialization::serializeShortString(stream, key.first);
        key.second->serialize(stream);
    }
}

Node::Type NodeMetadataHash::type() const
{ return Node::Type::MetadataHash; }

bool NodeMetadataHash::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::MetadataHash; }

void NodeMetadataHash::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeMetadataHashConstant>(*this); }

std::string NodeMetadataHash::describe() const
{
    std::string result = "MetadataHash(";
    result += describeInternal();
    result += " {";
    result += describeMap(keys);
    result += "})";
    return result;
}

std::size_t NodeMetadataHash::hash() const
{
    std::size_t result = INTEGER::mix(Serialization::ID_MetadataHash_v2, hashInternal());
    result = INTEGER::mix(result, keys.size());
    /* Just use simple XOR, since the order doesn't matter then */
    for (const auto &add : keys) {
        result ^= std::hash<PathElement::MetadataHashChildIndex>()(add.first);
        result ^= add.second->hash();
    }
    return result;
}

bool NodeMetadataHash::pathCompatible(PathElement::Type type) const
{ return type == PathElement::Type::MetadataHash; }

PathElement::Type NodeMetadataHash::pathType() const
{ return PathElement::Type::MetadataHash; }

bool NodeMetadataHash::typeCompatible(Node::Type type, bool allowConstant) const
{
    switch (type) {
    case Node::Type::MetadataHashConstant:
        if (!allowConstant)
            break;
    case Node::Type::MetadataHash:
        return true;
    default:
        break;
    }
    return false;
}

std::shared_ptr<Node> NodeMetadataHash::read(const PathElement &path) const
{
    auto point = children.end();
    switch (path.type) {
    case PathElement::Type::Empty:
        point = children.find(PathElement::MetadataIndex());
        break;
    case PathElement::Type::AnyMetadata:
        point = children.find(path.ref<PathElement::MetadataIndex>());
        break;
    case PathElement::Type::AnyString: {
        auto check = keys.find(path.ref<PathElement::StringIndex>());
        if (check == keys.end()) {
            check = keys.find(PathElement::MetadataHashChildIndex());
            if (check == keys.end()) {
                return std::shared_ptr<Node>();
            }
        }
        return check->second;
    }
    case PathElement::Type::MetadataHashChild: {
        auto check = keys.find(path.ref<PathElement::MetadataHashChildIndex>());
        if (check == keys.end()) {
            check = keys.find(PathElement::MetadataHashChildIndex());
            if (check == keys.end()) {
                return std::shared_ptr<Node>();
            }
        }
        return check->second;
    }
    default:
        if (!pathCompatible(path.type))
            break;
        point = children.find(path.ref<PathElement::MetadataIndex>());
        break;
    }
    if (point == children.end())
        return std::shared_ptr<Node>();
    return point->second;
}

bool NodeMetadataHash::childrenWritable(PathElement::Type type) const
{
    switch (type) {
    case PathElement::Type::Empty:
    case PathElement::Type::AnyMetadata:
    case PathElement::Type::AnyString:
    case PathElement::Type::MetadataHashChild:
        return true;
    default:
        break;
    }
    return pathCompatible(type);
}

std::shared_ptr<Node> NodeMetadataHash::write(const PathElement &path, Node::WriteGoal final)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto &child = children[PathElement::MetadataIndex()];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    case PathElement::Type::AnyString: {
        auto &child = keys[path.ref<PathElement::StringIndex>()];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    case PathElement::Type::MetadataHashChild: {
        auto &child = keys[path.ref<PathElement::MetadataHashChildIndex>()];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    default: {
        Q_ASSERT(path.type == PathElement::Type::AnyMetadata || pathCompatible(path.type));
        auto &child = children[path.ref<PathElement::MetadataIndex>()];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    }
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMetadataHash::write(const PathElement &path, PathElement::Type next)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto &child = children[PathElement::MetadataIndex()];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    case PathElement::Type::AnyString: {
        auto &child = keys[path.ref<PathElement::StringIndex>()];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    case PathElement::Type::MetadataHashChild: {
        auto &child = keys[path.ref<PathElement::MetadataHashChildIndex>()];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    default: {
        Q_ASSERT(path.type == PathElement::Type::AnyMetadata || pathCompatible(path.type));
        auto &child = children[path.ref<PathElement::MetadataIndex>()];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    }
    return std::shared_ptr<Node>();
}

void NodeMetadataHash::insert(const PathElement &path, const std::shared_ptr<Node> &final)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto &child = children[PathElement::MetadataIndex()];
        child = final;
        child->detach(child);
        break;
    }
    case PathElement::Type::AnyString: {
        auto &child = keys[path.ref<PathElement::StringIndex>()];
        child = final;
        child->detach(child);
        break;
    }
    case PathElement::Type::MetadataHashChild: {
        auto &child = keys[path.ref<PathElement::MetadataHashChildIndex>()];
        child = final;
        child->detach(child);
        break;
    }
    default: {
        Q_ASSERT(path.type == PathElement::Type::AnyMetadata || pathCompatible(path.type));
        auto &child = children[path.ref<PathElement::MetadataIndex>()];
        child = final;
        child->detach(child);
        break;
    }
    }
}

void NodeMetadataHash::remove(const PathElement &path)
{
    switch (path.type) {
    case PathElement::Type::Empty:
        children.erase(PathElement::MetadataIndex());
        break;
    case PathElement::Type::AnyString:
        keys.erase(path.ref<PathElement::StringIndex>());
        break;
    case PathElement::Type::MetadataHashChild:
        keys.erase(path.ref<PathElement::MetadataHashChildIndex>());
        break;
    default:
        Q_ASSERT(path.type == PathElement::Type::AnyMetadata || pathCompatible(path.type));
        children.erase(path.ref<PathElement::MetadataIndex>());
        break;
    }
}

bool NodeMetadataHash::bypassFirstOverlay(const Node &over) const
{
    if (!typeCompatible(over.type()))
        return Node::bypassFirstOverlay(over);
    if (!children.empty())
        return false;
    if (!keys.empty())
        return false;
    return over.bypassSecondOverlay();
}

bool NodeMetadataHash::bypassSecondOverlay() const
{
    for (const auto &child : children) {
        if (!child.second->bypassSecondOverlay())
            return false;
    }
    for (const auto &key : keys) {
        if (!key.second->bypassSecondOverlay())
            return false;
    }
    return true;
}

void NodeMetadataHash::overlayFirst(NodeMetadataHash &under, const NodeMetadataHash &over)
{
    Q_ASSERT(under.typeCompatible(under.type(), false));
    Q_ASSERT(under.typeCompatible(over.type()));

    for (const auto &add : over.children) {
        Overlay::first(under.children[add.first], add.second);
    }
    for (const auto &add : over.keys) {
        Overlay::first(under.keys[add.first], add.second);
    }
}

void NodeMetadataHash::overlaySecond(NodeMetadataHash &under,
                                     const NodeMetadataHash &over,
                                     const TopLevel &origin,
                                     const Path &path,
                                     uint_fast16_t depth)
{
    Q_ASSERT(under.typeCompatible(under.type(), true));
    Q_ASSERT(under.typeCompatible(over.type()));

    /* First, recurse the second stage */
    Path childPath = path;
    for (const auto &add : over.children) {
        childPath.emplace_back(under.pathType(), add.first);
        Overlay::second(under.children[add.first], add.second, origin, childPath, depth);
        childPath.pop_back();
    }
    for (const auto &add : over.keys) {
        childPath.emplace_back(PathElement::Type::MetadataHashChild, add.first);
        Overlay::second(under.keys[add.first], add.second, origin, childPath, depth);
        childPath.pop_back();
    }

    /* Now detach and make constant all children, so
     * future overlays can re-use them */
    for (auto &child : under.children) {
        child.second->detach(child.second);
    }
    for (auto &key : under.keys) {
        key.second->detach(key.second);
    }
}

void NodeMetadataHash::clear()
{
    children.clear();
    keys.clear();
}

bool NodeMetadataHash::identicalTo(const Node &other) const
{
    if (!typeCompatible(other.type()))
        return false;
    const auto &otherm = static_cast<const NodeMetadataHash &>(other);
    if (children.size() != otherm.children.size())
        return false;
    if (keys.size() != otherm.keys.size())
        return false;
    for (const auto &child : children) {
        auto check = otherm.children.find(child.first);
        if (check == otherm.children.end())
            return false;
        if (!child.second->identicalTo(*check->second))
            return false;
    }
    for (const auto &key : keys) {
        auto check = otherm.keys.find(key.first);
        if (check == otherm.keys.end())
            return false;
        if (!key.second->identicalTo(*check->second))
            return false;
    }
    return true;
}

NodeMetadataHashConstant::~NodeMetadataHashConstant() = default;

NodeMetadataHashConstant::NodeMetadataHashConstant() = default;

NodeMetadataHashConstant::NodeMetadataHashConstant(const NodeMetadataHash &other)
        : NodeMetadataHash(other)
{ }

NodeMetadataHashConstant::NodeMetadataHashConstant(NodeMetadataBase::Children &&children,
                                                   NodeMetadataHash::Keys &&keys)
        : NodeMetadataHash(std::move(children), std::move(keys))
{ }

NodeMetadataHashConstant::NodeMetadataHashConstant(QDataStream &stream) : NodeMetadataHash(stream)
{ }

Node::Type NodeMetadataHashConstant::type() const
{ return Node::Type::MetadataHashConstant; }

void NodeMetadataHashConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeMetadataHashConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeMetadataHashConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeMetadataHashConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMetadataHashConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeMetadataHashConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeMetadataHashConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeMetadataHashConstant::clear()
{ Q_ASSERT(false); }


NodeMetadataKeyframe::~NodeMetadataKeyframe() = default;

NodeMetadataKeyframe::NodeMetadataKeyframe() = default;

NodeMetadataKeyframe::NodeMetadataKeyframe(const NodeMetadataKeyframe &other) = default;

NodeMetadataKeyframe::NodeMetadataKeyframe(NodeMetadataBase::Children &&children)
        : NodeMetadataBase(std::move(children))
{ }

NodeMetadataKeyframe::NodeMetadataKeyframe(QDataStream &stream) : NodeMetadataBase(stream)
{ }

void NodeMetadataKeyframe::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_MetadataKeyframe_v2);
    serializeInternal(stream);
}

Node::Type NodeMetadataKeyframe::type() const
{ return Node::Type::MetadataKeyframe; }

bool NodeMetadataKeyframe::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::MetadataKeyframe; }

void NodeMetadataKeyframe::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeMetadataKeyframeConstant>(*this); }

std::string NodeMetadataKeyframe::describe() const
{ return std::string("MetadataKeyframe(") + describeInternal() + ")"; }

std::size_t NodeMetadataKeyframe::hash() const
{ return INTEGER::mix(Serialization::ID_MetadataKeyframe_v2, hashInternal()); }

bool NodeMetadataKeyframe::pathCompatible(PathElement::Type type) const
{ return type == PathElement::Type::MetadataKeyframe; }

PathElement::Type NodeMetadataKeyframe::pathType() const
{ return PathElement::Type::MetadataKeyframe; }

bool NodeMetadataKeyframe::typeCompatible(Node::Type type, bool allowConstant) const
{
    switch (type) {
    case Node::Type::MetadataKeyframeConstant:
        if (!allowConstant)
            break;
    case Node::Type::MetadataKeyframe:
        return true;
    default:
        break;
    }
    return false;
}

NodeMetadataKeyframeConstant::~NodeMetadataKeyframeConstant() = default;

NodeMetadataKeyframeConstant::NodeMetadataKeyframeConstant() = default;

NodeMetadataKeyframeConstant::NodeMetadataKeyframeConstant(const NodeMetadataKeyframe &other)
        : NodeMetadataKeyframe(other)
{ }

NodeMetadataKeyframeConstant::NodeMetadataKeyframeConstant(NodeMetadataBase::Children &&children)
        : NodeMetadataKeyframe(std::move(children))
{ }

NodeMetadataKeyframeConstant::NodeMetadataKeyframeConstant(QDataStream &stream)
        : NodeMetadataKeyframe(stream)
{ }

Node::Type NodeMetadataKeyframeConstant::type() const
{ return Node::Type::MetadataKeyframeConstant; }

void NodeMetadataKeyframeConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeMetadataKeyframeConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeMetadataKeyframeConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeMetadataKeyframeConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMetadataKeyframeConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeMetadataKeyframeConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeMetadataKeyframeConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeMetadataKeyframeConstant::clear()
{ Q_ASSERT(false); }


}
}
}