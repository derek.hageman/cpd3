/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "datacore/variant/root.hxx"
#include "datacore/variant/parse.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::Variant;

class TestComposite : public QObject {
Q_OBJECT

private
    slots:

    void inputHandling()
    {
        Write v = Write::empty();
        Write m = Write::empty();
        QString s;

        m.setEmpty();
        v.setDouble(1.0);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("1.0"));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toDouble(), 1.0);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toDouble(), 1.0);
        v.setEmpty();
        QVERIFY(!Parse::fromInputString(v, s, m, v, false));

        m.metadataReal("Format").setString("0.00");
        v.setDouble(1.0);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("1.00"));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toDouble(), 1.0);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toDouble(), 1.0);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v, false));
        QCOMPARE(v.toDouble(), 1.0);
        s = "1";
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v, false));
        QCOMPARE(v.getType(), Type::Real);
        QCOMPARE(v.toDouble(), 1.0);

        m.setEmpty();
        v.setDouble(FP::undefined());
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("NaN"));
        v.setDouble(1.0);
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.getType(), Type::Real);
        QVERIFY(!FP::defined(v.toDouble()));

        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, "412110912003"));
        QCOMPARE(v.getType(), Type::Integer);
        QCOMPARE((qint64) v.toInteger(), (qint64) Q_INT64_C(412110912003));


        m.setEmpty();
        v.setInt64(1);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("1"));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toInt64(), Q_INT64_C(1));
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toInt64(), Q_INT64_C(1));
        v.setEmpty();
        QVERIFY(!Parse::fromInputString(v, s, m, v, false));

        m.metadataInteger("Format").setString("0");
        v.setInt64(1);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("1"));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toInt64(), Q_INT64_C(1));
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toInt64(), Q_INT64_C(1));
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v, false));
        QCOMPARE(v.toInt64(), Q_INT64_C(1));
        s = "1.0";
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v, false));
        QCOMPARE(v.getType(), Type::Integer);
        QCOMPARE(v.toInt64(), Q_INT64_C(1));

        m.setEmpty();
        v.setInt64(INTEGER::undefined());
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("iNaN"));
        v.setInt64(1);
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.getType(), Type::Integer);
        QVERIFY(!INTEGER::defined(v.toInt64()));


        m.setEmpty();
        v.setBool(true);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("TRUE"));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toBool(), true);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toBool(), true);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v, false));
        s = "1";
        QVERIFY(!Parse::fromInputString(v, s, m, v, false));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toBool(), true);


        m.setEmpty();
        v.setBinary(QByteArray("12345"));
        s = Parse::toInputString(v, m);
        QVERIFY(!s.isEmpty());
        QVERIFY(s.startsWith('{'));
        QVERIFY(s.endsWith('}'));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toBinary(), QByteArray("12345"));
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toBinary(), QByteArray("12345"));
        v.setEmpty();
        m.metadataBytes("Stuff").setString("Things");
        s = s.mid(1);
        s.chop(1);
        QVERIFY(Parse::fromInputString(v, s, m, v, false));
        QCOMPARE(v.toBinary(), QByteArray("12345"));


        m.setEmpty();
        v.setFlags({"F1", "F2"});
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("|F1|F2"));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toFlags(), (Flags{"F1", "F2"}));
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toFlags(), (Flags{"F1", "F2"}));
        s = "F1|F2";
        v.setEmpty();
        QVERIFY(!Parse::fromInputString(v, s, m, v, false));
        QVERIFY(Parse::fromInputString(v, s, m, v));

        m.metadataFlags("Stuff").setString("Things");
        v.setFlags({"F1", "F2"});
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("|F1|F2"));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toFlags(), (Flags{"F1", "F2"}));
        v.setEmpty();
        s = "F1";
        QVERIFY(Parse::fromInputString(v, s, m, v, false));
        QCOMPARE(v.toFlags(), (Flags{"F1"}));
        m.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toFlags(), (Flags{"F1"}));


        m.setEmpty();
        v.setOverlay("/Some/Path");
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("~/Some/Path"));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.getType(), Type::Overlay);
        QCOMPARE(v.toString(), std::string("/Some/Path"));
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.getType(), Type::Overlay);
        QCOMPARE(v.toString(), std::string("/Some/Path"));
        s = "/Some/Path";
        v.setEmpty();
        QVERIFY(!Parse::fromInputString(v, s, m, v, false));
        v.setOverlay("/Another");
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.getType(), Type::Overlay);
        QCOMPARE(v.toString(), std::string("/Some/Path"));


        m.setEmpty();
        v.setString("value");
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("\"value\""));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toString(), std::string("value"));
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v, false));
        QCOMPARE(v.toString(), std::string("value"));

        m.metadataString("Stuff").setString("Things");
        v.setString("value");
        QCOMPARE(s, QString("\"value\""));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toString(), std::string("value"));
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v, false));
        QCOMPARE(v.toString(), std::string("value"));
        s = "value";
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QVERIFY(!Parse::fromInputString(v, s, m, v, false));
        QCOMPARE(v.toString(), std::string("value"));
        m.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QVERIFY(!Parse::fromInputString(v, s, m, v, false));
        QCOMPARE(v.toString(), std::string("value"));
        v.setEmpty();
        QVERIFY(!Parse::fromInputString(v, s, m, v, false));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.toString(), std::string("value"));


        v.setEmpty();
        v.setType(Type::Hash);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("=HASH"));
        QVERIFY(Parse::fromInputString(v, s));
        QCOMPARE(v.getType(), Type::Hash);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, false));
        QCOMPARE(v.getType(), Type::Hash);


        v.setEmpty();
        v.setType(Type::Array);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("=ARRAY"));
        QVERIFY(Parse::fromInputString(v, s));
        QCOMPARE(v.getType(), Type::Array);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, false));
        QCOMPARE(v.getType(), Type::Array);


        v.setEmpty();
        v.setType(Type::Matrix);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("=MATRIX"));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v.getType(), Type::Matrix);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, false));
        QCOMPARE(v.getType(), Type::Matrix);


        v.setType(Type::Keyframe);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("=KEYFRAME"));
        QVERIFY(Parse::fromInputString(v, s));
        QCOMPARE(v.getType(), Type::Keyframe);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, false));
        QCOMPARE(v.getType(), Type::Keyframe);


        v.setEmpty();
        v.setType(Type::MetadataReal);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("=METAREAL"));
        QVERIFY(Parse::fromInputString(v, s));
        QCOMPARE(v.getType(), Type::MetadataReal);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, false));
        QCOMPARE(v.getType(), Type::MetadataReal);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, "=METADOUBLE", false));
        QCOMPARE(v.getType(), Type::MetadataReal);


        v.setEmpty();
        v.setType(Type::MetadataInteger);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("=METAINTEGER"));
        QVERIFY(Parse::fromInputString(v, s));
        QCOMPARE(v.getType(), Type::MetadataInteger);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, false));
        QCOMPARE(v.getType(), Type::MetadataInteger);


        v.setEmpty();
        v.setType(Type::MetadataBoolean);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("=METABOOLEAN"));
        QVERIFY(Parse::fromInputString(v, s));
        QCOMPARE(v.getType(), Type::MetadataBoolean);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, false));
        QCOMPARE(v.getType(), Type::MetadataBoolean);


        v.setEmpty();
        v.setType(Type::MetadataBytes);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("=METABYTES"));
        QVERIFY(Parse::fromInputString(v, s));
        QCOMPARE(v.getType(), Type::MetadataBytes);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, false));
        QCOMPARE(v.getType(), Type::MetadataBytes);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, "=METABINARY", false));
        QCOMPARE(v.getType(), Type::MetadataBytes);


        v.setEmpty();
        v.setType(Type::MetadataArray);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("=METAARRAY"));
        QVERIFY(Parse::fromInputString(v, s));
        QCOMPARE(v.getType(), Type::MetadataArray);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, false));
        QCOMPARE(v.getType(), Type::MetadataArray);


        v.setEmpty();
        v.setType(Type::MetadataKeyframe);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("=METAKEYFRAME"));
        QVERIFY(Parse::fromInputString(v, s));
        QCOMPARE(v.getType(), Type::MetadataKeyframe);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, false));
        QCOMPARE(v.getType(), Type::MetadataKeyframe);


        v.setEmpty();
        v.setType(Type::MetadataHash);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("=METAHASH"));
        QVERIFY(Parse::fromInputString(v, s));
        QCOMPARE(v.getType(), Type::MetadataHash);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, false));
        QCOMPARE(v.getType(), Type::MetadataHash);


        v.setEmpty();
        v.setType(Type::MetadataFlags);
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("=METAFLAGS"));
        QVERIFY(Parse::fromInputString(v, s));
        QCOMPARE(v.getType(), Type::MetadataFlags);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, false));
        QCOMPARE(v.getType(), Type::MetadataFlags);


        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, "=REAL"));
        QCOMPARE(v.getType(), Type::Real);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, "=DOUBLE"));
        QCOMPARE(v.getType(), Type::Real);

        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, "=INTEGER"));
        QCOMPARE(v.getType(), Type::Integer);

        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, "=BOOLEAN"));
        QCOMPARE(v.getType(), Type::Boolean);

        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, "=STRING"));
        QCOMPARE(v.getType(), Type::String);

        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, "=BINARY"));
        QCOMPARE(v.getType(), Type::Bytes);

        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, "=FLAGS"));
        QCOMPARE(v.getType(), Type::Flags);

        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, "=EMPTY"));
        QCOMPARE(v.getType(), Type::Empty);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, "=INVALID"));
        QCOMPARE(v.getType(), Type::Empty);


        v.setEmpty();
        v.setString("Base\n");
        v.setDisplayString("en", "Va\"l1");
        v.setDisplayString("es", "Val2");
        Write lv = v;
        lv.detachFromRoot();
        m.setEmpty();
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("\"Base\\n\" en\"Va\\\"l1\" es\"Val2\""));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v, lv);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v, lv);
        m.metadataString("Stuff").setString("Things");
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v, false));
        QCOMPARE(v, lv);

        v.setEmpty();
        v.setString("Foo\\s");
        lv.set(v);
        m.setEmpty();
        s = Parse::toInputString(v, m);
        QCOMPARE(s, QString("\"Foo\\\\s\""));
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v, lv);
        v.setEmpty();
        QVERIFY(Parse::fromInputString(v, s, m, v));
        QCOMPARE(v, lv);
    }

    void metadataEvaluation()
    {
        Write d = Write::empty();
        Write m = Write::empty();

        d["Path1/Value1"] = "String1";
        d["Path1/Value2"] = 2.0;
        d["Path2/Value1"] = "String2";

        m["/*cPath1/*hOverlay/#0/Require/Value1"] = "String1";
        m["/*cPath1/*hOverlay/#0/Exclude/Value2"] = 3.0;
        m["/*cPath1/*hOverlay/#0/Data/*hOutput"] = "Matched1";
        m["/*cPath1/*hOverlay/#1/Require/Value1"] = "String2";
        m["/*cPath1/*hOverlay/#1/Data/*hOutput"] = "Matched2";
        m["/*cPath1/*hBase"] = "Base1";
        m["/*cPath2/*hTemplatePath"] = "/*cPath1";
        m["/*cPath2/*hAlternate"] = "Base2";

        Root r;

        r = Parse::evaluateMetadata(m.getPath("/Path1"), d.getPath("/Path1"));
        QCOMPARE(r["/*hBase"].toString(), std::string("Base1"));
        QCOMPARE(r["/*hOutput"].toString(), std::string("Matched1"));

        r = Parse::evaluateMetadata(m.getPath("/Path2"), d.getPath("/Path2"));
        QCOMPARE(r["/*hBase"].toString(), std::string("Base1"));
        QCOMPARE(r["/*hAlternate"].toString(), std::string("Base2"));
        QCOMPARE(r["/*hOutput"].toString(), std::string("Matched2"));


        m.setEmpty();
        m["/*cPath1/*hOverlay/#0/Require/Value1"] = "String1";
        m["/*cPath1/*hOverlay/#0/Data/*hOutput"] = "Correct1";
        m["/*cPath1/*hOverlay/#0/Data/*cPath2/*hOverlay/#0/Require/Value2"] = "String2";
        m["/*cPath1/*hOverlay/#0/Data/*cPath2/*hOverlay/#0/Data/*hOutput"] = "Correct2";

        d.setEmpty();
        d["Path1/Value1"] = "String1";
        d["Path1/Path2/Value2"] = "String3";

        r = Parse::evaluateMetadataTree(m.getPath("/Path1/Path2"), d.getPath("/Path1/Path2"));
        QCOMPARE(r["/*hOutput"].toString(), std::string());

        d.setEmpty();
        d["Path1/Value1"] = "String1";
        d["Path1/Path2/Value2"] = "String2";

        r = Parse::evaluateMetadataTree(m.getPath("/Path1/Path2"), d.getPath("/Path1/Path2"));
        QCOMPARE(r["/*hOutput"].toString(), std::string("Correct2"));

        m.setEmpty();
        m["/*cPath1/*hOverlay/#0/Require/Value2"] = "Enum1";
        m["/*cPath1/*hOverlay/#0/Data/*hOutput"] = "Correct1";
        m["/*cPath1/*hOverlay/#1/Require/Value2"] = "Enum2";
        m["/*cPath1/*hOverlay/#1/Data/*hOutput"] = "Correct2";
        m["/*cPath1/*cValue2/*sEnumerationValues/Enum1/Alias"].setFlags({"Enum1A"});

        d.setEmpty();
        d["Path1/Value1"] = "Main1";
        d["Path1/Value2"] = "Enum1";

        r = Parse::evaluateMetadata(m.getPath("/Path1"), d.getPath("/Path1"));
        QCOMPARE(r["/*hOutput"].toString(), std::string("Correct1"));

        d["Path1/Value2"] = "enum1";
        r = Parse::evaluateMetadata(m.getPath("/Path1"), d.getPath("/Path1"));
        QCOMPARE(r["/*hOutput"].toString(), std::string("Correct1"));

        d["Path1/Value2"] = "Enum1A";
        r = Parse::evaluateMetadata(m.getPath("/Path1"), d.getPath("/Path1"));
        QCOMPARE(r["/*hOutput"].toString(), std::string("Correct1"));

        d["Path1/Value2"] = "Enum2";
        r = Parse::evaluateMetadata(m.getPath("/Path1"), d.getPath("/Path1"));
        QCOMPARE(r["/*hOutput"].toString(), std::string("Correct2"));

        d["Path1/Value2"] = "Enum3";
        r = Parse::evaluateMetadata(m.getPath("/Path1"), d.getPath("/Path1"));
        QCOMPARE(r["/*hOutput"].toString(), std::string());
    }

    void parseHandler()
    {
        std::vector<Parse::InputNodeString> list;
        list.emplace_back(PathElement::parse("/v1/real"), "1.0");
        list.emplace_back(PathElement::parse("/v1/integer"), "0i2");

        auto result = Parse::InputNode::fromReferences(list.begin(), list.end());

        Variant::Root expected;
        expected["/v1/real"] = 1.0;
        expected["/v1/integer"] = 2;

        QCOMPARE(result.read(), expected.read());
    }
};

QTEST_APPLESS_MAIN(TestComposite)

#include "parse.moc"
