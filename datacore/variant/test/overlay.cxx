/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "datacore/variant/toplevel.hxx"
#include "datacore/variant/overlay.hxx"
#include "datacore/variant/hash.hxx"
#include "datacore/variant/path.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::Variant;

class TestOverlay : public QObject {
Q_OBJECT

private slots:

    void basicHash()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Hash, "A"}}, Node::WriteGoal::Real))->setReal(
                    1.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Hash, "B"},
                                                           {PathElement::Type::Hash, "C"}},
                                                          Node::WriteGoal::Real))->setReal(2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Hash, "A"}}, Node::WriteGoal::Real))->setReal(
                    -1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::Hash, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "B"},
                             {PathElement::Type::Hash, "C"}})->toReal(), 2.0);


        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Hash, "D"}}, Node::WriteGoal::Real))->setReal(
                    3.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Hash, "B"},
                                                           {PathElement::Type::Hash, "C"}},
                                                          Node::WriteGoal::Real))->setReal(4.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Hash, "D"}}, Node::WriteGoal::Real))->setReal(
                    -1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::Hash, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "B"},
                             {PathElement::Type::Hash, "C"}})->toReal(), 4.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Hash, "B"},
                                                           {PathElement::Type::Hash, "E"}},
                                                          Node::WriteGoal::Real))->setReal(5.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Hash, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "B"},
                             {PathElement::Type::Hash, "C"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "B"},
                             {PathElement::Type::Hash, "E"}})->toReal(), 5.0);
    }

    void basicArray()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Array, 0}}, Node::WriteGoal::Real))->setReal(
                    1.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Array, 1},
                                                           {PathElement::Type::Array, 0}},
                                                          Node::WriteGoal::Real))->setReal(2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Array, 0}}, Node::WriteGoal::Real))->setReal(
                    -1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::Array, 0}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::Array, 1},
                             {PathElement::Type::Array, 0}})->toReal(), 2.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Array, 0}}, Node::WriteGoal::Real))->setReal(
                    3.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Array, 0}}, Node::WriteGoal::Real))->setReal(
                    -1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::Array, 0}})->toReal(), 3.0);
        QVERIFY(!under.read({{PathElement::Type::Array, 1}}).get());

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Array, 0},
                                                           {PathElement::Type::Hash,  "A"}},
                                                          Node::WriteGoal::Real))->setReal(4.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Array, 1},
                                                           {PathElement::Type::Hash,  "B"},
                                                           {PathElement::Type::Array, 0},
                                                           {PathElement::Type::Hash,  "C"}},
                                                          Node::WriteGoal::Real))->setReal(5.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Array, 0},
                             {PathElement::Type::Hash,  "A"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::Array, 1},
                             {PathElement::Type::Hash,  "B"},
                             {PathElement::Type::Array, 0},
                             {PathElement::Type::Hash,  "C"}})->toReal(), 5.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Array, 0},
                                                           {PathElement::Type::Hash,  "A"}},
                                                          Node::WriteGoal::Real))->setReal(6.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Array, 0},
                             {PathElement::Type::Hash,  "A"}})->toReal(), 6.0);
        QCOMPARE(under.read({{PathElement::Type::Array, 1},
                             {PathElement::Type::Hash,  "B"},
                             {PathElement::Type::Array, 0},
                             {PathElement::Type::Hash,  "C"}})->toReal(), 5.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Array, 0},
                                                           {PathElement::Type::Hash,  "D"}},
                                                          Node::WriteGoal::Real))->setReal(7.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Array, 1},
                                                           {PathElement::Type::Hash,  "B"},
                                                           {PathElement::Type::Array, 0},
                                                           {PathElement::Type::Hash,  "C"}},
                                                          Node::WriteGoal::Real))->setReal(8.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Array, 0},
                             {PathElement::Type::Hash,  "A"}})->toReal(), 6.0);
        QCOMPARE(under.read({{PathElement::Type::Array, 0},
                             {PathElement::Type::Hash,  "D"}})->toReal(), 7.0);
        QCOMPARE(under.read({{PathElement::Type::Array, 1},
                             {PathElement::Type::Hash,  "B"},
                             {PathElement::Type::Array, 0},
                             {PathElement::Type::Hash,  "C"}})->toReal(), 8.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Array, 0},
                                                           {PathElement::Type::Hash,  "D"}},
                                                          Node::WriteGoal::Real))->setReal(7.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Array, 1},
                                                           {PathElement::Type::Hash,  "B"},
                                                           {PathElement::Type::Array, 0},
                                                           {PathElement::Type::Hash,  "X"}},
                                                          Node::WriteGoal::Real))->setReal(-2.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Array, 1},
                                                           {PathElement::Type::Hash,  "B"},
                                                           {PathElement::Type::Array, 1},
                                                           {PathElement::Type::Hash,  "E"}},
                                                          Node::WriteGoal::Real))->setReal(9.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Array, 0},
                             {PathElement::Type::Hash,  "A"}})->toReal(), 6.0);
        QCOMPARE(under.read({{PathElement::Type::Array, 0},
                             {PathElement::Type::Hash,  "D"}})->toReal(), 7.0);
        QCOMPARE(under.read({{PathElement::Type::Array, 1},
                             {PathElement::Type::Hash,  "B"},
                             {PathElement::Type::Array, 0},
                             {PathElement::Type::Hash,  "C"}})->toReal(), 8.0);
        QCOMPARE(under.read({{PathElement::Type::Array, 1},
                             {PathElement::Type::Hash,  "B"},
                             {PathElement::Type::Array, 0},
                             {PathElement::Type::Hash,  "X"}})->toReal(), -2.0);
        QCOMPARE(under.read({{PathElement::Type::Array, 1},
                             {PathElement::Type::Hash,  "B"},
                             {PathElement::Type::Array, 1},
                             {PathElement::Type::Hash,  "E"}})->toReal(), 9.0);
    }

    void basicMatrix()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Matrix, PathElement::MatrixIndex{0, 0}}},
                               Node::WriteGoal::Real))->setReal(1.0);
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::Matrix, PathElement::MatrixIndex{1, 1}},
                     {PathElement::Type::Matrix, PathElement::MatrixIndex{1}}},
                    Node::WriteGoal::Real))->setReal(2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Matrix, PathElement::MatrixIndex{0, 0}}},
                               Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(
                under.read({{PathElement::Type::Matrix, PathElement::MatrixIndex{0, 0}}})->toReal(),
                1.0);
        QCOMPARE(under.read({{PathElement::Type::Matrix, PathElement::MatrixIndex{1, 1}},
                             {PathElement::Type::Matrix, PathElement::MatrixIndex{1}}})->toReal(),
                 2.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Matrix, PathElement::MatrixIndex{0, 0}}},
                               Node::WriteGoal::Real))->setReal(3.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Matrix, PathElement::MatrixIndex{0, 0}}},
                               Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(
                under.read({{PathElement::Type::Matrix, PathElement::MatrixIndex{0, 0}}})->toReal(),
                3.0);
        QVERIFY(!under.read({{PathElement::Type::Matrix, PathElement::MatrixIndex{1, 1}}}).get());

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::Matrix, PathElement::MatrixIndex{0}},
                     {PathElement::Type::Hash,   "A"}}, Node::WriteGoal::Real))->setReal(4.0);
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::Matrix, PathElement::MatrixIndex{1}},
                     {PathElement::Type::Hash,   "B"},
                     {PathElement::Type::Matrix, PathElement::MatrixIndex{0}},
                     {PathElement::Type::Hash,   "C"}}, Node::WriteGoal::Real))->setReal(5.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Matrix, PathElement::MatrixIndex{0}},
                             {PathElement::Type::Hash,   "A"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::Matrix, PathElement::MatrixIndex{1}},
                             {PathElement::Type::Hash,   "B"},
                             {PathElement::Type::Matrix, PathElement::MatrixIndex{0}},
                             {PathElement::Type::Hash,   "C"}})->toReal(), 5.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::Matrix, PathElement::MatrixIndex{0}},
                     {PathElement::Type::Hash,   "D"}}, Node::WriteGoal::Real))->setReal(7.0);
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::Matrix, PathElement::MatrixIndex{1}},
                     {PathElement::Type::Hash,   "B"},
                     {PathElement::Type::Matrix, PathElement::MatrixIndex{0}},
                     {PathElement::Type::Hash,   "E"}}, Node::WriteGoal::Real))->setReal(8.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Matrix, PathElement::MatrixIndex{0}},
                             {PathElement::Type::Hash,   "A"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::Matrix, PathElement::MatrixIndex{0}},
                             {PathElement::Type::Hash,   "D"}})->toReal(), 7.0);
        QCOMPARE(under.read({{PathElement::Type::Matrix, PathElement::MatrixIndex{1}},
                             {PathElement::Type::Hash,   "B"},
                             {PathElement::Type::Matrix, PathElement::MatrixIndex{0}},
                             {PathElement::Type::Hash,   "C"}})->toReal(), 5.0);
        QCOMPARE(under.read({{PathElement::Type::Matrix, PathElement::MatrixIndex{1}},
                             {PathElement::Type::Hash,   "B"},
                             {PathElement::Type::Matrix, PathElement::MatrixIndex{0}},
                             {PathElement::Type::Hash,   "E"}})->toReal(), 8.0);
    }

    void basicKeyframe()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Keyframe, 1.0}},
                                                          Node::WriteGoal::Real))->setReal(1.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Keyframe, 2.0},
                                                           {PathElement::Type::Keyframe, 3.0}},
                                                          Node::WriteGoal::Real))->setReal(2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Keyframe, 1.0}},
                                                          Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::Keyframe, 1.0}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::Keyframe, 2.0},
                             {PathElement::Type::Keyframe, 3.0}})->toReal(), 2.0);


        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Keyframe, 4.0}},
                                                          Node::WriteGoal::Real))->setReal(3.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Keyframe, 2.0},
                                                           {PathElement::Type::Keyframe, 4.0}},
                                                          Node::WriteGoal::Real))->setReal(4.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Keyframe, 4.0}},
                                                          Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::Keyframe, 4.0}})->toReal(), 3.0);
        QVERIFY(!under.read({{PathElement::Type::Keyframe, 1.0}}));
        QCOMPARE(under.read({{PathElement::Type::Keyframe, 2.0},
                             {PathElement::Type::Keyframe, 4.0}})->toReal(), 4.0);
        QVERIFY(!under.read({{PathElement::Type::Keyframe, 2.0},
                             {PathElement::Type::Keyframe, 3.0}}));
    }

    void basicMetadataReal()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataReal, "A"}},
                                                          Node::WriteGoal::Real))->setReal(1.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataReal, "B"},
                                                           {PathElement::Type::MetadataReal, "C"}},
                                                          Node::WriteGoal::Real))->setReal(2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataReal, "A"}},
                                                          Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataReal, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataReal, "B"},
                             {PathElement::Type::MetadataReal, "C"}})->toReal(), 2.0);


        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataReal, "D"}},
                                                          Node::WriteGoal::Real))->setReal(3.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataReal, "B"},
                                                           {PathElement::Type::MetadataReal, "C"}},
                                                          Node::WriteGoal::Real))->setReal(4.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataReal, "D"}},
                                                          Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataReal, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataReal, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataReal, "B"},
                             {PathElement::Type::MetadataReal, "C"}})->toReal(), 4.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataReal, "B"},
                                                           {PathElement::Type::MetadataReal, "E"}},
                                                          Node::WriteGoal::Real))->setReal(5.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataReal, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataReal, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataReal, "B"},
                             {PathElement::Type::MetadataReal, "C"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataReal, "B"},
                             {PathElement::Type::MetadataReal, "E"}})->toReal(), 5.0);
    }

    void basicMetadataInteger()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataInteger, "A"}}, Node::WriteGoal::Real))
                    ->setReal(1.0);
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::MetadataInteger, "B"},
                     {PathElement::Type::MetadataInteger, "C"}}, Node::WriteGoal::Real))->setReal(
                    2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataInteger, "A"}}, Node::WriteGoal::Real))
                    ->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataInteger, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataInteger, "B"},
                             {PathElement::Type::MetadataInteger, "C"}})->toReal(), 2.0);


        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataInteger, "D"}}, Node::WriteGoal::Real))
                    ->setReal(3.0);
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::MetadataInteger, "B"},
                     {PathElement::Type::MetadataInteger, "C"}}, Node::WriteGoal::Real))->setReal(
                    4.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataInteger, "D"}}, Node::WriteGoal::Real))
                    ->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataInteger, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataInteger, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataInteger, "B"},
                             {PathElement::Type::MetadataInteger, "C"}})->toReal(), 4.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::MetadataInteger, "B"},
                     {PathElement::Type::MetadataInteger, "E"}}, Node::WriteGoal::Real))->setReal(
                    5.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataInteger, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataInteger, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataInteger, "B"},
                             {PathElement::Type::MetadataInteger, "C"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataInteger, "B"},
                             {PathElement::Type::MetadataInteger, "E"}})->toReal(), 5.0);
    }

    void basicMetadataBoolean()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataBoolean, "A"}}, Node::WriteGoal::Real))
                    ->setReal(1.0);
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::MetadataBoolean, "B"},
                     {PathElement::Type::MetadataBoolean, "C"}}, Node::WriteGoal::Real))->setReal(
                    2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataBoolean, "A"}}, Node::WriteGoal::Real))
                    ->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataBoolean, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataBoolean, "B"},
                             {PathElement::Type::MetadataBoolean, "C"}})->toReal(), 2.0);


        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataBoolean, "D"}}, Node::WriteGoal::Real))
                    ->setReal(3.0);
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::MetadataBoolean, "B"},
                     {PathElement::Type::MetadataBoolean, "C"}}, Node::WriteGoal::Real))->setReal(
                    4.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataBoolean, "D"}}, Node::WriteGoal::Real))
                    ->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataBoolean, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataBoolean, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataBoolean, "B"},
                             {PathElement::Type::MetadataBoolean, "C"}})->toReal(), 4.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::MetadataBoolean, "B"},
                     {PathElement::Type::MetadataBoolean, "E"}}, Node::WriteGoal::Real))->setReal(
                    5.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataBoolean, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataBoolean, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataBoolean, "B"},
                             {PathElement::Type::MetadataBoolean, "C"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataBoolean, "B"},
                             {PathElement::Type::MetadataBoolean, "E"}})->toReal(), 5.0);
    }

    void basicMetadataString()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataString, "A"}},
                               Node::WriteGoal::Real))->setReal(1.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataString, "B"},
                                                           {PathElement::Type::MetadataString, "C"}},
                                                          Node::WriteGoal::Real))->setReal(2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataString, "A"}},
                               Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataString, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataString, "B"},
                             {PathElement::Type::MetadataString, "C"}})->toReal(), 2.0);


        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataString, "D"}},
                               Node::WriteGoal::Real))->setReal(3.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataString, "B"},
                                                           {PathElement::Type::MetadataString, "C"}},
                                                          Node::WriteGoal::Real))->setReal(4.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataString, "D"}},
                               Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataString, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataString, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataString, "B"},
                             {PathElement::Type::MetadataString, "C"}})->toReal(), 4.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataString, "B"},
                                                           {PathElement::Type::MetadataString, "E"}},
                                                          Node::WriteGoal::Real))->setReal(5.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataString, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataString, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataString, "B"},
                             {PathElement::Type::MetadataString, "C"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataString, "B"},
                             {PathElement::Type::MetadataString, "E"}})->toReal(), 5.0);
    }

    void basicMetadataBytes()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataBytes, "A"}},
                                                          Node::WriteGoal::Real))->setReal(1.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataBytes, "B"},
                                                           {PathElement::Type::MetadataBytes, "C"}},
                                                          Node::WriteGoal::Real))->setReal(2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataBytes, "A"}},
                                                          Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataBytes, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataBytes, "B"},
                             {PathElement::Type::MetadataBytes, "C"}})->toReal(), 2.0);


        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataBytes, "D"}},
                                                          Node::WriteGoal::Real))->setReal(3.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataBytes, "B"},
                                                           {PathElement::Type::MetadataBytes, "C"}},
                                                          Node::WriteGoal::Real))->setReal(4.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataBytes, "D"}},
                                                          Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataBytes, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataBytes, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataBytes, "B"},
                             {PathElement::Type::MetadataBytes, "C"}})->toReal(), 4.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataBytes, "B"},
                                                           {PathElement::Type::MetadataBytes, "E"}},
                                                          Node::WriteGoal::Real))->setReal(5.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataBytes, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataBytes, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataBytes, "B"},
                             {PathElement::Type::MetadataBytes, "C"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataBytes, "B"},
                             {PathElement::Type::MetadataBytes, "E"}})->toReal(), 5.0);
    }

    void basicMetadataFlags()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataFlags, "A"}},
                                                          Node::WriteGoal::Real))->setReal(1.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataFlags, "B"},
                                                           {PathElement::Type::MetadataFlags, "C"}},
                                                          Node::WriteGoal::Real))->setReal(2.0);
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataSingleFlag, "U"}},
                               Node::WriteGoal::Real))->setReal(-1.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataFlags,      "B"},
                                                           {PathElement::Type::MetadataSingleFlag, "V"}},
                                                          Node::WriteGoal::Real))->setReal(-2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataFlags, "A"}},
                                                          Node::WriteGoal::Real))->setReal(0.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataFlags, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataSingleFlag, "U"}})->toReal(), -1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataFlags, "B"},
                             {PathElement::Type::MetadataFlags, "C"}})->toReal(), 2.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataFlags,      "B"},
                             {PathElement::Type::MetadataSingleFlag, "V"}})->toReal(), -2.0);


        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataSingleFlag, "X"}},
                               Node::WriteGoal::Real))->setReal(-3.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataFlags, "D"}},
                                                          Node::WriteGoal::Real))->setReal(3.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataFlags,      "B"},
                                                           {PathElement::Type::MetadataSingleFlag, "V"}},
                                                          Node::WriteGoal::Real))->setReal(-4.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataFlags, "B"},
                                                           {PathElement::Type::MetadataFlags, "C"}},
                                                          Node::WriteGoal::Real))->setReal(4.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataFlags, "D"}},
                                                          Node::WriteGoal::Real))->setReal(0.0);
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataSingleFlag, "X"}},
                               Node::WriteGoal::Real))->setReal(0.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataFlags, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataSingleFlag, "U"}})->toReal(), -1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataSingleFlag, "X"}})->toReal(), -3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataFlags, "B"},
                             {PathElement::Type::MetadataFlags, "C"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataFlags,      "B"},
                             {PathElement::Type::MetadataSingleFlag, "V"}})->toReal(), -4.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataFlags, "B"},
                                                           {PathElement::Type::MetadataFlags, "E"}},
                                                          Node::WriteGoal::Real))->setReal(5.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataFlags,      "B"},
                                                           {PathElement::Type::MetadataSingleFlag, "Y"}},
                                                          Node::WriteGoal::Real))->setReal(-5.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataFlags, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataSingleFlag, "U"}})->toReal(), -1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataSingleFlag, "X"}})->toReal(), -3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataFlags, "B"},
                             {PathElement::Type::MetadataFlags, "C"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataFlags, "B"},
                             {PathElement::Type::MetadataFlags, "E"}})->toReal(), 5.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataFlags,      "B"},
                             {PathElement::Type::MetadataSingleFlag, "V"}})->toReal(), -4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataFlags,      "B"},
                             {PathElement::Type::MetadataSingleFlag, "Y"}})->toReal(), -5.0);
    }

    void basicMetadataArray()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataArray, "A"}},
                                                          Node::WriteGoal::Real))->setReal(1.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataArray, "B"},
                                                           {PathElement::Type::MetadataArray, "C"}},
                                                          Node::WriteGoal::Real))->setReal(2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataArray, "A"}},
                                                          Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataArray, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataArray, "B"},
                             {PathElement::Type::MetadataArray, "C"}})->toReal(), 2.0);


        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataArray, "D"}},
                                                          Node::WriteGoal::Real))->setReal(3.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataArray, "B"},
                                                           {PathElement::Type::MetadataArray, "C"}},
                                                          Node::WriteGoal::Real))->setReal(4.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataArray, "D"}},
                                                          Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataArray, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataArray, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataArray, "B"},
                             {PathElement::Type::MetadataArray, "C"}})->toReal(), 4.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataArray, "B"},
                                                           {PathElement::Type::MetadataArray, "E"}},
                                                          Node::WriteGoal::Real))->setReal(5.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataArray, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataArray, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataArray, "B"},
                             {PathElement::Type::MetadataArray, "C"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataArray, "B"},
                             {PathElement::Type::MetadataArray, "E"}})->toReal(), 5.0);
    }

    void basicMetadataMatrix()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataMatrix, "A"}},
                               Node::WriteGoal::Real))->setReal(1.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataMatrix, "B"},
                                                           {PathElement::Type::MetadataMatrix, "C"}},
                                                          Node::WriteGoal::Real))->setReal(2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataMatrix, "A"}},
                               Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataMatrix, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataMatrix, "B"},
                             {PathElement::Type::MetadataMatrix, "C"}})->toReal(), 2.0);


        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataMatrix, "D"}},
                               Node::WriteGoal::Real))->setReal(3.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataMatrix, "B"},
                                                           {PathElement::Type::MetadataMatrix, "C"}},
                                                          Node::WriteGoal::Real))->setReal(4.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataMatrix, "D"}},
                               Node::WriteGoal::Real))->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataMatrix, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataMatrix, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataMatrix, "B"},
                             {PathElement::Type::MetadataMatrix, "C"}})->toReal(), 4.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataMatrix, "B"},
                                                           {PathElement::Type::MetadataMatrix, "E"}},
                                                          Node::WriteGoal::Real))->setReal(5.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataMatrix, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataMatrix, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataMatrix, "B"},
                             {PathElement::Type::MetadataMatrix, "C"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataMatrix, "B"},
                             {PathElement::Type::MetadataMatrix, "E"}})->toReal(), 5.0);
    }

    void basicMetadataHash()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataHash, "A"}},
                                                          Node::WriteGoal::Real))->setReal(1.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataHash, "B"},
                                                           {PathElement::Type::MetadataHash, "C"}},
                                                          Node::WriteGoal::Real))->setReal(2.0);
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataHashChild, "U"}},
                               Node::WriteGoal::Real))->setReal(-1.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataHash,      "B"},
                                                           {PathElement::Type::MetadataHashChild, "V"}},
                                                          Node::WriteGoal::Real))->setReal(-2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataHash, "A"}},
                                                          Node::WriteGoal::Real))->setReal(0.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataHash, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataHashChild, "U"}})->toReal(), -1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataHash, "B"},
                             {PathElement::Type::MetadataHash, "C"}})->toReal(), 2.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataHash,      "B"},
                             {PathElement::Type::MetadataHashChild, "V"}})->toReal(), -2.0);


        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataHashChild, "X"}},
                               Node::WriteGoal::Real))->setReal(-3.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataHash, "D"}},
                                                          Node::WriteGoal::Real))->setReal(3.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataHash,      "B"},
                                                           {PathElement::Type::MetadataHashChild, "V"}},
                                                          Node::WriteGoal::Real))->setReal(-4.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataHash, "B"},
                                                           {PathElement::Type::MetadataHash, "C"}},
                                                          Node::WriteGoal::Real))->setReal(4.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataHash, "D"}},
                                                          Node::WriteGoal::Real))->setReal(0.0);
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataHashChild, "X"}},
                               Node::WriteGoal::Real))->setReal(0.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataHash, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataHashChild, "U"}})->toReal(), -1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataHashChild, "X"}})->toReal(), -3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataHash, "B"},
                             {PathElement::Type::MetadataHash, "C"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataHash,      "B"},
                             {PathElement::Type::MetadataHashChild, "V"}})->toReal(), -4.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataHash, "B"},
                                                           {PathElement::Type::MetadataHash, "E"}},
                                                          Node::WriteGoal::Real))->setReal(5.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::MetadataHash,      "B"},
                                                           {PathElement::Type::MetadataHashChild, "Y"}},
                                                          Node::WriteGoal::Real))->setReal(-5.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataHash, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataHashChild, "U"}})->toReal(), -1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataHashChild, "X"}})->toReal(), -3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataHash, "B"},
                             {PathElement::Type::MetadataHash, "C"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataHash, "B"},
                             {PathElement::Type::MetadataHash, "E"}})->toReal(), 5.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataHash,      "B"},
                             {PathElement::Type::MetadataHashChild, "V"}})->toReal(), -4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataHash,      "B"},
                             {PathElement::Type::MetadataHashChild, "Y"}})->toReal(), -5.0);
    }

    void basicMetadataKeyframe()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataKeyframe, "A"}}, Node::WriteGoal::Real))
                    ->setReal(1.0);
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::MetadataKeyframe, "B"},
                     {PathElement::Type::MetadataKeyframe, "C"}}, Node::WriteGoal::Real))->setReal(
                    2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataKeyframe, "A"}}, Node::WriteGoal::Real))
                    ->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataKeyframe, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataKeyframe, "B"},
                             {PathElement::Type::MetadataKeyframe, "C"}})->toReal(), 2.0);


        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataKeyframe, "D"}}, Node::WriteGoal::Real))
                    ->setReal(3.0);
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::MetadataKeyframe, "B"},
                     {PathElement::Type::MetadataKeyframe, "C"}}, Node::WriteGoal::Real))->setReal(
                    4.0);

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::MetadataKeyframe, "D"}}, Node::WriteGoal::Real))
                    ->setReal(-1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataKeyframe, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataKeyframe, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataKeyframe, "B"},
                             {PathElement::Type::MetadataKeyframe, "C"}})->toReal(), 4.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write(
                    {{PathElement::Type::MetadataKeyframe, "B"},
                     {PathElement::Type::MetadataKeyframe, "E"}}, Node::WriteGoal::Real))->setReal(
                    5.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::MetadataKeyframe, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataKeyframe, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataKeyframe, "B"},
                             {PathElement::Type::MetadataKeyframe, "C"}})->toReal(), 4.0);
        QCOMPARE(under.read({{PathElement::Type::MetadataKeyframe, "B"},
                             {PathElement::Type::MetadataKeyframe, "E"}})->toReal(), 5.0);
    }

    void paths()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Hash, "A"}}, Node::WriteGoal::Real))->setReal(
                    1.0);
            std::static_pointer_cast<NodeOverlay>(over.write({{PathElement::Type::Hash, "B"},
                                                              {PathElement::Type::Hash, "C"}},
                                                             Node::WriteGoal::Overlay))->setOverlayPath(
                    "/A");

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Hash, "A"}}, Node::WriteGoal::Real))->setReal(
                    -1.0);
        }

        QCOMPARE(under.read({{PathElement::Type::Hash, "A"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "B"},
                             {PathElement::Type::Hash, "C"}})->toReal(), 1.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Hash, "A"}}, Node::WriteGoal::Real))->setReal(
                    2.0);
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Hash, "B"},
                                                           {PathElement::Type::Hash, "D"}},
                                                          Node::WriteGoal::Real))->setReal(3.0);
            std::static_pointer_cast<NodeOverlay>(over.write({{PathElement::Type::Hash, "E"}},
                                                             Node::WriteGoal::Overlay))->setOverlayPath(
                    "/B/D");
            std::static_pointer_cast<NodeOverlay>(over.write({{PathElement::Type::Hash, "F"}},
                                                             Node::WriteGoal::Overlay))->setOverlayPath(
                    "/B");

            Overlay::first(under, over);
            Overlay::second(under, over);

            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Hash, "A"}}, Node::WriteGoal::Real))->setReal(
                    -1.0);
        }


        QCOMPARE(under.read({{PathElement::Type::Hash, "A"}})->toReal(), 2.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "E"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "B"},
                             {PathElement::Type::Hash, "C"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "B"},
                             {PathElement::Type::Hash, "D"}})->toReal(), 3.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "F"},
                             {PathElement::Type::Hash, "C"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "F"},
                             {PathElement::Type::Hash, "D"}})->toReal(), 3.0);
    }

    void primitiveOverwrite()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(
                    over.write({{PathElement::Type::Hash, "A"}}, Node::WriteGoal::Real))->setReal(
                    1.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Hash, "A"}})->toReal(), 1.0);

        {
            TopLevel over;
            static_cast<NodeReal *>(over.write(Node::WriteGoal::Real))->setReal(2.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.node()->toReal(), 2.0);

        {
            TopLevel over;
            std::static_pointer_cast<NodeInteger>(over.write({{PathElement::Type::Hash, "A"}},
                                                             Node::WriteGoal::Integer))->setInteger(
                    1);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE((int) under.read({{PathElement::Type::Hash, "A"}})->toInteger(), 1);

        {
            TopLevel over;
            static_cast<NodeInteger *>(over.write(Node::WriteGoal::Integer))->setInteger(2);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE((int) under.node()->toInteger(), 2);

        {
            TopLevel over;
            std::static_pointer_cast<NodeBoolean>(over.write({{PathElement::Type::Hash, "A"}},
                                                             Node::WriteGoal::Boolean))->setBoolean(
                    true);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Hash, "A"}})->toBoolean(), true);

        {
            TopLevel over;
            static_cast<NodeBoolean *>(over.write(Node::WriteGoal::Boolean))->setBoolean(true);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.node()->toBoolean(), true);

        {
            TopLevel over;
            std::static_pointer_cast<NodeString>(over.write({{PathElement::Type::Hash, "A"}},
                                                            Node::WriteGoal::String))->setString(
                    "ABC");

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Hash, "A"}})->toString(), std::string("ABC"));

        {
            TopLevel over;
            static_cast<NodeString *>(over.write(Node::WriteGoal::String))->setString("CDE");

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.node()->toString(), std::string("CDE"));

        {
            TopLevel over;
            std::static_pointer_cast<NodeLocalizedString>(
                    over.write({{PathElement::Type::Hash, "A"}}, Node::WriteGoal::LocalizedString))
                    ->setDisplayString("es", "B");

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Hash, "A"}})->toDisplayString(QLocale("es")),
                 QString("B"));

        {
            TopLevel over;
            static_cast<NodeLocalizedString *>(over.write(
                    Node::WriteGoal::LocalizedString))->setDisplayString("en", "D");

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.node()->toDisplayString(QLocale("en")), QString("D"));
        QCOMPARE(under.node()->toDisplayString(QLocale("es")), QString());

        {
            TopLevel over;
            std::static_pointer_cast<NodeBytes>(
                    over.write({{PathElement::Type::Hash, "A"}}, Node::WriteGoal::Bytes))->setBytes(
                    QByteArray("ABC"));

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Hash, "A"}})->toBytes(), QByteArray("ABC"));

        {
            TopLevel over;
            static_cast<NodeBytes *>(over.write(Node::WriteGoal::Bytes))->setBytes("CDE");

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.node()->toBytes(), QByteArray("CDE"));

        {
            TopLevel over;
            std::static_pointer_cast<NodeFlags>(
                    over.write({{PathElement::Type::Hash, "A"}}, Node::WriteGoal::Flags))->setFlags(
                    {"Flag1"});

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Hash, "A"}})->toFlags(), Flags{"Flag1"});

        {
            TopLevel over;
            static_cast<NodeFlags *>(over.write(Node::WriteGoal::Flags))->setFlags({"Flag2"});

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.node()->toFlags(), Flags{"Flag2"});
    }

    void overlayOrderingFailure()
    {
        TopLevel under;

        {
            TopLevel over;
            std::static_pointer_cast<NodeReal>(over.write({{PathElement::Type::Hash, "Input"},
                                                           {PathElement::Type::Hash, "Value"}},
                                                          Node::WriteGoal::Real))->setReal(1.0);

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Hash, "Input"},
                             {PathElement::Type::Hash, "Value"}})->toReal(), 1.0);

        {
            TopLevel over;

            std::static_pointer_cast<NodeOverlay>(over.write({{PathElement::Type::Hash, "Output"},
                                                              {PathElement::Type::Hash, "Third"}},
                                                             Node::WriteGoal::Overlay))->setOverlayPath(
                    "/Output/First");

            std::static_pointer_cast<NodeOverlay>(over.write({{PathElement::Type::Hash, "Output"},
                                                              {PathElement::Type::Hash, "Second"}},
                                                             Node::WriteGoal::Overlay))->setOverlayPath(
                    "/Output/Third");

            std::static_pointer_cast<NodeOverlay>(over.write({{PathElement::Type::Hash, "Output"},
                                                              {PathElement::Type::Hash, "First"}},
                                                             Node::WriteGoal::Overlay))->setOverlayPath(
                    "/Input/Value");

            Overlay::first(under, over);
            Overlay::second(under, over);
        }

        QCOMPARE(under.read({{PathElement::Type::Hash, "Input"},
                             {PathElement::Type::Hash, "Value"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "Output"},
                             {PathElement::Type::Hash, "First"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "Output"},
                             {PathElement::Type::Hash, "Second"}})->toReal(), 1.0);
        QCOMPARE(under.read({{PathElement::Type::Hash, "Output"},
                             {PathElement::Type::Hash, "Third"}})->toReal(), 1.0);
    }
};

QTEST_APPLESS_MAIN(TestOverlay)

#include "overlay.moc"
