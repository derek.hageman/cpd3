/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QLocale>

#include "datacore/variant/handle.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::Variant;

class TestHandle : public QObject {
Q_OBJECT

    template<typename T>
    static const T &cref(const T &input)
    { return input; }

    static const std::string &cstring(const char *str)
    {
        static std::string s;
        s = str;
        return s;
    }

private slots:

    void initTestCase()
    {
        QLocale::setDefault(QLocale("ru_RU"));
    }

    void readPaths()
    {
        Read base;
        base.detachFromRoot();

        QVERIFY(base.currentPath().empty());
        QVERIFY(base.getRoot().currentPath().empty());

        Read next = base;
        QVERIFY(next.currentPath().empty());

        next = base.getPath("/A/B");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));
        QCOMPARE(next.getPath("&C").currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                                         {PathElement::Type::AnyString, "B"},
                                                         {PathElement::Type::Hash,      "C"}}));

        next = base.getPath(std::string("/A/D"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "D"}}));
        next = base.getPath(cstring("/A/B"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));

        next = base.getPath(QString("/A/B"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));

        next = base["/A/B"];
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));
        QCOMPARE(next["&C"].currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                                 {PathElement::Type::AnyString, "B"},
                                                 {PathElement::Type::Hash,      "C"}}));

        next = base[std::string("/A/B")];
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));
        next = base[cstring("/A/B")];
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));

        next = base[QString("/A/B")];
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));

        next = base.getPath(PathElement(PathElement::Type::Hash, "A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Hash, "A"}}));
        QCOMPARE(next.getPath(PathElement(PathElement::Type::Hash, "C")).currentPath(),
                 (Path{{PathElement::Type::Hash, "A"},
                       {PathElement::Type::Hash, "C"}}));

        {
            PathElement e(PathElement::Type::Hash, "A");
            next = base.getPath(e);
            QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Hash, "A"}}));
        }

        next = base.child("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"}}));
        next = base.child(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"}}));
        next = base.child(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"}}));
        next = base.child(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"}}));

        next = base.hash("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Hash, "A"}}));
        next = base.hash(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Hash, "A"}}));
        next = base.hash(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Hash, "A"}}));
        next = base.hash(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Hash, "A"}}));

        next = base.array(7);
        QCOMPARE(next.currentPath(), (Path{PathElement(PathElement::Type::Array, 7)}));

        next = base.matrix(PathElement::MatrixIndex{1, 2});
        QCOMPARE(next.currentPath(),
                 (Path{PathElement(PathElement::Type::Matrix, PathElement::MatrixIndex{1, 2})}));
        {
            PathElement::MatrixIndex mi{1, 2};
            next = base.matrix(mi);
            QCOMPARE(next.currentPath(), (Path{PathElement(PathElement::Type::Matrix,
                                                           PathElement::MatrixIndex{1, 2})}));
        }

        next = base.keyframe(2.0);
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Keyframe, 2.0}}));

        next = base.metadata("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyMetadata, "A"}}));
        next = base.metadata(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyMetadata, "A"}}));
        next = base.metadata(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyMetadata, "A"}}));
        next = base.metadata(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyMetadata, "A"}}));

        next = base.metadataReal("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataReal, "A"}}));
        next = base.metadataReal(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataReal, "A"}}));
        next = base.metadataReal(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataReal, "A"}}));
        next = base.metadataReal(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataReal, "A"}}));

        next = base.metadataInteger("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataInteger, "A"}}));
        next = base.metadataInteger(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataInteger, "A"}}));
        next = base.metadataInteger(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataInteger, "A"}}));
        next = base.metadataInteger(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataInteger, "A"}}));

        next = base.metadataBoolean("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBoolean, "A"}}));
        next = base.metadataBoolean(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBoolean, "A"}}));
        next = base.metadataBoolean(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBoolean, "A"}}));
        next = base.metadataBoolean(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBoolean, "A"}}));

        next = base.metadataString("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataString, "A"}}));
        next = base.metadataString(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataString, "A"}}));
        next = base.metadataString(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataString, "A"}}));
        next = base.metadataString(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataString, "A"}}));

        next = base.metadataBytes("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBytes, "A"}}));
        next = base.metadataBytes(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBytes, "A"}}));
        next = base.metadataBytes(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBytes, "A"}}));
        next = base.metadataBytes(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBytes, "A"}}));

        next = base.metadataFlags("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataFlags, "A"}}));
        next = base.metadataFlags(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataFlags, "A"}}));
        next = base.metadataFlags(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataFlags, "A"}}));
        next = base.metadataFlags(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataFlags, "A"}}));

        next = base.metadataArray("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataArray, "A"}}));
        next = base.metadataArray(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataArray, "A"}}));
        next = base.metadataArray(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataArray, "A"}}));
        next = base.metadataArray(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataArray, "A"}}));

        next = base.metadataMatrix("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataMatrix, "A"}}));
        next = base.metadataMatrix(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataMatrix, "A"}}));
        next = base.metadataMatrix(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataMatrix, "A"}}));
        next = base.metadataMatrix(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataMatrix, "A"}}));

        next = base.metadataHash("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHash, "A"}}));
        next = base.metadataHash(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHash, "A"}}));
        next = base.metadataHash(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHash, "A"}}));
        next = base.metadataHash(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHash, "A"}}));

        next = base.metadataKeyframe("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataKeyframe, "A"}}));
        next = base.metadataKeyframe(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataKeyframe, "A"}}));
        next = base.metadataKeyframe(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataKeyframe, "A"}}));
        next = base.metadataKeyframe(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataKeyframe, "A"}}));

        next = base.metadataHashChild("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHashChild, "A"}}));
        next = base.metadataHashChild(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHashChild, "A"}}));
        next = base.metadataHashChild(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHashChild, "A"}}));
        next = base.metadataHashChild(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHashChild, "A"}}));

        next = base.metadataSingleFlag("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataSingleFlag, "A"}}));
        next = base.metadataSingleFlag(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataSingleFlag, "A"}}));
        next = base.metadataSingleFlag(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataSingleFlag, "A"}}));
        next = base.metadataSingleFlag(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataSingleFlag, "A"}}));
    }

    void writePaths()
    {
        Write base;
        base.detachFromRoot();

        QVERIFY(base.currentPath().empty());
        QVERIFY(base.getRoot().currentPath().empty());

        Write next = base;
        QVERIFY(next.currentPath().empty());

        {
            Read check = base;
            QVERIFY(check.currentPath().empty());
        }

        next = base.getPath("/A/B");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));
        QCOMPARE(next.getPath("&C").currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                                         {PathElement::Type::AnyString, "B"},
                                                         {PathElement::Type::Hash,      "C"}}));

        next = base.getPath(std::string("/A/D"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "D"}}));
        next = base.getPath(cstring("/A/B"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));

        next = base.getPath(QString("/A/B"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));
        {
            Read check = next;
            QCOMPARE(check.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                                {PathElement::Type::AnyString, "B"}}));
        }

        next = base["/A/B"];
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));
        QCOMPARE(next["&C"].currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                                 {PathElement::Type::AnyString, "B"},
                                                 {PathElement::Type::Hash,      "C"}}));

        next = base[std::string("/A/B")];
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));
        next = base[cstring("/A/B")];
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));

        next = base[QString("/A/B")];
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"},
                                           {PathElement::Type::AnyString, "B"}}));

        next = base.getPath(PathElement(PathElement::Type::Hash, "A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Hash, "A"}}));
        QCOMPARE(next.getPath(PathElement(PathElement::Type::Hash, "C")).currentPath(),
                 (Path{{PathElement::Type::Hash, "A"},
                       {PathElement::Type::Hash, "C"}}));

        next = base.getPath(cref(PathElement(PathElement::Type::Hash, "A")));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Hash, "A"}}));

        next = base.child("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"}}));
        next = base.child(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"}}));
        next = base.child(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"}}));
        next = base.child(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyString, "A"}}));

        next = base.hash("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Hash, "A"}}));
        next = base.hash(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Hash, "A"}}));
        next = base.hash(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Hash, "A"}}));
        next = base.hash(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Hash, "A"}}));

        next = base.array(7);
        QCOMPARE(next.currentPath(), (Path{PathElement(PathElement::Type::Array, 7)}));

        next = base.matrix(PathElement::MatrixIndex{1, 2});
        QCOMPARE(next.currentPath(),
                 (Path{PathElement(PathElement::Type::Matrix, PathElement::MatrixIndex{1, 2})}));
        next = base.matrix(cref(PathElement::MatrixIndex{1, 2}));
        QCOMPARE(next.currentPath(),
                 (Path{PathElement(PathElement::Type::Matrix, PathElement::MatrixIndex{1, 2})}));

        next = base.keyframe(2.0);
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::Keyframe, 2.0}}));

        next = base.metadata("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyMetadata, "A"}}));
        next = base.metadata(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyMetadata, "A"}}));
        next = base.metadata(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyMetadata, "A"}}));
        next = base.metadata(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::AnyMetadata, "A"}}));

        next = base.metadataReal("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataReal, "A"}}));
        next = base.metadataReal(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataReal, "A"}}));
        next = base.metadataReal(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataReal, "A"}}));
        next = base.metadataReal(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataReal, "A"}}));

        next = base.metadataInteger("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataInteger, "A"}}));
        next = base.metadataInteger(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataInteger, "A"}}));
        next = base.metadataInteger(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataInteger, "A"}}));
        next = base.metadataInteger(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataInteger, "A"}}));

        next = base.metadataBoolean("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBoolean, "A"}}));
        next = base.metadataBoolean(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBoolean, "A"}}));
        next = base.metadataBoolean(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBoolean, "A"}}));
        next = base.metadataBoolean(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBoolean, "A"}}));

        next = base.metadataString("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataString, "A"}}));
        next = base.metadataString(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataString, "A"}}));
        next = base.metadataString(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataString, "A"}}));
        next = base.metadataString(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataString, "A"}}));

        next = base.metadataBytes("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBytes, "A"}}));
        next = base.metadataBytes(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBytes, "A"}}));
        next = base.metadataBytes(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBytes, "A"}}));
        next = base.metadataBytes(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataBytes, "A"}}));

        next = base.metadataFlags("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataFlags, "A"}}));
        next = base.metadataFlags(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataFlags, "A"}}));
        next = base.metadataFlags(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataFlags, "A"}}));
        next = base.metadataFlags(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataFlags, "A"}}));

        next = base.metadataArray("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataArray, "A"}}));
        next = base.metadataArray(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataArray, "A"}}));
        next = base.metadataArray(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataArray, "A"}}));
        next = base.metadataArray(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataArray, "A"}}));

        next = base.metadataMatrix("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataMatrix, "A"}}));
        next = base.metadataMatrix(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataMatrix, "A"}}));
        next = base.metadataMatrix(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataMatrix, "A"}}));
        next = base.metadataMatrix(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataMatrix, "A"}}));

        next = base.metadataHash("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHash, "A"}}));
        next = base.metadataHash(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHash, "A"}}));
        next = base.metadataHash(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHash, "A"}}));
        next = base.metadataHash(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHash, "A"}}));

        next = base.metadataKeyframe("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataKeyframe, "A"}}));
        next = base.metadataKeyframe(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataKeyframe, "A"}}));
        next = base.metadataKeyframe(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataKeyframe, "A"}}));
        next = base.metadataKeyframe(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataKeyframe, "A"}}));

        next = base.metadataHashChild("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHashChild, "A"}}));
        next = base.metadataHashChild(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHashChild, "A"}}));
        next = base.metadataHashChild(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHashChild, "A"}}));
        next = base.metadataHashChild(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataHashChild, "A"}}));

        next = base.metadataSingleFlag("A");
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataSingleFlag, "A"}}));
        next = base.metadataSingleFlag(std::string("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataSingleFlag, "A"}}));
        next = base.metadataSingleFlag(cstring("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataSingleFlag, "A"}}));
        next = base.metadataSingleFlag(QString("A"));
        QCOMPARE(next.currentPath(), (Path{{PathElement::Type::MetadataSingleFlag, "A"}}));
    }

    void genericAccess()
    {
        Write base;
        base.detachFromRoot();

        QCOMPARE(base.getType(), Type::Empty);
        QVERIFY(!base.exists());

        std::vector<Type> types
                {Type::Real, Type::Integer, Type::Boolean, Type::String, Type::Bytes, Type::Flags,
                 Type::Hash, Type::Array, Type::Matrix, Type::Keyframe, Type::MetadataReal,
                 Type::MetadataInteger, Type::MetadataBoolean, Type::MetadataString,
                 Type::MetadataBytes, Type::MetadataFlags, Type::MetadataHash, Type::MetadataArray,
                 Type::MetadataMatrix, Type::MetadataKeyframe, Type::Overlay};
        for (auto t : types) {
            base.setType(t);
            QCOMPARE(base.getType(), t);
            QVERIFY(base.exists());
            base.clear();
            base.remove();

            base["A"].setType(t);
            QCOMPARE(base["A"].getType(), t);
            QVERIFY(base["A"].exists());
            QCOMPARE(Read(base)["A"].getType(), t);
            base["A"].remove();
            QVERIFY(!base["A"].exists());
        }

        base.setType(Type::Real);
        base["A"].setType(Type::Empty);
        QCOMPARE(base["A"].getType(), Type::Empty);
        QVERIFY(!base["A"].exists());
        QCOMPARE(Read(base)["A"].getType(), Type::Empty);

        base["A/B/C"].remove(false);
        QCOMPARE(base["A/B"].getType(), Type::Empty);
        QVERIFY(!base["A/B"].exists());
        QVERIFY(!base["A/B/C"].exists());
        base["A/B/C"].remove(true);
        QCOMPARE(base["A/B"].getType(), Type::Hash);
        QVERIFY(base["A/B"].exists());
        QVERIFY(!base["A/B/C"].exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(1.0);
            base.set(other);
            QCOMPARE(base.getType(), Type::Real);
            QCOMPARE(base.toDouble(), 1.0);
            other.setDouble(-1.0);
        }
        QCOMPARE(base.getType(), Type::Real);
        QCOMPARE(base.toDouble(), 1.0);

        {
            Write other;
            other.detachFromRoot();
            other["A"].setDouble(2.0);
            base.set(other["A"]);
            QCOMPARE(base.getType(), Type::Real);
            QCOMPARE(base.toDouble(), 2.0);
            other["A"].setDouble(-1.0);
        }
        QCOMPARE(base.getType(), Type::Real);
        QCOMPARE(base.toDouble(), 2.0);

        {
            Write other;
            other.detachFromRoot();
            other["A"].setDouble(2.0);
            base.set(other["Q"]);
        }
        QCOMPARE(base.getType(), Type::Empty);

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base["A"].set(other);
        }
        QCOMPARE(base.getType(), Type::Hash);
        QCOMPARE(base["A"].getType(), Type::Real);
        QCOMPARE(base["A"].toDouble(), 3.0);

        {
            Write other;
            other.detachFromRoot();
            other["C"].setDouble(4.0);
            Read copy = other;
            copy.detachFromRoot();
            base["A/B"].set(copy["C"]);
            other["C"].setDouble(-1.0);
        }
        QCOMPARE(base.getType(), Type::Hash);
        QCOMPARE(base["A"].getType(), Type::Hash);
        QCOMPARE(base["A/B"].getType(), Type::Real);
        QCOMPARE(base["A/B"].toDouble(), 4.0);
    }

    void primitiveAccess()
    {
        Write base;
        base.detachFromRoot();

        QCOMPARE(base.getType(), Type::Empty);
        QVERIFY(!base.exists());
        QVERIFY(!base.isMetadata());

        base.setDouble(1.0);
        QCOMPARE(base.getType(), Type::Real);
        QVERIFY(base.exists());
        QVERIFY(!base.isMetadata());
        QCOMPARE(base.toDouble(), 1.0);
        base.setReal(2.0);
        QCOMPARE(base.toReal(), 2.0);
        base = 3.0;
        QCOMPARE(base.toDouble(), 3.0);
        base["A"].setDouble(1.0);
        QCOMPARE(base.getType(), Type::Hash);
        QCOMPARE(base["A"].getType(), Type::Real);
        QVERIFY(base["A"].exists());
        QVERIFY(!base["A"].isMetadata());
        QCOMPARE(Read(base["A"]).getType(), Type::Real);
        QCOMPARE(base["A"].toDouble(), 1.0);
        QCOMPARE(base["A"].toDisplayString(), QString("1"));
        base.setReal(10000.0);
        QCOMPARE(base.toDisplayString(), QString("10000"));

        base.setInteger(1);
        QCOMPARE(base.getType(), Type::Integer);
        QVERIFY(base.exists());
        QVERIFY(!base.isMetadata());
        QCOMPARE((int) base.toInteger(), 1);
        base.setInt64(2);
        QCOMPARE((int) base.toInteger(), 2);
        base = 3;
        QCOMPARE((int) base.toInteger(), 3);
        base = static_cast<std::int_fast64_t>(4);
        QCOMPARE((int) base.toInteger(), 4);
        base = static_cast<qint64>(5);
        QCOMPARE((int) base.toInteger(), 5);
        base["A"].setInteger(1);
        QCOMPARE(base.getType(), Type::Hash);
        QVERIFY(base["A"].exists());
        QVERIFY(!base["A"].isMetadata());
        QCOMPARE(base["A"].getType(), Type::Integer);
        QCOMPARE((int) base["A"].toInteger(), 1);
        QCOMPARE(base["A"].toDisplayString(), QString("1"));
        base.setInteger(10000);
        QCOMPARE(base.toDisplayString(), QString("10000"));

        base.setBoolean(true);
        QCOMPARE(base.getType(), Type::Boolean);
        QVERIFY(base.exists());
        QVERIFY(!base.isMetadata());
        QVERIFY(base.toBoolean());
        base.setBool(false);
        QVERIFY(!base.toBoolean());
        base = true;
        QVERIFY(base.toBoolean());
        base["A"].setBoolean(true);
        QCOMPARE(base.getType(), Type::Hash);
        QVERIFY(base["A"].exists());
        QVERIFY(!base["A"].isMetadata());
        QCOMPARE(base["A"].getType(), Type::Boolean);
        QVERIFY(base["A"].toBoolean());

        base.setBytes(Bytes("123"));
        QCOMPARE(base.getType(), Type::Bytes);
        QVERIFY(base.exists());
        QVERIFY(!base.isMetadata());
        QCOMPARE(base.toBytes(), Bytes("123"));
        base.setBytes(cref(Bytes("456")));
        QCOMPARE(base.toBytes(), Bytes("456"));
        base.setBinary(Bytes("abcd"));
        QCOMPARE(base.toBytes(), Bytes("abcd"));
        base.setBytes(cref(Bytes("efg")));
        QCOMPARE(base.toBytes(), Bytes("efg"));
        base["A"].setBytes(Bytes("z"));
        base["B"].setBytes(cref(Bytes("zzz")));
        QCOMPARE(base.getType(), Type::Hash);
        QVERIFY(base["A"].exists());
        QVERIFY(!base["A"].isMetadata());
        QCOMPARE(base["A"].getType(), Type::Bytes);
        QCOMPARE(base["A"].toBytes(), Bytes("z"));
        QCOMPARE(base["B"].getType(), Type::Bytes);
        QCOMPARE(base["B"].toBytes(), Bytes("zzz"));

        base.setFlags(Flags{});
        QCOMPARE(base.getType(), Type::Flags);
        QVERIFY(base.exists());
        QVERIFY(!base.isMetadata());
        QVERIFY(base.toFlags().empty());
        base.setFlags({"Flag1"});
        QCOMPARE(cref(base).toFlags(), (Flags{"Flag1"}));
        base.setFlags(cref(Flags{"Flag1", "Flag2"}));
        QCOMPARE(cref(base).toFlags(), (Flags{"Flag1", "Flag2"}));
        QVERIFY(base.testFlag("Flag1"));
        QVERIFY(!base.testFlag("Flag3"));
        QVERIFY(base.testFlags((Flags{"Flag1", "Flag2"})));
        QVERIFY(!base.testFlags((Flags{"Flag1", "Flag2", "Flag3"})));
        QVERIFY(base.testAnyFlags((Flags{"Flag1", "Flag3"})));
        QVERIFY(!base.testAnyFlags((Flags{"Flag3"})));
        {
            Write other;
            other.detachFromRoot();
            other.setFlags((Flags{"Flag1"}));
            QCOMPARE(cref(other).toFlags(), (Flags{"Flag1"}));
            Container::WriteFlags f = other.writeFlags();
            base.setFlags(f);
            QCOMPARE((int) f.size(), 1);
            QVERIFY(f.count("Flag1") == 1);
            QVERIFY(f == (Flags{"Flag1"}));
        }
        QCOMPARE(cref(base).toFlags(), (Flags{"Flag1"}));
        base.addFlags({"Flag2"});
        base.addFlags(cref(Flags{"Flag3"}));
        base.applyFlag("Flag4");
        base.applyFlag("Flag3");
        base.applyFlag(cref(Flag("Flag5")));
        QCOMPARE(cref(base).toFlags(), (Flags{"Flag1", "Flag2", "Flag3", "Flag4", "Flag5"}));
        base.removeFlags({"Flag5"});
        base.removeFlags(cref(Flags{"Flag4"}));
        base.clearFlag("Flag3");
        base.clearFlag("Flag3");
        base.clearFlag(cref(Flag("Flag2")));
        QCOMPARE(cref(base).toFlags(), (Flags{"Flag1"}));
        base["1"].setFlags({"Flag1"});
        base["2"].setFlags(cref(Flags{"Flag1", "Flag2"}));
        QCOMPARE(base.getType(), Type::Hash);
        QVERIFY(base["1"].exists());
        QVERIFY(!base["1"].isMetadata());
        QCOMPARE(base["1"].getType(), Type::Flags);
        QCOMPARE(cref(base["1"]).toFlags(), (Flags{"Flag1"}));
        QCOMPARE(base["2"].getType(), Type::Flags);
        QCOMPARE(cref(base["2"]).toFlags(), (Flags{"Flag1", "Flag2"}));
        QVERIFY(base["2"].testFlag("Flag1"));
        QVERIFY(!base.testFlag("Flag1"));
        QVERIFY(base["2"].testFlags((Flags{"Flag1", "Flag2"})));
        QVERIFY(!base.testFlags((Flags{"Flag1", "Flag2"})));
        QVERIFY(base["2"].testAnyFlags((Flags{"Flag1", "Flag3"})));
        QVERIFY(!base.testAnyFlags((Flags{"Flag1", "Flag3"})));
        {
            Write other;
            other.detachFromRoot();
            other.setFlags((Flags{"Flag1"}));
            QCOMPARE(cref(other).toFlags(), (Flags{"Flag1"}));
            Container::WriteFlags f = other.writeFlags();
            base["3"].setFlags(f);
        }
        QCOMPARE(cref(base["3"]).toFlags(), (Flags{"Flag1"}));
        base["3"].addFlags({"Flag2"});
        base["3"].addFlags(cref(Flags{"Flag3"}));
        base["3"].applyFlag("Flag4");
        base["3"].applyFlag("Flag3");
        base["3"].applyFlag(cref(Flag("Flag5")));
        QCOMPARE(cref(base["3"]).toFlags(), (Flags{"Flag1", "Flag2", "Flag3", "Flag4", "Flag5"}));
        base["3"].removeFlags({"Flag5"});
        base["3"].removeFlags(cref(Flags{"Flag4"}));
        base["3"].clearFlag("Flag3");
        base["3"].clearFlag("Flag3");
        base["3"].clearFlag(cref(Flag("Flag2")));
        QCOMPARE(cref(base["3"]).toFlags(), (Flags{"Flag1"}));
        QCOMPARE(cref(base["1"]).toFlags(), (Flags{"Flag1"}));
        QCOMPARE(cref(base["2"]).toFlags(), (Flags{"Flag1", "Flag2"}));
    }

    void stringAccess()
    {
        Write base;
        base.detachFromRoot();

        QCOMPARE(base.getType(), Type::Empty);
        QVERIFY(!base.exists());

        base.setString("abc");
        QCOMPARE(base.getType(), Type::String);
        QVERIFY(base.exists());
        QVERIFY(!base.isMetadata());
        QCOMPARE(base.toString(), std::string("abc"));
        QCOMPARE(base.toQString(), QString("abc"));
        QCOMPARE(base.toDisplayString(), QString("abc"));
        base.setString(std::string("def"));
        QCOMPARE(base.getType(), Type::String);
        QCOMPARE(base.toQString(), QString("def"));
        QCOMPARE(base.toQString(), QString("def"));
        QCOMPARE(base.toDisplayString(), QString("def"));
        base.setString(cstring("foo"));
        QCOMPARE(base.getType(), Type::String);
        QCOMPARE(base.toString(), std::string("foo"));
        base.setString(QString("a"));
        QCOMPARE(base.getType(), Type::String);
        QCOMPARE(base.toString(), std::string("a"));
        base = "1234";
        QCOMPARE(base.toString(), std::string("1234"));
        base = std::string("foobar");
        QCOMPARE(base.toString(), std::string("foobar"));
        base = cstring("mnb");
        QCOMPARE(base.toString(), std::string("mnb"));
        base = QString("QWERTY");
        QCOMPARE(base.toString(), std::string("QWERTY"));
        base.setDisplayString("en", "LValue1");
        QCOMPARE(base.toString(), std::string("QWERTY"));
        QCOMPARE(base.toDisplayString("en"), QString("LValue1"));
        QCOMPARE(base.toDisplayString(), QString("QWERTY"));

        base["A"].setString("abc");
        base["B"].setString(cstring("def"));
        QCOMPARE(base.getType(), Type::Hash);
        QVERIFY(base["A"].exists());
        QVERIFY(!base["A"].isMetadata());
        QCOMPARE(base["A"].getType(), Type::String);
        QCOMPARE(base["A"].toString(), std::string("abc"));
        QCOMPARE(base["B"].toString(), std::string("def"));
        base["A"].setDisplayString("es", "LValue2");
        QCOMPARE(base["A"].toString(), std::string("abc"));
        QCOMPARE(base["A"].toDisplayString("es"), QString("LValue2"));
        base["C"].setDisplayString("ko", "LValue3");
        QCOMPARE(base["C"].getType(), Type::String);
        QCOMPARE(base["C"].toDisplayString("ko"), QString("LValue3"));

        base.setDisplayString("en", "LValue1");
        base.setString("XXXX");
        base.setDisplayString(QString(), "Value");
        QCOMPARE(base.toDisplayString("en"), QString("LValue1"));
        QCOMPARE(base.toDisplayString(), QString("Value"));
    }

    void overlayAccess()
    {
        Write base;
        base.detachFromRoot();

        QCOMPARE(base.getType(), Type::Empty);
        QVERIFY(!base.exists());

        base.setOverlay("/ABC");
        QCOMPARE(base.getType(), Type::Overlay);
        QVERIFY(base.exists());
        QVERIFY(!base.isMetadata());
        QCOMPARE(base.toString(), std::string("/ABC"));
        base.setOverlay(std::string("/abc"));
        QCOMPARE(base.getType(), Type::Overlay);
        QCOMPARE(base.toString(), std::string("/abc"));
        base.setOverlay(cstring("/qwer"));
        QCOMPARE(base.getType(), Type::Overlay);
        QCOMPARE(base.toString(), std::string("/qwer"));
        base.setOverlay(QString("/foo"));
        QCOMPARE(base.getType(), Type::Overlay);
        QCOMPARE(base.toString(), std::string("/foo"));

        base["A"].setOverlay("Abc");
        base["B"].setOverlay(cstring("1234"));
        QCOMPARE(base.getType(), Type::Hash);
        QVERIFY(base["A"].exists());
        QVERIFY(!base["A"].isMetadata());
        QCOMPARE(base["A"].getType(), Type::Overlay);
        QCOMPARE(base["A"].toString(), std::string("Abc"));
        QCOMPARE(base["B"].getType(), Type::Overlay);
        QCOMPARE(base["B"].toString(), std::string("1234"));
    }

    void hashAccess()
    {
        Write base;
        base.detachFromRoot();

        base.hash("A").setDouble(1.0);
        QCOMPARE(base.hash("A").toDouble(), 1.0);
        QCOMPARE(Read(base).hash("A").toDouble(), 1.0);

        base.hash("B").hash("C").setDouble(2.0);
        QCOMPARE(base.hash("B").hash("C").toDouble(), 2.0);
        QCOMPARE(Read(base).hash("B").hash("C").toDouble(), 2.0);
        QCOMPARE(base.child("B").child("C").toDouble(), 2.0);

        QVERIFY(base.hash("B").hash("C").exists());
        QVERIFY(!base.hash("B").isMetadata());
        base.child("B").clear();
        QVERIFY(!base.hash("B").hash("C").exists());
        base.hash("B").hash("C").setDouble(2.0);
        QVERIFY(base.hash("B").hash("C").exists());
        base.hash("B").hash("C").remove();
        QVERIFY(!base.hash("B").hash("C").exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.hash("B").hash("C").set(other);
        }
        base.hash("B").hash("C").setDouble(3.0);

        base.hash("B").remove();
        QVERIFY(!base.hash("B").exists());
        QVERIFY(base.hash("A").exists());
        base.clear();
        QVERIFY(!base.hash("A").exists());
        QCOMPARE(base.getType(), Type::Hash);
        QVERIFY(base.exists());
        QVERIFY(!base.isMetadata());

        base.hash("A").hash("B").hash("C").setDouble(1.0);
        base.detachFromRoot();
        base.hash("A").hash("B").setType(Type::Hash);
        QCOMPARE(base.hash("A").hash("B").hash("C").toDouble(), 1.0);
        base["A/B"].setType(Type::Hash);
        QCOMPARE(base.hash("A").hash("B").hash("C").toDouble(), 1.0);
    }

    void arrayAccess()
    {
        Write base;
        base.detachFromRoot();

        base.array(0).setDouble(1.0);
        QCOMPARE(base.array(0).toDouble(), 1.0);
        QCOMPARE(Read(base).array(0).toDouble(), 1.0);

        base.array(1).array(2).setDouble(2.0);
        QCOMPARE(base.array(1).array(2).toDouble(), 2.0);
        QCOMPARE(Read(base).array(1).array(2).toDouble(), 2.0);
        QCOMPARE(base.array(1).array(2).toDouble(), 2.0);

        QVERIFY(base.array(1).array(2).exists());
        QVERIFY(!base.array(1).isMetadata());
        base.array(1).clear();
        QVERIFY(!base.array(1).array(2).exists());
        base.array(1).array(2).setDouble(2.0);
        QVERIFY(base.array(1).array(2).exists());
        base.array(1).array(2).remove();
        QVERIFY(!base.array(1).array(2).exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.array(1).array(2).set(other);
        }
        base.array(1).array(2).setDouble(3.0);

        base.array(1).remove();
        QVERIFY(!base.array(1).exists());
        QVERIFY(base.array(0).exists());
        base.clear();
        QVERIFY(!base.array(0).exists());
        QCOMPARE(base.getType(), Type::Array);
        QVERIFY(base.exists());
        QVERIFY(!base.isMetadata());

        base.array(0).array(1).array(2).setDouble(1.0);
        base.detachFromRoot();
        base.array(0).array(1).setType(Type::Array);
        QCOMPARE(base.array(0).array(1).array(2).toDouble(), 1.0);
    }

    void matrixAccess()
    {
        Write base;
        base.detachFromRoot();

        base.matrix({0, 0}).setDouble(1.0);
        QCOMPARE(base.matrix({0, 0}).toDouble(), 1.0);
        QCOMPARE(Read(base).matrix({ 0, 0 }).toDouble(), 1.0);

        base.matrix({1, 1}).matrix({1, 0}).setDouble(2.0);
        QCOMPARE(base.matrix({0, 0}).toDouble(), 1.0);
        QCOMPARE(base.matrix({1, 1}).matrix({1, 0}).toDouble(), 2.0);
        QCOMPARE(Read(base).matrix({ 1, 1 }).matrix({ 1, 0 }).toDouble(), 2.0);
        QCOMPARE(base.matrix({1, 1}).matrix({1, 0}).toDouble(), 2.0);

        QVERIFY(base.matrix({1, 1}).matrix({1, 0}).exists());
        QVERIFY(!base.matrix({1, 1}).isMetadata());
        base.matrix({1, 1}).clear();
        QVERIFY(!base.matrix({1, 1}).matrix({1, 0}).exists());
        base.matrix({1, 1}).matrix({1, 0}).setDouble(2.0);
        QVERIFY(base.matrix({1, 1}).matrix({1, 0}).exists());
        base.matrix({1, 1}).matrix({1, 0}).remove();
        QVERIFY(!base.matrix({1, 1}).matrix({1, 0}).exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.matrix({1, 1}).matrix({1, 0}).set(other);
        }
        base.matrix({1, 1}).matrix({1, 0}).setDouble(3.0);

        base.matrix({1, 1}).remove();
        QVERIFY(!base.matrix({1, 1}).exists());
        QVERIFY(base.matrix({0, 0}).exists());
        base.clear();
        QVERIFY(!base.matrix({0, 0}).exists());
        QCOMPARE(base.getType(), Type::Matrix);
        QVERIFY(base.exists());
        QVERIFY(!base.isMetadata());

        base.matrix({0, 0}).matrix({1, 1}).matrix({2, 2}).setDouble(1.0);
        base.detachFromRoot();
        base.matrix({0, 0}).matrix({1, 1}).setType(Type::Matrix);
        QCOMPARE(base.matrix({0, 0}).matrix({1, 1}).matrix({2, 2}).toDouble(), 1.0);
    }

    void keyframeAccess()
    {
        Write base;
        base.detachFromRoot();

        base.keyframe(-1.0).setDouble(1.0);
        QCOMPARE(base.keyframe(-1.0).toDouble(), 1.0);
        QCOMPARE(Read(base).keyframe(-1.0).toDouble(), 1.0);

        base.keyframe(-2.0).keyframe(-3.0).setDouble(2.0);
        QCOMPARE(base.keyframe(-2.0).keyframe(-3.0).toDouble(), 2.0);
        QCOMPARE(Read(base).keyframe(-2.0).keyframe(-3.0).toDouble(), 2.0);
        QCOMPARE(base.keyframe(-2.0).keyframe(-3.0).toDouble(), 2.0);

        QVERIFY(base.keyframe(-2.0).keyframe(-3.0).exists());
        QVERIFY(!base.keyframe(-2.0).isMetadata());
        base.keyframe(-2.0).clear();
        QVERIFY(!base.keyframe(-2.0).keyframe(-3.0).exists());
        base.keyframe(-2.0).keyframe(-3.0).setDouble(2.0);
        QVERIFY(base.keyframe(-2.0).keyframe(-3.0).exists());
        base.keyframe(-2.0).keyframe(-3.0).remove();
        QVERIFY(!base.keyframe(-2.0).keyframe(-3.0).exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.keyframe(-2.0).keyframe(-3.0).set(other);
        }
        base.keyframe(-2.0).keyframe(-3.0).setDouble(3.0);

        base.keyframe(-2.0).remove();
        QVERIFY(!base.keyframe(-2.0).exists());
        QVERIFY(base.keyframe(-1.0).exists());
        base.clear();
        QVERIFY(!base.keyframe(-1.0).exists());
        QCOMPARE(base.getType(), Type::Keyframe);
        QVERIFY(base.exists());
        QVERIFY(!base.isMetadata());

        base.keyframe(-1.0).keyframe(-2.0).keyframe(-3.0).setDouble(1.0);
        base.detachFromRoot();
        base.keyframe(-1.0).keyframe(-2.0).setType(Type::Keyframe);
        QCOMPARE(base.keyframe(-1.0).keyframe(-2.0).keyframe(-3.0).toDouble(), 1.0);
    }

    void metadataRealAccess()
    {
        Write base;
        base.detachFromRoot();

        base.metadataReal("A").setDouble(1.0);
        QCOMPARE(base.metadataReal("A").toDouble(), 1.0);
        QCOMPARE(Read(base).metadataReal("A").toDouble(), 1.0);

        base.metadataReal("B").metadataReal("C").setDouble(2.0);
        QCOMPARE(base.metadataReal("B").metadataReal("C").toDouble(), 2.0);
        QCOMPARE(Read(base).metadataReal("B").metadataReal("C").toDouble(), 2.0);
        QCOMPARE(base.metadata("B").metadata("C").toDouble(), 2.0);

        QVERIFY(base.metadataReal("B").metadataReal("C").exists());
        QVERIFY(base.metadataReal("B").isMetadata());
        base.metadata("B").clear();
        QVERIFY(!base.metadataReal("B").metadataReal("C").exists());
        base.metadataReal("B").metadataReal("C").setDouble(2.0);
        QVERIFY(base.metadataReal("B").metadataReal("C").exists());
        base.metadataReal("B").metadataReal("C").remove();
        QVERIFY(!base.metadataReal("B").metadataReal("C").exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.metadataReal("B").metadataReal("C").set(other);
        }
        base.metadataReal("B").metadataReal("C").setDouble(3.0);

        base.metadataReal("B").remove();
        QVERIFY(!base.metadataReal("B").exists());
        QVERIFY(base.metadataReal("A").exists());
        base.clear();
        QVERIFY(!base.metadataReal("A").exists());
        QCOMPARE(base.getType(), Type::MetadataReal);
        QVERIFY(base.exists());
        QVERIFY(base.isMetadata());

        base.metadataReal("A").metadataReal("B").metadataReal("C").setDouble(1.0);
        base.detachFromRoot();
        base.metadataReal("A").metadataReal("B").setType(Type::MetadataReal);
        QCOMPARE(base.metadataReal("A").metadataReal("B").metadataReal("C").toDouble(), 1.0);
        base["^A/^B"].setType(Type::MetadataReal);
        QCOMPARE(base.metadataReal("A").metadataReal("B").metadataReal("C").toDouble(), 1.0);
    }

    void metadataIntegerAccess()
    {
        Write base;
        base.detachFromRoot();

        base.metadataInteger("A").setDouble(1.0);
        QCOMPARE(base.metadataInteger("A").toDouble(), 1.0);
        QCOMPARE(Read(base).metadataInteger("A").toDouble(), 1.0);

        base.metadataInteger("B").metadataInteger("C").setDouble(2.0);
        QCOMPARE(base.metadataInteger("B").metadataInteger("C").toDouble(), 2.0);
        QCOMPARE(Read(base).metadataInteger("B").metadataInteger("C").toDouble(), 2.0);
        QCOMPARE(base.metadata("B").metadata("C").toDouble(), 2.0);

        QVERIFY(base.metadataInteger("B").metadataInteger("C").exists());
        QVERIFY(base.metadataInteger("B").isMetadata());
        base.metadata("B").clear();
        QVERIFY(!base.metadataInteger("B").metadataInteger("C").exists());
        base.metadataInteger("B").metadataInteger("C").setDouble(2.0);
        QVERIFY(base.metadataInteger("B").metadataInteger("C").exists());
        base.metadataInteger("B").metadataInteger("C").remove();
        QVERIFY(!base.metadataInteger("B").metadataInteger("C").exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.metadataInteger("B").metadataInteger("C").set(other);
        }
        base.metadataInteger("B").metadataInteger("C").setDouble(3.0);

        base.metadataInteger("B").remove();
        QVERIFY(!base.metadataInteger("B").exists());
        QVERIFY(base.metadataInteger("A").exists());
        base.clear();
        QVERIFY(!base.metadataInteger("A").exists());
        QCOMPARE(base.getType(), Type::MetadataInteger);
        QVERIFY(base.exists());
        QVERIFY(base.isMetadata());

        base.metadataInteger("A").metadataInteger("B").metadataInteger("C").setDouble(1.0);
        base.detachFromRoot();
        base.metadataInteger("A").metadataInteger("B").setType(Type::MetadataInteger);
        QCOMPARE(base.metadataInteger("A").metadataInteger("B").metadataInteger("C").toDouble(),
                 1.0);
        base["^A/^B"].setType(Type::MetadataInteger);
        QCOMPARE(base.metadataInteger("A").metadataInteger("B").metadataInteger("C").toDouble(),
                 1.0);
    }

    void metadataBooleanAccess()
    {
        Write base;
        base.detachFromRoot();

        base.metadataBoolean("A").setDouble(1.0);
        QCOMPARE(base.metadataBoolean("A").toDouble(), 1.0);
        QCOMPARE(Read(base).metadataBoolean("A").toDouble(), 1.0);

        base.metadataBoolean("B").metadataBoolean("C").setDouble(2.0);
        QCOMPARE(base.metadataBoolean("B").metadataBoolean("C").toDouble(), 2.0);
        QCOMPARE(Read(base).metadataBoolean("B").metadataBoolean("C").toDouble(), 2.0);
        QCOMPARE(base.metadata("B").metadata("C").toDouble(), 2.0);

        QVERIFY(base.metadataBoolean("B").metadataBoolean("C").exists());
        QVERIFY(base.metadataBoolean("B").isMetadata());
        base.metadata("B").clear();
        QVERIFY(!base.metadataBoolean("B").metadataBoolean("C").exists());
        base.metadataBoolean("B").metadataBoolean("C").setDouble(2.0);
        QVERIFY(base.metadataBoolean("B").metadataBoolean("C").exists());
        base.metadataBoolean("B").metadataBoolean("C").remove();
        QVERIFY(!base.metadataBoolean("B").metadataBoolean("C").exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.metadataBoolean("B").metadataBoolean("C").set(other);
        }
        base.metadataBoolean("B").metadataBoolean("C").setDouble(3.0);

        base.metadataBoolean("B").remove();
        QVERIFY(!base.metadataBoolean("B").exists());
        QVERIFY(base.metadataBoolean("A").exists());
        base.clear();
        QVERIFY(!base.metadataBoolean("A").exists());
        QCOMPARE(base.getType(), Type::MetadataBoolean);
        QVERIFY(base.exists());
        QVERIFY(base.isMetadata());

        base.metadataBoolean("A").metadataBoolean("B").metadataBoolean("C").setDouble(1.0);
        base.detachFromRoot();
        base.metadataBoolean("A").metadataBoolean("B").setType(Type::MetadataBoolean);
        QCOMPARE(base.metadataBoolean("A").metadataBoolean("B").metadataBoolean("C").toDouble(),
                 1.0);
        base["^A/^B"].setType(Type::MetadataBoolean);
        QCOMPARE(base.metadataBoolean("A").metadataBoolean("B").metadataBoolean("C").toDouble(),
                 1.0);
    }

    void metadataStringAccess()
    {
        Write base;
        base.detachFromRoot();

        base.metadataString("A").setDouble(1.0);
        QCOMPARE(base.metadataString("A").toDouble(), 1.0);
        QCOMPARE(Read(base).metadataString("A").toDouble(), 1.0);

        base.metadataString("B").metadataString("C").setDouble(2.0);
        QCOMPARE(base.metadataString("B").metadataString("C").toDouble(), 2.0);
        QCOMPARE(Read(base).metadataString("B").metadataString("C").toDouble(), 2.0);
        QCOMPARE(base.metadata("B").metadata("C").toDouble(), 2.0);

        QVERIFY(base.metadataString("B").metadataString("C").exists());
        QVERIFY(base.metadataString("B").isMetadata());
        base.metadata("B").clear();
        QVERIFY(!base.metadataString("B").metadataString("C").exists());
        base.metadataString("B").metadataString("C").setDouble(2.0);
        QVERIFY(base.metadataString("B").metadataString("C").exists());
        base.metadataString("B").metadataString("C").remove();
        QVERIFY(!base.metadataString("B").metadataString("C").exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.metadataString("B").metadataString("C").set(other);
        }
        base.metadataString("B").metadataString("C").setDouble(3.0);

        base.metadataString("B").remove();
        QVERIFY(!base.metadataString("B").exists());
        QVERIFY(base.metadataString("A").exists());
        base.clear();
        QVERIFY(!base.metadataString("A").exists());
        QCOMPARE(base.getType(), Type::MetadataString);
        QVERIFY(base.exists());
        QVERIFY(base.isMetadata());

        base.metadataString("A").metadataString("B").metadataString("C").setDouble(1.0);
        base.detachFromRoot();
        base.metadataString("A").metadataString("B").setType(Type::MetadataString);
        QCOMPARE(base.metadataString("A").metadataString("B").metadataString("C").toDouble(), 1.0);
        base["^A/^B"].setType(Type::MetadataString);
        QCOMPARE(base.metadataString("A").metadataString("B").metadataString("C").toDouble(), 1.0);
    }

    void metadataBytesAccess()
    {
        Write base;
        base.detachFromRoot();

        base.metadataBytes("A").setDouble(1.0);
        QCOMPARE(base.metadataBytes("A").toDouble(), 1.0);
        QCOMPARE(Read(base).metadataBytes("A").toDouble(), 1.0);

        base.metadataBytes("B").metadataBytes("C").setDouble(2.0);
        QCOMPARE(base.metadataBytes("B").metadataBytes("C").toDouble(), 2.0);
        QCOMPARE(Read(base).metadataBytes("B").metadataBytes("C").toDouble(), 2.0);
        QCOMPARE(base.metadata("B").metadata("C").toDouble(), 2.0);

        QVERIFY(base.metadataBytes("B").metadataBytes("C").exists());
        QVERIFY(base.metadataBytes("B").isMetadata());
        base.metadata("B").clear();
        QVERIFY(!base.metadataBytes("B").metadataBytes("C").exists());
        base.metadataBytes("B").metadataBytes("C").setDouble(2.0);
        QVERIFY(base.metadataBytes("B").metadataBytes("C").exists());
        base.metadataBytes("B").metadataBytes("C").remove();
        QVERIFY(!base.metadataBytes("B").metadataBytes("C").exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.metadataBytes("B").metadataBytes("C").set(other);
        }
        base.metadataBytes("B").metadataBytes("C").setDouble(3.0);

        base.metadataBytes("B").remove();
        QVERIFY(!base.metadataBytes("B").exists());
        QVERIFY(base.metadataBytes("A").exists());
        base.clear();
        QVERIFY(!base.metadataBytes("A").exists());
        QCOMPARE(base.getType(), Type::MetadataBytes);
        QVERIFY(base.exists());
        QVERIFY(base.isMetadata());

        base.metadataBytes("A").metadataBytes("B").metadataBytes("C").setDouble(1.0);
        base.detachFromRoot();
        base.metadataBytes("A").metadataBytes("B").setType(Type::MetadataBytes);
        QCOMPARE(base.metadataBytes("A").metadataBytes("B").metadataBytes("C").toDouble(), 1.0);
        base["^A/^B"].setType(Type::MetadataBytes);
        QCOMPARE(base.metadataBytes("A").metadataBytes("B").metadataBytes("C").toDouble(), 1.0);
    }

    void metadataFlagsAccess()
    {
        Write base;
        base.detachFromRoot();

        base.metadataFlags("A").setDouble(1.0);
        base.metadataSingleFlag("U").setDouble(-1.0);
        QCOMPARE(base.metadataFlags("A").toDouble(), 1.0);
        QCOMPARE(Read(base).metadataFlags("A").toDouble(), 1.0);
        QCOMPARE(base.metadataSingleFlag("U").toDouble(), -1.0);
        QCOMPARE(Read(base).metadataSingleFlag("U").toDouble(), -1.0);

        base.metadataFlags("B").metadataFlags("C").setDouble(2.0);
        base.metadataFlags("B").metadataSingleFlag("V").setDouble(-2.0);
        QCOMPARE(base.metadataFlags("B").metadataFlags("C").toDouble(), 2.0);
        QCOMPARE(Read(base).metadataFlags("B").metadataFlags("C").toDouble(), 2.0);
        QCOMPARE(base.metadata("B").metadata("C").toDouble(), 2.0);
        QCOMPARE(base.metadataFlags("B").metadataSingleFlag("V").toDouble(), -2.0);
        QCOMPARE(Read(base).metadataFlags("B").metadataSingleFlag("V").toDouble(), -2.0);
        QCOMPARE(base.metadata("B").child("V").toDouble(), -2.0);
        QCOMPARE(base.child("U").toDouble(), -1.0);

        QVERIFY(base.metadataFlags("B").metadataSingleFlag("V").exists());
        QVERIFY(base.metadataFlags("B").metadataFlags("C").exists());
        QVERIFY(base.metadataFlags("B").isMetadata());
        base.metadata("B").clear();
        QVERIFY(!base.metadataFlags("B").metadataFlags("C").exists());
        QVERIFY(!base.metadataFlags("B").metadataSingleFlag("V").exists());
        base.metadataFlags("B").metadataSingleFlag("V").setDouble(-2.0);
        QVERIFY(base.metadataFlags("B").metadataSingleFlag("V").exists());
        QVERIFY(!base.metadataFlags("B").metadataFlags("C").exists());
        base.metadataFlags("B").metadataFlags("C").setDouble(2.0);
        QVERIFY(base.metadataFlags("B").metadataFlags("C").exists());
        QVERIFY(base.metadataFlags("B").metadataSingleFlag("V").exists());
        base.metadataFlags("B").metadataFlags("C").remove();
        base.metadataFlags("B").metadataSingleFlag("V").remove();
        QVERIFY(!base.metadataFlags("B").metadataFlags("C").exists());
        QVERIFY(!base.metadataFlags("B").metadataSingleFlag("V").exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.metadataSingleFlag("B").metadataSingleFlag("V").set(other);
            base.metadataFlags("B").metadataFlags("C").set(other);
            base.metadataFlags("D").metadataSingleFlag("X").set(other);
        }
        base.metadataFlags("B").metadataFlags("C").setDouble(3.0);
        base.metadataFlags("B").metadataSingleFlag("V").setDouble(-3.0);
        base.metadataFlags("D").metadataSingleFlag("X").setDouble(-3.0);

        base.metadataFlags("B").remove();
        QVERIFY(!base.metadataFlags("B").exists());
        QVERIFY(base.metadataFlags("A").exists());
        base.metadataSingleFlag("U").remove();
        QVERIFY(!base.metadataSingleFlag("U").exists());
        QVERIFY(base.metadataFlags("A").exists());
        base.clear();
        QVERIFY(!base.metadataFlags("A").exists());
        QVERIFY(!base.metadataSingleFlag("U").exists());
        QCOMPARE(base.getType(), Type::MetadataFlags);
        QVERIFY(base.exists());
        QVERIFY(base.isMetadata());

        base.metadataFlags("A").metadataFlags("B").metadataFlags("C").setDouble(1.0);
        base.metadataSingleFlag("D").metadataSingleFlag("E").metadataSingleFlag("F").setDouble(2.0);
        base.detachFromRoot();
        base.metadataFlags("A").metadataFlags("B").setType(Type::MetadataFlags);
        QCOMPARE(base.metadataFlags("A").metadataFlags("B").metadataFlags("C").toDouble(), 1.0);
        QCOMPARE(base.metadataSingleFlag("D")
                     .metadataSingleFlag("E")
                     .metadataSingleFlag("F")
                     .toDouble(), 2.0);
        base.metadataSingleFlag("D").metadataSingleFlag("E").setType(Type::MetadataFlags);
        QCOMPARE(base.metadataFlags("A").metadataFlags("B").metadataFlags("C").toDouble(), 1.0);
        QCOMPARE(base.metadataSingleFlag("D")
                     .metadataSingleFlag("E")
                     .metadataSingleFlag("F")
                     .toDouble(), 2.0);
        base["^A/^B"].setType(Type::MetadataFlags);
        QCOMPARE(base.metadataFlags("A").metadataFlags("B").metadataFlags("C").toDouble(), 1.0);
        QCOMPARE(base.metadataSingleFlag("D")
                     .metadataSingleFlag("E")
                     .metadataSingleFlag("F")
                     .toDouble(), 2.0);
        base["D/E"].setType(Type::MetadataFlags);
        QCOMPARE(base.metadataFlags("A").metadataFlags("B").metadataFlags("C").toDouble(), 1.0);
        QCOMPARE(base.metadataSingleFlag("D")
                     .metadataSingleFlag("E")
                     .metadataSingleFlag("F")
                     .toDouble(), 2.0);
    }

    void metadataArrayAccess()
    {
        Write base;
        base.detachFromRoot();

        base.metadataArray("A").setDouble(1.0);
        QCOMPARE(base.metadataArray("A").toDouble(), 1.0);
        QCOMPARE(Read(base).metadataArray("A").toDouble(), 1.0);

        base.metadataArray("B").metadataArray("C").setDouble(2.0);
        QCOMPARE(base.metadataArray("B").metadataArray("C").toDouble(), 2.0);
        QCOMPARE(Read(base).metadataArray("B").metadataArray("C").toDouble(), 2.0);
        QCOMPARE(base.metadata("B").metadata("C").toDouble(), 2.0);

        QVERIFY(base.metadataArray("B").metadataArray("C").exists());
        QVERIFY(base.metadataArray("B").isMetadata());
        base.metadata("B").clear();
        QVERIFY(!base.metadataArray("B").metadataArray("C").exists());
        base.metadataArray("B").metadataArray("C").setDouble(2.0);
        QVERIFY(base.metadataArray("B").metadataArray("C").exists());
        base.metadataArray("B").metadataArray("C").remove();
        QVERIFY(!base.metadataArray("B").metadataArray("C").exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.metadataArray("B").metadataArray("C").set(other);
        }
        base.metadataArray("B").metadataArray("C").setDouble(3.0);

        base.metadataArray("B").remove();
        QVERIFY(!base.metadataArray("B").exists());
        QVERIFY(base.metadataArray("A").exists());
        base.clear();
        QVERIFY(!base.metadataArray("A").exists());
        QCOMPARE(base.getType(), Type::MetadataArray);
        QVERIFY(base.exists());
        QVERIFY(base.isMetadata());

        base.metadataArray("A").metadataArray("B").metadataArray("C").setDouble(1.0);
        base.detachFromRoot();
        base.metadataArray("A").metadataArray("B").setType(Type::MetadataArray);
        QCOMPARE(base.metadataArray("A").metadataArray("B").metadataArray("C").toDouble(), 1.0);
        base["^A/^B"].setType(Type::MetadataArray);
        QCOMPARE(base.metadataArray("A").metadataArray("B").metadataArray("C").toDouble(), 1.0);
    }

    void metadataMatrixAccess()
    {
        Write base;
        base.detachFromRoot();

        base.metadataMatrix("A").setDouble(1.0);
        QCOMPARE(base.metadataMatrix("A").toDouble(), 1.0);
        QCOMPARE(Read(base).metadataMatrix("A").toDouble(), 1.0);

        base.metadataMatrix("B").metadataMatrix("C").setDouble(2.0);
        QCOMPARE(base.metadataMatrix("B").metadataMatrix("C").toDouble(), 2.0);
        QCOMPARE(Read(base).metadataMatrix("B").metadataMatrix("C").toDouble(), 2.0);
        QCOMPARE(base.metadata("B").metadata("C").toDouble(), 2.0);

        QVERIFY(base.metadataMatrix("B").metadataMatrix("C").exists());
        QVERIFY(base.metadataMatrix("B").isMetadata());
        base.metadata("B").clear();
        QVERIFY(!base.metadataMatrix("B").metadataMatrix("C").exists());
        base.metadataMatrix("B").metadataMatrix("C").setDouble(2.0);
        QVERIFY(base.metadataMatrix("B").metadataMatrix("C").exists());
        base.metadataMatrix("B").metadataMatrix("C").remove();
        QVERIFY(!base.metadataMatrix("B").metadataMatrix("C").exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.metadataMatrix("B").metadataMatrix("C").set(other);
        }
        base.metadataMatrix("B").metadataMatrix("C").setDouble(3.0);

        base.metadataMatrix("B").remove();
        QVERIFY(!base.metadataMatrix("B").exists());
        QVERIFY(base.metadataMatrix("A").exists());
        base.clear();
        QVERIFY(!base.metadataMatrix("A").exists());
        QCOMPARE(base.getType(), Type::MetadataMatrix);
        QVERIFY(base.exists());
        QVERIFY(base.isMetadata());

        base.metadataMatrix("A").metadataMatrix("B").metadataMatrix("C").setDouble(1.0);
        base.detachFromRoot();
        base.metadataMatrix("A").metadataMatrix("B").setType(Type::MetadataMatrix);
        QCOMPARE(base.metadataMatrix("A").metadataMatrix("B").metadataMatrix("C").toDouble(), 1.0);
        base["^A/^B"].setType(Type::MetadataMatrix);
        QCOMPARE(base.metadataMatrix("A").metadataMatrix("B").metadataMatrix("C").toDouble(), 1.0);
    }

    void metadataHashAccess()
    {
        Write base;
        base.detachFromRoot();

        base.metadataHash("A").setDouble(1.0);
        base.metadataHashChild("U").setDouble(-1.0);
        QCOMPARE(base.metadataHash("A").toDouble(), 1.0);
        QCOMPARE(Read(base).metadataHash("A").toDouble(), 1.0);
        QCOMPARE(base.metadataHashChild("U").toDouble(), -1.0);
        QCOMPARE(Read(base).metadataHashChild("U").toDouble(), -1.0);

        base.metadataHash("B").metadataHash("C").setDouble(2.0);
        base.metadataHash("B").metadataHashChild("V").setDouble(-2.0);
        QCOMPARE(base.metadataHash("B").metadataHash("C").toDouble(), 2.0);
        QCOMPARE(Read(base).metadataHash("B").metadataHash("C").toDouble(), 2.0);
        QCOMPARE(base.metadata("B").metadata("C").toDouble(), 2.0);
        QCOMPARE(base.metadataHash("B").metadataHashChild("V").toDouble(), -2.0);
        QCOMPARE(Read(base).metadataHash("B").metadataHashChild("V").toDouble(), -2.0);
        QCOMPARE(base.metadata("B").child("V").toDouble(), -2.0);
        QCOMPARE(base.child("U").toDouble(), -1.0);

        QVERIFY(base.metadataHash("B").metadataHashChild("V").exists());
        QVERIFY(base.metadataHash("B").metadataHash("C").exists());
        QVERIFY(base.metadataHash("B").isMetadata());
        base.metadata("B").clear();
        QVERIFY(!base.metadataHash("B").metadataHash("C").exists());
        QVERIFY(!base.metadataHash("B").metadataHashChild("V").exists());
        base.metadataHash("B").metadataHashChild("V").setDouble(-2.0);
        QVERIFY(base.metadataHash("B").metadataHashChild("V").exists());
        QVERIFY(!base.metadataHash("B").metadataHash("C").exists());
        base.metadataHash("B").metadataHash("C").setDouble(2.0);
        QVERIFY(base.metadataHash("B").metadataHash("C").exists());
        QVERIFY(base.metadataHash("B").metadataHashChild("V").exists());
        base.metadataHash("B").metadataHash("C").remove();
        base.metadataHash("B").metadataHashChild("V").remove();
        QVERIFY(!base.metadataHash("B").metadataHash("C").exists());
        QVERIFY(!base.metadataHash("B").metadataHashChild("V").exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.metadataHashChild("B").metadataHashChild("V").set(other);
            base.metadataHash("B").metadataHash("C").set(other);
            base.metadataHash("D").metadataHashChild("X").set(other);
        }
        base.metadataHash("B").metadataHash("C").setDouble(3.0);
        base.metadataHash("B").metadataHashChild("V").setDouble(-3.0);
        base.metadataHash("D").metadataHashChild("X").setDouble(-3.0);

        base.metadataHash("B").remove();
        QVERIFY(!base.metadataHash("B").exists());
        QVERIFY(base.metadataHash("A").exists());
        base.metadataHashChild("U").remove();
        QVERIFY(!base.metadataHashChild("U").exists());
        QVERIFY(base.metadataHash("A").exists());
        base.clear();
        QVERIFY(!base.metadataHash("A").exists());
        QVERIFY(!base.metadataHashChild("U").exists());
        QCOMPARE(base.getType(), Type::MetadataHash);
        QVERIFY(base.exists());
        QVERIFY(base.isMetadata());

        base.metadataHash("A").metadataHash("B").metadataHash("C").setDouble(1.0);
        base.metadataHashChild("D").metadataHashChild("E").metadataHashChild("F").setDouble(2.0);
        base.detachFromRoot();
        base.metadataHash("A").metadataHash("B").setType(Type::MetadataHash);
        QCOMPARE(base.metadataHash("A").metadataHash("B").metadataHash("C").toDouble(), 1.0);
        QCOMPARE(base.metadataHashChild("D")
                     .metadataHashChild("E")
                     .metadataHashChild("F")
                     .toDouble(), 2.0);
        base.metadataHashChild("D").metadataHashChild("E").setType(Type::MetadataHash);
        QCOMPARE(base.metadataHash("A").metadataHash("B").metadataHash("C").toDouble(), 1.0);
        QCOMPARE(base.metadataHashChild("D")
                     .metadataHashChild("E")
                     .metadataHashChild("F")
                     .toDouble(), 2.0);
        base["^A/^B"].setType(Type::MetadataHash);
        QCOMPARE(base.metadataHash("A").metadataHash("B").metadataHash("C").toDouble(), 1.0);
        QCOMPARE(base.metadataHashChild("D")
                     .metadataHashChild("E")
                     .metadataHashChild("F")
                     .toDouble(), 2.0);
        base["D/E"].setType(Type::MetadataHash);
        QCOMPARE(base.metadataHash("A").metadataHash("B").metadataHash("C").toDouble(), 1.0);
        QCOMPARE(base.metadataHashChild("D")
                     .metadataHashChild("E")
                     .metadataHashChild("F")
                     .toDouble(), 2.0);
    }

    void metadataKeyframeAccess()
    {
        Write base;
        base.detachFromRoot();

        base.metadataKeyframe("A").setDouble(1.0);
        QCOMPARE(base.metadataKeyframe("A").toDouble(), 1.0);
        QCOMPARE(Read(base).metadataKeyframe("A").toDouble(), 1.0);

        base.metadataKeyframe("B").metadataKeyframe("C").setDouble(2.0);
        QCOMPARE(base.metadataKeyframe("B").metadataKeyframe("C").toDouble(), 2.0);
        QCOMPARE(Read(base).metadataKeyframe("B").metadataKeyframe("C").toDouble(), 2.0);
        QCOMPARE(base.metadata("B").metadata("C").toDouble(), 2.0);

        QVERIFY(base.metadataKeyframe("B").metadataKeyframe("C").exists());
        QVERIFY(base.metadataKeyframe("B").isMetadata());
        base.metadata("B").clear();
        QVERIFY(!base.metadataKeyframe("B").metadataKeyframe("C").exists());
        base.metadataKeyframe("B").metadataKeyframe("C").setDouble(2.0);
        QVERIFY(base.metadataKeyframe("B").metadataKeyframe("C").exists());
        base.metadataKeyframe("B").metadataKeyframe("C").remove();
        QVERIFY(!base.metadataKeyframe("B").metadataKeyframe("C").exists());

        {
            Write other;
            other.detachFromRoot();
            other.setDouble(3.0);
            base.metadataKeyframe("B").metadataKeyframe("C").set(other);
        }
        base.metadataKeyframe("B").metadataKeyframe("C").setDouble(3.0);

        base.metadataKeyframe("B").remove();
        QVERIFY(!base.metadataKeyframe("B").exists());
        QVERIFY(base.metadataKeyframe("A").exists());
        base.clear();
        QVERIFY(!base.metadataKeyframe("A").exists());
        QCOMPARE(base.getType(), Type::MetadataKeyframe);
        QVERIFY(base.exists());
        QVERIFY(base.isMetadata());

        base.metadataKeyframe("A").metadataKeyframe("B").metadataKeyframe("C").setDouble(1.0);
        base.detachFromRoot();
        base.metadataKeyframe("A").metadataKeyframe("B").setType(Type::MetadataKeyframe);
        QCOMPARE(base.metadataKeyframe("A").metadataKeyframe("B").metadataKeyframe("C").toDouble(),
                 1.0);
        base["^A/^B"].setType(Type::MetadataKeyframe);
        QCOMPARE(base.metadataKeyframe("A").metadataKeyframe("B").metadataKeyframe("C").toDouble(),
                 1.0);
    }
};

QTEST_APPLESS_MAIN(TestHandle)

#include "handle.moc"
