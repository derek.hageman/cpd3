/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "datacore/variant/root.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::Variant;

class TestComposite : public QObject {
Q_OBJECT

private
    slots:

    void format()
    {
        Root v;
        v.write()["/PathDouble"].setDouble(1.0);
        v.write()["/PathInteger"].setInt64(2);
        v.write()["/PathString"].setString("str");
        v.write()["/PathTime"].setDouble(1430438400.0);
        v.write()["/PathArray/#0"].setString("1");
        v.write()["/PathArray/#1"].setString("2");

        QCOMPARE(Composite::applyFormat(v.read(), QStringList() << "double" << "/PathDouble"),
                 QString("1.000"));
        QCOMPARE(Composite::applyFormat(v.read(), QStringList() << "int" << "/PathInteger" << "00"),
                 QString("02"));
        QCOMPARE(Composite::applyFormat(v.read(), QStringList() << "str" << "/PathString" << "uc"),
                 QString("STR"));
        QCOMPARE(Composite::applyFormat(v.read(), QStringList() << "time" << "/PathTime"),
                 QString("2015-05-01T00:00:00Z"));
        QCOMPARE(Composite::applyFormat(v.read(), QStringList() << "join" << "/PathArray" << "|"),
                 QString("1|2"));
        QCOMPARE(Composite::applyFormat(v.read(), QStringList() << "lookup" << "" << "|"
                                                                << "PathString,PathArray/#0" << ""
                                                                << "str"), QString("str|1"));
        QCOMPARE(Composite::applyFormat(v.read(), QStringList() << "ifdef" << "/PathDouble" << "def"
                                                                << "undef"), QString("def"));
        QCOMPARE(Composite::applyFormat(v.read(), QStringList() << "ifdef" << "/Undefined" << "def"
                                                                << "undef"), QString("undef"));
        QCOMPARE(Composite::applyFormat(v.read(), QStringList() << "" << "/PathInteger"),
                 QString("2"));
        QCOMPARE(Composite::applyFormat(v.read(), QStringList() << "" << "/PathString"),
                 QString("str"));
    }

    void calibration()
    {
        Calibration c1;
        Calibration c2;
        Write v = Write::empty();
        Write rv = Write::empty();

        c1 = Composite::toCalibration(v);
        QVERIFY(c1 == c2);

        v.setDouble(3.0);
        c1 = Composite::toCalibration(v);
        c2.clear();
        c2.set(0, 3.0);
        QVERIFY(c1 == c2);

        v.array(0).setDouble(1.0);
        v.array(1).setDouble(2.0);
        v.array(2).setDouble(3.0);
        c1 = Composite::toCalibration(v);
        c2.clear();
        c2.append(1.0);
        c2.append(2.0);
        c2.append(3.0);
        QVERIFY(c1 == c2);
        Composite::fromCalibration(rv, c1, false);
        QVERIFY(v == rv);

        v.hash("Coefficients").array(0).setDouble(1.0);
        v.hash("Coefficients").array(1).setDouble(2.0);
        v.hash("Coefficients").array(2).setDouble(3.0);
        v.hash("Root").setString("PreferNegative");
        c1 = Composite::toCalibration(v);
        QVERIFY(c1 == c2);
        Composite::fromCalibration(rv, c1, true);
        QVERIFY(v == rv);
    }

    void applyTransform()
    {
        Write value = Write::empty();

        value.setReal(1.0);
        auto result = Composite::applyTransform(value, [](const Read &input, Write &output) {
            output.setReal(input.toReal() + 1.0);
        });
        QCOMPARE(result.read().toReal(), 2.0);

        Composite::applyInplace(value, [](Write &data) {
            data.setReal(data.toReal() + 2.0);
        });
        QCOMPARE(value.toReal(), 3.0);

        value.array(0).setReal(1.0);
        value.array(1).setReal(2.0);
        result = Composite::applyTransform(value, [](const Read &input, Write &output) {
            output.setReal(input.toReal() + 0.5);
        });
        QCOMPARE((int) result.read().toArray().size(), 2);
        QCOMPARE(result.read().array(0).toReal(), 1.5);
        QCOMPARE(result.read().array(1).toReal(), 2.5);

        Composite::applyInplace(value, [](Write &data) {
            data.setReal(data.toReal() + 1.5);
        });
        QCOMPARE((int) value.toArray().size(), 2);
        QCOMPARE(value.array(0).toReal(), 2.5);
        QCOMPARE(value.array(1).toReal(), 3.5);

        value.matrix({0, 0}).setReal(1.0);
        value.matrix({0, 1}).setReal(2.0);
        value.matrix({1, 0}).setReal(3.0);
        value.matrix({1, 1}).setReal(4.0);
        result = Composite::applyTransform(value, [](const Read &input, Write &output) {
            output.setReal(input.toReal() + 0.25);
        });
        QCOMPARE(result.read().toMatrix().shape(), (Container::ReadMatrix::key_type{2, 2}));
        QCOMPARE((result.read().matrix({0, 0}).toReal()), 1.25);
        QCOMPARE((result.read().matrix({0, 1}).toReal()), 2.25);
        QCOMPARE((result.read().matrix({1, 0}).toReal()), 3.25);
        QCOMPARE((result.read().matrix({1, 1}).toReal()), 4.25);

        Composite::applyInplace(value, [](Write &data) {
            data.setReal(data.toReal() + 1.25);
        });
        QCOMPARE(value.toMatrix().shape(), (Container::ReadMatrix::key_type{2, 2}));
        QCOMPARE((value.matrix({0, 0}).toReal()), 2.25);
        QCOMPARE((value.matrix({0, 1}).toReal()), 3.25);
        QCOMPARE((value.matrix({1, 0}).toReal()), 4.25);
        QCOMPARE((value.matrix({1, 1}).toReal()), 5.25);
    }

    void invalidate()
    {
        Write value = Write::empty();
        QVERIFY(!Composite::isDefined(value));

        Composite::invalidate(value);
        QCOMPARE(value.getType(), Variant::Type::Empty);

        value.setString("");
        QVERIFY(Composite::isDefined(value));
        Composite::invalidate(value);
        QCOMPARE(value.getType(), Variant::Type::Empty);
        QVERIFY(!Composite::isDefined(value));

        value.setReal(1.0);
        QVERIFY(Composite::isDefined(value));
        Composite::invalidate(value);
        QCOMPARE(value.getType(), Variant::Type::Real);
        QVERIFY(!FP::defined(value.toReal()));
        QVERIFY(!Composite::isDefined(value));

        value.setInteger(1);
        QVERIFY(Composite::isDefined(value));
        Composite::invalidate(value);
        QCOMPARE(value.getType(), Variant::Type::Integer);
        QVERIFY(!INTEGER::defined(value.toInteger()));
        QVERIFY(!Composite::isDefined(value));
    }

    void contamination()
    {
        QVERIFY(Composite::isContaminationFlag("Contaminate"));
        QVERIFY(Composite::isContaminationFlag("Contaminated"));
        QVERIFY(Composite::isContaminationFlag("ContaminateWindSpeed"));
        QVERIFY(!Composite::isContaminationFlag("EBASFlag"));
        QVERIFY(Composite::isContaminationFlag(Flags{"ContaminateWindSpeed"}));
        QVERIFY(Composite::isContaminationFlag(Flags{"ContaminateWindSpeed", "EBASFlag"}));
        QVERIFY(!Composite::isContaminationFlag(Flags{"EBASFlag"}));
    }

    void toNumber()
    {
        Write value = Write::empty();

        QVERIFY(!FP::defined(Composite::toNumber(value)));
        QVERIFY(!INTEGER::defined(Composite::toInteger(value)));

        value.setReal(1.0);
        QCOMPARE(Composite::toNumber(value), 1.0);
        QCOMPARE((int) Composite::toInteger(value), 1);

        value.setInteger(2);
        QCOMPARE(Composite::toNumber(value), 2.0);
        QCOMPARE((int) Composite::toInteger(value), 2);

        value.setString("3");
        QVERIFY(!FP::defined(Composite::toNumber(value)));
        QVERIFY(!INTEGER::defined(Composite::toInteger(value)));
        QCOMPARE(Composite::toNumber(value, true), 3.0);
        QCOMPARE((int) Composite::toInteger(value, true), 3);
    }
};

QTEST_APPLESS_MAIN(TestComposite)

#include "composite.moc"
