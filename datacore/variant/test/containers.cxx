/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "datacore/variant/root.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::Variant;

class TestContainers : public QObject {
Q_OBJECT

private slots:

    void basicFlags()
    {
        Root root;

        QCOMPARE((int) root.read().toFlags().size(), 0);

        root.write().setFlags({"Flag1", "Flag2"});
        QCOMPARE(root.read().toFlags(), (Flags{"Flag1", "Flag2"}));

        Container::WriteFlags w = root.write().writeFlags();
        QCOMPARE((int) w.size(), 2);
        QVERIFY(w.count("Flag1") == 1);
        QVERIFY(w.count("Flag2") == 1);
        QVERIFY(w.count("Flag3") == 0);

        w.insert("Flag3");
        QCOMPARE((int) w.size(), 3);
        QVERIFY(w.count("Flag3") == 1);

        QCOMPARE(root.read().toFlags(), (Flags{"Flag1", "Flag2", "Flag3"}));

        {
            int counter = 0;
            for (const auto &f : w) {
                ++counter;
                if (f == "Flag1")
                    continue;
                if (f == "Flag2")
                    continue;
                if (f == "Flag3")
                    continue;
                QFAIL("Invalid flag");
            }
            QCOMPARE(counter, 3);
        }

        root.write().setEmpty();
        w = root.write().writeFlags();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w.insert("Another");
        QCOMPARE(root.read().getType(), Type::Flags);
    }

    void basicHash()
    {
        Root root;

        QCOMPARE((int) root.read().toHash().size(), 0);

        root.write()["/A/B"] = 1.0;
        root.write()["/C"] = 2.0;
        root.write()["/D"] = 3.0;

        Container::ReadHash r = root.read().toHash();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/A"].toConstHash();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/C"].toHash();
        QCOMPARE((int) r.size(), 0);


        Container::WriteHash w = root.write().toHash();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["C"].toReal(), 2.0);
        QCOMPARE(w["D"].toReal(), 3.0);
        w["C"].setDouble(4.0);
        w["E"].setDouble(5.0);
        w.emplace("F", Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find("C").value().toDouble(), 4.0);
        QCOMPARE(w.find("AAAA"), w.end());
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);
        w.erase("D");
        w.erase(w.find("C"));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);

        w = root["/A"].toHash();
        QCOMPARE((int) w.size(), 1);
        w["G"].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w["G"].toReal(), 7.0);
        QCOMPARE(root["/A/B"].toDouble(), 1.0);
        QCOMPARE(root["/A/G"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toHash();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w["A"].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::Hash);
    }

    void basicArray()
    {
        Root root;

        QCOMPARE((int) root.read().toArray().size(), 0);

        root.write()["#0"] = 1.0;
        root.write()["#1/#0"] = 2.0;
        root.write()["#2"] = 3.0;

        Container::ReadArray r = root.read().toArray();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r[0].toReal(), 1.0);
        QCOMPARE(r[2].toReal(), 3.0);
        QCOMPARE((*r.begin()).toReal(), 1.0);
        QCOMPARE(r.begin()->toReal(), 1.0);
        {
            int counter = 0;
            for (auto v : r) {
                if (counter == 0) {
                    QCOMPARE(v.toReal(), 1.0);
                } else if (counter == 2) {
                    QCOMPARE(v.toReal(), 3.0);
                }
                ++counter;
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.front().toReal(), 1.0);
        QCOMPARE(r.back().toReal(), 3.0);

        r = root["/#1"].toConstArray();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["#0"].toArray();
        QCOMPARE((int) r.size(), 0);


        Container::WriteArray w = root.write().toArray();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w[0].toReal(), 1.0);
        QCOMPARE(w[2].toReal(), 3.0);
        QCOMPARE((*w.begin()).toReal(), 1.0);
        QCOMPARE(w.begin()->toReal(), 1.0);
        QCOMPARE(w.front().toReal(), 1.0);
        QCOMPARE(w.back().toReal(), 3.0);
        w[0].setDouble(1.5);
        w[2].setDouble(3.5);
        QCOMPARE(w[0].toReal(), 1.5);
        QCOMPARE(w[2].toReal(), 3.5);
        QCOMPARE(w.front().toReal(), 1.5);
        QCOMPARE(w.back().toReal(), 3.5);
        w.front().setDouble(1.0);
        w.back().setDouble(3.0);
        QCOMPARE(w[0].toReal(), 1.0);
        QCOMPARE(w[2].toReal(), 3.0);

        QCOMPARE((int) w.size(), 3);
        w.resize(4);
        w.back().setDouble(4.0);
        w.resize(6, Root(5.0));
        QCOMPARE((int) w.size(), 6);
        QCOMPARE(w.back().toReal(), 5.0);
        w.pop_back();
        QCOMPARE(w.back().toReal(), 5.0);
        w.pop_front();
        QCOMPARE((int) w.size(), 4);
        QCOMPARE(w.front().getType(), Type::Array);
        QCOMPARE(w[1].toReal(), 3.0);
        QCOMPARE(w[2].toReal(), 4.0);
        QCOMPARE(w[3].toReal(), 5.0);
        Container::WriteArray::iterator i = w.insert(w.begin(), Root(6.0));
        QCOMPARE(i->toReal(), 6.0);
        QCOMPARE(i[0].toReal(), 6.0);
        i = w.insert(w.end(), 2, Root(7.0));
        QCOMPARE(i->toReal(), 7.0);
        QCOMPARE((int) w.size(), 7);
        i = w.erase(w.end() - 1);
        QVERIFY(i == w.end());
        QCOMPARE((int) w.size(), 6);
        i = w.insert(w.begin(), {Root(1.0), Root(1.5)});
        QCOMPARE(i->toReal(), 1.0);
        QCOMPARE(w[0].toReal(), 1.0);
        QCOMPARE(w[1].toReal(), 1.5);
        i = w.erase(w.begin(), w.begin() + 2);
        QCOMPARE(i->toReal(), 6.0);
        i = w.erase(3);
        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w[0].toReal(), 6.0);
        QCOMPARE(w[1].getType(), Type::Array);
        QCOMPARE(w[2].toReal(), 3.0);
        QCOMPARE(w[3].toReal(), 5.0);
        QCOMPARE(w[4].toReal(), 7.0);

        w = root["/#1"].toArray();
        QCOMPARE((int) w.size(), 1);
        w.push_back(Root(8.0));
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w[0].toReal(), 2.0);
        QCOMPARE(w[1].toReal(), 8.0);
        w.clear();
        QVERIFY(w.empty());
        QCOMPARE((int) w.size(), 0);
        w.push_back(Root(9.0));
        QCOMPARE((int) w.size(), 1);
        QCOMPARE(w[0].toReal(), 9.0);

        w = root.write().toArray();
        QCOMPARE((int) w.size(), 5);

        QCOMPARE(root["/#0"].toReal(), 6.0);
        QCOMPARE(root["/#1/#0"].toReal(), 9.0);
        QCOMPARE(root["/#2"].toReal(), 3.0);
        QCOMPARE(root["/#3"].toReal(), 5.0);
        QCOMPARE(root["/#4"].toReal(), 7.0);


        root.write().setEmpty();
        w = root.write().toArray();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w.push_back(Root(1.0));
        QCOMPARE(root.read().getType(), Type::Array);
    }

    void basicMatrix()
    {
        Root root;

        QCOMPARE((int) root.read().toMatrix().size(), 0);

        root.write()["[0,0]"] = 1.0;
        root.write()["[1,0]/[0,0,0]"] = 2.0;
        root.write()["[1,1]"] = 3.0;

        Container::ReadMatrix r = root.read().toMatrix();
        QCOMPARE((int) r.size(), 4);
        QCOMPARE(r[0].toReal(), 1.0);
        QCOMPARE(r[3].toReal(), 3.0);
        QCOMPARE((r[{0, 0}].toReal()), 1.0);
        QCOMPARE((r[{1, 1}].toReal()), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                switch (counter) {
                case 0:
                    QCOMPARE(v.first, (PathElement::MatrixIndex{0, 0}));
                    QCOMPARE(v.second.toReal(), 1.0);
                    break;
                case 1:
                    QCOMPARE(v.first, (PathElement::MatrixIndex{1, 0}));
                    QCOMPARE(v.second.getType(), Type::Matrix);
                    break;
                case 2:
                    QCOMPARE(v.first, (PathElement::MatrixIndex{0, 1}));
                    QCOMPARE(v.second.getType(), Type::Empty);
                    break;
                case 3:
                    QCOMPARE(v.first, (PathElement::MatrixIndex{1, 1}));
                    QCOMPARE(v.second.toReal(), 3.0);
                    break;
                default:
                    break;
                }
                ++counter;
            }
            QCOMPARE(counter, 4);
        }
        QCOMPARE(r.front().toReal(), 1.0);
        QCOMPARE(r.back().toReal(), 3.0);
        QCOMPARE(r.shape(), (PathElement::MatrixIndex{2, 2}));
        QCOMPARE(r.key(0), (PathElement::MatrixIndex{0, 0}));
        QCOMPARE(r.key(1), (PathElement::MatrixIndex{1, 0}));
        QCOMPARE(r.key(2), (PathElement::MatrixIndex{0, 1}));
        QCOMPARE(r.key(3), (PathElement::MatrixIndex{1, 1}));

        r = root["/[1,0]"].toConstMatrix();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["[0,0]"].toMatrix();
        QCOMPARE((int) r.size(), 0);


        Container::WriteMatrix w = root.write().toMatrix();
        QCOMPARE((int) w.size(), 4);
        QCOMPARE(w[0].toReal(), 1.0);
        QCOMPARE(w[3].toReal(), 3.0);
        QCOMPARE(w.front().toReal(), 1.0);
        QCOMPARE(w.back().toReal(), 3.0);
        w[0].setDouble(1.5);
        w[3].setDouble(3.5);
        QCOMPARE(w[0].toReal(), 1.5);
        QCOMPARE(w[3].toReal(), 3.5);
        QCOMPARE((w[{1, 1}].toReal()), 3.5);
        QCOMPARE(w.front().toReal(), 1.5);
        QCOMPARE(w.back().toReal(), 3.5);
        w[{0, 1}].setDouble(4.0);
        QCOMPARE(w[2].toReal(), 4.0);
        QCOMPARE((w[{0, 1}].toReal()), 4.0);
        w.front().setDouble(1.0);
        w.back().setDouble(3.0);
        QCOMPARE(w[0].toReal(), 1.0);
        QCOMPARE(w[3].toReal(), 3.0);

        QCOMPARE((int) w.size(), 4);
        w.resize(3);
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w[2].toReal(), 4.0);
        w.resize(4, Root(3.0));
        QCOMPARE((int) w.size(), 4);
        QCOMPARE(w.back().toReal(), 3.0);
        w.pop_back();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w[2].toReal(), 4.0);
        w.push_back(Root(3.0));
        QCOMPARE((int) w.size(), 4);
        QCOMPARE(w.back().toReal(), 3.0);
        w.pop_front();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w.back().toReal(), 3.0);
        QCOMPARE(w.front().getType(), Type::Matrix);
        w.push_front(Root(1.0));
        QCOMPARE((int) w.size(), 4);
        QCOMPARE(w.back().toReal(), 3.0);
        QCOMPARE(w.front().toReal(), 1.0);
        Container::WriteMatrix::iterator i = w.erase(w.begin());
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w.front().getType(), Type::Matrix);
        QCOMPARE(i->second.getType(), Type::Matrix);
        i = w.insert(w.begin(), Root(1.0));
        QCOMPARE((int) w.size(), 4);
        QCOMPARE(w.front().toReal(), 1.0);
        QCOMPARE(i.value().toReal(), 1.0);
        i = w.erase(3);
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w.back().toReal(), 4.0);
        QVERIFY(i == w.end());
        i = w.insert(w.end(), 1, Root(3.0));
        QCOMPARE((int) w.size(), 4);
        QCOMPARE(w.back().toReal(), 3.0);
        QCOMPARE((*i).second.toReal(), 3.0);
        i = w.erase(w.end() - 2, w.end());
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w.back().getType(), Type::Matrix);
        QVERIFY(i == w.end());
        i = w.insert(w.end(), {Root(4.0), Root(3.0)});
        QCOMPARE((int) w.size(), 4);
        QCOMPARE(w.back().toReal(), 3.0);
        QCOMPARE((w[{0, 1}].toReal()), 4.0);
        QCOMPARE((w[{1, 1}].toReal()), 3.0);
        QCOMPARE(i.key(), (PathElement::MatrixIndex{0, 1}));

        w.reshape({2});
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w.shape(), (PathElement::MatrixIndex{2}));
        QCOMPARE((w[{0}].toReal()), 1.0);
        QCOMPARE(w[1].getType(), Type::Matrix);
        w.reshape({3, 2}, Root(5.0));
        QCOMPARE((int) w.size(), 6);
        QCOMPARE(w.shape(), (PathElement::MatrixIndex{3, 2}));
        QCOMPARE((w[{0, 0}].toReal()), 1.0);
        QCOMPARE((w[{1, 0}].getType()), Type::Matrix);
        QCOMPARE((w[{2, 0}].toReal()), 5.0);
        QCOMPARE((w[{0, 1}].toReal()), 5.0);
        QCOMPARE((w[{1, 1}].toReal()), 5.0);
        QCOMPARE((w[{2, 1}].toReal()), 5.0);
        w.reshape({2, 3}, Root(6.0));
        QCOMPARE((w[{0, 0}].toReal()), 1.0);
        QCOMPARE((w[{1, 0}].getType()), Type::Matrix);
        QCOMPARE((w[{0, 1}].toReal()), 5.0);
        QCOMPARE((w[{1, 1}].toReal()), 5.0);
        QCOMPARE((w[{0, 2}].toReal()), 6.0);
        QCOMPARE((w[{1, 2}].toReal()), 6.0);

        r = root.read().toMatrix();
        QCOMPARE((r[{0, 0}].toReal()), 1.0);
        QCOMPARE((r[{1, 0}].getType()), Type::Matrix);
        QCOMPARE((r[{0, 1}].toReal()), 5.0);
        QCOMPARE((r[{1, 1}].toReal()), 5.0);
        QCOMPARE((r[{0, 2}].toReal()), 6.0);
        QCOMPARE((r[{1, 2}].toReal()), 6.0);


        w = root["/[1,0]"].toMatrix();
        QCOMPARE((int) w.size(), 1);
        w.push_back(Root(3.0));
        QCOMPARE((int) w.size(), 2);
        w.clear();
        QCOMPARE((int) w.size(), 0);
        w.reshape({2, 2}, Root(4.0));
        QCOMPARE((int) w.size(), 4);
        QCOMPARE((w[{0, 0}].toReal()), 4.0);
        QCOMPARE((w[{1, 0}].toReal()), 4.0);
        QCOMPARE((w[{0, 1}].toReal()), 4.0);
        QCOMPARE((w[{1, 1}].toReal()), 4.0);
        w.reshape({3});
        QCOMPARE((int) w.size(), 3);
        QCOMPARE((w[{0}].toReal()), 4.0);
        QCOMPARE((w[{1}].toReal()), 4.0);
        QCOMPARE((w[{2}].getType()), Type::Empty);

        QCOMPARE(root["[0,0]"].toDouble(), 1.0);
        QCOMPARE(root["[1,0]/[0]"].toDouble(), 4.0);
        QCOMPARE(root["[1,0]/[1]"].toDouble(), 4.0);


        root.write().setEmpty();
        w = root.write().toMatrix();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w[{0, 1}].set(Root(1.0));
        QCOMPARE(root.read().getType(), Type::Matrix);
    }

    void basicKeyframe()
    {
        Root root;

        QCOMPARE((int) root.read().toKeyframe().size(), 0);

        root.write()["/@0/@1"] = 1.0;
        root.write()["/@2"] = 2.0;
        root.write()["/@3"] = 3.0;

        Container::ReadKeyframe r = root.read().toKeyframe();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r[2].toReal(), 2.0);
        QCOMPARE(r[3].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == 0.0 || v.first == 2.0 || v.first == 3.0);
                if (v.first == 2.0) {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == 3.0) {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find(2.0).value().toDouble(), 2.0);
        QCOMPARE(r.find(9999.0), r.end());
        QCOMPARE(r.lower_bound(2.0).value().toDouble(), 2.0);
        QCOMPARE(r.upper_bound(2.0).value().toDouble(), 3.0);

        r = root["/@0.0"].toConstKeyframe();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/@2"].toKeyframe();
        QCOMPARE((int) r.size(), 0);


        Container::WriteKeyframe w = root.write().toKeyframe();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w[2.0].toReal(), 2.0);
        QCOMPARE(w[3.0].toReal(), 3.0);
        w[2.0].setDouble(4.0);
        w[5.5].setDouble(5.0);
        w.emplace(6.0, Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find(2.0).value().toDouble(), 4.0);
        QCOMPARE(w.find(9999), w.end());
        QCOMPARE(w.lower_bound(2.0).value().toDouble(), 4.0);
        QCOMPARE(w.lower_bound(2.0)->first, 2.0);
        QCOMPARE(w.upper_bound(4.0).value().toDouble(), 5.0);
        QCOMPARE((*w.upper_bound(4.0)).first, 5.5);
        QCOMPARE(w[5.5].toReal(), 5.0);
        QCOMPARE(w[6.0].toReal(), 6.0);
        w.erase(3.0);
        w.erase(w.find(2.0));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w[5.5].toReal(), 5.0);
        QCOMPARE(w[6.0].toReal(), 6.0);

        w = root["@0"].toKeyframe();
        QCOMPARE((int) w.size(), 1);
        w[7.0].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w[7.0].toReal(), 7.0);
        QCOMPARE(root["/@0/@1"].toDouble(), 1.0);
        QCOMPARE(root["/@0/@7.0"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toKeyframe();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w[2.0].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::Keyframe);
    }

    void basicMetadata()
    {
        Root root;

        QCOMPARE((int) root.read().toHash().size(), 0);

        root.write()["/*rA/*iB"] = 1.0;
        root.write()["/*rC"] = 2.0;
        root.write()["/*rD"] = 3.0;

        Container::ReadMetadata r = root.read().toMetadata();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/*dA"].toMetadata();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/*rC"].toMetadata();
        QCOMPARE((int) r.size(), 0);
    }

    void basicMetadataReal()
    {
        Root root;

        QCOMPARE((int) root.read().toMetadataReal().size(), 0);

        root.write()["/*dA/*rB"] = 1.0;
        root.write()["/*dC"] = 2.0;
        root.write()["/*dD"] = 3.0;

        Container::ReadMetadataReal r = root.read().toMetadataReal();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/*rA"].toConstMetadataReal();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/*dC"].toMetadataReal();
        QCOMPARE((int) r.size(), 0);


        Container::WriteMetadataReal w = root.write().toMetadataReal();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["C"].toReal(), 2.0);
        QCOMPARE(w["D"].toReal(), 3.0);
        w["C"].setDouble(4.0);
        w["E"].setDouble(5.0);
        w.emplace("F", Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find("C").value().toDouble(), 4.0);
        QCOMPARE(w.find("AAAA"), w.end());
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);
        w.erase("D");
        w.erase(w.find("C"));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);

        w = root["/*dA"].toMetadataReal();
        QCOMPARE((int) w.size(), 1);
        w["G"].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w["G"].toReal(), 7.0);
        QCOMPARE(root["/*dA/*rB"].toDouble(), 1.0);
        QCOMPARE(root["/*rA/*dG"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toMetadataReal();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w["A"].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::MetadataReal);
    }

    void basicMetadataInteger()
    {
        Root root;

        QCOMPARE((int) root.read().toMetadataInteger().size(), 0);

        root.write()["/*iA/*iB"] = 1.0;
        root.write()["/*iC"] = 2.0;
        root.write()["/*iD"] = 3.0;

        Container::ReadMetadataInteger r = root.read().toMetadataInteger();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/*iA"].toConstMetadataInteger();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/*iC"].toMetadataInteger();
        QCOMPARE((int) r.size(), 0);


        Container::WriteMetadataInteger w = root.write().toMetadataInteger();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["C"].toReal(), 2.0);
        QCOMPARE(w["D"].toReal(), 3.0);
        w["C"].setDouble(4.0);
        w["E"].setDouble(5.0);
        w.emplace("F", Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find("C").value().toDouble(), 4.0);
        QCOMPARE(w.find("AAAA"), w.end());
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);
        w.erase("D");
        w.erase(w.find("C"));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);

        w = root["/*iA"].toMetadataInteger();
        QCOMPARE((int) w.size(), 1);
        w["G"].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w["G"].toReal(), 7.0);
        QCOMPARE(root["/*iA/*iB"].toDouble(), 1.0);
        QCOMPARE(root["/*iA/*iG"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toMetadataInteger();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w["A"].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::MetadataInteger);
    }

    void basicMetadataBoolean()
    {
        Root root;

        QCOMPARE((int) root.read().toMetadataBoolean().size(), 0);

        root.write()["/*bA/*bB"] = 1.0;
        root.write()["/*bC"] = 2.0;
        root.write()["/*bD"] = 3.0;

        Container::ReadMetadataBoolean r = root.read().toMetadataBoolean();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/*bA"].toConstMetadataBoolean();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/*bC"].toMetadataBoolean();
        QCOMPARE((int) r.size(), 0);


        Container::WriteMetadataBoolean w = root.write().toMetadataBoolean();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["C"].toReal(), 2.0);
        QCOMPARE(w["D"].toReal(), 3.0);
        w["C"].setDouble(4.0);
        w["E"].setDouble(5.0);
        w.emplace("F", Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find("C").value().toDouble(), 4.0);
        QCOMPARE(w.find("AAAA"), w.end());
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);
        w.erase("D");
        w.erase(w.find("C"));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);

        w = root["/*bA"].toMetadataBoolean();
        QCOMPARE((int) w.size(), 1);
        w["G"].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w["G"].toReal(), 7.0);
        QCOMPARE(root["/*bA/*bB"].toDouble(), 1.0);
        QCOMPARE(root["/*bA/*bG"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toMetadataBoolean();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w["A"].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::MetadataBoolean);
    }

    void basicMetadataString()
    {
        Root root;

        QCOMPARE((int) root.read().toMetadataString().size(), 0);

        root.write()["/*sA/*sB"] = 1.0;
        root.write()["/*sC"] = 2.0;
        root.write()["/*sD"] = 3.0;

        Container::ReadMetadataString r = root.read().toMetadataString();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/*sA"].toConstMetadataString();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/*sC"].toMetadataString();
        QCOMPARE((int) r.size(), 0);


        Container::WriteMetadataString w = root.write().toMetadataString();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["C"].toReal(), 2.0);
        QCOMPARE(w["D"].toReal(), 3.0);
        w["C"].setDouble(4.0);
        w["E"].setDouble(5.0);
        w.emplace("F", Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find("C").value().toDouble(), 4.0);
        QCOMPARE(w.find("AAAA"), w.end());
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);
        w.erase("D");
        w.erase(w.find("C"));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);

        w = root["/*sA"].toMetadataString();
        QCOMPARE((int) w.size(), 1);
        w["G"].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w["G"].toReal(), 7.0);
        QCOMPARE(root["/*sA/*sB"].toDouble(), 1.0);
        QCOMPARE(root["/*sA/*sG"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toMetadataString();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w["A"].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::MetadataString);
    }

    void basicMetadataBytes()
    {
        Root root;

        QCOMPARE((int) root.read().toMetadataBytes().size(), 0);

        root.write()["/*nA/*nB"] = 1.0;
        root.write()["/*nC"] = 2.0;
        root.write()["/*nD"] = 3.0;

        Container::ReadMetadataBytes r = root.read().toMetadataBytes();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/*nA"].toConstMetadataBytes();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/*nC"].toMetadataBytes();
        QCOMPARE((int) r.size(), 0);


        Container::WriteMetadataBytes w = root.write().toMetadataBytes();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["C"].toReal(), 2.0);
        QCOMPARE(w["D"].toReal(), 3.0);
        w["C"].setDouble(4.0);
        w["E"].setDouble(5.0);
        w.emplace("F", Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find("C").value().toDouble(), 4.0);
        QCOMPARE(w.find("AAAA"), w.end());
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);
        w.erase("D");
        w.erase(w.find("C"));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);

        w = root["/*nA"].toMetadataBytes();
        QCOMPARE((int) w.size(), 1);
        w["G"].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w["G"].toReal(), 7.0);
        QCOMPARE(root["/*nA/*nB"].toDouble(), 1.0);
        QCOMPARE(root["/*nA/*nG"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toMetadataBytes();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w["A"].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::MetadataBytes);
    }

    void basicMetadataFlags()
    {
        Root root;

        QCOMPARE((int) root.read().toMetadataFlags().size(), 0);

        root.write()["/*fA/*fB"] = 1.0;
        root.write()["/*fC"] = 2.0;
        root.write()["/*fD"] = 3.0;

        Container::ReadMetadataFlags r = root.read().toMetadataFlags();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/*fA"].toConstMetadataFlags();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/*fC"].toMetadataFlags();
        QCOMPARE((int) r.size(), 0);


        Container::WriteMetadataFlags w = root.write().toMetadataFlags();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["C"].toReal(), 2.0);
        QCOMPARE(w["D"].toReal(), 3.0);
        w["C"].setDouble(4.0);
        w["E"].setDouble(5.0);
        w.emplace("F", Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find("C").value().toDouble(), 4.0);
        QCOMPARE(w.find("AAAA"), w.end());
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);
        w.erase("D");
        w.erase(w.find("C"));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);

        w = root["/*fA"].toMetadataFlags();
        QCOMPARE((int) w.size(), 1);
        w["G"].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w["G"].toReal(), 7.0);
        QCOMPARE(root["/*fA/*fB"].toDouble(), 1.0);
        QCOMPARE(root["/*fA/*fG"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toMetadataFlags();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w["A"].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::MetadataFlags);
    }

    void basicMetadataSingleFlag()
    {
        Root root;

        QCOMPARE((int) root.read().toMetadataSingleFlag().size(), 0);

        root.write()["/*lA/*lB"] = 1.0;
        root.write()["/*lC"] = 2.0;
        root.write()["/*lD"] = 3.0;

        Container::ReadMetadataSingleFlag r = root.read().toMetadataSingleFlag();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/*lA"].toConstMetadataSingleFlag();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/*lC"].toMetadataSingleFlag();
        QCOMPARE((int) r.size(), 0);


        Container::WriteMetadataSingleFlag w = root.write().toMetadataSingleFlag();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["C"].toReal(), 2.0);
        QCOMPARE(w["D"].toReal(), 3.0);
        w["C"].setDouble(4.0);
        w["E"].setDouble(5.0);
        w.emplace("F", Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find("C").value().toDouble(), 4.0);
        QCOMPARE(w.find("AAAA"), w.end());
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);
        w.erase("D");
        w.erase(w.find("C"));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);

        w = root["/*lA"].toMetadataSingleFlag();
        QCOMPARE((int) w.size(), 1);
        w["G"].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w["G"].toReal(), 7.0);
        QCOMPARE(root["/*lA/*lB"].toDouble(), 1.0);
        QCOMPARE(root["/*lA/*lG"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toMetadataSingleFlag();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w["A"].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::MetadataFlags);
    }

    void basicMetadataArray()
    {
        Root root;

        QCOMPARE((int) root.read().toMetadataArray().size(), 0);

        root.write()["/*aA/*aB"] = 1.0;
        root.write()["/*aC"] = 2.0;
        root.write()["/*aD"] = 3.0;

        Container::ReadMetadataArray r = root.read().toMetadataArray();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/*aA"].toConstMetadataArray();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/*aC"].toMetadataArray();
        QCOMPARE((int) r.size(), 0);


        Container::WriteMetadataArray w = root.write().toMetadataArray();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["C"].toReal(), 2.0);
        QCOMPARE(w["D"].toReal(), 3.0);
        w["C"].setDouble(4.0);
        w["E"].setDouble(5.0);
        w.emplace("F", Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find("C").value().toDouble(), 4.0);
        QCOMPARE(w.find("AAAA"), w.end());
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);
        w.erase("D");
        w.erase(w.find("C"));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);

        w = root["/*aA"].toMetadataArray();
        QCOMPARE((int) w.size(), 1);
        w["G"].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w["G"].toReal(), 7.0);
        QCOMPARE(root["/*aA/*aB"].toDouble(), 1.0);
        QCOMPARE(root["/*aA/*aG"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toMetadataArray();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w["A"].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::MetadataArray);
    }

    void basicMetadataMatrix()
    {
        Root root;

        QCOMPARE((int) root.read().toMetadataMatrix().size(), 0);

        root.write()["/*tA/*tB"] = 1.0;
        root.write()["/*tC"] = 2.0;
        root.write()["/*tD"] = 3.0;

        Container::ReadMetadataMatrix r = root.read().toMetadataMatrix();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/*tA"].toConstMetadataMatrix();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/*tC"].toMetadataMatrix();
        QCOMPARE((int) r.size(), 0);


        Container::WriteMetadataMatrix w = root.write().toMetadataMatrix();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["C"].toReal(), 2.0);
        QCOMPARE(w["D"].toReal(), 3.0);
        w["C"].setDouble(4.0);
        w["E"].setDouble(5.0);
        w.emplace("F", Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find("C").value().toDouble(), 4.0);
        QCOMPARE(w.find("AAAA"), w.end());
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);
        w.erase("D");
        w.erase(w.find("C"));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);

        w = root["/*tA"].toMetadataMatrix();
        QCOMPARE((int) w.size(), 1);
        w["G"].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w["G"].toReal(), 7.0);
        QCOMPARE(root["/*tA/*tB"].toDouble(), 1.0);
        QCOMPARE(root["/*tA/*tG"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toMetadataMatrix();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w["A"].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::MetadataMatrix);
    }

    void basicMetadataHash()
    {
        Root root;

        QCOMPARE((int) root.read().toMetadataHash().size(), 0);

        root.write()["/*hA/*hB"] = 1.0;
        root.write()["/*hC"] = 2.0;
        root.write()["/*hD"] = 3.0;

        Container::ReadMetadataHash r = root.read().toMetadataHash();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/*hA"].toConstMetadataHash();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/*hC"].toMetadataHash();
        QCOMPARE((int) r.size(), 0);


        Container::WriteMetadataHash w = root.write().toMetadataHash();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["C"].toReal(), 2.0);
        QCOMPARE(w["D"].toReal(), 3.0);
        w["C"].setDouble(4.0);
        w["E"].setDouble(5.0);
        w.emplace("F", Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find("C").value().toDouble(), 4.0);
        QCOMPARE(w.find("AAAA"), w.end());
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);
        w.erase("D");
        w.erase(w.find("C"));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);

        w = root["/*hA"].toMetadataHash();
        QCOMPARE((int) w.size(), 1);
        w["G"].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w["G"].toReal(), 7.0);
        QCOMPARE(root["/*hA/*hB"].toDouble(), 1.0);
        QCOMPARE(root["/*hA/*hG"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toMetadataHash();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w["A"].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::MetadataHash);
    }

    void basicMetadataHashChild()
    {
        Root root;

        QCOMPARE((int) root.read().toMetadataHashChild().size(), 0);

        root.write()["/*cA/*cB"] = 1.0;
        root.write()["/*cC"] = 2.0;
        root.write()["/*cD"] = 3.0;

        Container::ReadMetadataHashChild r = root.read().toMetadataHashChild();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/*cA"].toConstMetadataHashChild();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/*cC"].toMetadataHashChild();
        QCOMPARE((int) r.size(), 0);


        Container::WriteMetadataHashChild w = root.write().toMetadataHashChild();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["C"].toReal(), 2.0);
        QCOMPARE(w["D"].toReal(), 3.0);
        w["C"].setDouble(4.0);
        w["E"].setDouble(5.0);
        w.emplace("F", Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find("C").value().toDouble(), 4.0);
        QCOMPARE(w.find("AAAA"), w.end());
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);
        w.erase("D");
        w.erase(w.find("C"));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);

        w = root["/*cA"].toMetadataHashChild();
        QCOMPARE((int) w.size(), 1);
        w["G"].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w["G"].toReal(), 7.0);
        QCOMPARE(root["/*cA/*cB"].toDouble(), 1.0);
        QCOMPARE(root["/*cA/*cG"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toMetadataHashChild();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w["A"].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::MetadataHash);
    }

    void basicMetadataKeyframe()
    {
        Root root;

        QCOMPARE((int) root.read().toMetadataKeyframe().size(), 0);

        root.write()["/*eA/*eB"] = 1.0;
        root.write()["/*eC"] = 2.0;
        root.write()["/*eD"] = 3.0;

        Container::ReadMetadataKeyframe r = root.read().toMetadataKeyframe();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r["C"].toReal(), 2.0);
        QCOMPARE(r["D"].toReal(), 3.0);
        {
            int counter = 0;
            for (auto v : r) {
                ++counter;
                QVERIFY(v.first == "A" || v.first == "C" || v.first == "D");
                if (v.first == "C") {
                    QCOMPARE(v.second.toDouble(), 2.0);
                } else if (v.first == "D") {
                    QCOMPARE(v.second.toDouble(), 3.0);
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.find("C").value().toDouble(), 2.0);
        QCOMPARE(r.find("AAAA"), r.end());

        r = root["/*eA"].toConstMetadataKeyframe();
        QCOMPARE((int) r.size(), 1);

        r = static_cast<const Root &>(root)["/*eC"].toMetadataKeyframe();
        QCOMPARE((int) r.size(), 0);


        Container::WriteMetadataKeyframe w = root.write().toMetadataKeyframe();
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["C"].toReal(), 2.0);
        QCOMPARE(w["D"].toReal(), 3.0);
        w["C"].setDouble(4.0);
        w["E"].setDouble(5.0);
        w.emplace("F", Root(6.0));

        QCOMPARE((int) w.size(), 5);
        QCOMPARE(w.find("C").value().toDouble(), 4.0);
        QCOMPARE(w.find("AAAA"), w.end());
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);
        w.erase("D");
        w.erase(w.find("C"));
        QCOMPARE((int) w.size(), 3);
        QCOMPARE(w["E"].toReal(), 5.0);
        QCOMPARE(w["F"].toReal(), 6.0);

        w = root["/*eA"].toMetadataKeyframe();
        QCOMPARE((int) w.size(), 1);
        w["G"].setDouble(7.0);
        QCOMPARE((int) w.size(), 2);
        QCOMPARE(w["G"].toReal(), 7.0);
        QCOMPARE(root["/*eA/*eB"].toDouble(), 1.0);
        QCOMPARE(root["/*eA/*eG"].toDouble(), 7.0);

        root.write().setEmpty();
        w = root.write().toMetadataKeyframe();
        QCOMPARE(root.read().getType(), Type::Empty);
        QCOMPARE((int) w.size(), 0);
        QCOMPARE(root.read().getType(), Type::Empty);
        w["A"].setDouble(1.0);
        QCOMPARE(root.read().getType(), Type::MetadataKeyframe);
    }

    void genericHash()
    {
        Root root;

        root.write()["/A"] = 1.0;
        root.write()["/B"] = 2.0;
        root.write()["/C"] = 3.0;

        Container::ReadGeneric r = root.read().toChildren();
        {
            int counter = 0;
            for (auto i = r.begin(); i != r.end(); ++i) {
                ++counter;
                if (i.stringKey() == "A") {
                    QCOMPARE(i.value().toDouble(), 1.0);
                    QCOMPARE(i.hashIndex(), PathElement::HashIndex("A"));
                } else if (i.stringKey() == "B") {
                    QCOMPARE(i.value().toDouble(), 2.0);
                    QCOMPARE(i.hashIndex(), PathElement::HashIndex("B"));
                } else if (i.stringKey() == "C") {
                    QCOMPARE(i.value().toDouble(), 3.0);
                    QCOMPARE(i.hashIndex(), PathElement::HashIndex("C"));
                } else {
                    QFAIL("invalid key");
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.keys(), (std::unordered_set<std::string>{"A", "B", "C"}));

        Container::ReadGeneric::Control c;
        c.sortStrings = true;
        r = Container::ReadGeneric(root.read(), c);
        {
            auto i = r.begin();
            auto e = r.end();

            QVERIFY(i != e);
            QCOMPARE(i.value().toDouble(), 1.0);
            QCOMPARE(i.hashIndex(), PathElement::HashIndex("A"));
            QCOMPARE(i.stringKey(), std::string("A"));
            ++i;

            QVERIFY(i != e);
            QCOMPARE(i.value().toDouble(), 2.0);
            QCOMPARE(i.hashIndex(), PathElement::HashIndex("B"));
            QCOMPARE(i.stringKey(), std::string("B"));
            ++i;

            QVERIFY(i != e);
            QCOMPARE(i.value().toDouble(), 3.0);
            QCOMPARE(i.hashIndex(), PathElement::HashIndex("C"));
            QCOMPARE(i.stringKey(), std::string("C"));
            ++i;

            QVERIFY(i == e);
        }
        QCOMPARE(r.keys(), (std::unordered_set<std::string>{"A", "B", "C"}));


        root.write()["/A"] = false;
        r = Container::ReadGeneric(root.read());
        QCOMPARE(r.keys(), (std::unordered_set<std::string>{"B", "C"}));
    }

    void genericMetadata()
    {
        Root root;

        root.write()["/*rA"] = 1.0;
        root.write()["/*rB"] = 2.0;
        root.write()["/*rC"] = 3.0;

        Container::ReadGeneric r = root.read().toChildren();
        {
            int counter = 0;
            for (auto i = r.begin(); i != r.end(); ++i) {
                ++counter;
                if (i.stringKey() == "A") {
                    QCOMPARE(i.value().toDouble(), 1.0);
                    QCOMPARE(i.metadataIndex(), PathElement::MetadataIndex("A"));
                } else if (i.stringKey() == "B") {
                    QCOMPARE(i.value().toDouble(), 2.0);
                    QCOMPARE(i.metadataIndex(), PathElement::MetadataIndex("B"));
                } else if (i.stringKey() == "C") {
                    QCOMPARE(i.value().toDouble(), 3.0);
                    QCOMPARE(i.metadataIndex(), PathElement::MetadataIndex("C"));
                } else {
                    QFAIL("invalid key");
                }
            }
            QCOMPARE(counter, 3);
        }
        QCOMPARE(r.keys(), (std::unordered_set<std::string>{"A", "B", "C"}));

        Container::ReadGeneric::Control c;
        c.sortStrings = true;
        r = Container::ReadGeneric(root.read(), c);
        {
            auto i = r.begin();
            auto e = r.end();

            QVERIFY(i != e);
            QCOMPARE(i.value().toDouble(), 1.0);
            QCOMPARE(i.metadataIndex(), PathElement::MetadataIndex("A"));
            QCOMPARE(i.stringKey(), std::string("A"));
            ++i;

            QVERIFY(i != e);
            QCOMPARE(i.value().toDouble(), 2.0);
            QCOMPARE(i.metadataIndex(), PathElement::MetadataIndex("B"));
            QCOMPARE(i.stringKey(), std::string("B"));
            ++i;

            QVERIFY(i != e);
            QCOMPARE(i.value().toDouble(), 3.0);
            QCOMPARE(i.metadataIndex(), PathElement::MetadataIndex("C"));
            QCOMPARE(i.stringKey(), std::string("C"));
            ++i;

            QVERIFY(i == e);
        }
        QCOMPARE(r.keys(), (std::unordered_set<std::string>{"A", "B", "C"}));
    }

    void genericKeyframe()
    {
        Root root;

        root.write()["/@11"] = 1.0;
        root.write()["/@12"] = 2.0;
        root.write()["/@13"] = "3";

        Container::ReadGeneric r = root.read().toChildren();
        {
            auto i = r.begin();
            auto e = r.end();

            QVERIFY(i != e);
            QCOMPARE(i.value().toDouble(), 1.0);
            QCOMPARE(i.keyframeIndex(), 11.0);
            QCOMPARE(i.stringKey(), std::string());
            ++i;

            QVERIFY(i != e);
            QCOMPARE(i.value().toDouble(), 2.0);
            QCOMPARE(i.keyframeIndex(), 12.0);
            QCOMPARE(i.stringKey(), std::string());
            ++i;

            QVERIFY(i != e);
            QCOMPARE(i.value().toString(), std::string("3"));
            QCOMPARE(i.keyframeIndex(), 13.0);
            QCOMPARE(i.stringKey(), std::string("3"));
            ++i;

            QVERIFY(i == e);
        }
        QCOMPARE(r.keys(), (std::unordered_set<std::string>{"3"}));
    }

    void genericArray()
    {
        Root root;

        root.write()["/#0"] = 1.0;
        root.write()["/#1"] = 2.0;
        root.write()["/#2"] = "3";

        Container::ReadGeneric r = root.read().toChildren();
        {
            auto i = r.begin();
            auto e = r.end();

            QVERIFY(i != e);
            QCOMPARE(i.value().toDouble(), 1.0);
            QCOMPARE(static_cast<int>(i.arrayIndex()), 0);
            QCOMPARE(i.stringKey(), std::string());
            ++i;

            QVERIFY(i != e);
            QCOMPARE(i.value().toDouble(), 2.0);
            QCOMPARE(static_cast<int>(i.arrayIndex()), 1);
            QCOMPARE(i.stringKey(), std::string());
            ++i;

            QVERIFY(i != e);
            QCOMPARE(i.value().toString(), std::string("3"));
            QCOMPARE(static_cast<int>(i.arrayIndex()), 2);
            QCOMPARE(i.stringKey(), std::string("3"));
            ++i;

            QVERIFY(i == e);
        }
        QCOMPARE(r.keys(), (std::unordered_set<std::string>{"3"}));
    }

    void genericMatrix()
    {
        Root root;

        root.write()["/[0,0]"] = 1.0;
        root.write()["/[0,1]"] = 2.0;
        root.write()["/[0,2]"] = "3";

        Container::ReadGeneric r = root.read().toChildren();
        {
            auto i = r.begin();
            auto e = r.end();

            QVERIFY(i != e);
            QCOMPARE(i.value().toDouble(), 1.0);
            QCOMPARE(i.matrixIndex(), (PathElement::MatrixIndex{0, 0}));
            QCOMPARE(i.stringKey(), std::string());
            ++i;

            QVERIFY(i != e);
            QCOMPARE(i.value().toDouble(), 2.0);
            QCOMPARE(i.matrixIndex(), (PathElement::MatrixIndex{0, 1}));
            QCOMPARE(i.stringKey(), std::string());
            ++i;

            QVERIFY(i != e);
            QCOMPARE(i.value().toString(), std::string("3"));
            QCOMPARE(i.matrixIndex(), (PathElement::MatrixIndex{0, 2}));
            QCOMPARE(i.stringKey(), std::string("3"));
            ++i;

            QVERIFY(i == e);
        }
        QCOMPARE(r.keys(), (std::unordered_set<std::string>{"3"}));
    }

    void genericFlags()
    {
        Root root;

        root.write().setFlags({"A", "B", "C"});

        Container::ReadGeneric r = root.read().toChildren();
        QCOMPARE(r.keys(), (std::unordered_set<std::string>{"A", "B", "C"}));
    }

    void genericString()
    {
        Root root;

        root.write().setString("ABC");

        Container::ReadGeneric r = root.read().toChildren();
        QCOMPARE(r.keys(), (std::unordered_set<std::string>{"ABC"}));
    }
};

QTEST_APPLESS_MAIN(TestContainers)

#include "containers.moc"
