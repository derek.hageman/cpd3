/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "datacore/variant/path.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::Variant;

class TestPath : public QObject {
Q_OBJECT

private slots:

    void types()
    {
        std::string str;
        PathElement::Encoded enc;

        PathElement empty;
        QVERIFY(empty == PathElement());
        QCOMPARE(empty.toString(), std::string("\\_"));

        PathElement anyString(PathElement::Type::AnyString, "aaa");
        QVERIFY(anyString != empty);
        QVERIFY(anyString == PathElement(PathElement::Type::AnyString, "aaa"));
        QVERIFY(anyString != PathElement(PathElement::Type::AnyString, "aaab"));
        QVERIFY(anyString.hash() == PathElement(PathElement::Type::AnyString, "aaa").hash());
        QCOMPARE(anyString.toString(), std::string("aaa"));
        QCOMPARE(PathElement::fromString("aaa"), anyString);
        str = "aaa";
        QCOMPARE(PathElement::fromString(str), anyString);
        QCOMPARE(anyString.encode(), PathElement::Encoded("string", "aaa"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("string", "aaa")), anyString);
        enc = PathElement::Encoded("string", "aaa");
        QCOMPARE(PathElement::decode(enc), anyString);

        PathElement anyMetadata(PathElement::Type::AnyMetadata, "aaa");
        QVERIFY(anyMetadata != empty);
        QVERIFY(anyMetadata == PathElement(PathElement::Type::AnyMetadata, "aaa"));
        QVERIFY(anyMetadata != PathElement(PathElement::Type::AnyMetadata, "aaab"));
        QVERIFY(anyMetadata.hash() == PathElement(PathElement::Type::AnyMetadata, "aaa").hash());
        QCOMPARE(anyMetadata.toString(), std::string("^aaa"));
        QCOMPARE(PathElement::fromString("^aaa"), anyMetadata);
        str = "^aaa";
        QCOMPARE(PathElement::fromString(str), anyMetadata);
        QCOMPARE(anyMetadata.encode(), PathElement::Encoded("metadata", "aaa"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("metadata", "aaa")), anyMetadata);
        enc = PathElement::Encoded("metadata", "aaa");
        QCOMPARE(PathElement::decode(enc), anyMetadata);

        PathElement hash(PathElement::Type::Hash, "aaa");
        QVERIFY(hash != empty);
        QVERIFY(hash == PathElement(PathElement::Type::Hash, "aaa"));
        QVERIFY(hash != PathElement(PathElement::Type::Hash, "aaab"));
        QVERIFY(hash.hash() == PathElement(PathElement::Type::Hash, "aaa").hash());
        QCOMPARE(hash.toString(), std::string("aaa"));
        QCOMPARE(hash.toString(false, true), std::string("&aaa"));
        QCOMPARE(PathElement::fromString("&aaa"), hash);
        str = "&aaa";
        QCOMPARE(PathElement::fromString(str), hash);
        QCOMPARE(hash.encode(), PathElement::Encoded("hash", "aaa"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("hash", "aaa")), hash);
        enc = PathElement::Encoded("hash", "aaa");
        QCOMPARE(PathElement::decode(enc), hash);

        PathElement array(PathElement::Type::Array, 1);
        QVERIFY(array != empty);
        QVERIFY(array == PathElement(PathElement::Type::Array, 1));
        QVERIFY(array != PathElement(PathElement::Type::Array, 2));
        QVERIFY(array.hash() == PathElement(PathElement::Type::Array, 1).hash());
        QCOMPARE(array.toString(), std::string("#1"));
        QCOMPARE(PathElement::fromString("#1"), array);
        str = "#1";
        QCOMPARE(PathElement::fromString(str), array);
        QCOMPARE(array.encode(), PathElement::Encoded("array", "1"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("array", "1")), array);
        enc = PathElement::Encoded("array", "1");
        QCOMPARE(PathElement::decode(enc), array);

        PathElement matrix(PathElement::Type::Matrix, PathElement::MatrixIndex{1, 2});
        QVERIFY(matrix != empty);
        QVERIFY(matrix == PathElement(PathElement::Type::Matrix, PathElement::MatrixIndex{1, 2}));
        QVERIFY(matrix !=
                        PathElement(PathElement::Type::Matrix, PathElement::MatrixIndex{1, 2, 3}));
        QVERIFY(matrix.hash() ==
                        PathElement(PathElement::Type::Matrix,
                                    PathElement::MatrixIndex{1, 2}).hash());
        QCOMPARE(matrix.toString(), std::string("[1:2]"));
        QCOMPARE(PathElement::fromString("[1:2]"), matrix);
        str = "[1:2]";
        QCOMPARE(PathElement::fromString(str), matrix);
        QCOMPARE(matrix.encode(), PathElement::Encoded("matrix", "1,2"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("matrix", "1,2")), matrix);
        enc = PathElement::Encoded("matrix", "1,2");
        QCOMPARE(PathElement::decode(enc), matrix);

        PathElement keyframe(PathElement::Type::Keyframe, 2.5);
        QVERIFY(keyframe != empty);
        QVERIFY(keyframe == PathElement(PathElement::Type::Keyframe, 2.5));
        QVERIFY(keyframe != PathElement(PathElement::Type::Keyframe, 2.75));
        QVERIFY(keyframe.hash() == PathElement(PathElement::Type::Keyframe, 2.5).hash());
        QCOMPARE(keyframe.toString(), std::string("@2.5"));
        QCOMPARE(PathElement::fromString("@2.5"), keyframe);
        str = "@2.5";
        QCOMPARE(PathElement::fromString(str), keyframe);
        QCOMPARE(keyframe.encode(), PathElement::Encoded("keyframe", "2.5"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("keyframe", "2.5")), keyframe);
        enc = PathElement::Encoded("keyframe", "2.5");
        QCOMPARE(PathElement::decode(enc), keyframe);

        PathElement metadataReal(PathElement::Type::MetadataReal, "abc");
        QVERIFY(metadataReal != empty);
        QVERIFY(metadataReal == PathElement(PathElement::Type::MetadataReal, "abc"));
        QVERIFY(metadataReal != PathElement(PathElement::Type::MetadataReal, "abce"));
        QVERIFY(metadataReal.hash() == PathElement(PathElement::Type::MetadataReal, "abc").hash());
        QCOMPARE(metadataReal.toString(), std::string("*rabc"));
        QCOMPARE(PathElement::fromString("*rabc"), metadataReal);
        QCOMPARE(PathElement::fromString("*dabc"), metadataReal);
        str = "*rabc";
        QCOMPARE(PathElement::fromString(str), metadataReal);
        str = "*dabc";
        QCOMPARE(PathElement::fromString(str), metadataReal);
        QCOMPARE(metadataReal.encode(), PathElement::Encoded("metareal", "abc"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("metareal", "abc")), metadataReal);
        enc = PathElement::Encoded("metadatareal", "abc");
        QCOMPARE(PathElement::decode(enc), metadataReal);
        enc = PathElement::Encoded("metadatadouble", "abc");
        QCOMPARE(PathElement::decode(enc), metadataReal);

        PathElement metadataInteger(PathElement::Type::MetadataInteger, "abc");
        QVERIFY(metadataInteger != empty);
        QVERIFY(metadataInteger == PathElement(PathElement::Type::MetadataInteger, "abc"));
        QVERIFY(metadataInteger != PathElement(PathElement::Type::MetadataInteger, "abce"));
        QVERIFY(metadataInteger.hash() ==
                        PathElement(PathElement::Type::MetadataInteger, "abc").hash());
        QCOMPARE(metadataInteger.toString(), std::string("*iabc"));
        QCOMPARE(PathElement::fromString("*iabc"), metadataInteger);
        str = "*iabc";
        QCOMPARE(PathElement::fromString(str), metadataInteger);
        QCOMPARE(metadataInteger.encode(), PathElement::Encoded("metainteger", "abc"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("metainteger", "abc")), metadataInteger);
        enc = PathElement::Encoded("metadatainteger", "abc");
        QCOMPARE(PathElement::decode(enc), metadataInteger);

        PathElement metadataBoolean(PathElement::Type::MetadataBoolean, "abc");
        QVERIFY(metadataBoolean != empty);
        QVERIFY(metadataBoolean == PathElement(PathElement::Type::MetadataBoolean, "abc"));
        QVERIFY(metadataBoolean != PathElement(PathElement::Type::MetadataBoolean, "abce"));
        QVERIFY(metadataBoolean.hash() ==
                        PathElement(PathElement::Type::MetadataBoolean, "abc").hash());
        QCOMPARE(metadataBoolean.toString(), std::string("*babc"));
        QCOMPARE(PathElement::fromString("*babc"), metadataBoolean);
        str = "*babc";
        QCOMPARE(PathElement::fromString(str), metadataBoolean);
        QCOMPARE(metadataBoolean.encode(), PathElement::Encoded("metaboolean", "abc"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("metaboolean", "abc")), metadataBoolean);
        enc = PathElement::Encoded("metadataboolean", "abc");
        QCOMPARE(PathElement::decode(enc), metadataBoolean);

        PathElement metadataString(PathElement::Type::MetadataString, "abc");
        QVERIFY(metadataString != empty);
        QVERIFY(metadataString == PathElement(PathElement::Type::MetadataString, "abc"));
        QVERIFY(metadataString != PathElement(PathElement::Type::MetadataString, "abce"));
        QVERIFY(metadataString.hash() ==
                        PathElement(PathElement::Type::MetadataString, "abc").hash());
        QCOMPARE(metadataString.toString(), std::string("*sabc"));
        QCOMPARE(PathElement::fromString("*sabc"), metadataString);
        str = "*sabc";
        QCOMPARE(PathElement::fromString(str), metadataString);
        QCOMPARE(metadataString.encode(), PathElement::Encoded("metastring", "abc"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("metastring", "abc")), metadataString);
        enc = PathElement::Encoded("metadatastring", "abc");
        QCOMPARE(PathElement::decode(enc), metadataString);

        PathElement metadataBytes(PathElement::Type::MetadataBytes, "abc");
        QVERIFY(metadataBytes != empty);
        QVERIFY(metadataBytes == PathElement(PathElement::Type::MetadataBytes, "abc"));
        QVERIFY(metadataBytes != PathElement(PathElement::Type::MetadataBytes, "abce"));
        QVERIFY(metadataBytes.hash() ==
                        PathElement(PathElement::Type::MetadataBytes, "abc").hash());
        QCOMPARE(metadataBytes.toString(), std::string("*nabc"));
        QCOMPARE(PathElement::fromString("*nabc"), metadataBytes);
        str = "*nabc";
        QCOMPARE(PathElement::fromString(str), metadataBytes);
        QCOMPARE(metadataBytes.encode(), PathElement::Encoded("metabinary", "abc"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("metabinary", "abc")), metadataBytes);
        enc = PathElement::Encoded("metadatabinary", "abc");
        QCOMPARE(PathElement::decode(enc), metadataBytes);

        PathElement metadataFlags(PathElement::Type::MetadataFlags, "abc");
        QVERIFY(metadataFlags != empty);
        QVERIFY(metadataFlags == PathElement(PathElement::Type::MetadataFlags, "abc"));
        QVERIFY(metadataFlags != PathElement(PathElement::Type::MetadataFlags, "abce"));
        QVERIFY(metadataFlags.hash() ==
                        PathElement(PathElement::Type::MetadataFlags, "abc").hash());
        QCOMPARE(metadataFlags.toString(), std::string("*fabc"));
        QCOMPARE(PathElement::fromString("*fabc"), metadataFlags);
        str = "*fabc";
        QCOMPARE(PathElement::fromString(str), metadataFlags);
        QCOMPARE(metadataFlags.encode(), PathElement::Encoded("metaflags", "abc"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("metaflags", "abc")), metadataFlags);
        enc = PathElement::Encoded("metadataflags", "abc");
        QCOMPARE(PathElement::decode(enc), metadataFlags);

        PathElement metadataArray(PathElement::Type::MetadataArray, "abc");
        QVERIFY(metadataArray != empty);
        QVERIFY(metadataArray == PathElement(PathElement::Type::MetadataArray, "abc"));
        QVERIFY(metadataArray != PathElement(PathElement::Type::MetadataArray, "abce"));
        QVERIFY(metadataArray.hash() ==
                        PathElement(PathElement::Type::MetadataArray, "abc").hash());
        QCOMPARE(metadataArray.toString(), std::string("*aabc"));
        QCOMPARE(PathElement::fromString("*aabc"), metadataArray);
        str = "*aabc";
        QCOMPARE(PathElement::fromString(str), metadataArray);
        QCOMPARE(metadataArray.encode(), PathElement::Encoded("metaarray", "abc"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("metaarray", "abc")), metadataArray);
        enc = PathElement::Encoded("metadataarray", "abc");
        QCOMPARE(PathElement::decode(enc), metadataArray);

        PathElement metadataMatrix(PathElement::Type::MetadataMatrix, "abc");
        QVERIFY(metadataMatrix != empty);
        QVERIFY(metadataMatrix == PathElement(PathElement::Type::MetadataMatrix, "abc"));
        QVERIFY(metadataMatrix.hash() ==
                        PathElement(PathElement::Type::MetadataMatrix, "abc").hash());
        QCOMPARE(metadataMatrix.toString(), std::string("*tabc"));
        QCOMPARE(PathElement::fromString("*tabc"), metadataMatrix);
        str = "*tabc";
        QCOMPARE(PathElement::fromString(str), metadataMatrix);
        QCOMPARE(metadataMatrix.encode(), PathElement::Encoded("metamatrix", "abc"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("metamatrix", "abc")), metadataMatrix);
        enc = PathElement::Encoded("metadatamatrix", "abc");
        QCOMPARE(PathElement::decode(enc), metadataMatrix);

        PathElement metadataHash(PathElement::Type::MetadataHash, "abc");
        QVERIFY(metadataHash != empty);
        QVERIFY(metadataHash == PathElement(PathElement::Type::MetadataHash, "abc"));
        QVERIFY(metadataHash != PathElement(PathElement::Type::MetadataHash, "abce"));
        QVERIFY(metadataHash.hash() == PathElement(PathElement::Type::MetadataHash, "abc").hash());
        QCOMPARE(metadataHash.toString(), std::string("*habc"));
        QCOMPARE(PathElement::fromString("*habc"), metadataHash);
        str = "*habc";
        QCOMPARE(PathElement::fromString(str), metadataHash);
        QCOMPARE(metadataHash.encode(), PathElement::Encoded("metahash", "abc"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("metahash", "abc")), metadataHash);
        enc = PathElement::Encoded("metadatahash", "abc");
        QCOMPARE(PathElement::decode(enc), metadataHash);

        PathElement metadataKeyframe(PathElement::Type::MetadataKeyframe, "abc");
        QVERIFY(metadataKeyframe != empty);
        QVERIFY(metadataKeyframe == PathElement(PathElement::Type::MetadataKeyframe, "abc"));
        QVERIFY(metadataKeyframe != PathElement(PathElement::Type::MetadataKeyframe, "abce"));
        QVERIFY(metadataKeyframe.hash() ==
                        PathElement(PathElement::Type::MetadataKeyframe, "abc").hash());
        QCOMPARE(metadataKeyframe.toString(), std::string("*eabc"));
        QCOMPARE(PathElement::fromString("*eabc"), metadataKeyframe);
        str = "*eabc";
        QCOMPARE(PathElement::fromString(str), metadataKeyframe);
        QCOMPARE(metadataKeyframe.encode(), PathElement::Encoded("metakeyframe", "abc"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("metakeyframe", "abc")),
                 metadataKeyframe);
        enc = PathElement::Encoded("metadatakeyframe", "abc");
        QCOMPARE(PathElement::decode(enc), metadataKeyframe);

        PathElement metadataHashChild(PathElement::Type::MetadataHashChild, "abc");
        QVERIFY(metadataHashChild != empty);
        QVERIFY(metadataHashChild == PathElement(PathElement::Type::MetadataHashChild, "abc"));
        QVERIFY(metadataHashChild != PathElement(PathElement::Type::MetadataHashChild, "abce"));
        QVERIFY(metadataHashChild.hash() ==
                        PathElement(PathElement::Type::MetadataHashChild, "abc").hash());
        QCOMPARE(metadataHashChild.toString(), std::string("*cabc"));
        QCOMPARE(PathElement::fromString("*cabc"), metadataHashChild);
        str = "*cabc";
        QCOMPARE(PathElement::fromString(str), metadataHashChild);
        QCOMPARE(metadataHashChild.encode(), PathElement::Encoded("metahashkey", "abc"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("metahashkey", "abc")),
                 metadataHashChild);
        enc = PathElement::Encoded("metahashkey", "abc");
        QCOMPARE(PathElement::decode(enc), metadataHashChild);

        PathElement metadataSingleFlag(PathElement::Type::MetadataSingleFlag, "abc");
        QVERIFY(metadataSingleFlag != empty);
        QVERIFY(metadataSingleFlag == PathElement(PathElement::Type::MetadataSingleFlag, "abc"));
        QVERIFY(metadataSingleFlag != PathElement(PathElement::Type::MetadataSingleFlag, "abce"));
        QVERIFY(metadataSingleFlag.hash() ==
                        PathElement(PathElement::Type::MetadataSingleFlag, "abc").hash());
        QCOMPARE(metadataSingleFlag.toString(), std::string("*labc"));
        QCOMPARE(PathElement::fromString("*labc"), metadataSingleFlag);
        str = "*labc";
        QCOMPARE(PathElement::fromString(str), metadataSingleFlag);
        QCOMPARE(metadataSingleFlag.encode(), PathElement::Encoded("metasingleflag", "abc"));
        QCOMPARE(PathElement::decode(PathElement::Encoded("metasingleflag", "abc")),
                 metadataSingleFlag);
        enc = PathElement::Encoded("metasingleflag", "abc");
        QCOMPARE(PathElement::decode(enc), metadataSingleFlag);

        hash = PathElement(PathElement::Type::Hash, "*daaa");
        {
            auto temp = hash;
            hash = temp;
            QVERIFY(hash == temp);
        }
        QVERIFY(hash != empty);
        QVERIFY(hash == PathElement(PathElement::Type::Hash, "*daaa"));
        QCOMPARE(hash.toString(), std::string("\\*daaa"));
        QCOMPARE(hash.toString(false, true), std::string("&*daaa"));
    }

    void parse()
    {
        QFETCH(std::string, str);
        QFETCH(Path, path);

        Path existing({{PathElement::Type::Hash, "A"},
                       {PathElement::Type::Hash, "B"}});

        QCOMPARE(PathElement::parse(str, existing), path);
        QCOMPARE(PathElement::parse(std::move(str), existing), path);
    }

    void parse_data()
    {
        QTest::addColumn<std::string>("str");
        QTest::addColumn<Path>("path");

        QTest::newRow("Empty") << std::string() << Path{{PathElement::Type::Hash, "A"},
                                                        {PathElement::Type::Hash, "B"}};
        QTest::newRow("Existing") << std::string(".") << Path{{PathElement::Type::Hash, "A"},
                                                              {PathElement::Type::Hash, "B"}};
        QTest::newRow("Root") << std::string("/") << Path();
        QTest::newRow("Add") << std::string("CDE") << Path{{PathElement::Type::Hash,      "A"},
                                                           {PathElement::Type::Hash,      "B"},
                                                           {PathElement::Type::AnyString, "CDE"}};
        QTest::newRow("Parent") << std::string("..") << Path{{PathElement::Type::Hash, "A"}};
        QTest::newRow("Parent other") << std::string("../C") << Path{{PathElement::Type::Hash,      "A"},
                                                                     {PathElement::Type::AnyString, "C"}};

        QTest::newRow("Add multiple") << std::string("Foo/BarBaz")
                                      << Path{{PathElement::Type::Hash,      "A"},
                                              {PathElement::Type::Hash,      "B"},
                                              {PathElement::Type::AnyString, "Foo"},
                                              {PathElement::Type::AnyString, "BarBaz"}};

        QTest::newRow("Empty Element") << std::string("/\\_/Foo")
                                       << Path{{PathElement::Type::AnyString, ""},
                                               {PathElement::Type::AnyString, "Foo"}};
        QTest::newRow("Escaped Element") << std::string("/\\*dA/Foo")
                                         << Path{{PathElement::Type::AnyString, "*dA"},
                                                 {PathElement::Type::AnyString, "Foo"}};

        QTest::newRow("Escaped LF") << std::string("AB\\nCD")
                                    << Path{{PathElement::Type::Hash,      "A"},
                                            {PathElement::Type::Hash,      "B"},
                                            {PathElement::Type::AnyString, "AB\nCD"}};
        QTest::newRow("Escaped CR") << std::string("/ABCD\\r")
                                    << Path{{PathElement::Type::AnyString, "ABCD\r"}};

        QTest::newRow("Composite path") << std::string("/A/./&D/Q/../*dF/")
                                        << Path{{PathElement::Type::AnyString,    "A"},
                                                {PathElement::Type::Hash,         "D"},
                                                {PathElement::Type::MetadataReal, "F"}};

        QTest::newRow("Multiple escape") << std::string("/qwerty/asdf/\\\\Stuff\"")
                                         << Path{{PathElement::Type::AnyString, "qwerty"},
                                                 {PathElement::Type::AnyString, "asdf"},
                                                 {PathElement::Type::AnyString, "\\Stuff\""}};
    }
};

QTEST_APPLESS_MAIN(TestPath)

#include "path.moc"
