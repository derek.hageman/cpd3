/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "datacore/variant/serialization.hxx"
#include "datacore/variant/node.hxx"
#include "datacore/variant/common.hxx"
#include "datacore/variant/handle.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::Variant;

enum LegacyType {
    Legacy_Invalid = 0,
    Legacy_Double = 1,
    Legacy_Integer = 2,
    Legacy_Boolean = 3,
    Legacy_String = 4,
    Legacy_Binary = 5,
    Legacy_Flags = 6,
    Legacy_Array = 7,
    Legacy_Matrix = 8,
    Legacy_Hash = 9,
    Legacy_Keyframe = 10,
    Legacy_MetaDouble = 11,
    Legacy_MetaInteger = 12,
    Legacy_MetaBoolean = 13,
    Legacy_MetaString = 14,
    Legacy_MetaBinary = 15,
    Legacy_MetaFlags = 16,
    Legacy_MetaArray = 17,
    Legacy_MetaMatrix = 18,
    Legacy_MetaHash = 19,
    Legacy_MetaKeyframe = 20,
    Legacy_Overlay = 21
};

class TestSerialization : public QObject {
Q_OBJECT

private slots:

    void legacyEmpty()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_Invalid);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::Empty);
        QCOMPARE(instance.get(), NodeEmpty::instance.get());

        {
            Write input;
            input.detachFromRoot();
            input.setEmpty();
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Invalid));
        }
    }

    void legacyDouble()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::RealConstant);
        QCOMPARE(instance->toReal(), 1.0);

        {
            Write input;
            input.detachFromRoot();
            input.setDouble(1.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
        }
    }

    void legacyInteger()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_Integer);
            stream << static_cast<qint64>(1);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::IntegerConstant);
        QCOMPARE((int) instance->toInteger(), 1);

        {
            Write input;
            input.detachFromRoot();
            input.setInteger(1);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Integer));
            qint64 value = 0;
            stream >> value;
            QCOMPARE(static_cast<qint64>(value), 1);
        }
    }

    void legacyBoolean()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_Boolean);
            stream << static_cast<bool>(true);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::BooleanConstant);
        QCOMPARE(instance->toBoolean(), true);

        {
            Write input;
            input.detachFromRoot();
            input.setBoolean(true);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Boolean));
            bool value = false;
            stream >> value;
            QCOMPARE(value, true);
        }
    }

    void legacyString()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("StringValue");
            stream << static_cast<quint32>(0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::StringConstant);
        QCOMPARE(instance->toString(), std::string("StringValue"));

        {
            Write input;
            input.detachFromRoot();
            input.setString("Value");
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_String));
            QString value;
            stream >> value;
            QCOMPARE(value, QString("Value"));
            quint32 n = 1;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 0);
        }
    }

    void legacyLocalizedString()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("StringValue");
            stream << static_cast<quint32>(1);
            stream << QString("en");
            stream << QString("LValue1");
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::LocalizedStringConstant);
        QCOMPARE(instance->toString(), std::string("StringValue"));
        QCOMPARE(instance->toDisplayString(QLocale("en")), QString("LValue1"));

        {
            Write input;
            input.detachFromRoot();
            input.setDisplayString("en", "Val1");
            input.setString("Value");
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_String));
            QString value;
            stream >> value;
            QCOMPARE(value, QString("Value"));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            stream >> value;
            QCOMPARE(value, QString("en"));
            stream >> value;
            QCOMPARE(value, QString("Val1"));
        }
    }

    void legacyBinary()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_Binary);
            stream << QByteArray("ABCD");
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::BytesConstant);
        QCOMPARE(instance->toBytes(), Bytes("ABCD"));

        {
            Write input;
            input.detachFromRoot();
            input.setBytes(Bytes("Foo"));
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Binary));
            QByteArray value;
            stream >> value;
            QCOMPARE(value, QByteArray("Foo"));
        }
    }

    void legacyFlags()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_Flags);
            stream << (QSet<QString>{"Flag1", "F"});
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::FlagsConstant);
        QCOMPARE(instance->toFlags(), (Flags{"Flag1", "F"}));

        {
            Write input;
            input.detachFromRoot();
            input.setFlags(Flags{"Foobar"});
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Flags));
            QSet<QString> value;
            stream >> value;
            QCOMPARE(value, (QSet<QString>{"Foobar"}));
        }
    }

    void legacyHash()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_Hash);
            stream << static_cast<quint32>(2);
            stream << QString("Key1");
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("Value1");
            stream << static_cast<quint32>(0);
            stream << QString("Key2");
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::HashConstant);
        QCOMPARE(instance->read({PathElement::Type::Hash, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::Hash, "Key2"})->toReal(), 1.0);

        {
            Write input;
            input.detachFromRoot();
            input.hash("ABC").setReal(1.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Hash));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            QString key;
            stream >> key;
            QCOMPARE(key, QString("ABC"));
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
        }
    }

    void legacyArray()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_Array);
            stream << static_cast<quint32>(2);
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(2.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::ArrayConstant);
        QCOMPARE(instance->read({PathElement::Type::Array, 0})->toReal(), 1.0);
        QCOMPARE(instance->read({PathElement::Type::Array, 1})->toReal(), 2.0);

        {
            Write input;
            input.detachFromRoot();
            input.array(0).setReal(1.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Array));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
        }
    }

    void legacyMatrix()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_Matrix);
            stream << static_cast<quint32>(4);
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(2.0);
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(3.0);
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(4.0);
            stream << static_cast<quint8>(2);
            stream << static_cast<quint32>(2);
            stream << static_cast<quint32>(2);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MatrixConstant);
        QCOMPARE(instance->read({PathElement::Type::Matrix, PathElement::MatrixIndex{0, 0}})
                         ->toReal(), 1.0);
        QCOMPARE(instance->read({PathElement::Type::Matrix, PathElement::MatrixIndex{0, 1}})
                         ->toReal(), 3.0);

        {
            Write input;
            input.detachFromRoot();
            input.matrix({0, 0}).setReal(1.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Matrix));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
            quint8 nShape = 0;
            stream >> nShape;
            QCOMPARE(static_cast<int>(nShape), 2);
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
        }
    }

    void legacyKeyframe()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_Keyframe);
            stream << static_cast<quint32>(2);
            stream << static_cast<double>(-1.0);
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("Value1");
            stream << static_cast<quint32>(0);
            stream << static_cast<double>(-2.0);
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::KeyframeConstant);
        QCOMPARE(instance->read({PathElement::Type::Keyframe, -1.0})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::Keyframe, -2.0})->toReal(), 1.0);

        {
            Write input;
            input.detachFromRoot();
            input.keyframe(0.5).setReal(1.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Keyframe));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            double key = 0.0;
            stream >> key;
            QCOMPARE(key, 0.5);
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
        }
    }

    void legacyMetadataDouble()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_MetaDouble);
            stream << static_cast<quint32>(2);
            stream << QString("Key1");
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("Value1");
            stream << static_cast<quint32>(0);
            stream << QString("Key2");
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataRealConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataReal, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataReal, "Key2"})->toReal(), 1.0);

        {
            Write input;
            input.detachFromRoot();
            input.metadataReal("ABC").setReal(1.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_MetaDouble));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            QString key;
            stream >> key;
            QCOMPARE(key, QString("ABC"));
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
        }
    }

    void legacyMetadataInteger()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_MetaInteger);
            stream << static_cast<quint32>(2);
            stream << QString("Key1");
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("Value1");
            stream << static_cast<quint32>(0);
            stream << QString("Key2");
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataIntegerConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataInteger, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataInteger, "Key2"})->toReal(), 1.0);

        {
            Write input;
            input.detachFromRoot();
            input.metadataInteger("ABC").setReal(1.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_MetaInteger));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            QString key;
            stream >> key;
            QCOMPARE(key, QString("ABC"));
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
        }
    }

    void legacyMetadataBoolean()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_MetaBoolean);
            stream << static_cast<quint32>(2);
            stream << QString("Key1");
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("Value1");
            stream << static_cast<quint32>(0);
            stream << QString("Key2");
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataBooleanConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataBoolean, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataBoolean, "Key2"})->toReal(), 1.0);

        {
            Write input;
            input.detachFromRoot();
            input.metadataBoolean("ABC").setReal(1.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_MetaBoolean));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            QString key;
            stream >> key;
            QCOMPARE(key, QString("ABC"));
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
        }
    }

    void legacyMetadataString()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_MetaString);
            stream << static_cast<quint32>(2);
            stream << QString("Key1");
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("Value1");
            stream << static_cast<quint32>(0);
            stream << QString("Key2");
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataStringConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataString, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataString, "Key2"})->toReal(), 1.0);

        {
            Write input;
            input.detachFromRoot();
            input.metadataString("ABC").setReal(1.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_MetaString));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            QString key;
            stream >> key;
            QCOMPARE(key, QString("ABC"));
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
        }
    }

    void legacyMetadataBinary()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_MetaBinary);
            stream << static_cast<quint32>(2);
            stream << QString("Key1");
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("Value1");
            stream << static_cast<quint32>(0);
            stream << QString("Key2");
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataBytesConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataBytes, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataBytes, "Key2"})->toReal(), 1.0);

        {
            Write input;
            input.detachFromRoot();
            input.metadataBytes("ABC").setReal(1.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_MetaBinary));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            QString key;
            stream >> key;
            QCOMPARE(key, QString("ABC"));
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
        }
    }

    void legacyMetadataFlags()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_MetaFlags);
            stream << static_cast<quint32>(2);
            stream << QString("Key1");
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("Value1");
            stream << static_cast<quint32>(0);
            stream << QString("Key2");
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
            stream << static_cast<quint32>(1);
            stream << QString("Key3");
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(2.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataFlagsConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataFlags, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataFlags, "Key2"})->toReal(), 1.0);
        QCOMPARE(instance->read({PathElement::Type::MetadataSingleFlag, "Key3"})->toReal(), 2.0);

        {
            Write input;
            input.detachFromRoot();
            input.metadataFlags("ABC").setReal(1.0);
            input.metadataSingleFlag("DEFG").setReal(2.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_MetaFlags));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            QString key;
            stream >> key;
            QCOMPARE(key, QString("ABC"));
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            stream >> key;
            QCOMPARE(key, QString("DEFG"));
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            stream >> value;
            QCOMPARE(value, 2.0);
        }
    }

    void legacyMetadataArray()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_MetaArray);
            stream << static_cast<quint32>(2);
            stream << QString("Key1");
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("Value1");
            stream << static_cast<quint32>(0);
            stream << QString("Key2");
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataArrayConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataArray, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataArray, "Key2"})->toReal(), 1.0);

        {
            Write input;
            input.detachFromRoot();
            input.metadataArray("ABC").setReal(1.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_MetaArray));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            QString key;
            stream >> key;
            QCOMPARE(key, QString("ABC"));
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
        }
    }

    void legacyMetadataMatrix()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_MetaMatrix);
            stream << static_cast<quint32>(2);
            stream << QString("Key1");
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("Value1");
            stream << static_cast<quint32>(0);
            stream << QString("Key2");
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataMatrixConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataMatrix, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataMatrix, "Key2"})->toReal(), 1.0);

        {
            Write input;
            input.detachFromRoot();
            input.metadataMatrix("ABC").setReal(1.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_MetaMatrix));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            QString key;
            stream >> key;
            QCOMPARE(key, QString("ABC"));
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
        }
    }

    void legacyMetadataHash()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_MetaHash);
            stream << static_cast<quint32>(2);
            stream << QString("Key1");
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("Value1");
            stream << static_cast<quint32>(0);
            stream << QString("Key2");
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
            stream << static_cast<quint32>(1);
            stream << QString("Key3");
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(2.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataHashConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataHash, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataHash, "Key2"})->toReal(), 1.0);
        QCOMPARE(instance->read({PathElement::Type::MetadataHashChild, "Key3"})->toReal(), 2.0);

        {
            Write input;
            input.detachFromRoot();
            input.metadataHash("ABC").setReal(1.0);
            input.metadataHashChild("DEFG").setReal(2.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_MetaHash));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            QString key;
            stream >> key;
            QCOMPARE(key, QString("ABC"));
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            stream >> key;
            QCOMPARE(key, QString("DEFG"));
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            stream >> value;
            QCOMPARE(value, 2.0);
        }
    }

    void legacyMetadataKeyframe()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_MetaKeyframe);
            stream << static_cast<quint32>(2);
            stream << QString("Key1");
            stream << static_cast<quint8>(Legacy_String);
            stream << QString("Value1");
            stream << static_cast<quint32>(0);
            stream << QString("Key2");
            stream << static_cast<quint8>(Legacy_Double);
            stream << static_cast<double>(1.0);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataKeyframeConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataKeyframe, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataKeyframe, "Key2"})->toReal(), 1.0);

        {
            Write input;
            input.detachFromRoot();
            input.metadataKeyframe("ABC").setReal(1.0);
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_MetaKeyframe));
            quint32 n = 0;
            stream >> n;
            QCOMPARE(static_cast<int>(n), 1);
            QString key;
            stream >> key;
            QCOMPARE(key, QString("ABC"));
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Double));
            double value = 0.0;
            stream >> value;
            QCOMPARE(value, 1.0);
        }
    }

    void legacyOverlay()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << static_cast<quint8>(Legacy_Overlay);
            stream << QString("Some/Path");
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::OverlayConstant);
        QCOMPARE(instance->toString(), std::string("Some/Path"));

        {
            Write input;
            input.detachFromRoot();
            input.setOverlay("/Path");
            QDataStream stream(&data, QIODevice::WriteOnly);
            Serialization::serializeLegacy(stream, input);
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            quint8 type = 0;
            stream >> type;
            QCOMPARE(static_cast<int>(type), static_cast<int>(Legacy_Overlay));
            QString value;
            stream >> value;
            QCOMPARE(value, QString("/Path"));
        }
    }


    void basicEmpty()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeEmpty::instance->serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::Empty);
        QCOMPARE(instance.get(), NodeEmpty::instance.get());
    }

    void basicReal()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeReal(1.0).serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::RealConstant);
        QCOMPARE(instance->toReal(), 1.0);
    }

    void basicInteger()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeInteger(1).serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::IntegerConstant);
        QCOMPARE((int) instance->toInteger(), 1);
    }

    void basicBoolean()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeBoolean(true).serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::BooleanConstant);
        QCOMPARE(instance->toBoolean(), true);
    }

    void basicString()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeString("StringValue").serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::StringConstant);
        QCOMPARE(instance->toString(), std::string("StringValue"));
    }

    void basicLocalizedString()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeLocalizedString str;
            str.setString("StringValue");
            str.setDisplayString("es", "LValue1");
            str.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::LocalizedStringConstant);
        QCOMPARE(instance->toString(), std::string("StringValue"));
        QCOMPARE(instance->toDisplayString(QLocale("es")), QString("LValue1"));
    }

    void basicLocalizedStringDemote()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeLocalizedString str;
            str.setString("StringValue");
            str.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::StringConstant);
        QCOMPARE(instance->toString(), std::string("StringValue"));
    }

    void basicBytes()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeBytes(Bytes("ABCD")).serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::BytesConstant);
        QCOMPARE(instance->toBytes(), Bytes("ABCD"));
    }

    void basicFlags()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeFlags(Flags{"Flag1", "F"}).serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::FlagsConstant);
        QCOMPARE(instance->toFlags(), (Flags{"Flag1", "F"}));
    }

    void basicHash()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeHash top;
            top.insert({PathElement::Type::Hash, "Key1"},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::Hash, "Key2"}, std::make_shared<NodeRealConstant>(1.0));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::HashConstant);
        QCOMPARE(instance->read({PathElement::Type::Hash, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::Hash, "Key2"})->toReal(), 1.0);
    }

    void basicArray()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeArray top;
            top.insert({PathElement::Type::Array, 1},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::Array, 0}, std::make_shared<NodeRealConstant>(1.0));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::ArrayConstant);
        QCOMPARE(instance->read({PathElement::Type::Array, 0})->toReal(), 1.0);
        QCOMPARE(instance->read({PathElement::Type::Array, 1})->toString(), std::string("Value1"));
    }

    void basicMatrix()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeMatrix top;
            top.setSize(PathElement::MatrixIndex{2, 3});
            top.insert({PathElement::Type::Matrix, PathElement::MatrixIndex{0, 1}},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::Matrix, PathElement::MatrixIndex{1, 2}},
                       std::make_shared<NodeRealConstant>(1.0));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MatrixConstant);
        QCOMPARE(instance->read({PathElement::Type::Matrix, PathElement::MatrixIndex{1, 2}})
                         ->toReal(), 1.0);
        QCOMPARE(instance->read({PathElement::Type::Matrix, PathElement::MatrixIndex{0, 1}})
                         ->toString(), std::string("Value1"));
    }

    void basicKeyframe()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeKeyframe top;
            top.insert({PathElement::Type::Keyframe, 2.0},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::Keyframe, 3.0}, std::make_shared<NodeRealConstant>(1.0));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::KeyframeConstant);
        QCOMPARE(instance->read({PathElement::Type::Keyframe, 2.0})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::Keyframe, 3.0})->toReal(), 1.0);
    }

    void basicMetadataReal()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeMetadataReal top;
            top.insert({PathElement::Type::MetadataReal, "Key1"},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::MetadataReal, "Key2"},
                       std::make_shared<NodeRealConstant>(1.0));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataRealConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataReal, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataReal, "Key2"})->toReal(), 1.0);
    }

    void basicMetadataInteger()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeMetadataInteger top;
            top.insert({PathElement::Type::MetadataInteger, "Key1"},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::MetadataInteger, "Key2"},
                       std::make_shared<NodeRealConstant>(1.0));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataIntegerConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataInteger, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataInteger, "Key2"})->toReal(), 1.0);
    }

    void basicMetadataBoolean()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeMetadataBoolean top;
            top.insert({PathElement::Type::MetadataBoolean, "Key1"},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::MetadataBoolean, "Key2"},
                       std::make_shared<NodeRealConstant>(1.0));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataBooleanConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataBoolean, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataBoolean, "Key2"})->toReal(), 1.0);
    }

    void basicMetadataString()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeMetadataString top;
            top.insert({PathElement::Type::MetadataString, "Key1"},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::MetadataString, "Key2"},
                       std::make_shared<NodeRealConstant>(1.0));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataStringConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataString, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataString, "Key2"})->toReal(), 1.0);
    }

    void basicMetadataBytes()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeMetadataBytes top;
            top.insert({PathElement::Type::MetadataBytes, "Key1"},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::MetadataBytes, "Key2"},
                       std::make_shared<NodeRealConstant>(1.0));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataBytesConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataBytes, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataBytes, "Key2"})->toReal(), 1.0);
    }

    void basicMetadataFlags()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeMetadataFlags top;
            top.insert({PathElement::Type::MetadataFlags, "Key1"},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::MetadataFlags, "Key2"},
                       std::make_shared<NodeRealConstant>(1.0));
            top.insert({PathElement::Type::MetadataSingleFlag, "Key3"},
                       std::make_shared<NodeIntegerConstant>(2));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataFlagsConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataFlags, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataFlags, "Key2"})->toReal(), 1.0);
        QCOMPARE(static_cast<int>(instance->read({PathElement::Type::MetadataSingleFlag, "Key3"})
                                          ->toInteger()), 2);
    }

    void basicMetadataArray()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeMetadataArray top;
            top.insert({PathElement::Type::MetadataArray, "Key1"},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::MetadataArray, "Key2"},
                       std::make_shared<NodeRealConstant>(1.0));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataArrayConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataArray, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataArray, "Key2"})->toReal(), 1.0);
    }

    void basicMetadataMatrix()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeMetadataMatrix top;
            top.insert({PathElement::Type::MetadataMatrix, "Key1"},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::MetadataMatrix, "Key2"},
                       std::make_shared<NodeRealConstant>(1.0));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataMatrixConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataMatrix, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataMatrix, "Key2"})->toReal(), 1.0);
    }

    void basicMetadataHash()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeMetadataHash top;
            top.insert({PathElement::Type::MetadataHash, "Key1"},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::MetadataHash, "Key2"},
                       std::make_shared<NodeRealConstant>(1.0));
            top.insert({PathElement::Type::MetadataHashChild, "Key3"},
                       std::make_shared<NodeIntegerConstant>(2));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataHashConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataHash, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataHash, "Key2"})->toReal(), 1.0);
        QCOMPARE(static_cast<int>(instance->read({PathElement::Type::MetadataHashChild, "Key3"})
                                          ->toInteger()), 2);
    }

    void basicMetadataKeyframe()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeMetadataKeyframe top;
            top.insert({PathElement::Type::MetadataKeyframe, "Key1"},
                       std::make_shared<NodeStringConstant>("Value1"));
            top.insert({PathElement::Type::MetadataKeyframe, "Key2"},
                       std::make_shared<NodeRealConstant>(1.0));
            top.serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::MetadataKeyframeConstant);
        QCOMPARE(instance->read({PathElement::Type::MetadataKeyframe, "Key1"})->toString(),
                 std::string("Value1"));
        QCOMPARE(instance->read({PathElement::Type::MetadataKeyframe, "Key2"})->toReal(), 1.0);
    }

    void basicOverlay()
    {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            NodeOverlay(std::string("Some/Path")).serialize(stream);
        }
        std::shared_ptr<Node> instance;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            instance = Serialization::construct(stream);
        }

        QVERIFY(instance.get() != nullptr);
        QCOMPARE(instance->type(), Node::Type::OverlayConstant);
        QCOMPARE(instance->toString(), std::string("Some/Path"));
    }
};

QTEST_APPLESS_MAIN(TestSerialization)

#include "serialization.moc"
