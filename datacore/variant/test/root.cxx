/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "datacore/variant/root.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::Variant;

class TestRoot : public QObject {
Q_OBJECT

private slots:

    void empty()
    {
        Root root;
        QCOMPARE(root.read().getType(), Type::Empty);
        QVERIFY(root.isOverlayOpaque());

        Root other = root;
        QCOMPARE(other.read().getType(), Type::Empty);

        other = root;
        QCOMPARE(other.read().getType(), Type::Empty);

        root = std::move(other);
        QCOMPARE(root.read().getType(), Type::Empty);

        Root another = std::move(root);
        QCOMPARE(another.read().getType(), Type::Empty);

        using std::swap;
        swap(another, root);
        QCOMPARE(root.read().getType(), Type::Empty);
    }

    void real()
    {
        Root root(1.0);
        QCOMPARE(root.read().getType(), Type::Real);
        QCOMPARE(root.read().toReal(), 1.0);
        QVERIFY(root.isOverlayOpaque());

        Root other = root;
        QCOMPARE(other.read().toReal(), 1.0);

        other = root;
        QCOMPARE(other.read().toReal(), 1.0);

        root = std::move(other);
        QCOMPARE(root.read().toReal(), 1.0);

        Root another = std::move(root);
        QCOMPARE(another.read().toReal(), 1.0);
    }

    void integer()
    {
        Root root(1);
        QCOMPARE(root.read().getType(), Type::Integer);
        QCOMPARE((int) root.read().toInteger(), 1);
        QVERIFY(root.isOverlayOpaque());

        Root other = root;
        QCOMPARE((int) other.read().toInteger(), 1);

        other = root;
        QCOMPARE((int) other.read().toInteger(), 1);

        root = std::move(other);
        QCOMPARE((int) root.read().toInteger(), 1);

        Root another = std::move(root);
        QCOMPARE((int) another.read().toInteger(), 1);

        Root i64(static_cast<std::int_fast64_t>(2));
        QCOMPARE((int) i64.read().toInteger(), 2);
        Root qi64(static_cast<qint64>(3));
        QCOMPARE((int) qi64.read().toInteger(), 3);
    }

    void boolean()
    {
        Root root(true);
        QCOMPARE(root.read().getType(), Type::Boolean);
        QCOMPARE(root.read().toBoolean(), true);
        QVERIFY(root.isOverlayOpaque());

        Root other = root;
        QCOMPARE(other.read().toBoolean(), true);

        other = root;
        QCOMPARE(other.read().toBoolean(), true);

        root = std::move(other);
        QCOMPARE(root.read().toBoolean(), true);

        Root another = std::move(root);
        QCOMPARE(another.read().toBoolean(), true);
    }

    void string()
    {
        Root root("abcd");
        QCOMPARE(root.read().getType(), Type::String);
        QCOMPARE(root.read().toString(), std::string("abcd"));
        QVERIFY(root.isOverlayOpaque());

        Root other = root;
        QCOMPARE(other.read().toString(), std::string("abcd"));

        other = root;
        QCOMPARE(other.read().toString(), std::string("abcd"));

        root = std::move(other);
        QCOMPARE(root.read().toString(), std::string("abcd"));

        Root another = std::move(root);
        QCOMPARE(another.read().toString(), std::string("abcd"));
    }

    void bytes()
    {
        Root root(Bytes("abcd"));
        QCOMPARE(root.read().getType(), Type::Bytes);
        QCOMPARE(root.read().toBytes(), Bytes("abcd"));
        QVERIFY(root.isOverlayOpaque());

        Root other = root;
        QCOMPARE(other.read().toBytes(), Bytes("abcd"));

        other = root;
        QCOMPARE(other.read().toBytes(), Bytes("abcd"));

        root = std::move(other);
        QCOMPARE(root.read().toBytes(), Bytes("abcd"));

        Root another = std::move(root);
        QCOMPARE(another.read().toBytes(), Bytes("abcd"));
    }

    void flags()
    {
        Root root((Flags{"Flag1", "Flag2"}));
        QCOMPARE(root.read().getType(), Type::Flags);
        QCOMPARE(root.read().toFlags(), (Flags{"Flag1", "Flag2"}));
        QVERIFY(root.isOverlayOpaque());

        Root other = root;
        QCOMPARE(other.read().toFlags(), (Flags{"Flag1", "Flag2"}));

        other = root;
        QCOMPARE(other.read().toFlags(), (Flags{"Flag1", "Flag2"}));

        root = std::move(other);
        QCOMPARE(root.read().toFlags(), (Flags{"Flag1", "Flag2"}));

        Root another = std::move(root);
        QCOMPARE(another.read().toFlags(), (Flags{"Flag1", "Flag2"}));
    }

    void pathAccess()
    {
        Root root;

        root["/A/B/C"] = 1.0;

        QCOMPARE(root.read().getType(), Type::Hash);

        QCOMPARE(static_cast<const Root &>(root)["A/B/C"].toDouble(), 1.0);
    }
};

QTEST_APPLESS_MAIN(TestRoot)

#include "root.moc"
