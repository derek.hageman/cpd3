/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_METADATA_HXX
#define CPD3DATACOREVARIANT_METADATA_HXX

#include "core/first.hxx"


#include <string>
#include <memory>
#include <unordered_map>

#include "node.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

namespace Container {
class ReadMetadata;

class WriteMetadataReal;

class WriteMetadataInteger;

class WriteMetadataBoolean;

class WriteMetadataString;

class WriteMetadataBytes;

class WriteMetadataFlags;

class WriteMetadataArray;

class WriteMetadataMatrix;

class WriteMetadataHash;

class WriteMetadataKeyframe;

class ReadMetadataSingleFlag;

class WriteMetadataSingleFlag;

class ReadMetadataHashChild;

class WriteMetadataHashChild;
}

class NodeMetadataFlags;

class NodeMetadataHash;

/**
 * The base class for metadata nodes
 */
class CPD3DATACORE_EXPORT NodeMetadataBase : public Node {
public:
    using Children = std::unordered_map<PathElement::MetadataIndex, std::shared_ptr<Node>>;
private:
    Children children;

    friend class NodeMetadataFlags;

    friend class NodeMetadataHash;

    friend class Container::ReadMetadata;

    friend class Container::WriteMetadataReal;

    friend class Container::WriteMetadataInteger;

    friend class Container::WriteMetadataBoolean;

    friend class Container::WriteMetadataString;

    friend class Container::WriteMetadataBytes;

    friend class Container::WriteMetadataFlags;

    friend class Container::WriteMetadataArray;

    friend class Container::WriteMetadataMatrix;

    friend class Container::WriteMetadataHash;

    friend class Container::WriteMetadataKeyframe;

public:
    virtual ~NodeMetadataBase();

    std::shared_ptr<Node> read(const PathElement &path) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &path) override;

    bool bypassFirstOverlay(const Node &over) const override;

    bool bypassSecondOverlay() const override;

    bool isOverlayOpaque() const override;

    static void overlayFirst(NodeMetadataBase &under, const NodeMetadataBase &over);

    static void overlaySecond(NodeMetadataBase &under,
                              const NodeMetadataBase &over,
                              const TopLevel &origin,
                              const Path &path = Path(),
                              std::uint_fast16_t depth = 0);

    void clear() override;

    bool identicalTo(const Node &other) const override;

protected:
    NodeMetadataBase();

    NodeMetadataBase(const NodeMetadataBase &other);

    explicit NodeMetadataBase(Children &&children);

    explicit NodeMetadataBase(QDataStream &stream);

    void serializeInternal(QDataStream &stream) const;

    virtual bool pathCompatible(PathElement::Type type) const = 0;

    virtual PathElement::Type pathType() const = 0;

    virtual bool typeCompatible(Node::Type type, bool allowConstant = true) const = 0;

    std::string describeInternal() const;

    std::size_t hashInternal() const;
};

/**
 * A node containing metadata about a real number.
 */
class CPD3DATACORE_EXPORT NodeMetadataReal : public NodeMetadataBase {
public:
    virtual ~NodeMetadataReal();

    NodeMetadataReal();

    NodeMetadataReal(const NodeMetadataReal &other);

    explicit NodeMetadataReal(Children &&children);

    explicit NodeMetadataReal(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    bool compatible(WriteGoal goal) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::string describe() const override;

    std::size_t hash() const override;

protected:
    bool pathCompatible(PathElement::Type type) const override;

    PathElement::Type pathType() const override;

    bool typeCompatible(Node::Type type, bool allowConstant = true) const override;
};

/**
 * A real metadata node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeMetadataRealConstant : public NodeMetadataReal {
public:
    virtual ~NodeMetadataRealConstant();

    NodeMetadataRealConstant();

    explicit NodeMetadataRealConstant(const NodeMetadataReal &other);

    explicit NodeMetadataRealConstant(Children &&children);

    explicit NodeMetadataRealConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

/**
 * A node containing metadata about a integer number.
 */
class CPD3DATACORE_EXPORT NodeMetadataInteger : public NodeMetadataBase {
public:
    virtual ~NodeMetadataInteger();

    NodeMetadataInteger();

    NodeMetadataInteger(const NodeMetadataInteger &other);

    explicit NodeMetadataInteger(Children &&children);

    explicit NodeMetadataInteger(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    bool compatible(WriteGoal goal) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::string describe() const override;

    std::size_t hash() const override;

protected:
    bool pathCompatible(PathElement::Type type) const override;

    PathElement::Type pathType() const override;

    bool typeCompatible(Node::Type type, bool allowConstant = true) const override;
};

/**
 * A integer metadata node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeMetadataIntegerConstant : public NodeMetadataInteger {
public:
    virtual ~NodeMetadataIntegerConstant();

    NodeMetadataIntegerConstant();

    explicit NodeMetadataIntegerConstant(const NodeMetadataInteger &other);

    explicit NodeMetadataIntegerConstant(Children &&children);

    explicit NodeMetadataIntegerConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

/**
 * A node containing metadata about a boolean value.
 */
class CPD3DATACORE_EXPORT NodeMetadataBoolean : public NodeMetadataBase {
public:
    virtual ~NodeMetadataBoolean();

    NodeMetadataBoolean();

    NodeMetadataBoolean(const NodeMetadataBoolean &other);

    explicit NodeMetadataBoolean(Children &&children);

    explicit NodeMetadataBoolean(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    bool compatible(WriteGoal goal) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::string describe() const override;

    std::size_t hash() const override;

protected:
    bool pathCompatible(PathElement::Type type) const override;

    PathElement::Type pathType() const override;

    bool typeCompatible(Node::Type type, bool allowConstant = true) const override;
};

/**
 * A boolean metadata node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeMetadataBooleanConstant : public NodeMetadataBoolean {
public:
    virtual ~NodeMetadataBooleanConstant();

    NodeMetadataBooleanConstant();

    explicit NodeMetadataBooleanConstant(const NodeMetadataBoolean &other);

    explicit NodeMetadataBooleanConstant(Children &&children);

    explicit NodeMetadataBooleanConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

/**
 * A node containing metadata about a string.
 */
class CPD3DATACORE_EXPORT NodeMetadataString : public NodeMetadataBase {
public:
    virtual ~NodeMetadataString();

    NodeMetadataString();

    NodeMetadataString(const NodeMetadataString &other);

    explicit NodeMetadataString(Children &&children);

    explicit NodeMetadataString(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    bool compatible(WriteGoal goal) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::string describe() const override;

    std::size_t hash() const override;

protected:
    bool pathCompatible(PathElement::Type type) const override;

    PathElement::Type pathType() const override;

    bool typeCompatible(Node::Type type, bool allowConstant = true) const override;
};

/**
 * A string metadata node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeMetadataStringConstant : public NodeMetadataString {
public:
    virtual ~NodeMetadataStringConstant();

    NodeMetadataStringConstant();

    explicit NodeMetadataStringConstant(const NodeMetadataString &other);

    explicit NodeMetadataStringConstant(Children &&children);

    explicit NodeMetadataStringConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

/**
 * A node containing metadata about binary data.
 */
class CPD3DATACORE_EXPORT NodeMetadataBytes : public NodeMetadataBase {
public:
    virtual ~NodeMetadataBytes();

    NodeMetadataBytes();

    NodeMetadataBytes(const NodeMetadataBytes &other);

    explicit NodeMetadataBytes(Children &&children);

    explicit NodeMetadataBytes(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    bool compatible(WriteGoal goal) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::string describe() const override;

    std::size_t hash() const override;

protected:
    bool pathCompatible(PathElement::Type type) const override;

    PathElement::Type pathType() const override;

    bool typeCompatible(Node::Type type, bool allowConstant = true) const override;
};

/**
 * A bytes metadata node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeMetadataBytesConstant : public NodeMetadataBytes {
public:
    virtual ~NodeMetadataBytesConstant();

    NodeMetadataBytesConstant();

    explicit NodeMetadataBytesConstant(const NodeMetadataBytes &other);

    explicit NodeMetadataBytesConstant(Children &&children);

    explicit NodeMetadataBytesConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

/**
 * A node containing metadata about flags.
 */
class CPD3DATACORE_EXPORT NodeMetadataFlags : public NodeMetadataBase {
public:
    using Flags = std::unordered_map<PathElement::MetadataSingleFlagIndex, std::shared_ptr<Node>>;
private:
    Flags flags;

    friend class Container::ReadMetadataSingleFlag;

    friend class Container::WriteMetadataSingleFlag;

public:
    virtual ~NodeMetadataFlags();

    NodeMetadataFlags();

    NodeMetadataFlags(const NodeMetadataFlags &other);

    NodeMetadataFlags(Children &&children, Flags &&flags);

    explicit NodeMetadataFlags(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    bool compatible(WriteGoal goal) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::string describe() const override;

    std::size_t hash() const override;

    std::shared_ptr<Node> read(const PathElement &path) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &path) override;

    bool bypassFirstOverlay(const Node &over) const override;

    bool bypassSecondOverlay() const override;

    static void overlayFirst(NodeMetadataFlags &under, const NodeMetadataFlags &over);

    static void overlaySecond(NodeMetadataFlags &under,
                              const NodeMetadataFlags &over,
                              const TopLevel &origin,
                              const Path &path = Path(),
                              std::uint_fast16_t depth = 0);

    void clear() override;

    bool identicalTo(const Node &other) const override;

protected:
    bool pathCompatible(PathElement::Type type) const override;

    PathElement::Type pathType() const override;

    bool typeCompatible(Node::Type type, bool allowConstant = true) const override;
};

/**
 * A flags metadata node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeMetadataFlagsConstant : public NodeMetadataFlags {
public:
    virtual ~NodeMetadataFlagsConstant();

    NodeMetadataFlagsConstant();

    explicit NodeMetadataFlagsConstant(const NodeMetadataFlags &other);

    NodeMetadataFlagsConstant(Children &&children, NodeMetadataFlags::Flags &&flags);

    explicit NodeMetadataFlagsConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

/**
 * A node containing metadata about an array.
 */
class CPD3DATACORE_EXPORT NodeMetadataArray : public NodeMetadataBase {
public:
    virtual ~NodeMetadataArray();

    NodeMetadataArray();

    NodeMetadataArray(const NodeMetadataArray &other);

    explicit NodeMetadataArray(Children &&children);

    explicit NodeMetadataArray(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    bool compatible(WriteGoal goal) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::string describe() const override;

    std::size_t hash() const override;

protected:
    bool pathCompatible(PathElement::Type type) const override;

    PathElement::Type pathType() const override;

    bool typeCompatible(Node::Type type, bool allowConstant = true) const override;
};

/**
 * An array metadata node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeMetadataArrayConstant : public NodeMetadataArray {
public:
    virtual ~NodeMetadataArrayConstant();

    NodeMetadataArrayConstant();

    explicit NodeMetadataArrayConstant(const NodeMetadataArray &other);

    explicit NodeMetadataArrayConstant(Children &&children);

    explicit NodeMetadataArrayConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

/**
 * A node containing metadata about a matrix.
 */
class CPD3DATACORE_EXPORT NodeMetadataMatrix : public NodeMetadataBase {
public:
    virtual ~NodeMetadataMatrix();

    NodeMetadataMatrix();

    NodeMetadataMatrix(const NodeMetadataMatrix &other);

    explicit NodeMetadataMatrix(Children &&children);

    explicit NodeMetadataMatrix(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    bool compatible(WriteGoal goal) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::string describe() const override;

    std::size_t hash() const override;

protected:
    bool pathCompatible(PathElement::Type type) const override;

    PathElement::Type pathType() const override;

    bool typeCompatible(Node::Type type, bool allowConstant = true) const override;
};

/**
 * A matrix metadata node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeMetadataMatrixConstant : public NodeMetadataMatrix {
public:
    virtual ~NodeMetadataMatrixConstant();

    NodeMetadataMatrixConstant();

    explicit NodeMetadataMatrixConstant(const NodeMetadataMatrix &other);

    explicit NodeMetadataMatrixConstant(Children &&children);

    explicit NodeMetadataMatrixConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

/**
 * A node containing metadata about a hash map.
 */
class CPD3DATACORE_EXPORT NodeMetadataHash : public NodeMetadataBase {
public:
    using Keys = std::unordered_map<PathElement::MetadataHashChildIndex, std::shared_ptr<Node>>;
private:
    Keys keys;

    friend class Container::ReadMetadataHashChild;

    friend class Container::WriteMetadataHashChild;

public:
    virtual ~NodeMetadataHash();

    NodeMetadataHash();

    NodeMetadataHash(const NodeMetadataHash &other);

    NodeMetadataHash(Children &&children, Keys &&keys);

    explicit NodeMetadataHash(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    bool compatible(WriteGoal goal) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::string describe() const override;

    std::size_t hash() const override;

    std::shared_ptr<Node> read(const PathElement &path) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &path) override;

    bool bypassFirstOverlay(const Node &over) const override;

    bool bypassSecondOverlay() const override;

    static void overlayFirst(NodeMetadataHash &under, const NodeMetadataHash &over);

    static void overlaySecond(NodeMetadataHash &under,
                              const NodeMetadataHash &over,
                              const TopLevel &origin,
                              const Path &path = Path(),
                              std::uint_fast16_t depth = 0);

    void clear() override;

    bool identicalTo(const Node &other) const override;

protected:
    bool pathCompatible(PathElement::Type type) const override;

    PathElement::Type pathType() const override;

    bool typeCompatible(Node::Type type, bool allowConstant = true) const override;
};

/**
 * A hash metadata node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeMetadataHashConstant : public NodeMetadataHash {
public:
    virtual ~NodeMetadataHashConstant();

    NodeMetadataHashConstant();

    explicit NodeMetadataHashConstant(const NodeMetadataHash &other);

    NodeMetadataHashConstant(Children &&children, NodeMetadataHash::Keys &&keys);

    explicit NodeMetadataHashConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

/**
 * A node containing metadata about a keyframe map.
 */
class CPD3DATACORE_EXPORT NodeMetadataKeyframe : public NodeMetadataBase {
public:
    virtual ~NodeMetadataKeyframe();

    NodeMetadataKeyframe();

    NodeMetadataKeyframe(const NodeMetadataKeyframe &other);

    explicit NodeMetadataKeyframe(Children &&children);

    explicit NodeMetadataKeyframe(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    bool compatible(WriteGoal goal) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::string describe() const override;

    std::size_t hash() const override;

protected:
    bool pathCompatible(PathElement::Type type) const override;

    PathElement::Type pathType() const override;

    bool typeCompatible(Node::Type type, bool allowConstant = true) const override;
};

/**
 * A keyframe metadata node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeMetadataKeyframeConstant : public NodeMetadataKeyframe {
public:
    virtual ~NodeMetadataKeyframeConstant();

    NodeMetadataKeyframeConstant();

    explicit NodeMetadataKeyframeConstant(const NodeMetadataKeyframe &other);

    explicit NodeMetadataKeyframeConstant(Children &&children);

    explicit NodeMetadataKeyframeConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

}
}
}

#endif //CPD3DATACOREVARIANT_METADATA_HXX
