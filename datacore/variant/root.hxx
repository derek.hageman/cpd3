/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_ROOT_HXX
#define CPD3DATACOREVARIANT_ROOT_HXX

#include "core/first.hxx"

#include <memory>
#include <cstdint>
#include <type_traits>
#include <iterator>

#include <QDataStream>
#include <QDebug>

#include "datacore/datacore.hxx"
#include "core/qtcompat.hxx"
#include "common.hxx"
#include "handle.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

class TopLevel;

class Root;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Root &root);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, Root &root);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const Root &root);

/**
 * The top level root storage of variant values.  This is the base
 * root of the tree and so copying it results in a deep copy of
 * the entire value tree.  However, this copy invalidates any
 * existing access handles (they remain connected to the original
 * data).  To preserve the handle contents, use a set operation
 * on a write handle.
 */
class CPD3DATACORE_EXPORT Root final {
    std::shared_ptr<TopLevel> top;

    friend class Read;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Root &root);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, Root &root);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const Root &root);

    void overlayStep(const Root &over);

    static void overlayUnroll(Root &output, const Root &first);

    template<typename...Args>
    static void overlayUnroll(Root &output, const Root &first, Args &&... args)
    {
        overlayUnroll(output, first);
        return overlayUnroll(output, std::forward<Args>(args)...);
    }

    explicit Root(QDataStream &stream);

public:
    Root();

    ~Root();

    Root(const Root &other);

    Root &operator=(const Root &other);

    Root(Root &&) noexcept;

    Root &operator=(Root &&) noexcept;

    explicit Root(const Read &from);

    explicit Root(Type type);

    explicit Root(double value);

    explicit Root(std::int_fast64_t value);

    template<class Integer, class = typename std::enable_if<
            ((std::is_same<Integer, int>::value || std::is_same<Integer, qint64>::value) &&
                    !std::is_same<std::int_fast64_t, Integer>::value)>::type>
    inline explicit Root(Integer value) : Root(static_cast<std::int_fast64_t>(value))
    { }

    explicit Root(bool value);

    explicit Root(const std::string &value);

    explicit Root(std::string &&value);

    explicit Root(const char *value);

    explicit Root(const QString &value);

    explicit Root(const Bytes &value);

    explicit Root(Bytes &&value);

    explicit Root(const Flags &value);

    explicit Root(Flags &&value);

    /**
     * Deserialize a root from a data stream.
     *
     * @param stream    the input data stream
     * @return          the deserialized root
     */
    static Root deserialize(QDataStream &stream);

    /**
     * Swap two variant roots
     *
     * @param a     the first variant root
     * @param b     the second variant root
     */
    static void swap(Root &a, Root &b) noexcept;


    /**
     * Get a read handle at the root of the variant.
     *
     * @return      a read handle
     */
    inline Read read() const
    { return Read(*this); }

    /**
     * Get a write handle at the root of the variant.
     *
     * @return      a write handle
     */
    inline Write write()
    { return Write(*this); }


    /**
     * Get a read handle in the root at a specified
     * path.
     *
     * @param path  the path to read at
     * @return      a read handle
     */
    Read operator[](const std::string &path) const;

    /**
     * Get a read handle in the root at a specified
     * path.
     *
     * @param path  the path to read at
     * @return      a read handle
     */
    Read operator[](std::string &&path) const;

    /**
     * Get a read handle in the root at a specified
     * path.
     *
     * @param path  the path to read at
     * @return      a read handle
     */
    inline Read operator[](const char *path) const
    { return (*this)[std::string(path)]; }

    /**
     * Get a read handle in the root at a specified
     * path.
     *
     * @param path  the path to read at
     * @return      a read handle
     */
    inline Read operator[](const QString &path) const
    { return (*this)[path.toStdString()]; }

    /**
     * Get a write handle in the root at a specified
     * path.
     *
     * @param path  the path to write at
     * @return      a write handle
     */
    Write operator[](const std::string &path);

    /**
     * Get a write handle in the root at a specified
     * path.
     *
     * @param path  the path to write at
     * @return      a write handle
     */
    Write operator[](std::string &&path);

    /**
     * Get a write handle in the root at a specified
     * path.
     *
     * @param path  the path to write at
     * @return      a write handle
     */
    inline Write operator[](const char *path)
    { return (*this)[std::string(path)]; }

    /**
     * Get a write handle in the root at a specified
     * path.
     *
     * @param path  the path to write at
     * @return      a write handle
     */
    inline Write operator[](const QString &path)
    { return (*this)[path.toStdString()]; }


    /**
     * Test if the variant is opaque when overlaying.
     * This means that it would replace the one below
     * it entirely.
     *
     * @return      true if the variant is opaque
     */
    bool isOverlayOpaque() const;

    /**
     * Test if the variant is opaque when overlaying.
     * This means that it would replace the one below
     * it entirely.
     *
     * @param base  the base point to test from
     * @return      true if the variant is opaque
     */
    static bool isOverlayOpaque(const Read &base);

    /**
     * Create a new root constructed from an overlay of
     * a sequence of other roots.
     *
     * @tparam Iterator the sequential iterator type
     * @param begin     the begin iterator
     * @param end       the end iterator
     * @return          a new combined root
     */
    template<typename Iterator>
    static Root overlay(typename std::enable_if<std::is_base_of<std::input_iterator_tag,
                                                                typename std::iterator_traits<
                                                                        Iterator>::iterator_category>::value,
                                                Iterator>::type begin, Iterator end)
    {
        Root result;
        for (; begin != end; ++begin) {
            result.overlayStep(*begin);
        }
        return std::move(result);
    }

    /**
     * Create a new root constructed from an overlay
     * of all the roots in a list.
     *
     * @param list          the list of roots
     * @return              a new combined root
     */
    template<typename...Args>
    static Root overlay(Args &&... args)
    {
        Root result;
        overlayUnroll(result, std::forward<Args>(args)...);
        return std::move(result);
    }
};

CPD3DATACORE_EXPORT inline void swap(Root &lhs, Root &rhs) noexcept
{ Root::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(Root &lhs, Root &rhs) noexcept
{ Root::swap(lhs, rhs); }

}
}
}

#ifdef CPD3_QTEST
namespace CPD3 {
namespace Data {
namespace Variant {

inline char *toString(const CPD3::Data::Variant::Root &root)
{
    QByteArray ba = "Variant::Root(";
    ba += root.read().describe().data();
    ba += ")";
    return ::qstrdup(ba.data());
}

}
}
}
#endif

Q_DECLARE_METATYPE(CPD3::Data::Variant::Root);

#endif //CPD3DATACOREVARIANT_ROOT_HXX
