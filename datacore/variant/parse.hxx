/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_PARSE_HXX
#define CPD3DATACOREVARIANT_PARSE_HXX

#include "core/first.hxx"

#include <functional>

#include <QString>
#include <QStringList>

#include "datacore/datacore.hxx"
#include "handle.hxx"
#include "root.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {
namespace Parse {

/**
 * Convert a value into the input string representation of it.
 *
 * @param value     the value
 * @param metadata  the associated metadata
 * @return          the input string representation of the value
 */
CPD3DATACORE_EXPORT QString toInputString(const Read &value, const Read &metadata);

/**
 * Convert a value into the input string representation of it.
 *
 * @param value     the value
 * @return          the input string representation of the value
 */
CPD3DATACORE_EXPORT QString toInputString(const Read &value);

/**
 * Set a value based on an input string.
 *
 * @param value         the output value
 * @param input         the input string
 * @param metadata      the metadata context
 * @param reference     the reference existing value
 * @param acceptAmbiguous true to set even in ambiguous cases
 * @return              true if the input as accepted and the value set
 */
CPD3DATACORE_EXPORT bool fromInputString(Write &value,
                                         const QString &input,
                                         const Read &metadata,
                                         const Read &reference = Read::empty(),
                                         bool acceptAmbiguous = true);

CPD3DATACORE_EXPORT bool fromInputString(Write &&value,
                                         const QString &input,
                                         const Read &metadata,
                                         const Read &reference = Read::empty(),
                                         bool acceptAmbiguous = true);

/**
 * Set a value based on an input string.
 *
 * @param value         the input and output value
 * @param input         the input string
 * @param acceptAmbiguous true to set even in ambiguous cases
 * @return              true if the input as accepted and the value set
 */
CPD3DATACORE_EXPORT bool fromInputString(Write &value,
                                         const QString &input,
                                         bool acceptAmbiguous = true);

CPD3DATACORE_EXPORT bool fromInputString(Write &&value,
                                         const QString &input,
                                         bool acceptAmbiguous = true);


/**
 * Perform metadata merge evaluation.
 *
 * @param metadata  the metadata
 * @param data      the data context
 * @return          the merged metadata
 */
CPD3DATACORE_EXPORT Root evaluateMetadata(const Read &metadata, const Read &data);

/**
 * Perform metadata merge evaluation.
 *
 * @param metadata  the metadata
 * @return          the merged metadata
 */
CPD3DATACORE_EXPORT Root evaluateMetadata(const Read &metadata);

/**
 * Perform metadata merge evaluation for the whole tree to the location of the
 * metadata.
 *
 * @param metadata  the metadata location
 * @param data      the data context
 * @return          the merged metadata
 */
CPD3DATACORE_EXPORT Root evaluateMetadataTree(const Read &metadata, const Read &data);

/**
 * Perform metadata merge evaluation for the whole tree to the location of the
 * metadata.
 *
 * @param metadata  the metadata location
 * @return          the merged metadata
 */
CPD3DATACORE_EXPORT Root evaluateMetadataTree(const Read &metadata);

/**
 * A single node in a set of paths and values to be parsed into a combined
 * value.
 */
class CPD3DATACORE_EXPORT InputNode {
    Path path;

    static Root parse(std::vector<std::reference_wrapper<InputNode>> &&nodes,
                      const Read &metadata = Read::empty(),
                      const Read &reference = Read::empty());

public:
    InputNode() = delete;

    /**
     * Create the node.
     *
     * @param path  the location of the node in the output hierarchy
     */
    explicit InputNode(const Path &path);

    /**
     * Create the node.
     *
     * @param path  the location of the node in the output hierarchy
     */
    explicit InputNode(Path &&path);

    InputNode(const InputNode &);

    InputNode &operator=(const InputNode &);

    InputNode(InputNode &&);

    InputNode &operator=(InputNode &&);

    virtual ~InputNode();

    /**
     * Get the location of the node in the hierarchy.
     *
     * @return  the node location
     */
    inline const Path &getPath() const
    { return path; }

    /**
     * Attempt a write based on the currently available context.
     *
     * @param value     the target value
     * @param metadata  the metadata context
     * @param reference the reference context
     * @return          true if the write succeeded unambiguously (no further attempts required)
     */
    virtual bool attemptWrite(Write &value, const Read &metadata, const Read &reference) = 0;

    /**
     * Perform a fallback write, setting the value regardless of context.  No further write
     * attempts will be made after a call to this.
     *
     * @param value     the target value
     * @param metadata  the metadata context
     * @param reference the reference context
     */
    virtual void fallbackWrite(Write &value, const Read &metadata, const Read &reference) = 0;

    /**
     * Merge a set of nodes into a final output value, from pointers to the nodes.
     *
     * @tparam InputIterator    the input iterator type, dereferencing to pointers to InputNode
     * @param begin             the start to merge
     * @param end               the end to merge
     * @param metadata          the root metadata context
     * @param reference         the root reference value context
     * @return                  the output value
     */
    template<typename InputIterator>
    static Root fromPointers(InputIterator begin,
                             InputIterator end,
                             const Read &metadata = Read::empty(),
                             const Read &reference = Read::empty())
    {
        std::vector<std::reference_wrapper<InputNode>> nodes;
        for (; begin != end; ++begin) {
            nodes.emplace_back(*(*begin));
        }
        return parse(std::move(nodes), metadata, reference);
    }

    /**
     * Merge a set of nodes into a final output value, from references to the nodes.
     *
     * @tparam InputIterator    the input iterator type, dereferencing to references to InputNode
     * @param begin             the start to merge
     * @param end               the end to merge
     * @param metadata          the root metadata context
     * @param reference         the root reference value context
     * @return                  the output value
     */
    template<typename InputIterator>
    static Root fromReferences(InputIterator begin,
                               InputIterator end,
                               const Read &metadata = Read::empty(),
                               const Read &reference = Read::empty())
    {
        std::vector<std::reference_wrapper<InputNode>> nodes;
        for (; begin != end; ++begin) {
            nodes.emplace_back(*begin);
        }
        return parse(std::move(nodes), metadata, reference);
    }
};

/**
 * A single with a string value to be evaluated with fromInputString(Write &value, const QString &,
 * const Read &, const Read &, bool) when parsing.
 */
class CPD3DATACORE_EXPORT InputNodeString : public InputNode {
    QString value;
public:
    /**
     * Create the node.
     *
     * @param path  the location of the node in the output hierarchy
     * @param value the value to be parsed
     */
    InputNodeString(const Path &path, const QString &value);

    /**
     * Create the node.
     *
     * @param path  the location of the node in the output hierarchy
     * @param value the value to be parsed
     */
    InputNodeString(Path &&path, const QString &value);

    InputNodeString(const InputNodeString &);

    InputNodeString &operator=(const InputNodeString &);

    InputNodeString(InputNodeString &&);

    InputNodeString &operator=(InputNodeString &&);

    virtual ~InputNodeString();

    bool attemptWrite(Write &value, const Read &metadata, const Read &reference) override;

    void fallbackWrite(Write &value, const Read &metadata, const Read &reference) override;
};

}
}
}
}

#endif //CPD3DATACOREVARIANT_PARSE_HXX
