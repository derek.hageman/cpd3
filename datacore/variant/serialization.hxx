/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_SERIALIZATION_HXX
#define CPD3DATACOREVARIANT_SERIALIZATION_HXX

#include "core/first.hxx"

#include <memory>
#include <QDataStream>
#include <QDebug>
#include <QLoggingCategory>

#include "datacore/datacore.hxx"
#include "node.hxx"
#include "primitive.hxx"
#include "strings.hxx"
#include "hash.hxx"
#include "array.hxx"
#include "matrix.hxx"
#include "keyframe.hxx"
#include "overlay.hxx"
#include "metadata.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_datacore_variant_serialization)

namespace CPD3 {
namespace Data {
namespace Variant {

class Read;

namespace Serialization {

/**
 * The on disk type codes used for serialization.
 */
enum ID {
    ID_Empty = 0,
    ID_Real = 1,
    ID_Integer = 2,
    ID_Boolean = 3,
    ID_String_v1 = 4,
    ID_Bytes = 5,
    ID_Flags_v1 = 6,
    ID_Array_v1 = 7,
    ID_Matrix_v1 = 8,
    ID_Hash_v1 = 9,
    ID_Keyframe_v1 = 10,

    ID_MetadataReal_v1 = 11,
    ID_MetadataInteger_v1 = 12,
    ID_MetadataBoolean_v1 = 13,
    ID_MetadataString_v1 = 14,
    ID_MetadataBytes_v1 = 15,
    ID_MetadataFlags_v1 = 16,
    ID_MetadataArray_v1 = 17,
    ID_MetadataMatrix_v1 = 18,
    ID_MetadataHash_v1 = 19,
    ID_MetadataKeyframe_v1 = 20,
    ID_Overlay_v1 = 21,


    ID_String_v2,
    ID_LocalizeString_v2,
    ID_Flags_v2,

    ID_Hash_v2,
    ID_Array_v2,
    ID_Matrix_v2,
    ID_Keyframe_v2,

    ID_MetadataReal_v2,
    ID_MetadataInteger_v2,
    ID_MetadataBoolean_v2,
    ID_MetadataString_v2,
    ID_MetadataBytes_v2,
    ID_MetadataFlags_v2,
    ID_MetadataArray_v2,
    ID_MetadataMatrix_v2,
    ID_MetadataHash_v2,
    ID_MetadataKeyframe_v2,

    ID_Overlay_v2,


    ID_Reserved_Last,
};
static_assert(ID_Reserved_Last < 255, "too many ID codes defined");

/**
 * Construct a shared node by deserializing data.
 *
 * @param stream        the stream to read from
 * @return              a newly constructed node
 */
CPD3DATACORE_EXPORT std::shared_ptr<Node> construct(QDataStream &stream);

/**
 * Deserilize and construct nodes from a stream.  This calls
 * construct<NodeType>(args...) on the operator to perform
 * the actual construction.
 *
 * @tparam Operator     the operator type
 * @param stream        the stream to read from
 * @param op            the operator
 * @param useConstant   construct constant nodes
 * @return              the result of construction
 */
template<typename Operator>
auto deserialize(QDataStream &stream,
                 Operator op,
                 bool useConstant = true) -> decltype(op.template construct<NodeEmpty>())
{
    quint8 type = 0;
    stream >> type;
    switch (type) {
    case ID_Empty:
        return op.template construct<NodeEmpty>();

    case ID_Real:
        if (useConstant)
            return op.template construct<NodeRealConstant>(stream);
        else
            return op.template construct<NodeReal>(stream);
    case ID_Integer:
        if (useConstant)
            return op.template construct<NodeIntegerConstant>(stream);
        else
            return op.template construct<NodeInteger>(stream);
    case ID_Boolean:
        if (useConstant)
            return op.template construct<NodeBooleanConstant>(stream);
        else
            return op.template construct<NodeBoolean>(stream);
    case ID_String_v2:
        if (useConstant)
            return op.template construct<NodeStringConstant>(stream);
        else
            return op.template construct<NodeString>(stream);
    case ID_LocalizeString_v2:
        if (useConstant)
            return op.template construct<NodeLocalizedStringConstant>(stream);
        else
            return op.template construct<NodeLocalizedString>(stream);
    case ID_Bytes:
        if (useConstant)
            return op.template construct<NodeBytesConstant>(stream);
        else
            return op.template construct<NodeBytes>(stream);
    case ID_Flags_v2:
        if (useConstant)
            return op.template construct<NodeFlagsConstant>(stream);
        else
            return op.template construct<NodeFlags>(stream);

    case ID_Hash_v2:
        if (useConstant)
            return op.template construct<NodeHashConstant>(stream);
        else
            return op.template construct<NodeHash>(stream);
    case ID_Array_v2:
        if (useConstant)
            return op.template construct<NodeArrayConstant>(stream);
        else
            return op.template construct<NodeArray>(stream);
    case ID_Matrix_v2:
        if (useConstant)
            return op.template construct<NodeMatrixConstant>(stream);
        else
            return op.template construct<NodeMatrix>(stream);
    case ID_Keyframe_v2:
        if (useConstant)
            return op.template construct<NodeKeyframeConstant>(stream);
        else
            return op.template construct<NodeKeyframe>(stream);

    case ID_MetadataReal_v2:
        if (useConstant)
            return op.template construct<NodeMetadataRealConstant>(stream);
        else
            return op.template construct<NodeMetadataReal>(stream);
    case ID_MetadataInteger_v2:
        if (useConstant)
            return op.template construct<NodeMetadataIntegerConstant>(stream);
        else
            return op.template construct<NodeMetadataInteger>(stream);
    case ID_MetadataBoolean_v2:
        if (useConstant)
            return op.template construct<NodeMetadataBooleanConstant>(stream);
        else
            return op.template construct<NodeMetadataBoolean>(stream);
    case ID_MetadataString_v2:
        if (useConstant)
            return op.template construct<NodeMetadataStringConstant>(stream);
        else
            return op.template construct<NodeMetadataString>(stream);
    case ID_MetadataBytes_v2:
        if (useConstant)
            return op.template construct<NodeMetadataBytesConstant>(stream);
        else
            return op.template construct<NodeMetadataBytes>(stream);
    case ID_MetadataFlags_v2:
        if (useConstant)
            return op.template construct<NodeMetadataFlagsConstant>(stream);
        else
            return op.template construct<NodeMetadataFlags>(stream);
    case ID_MetadataArray_v2:
        if (useConstant)
            return op.template construct<NodeMetadataArrayConstant>(stream);
        else
            return op.template construct<NodeMetadataArray>(stream);
    case ID_MetadataMatrix_v2:
        if (useConstant)
            return op.template construct<NodeMetadataMatrixConstant>(stream);
        else
            return op.template construct<NodeMetadataMatrix>(stream);
    case ID_MetadataHash_v2:
        if (useConstant)
            return op.template construct<NodeMetadataHashConstant>(stream);
        else
            return op.template construct<NodeMetadataHash>(stream);
    case ID_MetadataKeyframe_v2:
        if (useConstant)
            return op.template construct<NodeMetadataKeyframeConstant>(stream);
        else
            return op.template construct<NodeMetadataKeyframe>(stream);

    case ID_Overlay_v2:
        if (useConstant)
            return op.template construct<NodeOverlayConstant>(stream);
        else
            return op.template construct<NodeOverlay>(stream);


    case ID_String_v1: {
        QString value;
        stream >> value;
        quint32 nLocalized = 0;
        stream >> nLocalized;
        if (nLocalized == 0) {
            if (useConstant)
                return op.template construct<NodeStringConstant>(value.toStdString());
            else
                return op.template construct<NodeString>(value.toStdString());
        }

        NodeLocalizedString::Localization localization;
        for (quint32 i = 0; i < nLocalized; i++) {
            QString localeName;
            QString localeValue;
            stream >> localeName >> localeValue;
            localization.emplace(localeName, localeValue);
        }
        if (useConstant)
            return op.template construct<NodeLocalizedStringConstant>(value.toStdString(),
                                                                      std::move(localization));
        else
            return op.template construct<NodeLocalizedString>(value.toStdString(),
                                                              std::move(localization));
    }
    case ID_Flags_v1: {
        quint32 n = 0;
        stream >> n;
        Flags flags;
        for (quint32 i = 0; i < n; i++) {
            QString flag;
            stream >> flag;
            flags.insert(flag.toStdString());
        }
        if (useConstant)
            return op.template construct<NodeFlagsConstant>(std::move(flags));
        else
            return op.template construct<NodeFlags>(std::move(flags));
    }

    case ID_Hash_v1: {
        quint32 n = 0;
        stream >> n;
        NodeHash::Children children;
        for (quint32 i = 0; i < n; i++) {
            QString key;
            stream >> key;
            children.emplace(key.toStdString(), construct(stream));
        }
        if (useConstant)
            return op.template construct<NodeHashConstant>(std::move(children));
        else
            return op.template construct<NodeHash>(std::move(children));
    }
    case ID_Array_v1: {
        quint32 n = 0;
        stream >> n;
        NodeArray::Children children;
        for (quint32 i = 0; i < n; i++) {
            children.emplace_back(construct(stream));
        }
        if (useConstant)
            return op.template construct<NodeArrayConstant>(std::move(children));
        else
            return op.template construct<NodeArray>(std::move(children));
    }
    case ID_Matrix_v1: {
        NodeArray::Children children;
        PathElement::MatrixIndex size;
        {
            quint32 n = 0;
            stream >> n;
            for (quint32 i = 0; i < n; i++) {
                children.emplace_back(construct(stream));
            }
        }
        {
            quint8 n = 0;
            stream >> n;
            for (quint8 i = 0; i < n; ++i) {
                quint32 v = 0;
                stream >> v;
                size.emplace_back(v);
            }
        }
        if (useConstant)
            return op.template construct<NodeMatrixConstant>(std::move(children), std::move(size));
        else
            return op.template construct<NodeMatrix>(std::move(children), std::move(size));
    }
    case ID_Keyframe_v1: {
        quint32 n = 0;
        stream >> n;
        NodeKeyframe::Children children;
        for (quint32 i = 0; i < n; i++) {
            double key = 0;
            stream >> key;
            children.emplace(key, construct(stream));
        }
        if (useConstant)
            return op.template construct<NodeKeyframeConstant>(std::move(children));
        else
            return op.template construct<NodeKeyframe>(std::move(children));
    }

    case ID_MetadataReal_v1: {
        quint32 n = 0;
        stream >> n;
        NodeMetadataReal::Children children;
        for (quint32 i = 0; i < n; i++) {
            QString key;
            stream >> key;
            children.emplace(key.toStdString(), construct(stream));
        }
        if (useConstant)
            return op.template construct<NodeMetadataRealConstant>(std::move(children));
        else
            return op.template construct<NodeMetadataReal>(std::move(children));
    }
    case ID_MetadataInteger_v1: {
        quint32 n = 0;
        stream >> n;
        NodeMetadataInteger::Children children;
        for (quint32 i = 0; i < n; i++) {
            QString key;
            stream >> key;
            children.emplace(key.toStdString(), construct(stream));
        }
        if (useConstant)
            return op.template construct<NodeMetadataIntegerConstant>(std::move(children));
        else
            return op.template construct<NodeMetadataInteger>(std::move(children));
    }
    case ID_MetadataBoolean_v1: {
        quint32 n = 0;
        stream >> n;
        NodeMetadataBoolean::Children children;
        for (quint32 i = 0; i < n; i++) {
            QString key;
            stream >> key;
            children.emplace(key.toStdString(), construct(stream));
        }
        if (useConstant)
            return op.template construct<NodeMetadataBooleanConstant>(std::move(children));
        else
            return op.template construct<NodeMetadataBoolean>(std::move(children));
    }
    case ID_MetadataString_v1: {
        quint32 n = 0;
        stream >> n;
        NodeMetadataString::Children children;
        for (quint32 i = 0; i < n; i++) {
            QString key;
            stream >> key;
            children.emplace(key.toStdString(), construct(stream));
        }
        if (useConstant)
            return op.template construct<NodeMetadataStringConstant>(std::move(children));
        else
            return op.template construct<NodeMetadataString>(std::move(children));
    }
    case ID_MetadataBytes_v1: {
        quint32 n = 0;
        stream >> n;
        NodeMetadataBytes::Children children;
        for (quint32 i = 0; i < n; i++) {
            QString key;
            stream >> key;
            children.emplace(key.toStdString(), construct(stream));
        }
        if (useConstant)
            return op.template construct<NodeMetadataBytesConstant>(std::move(children));
        else
            return op.template construct<NodeMetadataBytes>(std::move(children));
    }
    case ID_MetadataFlags_v1: {
        NodeMetadataFlags::Children children;
        NodeMetadataFlags::Flags flags;
        {
            quint32 n = 0;
            stream >> n;
            for (quint32 i = 0; i < n; i++) {
                QString key;
                stream >> key;
                children.emplace(key.toStdString(), construct(stream));
            }
        }
        {
            quint32 n = 0;
            stream >> n;
            for (quint32 i = 0; i < n; i++) {
                QString key;
                stream >> key;
                flags.emplace(key.toStdString(), construct(stream));
            }
        }
        if (useConstant)
            return op.template construct<NodeMetadataFlagsConstant>(std::move(children),
                                                                    std::move(flags));
        else
            return op.template construct<NodeMetadataFlags>(std::move(children), std::move(flags));
    }
    case ID_MetadataArray_v1: {
        quint32 n = 0;
        stream >> n;
        NodeMetadataArray::Children children;
        for (quint32 i = 0; i < n; i++) {
            QString key;
            stream >> key;
            children.emplace(key.toStdString(), construct(stream));
        }
        if (useConstant)
            return op.template construct<NodeMetadataArrayConstant>(std::move(children));
        else
            return op.template construct<NodeMetadataArray>(std::move(children));
    }
    case ID_MetadataMatrix_v1: {
        quint32 n = 0;
        stream >> n;
        NodeMetadataMatrix::Children children;
        for (quint32 i = 0; i < n; i++) {
            QString key;
            stream >> key;
            children.emplace(key.toStdString(), construct(stream));
        }
        if (useConstant)
            return op.template construct<NodeMetadataMatrixConstant>(std::move(children));
        else
            return op.template construct<NodeMetadataMatrix>(std::move(children));
    }
    case ID_MetadataHash_v1: {
        NodeMetadataHash::Children children;
        NodeMetadataHash::Keys keys;
        {
            quint32 n = 0;
            stream >> n;
            for (quint32 i = 0; i < n; i++) {
                QString key;
                stream >> key;
                children.emplace(key.toStdString(), construct(stream));
            }
        }
        {
            quint32 n = 0;
            stream >> n;
            for (quint32 i = 0; i < n; i++) {
                QString key;
                stream >> key;
                keys.emplace(key.toStdString(), construct(stream));
            }
        }
        if (useConstant)
            return op.template construct<NodeMetadataHashConstant>(std::move(children),
                                                                   std::move(keys));
        else
            return op.template construct<NodeMetadataHash>(std::move(children), std::move(keys));
    }
    case ID_MetadataKeyframe_v1: {
        quint32 n = 0;
        stream >> n;
        NodeMetadataKeyframe::Children children;
        for (quint32 i = 0; i < n; i++) {
            QString key;
            stream >> key;
            children.emplace(key.toStdString(), construct(stream));
        }
        if (useConstant)
            return op.template construct<NodeMetadataKeyframeConstant>(std::move(children));
        else
            return op.template construct<NodeMetadataKeyframe>(std::move(children));
    }

    case ID_Overlay_v1: {
        QString path;
        stream >> path;
        if (useConstant)
            return op.template construct<NodeOverlayConstant>(path.toStdString());
        else
            return op.template construct<NodeOverlay>(path.toStdString());
    }

    default:
        qCWarning(log_datacore_variant_serialization) << "Unrecognized serialization type" << type;
        stream.setStatus(QDataStream::ReadCorruptData);
        Q_ASSERT(false);
        return op.template construct<NodeEmpty>();
    }
    Q_ASSERT(false);
    return op.template construct<NodeEmpty>();
}


/**
 * Serialize a length in short form.
 *
 * @param stream    the target stream
 * @param length    the length to serialize
 */
CPD3DATACORE_EXPORT void serializeShortLength(QDataStream &stream, std::size_t length);

/**
 * Deserialize a length from short form.
 *
 * @param stream    the source stream
 * @return          the length
 */
CPD3DATACORE_EXPORT std::size_t deserializeShortLength(QDataStream &stream);

/**
 * Serialize a string in short form.
 *
 * @param stream    the target stream
 * @param str       the string to serialize
 */
CPD3DATACORE_EXPORT void serializeShortString(QDataStream &stream, const std::string &str);

/**
 * Deserialze a string from short form.
 *
 * @param stream    the source stream
 * @return          the string
 */
CPD3DATACORE_EXPORT std::string deserializeShortString(QDataStream &stream);


/**
 * Serialize a handle in legacy mode.  This is used when the format must
 * be compatible with the pre-variant Value types.
 *
 * @param stream    the target stream
 * @param value     the variant to serialize
 */
CPD3DATACORE_EXPORT void serializeLegacy(QDataStream &stream, const Read &value);

}
}
}
}

#endif //CPD3DATACOREVARIANT_SERIALIZATION_HXX
