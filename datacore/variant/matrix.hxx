/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_MATRIX_HXX
#define CPD3DATACOREVARIANT_MATRIX_HXX

#include "core/first.hxx"

#include <vector>
#include <memory>

#include "datacore/datacore.hxx"
#include "node.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

namespace Container {
class ReadMatrix;

class WriteMatrix;
}

/**
 * A node containing a matrix map of strings to other nodes.
 */
class CPD3DATACORE_EXPORT NodeMatrix : public Node {
public:
    using Children = std::vector<std::shared_ptr<Node>>;
private:
    Children children;
    PathElement::MatrixIndex size;

    friend class Container::ReadMatrix;

    friend class Container::WriteMatrix;

    bool indexInBounds(const PathElement::MatrixIndex &index) const;

    void maybeReshape(const PathElement::MatrixIndex &index);

public:
    virtual ~NodeMatrix();

    NodeMatrix();

    NodeMatrix(const NodeMatrix &other);

    explicit NodeMatrix(Children &&children, PathElement::MatrixIndex &&size);

    explicit NodeMatrix(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::shared_ptr<Node> read(const PathElement &path) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &path) override;

    bool bypassFirstOverlay(const Node &over) const override;

    bool bypassSecondOverlay() const override;

    bool isOverlayOpaque() const override;

    static void overlayFirst(NodeMatrix &under, const NodeMatrix &over);

    static void overlaySecond(NodeMatrix &under,
                              const NodeMatrix &over,
                              const TopLevel &origin,
                              const Path &path = Path(),
                              std::uint_fast16_t depth = 0);

    std::string describe() const override;

    void clear() override;

    bool identicalTo(const Node &other) const override;

    bool equalTo(const Node &other, const Node &metadata) const override;

    std::size_t hash() const override;

    static std::size_t toIndex(const PathElement::MatrixIndex &index,
                               const PathElement::MatrixIndex &size);

    static PathElement::MatrixIndex fromIndex(std::size_t index,
                                              const PathElement::MatrixIndex &size);

    std::size_t toIndex(const PathElement::MatrixIndex &index) const;

    PathElement::MatrixIndex fromIndex(std::size_t index) const;

    void reshape(const PathElement::MatrixIndex &size,
                 const std::shared_ptr<Node> &fill = NodeEmpty::instance);

    inline void setSize(const PathElement::MatrixIndex &size)
    { this->size = size; }

    void setSize(PathElement::MatrixIndex &&size)
    { this->size = std::move(size); }
};

/**
 * A matrix node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeMatrixConstant : public NodeMatrix {
public:
    virtual ~NodeMatrixConstant();

    NodeMatrixConstant();

    explicit NodeMatrixConstant(const NodeMatrix &other);

    explicit NodeMatrixConstant(Children &&children, PathElement::MatrixIndex &&size);

    explicit NodeMatrixConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

}
}
}

#endif //CPD3DATACOREVARIANT_MATRIX_HXX
