/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <sstream>
#include <QLoggingCategory>

#include "path.hxx"
#include "core/number.hxx"
#include "core/qtcompat.hxx"
#include "core/util.hxx"


Q_LOGGING_CATEGORY(log_datacore_variant_path, "cpd3.datacore.variant.path", QtWarningMsg)

namespace CPD3 {
namespace Data {
namespace Variant {


PathElement::PathElement() : type(Type::Empty)
{ }

void PathElement::destructData()
{
    static_assert(std::is_trivially_destructible<ArrayIndex>::value, "non trivial array index");
    static_assert(std::is_trivially_destructible<KeyframeIndex>::value,
                  "non trivial keyframe index");

    switch (type) {
    case Type::Empty:
        break;

    case Type::AnyString:
        data.destruct<StringIndex>();
        break;

    case Type::AnyMetadata:
        data.destruct<MetadataIndex>();
        break;

    case Type::Hash:
        data.destruct<HashIndex>();
        break;
    case Type::Array:
        break;
    case Type::Matrix:
        data.destruct<MatrixIndex>();
        break;
    case Type::Keyframe:
        break;

    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
        data.destruct<MetadataIndex>();
        break;

    case Type::MetadataHashChild:
        data.destruct<MetadataHashChildIndex>();
        break;
    case Type::MetadataSingleFlag:
        data.destruct<MetadataSingleFlagIndex>();
        break;
    }
}

PathElement::~PathElement()
{ destructData(); }

void PathElement::constructFrom(const PathElement &other)
{
    type = other.type;
    switch (other.type) {
    case Type::Empty:
        break;

    case Type::AnyString:
        data.construct<StringIndex>(other.ref<StringIndex>());
        break;
    case Type::AnyMetadata:
        data.construct<MetadataIndex>(other.ref<MetadataIndex>());
        break;

    case Type::Hash:
        data.construct<HashIndex>(other.ref<HashIndex>());
        break;
    case Type::Array:
        data.construct<ArrayIndex>(other.ref<ArrayIndex>());
        break;
    case Type::Matrix:
        data.construct<MatrixIndex>(other.ref<MatrixIndex>());
        break;
    case Type::Keyframe:
        data.construct<KeyframeIndex>(other.ref<KeyframeIndex>());
        break;

    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
        data.construct<MetadataIndex>(other.ref<MetadataIndex>());
        break;

    case Type::MetadataHashChild:
        data.construct<MetadataHashChildIndex>(other.ref<MetadataHashChildIndex>());
        break;
    case Type::MetadataSingleFlag:
        data.construct<MetadataSingleFlagIndex>(other.ref<MetadataSingleFlagIndex>());
        break;
    }
}

void PathElement::constructFrom(PathElement &&other)
{
    type = other.type;
    switch (other.type) {
    case Type::Empty:
        break;

    case Type::AnyString:
        data.construct<StringIndex>(std::move(other.data.ref<StringIndex>()));
        break;
    case Type::AnyMetadata:
        data.construct<MetadataIndex>(std::move(other.data.ref<MetadataIndex>()));
        break;

    case Type::Hash:
        data.construct<HashIndex>(std::move(other.data.ref<HashIndex>()));
        break;
    case Type::Array:
        data.construct<ArrayIndex>(std::move(other.data.ref<ArrayIndex>()));
        break;
    case Type::Matrix:
        data.construct<MatrixIndex>(std::move(other.data.ref<MatrixIndex>()));
        break;
    case Type::Keyframe:
        data.construct<KeyframeIndex>(std::move(other.data.ref<KeyframeIndex>()));
        break;

    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
        data.construct<MetadataIndex>(std::move(other.data.ref<MetadataIndex>()));
        break;

    case Type::MetadataHashChild:
        data.construct<MetadataHashChildIndex>(std::move(other.data.ref<MetadataHashChildIndex>()));
        break;
    case Type::MetadataSingleFlag:
        data.construct<MetadataSingleFlagIndex>(
                std::move(other.data.ref<MetadataSingleFlagIndex>()));
        break;
    }
}

PathElement::PathElement(const PathElement &other)
{ constructFrom(other); }

PathElement &PathElement::operator=(const PathElement &other)
{
    if (&other.data == &data)
        return *this;

    if (other.type == type) {
        switch (type) {
        case Type::Empty:
            break;

        case Type::AnyString:
            data.ref<StringIndex>() = other.data.ref<StringIndex>();
            break;
        case Type::AnyMetadata:
            data.ref<MetadataIndex>() = other.data.ref<MetadataIndex>();
            break;

        case Type::Hash:
            data.ref<HashIndex>() = other.data.ref<HashIndex>();
            break;
        case Type::Array:
            data.ref<ArrayIndex>() = other.data.ref<ArrayIndex>();
            break;
        case Type::Matrix:
            data.ref<MatrixIndex>() = other.data.ref<MatrixIndex>();
            break;
        case Type::Keyframe:
            data.ref<KeyframeIndex>() = other.data.ref<KeyframeIndex>();
            break;

        case Type::MetadataReal:
        case Type::MetadataInteger:
        case Type::MetadataBoolean:
        case Type::MetadataString:
        case Type::MetadataBytes:
        case Type::MetadataFlags:
        case Type::MetadataArray:
        case Type::MetadataMatrix:
        case Type::MetadataHash:
        case Type::MetadataKeyframe:
            data.ref<MetadataIndex>() = other.data.ref<MetadataIndex>();
            break;

        case Type::MetadataHashChild:
            data.ref<MetadataHashChildIndex>() = other.data.ref<MetadataHashChildIndex>();
            break;
        case Type::MetadataSingleFlag:
            data.ref<MetadataSingleFlagIndex>() = other.data.ref<MetadataSingleFlagIndex>();
            break;
        }
        return *this;
    }

    destructData();
    constructFrom(other);
    return *this;
}

PathElement::PathElement(PathElement &&other)
{ constructFrom(std::move(other)); }

PathElement &PathElement::operator=(PathElement &&other)
{
    if (&other.data == &data)
        return *this;

    if (other.type == type) {
        switch (other.type) {
        case Type::Empty:
            break;

        case Type::AnyString:
            data.ref<StringIndex>() = std::move(other.data.ref<StringIndex>());
            break;
        case Type::AnyMetadata:
            data.ref<MetadataIndex>() = std::move(other.data.ref<MetadataIndex>());
            break;

        case Type::Hash:
            data.ref<HashIndex>() = std::move(other.data.ref<HashIndex>());
            break;
        case Type::Array:
            data.ref<ArrayIndex>() = std::move(other.data.ref<ArrayIndex>());
            break;
        case Type::Matrix:
            data.ref<MatrixIndex>() = std::move(other.data.ref<MatrixIndex>());
            break;
        case Type::Keyframe:
            data.ref<KeyframeIndex>() = std::move(other.data.ref<KeyframeIndex>());
            break;

        case Type::MetadataReal:
        case Type::MetadataInteger:
        case Type::MetadataBoolean:
        case Type::MetadataString:
        case Type::MetadataBytes:
        case Type::MetadataFlags:
        case Type::MetadataArray:
        case Type::MetadataMatrix:
        case Type::MetadataHash:
        case Type::MetadataKeyframe:
            data.ref<MetadataIndex>() = std::move(other.data.ref<MetadataIndex>());
            break;

        case Type::MetadataHashChild:
            data.ref<MetadataHashChildIndex>() =
                    std::move(other.data.ref<MetadataHashChildIndex>());
            break;
        case Type::MetadataSingleFlag:
            data.ref<MetadataSingleFlagIndex>() =
                    std::move(other.data.ref<MetadataSingleFlagIndex>());
            break;
        }
        return *this;
    }

    destructData();
    constructFrom(std::move(other));
    return *this;
}

PathElement::PathElement(PathElement::Type type, const char *value) : type(type)
{
    switch (type) {
    case Type::Empty:
    case Type::Array:
    case Type::Matrix:
    case Type::Keyframe:
        Q_ASSERT(false);
        this->type = Type::Empty;
        break;

    case Type::AnyString:
        data.construct<StringIndex>(value);
        break;
    case Type::AnyMetadata:
        data.construct<MetadataIndex>(value);
        break;

    case Type::Hash:
        data.construct<HashIndex>(value);
        break;

    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
        data.construct<MetadataIndex>(value);
        break;

    case Type::MetadataHashChild:
        data.construct<MetadataHashChildIndex>(value);
        break;
    case Type::MetadataSingleFlag:
        data.construct<MetadataSingleFlagIndex>(value);
        break;
    }
}

PathElement::PathElement(PathElement::Type type, const std::string &value) : type(type)
{
    switch (type) {
    case Type::Empty:
    case Type::Array:
    case Type::Matrix:
    case Type::Keyframe:
        Q_ASSERT(false);
        this->type = Type::Empty;
        break;

    case Type::AnyString:
        data.construct<StringIndex>(value);
        break;
    case Type::AnyMetadata:
        data.construct<MetadataIndex>(value);
        break;

    case Type::Hash:
        data.construct<HashIndex>(value);
        break;

    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
        data.construct<MetadataIndex>(value);
        break;

    case Type::MetadataHashChild:
        data.construct<MetadataHashChildIndex>(value);
        break;
    case Type::MetadataSingleFlag:
        data.construct<MetadataSingleFlagIndex>(value);
        break;
    }
}

PathElement::PathElement(PathElement::Type type, std::string &&value) : type(type)
{
    switch (type) {
    case Type::Empty:
    case Type::Array:
    case Type::Matrix:
    case Type::Keyframe:
        Q_ASSERT(false);
        this->type = Type::Empty;
        break;

    case Type::AnyString:
        data.construct<StringIndex>(std::move(value));
        break;
    case Type::AnyMetadata:
        data.construct<MetadataIndex>(std::move(value));
        break;

    case Type::Hash:
        data.construct<HashIndex>(std::move(value));
        break;

    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
        data.construct<MetadataIndex>(std::move(value));
        break;

    case Type::MetadataHashChild:
        data.construct<MetadataHashChildIndex>(std::move(value));
        break;
    case Type::MetadataSingleFlag:
        data.construct<MetadataSingleFlagIndex>(std::move(value));
        break;
    }
}

PathElement::PathElement(PathElement::Type type, const QString &value) : type(type)
{
    switch (type) {
    case Type::Empty:
    case Type::Array:
    case Type::Matrix:
    case Type::Keyframe:
        Q_ASSERT(false);
        this->type = Type::Empty;
        break;

    case Type::AnyString:
        data.construct<StringIndex>(value.toStdString());
        break;
    case Type::AnyMetadata:
        data.construct<MetadataIndex>(value.toStdString());
        break;

    case Type::Hash:
        data.construct<HashIndex>(value.toStdString());
        break;

    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
        data.construct<MetadataIndex>(value.toStdString());
        break;

    case Type::MetadataHashChild:
        data.construct<MetadataHashChildIndex>(value.toStdString());
        break;
    case Type::MetadataSingleFlag:
        data.construct<MetadataSingleFlagIndex>(value.toStdString());
        break;
    }
}

PathElement::PathElement(PathElement::Type type, QString &&value) : PathElement(type, value)
{ }

PathElement::PathElement(PathElement::Type type, const PathElement::MatrixIndex &value) : type(type)
{
    switch (type) {
    case Type::Empty:
    case Type::AnyString:
    case Type::AnyMetadata:
    case Type::Hash:
    case Type::Array:
    case Type::Keyframe:
    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
    case Type::MetadataHashChild:
    case Type::MetadataSingleFlag:
        Q_ASSERT(false);
        this->type = Type::Empty;
        break;
    case Type::Matrix:
        data.construct<MatrixIndex>(value);
        break;
    }
}

PathElement::PathElement(PathElement::Type type, PathElement::MatrixIndex &&value) : type(type)
{
    switch (type) {
    case Type::Empty:
    case Type::AnyString:
    case Type::AnyMetadata:
    case Type::Hash:
    case Type::Array:
    case Type::Keyframe:
    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
    case Type::MetadataHashChild:
    case Type::MetadataSingleFlag:
        Q_ASSERT(false);
        this->type = Type::Empty;
        break;
    case Type::Matrix:
        data.construct<MatrixIndex>(std::move(value));
        break;
    }
}

PathElement::PathElement(PathElement::Type type, double value) : type(type)
{
    switch (type) {
    case Type::Empty:
    case Type::AnyString:
    case Type::AnyMetadata:
    case Type::Hash:
    case Type::Array:
    case Type::Matrix:
    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
    case Type::MetadataHashChild:
    case Type::MetadataSingleFlag:
        Q_ASSERT(false);
        this->type = Type::Empty;
        break;
    case Type::Keyframe:
        data.construct<KeyframeIndex>(value);
        break;
    }
}

PathElement::PathElement(PathElement::Type type, std::size_t value) : type(type)
{
    switch (type) {
    case Type::Empty:
    case Type::AnyString:
    case Type::AnyMetadata:
    case Type::Hash:
    case Type::Matrix:
    case Type::Keyframe:
    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
    case Type::MetadataHashChild:
    case Type::MetadataSingleFlag:
        Q_ASSERT(false);
        this->type = Type::Empty;
        break;
    case Type::Array:
        data.construct<ArrayIndex>(value);
        break;
    }
}


static std::string performPathEscape(std::string &&input, bool escape)
{
    if (input.empty())
        return "\\_";
    if (!escape)
        return std::move(input);

    Util::replace_all(input, "\\", "\\\\");

    switch (input[0]) {
    case '#':
    case '@':
    case '[':
    case '^':
    case '*':
    case '&':
    case '`':
        input.insert(0, 1, '\\');
        break;
    default:
        break;
    }

    Util::replace_all(input, "\n", "\\n");
    Util::replace_all(input, "\r", "\\r");
    Util::replace_all(input, "/", "\\/");
    Util::replace_all(input, ",", "\\,");
    return std::move(input);
}

std::string PathElement::toString(bool escape, bool explicitHash) const
{
    switch (type) {
    case Type::Empty:
        return "\\_";
    case Type::Hash:
        if (explicitHash)
            return std::string("&") + performPathEscape(std::string(ref<HashIndex>()), escape);
        return performPathEscape(std::string(ref<HashIndex>()), escape);
    case Type::AnyString:
        return performPathEscape(std::string(ref<StringIndex>()), escape);
    case Type::AnyMetadata:
        return std::string("^") + performPathEscape(std::string(ref<MetadataIndex>()), escape);
    case Type::Array:
        return std::string("#") + std::to_string(ref<ArrayIndex>());
    case Type::Matrix: {
        std::string result = "[";
        bool first = true;
        for (auto add : ref<MatrixIndex>()) {
            if (!first)
                result += ":";
            first = false;
            result += std::to_string(add);
        }
        result += "]";
        return result;
    }
    case Type::Keyframe: {
        std::ostringstream b;
        b << "@" << ref<KeyframeIndex>();
        return b.str();
    }
    case Type::MetadataReal:
        return std::string("*r") + performPathEscape(std::string(ref<MetadataIndex>()), escape);
    case Type::MetadataInteger:
        return std::string("*i") + performPathEscape(std::string(ref<MetadataIndex>()), escape);
    case Type::MetadataBoolean:
        return std::string("*b") + performPathEscape(std::string(ref<MetadataIndex>()), escape);
    case Type::MetadataString:
        return std::string("*s") + performPathEscape(std::string(ref<MetadataIndex>()), escape);
    case Type::MetadataBytes:
        return std::string("*n") + performPathEscape(std::string(ref<MetadataIndex>()), escape);
    case Type::MetadataFlags:
        return std::string("*f") + performPathEscape(std::string(ref<MetadataIndex>()), escape);
    case Type::MetadataArray:
        return std::string("*a") + performPathEscape(std::string(ref<MetadataIndex>()), escape);
    case Type::MetadataMatrix:
        return std::string("*t") + performPathEscape(std::string(ref<MetadataIndex>()), escape);
    case Type::MetadataHash:
        return std::string("*h") + performPathEscape(std::string(ref<MetadataIndex>()), escape);
    case Type::MetadataKeyframe:
        return std::string("*e") + performPathEscape(std::string(ref<MetadataIndex>()), escape);
    case Type::MetadataHashChild:
        return std::string("*c") + performPathEscape(std::string(ref<MetadataIndex>()), escape);
    case Type::MetadataSingleFlag:
        return std::string("*l") + performPathEscape(std::string(ref<MetadataIndex>()), escape);
    }

    Q_ASSERT(false);
    return std::string();
}

PathElement PathElement::fromString(const std::string &string)
{
    if (string.length() <= 1) {
        if (!string.empty() && string[0] == '`')
            return PathElement();
        return PathElement(Type::AnyString, string);
    }

    switch (string[0]) {
    case '#':
        try {
            std::size_t n = static_cast<std::size_t>(std::stoul(string.substr(1)));
            return PathElement(Type::Array, n);
        } catch (std::exception &) {
        }
        break;

    case '@':
        try {
            double n = std::stod(string.substr(1));
            if (FP::defined(n))
                return PathElement(Type::Keyframe, n);
        } catch (std::exception &) {
        }
        break;

    case '[':
        if (string.length() >= 3 && string.back() == ']') {
            try {
                MatrixIndex index;

                for (std::size_t begin = 1;;) {
                    std::size_t count = string.length() - 1 - begin;
                    if (count <= 0)
                        break;
                    std::size_t delim = string.find_first_of(":;,", begin, count);

                    if (delim == string.npos) {
                        index.emplace_back(static_cast<std::size_t>(std::stoul(
                                std::string(string, begin, count))));
                        break;
                    }

                    count = delim - begin;
                    index.emplace_back(static_cast<std::size_t>(std::stoul(
                            std::string(string, begin, count))));

                    begin += count + 1;
                }

                if (index.empty())
                    break;

                return PathElement(Type::Matrix, std::move(index));
            } catch (std::exception &) {
            }
            break;
        }
        break;

    case '*':
        if (string.length() > 2) {
            switch (string[1]) {
            case 'd':
            case 'D':
            case 'r':
            case 'R':
                return PathElement(Type::MetadataReal, string.substr(2));
            case 'i':
            case 'I':
                return PathElement(Type::MetadataInteger, string.substr(2));
            case 'b':
            case 'B':
                return PathElement(Type::MetadataBoolean, string.substr(2));
            case 's':
            case 'S':
                return PathElement(Type::MetadataString, string.substr(2));
            case 'n':
            case 'N':
                return PathElement(Type::MetadataBytes, string.substr(2));
            case 'a':
            case 'A':
                return PathElement(Type::MetadataArray, string.substr(2));
            case 't':
            case 'T':
                return PathElement(Type::MetadataMatrix, string.substr(2));
            case 'e':
            case 'E':
                return PathElement(Type::MetadataKeyframe, string.substr(2));
            case 'h':
            case 'H':
                return PathElement(Type::MetadataHash, string.substr(2));
            case 'c':
            case 'C':
                return PathElement(Type::MetadataHashChild, string.substr(2));
            case 'f':
            case 'F':
                return PathElement(Type::MetadataFlags, string.substr(2));
            case 'l':
            case 'L':
                return PathElement(Type::MetadataSingleFlag, string.substr(2));
            default:
                break;
            }
        }
        break;

    case '^':
        return PathElement(Type::AnyMetadata, string.substr(1));
    case '&':
        return PathElement(Type::Hash, string.substr(1));

    default:
        break;
    }

    return PathElement(Type::AnyString, string);
}

PathElement PathElement::fromString(std::string &&string)
{
    if (string.length() <= 1) {
        if (!string.empty() && string[0] == '`')
            return PathElement();
        return PathElement(Type::AnyString, std::move(string));
    }

    switch (string[0]) {
    case '#':
        try {
            std::size_t n = static_cast<std::size_t>(std::stoul(string.substr(1)));
            return PathElement(Type::Array, n);
        } catch (std::exception &) {
        }
        break;

    case '@':
        try {
            double n = std::stod(string.substr(1));
            if (FP::defined(n))
                return PathElement(Type::Keyframe, n);
        } catch (std::exception &) {
        }
        break;

    case '[':
        if (string.length() >= 3 && string.back() == ']') {
            try {
                MatrixIndex index;

                for (std::size_t begin = 1;;) {
                    std::size_t count = string.length() - 1 - begin;
                    if (count <= 0)
                        break;
                    std::size_t delim = string.find_first_of(":;,", begin, count);

                    if (delim == string.npos) {
                        index.emplace_back(static_cast<std::size_t>(std::stoul(
                                std::string(string, begin, count))));
                        break;
                    }

                    count = delim - begin;
                    index.emplace_back(static_cast<std::size_t>(std::stoul(
                            std::string(string, begin, count))));

                    begin += count + 1;
                }

                if (index.empty())
                    break;

                return PathElement(Type::Matrix, std::move(index));
            } catch (std::exception &) {
            }
            break;
        }
        break;

    case '*':
        if (string.length() > 2) {
            switch (string[1]) {
            case 'd':
            case 'D':
            case 'r':
            case 'R':
                return PathElement(Type::MetadataReal, string.substr(2));
            case 'i':
            case 'I':
                return PathElement(Type::MetadataInteger, string.substr(2));
            case 'b':
            case 'B':
                return PathElement(Type::MetadataBoolean, string.substr(2));
            case 's':
            case 'S':
                return PathElement(Type::MetadataString, string.substr(2));
            case 'n':
            case 'N':
                return PathElement(Type::MetadataBytes, string.substr(2));
            case 'a':
            case 'A':
                return PathElement(Type::MetadataArray, string.substr(2));
            case 't':
            case 'T':
                return PathElement(Type::MetadataMatrix, string.substr(2));
            case 'e':
            case 'E':
                return PathElement(Type::MetadataKeyframe, string.substr(2));
            case 'h':
            case 'H':
                return PathElement(Type::MetadataHash, string.substr(2));
            case 'c':
            case 'C':
                return PathElement(Type::MetadataHashChild, string.substr(2));
            case 'f':
            case 'F':
                return PathElement(Type::MetadataFlags, string.substr(2));
            case 'l':
            case 'L':
                return PathElement(Type::MetadataSingleFlag, string.substr(2));
            default:
                break;
            }
        }
        break;

    case '^':
        return PathElement(Type::AnyMetadata, string.substr(1));
    case '&':
        return PathElement(Type::Hash, string.substr(1));

    default:
        break;
    }

    return PathElement(Type::AnyString, string);
}

void PathElement::parsePath(const std::string &string, std::vector<PathElement> &path)
{
    Q_ASSERT(!string.empty());

    std::size_t start = 0;
    if (string[0] == '/') {
        path.clear();
        ++start;
    }

    std::size_t length = string.length();

    for (;;) {
        for (; start < length && string[start] == '/'; start++) { };
        if (start >= length)
            return;

        std::size_t escapeCheck = start;
        std::string component(1, string[start]);
        ++start;
        bool startEscaped = false;
        while (start < length) {
            if (!component.empty() && start - 1 >= escapeCheck && string[start - 1] == '\\') {
                switch (string[start]) {
                case 'n':
                    component.back() = '\n';
                    break;
                case 'r':
                    component.back() = '\r';
                    break;
                case 't':
                    component.back() = '\t';
                    break;
                case '_':
                    if (component.length() == 1)
                        startEscaped = true;
                    component.pop_back();
                    break;
                default:
                    if (component.length() == 1)
                        startEscaped = true;
                    component.back() = string[start];
                    break;
                }
                ++start;
                escapeCheck = start;
                continue;
            }
            if (string[start] == '/') {
                ++start;
                break;
            }

            component.push_back(string[start]);
            ++start;
        }

        if (startEscaped) {
            path.emplace_back(Type::AnyString, std::move(component));
        } else if (component == "..") {
            if (!path.empty())
                path.pop_back();
        } else if (!component.empty() && component != ".") {
            path.emplace_back(fromString(std::move(component)));
        }
    }
}

std::vector<PathElement> PathElement::parse(const std::string &string,
                                            std::vector<PathElement> result)
{
    if (string.empty())
        return std::move(result);

    if (string.find_first_of("/\\") == string.npos) {
        if (string == "..") {
            if (!result.empty())
                result.pop_back();
            return std::move(result);
        } else if (string == ".") {
            return std::move(result);
        }
        result.emplace_back(fromString(string));
        return std::move(result);
    }

    parsePath(string, result);
    return std::move(result);
}

std::vector<PathElement> PathElement::parse(std::string &&string, std::vector<PathElement> result)
{
    if (string.empty())
        return std::move(result);

    if (string.find_first_of("/\\") == string.npos) {
        if (string == "..") {
            if (!result.empty())
                result.pop_back();
            return std::move(result);
        } else if (string == ".") {
            return std::move(result);
        }
        result.emplace_back(fromString(std::move(string)));
        return std::move(result);
    }

    parsePath(string, result);
    return std::move(result);
}

std::size_t PathElement::toArrayIndex() const
{
    if (type != Type::Array)
        return static_cast<std::size_t>(-1);
    return ref<ArrayIndex>();
}

PathElement::Encoded PathElement::encode() const
{
    switch (type) {
    case Type::Empty:
        return Encoded();
    case Type::Hash:
        return Encoded("hash", ref<HashIndex>());
    case Type::AnyString:
        return Encoded("string", ref<StringIndex>());
    case Type::AnyMetadata:
        return Encoded("metadata", ref<MetadataIndex>());
    case Type::Array:
        return Encoded("array", std::to_string(ref<ArrayIndex>()));
    case Type::Matrix: {
        std::string result;
        bool first = true;
        for (auto add : ref<MatrixIndex>()) {
            if (!first)
                result += ",";
            first = false;
            result += std::to_string(add);
        }
        return Encoded("matrix", std::move(result));
    }
    case Type::Keyframe: {
        std::ostringstream b;
        b << ref<KeyframeIndex>();
        return Encoded("keyframe", b.str());
    }
    case Type::MetadataReal:
        return Encoded("metareal", ref<MetadataIndex>());
    case Type::MetadataInteger:
        return Encoded("metainteger", ref<MetadataIndex>());
    case Type::MetadataBoolean:
        return Encoded("metaboolean", ref<MetadataIndex>());
    case Type::MetadataString:
        return Encoded("metastring", ref<MetadataIndex>());
    case Type::MetadataBytes:
        return Encoded("metabinary", ref<MetadataIndex>());
    case Type::MetadataFlags:
        return Encoded("metaflags", ref<MetadataIndex>());
    case Type::MetadataArray:
        return Encoded("metaarray", ref<MetadataIndex>());
    case Type::MetadataMatrix:
        return Encoded("metamatrix", ref<MetadataIndex>());
    case Type::MetadataHash:
        return Encoded("metahash", ref<MetadataIndex>());
    case Type::MetadataKeyframe:
        return Encoded("metakeyframe", ref<MetadataIndex>());
    case Type::MetadataHashChild:
        return Encoded("metahashkey", ref<MetadataHashChildIndex>());
    case Type::MetadataSingleFlag:
        return Encoded("metasingleflag", ref<MetadataSingleFlagIndex>());
    }

    Q_ASSERT(false);
    return Encoded();
}

PathElement PathElement::decode(const Encoded &encoded)
{
    if (encoded.first.empty())
        return PathElement();
    if (Util::equal_insensitive(encoded.first, "hash")) {
        return PathElement(Type::Hash, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "string")) {
        return PathElement(Type::AnyString, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "metadata")) {
        return PathElement(Type::AnyMetadata, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "array")) {
        try {
            std::size_t n = static_cast<std::size_t>(std::stoul(encoded.second));
            return PathElement(Type::Array, n);
        } catch (std::exception &) {
        }
        return PathElement();
    } else if (Util::equal_insensitive(encoded.first, "matrix")) {
        try {
            MatrixIndex index;

            for (std::size_t begin = 0;;) {
                std::size_t count = encoded.second.length() - begin;
                if (count <= 0)
                    break;
                std::size_t delim = encoded.second.find_first_of(":;,", begin, count);

                if (delim == encoded.second.npos) {
                    index.emplace_back(static_cast<std::size_t>(std::stoul(
                            std::string(encoded.second, begin, count))));
                    break;
                }

                count = delim - begin;
                index.emplace_back(static_cast<std::size_t>(std::stoul(
                        std::string(encoded.second, begin, count))));

                begin += count + 1;
            }

            if (index.empty())
                return PathElement();

            return PathElement(Type::Matrix, std::move(index));
        } catch (std::exception &) {
        }
        return PathElement();
    } else if (Util::equal_insensitive(encoded.first, "keyframe")) {
        try {
            double n = std::stod(encoded.second);
            if (FP::defined(n))
                return PathElement(Type::Keyframe, n);
        } catch (std::exception &) {
        }
        return PathElement();
    } else if (Util::equal_insensitive(encoded.first, "metareal", "metadouble", "metadatareal",
                                       "metadatadouble")) {
        return PathElement(Type::MetadataReal, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "metainteger", "metadatainteger")) {
        return PathElement(Type::MetadataInteger, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "metaboolean", "metabool", "metadataboolean",
                                       "metadataboolean")) {
        return PathElement(Type::MetadataBoolean, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "metastring", "metadatastring")) {
        return PathElement(Type::MetadataString, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "metabinary", "metabytes", "metadatabinary",
                                       "metadatabytes")) {
        return PathElement(Type::MetadataBytes, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "metaflags", "metadataflags")) {
        return PathElement(Type::MetadataFlags, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "metaarray", "metadataarray")) {
        return PathElement(Type::MetadataArray, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "metamatrix", "metadatamatrix")) {
        return PathElement(Type::MetadataMatrix, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "metahash", "metadatahash")) {
        return PathElement(Type::MetadataHash, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "metakeyframe", "metadatakeyframe")) {
        return PathElement(Type::MetadataKeyframe, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "metahashkey", "metahashchild",
                                       "metadatahashkey", "metadatahashchild")) {
        return PathElement(Type::MetadataHashChild, encoded.second);
    } else if (Util::equal_insensitive(encoded.first, "metasingleflag", "metadatasingleflah")) {
        return PathElement(Type::MetadataSingleFlag, encoded.second);
    }
    return PathElement();
}

PathElement PathElement::decode(Encoded &&encoded)
{
    if (encoded.first.empty())
        return PathElement();
    if (Util::equal_insensitive(encoded.first, "hash")) {
        return PathElement(Type::Hash, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "string")) {
        return PathElement(Type::AnyString, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "metadata")) {
        return PathElement(Type::AnyMetadata, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "array")) {
        try {
            std::size_t n = static_cast<std::size_t>(std::stoul(encoded.second));
            return PathElement(Type::Array, n);
        } catch (std::exception &) {
        }
        return PathElement();
    } else if (Util::equal_insensitive(encoded.first, "matrix")) {
        try {
            MatrixIndex index;

            for (std::size_t begin = 0;;) {
                std::size_t count = encoded.second.length() - begin;
                if (count <= 0)
                    break;
                std::size_t delim = encoded.second.find_first_of(":;,", begin, count);

                if (delim == encoded.second.npos) {
                    index.emplace_back(static_cast<std::size_t>(std::stoul(
                            std::string(encoded.second, begin, count))));
                    break;
                }

                count = delim - begin;
                index.emplace_back(static_cast<std::size_t>(std::stoul(
                        std::string(encoded.second, begin, count))));

                begin += count + 1;
            }

            if (index.empty())
                return PathElement();

            return PathElement(Type::Matrix, std::move(index));
        } catch (std::exception &) {
        }
        return PathElement();
    } else if (Util::equal_insensitive(encoded.first, "keyframe")) {
        try {
            double n = std::stod(encoded.second);
            if (FP::defined(n))
                return PathElement(Type::Keyframe, n);
        } catch (std::exception &) {
        }
        return PathElement();
    } else if (Util::equal_insensitive(encoded.first, "metareal", "metadouble", "metadatareal",
                                       "metadatadouble")) {
        return PathElement(Type::MetadataReal, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "metainteger", "metadatainteger")) {
        return PathElement(Type::MetadataInteger, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "metaboolean", "metabool", "metadataboolean",
                                       "metadataboolean")) {
        return PathElement(Type::MetadataBoolean, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "metastring", "metadatastring")) {
        return PathElement(Type::MetadataString, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "metabinary", "metabytes", "metadatabinary",
                                       "metadatabytes")) {
        return PathElement(Type::MetadataBytes, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "metaflags", "metadataflags")) {
        return PathElement(Type::MetadataFlags, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "metaarray", "metadataarray")) {
        return PathElement(Type::MetadataArray, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "metamatrix", "metadatamatrix")) {
        return PathElement(Type::MetadataMatrix, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "metahash", "metadatahash")) {
        return PathElement(Type::MetadataHash, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "metakeyframe", "metadatakeyframe")) {
        return PathElement(Type::MetadataKeyframe, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "metahashkey", "metahashchild",
                                       "metadatahashkey", "metadatahashchild")) {
        return PathElement(Type::MetadataHashChild, std::move(encoded.second));
    } else if (Util::equal_insensitive(encoded.first, "metasingleflag", "metadatasingleflah")) {
        return PathElement(Type::MetadataSingleFlag, std::move(encoded.second));
    }
    return PathElement();
}

PathElement PathElement::toMetadata(const PathElement &data)
{
    switch (data.type) {
    case Type::Empty:
        return PathElement();
    case Type::Hash:
        return PathElement(Type::MetadataHashChild, data.ref<HashIndex>());
    case Type::Array:
        return PathElement(Type::MetadataArray, "Children");
    case Type::Matrix:
        return PathElement(Type::MetadataMatrix, "Children");
    case Type::Keyframe:
        return PathElement(Type::MetadataKeyframe, "Children");
    case Type::AnyString:
    case Type::AnyMetadata:
    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
    case Type::MetadataHashChild:
    case Type::MetadataSingleFlag:
        return data;
    }

    Q_ASSERT(false);
    return PathElement();
}

PathElement PathElement::toMetadata(PathElement &&data)
{
    switch (data.type) {
    case Type::Empty:
        return PathElement();
    case Type::Hash:
        return PathElement(Type::MetadataHashChild, std::move(data.data.ref<HashIndex>()));
    case Type::Array:
        return PathElement(Type::MetadataArray, "Children");
    case Type::Matrix:
        return PathElement(Type::MetadataMatrix, "Children");
    case Type::Keyframe:
        return PathElement(Type::MetadataKeyframe, "Children");
    case Type::AnyString:
    case Type::AnyMetadata:
    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
    case Type::MetadataHashChild:
    case Type::MetadataSingleFlag:
        return std::move(data);
    }

    Q_ASSERT(false);
    return PathElement();
}

std::vector<PathElement> PathElement::toMetadata(const std::vector<PathElement> &data)
{
    std::vector<PathElement> result;
    for (const auto &add : data) {
        result.emplace_back(toMetadata(add));
    }
    return result;
}

std::vector<PathElement> PathElement::toMetadata(std::vector<PathElement> &&data)
{
    std::vector<PathElement> result;
    for (auto &add : data) {
        result.emplace_back(toMetadata(std::move(add)));
    }
    return result;
}

bool PathElement::equal(const PathElement &other) const
{
    if (type != other.type)
        return false;
    switch (type) {
    case Type::Empty:
        return true;

    case Type::AnyString:
        return ref<StringIndex>() == other.ref<StringIndex>();
    case Type::AnyMetadata:
        return ref<MetadataIndex>() == other.ref<MetadataIndex>();

    case Type::Hash:
        return ref<HashIndex>() == other.ref<HashIndex>();
    case Type::Array:
        return ref<ArrayIndex>() == other.ref<ArrayIndex>();
    case Type::Matrix:
        return ref<MatrixIndex>() == other.ref<MatrixIndex>();
    case Type::Keyframe:
        return ref<KeyframeIndex>() == other.ref<KeyframeIndex>();

    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
        return ref<MetadataIndex>() == other.ref<MetadataIndex>();

    case Type::MetadataHashChild:
        return ref<MetadataHashChildIndex>() == other.ref<MetadataHashChildIndex>();
    case Type::MetadataSingleFlag:
        return ref<MetadataSingleFlagIndex>() == other.ref<MetadataSingleFlagIndex>();
    }

    Q_ASSERT(false);
    return false;
}

void PathElement::swap(PathElement &a, PathElement &b)
{
    using std::swap;
    if (a.type == b.type) {
        switch (a.type) {
        case Type::Empty:
            break;

        case Type::AnyString:
            swap(a.data.ref<StringIndex>(), b.data.ref<StringIndex>());
            break;
        case Type::AnyMetadata:
            swap(a.data.ref<MetadataIndex>(), b.data.ref<MetadataIndex>());
            break;

        case Type::Hash:
            swap(a.data.ref<HashIndex>(), b.data.ref<HashIndex>());
            break;
        case Type::Array:
            swap(a.data.ref<ArrayIndex>(), b.data.ref<ArrayIndex>());
            break;
        case Type::Matrix:
            swap(a.data.ref<MatrixIndex>(), b.data.ref<MatrixIndex>());
            break;
        case Type::Keyframe:
            swap(a.data.ref<KeyframeIndex>(), b.data.ref<KeyframeIndex>());
            break;

        case Type::MetadataReal:
        case Type::MetadataInteger:
        case Type::MetadataBoolean:
        case Type::MetadataString:
        case Type::MetadataBytes:
        case Type::MetadataFlags:
        case Type::MetadataArray:
        case Type::MetadataMatrix:
        case Type::MetadataHash:
        case Type::MetadataKeyframe:
            swap(a.data.ref<MetadataIndex>(), b.data.ref<MetadataIndex>());
            break;

        case Type::MetadataHashChild:
            swap(a.data.ref<MetadataHashChildIndex>(), b.data.ref<MetadataHashChildIndex>());
            break;
        case Type::MetadataSingleFlag:
            swap(a.data.ref<MetadataSingleFlagIndex>(), b.data.ref<MetadataSingleFlagIndex>());
            break;
        }
        return;
    }

    PathElement temp = std::move(a);
    a = std::move(b);
    b = std::move(temp);
}

std::size_t PathElement::hash() const
{
    std::size_t result = 0;
    switch (type) {
    case Type::Empty:
        result = 0;
        break;

    case Type::AnyString:
        result = INTEGER::mix(1, std::hash<StringIndex>()(ref<StringIndex>()));
        break;
    case Type::AnyMetadata:
        result = INTEGER::mix(2, std::hash<MetadataIndex>()(ref<MetadataIndex>()));
        break;

    case Type::Hash:
        result = INTEGER::mix(3, std::hash<HashIndex>()(ref<HashIndex>()));
        break;
    case Type::Array:
        result = INTEGER::mix(4, std::hash<ArrayIndex>()(ref<ArrayIndex>()));
        break;
    case Type::Matrix:
        result = 5;
        for (auto add : ref<MatrixIndex>()) {
            result = INTEGER::mix(result, add);
        }
        break;
    case Type::Keyframe:
        result = INTEGER::mix(6, std::hash<KeyframeIndex>()(ref<KeyframeIndex>()));
        break;

    case Type::MetadataReal:
        result = INTEGER::mix(7, std::hash<MetadataIndex>()(ref<MetadataIndex>()));
        break;
    case Type::MetadataInteger:
        result = INTEGER::mix(8, std::hash<MetadataIndex>()(ref<MetadataIndex>()));
        break;
    case Type::MetadataBoolean:
        result = INTEGER::mix(9, std::hash<MetadataIndex>()(ref<MetadataIndex>()));
        break;
    case Type::MetadataString:
        result = INTEGER::mix(10, std::hash<MetadataIndex>()(ref<MetadataIndex>()));
        break;
    case Type::MetadataBytes:
        result = INTEGER::mix(11, std::hash<MetadataIndex>()(ref<MetadataIndex>()));
        break;
    case Type::MetadataFlags:
        result = INTEGER::mix(12, std::hash<MetadataIndex>()(ref<MetadataIndex>()));
        break;
    case Type::MetadataArray:
        result = INTEGER::mix(13, std::hash<MetadataIndex>()(ref<MetadataIndex>()));
        break;
    case Type::MetadataMatrix:
        result = INTEGER::mix(14, std::hash<MetadataIndex>()(ref<MetadataIndex>()));
        break;
    case Type::MetadataHash:
        result = INTEGER::mix(15, std::hash<MetadataIndex>()(ref<MetadataIndex>()));
        break;
    case Type::MetadataKeyframe:
        result = INTEGER::mix(16, std::hash<MetadataIndex>()(ref<MetadataIndex>()));
        break;

    case Type::MetadataHashChild:
        result = INTEGER::mix(17,
                              std::hash<MetadataHashChildIndex>()(ref<MetadataHashChildIndex>()));
        break;
    case Type::MetadataSingleFlag:
        result = INTEGER::mix(18,
                              std::hash<MetadataSingleFlagIndex>()(ref<MetadataSingleFlagIndex>()));
        break;
    }
    return result;
}

static int typeCompareID(PathElement::Type type)
{
    switch (type) {
    case PathElement::Type::Empty:
        return 0;
    case PathElement::Type::AnyString:
        return 1;
    case PathElement::Type::AnyMetadata:
        return 2;
    case PathElement::Type::Array:
        return 3;
    case PathElement::Type::Matrix:
        return 4;
    case PathElement::Type::Keyframe:
        return 5;
    case PathElement::Type::Hash:
        return 6;
    case PathElement::Type::MetadataReal:
        return 7;
    case PathElement::Type::MetadataInteger:
        return 8;
    case PathElement::Type::MetadataBoolean:
        return 9;
    case PathElement::Type::MetadataString:
        return 10;
    case PathElement::Type::MetadataBytes:
        return 11;
    case PathElement::Type::MetadataFlags:
        return 12;
    case PathElement::Type::MetadataArray:
        return 13;
    case PathElement::Type::MetadataMatrix:
        return 14;
    case PathElement::Type::MetadataHash:
        return 15;
    case PathElement::Type::MetadataKeyframe:
        return 16;
    case PathElement::Type::MetadataHashChild:
        return 17;
    case PathElement::Type::MetadataSingleFlag:
        return 18;
    }
    Q_ASSERT(false);
    return -1;
}

bool PathElement::OrderLogical::operator()(const PathElement &a, const PathElement &b) const
{
    if (a.type != b.type) {
        return typeCompareID(a.type) < typeCompareID(b.type);
    }

    Q_ASSERT(a.type == b.type);
    switch (a.type) {
    case Type::Empty:
        return false;
    case Type::AnyString:
        return a.ref<PathElement::StringIndex>() < b.ref<PathElement::StringIndex>();
    case Type::AnyMetadata:
        return a.ref<PathElement::MetadataIndex>() < b.ref<PathElement::MetadataIndex>();
    case Type::Hash:
        return a.ref<PathElement::HashIndex>() < b.ref<PathElement::HashIndex>();
    case Type::Array:
        return a.ref<PathElement::ArrayIndex>() < b.ref<PathElement::ArrayIndex>();
    case Type::Matrix: {
        const auto &ai = a.ref<PathElement::MatrixIndex>();
        const auto &bi = b.ref<PathElement::MatrixIndex>();
        if (ai.size() != bi.size())
            return ai.size() < bi.size();
        auto bci = bi.begin();
        for (auto check : ai) {
            if (check != *bci)
                return check < *bci;
            ++bci;
        }
        return false;
    }
    case Type::Keyframe:
        return a.ref<PathElement::KeyframeIndex>() < b.ref<PathElement::KeyframeIndex>();
    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataFlags:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataHash:
    case Type::MetadataKeyframe:
    case Type::MetadataHashChild:
    case Type::MetadataSingleFlag:
        return a.ref<PathElement::MetadataIndex>() < b.ref<PathElement::MetadataIndex>();
    }

    Q_ASSERT(false);
    return false;
}

enum {
    Serialize_Empty = 0,
    Serialize_AnyString,
    Serialize_AnyMetadata,
    Serialize_Hash,
    Serialize_Array,
    Serialize_Matrix,
    Serialize_Keyframe,
    Serialize_MetadataReal,
    Serialize_MetadataInteger,
    Serialize_MetadataBoolean,
    Serialize_MetadataString,
    Serialize_MetadataBytes,
    Serialize_MetadataFlags,
    Serialize_MetadataArray,
    Serialize_MetadataMatrix,
    Serialize_MetadataHash,
    Serialize_MetadataKeyframe,
    Serialize_MetadataHashChild,
    Serialize_MetadataSingleFlag,
};

QDataStream &operator<<(QDataStream &stream, const PathElement &value)
{
    switch (value.type) {
    case PathElement::Type::Empty:
        stream << static_cast<quint8>(Serialize_Empty);
        break;

    case PathElement::Type::AnyString:
        stream << static_cast<quint8>(Serialize_AnyString) << value.ref<PathElement::StringIndex>();
        break;
    case PathElement::Type::AnyMetadata:
        stream << static_cast<quint8>(Serialize_AnyMetadata)
               << value.ref<PathElement::MetadataIndex>();
        break;

    case PathElement::Type::Hash:
        stream << static_cast<quint8>(Serialize_Hash) << value.ref<PathElement::HashIndex>();
        break;
    case PathElement::Type::Array:
        stream << static_cast<quint8>(Serialize_Array)
               << static_cast<quint32>(value.ref<PathElement::ArrayIndex>());
        break;
    case PathElement::Type::Matrix:
        stream << static_cast<quint8>(Serialize_Matrix);
        Serialize::container(stream, value.ref<PathElement::MatrixIndex>(),
                             [&stream](std::size_t i) {
                                 stream << static_cast<quint32>(i);
                             });
        break;
    case PathElement::Type::Keyframe:
        stream << static_cast<quint8>(Serialize_Keyframe)
               << value.ref<PathElement::KeyframeIndex>();
        break;
    case PathElement::Type::MetadataReal:
        stream << static_cast<quint8>(Serialize_MetadataReal)
               << value.ref<PathElement::MetadataIndex>();
        break;

    case PathElement::Type::MetadataInteger:
        stream << static_cast<quint8>(Serialize_MetadataInteger)
               << value.ref<PathElement::MetadataIndex>();
        break;
    case PathElement::Type::MetadataBoolean:
        stream << static_cast<quint8>(Serialize_MetadataBoolean)
               << value.ref<PathElement::MetadataIndex>();
        break;
    case PathElement::Type::MetadataString:
        stream << static_cast<quint8>(Serialize_MetadataString)
               << value.ref<PathElement::MetadataIndex>();
        break;
    case PathElement::Type::MetadataBytes:
        stream << static_cast<quint8>(Serialize_MetadataBytes)
               << value.ref<PathElement::MetadataIndex>();
        break;
    case PathElement::Type::MetadataFlags:
        stream << static_cast<quint8>(Serialize_MetadataFlags)
               << value.ref<PathElement::MetadataIndex>();
        break;
    case PathElement::Type::MetadataArray:
        stream << static_cast<quint8>(Serialize_MetadataArray)
               << value.ref<PathElement::MetadataIndex>();
        break;
    case PathElement::Type::MetadataMatrix:
        stream << static_cast<quint8>(Serialize_MetadataMatrix)
               << value.ref<PathElement::MetadataIndex>();
        break;
    case PathElement::Type::MetadataHash:
        stream << static_cast<quint8>(Serialize_MetadataHash)
               << value.ref<PathElement::MetadataIndex>();
        break;
    case PathElement::Type::MetadataKeyframe:
        stream << static_cast<quint8>(Serialize_MetadataKeyframe)
               << value.ref<PathElement::MetadataIndex>();
        break;

    case PathElement::Type::MetadataHashChild:
        stream << static_cast<quint8>(Serialize_MetadataHashChild)
               << value.ref<PathElement::MetadataHashChildIndex>();
        break;
    case PathElement::Type::MetadataSingleFlag:
        stream << static_cast<quint8>(Serialize_MetadataSingleFlag)
               << value.ref<PathElement::MetadataSingleFlagIndex>();
        break;
    }
    return stream;
}

namespace {
template<typename T>
T deserializeSimple(QDataStream &stream)
{
    T result;
    stream >> result;
    return result;
}
}

QDataStream &operator>>(QDataStream &stream, PathElement &value)
{
    quint8 type = 0;
    stream >> type;
    switch (type) {
    case Serialize_Empty:
        value = PathElement();
        break;

    case Serialize_AnyString:
        value = PathElement(PathElement::Type::AnyString,
                            deserializeSimple<PathElement::StringIndex>(stream));
        break;
    case Serialize_AnyMetadata:
        value = PathElement(PathElement::Type::AnyMetadata,
                            deserializeSimple<PathElement::MetadataIndex>(stream));
        break;

    case Serialize_Hash:
        value = PathElement(PathElement::Type::Hash,
                            deserializeSimple<PathElement::HashIndex>(stream));
        break;
    case Serialize_Array:
        value = PathElement(PathElement::Type::AnyString,
                            static_cast<PathElement::ArrayIndex>(deserializeSimple<quint32>(
                                    stream)));
        break;

    case Serialize_Matrix: {
        PathElement::MatrixIndex index;
        Deserialize::container(stream, index, [&stream] {
            quint32 i = 0;
            stream >> i;
            return static_cast<std::size_t>(i);
        });
        value = PathElement(PathElement::Type::Matrix, std::move(index));
        break;
    }
    case Serialize_Keyframe:
        value = PathElement(PathElement::Type::Keyframe,
                            deserializeSimple<PathElement::KeyframeIndex>(stream));
        break;

    case Serialize_MetadataReal:
        value = PathElement(PathElement::Type::MetadataReal,
                            deserializeSimple<PathElement::MetadataIndex>(stream));
        break;
    case Serialize_MetadataInteger:
        value = PathElement(PathElement::Type::MetadataInteger,
                            deserializeSimple<PathElement::MetadataIndex>(stream));
        break;
    case Serialize_MetadataBoolean:
        value = PathElement(PathElement::Type::MetadataBoolean,
                            deserializeSimple<PathElement::MetadataIndex>(stream));
        break;
    case Serialize_MetadataString:
        value = PathElement(PathElement::Type::MetadataString,
                            deserializeSimple<PathElement::MetadataIndex>(stream));
        break;
    case Serialize_MetadataBytes:
        value = PathElement(PathElement::Type::MetadataBytes,
                            deserializeSimple<PathElement::MetadataIndex>(stream));
        break;
    case Serialize_MetadataFlags:
        value = PathElement(PathElement::Type::MetadataFlags,
                            deserializeSimple<PathElement::MetadataIndex>(stream));
        break;
    case Serialize_MetadataArray:
        value = PathElement(PathElement::Type::MetadataArray,
                            deserializeSimple<PathElement::MetadataIndex>(stream));
        break;
    case Serialize_MetadataMatrix:
        value = PathElement(PathElement::Type::MetadataMatrix,
                            deserializeSimple<PathElement::MetadataIndex>(stream));
        break;
    case Serialize_MetadataHash:
        value = PathElement(PathElement::Type::MetadataHash,
                            deserializeSimple<PathElement::MetadataIndex>(stream));
        break;
    case Serialize_MetadataKeyframe:
        value = PathElement(PathElement::Type::MetadataKeyframe,
                            deserializeSimple<PathElement::MetadataSingleFlagIndex>(stream));
        break;

    case Serialize_MetadataHashChild:
        value = PathElement(PathElement::Type::MetadataHashChild,
                            deserializeSimple<PathElement::MetadataHashChildIndex>(stream));
        break;
    case Serialize_MetadataSingleFlag:
        value = PathElement(PathElement::Type::MetadataSingleFlag,
                            deserializeSimple<PathElement::MetadataHashChildIndex>(stream));
        break;

    default:
        qCWarning(log_datacore_variant_path) << "Unrecognized serialization type" << type;
        stream.setStatus(QDataStream::ReadCorruptData);
        Q_ASSERT(false);
        value = PathElement();
        break;
    }

    return stream;
}

QDebug operator<<(QDebug stream, const PathElement &value)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    switch (value.type) {
    case PathElement::Type::Empty:
        stream << '`';
        break;
    case PathElement::Type::Hash:
        stream << '&'
               << performPathEscape(std::string(value.ref<PathElement::HashIndex>()), true).data();
        break;
    default:
        stream << value.toString().data();
        break;
    }
    return stream;
}

QDebug operator<<(QDebug stream, const Path &value)
{
    QDebugStateSaver saver(stream);
    stream.nospace();

    stream.maybeQuote();
    for (const auto &add : value) {
        stream << '/' << add;
    }
    stream.maybeQuote();
    return stream;
}

}
}
}