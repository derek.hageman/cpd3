/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <sstream>

#include "keyframe.hxx"
#include "overlay.hxx"
#include "serialization.hxx"
#include "core/util.hxx"


namespace CPD3 {
namespace Data {
namespace Variant {

NodeKeyframe::~NodeKeyframe() = default;

NodeKeyframe::NodeKeyframe() = default;

NodeKeyframe::NodeKeyframe(const NodeKeyframe &other) : children(other.children)
{
    for (auto &mod : children) {
        mod.second->detach(mod.second);
    }
}

NodeKeyframe::NodeKeyframe(Children &&children) : children(std::move(children))
{ }

NodeKeyframe::NodeKeyframe(QDataStream &stream)
{
    std::size_t n = Serialization::deserializeShortLength(stream);
    for (std::size_t i = 0; i < n; ++i) {
        double key = 0;
        stream >> key;
        children.emplace(key, Serialization::construct(stream));
    }
}

void NodeKeyframe::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_Keyframe_v2);
    Serialization::serializeShortLength(stream, children.size());
    for (const auto &child : children) {
        stream << child.first;
        child.second->serialize(stream);
    }
}

Node::Type NodeKeyframe::type() const
{ return Node::Type::Keyframe; }

void NodeKeyframe::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeKeyframeConstant>(*this); }

std::shared_ptr<Node> NodeKeyframe::read(const PathElement &path) const
{
    auto point = children.end();
    switch (path.type) {
    case PathElement::Type::Empty:
        point = children.begin();
        break;
    case PathElement::Type::Keyframe:
        point = children.find(path.ref<PathElement::KeyframeIndex>());
        break;
    default:
        break;
    }
    if (point == children.end())
        return std::shared_ptr<Node>();
    return point->second;
}

bool NodeKeyframe::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::Keyframe; }

bool NodeKeyframe::childrenWritable(PathElement::Type type) const
{
    switch (type) {
    case PathElement::Type::Empty:
    case PathElement::Type::Keyframe:
        return true;
    default:
        break;
    }
    return false;
}

std::shared_ptr<Node> NodeKeyframe::write(const PathElement &path, Node::WriteGoal final)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto child = children.begin();
        if (child == children.end()) {
            child = children.emplace(0.0, create(final)).first;
        } else if (!child->second->compatible(final)) {
            child->second = create(final, child->second.get());
        }
        return child->second;
    }
    case PathElement::Type::Keyframe: {
        PathElement::KeyframeIndex index = path.ref<PathElement::KeyframeIndex>();
        Q_ASSERT(FP::defined(index));
        auto &child = children[index];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    default:
        Q_ASSERT(false);
        break;
    }
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeKeyframe::write(const PathElement &path, PathElement::Type next)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto child = children.begin();
        if (child == children.end()) {
            child = children.emplace(0.0, create(next)).first;
        } else if (!child->second->childrenWritable(next)) {
            child->second = create(next, child->second.get());
        }
        return child->second;
    }
    case PathElement::Type::Keyframe: {
        PathElement::KeyframeIndex index = path.ref<PathElement::KeyframeIndex>();
        Q_ASSERT(FP::defined(index));
        auto &child = children[index];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    default:
        Q_ASSERT(false);
        break;
    }
    return std::shared_ptr<Node>();
}

void NodeKeyframe::insert(const PathElement &path, const std::shared_ptr<Node> &final)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto child = children.begin();
        if (child == children.end()) {
            child = children.emplace(0.0, final).first;
        } else {
            child->second = final;
        }
        child->second->detach(child->second);
        break;
    }
    case PathElement::Type::Keyframe: {
        PathElement::KeyframeIndex index = path.ref<PathElement::KeyframeIndex>();
        Q_ASSERT(FP::defined(index));
        auto &child = children[index];
        child = final;
        child->detach(child);
        break;
    }
    default:
        Q_ASSERT(false);
        break;
    }
}

void NodeKeyframe::remove(const PathElement &path)
{
    switch (path.type) {
    case PathElement::Type::Empty:
        if (!children.empty())
            children.erase(children.begin());
        break;
    case PathElement::Type::Keyframe:
        children.erase(path.ref<PathElement::KeyframeIndex>());
        break;
    default:
        Q_ASSERT(false);
        break;
    }
}

bool NodeKeyframe::bypassFirstOverlay(const Node &over) const
{
    switch (over.type()) {
    case Node::Type::Keyframe:
    case Node::Type::KeyframeConstant:
        break;
    default:
        return Node::bypassFirstOverlay(over);
    }
    return over.bypassSecondOverlay();
}

bool NodeKeyframe::bypassSecondOverlay() const
{
    for (const auto &child : children) {
        if (!child.second->bypassSecondOverlay())
            return false;
    }
    return true;
}

void NodeKeyframe::overlayFirst(NodeKeyframe &under, const NodeKeyframe &over)
{
    Q_ASSERT(under.type() == Node::Type::Keyframe);

    under.children.clear();
    for (const auto &add : over.children) {
        Overlay::first(under.children[add.first], add.second);
    }
}

void NodeKeyframe::overlaySecond(NodeKeyframe &under,
                                 const NodeKeyframe &over,
                                 const TopLevel &origin,
                                 const Path &path,
                                 uint_fast16_t depth)
{
    Q_ASSERT(under.type() == Node::Type::Keyframe);

    /* First, recurse the second stage */
    Path childPath = path;
    for (const auto &add : over.children) {
        childPath.emplace_back(PathElement::Type::Keyframe, add.first);
        Overlay::second(under.children[add.first], add.second, origin, childPath, depth);
        childPath.pop_back();
    }

    /* Now detach and make constant all children, so
     * future overlays can re-use them */
    for (auto &child : under.children) {
        child.second->detach(child.second);
    }
}

std::string NodeKeyframe::describe() const
{
    std::stringstream str;
    str << "Keyframe(";
    bool first = true;

    for (const auto &child : children) {
        if (!first)
            str << ",";
        first = false;

        str << child.first << "=" << child.second->describe();
    }
    str << ")";
    return str.str();
}

void NodeKeyframe::clear()
{ children.clear(); }

bool NodeKeyframe::identicalTo(const Node &other) const
{
    switch (other.type()) {
    case Type::Keyframe:
    case Type::KeyframeConstant:
        break;
    default:
        return false;
    }
    const auto &otherk = static_cast<const NodeKeyframe &>(other);
    if (children.size() != otherk.children.size())
        return false;
    for (const auto &child : children) {
        auto check = otherk.children.find(child.first);
        if (check == otherk.children.end())
            return false;
        if (!child.second->identicalTo(*check->second))
            return false;
    }
    return true;
}

bool NodeKeyframe::equalTo(const Node &other, const Node &metadata) const
{
    switch (other.type()) {
    case Type::Keyframe:
    case Type::KeyframeConstant:
        break;
    default:
        return false;
    }

    const auto &otherk = static_cast<const NodeKeyframe &>(other);
    if (children.size() != otherk.children.size())
        return false;

    auto nextMeta = metadata.read({PathElement::Type::MetadataKeyframe, "Children"});
    if (!nextMeta)
        nextMeta = NodeEmpty::instance;

    for (const auto &child : children) {
        auto check = otherk.children.find(child.first);
        if (check == otherk.children.end())
            return false;

        if (!child.second->equalTo(*check->second, *nextMeta))
            return false;
    }
    return true;
}

std::size_t NodeKeyframe::hash() const
{
    std::size_t result = Serialization::ID_Keyframe_v2;
    result = INTEGER::mix(result, children.size());
    for (const auto &add : children) {
        result = INTEGER::mix(result, std::hash<PathElement::KeyframeIndex>()(add.first));
        result = INTEGER::mix(result, add.second->hash());
    }
    return result;
}


NodeKeyframeConstant::~NodeKeyframeConstant() = default;

NodeKeyframeConstant::NodeKeyframeConstant() = default;

NodeKeyframeConstant::NodeKeyframeConstant(const NodeKeyframe &other) : NodeKeyframe(other)
{ }

NodeKeyframeConstant::NodeKeyframeConstant(Children &&children) : NodeKeyframe(std::move(children))
{ }

NodeKeyframeConstant::NodeKeyframeConstant(QDataStream &stream) : NodeKeyframe(stream)
{ }

Node::Type NodeKeyframeConstant::type() const
{ return Node::Type::KeyframeConstant; }

void NodeKeyframeConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeKeyframeConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeKeyframeConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeKeyframeConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeKeyframeConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeKeyframeConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeKeyframeConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeKeyframeConstant::clear()
{ Q_ASSERT(false); }


}
}
}