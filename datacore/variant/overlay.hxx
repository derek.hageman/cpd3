/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_OVERLAY_HXX
#define CPD3DATACOREVARIANT_OVERLAY_HXX

#include "core/first.hxx"

#include <string>
#include <QDataStream>

#include "datacore/datacore.hxx"
#include "node.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

class TopLevel;

/**
 * A node containing an overlay parse point.
 */
class CPD3DATACORE_EXPORT NodeOverlay : public Node {
    std::string value;
public:
    virtual ~NodeOverlay();

    NodeOverlay();

    NodeOverlay(const NodeOverlay &other);

    explicit NodeOverlay(const std::string &value);

    explicit NodeOverlay(std::string &&value);

    explicit NodeOverlay(QDataStream &stream);

    Type type() const override;

    void serialize(QDataStream &stream) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool bypassSecondOverlay() const override;

    std::string describe() const override;

    inline const std::string &getOverlayPath() const
    { return value; }

    const std::string &toString() const override;

    void setOverlayPath(const std::string &value);

    void setOverlayPath(std::string &&value);

    void clear() override;

    bool identicalTo(const Node &other) const override;

    std::size_t hash() const override;
};

/**
 * An overlay node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeOverlayConstant : public NodeOverlay {
public:
    virtual ~NodeOverlayConstant();

    NodeOverlayConstant();

    explicit NodeOverlayConstant(const NodeOverlay &other);

    explicit NodeOverlayConstant(const std::string &value);

    explicit NodeOverlayConstant(std::string &&value);

    explicit NodeOverlayConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    void clear() override;
};


namespace Overlay {

/**
 * Do the first stage of an overlay operation.
 *
 * @param under     the target data
 * @param over      the data to overlay
 */
CPD3DATACORE_EXPORT void first(TopLevel &under, const TopLevel &over);

/**
 * Do the first stage of an overlay operation.
 *
 * @param under     the target data
 * @param over      the data to overlay
 */
CPD3DATACORE_EXPORT void first(std::shared_ptr<Node> &under, const std::shared_ptr<Node> &over);

/**
 * Do the second stage of an overlay operation.
 *
 * @param under     the target data
 * @param over      the data to overlay
 */
CPD3DATACORE_EXPORT void second(TopLevel &under, const TopLevel &over);

/**
 * Do the second stage of an overlay operation.
 *
 * @param under     the target data
 * @param over      the data to overlay
 * @param origin    the origin of the underlay
 * @param path      the current path in the underlay
 * @param depth     the depth counter for recursion limiting
 */
CPD3DATACORE_EXPORT void second(std::shared_ptr<Node> &under,
                                const std::shared_ptr<Node> &over,
                                const TopLevel &origin,
                                const Path &path,
                                std::uint_fast16_t depth = 0);

}

}
}
}

#endif //CPD3DATACOREVARIANT_OVERLAY_HXX
